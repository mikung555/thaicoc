/*
 Navicat Premium Data Transfer

 Source Server         : MySQL
 Source Server Type    : MySQL
 Source Server Version : 50534
 Source Host           : localhost
 Source Database       : dmp

 Target Server Type    : MySQL
 Target Server Version : 50534
 File Encoding         : utf-8

 Date: 10/14/2015 14:25:51 PM
*/

SET NAMES utf8;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `rbac_auth_item_child`
-- ----------------------------
DROP TABLE IF EXISTS `rbac_auth_item_child`;
CREATE TABLE `rbac_auth_item_child` (
  `parent` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `child` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`parent`,`child`),
  KEY `child` (`child`),
  CONSTRAINT `rbac_auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `rbac_auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `rbac_auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `rbac_auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

-- ----------------------------
--  Records of `rbac_auth_item_child`
-- ----------------------------
BEGIN;
INSERT INTO `rbac_auth_item_child` VALUES ('fullPage', '/act-category/*'), ('administrator', '/admin/*'), ('fullPage', '/admin/*'), ('fullPage', '/article-category/*'), ('fullPage', '/article/create'), ('fullPage', '/article/delete'), ('fullPage', '/article/index'), ('fullPage', '/article/research/*'), ('fullPage', '/article/update'), ('fullPage', '/cache/*'), ('fullPage', '/category/*'), ('fullPage', '/component/*'), ('fullPage', '/configmode/*'), ('administrator', '/debug/*'), ('fullPage', '/debug/*'), ('fullPage', '/dynagrid/*'), ('fullPage', '/edat/*'), ('fullPage', '/ezforms/*'), ('fullPage', '/field/*'), ('fullPage', '/file-manager-elfinder/*'), ('fullPage', '/file-storage/*'), ('fullPage', '/file-upload/*'), ('administrator', '/gii/*'), ('fullPage', '/gii/*'), ('fullPage', '/gridview/*'), ('fullPage', '/guide/*'), ('fullPage', '/i18n/*'), ('fullPage', '/inputdata/*'), ('fullPage', '/key-storage/*'), ('administrator', '/log/*'), ('fullPage', '/log/*'), ('fullPage', '/manage-data/*'), ('fullPage', '/managedata/*'), ('fullPage', '/page/*'), ('fullPage', '/report-dynamic/*'), ('fullPage', '/report/*'), ('administrator', '/sign-in/*'), ('fullPage', '/sign-in/*'), ('usePage', '/sign-in/*'), ('administrator', '/site/*'), ('fullPage', '/site/*'), ('usePage', '/site/*'), ('user', '/site/*'), ('fullPage', '/system-information/*'), ('fullPage', '/tbl-tree/*'), ('usePage', '/tbl-tree/*'), ('fullPage', '/timeline-event/*'), ('fullPage', '/treemanager/*'), ('administrator', '/user/*'), ('fullPage', '/user/*'), ('fullPage', '/verification/*'), ('fullPage', '/view-data/*'), ('fullPage', '/widget-backend-menu/*'), ('fullPage', '/widget-carousel-item/*'), ('fullPage', '/widget-carousel/*'), ('fullPage', '/widget-menu/*'), ('fullPage', '/widget-text/*'), ('user', 'fullPage'), ('manager', 'loginToBackend'), ('administrator', 'manager'), ('manager', 'user');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
