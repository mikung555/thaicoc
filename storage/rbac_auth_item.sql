/*
 Navicat Premium Data Transfer

 Source Server         : MySQL
 Source Server Type    : MySQL
 Source Server Version : 50534
 Source Host           : localhost
 Source Database       : dmp

 Target Server Type    : MySQL
 Target Server Version : 50534
 File Encoding         : utf-8

 Date: 10/14/2015 14:25:59 PM
*/

SET NAMES utf8;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `rbac_auth_item`
-- ----------------------------
DROP TABLE IF EXISTS `rbac_auth_item`;
CREATE TABLE `rbac_auth_item` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `type` int(11) NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `rule_name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` text COLLATE utf8_unicode_ci,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`),
  KEY `rule_name` (`rule_name`),
  KEY `idx-auth_item-type` (`type`),
  CONSTRAINT `rbac_auth_item_ibfk_1` FOREIGN KEY (`rule_name`) REFERENCES `rbac_auth_rule` (`name`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

-- ----------------------------
--  Records of `rbac_auth_item`
-- ----------------------------
BEGIN;
INSERT INTO `rbac_auth_item` VALUES ('/act-category/*', '2', null, null, null, '1444805521', '1444805521'), ('/admin/*', '2', null, null, null, '1444805342', '1444805342'), ('/article-category/*', '2', null, null, null, '1444805552', '1444805552'), ('/article/create', '2', null, null, null, '1444805577', '1444805577'), ('/article/delete', '2', null, null, null, '1444805600', '1444805600'), ('/article/index', '2', null, null, null, '1444805572', '1444805572'), ('/article/research/*', '2', null, null, null, '1444805455', '1444805455'), ('/article/update', '2', null, null, null, '1444805596', '1444805596'), ('/cache/*', '2', null, null, null, '1444805605', '1444805605'), ('/category/*', '2', null, null, null, '1444805629', '1444805629'), ('/component/*', '2', null, null, null, '1444805481', '1444805481'), ('/configmode/*', '2', null, null, null, '1444805648', '1444805648'), ('/debug/*', '2', null, null, null, '1444805349', '1444805349'), ('/dynagrid/*', '2', null, null, null, '1444805492', '1444805492'), ('/edat/*', '2', null, null, null, '1444805643', '1444805643'), ('/ezforms/*', '2', null, null, null, '1444805440', '1444805440'), ('/field/*', '2', null, null, null, '1444805657', '1444805657'), ('/file-manager-elfinder/*', '2', null, null, null, '1444805527', '1444805527'), ('/file-storage/*', '2', null, null, null, '1444805662', '1444805662'), ('/file-upload/*', '2', null, null, null, '1444805668', '1444805668'), ('/gii/*', '2', null, null, null, '1444805238', '1444805238'), ('/gridview/*', '2', null, null, null, '1444805496', '1444805496'), ('/guide/*', '2', null, null, null, '1444805393', '1444805393'), ('/i18n/*', '2', null, null, null, '1444805405', '1444805405'), ('/inputdata/*', '2', null, null, null, '1444805674', '1444805674'), ('/key-storage/*', '2', null, null, null, '1444805678', '1444805678'), ('/log/*', '2', null, null, null, '1444805685', '1444805685'), ('/manage-data/*', '2', null, null, null, '1444805688', '1444805688'), ('/managedata/*', '2', null, null, null, '1444805472', '1444805472'), ('/page/*', '2', null, null, null, '1444805698', '1444805698'), ('/report-dynamic/*', '2', null, null, null, '1444805706', '1444805706'), ('/report/*', '2', null, null, null, '1444805702', '1444805702'), ('/sign-in/*', '2', null, null, null, '1444805718', '1444805718'), ('/site/*', '2', null, null, null, '1444805722', '1444805722'), ('/system-information/*', '2', null, null, null, '1444805730', '1444805730'), ('/tbl-tree/*', '2', null, null, null, '1444805727', '1444805727'), ('/timeline-event/*', '2', null, null, null, '1444805734', '1444805734'), ('/treemanager/*', '2', null, null, null, '1444805504', '1444805504'), ('/user/*', '2', null, null, null, '1444805739', '1444805739'), ('/verification/*', '2', null, null, null, '1444805745', '1444805745'), ('/view-data/*', '2', null, null, null, '1444805750', '1444805750'), ('/widget-backend-menu/*', '2', null, null, null, '1444805756', '1444805756'), ('/widget-carousel-item/*', '2', null, null, null, '1444805764', '1444805764'), ('/widget-carousel/*', '2', null, null, null, '1444805759', '1444805759'), ('/widget-menu/*', '2', null, null, null, '1444805768', '1444805768'), ('/widget-text/*', '2', null, null, null, '1444805774', '1444805774'), ('administrator', '1', 'ผู้ดูแลระบบ', null, null, '1435861615', '1435861615'), ('fullPage', '2', null, null, null, '1444806907', '1444806907'), ('loginToBackend', '2', 'loginToBackend', null, null, '1435861615', '1435861615'), ('manager', '1', 'เจ้าหน้าที่', null, null, '1435861615', '1435861615'), ('usePage', '2', null, null, null, '1444805933', '1444805933'), ('user', '1', 'ผู้ใช้ทั่วไป', null, null, '1435861614', '1435861614');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
