<?php

/* AdmisReportController
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace backend\controllers;

use backend\modules\ezforms\components\EzformQuery;
use backend\modules\ezforms\models\EzformFields;
use Yii;
use backend\models\Ezform;
use yii\data\ActiveDataProvider;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;

class DataManagementstatController extends \yii\web\Controller
{
    public function actionIndex(){
                
        //Get Value            
        $typestat = $_GET['typestat'];
        $typeyear = $_GET['year'];
        

        // มีกี่สัปดาห์
        //$w = getIsoWeeksInYear($typeyear); // มีกี่สัปดาห์
        $w = DataManagementstatController::getIsoWeeksInYear($typeyear); 
           
        $query = [];
           

        //chk value       
        for($i=1;$i<=$w;$i++){
            //$data[$i] = \backend\controllers\getStartAndEndDate($i,$typeyear);
            $data[$i] = DataManagementstatController::getStartAndEndDate($i,$typeyear);
            //$week = $data[$i][0].' - '.$data[$i][1];
            $week = "'" . $data[$i][0] . "' AND '" . $data[$i][1] . "'";
            
            if($typestat == 'admission'){
                $type = 'nadmdate';
                $sql = "SELECT COUNT(tbdata.$type) AS Amount FROM tbdata_1477041444036408600 as tbdata WHERE tbdata.$type BETWEEN $week  ";
                array_push($query, Yii::$app->db->createCommand($sql)->queryAll());
            }else if ($typestat == 'discharge'){
                $type = 'ndcdate'; 
                $sql = "SELECT COUNT(tbdata.$type) AS Amount FROM tbdata_1477041444036408600 as tbdata WHERE tbdata.$type BETWEEN $week  ";
                array_push($query, Yii::$app->db->createCommand($sql)->queryAll());
            }else if ($typestat == 'morbidity'){
                $type = 'noutcome';
                $sql = "SELECT COUNT(tbdata.$type) AS Amount FROM tbdata_1477041444036408600 as tbdata 
                    WHERE tbdata.$type in('1','2','3') AND tbdata.nadmdate BETWEEN $week  ";
                array_push($query, Yii::$app->db->createCommand($sql)->queryAll());
                
            }else if ($typestat == 'mortality'){
                $type = 'noutcome'; 
                $sql = "SELECT COUNT(tbdata.$type) AS Amount FROM tbdata_1477041444036408600 as tbdata 
                    WHERE tbdata.$type in('8','9') AND tbdata.nadmdate BETWEEN $week  ";
                array_push($query, Yii::$app->db->createCommand($sql)->queryAll());
            
            }else if ($typestat == 'summary'){
                //$type = 'summary';
                $sql = "
                    SELECT 'Admission' AS 'Detail',count(tbdata.nadmdate) AS 'Amount' FROM tbdata_1477041444036408600 AS tbdata WHERE tbdata.nadmdate BETWEEN $week
                    UNION ALL
                    SELECT 'Discharge' AS 'Detail',count(tbdata.ndcdate) AS 'Amount' FROM tbdata_1477041444036408600 AS tbdata WHERE tbdata.ndcdate BETWEEN $week
                    UNION ALL
                    SELECT 'Morbidity' AS 'Detail',count(tbdata.noutcome) AS 'Amount' FROM tbdata_1477041444036408600 AS tbdata WHERE tbdata.noutcome in('1','2','3') AND tbdata.nadmdate BETWEEN $week
                    UNION ALL
                    SELECT 'Mortality' AS 'Detail',count(tbdata.noutcome) AS 'Amount' FROM tbdata_1477041444036408600 AS tbdata WHERE tbdata.noutcome in('8','9') AND tbdata.nadmdate BETWEEN $week
                ";
                array_push($query, Yii::$app->db->createCommand($sql)->queryAll());
               
            }
          
        }
        
        return $this->render('/surgery-form/data-managementstat',[
            'year' => $Yearselect, //select year
            'years' => $typeyear, //ส่งค่า years ไปเช็ค
            'type' => $typestat, //ส่งค่า type ไปเช็ค
            'query' => $query, 
                       
        ]);
        
    }
    // เช็คสัปดาห์
    public function getStartAndEndDate($week, $year) {
        $date_string = $year . 'W' . sprintf('%02d', $week);
        $return[0] = date('Y-m-d', strtotime($date_string));
        $return[1] = date('Y-m-d', strtotime($date_string . '7'));
        return $return;
    }

    public function getIsoWeeksInYear($year) {
        $date = new \DateTime();
        $date->setISODate($year, 53);
        return ($date->format("W") === "53" ? 53 : 52);
    }
}