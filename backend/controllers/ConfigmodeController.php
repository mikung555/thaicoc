<?php
namespace backend\controllers;

use common\lib\codeerror\helpers\GenMillisecTime;
use Yii;
use yii\web\Controller;
use app\models\SiteConfig;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ConfigmodeController
 *
 * @author Balz
 */
class ConfigmodeController extends Controller {
    //put your code here
    public function actionIndex(){
        $findconfig = SiteConfig::find()->count();
        if($findconfig==0){
            $siteconfig = new SiteConfig();

        }else{
            $siteconfig = SiteConfig::find()->where('id = 1')->One();

        }
        if($siteconfig->load(Yii::$app->request->post())){
            $siteconfig->id = 1;
            if($siteconfig->save()){
//                $session = Yii::$app->session;
//                $session->open();
//                $session->set('site',$siteconfig);
                return $this->redirect(['index']);
            }
        }else{
            return $this->render('index',['siteconfig'=>$siteconfig]);
        }
    }
    public function actionGenmill(){
        $numberGen = GenMillisecTime::getMillisecTime();
        echo $numberGen;
    }
}
