<?php

namespace backend\controllers;

use common\models\SiteConfig;
use Yii;
use backend\models\search\TimelineEventSearch;
//use yii\helpers\VarDumper;
use yii\data\SqlDataProvider;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\VarDumper;
use yii\web\Controller;
use \backend\modules\managedata\models\ManagedataSearch;
use \yii\helpers\ArrayHelper;
use yii\data\ArrayDataProvider;
use backend\models\UserAllow;
use yii\data\ActiveDataProvider;
use yii\jui\DatePicker;
//use miloschuman\highcharts\Highcharts;
/**
 * Application timeline controller
 */
class TimelineEventController extends Controller
{
    public $layout = 'common';
    /**
     * Lists all TimelineEvent models.
     * @return mixed
     */
    public function actionIndex()
    {
	
        $userallow = UserAllow::find()->where(['user_id'=>Yii::$app->user->id]);

        $userallowdata = new ActiveDataProvider([
            'query' => $userallow,
        ]);
        
//        $siteconfig = SiteConfig::find()->One();
//
//        $debug = false;
//        // $debug = true;
//        if($debug || Yii::$app->keyStorage->get('frontend.domain')=='cascap.in.th') {
//            $siteCode = Yii::$app->user->identity->userProfile->sitecode;
//            $sqlHospital = "SELECT `hcode`,`name` FROM all_hospital_thai WHERE hcode='".($siteCode)."' ";
//            //$dataHospital = Yii::$app->db->createCommand($sqlHospital)->queryOne();
//
//            Yii::$app->db->createCommand('CALL spOverview("'.$siteCode.'",0)')->execute();
//
//            $table = $this->renderAjax('_ajaxOverviewReport', [
//                'siteconfig' => $siteconfig,
//            ]);
//
//            $userallow = UserAllow::find()->where(['user_id'=>Yii::$app->user->id]);
//
//            $userallowdata = new ActiveDataProvider([
//                'query' => $userallow,
//            ]);            
//            return $this->render('index', [
//                'siteconfig' => $siteconfig,
//                'table'=>$table,
//                'debug'=>$debug,
//                'mysitecode'=>$siteCode,
//                'userallowdata' => $userallowdata,
//            ]);
//        }
//
//        if(Yii::$app->keyStorage->get('frontend.domain')=='thaipalliative.org') {
//        
//            return $this->render('index', [
//                'siteconfig' => $siteconfig,
//                'userallowdata' => $userallowdata,
//            ]);
//        }
//
//
//        //$userallowdata = $provider->getModels();
//        //VarDumper::dump($userallowdata->getModels(),10,true);exit;
//        return $this->render('index', [
//            'siteconfig' => $siteconfig,
//            'dataProviderReport1' => [],
//            'dataProviderReport2' => [],
//            'userallowdata' => $userallowdata,
//        ]);
	
	$sitecode = Yii::$app->user->identity->userProfile->sitecode;
	$userId = Yii::$app->user->id;
	
	$siteconfig = \common\models\SiteConfig::find()->One();
	
	$sqlHospital = "SELECT `hcode`,`name` FROM all_hospital_thai WHERE hcode='".($siteCode)."' ";
	
	$table = $this->renderAjax('_ajaxOverviewReportAuto', [
	    'siteconfig' => $siteconfig,
	]);
        
        $siteCodeCheckDate = Yii::$app->user->identity->userProfile->sitecode;
        $queryOverviewControl = Yii::$app->db->createCommand('SELECT * FROM report_overview_control WHERE sitecode="' . $siteCodeCheckDate . '"')->queryOne();
        $lastcalDateTime = $queryOverviewControl['lastcal'];
        $checkDate = date_format(date_create("$lastcalDateTime"), "Y-m-d");
//       $checkDate = "2016-07-19";
	return $this->render('index', [
	    'siteconfig' => $siteconfig,
	    'table'=>$table,
            'checkDate' => $checkDate,
	    'debug'=>$debug,
	    'mysitecode'=>$siteCode,
	    'userallowdata' =>$userallowdata,
	]);
    }
    
    public function actionReportOverviewRefresh()
    {
	
	$userId = Yii::$app->user->id;
	
	$siteconfig = \common\models\SiteConfig::find()->One();
	
	$sitecode = Yii::$app->user->identity->userProfile->sitecode;
	
	\backend\modules\inv\classes\InvFunc::createReportOverall(null, $sitecode);
	
        return $this->renderAjax('_ajaxOverviewReportAuto', [
            'siteconfig' => $siteconfig,
        ]);
    }

    #palliative report
    public function actionPalliativeReport($type){
        $date_start = $_POST['date_start'];
        $date_end = $_POST['date_end'];
        if($type=='report1') {
            //report การนำเข้าข้อมูล
            $sql = "SELECT
	SUM(a.ezf_id = 1443588615086934400) AS register,
	SUM(a.ezf_id = 1443588625083502400) AS treatment,
	SUM(a.ezf_id = 1443588641092943800) AS followup,
	SUM(a.ezf_id = 1450928555015607100) AS refer,
	b.hcode as xsourcex
FROM
	`ezform_target` as a
RIGHT JOIN all_hospital_thai as b
ON a.xsourcex = b.hcode
WHERE (b.code2 <> 'demo' AND b.zone_code = '07' AND a.rstat <> 3) AND a.create_date BETWEEN  :date_start AND :date_end
GROUP BY
	b.hcode
ORDER BY register DESC";
            $count = \Yii::$app->db->createCommand($sql, [':date_start' => $date_start, ':date_end' =>$date_end])->query()->count();
            $dataProviderReport = new SqlDataProvider([
                'sql' => $sql,
                'params' => [':date_start' => $date_start, ':date_end' =>$date_end],
                'pagination' => [
                    'pageSize' => 100,
                ],
                'totalCount' => $count
            ]);

            return $this->renderAjax('palliative-report1', [
                'dataProviderReport' => $dataProviderReport,
            ]);
        }else if($type=='report2') {
            //report สิทธิ์การรักษา
            $sql = 'SELECT
SUM(a.var70 =1) val1,
SUM(a.var70 =2) val2,
SUM(a.var70 =3) val3,
SUM(a.var70 =4) val4,
SUM(a.var70 =5) val5,
SUM(a.var70 =6) val6,
SUM(a.var70 = "" OR a.var70 is null) space,
COUNT(*) as total, b.province, b.provincecode FROM `tbdata_1` as a
INNER JOIN all_hospital_thai as b
ON a.xsourcex = b.hcode
WHERE (create_date BETWEEN  :date_start AND :date_end) AND ptid=id  AND a.rstat <>3 AND b.code2 <> \'demo\'
GROUP  BY b.provincecode;';
            $count = \Yii::$app->db->createCommand($sql, [':date_start' => $date_start, ':date_end' =>$date_end])->query()->count();
            $dataProviderReport = new SqlDataProvider([
                'sql' => $sql,
                'params' => [':date_start' => $date_start, ':date_end' =>$date_end],
                'pagination' => [
                    'pageSize' => 50,
                ],
                'totalCount' => $count
            ]);
            return $this->renderAjax('palliative-report2', [
                'dataProviderReport' => $dataProviderReport,
            ]);
        }
        else if($type=='report3') {
            //นับ treatment
            $sql= "SELECT c.hcode, count(*) as total FROM `tbdata_2` as a
 INNER JOIN tbdata_1 as b ON a.ptid = b.ptid
	INNER JOIN all_hospital_thai as c ON c.hcode = b.xsourcex
WHERE (b.create_date BETWEEN  :date_start AND :date_end) AND a.rstat <> 3 AND b.rstat <> 3 AND c.code2 <> 'demo' AND c.provincecode= :provincecode ";
            if ($_POST['pay_right'] != 0)
                $sql .= " AND b.var70 = '{$_POST['pay_right']}'";
            $sql .= ' GROUP BY c.hcode  ORDER BY total DESC';
            $totalTreatment = Yii::$app->db->createCommand($sql, [':date_start' => $date_start, ':date_end' =>$date_end, ':provincecode'=>$_POST['province']])->queryAll();
            $totalTreatment = ArrayHelper::map($totalTreatment, 'hcode', 'total');

            //นับ follow-up
            $sql="SELECT c.hcode,  count(*) as total FROM `tbdata_3` as a
INNER JOIN tbdata_1 as b ON a.ptid = b.ptid
INNER JOIN all_hospital_thai as c ON c.hcode = b.xsourcex
WHERE (b.create_date BETWEEN  :date_start AND :date_end) AND a.rstat <> 3 AND b.rstat <> 3 AND c.code2 <> 'demo' AND c.provincecode= :provincecode ";
            if ($_POST['pay_right'] != 0)
                $sql .= " AND b.var70 = '{$_POST['pay_right']}'";
            $sql .= ' GROUP BY c.hcode  ORDER BY total DESC';
            $totalFollowup = Yii::$app->db->createCommand($sql, [':date_start' => $date_start, ':date_end' =>$date_end, ':provincecode'=>$_POST['province']])->queryAll();
            $totalFollowup = ArrayHelper::map($totalFollowup, 'hcode', 'total');

            //report ข้อมูลเครือข่าย
            $sql = 'SELECT
b.hcode,
b.`name`,
:pay_right as pay_right,
COUNT(*) as total
FROM `tbdata_1` as a
INNER JOIN all_hospital_thai as b
ON a.xsourcex = b.hcode
WHERE (create_date BETWEEN :date_start AND :date_end) AND a.rstat <>3 AND b.code2 <> \'demo\' AND b.provincecode = :provincecode';
            if ($_POST['pay_right'] != 0)
                $sql .= " AND var70 = :pay_right";
            $sql .= ' GROUP BY b.hcode ORDER BY total DESC;';
            $count = \Yii::$app->db->createCommand($sql, [':date_start' => $date_start, ':date_end' =>$date_end, ':provincecode' => $_POST['province'], ':pay_right' => $_POST['pay_right']])->query()->count();
            $dataProviderReport = new SqlDataProvider([
                'sql' => $sql,
                'params' => [':date_start' => $date_start, ':date_end' =>$date_end, ':provincecode' => $_POST['province'], ':pay_right' => $_POST['pay_right']],
                'pagination' => [
                    'pageSize' => 50,
                ],
                'totalCount' => $count
            ]);
            $date['date_start'] = $date_start;
            $date['date_end'] = $date_end;
            return $this->renderAjax('palliative-report3', [
                'dataProviderReport' => $dataProviderReport,
                'date' =>$date,
                'totalFollowup' => $totalFollowup,
                'totalTreatment' => $totalTreatment
            ]);
        }else if($type=='report4') {
            //นับ treatment
            $sql= "SELECT c.provincecode, count(*) as total FROM `tbdata_2` as a
 INNER JOIN tbdata_1 as b ON a.ptid = b.ptid
	INNER JOIN all_hospital_thai as c ON c.hcode = b.xsourcex
WHERE (b.create_date BETWEEN  :date_start AND :date_end) AND a.rstat <> 3 AND b.rstat <> 3 AND c.code2 <> 'demo' AND c.zone_code= :zone_code ";
            if ($_POST['pay_right'] != 0)
                $sql .= " AND b.var70 = '{$_POST['pay_right']}'";
            $sql .= ' GROUP BY c.provincecode  ORDER BY total DESC';
            $totalTreatment = Yii::$app->db->createCommand($sql, [':date_start' => $date_start, ':date_end' =>$date_end, ':zone_code'=>$_POST['zone_code']])->queryAll();
            $totalTreatment = ArrayHelper::map($totalTreatment, 'provincecode', 'total');

            //นับ follow-up
            $sql="SELECT c.provincecode,  count(*) as total FROM `tbdata_3` as a
INNER JOIN tbdata_1 as b ON a.ptid = b.ptid
INNER JOIN all_hospital_thai as c ON c.hcode = b.xsourcex
WHERE (b.create_date BETWEEN  :date_start AND :date_end) AND a.rstat <> 3 AND b.rstat <> 3 AND c.code2 <> 'demo' AND c.zone_code= :zone_code ";
            if ($_POST['pay_right'] != 0)
                $sql .= " AND b.var70 = '{$_POST['pay_right']}'";
            $sql .= ' GROUP BY c.provincecode  ORDER BY total DESC';
            $totalFollowup = Yii::$app->db->createCommand($sql, [':date_start' => $date_start, ':date_end' =>$date_end, ':zone_code'=>$_POST['zone_code']])->queryAll();
            $totalFollowup = ArrayHelper::map($totalFollowup, 'provincecode', 'total');

            //report ข้อมูลเครือข่าย
            $sql ="SELECT b.provincecode, b.province, :pay_right as pay_right, COUNT(*) as total FROM `tbdata_1` as a INNER JOIN all_hospital_thai as b ON a.xsourcex = b.hcode 
WHERE (create_date BETWEEN :date_start AND :date_end) AND a.rstat <>3 AND b.code2 <> 'demo' AND b.zone_code= :zone_code";
            if ($_POST['pay_right'] != 0)
                $sql .= " AND var70 = '{$_POST['pay_right']}'";
            $sql .= ' GROUP BY b.provincecode  ORDER BY total DESC';

            $dataProviderReport = new SqlDataProvider([
                'sql' => $sql,
                'params' => [':date_start' => $date_start, ':date_end' =>$date_end, ':zone_code'=>$_POST['zone_code'], ':pay_right' => $_POST['pay_right']],
                'pagination' => [
                    'pageSize' => 50,
                ],
            ]);

            $date['date_start'] = $date_start;
            $date['date_end'] = $date_end;
            return $this->renderAjax('palliative-report4', [
                'dataProviderReport' => $dataProviderReport,
                'date' =>$date,
                'totalFollowup' => $totalFollowup,
                'totalTreatment' => $totalTreatment
            ]);
        }
    }
    public function actionChangesite($user_id, $sitecode) {
        $userallow = UserAllow::findOne(['user_id'=>$user_id, 'sitecode'=>$sitecode]);
        if ($userallow) {
            $profile = Yii::$app->user->identity->userProfile;
            $profile->sitecode = $sitecode;
            if ($profile->save()) {
                $this->redirect(Yii::getAlias('@backendUrl')); //return $this->redirect("/");
            }else{
                VarDumper::dump($model->getErrors(),10,true);
            }      
        }else{
            throw new NotFoundHttpException('Invalid request. Please do not repeat this request again.');
        }
        
    }
    public function actionCca01Cca02Select($filterType=null , $filterRange=null){

        if(
            ($filterType != null && !($filterType == 'day' || $filterType == 'year')) ||
            ($filterType == "day" && sizeof(explode('_',$filterRange))!=2)
        )return;

        $thaiMonth = ['',
            1=>['abvt'=>'ม.ค.','full'=>'มกราคม'],
            2=>['abvt'=>'ก.พ.', 'full'=>'กุมภาพันธ์'],
            3=>['abvt'=>'มี.ค.', 'full'=>'มีนาคม'],
            4=>['abvt'=>'เม.ย.', 'full'=>'เมษายน'],
            5=>['abvt'=>'พ.ค.', 'full'=>'พฤษภาคม'],
            6=>['abvt'=>'มิ.ย.', 'full'=>'มิถุนายน'],
            7=>['abvt'=>'ก.ค.', 'full'=>'กรกฎาคม'],
            8=>['abvt'=>'ส.ค.', 'full'=>'สิงหาคม'],
            9=>['abvt'=>'ก.ย.', 'full'=>'กันยายน'],
            10=>['abvt'=>'ต.ค.', 'full'=>'ตุลาคม'],
            11=>['abvt'=>'พ.ย.', 'full'=>'พฤศจิกายน'],
            12=>['abvt'=>'ธ.ค.', 'full'=>'ธันวาคม']
        ];

        $cca01Text = 'จำนวนกลุ่มเสี่ยงที่รับการลงทะเบียน';
        $cca02Text = 'จำนวนครั้งการตรวจอัลตร้าซาวด์';

        if($filterType==null){
            $fromDate = '2013-02-09 00:00:00';
            $toDate = date('Y-m-d H:i:s');

            $cca01Text.="ทั้งหมด (CCA-01)";
            $cca02Text.="ทั้งหมด (CCA-02)";


        }else if ($filterType == 'year'){
            $lastRange = intval($filterRange)-1;
            $fromDate = $lastRange.'-10-01 00:00:00';
            $toDate = $filterRange.'-09-30 23:59:59';

            $cca01Text.=' (CCA01)<br><br>ในช่วงปีงบประมาณ '.(intval($filterRange)+543);
            $cca02Text.=' (CCA02)<br><br>ในช่วงปีงบประมาณ '.(intval($filterRange)+543);


        }else if ($filterType == 'day'){
            $ft = explode('_',$filterRange);
            $fromDate = $ft[0].' 00:00:00';
            $toDate = $ft[1].' 23:59:59';

            $fromDateArray = explode('-',$ft[0]);
            $toDateArray = explode('-',$ft[1]);

            $fromDateShow = intval($fromDateArray[2]).' '.$thaiMonth[intval($fromDateArray[1])]['full'].' '.(intval($fromDateArray[0])+543);
            $toDateShow = intval($toDateArray[2]).' '.$thaiMonth[intval($toDateArray[1])]['full'].' '.(intval($toDateArray[0])+543);

            $cca01Text.=' (CCA01)<br><br>ในช่วงวันที่ '.$fromDateShow.' ถึงวันที่ '.$toDateShow;
            $cca02Text.=' (CCA02)<br><br>ในช่วงวันที่ '.$fromDateShow.' ถึงวันที่ '.$toDateShow;


        }

        $stats_tb_data_2 = array();
        $stats_tb_data_3 = array();

        /*
         * CCA-01
         */

        $sql = 'SELECT
            DATE_FORMAT(f1vdcomp, "%Y-%m") AS cca01Ym,
            COUNT(DISTINCT ptid) AS cca01Count
        FROM
            tb_data_2
        WHERE
        rstat != 0
        AND rstat != 3
        AND hsitecode IS NOT NULL
        AND hsitecode != 0
        AND (f1vdcomp BETWEEN "'.$fromDate.'" AND "'.$toDate.'")
        GROUP BY
            cca01Ym
        ORDER BY
            f1vdcomp ASC';
        $query = Yii::$app->db->createCommand($sql)->queryAll();
        //ArrayHelper::multisort($query, 'cca01Ym', SORT_ASC);
        $countQuery = count($query);
        if($countQuery > 0){
            $stats_tb_data_2['count'] = $countQuery;
        }

        //$cca01LabelText;

        $stats_tb_data_2['options']['title']['text'] = $cca01Text;
        $categories = array();
        $stat = array();
        $accumulateArray = array();
        $accumulate = 0;
        foreach ($query as $value) {
            $Ym = explode('-',$value['cca01Ym']);
            array_push($categories, $thaiMonth[intval($Ym[1])]['abvt'].' '.(intval($Ym[0])+543) );

            $accumulate +=(int)$value['cca01Count'];
            array_push($accumulateArray, $accumulate);
            array_push($stat, (int)$value['cca01Count']);
        }
        $stats_tb_data_2['options']['xAxis'] = ['categories' => $categories];
        $stats_tb_data_2['options']['yAxis']['title']['text'] = 'จำนวน (ราย)';
        $stats_tb_data_2['options']['series'] = [
            [
                'type' => 'column',
                'name' => 'จำนวน',
                'data' => $stat
            ],
            [
                'type' => 'spline',
                'name' => 'จำนวนสะสม',
                'data' => $accumulateArray,
            ]
        ];

        /*
         * CCA-02
         */

        $sql2 = 'SELECT
            DATE_FORMAT(f2v1, "%Y-%m") AS cca02Ym,
            COUNT(*) AS cca02Count
        FROM
            tb_data_3
        WHERE
            rstat != 0
        AND rstat != 3
        AND hsitecode IS NOT NULL
        AND hsitecode != 0
        AND (f2v1 BETWEEN "'.$fromDate.'" AND "'.$toDate.'")
        GROUP BY
            cca02Ym
        ORDER BY
            f2v1 ASC';
        $query2 = Yii::$app->db->createCommand($sql2)->queryAll();
        //ArrayHelper::multisort($query2, 'cca02Ym', SORT_ASC);
        $countQuery2 = count($query2);
        if($countQuery2 > 0){
            $stats_tb_data_3['count'] = $countQuery2;
        }

        //$cca02LabelText;

        $stats_tb_data_3['options']['title']['text'] = $cca02Text;
        $categories = array();
        $stat = array();
        $accumulateArray = array();
        $accumulate = 0;
        foreach ($query2 as $value) {
            $Ym = explode('-',$value['cca02Ym']);
            array_push($categories, $thaiMonth[intval($Ym[1])]['abvt'].' '.(intval($Ym[0])+543) );

            $accumulate +=(int)$value['cca02Count'];
            array_push($accumulateArray, $accumulate);
            array_push($stat, (int)$value['cca02Count']);
        }
        $stats_tb_data_3['options']['xAxis'] = ['categories' => $categories];
        $stats_tb_data_3['options']['yAxis']['title']['text'] = 'จำนวน (ครั้ง)';
        $stats_tb_data_3['options']['series'] = [
            [
                'type' => 'column',
                'name' => 'จำนวน',
                'data' => $stat
            ],
            [
                'type' => 'spline',
                'name' => 'จำนวนสะสม',
                'data' => $accumulateArray,
            ]
        ];

        Yii::$app->response->format = \yii\web\Response::FORMAT_HTML;
        return $this->renderAjax('_ajaxCca01Cca02Select', [
            'query' => $stats_tb_data_2,
            'query3' => $stats_tb_data_3
        ]);
    }

    public function actionCca01Cca02Zone(){

        $graphSiteOrder = array();
        $graphSiteOrder2 = array();

        /*
         * CCA-01
         */

        $sql5 = 'SELECT
            zone_code,
            zone_name,
            COUNT(DISTINCT ptid) AS cca01Count
        FROM
            tb_data_2
        INNER JOIN all_hospital_thai ON tb_data_2.hsitecode = all_hospital_thai.hcode
        WHERE
            rstat != 0
        AND rstat != 3
        AND (f1vdcomp BETWEEN "2013-02-09" AND NOW())
        AND zone_code IS NOT NULL
        GROUP BY
            zone_code';
        $query5 = Yii::$app->db->createCommand($sql5)->queryAll();
        $countQuery5 = count($query5);
        if($countQuery5 > 0){
            $graphSiteOrder['count'] = $countQuery5;
        }
        $graphSiteOrder['options']['title']['text'] = 'จำนวนกลุ่มเสี่ยงที่ได้รับการลงทะเบียน (CCA-01)';
        $categories = array();
        $stat = array();
        foreach ($query5 as $value) {
            array_push($categories, ("เขต ".intval($value['zone_code']).' '.str_replace('สาขาพื้นที่','',$value['zone_name'])));
            array_push($stat, (int)$value['cca01Count']);
        }
        $graphSiteOrder['options']['xAxis'] = ['categories' => $categories];
        $graphSiteOrder['options']['yAxis']['title']['text'] = 'จำนวน (ราย)';
        $graphSiteOrder['options']['series'] = [
            [
                'type' => 'column',
                'name' => 'จำนวน',
                'data' => $stat
            ]
        ];

        /*
         * CCA-02
         */

        $sql6 = 'SELECT
            zone_code,
            zone_name,
            COUNT(*) AS cca02Count
        FROM
            tb_data_3
        INNER JOIN all_hospital_thai ON tb_data_3.hsitecode = all_hospital_thai.hcode
        WHERE
            rstat != 0
        AND rstat != 3
        AND (	f2v1 BETWEEN "2013-02-09" AND NOW())
        AND zone_code IS NOT NULL
        GROUP BY
            zone_code';
        $query6 = Yii::$app->db->createCommand($sql6)->queryAll();
        $countQuery6 = count($query6);
        if($countQuery6 > 0){
            $graphSiteOrder2['count'] = $countQuery6;
        }
        $graphSiteOrder2['options']['title']['text'] = 'จำนวนครั้งการตรวจอัลตร้าซาวด์ (CCA-02)';
        $categories = array();
        $stat = array();
        foreach ($query6 as $value) {
            array_push($categories, ("เขต ".intval($value['zone_code']).' '.str_replace('สาขาพื้นที่','',$value['zone_name'])));
            array_push($stat, (int)$value['cca02Count']);
        }
        $graphSiteOrder2['options']['xAxis'] = ['categories' => $categories];
        $graphSiteOrder2['options']['yAxis']['title']['text'] = 'จำนวน (ครั้ง)';
        $graphSiteOrder2['options']['series'] = [
            [
                'type' => 'column',
                'name' => 'จำนวน',
                'data' => $stat
            ]
        ];

        Yii::$app->response->format = \yii\web\Response::FORMAT_HTML;
        return $this->renderAjax('_ajaxCca01Cca02Zone', [
            'graphSiteOrder' => $graphSiteOrder,
            'graphSiteOrder2' => $graphSiteOrder2,
            'chartId'=>'zone-hc'
        ]);
    }

    public function actionCca01Cca02Province(){

        $graphSiteOrder = array();
        $graphSiteOrder2 = array();

        /*
         * CCA-01
         */

        $sql5 = 'SELECT
            zone_code+0 AS zone_code,
            provincecode,
            province,
            COUNT(DISTINCT ptid) AS cca01Count
        FROM
            tb_data_2
        INNER JOIN all_hospital_thai ON tb_data_2.hsitecode = all_hospital_thai.hcode
        WHERE
            rstat != 0
        AND rstat != 3
        AND (f1vdcomp BETWEEN "2013-02-09" AND NOW())
        AND provincecode IS NOT NULL
        GROUP BY
            provincecode
        ORDER BY
            zone_code,province';
        $query5 = Yii::$app->db->createCommand($sql5)->queryAll();
        $countQuery5 = count($query5);
        if($countQuery5 > 0){
            $graphSiteOrder['count'] = $countQuery5;
        }
        $graphSiteOrder['options']['title']['text'] = 'จำนวนกลุ่มเสี่ยงที่ได้รับการลงทะเบียน (CCA-01)';
        $categories = array();
        $stat = array();
        foreach ($query5 as $value) {
            array_push($categories, ('เขต '.$value['zone_code'].' จ.'.$value['province']));
            array_push($stat, (int)$value['cca01Count']);
        }
        $graphSiteOrder['options']['xAxis'] = ['categories' => $categories];
        $graphSiteOrder['options']['yAxis']['title']['text'] = 'จำนวน (ราย)';
        $graphSiteOrder['options']['series'] = [
            [
                'type' => 'column',
                'name' => 'จำนวน',
                'data' => $stat
            ]
        ];

        /*
         * CCA-02
         */

        $sql6 = 'SELECT
            zone_code+0 AS zone_code,
            provincecode,
            province,
            COUNT(*) AS cca02Count
        FROM
            tb_data_3
        INNER JOIN all_hospital_thai ON tb_data_3.hsitecode = all_hospital_thai.hcode
        WHERE
            rstat != 0
        AND rstat != 3
        AND (	f2v1 BETWEEN "2013-02-09" AND NOW())
        AND provincecode IS NOT NULL
        GROUP BY
            provincecode
        ORDER BY
            zone_code,province';
        $query6 = Yii::$app->db->createCommand($sql6)->queryAll();
        $countQuery6 = count($query6);
        if($countQuery6 > 0){
            $graphSiteOrder2['count'] = $countQuery6;
        }
        $graphSiteOrder2['options']['title']['text'] = 'จำนวนครั้งการตรวจอัลตร้าซาวด์ (CCA-02)';
        $categories = array();
        $stat = array();
        foreach ($query6 as $value) {
            array_push($categories, ('เขต '.$value['zone_code'].' จ.'.$value['province']));
            array_push($stat, (int)$value['cca02Count']);
        }
        $graphSiteOrder2['options']['xAxis'] = ['categories' => $categories];
        $graphSiteOrder2['options']['yAxis']['title']['text'] = 'จำนวน (ครั้ง)';
        $graphSiteOrder2['options']['series'] = [
            [
                'type' => 'column',
                'name' => 'จำนวน',
                'data' => $stat
            ]
        ];

        Yii::$app->response->format = \yii\web\Response::FORMAT_HTML;
        return $this->renderAjax('_ajaxCca01Cca02Zone', [
            'graphSiteOrder' => $graphSiteOrder,
            'graphSiteOrder2' => $graphSiteOrder2,
            'chartId'=>'province-hc'
        ]);
    }

    public function actionOverviewReport(){
        $dataAll = array();

        /*
            10 	หน่วยงานส่วนกลาง
            1	สำนักงานสาธารณะสุขจังหวัด
            2	สำนักงานสาธารณะสุขอำเภอ
            5, 6	รพศ. รพท.
            7	รพช. รพร.
            18, 3, 4, 8, 13, 17	รพ.สต. สถานีอนามัย
                หน่วยงานบริการอื่นๆ
            11, 12	โรงพยาบาล นอก สธ.
            15, 16	โรงพยาบาลเอกชน และคลินิกเอกชน
         */
//        $listDeptMember = [
//            "publicAgencies" => "`code4` = '10'",
//            "provincialPublicHealthOffice" => "`code4` = '1'",
//            "districtPublicHealthOffice" => "`code4` = '2'",
//            "deptCentral" => "`code4` = '5' OR `code4` = '6'",
//            "centralGeneral" => "`code4` = '7'",
//            "rural" => "`code4` = '18' OR `code4` = '3' OR `code4` = '4' OR `code4` = '8' OR `code4` = '13' OR `code4` = '17'",
//            "outsideTheHospital" => "`code4` = '11' OR `code4` = '12'",
//            "privateHospitalsAndClinics" => "`code4` = '15' OR `code4` = '16'",
//            "other" => "`code4`+0 >= '19' OR `code4` = '9' OR `code4` = '14'"
//        ];

//        array_push($dataAll, [
//                'section',['จำนวนหน่วยงานสมาชิก','ทั้งหมด','ปีนี้','เดือนนี้','สัปดาห์นี้','วันนี้']
//            ]
//        );
//        $sumAll = 0;
//        $sumYear = 0;
//        $sumMonth = 0;
//        $sumYearWeekRegis = 0;
//        $sumDay = 0;
//
//        foreach($listDeptMember as $key => $deptMember){
//            $sqlDeptMember = "SELECT countAll,
//                    YEAR(NOW()) as `yearNow`,
//                    countYear,
//                    MONTH(NOW()) as `monthNow`,
//                    countMonth,
//                    YEARWEEK(NOW()) as `yearWeekNow`,
//                    countYearWeekRegis,
//                    DAY(NOW()) as `dayNow`,
//                    countDay
//                FROM (
//                    SELECT COUNT(*) as countAll
//                    FROM `user_profile`, `user`, `all_hospital_thai`
//                    WHERE `user_profile`.`sitecode` = `all_hospital_thai`.`hcode`
//                    AND `user_profile`.`user_id` = `user`.`id`
//                    AND ( " . ($deptMember) . " )
//                ) as `allRegis`
//                LEFT OUTER JOIN (
//                    SELECT COUNT(*) as countYear
//                    FROM `user_profile`, `user`, `all_hospital_thai`
//                    WHERE `user_profile`.`sitecode` = `all_hospital_thai`.`hcode`
//                    AND `user_profile`.`user_id` = `user`.`id`
//                    AND YEAR(FROM_UNIXTIME(`created_at`)) = YEAR(NOW())
//                    AND ( " . ($deptMember) . " )
//                ) as `yearRegis`
//                    ON ( 1 )
//                LEFT OUTER JOIN (
//                    SELECT COUNT(*) as countMonth
//                    FROM `user_profile`, `user`, `all_hospital_thai`
//                    WHERE `user_profile`.`sitecode` = `all_hospital_thai`.`hcode`
//                    AND `user_profile`.`user_id` = `user`.`id`
//                    AND YEAR(FROM_UNIXTIME(`created_at`)) = YEAR(NOW())
//                    AND MONTH(FROM_UNIXTIME(`created_at`)) = MONTH(NOW())
//                    AND ( " . ($deptMember) . " )
//                ) as `monthRegis`
//                    ON ( 1 )
//                LEFT OUTER JOIN (
//                    SELECT COUNT(*) as countYearWeekRegis
//                    FROM `user_profile`, `user`, `all_hospital_thai`
//                    WHERE `user_profile`.`sitecode` = `all_hospital_thai`.`hcode`
//                    AND `user_profile`.`user_id` = `user`.`id`
//                    AND YEAR(FROM_UNIXTIME(`created_at`)) = YEAR(NOW())
//                    AND YEARWEEK(FROM_UNIXTIME(`created_at`)) = YEARWEEK(NOW())
//                    AND ( " . ($deptMember) . " )
//                ) as `yearWeekRegis`
//                    ON ( 1 )
//                LEFT OUTER JOIN (
//                    SELECT COUNT(*) as countDay
//                    FROM `user_profile`, `user`, `all_hospital_thai`
//                    WHERE `user_profile`.`sitecode` = `all_hospital_thai`.`hcode`
//                    AND `user_profile`.`user_id` = `user`.`id`
//                    AND YEAR(FROM_UNIXTIME(`created_at`)) = YEAR(NOW())
//                    AND MONTH(FROM_UNIXTIME(`created_at`)) = MONTH(NOW())
//                    AND DAY(FROM_UNIXTIME(`created_at`)) = DAY(NOW())
//                    AND ( " . ($deptMember) . " )
//                ) as `day`
//                    ON ( 1 ) ";
//
////            echo "<pre>$sqlDeptMember</pre>";
//
//            if($key=='publicAgencies'){
//                $strHeadDeptMember = "หน่วยงานส่วนกลาง (กรม สำนัก ศูนย์)";
//            }else if($key=='provincialPublicHealthOffice'){
//                $strHeadDeptMember = "สำนักงานสาธารณะสุขจังหวัด";
//            }else if($key=='districtPublicHealthOffice'){
//                $strHeadDeptMember = "สำนักงานสาธารณะสุขอำเภอ";
//            }else if($key=='deptCentral'){
//                $strHeadDeptMember = "รพศ. รพท.";
//            }else if($key=='centralGeneral'){
//                $strHeadDeptMember = "รพช. รพร.";
//            }else if($key=='rural'){
//                $strHeadDeptMember = "รพ.สต. และ สถานีอนามัย";
//            }else if($key=='outsideTheHospital'){
//                $strHeadDeptMember = "โรงพยาบาล นอก สธ.";
//            }else if($key=='privateHospitalsAndClinics'){
//                $strHeadDeptMember = "โรงพยาบาลเอกชน และคลินิกเอกชน";
//            }else if($key=='other'){
//                $strHeadDeptMember = "หน่วยงานบริการอื่นๆ";
//            }
//
//            $qryDeptMember = Yii::$app->db->createCommand($sqlDeptMember)->queryAll();
//
//            $countAll = $qryDeptMember[0]['countAll'];
//            $countYear = $qryDeptMember[0]['countYear'];
//            $countMonth = $qryDeptMember[0]['countMonth'];
//            $countYearWeekRegis = $qryDeptMember[0]['countYearWeekRegis'];
//            $countDay = $qryDeptMember[0]['countDay'];
//
////            array_push($dataAll, [
////                    'name', [$strHeadDeptMember,$countAll,$countYear,$countMonth,$countYearWeekRegis,$countDay]
////                ]
////            );
//
//            $sumAll+=$countAll;
//            $sumYear+=$countYear;
//            $sumMonth+=$countMonth;
//            $sumYearWeekRegis+=$countYearWeekRegis;
//            $sumDay+=$countDay;
//        }

//        array_push($dataAll, [
//                'name', ['รวมทั้งหมด',$sumAll,$sumYear,$sumMonth,$sumYearWeekRegis,$sumDay]
//            ]
//        );

        $listResultReport = [
            "CCA-01" => ["cascap_stddataset.tb_data_2", "ptid", "id", "f1vdcompdb"],
            "OV-01K" => ["tbdata_21", "target", "id", "exdate"],
            "OV-01P" => ["tbdata_22", "target", "id", "exdate"],
            "OV-01F" => ["tbdata_23", "target", "id", "exdate"],
            "OV-01U" => ["tbdata_24", "target", "id", "exdate"],
            "OV-02" => ["tbdata_25", "target", "id", "vdate"],
            "OV-03" => ["tbdata_26", "target", "id", "vdate"],
            "CCA-02" => ["cascap_stddataset.tb_data_3", "ptid", "id", "f2v1db"],
            "CCA-02.1" => ["cascap_stddataset.tb_data_4", "ptid", "id", "f2p1v1"],
            "CCA-03" => ["cascap_stddataset.tb_data_5", "ptid", "id", "f3v5dvisitdb"],
            "CCA-03.1" => ["cascap_stddataset.tb_data_6", "ptid", "id", "f3p1v1a1"],
            "CCA-04" => ["cascap_stddataset.tb_data_8", "ptid", "id", "f4dcomplete"],
            "CCA-05" => ["cascap_stddataset.tb_data_11", "ptid", "id", "f5dcomplete"]
        ];

        array_push($dataAll, [
                'section',['สรุปผลงานรวม','','','','','']
            ]
        );
        foreach($listResultReport as $key => $resultReport){
            $sqlResultReportTarget = "SELECT *
                FROM (
                    SELECT COUNT(DISTINCT(" . $resultReport[1] . ")) as countAll
                    FROM " . $resultReport[0] . ",	all_hospital_thai
                    WHERE	" . $resultReport[0] . "." . $resultReport[3] . " IS NOT NULL
                    AND " . $resultReport[0] . "." . $resultReport[3] . " >= '2013-02-09 00:00:00'
                    AND " . $resultReport[0] . ".hsitecode = all_hospital_thai.hcode
                ) as resultReportAll
                LEFT OUTER JOIN(
                    SELECT COUNT(DISTINCT(" . $resultReport[1] . ")) as countYear
                    FROM " . $resultReport[0] . ", all_hospital_thai
                    WHERE " . $resultReport[0] . "." . $resultReport[3] . " IS NOT NULL
                    AND YEAR(" . $resultReport[0] . "." . $resultReport[3] . " ) = YEAR(NOW())
                    AND " . $resultReport[0] . ".hsitecode = all_hospital_thai.hcode
                ) as resultReportYear ON ( 1 )
                LEFT OUTER JOIN(
                    SELECT COUNT(DISTINCT(" . $resultReport[1] . ")) as countMonth
                    FROM " . $resultReport[0] . ", all_hospital_thai
                    WHERE " . $resultReport[0] . "." . $resultReport[3] . " IS NOT NULL
                    AND YEAR(" . $resultReport[0] . "." . $resultReport[3] . " ) = YEAR(NOW())
                    AND MONTH(" . $resultReport[0] . "." . $resultReport[3] . " ) = MONTH(NOW())
                    AND " . $resultReport[0] . ".hsitecode = all_hospital_thai.hcode
                ) as resultReportMonth ON ( 1 )
                LEFT OUTER JOIN(
                    SELECT COUNT(DISTINCT(" . $resultReport[1] . ")) as countYearWeek
                    FROM " . $resultReport[0] . ", all_hospital_thai
                    WHERE " . $resultReport[0] . "." . $resultReport[3] . " IS NOT NULL
                    AND YEAR(" . $resultReport[0] . "." . $resultReport[3] . " ) = YEAR(NOW())
                    AND YEARWEEK(" . $resultReport[0] . "." . $resultReport[3] . " ) = YEARWEEK(NOW())
                    AND " . $resultReport[0] . ".hsitecode = all_hospital_thai.hcode
                ) as resultReportYearWeek ON ( 1 )
                LEFT OUTER JOIN(
                    SELECT COUNT(DISTINCT(" . $resultReport[1] . ")) as countDay
                    FROM " . $resultReport[0] . ", all_hospital_thai
                    WHERE " . $resultReport[0] . "." . $resultReport[3] . " IS NOT NULL
                    AND YEAR(" . $resultReport[0] . "." . $resultReport[3] . " ) = YEAR(NOW())
                    AND MONTH(" . $resultReport[0] . "." . $resultReport[3] . " ) = MONTH(NOW())
                    AND DAY(" . $resultReport[0] . "." . $resultReport[3] . " ) = DAY(NOW())
                    AND " . $resultReport[0] . ".hsitecode = all_hospital_thai.hcode
                ) as resultReportDay ON ( 1 )";

            $sqlResultReportAll = "SELECT *
                FROM (
                    SELECT COUNT(DISTINCT(" . $resultReport[2] . ")) as countAll
                    FROM " . $resultReport[0] . ",	all_hospital_thai
                    WHERE	" . $resultReport[0] . "." . $resultReport[3] . " IS NOT NULL
                    AND " . $resultReport[0] . "." . $resultReport[3] . " >= '2013-02-09 00:00:00'
                    AND " . $resultReport[0] . ".hsitecode = all_hospital_thai.hcode
                ) as resultReportAll
                LEFT OUTER JOIN(
                    SELECT COUNT(DISTINCT(" . $resultReport[2] . ")) as countYear
                    FROM " . $resultReport[0] . ", all_hospital_thai
                    WHERE " . $resultReport[0] . "." . $resultReport[3] . " IS NOT NULL
                    AND YEAR(" . $resultReport[0] . "." . $resultReport[3] . " ) = YEAR(NOW())
                    AND " . $resultReport[0] . ".hsitecode = all_hospital_thai.hcode
                ) as resultReportYear ON ( 1 )
                LEFT OUTER JOIN(
                    SELECT COUNT(DISTINCT(" . $resultReport[2] . ")) as countMonth
                    FROM " . $resultReport[0] . ", all_hospital_thai
                    WHERE " . $resultReport[0] . "." . $resultReport[3] . " IS NOT NULL
                    AND YEAR(" . $resultReport[0] . "." . $resultReport[3] . " ) = YEAR(NOW())
                    AND MONTH(" . $resultReport[0] . "." . $resultReport[3] . " ) = MONTH(NOW())
                    AND " . $resultReport[0] . ".hsitecode = all_hospital_thai.hcode
                ) as resultReportMonth ON ( 1 )
                LEFT OUTER JOIN(
                    SELECT COUNT(DISTINCT(" . $resultReport[2] . ")) as countYearWeek
                    FROM " . $resultReport[0] . ", all_hospital_thai
                    WHERE " . $resultReport[0] . "." . $resultReport[3] . " IS NOT NULL
                    AND YEAR(" . $resultReport[0] . "." . $resultReport[3] . " ) = YEAR(NOW())
                    AND YEARWEEK(" . $resultReport[0] . "." . $resultReport[3] . " ) = YEARWEEK(NOW())
                    AND " . $resultReport[0] . ".hsitecode = all_hospital_thai.hcode
                ) as resultReportYearWeek ON ( 1 )
                LEFT OUTER JOIN(
                    SELECT COUNT(DISTINCT(" . $resultReport[2] . ")) as countDay
                    FROM " . $resultReport[0] . ", all_hospital_thai
                    WHERE " . $resultReport[0] . "." . $resultReport[3] . " IS NOT NULL
                    AND YEAR(" . $resultReport[0] . "." . $resultReport[3] . " ) = YEAR(NOW())
                    AND MONTH(" . $resultReport[0] . "." . $resultReport[3] . " ) = MONTH(NOW())
                    AND DAY(" . $resultReport[0] . "." . $resultReport[3] . " ) = DAY(NOW())
                    AND " . $resultReport[0] . ".hsitecode = all_hospital_thai.hcode
                ) as resultReportDay ON ( 1 )";

//            echo "<pre>$sqlResultReportTarget</pre>";
//            echo "<pre>$sqlResultReportAll</pre>";
//            echo "###############################################<br/>";
//            echo "###############################################<br/>";
//            echo "###############################################<br/>";

            if ($key == 'CCA-01') {
                $strHeadDeptMember = "ลงทะเบียนกลุ่มเสี่ยง (CCA-01)";
            } else if ($key == 'OV-01K') {
                array_push($dataAll, [
                        'section', ['ตรวจรักษาพยาธิใบไม้ตับ (OV-01)', '', '', '', '', '']
                    ]
                );
                $strHeadDeptMember = "OV-01K (Kato-Katz)";
            } else if ($key == 'OV-01P') {
                $strHeadDeptMember = "OV-01P (Parasep)";
            } else if ($key == 'OV-01F') {
                $strHeadDeptMember = "OV-01F (FECT)";
            } else if ($key == 'OV-01U') {
                $strHeadDeptMember = "OV-01U (Urine)";
            } else if ($key == 'OV-02') {
                $strHeadDeptMember = "ปรับเปลี่ยนพฤติกรรมเสี่ยง (OV-02)	";
            } else if ($key == 'OV-03') {
                $strHeadDeptMember = "ให้การรักษาพยาธิ (OV-03)	";
            } else if ($key == 'CCA-02') {
                $strHeadDeptMember = "ตรวจคัดกรองมะเร็งท่อน้ำดี (CCA-02)";
            } else if ($key == 'CCA-02.1') {
                $strHeadDeptMember = "ตรวจ CT/MRI (CCA-02.1)";
            } else if ($key == 'CCA-03') {
//                array_push($dataAll, [
//                        'section', ['ให้การรักษา', '', '', '', '', '']
//                    ]
//                );
                $strHeadDeptMember = "ให้การรักษา (CCA-03)";
            } else if ($key == 'CCA-03.1') {
                $strHeadDeptMember = "ติดตามผลการรักษา (CCA-03.1)";
            } else if ($key == 'CCA-04') {
                $strHeadDeptMember = "ตรวจพยาธิวิทยา (CCA-04)";
            } else if ($key == 'CCA-05') {
                $strHeadDeptMember = "ติดตามผลการรักษามะเร็งท่อน้ำดี (CCA-05)";
            }
            array_push($dataAll, [
                    'section',[$strHeadDeptMember,'ทั้งหมด','ปีนี้','เดือนนี้','สัปดาห์นี้','วันนี้']
                ]
            );

            $qryResultReportTarget = Yii::$app->db->createCommand($sqlResultReportTarget)->queryAll();
            $countAll = $qryResultReportTarget[0]['countAll'];
            $countYear = $qryResultReportTarget[0]['countYear'];
            $countMonth = $qryResultReportTarget[0]['countMonth'];
            $countYearWeekRegis = $qryResultReportTarget[0]['countYearWeek'];
            $countDay = $qryResultReportTarget[0]['countDay'];

            array_push($dataAll, [
                    'name', ['&emsp;&emsp;จำนวนราย',$countAll,$countYear,$countMonth,$countYearWeekRegis,$countDay]
                ]
            );

            if($key!='CCA-01') {
                $qryResultReportAll = Yii::$app->db->createCommand($sqlResultReportAll)->queryAll();
                $countAll = $qryResultReportAll[0]['countAll'];
                $countYear = $qryResultReportAll[0]['countYear'];
                $countMonth = $qryResultReportAll[0]['countMonth'];
                $countYearWeekRegis = $qryResultReportAll[0]['countYearWeek'];
                $countDay = $qryResultReportAll[0]['countDay'];

                array_push($dataAll, [
                        'name', ['&emsp;&emsp;จำนวนครั้ง', $countAll, $countYear, $countMonth, $countYearWeekRegis, $countDay]
                    ]
                );
            }

//            echo "<pre>$sqlResultReportTarget</pre>";
//            echo "#####################################################################<br>";
//            echo "#####################################################################<br>";
//            echo "#####################################################################<br>";
//            echo "<pre>$sqlResultReportAll</pre>";
//            echo "#####################################################################<br>";
//            echo "#####################################################################<br>";
//            echo "#####################################################################<br>";
//            echo "#####################################################################<br>";
//            echo "#####################################################################<br>";
//            echo "#####################################################################<br>";
        }
//        \yii\helpers\VarDumper::dump($dataAll,10,true);
        Yii::$app->response->format = \yii\web\Response::FORMAT_HTML;
        return $this->renderAjax('_ajaxOverviewReport', [
            'dataAll' => $dataAll
        ]);



//        $listMemberRegisBySite = [
//            "central" => "`code4` = '5'",
//            "general" => "`code4` = '6'",
//            "rural" => "`code4` = '11' OR `code4` = '12' OR `code4` = '7'",
//            "district" => "`code4` = '8' OR `code4` = '18'"
//        ];
//        $resultMemberRegisBySite = array();
//        foreach( $listMemberRegisBySite as $key => $memberRegisBySite ){
//            $sqlMemberRegisBySite = "SELECT countAll,
//                        YEAR(NOW()) as `yearNow`,
//                        countYear,
//                        MONTH(NOW()) as `monthNow`,
//                        countMonth,
//                        YEARWEEK(NOW()) as `yearWeekNow`,
//                        countYearWeekRegis
//                    FROM (
//                        SELECT COUNT(*) as countAll
//                        FROM `user_profile`, `user`, `all_hospital_thai`
//                        WHERE `user_profile`.`sitecode` = `all_hospital_thai`.`hcode`
//                        AND `user_profile`.`user_id` = `user`.`id`
//                        AND ( ".( $memberRegisBySite )." )
//                    ) as `allRegis`
//                    LEFT OUTER JOIN (
//                        SELECT COUNT(*) as countYear
//                        FROM `user_profile`, `user`, `all_hospital_thai`
//                        WHERE `user_profile`.`sitecode` = `all_hospital_thai`.`hcode`
//                        AND `user_profile`.`user_id` = `user`.`id`
//                        AND YEAR(FROM_UNIXTIME(`created_at`)) = YEAR(NOW())
//                        AND ( ".( $memberRegisBySite )." )
//                    ) as `yearRegis`
//                        ON ( 1 )
//                    LEFT OUTER JOIN (
//                        SELECT COUNT(*) as countMonth
//                        FROM `user_profile`, `user`, `all_hospital_thai`
//                        WHERE `user_profile`.`sitecode` = `all_hospital_thai`.`hcode`
//                        AND `user_profile`.`user_id` = `user`.`id`
//                        AND MONTH(FROM_UNIXTIME(`created_at`)) = MONTH(NOW())
//                        AND ( ".( $memberRegisBySite )." )
//                    ) as `monthRegis`
//                        ON ( 1 )
//                    LEFT OUTER JOIN (
//                        SELECT COUNT(*) as countYearWeekRegis
//                        FROM `user_profile`, `user`, `all_hospital_thai`
//                        WHERE `user_profile`.`sitecode` = `all_hospital_thai`.`hcode`
//                        AND `user_profile`.`user_id` = `user`.`id`
//                        AND YEARWEEK(FROM_UNIXTIME(`created_at`)) = YEARWEEK(NOW())
//                        AND ( ".( $memberRegisBySite )." )
//                    ) as `yearWeekRegis`
//                        ON ( 1 ) ";
//            $qryMemberRegisBySite = Yii::$app->db->createCommand($sqlMemberRegisBySite)->queryAll();
////                $resultMemberRegisBySite[$key]['SQL'] = $sqlMemberRegisBySite;
//            $resultMemberRegisBySite[$key]['all'] = $qryMemberRegisBySite[0]['countAll'];
//            $resultMemberRegisBySite[$key]['year'] = $qryMemberRegisBySite[0]['countYear'];
//            $resultMemberRegisBySite[$key]['month'] = $qryMemberRegisBySite[0]['countMonth'];
//            $resultMemberRegisBySite[$key]['week'] = $qryMemberRegisBySite[0]['countYearWeekRegis'];
//        }
//
//
//        $resultMemberRegisCCA = array();
//        $listTableCCA = [
//            "CCA-01" => ["tb_data_2", "f1vdcompdb"],
//            "CCA-02" => ["tb_data_3", "f2v1db"]
//        ];
//        foreach( $listTableCCA as $key => $tableCCA ) {
//            $sqlMemberRegisCCA = "SELECT *
//                    FROM (
//                        SELECT COUNT(DISTINCT(target)) as countAll
//                        FROM ".$tableCCA[0].",	all_hospital_thai
//                        WHERE	".$tableCCA[0].".".$tableCCA[1]." IS NOT NULL
//                        AND ".$tableCCA[0].".".$tableCCA[1]." >= '2013-02-09 00:00:00'
//                        AND ".$tableCCA[0].".".$tableCCA[1]." <= DATE(NOW())
//                        AND ".$tableCCA[0].".hsitecode = all_hospital_thai.hcode
//                    ) as ccaAll
//                    LEFT OUTER JOIN(
//                        SELECT COUNT(DISTINCT(target)) as countYear
//                        FROM ".$tableCCA[0].", all_hospital_thai
//                        WHERE ".$tableCCA[0].".".$tableCCA[1]." IS NOT NULL
//                        AND YEAR(".$tableCCA[0].".".$tableCCA[1]." ) = YEAR(NOW())
//                        AND ".$tableCCA[0].".hsitecode = all_hospital_thai.hcode
//                    ) as ccaYear ON ( 1 )
//                    LEFT OUTER JOIN(
//                        SELECT COUNT(DISTINCT(target)) as countMonth
//                        FROM ".$tableCCA[0].", all_hospital_thai
//                        WHERE ".$tableCCA[0].".".$tableCCA[1]." IS NOT NULL
//                        AND YEAR(".$tableCCA[0].".".$tableCCA[1]." ) = YEAR(NOW())
//                        AND MONTH(".$tableCCA[0].".".$tableCCA[1]." ) = MONTH(NOW())
//                        AND ".$tableCCA[0].".hsitecode = all_hospital_thai.hcode
//                    ) as ccaMonth ON ( 1 )
//                    LEFT OUTER JOIN(
//                        SELECT COUNT(DISTINCT(target)) as countYearWeek
//                        FROM ".$tableCCA[0].", all_hospital_thai
//                        WHERE ".$tableCCA[0].".".$tableCCA[1]." IS NOT NULL
//                        AND YEAR(".$tableCCA[0].".".$tableCCA[1]." ) = YEAR(NOW())
//                        AND YEARWEEK(".$tableCCA[0].".".$tableCCA[1]." ) = YEARWEEK(NOW())
//                        AND ".$tableCCA[0].".hsitecode = all_hospital_thai.hcode
//                    ) as ccaYearWeek ON ( 1 )";
//            $qryMemberRegisCCA = Yii::$app->db->createCommand($sqlMemberRegisCCA)->queryAll();
////                $resultMemberRegisCCA[$key]['SQL'] = $sqlMemberRegisCCA;
//            $resultMemberRegisCCA[$key]['all'] = $qryMemberRegisCCA[0]['countAll'];
//            $resultMemberRegisCCA[$key]['year'] = $qryMemberRegisCCA[0]['countYear'];
//            $resultMemberRegisCCA[$key]['month'] = $qryMemberRegisCCA[0]['countMonth'];
//            $resultMemberRegisCCA[$key]['week'] = $qryMemberRegisCCA[0]['countYearWeek'];
//        }


    }

    public function actionAjaxDataMonitor($fromDate, $toDate, $zone1=null, $zone6=null, $zone7=null, $zone8=null, $zone9=null, $zone10=null,$refresh=false,$type=null,$str=null){
        if($fromDate=='now')$fromDate=date('Y-m-d');
        if($toDate=='now')$toDate=date('Y-m-d');
        if(
            !strtotime($fromDate) ||
            !strtotime($toDate) ||
            ($zone1 != null && $zone1 != 1) ||
            ($zone6 != null && $zone6 != 6) ||
            ($zone7 != null && $zone7 != 7) ||
            ($zone8 != null && $zone8 != 8) ||
            ($zone9 != null && $zone9 != 9) ||
            ($zone10 != null && $zone10 != 10)
        )return;



        if(1||Yii::$app->keyStorage->get('frontend.domain')=='cascap.in.th') {
            if (1||Yii::$app->getRequest()->isAjax) {
                //VarDumper::dump($_GET,10,true);
                //return;
                /*************************** user section ***************************/ //suspended
                $userId = Yii::$app->user->id;

                //$userId = '1435745159010043318';


                $userDetail['sitecode'] = Yii::$app->user->identity->userProfile['sitecode'];

                //$userDetail['sitecode'] = '23575';

                $sqlUserType = 'SELECT code4,name5,
                    zone_code,
                    provincecode,province,
                    amphurcode,amphur
                FROM all_hospital_thai,user_profile
                WHERE user_profile.user_id = "'.$userId.'"
                AND user_profile.sitecode = all_hospital_thai.hcode';

                //echo $sqlUserType.'<br/>';
                //Yii::$app->end();


                $queryUserType = Yii::$app->db->createCommand($sqlUserType)->queryOne();
                $userDetail['zone_code'] = $queryUserType['zone_code'];
                $userDetail['provincecode'] = $queryUserType['provincecode'];
                $userDetail['amphurcode'] = $queryUserType['amphurcode'];
                $userDetail['province'] = $queryUserType['province'];
                $userDetail['amphur'] = $queryUserType['amphur'];
                $userDetail['level'] = $queryUserType['code4'];
                //$userType = 8;


                /*************************** date section ***************************/

                $thaiMonth = ['',
                    1=>['abvt'=>'ม.ค.','full'=>'มกราคม'],
                    2=>['abvt'=>'ก.พ.', 'full'=>'กุมภาพันธ์'],
                    3=>['abvt'=>'มี.ค.', 'full'=>'มีนาคม'],
                    4=>['abvt'=>'เม.ย.', 'full'=>'เมษายน'],
                    5=>['abvt'=>'พ.ค.', 'full'=>'พฤษภาคม'],
                    6=>['abvt'=>'มิ.ย.', 'full'=>'มิถุนายน'],
                    7=>['abvt'=>'ก.ค.', 'full'=>'กรกฎาคม'],
                    8=>['abvt'=>'ส.ค.', 'full'=>'สิงหาคม'],
                    9=>['abvt'=>'ก.ย.', 'full'=>'กันยายน'],
                    10=>['abvt'=>'ต.ค.', 'full'=>'ตุลาคม'],
                    11=>['abvt'=>'พ.ย.', 'full'=>'พฤศจิกายน'],
                    12=>['abvt'=>'ธ.ค.', 'full'=>'ธันวาคม']
                ];

                if(strtotime($fromDate.' 00:00:00') < strtotime('2013-02-09 00:00:00'))$fromDate = '2013-02-09';
                if(strtotime($toDate.' 23:59:59') > strtotime(date('Y-m-d 23:59:59')))$toDate = date('Y-m-d');

                $listMonth = function($f,$t) use ($fromDate, $toDate) {
                    $aml = array();
                    list($sYear, $sMonth, $sDay) = explode("-", $f);
                    list($eYear, $eMonth, $eDay) = explode("-", $t);
                    $startMonth = $sMonth;
                    do
                    {
                        $mk = mktime(0, 0, 0, $sMonth, 1, $sYear);
                        $mY = date("Ym", $mk);
                        $endFormat = "Y-m-t";
                        if ($sMonth == $startMonth){
                            $mk = mktime(0, 0, 0, $sMonth, $sDay, $sYear);
                        } else if ($mY == $eYear.$eMonth ) {
                            $endFormat =  "Y-m-".$eDay;
                        }
                        //echo date("Y-m-d", $mk)    . " to " . date($endFormat, $mk) . "<br />";
                        array_push($aml, array('fromDate'=>date("Y-m-d", $mk),'toDate'=>date($endFormat, $mk),'Ym'=>date("Y_m", $mk)));
                        $sMonth++;
                    }
                    while ($eYear.$eMonth  > $mY);
                    return $aml;
                };

                $allMonthList = $listMonth($fromDate, $toDate);

                //var_dump($allMonthList);
                //return;


                $sqlAddControlTable = 'CREATE TABLE IF NOT EXISTS report_control_data(
                    `reportid`  int(5) NOT NULL AUTO_INCREMENT ,
                    `fromDate`  varchar(20) NULL ,
                    `toDate`  varchar(20) NULL ,
                    `lastcal`  datetime NULL ,
                    PRIMARY KEY (`reportid`)
                );';

                Yii::$app->db->createCommand($sqlAddControlTable)->execute();

                //return;
                /*************************** query section ***************************/



                $sqlControl = 'SELECT reportid,lastcal
                    FROM report_control_data
                    WHERE fromDate = "'.$fromDate.'"
                    AND toDate = "'.$toDate.'"
                    LIMIT 0,1';

                $queryControl = Yii::$app->db->createCommand($sqlControl)->queryOne();

                if($refresh=='true' || $queryControl == null || ((strtotime( date('Y-m-d H:i:s') ) - strtotime( $queryControl['lastcal'] )) > 86400) ){
                    if($queryControl == null){
                        $sqlReportId = 'INSERT INTO report_control_data(fromDate,toDate,lastcal) VALUES("'.$fromDate.'","'.$toDate.'",NOW())';
                        Yii::$app->db->createCommand($sqlReportId)->execute();

                        $reportId = Yii::$app->db->getLastInsertID();

                    }else{
                        //echo '';

                        $reportId = $queryControl['reportid'];
                        $sqlReportId = 'UPDATE report_control_data SET lastcal = NOW() WHERE reportid = '.$reportId;
                        Yii::$app->db->createCommand($sqlReportId)->execute();
                    }


                    $sqlProcedure = 'CALL spMonitor("'.$fromDate.' 00:00:00","'.$toDate.' 23:59:59",'.$reportId.')';
                    Yii::$app->db->createCommand($sqlProcedure)->execute();



                    unset($queryControl);
                }

                $queryControl = Yii::$app->db->createCommand($sqlControl)->queryOne();

                $lastcal = $queryControl['lastcal'];

                $heading = [
                    'heading'=>'พื้นที่'
                ];

                $queryAllZone = array(['section'=>'เขตบริการสุขภาพ']);
                array_push($queryAllZone,$heading);

                $sumAllZone = ['label'=>'รวม','sumRegis'=>0];

                for($z_code = 1; $z_code<=10; ++$z_code) {
                    if ($z_code > 1 && $z_code < 6) continue;

                    $queryAllZone['zone_'.$z_code]['label'] = 'เขต '.$z_code;
                    $queryAllZone['zone_'.$z_code]['sumRegis'] = 0;


                    foreach ($allMonthList as $key => $eachMonth) {
                        //rem 031
                        $sqlZone = 'SELECT SUM(registerCount) AS registerCount,
                            SUM(confirmCount) AS confirmCount,
                            SUM(cca01Count) AS cca01Count,
                            SUM(cca02Count) AS cca02Count,
                            SUM(cca021Count) AS cca021Count,
                            SUM(cca03Count) AS cca03Count,
                            SUM(cca04Count) AS cca04Count,
                            SUM(cca05Count) AS cca05Count
                        FROM report_data_backend
                        WHERE rptid = '.$queryControl['reportid'].'
                        AND zone_code = '.$z_code.'
                        AND ym = "'.$eachMonth['Ym'].'"';

                        //echo $sqlZone.'<br/>';

                        $queryZone = Yii::$app->db->createCommand($sqlZone)->queryOne();


                        $queryAllZone['zone_'.$z_code][$eachMonth['Ym']] = $queryZone;

                        $queryAllZone['zone_'.$z_code]['sumRegis'] += $queryZone['registerCount'];


                        $sumAllZone['sumRegis'] += $queryZone['registerCount'];

                        foreach($queryZone as $colIndex => $col){
                            $sumAllZone[$eachMonth['Ym']][$colIndex] += $col;
                        }

                    }

                    //array_push($queryAllZone, $rowArr);

                }

                array_push($queryAllZone, $sumAllZone);

//                var_dump($queryAllZone);
//                return;

                if($zone1==null && $zone6==null && $zone7==null && $zone8==null && $zone9==null && $zone10==null){
                    Yii::$app->response->format = \yii\web\Response::FORMAT_HTML;
                    return $this->renderAjax('_ajaxDataMonitor', [
                        'lastcal' => $lastcal,
                        'thaimonth'=>$thaiMonth,
                        'allMonthList'=>$allMonthList,
                        'dataAllZone'=>$queryAllZone,
                        'getFromDate'=>$fromDate,
                        'getToDate'=>$toDate,
                        'getType'=>$type,
                        'str'=>$str,
                        'userDetail'=>$userDetail
                    ]);
                }



                //province in zone

                array_push($queryAllZone,array('section'=>'รายจังหวัด'));



                $sqlBody = 'SUM(registerCount) AS registerCount,
                    SUM(confirmCount) AS confirmCount,
                    SUM(cca01Count) AS cca01Count,
                    SUM(cca02Count) AS cca02Count,
                    SUM(cca021Count) AS cca021Count,
                    SUM(cca03Count) AS cca03Count,
                    SUM(cca04Count) AS cca04Count,
                    SUM(cca05Count) AS cca05Count
                FROM report_data_backend WHERE rptid = '.$queryControl['reportid'].'
                AND zone_code = ';

                $sqlHead = 'SELECT provincecode,';

                $sqlMonth = ' AND ym = "';

                $sqlTail = '" GROUP BY provincecode ORDER BY province';


                for($z_code = 1; $z_code<=10; ++$z_code) {
                    if($z_code>1 && $z_code<6)continue;
                    if($z_code == 1 && $zone1!=1)continue;
                    else if($z_code == 6 && $zone6!=6)continue;
                    else if($z_code == 7 && $zone7!=7)continue;
                    else if($z_code == 8 && $zone8!=8)continue;
                    else if($z_code == 9 && $zone9!=9)continue;
                    else if($z_code == 10 && $zone10!=10)continue;


                    $sumAllProvince = ['label'=>'รวม','sumRegis'=>0];

                    $sqlDataRow = 'SELECT provincecode,province
                        FROM report_data_backend
                        WHERE rptid = '.$queryControl['reportid'].'
                        AND zone_code = '.$z_code.'
                        GROUP BY province';

                    $queryDataRow = Yii::$app->db->createCommand($sqlDataRow)->queryAll();

                    if(!$queryDataRow==null){
                        array_push($queryAllZone,['separator'=>'เขต '.$z_code,'separator_zone'=>$z_code]);
                        $heading['heading'] = 'จังหวัด';
                        array_push($queryAllZone,$heading);

                        foreach($queryDataRow as $key => $row){
                            $queryAllZone['province_'.$row['provincecode']]['label'] = $row['province'];
                            $queryAllZone['province_'.$row['provincecode']]['sumRegis'] = 0;
                            foreach ($allMonthList as $key => $eachMonth) {
                                $queryAllZone['province_'.$row['provincecode']][$eachMonth['Ym']]=[];
                                $sumAllProvince[$eachMonth['Ym']]=[];
                            }
                        }

                        foreach ($allMonthList as $key => $eachMonth) {
                            $sql = $sqlHead.$sqlBody.$z_code.$sqlMonth.$eachMonth['Ym'].$sqlTail;
                            //echo $sql;
                            $query = Yii::$app->db->createCommand($sql)->queryAll();

                            foreach($query as $keyQuery => $row){
                                $queryAllZone['province_'.$row['provincecode']][$eachMonth['Ym']] = $row;
                                $queryAllZone['province_'.$row['provincecode']]['sumRegis'] += $row['registerCount'];
                                $sumAllProvince['sumRegis']+= $row['registerCount'];
                                foreach($row as $indexQuery => $col){

                                    $sumAllProvince[$eachMonth['Ym']][$indexQuery] += (0+$col);
                                }

                            }
                        }

                        array_push($queryAllZone,$sumAllProvince);

                    }
                }


                //amphur in zone

                array_push($queryAllZone,array('section'=>'รายอำเภอ'));

                $sqlHead = 'SELECT CONCAT(provincecode,"_",amphurcode) AS pamphurcode,provincecode,';

                $sqlMonth = ' AND ym = "';

                $sqlTail = '" GROUP BY provincecode, amphurcode ORDER BY province, amphurcode';


                for($z_code = 1; $z_code<=10; ++$z_code) {
                    if($z_code>1 && $z_code<6)continue;
                    if($z_code == 1 && $zone1!=1)continue;
                    else if($z_code == 6 && $zone6!=6)continue;
                    else if($z_code == 7 && $zone7!=7)continue;
                    else if($z_code == 8 && $zone8!=8)continue;
                    else if($z_code == 9 && $zone9!=9)continue;
                    else if($z_code == 10 && $zone10!=10)continue;


                    $sqlDataRow = 'SELECT CONCAT(provincecode,"_",amphurcode) AS pamphurcode,amphur,provincecode,province
                        FROM report_data_backend
                        WHERE rptid = '.$queryControl['reportid'].'
                        AND zone_code = '.$z_code.'
                        GROUP BY provincecode,amphurcode
                        ORDER BY province,amphurcode';

                    $queryDataRow = Yii::$app->db->createCommand($sqlDataRow)->queryAll();
//                    var_dump($queryAllZone);
                    //var_dump($queryDataRow);

                    if(!$queryDataRow==null){
                        $province = '';
                        $provincecode = null;

                        $heading['heading'] = 'อำเภอ';

                        foreach($queryDataRow as $key => $row){
                            if($province != $row['province']){
                                if(isset($sumAllAmphur)&&isset($provincecode)){
                                    $queryAllZone['sumAllAmphur_'.$provincecode] = $sumAllAmphur;
                                }
                                $province = $row['province'];
                                $provincecode = $row['provincecode'];
                                $sumAllAmphur = ['label'=>'รวม','sumRegis'=>0];
                                array_push($queryAllZone,['separator'=>'เขต '.$z_code.' / จ.'.$row['province'],'separator_province'=>$row['provincecode']]);
                                array_push($queryAllZone,$heading);
                            }
                            $queryAllZone['amphur_'.$row['pamphurcode']]['label'] = $row['amphur'];
                            $queryAllZone['amphur_'.$row['pamphurcode']]['sumRegis'] = 0;
                            foreach ($allMonthList as $key => $eachMonth) {
                                $queryAllZone['amphur_'.$row['pamphurcode']][$eachMonth['Ym']]=[];
                                $sumAllAmphur[$eachMonth['Ym']]=[];
                            }
                        }
                        $queryAllZone['sumAllAmphur_'.$provincecode] = $sumAllAmphur;
                        //echo '++++'.$provincecode;





                        foreach ($allMonthList as $key => $eachMonth) {
                            $sql = $sqlHead.$sqlBody.$z_code.$sqlMonth.$eachMonth['Ym'].$sqlTail;
                            //echo $sql;
                            $query = Yii::$app->db->createCommand($sql)->queryAll();

                            foreach($query as $keyQuery => $row){

                                $queryAllZone['amphur_'.$row['pamphurcode']][$eachMonth['Ym']] = $row;
                                $queryAllZone['amphur_'.$row['pamphurcode']]['sumRegis'] += $row['registerCount'];

                                $queryAllZone['sumAllAmphur_'.$row['provincecode']]['sumRegis'] += intval($row['registerCount']);

                                foreach($row as $indexQuery => $col){
                                    if($indexQuery=='pamphurcode'||$indexQuery=='provincecode')continue;
                                    $queryAllZone['sumAllAmphur_'.$row['provincecode']][$eachMonth['Ym']][$indexQuery] += (0+$col);
                                }
                            }


                        }
//                        if($z_code=8){
//                            var_dump($queryAllZone);
//
//                        }

                    }
                    //var_dump($queryAllZone);
                }
                //array_push($queryAllZone,$sumAllAmphur);

                //var_dump($queryAllZone);


                //sitecode

                array_push($queryAllZone,array('section'=>'ระดับสถานบริการ'));

                $sqlHead = 'SELECT sitecode,CONCAT(provincecode,"_",amphurcode) AS pamphurcode,';

                $sqlMonth = ' AND ym = "';

                $sqlTail = '" GROUP BY sitecode ORDER BY province,amphurcode,FIELD(hlevel,1,2,12,11,10,5,17,6,15,7,4,18,3,13,16,8,80,""),sitecode+0';

                for($z_code = 1; $z_code<=10; ++$z_code) {
                    if($z_code>1 && $z_code<6)continue;
                    if($z_code == 1 && $zone1!=1)continue;
                    else if($z_code == 6 && $zone6!=6)continue;
                    else if($z_code == 7 && $zone7!=7)continue;
                    else if($z_code == 8 && $zone8!=8)continue;
                    else if($z_code == 9 && $zone9!=9)continue;
                    else if($z_code == 10 && $zone10!=10)continue;


                    $sqlDataRow = 'SELECT sitecode,`name`,hlevel,levelname,CONCAT(provincecode,"_",amphurcode) AS pamphurcode,amphur,province
                        FROM report_data_backend
                        WHERE rptid = '.$queryControl['reportid'].'
                        AND zone_code = '.$z_code.'
                        GROUP BY sitecode
                        ORDER BY province,amphurcode,FIELD(hlevel,1,2,12,11,10,5,17,6,15,7,4,18,3,13,16,8,80,""),sitecode+0';
                    //echo $sqlDataRow;
                    $queryDataRow = Yii::$app->db->createCommand($sqlDataRow)->queryAll();
                    //var_dump($queryDataRow);

                    if(!$queryDataRow==null){
                        $province = '';
                        $amphur = '';
                        $levelname = '';
                        $pamphurcode = null;

                        $heading['heading'] = 'สถานบริการ';

                        foreach($queryDataRow as $key => $row){
                            if($amphur != $row['amphur'] || $province != $row['province']){
                                if(isset($sumAllSite)&&isset($pamphurcode)){
                                    $queryAllZone['sumAllSite_'.$pamphurcode] = $sumAllSite;
                                }
                                $levelname = '';
                                $amphur = $row['amphur'];
                                $pamphurcode = $row['pamphurcode'];
                                $province = $row['province'];
                                array_push($queryAllZone,['separator'=>'เขต '.$z_code.' / จ.'.$row['province'].' / อ.'.$row['amphur'],'separator_amphur'=>$row['pamphurcode']]);
                                array_push($queryAllZone,$heading);
                            }
                            if($levelname != $row['levelname']){
                                $levelname = $row['levelname'];
                                array_push($queryAllZone,['typesplit'=>$row['levelname']]);
                            }

                            $sumAllSite = ['label'=>'รวม','sumRegis'=>0];

                            $queryAllZone['sitecode_'.$row['sitecode']]['label'] = $row['name'];
                            $queryAllZone['sitecode_'.$row['sitecode']]['sumRegis'] = 0;

                            foreach ($allMonthList as $key => $eachMonth) {
                                $queryAllZone['sitecode_'.$row['sitecode']][$eachMonth['Ym']]=[];
                                $sumAllSite[$eachMonth['Ym']]=[];
                            }
                        }
                        $queryAllZone['sumAllSite_'.$pamphurcode] = $sumAllSite;

                        foreach ($allMonthList as $key => $eachMonth) {
                            $sql = $sqlHead.$sqlBody.$z_code.$sqlMonth.$eachMonth['Ym'].$sqlTail;
                            //echo $sql;
                            $query = Yii::$app->db->createCommand($sql)->queryAll();
                            //var_dump($query);

                            foreach($query as $keyQuery => $row){

                                $queryAllZone['sitecode_'.$row['sitecode']][$eachMonth['Ym']] = $row;

                                $queryAllZone['sitecode_'.$row['sitecode']]['sumRegis'] += $row['registerCount'];

                                $queryAllZone['sumAllSite_'.$row['pamphurcode']]['sumRegis'] += intval($row['registerCount']);

                                foreach($row as $indexQuery => $col){
                                    if($indexQuery=='amphurcode')continue;
                                    $queryAllZone['sumAllSite_'.$row['pamphurcode']][$eachMonth['Ym']][$indexQuery] += (0+$col);
                                }
                            }
                        }

                    }
                }
                //var_dump($queryAllZone);

                Yii::$app->response->format = \yii\web\Response::FORMAT_HTML;
                return $this->renderAjax('_ajaxDataMonitor', [
                    'lastcal' => $lastcal,
                    'thaimonth'=>$thaiMonth,
                    'allMonthList'=>$allMonthList,
                    'dataAllZone' => $queryAllZone,
                    'getFromDate'=>$fromDate,
                    'getToDate'=>$toDate,
                    'getType'=>$type,
                    'str'=>$str,
                    'userDetail'=>$userDetail
                ]);
            }
        }
    }

    public function actionGetListPt($table,$hsitecode,$filter,$date,$rstat,$state=0,$startdate=null,$enddate=null){

        $sqlSelectForm = "SELECT * FROM ezform WHERE ezf_table = '$table'";
        $qryEzfDetails = Yii::$app->db->createCommand($sqlSelectForm)->queryAll();
        if(strlen($qryEzfDetails[0]['field_detail'])<1){
            $qryEzfDetails[0]['field_detail'] = "'',''";
        }
        
        $tableShow = '';
        $sqlShow = '';
        if($table!='tb_data_1'){
            $tableShow = $table.', ';
            $sqlShow = "AND $table.ptid = tb_data_1.ptid ";
        }
        $sql = "SELECT DISTINCT $table.id, $table.ptid, $table.target, $table.user_create, $table.create_date, $table.rstat, $table.xsourcex ,$table.sitecode ,$table.hsitecode, LPAD($table.hptcode,5,0) AS ptcode, tb_data_1.surname, tb_data_1.name, CONCAT(".$qryEzfDetails[0]['field_detail'].") as detail FROM $tableShow tb_data_1 WHERE $table.hsitecode = '$hsitecode' ";
        $sql .= $sqlShow;

        if($filter=='all'){
            $startdate = "2013-02-09";
            $sql .= "AND $table.$date BETWEEN '2013-02-09' AND NOW() ";
        }else if($filter=='year'){
            $startdate = date("Y-01-01");
            $sql .= "AND YEAR($table.$date) = YEAR(NOW()) ";
        }else if($filter=='month'){
            $startdate = date("Y-m-01");
            $sql .= "AND DATE_FORMAT($table.$date,'%Y_%m') = DATE_FORMAT(NOW(),'%Y_%m') ";
        }else if($filter=='week'){
            $numWeek = date("W", strtotime(date("Y-m-d")));
            $dayWeek = new \DateTime();
            $dayWeek->setISODate(date("Y"), $numWeek);
            $startdate = $dayWeek->format("Y-m-d");
            $sql .= "AND DATE_FORMAT($table.$date,'%Y_%U') = DATE_FORMAT(NOW(),'%Y_%U') ";
        }else if($filter=='today'){
            $startdate = date("Y-m-d");
            $sql .= "AND DATE_FORMAT($table.$date,'%Y_%m_%d') = DATE_FORMAT(NOW(),'%Y_%m_%d') ";
        }else if($filter=='search'){
            $sql .= "AND $table.$date BETWEEN '$startdate' AND '$enddate' ";
        }

        if($rstat=='submitted'){
            $sql .= "AND ($table.rstat = 2 or $table.rstat = 4 or $table.rstat = 5)";
        }else if($rstat=='draft'){
            $sql .= "AND $table.rstat = 1 ";
        }else{
            $sql .= "AND ( $table.rstat = 1 or $table.rstat = 2 or $table.rstat = 4 or $table.rstat = 5 ) ";
        }

        if($table=="tb_data_2"){
            $sql .= "GROUP BY $table.ptid ";
        }
        if( $_GET["orderby"]=="f1vdcomp" ){
            $sql .= "ORDER BY $table.f1vdcomp DESC ";
        }else{
            $sql .= "ORDER BY $table.hsitecode ASC, $table.hptcode ASC ";
        }

        $qryDetails = Yii::$app->db->createCommand($sql)->queryAll();

        $strTable = '';
        $strTable .= '<table id="table-overview-drilldown" class="table table-hover">';
        $strTable .= '<thead>';

//        $strTable .= '<tr><td colspan="6">'.$qryEzfDetails[0]['field_detail'].'</td></tr>';
//        $strTable .= '<tr><td colspan="6">'.$sql.'</td></tr>';
//        $strTable .= '<tr><td colspan="6">'.VarDumper::dump($qryEzfDetails,10,true).'</td></tr>';
        // URL Header
        $url="";
        if( count($_GET)>0 ){
            foreach($_GET as $kg => $vg){
                if(strlen($url)>0){
                    $url.="&".$kg."=".$vg;
                }else{
                    $url.="?".$kg."=".$vg;
                }
            }
        }
        if(strlen($url)>0){
            $url_target="/timeline-event/get-list-pt/".$url."";
            $url_det="/timeline-event/get-list-pt/".$url."&orderby=f1vdcomp";
        }
        if($enddate == null)
            $enddate = date("Y-m-d");
        
        $startDate = DatePicker::widget([
                    "id" => "startDate",
                    "language" => "th",
                    "dateFormat" => "dd/MM/yyyy",
                    "value" => $startdate,
                    "options" => [
                        "class" => "form-control",
                        "style" => "width:11%",
                    ],
                    "clientOptions" => [ 
                        "defaultDate" => $startdate,
                        "minDate" => "09/02/2013",
                        "maxDate" => Yii::$app->formatter->asDate("now", "php:d/m/Y"),
                    ],
                ]);
        $endDate = DatePicker::widget([
                    "id" => "endDate",
                    "language" => "th",
                    "dateFormat" => "dd/MM/yyyy",
                    "value" => $enddate,
                    "options" => [
                        "class" => "form-control",
                        "style" => "width:11%",
                    ],
                    "clientOptions" => [ 
                        "defaultDate" => $enddate,
                        "minDate" => "09/02/2013",
                        "maxDate" => Yii::$app->formatter->asDate("now", "php:d/m/Y"),
                    ],
                ]);

        $strTable .= '<tr><td colspan="8">
            <div class="form-inline">
            <div class="form-group"><center>
            <label class="control-label">เริ่มวันที่</label>'." ".$startDate.
            '<label class="control-label">ถึงวันที่</label>'." ".$endDate.
                Html::a('<button class="btn btn-success">ค้นหา</button>','#',['id'=>'btnSearch','formData'=>'table='.$table.'&hsitecode='.$hsitecode.'&filter=search'.'&date='.$date.'&rstat='.$rstat.'&state=0'])." ".
                Html::a('<button class="btn btn-info">ผู้ป่วยทั้งหมด</button>','#',['id'=>'btnAll','formData'=>'table='.$table.'&hsitecode='.$hsitecode.'&filter='.$filter.'&date='.$date.'&rstat='.$rstat.'&state=0'])." ".
                Html::a('<button class="btn btn-info">ลงทะเบียนครั้งแรกที่นี่</button>','#',['id'=>'btnIn','formData'=>'table='.$table.'&hsitecode='.$hsitecode.'&filter='.$filter.'&date='.$date.'&rstat='.$rstat.'&state=2'])." ".
                Html::a('<button class="btn btn-info">ลงทะเบียนครั้งแรกที่โรงพยาบาลอื่น</button>','#',['id'=>'btnOut','formData'=>'table='.$table.'&hsitecode='.$hsitecode.'&filter='.$filter.'&date='.$date.'&rstat='.$rstat.'&state=1']).
            '</center></div></div></td></tr>'.

            '<tr>'.
            '<td>#</td>'.
            '<td>ชื่อเป้าหมาย</td>'.
            '<td><div id="report-overview-modal-btn-det" onclick="sortReport(2);" >รายละเอียด</div></td>'.
            '<td>บันทึกโดย</td>'.
            '<td>สถานะ</td>'.
            '<td colspan="2" width="15%">Action</td>'.
            '</tr>';
        $strTable .= '</thead>';
        $strTable .= '<tbody>';
        $i = 1;

        foreach($qryDetails as $value){
            $urlToCca02 = "/inputdata/redirect-page?dataid=".$value['id']."&ezf_id=".$qryEzfDetails[0]['ezf_id']."&rurl=".base64_encode(Yii::$app->request->url);
            $urlToEMR = "/inputdata/step4?comp_id_target=".$qryEzfDetails[0]['comp_id_target']."&ezf_id=".$qryEzfDetails[0]['ezf_id']."&target=".base64_encode($value['ptid']);
            
            if($state == 1 && $value['sitecode'] == $hsitecode) continue; //ผู้ป่วยนอก
            else if($state == 2 && $value['sitecode'] != $hsitecode) continue; //ผู้ป่วยใน
            
//            $register = "SELECT * FROM tb_data_1 WHERE ptid = '".$value['ptid']."'"; colspan="2" align="center" width="15%"  $form->field($model , 'gender') -> radio();
//            $qryregister = Yii::$app->db->createCommand($register)->queryOne();
            $user = "SELECT * FROM user_profile WHERE user_id = '".$value['user_create']."'";
            $qryuser = Yii::$app->db->createCommand($user)->queryOne();

            if($value['rstat']==1){
                $descRstat = '<span class="text-warning" title="ข้อมูลยังไม่ถูกส่งเข้าระบบด้วยการคลิก Submitted" style="color: #8a6d3b;"><i class="fa fa-pencil-square-o"></i> Save Draft</span>';
            }else if($value['rstat']==2){
                $descRstat = '<span class="text-success" title="ข้อมูลถูกส่งเข้าระบบเรียบร้อยแล้ว" style="color: #3c763d;"><i class="fa fa-send"></i> Submitted</span>';
            }else if($value['rstat']==4){
                $descRstat = '<span class="text-success" title="ข้อมูลถูกส่งเข้าระบบเรียบร้อยแล้ว" style="color: #3c763d;"><i class="fa fa-check"></i> Submitted</span>';
            }else if($value['rstat']==5){
                $descRstat = '<span clas'
                        . 's="text-success" title="ข้อมูลถูกส่งเข้าระบบเรียบร้อยแล้ว" style="color: #3c763d;"><i class="fa fa-lock"></i> Submitted</span>';
            }
            
            $strTable .= '<tr class="drilldown-emr" >'.
                '<td class="perform-click-emr">'.$i.'</td>'.//.' '.$value['surname'].' '.$value['sitecode']
                '<td class="perform-click-emr">'.($value['hsitecode'].' '.$value['ptcode'].' '.$value['name'].' '.$value['surname']).'</td>'.
                '<td class="perform-click-emr">'.$value['detail'].'</td>'.
                '<td class="perform-click-emr"><span class="text-center fa fa-2x fa-user text-success" title="บันทึกล่าสุดโดย : '.($qryuser['firstname'].' '.$qryuser['lastname']).'"></span></td>'.
                '<td class="perform-click-emr">'.$descRstat.'</td>'.
                '<td>'.
                Html::a(
                    '<button class="btn btn-primary" title="ดู / แก้ไขข้อมูลนี้"><i class="fa fa-edit"></i> ดู / แก้ไข</button>',
                    Url::to($urlToCca02),
                    [
                        'target'=>'_blank',
                    ]
                ).
                '</td>'. //step4?comp_id_target=1437725343023632100&amp;ezf_id=1437377239070461301&amp;target=all&amp;dataid=1453280105066580400
                '<td>'.
                Html::a(
                    '<button class="btn btn-primary" title="ดูรายการข้อมูลทั้งหมดของรายนี้"><i class="fa fa-eye"></i> EMR</button>',
                    Url::to($urlToEMR),
                    [
                        'target'=>'_blank',
                    ]
                ).
                '</td>'.
                '</tr>';
            $i+=1;
        }
        $strTable .= '</tbody>';
        $strTable .= '</table>';

        return $this->renderAjax('_modalListPt',[
            'strTable'=>$strTable,
        ]);
    }

//    public function actionReportOverviewRefresh()
//    {
//        $sitecode = $_GET['sitecode'];
//        Yii::$app->db->createCommand('CALL spOverview("'.$sitecode.'",1)')->execute();
//        
//        return $this->renderAjax('_ajaxOverviewReport', [
//            'siteconfig' => SiteConfig::find()->One()
//        ]);
//
//
//    }
    /******************************************************************************************************************/
    /******************************************************************************************************************/
    /******************************************************************************************************************/

    public function actionAjaxReportCca02($fiscalYear, $zone1=null, $zone6=null, $zone7=null, $zone8=null, $zone9=null, $zone10=null, $refresh=false){

        if(
            (!is_numeric($fiscalYear)) ||
            ($zone1 != null && $zone1 != 1) ||
            ($zone6 != null && $zone6 != 6) ||
            ($zone7 != null && $zone7 != 7) ||
            ($zone8 != null && $zone8 != 8) ||
            ($zone9 != null && $zone9 != 9) ||
            ($zone10 != null && $zone10 != 10)
        )return;

        if(1||Yii::$app->keyStorage->get('frontend.domain')=='cascap.in.th') {
            if (1||Yii::$app->getRequest()->isAjax) {

                $userId = Yii::$app->user->id;

                //$userId = '1435745159010042200';


                $userDetail['sitecode'] = Yii::$app->user->identity->userProfile['sitecode'];

                //$userDetail['sitecode'] = '04250';

                $sqlUserType = 'SELECT code4,name5,
                    zone_code,
                    provincecode,province,
                    amphurcode,amphur
                FROM all_hospital_thai,user_profile
                WHERE user_profile.user_id = "'.$userId.'"
                AND user_profile.sitecode = all_hospital_thai.hcode';

                //echo $sqlUserType.'<br/>';
                //Yii::$app->end();


                $queryUserType = Yii::$app->db->createCommand($sqlUserType)->queryOne();
                $userDetail['zone_code'] = $queryUserType['zone_code'];
                $userDetail['provincecode'] = $queryUserType['provincecode'];
                $userDetail['amphurcode'] = $queryUserType['amphurcode'];
                $userDetail['province'] = $queryUserType['province'];
                $userDetail['amphur'] = $queryUserType['amphur'];
                $userDetail['level'] = $queryUserType['code4'];

                $thaiMonth = ['',
                    1=>['abvt'=>'ม.ค.','full'=>'มกราคม'],
                    2=>['abvt'=>'ก.พ.', 'full'=>'กุมภาพันธ์'],
                    3=>['abvt'=>'มี.ค.', 'full'=>'มีนาคม'],
                    4=>['abvt'=>'เม.ย.', 'full'=>'เมษายน'],
                    5=>['abvt'=>'พ.ค.', 'full'=>'พฤษภาคม'],
                    6=>['abvt'=>'มิ.ย.', 'full'=>'มิถุนายน'],
                    7=>['abvt'=>'ก.ค.', 'full'=>'กรกฎาคม'],
                    8=>['abvt'=>'ส.ค.', 'full'=>'สิงหาคม'],
                    9=>['abvt'=>'ก.ย.', 'full'=>'กันยายน'],
                    10=>['abvt'=>'ต.ค.', 'full'=>'ตุลาคม'],
                    11=>['abvt'=>'พ.ย.', 'full'=>'พฤศจิกายน'],
                    12=>['abvt'=>'ธ.ค.', 'full'=>'ธันวาคม']
                ];

                $fiscalYear = intval($fiscalYear);

                $sqlAddControlTable = 'CREATE TABLE IF NOT EXISTS report_control_cca02(
                    `reportid`  int(5) NOT NULL AUTO_INCREMENT ,
                    `fiscalyear`  varchar(4) NULL ,
                    `lastcal`  datetime NULL ,
                    PRIMARY KEY (`reportid`)
                );';
                Yii::$app->db->createCommand($sqlAddControlTable)->execute();

                $sqlControl = 'SELECT reportid,lastcal FROM report_control_cca02 WHERE fiscalyear = '.$fiscalYear. ' LIMIT 0,1';
                $queryControl = Yii::$app->db->createCommand($sqlControl)->queryOne();

                /*
                echo '---'.($refresh=='true'?'yes':'no').'---';
                echo '---'.($queryControl == null?'yes':'no').'---';
                echo '---'.(((strtotime( date('Y-m-d H:i:s') ) - strtotime( $queryControl['lastcal'] )) > 3600)?'yes':'no').'---';
                */
                if($refresh=='true' || $queryControl == null || ((strtotime( date('Y-m-d H:i:s') ) - strtotime( $queryControl['lastcal'] )) > 86400) ){

                    if($queryControl == null){

                        $sqlReportId = 'insert into report_control_cca02(fiscalyear,lastcal) values('.$fiscalYear.',NOW())';
                        Yii::$app->db->createCommand($sqlReportId)->execute();

                        $reportId = Yii::$app->db->getLastInsertID();

                    }else{
                        //echo '';

                        $reportId = $queryControl['reportid'];
                        $sqlReportId = 'update report_control_cca02 set lastcal = NOW() WHERE reportid = '.$reportId;
                        Yii::$app->db->createCommand($sqlReportId)->execute();
                    }

                    $sqlProcedure = 'CALL spCca02('.$fiscalYear.','.$reportId.')';
                    Yii::$app->db->createCommand($sqlProcedure)->execute();

                    unset($queryControl);
                }
                //return;

                $queryControl = Yii::$app->db->createCommand($sqlControl)->queryOne();

                $lastcal = $queryControl['lastcal'];

                //var_dump($querycontrol);

                $heading = [
                    'heading'=>'พื้นที่'
                ];

                $queryAllZone = array(['section'=>'เขตบริการสุขภาพ']);
                array_push($queryAllZone,$heading);

                for($z_code = 1; $z_code<=10; ++$z_code){
                    if($z_code>1 && $z_code<6)continue;

                    $sqlZone = 'SELECT CONCAT("เขต ",'.$z_code.'+0) as zone,
                        sum(registerCount) AS registerCount,
                        sum(m10) as m10,
                        sum(m11) as m11,
                        sum(m12) as m12,
                        sum(m01) as m01,
                        sum(m02) as m02,
                        sum(m03) as m03,
                        sum(m04) as m04,
                        sum(m05) as m05,
                        sum(m06) as m06,
                        sum(m07) as m07,
                        sum(m08) as m08,
                        sum(m09) as m09
                    FROM report_cca02_backend WHERE rptid = '.$queryControl['reportid'].'
                    AND zone_code='.$z_code;

                    //echo $sqlzone;

                    $queryZone = Yii::$app->db->createCommand($sqlZone)->queryOne();
                    array_push($queryAllZone, $queryZone);
                }

                $sumEachMonth = array('summation'=>'รวม');
                $sumTotal = 0;
                foreach($queryAllZone as $key => $row){

                    if(isset($row['heading']) || isset($row['section']) || isset($row['separator']))continue;

                    $sumEachMonth['registerCount'] += (0+intval($row['registerCount']));

                    foreach($row as $colIndex => $col){
                        if(preg_match('/^m[\d]{2}/',$colIndex)){
                            $sumEachMonth[$colIndex] += (0+intval($col));
                           $sumTotal += intval($col);
                        }
                    }

                    $sumEachZone =
                        intval($row['m10'])+
                        intval($row['m11'])+
                        intval($row['m12'])+
                        intval($row['m01'])+
                        intval($row['m02'])+
                        intval($row['m03'])+
                        intval($row['m04'])+
                        intval($row['m05'])+
                        intval($row['m06'])+
                        intval($row['m07'])+
                        intval($row['m08'])+
                        intval($row['m09']);

                    $queryAllZone[$key]['total'] = $sumEachZone;
                    $queryAllZone[$key]['pct'] =  ($sumEachZone == 0 || intval($row['registerCount']) == 0 ? 0 : ($sumEachZone / intval($row['registerCount']) ) * 100);

                }

                $sumEachMonth['total'] = $sumTotal;
                $sumEachMonth['pct'] = ($sumTotal == 0 || intval($sumEachMonth['registerCount']) == 0 ? 0 : ($sumTotal / intval($sumEachMonth['registerCount']) ) * 100);


                array_push($queryAllZone,$sumEachMonth);

                //var_dump($queryAllZone);

                if($zone1==null && $zone6==null && $zone7==null && $zone8==null && $zone9==null && $zone10==null){
                    Yii::$app->response->format = \yii\web\Response::FORMAT_HTML;
                    return $this->renderAjax('_ajaxReportCca02', [
                        'fiscalYear' => $fiscalYear,
                        'lastcal' => $lastcal,
                        'thaimonth'=>$thaiMonth,
                        'cca02AllZone' => $queryAllZone,
                        'userDetail'=>$userDetail
                    ]);
                }


                $sqlBody = 'sum(registerCount) AS registerCount,
                    sum(m10) as m10,
                    sum(m11) as m11,
                    sum(m12) as m12,
                    sum(m01) as m01,
                    sum(m02) as m02,
                    sum(m03) as m03,
                    sum(m04) as m04,
                    sum(m05) as m05,
                    sum(m06) as m06,
                    sum(m07) as m07,
                    sum(m08) as m08,
                    sum(m09) as m09
                FROM report_cca02_backend WHERE rptid = '.$queryControl['reportid'].'
                AND zone_code=';

                $sqlHead = 'SELECT province,';

                $sqlTail = ' GROUP BY provincecode ORDER BY province';



                // province in zone

                $sumCol = null;
                array_push($queryAllZone,array('section'=>'รายจังหวัด'));
                for($z_code = 1; $z_code<=10; ++$z_code){
                    if($z_code>1 && $z_code<6)continue;
                    if($z_code == 1 && $zone1!=1)continue;
                    else if($z_code == 6 && $zone6!=6)continue;
                    else if($z_code == 7 && $zone7!=7)continue;
                    else if($z_code == 8 && $zone8!=8)continue;
                    else if($z_code == 9 && $zone9!=9)continue;
                    else if($z_code == 10 && $zone10!=10)continue;


                    $sql = $sqlHead.$sqlBody.$z_code.$sqlTail;
                    $query = Yii::$app->db->createCommand($sql)->queryAll();


                    if(!$query==null){

                        if($sumCol!=null){

                            $sumCol['pct'] = $sumCol['registerCount']==0?0:(floatval($sumCol['total'])/floatval($sumCol['registerCount']))*100;
                            array_push($queryAllZone,$sumCol);
                        }

                        array_push($queryAllZone,['separator'=>'เขต '.$z_code,'separator_zone'=>$z_code]);
                        $heading['heading'] = 'จังหวัด';
                        array_push($queryAllZone,$heading);

                        $sumCol = [
                            'summation'=>'รวม',
                            'registerCount'=>0,
                            'm10'=>0,
                            'm11'=>0,
                            'm12'=>0,
                            'm01'=>0,
                            'm02'=>0,
                            'm03'=>0,
                            'm04'=>0,
                            'm05'=>0,
                            'm06'=>0,
                            'm07'=>0,
                            'm08'=>0,
                            'm09'=>0,
                            'total'=>0
                        ];
                    }


                    //var_dump($queryProvince);
                    foreach($query as $key => $row){
                        $sumEachProvince =
                            intval($row['m10'])+
                            intval($row['m11'])+
                            intval($row['m12'])+
                            intval($row['m01'])+
                            intval($row['m02'])+
                            intval($row['m03'])+
                            intval($row['m04'])+
                            intval($row['m05'])+
                            intval($row['m06'])+
                            intval($row['m07'])+
                            intval($row['m08'])+
                            intval($row['m09']);

                        $query[$key]['total'] = $sumEachProvince;
                        $query[$key]['pct'] =  ($sumEachProvince == 0 || intval($row['registerCount']) == 0 ? 0 : ($sumEachProvince / intval($row['registerCount']) ) * 100);

                        array_push($queryAllZone,$query[$key]);

                        $sumCol['registerCount']+=$row['registerCount'];
                        $sumCol['m10']+=$row['m10'];
                        $sumCol['m11']+=$row['m11'];
                        $sumCol['m12']+=$row['m12'];
                        $sumCol['m01']+=$row['m01'];
                        $sumCol['m02']+=$row['m02'];
                        $sumCol['m03']+=$row['m03'];
                        $sumCol['m04']+=$row['m04'];
                        $sumCol['m05']+=$row['m05'];
                        $sumCol['m06']+=$row['m06'];
                        $sumCol['m07']+=$row['m07'];
                        $sumCol['m08']+=$row['m08'];
                        $sumCol['m09']+=$row['m09'];
                        $sumCol['total']+=$sumEachProvince;

                    }

                }
                if(!$query==null){
                    $sumCol['pct'] = $sumCol['registerCount']==0?0:($sumCol['total']/$sumCol['registerCount'])*100;
                    array_push($queryAllZone,$sumCol);
                }


//                var_dump($queryAllZone);
//                return;


                // amphur in zone
                $sqlHead = 'SELECT province,provincecode,amphur,';
                $sqlTail = ' GROUP BY provincecode, amphurcode ORDER BY province, amphurcode';
                array_push($queryAllZone,array('section'=>'รายอำเภอ'));

                $sumCol = null;

                for($z_code = 1; $z_code<=10; ++$z_code){
                    if($z_code>1 && $z_code<6)continue;
                    if($z_code == 1 && $zone1!=1)continue;
                    else if($z_code == 6 && $zone6!=6)continue;
                    else if($z_code == 7 && $zone7!=7)continue;
                    else if($z_code == 8 && $zone8!=8)continue;
                    else if($z_code == 9 && $zone9!=9)continue;
                    else if($z_code == 10 && $zone10!=10)continue;

                    $sql = $sqlHead.$sqlBody.$z_code.$sqlTail;

                    $query = Yii::$app->db->createCommand($sql)->queryAll();
                    if(!$query==null){
                        //array_push($queryAllZone,array('separator'=>'เขต '.$z_code));
                    }

                    //var_dump($queryProvince);
                    $province = '';

                    foreach($query as $key => $row){

                        if($province != $row['province']){

                            if($sumCol!=null){
                                $sumCol['pct'] = $sumCol['registerCount']==0?0:(($sumCol['total']*1.00)/$sumCol['registerCount'])*100;
                                array_push($queryAllZone,$sumCol);
                            }

                            $province = $row['province'];
                            array_push($queryAllZone,['separator'=>'เขต '.$z_code.' / จ.'.$row['province'],'separator_province'=>$row['provincecode']]);
                            $heading['heading'] = 'อำเภอ';
                            array_push($queryAllZone,$heading);

                            $sumCol = [
                                'summation'=>'รวม',
                                'registerCount'=>0,
                                'm10'=>0,
                                'm11'=>0,
                                'm12'=>0,
                                'm01'=>0,
                                'm02'=>0,
                                'm03'=>0,
                                'm04'=>0,
                                'm05'=>0,
                                'm06'=>0,
                                'm07'=>0,
                                'm08'=>0,
                                'm09'=>0,
                                'total'=>0
                            ];
                        }

                        unset($query[$key]['province']);

                        $sumEachProvince =
                            intval($row['m10'])+
                            intval($row['m11'])+
                            intval($row['m12'])+
                            intval($row['m01'])+
                            intval($row['m02'])+
                            intval($row['m03'])+
                            intval($row['m04'])+
                            intval($row['m05'])+
                            intval($row['m06'])+
                            intval($row['m07'])+
                            intval($row['m08'])+
                            intval($row['m09']);

                        $query[$key]['total'] = $sumEachProvince;
                        $query[$key]['pct'] =  ($sumEachProvince == 0 || intval($row['registerCount']) == 0 ? 0 : ($sumEachProvince / intval($row['registerCount']) ) * 100);

                        array_push($queryAllZone,$query[$key]);

                        $sumCol['registerCount']+=$row['registerCount'];
                        $sumCol['m10']+=$row['m10'];
                        $sumCol['m11']+=$row['m11'];
                        $sumCol['m12']+=$row['m12'];
                        $sumCol['m01']+=$row['m01'];
                        $sumCol['m02']+=$row['m02'];
                        $sumCol['m03']+=$row['m03'];
                        $sumCol['m04']+=$row['m04'];
                        $sumCol['m05']+=$row['m05'];
                        $sumCol['m06']+=$row['m06'];
                        $sumCol['m07']+=$row['m07'];
                        $sumCol['m08']+=$row['m08'];
                        $sumCol['m09']+=$row['m09'];
                        $sumCol['total']+=$sumEachProvince;

                    }

                }
                if(!$query==null){
                    $sumCol['pct'] = $sumCol['registerCount']==0?0:(($sumCol['total']*1.00)/$sumCol['registerCount'])*100;
                    array_push($queryAllZone,$sumCol);
                }



                // hcode in zone
                $sqlHead = 'SELECT hsitecode,province,amphur,CONCAT(provincecode,"_",amphurcode) AS pamphurcode,levelname,name,';
                $sqlTail = ' GROUP BY hsitecode ORDER BY province,amphurcode,FIELD(hlevel,1,2,12,11,10,5,17,6,15,7,4,18,3,13,16,8,80,""),hsitecode+0';
                array_push($queryAllZone,array('section'=>'ระดับสถานบริการ'));
                $sumCol = null;
                for($z_code = 1; $z_code<=10; ++$z_code){
                    if($z_code>1 && $z_code<6)continue;
                    if($z_code == 1 && $zone1!=1)continue;
                    else if($z_code == 6 && $zone6!=6)continue;
                    else if($z_code == 7 && $zone7!=7)continue;
                    else if($z_code == 8 && $zone8!=8)continue;
                    else if($z_code == 9 && $zone9!=9)continue;
                    else if($z_code == 10 && $zone10!=10)continue;

                    $sql = $sqlHead.$sqlBody.$z_code.$sqlTail;

                    $query = Yii::$app->db->createCommand($sql)->queryAll();
                    if(!$query==null){

                    }
                    //var_dump($queryProvince);
                    $province = '';
                    $amphur = '';
                    $levelname = '';
                    foreach($query as $key => $row){
                        if( $amphur != $row['amphur'] || $province != $row['province']){

                            if($sumCol!=null){
                                $sumCol['pct'] = $sumCol['registerCount']==0?0:($sumCol['total']/$sumCol['registerCount'])*100;
                                array_push($queryAllZone,$sumCol);
                            }

                            $levelname = '';
                            $amphur = $row['amphur'];
                            $province = $row['province'];
                            array_push($queryAllZone,['separator'=>'เขต '.$z_code.' / จ.'.$row['province'].' / อ.'.$row['amphur'],'separator_amphur'=>$row['pamphurcode']]);
                            //array_push($queryAllZone,array('separator'=>'อ.'.$amphur));
                            $heading['heading'] = 'สถานบริการ';
                            array_push($queryAllZone,$heading);

                            $sumCol = [
                                'summation'=>'รวม',
                                'registerCount'=>0,
                                'm10'=>0,
                                'm11'=>0,
                                'm12'=>0,
                                'm01'=>0,
                                'm02'=>0,
                                'm03'=>0,
                                'm04'=>0,
                                'm05'=>0,
                                'm06'=>0,
                                'm07'=>0,
                                'm08'=>0,
                                'm09'=>0,
                                'total'=>0
                            ];
                        }

                        if($levelname != $row['levelname']){
                            $levelname = $row['levelname'];
                            array_push($queryAllZone,array('typesplit'=>$levelname));
                        }
                        unset($query[$key]['province']);
                        unset($query[$key]['amphur']);
                        unset($query[$key]['levelname']);

                        $sumEachProvince =
                            intval($row['m10'])+
                            intval($row['m11'])+
                            intval($row['m12'])+
                            intval($row['m01'])+
                            intval($row['m02'])+
                            intval($row['m03'])+
                            intval($row['m04'])+
                            intval($row['m05'])+
                            intval($row['m06'])+
                            intval($row['m07'])+
                            intval($row['m08'])+
                            intval($row['m09']);

                        $query[$key]['total'] = $sumEachProvince;
                        $query[$key]['pct'] =  ($sumEachProvince == 0 || intval($row['registerCount']) == 0 ? 0 : ($sumEachProvince / intval($row['registerCount']) ) * 100);

                        array_push($queryAllZone,$query[$key]);

                        $sumCol['registerCount']+=$row['registerCount'];
                        $sumCol['m10']+=$row['m10'];
                        $sumCol['m11']+=$row['m11'];
                        $sumCol['m12']+=$row['m12'];
                        $sumCol['m01']+=$row['m01'];
                        $sumCol['m02']+=$row['m02'];
                        $sumCol['m03']+=$row['m03'];
                        $sumCol['m04']+=$row['m04'];
                        $sumCol['m05']+=$row['m05'];
                        $sumCol['m06']+=$row['m06'];
                        $sumCol['m07']+=$row['m07'];
                        $sumCol['m08']+=$row['m08'];
                        $sumCol['m09']+=$row['m09'];
                        $sumCol['total']+=$sumEachProvince;
                    }
                }

                if(!$query==null){
                    $sumCol['pct'] = $sumCol['registerCount']==0?0:($sumCol['total']/$sumCol['registerCount'])*100;
                    array_push($queryAllZone,$sumCol);
                }

                Yii::$app->response->format = \yii\web\Response::FORMAT_HTML;
                return $this->renderAjax('_ajaxReportCca02', [
                    'fiscalYear' => $fiscalYear,
                    'lastcal' => $lastcal,
                    'thaimonth'=>$thaiMonth,
                    'cca02AllZone' => $queryAllZone,
                    'userDetail'=>$userDetail
                ]);
            }
        }
    }

    public function actionAllProofCca($fiscalYear, $zone1=null, $zone6=null, $zone7=null, $zone8=null, $zone9=null, $zone10=null, $refresh=false){
        if(
            (!is_numeric($fiscalYear)) ||
            ($zone1 != null && $zone1 != 1) ||
            ($zone6 != null && $zone6 != 6) ||
            ($zone7 != null && $zone7 != 7) ||
            ($zone8 != null && $zone8 != 8) ||
            ($zone9 != null && $zone9 != 9) ||
            ($zone10 != null && $zone10 != 10)
        )return;
        if(1||Yii::$app->keyStorage->get('frontend.domain')=='cascap.in.th') {
            if (1 || Yii::$app->getRequest()->isAjax) {

                $userId = Yii::$app->user->id;

                //$userId = '1435745159010042200';


                $userDetail['sitecode'] = Yii::$app->user->identity->userProfile['sitecode'];

                //$userDetail['sitecode'] = '04250';

                $sqlUserType = 'SELECT code4,name5,
                    zone_code,
                    provincecode,province,
                    amphurcode,amphur
                FROM all_hospital_thai,user_profile
                WHERE user_profile.user_id = "'.$userId.'"
                AND user_profile.sitecode = all_hospital_thai.hcode';

                //echo $sqlUserType.'<br/>';
                //Yii::$app->end();


                $queryUserType = Yii::$app->db->createCommand($sqlUserType)->queryOne();
                $userDetail['zone_code'] = $queryUserType['zone_code'];
                $userDetail['provincecode'] = $queryUserType['provincecode'];
                $userDetail['amphurcode'] = $queryUserType['amphurcode'];
                $userDetail['province'] = $queryUserType['province'];
                $userDetail['amphur'] = $queryUserType['amphur'];
                $userDetail['level'] = $queryUserType['code4'];

                $thaiMonth = ['',
                    1=>['abvt'=>'ม.ค.','full'=>'มกราคม'],
                    2=>['abvt'=>'ก.พ.', 'full'=>'กุมภาพันธ์'],
                    3=>['abvt'=>'มี.ค.', 'full'=>'มีนาคม'],
                    4=>['abvt'=>'เม.ย.', 'full'=>'เมษายน'],
                    5=>['abvt'=>'พ.ค.', 'full'=>'พฤษภาคม'],
                    6=>['abvt'=>'มิ.ย.', 'full'=>'มิถุนายน'],
                    7=>['abvt'=>'ก.ค.', 'full'=>'กรกฎาคม'],
                    8=>['abvt'=>'ส.ค.', 'full'=>'สิงหาคม'],
                    9=>['abvt'=>'ก.ย.', 'full'=>'กันยายน'],
                    10=>['abvt'=>'ต.ค.', 'full'=>'ตุลาคม'],
                    11=>['abvt'=>'พ.ย.', 'full'=>'พฤศจิกายน'],
                    12=>['abvt'=>'ธ.ค.', 'full'=>'ธันวาคม']
                ];

                $fiscalYear = intval($fiscalYear);

                $sqlAddControlTable = 'CREATE TABLE IF NOT EXISTS report_control_proof(
                    `reportid`  int(5) NOT NULL AUTO_INCREMENT ,
                    `fiscalyear`  varchar(4) NULL ,
                    `lastcal`  datetime NULL ,
                    PRIMARY KEY (`reportid`)
                );';
                Yii::$app->db->createCommand($sqlAddControlTable)->execute();

                $sqlControl = 'SELECT reportid,lastcal FROM report_control_proof WHERE fiscalyear = '.$fiscalYear. ' LIMIT 0,1';
                $queryControl = Yii::$app->db->createCommand($sqlControl)->queryOne();

                if($refresh=='true' || $queryControl == null || ((strtotime( date('Y-m-d H:i:s') ) - strtotime( $queryControl['lastcal'] )) > 86400) ){

                    if($queryControl == null){

                        $sqlReportId = 'insert into report_control_proof(fiscalyear,lastcal) values('.$fiscalYear.',NOW())';
                        Yii::$app->db->createCommand($sqlReportId)->execute();

                        $reportId = Yii::$app->db->getLastInsertID();

                    }else{
                        //echo '';

                        $reportId = $queryControl['reportid'];
                        $sqlReportId = 'update report_control_proof set lastcal = NOW() WHERE reportid = '.$reportId;
                        Yii::$app->db->createCommand($sqlReportId)->execute();
                    }

                    $sqlProcedure = 'CALL spProof('.$fiscalYear.','.$reportId.')';
                    Yii::$app->db->createCommand($sqlProcedure)->execute();

                    unset($queryControl);
                }
                //return;

                $queryControl = Yii::$app->db->createCommand($sqlControl)->queryOne();

                $lastcal = $queryControl['lastcal'];

                //var_dump($querycontrol);

                $heading = [
                    'heading'=>'พื้นที่'
                ];

                $queryAllZone = array(['section'=>'เขตบริการสุขภาพ']);
                array_push($queryAllZone,$heading);

                for($z_code = 1; $z_code<=10; ++$z_code){
                    if($z_code>1 && $z_code<6)continue;

                    $sqlZone = 'SELECT CONCAT("เขต ",'.$z_code.'+0) as zone,
                        SUM(registerCount) AS registerCount,
                        SUM(cca02Count) AS cca02Count,
                        0 AS cca02Pct,
                        SUM(suspected) AS suspected,
                        SUM(ctmriCount) AS ctmriCount,
                        SUM(ccaCount) AS ccaCount,
                        0 AS ccaPct
                    FROM report_proof_backend WHERE rptid = '.$queryControl['reportid'].'
                    AND zone_code='.$z_code;

                    //echo $sqlzone;

                    $queryZone = Yii::$app->db->createCommand($sqlZone)->queryOne();
                    array_push($queryAllZone, $queryZone);
                }

                $sumAll = array('summation'=>'รวม');

                foreach($queryAllZone as $key => $row){

                    if(isset($row['heading']) || isset($row['section']) || isset($row['separator']))continue;

                    $sumAll['registerCount'] += (0+intval($row['registerCount']));
                    $sumAll['cca02Count'] += (0+intval($row['cca02Count']));
                    $sumAll['cca02Pct'] = 0;
                    $sumAll['suspected'] += (0+intval($row['suspected']));
                    $sumAll['ctmriCount'] += (0+intval($row['ctmriCount']));
                    $sumAll['ccaCount'] += (0+intval($row['ccaCount']));
                    $sumAll['ccaPct'] = 0;

                    $queryAllZone[$key]['cca02Pct'] = ( intval($row['registerCount']) == 0 ? 0 : ($row['cca02Count']/$row['registerCount'])*100 );
                    $queryAllZone[$key]['ccaPct'] = ( intval($row['cca02Count']) == 0 ? 0 : ($row['ccaCount']/$row['cca02Count'])*100 );

                }

                $sumAll['cca02Pct'] = ( intval($sumAll['registerCount']) == 0 ? 0 : ($sumAll['cca02Count']/$sumAll['registerCount'])*100 );
                $sumAll['ccaPct'] = ( intval($sumAll['cca02Count']) == 0 ? 0 : ($sumAll['ccaCount']/$sumAll['cca02Count'])*100 );


                array_push($queryAllZone,$sumAll);

                //var_dump($queryAllZone);

                if($zone1==null && $zone6==null && $zone7==null && $zone8==null && $zone9==null && $zone10==null){
                    Yii::$app->response->format = \yii\web\Response::FORMAT_HTML;
                    return $this->renderAjax('_ajaxAllProofCca', [
                        'fiscalYear' => $fiscalYear,
                        'lastcal' => $lastcal,
                        'thaimonth'=>$thaiMonth,
                        'proofAllZone' => $queryAllZone,
                        'userDetail'=>$userDetail
                    ]);
                }


                $sqlBody = 'SUM(registerCount) AS registerCount,
                        SUM(registerCount) AS registerCount,
                        SUM(cca02Count) AS cca02Count,
                        0 AS cca02Pct,
                        SUM(suspected) AS suspected,
                        SUM(ctmriCount) AS ctmriCount,
                        SUM(ccaCount) AS ccaCount,
                        0 AS ccaPct
                    FROM report_proof_backend WHERE rptid = '.$queryControl['reportid'].'
                    AND zone_code=';

                $sqlHead = 'SELECT province,';

                $sqlTail = ' GROUP BY provincecode ORDER BY province';

                $sumCol = null;
                // province in zone
                array_push($queryAllZone,array('section'=>'รายจังหวัด'));
                for($z_code = 1; $z_code<=10; ++$z_code){
                    if($z_code>1 && $z_code<6)continue;
                    if($z_code == 1 && $zone1!=1)continue;
                    else if($z_code == 6 && $zone6!=6)continue;
                    else if($z_code == 7 && $zone7!=7)continue;
                    else if($z_code == 8 && $zone8!=8)continue;
                    else if($z_code == 9 && $zone9!=9)continue;
                    else if($z_code == 10 && $zone10!=10)continue;


                    $sql = $sqlHead.$sqlBody.$z_code.$sqlTail;
                    $query = Yii::$app->db->createCommand($sql)->queryAll();
                    if(!$query==null){

                        if($sumCol!=null){
                            $sumCol['cca02Pct'] = $sumCol['registerCount']==0?0:($sumCol['cca02Count']/$sumCol['registerCount'])*100;
                            $sumCol['ccaPct'] = $sumCol['cca02Count']==0?0:($sumCol['ccaCount']/$sumCol['cca02Count'])*100;

                            array_push($queryAllZone,$sumCol);
                        }

                        array_push($queryAllZone,['separator'=>'เขต '.$z_code,'separator_zone'=>$z_code]);
                        $heading['heading'] = 'จังหวัด';
                        array_push($queryAllZone,$heading);

                        $sumCol = [
                            'summation'=>'รวม',
                            'registerCount'=>0,
                            'cca02Count'=>0,
                            'cca02Pct'=>0,
                            'suspected'=>0,
                            'ctmriCount'=>0,
                            'ccaCount'=>0,
                            'ccaPct'=>0
                        ];

                    }

                    //var_dump($queryProvince);
                    foreach($query as $key => $row){

                        $query[$key]['cca02Pct'] = ( intval($row['registerCount']) == 0 ? 0 : ($row['cca02Count']/$row['registerCount'])*100 );
                        $query[$key]['ccaPct'] = ( intval($row['cca02Count']) == 0 ? 0 : ($row['ccaCount']/$row['cca02Count'])*100 );

                        array_push($queryAllZone,$query[$key]);

                        $sumCol['registerCount']+=$row['registerCount'];
                        $sumCol['cca02Count']+=$row['cca02Count'];
                        $sumCol['cca02Pct']=0;
                        $sumCol['suspected']+=$row['suspected'];
                        $sumCol['ctmriCount']+=$row['ctmriCount'];
                        $sumCol['ccaCount']+=$row['ccaCount'];
                        $sumCol['ccaPct']=0;

                    }
                }
                if(!$query==null){
                    $sumCol['cca02Pct'] = $sumCol['registerCount']==0?0:($sumCol['cca02Count']/$sumCol['registerCount'])*100;
                    $sumCol['ccaPct'] = $sumCol['cca02Count']==0?0:($sumCol['ccaCount']/$sumCol['cca02Count'])*100;

                    array_push($queryAllZone,$sumCol);
                }

                // amphur in zone
                $sumCol=null;
                $sqlHead = 'SELECT province,provincecode,amphur,';
                $sqlTail = ' GROUP BY provincecode, amphurcode ORDER BY province, amphurcode';
                array_push($queryAllZone,array('section'=>'รายอำเภอ'));
                for($z_code = 1; $z_code<=10; ++$z_code){
                    if($z_code>1 && $z_code<6)continue;
                    if($z_code == 1 && $zone1!=1)continue;
                    else if($z_code == 6 && $zone6!=6)continue;
                    else if($z_code == 7 && $zone7!=7)continue;
                    else if($z_code == 8 && $zone8!=8)continue;
                    else if($z_code == 9 && $zone9!=9)continue;
                    else if($z_code == 10 && $zone10!=10)continue;

                    $sql = $sqlHead.$sqlBody.$z_code.$sqlTail;

                    $query = Yii::$app->db->createCommand($sql)->queryAll();
                    if(!$query==null){
                        //array_push($queryAllZone,array('separator'=>'เขต '.$z_code));
                    }

                    //var_dump($queryProvince);
                    $province = '';
                    foreach($query as $key => $row){
                        if($province != $row['province']){

                            if($sumCol!=null){
                                $sumCol['cca02Pct'] = $sumCol['registerCount']==0?0:($sumCol['cca02Count']/$sumCol['registerCount'])*100;
                                $sumCol['ccaPct'] = $sumCol['cca02Count']==0?0:($sumCol['ccaCount']/$sumCol['cca02Count'])*100;

                                array_push($queryAllZone,$sumCol);
                            }

                            $province = $row['province'];
                            array_push($queryAllZone,['separator'=>'เขต '.$z_code.' / จ.'.$row['province'],'separator_province'=>$row['provincecode']]);
                            $heading['heading'] = 'อำเภอ';
                            array_push($queryAllZone,$heading);

                            $sumCol = [
                                'summation'=>'รวม',
                                'registerCount'=>0,
                                'cca02Count'=>0,
                                'cca02Pct'=>0,
                                'suspected'=>0,
                                'ctmriCount'=>0,
                                'ccaCount'=>0,
                                'ccaPct'=>0
                            ];
                        }
                        unset($query[$key]['province']);

                        $query[$key]['cca02Pct'] = ( intval($row['registerCount']) == 0 ? 0 : ($row['cca02Count']/$row['registerCount'])*100 );
                        $query[$key]['ccaPct'] = ( intval($row['cca02Count']) == 0 ? 0 : ($row['ccaCount']/$row['cca02Count'])*100 );

                        array_push($queryAllZone,$query[$key]);

                        $sumCol['registerCount']+=$row['registerCount'];
                        $sumCol['cca02Count']+=$row['cca02Count'];
                        $sumCol['cca02Pct']=0;
                        $sumCol['suspected']+=$row['suspected'];
                        $sumCol['ctmriCount']+=$row['ctmriCount'];
                        $sumCol['ccaCount']+=$row['ccaCount'];
                        $sumCol['ccaPct']=0;
                    }
                }
                if(!$query==null){
                    $sumCol['cca02Pct'] = $sumCol['registerCount']==0?0:($sumCol['cca02Count']/$sumCol['registerCount'])*100;
                    $sumCol['ccaPct'] = $sumCol['cca02Count']==0?0:($sumCol['ccaCount']/$sumCol['cca02Count'])*100;

                    array_push($queryAllZone,$sumCol);
                }

                // hcode in zone
                $sqlHead = 'SELECT sitecode,province,CONCAT(provincecode,"_",amphurcode) AS pamphurcode,amphur,levelname,name,';
                $sqlTail = ' GROUP BY sitecode ORDER BY province,amphurcode,FIELD(hlevel,1,2,12,11,10,5,17,6,15,7,4,18,3,13,16,8,80,""),sitecode+0';
                array_push($queryAllZone,array('section'=>'ระดับสถานบริการ'));

                $sumCol = null;
                for($z_code = 1; $z_code<=10; ++$z_code){
                    if($z_code>1 && $z_code<6)continue;
                    if($z_code == 1 && $zone1!=1)continue;
                    else if($z_code == 6 && $zone6!=6)continue;
                    else if($z_code == 7 && $zone7!=7)continue;
                    else if($z_code == 8 && $zone8!=8)continue;
                    else if($z_code == 9 && $zone9!=9)continue;
                    else if($z_code == 10 && $zone10!=10)continue;

                    $sql = $sqlHead.$sqlBody.$z_code.$sqlTail;

                    $query = Yii::$app->db->createCommand($sql)->queryAll();
                    if(!$query==null){

                    }
                    //var_dump($queryProvince);
                    $province = '';
                    $amphur = '';
                    $levelname = '';
                    foreach($query as $key => $row){
                        if( $amphur != $row['amphur'] || $province != $row['province']){

                            if($sumCol!=null){
                                $sumCol['cca02Pct'] = $sumCol['registerCount']==0?0:($sumCol['cca02Count']/$sumCol['registerCount'])*100;
                                $sumCol['ccaPct'] = $sumCol['cca02Count']==0?0:($sumCol['ccaCount']/$sumCol['cca02Count'])*100;

                                array_push($queryAllZone,$sumCol);
                            }

                            $levelname = '';
                            $amphur = $row['amphur'];
                            $province = $row['province'];
                            array_push($queryAllZone,['separator'=>'เขต '.$z_code.' / จ.'.$row['province'].' / อ.'.$row['amphur'],'separator_amphur'=>$row['pamphurcode']]);
                            //array_push($queryAllZone,array('separator'=>'อ.'.$amphur));
                            $heading['heading'] = 'สถานบริการ';
                            array_push($queryAllZone,$heading);

                            $sumCol = [
                                'summation'=>'รวม',
                                'registerCount'=>0,
                                'cca02Count'=>0,
                                'cca02Pct'=>0,
                                'suspected'=>0,
                                'ctmriCount'=>0,
                                'ccaCount'=>0,
                                'ccaPct'=>0
                            ];
                        }

                        if($levelname != $row['levelname']){
                            $levelname = $row['levelname'];
                            array_push($queryAllZone,array('typesplit'=>$levelname));
                        }
                        unset($query[$key]['province']);
                        unset($query[$key]['amphur']);
                        unset($query[$key]['levelname']);

                        $query[$key]['cca02Pct'] = ( intval($row['registerCount']) == 0 ? 0 : ($row['cca02Count']/$row['registerCount'])*100 );
                        $query[$key]['ccaPct'] = ( intval($row['cca02Count']) == 0 ? 0 : ($row['ccaCount']/$row['cca02Count'])*100 );

                        array_push($queryAllZone,$query[$key]);

                        $sumCol['registerCount']+=$row['registerCount'];
                        $sumCol['cca02Count']+=$row['cca02Count'];
                        $sumCol['cca02Pct']=0;
                        $sumCol['suspected']+=$row['suspected'];
                        $sumCol['ctmriCount']+=$row['ctmriCount'];
                        $sumCol['ccaCount']+=$row['ccaCount'];
                        $sumCol['ccaPct']=0;
                    }
                }
                if(!$query==null){
                    $sumCol['cca02Pct'] = $sumCol['registerCount']==0?0:($sumCol['cca02Count']/$sumCol['registerCount'])*100;
                    $sumCol['ccaPct'] = $sumCol['cca02Count']==0?0:($sumCol['ccaCount']/$sumCol['cca02Count'])*100;

                    array_push($queryAllZone,$sumCol);
                }

                Yii::$app->response->format = \yii\web\Response::FORMAT_HTML;
                return $this->renderAjax('_ajaxAllProofCca', [
                    'fiscalYear' => $fiscalYear,
                    'lastcal' => $lastcal,
                    'thaimonth'=>$thaiMonth,
                    'proofAllZone' => $queryAllZone,
                    'userDetail'=>$userDetail
                ]);

            }
        }
    }

    public function actionDismissChromeAlert($dismiss){
        if(Yii::$app->session->has('chrome-alert') && Yii::$app->session->get('chrome-alert')=='dismissed'){
            return json_encode(['dismiss'=>'ok']);
        }
        Yii::$app->session->set('chrome-alert', 'dismissed');
        return json_encode(['dismiss'=>'ok']);
    }
}
