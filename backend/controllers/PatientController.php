<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace backend\controllers;

use Yii;

/**
 * Description of PatientController
 *
 * @author AK
 */
class PatientController extends \yii\web\Controller {

//put your code here


    public function actionIndex() {

        $tbDivision = 'tbdata_1464024004038379500';
        $ezdata = isset($_GET['ezdata']) ? $_GET['ezdata'] : '';
        $week = isset($_GET['week']) ? explode(" ", $_GET['week']) : '';
        $weekStart = date_format(date_create($week[0]), "Y-m-d");
        $weekEnd = date_format(date_create($week[2]), "Y-m-d");
        if ($ezdata == '1471505524053879200') {

            $ezfdata = Yii::$app->db->createCommand("SELECT ezf_id,detail FROM ezform_data_manage WHERE id = :id;", [':id' => $ezdata])->queryOne();
            //\appxq\sdii\utils\VarDumper::dump($ezfdata);
            $ezf = Yii::$app->db->createCommand("SELECT ezf_table FROM ezform WHERE ezf_id = :id;", [':id' => $ezfdata['ezf_id']])->queryOne();
            $division = \Yii::$app->db->createCommand("SELECT dorder,name FROM {$tbDivision} WHERE dorder > 0 ORDER BY CAST(dorder AS SIGNED) ASC")->queryAll();
            //\appxq\sdii\utils\VarDumper::dump($division);
            $sum = '';
            $sql = '';
            foreach ($division as $value) {


                $patho = \Yii::$app->db->createCommand("SELECT patho FROM  {$ezf['ezf_table']} WHERE ndivision = {$value['dorder']} "
                                . "AND (nbdate = '" . $weekStart . "' AND nedate = '" . $weekEnd . "')")->queryAll();


                $dataPatho = '';
                foreach ($patho as $v) {
                    if ($dataPatho == '') {

                        $dataPatho = $v['patho'];
                    } else {

                        $dataPatho .= "," . $v['patho'];
                    }
                }



                if ($sum == '') {

                    $sum = $dataPatho;
                } else {
                    if ($dataPatho != '') {
                        $sum .= "," . $dataPatho;
                    } else {
                        $sum .= $dataPatho;
                    }
                }



                if ($sql == '') {



                    $sql = "SELECT '" . $value['name'] . "' AS department, IFNULL(SUM(nadmit),'0') AS admit,IFNULL(SUM(ndischarge),'0') AS dis,IFNULL(SUM(nmb),'0') AS morbi,"
                            . "IFNULL(SUM(nmt),'0') AS mortal,'" . $dataPatho . "' AS patho FROM  {$ezf['ezf_table']} WHERE ndivision = {$value['dorder']} "
                            . "AND (nbdate = '" . $weekStart . "' AND nedate = '" . $weekEnd . "')";
                } else {



                    $sql .= "UNION ALL SELECT '" . $value['name'] . "' AS department, IFNULL(SUM(nadmit),'0') AS admit,IFNULL(SUM(ndischarge),'0') AS dis,IFNULL(SUM(nmb),'0') AS morbi,"
                            . "IFNULL(SUM(nmt),'0'),'" . $dataPatho . "' AS patho FROM  {$ezf['ezf_table']} WHERE ndivision = {$value['dorder']}  "
                            . "AND (nbdate = '" . $weekStart . "' AND nedate = '" . $weekEnd . "')";
                }
            }
            $sql .= "UNION ALL SELECT 'รวม' AS department, IFNULL(SUM(nadmit),'0') AS admit,IFNULL(SUM(ndischarge),'0') AS dis,IFNULL(SUM(nmb),'0') AS morbi,"
                            . "IFNULL(SUM(nmt),'0'),'" . $sum . "' AS patho FROM  {$ezf['ezf_table']} WHERE ndivision > 0 "
                    . "AND (nbdate = '" . $weekStart . "' AND nedate = '" . $weekEnd . "')";
            $sqlData = \Yii::$app->db->createCommand($sql)->queryAll();

            //\appxq\sdii\utils\VarDumper::dump($sqlData);
        } else if ($ezdata == '1490342148003178500') {

            $ezfdata = Yii::$app->db->createCommand("SELECT ezf_id FROM ezform_data_manage WHERE id = :id;", [':id' => $ezdata])->queryOne();
            $ezf = Yii::$app->db->createCommand("SELECT ezf_table FROM ezform WHERE ezf_id = :id;", [':id' => $ezfdata['ezf_id']])->queryOne();
            $sqlData = Yii::$app->db->createCommand("SELECT tbEzf.hn,tbEzf.nage,tbEzf.nward,tbEzf.npostdx,tbEzf.sergeon1,tbDivision.name FROM {$ezf['ezf_table']} AS tbEzf JOIN {$tbDivision} AS tbDivision ON tbEzf.ndivision = tbDivision.dorder WHERE nweek1 = '" . $weekStart . "' AND nweek2 = '" . $weekEnd . "'")->queryAll();
        }



        $dataProvider = new \yii\data\ArrayDataProvider([
            'allModels' => $sqlData,
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);

        //getMonth
        $dataMonth = ["01" => "มกราคม", "02" => "กุมภาพันธ์", "03" => "มีนาคม", "04" => "เมษายน", "05" => "พฤษภาคม", "06" => "มิถุนายน",
            "07" => "กรกฎาคม", "08" => "สิงหาคม", "09" => "กันยายน", "10" => "ตุลาคม", "11" => "พฤศจิกายน", "12" => "ธันวาคม"];
        //end getMonth
        //getYear
        $minYearSql = "SELECT MIN(YEAR(nadmdate)) AS minYear FROM tbdata_1477041444036408600 WHERE YEAR(nadmdate) > 0";
        $minYearData = Yii::$app->db->createCommand($minYearSql)->queryOne();
        $years = [];
        for ($i = 1; $i <= 200; $i++) {
            array_push($years, ['id' => $minYearData['minYear'] + $i, 'name' => $minYearData['minYear'] + $i]);
        }
        $years = \yii\helpers\ArrayHelper::map($years, 'id', 'name');
        //end getYear


        return $this->render('index', [
                    'dataProvider' => $dataProvider,
                    'dataMonth' => $dataMonth,
                    'years' => $years,
                    'ezfdata' => $ezfdata,
                    'ezdata' => $ezdata,
        ]);
    }

    public function actionGetWeek() {
        if (!empty($_POST['depdrop_parents'])) {
            $mweek = $this->rangweek($_POST['depdrop_parents'][1], $_POST['depdrop_parents'][0]);
            $cm = count($mweek);
            $week = [];

            for ($i = 0; $i <= $cm - 1; $i++) {
                $week[] = ['id' => $mweek[$i][0] . " to " . $mweek[$i][1], 'name' => $mweek[$i][0] . " to " . $mweek[$i][1]];
            }

            $selected = $_POST['depdrop_parents'][2];

            echo \yii\helpers\Json::encode(['output' => $week, 'selected' => $selected]);
            return;
        }
        echo \yii\helpers\Json::encode(['output' => '', 'selected' => '']);
    }

    function rangweek($year, $month) {
        $last_month_day_num = cal_days_in_month(CAL_GREGORIAN, $month, $year);
        $first_month_day_timestamp = strtotime($year . '-' . $month . '-01');

        $last_month_daty_timestamp = strtotime($year . '-' . $month . '-' . $last_month_day_num);

        $first_month_week = date('W', $first_month_day_timestamp);

        $last_month_week = date('W', $last_month_daty_timestamp);

        $mweek = array();
        if ($first_month_week == '52' || $first_month_week == '53') {
            array_push($mweek, array(
                date("d-M-Y", strtotime(sprintf('%dW%02d-1', $year - 1, $first_month_week))),
                date("d-M-Y", strtotime(sprintf('%dW%02d-7', $year - 1, $first_month_week))),
            ));
            for ($week = 01; $week <= $last_month_week; $week ++) {

                array_push($mweek, array(
                    date("d-M-Y", strtotime(sprintf('%dW%02d-1', $year, $week))),
                    date("d-M-Y", strtotime(sprintf('%dW%02d-7', $year, $week))),
                ));
            }
        } else {
            for ($week = $first_month_week; $week <= $last_month_week; $week ++) {

                array_push($mweek, array(
                    date("d-M-Y", strtotime(sprintf('%dW%02d-1', $year, $week))),
                    date("d-M-Y", strtotime(sprintf('%dW%02d-7', $year, $week))),
                ));
            }
        }
        return $mweek;
    }

}
