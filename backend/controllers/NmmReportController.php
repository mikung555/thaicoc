<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace backend\controllers;

use backend\modules\ezforms\components\EzformQuery;
use backend\modules\ezforms\models\EzformFields;
use Yii;
use backend\models\Ezform;
use yii\data\ActiveDataProvider;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;

class NmmReportController extends \yii\web\Controller {

    public function actionIndex() {
        //  return "test";  

        $query_nmm = \backend\classes\NmmReport::QueryNmmcause_1();
        
        //\appxq\sdii\utils\VarDumper::dump($cc1->allModels);

        return $this->render('/surgery-form/nmm-report', [
                    'query_nmm' => $query_nmm,
                   
        ]);
    }

    

}
