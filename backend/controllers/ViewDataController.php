<?php

namespace backend\controllers;

use Yii;
use backend\models\Tbdata;
use backend\models\TbdataSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ViewDataController implements the CRUD actions for Tbdata model.
 */
class ViewDataController extends Controller
{
     public $layout = '@backend/views/layouts/common';
   
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Tbdata models.
     * @return mixed
     */
    public function actionIndex()
    {
        $params=Yii::$app->request->queryParams;
        $ezfid=$params['id'];
        $sql = "SELECT * FROM ezform WHERE ezf_id='".$ezfid."' ";
        $dataForm = \Yii::$app->db->createCommand($sql)->queryOne();
        $sqlColumn = "SELECT COLUMN_NAME AS `column` FROM INFORMATION_SCHEMA.COLUMNS "
                                    . "WHERE TABLE_NAME = '".$dataForm["ezf_table"]."' ";
        $dataColumn = \Yii::$app->db->createCommand($sqlColumn)->queryAll();
		
		$dataField = \backend\modules\ezforms\components\EzformQuery::getFieldsByEzf_id($dataForm['ezf_id']);
		
        $searchModel = new TbdataSearch();
        $searchModel->setTableName($dataForm["ezf_table"]);
        $searchModel->setEZFid($ezfid);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'dataColumn'=>$dataColumn,
            'dataForm'=>$dataForm,
			'dataField'=>$dataField
        ]);
    }

    /**
     * Displays a single Tbdata model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Tbdata model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Tbdata();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Tbdata model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Tbdata model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Tbdata model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Tbdata the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Tbdata::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
