<?php

namespace backend\controllers;

use Yii;
use common\models\User;
use backend\models\UserForm;
use backend\models\search\UserSearch;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use common\models\UserProfile;
use backend\modules\ezforms\components\EzformQuery;
use yii\validators\EmailValidator;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public function actionIndex2()
    {
        $searchModel = new UserSearch();
	$searchModel->approve_profile = -2;
        $dataProvider = $searchModel->searchManage(Yii::$app->request->queryParams, 1);
	$dataAdminsite = UserProfile::getNotAdminsite();
	
        return $this->render('restore', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
	    'dataAdminsite' => $dataAdminsite,
        ]);
    }
    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserSearch();
	$searchModel->approve_profile = -2;
        $dataProvider = $searchModel->searchManage(Yii::$app->request->queryParams, 0);
	$dataAdminsite = UserProfile::getNotAdminsite();
	
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
	    'dataAdminsite' => $dataAdminsite,
        ]);
    }
    
    public function actionImportFile($id, $uid)
    {
	if (Yii::$app->getRequest()->isAjax) {
	    Yii::$app->response->format = Response::FORMAT_JSON;
	    
	    $sql = "SELECT puser.pid, 
			puser.username, 
			puser.cid, 
			puser.Email, 
			puser.OrganizationURL, 
			puser.FundingOrganization, 
			puser.RegulatoryAuthority, 
			puser.RegulatoryAuthorityAddress, 
			puser.dadd, 
			puser.dupdate, 
			puser.lastsessionreg, 
			puser.stdid, 
			puser.stdprogram, 
			puser.stduniversity, 
			puser.hcode, 
			puser.tamboncode, 
			puser.tambon, 
			puser.amphurcode, 
			puser.amphur, 
			puser.provincecode, 
			puser.province, 
			puser.postcode, 
			puser.pid_link, 
			file_upload.tablename, 
			file_upload.fieldname, 
			file_upload.filename, 
			file_upload.filenamesys, 
			file_upload.fullpath, 
			file_upload.filetype, 
			file_upload.filestatus, 
			file_upload.fileactive
		FROM puser INNER JOIN file_upload ON puser.pid_link = file_upload.ptid
		WHERE puser.pid=:id and fieldname='siteadmin' and fileactive='1'
	    ";
	    
	    $data = Yii::$app->dbcascap->createCommand($sql, [':id'=>$id])->queryOne();
	    if($data){
		$modelProfile = UserProfile::find()->where('user_id=:uid', [':uid'=>$uid])->one();
		$modelProfile->secret_file = $data['filenamesys'];
		
		$urlfile = "http://www1.cascap.in.th{$data['fullpath']}/{$data['filenamesys']}";
		//$modelProfile->citizenid_file = $data['filename'];
		$file_target = Yii::$app->basePath . '/../storage/web/source/' . $data['filenamesys'];
		
		$imageString = file_get_contents($urlfile);
		$save = file_put_contents($file_target, $imageString);

		if(!$save){
		    $result = [
			    'status' => 'error',
			    'message' => '<strong><i class="glyphicon glyphicon-warning-sign"></i> Error!</strong> ' . Yii::t('user', 'import file ไม่ได้.'),
			    'data' => $id,
		    ];
		    return $result;
		}
		
		$modelProfile->save();
			
		$result = [
			'status' => 'success',
			'message' => '<strong><i class="glyphicon glyphicon-ok-sign"></i> Success!</strong> ' . Yii::t('user', 'import file แล้ว.'),
			'data' => $id,
		];
		return $result;
	    }
	    
	    $result = [
		    'status' => 'error',
		    'message' => '<strong><i class="glyphicon glyphicon-warning-sign"></i> Error!</strong> ' . Yii::t('user', 'ไม่พบไฟล์.'),
		    'data' => $id,
	    ];
	    return $result;
	} else {
	    throw new \yii\web\NotFoundHttpException('Invalid request. Please do not repeat this request again.');
	}
    }
    
    public function actionImportUser($id)
    {
	if (Yii::$app->getRequest()->isAjax) {
	    Yii::$app->response->format = Response::FORMAT_JSON;
	    
	    $sql = "SELECT puser.pid, 
		    puser.username, 
		    puser.passwords, 
		    puser.title, 
		    puser.`name`, 
		    puser.surname, 
		    puser.upriv, 
		    puser.priv29txt, 
		    puser.cid, 
		    puser.cidstart, 
		    puser.cidend, 
		    puser.bankcompany, 
		    puser.bankid, 
		    puser.InvestigatorName, 
		    puser.Affiliation, 
		    puser.InvestigatorPhone, 
		    puser.InvestigatorEmail, 
		    puser.Registering, 
		    puser.TypeofOrg, 
		    puser.Country, 
		    puser.HospitalName, 
		    puser.OrganizationName, 
		    puser.OrganizationAddress, 
		    puser.OrganizationAcronyms, 
		    puser.OrganizationParent, 
		    puser.OrgProvCode, 
		    puser.OrgProvince, 
		    puser.OfficialRepresentative, 
		    puser.Phone, 
		    puser.Email, 
		    puser.OrganizationURL, 
		    puser.FundingOrganization, 
		    puser.RegulatoryAuthority, 
		    puser.RegulatoryAuthorityAddress, 
		    puser.dadd, 
		    puser.dupdate, 
		    puser.lastsessionreg, 
		    puser.stdid, 
		    puser.stdprogram, 
		    puser.stduniversity, 
		    puser.hcode, 
		    puser.tamboncode, 
		    puser.tambon, 
		    puser.amphurcode, 
		    puser.amphur, 
		    puser.provincecode, 
		    puser.province, 
		    puser.postcode, 
		    puser.pid_link
	    FROM puser INNER JOIN puser_priv ON puser.pid = puser_priv.pid
		WHERE (puser_priv.nologin = 0 or puser_priv.nologin is null) and puser.pid = :id
	    ";
	    
	    $data = Yii::$app->dbcascap->createCommand($sql, [':id'=>$id])->queryOne();
	    if($data){
		
		$email = $data['Email'];
		$validator = new EmailValidator();

		if (!$validator->validate($email, $error)) {
		   $email = \common\lib\codeerror\helpers\GenMillisecTime::getMillisecTime().'@gmail.com';
		} 
		
		$model = new User();
		$model->id = \common\lib\codeerror\helpers\GenMillisecTime::getMillisecTime();
		$model->username = $data['username'];
		$model->email = $email;
		$model->status = true;
		
		if (isset($data['passwords'])) {
		    $model->setPassword($data['passwords']);
		}
		if(isset($data['OrganizationName']) && !empty($data['OrganizationName'])){
		    if($model->save()){

			$auth =  Yii::$app->authManager;
			//$auth->revokeAll($model->getId());
			$auth->assign($auth->getRole(User::ROLE_MANAGER), $model->getId());

			$modelProfile = new UserProfile();

			$modelProfile->user_id = $model->getId();
			$modelProfile->firstname = iconv('TIS-620', 'UTF-8', $data['name']);
			$modelProfile->lastname = iconv('TIS-620', 'UTF-8', $data['surname']);
			$modelProfile->cid = isset($data['cid'])?$data['cid']:'9999999999999';
			$modelProfile->gender = 1;
			$modelProfile->sitecode = $data['OrganizationName'];
			$modelProfile->department = $data['OrganizationName'];
			$modelProfile->email = $email;
			$modelProfile->telephone = isset($data['Phone'])?$data['Phone']:\common\lib\codeerror\helpers\GenMillisecTime::getMillisecTime();
			$modelProfile->title = $data['title'];
			$modelProfile->status = '1';
			$modelProfile->status_personal = '2';
			$modelProfile->address_province = $data['provincecode'];
			$modelProfile->address_amphur = $data['amphurcode'];
			$modelProfile->address_tambon = $data['tamboncode'];
			$modelProfile->address_text = iconv('TIS-620', 'UTF-8', $data['OrganizationAddress']);
			$modelProfile->approved = 1;
			
			$modelProfile->save();
			
			$result = [
				'status' => 'success',
				'message' => '<strong><i class="glyphicon glyphicon-ok-sign"></i> Success!</strong> ' . Yii::t('user', 'import ข้อมูลแล้ว.'),
				'data' => $id,
			];
			return $result;
		    } else {
			$result = [
				'status' => 'error',
				'message' => '<strong><i class="glyphicon glyphicon-warning-sign"></i> Error!</strong> ' . Yii::t('user', 'ไม่สามารถ import ข้อมูล.'),
				'data' => $id,
			];
			return $result;
		    }
		}
		
	    }
	    
	    $result = [
		    'status' => 'error',
		    'message' => '<strong><i class="glyphicon glyphicon-warning-sign"></i> Error!</strong> ' . Yii::t('user', 'ไม่พบข้อมูล.'),
		    'data' => $id,
	    ];
	    return $result;
	} else {
	    throw new \yii\web\NotFoundHttpException('Invalid request. Please do not repeat this request again.');
	}
    }
    
    public function actionTool()
    {
	if(!Yii::$app->user->can('administrator')){
	    echo 'Drump user by administrator only';
	    exit();
	}
        
	$sitecode = isset($_GET['sitecode'])?$_GET['sitecode']:'';
	$term = isset($_GET['term'])?$_GET['term']:'';
	
	$term = iconv('UTF-8', 'TIS-620', $term);
	
	$sql = "SELECT puser.pid, 
		    puser.username, 
		    puser.passwords, 
		    puser.title, 
		    puser.`name`, 
		    puser.surname, 
		    puser.upriv, 
		    puser.priv29txt, 
		    puser.cid, 
		    puser.cidstart, 
		    puser.cidend, 
		    puser.bankcompany, 
		    puser.bankid, 
		    puser.InvestigatorName, 
		    puser.Affiliation, 
		    puser.InvestigatorPhone, 
		    puser.InvestigatorEmail, 
		    puser.Registering, 
		    puser.TypeofOrg, 
		    puser.Country, 
		    puser.HospitalName, 
		    puser.OrganizationName, 
		    puser.OrganizationAddress, 
		    puser.OrganizationAcronyms, 
		    puser.OrganizationParent, 
		    puser.OrgProvCode, 
		    puser.OrgProvince, 
		    puser.OfficialRepresentative, 
		    puser.Phone, 
		    puser.Email, 
		    puser.OrganizationURL, 
		    puser.FundingOrganization, 
		    puser.RegulatoryAuthority, 
		    puser.RegulatoryAuthorityAddress, 
		    puser.dadd, 
		    puser.dupdate, 
		    puser.lastsessionreg, 
		    puser.stdid, 
		    puser.stdprogram, 
		    puser.stduniversity, 
		    puser.hcode, 
		    puser.tamboncode, 
		    puser.tambon, 
		    puser.amphurcode, 
		    puser.amphur, 
		    puser.provincecode, 
		    puser.province, 
		    puser.postcode, 
		    puser.pid_link
	    FROM puser INNER JOIN puser_priv ON puser.pid = puser_priv.pid
		WHERE (puser_priv.nologin = 0 or puser_priv.nologin is null) and puser.OrganizationName LIKE :sitecode and (puser.username LIKE :term or concat(puser.`name`, ' ', puser.surname) LIKE :term)
	    ";
	
	$data = Yii::$app->dbcascap->createCommand($sql, [':sitecode'=>"$sitecode%", ':term'=>"%$term%"])->queryAll();
	
	$dataProvider = new \yii\data\ArrayDataProvider([
	    'allModels'=>$data,
	    'key'=>'pid',
	]);
	
	$sql = "select distinct h.hcode,CONCAT(h.`name`, ' ต.', h.`tambon`,' อ.', h.`amphur`,' จ.', h.`province`) AS name from user_profile as p left join all_hospital_thai as h on p.sitecode=h.hcode"
                . " where h.hcode=:code";
	
	$sitelist = Yii::$app->db->createCommand($sql, [':code'=>$sitecode])->queryOne();
	
//	\yii\helpers\VarDumper::dump(count($data));
//	exit();
        return $this->render('tool', [
            'dataProvider'=>$dataProvider,
	    'sitecode' => $sitecode,	
	    'sitelist'=>$sitelist,
	    'term'=>$term
        ]);
    }
    
    public function actionSiteList($q = null, $id = null) {
	Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
	$out = ['results' => []];
	if (!is_null($q)) {
	    $sql = "select distinct h.hcode as id,CONCAT(h.`code`, ' ', h.`name`, ' ต.', h.`tambon`,' อ.', h.`amphur`,' จ.', h.`province`) as text from user_profile as p left join all_hospital_thai as h on p.sitecode=h.hcode
                    WHERE h.hcode like :code OR h.`name` like :name GROUP BY h.`hcode` limit 20";
	    $data = Yii::$app->db->createCommand($sql, [':code'=>"$q%", ':name'=>"%$q%"])->queryAll();
	    
	    if($data){
		$out['results'] = array_values($data);
	    }
	} elseif ($id > 0) {
	    $model = \backend\models\SiteUrl::find()->where('code=:code', [':code'=>$id])->one();
	    $out['results'] = ['id' => $id, 'text' => $model->name];
	}
	return $out;
    }
    
    public function actionAdminSite()
    {
	$sitecode = Yii::$app->user->identity->userProfile->sitecode;
	
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->searchSite(Yii::$app->request->queryParams, $sitecode);
	
        return $this->render('admin-site', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionManager($id, $auth)
    {
	Yii::$app->response->format = Response::FORMAT_JSON;
        if ($id == Yii::$app->user->getId()) {
	    
	    $result = [
		    'status' => 'error',
		    'action' => 'alert',
		    'message' => '<strong><i class="glyphicon glyphicon-warning-sign"></i> Error!</strong> ' . Yii::t('user', 'You can not action your own account.'),
		    'data' => $id,
	    ];
	    return $result;
        } else {
            $user = $this->findModel($id);
            $getAuth = Yii::$app->authManager->getAssignment($auth, $id);
	    $roles_db = ArrayHelper::map(Yii::$app->authManager->getRoles(), 'name', 'description');
	    if (isset($getAuth->roleName)) {
		$authorRole = Yii::$app->authManager->getRole($auth);
		Yii::$app->authManager->revoke($authorRole, $id);
		
		$modelProfile = UserProfile::find()->where('user_id=:id', [':id'=>$id])->one();
//		if($auth=='manager'){
//		    $modelProfile->approved = null;
//		    $modelProfile->save();
//		}
		$userProfile = \Yii::$app->user->identity->userProfile;
		
		$modelComment = new \backend\models\UserComment();
		$modelComment->user_id = $id;
		$modelComment->name = $userProfile->firstname. ' '. $userProfile->lastname;
		$modelComment->action = $modelProfile->approved;
		$modelComment->comment = "ยกเลิกสิทธิ์ {$roles_db[$auth]}";
		$modelComment->save();
		
		$result = [
			'status' => 'success',
			'action' => 'update',
			'message' => '<strong><i class="glyphicon glyphicon-ok-sign"></i> Success!</strong> ' . Yii::t('user', 'User has been remove manager.'),
			'data' => $id,
		];
		return $result;
            } else {
		$authorRole = Yii::$app->authManager->getRole($auth);
                $setAuth = Yii::$app->authManager->assign($authorRole, $id);
		
		$modelProfile = UserProfile::find()->where('user_id=:id', [':id'=>$id])->one();
//		if($auth=='manager'){
//		    $modelProfile->approved = 1;
//		    $modelProfile->save();
//		}
		$userProfile = \Yii::$app->user->identity->userProfile;
		$modelComment = new \backend\models\UserComment();
		$modelComment->user_id = $id;
		$modelComment->name = $userProfile->firstname. ' '. $userProfile->lastname;
		$modelComment->action = $modelProfile->approved;
		$modelComment->comment = "ให้สิทธิ์ {$roles_db[$auth]}";
		$modelComment->save();
		
		$result = [
			'status' => 'success',
			'action' => 'update',
			'message' => '<strong><i class="glyphicon glyphicon-ok-sign"></i> Success!</strong> ' . Yii::t('user', 'User has been add manager.'),
			'data' => $id,
		];
		return $result;
            }
        }
    }
    
    public function actionStatus($id)
    {
	Yii::$app->response->format = Response::FORMAT_JSON;
        if ($id == Yii::$app->user->getId()) {
	    $result = [
		    'status' => 'error',
		    'action' => 'alert',
		    'message' => '<strong><i class="glyphicon glyphicon-warning-sign"></i> Error!</strong> ' . Yii::t('user', 'You can not action your own account.'),
		    'data' => $id,
	    ];
	    return $result;
        } else {
            $user = $this->findModel($id);
            
	    if ($user->status==1) {
		$user->updateAttributes(['status' => 0]);
		$result = [
			'status' => 'success',
			'action' => 'update',
			'message' => '<strong><i class="glyphicon glyphicon-ok-sign"></i> Success!</strong> ' . Yii::t('user', 'User has been disabled.'),
			'data' => $id,
		];
		return $result;
            } else {
                $user->updateAttributes(['status' => 1]);
		$result = [
			'status' => 'success',
			'action' => 'update',
			'message' => '<strong><i class="glyphicon glyphicon-ok-sign"></i> Success!</strong> ' . Yii::t('user', 'User has been active.'),
			'data' => $id,
		];
		return $result;
            }
        }
    }
    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new UserForm();
	$modelProfile = new UserProfile();
	$modelProfile->status = 0;
        $dataProvince = EzformQuery::getProvince();
	$dataHospital = [];
		
        $model->setScenario('create');
        if ($model->load(Yii::$app->request->post()) && $modelProfile->load(Yii::$app->request->post()) && $model->save()) {
	    if($modelProfile->sitecode == '00000' || $modelProfile->sitecode == '') {
                $modelProfile->sitecode = $_POST['UserProfile']['sitecode'];
            }
	    
	    $modelProfile->save();
	    
            return $this->redirect(['index']);
        }

        return $this->render('create', [
            'model' => $model,
	    'modelProfile'=>$modelProfile,
	    'dataProvince'=>$dataProvince,
	    'dataHospital'=>$dataHospital,
            'roles' => ArrayHelper::map(Yii::$app->authManager->getRoles(), 'name', 'description')
        ]);
    }

    public function actionGenamphur(){
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $id = end($_POST['depdrop_parents']);
            $sql = "SELECT `AMPHUR_CODE` as id,`AMPHUR_NAME` as name FROM `const_amphur` WHERE `AMPHUR_CODE` LIKE :id ";
            $list = Yii::$app->db->createCommand($sql, [':id'=>"$id%"])->queryAll();
            $selected  = null;
            if ($id != null && count($list) > 0) {
                $selected = '';
                if (!empty($_POST['depdrop_params'])) {
                    $params = $_POST['depdrop_params'];
                    $selected = $params[0]; // get the value of input-type-1
                }
                foreach ($list as $i => $amphur) {
                    $out[] = ['id' => $amphur['id'], 'name' => $amphur['name']];
                }
                
                // Shows how you can preselect a value
                echo \yii\helpers\Json::encode(['output' => $out, 'selected'=>$selected]);
                return;
            }
        }
        echo \yii\helpers\Json::encode(['output' => '', 'selected'=>'']);
    }
    public function actionQuerys($q){
	$sql = "SELECT hcode AS `code`, CONCAT(IFNULL(`hcode`,''), ' : ', IFNULL(`name`,''), ' ต.', IFNULL(`tambon`,''), ' อ.', IFNULL(`amphur`,''), ' จ.', IFNULL(`province`,'')) AS `name` FROM `all_hospital_thai` WHERE `name` LIKE '%".$q."%' OR `hcode` LIKE '%".$q."%' LIMIT 0,10";
        Yii::$app->response->format = Response::FORMAT_JSON;
        $data = Yii::$app->db->createCommand($sql)->queryAll();

        $json = array();
        foreach($data as $value){
            $json[] = ['id'=>$value['code'],'label'=>$value["name"]];
        }
        return $json;
    }
    public function actionQuerys2($q){
        if(Yii::$app->keyStorage->get('frontend.domain')=='thaicarecloud.org'){
	    $sql = "SELECT medshop AS `code`, CONCAT(IFNULL(`medshop`,''), ' : ', IFNULL(`nameshop`,'')) AS `name` FROM `tbdata_1483603520065899000` WHERE `nameshop` LIKE '%".$q."%' OR `medshop` LIKE '%".$q."%' LIMIT 0,10";
	} else {
	    $sql = "SELECT hcode AS `code`, CONCAT(IFNULL(`hcode`,''), ' : ', IFNULL(`name`,''), ' ต.', IFNULL(`tambon`,''), ' อ.', IFNULL(`amphur`,''), ' จ.', IFNULL(`province`,'')) AS `name` FROM `all_hospital_thai` WHERE `name` LIKE '%".$q."%' OR `hcode` LIKE '%".$q."%' LIMIT 0,10";
	}
        
        Yii::$app->response->format = Response::FORMAT_JSON;
        $data = Yii::$app->db->createCommand($sql)->queryAll();

        $json = array();
        foreach($data as $value){
            $json[] = ['id'=>$value['code'],'label'=>$value["name"]];
        }
        return $json;

    }
    public function actionQuerys3($q){
        if(Yii::$app->keyStorage->get('frontend.domain')=='thaicarecloud.org' || Yii::$app->keyStorage->get('frontend.domain')=='yii2-starter-kit.dev'){
	    $sql = "SELECT orgcode AS `code`, CONCAT(IFNULL(`orgcode`,''), ' : ', IFNULL(`name`,''), ' จ.', IFNULL(`province`,'')) AS `name` FROM `all_hos_org` WHERE `name` LIKE '%".$q."%' OR `orgcode` LIKE '%".$q."%' LIMIT 0,10";
	} else {
	    $sql = "SELECT hcode AS `code`, CONCAT(IFNULL(`hcode`,''), ' : ', IFNULL(`name`,''), ' ต.', IFNULL(`tambon`,''), ' อ.', IFNULL(`amphur`,''), ' จ.', IFNULL(`province`,'')) AS `name` FROM `all_hospital_thai` WHERE `name` LIKE '%".$q."%' OR `hcode` LIKE '%".$q."%' LIMIT 0,10";
	}
        
        Yii::$app->response->format = Response::FORMAT_JSON;
        $data = Yii::$app->db->createCommand($sql)->queryAll();

        $json = array();
        foreach($data as $value){
            $json[] = ['id'=>$value['code'],'label'=>$value["name"]];
        }
        return $json;

    }
    /**
     * Updates an existing User model.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = new UserForm();
	$modelProfile = UserProfile::find()->where('user_id=:id', [':id'=>$id])->one();
        $dataProvince = EzformQuery::getProvince();
	$sqlHospital = "SELECT * FROM all_hospital_thai WHERE hcode=:hcode ";//, ' ต.', h.`tambon`,' อ.', h.`amphur`,' จ.', h.`province`
        $dataHospital = Yii::$app->db->createCommand($sqlHospital,[':hcode'=>$modelProfile->sitecode])->queryOne();
	$modelProfile->sitecode = $dataHospital['hcode'] . ' ' . $dataHospital['name']. ' ต.' . $dataHospital['tambon']. ' อ.' . $dataHospital['amphur']. ' จ.' . $dataHospital['province'];
	$modelProfile->department = $dataHospital['hcode'];
        $model->setModel($this->findModel($id));
	
	
	$dataComment = \backend\models\UserComment::find()
		->where('user_id=:id', [':id'=>$id])
		->orderBy('update_at DESC')
		->all();
	$userProfile = \Yii::$app->user->identity->userProfile;
	$modelComment = new \backend\models\UserComment();
	$modelComment->user_id = $id;
	$modelComment->name = $userProfile->firstname. ' '. $userProfile->lastname;
	
	$user = $this->findModel($id);
	
	$approved = $user->userProfile->approved;
	
	
	$readonly = isset($_GET['readonly']) && $_GET['readonly']>0?true:false;
	$roles_db = ArrayHelper::map(Yii::$app->authManager->getRoles(), 'name', 'description');
	
	$roles_str = implode(', ', $model->roles);
	$dept_old = $modelProfile->department;
	
        if ($model->load(Yii::$app->request->post()) && $modelProfile->load(Yii::$app->request->post()) && $modelComment->load(Yii::$app->request->post())) {
	    
	    if($modelProfile->department==''){
		Yii::$app->session->setFlash('alert', [
		      'options' => ['class' => 'alert-danger'],
		      'body' => Yii::t('backend', 'กรุณาระบุหน่วยบริการของท่าน', [], $modelProfile->locale)
		]);
		  return $this->render('update', [
		    'model' => $model,
		    'modelProfile'=>$modelProfile,
		    'dataProvince'=>$dataProvince,
		    'dataHospital'=>$dataHospital,
		    'dataComment'=>$dataComment,
		    'modelComment'=>$modelComment,
		    'user'=>$user,
		    'roles' => $roles_db
		]);
	    }
	    
            
            
	    if($readonly){
		if($model->save()){
		    if($modelProfile->cid==NULL || $modelProfile->cid==''){
		    	$modelProfile->cid = $model->username;
		    }
		    
		    $modelProfile->sitecode = $modelProfile->department;
		    $dept_new = $modelProfile->department;
		    
		    if($approved==3 && $modelProfile->approved==''){
			$modelProfile->approved = 3;
		    }
		    
		    $modelProfile->save();
		    
		    $roles = implode(', ', $model->roles);
		    
		    $roles_name=[];
		    foreach ($model->roles as $key => $value) {
			$roles_name[] = $roles_db[$value];
		    }
		    
		    $strR = '';
		    $roles_name_str = implode(', ', $roles_name);
		    $strR = "[$roles_name_str] ";
//			
		    $conditionR = ($roles!=$roles_str);
//		    if($conditionR){
//			$roles_name_str = implode(', ', $roles_name);
//			$strR = "[$roles_name_str] ";
//		    }
		    $strD = '';
		    $conditionD = ($dept_old!=$dept_new);
		    if($conditionD){
			$strD = "[$dept_old -> $dept_new] ";
		    }
		    
		    if((!empty($modelComment->name) && !empty($modelComment->comment)) || $approved!=$modelProfile->approved || $conditionR || $conditionD){//
			$modelComment->action = $modelProfile->approved;
			$modelComment->comment = $strR.$strD.$modelComment->comment;
			$modelComment->save();
		    }
		}
		    
		return $this->redirect(['update', 'id'=>$id, 'readonly'=>$readonly]);
	    } else {
		if($model->save()){
		
		    if($modelProfile->cid==NULL || $modelProfile->cid==''){
			$modelProfile->cid = $model->username;
		    }
		    
		    
                    if($modelProfile->volunteer_status==1 && $modelProfile->volunteer==1){
                        $modelProfile->inout=1;
                  }

                  if(strlen($modelProfile->sitecode)<8){
                    Yii::$app->session->setFlash('alert', [
                        'options' => ['class' => 'alert-danger'],
                        'body' => Yii::t('backend', 'กรุณาระบุหน่วยบริการของท่าน', [], $modelProfile->locale)
                    ]);
                    return $this->render('update', [
                        'model' => $model,
                        'modelProfile'=>$modelProfile,
                        'dataProvince'=>$dataProvince,
                        'dataHospital'=>$dataHospital,
                        'dataComment'=>$dataComment,
                        'modelComment'=>$modelComment,
                        'user'=>$user,
                        'roles' => $roles_db
                    ]);
                  }else{
                      $name = $modelProfile->sitecode;
                      if($modelProfile->inout==1 && $modelProfile->department==''){
                            $sql = "SELECT * FROM `all_hospital_thai` WHERE `name` = :name ";
                            $dataHos = Yii::$app->db->createCommand($sql, [':name'=>$name])->queryOne();
                            if($dataHos){
                                $modelProfile->department = $dataHos['hcode'];
                            } else {
                                $sql = "SELECT * FROM `all_hospital_thai` WHERE `hcode` LIKE 'C%'";
                                $count = Yii::$app->db->createCommand($sql)->query()->count();
                                $count++;
                                $code = 'C'.str_pad($count,'4','0',STR_PAD_LEFT);

                                $queryInsert = Yii::$app->db->createCommand()
                                        ->insert('all_hospital_thai', [
                                            'hcode' => $code,
                                            'code2' => 'demo',
                                            'name' => $name,
                                        ])
                                        ->execute();

                                $modelProfile->department = $code;
                            }
                      } elseif($modelProfile->inout==0 && $modelProfile->department==''){
                          $sql = "SELECT * FROM `all_hospital_thai` WHERE `name` = :name ";
                            $dataHos = Yii::$app->db->createCommand($sql, [':name'=>$name])->queryOne();
                            if($dataHos){
                                $modelProfile->department = $dataHos['hcode'];
                            } else {
                                $sql = "SELECT * FROM `all_hospital_thai` WHERE `hcode` LIKE 'B%'";
                                $count = Yii::$app->db->createCommand($sql)->query()->count();
                                $count++;
                                $code = 'B'.str_pad($count,'4','0',STR_PAD_LEFT);

                                $queryInsert = Yii::$app->db->createCommand()
                                        ->insert('all_hospital_thai', [
                                            'hcode' => $code,
                                            'code2' => 'demo',
                                            'name' => $name,
                                        ])
                                        ->execute();

                                $modelProfile->department = $code;
                            }
                      } elseif($modelProfile->inout==2 && $modelProfile->department!=''){
                          if(Yii::$app->keyStorage->get('frontend.domain')=='thaicarecloud.org'){
                            $sql = "SELECT * FROM `all_hospital_thai` WHERE `hcode` = :hcode ";
                            $dataHos = Yii::$app->db->createCommand($sql, [':hcode'=>$modelProfile->department])->queryOne();
                            if($dataHos){
        //			$modelProfile->department = $dataHos['hcode'];
        //			$_POST["UserProfile"]["department"] = $dataHos['hcode'];
                            } else {

                                $sql = "SELECT * FROM `tbdata_1483603520065899000` WHERE `medshop` = :hcode";
                                $dataDrug = Yii::$app->db->createCommand($sql, [':hcode'=>$modelProfile->department])->queryOne();
                                if($dataDrug){
                                    $queryInsert = Yii::$app->db->createCommand()
                                            ->insert('all_hospital_thai', [
                                                'hcode' => $dataDrug['medshop'],
                                                'code' => $dataDrug['id'],
                                                'code2' => 'demo',
                                                'code4' => '100',
                                                'name5' => 'ร้านยา',
                                                'name' => $dataDrug['nameshop'],
                                                'code7' => '1',
                                                'name8' => 'เปิดดำเนินการ',
                                                'provincecode' => $dataDrug['var13_province'],
                                                'amphurcode' => $dataDrug['var13_amphur'],
                                                'tamboncode' => $dataDrug['var13_tumbon'],
                                                'moo' => $dataDrug['b2'],
                                                'address' => $dataDrug['b1'],
                                                'postcode' => $dataDrug['postcode'],
                                                'tel' => $dataDrug['phone'],
                                            ])
                                            ->execute();

                                }

                            }
                        }
                    } elseif($modelProfile->inout==2 && $modelProfile->department==''){
                        $sql = "SELECT * FROM `all_hospital_thai` WHERE `name` = :name ";
                        $dataHos = Yii::$app->db->createCommand($sql, [':name'=>$name])->queryOne();
                        if($dataHos){
                            $modelProfile->department = $dataHos['hcode'];
                        } else {
                            $sql = "SELECT * FROM `all_hospital_thai` WHERE `hcode` LIKE 'D%'";
                            $count = Yii::$app->db->createCommand($sql)->query()->count();
                            $count++;
                            $code = 'D'.str_pad($count,'4','0',STR_PAD_LEFT);

                            $queryInsert = Yii::$app->db->createCommand()
                                    ->insert('all_hospital_thai', [
                                        'hcode' => $code,
                                        'code2' => 'demo',
                                        'code4' => '100',
                                        'name5' => 'ร้านยา',
                                        'name' => $name,
                                        'code7' => '1',
                                        'name8' => 'เปิดดำเนินการ',
                                    ])
                                    ->execute();

                            $modelProfile->department = $code;
                        }

                    } elseif($modelProfile->inout==3 && $modelProfile->department!=''){
                          if(Yii::$app->keyStorage->get('frontend.domain') == 'thaicarecloud.org' || Yii::$app->keyStorage->get('frontend.domain')=='yii2-starter-kit.dev'){
                            $sql = "SELECT * FROM `all_hospital_thai` WHERE `hcode` = :hcode ";
                            $dataHos = Yii::$app->db->createCommand($sql, [':hcode'=>$modelProfile->department])->queryOne();
                            if($dataHos){
        //			$modelProfile->department = $dataHos['hcode'];
        //			$_POST["UserProfile"]["department"] = $dataHos['hcode'];
                            } else {

                                $sql = "SELECT * FROM `all_hos_org` WHERE `orgcode` = :hcode";
                                $dataDrug = Yii::$app->db->createCommand($sql, [':hcode'=>$modelProfile->department])->queryOne();
                                if($dataDrug){
                                    $tambon = substr($dataDrug['orgcode'], 1);
                                    $amphur = substr($tambon, 2, 4);
                                    $province = substr($tambon, 0, 2);
                                    $queryInsert = Yii::$app->db->createCommand()
                                            ->insert('all_hospital_thai', [
                                                'hcode' => $dataDrug['orgcode'],
                                                'code' => $dataDrug['orgcode'],
                                                'code2' => 'demo',
                                                'code4' => '200',
                                                'name5' => 'องค์กรปกครองส่วนท้องถิน',
                                                'name' => $dataDrug['name'],
                                                'code7' => '1',
                                                'name8' => 'เปิดดำเนินการ',
                                                'provincecode' => $province,
                                                'amphurcode' => $amphur,
                                                'tamboncode' => $tambon,
                                            ])
                                            ->execute();

                                }

                            }
                        }
                    } elseif($modelProfile->inout==3 && $modelProfile->department==''){
                        $sql = "SELECT * FROM `all_hospital_thai` WHERE `name` = :name ";
                        $dataHos = Yii::$app->db->createCommand($sql, [':name'=>$name])->queryOne();
                        if($dataHos){
                            $modelProfile->department = $dataHos['hcode'];
                        } else {
                            $sql = "SELECT * FROM `all_hospital_thai` WHERE `hcode` LIKE 'E%'";
                            $count = Yii::$app->db->createCommand($sql)->query()->count();
                            $count++;
                            $code = 'E'.str_pad($count,'7','0',STR_PAD_LEFT);

                            $queryInsert = Yii::$app->db->createCommand()
                                    ->insert('all_hospital_thai', [
                                        'hcode' => $code,
                                        'code2' => 'demo',
                                        'code4' => '200',
                                        'name5' => 'องค์กรปกครองส่วนท้องถิน',
                                        'name' => $name,
                                        'code7' => '1',
                                        'name8' => 'เปิดดำเนินการ',
                                    ])
                                    ->execute();

                            $modelProfile->department = $code;
                        }

                    }

                  }
                  
                    $modelProfile->sitecode = $modelProfile->department;
		    $dept_new = $modelProfile->department;
                  
		    if($approved==3 && $modelProfile->approved==''){
			$modelProfile->approved = 3;
		    }
    //	    
		    $modelProfile->save();

		    $roles = implode(', ', $model->roles);
		    
		    $roles_name=[];
		    foreach ($model->roles as $key => $value) {
			$roles_name[] = $roles_db[$value];
		    }
		    
		    $strR = '';
		    $roles_name_str = implode(', ', $roles_name);
		    $strR = "[$roles_name_str] ";
//			
		    $conditionR = ($roles!=$roles_str);
//		    if($conditionR){
//			$roles_name_str = implode(', ', $roles_name);
//			$strR = "[$roles_name_str] ";
//		    }
		    $strD = '';
		    $conditionD = ($dept_old!=$dept_new);
		    if($conditionD){
			$strD = "[$dept_old -> $dept_new] ";
		    }
		    
		    if((!empty($modelComment->name) && !empty($modelComment->comment)) || $approved!=$modelProfile->approved || $conditionR || $conditionD){//$conditionR || 
			$modelComment->action = $modelProfile->approved;
			$modelComment->comment = $strR.$strD.$modelComment->comment;
			$modelComment->save();
		    }

		    return $this->redirect(['update', 'id'=>$id, 'readonly'=>$readonly]);
		}
	    }
        }
	
        return $this->render('update', [
            'model' => $model,
	    'modelProfile'=>$modelProfile,
	    'dataProvince'=>$dataProvince,
	    'dataHospital'=>$dataHospital,
	    'dataComment'=>$dataComment,
	    'modelComment'=>$modelComment,
	    'user'=>$user,
            'roles' => $roles_db
        ]);
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    
    
    public function actionDelete($id)
    {
        //Yii::$app->authManager->revokeAll($id);
	
        $model = $this->findModel($id);
	
	$model->status = 0;
	$model->status_del = 1;
	$model->save();
	
        return $this->redirect(['index']);
    }

    public function actionRestore($id)
    {
        
        $model = $this->findModel($id);
	
	$model->status = 1;
	$model->status_del = 0;
	$model->save();
	
        return $this->redirect(['index2']);
    }
    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */

    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
