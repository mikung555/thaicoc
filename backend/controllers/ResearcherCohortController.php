<?php
namespace backend\controllers;

use backend\modules\ezforms\models\EzformDynamic;
use Yii;
use yii\helpers\VarDumper;

/**
 * Site controller
 */
class ResearcherCohortController extends \yii\web\Controller
{

    public function actionIndex(){
        //1444887998010303900
        $modelDynamic = new EzformDynamic('tbdata_1444887998010303900');
        $countAddData = $modelDynamic->find()->select('user_create')->where('user_create = :user_create', [':user_create' => Yii::$app->user->id])->count();
        $countAddDataPass = $modelDynamic->find()->select('user_create')->where('user_create = :user_create', [':user_create' => Yii::$app->user->id])->andWhere('var5 = :var5', [':var5' => 1])->count();
        /*
        $res = $modelDynamic->find()->select('a.user_create, b.mabstract, b.mmanuscript, b.status')->from(['a' => 'tbdata_1444887998010303900'])
            ->innerJoin(['b' => 'research'], '`a`.`user_create` = `b`.`created_by`')
            ->where('a.var5 = :var5', [':var5' => 1])->groupBy('a.user_create')->orderBy('b.created_at');
        */
        $res = $modelDynamic->find()->select('user_create')->distinct('user_create')->where('var5 = :var5', [':var5' => 1])->orderBy('create_date')->all();
        //$res = Yii::$app->db->createCommand("a.user_create, b.mabstract, b.mmanuscript, b.status FROM tbdata_1444887998010303900 AS a
        //INNER JOIN research AS b ON a.user_create = b.created_by WHERE a.var5 = 1 GROUP BY a.user_create ORDER BY b.created_at");
        //VarDumper::dump($res->all(), 10, true);
        //Yii::$app->end();
        return $this->render('index', [
            'model' => $res,
            'countAddData' => $countAddData,
            'countAddDataPass' => $countAddDataPass,
        ]);
    }

}
