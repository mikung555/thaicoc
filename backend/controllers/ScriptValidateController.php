<?php

namespace backend\controllers;

use Yii;
use backend\models\ScriptValidate;
use backend\models\ScriptValidateSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\Response;
use appxq\sdii\helpers\SDHtml;
use backend\models\ScriptLog;
use backend\models\ScriptResults;
use yii\db\Expression;

/**
 * ScriptValidateController implements the CRUD actions for ScriptValidate model.
 */
class ScriptValidateController extends Controller
{
    public function behaviors()
    {
        return [
	    'access' => [
		'class' => AccessControl::className(),
		'rules' => [
		    [
			'allow' => true,
			'actions' => ['index', 'view'], 
			'roles' => ['?', '@'],
		    ],
		    [
			'allow' => true,
			'actions' => ['view', 'create', 'update', 'delete', 'deletes'], 
			'roles' => ['@'],
		    ],
		],
	    ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public function beforeAction($action) {
	if (parent::beforeAction($action)) {
	    if (in_array($action->id, array('create', 'update'))) {
		
	    }
	    return true;
	} else {
	    return false;
	}
    }
    
    /**
     * Lists all ScriptValidate models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ScriptValidateSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionResult($gid)
    {
        $searchModel = new \backend\models\ScriptResultsSearch();
	$searchModel->gid = $gid;
	
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('result', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionGroup()
    {
        $searchModel = new \backend\models\ScriptGroupSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('group', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public static function getTranslated($fields) {
	    $arr = [];
	    if (!empty($fields)) {
		    foreach ($fields as $key => $value) {
			    $arr['{' . $key . '}'] = $fields[$key];
		    }
	    }
	    return $arr;
    }
    
    public function actionScript()
    {
	ini_set('max_execution_time', 0);
	set_time_limit(0);
	ini_set('memory_limit','1012M'); 
	
        $data = \backend\models\ScriptValidate::find()->where('enable=1')->orderBy('rating DESC')->all();
	$html = '';
	
	if($data){
	    $x=1;
	    $gid_validate = \common\lib\codeerror\helpers\GenMillisecTime::getMillisecTime();
	    $error_validate = 0;
	    $total_validate = 0;
	    $passed_validate = 0;
	    foreach ($data as $key => $value_validate) {
		$sql = $value_validate['sql'];
		$data_script;
		if($value_validate['return']=='one'){
		    $data_script = Yii::$app->db->createCommand($sql)->queryOne();
		} else {
		    $data_script = Yii::$app->db->createCommand($sql)->queryAll();
		}
		
		if($data_script){
		    $error_validate++;
		    
		    $fields = $data_script;
		    
		    if($value_validate['process'] != ''){
			eval($value_validate['process']);
		    }
		    
		    if($value_validate['sql_log'] != ''){
			try {
			    $sql_log = $value_validate['sql_log'];
			    Yii::$app->db->createCommand($sql_log)->execute();
			} catch (\yii\db\Exception $e) {
			    $html .= '<code>SQL LOG ERROR => CODE : '.$e->getCode(). ' MESSAGE : '.$e->getMessage().'</code><br>';
			}
		    }
		    
		    $result = strtr($value_validate['template'], self::getTranslated($fields));
		    
		    $model_log = new \backend\models\ScriptLog();
		    $model_log->gid = $gid_validate;
		    $model_log->active = 1;
		    $model_log->action_at = new \yii\db\Expression('NOW()');
		    $model_log->sid = $value_validate['sid'];
		    $model_log->error = 1;
		    $model_log->save();
		    
		    $model_result = new \backend\models\ScriptResults();
		    $model_result->gid = $gid_validate;
		    $model_result->sid = $value_validate['sid'];
		    $model_result->aid = $model_log->aid;
		    $model_result->title = $value_validate['title'];
		    $model_result->name = $value_validate['name'];
		    $model_result->description = $value_validate['description'];
		    $model_result->rating = $value_validate['rating'];
		    $model_result->result = $result;
		    $model_result->result_at = new \yii\db\Expression('NOW()');
		    $model_result->save();
		    
		    $html .= $value_validate['title'].' => '.$value_validate['description'].' [ '.$value_validate['name'].' ] ERROR = 1 Result = '.$result.'<br>';
		} else {
		    $passed_validate++;
		    
		    $model_log = new \backend\models\ScriptLog();
		    $model_log->gid = $gid_validate;
		    $model_log->active = 1;
		    $model_log->action_at = new \yii\db\Expression('NOW()');
		    $model_log->sid = $value_validate['sid'];
		    $model_log->error = 0;
		    $model_log->save();
		    
		    $html .= $value_validate['title'].' => '.$value_validate['description'].' [ '.$value_validate['name'].' ] ERROR = 0 <br>';
		}
		$x++;
		$total_validate++;
	    }
	    $model_group = new \backend\models\ScriptGroup();
	    $model_group->gid = $gid_validate;
	    $model_group->total = $total_validate;
	    $model_group->created_at = new \yii\db\Expression('NOW()');
	    $model_group->passed = $passed_validate;
	    $model_group->error = $error_validate;
	    $model_group->save();

	    return $this->render('script', [
		'html' => $html,
	    ]);
	}
    }
    /**
     * Displays a single ScriptValidate model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
	if (Yii::$app->getRequest()->isAjax) {
	    return $this->renderAjax('view', [
		'model' => $this->findModel($id),
	    ]);
	} else {
	    return $this->render('view', [
		'model' => $this->findModel($id),
	    ]);
	}
    }

    public function actionLogdb($id)
    {
	$model = ScriptResults::find()->where('rid=:rid', [':rid'=>$id])->one();
	
	return $this->renderAjax('logdb', [
	    'model' => $model,
	]);
    }
    
    public function actionTesting($id)
    {
	
	$model = $this->findModel($id);

	$result = '';

	if($model){
	    $sql = $model['sql'];
	    $data_script;
	    if($model['return']=='one'){
		$data_script = Yii::$app->db->createCommand($sql)->queryOne();
	    } else {
		$data_script = Yii::$app->db->createCommand($sql)->queryAll();
	    }

	    if($data_script){
		$fields = $data_script;

		if($model['process'] != ''){
		    eval($model['process']);
		}

		$result = strtr($model['template'], self::getTranslated($fields));
		
		$result_text = '<span class="label label-primary">'.$model['title'].'</span> => <span class="label label-warning">'.$model['description'].' [ '.$model['name'].' ]</span> <span class="label label-danger">ERROR</span> = 1 <br><span class="label label-success">Result</span> = '.$result.'<br>';
	    } else {
		$result_text = '<span class="label label-primary">'.$model['title'].'</span> => <span class="label label-warning">'.$model['description'].' [ '.$model['name'].' ]</span> <span class="label label-danger">ERROR</span> = 0 <br>';
	    }
	}

	return $this->renderAjax('testing', [
	    'model' => $model,
	    'result' => $result_text,
	]);
	
    }
    /**
     * Creates a new ScriptValidate model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
	if (Yii::$app->getRequest()->isAjax) {
	    $model = new ScriptValidate();
	    $model->enable = 1;
	    $model->rating = 3;
	    $model->return = 'one';
	    
	    if ($model->load(Yii::$app->request->post())) {
		Yii::$app->response->format = Response::FORMAT_JSON;
		
		
		
		if ($model->save()) {
		    $result = [
			'status' => 'success',
			'action' => 'create',
			'message' => SDHtml::getMsgSuccess() . Yii::t('app', 'Data completed.'),
			'data' => $model,
		    ];
		    return $result;
		} else {
		    $result = [
			'status' => 'error',
			'message' => SDHtml::getMsgError() . Yii::t('app', 'Can not create the data.'),
			'data' => $model,
		    ];
		    return $result;
		}
	    } else {
		return $this->renderAjax('create', [
		    'model' => $model,
		]);
	    }
	} else {
	    throw new NotFoundHttpException('Invalid request. Please do not repeat this request again.');
	}
    }

    /**
     * Updates an existing ScriptValidate model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
	if (Yii::$app->getRequest()->isAjax) {
	    $model = $this->findModel($id);

	    if ($model->load(Yii::$app->request->post())) {
		Yii::$app->response->format = Response::FORMAT_JSON;
		if ($model->save()) {
		    $result = [
			'status' => 'success',
			'action' => 'update',
			'message' => SDHtml::getMsgSuccess() . Yii::t('app', 'Data completed.'),
			'data' => $model,
		    ];
		    return $result;
		} else {
		    $result = [
			'status' => 'error',
			'message' => SDHtml::getMsgError() . Yii::t('app', 'Can not update the data.'),
			'data' => $model,
		    ];
		    return $result;
		}
	    } else {
		return $this->renderAjax('update', [
		    'model' => $model,
		]);
	    }
	} else {
	    throw new NotFoundHttpException('Invalid request. Please do not repeat this request again.');
	}
    }

    /**
     * Deletes an existing ScriptValidate model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
	if (Yii::$app->getRequest()->isAjax) {
	    Yii::$app->response->format = Response::FORMAT_JSON;
	    if ($this->findModel($id)->delete()) {
		$result = [
		    'status' => 'success',
		    'action' => 'update',
		    'message' => SDHtml::getMsgSuccess() . Yii::t('app', 'Deleted completed.'),
		    'data' => $id,
		];
		return $result;
	    } else {
		$result = [
		    'status' => 'error',
		    'message' => SDHtml::getMsgError() . Yii::t('app', 'Can not delete the data.'),
		    'data' => $id,
		];
		return $result;
	    }
	} else {
	    throw new NotFoundHttpException('Invalid request. Please do not repeat this request again.');
	}
    }

    public function actionDeletes() {
	if (Yii::$app->getRequest()->isAjax) {
	    Yii::$app->response->format = Response::FORMAT_JSON;
	    if (isset($_POST['selection'])) {
		foreach ($_POST['selection'] as $id) {
		    $this->findModel($id)->delete();
		}
		$result = [
		    'status' => 'success',
		    'action' => 'deletes',
		    'message' => SDHtml::getMsgSuccess() . Yii::t('app', 'Deleted completed.'),
		    'data' => $_POST['selection'],
		];
		return $result;
	    } else {
		$result = [
		    'status' => 'error',
		    'message' => SDHtml::getMsgError() . Yii::t('app', 'Can not delete the data.'),
		    'data' => $id,
		];
		return $result;
	    }
	} else {
	    throw new NotFoundHttpException('Invalid request. Please do not repeat this request again.');
	}
    }
    
    /**
     * Finds the ScriptValidate model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ScriptValidate the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ScriptValidate::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
