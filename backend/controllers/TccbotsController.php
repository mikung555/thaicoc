<?php

namespace backend\controllers;

use Yii;
use yii\data\SqlDataProvider;
use backend\classes\MonitorReport;

/**
 * Site controller
 */
class TccbotsController extends \yii\web\Controller {

    public function beforeAction($action) {
        if (empty(Yii::$app->user->identity->id)) {
            return $this->layout = "@frontend/views/layouts/mainreport";
        }
        return parent::beforeAction($action);
    }

    public function actionSetup() {
        return $this->render('setup', [
        ]);
    }

    public function actionSetupTccbot() {
        return $this->render('setup-tccbot', [
        ]);
    }

    public function actionCreateModule() {
        return $this->render('create-module');
    }

    public function actionMyModule() {
        return $this->render('my-module');
    }

    public function actionMyModuleApp() {
        return $this->render('my-module-app');
    }

    public function actionMyModuleCancer() {
        return $this->render('my-module-cancer');
    }

    public function actionModules() {
        return $this->render('modules');
    }

    public function actionMonitorReport() {
        return $this->render('monitor-report');
    }

    public function actionShowModules() {
        $dataProviderArray = MonitorReport::getReportOfModules();
        //\appxq\sdii\utils\VarDumper::dump($dataProvider);
        return $this->renderAjax('ff-modules-tab', $dataProviderArray);
    }

    public function actionShowModuleReport() {
        $request = \Yii::$app->request;
        $module_id = $request->post('module_id');
        $sqlMain = " SELECT main_ezf_table FROM inv_gen WHERE gid=:gid ";
        $resMain = \Yii::$app->db->createCommand($sqlMain, [':gid' => $module_id])->queryOne();
        if ($resMain['main_ezf_table'] != '') {
            $sqlDate = " SELECT MIN(DATE(create_date)) as begin_date FROM " . $resMain['main_ezf_table'] . " ";
            $resDate = \Yii::$app->db->createCommand($sqlDate)->queryOne();
        }
        return $this->renderAjax('ff-module-report-tab', ['module_id' => $module_id, 'begin_date' => $resDate['begin_date']]);
    }

    public function actionFormsReport() {
        $request = \Yii::$app->request;
        $module_id = $request->post('module_id');
        $sitecode = $request->post('sitecode');
        $startDate = $request->post('startdate');
        $endDate = $request->post('enddate');

        $module_name = $request->get('module_name');

        $sqlMain = " SELECT main_ezf_table FROM inv_gen WHERE gid=:gid ";
        $resMain = \Yii::$app->db->createCommand($sqlMain, [':gid' => $module_id])->queryOne();
        $whereSite = "";
        if ($sitecode != null) {
            $wheresite = " AND xsourcex='$sitecode' ";
        }
        if ($resMain['main_ezf_table'] != '') {
            $sqlDate = " SELECT MIN(DATE(create_date)) as begin_date FROM " . $resMain['main_ezf_table'] . " WHERE 1 $wheresite ";
            $resDate = \Yii::$app->db->createCommand($sqlDate)->queryOne();
        }

        $dataQuery = MonitorReport::getReportOfForms($module_id, $sitecode, $startDate, $endDate);
        $dataForms = new \yii\data\ArrayDataProvider([
            'allModels' => $dataQuery,
            'pagination' => [
                'pageSize' => 50,
            ],
        ]);
        if ($resDate != null) {
            $begin_date = $resDate['begin_date'];
        } else {
            $begin_date = date('Y-m-d');
        }

        if ($sitecode == null) {
            $begin_date = $startDate;
        }
        $siteName = "";
        if ($sitecode != null) {
            $siteName = MonitorReport::getSiteName($sitecode);
        }
        $render_page = 'ff-module-forms';
        return $this->renderAjax($render_page, [
                    'dataForms' => $dataForms, 'mod_name' => $module_name, 'sitename' => $siteName,
                    'startDate' => $startDate,
                    'endDate' => $endDate,
                    'begin_date' => $begin_date
        ]);
    }

    public function actionCumulativeGraph() {
        $request = \Yii::$app->request;
        $module_id = $request->post('module_id');
        $sitecode = $request->post('sitecode');
        $startDate = $request->post('startdate');
        $endDate = $request->post('enddate');

        $siteName = "";
        //$dataQuery = ModulesReport::getCumulativeGraph($module_id, $sitecode, $startDate, $endDate);
        //exit();
        $sqlInv = " SELECT main_ezf_table, ezf_table, enable_form FROM inv_gen WHERE gid=:gid ";
        $resInv = \Yii::$app->db->createCommand($sqlInv, [':gid' => $module_id])->queryOne();
        //\appxq\sdii\utils\VarDumper::dump($resInv);
        $form_data = \appxq\sdii\utils\SDUtility::string2Array($resInv['enable_form']);

        $form_lst = $form_data['form'];
        $whereDate = "";
        if ($startDate != null || $endDate != null) {
            $whereDate = " AND create_date BETWEEN '$startDate' AND '$endDate'";
        }
        if ($sitecode != null) {
            $whereDate .= " AND sitecode ='$sitecode' ";
            $siteName = MonitorReport::getSiteName($sitecode);
        }
        if ($resInv['main_ezf_table'] != '') {
            $sqlPatient = " SELECT count(DISTINCT ptid) as pcount, count(*) as ccount ,DAYOFMONTH(create_date) as 'day',MONTH(create_date) as 'month',YEAR(create_date) as 'year'
                    FROM " . $resInv['main_ezf_table'] . " WHERE YEAR(create_date) > '2013' $whereDate
                    GROUP by DATE(create_date) ORDER BY DATE(create_date) ASC;
                ";
            $resPatient = \Yii::$app->db->createCommand($sqlPatient)->queryAll();
        }
        $x = 0;
        $sumPatient = 0;
        $sumCare = '';
        $beginDay = '';
        $beginMonth = '';
        $beginYear = '';
        foreach ($resPatient as $val) {
            if ($beginDay == null) {
                $beginDay = $val['day'];
            }
            if ($beginMonth == null) {
                $beginMonth = $val['month'];
            }
            if ($beginYear == null) {
                $beginYear = $val['year'];
            }
            if ($x < count($resPatient) - 1) {
                $sumPatient += $val['pcount'];
                $sumCare += $val['ccount'];
                $day = $day . $val['day'] . ',';
                $dataPatient = $dataPatient . $sumPatient . ',';
                $dataCare = $dataCare . $sumCare . ',';
            } else {
                $sumPatient += $val['pcount'];
                $sumCare += $val['ccount'];
                $day = $day . "วันที่ " . $val['day'];
                $dataPatient = $dataPatient . $sumPatient;
                $dataCare = $dataCare . $sumCare;
            }
            $x++;
        }

        return $this->renderAjax('ff-cumulative-graph', [
                    'module_id' => $module_id,
                    'sitename' => $siteName,
                    'dataSeries' => $day,
                    'dataPatient' => $dataPatient,
                    'dataCare' => $dataCare,
                    'day' => $beginDay,
                    'month' => $beginMonth,
                    'year' => $beginYear,
                    'startDate' => $startDate,
                    'endDate' => $endDate
        ]);
    }

    public function actionGetSite($q = null, $id = null) {
        $sqlSite = " SELECT `name`, hcode FROM all_hospital_thai WHERE CONCAT(`hcode`, ' ', `name`) LIKE '%" . $q . "%' LIMIT 0,100";
        $resSite = \Yii::$app->db->createCommand($sqlSite)->queryAll();
        $out = [];
        $i = 0;
        foreach ($resSite as $value) {
            $out["results"][$i] = ['id' => $value['hcode'], 'text' => $value["hcode"] . " " . $value["name"]];
            $i++;
        }

        return json_encode($out);
    }

    public function actionSitesReport() {
        $request = \Yii::$app->request;
        $module_id = $request->get('module_id');
        $sitecode = $request->get('sitecode');
        $module_name = $request->get('module_name');
        $dataQuery = MonitorReport::getReportOfSites($module_id);
        $dataSites = new \yii\data\ArrayDataProvider([
            'allModels' => $dataQuery,
            'pagination' => [
                'pageSize' => 50,
            ],
        ]);
     

        $siteName = "";
        if ($sitecode != null) {
            $siteName = MonitorReport::getSiteName($sitecode);
        }
        $render_page = 'ff-module-sites';
        return $this->renderAjax($render_page, ['dataSites' => $dataSites, 'mod_name' => $module_name, 'sitename' => $siteName]);
    }

    public function actionShowOverview() {
        //\appxq\sdii\utils\VarDumper::dump('Render');
        return $this->renderAjax('ff-overview-tab');
    }

    public function actionAjaxReportOverview() {
        $dataAll = array();

        array_push($dataAll, [
            'section', ['จำนวนหน่วยงาน (หน่วย)', 'ทั้งหมด', 'ปีนี้', 'เดือนนี้', 'สัปดาห์นี้', 'วันนี้']
                ]
        );


        $sqlDeptMember = "select code4,COUNT(distinct sitecode) as nall,count(distinct IF(YEAR(FROM_UNIXTIME(created_at))=YEAR(CURDATE()),sitecode,null)) as thisYear,count(distinct IF(YEAR(FROM_UNIXTIME(created_at))=YEAR(CURDATE()) AND MONTH(FROM_UNIXTIME(created_at))=MONTH(CURDATE()),sitecode,null)) as thisMonth,count(distinct IF(YEARWEEK(FROM_UNIXTIME(`created_at`)) = YEARWEEK(NOW()),sitecode,null)) as thisWeek,count(distinct IF(DATE(FROM_UNIXTIME(created_at))=CURDATE(),sitecode,null)) as today  from user u inner join user_profile p ON u.id=p.user_id inner join all_hospital_thai h on p.sitecode=h.hcode where h.code2 not like '%demo%' and status_del=0 group by code4 order by code4+0;";
        $qryDeptMember = Yii::$app->db->createCommand($sqlDeptMember)->queryAll();

        foreach ($qryDeptMember as $membercount) {
            if (in_array($membercount['code4'], array("10"))) {
                $orgname = "หน่วยงานส่วนกลาง";
            } else if (in_array($membercount['code4'], array("1"))) {
                $orgname = "สำนักงานสาธารณสุขจังหวัด";
            } else if (in_array($membercount['code4'], array("2"))) {
                $orgname = "สำนักงานสาธารณสุขอำเภอ";
            } else if (in_array($membercount['code4'], array("5"))) {
                $orgname = "รพศ.";
            } else if (in_array($membercount['code4'], array("6"))) {
                $orgname = "รพท.";
            } else if (in_array($membercount['code4'], array("7"))) {
                $orgname = "รพช. รพร.";
            } else if (in_array($membercount['code4'], array("18", "3", "4", "8", "13", "17"))) {
                $orgname = "รพ.สต. สถานีอนามัย";
            } else if (in_array($membercount['code4'], array("11", "12"))) {
                $orgname = "โรงพยาบาล นอก สธ.";
            } else if (in_array($membercount['code4'], array("15", "16"))) {
                $orgname = "โรงพยาบาลเอกชน และคลินิกเอกชน";
            } else {
                $orgname = "อื่นๆ";
            }

            $dataAllP[$orgname]['All'] += $membercount['nall'];
            $dataAllP['All']['All'] += $membercount['nall'];

            $countp[$orgname]['All'] += $membercount['nall'];
            $countp[$orgname]['Year'] += $membercount['thisYear'];
            $countp[$orgname]['Month'] += $membercount['thisMonth'];
            $countp[$orgname]['Week'] += $membercount['thisWeek'];
            $countp[$orgname]['Day'] += $membercount['today'];
        }

        foreach ($countp as $orgname => $data) {
            $countAllP = $data['All'];
            $countYearP = $data['Year'];
            $countMonthP = $data['Month'];
            $countYearWeekRegisP = $data['Week'];
            $countDayP = $data['Day'];

            $sumAllP += $countAllP;
            $sumYearP += $countYearP;
            $sumMonthP += $countMonthP;
            $sumYearWeekRegisP += $countYearWeekRegisP;
            $sumDayP += $countDayP;


            array_push($dataAll, [
                'name', [$orgname, $countAllP, $countYearP, $countMonthP, $countYearWeekRegisP, $countDayP]
            ]);
        }

        array_push($dataAll, [
            'name', ['รวมทั้งหมด', $sumAllP, $sumYearP, $sumMonthP, $sumYearWeekRegisP, $sumDayP]
                ]
        );

        array_push($dataAll, [
            'section', ['จำนวนสมาชิก (คน)', 'ทั้งหมด', 'ปีนี้', 'เดือนนี้', 'สัปดาห์นี้', 'วันนี้']
                ]
        );
        $sqlDeptMember = "select code4,COUNT(*) as nall,SUM(YEAR(FROM_UNIXTIME(created_at))=YEAR(CURDATE())) as thisYear,SUM(YEAR(FROM_UNIXTIME(created_at))=YEAR(CURDATE()) AND MONTH(FROM_UNIXTIME(created_at))=MONTH(CURDATE())) as thisMonth,SUM(YEARWEEK(FROM_UNIXTIME(`created_at`)) = YEARWEEK(NOW())) as thisWeek,SUM(DATE(FROM_UNIXTIME(created_at))=CURDATE()) as today  from user u inner join user_profile p ON u.id=p.user_id inner join all_hospital_thai h on p.sitecode=h.hcode where h.code2 not like '%demo%' and status_del=0 group by code4 order by code4+0;";
        $qryDeptMember = Yii::$app->db->createCommand($sqlDeptMember)->queryAll();

        foreach ($qryDeptMember as $membercount) {
            if (in_array($membercount['code4'], array("10"))) {
                $orgname = "หน่วยงานส่วนกลาง";
            } else if (in_array($membercount['code4'], array("1"))) {
                $orgname = "สำนักงานสาธารณสุขจังหวัด";
            } else if (in_array($membercount['code4'], array("2"))) {
                $orgname = "สำนักงานสาธารณสุขอำเภอ";
            } else if (in_array($membercount['code4'], array("5"))) {
                $orgname = "รพศ.";
            } else if (in_array($membercount['code4'], array("6"))) {
                $orgname = "รพท.";
            } else if (in_array($membercount['code4'], array("7"))) {
                $orgname = "รพช. รพร.";
            } else if (in_array($membercount['code4'], array("18", "3", "4", "8", "13", "17"))) {
                $orgname = "รพ.สต. สถานีอนามัย";
            } else if (in_array($membercount['code4'], array("11", "12"))) {
                $orgname = "โรงพยาบาล นอก สธ.";
            } else if (in_array($membercount['code4'], array("15", "16"))) {
                $orgname = "โรงพยาบาลเอกชน และคลินิกเอกชน";
            } else {
                $orgname = "อื่นๆ";
            }

            $count[$orgname]['All'] += $membercount['nall'];
            $count[$orgname]['Year'] += $membercount['thisYear'];
            $count[$orgname]['Month'] += $membercount['thisMonth'];
            $count[$orgname]['Week'] += $membercount['thisWeek'];
            $count[$orgname]['Day'] += $membercount['today'];
        }

        foreach ($count as $orgname => $data) {
            $countAll = $data['All'];
            $countYear = $data['Year'];
            $countMonth = $data['Month'];
            $countYearWeekRegis = $data['Week'];
            $countDay = $data['Day'];

            $sumAll += $countAll;
            $sumYear += $countYear;
            $sumMonth += $countMonth;
            $sumYearWeekRegis += $countYearWeekRegis;
            $sumDay += $countDay;


            array_push($dataAll, [
                'name', [$orgname, $countAll, $countYear, $countMonth, $countYearWeekRegis, $countDay]
            ]);
        }

        array_push($dataAll, [
            'name', ['รวมทั้งหมด', $sumAll, $sumYear, $sumMonth, $sumYearWeekRegis, $sumDay]
                ]
        );

        $listResultReport = [
            "Register" => ["tb_data_1", "ptid", "id", "create_date"],
            "CCA-01 (ลงทะเบียนกลุ่มเสี่ยง)" => ["tb_data_2", "ptid", "id", "f1vdcomp"],
            "OV-01K (Kato-Katz)" => ["tbdata_21", "target", "id", "exdate"],
            "OV-01P (Parasep)" => ["tbdata_22", "target", "id", "exdate"],
            "OV-01F (FECT)" => ["tbdata_23", "target", "id", "exdate"],
            "OV-01U (Urine)" => ["tbdata_24", "target", "id", "exdate"],
            "OV-02 (ปรับเปลี่ยนพฤติกรรมเสี่ยงปรับเปลี่ยนพฤติกรรมเสี่ยง)" => ["tbdata_25", "target", "id", "vdate"],
            "OV-03 (ให้การรักษาพยาธิ)" => ["tbdata_26", "target", "id", "vdate"],
            "CCA-02 (ตรวจคัดกรองมะเร็งท่อน้ำดี)" => ["tb_data_3", "ptid", "id", "f2v1"],
            "CCA-02.1 (ตรวจ CT/MRI)" => ["tb_data_4", "ptid", "id", "f2p1v1"],
            "CCA-03 (ให้การรักษา)" => ["tb_data_7", "ptid", "id", "f3v2a2d"],
            //"CCA-03.1" => ["tb_data_6", "ptid", "id", "f3p1v1a1"],
            "CCA-04 (ตรวจพยาธิวิทยา)" => ["tb_data_8", "ptid", "id", "f4complete"],
            "CCA-05 (ติดตามผลการรักษามะเร็งท่อน้ำดี)" => ["tb_data_11", "ptid", "id", "f5v1a1"]
        ];
        array_push($dataAll, [
            'section', ['สรุปผลงานรวม', '', '', '', '', '']
                ]
        );

        $sql = "SELECT count(*) as nall from person";
        $sql = "SHOW TABLE STATUS LIKE 'person'";
        $data = Yii::$app->dbbot->createCommand($sql)->queryOne();
        $dataAllP['TCC'] = $data['Rows'];
        $sql = "SELECT count(*) as nall from person";
        $sql = "SHOW TABLE STATUS LIKE 'person'";
        $data = Yii::$app->dbnemo->createCommand($sql)->queryOne();
        $dataAllP['NEMO'] = $data['Rows'];

        foreach ($listResultReport as $key => $param) {
            if ($param[0] == "tb_data_1") {
                $sql = "SELECT count(distinct ptid) as AllP from tb_data_1 p INNER JOIN all_hospital_thai h on p.hsitecode=h.hcode where code2<>'demo' and rstat not in ('0','3') and DATE(create_date)>='2013-02-09';";
                $data = Yii::$app->db->createCommand($sql)->queryOne();
                $dataAllP['Form']['Register'] = $data['AllP'];
            } else if ($param[0] == "tb_data_2") {
                array_push($dataAll, [
                    'section', [$key, 'ทั้งหมด', 'ปีนี้', 'เดือนนี้', 'สัปดาห์นี้', 'วันนี้']
                        ]
                );
                $sql = "SELECT count(distinct ptid) as AllP,count(distinct IF(YEAR(f1vdcomp)=YEAR(CURDATE()),ptid,null)) as YearP,count(distinct IF(YEAR(f1vdcomp)=YEAR(CURDATE())  AND MONTH(f1vdcomp)=MONTH(CURDATE()),ptid,null)) as MonthP,count(distinct IF(YEARWEEK(f1vdcomp)=YEARWEEK(CURDATE()),ptid,null)) as WeekP,count(distinct IF(f1vdcomp=CURDATE(),ptid,null)) as DayP from tb_data_2 p INNER JOIN all_hospital_thai h on p.hsitecode=h.hcode where code2<>'demo' and rstat not in ('0','3') and DATE(f1vdcomp)>='2013-02-09';";
                $data = Yii::$app->db->createCommand($sql)->queryOne();
                $dataAllP['Form']['CCA-01'] = $data['AllP'];

                array_push($dataAll, [
                    'name', ['&emsp;&emsp;จำนวนราย', $data['AllP'], $data['YearP'], $data['MonthP'], $data['WeekP'], $data['DayP']]
                        ]
                );

                array_push($dataAll, [
                    'section', ['ตรวจรักษาพยาธิใบไม้ตับ (OV-01)', '', '', '', '', '']
                        ]
                );
                $dataAllP['Form']['CCA-01'] = $data['AllP'];
            } else {
                array_push($dataAll, [
                    'section', [$key, 'ทั้งหมด', 'ปีนี้', 'เดือนนี้', 'สัปดาห์นี้', 'วันนี้']
                        ]
                );

                if ($param[0] == "tb_data_3") {
                    $sql = "SELECT count(*) as AllT,count(distinct ptid) as AllP,SUM(YEAR(visitdate)=YEAR(CURDATE())) as YearT,count(distinct IF(YEAR(visitdate)=YEAR(CURDATE()),ptid,null)) as YearP,SUM(YEAR(visitdate)=YEAR(CURDATE()) AND MONTH(visitdate)=MONTH(CURDATE())) as MonthT,count(distinct IF(YEAR(visitdate)=YEAR(CURDATE())  AND MONTH(visitdate)=MONTH(CURDATE()),ptid,null)) as MonthP,SUM(YEARWEEK(visitdate)=YEARWEEK(CURDATE())) as WeekT,count(distinct IF(YEARWEEK(visitdate)=YEARWEEK(CURDATE()),ptid,null)) as WeekP,SUM(DATE(visitdate)=CURDATE()) as DayT,count(distinct IF(visitdate=CURDATE(),ptid,null)) as DayP,count(distinct IF(f2v6a3b1=1,ptid,null)) as suspected from table p INNER JOIN all_hospital_thai h on p.hsitecode=h.hcode where code2<>'demo' and rstat not in ('0','3') and DATE(visitdate)>='2013-02-09';";
                    $sql = str_replace('table', $param[0], $sql);
                    $sql = str_replace('visitdate', $param[3], $sql);
                    $data = Yii::$app->db->createCommand($sql)->queryOne();

                    $dataAllP['Form']['CCA-02P'] = $data['AllP'];
                    $dataAllP['Form']['CCA-02T'] = $data['AllT'];
                    $dataAllP['Form']['CCA-02S'] = $data['suspected'];
                } else {
                    $sql = "SELECT count(*) as AllT,count(distinct ptid) as AllP,SUM(YEAR(visitdate)=YEAR(CURDATE())) as YearT,count(distinct IF(YEAR(visitdate)=YEAR(CURDATE()),ptid,null)) as YearP,SUM(YEAR(visitdate)=YEAR(CURDATE()) AND MONTH(visitdate)=MONTH(CURDATE())) as MonthT,count(distinct IF(YEAR(visitdate)=YEAR(CURDATE())  AND MONTH(visitdate)=MONTH(CURDATE()),ptid,null)) as MonthP,SUM(YEARWEEK(visitdate)=YEARWEEK(CURDATE())) as WeekT,count(distinct IF(YEARWEEK(visitdate)=YEARWEEK(CURDATE()),ptid,null)) as WeekP,SUM(DATE(visitdate)=CURDATE()) as DayT,count(distinct IF(visitdate=CURDATE(),ptid,null)) as DayP from table p INNER JOIN all_hospital_thai h on p.hsitecode=h.hcode where code2<>'demo' and rstat not in ('0','3') and DATE(visitdate)>='2013-02-09';";
                    $sql = str_replace('table', $param[0], $sql);
                    $sql = str_replace('visitdate', $param[3], $sql);
                    $data = Yii::$app->db->createCommand($sql)->queryOne();
                }
                array_push($dataAll, [
                    'name', ['&emsp;&emsp;จำนวนราย', $data['AllP'], $data['YearP'], $data['MonthP'], $data['WeekP'], $data['DayP']]
                        ]
                );
                array_push($dataAll, [
                    'name', ['&emsp;&emsp;จำนวนครั้ง', $data['AllT'], $data['YearT'], $data['MonthT'], $data['WeekT'], $data['DayT']]
                        ]
                );
            }
            //\appxq\sdii\utils\VarDumper::dump($dataAll);
        }

        Yii::$app->response->format = \yii\web\Response::FORMAT_HTML;
        return $this->renderAjax('/tccbots/ff-overview-report', [
                    'dataAll' => $dataAll,
                    'dataAllP' => $dataAllP,
        ]);
    }

    public function actionTimelineFlukefree() {
        ini_set("memory_limit", "256M");
        ini_set('max_execution_time', 30000);
        $dataPost = Yii::$app->request->post();
        $page = $dataPost['page'];
        if (isset($page)) {
            $page = $page;
        } else {
            $page = 1;
        }
        $perPage = 50;
        $pageStart = ($page - 1) * $perPage;


        if (!empty($dataPost['module_id'])) { //ไม่เท่าค่าว่าง
            $result_timeline = \backend\classes\FlukefreeTimeline::getFlukefreeTimeline($dataPost);
            //$pagination = new Pagination(['totalCount' => count($result_timeline), 'pageSize' => 1,]);
        }


        //\appxq\sdii\utils\VarDumper::dump($result_timeline);
//        return $this->renderAjax('ff-timeline-report',[
//            'result_timeline' => $result_timeline,
//            //'pagination' => $pagination,
//        ]);
        $data['timeline'] = $this->renderAjax('ff-timeline-report', [
            'result_timeline' => array_slice($result_timeline, $pageStart, $perPage),
        ]);
        $totalPage = ceil(count($result_timeline) / 50);
        $html = "<ul class='pagination'>";

        for ($i = 1; $i <= $totalPage; $i++) {
            if ($page == $i) {
                $html .= "<li class='active'><a href='javascript:void(0)' onclick='pageSet({$i});' '>" . $i . "</a></li>";
            } else {
                $html .= "<li><a href='javascript:void(0)'  onclick='pageSet({$i});' >" . $i . "</a></li>";
            }
        }

        $html .= "</ul>";
        $data['page'] = $html;
        return json_encode($data);
    }

    public function actionShowMonitor() {

        $org = $person = \frontend\modules\hismonitor\classest\ReportFunc::getOrgCount();
        $person = \frontend\modules\hismonitor\classest\ReportFunc::getCount();
        $count_patient = \frontend\modules\hismonitor\classest\ReportFunc::getCountPatient();
        $sum_patint = \frontend\modules\hismonitor\classest\ReportFunc::getSum('', '', "SUM(npatient) AS sum_patint");
        $count_tdc = \frontend\modules\hismonitor\classest\ReportFunc::getCountTDC();
        $count_all_hospital_th = \frontend\modules\hismonitor\classest\ReportFunc::getCount("SELECT count(*) FROM all_hospital_thai where code2 not like '%demo%'and (code4 <> 15 and code4 <> 16 AND code4 <> 80 )");

        return $this->renderAjax('ff-monitor-tab', [
                    'org' => $org,
                    'person' => $person,
                    'count_patient' => $count_patient,
                    'sum_patint' => $sum_patint,
                    'count_tdc' => $count_tdc,
                    'count_all_hospital_th' => $count_all_hospital_th
        ]);
    }

    public function actionFlukefreeCountAll() {
        //echo 'TEST';exit();
        $query = \common\lib\nut\Lib::ArrayToObject($this->getSumFlukefreeCount("sum(item) as sum "));
        $sum = 0;
        foreach ($query as $q) {
            $sum += $q->sum;
        }
        return $this->renderAjax('_monitor-count-all', [
                    'sum' => $sum
        ]);
    }

    public function getSumFlukefreeCount($condition = "") {
        $sql = "SELECT $condition FROM buffe_tdc_count";
        $buffe_tdc_count = \Yii::$app->db->createCommand($sql)->queryAll();
        return $buffe_tdc_count;
    }

    public function actionFlukefreeCount() {
        if (\Yii::$app->request->isAjax) {

            $search = \Yii::$app->request->get("search", "");
            $condition = "";
            $params = [];
            if (!empty($search) || $search != "") {
                $condition = " AND `pac`.`sitecode` LIKE '%$search%' OR  `aht`.`name` LIKE '%$search%' ";

                // echo $condition;exit();
            }

//            $query = $this->getMonitorReport($condition);
            $query = \frontend\modules\hismonitor\classest\ReportFunc::getTdcCount($condition);

            $dataProvider = new \yii\data\ArrayDataProvider([
                'allModels' => $query,
                'pagination' => [
                    'pageSize' => 1000
                ]
            ]);

            return $this->renderAjax('monitor-count', [
                        'dataProvider' => $dataProvider,
                        'param_search' => $search
            ]);
        } else {
            return $this->redirect(['/tccbots/monitor-report']);
        }
    }

    public function actionDetail() {

        $sitecode = \Yii::$app->request->get("hcode");

        $sql = "
             SELECT '" . $sitecode . "' as sitecode
            ,IFNULL((SELECT bc.table_name from buffe_tdc_count bc WHERE bc.table_name = f43.f43 and sitecode = '" . $sitecode . "'),f43.f43) as `table_name`
            ,IFNULL((SELECT bc.`row` from buffe_tdc_count bc WHERE bc.table_name = f43.f43 and sitecode = '" . $sitecode . "'),0) as `row`
            ,IFNULL((SELECT bc.`field` from buffe_tdc_count bc WHERE bc.table_name = f43.f43 and sitecode = '" . $sitecode . "'),0) as `field`
            ,IFNULL((SELECT bc.`item` from buffe_tdc_count bc WHERE bc.table_name = f43.f43 and sitecode = '" . $sitecode . "'),0) as `item`

            from buffe_data.ezform_map_f43_tdc f43 ORDER BY `item` DESC 

           ";
        $sql = "
            SELECT 
                '" . $sitecode . "' as sitecode
                ,n.`ezf_name` as `table_name`
                ,IFNULL((SELECT bc.`row` from buffe_tdc_count bc WHERE bc.table_name = f.f43 and sitecode = '" . $sitecode . "'),0) as `row`
                ,IFNULL((SELECT bc.`field` from buffe_tdc_count bc WHERE bc.table_name = f.f43 and sitecode = '" . $sitecode . "'),0) as `field`
                ,IFNULL((SELECT bc.`item` from buffe_tdc_count bc WHERE bc.table_name = f.f43 and sitecode = '" . $sitecode . "'),0) as `item`
                from `ezform` n
                INNER JOIN `ezform_map_f43_tdc` f on f.`ezf_id` = n.`ezf_id` ORDER BY n.`ezf_name` asc
        ";
        //create talbe xxx(sitecode,tablename,rows,fields,items)
        $query = \Yii::$app->db->createCommand($sql)->queryAll();
        $dataProvider = new \yii\data\ArrayDataProvider([
            'allModels' => $query,
            'pagination' => [
                'pageSize' => 500
            ]
        ]);
        return $this->renderAjax('detail', [
                    'dataProvider' => $dataProvider
        ]);
    }

    public function actionSumTable() {

        $search = \Yii::$app->request->get("param_search", "");
        $condition = "";
        if (!empty($search) || $search != "") {
            $condition = " AND `pac`.`sitecode` LIKE '%$search%' OR  `aht`.`name` LIKE '%$search%' ";

            // echo $condition;exit();
        }
//        $model = $this->getMonitorReport($condition);
        $model = \frontend\modules\hismonitor\classest\ReportFunc::getTdcCount($condition);
        return $this->renderAjax("_sumtable", ['model' => $model]);
    }

    public function getMonitorReport($condition = '') {
        $sql = "
            SELECT 
                `pac`.`sitecode` as `hcodes`,
                `aht`.`name` as `hnames`,
                #`pac`.`npatient` as `count_patient`,
                count(`btc`.`table_name`) as `count_table`,
                IFNULL(sum(`btc`.`row`), 0) as `rows`,
                IFNULL(sum(`btc`.`field`), 0) as `fields`,
                IFNULL(sum(`btc`.`item`), 0) as `items`
                FROM `buffe_patient_count` as `pac` 
                LEFT JOIN buffe_tdc_count as `btc` ON `btc`.`sitecode` = `pac`.`sitecode`
                INNER JOIN `all_hospital_thai` as `aht` ON `pac`.`sitecode` = `aht`.`hcode`
                WHERE aht.code2 not like '%demo%' $condition
                #JOIN `buffe_patient_count` as `pac` ON `btc`.`sitecode` = `pac`.`sitecode`
                GROUP BY `pac`.`sitecode`
                ORDER BY `items` DESC;
        ";
        $query = \Yii::$app->db->createCommand($sql)->queryAll();



        return $query;
    }

    public function actionShowMap() {
        return $this->renderAjax("ff-map-report", ['model' => $model]);
    }

}
