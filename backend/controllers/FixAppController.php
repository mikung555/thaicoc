<?php
/**
 * Created by PhpStorm.
 * User: kongvut sangkla
 * Date: 11/19/2015 AD
 * Time: 11:55 AM
 */
namespace backend\controllers;
use backend\models\QueryList;
use backend\modules\ezforms\components\EzformQuery;
use backend\modules\ezforms\models\Ezform;
use backend\modules\ezforms\models\EzformFields;
use Symfony\Component\Yaml\Escaper;
use Yii;
use yii\bootstrap\Html;
use yii\db\Expression;
use yii\helpers\StringHelper;
use yii\helpers\VarDumper;

/**
 * Site controller
 */
class FixAppController extends \yii\web\Controller
{
    const URL = 'https://api.telegram.org/bot';
    const FILE_URL = 'https://api.telegram.org/file/bot';

    public $token;

    public function sendMessage($chat_id, $text){
        $response = json_decode(file_get_contents(self::URL . $this->token . '/sendMessage?chat_id=' . $chat_id . '&text=' . $text.'&parse_mode=markdown'));
        if ($response->ok == true)
            return $response->result;
        throw new \yii\web\HttpException(400, __METHOD__ . ': something went wrong with the Telegram Bot.');
    }

    public function actionTestTelegram(){
        /*
        $this->token = "199131849:AAFfaaZ5mhAfp0NWT0rqpgK2psfcx2sv0cg";
        $str ='*Help Desk* (ขอความช่วยเหลือกับผู้พัฒนา)%0A
*เรื่อง :* [ขอความช่วยเหลือ] ทดสอบ...%0A
*ชื่อ-สกุล :*  Administrator DAMASAC%0A
*E-mail :*  cascapcloud@gmail.com%0A
*เบอร์โทรศัพท์ :*  0874379405%0A
*URL ที่รายงาน หรือขอความช่วยเหลือ*%0A
https://cloudbackend.cascap.in.th/ovcca/ov-person/index?page=3%0A
*รายละเอียดการขอความช่วยเหลือ*%0A
*ทดสอบ*';
        //echo strip_tags($str); exit;
        $this->sendMessage('-1001030196726', $str);
        */

        $text =  addcslashes('**asdasd /asd  * _ [ ] ( )', '*,/,_,[,],\\');
        echo $text; exit;
        $bot = new \common\lib\telegram\helpers\TelegramBot();
        $bot->token = Yii::$app->keyStorage->get('telegram.token');
        echo $bot->sendMessage('-1001030196726', 'send text');

    }
    public function actionFixlpadHospital()
    {
        $ezform_field = EzformFields::find()->select('ezf_id, ezf_field_name')->where('ezf_field_type = 17')->all();
        foreach($ezform_field as $key => $val){
            $ezform = EzformQuery::getFormTableName($val->ezf_id);
            try {
                Yii::$app->db->createCommand()->update($ezform->ezf_table, [
                    $val->ezf_field_name => new Expression('LPAD('.$val->ezf_field_name.',5,0)'),
                    'update_date' => new Expression('NOW()'),
                ])->execute();
                echo $ezform->ezf_table.'<br>';
            }catch(\yii\db\Exception $e){

            }
        }
    }

    public function actionRemoveDataAgeing(){
        $ezfrom = Ezform::find()->select('ezf_id, ezf_table')
                ->where('comp_id_target = :comp_id', [':comp_id' => '1437725343023632100'])
                ->all();
        foreach ($ezfrom as $val){
            Yii::$app->db->createCommand()->delete("ezform_target", "ezf_id=:ezf_id", [":ezf_id" => $val->ezf_id])->execute();
            Yii::$app->db->createCommand()->delete("ezform_reply", "ezf_id=:ezf_id", [":ezf_id" => $val->ezf_id])->execute();
            Yii::$app->db->createCommand("truncate table `".($val->ezf_table)."`")->execute();
        }
    }

    public function actionCreateTableAgeing(){
        $ezform_field = EzformFields::find()->select('ezf_field_name, ezf_field_label, ezf_field_type')
            ->where('ezf_id = :ezf_id', [':ezf_id' => '1443080669047217100'])
            ->orWhere("ezf_id = '1442989979060379400'")
            ->orWhere("ezf_id = '1442903651009516800'")
            ->orWhere("ezf_id = '1442895844095608400'")
            ->orWhere("ezf_id = '1442849108041706000'")
            ->orWhere("ezf_id = '1442847910011085400'")
            ->orWhere("ezf_id = '1442846753060382900'")
            ->orWhere("ezf_id = '1442845413061287600'")
            ->orWhere("ezf_id = '1442809700000674000'")
            ->orWhere("ezf_id = '1442768005058344400'")
            ->all();
        //VarDumper::dump($ezform_field, 10, true);
        echo 'CREATE TABLE `tbdata_1443588615086934400` (<br>
        `id` bigint(20) NOT NULL,<br>
          `ptid_key` bigint(20) DEFAULT NULL,<br>
          `xsourcex` varchar(20) COLLATE utf8_unicode_ci NOT NULL,<br>
          `rstat` int(10) DEFAULT NULL,<br>
          `user_create` bigint(20) DEFAULT NULL,<br>
          `create_date` datetime DEFAULT NULL,<br>
          `user_update` bigint(20) DEFAULT NULL,<br>
          `update_date` datetime DEFAULT NULL,<br>
          `target` text COLLATE utf8_unicode_ci,<br>
          `error` mediumtext COLLATE utf8_unicode_ci,<br>';

        foreach($ezform_field AS $val) {
            if($val->ezf_field_name =='SYSCHECK'){
            }
            else if($val->ezf_field_type ==1){
              echo '`'.$val->ezf_field_name.'` text COLLATE utf8_unicode_ci DEFAULT NULL COMMENT \''.($val->ezf_field_label).'\',<br>';
            }
            else if($val->ezf_field_type ==3){
                echo '`'.$val->ezf_field_name.'` text COLLATE utf8_unicode_ci COMMENT \''.($val->ezf_field_label).'\',<br>';
            }
            else if($val->ezf_field_type ==4){
                echo '`'.$val->ezf_field_name.'` tinyint(4) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT \''.($val->ezf_field_label).'\',<br>';
            }
            else if($val->ezf_field_type ==6){
                echo '`'.$val->ezf_field_name.'` tinyint(4) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT \''.($val->ezf_field_label).'\',<br>';
            }
            else if($val->ezf_field_type ==7){
                echo '`'.$val->ezf_field_name.'` date DEFAULT NULL COMMENT \''.($val->ezf_field_label).'\',<br>';
            }
            else if($val->ezf_field_type ==8){
                echo '`'.$val->ezf_field_name.'` time DEFAULT NULL COMMENT \''.($val->ezf_field_label).'\',<br>';
            }
            else if($val->ezf_field_type ==9){
                echo '`'.$val->ezf_field_name.'` datetime DEFAULT NULL COMMENT \''.($val->ezf_field_label).'\',<br>';
            }
            else if($val->ezf_field_type ==10){
                echo '`'.$val->ezf_field_name.'` tinyint(4) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT \''.($val->ezf_field_label).'\',<br>';
            }
            else if($val->ezf_field_type ==11){
                echo '`'.$val->ezf_field_name.'` tinyint(4) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT \''.($val->ezf_field_label).'\',<br>';
            }
            else if($val->ezf_field_type ==12){
                echo '`'.$val->ezf_field_name.'` varchar(14) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT \''.($val->ezf_field_label).'\',<br>';
            }
            else if($val->ezf_field_type ==13){
                echo '`'.$val->ezf_field_name.'` varchar(14) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT \''.($val->ezf_field_label).'\',<br>';
            }
            else if($val->ezf_field_type ==14){
                echo '`'.$val->ezf_field_name.'` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT \''.($val->ezf_field_label).'\',<br>';
            }
            else if($val->ezf_field_type ==16){
                echo '`'.$val->ezf_field_name.'` tinyint(4) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT \''.($val->ezf_field_label).'\',<br>';
            }
            else if($val->ezf_field_type ==17){
                echo '`'.$val->ezf_field_name.'` tinyint(4) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT \''.($val->ezf_field_label).'\',<br>';
            }
            else if($val->ezf_field_type ==24){
                echo '`'.$val->ezf_field_name.'` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT \''.($val->ezf_field_label).'\',<br>';
            }
            else if($val->ezf_field_type =='' AND $val->ezf_field_type != 0){
                echo '`'.$val->ezf_field_name.'` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT \''.($val->ezf_field_label).'\',<br>';
            }
            else if($val->ezf_field_type ==231){
                echo '`'.$val->ezf_field_name.'` tinyint(4) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT \''.($val->ezf_field_label).'\',<br>';
            }
            else if($val->ezf_field_type ==252){
                echo '`'.$val->ezf_field_name.'` text COLLATE utf8_unicode_ci COMMENT \''.($val->ezf_field_label).'\',<br>';
            }
            else if($val->ezf_field_type ==253){
                echo '`'.$val->ezf_field_name.'` date utf8_unicode_ci COMMENT \''.($val->ezf_field_label).'\',<br>';
            }
            else if($val->ezf_field_type ==254){
                echo '`'.$val->ezf_field_name.'` tinyint(4) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT \''.($val->ezf_field_label).'\',<br>';
            }else{
                //echo '<hr>name = '.$val->ezf_field_name.' /label = '.$val->ezf_field_label .' /type = '.$val->ezf_field_type.' /field name = '.$val->ezf_field_name.'<hr>';
            }
        }



          echo 'PRIMARY KEY (`id`)<br>
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;';
        return ;
    }

    public function actionInsertSiteDemo(){
        $sql ="INSERT INTO `all_hospital_thai` (`code2`, `name`, `hcode`) VALUES ";
	    for($i=1;$i<=100;$i++) {
            $sitecode = 'Z'.str_pad($i, 4, "0", STR_PAD_LEFT);
            $sql .= "('demo', 'Site Demo ".$sitecode."' , '".$sitecode."'), ";
        }
        $sql = substr($sql, 0, -2);
        $sql .= ';';
        echo $sql;
        return;
    }

    public  function actionTest(){
        $res = Yii::$app->db->createCommand("select ezf_name, ezf_table  from ezform WHERE ezf_id <>0 AND ezf_id <> 1;")->queryAll();
        //VarDumper::dump($res, 10, true);
        if(Yii::$app->request->get('user_create')) {
            foreach($res as $val) {
                Yii::$app->db->createCommand("DELETE FROM `".$val['ezf_table']."` WHERE user_create = :user_create", [':user_create' => Yii::$app->request->get('user_create')])->execute();
                echo $val['ezf_table'].'<br>';
            }
        }
    }

    public  function actionUpdateUsercreate(){
        $res = Yii::$app->db->createCommand("select ezf_name, ezf_table  from ezform WHERE ezf_id <>0 AND ezf_id <> 1 AND status <> 3 AND ezf_id=:ezf_id;", [':ezf_id'=>$_GET['ezf_id']])->queryOne();
        //VarDumper::dump($res, 10, true);
        //foreach ($res as $key => $val){
            $res1 = Yii::$app->db->createCommand("select user_create, xsourcex from `" . $res['ezf_table'] . "`  WHERE LENGTH(user_create)<10 GROUP BY user_create;")->queryAll();
            foreach ($res1 as $key1 => $val1){
                $res2 = Yii::$app->db->createCommand("select username  from v04cascap.puser WHERE id = :id;", [':id'=>$val1['user_create']])->queryOne();
                $res3 = Yii::$app->db->createCommand("select id  from `user` WHERE username = :username;", [':username'=>$res2['username']])->queryOne();
                try {
                    Yii::$app->db->createCommand("UPDATE `" . $res['ezf_table'] . "` SET user_create = :user_create1  WHERE user_create = :user_create2", [':user_create1' => $res3['id'], ':user_create2' => $val1['user_create']])->execute();
                }catch(\yii\db\Exception $e){

                }
                $res1 = Yii::$app->db->createCommand("select user_create, xsourcex from `" . $res['ezf_table'] . "`  WHERE LENGTH(user_create)<10 GROUP BY user_create;")->queryAll();
            }
        //}

    }

    public  function  actionRuntest(){
        VarDumper::dump((explode('=', getenv('DB_DSN'))['3']), 10, true);
    }

    public  function  actionFixQuery(){
        //$res = Yii::$app->dbcascaputf8->createCommand("select ezf_name, ezf_table  from ezform WHERE ezf_id <>0 AND ezf_id <> 1;")->queryOne();
        //VarDumper::dump($res,10, true); exit;
        $queryList = Yii::$app->dbcascaputf8->createCommand("select *  from query_list")->queryAll();
        //VarDumper::dump($queryList,10, true); exit;
        foreach($queryList as $key => $val){
            $queryRequey = Yii::$app->dbcascaputf8->createCommand("select id  from query_request WHERE id = :id", [':id'=>$val['query_id']])->queryOne();
            if($queryRequey['id']){
                //
            }else{
                Yii::$app->db->createCommand("DELETE FROM `query_list` WHERE query_id = :query_id", [':query_id' => $val['query_id']])->execute();
            }
        }
    }

    public  function  actionFixReDraft(){
        //$res = Yii::$app->dbcascaputf8->createCommand("select ezf_name, ezf_table  from ezform WHERE ezf_id <>0 AND ezf_id <> 1;")->queryOne();
        //VarDumper::dump($res,10, true); exit;
        $queryList = Yii::$app->dbcascaputf8->createCommand("SELECT data_id FROM `query_request` WHERE `xsourcex` = '00533' AND `userkey_status` = '1' AND `ezf_id` = '1437377239070461302';")->queryAll();
        //VarDumper::dump($queryList,10, true); exit;
        $num = 0;
        foreach($queryList as $key => $val){
           Yii::$app->dbcascaputf8->createCommand()->update('tb_data_1', ['rstat' => 2], 'id = :data_id', [':data_id'=>$val['data_id']])->execute();
            Yii::$app->dbcascaputf8->createCommand()->update('ezform_target', ['rstat' => 2], 'data_id = :data_id', [':data_id'=>$val['data_id']])->execute();
            $num++;
        }
        echo $num;
    }

    public  function  actionFixReDraftSite(){

        $ezf_id = $_GET['ezf_id'];
        $sitecode = $_GET['sitecode'];
        $ezform = EzformQuery::getFormTableName($ezf_id);

        if($ezf_id == '1437377239070461301') {
            Yii::$app->dbcascaputf8->createCommand()->update($ezform->ezf_table, ['rstat' => 1], 'rstat = 2 AND xsourcex = :xsourcex AND (confirm <= 0 OR confirm is null)', [':xsourcex' => $sitecode])->execute();
            Yii::$app->dbcascaputf8->createCommand()->update('ezform_target', ['rstat' => 1], 'rstat = 2 AND xsourcex = :xsourcex AND ezf_id =:ezf_id', [':xsourcex' => $sitecode, ':ezf_id' => $ezf_id])->execute();
        }else{
            Yii::$app->dbcascaputf8->createCommand()->update($ezform->ezf_table, ['rstat' => 1], 'rstat = 2 AND xsourcex = :xsourcex', [':xsourcex' => $sitecode])->execute();
            Yii::$app->dbcascaputf8->createCommand()->update('ezform_target', ['rstat' => 1], 'rstat = 2 AND xsourcex = :xsourcex AND ezf_id =:ezf_id', [':xsourcex' => $sitecode, ':ezf_id' => $ezf_id])->execute();
        }
    }

    public  function  actionFixDelEmr(){

        $res = Yii::$app->dbcascaputf8->createCommand("select *, COUNT(*) FROM ezform_target WHERE  ezf_id = '1437377239070461301' GROUP BY data_id HAVING COUNT(*) > 1;")->queryAll();
        foreach ($res as $val){
            Yii::$app->dbcascaputf8->createCommand()->delete('ezform_target', 'data_id = :data_id', [':data_id'=> $val['data_id']])->execute();
            echo $val['data_id'].'<br>';
        }

    }
    //fix ptid = 1374400536041 กระต่ายจันทร์
    public  function  actionFixPtidX(){

        $res = Yii::$app->db->createCommand("select b.name, b.surname, cid, b.xsourcex from `ezform_target` as a  inner JOIN tb_data_1 as b on a.data_id=b.id where a.`target_id` = '1374400536041' AND a.`ezf_id` = '1437377239070461301' and b.rstat <> 3 ORDER BY b.create_date ASC")->queryAll();
        foreach ($res as $val){
            $res2 = Yii::$app->db->createCommand("SELECT * from tb_data_1 WHERE cid = :cid and rstat <> 3 ORDER BY create_date asc LIMIT 0,1", [':cid'=>$val['cid']])->queryOne();
            Yii::$app->db->createCommand()->update('tb_data_1', [
                'sitecode' => $val['sitecode'],
                'ptcode' => $val['ptcode'],
                'ptcodefull' => $val['ptcodefull'],
                'ptid' => $val['ptid']
            ])->execute();
        }

    }
    public function actionSiteLpad(){
        $ezform = \backend\models\Ezform::find()->where('ezf_id > 100')->all();
        //VarDumper::dump($ezform,10,true);exit();
        //update user
        Yii::$app->db->createCommand()->update('user_profile', [
            'sitecode' => new Expression('LPAD(sitecode,5,0)'),
        ])->execute();
        //update tb_data
        foreach($ezform as $key => $val){
            try {
                Yii::$app->db->createCommand()->update($val->ezf_table, [
                    'xsourcex' => new Expression('LPAD(xsourcex,5,0)'),
                    'sitecode' => new Expression('LPAD(sitecode,5,0)'),
                    'hsitecode' => new Expression('LPAD(hsitecode,5,0)'),
                ])->execute();
                echo $val->ezf_table.'<br>';
            }catch(\yii\db\Exception $e){

            }
        }
    }

    public static function actionFixAddColumn()
    {
        //run SQL fix app
        ini_set('memory_limit', '-1');

        $ezform = Ezform::find()->select('ezf_table')->where('ezf_id<>0 AND ezf_id<>1 AND ezf_id<>2')->all();
        if(Yii::$app->request->get('action')=="update-rstat") {
            foreach ($ezform as $val) {
                echo $val->ezf_table, '<br>';
                try {
                    $sql = "UPDATE `" . $val->ezf_table . "` SET rstat = 1 WHERE rstat =2;";
                    Yii::$app->db->createCommand($sql)->execute();
                    $sql = "UPDATE `" . $val->ezf_table . "` SET rstat = 2 WHERE rstat =4;";
                    Yii::$app->db->createCommand($sql)->execute();
                } catch (\yii\db\Exception $e){
                    echo "error = ".$sql.'<br>';
                }
            }
        }
        else if(Yii::$app->request->get('action')=="add-xdepartmentx") {
            foreach ($ezform as $val) {
                echo $val->ezf_table, '<br>';
                try {
                    $sql = "ALTER TABLE `" . $val->ezf_table . "` ADD COLUMN `xdepartmentx` varchar(20) NULL AFTER `xsourcex`;";
                    Yii::$app->db->createCommand($sql)->execute();
                } catch (\yii\db\Exception $e){
                    echo "error = ".$sql.'<br>';
                }
            }
        }
        else if(Yii::$app->request->get('action')=="add-error") {
            foreach ($ezform as $val) {
                echo $val->ezf_table, '<br>';
                try {
                    $sql = "ALTER TABLE `" . $val->ezf_table . "` ADD COLUMN `error` mediumtext;";
                    Yii::$app->db->createCommand($sql)->execute();
                } catch (\yii\db\Exception $e){
                    echo "error = ".$sql.'<br>';
                }
            }
        }
        else if(Yii::$app->request->get('action')=="add-ptid") {
            foreach ($ezform as $val) {
                echo $val->ezf_table, '<br>';
                try {
                    $sql = "ALTER TABLE `" . $val->ezf_table . "` ADD COLUMN `ptid`  bigint NULL AFTER `id`, ADD INDEX (`ptid`), ADD INDEX (`ptid`, `xsourcex`);";
                    Yii::$app->db->createCommand($sql)->execute();
                } catch (\yii\db\Exception $e){
                    echo "error = ".$sql.'<br>';
                }
            }
        }
        else if(Yii::$app->request->get('action')=="add-sitecode") {
            foreach ($ezform as $val) {
                echo $val->ezf_table;
                try {
                    $sql = "ALTER TABLE `" . $val->ezf_table . "` ADD COLUMN `sitecode` VARCHAR(10); ";
                    Yii::$app->db->createCommand($sql)->execute();
                } catch (\yii\db\Exception $e){
                    echo "error = ".$sql;
                }

                try{
                    $sql ="ALTER TABLE `" . $val->ezf_table . "` ADD COLUMN `ptcode` VARCHAR(10); ";
                    Yii::$app->db->createCommand($sql)->execute();
                } catch (\yii\db\Exception $e){
                    echo "error = ".$sql;
                }

                try{
                    $sql ="ALTER TABLE `" . $val->ezf_table . "` ADD COLUMN `ptcodefull` VARCHAR(20); ";
                    Yii::$app->db->createCommand($sql)->execute();
                } catch (\yii\db\Exception $e){
                    echo "error = ".$sql;
                }

                try{
                    $sql ="ALTER TABLE `" . $val->ezf_table . "` ADD COLUMN `hsitecode` VARCHAR(10); ";
                    Yii::$app->db->createCommand($sql)->execute();
                } catch (\yii\db\Exception $e){
                    echo "error = ".$sql;
                }

                try{
                    $sql ="ALTER TABLE `" . $val->ezf_table . "` ADD COLUMN `hptcode` VARCHAR(10); ";
                    Yii::$app->db->createCommand($sql)->execute();
                } catch (\yii\db\Exception $e){
                    echo "error = ".$sql;
                }

                echo '<hr>';

            }
        }


        Yii::$app->end();

    }

    public function actionMovePtid(){
        ini_set('memory_limit', '-1');
        $ezform = Ezform::find()->select('ezf_table, comp_id_target')->where('ezf_id<>0 AND ezf_id<>1 AND ezf_id<>2')->all();
        foreach ($ezform as $val){
//            try{
//                $sql ="ALTER TABLE `" . $val->ezf_table . "` ADD COLUMN `target` varchar(50) AFTER `update_date`, CHANGE COLUMN `target` `target_old` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL;";
//                Yii::$app->db->createCommand($sql)->execute();
//            } catch (\yii\db\Exception $e){
//                echo "<br>error1 = ".$sql;
//            }

//            try{
//                $sql ="UPDATE `" . ($val->ezf_table) . "` SET ptid = target_old";
//                Yii::$app->db->createCommand($sql)->execute();
//            } catch (\yii\db\Exception $e){
//                echo "<br>error2 = ".$sql;
//            }

            try{
                echo $val->comp_id_target.'<br>';
                if($val->comp_id_target) {
                    $tb_comp = EzformQuery::getTablenameFromComponent($val->comp_id_target);
                    if($tb_comp->ezf_table != $val->ezf_table) {
                        $sql = "UPDATE `" . $val->ezf_table . "` as a SET target = (SELECT id FROM `" . $tb_comp->ezf_table . "` WHERE ptid = a.ptid AND xsourcex = a.xsourcex)";
                        Yii::$app->db->createCommand($sql)->execute();
                    }
                }
            } catch (\yii\db\Exception $e){
                echo "<br>error3 = ".$sql;
            }
        }
    }

    public static function actionRemovePtidKey(){
        $ptid = Yii::$app->request->get('ptid');
        if($ptid) {
            $ezform = Ezform::find()->select('ezf_table')->where('ezf_id<>0 AND ezf_id<>1 AND ezf_id<>2')->all();
            //remove data all table
            foreach ($ezform as $val) {
                $sql = "SELECT id FROM `" . $val->ezf_table . "` WHERE ptid = '" . $ptid . "'";
                $res = Yii::$app->db->createCommand($sql)->query();
                if($res->count()) {
                    $sql = "DELETE FROM `" . $val->ezf_table . "` WHERE ptid = '" . $ptid . "'";
                    echo $val->ezf_table,'<br>';
                    Yii::$app->db->createCommand($sql)->execute();
                }
            }
            //remove data form ezform target
            $sql = "DELETE FROM `ezform_target` WHERE target_id = '" . $ptid . "'";
            Yii::$app->db->createCommand($sql)->execute();
        }

        Yii::$app->end();
    }

    public static function actionRemoveDataid(){
        $dataid = Yii::$app->request->get('dataid');
        $ezf_id = Yii::$app->request->get('ezf_id');
        $ezform = Ezform::find()->select('ezf_table')->where('ezf_id = :ezf_id', [':ezf_id' => $ezf_id])->one();
        $sql = "SELECT COUNT(*) AS total FROM `" . $ezform->ezf_table . "` WHERE id = '" . $dataid . "'";
        $res = Yii::$app->db->createCommand($sql)->queryOne();
        if($res['total']) {
            $sql = "DELETE FROM `" . $ezform->ezf_table . "` WHERE id = '" . $dataid . "'";
            Yii::$app->db->createCommand($sql)->execute();
            echo 'success remove from '.$ezform->ezf_table.'<br>';
            $sql = "DELETE FROM `ezform_data_relation` WHERE target_ezf_data_id = '" . $dataid . "'";
            Yii::$app->db->createCommand($sql)->execute();
            echo 'success remove from ezform_data_relation<br>';
            $sql = "DELETE FROM `ezform_target` WHERE data_id = '" . $dataid . "'";
            Yii::$app->db->createCommand($sql)->execute();
            echo 'success remove from ezform_target<br>';
        }
    }

    public function actionFixEmr(){
        ini_set('memory_limit', '-1');
        if(Yii::$app->request->get('stop')=='yes'){
            $sql = "truncate table `fix_emr`";
            Yii::$app->db->createCommand($sql)->execute();
            echo $sql;
            Yii::$app->end();
        }

        $sql = "SELECT ezf_id FROM `fix_emr`;";
        $res = Yii::$app->db->createCommand($sql);
        $total = $res->query()->count();
        if($total){
            echo '<h1>'.$total.'</h1><br>';
            $resx =$res->queryOne();
        }else {
            $sql = "INSERT INTO fix_emr (`ezf_id`) SELECT ezf_id  from ezform_target GROUP BY ezf_id";
            Yii::$app->db->createCommand($sql)->execute();
            echo 'add table!';
            Yii::$app->end();
        }

        $EzfTarget  = EzformTarget::find()->where('ezf_id = :ezf_id', [':ezf_id' => $resx['ezf_id']])->all();
        foreach($EzfTarget as $val){
            $ezform = Ezform::find()->select('ezf_table')->where('ezf_id = :ezf_idx', [':ezf_idx' => $val->ezf_id])->one();
            //echo  $val->ezf_id.' : '.$ezform->ezf_table.'<br>';
            //ค้นหา user
            $sql = "SELECT user_create, xsourcex FROM `" . $ezform->ezf_table . "` WHERE id = '" . $val->data_id . "'";
            $res = Yii::$app->db->createCommand($sql)->queryOne();
            $xsourcex = $res['xsourcex'];
            $user_create = $res['user_create'];
            //ดึงข้อมูล
            $sql = "SELECT user_id, sitecode FROM `user_profile` WHERE user_id = '" . $res['user_create'] . "'";
            $res = Yii::$app->db->createCommand($sql);

            if($res->query()->count()) {
                $res = $res->queryOne();
            } else{
                //กรณีหาไม่เจอ
                $sql = "SELECT OrganizationName AS sitecode FROM puser WHERE pid =  '" . $user_create . "'";
                $res = Yii::$app->dbcascap->createCommand($sql);
                if($res->query()->count()) {
                    $res = $res->queryOne();
                }else {
                    //กรณีหาไม่เจอแล้วแทนด้วยค่าเดิม
                    $res = [];
                    $res['sitecode'] = $xsourcex;
                }
            }

            //ปรับปรุงค่าใหม่
            if($xsourcex != $res['sitecode']) {
                $sql = "UPDATE `" . $ezform->ezf_table . "` SET xsourcex = '" . $res['sitecode'] . "' WHERE user_create = '" . $res['user_id'] . "'";
                Yii::$app->db->createCommand($sql)->execute();
                //เลือกข้อมูลใหม่อีกครั้ง
                $sql = "SELECT xsourcex, rstat FROM `" . $ezform->ezf_table . "` WHERE id = '" . $val->data_id . "'";
                $res = Yii::$app->db->createCommand($sql);
                if ($res->query()->count()) {
                    $res = $res->queryOne();
                    if ($res['rstat'] != 3) {
                        if($val->rstat != $res['rstat']) {
                            $sql = "UPDATE `" . $ezform->ezf_table . "` SET rstat = '" . $res['rstat'] . "' WHERE id = '" . $val->data_id . "'";
                            Yii::$app->db->createCommand($sql)->execute();
                            echo 'Update rstat Tb_data = ' . $ezform->ezf_table . '<br>';
                        }
                        if ($res['xsourcex'] == '') {
                        } else {
                            if($val->xsourcex != $res['xsourcex']) {
                                $sql = "UPDATE `ezform_target` SET rstat = '" . $res['rstat'] . "', xsourcex = '" . $res['xsourcex'] . "' WHERE data_id = '" . $val->data_id . "'";
                                echo '<hr>Update xsourcex = ' . $sql . '<br>';
                                Yii::$app->db->createCommand($sql)->execute();
                            }
                        }
                    } else if ($res['rstat'] == 3) {
                        if($val->xsourcex != $res['xsourcex']) {
                            $sql = "UPDATE `ezform_target` SET rstat = '3', xsourcex = '" . $res['xsourcex'] . "' WHERE data_id = '" . $val->data_id . "'";
                            Yii::$app->db->createCommand($sql)->execute();
                        }
                    }
                } else {
                    echo 'Remove ' . $val->data_id . '<br>';
                    $sql = "DELETE FROM `ezform_target` WHERE data_id = '" . $val->data_id . "'";
                    Yii::$app->db->createCommand($sql)->execute();
                }
            }
        }//end loop
        $sql = "DELETE FROM `fix_emr` WHERE ezf_id = '" . $resx['ezf_id'] . "'";
        Yii::$app->db->createCommand($sql)->execute();

        //Yii::$app->end();
    }

    public static function actionFixRadioEtc(){
        //EzformFields::find()->where('');
    }

    public static function actionFixDupPtcode(){

        //update table register
        /*
        $hpcode = Yii::$app->user->identity->userProfile->sitecode;
        //VarDumper::dump($res, 10, true); exit;
        $res = Yii::$app->db->createCommand("SELECT hptcode, COUNT(hptcode) as total ,xsourcex,target FROM `tbdata_1` WHERE id = ptid_key GROUP BY hptcode,xsourcex HAVING COUNT(hptcode)>1 ORDER BY total desc;")->queryAll();
        foreach($res as $key=>$val) {
            $resx = Yii::$app->db->createCommand("SELECT id, xsourcex FROM `tbdata_1` WHERE id = ptid_key AND xsourcex = :xsourcex AND hptcode = :hptcode;", [':xsourcex' => $val['xsourcex'], ':hptcode'=>$val['hptcode']])->queryAll();
            foreach ($resx as $valx) {
                $pid = Yii::$app->db->createCommand("SELECT MAX(CAST(hptcode AS UNSIGNED))  AS hptcode FROM `tbdata_1` WHERE xsourcex = '" . $valx['xsourcex'] . "';")->queryOne();
                $pid = str_pad($pid['hptcode'] + 1, 5, "0", STR_PAD_LEFT);
                $ptcodefull = $valx['xsourcex'] . $pid;
                Yii::$app->db->createCommand("UPDATE tbdata_1 SET ptcode = :ptcode , ptcodefull = :ptcodefull, hptcode = :hptcode WHERE id =:id", [':ptcode' => $pid, ':ptcodefull' => $ptcodefull, ':hptcode' => $pid, ':id' => $valx['id']])->execute();
            }
        }
        */
        //update other table

        /*
        $arrTable1 = [
            'tbdata_1442809700000674000',
'tbdata_1442845413061287600',
'tbdata_1442846753060382900',
'tbdata_1442847910011085400',
'tbdata_1442849108041706000',
'tbdata_1442895844095608400',
'tbdata_1442903651009516800',
'tbdata_1442989979060379400',
'tbdata_1443080669047217100',
        ];
        $arrTable2 = [
            'tbdata_2',
            'tbdata_3',
            'tbdata_4',
        ];
        if( Yii::$app->request->get('arrTable')==1) {
            $arrTable = $arrTable1;
            $tbRegis = 'tbdata_1';
        }
        else {
            $arrTable =$arrTable2;
            $tbRegis = 'tbdata_1';
        }

        foreach($arrTable as $key => $val){
            echo '<a href="?arrTable='.(Yii::$app->request->get('arrTable')).'&tb='.$key.'">'.$val.'</a><br>';
        }

        if(Yii::$app->request->get('tb')) {
            $res = Yii::$app->db->createCommand("SELECT xsourcex,ptcode,sitecode,hptcode, target FROM `".$tbRegis."` WHERE id = ptid_key;")->queryAll();
            $tb = Yii::$app->request->get('tb');
            foreach ($res as $val) {
                $ptcodefull = $val['sitecode'] . $val['ptcode'];

                Yii::$app->db->createCommand("update `" . $arrTable[$tb] . "` SET sitecode = :sitecode, ptcode = :ptcode, ptcodefull= :ptcodefull, hsitecode = xsourcex, hptcode = :hptcode WHERE xsourcex = :xsourcex AND  target = :target;",
                    [
                        ':sitecode' => $val['sitecode'],
                        ':ptcode' => $val['ptcode'],
                        ':ptcodefull' => $ptcodefull,
                        ':hptcode' => $val['hptcode'],
                        ':xsourcex' => $val['xsourcex'],
                        ':target' => $val['target']
                    ])->execute();

            }
        }
        */

        $res = Yii::$app->db->createCommand("SELECT xsourcex,ptcode,sitecode,hptcode, target FROM `tbdata_1` WHERE id = ptid;")->queryAll();
        foreach ($res as $val) {
            $ptcodefull = $val['sitecode'] . $val['ptcode'];

            $resx = Yii::$app->db->createCommand("update `tbdata_1442809700000674000` SET sitecode = :sitecode, ptcode = :ptcode, ptcodefull= :ptcodefull, hsitecode = xsourcex, hptcode = :hptcode WHERE xsourcex = :xsourcex AND  target = :target;",
                [
                    ':sitecode' => $val['sitecode'],
                    ':ptcode' => $val['ptcode'],
                    ':ptcodefull' => $ptcodefull,
                    ':hptcode' => $val['hptcode'],
                    ':xsourcex' => $val['xsourcex'],
                    ':target' => $val['target']
                ]);
            echo $resx->rawSql.'<br>';
            $resx->execute();

        }

    }

    public static function actionRemoveSupAdmin(){
        $arr_group = Yii::$app->db->createCommand("select item_name from rbac_auth_assignment GROUP BY item_name;")->queryAll();

        foreach ($arr_group as $val){
            echo '<a href="?type='.$val['item_name'].'">'.$val['item_name'].'</a> | ';
        }
        $user_type = $_GET['type'];

        if($_GET['action']=='remove' && $_GET['user_id']) {
            $user_id = $_GET['user_id'];
            $sql = "DELETE FROM `rbac_auth_assignment` WHERE item_name = :item_name AND user_id = :user_id;";
            Yii::$app->db->createCommand($sql, [':user_id'=>$user_id, ':item_name'=>$user_type])->execute();
        }

        $sql = "SELECT b.user_id, (SELECT username FROM `user` WHERE id = b.user_id) as username, b.firstname, b.lastname, b.email, b.cid,  b.sitecode, (SELECT name FROM all_hospital_thai WHERE hcode = b.sitecode) as hname 
FROM `rbac_auth_assignment` as a INNER JOIN user_profile as b ON a.user_id = b.user_id WHERE a.item_name = :item_name;";
        $res = Yii::$app->db->createCommand($sql, [':item_name'=>$user_type])->queryOne();
        echo '<table border="1"><tr><td>#</td>';
        foreach ($res as $key => $val) {
            echo '<td>' . $key . '</td>';
        }
        echo '</tr>';
        $res = Yii::$app->db->createCommand($sql, [':item_name'=>$user_type])->queryAll();
        //VarDumper::dump($res,10,true); exit;
        foreach ($res as $key => $val) {
            echo '<tr><td>' . ($key+1) . '</td>';
            echo '<td>' . $val['user_id'] . '</td>';
            echo '<td>' . $val['username'] . '</td>';
            echo '<td>' . $val['firstname'] . '</td>';
            echo '<td>' . $val['lastname'] . '</td>';
            echo '<td>' . $val['email'] . '</td>';
            echo '<td>' . $val['cid'] . '</td>';
            echo '<td>' . $val['sitecode'] . '</td>';
            echo '<td>' . $val['hname'] . '</td>';
            echo '<td><a href="?action=remove&type='.$user_type.'&user_id=' . $val['user_id'] . '" onclick="return confirm(\'Are you sure?\')">Remove</a></td></tr>';
        }
        echo '</table>';

    }
    //fix ptid = 1374400536041 กระต่ายจันทร์
    public  function  actionFixPtidX(){

        if($_GET['type']=='rabbit') {
            $res = Yii::$app->db->createCommand("select b.name, b.surname, cid, b.xsourcex from `ezform_target` as a  inner JOIN tb_data_1 as b on a.data_id=b.id where a.`target_id` = '1374400536041' AND a.`ezf_id` = '1437377239070461301' and b.rstat <> 3 ORDER BY b.create_date ASC")->queryAll();
            foreach ($res as $val) {
                $res2 = Yii::$app->db->createCommand("SELECT * from tb_data_1 WHERE cid = :cid and rstat <> 3 ORDER BY create_date asc LIMIT 0,1", [':cid' => $val['cid']])->queryOne();
                Yii::$app->db->createCommand()->update('tb_data_1', [
                    'sitecode' => $res2['sitecode'],
                    'ptcode' => $res2['ptcode'],
                    'ptcodefull' => $res2['ptcodefull'],
                    'ptid' => $res2['ptid']
                ], 'cid = :cid', [':cid' => $res2['cid']])->execute();
            }
        }else if($_GET['type']=='sitecode0'){
            $res = Yii::$app->db->createCommand("select cid from tb_data_1 WHERE (sitecode is null or LENGTH(sitecode) =0) or (ptcode is null or LENGTH(ptcode) =0)")->queryAll();
            foreach ($res as $val) {
                $res2 = Yii::$app->db->createCommand("SELECT * from tb_data_1 WHERE cid = :cid and rstat <> 3 ORDER BY create_date asc LIMIT 0,1", [':cid' => $val['cid']])->queryOne();
                Yii::$app->db->createCommand()->update('tb_data_1', [
                    'sitecode' => $res2['sitecode'],
                    'ptcode' => $res2['ptcode'],
                    'ptcodefull' => $res2['ptcodefull'],
                    'ptid' => $res2['ptid']
                ], 'cid = :cid', [':cid' => $res2['cid']])->execute();
            }
        }

    }


    public function actionDpmFixMember(){

        if($_GET['confirm']=='ok') {
            $n = $_GET['n'];
            $res = Yii::$app->dbbot->createCommand("SELECT username, password, substring_index(`name` , ' ',1) AS fname, substring_index(`name` , ' ',-1) as lname FROM surgery.`dep_member` WHERE id=:id;", [':id'=>$_GET['id']])->queryOne();
            VarDumper::dump($res,10,true);
            Yii::$app->db->createCommand()->insert('Sheet3', [
                'username'=>$res['username'],
                'passwords'=>$res['password'] ? $res['password'] : $res['username'],
                'title'=>'surgery',
                'name'=>$res['fname'],
                'surname'=>$res['lname'],
                'cid'=>'1119300193001',
                'OrganizationName'=>'93001',
                'Email'=>$res['username'].'@mail.com',
                'hcode'=>'93001'
            ])->execute();
            echo Html::a('<hr>n = '.($n+1), ['/fix-app/dpm-fix-member', 'n'=>$n+1]);

        }else{
            $n = $_GET['n'] ? $_GET['n'] : 0;
            $res = Yii::$app->dbbot->createCommand("SELECT id, substring_index(`name` , ' ',1) AS fname, substring_index(`name` , ' ',-1) as lname FROM surgery.`dep_member` LIMIT ".$n.",1;")->queryOne();
            VarDumper::dump($res,10,true);
            $res2=Yii::$app->dbcascap->createCommand("select title, name, surname from v04cascap.puser where title = :title AND (`name` LIKE :fname OR `name` LIKE :lname);", [':title'=>'surgery', ':fname'=>'%'.$res['fname'].'%', ':lname'=>'%'.$res['lname'].'%',])->queryAll();
            echo '<hr>from puser<br>';
            foreach ($res2 as $key=>$val){
                echo $val['title'].' '.$val['name'].' '.$val['surname'].'<br>';
            }

            $res3=Yii::$app->dpmcloud->createCommand("select firstname, lastname from dpmcloud.user_profile where `firstname` LIKE :fname AND `lastname` LIKE :lname;", [':fname'=>'%'.$res['fname'].'%', ':lname'=>'%'.$res['lname'].'%',])->queryAll();
            echo '<hr>from user_profile<br>';
            foreach ($res3 as $key=>$val){
                echo $val['firstname'].' '.$val['lastname'].'<br>';
            }
            echo Html::a('confirm n = '.($n), ['/fix-app/dpm-fix-member','confirm'=>'ok', 'n'=>$n, 'id'=>$res['id']]);
            echo Html::a(' | next = '.($n+1), ['/fix-app/dpm-fix-member', 'n'=>$n+1]);
        }

    }
}
