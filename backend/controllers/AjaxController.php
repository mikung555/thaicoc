<?php
/**
 * Created by PhpStorm.
 * User: hackablex
 * Date: 7/12/2016 AD
 * Time: 20:22
 */

namespace backend\controllers;


class AjaxController extends \yii\web\Controller
{
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionListProvince($zone){
        $res = \Yii::$app->db->createCommand("select provincecode, province from `all_hospital_thai` WHERE zone_code = :zone GROUP BY provincecode;", [':zone'=>$zone])->queryAll();
        return json_encode($res);
    }

    public function actionListEzfField($ezf_id){
        $res = \Yii::$app->db->createCommand("select ezf_field_name, ezf_field_label from `ezform_fields` WHERE ezf_field_type not in(2,13,15,'') and ezf_field_type is not null and  ezf_id = :ezf_id;", [':ezf_id'=>$ezf_id])->queryAll();
        return json_encode($res);
    }

    public function actionListEzfFieldDate($ezf_id){
        $res = \Yii::$app->db->createCommand("select ezf_field_name, ezf_field_label from `ezform_fields` WHERE ezf_field_type in(7,9) and ezf_id = :ezf_id;", [':ezf_id'=>$ezf_id])->queryAll();
        return json_encode($res);
    }

}