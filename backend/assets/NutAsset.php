<?php
 
namespace backend\assets;

use yii\web\AssetBundle;

class NutAsset extends AssetBundle
{
    public $sourcePath='@backend/modules/test/assets';

    public $css = [
        'drag/gridstack.css',
        'drag/gridstack-extra.css',
        'drag/style.css'
    ];
    public $js = [
	
        'drag/lodash.min.js',
        'drag/gridstack.js',
        'drag/gridstack.jQueryUI.js',
        
    ];

    public $depends = [
        'yii\web\YiiAsset',
    ];
}
