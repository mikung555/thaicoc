<?php
/**
 * Created by PhpStorm.
 * User: zein
 * Date: 7/3/14
 * Time: 3:14 PM
 */

namespace backend\assets;

use yii\web\AssetBundle;

class BackendAsset extends AssetBundle
{
    public $basePath = '/';
    public $baseUrl = '@backendUrl';

    public $css = [
        'css/style.css?9812',
	'css/style_fix.css?99',
        'css/cssdad/jquery.dad.css',
        'js-fileinput/css/fileinput.css',
        'bootstrap-chosen-master/bootstrap-chosen.css',
	'css/jquery.dataTables.min.css',
        'css/bs4fix.css'
        
    ];
    public $js = [
        'js/app.js',
        'js/jsdad/jquery.dad.js',
        'js/jsdad/jquery.nicescroll.min.js',
        'js-fileinput/js/fileinput.js',
        'js/chosen.jquery.js',
	'js/jquery.dataTables.min.js',
	'js/dataTables.fixedColumns.min.js',
    ];

    public $depends = [
        'yii\web\YiiAsset',
        'common\assets\AdminLte',
        'common\assets\Html5shiv',
        'common\lib\sdii\assets\SDAsset',
    ];
}
