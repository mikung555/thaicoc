<?php
 
namespace backend\assets;

use yii\web\AssetBundle;

class AngularjsAsset extends AssetBundle
{
    public $sourcePath='@backend/modules/angularjs/assets';

    public $css = [
        "//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css"
    ];
    public $js = [
        'js/angular.min.js',
        //'js/angular-route.min',
        '//code.angularjs.org/snapshot/angular-sanitize.js',
        'js/app.js' 
        
    ];

    public $depends = [
        'yii\web\YiiAsset',
    ];
}
