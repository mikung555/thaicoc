<?php
/**
 * Created by PhpStorm.
 * User: zein
 * Date: 7/3/14
 * Time: 3:14 PM
 */

namespace backend\assets;

use yii\web\AssetBundle;

class EzfGenAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [
        'css/sd.css'
    ];
    
    public $js = [
        'js/js-gen-condition.js?132423431244',
	//'js/appxqCore.js'
    ];

    public $depends = [
	
    ];
}
