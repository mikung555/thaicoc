<?php
/**
 * Created by PhpStorm.
 * User: Kongvut Sangkla
 * Date: 1/4/2559
 * Time: 1:25
 * E-mail: kongvut@gmail.com
 */
use yii\helpers\Html;

?>

<div class="media">
    <hr>
    <div class="media-left">
        <img class="media-object" alt="64x64"
             src="<?php echo Yii::$app->user->identity->userProfile->getAvatarById($model->user_create) ?: '/img/anonymous.jpg' ?>"
             data-holder-rendered="true" style="width: 64px; height: 64px;">
        <span><?php $user = common\models\UserProfile::findOne($model->user_create);
            echo $user->firstname . ' ' . $user->lastname; ?></span>
    </div>
    <div class="media-body">
        <?php
        //type = 1 comment
        //type = 2 consultant
        //type = 3 sos
        //type = 9 save data (input data)
        if ($model->type == 9) {
            $arrDiffOld = [];
            $arrDiffNew = [];
            $arrInit = [
                'id',
                'xsourcex',
                'rstat',
                'sitecode',
                'ptid',
                'ptcode',
                'ptcodefull',
                'hsitecode',
                'hptcode',
                'user_create',
                'create_date',
                'user_update',
                'update_date',
                'target',
                'error'
            ];
            $ezf_val_new = json_decode($model->ezf_json_new, true);
            $ezf_val_old = json_decode($model->ezf_json_old, true);
            foreach ($ezf_val_new as $key => $val) {
                if (in_array($key, $arrInit)) continue;
                if ($ezf_val_old[$key] != $val) {
                    $EzformFields = \backend\modules\ezforms\models\EzformFields::find()->select('ezf_field_type, ezf_field_id, ezf_field_name, ezf_field_label, ezf_field_sub_id')->where('ezf_id = :ezf_id AND ezf_field_name = :ezf_field_name', [':ezf_id' => $model->ezf_id, ':ezf_field_name' => $key])->one();

                    //แสดงค่าใหม่
                    $ezformChoice = \backend\modules\ezforms\models\EzformChoice::find()->where('ezf_field_id = :ezf_field_id AND ezf_choicevalue = :ezf_choicevalue', [':ezf_field_id' => $EzformFields->ezf_field_id, ':ezf_choicevalue' => $val])->one();
                    if (($EzformFields->ezf_field_type == 16 || ($EzformFields->ezf_field_type == 19 && $EzformFields->ezf_field_type == 0 && $EzformFields->ezf_field_sub_id)) && intval($ezf_val_old[$key]+0) != intval($val+0)) {
                        $arrDiffNew[$EzformFields->ezf_field_label] = ($val ? 'ใช่' : 'ไม่ใช่');
                    } else if($EzformFields->ezf_field_type != 16 && $EzformFields->ezf_field_type == 19) {
                        $arrDiffNew[$EzformFields->ezf_field_label] = ($ezformChoice->ezf_choicelabel != '' ? $ezformChoice->ezf_choicelabel : $val);
                    }

                    //แสดงค่าเก่า
                    $ezformChoice = \backend\modules\ezforms\models\EzformChoice::find()->where('ezf_field_id = :ezf_field_id AND ezf_choicevalue = :ezf_choicevalue', [':ezf_field_id' => $EzformFields->ezf_field_id, ':ezf_choicevalue' => $ezf_val_old[$key]])->one();
                    if (($EzformFields->ezf_field_type == 16 || ($EzformFields->ezf_field_type == 19 && $EzformFields->ezf_field_type == 0 && $EzformFields->ezf_field_sub_id)) && intval($ezf_val_old[$key]+0) != intval($val+0)) {
                        $arrDiffOld[$EzformFields->ezf_field_label] = (($ezf_val_old[$key]) ? 'ใช่' : 'ไม่ใช่');
                    } else if($EzformFields->ezf_field_type != 16 && $EzformFields->ezf_field_type == 19) {
                        $arrDiffOld[$EzformFields->ezf_field_label] = ($ezformChoice->ezf_choicelabel != '' ? $ezformChoice->ezf_choicelabel : $ezf_val_old[$key]);
                    }
                }
            }
            ?>
            <span>ข้อความ : <?= $model->ezf_comment; ?></span><br>
            <span>ประเภท : <?php if ($model->type == 1) echo 'แสดงความคิดเห็น'; else if ($model->type == 2) echo 'ขอคำปรึกษา'; else if ($model->type == 3) echo 'SOS'; else  echo 'บันทึกข้อมูล'; ?></span>
            <br>
            <span>เวลา : <?= $model->create_date; ?></span><br>
            <?php if (count($arrDiffNew)) { ?>
                <span>ค่าที่เปลี่ยน : </span> <a style="cursor: pointer;" onclick="$('#show-reply-val<?= $model->id; ?>').toggle();">(คลิกแสดงค่าเก่า- ค่าใหม่)</a><br>
                <div id="show-reply-val<?= $model->id; ?>" style="display: none;" class="alert alert-info" role="alert">
                    <div class="row">
                        <div class="col-md-6">
                            <span>ค่าเก่า</span><br>
                            <code><?php \yii\helpers\VarDumper::dump($arrDiffOld, 10, true); ?></code>
                        </div>
                        <div class="col-md-6">
                            <span>ค่าใหม่</span><br>
                            <code><?php \yii\helpers\VarDumper::dump($arrDiffNew, 10, true); ?></code>
                        </div>
                    </div>
                </div>
            <?php } else { ?>
                <span>ไม่มีการเปลี่ยนค่าข้อมูล</span><br>
            <?php }
        } else { ?>
            <span>ข้อความ : <?= $model->ezf_comment; ?></span><br>
            <span>ประเภท : <?php if ($model->type == 1) echo 'แสดงความคิดเห็น'; else if ($model->type == 2) echo 'ขอคำปรึกษา'; else if ($model->type == 3) echo 'SOS'; else if ($model->type == 0) echo 'เริ่มบันทึกข้อมูล'; else  echo 'บันทึกข้อมูล'; ?></span>
            <br>
            <span>เวลา : <?= $model->create_date; ?></span><br>
        <?php } ?>

        <?php if ($model->file_upload) { ?>
            <span>ไฟล์ที่แนบ</span><br>
            <ol>
                <?php
                $html = '';
                $array = explode(',', $model->file_upload);
                foreach ($array as $i => $value) {
                    $url = '/../uploads/ezf_reply/' . $value;
                    $html .= '<li>' . Html::a('เอกสารแนบ ' . ($i + 1), $url, ['target' => '_blank', 'class' => '', 'title' => 'เปิด',]);
                    $html .= '</li><br>';
                }
                echo $html;
                ?>
            </ol>
        <?php } ?>
    </div>
</div>

