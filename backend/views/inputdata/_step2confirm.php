<?php
/**
 * Created by PhpStorm.
 * User: Kongvut Sangkla
 * Date: 22/4/2559
 * Time: 13:34
 * E-mail: kongvut@gmail.com
 */
?>

<?php
use yii\bootstrap\Html;
use \yii\bootstrap\ActiveForm;
?>
    <div id="step2confirm" class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h3 class="modal-title" id="ModalUser"><i class="fa fa-plus" aria-hidden="true"></i> ยืนยันการเพิ่มข้อมูล</h3>
        </div>

        <?php
        $form = ActiveForm::begin([
            'id' => 'confirm-form',
            'options' => ['class' => 'form-vertical'],
            'action' => \yii\helpers\Url::to('insert-special'),
        ]);
        $hospital = \backend\modules\ezforms\components\EzformQuery::getHospital(Yii::$app->user->identity->userProfile->sitecode);
        ?>
        <!-- content -->
        <div class="modal-body">
            <?php if($queryParams['task'] == 'add-new') { ?>
                <?php if($queryParams['mode']=='gen-foreigner') echo '<div class="h3">คุณต้องการที่จะ (ลงทะเบียนชาวต่างชาติ) หรือไม่?</div>'; ?>
                <div class="h3">ยืนยันการเพิ่มข้อมูลใหม่สำหรับ "<code><?php echo $queryParams['target']; ?></code>" <br>เข้าสู่หน่วยงาน <?php echo $hospital['hcode'].' : '.$hospital['name']; ?></div>
                <hr>
            <?php } else if($queryParams['task'] == 'add-from-site') { ?>
                <div class="h3">พบข้อมูล <code><?php echo $queryParams['target']; ?></code> ที่ <?php echo $dataOtherSite['xsourcex'], ' : ', $dataOtherSite['hospital']; ?></div>
                <div class="h3">ข้อมูลที่พบคือ <span class="text-danger">
                    <?php foreach ($dataOtherSite as $key=>$val){
                        if($key=='xsourcex' || $key == 'hospital' || $key == 'id') continue;
                        echo $val, '  ';
                    } ?></span> นำเข้าข้อมูลนี้เข้าสู่หน่วยงานของท่าน?</div><hr>
                <?php
                echo (Yii::$app->user->can('datamanager') || Yii::$app->user->can('administrator') ? Html::a('<span class="fa fa-check-square-o"></span> ดูข้อมูลฟอร์ม '.$queryParams['comp_ezf_name'], ['/inputdata/redirect-page', 'ezf_id' => $queryParams['comp_ezf_id'], 'dataid'=>$dataOtherSite['id']], [
                    'target' => '_blank',
                    'class' => 'btn btn-warning btn-md',
                    'title' => Yii::t('app', 'ดูฟอร์มข้อมูล'),
                ]) : null);
                ?>
            <?php } //end add-from-site ?>
		    <div class="row">
			<div class="col-md-12 text-right">
            <?php /*
	    echo Html::a('<span class="fa fa-search"></span> ค้นหาข้อมูลจาก TDC', '#', [
                    'id' => 'btn-regis-tdc',
                    'target' => '_blank',
                    'class' => 'btn btn-primary btn-md ',
                    'title' => Yii::t('app', 'ดูฟอร์มข้อมูล'),
                ]).' '; 
	    */
		if($chkMappingTccBot){
                echo Html::a('<span class="fa fa-search"></span> ค้นหาข้อมูลจาก TDC', ['/inputdata/redirect-page', 'ezf_id' => $queryParams['comp_ezf_id'], 'dataid'=>$dataOtherSite['id']], [
                    'id' => 'btnRegfromBot',
                    'target' => '_blank',
                    'class' => 'btn btn-primary btn-md ',
                    'title' => Yii::t('app', 'ดูฟอร์มข้อมูล'),
                ]).($queryParams['task'] == 'add-new' ? '<br><br>' : '<br><br>');
            } ?>
		  </div>
		    </div>  
            <?php echo Html::hiddenInput('target', $queryParams['target']) ?>
            <?php echo Html::hiddenInput('task', $queryParams['task']) ?>
            <?php echo Html::hiddenInput('comp_id', $queryParams['comp_id']) ?>
            <?php echo Html::hiddenInput('mode', $queryParams['mode']); //gen-foreigner ?>
        </div>
        <div class="modal-footer">
            <?= Html::button('<i class="fa fa-check" aria-hidden="true"></i> สร้าง PID', ['class' => 'btn btn-success', 'id'=>'btn-confirm']) ?>
            <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i> ยกเลิก</button>
        </div>
        <?php ActiveForm::end() ?>
    </div>

<?php  $this->registerJs("
$('#btn-regis-tdc').on('click', function(e){
    $('#modal-query-request .modal-footer').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
    
    var cid = $(\"input[name=target]\").val();
    var \$form = $('form#confirm-form');
    var formData = new FormData(\$form[0]);
    $.ajax({
          url: '".\yii\helpers\Url::to(['/inputdata/reg-from-tdc'])."',
          type: 'POST',
          data: formData,
	  //dataType: 'JSON',
	  //enctype: 'multipart/form-data',
	processData: false,  // tell jQuery not to process the data
	contentType: false,   // tell jQuery not to set contentType
          success: function (result) {
	        $('div#step2confirm.modal-content').html(result);
          },
          error: function () {
	    console.log('server error');
          }
      });
    return false;
});

$('#btnRegfromBot').on('click', function(e){
    $('#modal-query-request .modal-footer').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
    
    var cid = $(\"input[name=target]\").val();
    var \$form = $('form#confirm-form');
    var formData = new FormData(\$form[0]);
    $.ajax({
          url: '".\yii\helpers\Url::to(['/inputdata/reg-from-bot'])."',
          type: 'POST',
          data: formData,
	  //dataType: 'JSON',
	  //enctype: 'multipart/form-data',
	processData: false,  // tell jQuery not to process the data
	contentType: false,   // tell jQuery not to set contentType
          success: function (result) {
	        $('div#step2confirm.modal-content').html(result);
          },
          error: function () {
	    console.log('server error');
          }
      });
    return false;
});

$('#btn-confirm').on('click', function(e){
    $('#modal-query-request .modal-footer').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');

    var \$form = $('form#confirm-form');
    var formData = new FormData(\$form[0]);
    $.ajax({
          url: \$form.attr('action'),
          type: 'POST',
          data: formData,
	  dataType: 'JSON',
	  //enctype: 'multipart/form-data',
	processData: false,  // tell jQuery not to process the data
	contentType: false,   // tell jQuery not to set contentType
          success: function (result) {
	    if(result.status == 'success'){
		". \common\lib\sdii\components\helpers\SDNoty::show('result.message', 'result.status') ."
		$('#modal-query-request .modal-body').html('<div class=\"h3\">สร้าง PID ด้วย <code id=\"cid\"></code> สำเร็จ! &nbsp;&nbsp;HOSPCODE : <span class=\"text-danger\" id=\"hsitecode\"></span>&nbsp;PID : <span class=\"text-danger\" id=\"hptcode\"></span></div>');
		$('#hsitecode').text(result.hsitecode);
		$('#hptcode').text(result.hptcode);
		$('#cid').text(result.cid);
		$('#modal-query-request .modal-footer').html('<a class=\"btn btn-success\" href=\"redirect-page?ezf_id='+result.ezf_id+'&dataid='+result.dataid+'\"><i class=\"fa fa-file-text\" aria-hidden=\"true\"></i> เปิดฟอร์ม</a>&nbsp;<button type=\"button\" class=\"btn btn-danger\" data-dismiss=\"modal\"><i class=\"fa fa-times\" aria-hidden=\"true\"></i> ปิด</button>');
		if(result.action == 'create'){
		    //$(document).find('#modal-query-request').modal('hide');
		} else if(result.action == 'update'){
		    //$(document).find('#modal-query-request').modal('hide');
		}
	    } else{
		". \common\lib\sdii\components\helpers\SDNoty::show('result.message', 'result.status') ."
		$('#modal-query-request .modal-footer').html('<button type=\"button\" class=\"btn btn-danger\" data-dismiss=\"modal\"><i class=\"fa fa-times\" aria-hidden=\"true\"></i> ปิด</button>');
	    }
          },
          error: function () {
	    console.log('server error');
          }
      });
    return false;
});

");?>