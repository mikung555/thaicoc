<?php
/**
 * Created by PhpStorm.
 * User: gundamx
 * Date: 10/20/2015 AD
 * Time: 9:55 PM
 */
?>


    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <span class="h3 modal-title" id="itemModalLabel"><i style="color: orangered;" class="fa fa-star"></i> Favorite Form</span>
        <span class="h4 text text-danger">(คลิกที่ <i class="fa fa-star-o"></i> เพื่อเลือกฟอร์มทำงาน)</span>
    </div>

    <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <!-- Custom Tabs -->
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="false">สาธารณะ</a></li>
                        <li class=""><a href="#tab_2" data-toggle="tab" aria-expanded="false">ถูกมอบหมาย</a></li>
                        <li class=""><a href="#tab_3" data-toggle="tab" aria-expanded="true">ใช้ร่วมกันภายในหน่วยงาน</a></li>
                        <li class=""><a href="#tab_4" data-toggle="tab" aria-expanded="true">ส่วนตัว</a></li>
                        <li class=""><a href="#tab_5" data-toggle="tab" aria-expanded="true">ตามหมวดกิจกรรม</a></li>
                        <li class=""><a href="#tab_6" data-toggle="tab" aria-expanded="true">เลือกไว้</a></li>
                    </ul>

                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_1">
                            <?php \yii\widgets\Pjax::begin(['id' => 'pjax_tab_1', 'enablePushState' => false]);?>
                            <table class="table table-bordered table-responsive table-hover">
                                <thead class="h4" style="background-color: #424242; color: #FFF;">
                                <tr>
                                    <th class="text-center" style="width:  5%;">#</th>
                                    <th class="text-center">Favorite</th>
                                    <th class="text-center">ชื่อฟอร์ม</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $i=1; foreach($publicForm as $val) { ?>
                                    <tr>
                                        <td class="text-center"><?php echo $i++; ?></td>
                                        <td class="text-center" width="15%"><?php $res = Yii::$app->db->createCommand("SELECT ezf_id FROM ezform_favorite WHERE ezf_id = '".$val['ezf_id']."' AND userid = '".Yii::$app->user->id."';")->query();
                                            if($res->count()){
                                                //echo '<span id="fav'.$i.'"><i data-div="'.$i.'" data-ezf_id="'.$val['ezf_id'].'" data-action="remove" style="color: orangered;" class="fa fa-star fa-2x clickable-fav"></i></span>';
                                                echo '<a href="/inputdata/favorite-form/?action=remove&ezf_id='.$val['ezf_id'].'"><i style="color: orangered;" class="fa fa-star fa-2x"></i></a>';
                                            }else{
                                                //echo '<span id="fav'.$i.'"><i data-div="'.$i.'" data-ezf_id="'.$val['ezf_id'].'" data-action="add" class="fa fa-star-o fa-2x clickable-fav"></i></span>';
                                                echo '<a href="/inputdata/favorite-form/?action=add&ezf_id='.$val['ezf_id'].'"><i class="fa fa-star-o fa-2x"></i></a>';

                                            } ?></td>
                                        <td><?php echo $val['ezf_name']; ?></td>
                                    </tr>
                                <?php } ?>
                                </tbody>

                            </table>
                            <?php \yii\widgets\Pjax::end(); ?>
                        </div>
                        <!-- /.tab-pane -->
                        <div class="tab-pane" id="tab_2">
                            <?php \yii\widgets\Pjax::begin(['id' => 'pjax_tab_2', 'enablePushState' => false]);?>
                            <table class="table table-bordered table-responsive table-hover">
                                <thead class="h4" style="background-color: #424242; color: #FFF;">
                                <tr>
                                    <th class="text-center" style="width:  5%;">#</th>
                                    <th class="text-center">Favorite</th>
                                    <th class="text-center">ชื่อฟอร์ม</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $i=1; foreach($assignForm as $val) { ?>
                                    <tr>
                                        <td class="text-center"><?php echo $i++; ?></td>
                                        <td class="text-center" width="15%"><?php $res = Yii::$app->db->createCommand("SELECT ezf_id FROM ezform_favorite WHERE ezf_id = '".$val['ezf_id']."' AND userid = '".Yii::$app->user->id."';")->query();
                                            if($res->count()){
                                                //echo '<span id="fav'.$i.'"><i data-div="'.$i.'" data-ezf_id="'.$val['ezf_id'].'" data-action="remove" style="color: orangered;" class="fa fa-star fa-2x clickable-fav"></i></span>';
                                                echo '<a href="/inputdata/favorite-form/?action=remove&ezf_id='.$val['ezf_id'].'"><i style="color: orangered;" class="fa fa-star fa-2x"></i></a>';
                                            }else{
                                                //echo '<span id="fav'.$i.'"><i data-div="'.$i.'" data-ezf_id="'.$val['ezf_id'].'" data-action="add" class="fa fa-star-o fa-2x clickable-fav"></i></span>';
                                                echo '<a href="/inputdata/favorite-form/?action=add&ezf_id='.$val['ezf_id'].'"><i class="fa fa-star-o fa-2x"></i></a>';

                                            } ?></td>
                                        <td><?php echo $val['ezf_name']; ?></td>
                                    </tr>
                                <?php } ?>
                                </tbody>

                            </table>
                            <?php \yii\widgets\Pjax::end(); ?>
                        </div>
                        <!-- /.tab-pane -->
                        <div class="tab-pane" id="tab_3">
                            <?php \yii\widgets\Pjax::begin(['id' => 'pjax_tab_3', 'enablePushState' => false]);?>
                            <table class="table table-bordered table-responsive table-hover">
                                <thead class="h4" style="background-color: #424242; color: #FFF;">
                                <tr>
                                    <th class="text-center" style="width:  5%;">#</th>
                                    <th class="text-center">Favorite</th>
                                    <th class="text-center">ชื่อฟอร์ม</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $i=1; foreach($siteForm as $val) { ?>
                                    <tr>
                                        <td class="text-center"><?php echo $i++; ?></td>
                                        <td class="text-center" width="15%"><?php $res = Yii::$app->db->createCommand("SELECT ezf_id FROM ezform_favorite WHERE ezf_id = '".$val['ezf_id']."' AND userid = '".Yii::$app->user->id."';")->query();
                                            if($res->count()){
                                                //echo '<span id="fav'.$i.'"><i data-div="'.$i.'" data-ezf_id="'.$val['ezf_id'].'" data-action="remove" style="color: orangered;" class="fa fa-star fa-2x clickable-fav"></i></span>';
                                                echo '<a href="/inputdata/favorite-form/?action=remove&ezf_id='.$val['ezf_id'].'"><i style="color: orangered;" class="fa fa-star fa-2x"></i></a>';
                                            }else{
                                                //echo '<span id="fav'.$i.'"><i data-div="'.$i.'" data-ezf_id="'.$val['ezf_id'].'" data-action="add" class="fa fa-star-o fa-2x clickable-fav"></i></span>';
                                                echo '<a href="/inputdata/favorite-form/?action=add&ezf_id='.$val['ezf_id'].'"><i class="fa fa-star-o fa-2x"></i></a>';

                                            } ?></td>
                                        <td><?php echo $val['ezf_name']; ?></td>
                                    </tr>
                                <?php } ?>
                                </tbody>

                            </table>
                            <?php \yii\widgets\Pjax::end(); ?>
                        </div>
                        <!-- /.tab-pane -->

                        <!-- /.tab-pane -->
                        <div class="tab-pane" id="tab_4">
                            <?php \yii\widgets\Pjax::begin(['id' => 'pjax_tab_4', 'enablePushState' => false]);?>
                            <table class="table table-bordered table-responsive table-hover">
                                <thead class="h4" style="background-color: #424242; color: #FFF;">
                                <tr>
                                    <th class="text-center" style="width:  5%;">#</th>
                                    <th class="text-center">Favorite</th>
                                    <th class="text-center">ชื่อฟอร์ม</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $i=1; foreach($privateForm as $val) { ?>
                                    <tr>
                                        <td class="text-center"><?php echo $i++; ?></td>
                                        <td class="text-center" width="15%"><?php $res = Yii::$app->db->createCommand("SELECT ezf_id FROM ezform_favorite WHERE ezf_id = '".$val['ezf_id']."' AND userid = '".Yii::$app->user->id."';")->query();
                                            if($res->count()){
                                                //echo '<span id="fav'.$i.'"><i data-div="'.$i.'" data-ezf_id="'.$val['ezf_id'].'" data-action="remove" style="color: orangered;" class="fa fa-star fa-2x clickable-fav"></i></span>';
                                                echo '<a href="/inputdata/favorite-form/?action=remove&ezf_id='.$val['ezf_id'].'"><i style="color: orangered;" class="fa fa-star fa-2x"></i></a>';
                                            }else{
                                                //echo '<span id="fav'.$i.'"><i data-div="'.$i.'" data-ezf_id="'.$val['ezf_id'].'" data-action="add" class="fa fa-star-o fa-2x clickable-fav"></i></span>';
                                                echo '<a href="/inputdata/favorite-form/?action=add&ezf_id='.$val['ezf_id'].'"><i class="fa fa-star-o fa-2x"></i></a>';

                                            } ?></td>
                                        <td><?php echo $val['ezf_name']; ?></td>
                                    </tr>
                                <?php } ?>
                                </tbody>

                            </table>
                            <?php \yii\widgets\Pjax::end(); ?>
                        </div>
                        <!-- /.tab-pane -->

                        <!-- /.tab-pane -->
                        <div class="tab-pane" id="tab_5">
                            <div class="alert alert-danger">อยู่ระหว่างเวอร์ชั่นทดสอบ</div>
                            <div class="row">
                                <div class="col-md-4">
                                    <?php
                                    echo \kartik\tree\TreeViewInput::widget(
                                        [
                                            'id' => 'EzTree',
                                            'name' => 'kvTreeInput',
                                            'value' => 'false', // preselected values
                                            'query' => backend\models\TblTree::find()->where('readonly=1 or userid='.Yii::$app->user->id.' or id IN (select distinct root from tbl_tree where userid='.Yii::$app->user->id.')')->addOrderBy('root, lft'),
                                            'headingOptions' => ['label' => 'Categories'],
                                            'treeOptions' => ['style' => 'height:500px;'],
                                            'rootOptions' => ['label'=>'<i class="fa fa-tree text-success"></i>'],
                                            'fontAwesome' => true,
                                            'asDropdown' => false,
                                            'multiple' => true,
                                            'options' => ['disabled' => false]

                                        ]
                                    );
                                    ?>
                                </div>
                            </div>
                        </div>
                        <!-- /.tab-pane -->

                        <!-- /.tab-pane -->
                        <div class="tab-pane" id="tab_6">
                            <?php \yii\widgets\Pjax::begin(['id' => 'pjax_tab_6', 'enablePushState' => false]);?>
                            <table class="table table-bordered table-responsive table-hover">
                                <thead class="h4" style="background-color: #424242; color: #FFF;">
                                <tr>
                                    <th class="text-center" style="width:  5%;">#</th>
                                    <th class="text-center">Favorite</th>
                                    <th class="text-center">ชื่อฟอร์ม</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $i=1; foreach($favForm as $val) { ?>
                                    <tr>
                                        <td class="text-center"><?php echo $i++; ?></td>
                                        <td class="text-center" width="15%"><?php $res = Yii::$app->db->createCommand("SELECT ezf_id FROM ezform_favorite WHERE ezf_id = '".$val['ezf_id']."' AND userid = '".Yii::$app->user->id."';")->query();
                                            if($res->count()){
                                                //echo '<span id="fav'.$i.'"><i data-div="'.$i.'" data-ezf_id="'.$val['ezf_id'].'" data-action="remove" style="color: orangered;" class="fa fa-star fa-2x clickable-fav"></i></span>';
                                                echo '<a href="/inputdata/favorite-form/?action=remove&ezf_id='.$val['ezf_id'].'"><i style="color: orangered;" class="fa fa-star fa-2x"></i></a>';
                                            }else{
                                                //echo '<span id="fav'.$i.'"><i data-div="'.$i.'" data-ezf_id="'.$val['ezf_id'].'" data-action="add" class="fa fa-star-o fa-2x clickable-fav"></i></span>';
                                                echo '<a href="/inputdata/favorite-form/?action=add&ezf_id='.$val['ezf_id'].'"><i class="fa fa-star-o fa-2x"></i></a>';

                                            } ?></td>
                                        <td><?php echo $val['ezf_name']; ?></td>
                                    </tr>
                                <?php } ?>
                                </tbody>

                            </table>
                            <?php \yii\widgets\Pjax::end(); ?>
                        </div>
                        <!-- /.tab-pane -->

                    </div>
                    <!-- /.tab-content -->
                </div>
                <!-- nav-tabs-custom -->
            </div>
        </div>

    </div>

<div class="modal-footer">
    <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i> ปิดหน้าต่าง</button>
</div>
<?php
$format = <<< SCRIPT
jQuery(document).ready(function($) {
    $(".clickable-fav").click(function() {
        var data_div =($(this).data("div"));
        var data_ezf_id =($(this).data("ezf_id"));
        var data_action =($(this).data("action"));
        $.post( "inputdata/favorite-form", { action: data_action, data_div: data_div, data_ezf_id : data_ezf_id }).done(function( data ) {
          $( "#fav"+data_div).html( data );
        });

    });
});

$("#EzTree").on('treeview.checked', function(event, key) {
    console.log($(this).val());
});

SCRIPT;

$this->registerJs($format, \yii\web\View::POS_END);
 ?>



