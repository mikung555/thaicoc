<?php
/**
 * Created by PhpStorm.
 * User: hackablex
 * Date: 5/16/2016 AD
 * Time: 10:39
 */
use yii\bootstrap\Html;
use yii\bootstrap\ActiveForm;
$this->title = '(Consult) : '.mb_substr($model->ezf_comment, 0,100,'UTF-8').'...';
?>


<div id="ezformview" class="box box-primary box-solid">
    <div class="box-header with-border">
        <h3 class="box-title"><i class="fa fa-globe"></i> Consult</h3>
        <div class="box-tools pull-right">
            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
        </div><!-- /.box-tools -->
    </div><!-- /.box-header -->
    <div class="box-body" style="display: block;">

        <!-- info row -->
        <div class="row invoice-info">
            <div class="col-sm-6 invoice-col">
                <strong class="h4">หัวข้อ</strong>
                <address>
                    เรื่อง : <?php echo $model->ezf_comment; ?><br>
                    ชื่อฟอร์ม : <?php echo $model->ezf_id; ?><br>
                    ประเภท : <?php echo $model->type; ?><br>
                    เมื่อ : <?php echo $model->create_date; ?><br>
                    <strong>รายละเอียด</strong>
                    <p class="well well-sm no-shadow" style="margin-top: 10px;">
                        <?php echo $model->oldAttributes['ezf_comment']; ?>
                    </p>
                </address>
            </div>
            <!-- /.col -->
            <div class="col-sm-4 invoice-col">
                <strong class="h4">โดย</strong>
                <address>
                    <?php
                    $user = \common\models\UserProfile::findOne($model->user_create);
                    echo $user->firstname . ' ' . $user->lastname;
                    ?><br>
                    ชื่อสถานบริการ : <?php echo $model->xsourcex;?><br>
                    Phone: <?php echo $user->telephone; ?><br>
                    Email: <?php echo $user->email; ?>
                </address>
            </div>
            <!-- /.col -->
            <div class="col-sm-2 invoice-col">
                <?php echo ' '.Html::a('<span class="fa fa-check-square-o"></span> View form', ['/inputdata/redirect-page', 'ezf_id' => $model->oldAttributes['ezf_id'], 'dataid'=>$model->data_id], [
                        'target' => '_blank',
                        'class' => 'btn btn-warning btn-lg',
                        'title' => Yii::t('app', 'ดูฟอร์มข้อมูล'),
                    ]); ?>
            </div>
            <!-- /.col -->
        </div>
    </div>
</div>

<div id="ezformview" class="box box-success box-solid">
    <div class="box-header with-border">
        <h3 class="box-title">แสดงความคิดเห็น</h3>
        <div class="box-tools pull-right">
            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
        </div><!-- /.box-tools -->
    </div><!-- /.box-header -->
    <div class="box-body" style="display: block;">

        <div class="media">
            <div class="media-left"><br>
                <img class="media-object" src="<?php echo Yii::$app->user->identity->userProfile->getAvatar() ?: '/img/anonymous.jpg' ?>" data-holder-rendered="true" style="width: 64px; height: 64px;">
                <?php echo Yii::$app->user->identity->userProfile->getFullName();?>
            </div>
            <div class="media-body">
                <?php \yii\widgets\Pjax::begin(['enablePushState' => false]);?>
                <?php
                $modelEzformReply = new \backend\modules\ezforms\models\EzformReply();
                $form = Activeform::begin(['id'=>$modelEzformReply->formName(), 'action' => ['/inputdata/ezf-reply'], 'options' => ['enctype' => 'multipart/form-data']]); ?>
                <?= $form->field($modelEzformReply, 'ezf_comment')->textarea(['rows' => 5, ]) ?>
                <?= $form->field($model, 'file_upload[]')->widget(\kartik\widgets\FileInput::classname(),[
                    'pluginOptions' => [
                        'allowedFileExtensions'=>['pdf','png','jpg','jpeg'],
                        'showRemove' => false,
                        'showUpload' => false,
                    ],
                    'options' => ['multiple' => true]
                ])->label('File Upload'); ?>
                <?= $form->field($modelEzformReply, 'ezf_id')->hiddenInput(['value'=>$model->oldAttributes['ezf_id']])->label(false) ?>
                <?= $form->field($modelEzformReply, 'data_id')->hiddenInput(['value'=>$model->oldAttributes['data_id']])->label(false) ?>
                <?php $items = [
                    '1' => 'แสดงความคิดเห็น',
                    '2' => 'ขอรับคำปรึกษา',
                    '3' => 'ขอคำปรึกษาด่วน (SOS)',
                ]; ?>
                <div class="row">
                    <div class="col-md-3 col-md-offset-9">
                        <?php
                        echo $form->field($modelEzformReply, 'type')->dropDownList($items, ['id' => 'ezf-reply-type', 'style'=>''])->label('ประเภทคำถาม');
                        ?>

                        <?= Html::submitButton('Comment', ['id' => 'btn-query', 'class' => 'btn btn-primary btn-lg']) ?>
                    </div>
                </div>
                <?php ActiveForm::end(); ?>
                <?php \yii\widgets\Pjax::end();?>
            </div>
        </div>

        <?php echo \backend\controllers\InputdataController::renderEzfReplyList($model->oldAttributes['ezf_id'], $model->oldAttributes['data_id']); ?>
    </div>
</div>


