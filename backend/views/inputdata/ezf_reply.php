<?php
/* @var $this yii\web\View */
?>
<?= \yii\widgets\ListView::widget([
    'dataProvider' => $modelReplyData,
    'itemOptions' => ['class' => 'ezform-reply-item'],
    'options' => [
        'tag' => 'div',
        'class' => 'list-wrapper',
        'id' => 'list-wrapper',
    ],
    //'layout' => "{pager}\n{items}\n{summary}",
    'itemView' => '_ezf_reply_list_view',
    'pager' => [
        'firstPageLabel' => 'first',
        'lastPageLabel' => 'last',
        'nextPageLabel' => 'next',
        'prevPageLabel' => 'previous',
        'maxButtonCount' => 3,
    ],
]);
?>