<?php
/**
 * Created by PhpStorm.
 * User: Kongvut Sangkla
 * Date: 5/4/2559
 * Time: 11:59
 * E-mail: kongvut@gmail.com
 */
?>
<h4>Query log</h4>
<hr>
<?= \yii\widgets\ListView::widget([
    'dataProvider' => $modelQueryLog,
    'itemOptions' => ['class' => 'ezform-reply-item'],
    'options' => [
        'tag' => 'div',
        'class' => 'list-wrapper',
        'id' => 'list-wrapper',
    ],
    //'layout' => "{pager}\n{items}\n{summary}",
    'itemView' => '_query_log_list',
    'pager' => [
        'firstPageLabel' => 'first',
        'lastPageLabel' => 'last',
        'nextPageLabel' => 'next',
        'prevPageLabel' => 'previous',
        'maxButtonCount' => 3,
    ],
]);
?>
