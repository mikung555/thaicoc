<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$this->title = Yii::t('', 'Data Monitor Tools');

$url_condition="/verification/datasummarycca01advance/?orderby=".$_vget['orderby'];
if( 0 ){
    echo "<pre align='left'>";
    print_r($ugpriv);
    echo "</pre>";
}
?>
<script type="text/javascript">
    function selectCriteriaMoreThan(obj)
    {
        window.open(obj.options[obj.selectedIndex].value,'_self');
    }
</script>
<div class="container">
    <h2>สรุปการนำเข้าข้อมูล (Data Monitor Tools)</h2>
  <table width="100%" class="table">
      <tr>
          <td width="50%">
              <p>
                  CCA-01: ลงทะเบียนและลงข้อมูลพื้นฐาน (คน)
                    <select class="btn btn-primary btn-large" id="selectCriteria" onchange="selectCriteriaMoreThan(this)">
                        <option value="<?php echo $url_condition; ?>">ตามจำนวนที่นำเข้าทั้งหมด</option>
                        <option value="<?php echo $url_condition; ?>&criteriamoreth=1" <?php if($_vget['criteriamoreth']=='1') { echo "selected"; } ?>>อย่างน้อย อายุตั้งแต่ 40 ปีขึ้นไป <?php if($_vget['criteriamoreth']=='1') { echo "และสามารถ + เพิ่มเงื่อนไขได้ตามที่เลือก"; } ?></option>
                        <option value="<?php echo $url_condition; ?>&criteriamoreth=2" <?php if($_vget['criteriamoreth']=='2') { echo "selected"; } ?>>ผ่านเกณฑ์การคัดเข้าอย่างน้อย 2 อย่าง</option>
                        <option value="<?php echo $url_condition; ?>&criteriamoreth=3" <?php if($_vget['criteriamoreth']=='3') { echo "selected"; } ?>>ผ่านเกณฑ์การคัดเข้าอย่างน้อย 3 อย่าง</option>
                        <option value="<?php echo $url_condition; ?>&criteriamoreth=4" <?php if($_vget['criteriamoreth']=='4') { echo "selected"; } ?>>ผ่านเกณฑ์การคัดเข้าอย่างน้อย 4 อย่าง</option>
                    </select>
              </p>
          </td>
          <td width="50%"><p>CCA-02: บันทึกผลอัลตราซาวด์ (ครั้ง)</p></td>
      </tr>
  </table>
  
  <table width="100%" class="table">
      <tr>
          <td width="50%">
            <table class="table table-striped">
              <thead>
                <tr class="success">
                  <th>ลำดับ</th>
                  <th><a href="/verification/datasummary/">จำนวน</a></th>
                  <th>อำเภอ</th>
                  <th><a href="/verification/datasummary/?orderby=province">จังหวัด</a></th>
                  <th>เขต</th>
                  <th>หมายเหตุ</th>
                </tr>
              </thead>
              <tbody>
<?php
    $irow=1;
    if( count($sumCCA01Amphur)>0 ){
        foreach($sumCCA01Amphur as $k => $v){
            // KEY->amphur
            $amphurkey=$sumCCA01Amphur[$k]['provincecode'].$sumCCA01Amphur[$k]['amphurcode'];
            if( $ugpriv['usergroup']['administrator']==true /* Admin กลาง */ 
                    || $ugpriv['usergroup']['fprovince']==true /* จังหวัด */ ){
                $txtAmpName="<a href=\"/verification/datasummaryamphur/?province=".$sumCCA01Amphur[$k]['provincecode']."&amphur=".$sumCCA01Amphur[$k]['amphurcode']."\" class=\"text-success\">".$hospitalOfAmp['amphur'][$amphurkey]['amphur']."</a>";
            }else{
                $txtAmpName=$hospitalOfAmp['amphur'][$amphurkey]['amphur'];
            }
?>
                <tr>
                  <td><?php echo $irow; ?></td>
                  <td><?php echo $sumCCA01Amphur[$k]['recs']; ?></td>
                  <td><?php echo $txtAmpName; ?></td>
                  <td><?php echo $hospitalOfAmp['amphur'][$amphurkey]['province']; //$sumAmphur[$k]['provincecode']; ?></td>
                  <td><?php echo $sumCCA01Amphur[$k]['zone_code']; ?></td>
                  <td class="text-danger"><?php echo $sumCCA01Amphur[$k]['xxxx']; 
            if( strlen($setupUS['amphur'][$amphurkey]['amphurcode'])>0 ){
                echo "ได้รับเครื่องแล้ว";
            }
                  ?></td>
                </tr>
<?php
            $irow++;
        }
    }
?>
                </tbody>
              </table>
          </td>
          <td width="50%">
            <table class="table table-striped">
              <thead>
                <tr class="success">
                  <th>ลำดับ</th>
                  <th><a href="/verification/datasummary/">จำนวน</a></th>
                  <th>อำเภอ</th>
                  <th><a href="/verification/datasummary/?orderby=province">จังหวัด</a></th>
                  <th>เขต</th>
                  <th>หมายเหตุ</th>
                </tr>
              </thead>
              <tbody>
<?php
    $irow=1;
    if( count($sumCCA02Amphur)>0 ){
        foreach($sumCCA02Amphur as $k => $v){
            $amphurkey=$sumCCA02Amphur[$k]['provincecode'].$sumCCA02Amphur[$k]['amphurcode'];
            $amptxt=$hospitalOfAmp['amphur'][$amphurkey]['amphur'];
            if(strlen($amptxt)==0){
                $amptxt="Province:".$sumCCA02Amphur[$k]['provincecode']." Amphur:".$sumCCA02Amphur[$k]['amphurcode'];
            }
?>
                <tr>
                  <td><?php echo $irow; ?></td>
                  <td><?php echo $sumCCA02Amphur[$k]['recs']; ?></td>
                  <td><a href="/verification/datasummaryamphur/?province=<?php echo $sumCCA02Amphur[$k]['provincecode'];?>&amphur=<?php echo $sumCCA02Amphur[$k]['amphurcode'];?>" class="text-success"><i class="icon-leaf icon-white"></i><?php echo $amptxt; ?></a></td>
                  <td><?php echo $hospitalOfAmp['amphur'][$amphurkey]['province']; //$sumAmphur[$k]['provincecode']; ?></td>
                  <td><?php echo $sumCCA02Amphur[$k]['zone_code']; ?></td>
                  <td class="text-danger"><?php echo $sumCCA01Amphur[$k]['xxxx']; 
            if( strlen($setupUS['amphur'][$amphurkey]['amphurcode'])>0 ){
                echo "ได้รับเครื่องแล้ว";
            }
                  ?></td>
                </tr>
<?php
            $irow++;
        }
    }
?>
                </tbody>
              </table>
          </td>
      </tr>
  </table>
</div>
<?php
//echo "Data Summary";
if( 0 ){
    echo "<pre align='left'>";
    print_r($sumAmphur);
    echo "</pre>";
}