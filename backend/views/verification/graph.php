<?php
use yii\helpers\Html;
use yii\helpers\Url;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 * URL: for test
 *  http://backend.yii2-starter-kit.dev/verification/screening
 */
//echo "Screening page";
//echo $z;
//print_r($result);
if( 0 ){
    echo "<pre align='left'>";
    //print_r($_COOKIE);
    //print_r($_SESSION);
    //print_r($_SERVER);
    print_r($userdet);
    print_r($userid);
    print_r($userprof);
    //print_r($puser_login);
    //echo "\n1435745159010041100";
    echo "</pre>";
}

if( 0 ){

}
?>
<table class="table">
    <thead>
        <tr>
            <th colspan="2">CASCAP Report</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td class="left" colspan="2">
                <?php
                    if( 0 ){
                        echo $phpsessionid;
                        echo $valuetime;
                        echo "<br />".$userdet[0]["username"];
                        echo "<br />".$userdet[0]["auth_key"];
                    }
                ?>
            </td>
        </tr>
        <tr>
            <td class="left" colspan="2">
                <span class="glyphicon glyphicon-cog"></span>
<?php
    $url="http://tools.cascap.in.th/v4cascap/version01/console/cloud/auth.php?dir=cascaptools&id=#username#&auth=#auth#";
    $url=str_replace("#username#",$userdet[0]["username"],$url);
    $url=str_replace("#auth#",$_COOKIE["PHPSESSID"].$userdet[0]["auth_key"],$url);

?>
                <a href="<?php echo $url; ?>" target="_blank">เข้าสู่ระบบ CASCAP Tools</a>
            </td>
        </tr>
        <tr>
            <td class="left" colspan="2">
                <span class="glyphicon glyphicon-cog"></span>
                รายงานผลอัลตราซาวด์
            </td>
        </tr>
        <tr>
            <td></td>
            <td class="left">
                <span class="glyphicon glyphicon-cog"></span>
<?php
    $url="http://tools.cascap.in.th/v4cascap/version01/console/cloud/auth.php?dir=worklist&hcode=".$userprof[0]["sitecode"]."&id=#username#&auth=#auth#";
    $url=str_replace("#username#",$userdet[0]["username"],$url);
    $url=str_replace("#auth#",$_COOKIE["PHPSESSID"].$userdet[0]["auth_key"],$url);

?>
                <a href="<?php echo $url; ?>" target="_blank">เครื่องมือ Export work list สำหรับเครื่องอัลตราซาวด์</a>
            </td>
        </tr>
        <tr>
            <td width="2%"></td>
            <td width="98%" class="left">
                <span class="glyphicon glyphicon-list-alt"></span>
<?php
    $url="http://www.cascap.in.th/v4cascap/version01/console/cloud/auth.php?dir=usfinding&id=#username#&auth=#auth#";
//    $url=str_replace("#username#",$userdet[0]["username"],$url);
//    $url=str_replace("#auth#",$_COOKIE["PHPSESSID"].$userdet[0]["auth_key"],$url);
    $url = \yii\helpers\Url::home()."/usfinding";
?>
                <a href="<?php echo $url; ?>" target="_blank">US Finding: เข้าสู่ระบบรายงานผลการตรวจอัลตราซาวด์</a>
            </td>
        </tr>
        <tr>
            <td></td>
            <td class="left">
                <span class="glyphicon glyphicon-list-alt"></span>
<?php
    $url="http://tools.cascap.in.th/v4cascap/version01/console/cloud/auth.php?dir=usfindingall&id=#username#&auth=#auth#";
    $url=str_replace("#username#",$userdet[0]["username"],$url);
    $url=str_replace("#auth#",$_COOKIE["PHPSESSID"].$userdet[0]["auth_key"],$url);

?>
                <a href="<?php echo $url; ?>" target="_blank">US Finding (all of ultrasound screening) show diagram</a>
            </td>
        </tr>
<?php
    if($userprof[0]["sitecode"]=="13777" || $userprof[0]["sitecode"]=="10666" || $userprof[0]["sitecode"]=="10667" || $userprof[0]["sitecode"]=="10668" || $userprof[0]["sitecode"]=="10669" || $userprof[0]["sitecode"]=="10670" || $userprof[0]["sitecode"]=="10671" || $userprof[0]["sitecode"]=="10708" || $userprof[0]["sitecode"]=="10710" || $userprof[0]["sitecode"]=="12276" || $userprof[0]["sitecode"]=="14201"){
?>
        <tr>
            <td class="left" colspan="2">
                <span class="glyphicon glyphicon-cog"></span>
                รายงานการนำเข้าข้อมูล เครื่องมือสำหรับหน่วยบริการ ที่รับรักษา
            </td>
        </tr>
        <tr>
            <td></td>
            <td class="left">
                <span class="glyphicon glyphicon-cog"></span>
<?php
    $url="http://tools.cascap.in.th/v4cascap/version01/console/cloud/auth.php?dir=toolsls2&hcode=".$userprof[0]["sitecode"]."&id=#username#&auth=#auth#";
    $url=str_replace("#username#",$userdet[0]["username"],$url);
    $url=str_replace("#auth#",$_COOKIE["PHPSESSID"].$userdet[0]["auth_key"],$url);

?>
                <a href="<?php echo $url; ?>" target="_blank">สรุปข้อมูลที่นำเข้า โดยจำแนกตามความสมบูรณ์ของข้อมูล</a>
            </td>
        </tr>
<?php
    }
?>
        <tr>
            <td colspan="2" height="30"></td>
        </tr>
    </tbody>
</table>
<div id="report-backend" class="col-md-12">
    <?php
        $loadIconData = '\'<i class="fa fa-spinner fa-spin fa-3x" style="margin:50px 0;"></i>\'';
        //$debug = false;
        //$debug = true;
        if($debug || Yii::$app->keyStorage->get('frontend.domain')=='cascap.in.th') {
            $currentYear = intval(date('Y'));

            $maxFiscalyear = (int)(intval(date('m')) < 10 ? $currentYear : $currentYear + 1);
            $fiscalYearList = array();
            for ($y = 2013; $y <= $maxFiscalyear; ++$y) {
                $fiscalYearList[$y] = $y + 543;
            }
            ?>
            <div class="row" id="cca01-cca02-select-control" style="text-align: left; ">
                <div class="col-md-12 panel panel-primary">
                    <h3 style="text-align:center;">กราฟแสดงจำนวนกลุ่มเสี่ยงที่ได้รับการลงทะเบียน
                        และจำนวนครั้งการตรวจอัลตร้าซาวด์</h3>
                    <h3 style="margin-left: 50px;">เลือกช่วงเวลา</h3>
                    <div class="form-inline" style="margin-left: 100px;">
                        <input type="radio" name="cca01-cca02-select-radio" id="cca01-cca02-radio-all" value="all"
                               checked>
                        <label for="cca01-cca02-radio-all">ทั้งหมด</label>
                        <br/><br/>
                        <input type="radio" name="cca01-cca02-select-radio" id="cca01-cca02-radio-day" value="day">
                        <label for="cca01-cca02-radio-day">ตามช่วงเวลา</label> เริ่ม
                        <input
                            type="date"
                            value="2013-02-09"
                            id="cca01-cca02-select-fromdate"
                            class="form-control"
                            min="2013-02-09"
                            max="<?= date('Y-m-d') ?>"
                        />
                        ถึง
                        <input
                            type="date"
                            value="<?= date('Y-m-d') ?>"
                            id="cca01-cca02-select-todate"
                            class="form-control"
                            min="2013-02-09"
                            max="<?= date('Y-m-d') ?>"
                        />

                        <br/><br/>
                        <input type="radio" name="cca01-cca02-select-radio" id="cca01-cca02-radio-year" value="year">
                        <label for="cca01-cca02-radio-year">ตามปีงบประมาณ</label>
                        <?=
                        Html::dropDownList(
                            'cca01-cca02-select-fiscalyear',
                            $maxFiscalyear,
                            $fiscalYearList,
                            [
                                'id' => 'ccaFiscalYear',
                                'class' => 'form-control'
                            ]
                        )
                        ?>
                        <br/><br/>
                        <button class="btn btn-info form-control" id="submit-cca01-cca02-select">แสดงข้อมูล</button>
                    </div>

                    <br/>
                    <div class="row" id="cca01-cca02-select"></div>
                    <br/>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12 panel panel-primary">
                    <h3 style="text-align:center;">กราฟแสดงจำนวนกลุ่มเสี่ยงที่ได้รับการลงทะเบียน
                        และจำนวนครั้งการตรวจอัลตร้าซาวด์<br/>จำแนกตามเขตบริการสุขภาพ</h3>
                    <div id="cca01-cca02-zone"></div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 panel panel-primary">
                    <h3 style="text-align:center;">กราฟแสดงจำนวนกลุ่มเสี่ยงที่ได้รับการลงทะเบียน
                        และจำนวนครั้งการตรวจอัลตร้าซาวด์<br/>จำแนกตามจังหวัด</h3>
                    <div id="cca01-cca02-province"></div>
                </div>
            </div>
            <?php
            $this->registerJs('
                initCca01Cca02();

                $("#submit-cca01-cca02-select").click(function(){

                    cca01Cca01Selected = true;
                    selectCca01Cca02();

                });
            ');

            $this->registerJs('

                var cca01Cca01Selected = false;

                function selectCca01Cca02(){

                    var selectCcaFormData = {};

                    if( cca01Cca01Selected && !( $("[name=\'cca01-cca02-select-radio\']:checked").val()=="all" ) ){

                        var fd = $("#cca01-cca02-select-fromdate").val();
                        var td = $("#cca01-cca02-select-todate").val();

                        if( new Date(fd).getTime() < new Date("2013-02-09").getTime() ){
                            fd = "2013-02-09";
                        }else if( new Date(fd).getTime() > new Date().getTime() ){
                            fd = getTodayFormat(new Date().getTime())
                        }

                        if( new Date(td).getTime() < new Date("2013-02-09").getTime() ){
                            td = "2013-02-09";
                        }else if( new Date(td).getTime() > new Date().getTime() ){
                            td = getTodayFormat(new Date().getTime())
                        }

                        if( new Date(td).getTime() <= new Date(fd).getTime() ){
                            var t = fd;
                            fd = td;
                            td = t;
                        }

                        $("#cca01-cca02-select-fromdate").val(fd);
                        $("#cca01-cca02-select-todate").val(td);


                        var filterType = $("[name=\'cca01-cca02-select-radio\']:checked").val();


                        selectCcaFormData["filterType"] = filterType;

                        if(filterType == "year"){
                            selectCcaFormData["filterRange"] = $("#ccaFiscalYear").val();
                        }else if(filterType == "day"){
                            selectCcaFormData["filterRange"] = fd+"_"+td;
                        }
                    }

                    $("#cca01-cca02-select").html(' . $loadIconData . ');

                    $("#submit-cca01-cca02-select").attr("disabled",true);
                    $("[name=\'cca01-cca02-select-radio\']").attr("disabled",true);
                    $("#cca01-cca02-select-fromdate,#cca01-cca02-select-todate,#ccaFiscalYear").attr("disabled",true);



                    $.ajax({
                        type    : "GET",
                        cache   : false,
                        url     : "' . Url::to('/../timeline-event/cca01-cca02-select') . '",
                        data    : selectCcaFormData,
                        success  : function(response) {
                            $("#cca01-cca02-select").html(response);
                            $("#submit-cca01-cca02-select").removeAttr("disabled");
                            $("[name=\'cca01-cca02-select-radio\']").removeAttr("disabled");
                            $("#cca01-cca02-select-fromdate,#cca01-cca02-select-todate,#ccaFiscalYear").removeAttr("disabled");
                        },
                        error : function(){
                            $("#cca01-cca02-select").html("แสดงข้อมูลผิดพลาด");
                            $("#submit-cca01-cca02-select").removeAttr("disabled");
                            $("[name=\'cca01-cca02-select-radio\']").removeAttr("disabled");
                            $("#cca01-cca02-select-fromdate,#cca01-cca02-select-todate,#ccaFiscalYear").removeAttr("disabled");
                        }
                    });
                }

                function initCca01Cca02(){

                    $("#cca01-cca02-select,#cca01-cca02-zone,#cca01-cca02-province").html(' . $loadIconData . ');

                    selectCca01Cca02();

                    $.ajax({
                        type    : "GET",
                        cache   : false,
                        url     : "' . Url::to('/../timeline-event/cca01-cca02-zone') . '",
                        data    : {
                            all : "all"
                        },
                        success  : function(response) {
                            $("#cca01-cca02-zone").html(response);
                        },
                        error : function(){
                            $("#cca01-cca02-zone").html("แสดงข้อมูลผิดพลาด");
                        }
                    });

                    $.ajax({
                        type    : "GET",
                        cache   : false,
                        url     : "' . Url::to('/../timeline-event/cca01-cca02-province') . '",
                        data    : {
                            all : "all"
                        },
                        success  : function(response) {
                            $("#cca01-cca02-province").html(response);
                        },
                        error : function(){
                            $("#cca01-cca02-province").html("แสดงข้อมูลผิดพลาด");
                        }
                    });

                    /*
                    $.ajax({
                        type    : "GET",
                        cache   : false,
                        url     : "' . Url::to('/../timeline-event/overview-report') . '",
                        data    : {
                            all : "all"
                        },
                        success  : function(response) {
                            $("#cca01-cca02-overview-report").html(response);
                        },
                        error : function(){
                            $("#cca01-cca02-overview-report").html("แสดงข้อมูลผิดพลาด");
                        }
                    });
                    */
                }

            ', 1);


            $this->registerCss('
                div[id^="cca01-cca02"]
                {
                    text-align: center;
                }

                #cca01-cca02-overview-report table{
                    background-color : #FFF;
                    text-align : left;
                }

                .tmp-section
                {
                    background-color:#eee !important;
                }

                .section-text
                {
                    text-align: left;
                    margin-left: 3%;
                }
            ');
        }
    ?>
</div>
