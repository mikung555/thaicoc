<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$amphurkey=$provincecode.$amphurcode;
?>
<div class="container">
    <h2>
        Data Monitor Tools
    </h2>
    <h2>
        สรุปการนำเข้าข้อมูล 
        อำเภอ: <?php echo $hospitalOfAmp['amphur'][$amphurkey]['amphur']; ?>
        จังหวัด: <?php echo $hospitalOfAmp['amphur'][$amphurkey]['province']; ?>
        <!-- เมื่อ --><?php //echo date('Y-m-d H:i:s') ?>
    </h2>
    <p>
        <a href="/verification/datasummary/" class="btn btn-primary btn-sm"><< ย้อนกลับไปแสดงทั้งหมด</a>
    </p>
  <table width="100%" class="table">
      <tr>
          <td width="50%"><p>CCA-01: ลงทะเบียนและลงข้อมูลพื้นฐาน (คน)</p></td>
          <td width="50%"><p>CCA-02: บันทึกผลอัลตราซาวด์ (ครั้ง)</p></td>
      </tr>
  </table>
  
  <table width="100%" class="table">
      <tr>
          <td width="50%">
            <table class="table table-striped">
              <thead>
                <tr class="success">
                    <th>ลำดับ</th>
                    <th>จำนวน</th>
                    <th>โรงพยาบาล</th>
                    <th>หมายเหตุ</th>
                </tr>
              </thead>
              <tbody>
<?php
    $irow=1;
    if( count($sumCCA01Hospital)>0 ){
        foreach($sumCCA01Hospital as $k => $v){
            $amphurkey=$sumCCA01Hospital[$k]['provincecode'].$sumCCA01Hospital[$k]['amphurcode'];
?>
                <tr>
                  <td><?php echo $irow; ?></td>
                  <td><?php echo $sumCCA01Hospital[$k]['recs']; ?></td>
                  <td><?php echo $hospitalOfAmp['hospital'][$sumCCA01Hospital[$k]['hsitecode']]['name']; ?></td>
                  <td class="text-danger"><?php  
            if( strlen($setupUS['hospital'][$sumCCA01Hospital[$k]['hsitecode']]['name'])>0 ){
                echo "ได้รับเครื่องแล้ว";
            }
                  ?></td>
                </tr>
<?php
            $irow++;
        }
    }
?>
                </tbody>
              </table>
          </td>
          <td width="50%">
            <table class="table table-striped">
              <thead>
                <tr class="success">
                    <th>ลำดับ</th>
                    <th>จำนวน</th>
                    <th>โรงพยาบาล</th>
                    <th>หมายเหตุ</th>
                </tr>
              </thead>
              <tbody>
<?php
    $irow=1;
    if( count($sumCCA02Hospital)>0 ){
        foreach($sumCCA02Hospital as $k => $v){
            $amphurkey=$sumCCA02Hospital[$k]['provincecode'].$sumCCA02Hospital[$k]['amphurcode'];
            $amptxt=$hospitalOfAmp['amphur'][$amphurkey]['amphur'];
            if(strlen($amptxt)==0){
                $amptxt="Province:".$sumCCA02Amphur[$k]['provincecode']." Amphur:".$sumCCA02Amphur[$k]['amphurcode'];
            }
?>
                <tr>
                  <td><?php echo $irow; ?></td>
                  <td><?php echo $sumCCA02Hospital[$k]['recs']; ?></td>
                  <td><?php echo $hospitalOfAmp['hospital'][$sumCCA02Hospital[$k]['hsitecode']]['name']; ?></td>
                  <td class="text-danger"><?php  
            if( strlen($setupUS['hospital'][$sumCCA02Hospital[$k]['hsitecode']]['name'])>0 ){
                echo "ได้รับเครื่องแล้ว";
            }
                  ?></td>
                </tr>
<?php
            $irow++;
        }
    }
?>
                </tbody>
              </table>
          </td>
      </tr>
  </table>
</div>
<?php
//echo "Data Summary";
if( 0 ){
    echo "<pre align='left'>";
    print_r($setupUS);
    echo "</pre>";
}