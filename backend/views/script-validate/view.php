<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\ScriptValidate */

$this->title = 'Script Validate#'.$model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Script Validates'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="script-validate-view">

    <div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	<h4 class="modal-title" id="itemModalLabel"><?= Html::encode($this->title) ?></h4>
    </div>
    <div class="modal-body">
        <?= DetailView::widget([
	    'model' => $model,
	    'attributes' => [
		'sid',
		'title',
		'name',
		'description:ntext',
		'sql:ntext',
		'sql_log:ntext',
		'return',
		'template:ntext',
		'process:ntext',
		'rating',
		'enable',
	    ],
	]) ?>
    </div>
</div>
