<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\helpers\Url;
use appxq\sdii\widgets\GridView;
use appxq\sdii\helpers\SDNoty;
use appxq\sdii\helpers\SDHtml;
use appxq\sdii\widgets\ModalForm;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\ScriptValidateSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Script Results');
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="script-results-index">

  
    <?php  // echo $this->render('_search', ['model' => $searchModel]); ?>
    <div class="btn-group" role="group" >
	<a href="<?=  Url::to(['script-validate/index'])?>" class="btn btn-default">Create Rule</a>
	<a href="<?=  Url::to(['script-validate/group'])?>" class="btn btn-success">Results</a>
    </div>
   

    <?php  Pjax::begin(['id'=>'script-results-grid-pjax']);?>
    <?= GridView::widget([
    'id' => 'script-results-grid',
	'panel'=>false,
    'panelBtn' => Html::button(SDHtml::getBtnAdd(), ['data-url'=>Url::to(['script-results/create']), 'class' => 'btn btn-success btn-sm', 'id'=>'modal-addbtn-script-results']). ' ' .
              Html::button(SDHtml::getBtnDelete(), ['data-url'=>Url::to(['script-results/deletes']), 'class' => 'btn btn-danger btn-sm', 'id'=>'modal-delbtn-script-results', 'disabled'=>true]),
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
        'columns' => [
        
        [
        'class' => 'yii\grid\SerialColumn',
        'headerOptions' => ['style'=>'text-align: center;'],
        'contentOptions' => ['style'=>'min-width:80px;text-align: center;'],
        ],
	    [
		'attribute'=>'result_at',
		'label'=>'Date',
		'value'=>function ($data){ 
		    return \common\lib\sdii\components\utils\SDdate::mysql2phpDateTime($data['result_at']);
		},
		'headerOptions'=>['style'=>'text-align: center;'],
		'contentOptions'=>['style'=>'min-width:130px; text-align: center;'],	
	    ],
	   
			[
		'attribute'=>'title',
		'contentOptions'=>['style'=>'min-width:350px; '],
			    ],
			[
		'attribute'=>'description',
		'contentOptions'=>['style'=>'min-width:200px; '],
			    ],
			[
		'attribute'=>'name',
		'contentOptions'=>['style'=>'min-width:200px; '],
			    ],
	    [
		'attribute'=>'rating',
		'value'=>function ($data){ 
		    $rating = $data['rating'];
		    $str = '';
		    for($i=1;$i<=5;$i++){
			if($rating>=$i){
			    $str .= '<i class="glyphicon glyphicon-star"></i> ';
			} else {
			    $str .= '<i class="glyphicon glyphicon-star-empty"></i> ';
			}
		    }
		    return $str;
		},
		'format' => 'raw',	
		'headerOptions'=>['style'=>'text-align: center;'],
		'contentOptions'=>['style'=>'min-width:120px; text-align: center;'],	
	    ],
//		[
//		'attribute'=>'result',
//		'format' => 'raw',
//			    ],	
		[
		'class' => 'appxq\sdii\widgets\ActionColumn',
		'contentOptions' => ['style'=>'width:80px;text-align: center;'],
		'template' => '{logdb}',
		'buttons' => [
                    'logdb' => function ($url, $model) {
			
			return Html::a('<span class="glyphicon glyphicon-search"></span>', $url, [
			    'title' => Yii::t('app', 'result'),
			    'data-action' => 'logdb',
			    'data-pjax' => 0,
			]);
                    },
                ],
		'contentOptions' => ['style' => 'width:60px;']	    
	    ],	
            // 'rating',
            // 'result_at',
        ],
    ]); ?>
    <?php  Pjax::end();?>

</div>

<?=  ModalForm::widget([
    'id' => 'modal-script-validate',
    'size'=>'modal-lg',
]);
?>

<?php  $this->registerJs("


$('#script-results-grid-pjax').on('click', 'tbody tr td a', function() {
    var url = $(this).attr('href');
    var action = $(this).attr('data-action');

    if(action === 'update' || action === 'view' || action === 'logdb') {
	modalScriptValidate(url);
    } else if(action === 'delete') {
	yii.confirm('".Yii::t('app', 'Are you sure you want to delete this item?')."', function() {
	    $.post(
		url
	    ).done(function(result) {
		if(result.status == 'success') {
		    ". SDNoty::show('result.message', 'result.status') ."
		    $.pjax.reload({container:'#script-validate-grid-pjax'});
		} else {
		    ". SDNoty::show('result.message', 'result.status') ."
		}
	    }).fail(function() {
		". SDNoty::show("'" . SDHtml::getMsgError() . "Server Error'", '"error"') ."
		console.log('server error');
	    });
	});
    }
    return false;
});

function modalScriptValidate(url) {
    $('#modal-script-validate .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
    $('#modal-script-validate').modal('show')
    .find('.modal-content')
    .load(url);
}

");?>