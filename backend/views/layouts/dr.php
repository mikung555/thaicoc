<?php
use kartik\tabs\TabsX;
/**
 * @var $this yii\web\View
 */
        $active[$this->context->activetab]=true;
        
        $items = [
            [
                'label'=>'<i class="fa fa-home"></i> หน้าแรก',
                'active'=>$active['home'],
                'linkOptions'=>['data-url'=>\yii\helpers\Url::to(['/dr/default/redirect?tab='])]                           
            ],
            [
                'label'=>'<i class="fa fa-check-square-o"></i> กำกับงาน',
                'active'=>$active['inv'],
                'linkOptions'=>['data-url'=>\yii\helpers\Url::to(['/dr/default/redirect?tab=inv'])]                
            ],
            [
                'label'=>'<i class="fa fa-male"></i> EMR',
                'active'=>$active['emr'],
                'linkOptions'=>['data-url'=>\yii\helpers\Url::to(['/dr/default/redirect?tab=emr'])]
            ],
            [
                'label'=>'<i class="fa fa-area-chart"></i> Report',
                'active'=>$active['report'],
                'linkOptions'=>['data-url'=>\yii\helpers\Url::to(['/dr/default/redirect?tab=report'])]
            ],
            
            
        ];        
        $tab = Yii::$app->request->get('tab');
?>
<?php $this->beginContent('@backend/views/layouts/common.php'); ?>
    <div class="box">
        <div class="box-body">
            <?php
            echo TabsX::widget([
                'id'=>'menu',
                'items'=>$items,
                'position'=>TabsX::POS_ABOVE,
                'encodeLabels'=>false
            ]);
            ?>            
            <?php echo $content ?>
        </div>
    </div>
<?php $this->endContent(); ?>