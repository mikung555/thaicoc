<?php
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

/* @var $this \yii\web\View */
/* @var $content string */
$this->registerCss('
@media (min-width: 768px){
    .navbar-header  {
        width: 350px;
    }
}
@media (min-width: 992px){
    .navbar-header  {
        width: 350px;
    }
}
@media (max-width: 1193px){
    .navbar-header {
        float: none;
        width: 100%;
    }
    .navbar-toggle {
        display: block;
    }
    .navbar-collapse {
        border-top: 1px solid transparent;
        box-shadow: inset 0 1px 0 rgba(255,255,255,0.1);
    }
    .navbar-collapse.collapse {
        display: none!important;
    }
    /* since 3.1.0 */
    .navbar-collapse.collapse.in { 
        display: block!important;
    }
    .collapsing {
        overflow: hidden!important;
    }
}
');

\backend\assets\BackendAsset::register($this);

$this->params['body-class'] = array_key_exists('body-class', $this->params) ?
    $this->params['body-class']
    : null;

?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?php echo Yii::$app->language ?>">
<head>
    <meta charset="<?php echo Yii::$app->charset ?>">
<meta name="viewport" content="initial-scale=1, maximum-scale=1">
    <?php echo Html::csrfMetaTags() ?>
    <title><?php echo Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <?php 
    $ga = Yii::$app->keyStorage->get('google-analytics');
    if(isset($ga) && $ga!='0'):?>
    <script>
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	ga('create', '<?=$ga?>', 'auto');
	ga('send', 'pageview');

      </script>
      <?php endif;?>
      
</head>
<?php echo Html::beginTag('body', [
    'class' => implode(' ', [
        ArrayHelper::getValue($this->params, 'body-class'),
        Yii::$app->keyStorage->get('backend.theme-skin', 'skin-blue'),
        Yii::$app->keyStorage->get('backend.layout-fixed') ? 'fixed' : null,
        Yii::$app->keyStorage->get('backend.layout-boxed') ? 'layout-boxed' : null,
        Yii::$app->keyStorage->get('backend.layout-collapsed-sidebar') ? 'sidebar-collapse' : null,
    ]),
    
])?>

    <?php
    if( isset($_SESSION['__id']) ){
        $path = 'sign-in/profile';
        $siteCode = Yii::$app->user->identity->userProfile->sitecode;
        if( (is_null($siteCode) || $siteCode=='' || $siteCode=='00000') ){
            if( $_SERVER['REQUEST_URI']!=('/'.$path.'') ){
                \backend\controllers\SiteController::actionRedirect($path);
                Yii::$app->end();
            }
        }
    }
    ?>

    <?php $this->beginBody() ?>
        <?php echo $content ?>
    <?php $this->endBody() ?>
<?php echo Html::endTag('body') ?>
</html>
<?php $this->endPage() ?>