<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use backend\modules\ckd\controllers\ReportController;
use yii\helpers\Url;
use appxq\sdii\widgets\ModalForm;
use yii\widgets\LinkPager;
use yii\widgets\ActiveForm;
use appxq\sdii\helpers\SDNoty;
use appxq\sdii\helpers\SDHtml;
use appxq\sdii\utils\ToDate;
?>

<div class="modal-content" >
    <?php
    // \appxq\sdii\utils\VarDumper::dump($dataCurrent);    
    if ($sitename != null) {
        $name = "หน่วยงาน " . $sitename;
    }

    if ($begin_date != null || $endDate != null) {
        $parseStartDate = ToDate::ToThaiDate($begin_date);
        $parsEndeDate = ToDate::ToThaiDate($endDate);
        $date .= "วันที่ " . $parseStartDate['d'].' '.$parseStartDate['m'].' '.$parseStartDate['y'] . " ถึง " . $parsEndeDate['d'].' '.$parsEndeDate['m'].' '.$parsEndeDate['y'];
    }
    ?>


    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h3 class="modal-title"><?= $name ?></h3>
        <h4 class="modal-title"><?= $date ?></h4>
    </div>
    <div class="modal-body"> 
        <div class="row">
            <div class="col-md-12 list-grid" id="list-grid-pjax">

                <?php
                echo GridView::widget([
                    'dataProvider' => $dataForms,
                    'id' => 'forms-report',
                    'panel' => [
                        'type' => \Yii::$app->request->get('action') ? Gridview::TYPE_SUCCESS : Gridview::TYPE_SUCCESS,
                    ],
                    'columns' => [
//                            [
//                            'class' => 'yii\grid\SerialColumn',
//                            'headerOptions' => ['style' => 'text-align: center;'],
//                            'contentOptions' => ['style' => 'width:5px;text-align: center;'],
//                        ],
                            [
                            'header' => 'ชื่อฟอร์ม',
                            'attribute' => 'form_name',
                            'headerOptions' => ['style' => 'text-align: center;'],
                            'contentOptions' => ['style' => 'width:300px;text-align: left;'],
                        ],
                            [
                            'header' => 'จำนวนข้อมูลทั้งหมด',
                            'attribute' => 'form_all',
                            'value' => function ($model) {
                                return number_format($model['form_all']);
                            },
                            'headerOptions' => ['style' => 'text-align: center;'],
                            'contentOptions' => ['style' => 'width:30px;text-align: center;'],
                        ],
                            [
                            'header' => 'จำนวนข้อมูลปีนี้',
                            'attribute' => 'form_year',
                            'value' => function ($model) {
                                return number_format($model['form_year']);
                            },
                            'headerOptions' => ['style' => 'text-align: center;'],
                            'contentOptions' => ['style' => 'width:30px;text-align: center;'],
                        ],
                            [
                            'header' => 'จำนวนข้อมูลเดือนนี้',
                            'attribute' => 'form_month',
                            'value' => function ($model) {
                                return number_format($model['form_month']);
                            },
                            'headerOptions' => ['style' => 'text-align: center;'],
                            'contentOptions' => ['style' => 'width:30px;text-align: center;'],
                        ], [
                            'header' => 'จำนวนข้อมูลสัปดาห์นี้',
                            'attribute' => 'form_week',
                            'value' => function ($model) {
                                return number_format($model['form_week']);
                            },
                            'headerOptions' => ['style' => 'text-align: center;'],
                            'contentOptions' => ['style' => 'width:40px;text-align: center;'],
                        ],
                            [
                            'header' => 'จำนวนข้อมูลวันนี้',
                            'attribute' => 'form_date',
                            'value' => function ($model) {
                                return number_format($model['form_date']);
                            },
                            'headerOptions' => ['style' => 'text-align: center;'],
                            'contentOptions' => ['style' => 'width:40px;text-align: center;'],
                        ],
                    ],
                    'pjax' => false
                ]);
                ?>

            </div> 
        </div>
    </div>

</div>

<?php ?>
