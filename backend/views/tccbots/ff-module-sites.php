<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use backend\modules\ckd\controllers\ReportController;
use yii\helpers\Url;
use appxq\sdii\widgets\ModalForm;
use yii\widgets\LinkPager;
use yii\widgets\ActiveForm;
use appxq\sdii\helpers\SDNoty;
use appxq\sdii\helpers\SDHtml;
?>

<div class="modal-content" >
    <?php // \appxq\sdii\utils\VarDumper::dump($dataCurrent);     ?>


    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h3 class="modal-title"><?= $mod_name ?></h3>
    </div>
    <div class="modal-body"> 
        <div class="row">
            <div class="col-md-12 list-grid" id="list-grid-pjax">

                <?php
                echo GridView::widget([
                    'dataProvider' => $dataSites,
                    'id'=>'sites-report',
                    'panel' => [
                        'type' => \Yii::$app->request->get('action') ? Gridview::TYPE_SUCCESS : Gridview::TYPE_SUCCESS,
                    ],
                    'columns' => [
//                            [
//                            'class' => 'yii\grid\SerialColumn',
//                            'headerOptions' => ['style' => 'text-align: center;'],
//                            'contentOptions' => ['style' => 'width:5px;text-align: center;'],
//                        ],
                            [
                            'header' => 'ชื่อหน่วยงาน',
                            'attribute' => 'site_name',
                            'headerOptions' => ['style' => 'text-align: center;'],
                            'contentOptions' => ['style' => 'width:300px;text-align: left;'],
                        ],
                            [
                            'header' => 'จำนวนผู้ใช้',
                            'attribute' => 'form_user',
                            'value' => function ($model) {
                                return number_format($model['form_user']);
                            },
                            'headerOptions' => ['style' => 'text-align: center;'],
                            'contentOptions' => ['style' => 'width:30px;text-align: center;'],
                        ],
                            [
                            'header' => 'จำนวนผู้ป่วย',
                            'attribute' => 'form_all',
                            'value' => function ($model) {
                                return number_format($model['form_all']);
                            },
                            'headerOptions' => ['style' => 'text-align: center;'],
                            'contentOptions' => ['style' => 'width:30px;text-align: center;'],
                        ],
                            [
                            'header' => 'จำนวนผู้ป่วยในโมดูลนี้',
                            'attribute' => 'form_patient',
                            'value' => function ($model) {
                                return number_format($model['form_patient']);
                            },
                            'headerOptions' => ['style' => 'text-align: center;'],
                            'contentOptions' => ['style' => 'width:30px;text-align: center;'],
                        ], [
                            'header' => 'จำนวนครั้ง',
                            'attribute' => 'form_amt',
                            'value' => function ($model) {
                                return number_format($model['form_amt']);
                            },
                            'headerOptions' => ['style' => 'text-align: center;'],
                            'contentOptions' => ['style' => 'width:40px;text-align: center;'],
                        ],
                    ],
                    'pjax' => false
                ]);
                ?>

            </div> 
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
    </div>

</div>

<?php ?>

<a href="../../../backend/views/tccbots/ff-modules-tab.php"></a>