<?php
use Yii;
use yii\helpers\Url;
use common\lib\sdii\components\helpers\SDNoty;

$public = isset($_GET['public'])?$_GET['public']:0;

 

?>
<?= \yii\bootstrap\Nav::widget([
    'items' => [
	[
	    'label' => 'My Modules',
	    'url' => Url::to(['/tccbots/my-module']),
	    'active'=>$public==0,
	],
        [
	    'label' => 'To me',
	    'url' => Url::to(['/tccbots/my-module', 'public'=>2]),
	    'active'=>$public==2,
	],
	[
	    'label' => 'Public Modules',
	    'url' => Url::to(['/tccbots/my-module', 'public'=>1]),
	    'active'=>$public==1,
	],
        [
	    'label' => 'Restircted',
	    'url' => Url::to(['/tccbots/my-module', 'public'=>3]),
	    'active'=>$public==3,
	],
        [
	    'label' => 'Fluke Free',
	    'url' => Url::to(['/tccbots/my-module', 'public'=>4]),
	    'active'=>$public==4,
            'visible'=>(Yii::$app->keyStorage->get('frontend.domain')=='cascap.in.th'),
	],
    ],
    'options' => ['class'=>'nav nav-tabs'],
]);


?>
<div id="main-app">
<?php if($public==0):?>
<div class="modal-header" style="margin-bottom: 15px;">
    <h3 class="modal-title" id="itemModalLabel">My Modules <?=  \yii\helpers\Html::a('<i class="glyphicon glyphicon-plus"></i> นำเข้าจาก Public Modules', ['/tccbots/my-module', 'public'=>1], ['class'=>'btn btn-link btn-sm'])?></h3>
</div>
<div class="modal-body">
    <div class="row">
	<?php
	$userId = Yii::$app->user->id;
	$dataFav = backend\modules\inv\classes\InvQuery::getModuleListFavorite($userId);
	$arrayMap = \yii\helpers\ArrayHelper::map($dataFav, 'gid', 'user_id');
	$arrayFav = array_keys($arrayMap);
	?>
	    
	<?php

	$dataModules = backend\modules\inv\classes\InvQuery::getModuleList($userId);
       // appxq\sdii\utils\VarDumper::dump($dataModules);
	if($dataModules){
	?>
	<?php foreach ($dataModules as $key => $value):?>
	<div class="col-xs-3 col-md-2" style="margin-bottom: 20px;">
		 <div class="media-left">
		    <?php
			$linkModule = '';
			if($value['gtype']==1){
//			    $pos = strpos($value['glink'], 'http');
//			    if ($pos === false) {
//				$linkModule = \yii\helpers\Url::to($value['glink']);
//			    } else {
//				$linkModule = $value['glink'];
//			    }
			    $linkModule = \yii\helpers\Url::to($value['glink']);
                        } elseif($value['gtype']==2){
                            $linkModule = \yii\helpers\Url::to(['/inv/inv-map/index', 'module'=>$value['gid']]);
                        } else {
			    $linkModule = \yii\helpers\Url::to(['/inv/inv-person/index', 'module'=>$value['gid']]);
			}
			
			$gname = yii\helpers\Html::encode($value['gname']);
			$checkthai = \backend\modules\ovcca\classes\OvccaFunc::checkthai($gname);
			$len = 12;
			if($checkthai!=''){
			    $len = $len*3;
			}
			if(strlen($gname)>$len){
			    $gname = substr($gname, 0, $len).'...';
			}
		    ?>
		    <a href="<?=$linkModule?>">
			<div style="margin: 4px;"><img src="<?= (isset($value['gicon']) && $value['gicon'] != '')? Yii::getAlias('@backendUrl').'/module_icon/'.$value['gicon']:Yii::getAlias('@backendUrl').'/module_icon/no_icon.png'?>" class="img-rounded" width="72" height="72"></div>
		    </a>
		    <h4 class="media-heading text-center" style="font-size: 13px;"><strong><?=  $gname?></strong> <a style="cursor: pointer;" data-url="<?=Url::to(['/inv/inv-person/info-app', 'gid'=>$value['gid']])?>" class="info-app"><i class="glyphicon glyphicon-info-sign"></i></a></h4>
		</div>
	</div>
	<?php endforeach;?>
	<?php } else {
	    if(count($arrayFav)==0){
		echo '<div class="col-md-4"><cite>ไม่พบโมดูล</cite></div>';
	    }
	}
	?>
    </div>
</div>


<div class="modal-header" style="margin-bottom: 15px;">
    <h3 class="modal-title" id="itemModalLabel">System Modules</h3>
</div>
<div class="modal-body">
    <div class="row">

	<?php

	$dataModules = backend\modules\inv\classes\InvQuery::getModuleListSys();

	if($dataModules){
	?>
	<?php foreach ($dataModules as $key => $value):?>
	<div class="col-xs-3 col-md-2" style="margin-bottom: 20px;">
		<div class="media-left">
		    <?php
			$linkModule = '';
			if($value['gtype']==1){
			    $linkModule = \yii\helpers\Url::to($value['glink']);
			} elseif($value['gtype']==2){
                            $linkModule = \yii\helpers\Url::to(['/inv/inv-map/index', 'module'=>$value['gid']]);
                        } else {
			    $linkModule = \yii\helpers\Url::to(['/inv/inv-person/index', 'module'=>$value['gid']]);
			}
			
			$gname = yii\helpers\Html::encode($value['gname']);
			$checkthai = \backend\modules\ovcca\classes\OvccaFunc::checkthai($gname);
			$len = 12;
			if($checkthai!=''){
			    $len = $len*3;
			}
			if(strlen($gname)>$len){
			    $gname = substr($gname, 0, $len).'...';
			}
		    ?>
		    <a href="<?=$linkModule?>">
			<div style="margin: 4px;"><img src="<?= (isset($value['gicon']) && $value['gicon'] != '')? Yii::getAlias('@backendUrl').'/module_icon/'.$value['gicon']:Yii::getAlias('@backendUrl').'/module_icon/no_icon.png'?>" class="img-rounded" width="72" height="72"></div>
		    </a>
		    <h4 class="media-heading text-center" style="font-size: 13px;"><strong><?= $gname?></strong> <a style="cursor: pointer;" data-url="<?=Url::to(['/inv/inv-person/info-app', 'gid'=>$value['gid']])?>" class="info-app"><i class="glyphicon glyphicon-info-sign"></i></a></h4>
		</div>
		
	    </div>
	<?php endforeach;?>
	<?php } 
	?>
	
    </div>
</div>
<?php elseif($public==2):?>

<div class="modal-body">
    <div class="row">
	<?php
	$userId = Yii::$app->user->id;
	
//	$dataFav = backend\modules\inv\classes\InvQuery::getModuleListFavorite($userId);
//	$arrayMap = \yii\helpers\ArrayHelper::map($dataFav, 'gid', 'user_id');
//	$arrayFav = array_keys($arrayMap);

	?>
	
	<?php

	$dataModules = backend\modules\inv\classes\InvQuery::getModuleAssign($userId);
        
	//appxq\sdii\utils\VarDumper::dump($dataModules);
	if($dataModules){
	    
	?>
	<?php foreach ($dataModules as $key => $value):?>
	<div class="col-xs-3 col-md-2" style="margin-bottom: 20px;">
		<div class="media-left">
		    <?php
			$linkModule = '';
			if($value['gtype']==1){
//			    $pos = strpos($value['glink'], 'http');
//			    if ($pos === false) {
//				$linkModule = \yii\helpers\Url::to($value['glink']);
//			    } else {
//				$linkModule = $value['glink'];
//			    }
			    $linkModule = \yii\helpers\Url::to($value['glink']);
			} elseif($value['gtype']==2){
                            $linkModule = \yii\helpers\Url::to(['/inv/inv-map/index', 'module'=>$value['gid']]);
                        } else {
			    $linkModule = \yii\helpers\Url::to(['/inv/inv-person/index', 'module'=>$value['gid']]);
			}
			
			$gname = yii\helpers\Html::encode($value['gname']);
			$checkthai = \backend\modules\ovcca\classes\OvccaFunc::checkthai($gname);
			$len = 12;
			if($checkthai!=''){
			    $len = $len*3;
			}
			if(strlen($gname)>$len){
			    $gname = substr($gname, 0, $len).'...';
			}
		    ?>
                    
		   <a href="<?=$linkModule?>">
			<div style="margin: 4px;"><img src="<?= (isset($value['gicon']) && $value['gicon'] != '')? Yii::getAlias('@backendUrl').'/module_icon/'.$value['gicon']:Yii::getAlias('@backendUrl').'/module_icon/no_icon.png'?>" class="img-rounded" width="72" height="72"></div>
		    </a>
		    <h4 class="media-heading text-center" style="font-size: 13px;"><strong><?=  $gname?></strong> <a style="cursor: pointer;" data-url="<?=Url::to(['/inv/inv-person/info-app', 'gid'=>$value['gid']])?>" class="info-app"><i class="glyphicon glyphicon-info-sign"></i></a></h4>
		</div>
		
	    </div>
	<?php endforeach;?>
	<?php } 
	?>
	
    </div>
</div>
    <?php elseif($public==3):?>
<div class="modal-body">
    <div class="row">
	<?php
	$userId = Yii::$app->user->id;
	
//	$dataFav = backend\modules\inv\classes\InvQuery::getModuleListFavorite($userId);
//	$arrayMap = \yii\helpers\ArrayHelper::map($dataFav, 'gid', 'user_id');
//	$arrayFav = array_keys($arrayMap);

	?>
	
	<?php

	$dataModules = \backend\modules\inv\classes\InvQuery::getModulePrivate();
	//appxq\sdii\utils\VarDumper::dump($dataModules);
	if($dataModules){
	    
	?>
	<?php foreach ($dataModules as $key => $value):?>
	<div class="col-xs-3 col-md-2" style="margin-bottom: 20px;">
		<div class="media-left">
		    <?php
			$linkModule = '';
			if($value['gtype']==1){
//			    $pos = strpos($value['glink'], 'http');
//			    if ($pos === false) {
//				$linkModule = \yii\helpers\Url::to($value['glink']);
//			    } else {
//				$linkModule = $value['glink'];
//			    }
			    $linkModule = \yii\helpers\Url::to($value['glink']);
			} elseif($value['gtype']==2){
                            $linkModule = \yii\helpers\Url::to(['/inv/inv-map/index', 'module'=>$value['gid']]);
                        } else {
			    $linkModule = \yii\helpers\Url::to(['/inv/inv-person/index', 'module'=>$value['gid']]);
			}
			
			$gname = yii\helpers\Html::encode($value['gname']);
			$checkthai = \backend\modules\ovcca\classes\OvccaFunc::checkthai($gname);
			$len = 12;
			if($checkthai!=''){
			    $len = $len*3;
			}
			if(strlen($gname)>$len){
			    $gname = substr($gname, 0, $len).'...';
			}
                        $private = $value['public'] == 0?1:0;
		    ?>
                    
		    <a style="cursor: pointer;" data-url="<?=Url::to(['/inv/inv-person/info-app', 'gid'=>$value['gid'], 'private'=>$private])?>" class="info-app">
			<div style="margin: 4px;"><img src="<?= (isset($value['gicon']) && $value['gicon'] != '')? Yii::getAlias('@backendUrl').'/module_icon/'.$value['gicon']:Yii::getAlias('@backendUrl').'/module_icon/no_icon.png'?>" class="img-rounded" width="72" height="72"></div>
		    </a>
		    <h4 class="media-heading text-center" style="font-size: 13px;"><strong><?=  $gname?></strong> </h4>
		</div>
		
	    </div>
	<?php endforeach;?>
	<?php } 
	?>
	
    </div>
</div>
    <?php 
    
    elseif($public==4 && (Yii::$app->keyStorage->get('frontend.domain')=='cascap.in.th' || Yii::$app->keyStorage->get('frontend.domain')=='backend.dpmcloud.dev') ):?>
    
<div class="modal-body">
    <div class="row">
	<?php
	$userId = Yii::$app->user->id;
	
//	$dataFav = backend\modules\inv\classes\InvQuery::getModuleListFavorite($userId);
//	$arrayMap = \yii\helpers\ArrayHelper::map($dataFav, 'gid', 'user_id');
//	$arrayFav = array_keys($arrayMap);

	?>
	
	<?php
        $inStr = '1498130302024554300,1497859920056436400,1496902325091526900
                ,1498622167035527500,1484721973020193700,1496678490037398000
                ,1496678203080761900,29,1498457589040399800,1498463555010552900,9911';
        //  /dpmcloud_online/backend/views/tccbots/my-module.php
	$dataModules = backend\modules\inv\classes\InvQuery::getModuleInStr($inStr);
	//appxq\sdii\utils\VarDumper::dump($dataModules);
	if($dataModules){
	    
	?>
	<?php foreach ($dataModules as $key => $value):?>
	<div class="col-xs-3 col-md-2" style="margin-bottom: 20px;">
		 <div class="media-left">
		    <?php
			$linkModule = '';
			if($value['gtype']==1){
//			    $pos = strpos($value['glink'], 'http');
//			    if ($pos === false) {
//				$linkModule = \yii\helpers\Url::to($value['glink']);
//			    } else {
//				$linkModule = $value['glink'];
//			    }
			    $linkModule = \yii\helpers\Url::to($value['glink']);
                        } elseif($value['gtype']==2){
                            $linkModule = \yii\helpers\Url::to(['/inv/inv-map/index', 'module'=>$value['gid']]);
                        } else {
			    $linkModule = \yii\helpers\Url::to(['/inv/inv-person/index', 'module'=>$value['gid']]);
			}
			
			$gname = yii\helpers\Html::encode($value['gname']);
			$checkthai = \backend\modules\ovcca\classes\OvccaFunc::checkthai($gname);
			$len = 12;
			if($checkthai!=''){
			    $len = $len*3;
			}
			if(strlen($gname)>$len){
			    $gname = substr($gname, 0, $len).'...';
			}
		    ?>
		    <a href="<?=$linkModule?>">
			<div style="margin: 4px;"><img src="<?= (isset($value['gicon']) && $value['gicon'] != '')? Yii::getAlias('@backendUrl').'/module_icon/'.$value['gicon']:Yii::getAlias('@backendUrl').'/module_icon/no_icon.png'?>" class="img-rounded" width="72" height="72"></div>
		    </a>
		    <h4 class="media-heading text-center" style="font-size: 13px;"><strong><?=  $gname?></strong> <a style="cursor: pointer;" data-url="<?=Url::to(['/inv/inv-person/info-app', 'gid'=>$value['gid']])?>" class="info-app"><i class="glyphicon glyphicon-info-sign"></i></a></h4>
		</div>
	</div>
	<?php endforeach;?>
	<?php } 
	?>

    </div>
    <div class='row'>
        
              <div id="fishImage">
    
</div>
    </div>
 
</div>

    <?php else:?>

<div class="modal-body">
    <div class="row">
	<?php
	$userId = Yii::$app->user->id;
	
//	$dataFav = backend\modules\inv\classes\InvQuery::getModuleListFavorite($userId);
//	$arrayMap = \yii\helpers\ArrayHelper::map($dataFav, 'gid', 'user_id');
//	$arrayFav = array_keys($arrayMap);

	?>
	
	<?php

	$dataModules = backend\modules\inv\classes\InvQuery::getModulePublic($userId);
	//appxq\sdii\utils\VarDumper::dump($dataModules);
	if($dataModules){
	    
	?>
	<?php foreach ($dataModules as $key => $value):?>
	<div class="col-xs-3 col-md-2" style="margin-bottom: 20px;">
		<div class="media-left">
		    <?php
			$linkModule = '';
			if($value['gtype']==1){
//			    $pos = strpos($value['glink'], 'http');
//			    if ($pos === false) {
//				$linkModule = \yii\helpers\Url::to($value['glink']);
//			    } else {
//				$linkModule = $value['glink'];
//			    }
			    $linkModule = \yii\helpers\Url::to($value['glink']);
			} elseif($value['gtype']==2){
                            $linkModule = \yii\helpers\Url::to(['/inv/inv-map/index', 'module'=>$value['gid']]);
                        } else {
			    $linkModule = \yii\helpers\Url::to(['/inv/inv-person/index', 'module'=>$value['gid']]);
			}
			
			$gname = yii\helpers\Html::encode($value['gname']);
			$checkthai = \backend\modules\ovcca\classes\OvccaFunc::checkthai($gname);
			$len = 12;
			if($checkthai!=''){
			    $len = $len*3;
			}
			if(strlen($gname)>$len){
			    $gname = substr($gname, 0, $len).'...';
			}
		    ?>
		    <a style="cursor: pointer;" data-url="<?=Url::to(['/inv/inv-person/info-app', 'gid'=>$value['gid']])?>"  class="info-app">
			<div style="margin: 4px;"><img src="<?= (isset($value['gicon']) && $value['gicon'] != '')? Yii::getAlias('@backendUrl').'/module_icon/'.$value['gicon']:Yii::getAlias('@backendUrl').'/module_icon/no_icon.png'?>" class="img-rounded" width="72" height="72"></div>
		    </a>
		    <h4 class="media-heading text-center" style="font-size: 13px;"><strong><?=  $gname?></strong> </h4>
		</div>
		
	    </div>
	<?php endforeach;?>
	<?php } 
	?>

    </div>
</div>
<?php endif;?>
</div>

<?=    \appxq\sdii\widgets\ModalForm::widget([
    'id' => 'modal-app',
    //'size'=>'modal-lg',
]);
?>

<?php  $this->registerJs("
$('#main-app').on('click', '.info-app', function() {
    modalApp($(this).attr('data-url'),$(this).attr('id'));
});    

function modalApp(url,directUrl) {
    $('#modal-app .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
    $('#modal-app').modal('show')
    .find('.modal-content')
    .load(url);
}
");?>

<?php
$this->registerJs("

       $.ajax({
  method: 'get',
  url: 'https://cloudbackend.cascap.in.th/interactiveimage'
})
  .done(function( msg ) {
      $('#fishImage').html(msg);
  });
" );
?>
