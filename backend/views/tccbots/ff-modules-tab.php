<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use backend\modules\ckd\controllers\ReportController;
use yii\helpers\Url;
use appxq\sdii\widgets\ModalForm;
?>
<div class="col-md-12">
    <?php
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'id' => 'modules-report',
        'panel' => [
            'before' => Html::label('<h4><b> จำนวนผู้รับบริการรวม <span class="label label-warning"> ' . number_format($dataTotal['mod_patient_total']) . '</span> ราย </b></h4>', '', ['class' => 'pull-left']),
            'type' => \Yii::$app->request->get('action') ? Gridview::TYPE_SUCCESS : Gridview::TYPE_SUCCESS,
            'footer' => false
        ],
        'showPageSummary' => true,
        'columns' => [
                [
                'header' => 'ชื่อโมดูล',
                'attribute' => 'mod_name',
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::a($model['mod_name'], "javascript:void(0)", [
                                'id' => 'modal-forms-report',
                                'name' => $model['mod_name'],
                                'onclick' => "showModuleReport('" . $model['mod_id'] . "')",
                                'style' => 'text-align:justify'
                    ]);
                },
                'pageSummary' => 'รวมทั้งหมด',
                'headerOptions' => ['style' => 'text-align: center;'],
                'contentOptions' => ['style' => 'width:25%;text-align: left;'],
            ],
                [
                'header' => 'จำนวนหน่วยงาน',
                'attribute' => 'mod_site',
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::a(number_format($model['mod_site']), "javascript:void(0)", [
                                'id' => 'modal-forms-report',
                                'name' => $model['mod_name'],
                                'onclick' => "modalGuideField('" . Url::to([
                                    'sites-report',
                                    'module_id' => $model['mod_id'],
                                    'module_name' => $model['mod_name'],
                                ]) . "')",
                                'style' => 'text-align:justify'
                    ]);
                },
                'pageSummary' => number_format($dataTotal['mod_site_total']),
                'headerOptions' => ['style' => 'text-align: center;'],
                'contentOptions' => ['style' => 'width:15%;text-align: right;'],
                'pageSummaryOptions' => ['style' => 'width:15%;text-align: right;'],
            ],
                [
                'header' => 'จำนวนผู้ใช้',
                'attribute' => 'mod_user',
                'format' => ['decimal', 0],
                'value' => function ($model) {
                    return $model['mod_user'];
                },
                'pageSummary' => number_format($dataTotal['mod_user_total']),
                'headerOptions' => ['style' => 'text-align: center;'],
                'contentOptions' => ['style' => 'width:15%;text-align: right;'],
                'pageSummaryOptions' => ['style' => 'width:15%;text-align: right;'],
            ], [
                'header' => 'จำนวนฟอร์ม',
                'attribute' => 'mod_form',
                'format' => ['decimal', 0],
                'value' => function ($model) {
                    return $model['mod_form'];
                },
                'pageSummary' => number_format($dataTotal['mod_form_total']),
                'headerOptions' => ['style' => 'text-align: center;'],
                'contentOptions' => ['style' => 'width:15%;text-align: right;'],
                'pageSummaryOptions' => ['style' => 'width:15%;text-align: right;'],
            ],
                [
                'header' => 'จำนวนผู้รับบริการในโมดูลนี้',
                'attribute' => 'mod_patient',
                'format' => ['decimal', 0],
                'value' => function ($model) {
                    return $model['mod_patient'];
                },
                'pageSummary' => number_format($dataTotal['mod_patient_total']),
                'headerOptions' => ['style' => 'text-align: center;'],
                'contentOptions' => ['style' => 'width:15%;text-align: right;'],
                'pageSummaryOptions' => ['style' => 'width:15%;text-align: right;'],
            ], [
                'header' => 'จำนวนครั้งที่ให้บริการ',
                'attribute' => 'mod_ntime',
                'format' => ['decimal', 0],
                'value' => function ($model) {
                    return $model['mod_ntime'];
                },
                'pageSummary' => number_format($dataTotal['mod_ntime_total']),
                'headerOptions' => ['style' => 'text-align: center;'],
                'contentOptions' => ['style' => 'width:15%;text-align: right;'],
                'pageSummaryOptions' => ['style' => 'width:15%;text-align: right;'],
            ]
        ],
        'pjax' => true,
    ]);
    ?>
    <!-- Modal Report -->
    <div class="modal fade" id="modal-forms" role="dialog">
        <div class="modal-dialog" style="width:90%">

            <!-- Modal content-->
            <div class="modal-content">

            </div>

        </div>
    </div>
<?php
$this->registerJs("
    
     $('#modal-forms-report').click(function() {
        modalGuideField($(this).attr('data-url'));
    });
    

    function modalGuideField(url) {
        $('#modal-forms .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
        $('#modal-forms').modal('show')
        .find('.modal-content')
        .load(url);
    }    
    function showModuleReport(mod_id){
            $('#tab-one').removeClass('active');
            $('#tab-two').removeClass('active');
            $('#tab-three').addClass('active');
            
            $('#output-tab').html('<center><i class=\"fa fa-spinner fa-pulse fa-3x fa-fw\"></i></center>');
             $.ajax({
                url:'" . Url::to(['show-module-report']) . "',
                method:'POST',
                data: {
                    module_id:mod_id
                },
                dataType:'HTML',
                success:function(result){
                   //console.log(result);
                   $('#output-tab').html(result); 
                },error: function (xhr, ajaxOptions, thrownError) {
                   console.log(xhr);
                }
            });
            return false;
        }
    ");
?>
</div>
