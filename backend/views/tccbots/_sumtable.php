<?php
    $sql = "SELECT SUM(`npatient`) FROM `buffe_patient_count` WHERE `sitecode`=:sitecode";
    $sumcount_patient=0;       
    $count_table =0;
    $fields=0;
    $rows=0;
    $items=0;
    foreach($model as $model){
       $query = \Yii::$app->db->createCommand($sql,[":sitecode"=>$model['hcodes']])->queryScalar();
       $sumcount_patient += (int)$query;
       $count_table += (int)$model['count_table'];
       $fields += (int)$model['fields'];
       $rows += (int)$model['rows'];
       $items += (int)$model['items'];
    }
    
?>
<tr style="background:#c9d7da;">
    <td></td>
    <td class="text-center"><b>ผลรวม</b></td>
    <td><b><?= number_format($sumcount_patient);?></b></td>
    <td><b><?= number_format($count_table);?></b></td>
    <td><b><?= number_format($fields);?></b></td>
    <td><b><?= number_format($rows);?></b></td>
    <td><b><?= number_format($items);?></b></td>    
</tr>