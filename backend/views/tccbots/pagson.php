<?php if ((Yii::$app->keyStorage->get('frontend.domain') == "dpmcloud.org") && ($sitecode == '93001')) { ?>
<?php 
    $domain = yii\helpers\Url::home();
?>
    <div class=" box">   
        <div class="modal-header" style="margin-bottom: 15px;">
            <h3 class="modal-title" id="itemModalLabel">ภาคศัลยศาสตร์</h3>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-xs-3 col-md-2" style="margin-bottom: 20px;">
                    <div class="media-left">
                        <a href="<?= yii\helpers\Url::to(['/dpm/menu/index2','page'=>3])?>">
                            <div style="margin: 4px;"><img src="<?= $domain?>/img/icondpm/nut001.jpg" class="img-rounded" width="72" height="72"></div>
                        </a>
                        <h4 class="media-heading text-center" style="font-size: 13px;"><strong>อาจารย์</strong></h4>
                    </div>

                </div>

                <div class="col-xs-3 col-md-2" style="margin-bottom: 20px;">
                    <div class="media-left">
                        <a href="<?= yii\helpers\Url::to(['/dpm/menu/index2','page'=>2])?>">
                            <div style="margin: 4px;"><img src="<?= $domain?>/img/icondpm/nut003.png" class="img-rounded" width="72" height="72"></div>
                        </a>
                        <h4 class="media-heading text-center" style="font-size: 13px;"><strong>บุคลากร</strong></h4>
                    </div>

                </div>

                <div class="col-xs-3 col-md-2" style="margin-bottom: 20px;">
                    <div class="media-left">
                        <a href="<?= yii\helpers\Url::to(['/dpm/menu/index2','page'=>4])?>">
                            <div style="margin: 4px;"><img src="<?= $domain?>/img/icondpm/nut002.jpg" class="img-rounded" width="72" height="72"></div>
                        </a>
                        <h4 class="media-heading text-center" style="font-size: 13px;"><strong>นักศึกษาแพทย์</strong></h4>
                    </div>

                </div>
            </div>
        </div>
    </div>
<?php
}?>