<?php

    use common\lib\sdii\components\utils\SDdate;
    use yii\widgets\LinkPager;
    
    $date = '';
?>


    
<?php  $date = '';
        if($result_timeline != NULL){       // appxq\sdii\utils\VarDumper::dump($result_timeline);
?>
    <div class="col-md-12">
        <ul class="timeline">
            <?php foreach ($result_timeline as $value) { 
                if($date != $value['date']){
                    $date = $value['date'];
            ?>
            <li class="time-label" style="text-align: left;">
                <span class="bg-green"> <?php echo SDdate::mysql2phpThDateSmall(date("d-m-Y")); ?></span>
            </li>
            <?php }
                if($value['data'] !=''){
            ?>
            <li>
                <div class="timeline-item" style="background: #CEFFCE;" >
                    <span class="times" style="float: left;padding:10px;"><i class="fa fa-clock-o"></i> <?= $value['time']; ?> | </span>
                    <div class="timeline-header" style="text-align: left;"><?= $value['data']; ?></div>
                </div>                     
            </li>
            
                <?php }} ?>
            <li>
                <i class="fa fa-clock-o">
                </i>
            </li>
        </ul>
    </div>
    <?php //echo \yii\widgets\LinkPager::widget(['pagination' => $pagination]);?>
<?php } else if($result_timeline == '' || $result_timeline == NULL){ ?>
    <div class="col-md-12">
        <ul class="timeline">
            <li class="time-label" style="text-align: left;">
                <span class="bg-green"> <?php echo date("d-m-Y"); ?></span>
            </li>
            <li>
                <div class="timeline-item" style="background: #CEFFCE;" >
                    <h3 class="timeline-header" style="text-align: left;">ไม่พบข้อมูล  </h3>
                </div>                    
            </li>
            <li>
                <i class="fa fa-clock-o">
                </i>
            </li>
        </ul>
    </div>

<?php } ?>
