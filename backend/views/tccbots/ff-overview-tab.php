
<?php

$this->registerJsFile('@web/js/excellentexport.js', ['depends' => [yii\web\JqueryAsset::className()]]);
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use yii\helpers\Html;
use yii\helpers\Url;
use common\lib\sdii\components\utils\SDdate;
use frontend\controllers\ReportController;
use frontend\controllers\OVSummary;
use yii\bootstrap\Progress;
?>

<?php

$debug = false;
//$debug = true;

$loadIconData = '\'<div class=\"sdloader \"><i class="fa fa-spinner fa-spin fa-3x" style="margin:50px 0;"></i></div>\'';
if ($debug || Yii::$app->keyStorage->get('frontend.domain') == 'cascap.in.th' || Yii::$app->keyStorage->get('frontend.domain') == 'dpmcloud.dev' || Yii::$app->keyStorage->get('frontend.domain') == 'yii2-starter-kit.dev') {
    echo '<div class="row">';
    echo '<div class="col-md-12" >';
//    echo '<h3>Fluke Free Overview</h3>';
    echo '<div class="table-responsive" id="frontend-overview-report"></div>';
    echo '</div>';
    echo '</div>';

    $this->registerJs('
            getReportOverview();
        ');

    $this->registerJs('

            function getReportOverview(){
                $("#frontend-overview-report").html(' . $loadIconData . ');

                $.ajax({
                    type    : "GET",
                    cache   : false,
                    url     : "' . yii\helpers\Url::to('ajax-report-overview') . '",
                    data    : {},
                    success :   function(response) {
                        $("#frontend-overview-report").html(response);
                    },
                    error   :   function(){
                        $("#frontend-overview-report").html("การแสดงข้อมูลผิดพลาด");
                    }
                });
            }
        ', 1);

    $currentYear = intval(date('Y'));

    $maxFiscalyear = (int) (intval(date('m')) < 10 ? $currentYear : $currentYear + 1);
    $fiscalYearList = array();
    for ($y = 2013; $y <= $maxFiscalyear; ++$y) {
        $fiscalYearList[$y] = $y + 543;
    }

    $thaiMonth = ['',
        1 => ['abvt' => 'ม.ค.', 'full' => 'มกราคม'],
        2 => ['abvt' => 'ก.พ.', 'full' => 'กุมภาพันธ์'],
        3 => ['abvt' => 'มี.ค.', 'full' => 'มีนาคม'],
        4 => ['abvt' => 'เม.ย.', 'full' => 'เมษายน'],
        5 => ['abvt' => 'พ.ค.', 'full' => 'พฤษภาคม'],
        6 => ['abvt' => 'มิ.ย.', 'full' => 'มิถุนายน'],
        7 => ['abvt' => 'ก.ค.', 'full' => 'กรกฎาคม'],
        8 => ['abvt' => 'ส.ค.', 'full' => 'สิงหาคม'],
        9 => ['abvt' => 'ก.ย.', 'full' => 'กันยายน'],
        10 => ['abvt' => 'ต.ค.', 'full' => 'ตุลาคม'],
        11 => ['abvt' => 'พ.ย.', 'full' => 'พฤศจิกายน'],
        12 => ['abvt' => 'ธ.ค.', 'full' => 'ธันวาคม']
    ];

    $reverted = false;
    for ($m = 10; $m < 13; $m++) {
        $yearText = ($m < 10 ? $currentYear : $currentYear - 1);
        $fiscalMonthList[$yearText . '_' . $m] = $thaiMonth[$m]['full'] . ' ' . (intval($yearText) + 543);
        if (!$reverted) {
            if ($m == 12) {
                $reverted = true;
                $m = 0;
            }
        } else {
            if ($m == 9) {
                break;
            }
        }
    }
    $loadIconData = '\'<div class=\"sdloader \"><i class="fa fa-spinner fa-spin fa-3x" style="margin:50px 0;"></i></div>\'';

    echo '<hr style="border: 1.5px solid #AAA;width:95%;">';
}
$this->registerCss('
            #data-report-table,
            #cca02-report-table,
            #allproof-report-table
            {
                background-color: #FFFFFF; border-radius: 5px;
                word-break: normal;
                /*transform: rotateX(180deg);*/
            }

            #data-report-table th,
            #cca02-report-table th,
            #allproof-report-table th
            {
                text-align:center !important;
                vertical-align:middle;

            }


            #data-report-table td,#data-report-table th,
            #cca02-report-table td,#cca02-report-table th,
            #allproof-report-table td,#allproof-report-table td
            {
                text-align:right;
                border-left:1px solid #f4f4f4;
            }

            #data-report-div,
            #cca02-report-div,
            #allproof-report-div
            {
                overflow-x:overlay;
                /*transform: rotateX(180deg);*/
                text-align:center;
            }

            .unit-section
            {
                background-color:#000;
                color:#FFF;
            }

            .unit-separator
            {
                background-color:#555;
                color:#FFF;
                text-align:left !important;
            }

            .unit-typesplit
            {
                background-color:#AAA;
                color:#FFF;
                text-align:left !important;
            }

            .unit-name
            {
                text-align:left !important;
            }

            .unit-heading
            {
                text-align:center !important;
                vertical-align:middle !important;
                font-weight:bold;
                background-color:#f5f5f5;
            }

            .unit-sum
            {
                text-align:right !important;
                color:blue;
            }

            .unit-sum.name
            {
                text-align:left !important;
            }

            [id$="-controls"]
            {
                display:none;
            }

            [id$="refresh"]
            {
                cursor:pointer;
                color: #3c8dbc;
            }

            [id$="refresh"]:hover
            {
                color: #72afd2;
            }

            .tmp-section
            {
                background-color:#eee !important;
            }

            .section-text
            {
                text-align: left;
                margin-left: 3%;
            }

            .row
            {
                margin-left: 0 !important;
                margin-right: 0 !important;
            }

            #ov01-report-div{
                overflow-x: auto;
                text-align: center;
            }
        ');
