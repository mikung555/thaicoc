<?php
use yii\helpers\Url;
use yii\helpers\Html;


$this->title = "Monitor Reports "; 
?>
<br>

<ul class="nav nav-tabs">
    <li id="tab-zero" ><a data-toggle="tab" href="#monitor-report" id="tab0"><i class="fa fa-line-chart fa-lg"></i> Overview</a></li>
    <li id="tab-one" ><a data-toggle="tab" href="#monitor-report" id="tab1"><i class="fa fa-desktop fa-lg"></i> Monitor</a></li>
    <li id="tab-two" class="active"><a data-toggle="tab" href="#monitor-report" id="tab2"><i class="fa fa-archive fa-lg"></i> Fluke Free Modules</a></li>
    <li id="tab-three" class="disabled"><a href="#monitor-report" id="tab3" disabled="true"><i class="fa fa-bar-chart fa-lg"></i> Fluke Free Report</a></li>
</ul>
<div class="tab-content">
<div id="output-tab"></div>
</div>
<?php
    $this->registerJs("
        show_modules();
        $('#tab0').click(function(){
//            $('#tab-zero').addClass('active');
//            $('#tab-one').removeClass('active');
//            $('#tab-two').removeClass('active');
//            $('#tab-three').removeClass('active');
            window.location = '".Yii::getAlias('@frontendUrl')."/report/?tab=overview';
//            show_overview();
        });
        
        $('#tab1').click(function(){
            $('#tab-one').addClass('active');
            $('#tab-two').removeClass('active');
            $('#tab-three').removeClass('active');
            $('#tab-zero').removeClass('active');
            show_monitor();
        });

        $('#tab2').click(function(){
            $('#tab-one').removeClass('active');
            $('#tab-three').removeClass('active');
            $('#tab-two').addClass('active');
            $('#tab-zero').removeClass('active');
            show_modules();
        });

        function show_monitor(){
            $('#output-tab').html('<div class=\"sdloader \"><i class=\"fa fa-spinner fa-pulse fa-3x fa-fw\"></i></div>');
             $.ajax({
                url:'" . Url::to(['tccbots/show-monitor']) . "',
                method:'POST',
                data: 'id',
                dataType:'HTML',
                success:function(result){
                   $('#output-tab').html(result); 
                },error: function (xhr, ajaxOptions, thrownError) {
                   console.log(xhr);
                }
            });
            return false;
        }
        function show_modules(){
            $('#output-tab').html('<div class=\"sdloader \"><i class=\"fa fa-spinner fa-pulse fa-3x fa-fw\"></i></div>');
             $.ajax({
                url:'" . Url::to(['tccbots/show-modules']) . "',
                method:'POST',
                data: 'id',
                dataType:'HTML',
                success:function(result){
                   $('#output-tab').html(result); 
                },error: function (xhr, ajaxOptions, thrownError) {
                   console.log(xhr);
                }
            });
            return false;
        }
        
        function show_overview(){
            $('#output-tab').html('<div class=\"sdloader \"><i class=\"fa fa-spinner fa-pulse fa-3x fa-fw\"></i></div>');
             $.ajax({
                url:'" . Url::to(['tccbots/show-overview']) . "',
                method:'POST',
                data: 'id',
                dataType:'HTML',
                success:function(result){
                   $('#output-tab').html(result); 
                },error: function (xhr, ajaxOptions, thrownError) {
                   console.log(xhr);
                }
            });
            return false;
        }
       
    ");

    $this->registerCss('

    .tab-content {
        border-left: 1px solid #ddd;
        border-right: 1px solid #ddd;
        padding: 25px;
    }
    
    ');
?>
