<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\WidgetBackendMenu */

$this->title = 'Create Widget Backend Menu';
$this->params['breadcrumbs'][] = ['label' => 'Widget Backend Menus', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="widget-backend-menu-create">
    <?= Html::a('< Back', ['index'], ['class' => 'btn btn-success']); ?>
    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
