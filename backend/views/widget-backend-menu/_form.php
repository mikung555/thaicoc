<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\WidgetBackendMenu */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="widget-backend-menu-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'titlename')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'url')->textarea(['maxlength' => true]) ?>

    <?= $form->field($model, 'target')->dropDownList(['none' => 'None', '_blank' => 'New Tab']); ?>

    <?= $form->field($model, 'type')->dropDownList(['report' => 'รายงาน']); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
