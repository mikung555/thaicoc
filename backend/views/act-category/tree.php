<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\helpers\Url;
use common\lib\sdii\widgets\SDGridView;
use common\lib\sdii\widgets\SDModalForm;
use common\lib\sdii\components\helpers\SDNoty;
use common\lib\codeerror\helpers\GenTree;

$this->title = Yii::t('backend', 'Act Categories');
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
  .tree {
    min-height:20px;
    padding:19px;
    margin-bottom:20px;
    background-color:#fbfbfb;
    border:1px solid #999;
    -webkit-border-radius:4px;
    -moz-border-radius:4px;
    border-radius:4px;
    -webkit-box-shadow:inset 0 1px 1px rgba(0, 0, 0, 0.05);
    -moz-box-shadow:inset 0 1px 1px rgba(0, 0, 0, 0.05);
    box-shadow:inset 0 1px 1px rgba(0, 0, 0, 0.05)
  }
  .tree li {
    list-style-type:none;
    margin:0;
    padding:10px 5px 0 5px;
    position:relative
  }
  .tree li::before, .tree li::after {
    content:'';
    left:-20px;
    position:absolute;
    right:auto
  }
  .tree li::before {
    border-left:1px solid #999;
    bottom:50px;
    height:100%;
    top:0;
    width:1px
  }
  .tree li::after {
    border-top:1px solid #999;
    height:20px;
    top:29px;
    width:25px
  }
  .tree li span {
    -moz-border-radius:5px;
    -webkit-border-radius:5px;
    border:1px solid #999;
    border-radius:5px;
    display:inline-block;
    padding:3px 8px;
    text-decoration:none;
    margin-top: 5px;
  }
  .tree li.parent_li>span {
    cursor:pointer
  }
  .tree>ul>li::before, .tree>ul>li::after {
    border:0
  }
  .tree li:last-child::before {
    height:30px
  }
  .tree li.parent_li>span:hover, .tree li.parent_li>span:hover+ul li span {
    background:#eee;
    border:1px solid #94a0b4;
    color:#000
  }
  .custom-menu {
    display: none;
    z-index: 1000;
    position: absolute;
    overflow: hidden;
    border: 1px solid #CCC;
    white-space: nowrap;
    font-family: sans-serif;
    background: #FFF;
    color: #333;
    border-radius: 5px;
  }

  .custom-menu li {
    padding: 8px 12px;
    cursor: pointer;
  }

  .custom-menu li:hover {
    background-color: #DEF;
  }

</style>

<?php
/*$root = '';

foreach ($trees as $key => $tree) {*/
  //echo $tree->act_name;
  //if (0 == $tree->parent_id) {
    //$root = GenTree::getTree(0);
  //}

//}

$nav = [];

foreach ($trees as $key => $tree) {
    $child['id'] = $tree->act_id;
    $child['name'] = $tree->act_name;
    $child['parent'] = $tree->parent_id;
    array_push($nav, $child);
}

//\yii\helpers\VarDumper::dump($nav, 10, true);
//Yii::$app->end();

$navarray = GenTree::generateChildArray($nav);


?>

<div id="context-menu2">
  <ul class="dropdown-menu" role="menu">
    <li><a tabindex="-1">เพิ่มหมวด</a></li>
    <li><a tabindex="-1">เพิ่มโครงการ</a></li>
    <li><a tabindex="-1">เพิ่มฟอร์ม</a></li>
  </ul>
</div>

<div class="row">
  <div class="col-sm-5">
    <div class="tree well">
      <!-- <ul> -->
        <?= GenTree::generateChildHTML($navarray, "") ?>
        <!-- <li>
          <span class="js-tree" data-id="codeerror"><i class="fa fa-fw fa-bookmark"></i> Parent</span>
          <ul>
            <li>
              <span class="js-tree" data-id="codeerror2"><i class="icon-minus-sign"></i> Child</span>
              <ul>
                <li>
                 <span><i class="icon-leaf"></i> Grand Child</span>
               </li>
             </ul>
           </li>
           <li>
            <span><i class="icon-minus-sign"></i> Child</span>
            <ul>
              <li>
               <span><i class="icon-leaf"></i> Grand Child</span>
             </li>
             <li>
               <span><i class="icon-minus-sign"></i> Grand Child</span>
               <ul>
                <li>
                  <span><i class="icon-minus-sign"></i> Great Grand Child</span>
                  <ul>
                   <li>
                     <span><i class="icon-leaf"></i> Great great Grand Child</span>
                   </li>
                   <li>
                     <span><i class="icon-leaf"></i> Great great Grand Child</span>
                   </li>
                 </ul>
               </li>
               <li>
                <span><i class="icon-leaf"></i> Great Grand Child</span>
              </li>
              <li>
                <span><i class="icon-leaf"></i> Great Grand Child</span>
              </li>
            </ul>
          </li>
          <li>
           <span><i class="icon-leaf"></i> Grand Child</span>
         </li>
       </ul>
     </li>
   </ul>
 </li> -->
 <!-- <li>
  <span><i class="fa fa-fw fa-bookmark"></i> Parent2</span>
  <ul>
    <li>
      <span><i class="icon-leaf"></i> Child</span>
    </li>
  </ul>
</li> -->

<!-- </ul> -->
</div>
</div>
<div class="col-sm-7">
  <div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
      <li class="active"><a href="#tab_1" data-toggle="tab">จัดการหมวดหมู่</a></li>
      <li><a href="#tab_2" data-toggle="tab">รายละเอียด</a></li>
      <li><a href="#tab_3" data-toggle="tab">รายงาน</a></li>
    </ul>
    <div class="tab-content">
      <div class="tab-pane active" id="tab_1">

      </div><!-- /.tab-pane -->
      <div class="tab-pane" id="tab_2">

      </div><!-- /.tab-pane -->
      <div class="tab-pane" id="tab_3">

      </div><!-- /.tab-pane -->
    </div><!-- /.tab-content -->
  </div>
</div>
</div>

<?php  $this->registerJs("
  $('.tree li:has(ul)').addClass('parent_li').find(' > span').attr('title', '');
  $('.tree li.parent_li > span').on('click', function (e) {
    var children = $(this).parent('li.parent_li').find(' > ul > li');
    if (children.is(':visible')) {
      children.hide('fast');
      $(this).attr('title', '').find(' > i').addClass('icon-plus-sign').removeClass('icon-minus-sign');
    } else {
      children.show('fast');
      $(this).attr('title', '').find(' > i').addClass('icon-minus-sign').removeClass('icon-plus-sign');
    }
    e.stopPropagation();
  });

");

$this->registerJsFile(Yii::$app->request->baseUrl.'/js/bootstrap-contextmenu.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJs("
  $('.js-tree').contextmenu({
    target: '#context-menu2',
    onItem: function (context, e) {
      alert($(context).data('id'));
    }
  });
");

?>
