<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\helpers\Url;
use common\lib\sdii\widgets\SDGridView;
use common\lib\sdii\widgets\SDModalForm;
use common\lib\sdii\components\helpers\SDNoty;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\ActCategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'Act Categories');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="act-category-index">

    <?php  Pjax::begin(['id'=>'act-category-grid-pjax']);?>
    <?= SDGridView::widget([
	'id' => 'act-category-grid',
	'panelBtn' => Html::button(Yii::t('app', '<span class="glyphicon glyphicon-plus"></span>'), ['data-url'=>Url::to(['act-category/create', 'type'=>$type]), 'class' => 'btn btn-success btn-sm', 'id'=>'modal-addbtn-act-category']),
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'act_id',
            'act_order',
            'act_name',
            'act_detail:ntext',
            'act_type',
            // 'parent_id',
            // 'ezform_count',
            // 'status',
            // 'user_create',
            // 'date_create',
            // 'user_update',
            // 'date_update',

            [
		'class' => 'common\lib\sdii\widgets\SDActionColumn',
		'template' => '{update} {delete}',
		'buttons' => [
		    'update' => function ($url, $model, $key) {
			return Html::a('<span class="glyphicon glyphicon-pencil"></span> Edit', Url::to(['act-category/update', 'type'=>$model['act_type'], 'id'=>$model['act_id']]), [
			    'data-action' => 'update',
                'title' => Yii::t('yii', 'Update'),
                'class'=>'btn btn-warning btn-xs',
			    'title' => Yii::t('yii', 'Update'),
			]);

		    },
		]
	    ],
        ],
    ]); ?>
    <?php  Pjax::end();?>

<?=  SDModalForm::widget([
    'id' => 'modal-act-category',
    'size'=>'',
]);
?>

<?php  $this->registerJs("
$('#act-category-grid-pjax').on('click', '#modal-addbtn-act-category', function(){
modalActCategory($(this).attr('data-url'));
});

$('#act-category-grid-pjax').on('dblclick', 'tbody tr', function() {
    var id = $(this).attr('data-key');
    modalActCategory('".Url::to(['act-category/update', 'type'=>$type, 'id'=>''])."'+id);
});

$('#act-category-grid-pjax').on('click', 'tbody tr td a', function() {
    var url = $(this).attr('href');
    var action = $(this).attr('data-action');

    if(action === 'update' || action == 'view'){
	modalActCategory(url);
    } else if(action === 'delete') {
	yii.confirm('".Yii::t('app', 'Are you sure you want to delete this item?')."', function(){
	    $.post(
		url
	    ).done(function(result){
		if(result.status == 'success'){
		    ". SDNoty::show('result.message', 'result.status') ."
		    $.pjax.reload({container:'#act-category-grid-pjax'});
		} else {
		    ". SDNoty::show('result.message', 'result.status') ."
		}
	    }).fail(function(){
		console.log('server error');
	    });
	})
    }
    return false;
});

function modalActCategory(url) {
    $('#modal-act-category .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
    $('#modal-act-category').modal('show')
    .find('.modal-content')
    .load(url);
}

");?>
