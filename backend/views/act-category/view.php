<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\ActCategory */

$this->title = 'Act Category#'.$model->act_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Act Categories'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="act-category-view">

    <div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	<h4 class="modal-title" id="itemModalLabel"><?= Html::encode($this->title) ?></h4>
    </div>
    <div class="modal-body">
        <?= DetailView::widget([
	    'model' => $model,
	    'attributes' => [
		'act_id',
		'act_order',
		'act_name',
		'act_detail:ntext',
		'act_type',
		'parent_id',
		'ezform_count',
		'status',
		'user_create',
		'date_create',
		'user_update',
		'date_update',
	    ],
	]) ?>
    </div>
</div>
