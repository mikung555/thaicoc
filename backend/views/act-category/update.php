<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\ActCategory */

$this->title = Yii::t('backend', 'Update {modelClass}: ', [
    'modelClass' => 'Act Category',
]) . ' ' . $model->act_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Act Categories'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->act_id, 'url' => ['view', 'id' => $model->act_id]];
$this->params['breadcrumbs'][] = Yii::t('backend', 'Update');
?>
<div class="act-category-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
