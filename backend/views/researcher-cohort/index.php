

<?php
//\yii\helpers\VarDumper::dump($model, 10, true);
?>
<div class="text-center">
<h2><b>รายงานอบรมเชิงปฏิบัติการเพื่อวิเคราะห์และเขียนบทความวิจัย
    ภายใต้โครงการแก้ไขปัญหาโรคพยาธิใบไม้ตับและมะเร็งท่อน้ำดีในภาคตะวันออกเฉียงเหนือ (CASCAP)</b></h2><hr>
    <h3>
    <br>วันที่ 16-18 ตุลาคม 2558
    <br>ณ โรงแรมวารี วัลเล่ย์ จ.ขอนแก่น</h3>
    <hr>
</div>

<? if(!$countAddData) {?>
<div class="alert alert-danger h2">
    <strong>Error!</strong> ไม่สามารถดูรายงานได้ เนื่องจากคุณยังไม่กรอกแบบสอบถาม.
</div>
<?  } else if(!$countAddDataPass) {?>
    <div class="alert alert-danger h2">
        <strong>Error!</strong> ไม่สามารถดูรายงานได้ เนื่องจากคุณไม่ยินดีเข้าร่วมเป็นสมาชิก Researcher Cohort.
    </div>
<?php } else {?>
<table class="table table-bordered table-responsive table-hover">
    <thead class="h4" style="background-color: #424242; color: #FFF;">
    <tr>
        <th class="text-center" style="width:  5%;" rowspan="2">No</th>
        <th class="text-center" style="width:  30%;" rowspan="2">ชื่อผู้เข้าร่วมอบรม</th>
        <th class="text-center" colspan="6">Progress</th>
    </tr>
    <tr class="h5">
        <th class="text-center">Mock <br>Abstract</th>
        <th class="text-center">Mock <br>Manuscript</th>
        <th class="text-center">Data collection <br>ongoing</th>
        <th class="text-center">Manuscript <br>preparation ongoing</th>
        <th class="text-center">Submitted</th>
        <th class="text-center">Published</th>
    </tr>
    </thead>
    <tbody class="h4">
    <?php $i=1; foreach($model as $val) {?>
        <tr <?php $research = \backend\modules\article\models\Research::find()->select('rid, mabstract, mmanuscript, status')->where('created_by = :user_id', [':user_id' => $val->user_create])->andWhere('`show` = :showx', [':showx' => 1])->orderBy('updated_at')->one(); if($research->rid) { ?> class='clickable-row' data-href='<?php echo Yii::getAlias('@frontendUrl').'/article/research/view?id='.$research->rid; }?>'>
            <td class="text-center"><?php echo $i++; ?></td>
            <td><?php $user = \common\models\UserProfile::find()->select('firstname, lastname')->where('user_id = :user_id', [':user_id' => $val->user_create])->one(); echo $user->firstname.' '. $user->lastname;?></td>
            <td class="text-center"><?php if($research->mabstract) { ?><i class="fa fa-check-square-o fa-2x text-success"></i><?php } else { ?><i class="fa fa-square-o fa-2x text-danger"></i><?php }?></td>
            <td class="text-center"><?php if($research->mmanuscript) { ?><i class="fa fa-check-square-o fa-2x text-success"></i><?php } else { ?><i class="fa fa-square-o fa-2x text-danger"></i><?php }?></td>
            <td class="text-center"><?php if($research->status >= 1) { ?><i class="fa fa-check-square-o fa-2x text-success"></i><?php } else { ?><i class="fa fa-square-o fa-2x text-danger"></i><?php }?></td>
            <td class="text-center"><?php if($research->status > 1 && $research->status <=2) { ?><i class="fa fa-check-square-o fa-2x text-success"></i><?php } else { ?><i class="fa fa-square-o fa-2x text-danger"></i><?php }?></td>
            <td class="text-center"><?php if($research->status > 2 && $research->status <=3) { ?><i class="fa fa-check-square-o fa-2x text-success"></i><?php } else { ?><i class="fa fa-square-o fa-2x text-danger"></i><?php }?></td>
            <td class="text-center"><?php if($research->status > 3 && $research->status <=4) { ?><i class="fa fa-check-square-o fa-2x text-success"></i><?php } else { ?><i class="fa fa-square-o fa-2x text-danger"></i><?php }?></td>
        </tr>
    <?php } ?>
    </tbody>
</table>
<?php
    $format = <<< SCRIPT
jQuery(document).ready(function($) {
    $(".clickable-row").click(function() {
        window.open($(this).data("href"));
    });
});
SCRIPT;

    $this->registerJs($format, \yii\web\View::POS_END);
    } ?>