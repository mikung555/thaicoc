<?php
/**
 * Created by Netbeans.
 * xxxxx
 * User: NANCE
 * xxxxx
 * Date: 21/03/2017
 */
use Yii;
use appxq\sdii\widgets\GridView;
use yii\widgets\Pjax;
use kartik\helpers\Html;
use yii\helpers\Url;

?>
   
<a href="/dpm/menu/index" class="btn btn-success" role="button"><i class="fa fa-cubes" aria-hidden="true"></i> All Module</a>
<br>
<h2 style="text-align: left;"><span style="color:"#000000";"><span lang="TH">Morbidity&Motality&nbsp;</span></h2> 
<a href='<?= Url::to(['patient/index']) ?>' class="btn btn-info" role="button"> บันทึกจำนวนผู้ป่วยใน และ Abstract ของผู้ป่วย M&M </a>
<a href='<?= Url::to(['data-managementstat/index']) ?>' class="btn btn-info" role="button"> จำนวนผู้ป่วยประจำปี M&M แจกแจงตาม Admission Discharge Morbidity Maltality และ Summary </a>
<a href='<?= Url::to(['nmm-report/index']) ?>' class="btn btn-info" role="button"> จำนวนผู้ป่วยประจำเดือน แจกแจงตาม Cause of complication และ MM Grading </a>
<hr>

<h3>จำนวนผู้ป่วยประจำเดือน แจกแจงตาม Cause of complication และ MM Grading</h3>

<hr>
<h4>เลือกช่วงเวลาแสดงข้อมูล</h4>
<div class="row">
    <form id="view-data-range" action="" method="get">
        <div class="col-6 col-sm-3">
            จำนวน
            <?php
            echo kartik\widgets\Select2::widget([
                'name' => 'option_nmm',
                'data' => ['1' => 'Cause of complication', '2' => 'MM Grading'],
                'value'=> '1',
                'options' => ['placeholder' => 'เลือกจากรายการ...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
            ?>
        </div>


        <div class="col-6 col-sm-3">
            เลือกเดือน
            <?php
            echo kartik\widgets\Select2::widget([
                'name' => 'month_nmm',
                'data' => ['1' => 'มกราคม'
                    , '2' => 'กุมภาพันธ์'
                    , '3' => 'มีนาคม'
                    , '4' => 'เมษายน'
                    , '5' => 'พฤษภาคม'
                    , '6' => 'มิถุนายน'
                    , '7' => 'กรกฎาคม'
                    , '8' => 'สิงหาคม'
                    , '9' => 'กันยายน'
                    , '10' => 'ตุลาคม'
                    , '11' => 'พฤศจิกายน'
                    , '12' => 'ธันวาคม'
                    ],
                'options' => ['placeholder' => 'เดือน...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
            ?>
        </div>
        <div class="col-6 col-sm-3">
            <!--query database-->
            เลือกปี
        
        <?php 
            $yearold = date('Y',strtotime('-70 year'));
            $current_year = date('Y');
            $arryYear=array();
            for($current_year; $current_year >= $yearold; $current_year--) {
                 $arryYear["$current_year"]=$current_year;
            
            }
        
            echo kartik\widgets\Select2::widget([
                'name' => 'year_nmm',
                'data' =>  $arryYear,
                'options' => ['placeholder' => 'ปี...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
          
        ?>    
      
        </div>
        <div class="col-6 col-sm-3">
            <br>
            
            <?php
         //   echo Html::hiddenInput('ezfdata', $_GET['ezfdata']);
           
                    
            echo Html::button('<span class="fa fa-search"></span> ค้นหา', 
            [
                'class' => 'btn btn-primary  btn-md',
                'type' =>'submit',
            ]);
            ?>
        </div>
    </form>
</div>
<hr>

<?php
    if (isset($query_nmm)){
        echo \kartik\grid\GridView::widget([
            'dataProvider' => $query_nmm
            
        ]);
        
    }
   
 
?>

