<?php

/**
 * Created by PhpStorm.
 * User: hackablex
 * Date: 7/26/2016 AD
 * Time: 13:23
 */
use appxq\sdii\widgets\GridView;
use yii\widgets\Pjax;
use kartik\helpers\Html;
use yii\helpers\Url;

$this->registerJS($ezfdata['js']);

$this->registerJsFile('https://maps.google.com/maps/api/js?key=AIzaSyCq1YL-LUao2xYx3joLEoKfEkLXsEVkeuk&' . http_build_query($q), [
    'position' => \yii\web\View::POS_HEAD,
    'depends' => 'yii\web\YiiAsset',
]);
?>

<?= $ezfdata['detail']; ?><hr>
<h4>เลือกช่วงเวลาแสดงข้อมูล</h4>
<div class="row">
    <form id="view-data-range" action="" method="get">
        <?= Html::hiddenInput('show', $_GET['show']) ?>
        <div class="col-md-6">
            จาก
            <?php
            echo \common\lib\damasac\widgets\DMSDateWidget::widget([
                //'model'=>$model,
                //'id'=>'start_date',
                'value' => $_GET['start_date'] ? $_GET['start_date'] : '01' . (date('-m-') . (date('Y') + 543)),
                'name' => 'start_date',
                'options' => ['id' => 'start_date'],
            ]);
            ?>
        </div>
        <div class="col-md-6">
            ถึง
            <?php
            echo \common\lib\damasac\widgets\DMSDateWidget::widget([
                //'model'=>$model,
                //'id'=>'end_date',
                'value' => $_GET['end_date'] ? $_GET['end_date'] : (date('d-m-') . (date('Y') + 543)),
                'name' => 'end_date',
                'options' => ['id' => 'end_date'],
            ]);
            ?>
        </div>
        <div class="col-md-12 text-right">
            <br>
            <?php
           echo ' '.Html::a('<span class="fa fa-list"></span> แสดงข้อมูลทั้งหมด',
                Url::to(['/surgery-form/data-management/', 'ezfdata' => $_GET['ezfdata'], 'show'=>'all']),
                [
                    'class' => 'btn btn-danger  btn-md',
                    'type' =>'submit',
                ]);
            ?>
            <?php
            echo Html::hiddenInput('ezfdata', $_GET['ezfdata']);
            echo Html::button('<span class="fa fa-search"></span> ค้นหา', [
                'class' => 'btn btn-primary btn-md',
                'type' => 'submit',
            ]);
            ?>
        </div>
    </form>
</div>
<hr>
<?php Pjax::begin(['id' => 'inv-person-map-pjax']); ?>
<?php Pjax::end(); ?>
<div id="inv-person-grid-pjax-top">
    <?php Pjax::begin(['id' => 'inv-person-grid-pjax']); ?>
    <?php
//            appxq\sdii\utils\VarDumper::dump($dataProvider);
    if ($_GET['ezfdata'] == '1476696513076161100') {
        $total = Yii::$app->db->createCommand('
            SELECT COUNT(*) FROM `dep_calendar`
        ', [':status' => 1])->queryScalar();

        $dpFix = new \yii\data\SqlDataProvider([
            'sql' => 'SELECT * FROM `dep_calendar`',
            'totalCount' => $total,
            'sort' => [
                'attributes' => [
                    'hday', 'hmonth', 'hname'
                ],
            ],
            'pagination' => [
                'pageSize' => 50,
            ],
        ]);


        echo GridView::widget([
            'panelBtn' => '',
            'dataProvider' => $dpFix,
            'columns' => [
                    [
                    'header' => 'วันที่',
                    'attribute' => 'hday',
                ],
                    [
                    'header' => 'วันเดือน',
                    'attribute' => 'hmonth',
                ],
                    [
                    'header' => 'วันหยุด',
                    'attribute' => 'hname',
                ]
            ],
        ]);
    }
    
    if($_GET['ezfdata'] == '1490342148003178500'){
        $format="raw";
    }else{
        $format="text";
    }
    $pkJoin = 'target';
    if ($ezform['comp_id_target']) {
        $ezform_comp = Yii::$app->db->createCommand("SELECT * FROM ezform_component WHERE comp_id=:comp_id;", [':comp_id' => $ezform['comp_id_target']])->queryOne();
        if ($ezform_comp['special'] == 1) {
            $pkJoin = 'ptid';
        }
    }


    $fieldSearch = $_GET['DataManagementSearch'];
    $columns = [
            [
            'class' => 'yii\grid\SerialColumn',
            'headerOptions' => ['style' => 'text-align: center;'],
            'contentOptions' => ['style' => 'width:50px;text-align: center;'],
        ],
    ];
    foreach ($fParams as $val) {
        $columns[] = [
            'attribute' => $val,
            'format' => $format,
            'value' => function($model) use($val) {
                if ($_GET['ezfdata'] == '1476364204008401300' && in_array($val, ['hncode', 'name', 'surname'])) {
                    //tb_data_1
                    $sql = "SELECT $val FROM tb_data_1 WHERE ptid = :ptid";
                    $data = Yii::$app->db->createCommand($sql, [':ptid' => $model->ptid])->queryScalar();
                    return $data;
                }
                return $model->{$val} != '' ? $model->{$val} : '';
            },
            'filter' => count($filterType[$val]) ? (
            \kartik\widgets\Select2::widget([
                'name' => 'DataManagementSearch[' . $val . ']',
                'value' => is_null($fieldSearch[$val]) ? null : $fieldSearch[$val],
                'data' => $filterType[$val],
                'options' => ['placeholder' => 'Select for filter ...',
                            'id'=>'DataManagementSearch-ele-'.$val],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ])
            ) : true,
            //'filterOptions'=> count($filterType[$val]) ? ['style'=>'width: 50px;'] : [],
            'headerOptions' => count($filterType[$val]) ? ['style' => 'width: 50px; text-align: center;'] : ['style' => 'width: 100px; text-align: center;'],
            'contentOptions' => ['style' => 'text-align: left;', 'field-id' => $val],
        ];
    }
    $columns[] = ['class' => 'yii\grid\ActionColumn',
        'header' => 'จัดการ',
        'template' => '{view} {print} {custom-print}',
        'buttons' => [
            //view button
            'view' => function ($url, $model) use ($ezfdata, $ezform, $pkJoin) {
//                    $campData = Yii::$app->db->createCommand()->queryOne();
                return Html::a('<span class="fa fa-edit"></span>', NULL, [
                            'data-url' => Url::to(['/inv/inv-person/ezform-print',
                                'ezf_id' => $ezfdata['ezf_id'],
                                'dataid' => $model['id'],
                                'comp_target' => isset($ezform['comp_id_target']) ? '' : 'skip',
                                'target' => (isset($model[$pkJoin]) && $model[$pkJoin] != '' && $model[$pkJoin] != 'null') ? base64_encode($model[$pkJoin]) : null,
                                'comp_id_target' => isset($ezform['comp_id_target']) ? $ezform['comp_id_target'] : NULL,
                            ]),
                            'style' => 'cursor: pointer;',
                            'title' => Yii::t('app', 'View'),
                            'class' => 'btn btn-primary btn-xs print-ezform',
                ]);

//                    return Html::a('<span class="fa fa-edit"></span>', ['/inputdata/redirect-page', 'ezf_id' => $ezfdata['ezf_id'], 'dataid'=>$model->id, 'rurl' => base64_encode(Yii::$app->request->url)], [
//                        'title' => Yii::t('app', 'View'),
//                        'class'=>'btn btn-primary btn-xs',
//                        'target'=>'_blank'
//                    ]);
            },
            'print' => function ($url, $model) use ($ezfdata) {
                return Html::a('<span class="fa fa-file-pdf-o"></span>', ['/managedata/managedata/ezform-pdf', 'ezf_id' => $ezfdata['ezf_id'], 'dataid' => $model->id], [
                            'title' => Yii::t('app', 'View'),
                            'class' => 'btn btn-primary btn-xs',
                            'target' => '_blank'
                ]);
            },
            'custom-print' => function ($url, $model) use ($ezfdata) {

//            /ezformreports/ezform-custom-report/print
                return Html::a('<span class="glyphicon glyphicon-print"></span>', ['/inputdata/printform',
                            'id' => $ezfdata['ezf_id'], 'dataid' => $model->id], [
                            'title' => Yii::t('app', 'View'),
                            'class' => 'btn btn-primary btn-xs',
                            'target' => '_blank',
                            'data-pjax' => "0"
                ]);
            },
        /*
          'update' => function ($url, $model) use ($ezfdata) {
          return Html::a('<span class="fa fa-edit"></span> Edit', ['/inputdata/redirect-page', 'ezf_id' => $ezfdata['ezf_id'], 'dataid'=>$model->id, 'rurl' => base64_encode(Yii::$app->request->url)], [
          'title' => Yii::t('app', 'Update'),
          'class'=>'btn btn-warning btn-xs',
          'target'=>'_blank'
          ]);

          },
         */
        ],
        'headerOptions' => ['style' => 'text-align: center;'],
        'contentOptions' => ['style' => 'width:100px;text-align: center;']
    ];
    ?>

    <?php
    $url = '';
    if ($ezform['comp_id_target']) {
        $url = Url::to([
                    '/inputdata/step2/',
                    'comp_id_target' => $ezform['comp_id_target'],
                    'ezf_id' => $ezfdata['ezf_id']
        ]);
    } else {
        $url = Url::to([
                    '/inputdata/insert-record/',
                    'insert' => 'url',
                    'ezf_id' => $ezfdata['ezf_id'],
                    'target' => 'skip',
                    'rurl' => base64_encode(Yii::$app->request->url)]);
    }

    $btn = Html::a('<i class="glyphicon glyphicon-plus"></i> เพิ่มข้อมูล', null, [
                'data-url' => Url::to(['/inv/inv-person/ezform-print',
                    'ezf_id' => $ezfdata['ezf_id'],
                    'end' => 1,
                    'comp_target' => isset($ezform['comp_id_target']) ? '' : 'skip',
                    'comp_id_target' => isset($ezform['comp_id_target']) ? $ezform['comp_id_target'] : NULL,
                ]),
                'class' => 'btn btn-success print-ezform',
                'style' => 'cursor: pointer;',
                'data-toggle' => 'tooltip',
                'title' => 'เพิ่มข้อมูล',
    ]);

    echo GridView::widget([
        'id'=>'gridview-wg',
        'panelBtn' => $btn,
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $columns,
    ]);
    ?>


</div>
<?php Pjax::end(); ?>

<input id="preload" type="hidden" value="0"/>
<input id="emr-preload" type="hidden" value="0"/>

<?=
\appxq\sdii\widgets\ModalForm::widget([
    'id' => 'modal-print',
    'size' => 'modal-lg',
    'tabindexEnable' => false,
    'options' => ['style' => 'overflow-y:scroll;']
]);
?>

<?php
//Yii::$app->session['pjax_reload'] = 'inv-person-grid-pjax';

echo \appxq\sdii\widgets\ModalForm::widget([
    'id' => 'modal-query-request',
    'size' => 'modal-lg',
    'tabindexEnable' => false,
    //'clientOptions'=>['backdrop'=>'static'],
    'options' => ['style' => 'overflow-y:scroll;']
]);
$this->registerJs("
$('a').attr('data-pjax','0');    
$('#modal-print').on('hidden.bs.modal', function (e) {
    if($('#preload').val()>0){
        $('#preload').val(0);
//        $.pjax.reload({container:'#inv-person-grid-pjax'});
        location.reload();
    }
});

$('#modal-inv-emr').on('hidden.bs.modal', function (e) {
    if($('#emr-preload').val()>0){
        $('#emr-preload').val(0);
//        $.pjax.reload({container:'#inv-person-grid-pjax'});
        location.reload();
    }
});

$('.modal-lg').width('90%');

$('#inv-person-grid-pjax').on('click', '.print-ezform', function() {
    var url = $(this).attr('data-url');
    modalEzfrom(url);
});

function modalEzfrom(url) {
    $('#modal-print .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
    $('#modal-print').modal('show');
    $.ajax({
	method: 'POST',
	url: url,
	dataType: 'HTML',
	success: function(result, textStatus) {
	    $('#modal-print .modal-content').html(result);
	    return false;
	}
    });
}


");


if($ezfdata['ezf_id'] == '1464070383027412600'){ //ezfdata = 1475657454095297700 เวชระเบียน
    // open javascript register
    $fawesome = ' <i class="fa fa-circle" aria-hidden="true"></i> ';
$this->registerJs(" 
    markMedRegTable();
    // do function when click 
    $(document).on('click', '.pagination ul li', function(){
        markMedRegTable();
    });
    function get_row_as_array(row) {
        var data = $('td', row).map(function(index, value) {
          return $(value).text();
        });
        return data;
      }
    
    function markMedRegTable(){
    
      var row = $('#alphabet tr:eq(1)');
      var result = get_row_as_array(row);
      $('#alphabet tr:eq(2) td:eq(2)').html(result[2]);
  
    
        var tableEle = $('.table-responsive table tbody tr');
        var rownum = tableEle.length;
        for(var i = 0; i < rownum ; i ++){
            // check each row
            var row = $('.table-responsive table tbody tr:eq('+i+')');
            var result = get_row_as_array(row);
            // convert raw data to date ;
            var date1 = result[9] != '' ? new Date(result[9]) : null;
            var retdate= result[10] != '' ? new Date(result[10]) : null;
            var staffretdate = result[11] != '' ? new Date(result[11]) : null;
            var returndate = result[12] != '' ? new Date(result[12]) : null;
            
            var nowdate = new Date(); // แสดงผลผิดพลาดเพราะวันที่ยังแปลงได้ไม่ถูกต้อง
            
            // case 1 วันที่รับไว้ - ส่งคืน 3 วัน
            // วันที่รับไว้จะน้อยกว่าเสมอ
            // วันที่รับไว้ ถ้าเป็น null จะไม่เตือน ถ้าวันส่งคืนเป็น null จะใช้วันปัจจุบันแทน
            
            var returnDays; // วันที่รับ - วันที่แพทย์ส่งคืน 
            
            if(date1 == null){
                // do nothing
                returnDays = null;
            }else if(retdate == null){
                // date 1 มีค่า 
                returnDays = parseInt((nowdate - date1) / (1000 * 60 * 60 * 24)); 
                // เช็คค่าว่า ยังไม่ส่ง แต่ยังอยู่ในช่วงเวลา = เหลือง, เกินเวลา = แดง
                if(returnDays <= 3){
                    $('.table-responsive table tbody tr:eq('+i+') td:eq(10)').append('".$fawesome."').css('color', 'yellow');
                }
                else{
                    $('.table-responsive table tbody tr:eq('+i+') td:eq(10)').append('".$fawesome."').css('color', 'red');
                }
                
            }else{
                returnDays = parseInt((retdate - date1) / (1000 * 60 * 60 * 24));
                // เช็คค่าว่า ส่งแล้วอยู่ในช่วงเวลา = เขียว, เกินเวลา = แดง
                if(returnDays <= 3){
                    $('.table-responsive table tbody tr:eq('+i+') td:eq(10)').css('color', 'green');
                }
                else{
                    $('.table-responsive table tbody tr:eq('+i+') td:eq(10)').css('color', 'red');
                }
            }
            
            var retDays; //วันที่แพทย์ส่ง - วันที่อาจารย์ส่งคืน
            if(date1 == null){
                // do nothing
                retDays = null;
            }else if(staffretdate == null){
                // retdate มีค่า 
                retDays = parseInt((nowdate - retdate) / (1000 * 60 * 60 * 24));
                if(retDays <=7){
                    $('.table-responsive table tbody tr:eq('+i+') td:eq(11)').append('".$fawesome."').css('color', 'yellow');
                }
                else{
                    $('.table-responsive table tbody tr:eq('+i+') td:eq(11)').append('".$fawesome."').css('color', 'red');
                }
            }else{
                retDays = parseInt((staffretdate - retdate) / (1000 * 60 * 60 * 24));
                if(retDays <=7){
                    $('.table-responsive table tbody tr:eq('+i+') td:eq(11)').css('color', 'green');
                }
                else{
                    $('.table-responsive table tbody tr:eq('+i+') td:eq(11)').css('color', 'red');
                }
            }
            
            var staffretDay; //  วันที่อาจารย์ส่งคืน - วันที่ส่งคืน
            if(date1 == null){
                // do nothing
                staffretDay = null;
            }else if(staffretdate == null){
                //staffretdate มีค่า 
                staffretDay = parseInt((nowdate - staffretdate) / (1000 * 60 * 60 * 24));
                if(staffretDay <=4){
                    $('.table-responsive table tbody tr:eq('+i+') td:eq(12)').append('".$fawesome."').css('color', 'yellow');
                }
                else{
                    $('.table-responsive table tbody tr:eq('+i+') td:eq(12)').append('".$fawesome."').css('color', 'red');
                }
            }else{
                staffretDay = parseInt((returndate - staffretdate) / (1000 * 60 * 60 * 24));
                if(staffretDay <=4){
                    $('.table-responsive table tbody tr:eq('+i+') td:eq(12)').css('color', 'green');
                }
                else{
                    $('.table-responsive table tbody tr:eq('+i+') td:eq(12)').css('color', 'red');
                }
            }
            // overall คำนวนช่วงเวลาทั้งหมด
            var overallDay; //  วันที่รับ - วันที่ส่งคืน
            // insert วงกลม
            if(result[1] == ''){
                $('.table-responsive table tbody tr:eq('+i+') td:eq(1)').append('".$fawesome."');
            }
            if(date1 == null){
                // do nothing
                overallDay = null;
            }else if(returndate == null){
                //staffretdate มีค่า 
                overallDay = parseInt((nowdate - date1) / (1000 * 60 * 60 * 24));
                if(overallDay <=14){
                    $('.table-responsive table tbody tr:eq('+i+') td:eq(1)').css('color', 'yellow');
                }
                else{
                    $('.table-responsive table tbody tr:eq('+i+') td:eq(1)').css('color', 'red');
                }
            }else{
                overallDay = parseInt((returndate - date1) / (1000 * 60 * 60 * 24));
                if(overallDay <=14){
                    $('.table-responsive table tbody tr:eq('+i+') td:eq(1)').css('color', 'green');
                }
                else{
                    $('.table-responsive table tbody tr:eq('+i+') td:eq(1)').css('color', 'red');
                }
            }
           
        }
    }
");
}
?>
