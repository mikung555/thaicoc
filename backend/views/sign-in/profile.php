<?php

use common\models\UserProfile;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use kartik\widgets\Select2;
use yii\web\JsExpression;
use yii\widgets\MaskedInput;

/* @var $this yii\web\View */
/* @var $model common\models\UserProfile */
/* @var $form yii\bootstrap\ActiveForm */

$this->title = Yii::t('backend', 'Edit profile');
?>

<div class="user-profile-form">
    <div class="row">
        <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
        <div class='col-lg-3'>
            <?php echo $form->field($model, 'picture')->widget(\trntv\filekit\widget\Upload::classname(), [
                'url' => ['avatar-upload']
            ]) ?>
            <p>บทบาท ::
                <?Php if ($model->status == 0) {
                    echo " ผู้รับบริการหรือบุคคลทั่วไป";
                } else if ($model->status == 1) {
                    echo " บุคลากร";
                }
                ?>

            <p>

            <p>
                บทบาทเฉพาะสังกัด ::
                <?Php if ($model->status_personal == 2) {
                    echo " ผู้ดูแลระบบ";
                } else if ($model->status_personal == 3) {
                    echo " <br><p>ผู้ให้บริการด้านการแพทย์และสาธารณสุข</p>";
                } else if ($model->status_personal == 4) {
                    echo " ผู้บริหาร";
                    if ($model->status_manager == 5) {
                        echo "หน่วยงานระดับประเทศ";
                        echo "ชื่อหน่วยงาน " . $model->department_nation_text;
                    } else if ($model->status_manager == 6) {
                        echo "หน่วยงานระดับเขต";
                        echo "ชื่อหน่วยงาน " . $model->department_area_text;
                    } else if ($model->status_manager == 7) {
                        echo "หน่วยงานระดับจังหวัด";
                        echo "ชื่อหน่วยงาน " . $model->department_province_text;
                    } else if ($model->status_manager == 8) {
                        echo "หน่วยงานระดับอำเภอ";
                        echo "ชื่อหน่วยงาน " . $model->department_amphur_text;
                    } else if ($model->status_manager == 9) {
                        echo "หน่วยบริการทางการแพทย์และสาธารณสุข";
                        echo "ชื่อกลุ่มงาน " . $model->department_group;
                    }
                } else if ($model->status_personal == 10) {
                    echo " นักวิจัย";
                } else if ($model->status_personal == 11) {
                    echo $model->status_other;
                }
                ?>
            </p>
        </div>
        <div class='col-lg-9'>
            <hr>
            <h3>ชื่อและข้อมูลการติดต่อ</h3>
            <hr>
            <?php
            //$model->cid = Yii::$app->user->identity->username;
            echo $form->field($model, 'cid')->widget(MaskedInput::className(), [
                'mask' => '9999999999999',
                'options' => ['readonly' => '', 'class' => 'form-control']
            ]); ?>

            <?php echo $form->field($model, 'firstname')->textInput(['maxlength' => 255]) ?>


            <?php //echo $form->field($model, 'middlename')->textInput(['maxlength' => 255]) ?>

            <?php echo $form->field($model, 'lastname')->textInput(['maxlength' => 255]) ?>

            <?php echo $form->field($model, 'telephone')->widget(MaskedInput::className(), [
                'name' => 'telephone',
                'mask' => '9999999999'
            ]); ?>

            <?php 
	    echo $form->field($model,'email')->textInput()->label();
//	    echo $form->field($model, 'email')->widget(MaskedInput::className(), [
//                'name' => 'email',
//                'clientOptions' => [
//                    'alias' => 'email'
//                ],
//            ]); 
	    ?>

            <?php //echo $form->field($model, 'locale')->dropDownlist(Yii::$app->params['availableLocales']) ?>


            <?php echo $form->field($model, 'gender')->radioList([
                UserProfile::GENDER_MALE => Yii::t('backend', 'ชาย'),
                UserProfile::GENDER_FEMALE => Yii::t('backend', 'หญิง'),
            ]) ?>
	    
	    <div class="pull-right">ดาวน์โหลดเอกสารรักษาความลับ เพื่อให้ผู้บริหารอนุมัติ <a href="<?php echo Yii::$app->keyStorage->get('frontend.domain')=='cascap.in.th' ? 'http://www1.cascap.in.th/v4cascap/central_docs/siteadmin20140210_01.pdf' : 'https://www.thaicarecloud.org/imgfile/agreement 20160503.pdf'; ?>" target="_blank" > Download</a></div>
	   <div style="color: #ff0000">หากไม่มีเอกสารรักษาความลับ ท่านจะไม่สามารถเข้าใช้งานในระบบ</div>
	    <label class="control-label" for="userprofile-citizenid_file">สำเนาบัตรประชาชน <span style="color: #999;">(สำหรับสมาชิกทุกประเภท โดยถ่ายภาพสำเนาบัตรประชาชนที่มีรหัส 13 หลัก ชัดเจนพร้อมทั้งลายเซ็นรับรองสำเนาถูกต้อง)</span></label>
	    <?= $form->field($model, 'citizenid_file')->fileInput()->label(false); ?>
	   <?php
		$citizenid_file = 'ยังไม่อัพโหลดไฟล์';
		if($model->citizenid_file!=''){
		    $citizenid_file = Yii::getAlias('@storageUrl') . '/source/'.$model->citizenid_file;
		    $mimeType = backend\modules\ovcca\classes\OvccaFunc::exists($citizenid_file);
		    $img = $file;
		    if ($mimeType != 'application/pdf') {
			$img = \yii\helpers\Url::to(['/site/view-img', 'img'=>$citizenid_file]);
		    }else{
                        $img = $citizenid_file;
                    }
		}
		?>
	    <div class=""> <a href="<?=$img?>" target="_blank"><?=$model->citizenid_file?></a></div>
		<br><br>
	    <label class="control-label" for="userprofile-secret_file">เอกสารรักษาความลับ <span style="color: #999;">(สำหรับผู้ดูแลระบบเท่านั้น)</span></label>
	    <?= $form->field($model, 'secret_file')->fileInput()->label(false); ?>
		<?php
		$secret_file = 'ยังไม่อัพโหลดไฟล์';
		if($model->secret_file!=''){
		    $secret_file = Yii::getAlias('@storageUrl') . '/source/'.$model->secret_file;
		    $mimeType = backend\modules\ovcca\classes\OvccaFunc::exists($secret_file);
		    $img = $file;
		    if ($mimeType != 'application/pdf') {
			$img = \yii\helpers\Url::to(['/site/view-img', 'img'=>$secret_file]);
		    }else{
                        $img = $secret_file;
                    }
		}
		?>
		<div class=""> <a href="<?=$img?>" target="_blank"><?=$model->secret_file?></a></div>
		
		<br>
		<?php
	    echo $form->field($model, 'inout')->radioList(['หน่วยงานบริการสุขภาพ', 'หน่วยงานอื่นๆ'])->label(FALSE);
		  
	    ?>
            <?php
	    $labelStr = '';
	    
	    if(Yii::$app->user->can('administrator')){
		$labelStr = 'ท่านเป็น แอดมินกลาง ท่านสามารถค้นหาได้ทุกโรงพยาบาล';
	    } elseif(Yii::$app->user->can('adminnchangwat')){
		$labelStr = 'ท่านเป็น แอดมินระดับจังหวัด ท่านสามารถค้นหาโรงพยาบาลภายในจังหวัดตัวเองเท่านั้น';
	    } elseif(Yii::$app->user->can('adminnamphur')){
		$labelStr = 'ท่านเป็น แอดมินระดับอำเภอ ท่านสามารถค้นหาโรงพยาบาลภายในอำเภอตัวเองเท่านั้น';
	    } 
	    
            echo $form->field($model, 'sitecode')->widget(Select2::classname(), [
                'initValueText' => $dataHospital["code"] . " " . $dataHospital["name"], // set the initial display text
                'options' => ['placeholder' => 'ค้นหาหน่วยงาน ...', 'readonly' => ''],
//                        'name'=>'hospitacode',
		
                        'id'=>'hospitalcode',
		'disabled' =>$model->sitecode == '00000' || $model->sitecode == '' || $model->sitecode == '0' || empty($model->sitecode) || Yii::$app->user->can('administrator') || Yii::$app->user->can('adminnchangwat') || Yii::$app->user->can('adminnamphur') ? false : true,
                'pluginOptions' => [
                    'allowClear' => true,
                    'minimumInputLength' => 3,
                    'ajax' => [
                        'url' => '/sign-in/findhospital',
                        'dataType' => 'json',
                        'data' => new JsExpression('function(params) { return {q:params.term}; }')
                    ],
                    'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                    'templateResult' => new JsExpression('function(city) { return city.text; }'),
                    'templateSelection' => new JsExpression('function (city) { return city.text; }'),
                ],
            ])->label('หน่วยงาน / '.$labelStr);
	    
	    $dataMU = \backend\models\MoveUnits::find()->where('user_id=:user_id',[':user_id'=>Yii::$app->user->id])->orderBy('updated_at DESC')->one();
	    $srt='';
	    $moveShow = true;
	    
	    if($dataMU){
		if($dataMU->status == 'waiting'){
		    $moveShow = false;
		}
	    } 
	    if($moveShow){
		echo Html::button(Yii::t('app', 'ขอย้ายหน่วยงาน'), ['data-url'=>  \yii\helpers\Url::to(['/move-units/create']), 'class' => 'btn btn-success', 'id'=>'modal-addbtn-move-units']);
	    
	    }
	    
            ?>
		<span style="color: #ff0000">เพื่อไปสู่หน่วยงานใหม่ที่จะเก็บข้อมูล ที่ท่านบันทึก โดยท่านไม่สามารถเข้าถึงข้อมูลจากหน่วยงานเดิมได้</span>
	    <?=	    common\lib\sdii\widgets\SDModalForm::widget([
		'id' => 'modal-move-units',
		'size'=>'modal-lg',
		'tabindexEnable' => false,
	    ]);
	    ?>
		<?php  $this->registerJs("
		    
	       
$('#modal-addbtn-move-units').on('click', function(){
modalMoveUnit($(this).attr('data-url'));
});
	
function modalMoveUnit(url) {
    $('#modal-move-units .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
    $('#modal-move-units').modal('show')
    .find('.modal-content')
    .load(url);
}

");
		
		if($dataMU){
		    if($dataMU->status == 'disapproval'){
			$srt = 'ไม่อนุมัติ : '.$dataMU->comment;
		    } elseif($dataMU->status == 'waiting') {
			$srt = 'รอการอนุมัติ';
		    }
		    if($srt!=''){
		?>
		<br><br>
		<div class="row">
		<div class="col-md-12 "><div class="alert alert-info" role="alert">
		<?= $srt;?>
			
	      </div>
		    
		<div class='row'>
		    <div class="col-md-6" col-lg-offset-11>
			<div class="form-group">
			    <code>รหัสผู้สัมภาษณ์ <?php echo substr($model->cid, -5, 5) ?></code>
			</div>
		    </div>
		</div>
		    
		</div>
</div>
		
<!--            <hr>
            <h3>ที่อยู่ปัจจุบัน</h3>
            <hr>-->
            <?php
		}}
//            $itemsProvince = \backend\modules\ezforms\components\EzformQuery::getProvince();
//            $html = "<div class='col-md-3'>";
//            $html .=  Select2::widget([
//                'options' => ['placeholder' => 'จังหวัด','id'=>'province','value'=>'test'],
//                'data' => yii\helpers\ArrayHelper::map($itemsProvince,'PROVINCE_CODE','PROVINCE_NAME'),
//                'model' =>$model,
//                'attribute'=>'address_province',
//                'pluginOptions' => [
//                    'allowClear' => true
//                ],
//            ]);
//            $html .= "</div>";
//
//            if($model->address_amphur){
//                $sqlAmphurEdit = "SELECT AMPHUR_NAME FROM `const_amphur` WHERE AMPHUR_CODE='".$model->department_amphur."'";
//                $dataAmphurEdit = Yii::$app->db->createCommand($sqlAmphurEdit)->queryOne();
//                $placeholderAmphur = $dataAmphurEdit["AMPHUR_NAME"];
//            }else{
//                $placeholderAmphur = 'เลือกอำเภอ';
//            }
//            $html .= "<div class='col-md-3'>";
//            $html .= $form->field($model, 'address_amphur')->widget(\kartik\depdrop\DepDrop::classname(),[
//                'type'=>  \kartik\depdrop\DepDrop::TYPE_SELECT2,
//                'options'=>['id'=>'amphur'],
//                'model'=>$model,
//                'attribute'=>'address_amphur',
//                'pluginOptions'=>[
//                    'depends'=>['province'],
//                    'placeholder'=>$placeholderAmphur,
//                    'url'=>\yii\helpers\Url::to(['/ezforms/province/genamphur'])
//                ]
//            ])->label(false);
//            $html .= "</div>";
//
//            if($model->address_tambon){
//                $sqlTumbonEdit = "SELECT DISTRICT_NAME FROM `const_district` WHERE DISTRICT_CODE='".$model->department_area."'";
//                $dataTumbonEdit = Yii::$app->db->createCommand($sqlTumbonEdit)->queryOne();
//                $placeholderTumbon = $dataTumbonEdit["DISTRICT_NAME"];
//            }else{
//                $placeholderTumbon = 'เลือกตำบล';
//            }
//            $html .= "<div class='col-md-3'>";
//            $html .= $form->field($model, 'address_tambon')->widget(\kartik\depdrop\DepDrop::classname(),[
//                'type'=>  \kartik\depdrop\DepDrop::TYPE_SELECT2,
//                'model'=>$model,
//                'attribute'=>'address_tambon',
//                'pluginOptions'=>[
//                    'depends'=>['province','amphur'],
//                    'placeholder'=>$placeholderTumbon,
//                    'url'=>\yii\helpers\Url::to(['/ezforms/province/gentumbon'])
//                ]
//            ])->label(false);
//            $html .= "</div>";
//            echo $html;
//            
            ?>
	    
	</div>
	
    </div>
    
    <div class='row'>
        <div class="col-lg-3 col-lg-offset-11">
            <div class="form-group">
                <?php echo Html::submitButton(Yii::t('backend', 'Update'), ['class' => 'btn btn-primary']) ?>
            </div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>

</div>
