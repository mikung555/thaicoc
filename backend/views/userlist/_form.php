<?php

use common\models\User;
use common\models\UserProfile;
use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\widgets\Select2;
use yii\web\JsExpression;
use yii\widgets\MaskedInput;
use kartik\widgets\DepDrop;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model backend\models\UserForm */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $roles yii\rbac\Role[] */
/* @var $permissions yii\rbac\Permission[] */
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>
	<div class="row">
		<div class="col-md-3 ">
		    <?php echo $form->field($modelProfile, 'picture')->widget(\trntv\filekit\widget\Upload::classname(), [
			'url' => ['/sign-in/avatar-upload']
		    ]) ?>
		</div>
	    <div class="col-md-9 sdbox-col">
		
		<?php echo $form->field($model, 'username') ?>
        <?php echo $form->field($model, 'email') ?>
        <?php echo $form->field($model, 'password')->passwordInput() ?>
        <?php echo $form->field($model, 'status')->label(Yii::t('backend', 'Active'))->checkbox() ?>
    
	<div class="box box-solid box-success " id="personalForm2" >
    <div class="box-header">
        ชื่อและข้อมูลการติดต่อ
    </div>
    <br>
    <div class="box-body">
        <div class='form-horizontal'>
            <div class='form-group'>
                <label for="inputEmail3" class="col-sm-2 control-label">คำนำหน้าชื่อ</label>
                <div class="col-sm-8">
                    <?php
                    echo $form->field($modelProfile,'title')->dropDownList(['0'=>'นาย','1'=>'นาง','2'=>'นางสาว'])->label(false)
                    ?>
                </div>
            </div>
            <div class='form-group'>
                <label for="inputEmail3" class="col-sm-2 control-label"><code>*</code> ชื่อ</label>
                <div class="col-sm-8">
                    <?php
                    echo $form->field($modelProfile,'firstname')->textInput()->label(false);
                    ?>
                </div>
            </div>
            <div class='form-group'>
                <label for="inputEmail3" class="col-sm-2 control-label"><code>*</code> นามสกุล</label>
                <div class="col-sm-8">
                    <?php
                    echo $form->field($modelProfile,'lastname')->textInput()->label(false);
                    ?>
                </div>
            </div>
            <div class='form-group'>
                <label for="inputEmail3" class="col-sm-2 control-label">เพศ</label>
                <div class="col-sm-8">
                    <?php
                    echo $form->field($modelProfile,'gender')->radioList(['1'=>'ชาย','2'=>'หญิง'])->label(false)
                    ?>
                </div>
            </div>
            <div class='form-group'>
                <label for="inputEmail3" class="col-sm-2 control-label"><code>*</code> อีเมล์</label>
                <div class="col-sm-8">
                    <?php 
			echo $form->field($modelProfile,'email')->textInput()->label(false);
//			echo $form->field($modelProfile,'email')->widget(MaskedInput::className(),[
//                        'clientOptions'=>[
//                            'alias'=>'email'
//                        ]
//                    ])->label(false);
			?>
                </div>
            </div>
            <div class='form-group'>
                <label for="inputEmail3" class="col-sm-2 control-label"><code>*</code> เบอร์โทรศัพท์</label>
                <div class="col-sm-8">
                    <?php echo $form->field($modelProfile,'telephone')->widget(MaskedInput::className(),[
                        'mask'=>'9999999999'
                    ])->label(false);
                    ?>
                </div>
            </div>
	    
        </div>

    </div>
    
    
        <!-- <div class="box box-footer"  >
            <button type="button" id='btnForm2'  style="float:right;" class="btn btn-success">ถัดไป</button>
        </div> -->
</div>
    
    <div class="box box-solid box-success  " id="personalForm3" >
    <div class="box-header">
        เลือกบทบาท
    </div>
    <br>
        <div class="box-body">
            
	
	<label>เลือกบทบาท (สามารถเลือกได้มากกว่าหนึ่งบทบาท)</label>
                    <?php
                    //$readonly = 1;
		    if($readonly){
			$sex = ArrayHelper::getValue(array('0'=>'ผู้รับบริการหรือบุคคลทั่วไป', '1'=>'บุคลากร'), $modelProfile->status);
			if($modelProfile->status==1){
			    $sex = 'ผู้รับบริการหรือบุคคลทั่วไป, '.$sex;
			}
                        
                        if($modelProfile->volunteer_status==1){
			    $sex .= ', อาสาสมัคร';
			}
			
			echo '<div class="form-group">'.Html::textInput('status', $sex, ['class'=>'form-control', 'readonly'=>$readonly]).'</div>';
                        
                        if($modelProfile->volunteer_status==1){
                            $volunteer = ArrayHelper::getValue([1=>'อสค. (อาสาสมัครประจำครอบครัว)', 2=>'อสม. (อาสาสมัครสาธารณสุขประจำหมู่บ้าน)'], $modelProfile->volunteer);
                            echo '<div class="form-group">'.Html::textInput('volunteer', $volunteer, ['class'=>'form-control', 'readonly'=>$readonly]).'</div>';
                        }
                        
                        if($modelProfile->status==1){
                            $personal = '';
                            if ($modelProfile->status_personal == 2) {
                                $personal .= "ผู้ดูแลระบบ";
                            } else if ($modelProfile->status_personal == 3) {
                                $personal .= "ผู้ให้บริการด้านการแพทย์และสาธารณสุข";
                            } else if ($modelProfile->status_personal == 4) {
                                $personal .= "ผู้บริหาร";
                                if ($modelProfile->status_manager == 5) {
                                    $personal .= " -> หน่วยงานระดับประเทศ";
                                    $personal .= " -> ชื่อหน่วยงาน " . $modelProfile->department_nation_text;
                                } else if ($modelProfile->status_manager == 6) {
                                    $personal .= " -> หน่วยงานระดับเขต";
                                    $personal .= " -> ชื่อหน่วยงาน " . $modelProfile->department_area_text;
                                } else if ($modelProfile->status_manager == 7) {
                                    $personal .= " -> หน่วยงานระดับจังหวัด";
                                    $personal .= " -> ชื่อหน่วยงาน " . $modelProfile->department_province_text;
                                } else if ($modelProfile->status_manager == 8) {
                                    $personal .= " -> หน่วยงานระดับอำเภอ";
                                    $personal .= " -> ชื่อหน่วยงาน " . $modelProfile->department_amphur_text;
                                } else if ($modelProfile->status_manager == 9) {
                                    $personal .= " -> หน่วยบริการทางการแพทย์และสาธารณสุข";
                                    $personal .= " -> ชื่อกลุ่มงาน " . $modelProfile->department_group_text;
                                }
                            } else if ($modelProfile->status_personal == 10) {
                                $personal .= "นักวิจัย";
                            } else if ($modelProfile->status_personal == 11) {
                                $personal .= $modelProfile->status_other;
                            }
                            
                            echo '<div class="form-group">'.Html::textInput('personal', $personal, ['class'=>'form-control', 'readonly'=>$readonly]).'</div>';
                        }
                        

			echo Html::activeHiddenInput($modelProfile,'volunteer_status');
                        echo Html::activeHiddenInput($modelProfile,'volunteer');
                        echo Html::activeHiddenInput($modelProfile,'status');
		    } else {
                        ?>
                    <div class="form-group">
                            
                            <div class="checkbox">
                                <?=  Html::checkbox('tmpStatus', true, ['label'=>'ผู้รับบริการหรือบุคคลทั่วไป'])?>
                            </div>
                    </div>

                    <div class="form-group">
                            <div class="checkbox">
                                <?= Html::activeCheckbox($modelProfile, 'volunteer_status', [])?>
                            </div>
                    </div>

                    <div id="volunteer-form" class="" style="<?=($modelProfile->volunteer_status==1)?'':'display:none;'?>">
                        <?= Html::activeDropDownList($modelProfile, 'volunteer', [1=>'อสค. (อาสาสมัครประจำครอบครัว)', 2=>'อสม. (อาสาสมัครสาธารณสุขประจำหมู่บ้าน)'], ['class'=>'form-control', 'prompt'=>'---เลือกอาสาสมัคร---'])?>
                    </div>
                    <?=$form->field($modelProfile,'status')->checkbox(array('label'=>'บุคลากร'))->label(false);?>
                    <div id="status-personal-form" style="<?=$modelProfile->status==1?'':'display:none;'?>">
	    
            <div class="row" id="rowPersonal">
                <div class="col-lg-6">
                    <label>เลือกบทบาทเฉพาะสังกัด</label>
                    <?php
                    echo $form->field($modelProfile,'status_personal')->dropDownList(
                        [
                            ''=>'-- เลือกหน่วยงานสังกัดและบทบาท --',
                            '2'=>'ผู้ดูแลระบบฐานข้อมูล',
                            '3'=>'ผู้ให้บริการด้านการแพทย์และสาธารณสุข',
                            '4'=>'ผู้บริหาร',
                            '10'=>'นักวิจัย',
                            '11'=>'อื่นๆ ',

                        ])->label(false)
                    ?>
                </div>
                <div class="col-lg-6" id="rowManager" style="<?=($modelProfile->status_personal == 4)?'':'display:none;'?>">
                    <label>เลือกระดับ</label>
                    <?php
                      echo $form->field($modelProfile,'status_manager')->dropDownList([
                              ''=>'-- เลือกระดับหน่วยงาน --',
                              '5'=>'หน่วยงานระดับประเทศ',
                              '6'=>'หน่วยงานระดับเขต',
                              '7'=>'หน่วยงานระดับจังหวัด',
                              '8'=>'หน่วยงานระดับอำเภอ',
                              '9'=>'หน่วยงานทางการแพทย์และสาธารณสุข'
                              ])->label(false);
                    ?>
                </div>

                <div class="col-lg-6" id="rowStatusOther" style="<?=($modelProfile->status_personal == 11)?'':'display:none;'?>">
                    <label>บทบาทอื่นๆ</label>
                    <?php
                    echo $form->field($modelProfile,'status_other')->label(false);
                    ?>
                </div>
            </div>
            <div class="row" id="rowNation" style="<?=($modelProfile->status_personal == 4  && $modelProfile->status_manager == 5)?'':'display:none;'?>">
                <div class="col-lg-12">
                    <label>ชื่อหน่วยงานระดับประเทศ</label>
                    <?php
                    echo $form->field($modelProfile,'department_area_text')->textInput()->label(false);
                    ?>
                </div>
            </div>
            <div class="row" id="rowArea" style="<?=($modelProfile->status_personal == 4  && $modelProfile->status_manager == 6)?'':'display:none;'?>">
                <div class="col-lg-6">
                    <label>หน่วยงานระดับเขต</label>
                    <?php
                    echo $form->field($modelProfile,'department_area')->dropDownList([''=>'-- เลือกเขตบริการ --',
                        '1'=>'เขตบริการที่ 1','2'=>'เขตบริการที่ 2','3'=>'เขตบริการที่ 3','4'=>'เขตบริการที่ 4','5'=>'เขตบริการที่ 5','6'=>'เขตบริการที่ 6','7'=>'เขตบริการที่ 7',
                        '8'=>'เขตบริการที่ 8','9'=>'เขตบริการที่ 9','10'=>'เขตบริการที่ 10','11'=>'เขตบริการที่ 12','13'=>'เขตบริการที่ 13','14'=>'เขตบริการที่ 14'
                    ])->label(false)
                    ?>
                </div>
                <div class="col-lg-6">
                    <label>ชื่อหน่วยงานระดับเขต</label>
                    <?php
                    echo $form->field($modelProfile,'department_area_text')->textInput()->label(false);
                    ?>
                </div>
            </div>
            <div class="row" id="rowProvince" style="<?=($modelProfile->status_personal == 4  && $modelProfile->status_manager == 7 || $modelProfile->status_manager == 8)?'':'display:none;'?>">
                <div class="col-lg-6">
                    <label>หน่วยงานระดับจังหวัด</label>
                    <?php
		    echo $form->field($modelProfile, 'department_province')->widget(Select2::className(), [
			    'options' => ['placeholder' => 'จังหวัด','id'=>'province','value'=>'test'],
			    'data' => ArrayHelper::map($dataProvince,'PROVINCE_CODE','PROVINCE_NAME'),
			    'pluginOptions' => [
				'allowClear' => true
			    ],
			])->label(false);
                    ?>
                </div>
                <div class="col-lg-6">
                    <label>ชื่อหน่วยงาน</label>
                    <?php
                        echo $form->field($modelProfile,'department_province_text')->textInput()->label(false);
                    ?>
                </div>
            </div>
            <div class="row"  id="rowAmphur" style="<?=($modelProfile->status_personal == 4  && ($modelProfile->status_manager == 8 ))?'':'display:none;'?>">
                <div class="col-lg-6"  >
                    <label>หน่วยงานระดับอำเภอ</label>
                    <?php
                    echo Html::hiddenInput('input-amphur', $modelProfile->department_amphur, ['id'=>'input-amphur']);
                    echo $form->field($modelProfile,'department_amphur')->widget(DepDrop::classname(),[
                        'type'=>  DepDrop::TYPE_SELECT2,
                        'options'=>['id'=>'amphur'],
                        'model'=>$modelProfile,
                        'attribute'=>'department_amphur',
                        'pluginOptions'=>[
                            'depends'=>['province'],
                            'initialize' => true,
                            'initDepends'=>['province'],
                            'params'=>['input-amphur'],
                            'placeholder'=>'อำเภอ',
                            'url'=>'/user/genamphur',
                        ]
                    ])->label(false);
                    ?>
                </div>
                <div class="col-lg-6">
                    <label>ชื่อหน่วยงานระดับอำเภอ</label>
                    <?php
                    echo $form->field($modelProfile,'department_amphur_text')->textInput()->label(false);
                    ?>
                </div>
            </div>
        </div> <!-- END -->
			<?php 
			
			//echo $form->field($modelProfile,'status')->radioList(array('0'=>'ผู้รับบริการหรือบุคคลทั่วไป', '1'=>'บุคลากร'))->label(false);
		    }  
		    ?>
               
            
	
	    <?php
                echo Html::activeHiddenInput($modelProfile, 'department');
                
		if($readonly){
			$sex = ArrayHelper::getValue([0=>'หน่วยงานบริการสุขภาพ', 2=>'ร้านยา', 3=>'องค์กรปกครองส่วนท้องถิน', 1=>'หน่วยงานอื่นๆ'], $modelProfile->inout);
			echo '<div class="form-group">'.Html::textInput('inout', $sex, ['class'=>'form-control', 'readonly'=>$readonly]).'</div>';
			echo Html::activeHiddenInput($modelProfile,'inout');
		    } else {
			if(Yii::$app->keyStorage->get('frontend.domain') == 'thaicarecloud.org' || Yii::$app->keyStorage->get('frontend.domain')=='yii2-starter-kit.dev'){
                            echo $form->field($modelProfile, 'inout')->radioList([0=>'หน่วยงานบริการสุขภาพ', 2=>'ร้านยา', 3=>'องค์กรปกครองส่วนท้องถิน', 1=>'หน่วยงานอื่นๆ']);
                        } else {
                            echo $form->field($modelProfile, 'inout')->radioList([0=>'หน่วยงานบริการสุขภาพ', 1=>'หน่วยงานอื่นๆ']);
                        }
		    }  
	    ?>
        
            <?php
	    echo $form->field($modelProfile, 'sitecode')->widget(yii\jui\AutoComplete::className(), [
		'clientOptions' => [
		'minChars' => 1,
		'source' => new JsExpression("function(request, response) {
		    var userType = $('input[name=\"UserProfile[inout]\"]:checked').val();
		    if(userType==2){
			$.getJSON('".yii\helpers\Url::to(['/user/querys2'])."', {
			    q: request.term
			}, response);
		    } else if(userType==3){
			$.getJSON('".yii\helpers\Url::to(['/user/querys3'])."', {
			    q: request.term
			}, response);
		    } else {
			$.getJSON('".yii\helpers\Url::to(['/user/querys'])."', {
			    q: request.term
			}, response);
		    }
		    
		}"),
		'select' => new JsExpression("function( event, ui ) {
		    $('#userprofile-department').val(ui.item.id);
		 }"),
		'change' => new JsExpression("function( event, ui ) {
		    if (!ui.item) {
			$('#userprofile-department').val('');
		    }
		 }"),
		],
	     'options'=>['class'=>'form-control', 'disabled'=>$readonly] 
	    ])->hint('กรุณาค้นด้วยเลขห้าหลัก หรือส่วนหนึ่งของชื่อแล้วคลิกเลือกรายการที่ปรากฏ หากไม่คลิกเลือกจะถือว่าเป็นการเพิ่มหน่วยงานใหม่');
	?>
            
            
            
            
            
    </div>
    <!-- <div class="box box-footer"  >
        <button type="button" id="btnForm3" class="btn btn-success" style="float:right;">ถัดไป</button>
    </div> -->
</div>
		
        <?php 
	    
	    $arrRoles = [];
	    foreach ($model->roles as $value) {
		$arrRoles[] = $roles[$value];
	    }
	    
	    echo '<label class="control-label" for="userform-roles">Roles</label>';
	    echo '<div class="form-group">'.Html::textInput('roles', implode(', ', $arrRoles), ['class'=>'form-control', 'readonly'=>true]).'</div>';
	    echo Html::activeHiddenInput($model,'roles');
	?>
	
	<div class='form-group'>
	    <label for="enroll_key" class="control-label"> Enroll key</label>
		<?php
		echo $form->field($modelProfile,'enroll_key')->textInput(['readonly'=>TRUE])->label(false);
		?>
	</div>	
    <label class="control-label" for="userprofile-secret_file">เอกสารรักษาความลับ</label>
    <?php
		$secret_file = '#';
		if($modelProfile->secret_file!=''){
		    $secret_file = Yii::getAlias('@storageUrl') . '/source/'.$modelProfile->secret_file;
		    $secret_file_old = $secret_file;
		    $ext = strtolower(pathinfo($modelProfile->secret_file, PATHINFO_EXTENSION));
		    if($ext=='pdf'){
			$secret_file = Yii::getAlias('@storageUrl').'/source/pdf_icon.png';
		    }
		?>
    <div class=""><a class="file-preview-image" data-filename="<?=$secret_file_old?>"><img src="<?=$secret_file?>"  height="100" ></a></div>
		<?php } ?>
		<br>
		<label class="control-label" for="userprofile-citizenid_file">สำเนาบัตรประชาชน</label>
		<?php
		$citizenid_file = '#';
		if($modelProfile->citizenid_file!=''){
		    $citizenid_file = Yii::getAlias('@storageUrl') . '/source/'.$modelProfile->citizenid_file;
		    $citizenid_file_old = $citizenid_file;
		    $ext = strtolower(pathinfo($modelProfile->citizenid_file, PATHINFO_EXTENSION));
		    if($ext=='pdf'){
			$citizenid_file = Yii::getAlias('@storageUrl').'/source/pdf_icon.png';
		    }
		?>
		<div class=""> <a class="file-preview-image" data-filename="<?=$citizenid_file_old?>"><img src="<?=$citizenid_file?>" height="100" ></a></div>
		<?php } ?>
		<br>
		<?php
	    echo Html::checkbox('reset_file', false, ['label'=>'เอกสารไม่ถูกต้อง คลิกเพื่อให้ผู้ใช้อัพโหลดเอกสารใหม่']);
		?>
        <div class="form-group">
            <?php echo Html::submitButton(Yii::t('backend', 'Save'), ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>
        </div>
	    </div>
	</div>
	
        
    <?php ActiveForm::end(); ?>

</div>
<?=	    common\lib\sdii\widgets\SDModalForm::widget([
		'id' => 'modal-move-units',
		'size'=>'modal-lg',
		'tabindexEnable' => false,
	    ]);
	    ?>
<?php
$this->registerJs(
    "	    $('body').tooltip('destroy');

            $('input:checkbox[name=\"UserProfile[status]\"]').on('click',function(){
                if($(this).is(':checked')){
                    $('#status-personal-form').show();  // checked
                } else {
                    $('#status-personal-form').hide();
		    $('#userprofile-status_personal').val('');
		    $('#userprofile-status_personal').trigger('change');
		}
            });

            $('#userprofile-status_personal').on('change',function(){
                var type = $(this).val();
                if(type==4){
		    $('#rowManager').show();
                    $('#rowStatusOther').hide();
                    // $('#rowDepartment').hide();
                }else if(type==11){
                    $('#rowStatusOther').show();
                    // $('#rowDepartment').hide();
		    $('#userprofile-status_manager').val('');
		    $('#userprofile-status_manager').trigger('change');
                    $('#rowManager').hide();
                }else{
                    $('#rowDepartment').show();
                    $('#rowStatusOther').hide();
		    $('#userprofile-status_manager').val('');
		    $('#userprofile-status_manager').trigger('change');
                    $('#rowManager').hide();
                }
            });
            $('#userprofile-status_manager').on('change',function(){
                var type = $(this).val();

                if(type==5){
                  $('#rowNation').show();
                  $('#rowProvince').hide();
                  $('#rowArea').hide();
                  $('#rowAmphur').hide();
                }else if(type==6){
                  $('#rowArea').show();
                  $('#rowNation').hide();
                  $('#rowProvince').hide();
                  $('#rowAmphur').hide();
                }
                else if(type==7){
                  $('#rowProvince').show();
                  $('#rowNation').hide();
                  $('#rowArea').hide();
                  $('#rowAmphur').hide();
                }else if(type==8){
                  $('#rowProvince').show();
                  $('#rowAmphur').show();
                  $('#rowNation').hide();
                  $('#rowArea').hide();
                }else{
                  $('#rowDepartment').show();
                  $('#rowArea').hide();
                  $('#rowNation').hide();
                  $('#rowProvince').hide();
                  $('#rowAmphur').hide();
                }
            });
	    
	    $('input:checkbox[name=\"UserProfile[volunteer_status]\"]').on('click',function(){
                if($(this).is(':checked')){
                    $('#volunteer-form').show();  // checked
                } else {
                    $('#volunteer-form').hide();
		    $('#userprofile-volunteer').val('');
		}
            });
            
            $('#btnAddMoreHospital').on('click',function(){
              var nameHospital = $('#addMoreHospital').val();
              if(nameHospital==''){
                $('#rowMoreHospital').attr('class','form-group has-error');
                $('.hospital-error').html('กรุณาระบุ');
              }else{
                $('#rowMoreHospital').attr('class','form-group');
                $('.hospital-error').html('');
                // $('#rowMoreHospital').hide();
                $.post('/user/formquery/addmorehospital',{nameHospital:nameHospital},function(result){
                    console.log(result);
                });
              }
            });
	    
	    $('.file-preview-image').click(function(){
                    var img = $(this).attr('src');
		    var filename = $(this).attr('data-filename');
                    modalMoveUnit('" . Url::to(['/sign-in/viewimg', 'img' => '']) . "'+filename);
               });
	       
	       function modalMoveUnit(url) {
		    $('#modal-move-units .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
		    $('#modal-move-units').modal('show')
		    .find('.modal-content')
		    .load(url);
		}
    "
);
?>