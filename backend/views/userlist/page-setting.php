<?php
use yii\helpers\Html;
use kartik\widgets\FileInput;
// or 'use kartikile\FileInput' if you have only installed yii2-widget-fileinput in isolation
use yii\helpers\Url;
use yii\widgets\Pjax;

$domain = Yii::$app->keyStorage->get('frontend.domain');
//    $siteprefix= trim(str_replace($domain, "", $_SERVER['HTTP_HOST']));
//    $siteprefix= trim(str_replace(".", "",$siteprefix));


$mySite = Yii::$app->user->identity->userProfile->sitecode;

if ($mySite != '') {
    if (is_numeric($mySite)) {
        $siteurlcode = $mySite + 0;
        if ($siteurlcode == 0) $siteurlcode = "";
    } else {
        $siteurlcode = $mySite;
    }

    $sqlSiteURL = "SELECT code,`url`,`name`, status, favicon FROM site_url WHERE code = :code";
    $dataSiteURL = Yii::$app->db->createCommand($sqlSiteURL, [':code' => $siteurlcode])->queryOne();

    $sqlHospital = "SELECT `hcode`,`name` FROM all_hospital_thai WHERE hcode='".($mySite)."' ";
    $dataHospital = Yii::$app->db->createCommand($sqlHospital)->queryOne();

    
    
    if ($dataSiteURL) {
	if(isset($dataSiteURL['status']) && $dataSiteURL['status']==3){
	    $frontendUrl=$dataSiteURL['url'].".".Yii::$app->keyStorage->get('frontend.domain');
	    if (Yii::$app->keyStorage->get('ssl.enable')=="1") {
		$http="https";
	    }else{
		$http="http";
	    }

	    echo '<div class="row">';
	    echo '<div class="col-md-12" style="padding: 15px 15px;">';
	    echo '<span class="h3"><li class="fa fa-building"></li> ';

	    echo $mySite ." : " . $dataHospital['name']." ";
	    echo '<il class="fa fa-globe"></il> ';
	    echo \yii\helpers\BaseHtml::a("{$http}://".$frontendUrl, "{$http}://".$frontendUrl).'</span>';
	    echo '</div>';
	    echo '</div><hr>';
	    ?>

<?php
	    
	    if (Yii::$app->user->can('administrator') || (Yii::$app->user->can('adminsite')) || (Yii::$app->user->can('frontend'))) {
		?>
		<form id="siteurl_form" enctype="multipart/form-data" method="post"
		      action="<?= yii\helpers\Url::to(['userlist/updatesite', 'id' => $dataSiteURL['code']]) ?>">
		    <div class="form-group" style="width: 50%">
			<label>Favicon Upload <?=  Html::a('Convert icons', 'https://iconverticons.com/online/', ['target'=>'_black'])?> </label> 
			
		    <?php
		    $initialPreview = [Yii::getAlias('@storageUrl/form-upload/').$dataSiteURL['favicon']];
		    
		    echo FileInput::widget([
			'name' => 'favicon',
			'options'=>[
			    
			],
			'pluginOptions' => [
			    'previewFileType' => 'any',
			    //'initialPreview' => $initialPreview,
			    'showPreview' => false,
			    'showCaption' => true,
			    'showRemove' => false,
			    'allowedFileExtensions' => ['ico'],
			    'maxFileSize' => 1000,
			    'uploadUrl' => Url::to(['/userlist/upload-favicon', 'id' => $dataSiteURL['code']]),
			]
		    ]);
		    if($dataSiteURL['favicon']!=''){
		    ?>
			<img src="<?=Yii::getAlias('@storageUrl/form-upload/').$dataSiteURL['favicon']?>">
			<?php
		    } else {
			echo 'ไม่พบ favicon';
		    }
			?>
		    </div>
		    <div class="form-group form-inline">
			<label for="siteurl">ที่อยู่เว็บไซต์</label>
			<div class="input-group">
			    <input type="text" class="form-control" id="siteurl" value="<?= $dataSiteURL['url'] ?>"
				   onsubmit="return false;">
			    <div class="input-group-addon"><?= $domain ?></div>
			</div>
			<button id="updateBtn" type="submit" class="btn btn-default"><li class="fa fa-link"></li> เปลียน URL</button>
		    </div>
		    &nbsp;&nbsp;&nbsp;
		    <a href="<?= 'http://' . $dataSiteURL['url'] . '.' . $domain ?>"
		       class="btn btn-primary"><li class="fa fa-edit"></li> ดูตัวอย่าง / จัดการหน้าแรก</a>
		    
		    <a href="<?= \yii\helpers\Url::to(['/userlist/page-setting', 'action'=>'ban', 'code'=>$dataSiteURL['code']]) ?>"
		       class="btn btn-warning"><li class="fa fa-ban"></li> ระงับชั่วคราว</a>
		    <a href="<?= \yii\helpers\Url::to(['/userlist/page-setting', 'action'=>'delete', 'code'=>$dataSiteURL['code']]) ?>"
		       class="btn btn-danger"><li class="fa fa-remove"></li> ลบออกอย่างถาวร</a>   
		</form>
		<?php
	    }else{
		echo '<div class="alert danger h3">ท่านไม่ได้รับอนุญาตให้แก้ไข Site URL เนื่องจากท่านไม่ได้รับสิทธิ์ Admin site หรือจัดการหน้าแรก</div>';
	    }
	}
	elseif (isset($dataSiteURL['status']) && $dataSiteURL['status']==1) {
	    echo '<div class="alert alert-info" role="alert" style="font-size: 20px;">';
	    echo 'Web site ถูกระงับชั่วคราว ท่านสามารถกู้ไซต์ของท่านคืน '. yii\helpers\Html::a('ได้ที่นี่คลิก', ['/userlist/page-setting', 'action'=>'restore', 'code'=>$dataSiteURL['code']]);
	    echo '</div>';
	} elseif (isset($dataSiteURL['status']) && $dataSiteURL['status']==0) {
	    ?>
<div class="row">
    <div class="col-md-offset-3 col-md-6 well text-center">
	<?=Yii::$app->keyStorage->get('accept_msg')?>
    </div>
</div>
<div class="row">
    <div class="col-md-offset-3 col-md-6 text-center">
	<a href="<?= \yii\helpers\Url::to(['/userlist/page-setting', 'action'=>'accept', 'code'=>$dataSiteURL['code']]) ?>"
		       class="btn btn-primary"><li class="glyphicon glyphicon-ok"></li> ยอมรับ</a>
    </div>
</div>
	    
	    
	    <?php
	} elseif (isset($dataSiteURL['status']) && $dataSiteURL['status']==2) {
	    echo '<div class="alert alert-info" role="alert" style="font-size: 20px;">';
	    echo 'Web site ถูกลบออกอย่างถาวร ท่านสามารถสร้างไซต์ของท่านใหม่ '. yii\helpers\Html::a('ได้ที่นี่คลิก', ['/userlist/page-setting', 'action'=>'create', 'code'=>$dataSiteURL['code']]);
	    echo '</div>';
	} elseif (isset($dataSiteURL['status']) && $dataSiteURL['status']==4) {
	    echo '<div class="alert alert-info" role="alert" style="font-size: 20px;">';
	    echo 'Web site ถูก Super Admin ระงับชั่วคราว ';
	    echo '</div>';
	} elseif (isset($dataSiteURL['status']) && $dataSiteURL['status']==5) {
	    echo '<div class="alert alert-info" role="alert" style="font-size: 20px;">';
	    echo 'Web site ถูก Super Admin ลบออกอย่างถาวร ';
	    echo '</div>';
	}
	
	//accept_msg
	if(Yii::$app->user->can('administrator')) {
	    $searchModel = new \backend\models\SiteUrlSearch();
	    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
	   
	    Pjax::begin(['id'=>'SiteUrl-grid-pjax']); 
	   
	echo '<hr>'.\common\lib\sdii\widgets\SDGridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
	    'class' => 'yii\grid\SerialColumn',
	    'headerOptions'=>['style'=>'text-align: center;'],
	    'contentOptions'=>['style'=>'width:80px;text-align: center;'],
	    ],
	    
	    [
		'attribute'=>'code',
		
		'headerOptions'=>['style'=>'text-align: center;'],
		'contentOptions'=>['style'=>'width:120px;text-align: center;'],
	    ],	
	    [
		'attribute'=>'name',
		
	    ],	
	    [
		'attribute'=>'url',
		
		'contentOptions'=>['style'=>'width:200px;'],
	    ],	
		[
		'attribute'=>'updated_at',
		'filter'=>'',    
		'value' => function ($model) {
		    Yii::$app->formatter->locale = 'th';

		   return Yii::$app->formatter->asDate($model->updated_at, 'short');
		},
		'headerOptions'=>['style'=>'text-align: center;'],
		'contentOptions'=>['style'=>'width:90px;text-align: center;'],
	    ],	
            ['class' => 'yii\grid\ActionColumn',
                'header'=>'จัดการ Site',
                'template' => '{block} {remove} {restore} {create}',
                'buttons' => [

                    'block' => function ($url, $model) {
			if(($model['status']==3 || $model['status']==0) && $model['code']!='0'){
			    return Html::a('<span class="fa fa-ban"></span> Ban', ['/userlist/page-setting', 'action'=>'block', 'code'=>$model['code']], [
                                    'title' => Yii::t('app', 'Ban'),
                                    'class'=>'btn btn-warning btn-xs',
			    ]);
			}
                    },
		    'remove' => function ($url, $model) {
			if(($model['status']==3 || $model['status']==0) && $model['code']!='0'){
			    return Html::a('<span class="glyphicon glyphicon-trash"></span> Delete', ['/userlist/page-setting', 'action'=>'remove', 'code'=>$model['code']], [
                                    'title' => Yii::t('app', 'Delete'),
                                    'class'=>'btn btn-danger btn-xs',
			    ]);
			}
                    },  
		    'restore' => function ($url, $model) {
			if(($model['status']==4 || $model['status']==1)){
			    return Html::a('<span class="glyphicon glyphicon-repeat"></span> Restore', ['/userlist/page-setting', 'action'=>'restore', 'code'=>$model['code']], [
                                    'title' => Yii::t('app', 'Restore'),
                                    'class'=>'btn btn-primary btn-xs',
			    ]);
			}
                    }, 
		    'create' => function ($url, $model) {
			if(($model['status']==2 || $model['status']==5)){
			    return Html::a('<span class="glyphicon glyphicon-plus"></span> Create', ['/userlist/page-setting', 'action'=>'create', 'code'=>$model['code']], [
                                    'title' => Yii::t('app', 'Create'),
                                    'class'=>'btn btn-primary btn-xs',
			    ]);
			}
                    }, 			    
                    
                ],
                'contentOptions' => ['style' => 'text-align: center;width:150px;']                
            ],
        ],
    ]); 
	  Pjax::end();  
	}
	
	
    }
}
?>


<?php  $this->registerJs("

$('#updateBtn').on('click', function(e){
    $.post(
		$('form#siteurl_form').attr('action'), //serialize Yii2 form
		{siteurl:$('#siteurl').val()}
    ).done(function(result){
		if(result.status == 'success'){
			". \common\lib\sdii\components\helpers\SDNoty::show('result.message', 'result.status') ."
			location.reload();
		} else{
			". \common\lib\sdii\components\helpers\SDNoty::show('result.message', 'result.status') ."
		} 
    }).fail(function(){
		console.log('server error');
    });
    return false;
});


");?>
