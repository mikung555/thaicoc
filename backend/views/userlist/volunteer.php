<?php

use common\models\User;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\Pjax;
use common\lib\sdii\components\helpers\SDNoty;
use yii\bootstrap\ActiveForm;
use yii\web\JsExpression;
use appxq\sdii\helpers\SDHtml;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'รายชื่อผู้ใช้งานในหน่วยบริการ');
$this->params['breadcrumbs'][] = $this->title;


?>
<ul class="nav nav-tabs">
  <li role="presentation" ><a href="<?=  Url::to(['userlist/index'])?>">บุคลากร</a></li>
  <li role="presentation" class="active"><a href="<?=  Url::to(['userlist/volunteer'])?>">อาสามาสมัคร</a></li>
  <li role="presentation"><a href="#">ประชาชน</a></li>
</ul>

<div class="user-index" >
    <div class="text-right" style="margin-top: 10px;" >
    <?php $form = ActiveForm::begin([
	'id' => 'jump_menu',
        'action' => ['volunteer'],
        'method' => 'get',
	'options' => ['class'=>'form-inline text-right']
    ]); ?>
	
	<?php // Html::dropDownList('site', $sitecode, \yii\helpers\ArrayHelper::map($sitelist, 'code', 'name'), ['class'=>'form-control pull-right', 'onChange'=>'$("#jump_menu").submit()'])
        echo '<div class="form-group">';
        echo '<label for="usersearch-username">ค้นหา</label> ';
        echo Html::activeTextInput($searchModel, 'username', ['style'=>'width: 200px;', 'class'=>'form-control', 'placeholder'=>'username, ชื่อ-สกุล', 'onChange'=>'$("#jump_menu").submit()']);
        echo '</div>';
	if (Yii::$app->user->can('administrator')) {
	?>
        <div class="form-group">
		    <?=	kartik\widgets\Select2::widget([
			'name' => 'pcode',
			'data' => yii\helpers\ArrayHelper::map(backend\models\ConstProvince::find()->all(), 'PROVINCE_CODE', 'PROVINCE_NAME'),
			'value'=>$pcode,
			'pluginOptions' => [
			    'allowClear' => true
			],
			'options' => [
			    'class'=>'pull-right',
			    'prompt'=>'All',
			    'onChange'=>'$("#jump_menu").submit()'
			],
		    ]);?>
		</div>
         <div class="form-group">
		    <?=	kartik\widgets\Select2::widget([
			'name' => 'site',
			'initValueText' => $sitelist['name'],
			'value'=>$sitecode,
			'options' => [
			    //'placeholder' => 'Select ...',
			    'class'=>'pull-right',
			    'onChange'=>'$("#jump_menu").submit()'
			],
			'pluginOptions' => [
			    'minimumInputLength' => 0,
			    'ajax' => [
				'url' => \yii\helpers\Url::to(['site-list', 'pcode'=>$pcode]),
				'dataType' => 'json',
				'data' => new JsExpression('function(params) { return {q:params.term}; }')
			    ],
			    'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
			    'templateResult' => new JsExpression('function(data) { return data.text; }'),
			    'templateSelection' => new JsExpression('function (data) { return data.text; }'),
		    ],
		    ]);?>
	    
	</div>
	
	<?php 
	}
	$authSite = $sitecode==Yii::$app->user->identity->userProfile->sitecode;
	 ActiveForm::end(); ?>
    </div>
    </p>
    
<?php Pjax::begin(['id'=>'user-grid-pjax']) ?>
    <?php echo \common\lib\sdii\widgets\SDGridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            [
	    'class' => 'yii\grid\SerialColumn',
	    'headerOptions'=>['style'=>'text-align: center;'],
	    'contentOptions'=>['style'=>'width:50px;text-align: center;'],
	    ],
	    [
		'header' => 'ชื่อ-สกุล',
		'value' => function ($model) {
		    return $model->userProfile->firstname.' '.$model->userProfile->lastname;
		},
		'contentOptions'=>['style'=>'width:190px;'],
		//'headerOptions'=>['style'=>'text-align: center;'],
		//'contentOptions'=>['style'=>'width:190px;'],//text-align: center;
	    ],
	
	    [
		'header' => 'Sitecode',
		'value' => function ($model) {
		    $sql = "SELECT all_hospital_thai.hcode, 
			    all_hospital_thai.`name`, 
			    all_hospital_thai.tambon, 
			    all_hospital_thai.amphur, 
			    all_hospital_thai.province, 
			    all_hospital_thai.code5
		    FROM all_hospital_thai
		    WHERE all_hospital_thai.hcode = :code";

		$sitecode = $model->userProfile->sitecode;
		$data = Yii::$app->db->createCommand($sql, [':code' => $sitecode])->queryOne();
		if($data){
		    $title = "{$data['name']} ต.{$data['tambon']} อ.{$data['amphur']} จ.{$data['province']}";
		    return '<a data-toggle="tooltip" title="'.$title.'">'.$sitecode.'</a>';
		}
		   return $sitecode;
		},
		'format' => 'raw',
		'headerOptions'=>['style'=>'text-align: center;'],
		'contentOptions'=>['style'=>'width:90px;text-align: center;'],
	    ],	
	    [
		'header' => 'ใบ/บัตร',
		'value' => function ($model) {
		   $userProfile = $model->userProfile;
		   $secret_file = !empty($userProfile->secret_file)?'y':'n';
		   $citizenid_file = !empty($userProfile->citizenid_file)?'y':'n';
		   return $secret_file.'/'.$citizenid_file;
		},
		//'format' => 'raw',
		'headerOptions'=>['style'=>'text-align: center;'],
		'contentOptions'=>['style'=>'width:70px;text-align: center;'],
	    ],
			[
		'header' => 'เวลา',
		'value' => function ($model) {
		    $date = Yii::$app->formatter->asDate($model->created_at, 'php:Y-m-d H:i:s');
		    $str = common\lib\sdii\components\utils\SDdate::differenceTimer($date);
		    
		    return $str;
		},
		'format' => 'raw',
		'headerOptions'=>['style'=>'text-align: center;'],
		'contentOptions'=>['style'=>'width:120px;text-align: center;'],
	    ],	
	    [
		'attribute'=>'created_at',
		'header' => 'วันที่สมัคร',
		'value' => function ($model) {
		    Yii::$app->formatter->locale = 'th';

		   return Yii::$app->formatter->asDate($model->created_at, 'short');
		},
		'headerOptions'=>['style'=>'text-align: center;'],
		'contentOptions'=>['style'=>'width:90px;text-align: center;'],
	    ],	
			[
		'header' => 'Enroll key',
		'value' => function ($model) {
		    return $model->userProfile->enroll_key;
		},
		'headerOptions'=>['style'=>'text-align: center;'],
		'contentOptions'=>['style'=>'width:90px;'],//text-align: center;
	    ],
	
	    [
		'header' => 'Active',
		'value' => function ($model) {
		    
		    if ($model->status==1) {
			return Html::button('<i class="glyphicon glyphicon-ok"></i>', [
			    'class' => 'manager-btn btn btn-xs btn-primary',
			    'data-id' => $model->id,
			    'data-url' => yii\helpers\Url::to(['status', 'id' => $model->id])
			]);
		    } else {
			return Html::button('<i class="glyphicon " style="padding-right: 6px; padding-left: 6px;"></i>',[
			    'class' => 'manager-btn btn btn-xs btn-default',
			    'data-id' => $model->id,
			    'data-url' => yii\helpers\Url::to(['status', 'id' => $model->id])
			]);
		    }
		},
		'format' => 'raw',
		'headerOptions'=>['style'=>'text-align: center;'],
		'contentOptions'=>['style'=>'width:80px;text-align: center;'],
		'visible'=>(Yii::$app->user->can('administrator') || (Yii::$app->user->can('adminsite') && $authSite))
	    ],
            //'email:email',
//            [
//                'class' => \common\grid\EnumColumn::className(),
//                'attribute' => 'status',
//                'enum' => User::getStatuses(),
//                'filter' => User::getStatuses()
//            ],
            //'created_at:datetime',
            //'logged_at:datetime',
            // 'updated_at',

            ['class' => 'yii\grid\ActionColumn',
                'header'=>'จัดการผู้ใช้',
                'template' => '{assign} {view} {update} {delete}',
                'buttons' => [

                    //view button
		    'assign' => function ($url, $model) {
			
			    return Html::a('<span class="glyphicon glyphicon-gift"></span> Assign', '#', [
                                    'title' => Yii::t('app', 'View'),
                                    'class'=>'btn btn-success btn-xs',
                                    'id'=>'nut-assigns'
			    ]);
			
                    },
                    'view' => function ($url, $model) {
			
			    return Html::a('<span class="fa fa-eye"></span> View', $url, [
                                    'title' => Yii::t('app', 'View'),
                                    'class'=>'btn btn-info btn-xs',
			    ]);
			
                    },
                    'update' => function ($url, $model) {
			$authSite = ($model->userProfile->sitecode==Yii::$app->user->identity->userProfile->sitecode);
			if(Yii::$app->user->can('administrator') || (Yii::$app->user->can('adminsite') && $authSite)){
			    return Html::a('<span class="fa fa-edit"></span> Edit', $url, [
                                    'title' => Yii::t('app', 'Update'),
                                    'class'=>'btn btn-warning btn-xs',
			    ]);
			}
                    },
                    'delete' => function ($url, $model) {
			$authSite = ($model->userProfile->sitecode==Yii::$app->user->identity->userProfile->sitecode);
			if(Yii::$app->user->can('administrator') || (Yii::$app->user->can('adminsite') && $authSite)){
			   if($model->id != Yii::$app->user->getId()){
			   return Html::a('<span class="fa fa-trash"></span> Delete', $url, [
                                    'title' => Yii::t('app', 'Delete'),
                                    'class'=>'btn btn-danger btn-xs',
                                    'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                                    'data-method' => 'post',                                    
			    ]); 
			    }
			}
                    },
                ],
                'contentOptions' => ['style' => 'width:190px;']                
            ],
        ],
    ]); ?>
<?php Pjax::end() ?>
    
<h3>รอการอนุมัติ</h3>
    <?php
    
    
    ?>
<?php  Pjax::begin(['id'=>'volunteer-job-grid-pjax']);?>
    <?=    \appxq\sdii\widgets\GridView::widget([
	'id' => 'volunteer-job-grid',
	'panelBtn' => '',
	'dataProvider' => $dp,
	//'filterModel' => $sModel,
        'columns' => [
	    
	    [
		'class' => 'yii\grid\SerialColumn',
		'headerOptions' => ['style'=>'text-align: center;'],
		'contentOptions' => ['style'=>'width:60px;text-align: center;'],
	    ],

	    [
		'attribute'=>'user_name',
		'header' => 'ชื่อ-สกุล อาสาสมัคร',
		
		//'headerOptions'=>['style'=>'text-align: center;'],
		'contentOptions'=>['style'=>'width:250px;'],
	    ],
	    [
		'attribute'=>'created_at',
		'header' => 'ผู้ป่วยที่รับบริการ',
		'value' => function ($model) {
		    
		   return "[{$model['job_cid']}] {$model['job_fname']} {$model['job_lname']}";
		},
		//'headerOptions'=>['style'=>'text-align: center;'],
		'contentOptions'=>['style'=>'width:250px;'],
	    ],
//            'job_cid',
//            'job_fname',
//            'job_lname',
	    [
		'attribute'=>'created_at',
		'header' => 'เมื่อ',
		'value' => function ($model) {
		    
		   return \common\lib\sdii\components\utils\SDdate::differenceTimer($model->created_at);
		},
		'headerOptions'=>['style'=>'text-align: center;'],
		'contentOptions'=>['style'=>'width:150px;text-align: center;'],
	    ],	
            //'job_start',
            // 'job_end',
            // 'job_type',
            // 'job_parent',
            // 'sitecode',
            // 'job_status',
            // 'job_active',
            // 'created_by',
            // 'created_at',
            // 'updated_by',
            // 'updated_at',

	    [
		'class' => 'appxq\sdii\widgets\ActionColumn',
		'contentOptions' => ['style'=>'width:80px;text-align: center;'],
		'template' => ' {update} {delete}',
		'buttons' => [
                    'update' => function ($url, $model) {
			
			    return Html::a('<span class="fa fa-edit"></span> อนุมัติ', ['update-vo', 'id'=>$model->job_id], [
                                    'title' => Yii::t('app', 'อนุมัติ'),
                                    'class'=>'btn btn-warning btn-xs',
				    'data-action' => 'update',
				'data-pjax' => isset($this->pjax_id) ? $this->pjax_id : '0',
			    ]);
			
                    },
                    'delete' => function ($url, $model) {
			
			   return Html::a('<span class="fa fa-trash"></span> ไม่อนุมัติ', ['delete-vo', 'id'=>$model->job_id], [
                                    'title' => Yii::t('app', 'ไม่อนุมัติ'),
                                    'class'=>'btn btn-danger btn-xs',
                                    'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                                    'data-method' => 'post',   
				    'data-action' => 'delete', 
			       'data-pjax' => isset($this->pjax_id) ? $this->pjax_id : '0',
			    ]); 
			    
			
                    },
                ],
	    ],
        ],
    ]); ?>
    <?php  Pjax::end();?>

</div>

<?= appxq\sdii\widgets\ModalForm::widget([
    'id' => 'modal-volunteer-job',
    'size'=>'modal-lg',
    'tabindexEnable'=>false,
]);
?>

<?php  $this->registerJs("

$('#user-grid-pjax').on('click', '.manager-btn', function(){
    updateAttr($(this).attr('data-url'));
});

$('#volunteer-job-grid-pjax').on('click', 'tbody tr td a', function() {
    var url = $(this).attr('href');
    var action = $(this).attr('data-action');

    if(action === 'update' || action === 'view') {
	modalVolunteerJob(url);
    } else if(action === 'delete') {
	yii.confirm('".Yii::t('app', 'Are you sure you want to delete this item?')."', function() {
	    $.post(
		url
	    ).done(function(result) {
		if(result.status == 'success') {
		    ". SDNoty::show('result.message', 'result.status') ."
		    $.pjax.reload({container:'#volunteer-job-grid-pjax'});
		} else {
		    ". SDNoty::show('result.message', 'result.status') ."
		}
	    }).fail(function() {
		". SDNoty::show("'" . SDHtml::getMsgError() . "Server Error'", '"error"') ."
		console.log('server error');
	    });
	});
    }
    return false;
});

function updateAttr(url) {
    $.post(
	url
    ).done(function(result){
	if(result.status == 'success'){
	    ". SDNoty::show('result.message', 'result.status') ."
	    $.pjax.reload({container:'#user-grid-pjax'});
	} else {
	    ". SDNoty::show('result.message', 'result.status') ."
	}
    }).fail(function(){
	console.log('server error');
    });
}

function modalVolunteerJob(url) {
    $('#modal-volunteer-job .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
    $('#modal-volunteer-job').modal('show')
    .find('.modal-content')
    .load(url);
}
");?>