<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;

$form = ActiveForm::begin(['action' => '/file-upload/genezform']);

$get=Yii::$app->request->get();
echo Html::activeHiddenInput($model,'ezf_id',['value' => $get['id']]);
echo Html::activeHiddenInput($model,'excelFile',['value' => $model->excelFile->name]);
?>
<button>สร้าง EZForm ด้วยข้อมูลนี้</button>

<?php ActiveForm::end() ?>


<?php
    $objPHPExcel = \PHPExcel_IOFactory::load(Yii::$app->basePath. '/uploads/'.urlencode($model->excelFile->name));
    
    $sheetData = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);
    $header = $sheetData[1];
    echo "<div class='table-responsive' style='height:500px'>";
    echo "<table class='table-responsive table-bordered' style='overflow: auto;'>";
    $header=true;
    foreach($sheetData as $rowkey => $row) {
        if ($header) {
            echo "<thead>";
        }else{
            echo "<tr>";
        }
        
        foreach ($row as $col => $value) {
            //$value = DateTime::createFromFormat("Y-m-d",$value);
            $time=strtotime($value);
            $newtime = date('d-m-Y',$time);

            if ($newtime <> "01-01-1970") {
                //$value=$newtime;
            }

            $newtime = date('d/m/Y',$time);

            if ($newtime <> "01/01/1970") {
                //$value=$newtime;
            }        
            if ($header) {
                echo "<th>{$value}</th>";
            }else{
                echo "<td>{$value}</td>";
            }
        }
        if ($header) {
            echo "</thead>";
            echo "<tbody>";
            $header=false;
        }else{
            echo "</tr>";
        }
    }
    echo "</tbody>";
    echo "</table>";
    echo "</div>";
    
?>