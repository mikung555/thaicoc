<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\models\UploadForm;
use kartik\widgets\FileInput;

$this->title = Yii::t('backend', 'ฟอร์ม ', [
            'modelClass' => 'Ezform',
        ]) . ':: สร้างฟอร์มจาก Excel';
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Ezforms'), 'url' => ['/ezforms/ezform']];
//$this->params['breadcrumbs'][] = ['label' => $model1->ezf_name, 'url' => ['view', 'id' => $model1->ezf_id]];
$this->params['breadcrumbs'][] = Yii::t('backend', 'สร้างฟอร์มจาก Excel');
$model = new UploadForm();
$ezf_id = $_GET["id"];
?>
    <br>
    <?= Html::a('<i class="fa fa-edit"></i>&nbsp;&nbsp;&nbsp;จัดการฟอร์ม', ['/ezforms/ezform/update', 'id' => $ezf_id], ['class' => 'btn btn-primary btn-flat']) ?>
    <?= Html::a('<i class="fa fa-comments-o"></i>&nbsp;&nbsp;&nbsp;เพิ่มคำถามพิเศษ', ['/component/ezform-component/index', 'id' => $ezf_id], ['class' => 'btn btn-primary btn-flat']) ?>
    <?= Html::a('<i class="fa fa-file-excel-o"></i>&nbsp;&nbsp;&nbsp;สร้าง EZForm จาก Excel', ['/file-upload', 'id' => $ezf_id], ['class' => 'btn btn-info btn-flat ']) ?>
    <?= Html::a('<i class="fa fa-globe"></i>&nbsp;&nbsp;&nbsp;ดูฟอร์มออนไลน์', ['/ezforms/ezform/viewform', 'id' => $ezf_id], ['class' => 'btn btn-primary btn-flat']) ?>
    <?= Html::a('<i class="fa fa-table"></i>&nbsp;&nbsp;&nbsp;ดูข้อมูล/ส่งออกข้อมูล', ['/view-data', 'id' => $ezf_id], ['class' => 'btn btn-primary btn-flat']) ?>
    <div style='float:right'> 
        <?= Html::a('<i class="fa fa-mail-reply"></i>&nbsp;&nbsp;&nbsp;กลับไปหน้าเลือกฟอร์ม', ['/ezforms/ezform/', 'id' => $ezf_id], ['class' => 'btn btn-warning btn-flat']) ?>
    </div>
    <br>
    <br>

    <div class="box">
    <div class="box-body">
<?php 
    $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]);

    echo $form->field($model, 'excelFile')->widget(FileInput::classname(), [
        'options'=>[
        'pluginOptions'=>['allowedFileExtensions'=>['xls','xlsx','csv']]]
    ]);

    //echo $form->field($model,'ezf_id')->hiddenInput();
    //$request = Yii::$app->request;
    //$get = $request->get();
    
    //echo Html::activeHiddenInput($model,'ezf_id',['value' => $get['id']]);
    
    ActiveForm::end();
    
?>
    </div></div>