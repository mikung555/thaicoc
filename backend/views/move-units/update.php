<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\MoveUnits */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Move Units',
]) . ' ' . $model->muid;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Move Units'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->muid, 'url' => ['view', 'id' => $model->muid]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="move-units-update">

    <?= $this->render('_form_approved', [
        'model' => $model,
    ]) ?>

</div>
