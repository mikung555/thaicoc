<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\helpers\Url;
use common\lib\sdii\widgets\SDGridView;
use common\lib\sdii\widgets\SDModalForm;
use common\lib\sdii\components\helpers\SDNoty;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\MoveUnitsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'ขอย้ายหน่วยงาน');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="move-units-index">

    <?php  Pjax::begin(['id'=>'move-units-grid-pjax']);?>
    <?= SDGridView::widget([
	'id' => 'move-units-grid',
	'panelBtn' => '',//Html::button(Yii::t('app', '<span class="glyphicon glyphicon-plus"></span>'), ['data-url'=>Url::to(['move-units/create']), 'class' => 'btn btn-success btn-sm', 'id'=>'modal-addbtn-move-units'])
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
	    'user',
            'user_name',
	    'user_lname',
            'sitecode_old',
            'sitecode_new',
            'status',
	    [
		    'attribute'=>'updated_at',
		    'value'=>function ($data){return common\lib\sdii\components\utils\SDdate::mysql2phpDate($data['updated_at']);},
		    'headerOptions'=>['style'=>'text-align: center;'],
		    'contentOptions'=>['style'=>'width:100px;text-align: center;'],
		    'filter'=>'',
	    ],
            // 'file:ntext',
            // 'comment:ntext',
            // 'created_by',
            // 'created_at',
            // 'updated_by',
            // 'updated_at',

            [
		'class' => 'common\lib\sdii\widgets\SDActionColumn',
		'template'=>'{update}',
	    ],
        ],
    ]); ?>
    <?php  Pjax::end();?>

</div>

<?=  SDModalForm::widget([
    'id' => 'modal-move-units',
    'size'=>'modal-lg',
]);
?>

<?php  $this->registerJs("
$('#move-units-grid-pjax').on('click', '#modal-addbtn-move-units', function(){
modalMoveUnit($(this).attr('data-url'));
});

$('#move-units-grid-pjax').on('dblclick', 'tbody tr', function() {
    var id = $(this).attr('data-key');
    modalMoveUnit('".Url::to(['move-units/update', 'id'=>''])."'+id);
});	

$('#move-units-grid-pjax').on('click', 'tbody tr td a', function() {
    var url = $(this).attr('href');
    var action = $(this).attr('data-action');

    if(action === 'update' || action == 'view'){
	modalMoveUnit(url);
    } else if(action === 'delete') {
	yii.confirm('".Yii::t('app', 'Are you sure you want to delete this item?')."', function(){
	    $.post(
		url
	    ).done(function(result){
		if(result.status == 'success'){
		    ". SDNoty::show('result.message', 'result.status') ."
		    $.pjax.reload({container:'#move-units-grid-pjax'});
		} else {
		    ". SDNoty::show('result.message', 'result.status') ."
		}
	    }).fail(function(){
		console.log('server error');
	    });
	})
    }
    return false;
});
	
function modalMoveUnit(url) {
    $('#modal-move-units .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
    $('#modal-move-units').modal('show')
    .find('.modal-content')
    .load(url);
}

");?>