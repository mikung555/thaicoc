<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

//$this->title = $name;
?>
<div class="error">
    <div class="row">
        <div class="col-xs-12">
            <div class="error-content text-center">
                <h3 class="headline">
                    <?php 
                    
			$code = property_exists($exception, 'statusCode') ? $exception->statusCode : 500;
			$msg = Yii::$app->keyStorage->get('error_403');
			if($code==403 && !empty($msg)){
			    $this->title = ' ';
			} else {
			    $this->title = $name;
			    
			    echo Yii::t(
			    'backend',
			    'Error {code}',
			    [
				'code' => $code
			    ]);
			    
			}
                    ?>
                </h3>
                <p>
                    <?php 
                    
		    if($code==403 && !empty($msg)){
			echo '<h1>รออนุมัติสิทธิ์การใช้งาน</h1><br>';
			$sitecode = Yii::$app->user->identity->userProfile->sitecode;
			$data = common\models\UserProfile::getAdminsite($sitecode);
			if($data){
			    $comma = '';
		    ?>
		
		    <strong>กรุณาติดต่อ Admin site ของท่านดังนี้</strong><br>
		    <?php foreach ($data as $key => $value):?>
			<?= $comma?> <?= $value['fullname']?> &nbsp; <strong>Email:</strong> <?= $value['email']?> &nbsp; <strong>เบอร์ติดต่อ</strong> <?= $value['telephone']?>
			<?php $comma = ',<br>';?>
		    <?php endforeach;?>
		    <br>
		    <br>
		    <?php
			}
			echo $msg;
			
			?>
		    <br>
		    <br>
		    <a href="<?=  yii\helpers\Url::to(['/sign-in/profile'])?>">Upload เอกสารรักษาความลับ</a>
		    <br><br>
		<div style="color: #ff0000">หากไม่มีเอกสารรักษาความลับ ท่านจะไม่สามารถเข้าใช้งานในระบบ</div>
		<?php
		$dataComment = \backend\models\UserComment::find()
		->where('user_id=:id', [':id'=>Yii::$app->user->id])->limit(1)
		->orderBy('update_at DESC')
		->one();
		
		$approved = $dataComment['action'];
		$icon = 'style=" color:#333;"';
		$css = 'heart';
		$title = 'ยังไม่มีการตรวจสอบ';
		if($approved===0){
		    $icon = 'style="color:#d73925;"';
		    $title = 'ไม่อนุมัติ';
		} elseif ($approved===1) {
		    $icon = 'style="color:#008d4c;"';
		    $title = 'อนุมัติ';
		} elseif ($approved===2) {
		    $icon = 'style="color:#e08e0b;"';
		    $title = 'รอเอกสาร';
		} elseif ($approved===3) {
		    $icon = 'style="color:#d73925;"';
		    $css = 'remove';
		    $title = 'มอบสิทธิ์โดย Site admin';
		} elseif ($approved===4) {
		    $icon = 'style="color:#e8889e;"';
		    $title = 'อัพโหดไฟล์ใหม่';
		} 

		?>
		<br>
		<div>
		    
		    <h4 class="media-heading"> <small>
				 <span <?=$icon?>>สถานะ: <i class="glyphicon glyphicon-<?=$css?>"></i> <?=$title?></span> </small></h4>
		    หมายเหตุ: <?=$dataComment['comment']?>
		      
		    
		</div>
		    <?php
		    } elseif($code==403 && empty($msg)){
                        Yii::$app->user->logout();
                        setcookie("CloudUserID", Yii::$app->user->identity->userProfile->user->username, time()-1, "/", Yii::$app->keyStorage->get('frontend.domain'), false);
        
                        Yii::$app->getResponse()->refresh();
                    } else {
			echo nl2br(Html::encode($message));
		    }
		    
		    ?>
                </p>
            </div>
        </div>
    </div>
</div><!-- /.error-page -->