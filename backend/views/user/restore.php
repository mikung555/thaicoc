<?php

use common\models\User;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use common\lib\sdii\components\helpers\SDNoty;
use yii\bootstrap\ActiveForm;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'Restore Users');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index" >

    <?php //echo $this->render('_search', ['model' => $searchModel]); ?>

  <?php
  Yii::$app->session['admindb'] = \yii\helpers\ArrayHelper::getColumn($dataAdminsite, 'sitecode');
  //yii\helpers\VarDumper::dump(Yii::$app->session['admindb']);
  ?>
   
    <p>
    <?php $form = ActiveForm::begin([
        'action' => ['index2'],
        'method' => 'get',
	'options' => ['class'=>'form-inline text-right']
    ]); ?>
	
    <?php 
    
    
    echo '<div class="form-group">';
    echo '<label for="usersearch-username">ค้นหา</label> ';
    echo Html::activeTextInput($searchModel, 'username', ['style'=>'width: 300px;', 'class'=>'form-control', 'placeholder'=>'username, ชื่อ-สกุล และ sitecode']);
    echo '</div>';
    echo ' <div class="form-group">';
    echo '<label for="usersearch-username">บทบาท</label> ';
    echo Html::activeDropDownList($searchModel, 'role', \backend\modules\article\components\ArticleFunc::itemAlias('signup'),['class'=>'form-control', 'prompt'=>'All']);
    echo '</div>';
    echo ' <div class="form-group">';
    echo '<label for="usersearch-username">จำนวนวัน</label> ';
    echo Html::activeTextInput($searchModel, 'difftime', ['style'=>'width: 100px;', 'class'=>'form-control', 'type'=>'number', 'min'=>0]);
    echo '</div>';
    echo ' <div class="form-group">';
    echo Html::activeCheckbox($searchModel, 'approve');
    echo '</div>';
    echo ' <div class="form-group">';
    echo Html::activeCheckbox($searchModel, 'notfile');
    echo '</div>';
    ?>
    
    <?php echo Html::submitButton(Yii::t('backend', 'ค้นหา'), ['class' => 'btn btn-primary']) ?>
	
    <?php ActiveForm::end(); ?>
    </p>
<?php Pjax::begin(['id'=>'user-grid-pjax']) ?>
    <?php echo \common\lib\sdii\widgets\SDGridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            [
	    'class' => 'yii\grid\SerialColumn',
	    'headerOptions'=>['style'=>'text-align: center;'],
	    'contentOptions'=>['style'=>'width:50px;text-align: center;'],
	    ],
	    [
		'header' => 'ชื่อ-สกุล',
		'value' => function ($model) {
		    return $model->userProfile->firstname.' '.$model->userProfile->lastname;
		},
		//'headerOptions'=>['style'=>'text-align: center;'],
		//'contentOptions'=>['style'=>'width:190px;'],//text-align: center;
	    ],
	    [
		'header' => 'Admin?',
		'value' => function ($model) {
		    $sitecode = $model->userProfile->sitecode;
		    if(in_array($sitecode, Yii::$app->session['admindb'])){
			return '<i class="glyphicon glyphicon-ok"></i>';
		    }
		    return '<i class="glyphicon glyphicon-remove-sign"></i>';
		},
		'format' => 'raw',
		'headerOptions'=>['style'=>'text-align: center;'],
		'contentOptions'=>['style'=>'width:90px;text-align: center;'],
	    ],	
	    [
		'header' => 'Sitecode',
		'value' => function ($model) {
		    $sql = "SELECT all_hospital_thai.hcode, 
			    all_hospital_thai.`name`, 
			    all_hospital_thai.tambon, 
			    all_hospital_thai.amphur, 
			    all_hospital_thai.province, 
			    all_hospital_thai.code5
		    FROM all_hospital_thai
		    WHERE all_hospital_thai.hcode = :code";
		
		$sitecode = $model->userProfile->sitecode;
		$data = Yii::$app->db->createCommand($sql, [':code' => $sitecode])->queryOne();
		if($data){
		    $title = "{$data['name']} ต.{$data['tambon']} อ.{$data['amphur']} จ.{$data['province']}";
		    return '<a data-toggle="tooltip" title="'.$title.'">'.$sitecode.'</a>';
		}
		   return $sitecode;
		},
		'format' => 'raw',
		'headerOptions'=>['style'=>'text-align: center;'],
		'contentOptions'=>['style'=>'width:90px;text-align: center;'],
	    ],	
	    [
		'header' => 'ใบ/บัตร',
		'value' => function ($model) {
		   $userProfile = $model->userProfile;
		   $secret_file = !empty($userProfile->secret_file)?'y':'n';
		   $citizenid_file = !empty($userProfile->citizenid_file)?'y':'n';
		   return $secret_file.'/'.$citizenid_file;
		},
		//'format' => 'raw',
		'headerOptions'=>['style'=>'text-align: center;'],
		'contentOptions'=>['style'=>'width:70px;text-align: center;'],
	    ],	
		[
		'header' => 'เวลา',
		'value' => function ($model) {
		    $date = Yii::$app->formatter->asDate($model->created_at, 'php:Y-m-d H:i:s');
		    $str = common\lib\sdii\components\utils\SDdate::differenceTimer($date);
		    
		    return $str;
		},
		'format' => 'raw',
		'headerOptions'=>['style'=>'text-align: center;'],
		'contentOptions'=>['style'=>'width:120px;text-align: center;'],
	    ],		
	    [
		'attribute'=>'created_at',
		'header' => 'วันที่สมัคร',
		'value' => function ($model) {
		    Yii::$app->formatter->locale = 'th';

		   return Yii::$app->formatter->asDate($model->created_at, 'short');
		},
		'headerOptions'=>['style'=>'text-align: center;'],
		'contentOptions'=>['style'=>'width:90px;text-align: center;'],
	    ],	
			[
		'header' => 'Enroll key',
		'value' => function ($model) {
		    return $model->userProfile->enroll_key;
		},
		'headerOptions'=>['style'=>'text-align: center;'],
		'contentOptions'=>['style'=>'width:90px;'],//text-align: center;
	    ],
	
            ['class' => 'yii\grid\ActionColumn',
                'header'=>'จัดการผู้ใช้',
                'template' => '{restore}',
                'buttons' => [

                    //view button
                    'view' => function ($url, $model) {
			
			    return Html::a('<span class="fa fa-eye"></span> View', $url, [
                                    'title' => Yii::t('app', 'View'),
                                    'class'=>'btn btn-info btn-xs',
			    ]);
			
                    },
                    'update' => function ($url, $model) {
			if(Yii::$app->user->can('administrator') || Yii::$app->user->can('adminsite')){
			    return Html::a('<span class="fa fa-edit"></span> Edit', $url, [
                                    'title' => Yii::t('app', 'Update'),
                                    'class'=>'btn btn-warning btn-xs',
			    ]);
			}
                    },
                    'delete' => function ($url, $model) {
			if(Yii::$app->user->can('administrator') || Yii::$app->user->can('adminsite')){
			   return Html::a('<span class="fa fa-trash"></span> Delete', $url, [
                                    'title' => Yii::t('app', 'Delete'),
                                    'class'=>'btn btn-danger btn-xs',
                                    'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                                    'data-method' => 'post',                                    
			    ]); 
			}
                    },
			'restore' => function ($url, $model) {
			if(Yii::$app->user->can('administrator') || Yii::$app->user->can('adminsite')){
			   return Html::a('<span class="fa fa-trash"></span> Restore', $url, [
                                    'title' => Yii::t('app', 'Restore'),
                                    'class'=>'btn btn-warning btn-xs',
                                    'data-confirm' => Yii::t('yii', 'Are you sure you want to restore this item?'),
                                    'data-method' => 'post',                                    
			    ]); 
			}
                    },    
                ],
                'contentOptions' => ['style' => 'width:90px;']                
            ],
        ],
    ]); ?>
<?php Pjax::end() ?>
</div>

<?php  $this->registerJs("

$('#user-grid-pjax').on('click', '.manager-btn', function(){
    updateAttr($(this).attr('data-url'));
});

function updateAttr(url) {
    $.post(
	url
    ).done(function(result){
	if(result.status == 'success'){
	    ". SDNoty::show('result.message', 'result.status') ."
	    $.pjax.reload({container:'#user-grid-pjax'});
	} else {
	    ". SDNoty::show('result.message', 'result.status') ."
	}
    }).fail(function(){
	console.log('server error');
    });
}
");?>