<?php

use Yii;
use yii\widgets\Pjax;
use common\lib\sdii\widgets\SDGridView;
use yii\helpers\Html;
use common\lib\sdii\components\helpers\SDNoty;
use yii\bootstrap\ActiveForm;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'Users Tools');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-tools" >
<p>
    <?php $form = ActiveForm::begin([
        'action' => ['tool'],
        'method' => 'get',
	//'options' => ['class'=>'form-inline']
    ]); ?>

    
<div class="form-group">
    <div class="row">
	<div class="col-md-6">
	    <?php 
	    $termView = iconv('TIS-620', 'UTF-8', $term);
	    ?>
	    <?=  Html::textInput('term', $termView, ['class'=>'form-control', 'placeholder' => 'ค้นหาจาก username และ ชื่อ-สกุล',])?>
	</div>
	<div class="col-md-6">
	    <?=	kartik\widgets\Select2::widget([
		'name' => 'sitecode',
		'initValueText' => $sitelist['name'],
		'value'=>$sitecode,
		'options' => [
		    'placeholder' => 'ค้นหาหน่วยงาน ...',
		    'onChange'=>'$("#jump_menu").submit()'
		],
		'pluginOptions' => [
		    'allowClear' => true,
		    'minimumInputLength' => 0,
		    'ajax' => [
			'url' => \yii\helpers\Url::to(['site-list', 'pcode'=>$pcode]),
			'dataType' => 'json',
			'data' => new JsExpression('function(params) { return {q:params.term}; }')
		    ],
		    'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
		    'templateResult' => new JsExpression('function(data) { return data.text; }'),
		    'templateSelection' => new JsExpression('function (data) { return data.text; }'),
	    ],
	    ]);?>
	</div>
    </div>
    
    </div>
    
<div class="form-group">
    <?php echo Html::submitButton(Yii::t('backend', 'ค้นหา'), ['class' => 'btn btn-primary btn-block']) ?>
</div>
    
	
    <?php ActiveForm::end(); ?>
    </p>
  <?php //echo $this->render('_search', ['model' => $searchModel]); ?>

  <?php  Pjax::begin(['id'=>'user-tool-grid-pjax']);?>
    <?= SDGridView::widget([
	'id' => 'user-tool-grid',
	'panelBtn' => '',
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn','contentOptions'=>['style'=>'text-align: center; width:60px;'],],
            'username',
	    [
		'attribute' => 'cid',
		'label' => 'CID',
	    ],
	    [
		'header' => 'ชื่อ-สกุล',
		'value' => function ($data) {
		    $name = $data['title'].$data['name'].' '.$data['surname'];
		    return iconv('TIS-620', 'UTF-8', $name);
		},
		//'headerOptions'=>['style'=>'text-align: center;'],
		//'contentOptions'=>['style'=>'width:190px;'],//text-align: center;
	    ],
	    [
		'attribute' => 'Email',
		'label' => 'E-mail',
	    ],	
	    [
		'header' => 'Sitecode',
		'value' => function ($data) {
		    $sql = "SELECT all_hospital_thai.hcode, 
			    all_hospital_thai.`name`, 
			    all_hospital_thai.tambon, 
			    all_hospital_thai.amphur, 
			    all_hospital_thai.province, 
			    all_hospital_thai.code5
		    FROM all_hospital_thai
		    WHERE all_hospital_thai.hcode = :code";
		
		$sitecode = $data['OrganizationName'];
		$dataHos = Yii::$app->db->createCommand($sql, [':code' => $sitecode])->queryOne();
		if($dataHos){
		    $title = "{$dataHos['name']} ต.{$dataHos['tambon']} อ.{$dataHos['amphur']} จ.{$dataHos['province']}";
		    return '<a data-toggle="tooltip" title="'.$title.'">'.$sitecode.'</a>';
		}
		   return $sitecode;
		},
		'format' => 'raw',
		'headerOptions'=>['style'=>'text-align: center;'],
		'contentOptions'=>['style'=>'width:90px;text-align: center;'],
	    ],	
	    [
		'header' => 'Import',
		'value' => function ($data) {
		    $model = \common\models\User::find()->where('username=:username', [':username'=>$data['username']])->one();
		    if($model){
			return 'นำเข้าแล้ว';
		    }
		    
		    return Html::button('<i class="glyphicon glyphicon-transfer"></i> Import',[
			'class' => 'import-btn btn btn-xs btn-primary',
			'data-id' => $data['pid'],
			'data-url' => yii\helpers\Url::to(['import-user', 'id' => $data['pid']])
		    ]);
		},
		'format' => 'raw',	
		'headerOptions'=>['style'=>'text-align: center;'],
		'contentOptions'=>['style'=>'width:120px;text-align: center;'],
	    ],
	[
		'header' => 'Import File',
		'value' => function ($data) {
		    $model = \common\models\User::find()->where('username=:username', [':username'=>$data['username']])->one();
		    if($model){
			return Html::button('<i class="glyphicon glyphicon-upload"></i> Import File',[
			    'class' => 'import-btn btn btn-xs btn-success',
			    'data-id' => $data['pid'],
			    'data-url' => yii\helpers\Url::to(['import-file', 'id' => $data['pid'], 'uid'=>$model['id']])
			]);
		    }
		    return 'โปรดนำเข้าข้อมูล';
		},
		'format' => 'raw',	
		'headerOptions'=>['style'=>'text-align: center;'],
		'contentOptions'=>['style'=>'width:160px;text-align: center;'],
	    ],		
        ],
    ]); 
//		[
//		'attribute'=>'aa',
//		'label'=>'aa',
//		'value'=>function ($data){ return $data['aa']; },
//		'headerOptions'=>['style'=>'text-align: center;'],
//		'contentOptions'=>['style'=>'width:100px; text-align: center;'],
//],
		?>
    <?php  Pjax::end();?>
   
</div>

<?php  $this->registerJs("
$('#user-tool-grid-pjax').on('click', '.import-btn', function(){
    updateAttr($(this).attr('data-url'));
});

function updateAttr(url) {
    $.post(
	url
    ).done(function(result){
	if(result.status == 'success'){
	    ". SDNoty::show('result.message', 'result.status') ."
	    $.pjax.reload({container:'#user-tool-grid-pjax'});
	} else {
	    ". SDNoty::show('result.message', 'result.status') ."
	}
    }).fail(function(){
	console.log('server error');
    });
}
");?>