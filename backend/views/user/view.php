<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\User */

$this->title = $model->getPublicIdentity();
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-view">

    <p>
        <?php echo Html::a(Yii::t('backend', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?php echo Html::a(Yii::t('backend', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('backend', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?php
    echo DetailView::widget([
	'model' => $model,
	'attributes' => [
	    //'id',
	    'username',
	    //'auth_key',
	    //'password_reset_token',
	    //'status',
	    'userProfile.cid',
	    [
		'label' => 'ชื่อ',
		'value' => $model->userProfile->firstname . ' ' . $model->userProfile->lastname,
	    ],
	    [
		'label' => 'เพศ',
		'value' => ($model->userProfile->gender == 1 ? 'ชาย' : ($model->userProfile->gender == 2 ? 'หญิง' : '?')),
	    ],
	    [
		'format' => 'html',
		'label' => 'รูปประจำตัว',
		'value' => '<img width="100" src="' . $model->userProfile->avatar_base_url . '/' . $model->userProfile->avatar_path . '" class="img-circle img-thumbnail img-responsive">',
	    ],
	    [
		'format' => 'html',
		'label' => 'เอกสารรักษาความลับ',
		'value' => '<a target="_blank" href="' . Yii::getAlias('@storageUrl') . '/source/' . $model->userProfile->citizenid_file . '">' . $model->userProfile->citizenid_file . '</a>',
	    ],
	    [
		'format' => 'html',
		'label' => 'สำเนาบัตรประชาชน',
		'value' => '<a target="_blank" href="' . Yii::getAlias('@storageUrl') . '/source/' . $model->userProfile->secret_file . '">' . $model->userProfile->secret_file . '</a>',
	    ],
	    'userProfile.telephone',
	    'email:email',
	    'userProfile.sitecode',
	    [
		'label' => 'บทบาท',
		'value' => ($model->userProfile->status == 0 ? 'ผู้รับบริการหรือบุคคลทั่วไป' : ($model->userProfile->status == 1 ? 'บุคลากร' : '?')),
	    ],
	    [
		'format' => 'html',
		'label' => 'บทบาทเฉพาะสังกัด',
		'value' => getPerson($model->userProfile),
	    ],
	    'created_at:datetime',
	    'updated_at:datetime',
	    'logged_at:datetime',
	],
    ]);

    function getPerson($model) {
	$html = '';
	if ($model->status_personal == 2) {
	    $html .= " ผู้ดูแลระบบ";
	} else if ($model->status_personal == 3) {
	    $html .= " ผู้ให้บริการด้านการแพทย์และสาธารณสุข";
	} else if ($model->status_personal == 4) {
	    $html .= " ผู้บริหาร => ";
	    if ($model->status_manager == 5) {
		$html .= "หน่วยงานระดับประเทศ => ";
		$html .= "ชื่อหน่วยงาน " . $model->department_nation_text;
	    } else if ($model->status_manager == 6) {
		$html .= "หน่วยงานระดับเขต => ";
		$html .= "ชื่อหน่วยงาน " . $model->department_area_text;
	    } else if ($model->status_manager == 7) {
		$html .= "หน่วยงานระดับจังหวัด => ";
		$html .= "ชื่อหน่วยงาน " . $model->department_province_text;
	    } else if ($model->status_manager == 8) {
		$html .= "หน่วยงานระดับอำเภอ => ";
		$html .= "ชื่อหน่วยงาน " . $model->department_amphur_text;
	    } else if ($model->status_manager == 9) {
		$html .= "หน่วยบริการทางการแพทย์และสาธารณสุข => ";
		$html .= "ชื่อกลุ่มงาน " . $model->department_group_text;
	    }
	} else if ($model->status_personal == 10) {
	    $html .= " นักวิจัย";
	} else if ($model->status_personal == 11) {
	    $html .= $model->status_other;
	}
	return $html;
    }
    ?>

</div>
