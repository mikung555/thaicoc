<?php
/**
 * Created by PhpStorm.
 * User: gundamx
 * Date: 11/19/2015 AD
 * Time: 12:49 AM
 */

use yii\helpers\Html;
?>

<div class="media">
    <hr>
    <div class="media-left">
        <a href="#">
            <img class="media-object" alt="64x64" src="<?php echo Yii::$app->user->identity->userProfile->getAvatar() ?: '/img/anonymous.jpg' ?>" data-holder-rendered="true" style="width: 64px; height: 64px;">
        </a>
    </div>
    <div class="media-body">
        <?= \yii\widgets\DetailView::widget([
            'model' => $model,
            'attributes' => [
                //'id',
                'ezf_id',
                'target_id',
                //'data_id',
                'ezf_field_id',
                //'old_val:ntext',
                //'new_val:ntext',
                //'note:ntext',
                'status',
                'user_create',
                //'user_update',
                'time_create:dateTime',
                //'time_update:dateTime',
                [
                    'attribute' => 'reply_note',
                    'label'=>'เหตุผล',
                    'format'=>'text',
                    'value' =>$model->reply_note,
                ],
                [
                    'attribute' => 'file_upload',
                    'label'=>'File Upload',
                    'format'=>'raw',
                    'value' =>($model->file_upload ? \backend\models\QueryReply::lookUpfile($model->file_upload) : 'ไม่ได้แนบไฟล์'),
                ],
            ],
        ]) ?>
    </div>
</div>
