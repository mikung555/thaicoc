<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\QueryRequest */
/* @var $form yii\widgets\ActiveForm */
?>
<style>
    .datepicker{
        z-index: 3000 !important;
    }
</style>
<div class="query-approved-form">

    <?php $form = ActiveForm::begin(['id'=>$model->formName(), 'options' => ['enctype' => 'multipart/form-data']]); ?>
    <div class="modal-header">
        <button type="button" class="close closeViewResolution" >&times;</button>
        <h1 class="modal-title" id="itemModalLabel">Query resolution</h1>
    </div>

    <div class="modal-body">

        <?php // echo $form->field($model, 'note')->textarea(['rows' => 6, ])->label('สาเหตุที่ต้องการแก้ไข') ?>

        <?php
//        if($queryManager->user_id){
//            echo Html::button('<i class="fa fa-pencil-square-o"></i> ' . 'Approve', ['class' => 'btn btn-danger btn-md', 'onclick' => '$(\'#query-status\').toggle();']);
//        }
        if($queryManager->user_id) {
            $items = [
                //'1' => 'Waiting',
                '2' => 'Resolve with some change',
                '3' => 'Resolve without any change',
                '4' => 'Unresolvable',
                //'5' => 'Remark',
            ];
            echo '<br><br>'.$form->field($model, 'status')->radioList($items, ['id' => 'query-status'])->label(false);
        ?>
            <div id="QueryRequest-new_changed">
                <?php //echo \yii\bootstrap\Html::button('คัดลอกค่าที่ต้องการแก้ไข', ['onclick'=>'$("#queryrequest-new_changed").val(\''.$model->new_val.'\');', 'class'=>'btn btn-success btn-xs']) ?>
                <?php //$form->field($model, 'new_changed')->textarea(['rows' => 4, ]);
                $options_input['query_tools_render'] = 2;
                $options_input['query_tools_input'] = 9;
                echo \backend\modules\ezforms\components\EzformFunc::getTypeEform($model_gen, $ezf_field, $form, $options_input);
                ?>
            </div>
        <?php
        } ?>

        <?= Html::hiddenInput('ezf_field_type', $ezf_field->ezf_field_type, []);?>
        <?= Html::hiddenInput('ezf_field_name', $ezf_field->ezf_field_name, []);?>
        <?php // echo  $form->field($model, 'user_create')->textInput(['maxlength' => true]) ?>

        <?php // echo $form->field($model, 'user_update')->textInput(['maxlength' => true]) ?>

        <?php // echo $form->field($model, 'time_create')->textInput() ?>

        <?php // echo $form->field($model, 'time_update')->textInput() ?>

        <div id="QueryRequest-approved_note">
            <?= $form->field($model, 'approved_note')->textarea(['rows' => 5, ]) ?>
            <?= $form->field($model, 'note[]')->widget(\kartik\widgets\FileInput::classname(),[
                'pluginOptions' => [
                    'allowedFileExtensions'=>['pdf','png','jpg','jpeg'],
                    'showRemove' => false,
                    'showUpload' => false,
                ],
                'options' => ['multiple' => true]
            ])->label('File Upload'); ?>
        </div>


    </div>

    <div class="modal-footer">
        <div class="form-group">
            <?= Html::submitButton('Submit', ['id' => 'btn-query', 'class' => 'btn btn-success btn-lg','data-url' => yii\helpers\Url::to(['/query-request/approved-query'])]) ?>
            <?= Html::button('Cancel', ['class' => 'btn btn-danger btn-lg closeViewResolution']); ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php
$this->registerJs('$(function() { $("div[id=\'QueryRequest-new_changed\']").find(\'input,select,textarea\').prop(\'disabled\', true); });');
$this->registerJs("

$('form#{$model->formName()}').on('submit', function(e) {
    e.preventDefault();
    var \$form = $(this);
    var url = \$form.attr('action');
        var data = \$form.serialize();
         $.ajax({
            url:url,
            method:'POST',
            dataType:'json',
            data:data,
            success:function(data){
                " . \appxq\sdii\helpers\SDNoty::show('data.message', 'data.status') . ";
                $('#modal-query-resolution').modal('hide');
                $('#saveValue').val('1');
            },
            error:function(error){
                " . \appxq\sdii\helpers\SDNoty::show("'" . \appxq\sdii\helpers\SDHtml::getMsgError() . "'", '"error"') . ";
                $('#modal-query-resolution').modal('hide');
                $('#saveValue').val('1');
            }
        });
        return false;
});

$('.closeViewResolution').on('click',function(){
    $('#modal-query-resolution').modal('hide');
});

//$(\"#query-status\").on('change', function() {
//    var status = $('input[name=\"QueryRequest[status]\"]:checked').val();
//    if(status==1){
//        $('#QueryRequest-new_changed').hide();
//        $('#QueryRequest-approved_note').hide();
//        //$('#btn-query').text('Query');
//    }
//    else if(status==2){
//        $('#QueryRequest-new_changed').fadeIn();
//        //$('#QueryRequest-approved_note').fadeIn();
//        //$('#btn-query').text('Query');
//    }
//    else if(status==3){
//        $('#QueryRequest-new_changed').hide();
//        //$('#QueryRequest-approved_note').fadeIn();
//        //$('#btn-query').text('Query');
//    }
//    else if(status >=4){
//        $('#QueryRequest-new_changed').hide();
//        //$('#QueryRequest-approved_note').fadeIn();
//        //$('#btn-query').text('Respond');
//    }
//});
//$(function() {
//    $('#QueryRequest-new_changed').hide();
// });
");
?>
