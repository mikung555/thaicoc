<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\ReportDynamicSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Dynamics Report';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="report-dynamic-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Report Dynamic', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'titlename',
            'detail:ntext',
            //'sqlcommand:ntext',
            [
            'label'=>'Custom Link',
            'format'=>'raw',
            'value' => function($data){
                $url = "http://www.bsourcecode.com";
                return Html::a('Report Link', $url, ['title' => 'Go']);
            }
        ],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
