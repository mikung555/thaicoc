<?php

/**
 * Created by PhpStorm.
 * User: hackablex
 * Date: 7/26/2016 AD
 * Time: 13:23
 */
use kartik\grid\GridView;
use yii\widgets\Pjax;
use kartik\helpers\Html;
use yii\helpers\Url;
use kartik\select2\Select2;
use kartik\depdrop\DepDrop;

//$this->title = 'Morbidity & Mortality ';
?>

<?php // Html::a("<i class='glyphicon glyphicon-arrow-left'></i> Back", Url::to(['surgery-form/data-management', 'ezfdata' => '1490342148003178500']), ['class' => 'btn btn-primary']) ?>
<a href="/dpm/menu/index" class="btn btn-success" role="button"><i class="fa fa-cubes" aria-hidden="true"></i> All Module</a>
<br>
<h2 style="text-align: left;"><span style="color:"#000000";"><span lang="TH">Morbidity&Motality&nbsp;</span></h2> 
<a href='<?= Url::to(['patient/index']) ?>' class="btn btn-info" role="button"> บันทึกจำนวนผู้ป่วยใน และ Abstract ของผู้ป่วย M&M </a>
<a href='<?= Url::to(['data-managementstat/index']) ?>' class="btn btn-info" role="button"> จำนวนผู้ป่วยประจำปี M&M แจกแจงตาม Admission Discharge Morbidity Maltality และ Summary </a>
<a href='<?= Url::to(['nmm-report/index']) ?>' class="btn btn-info" role="button"> จำนวนผู้ป่วยประจำเดือน แจกแจงตาม Cause of complication และ MM Grading </a>

<?php //$ezfdata['detail'];?>

<hr>
<h3>บันทึกจำนวนผู้ป่วยใน และ Abstract ของผู้ป่วย M&M</h3>

<hr>
<h4>เลือกช่วงเวลาแสดงข้อมูล</h4>
<div class="row">
    <?php Pjax::begin(['id' => 'pjax-content']) ?>
    <?php
    $form = yii\bootstrap\ActiveForm::begin([
                'id' => 'searhForm',
                'method' => 'GET',
                'action' => Url::to(['patient/index']),
                'options' => ['data-pjax' => true]
            ])
    ?>
    <div class="col-sm-3 col-md-3 col-lg-3">
        <?=
        Select2::widget([
            'name' => 'ezdata',
            'value' => $ezdata,
            'data' => [
                '1471505524053879200' => 'จำนวนผู้ป่วยใน',
                '1490342148003178500' => 'Abstract ของผู้ป่วย M&M'
            ],
            'hideSearch' => true,
        ])
        ?>
    </div>

    <div class="col-sm-2 col-md-2 col-lg-2">
        <?=
        Select2::widget([
            'name' => 'month',
            'id' => 'month',
            'value' => empty($_GET['month']) ? date('m') : $_GET['month'],
            'data' => $dataMonth,
            'hideSearch' => true,
        ])
        ?>
    </div>

    <div class="col-sm-2 col-md-2 col-lg-2">
        <?=
        Select2::widget([
            'name' => 'year',
            'id' => 'year',
            'value' => empty($_GET['year']) ? date('Y') : $_GET['year'],
            'data' => $years,
        ])
        ?>
    </div>

    <div class="col-sm-3 col-md-3 col-lg-3">
        <?php
        echo Html::hiddenInput('weeks', $_GET['week'], ['id' => 'weeks', 'disabled' => 'disabled']);

        echo DepDrop::widget([
            'name' => 'week',
            'type' => kartik\widgets\DepDrop::TYPE_SELECT2,
            'options' => [
                'id' => 'week',
            ],
            'pluginOptions' => [
                'depends' => ['month', 'year', 'weeks'],
                'initialize' => true,
                //'initDepends' => ['month', 'year', 'weeks'],
                'params' => ['month', 'year', 'weeks'],
                'placeholder' => 'select week...',
                'url' => Url::to(['patient/get-week']),
            ]
        ]);
        ?>
    </div>
    <div class="col-sm-2 col-md-2 col-lg-2">
        <?php
        echo yii\helpers\Html::submitButton("<i class='glyphicon glyphicon-search'></i> Search", ['class' => 'btn btn-primary'])
        ?>
    </div>
    <?php yii\bootstrap\ActiveForm::end(); ?>

  
    <center>
        <label style="font-size:18px; margin-top: 30px;">จำนวนผู้ป่วยในและผู้ป่วย M&M ระหว่างวันที่ <font color="blue"><?= $_GET['week'] ?> </font></label>
    </center>
    
    <div class="col-sm-12 col-md-12 col-lg-12" style="margin-top: 10px;">
        <?php
        if ($ezdata == '1471505524053879200') {

            echo GridView::widget([
                'id' => 'patientIn',
                'dataProvider' => $dataProvider,
                'layout' => '{items}{pager}',
                'columns' => [
                        [
                        'attribute' => 'department',
                        'label' => 'หน่วย',
                        'headerOptions' => ['style' => 'text-align: center;', 'width' => '15%'],
                    //'contentOptions' => ['style' => 'text-align: center;'],
                    ],
                        [
                        'attribute' => 'admit',
                        'label' => 'Admit',
                        'headerOptions' => ['style' => 'text-align: center;'],
                        'contentOptions' => ['style' => 'text-align: center;'],
                    ],
                        [
                        'attribute' => 'dis',
                        'label' => 'Discharge',
                        'headerOptions' => ['style' => 'text-align: center;'],
                        'contentOptions' => ['style' => 'text-align: center;'],
                    ],
                        [
                        'attribute' => 'morbi',
                        'label' => 'Morbidity',
                        'headerOptions' => ['style' => 'text-align: center;'],
                        'contentOptions' => ['style' => 'text-align: center;'],
                    ],
                        [
                        'attribute' => 'mortal',
                        'label' => 'Mortality',
                        'headerOptions' => ['style' => 'text-align: center;'],
                        'contentOptions' => ['style' => 'text-align: center;'],
                    ],
                        [
                        'attribute' => 'patho',
                        'label' => 'Patho',
                        'headerOptions' => ['style' => 'text-align: center;', 'width' => '25%'],
                        'contentOptions' => ['style' => 'text-align: center;'],
                    ]
                ]
            ]);
        } else if ($ezdata == '1490342148003178500') {
            echo GridView::widget([
                'id' => 'patientMM',
                'dataProvider' => $dataProvider,
                'layout' => '{items}{pager}',
                'columns' => [
                        [
                        'attribute' => 'hn',
                        'label' => 'HN',
                        'headerOptions' => ['style' => 'text-align: center;'],
                        'contentOptions' => ['style' => 'text-align: center;'],
                    ],
                        [
                        'attribute' => 'nage',
                        'label' => 'อายุ',
                        'headerOptions' => ['style' => 'text-align: center;'],
                        'contentOptions' => ['style' => 'text-align: center;'],
                    ],
                        [
                        'attribute' => 'nward',
                        'label' => 'Ward',
                        'headerOptions' => ['style' => 'text-align: center;'],
                        'contentOptions' => ['style' => 'text-align: center;'],
                    ],
                        [
                        'attribute' => 'npostdx',
                        'label' => 'Pre-op Diagnosis',
                        'format' => 'raw',
                        'headerOptions' => ['style' => 'text-align: center;'],
//                        'contentOptions' => ['style' => 'text-align: center;'],
                    ],
                        [
                        'attribute' => 'sergeon1',
                        'label' => 'อาจารย์',
                        'headerOptions' => ['style' => 'text-align: center;'],
                        'contentOptions' => ['style' => 'text-align: center;'],
                    ]
                ],
            ]);
        }
        ?>
    </div>

    <?php Pjax::end(); ?>


</div>






