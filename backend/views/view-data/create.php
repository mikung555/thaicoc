<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Tbdata */

$this->title = 'Create Tbdata';
$this->params['breadcrumbs'][] = ['label' => 'Tbdatas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tbdata-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
