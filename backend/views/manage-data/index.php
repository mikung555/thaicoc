<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'การจัดการข้อมูล';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="ezform-index">


    <p>
        <?php //echo Html::a('Create Ezform', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?=
    GridView::widget([
        'dataProvider' => $myezform,
        'caption' => '<h3>รายการฟอร์มเก็บข้อมูลของท่าน</h3>',
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'ezf_id',
            'ezf_name',
            'ezf_detail:ntext',
            //'ezf_table',
            'user_create',
             'create_date',
            // 'user_update',
            // 'update_date',
            // 'status',
            // 'shared',
            // 'public_listview',
            // 'public_edit',
            // 'public_delete',
            // 'comp_id_target',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]);
    ?>

</div>
