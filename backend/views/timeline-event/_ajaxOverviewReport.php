<?php
/**
 * Created by PhpStorm.
 * User: Mark and Tan
 * Date: 3/3/2016
 * Time: 1:07 PM
*/
    use yii\helpers\Html;
    use yii\helpers\Url;

    $this->registerCss('

        .fadeout{
            opacity :0;
        }

        #overview-report-table{
            transition: opacity 0.3s ease;
        }

		#overview-report-table tr>td:last-child, #overview-report-table tr>th:last-child{
			border-right: 1px solid #f4f4f4;
		}

		.overview-report-header:nth-child(1) th{
			text-align: center !important;
		}

		.overview-report-header h4{
			margin-bottom: 0;
		}

		.overview-report-header th{
			vertical-align: middle !important;
			text-align: right;
			background-color: #f4f4f4 !important;
			border: 1px solid #f9f9f9 !important;
		}

		.overview-report-header th:first-child{
			text-align: left;
		}

		.overview-report-header td{
			border-left: 1px solid #f4f4f4;
		}

		.overview-report-header td:first-child{
			border-left: 0 !important;
		}

		.overview-report-row>td:first-child{
			padding-left:50px;
			text-align: left;
		}

		.overview-report-row>td{
			vertical-align: middle !important;
			text-align: right;
			border-left: 1px solid #f4f4f4;
		}

		td[drillable="true"]{
			cursor: pointer;
			color: #337ab7;
		}

		/*td[drillable="false"]{
			color: #337ab7;
		}*/

		td[drillable="true"]:hover{
			text-decoration: underline;
			color: #3c8dbc;
		}

		.overview-report-plus{
			vertical-align: middle;
			cursor: pointer;
		}

		.table-left{
			text-align: left;
			vertical-align: middle;
		}
		.table-right{
			text-align: right;
			vertical-align: middle;
		}
		.table-left-td{
			padding-left: 50px;
		}
		.table-center{
			text-align: center;
			vertical-align: middle;
		}

		#report-overview-daterange > label{
		    font-size: 15px;
            padding: 5px;
            border: 1px solid #d9d9d9;
            border-radius: 5px;
		}
		#report-overview-info{
		    margin-bottom: 0px !important;
		}
	');

    $this->registerJs('

        $("[drillable=\'true\']").each(function(){
            $(this).attr( "tmp-title" , $(this).attr("title") );
        });

		$("[drillable=\'true\']").click(function(){
			var formData = {};
			formData["filter"] = $(this).attr("overview-filter");

			var thisParent = $(this).parent();

			formData["rstat"] = thisParent.attr("overview-rstat");
			formData["table"] = thisParent.attr("overview-form-table");
			formData["date"] = thisParent.attr("overview-table-date");
			formData["hsitecode"] = $("#overview-report-table").attr("sitecode");


            $("#report-overview-modal-head").html( $(this).attr("tmp-title").replace("จำนวน","")+"<br/>"+$("#in-site-title").html() );

			$("#report-overview-modal-btn").click();

			$.ajax({
				type    : "GET",
				cache   : false,
				url     : "'.Url::to('/timeline-event/get-list-pt/').'",
				data    : formData,
				success  : function(response) {
					//console.log(response);
					$("#report-overview-modal-div").html(response);
                    $(".drilldown-emr > td.perform-click-emr").attr("title","ดูรายการข้อมูลทั้งหมดของรายนี้").css("cursor","pointer");
					$(".drilldown-emr > td.perform-click-emr").off().click(function(){
                        $(this).parent().find("td:last-child > a")[0].click();
                    });

					//$("#inputProvence").html(response);

				},
				error : function(){
				    $("#report-overview-modal-div").html("การเรียกดูข้อมูลผิดพลาด");
					//$("#inputProvence").html("<option>Error</option>");

				}
			});
		});
                
                function sortReport(val){
                
                    var formData = {};
			formData["filter"] = $(this).attr("overview-filter");

			var thisParent = $(this).parent();

			formData["rstat"] = thisParent.attr("overview-rstat");
			formData["table"] = thisParent.attr("overview-form-table");
			formData["date"] = thisParent.attr("overview-table-date");
			formData["hsitecode"] = $("#overview-report-table").attr("sitecode");


            //$("#report-overview-modal-head").html( $(this).attr("tmp-title").replace("จำนวน","")+"<br/>"+$("#in-site-title").html() );

			$("#report-overview-modal-btn").click();

			$.ajax({
				type    : "GET",
				cache   : false,
				url     : "'.Url::to('/timeline-event/get-list-pt/').'",
				data    : {
                                xxx:1,
                                yyy:2,
                                },
				success  : function(response) {
					//console.log(response);
					$("#report-overview-modal-div").html(response);
                    $(".drilldown-emr > td.perform-click-emr").attr("title","ดูรายการข้อมูลทั้งหมดของรายนี้").css("cursor","pointer");
					$(".drilldown-emr > td.perform-click-emr").off().click(function(){
                        $(this).parent().find("td:last-child > a")[0].click();
                    });

					//$("#inputProvence").html(response);

				},
				error : function(){
				    $("#report-overview-modal-div").html("การเรียกดูข้อมูลผิดพลาด");
					//$("#inputProvence").html("<option>Error</option>");

				}
			});
                }
	');

    $thaiMonth = ['',
        1=>['abvt'=>'ม.ค.', 'full'=>'มกราคม'],
        2=>['abvt'=>'ก.พ.', 'full'=>'กุมภาพันธ์'],
        3=>['abvt'=>'มี.ค.', 'full'=>'มีนาคม'],
        4=>['abvt'=>'เม.ย.', 'full'=>'เมษายน'],
        5=>['abvt'=>'พ.ค.', 'full'=>'พฤษภาคม'],
        6=>['abvt'=>'มิ.ย.', 'full'=>'มิถุนายน'],
        7=>['abvt'=>'ก.ค.', 'full'=>'กรกฎาคม'],
        8=>['abvt'=>'ส.ค.', 'full'=>'สิงหาคม'],
        9=>['abvt'=>'ก.ย.', 'full'=>'กันยายน'],
        10=>['abvt'=>'ต.ค.', 'full'=>'ตุลาคม'],
        11=>['abvt'=>'พ.ย.', 'full'=>'พฤศจิกายน'],
        12=>['abvt'=>'ธ.ค.', 'full'=>'ธันวาคม']
    ];

    $plusIcon = '<i class="fa fa-plus-circle fa-2x" style="color:#00a65a;"></i>';

    $siteCode = Yii::$app->user->identity->userProfile->sitecode;
//    $siteCode = '13777';
    $sqlHospital = "SELECT `hcode`,`name` FROM all_hospital_thai WHERE hcode='".($siteCode)."' ";
    $dataHospital = Yii::$app->db->createCommand($sqlHospital)->queryOne();

    $queryOverviewControl = Yii::$app->db->createCommand('SELECT * FROM report_overview_control WHERE sitecode="'.$siteCode.'"')->queryOne();

    $lastcalDateTime = $queryOverviewControl['lastcal'];

    $lastcalTime = explode(' ',$lastcalDateTime)[1];
    $lastcalDate = explode('-',explode(' ',$lastcalDateTime)[0]);

    $lastcalDateShow = intval($lastcalDate[2]).' '.$thaiMonth[intval($lastcalDate[1])]['full'].' '.(intval($lastcalDate[0])+543);

    $presentDate = explode('-',date('Y-m-d'));
    $this->registerJs('
        $("#report-overview-lastcal").html("สถานะข้อมูลล่าสุด เมื่อวันที่ '.$lastcalDateShow.' เวลา '.$lastcalTime.'");
        $("#report-overview-daterange").html("<label>ข้อมูลระหว่างวันที่ 9 กุมภาพันธ์ 2556 ถึงวันที่ '.intval($presentDate[2]).' '.$thaiMonth[intval($presentDate[1])]['full'].' '.(543+intval($presentDate[0])).'</label>");
    ');

    $overviewForm = [
        'cca01'=>[
            'label'=>'ลงทะเบียนกลุ่มเสี่ยง (CCA-01)',
            'title'=>'CCA-01',
            'table'=>'tb_data_2',
            'form_id'=>'1437377239070461302',
            'date'=>'f1vdcomp'
        ],
        'ov01'=>[
            'label'=>'ตรวจรักษาพยาธิใบไม้ตับ (OV-01)',
            'table'=>'',
            'form_id'=>'',
            'date'=>''
        ],
        'ov01k'=>[
            'label'=>'OV-01K (Kato-Katz)',
            'title'=>'OV-01K',
            'table'=>'tbdata_21',
            'form_id'=>'1455214361078703000',
            'date'=>'exdate'
        ],
        'ov01p'=>[
            'label'=>'OV-01P (Parasep)',
            'title'=>'OV-01P',
            'table'=>'tbdata_22',
            'form_id'=>'1455214361078703001',
            'date'=>'exdate'
        ],
        'ov01f'=>[
            'label'=>'OV-01F (FECT)',
            'title'=>'OV-01F',
            'table'=>'tbdata_23',
            'form_id'=>'1455214361078703002',
            'date'=>'exdate'
        ],
        'ov01u'=>[
            'label'=>'OV-01U (Urine)',
            'title'=>'OV-01U',
            'table'=>'tbdata_24',
            'form_id'=>'1455214361078703003',
            'date'=>'exdate'
        ],
        'ov02'=>[
            'label'=>'ปรับเปลี่ยนพฤติกรรมเสี่ยง (OV-02)',
            'title'=>'OV-02',
            'table'=>'tbdata_25',
            'form_id'=>'1455257503082760700',
            'date'=>'vdate'
        ],
        'ov03'=>[
            'label'=>'ให้การรักษาพยาธิ (OV-03)',
            'title'=>'OV-03',
            'table'=>'tbdata_26',
            'form_id'=>'1455222779086150700',
            'date'=>'vdate'
        ],
        'cca02'=>[
            'label'=>'ตรวจคัดกรองมะเร็งท่อน้ำดี (CCA-02)',
            'title'=>'CCA-02',
            'table'=>'tb_data_3',
            'form_id'=>'1437619524091524800',
            'date'=>'f2v1'
        ],
        'cca021'=>[
            'label'=>'ตรวจ CT/MRI (CCA-02.1)',
            'title'=>'CCA-02.1',
            'table'=>'tb_data_4',
            'form_id'=>'1454041742064651700',
            'date'=>'f2p1v1'
        ],
        'cca03'=>[
            'label'=>'ให้การรักษา (CCA-03)',
            'title'=>'CCA-03',
            'table'=>'tb_data_7',
            'form_id'=>'1451381257025574200',
            'date'=>'f3v4dvisit'
        ],
        'cca04'=>[
            'label'=>'ตรวจพยาธิวิทยา (CCA-04)',
            'title'=>'CCA-04',
            'table'=>'tb_data_8',
            'form_id'=>'1452061550097822200',
            'date'=>'f4complete'
        ],
        'cca05'=>[
            'label'=>'ติดตามผลการรักษามะเร็งท่อน้ำดี (CCA-05)',
            'title'=>'CCA-05',
            'table'=>'tb_data_11',
            'form_id'=>'1440515302053265900',
            'date'=>'f5v1a1'
        ],
    ];

    $sql = 'SELECT
        (SELECT SUM(count_all) FROM report_overview_all WHERE form_name = "{FORMKEY}") AS isan_all,
        SUM(count_all) AS count_all,
        SUM(count_year) AS count_year,
        SUM(count_month) AS count_month,
        SUM(count_week) AS count_week,
        SUM(count_today) AS count_today
    FROM
        report_overview_site
    WHERE sitecode = "{SITECODE}"
    AND form_name = "{FORMKEY}"
    UNION ALL
    SELECT
        (SELECT SUM(count_all) FROM report_overview_all WHERE form_name = "{FORMKEY}" AND form_stat = 1) AS isan_all,
        count_all AS count_all,
        count_year AS count_year,
        count_month AS count_month,
        count_week AS count_week,
        count_today AS count_today
    FROM
        report_overview_site
    WHERE sitecode = "{SITECODE}"
    AND form_name = "{FORMKEY}"
    AND form_stat = 1
    UNION ALL
    SELECT
        (SELECT count_all FROM report_overview_all WHERE form_name = "{FORMKEY}" AND form_stat = 2) AS isan_all,
        count_all AS count_all,
        count_year AS count_year,
        count_month AS count_month,
        count_week AS count_week,
        count_today AS count_today
    FROM
        report_overview_site
    WHERE sitecode = "{SITECODE}"
    AND form_name = "{FORMKEY}"
    AND form_stat = 2';

    $drillable = false;
    $drillable = true;

    echo '<table class="table table-hover" id="overview-report-table" sitecode="'.$siteCode.'">';
        echo '<tr class="overview-report-header">';
            echo '<th colspan="2"></th>';
            echo '<th colspan="5" id="in-site-title">เฉพาะใน '.$siteCode.' [ '.$dataHospital['name'].' ]</th>';
        echo '</tr>';

        foreach($overviewForm as $formKey => $formDetail){
            if($formKey=='ov01'){
                echo '<tr class="overview-report-header">';
                    echo '<th colspan="7"><h4>'.$formDetail['label'].'</h4></th>';
                echo '</tr>';
                continue;
            }

            echo '<tr class="overview-report-header">';
                echo '<th>';
                    echo '<h4>'.$formDetail['label'].' ';
                        if($drillable){
                            echo Html::a(
                                '<label class="overview-report-plus" title="เพิ่มข้อมูลใหม่ในฟอร์ม '.$formDetail['title'].'">'.$plusIcon.'</label>',
                                Url::to('/inputdata/step2?comp_id_target=1437725343023632100&target=&ezf_id='.$formDetail['form_id']),
                                ['target'=>'blank']
                            );
                        }

                        //echo '<label>'.$plusIcon.'</label>';
                    echo '</h4>';
                echo '</th>';
                echo '<th>ทั้งหมดใน Isan Cohort</th>';
                echo '<th>ทั้งหมด</th>';
                echo '<th>ปีนี้</th>';
                echo '<th>เดือนนี้</th>';
                echo '<th>สัปดาห์นี้</th>';
                echo '<th>วันนี้</th>';
            echo '</tr>';

            $tempSql = str_replace('{FORMKEY}',$formKey,$sql);
            $tempSql = str_replace('{SITECODE}',$siteCode,$tempSql);

            $queryOverviewReportAll = Yii::$app->db->createCommand($tempSql)->queryAll();

            if($drillable){
                $drillDownRowAttr = ' overview-form-id="'.$formDetail['form_id']. '"
                    overview-form-table="'.$formDetail['table'].'"
                    overview-table-date="'.$formDetail['date'].'" ';
            }else{
                $drillDownRowAttr = '';
            }

//            echo "<tr><td>".(Yii::$app->db->createCommand($tempSql)->rawSql)."</td></tr>";
            foreach($queryOverviewReportAll as $key => $value){
                if($key==0){
                    $label = 'All';
                    $rstatAttr = ' overview-rstat="all" ';
                }else if($key==1){
                    $label = 'Save Draft';
                    $rstatAttr = ' overview-rstat="draft" ';
                }else if($key==2){
                    $label = 'Submitted';
                    $rstatAttr = ' overview-rstat="submitted" ';
                }

                if(!$drillable)$rstatAttr = '';

                echo '<tr class="overview-report-row"'.$drillDownRowAttr.$rstatAttr.'">';

                    echo '<td class="">'.$label.'</td>';
                    echo '<td title="จำนวนข้อมูล '.$formDetail['title'].' ทั้งหมดที่มีการบันทึกใน Isan Cohort">'.number_format($value['isan_all']).'</td>';

                    if($drillable&& $value['count_all']>0){
                        $drillAttr = ' drillable="true" overview-filter="all" title="จำนวนข้อมูล '.$formDetail['title'].' ('.$label.')'.' ที่มีการบันทึกทั้งหมด" ';
                    }else{
                        //$drillAttr= ' drillable="false" ';
                        $drillAttr= ' ';
                    }
                    echo '<td'.$drillAttr.'>'.number_format($value['count_all']).'</td>';


                    if($drillable&& $value['count_year']>0){
                        $drillAttr = ' drillable="true" overview-filter="year" title="จำนวนข้อมูล '.$formDetail['title'].' ('.$label.')'.' ที่มีการบันทึกในช่วงปีนี้" ';
                    }else{
                        $drillAttr= ' ';
                    }
                    echo '<td'.$drillAttr.'>'.number_format($value['count_year']).'</td>';


                    if($drillable&& $value['count_month']>0){
                        $drillAttr = ' drillable="true" overview-filter="month" title="จำนวนข้อมูล '.$formDetail['title'].' ('.$label.')'.' ที่มีการบันทึกในช่วงเดือนนี้" ';
                    }else{
                        $drillAttr= ' ';
                    }
                    echo '<td'.$drillAttr.'>'.number_format($value['count_month']).'</td>';


                    if($drillable&& $value['count_week']>0){
                        $drillAttr = ' drillable="true" overview-filter="week" title="จำนวนข้อมูล '.$formDetail['title'].' ('.$label.')'.' ที่มีการบันทึกในช่วงสัปดาห์นี้" ';
                    }else{
                        $drillAttr= ' ';
                    }
                    echo '<td'.$drillAttr.'>'.number_format($value['count_week']).'</td>';


                    if($drillable&& $value['count_today']>0){
                        $drillAttr = ' drillable="true" overview-filter="today" title="จำนวนข้อมูล '.$formDetail['title'].' ('.$label.')'.' ที่มีการบันทึกในวันนี้" ';
                    }else{
                        $drillAttr= ' ';
                    }
                    echo '<td'.$drillAttr.'>'.number_format($value['count_today']).'</td>';
                echo '</tr>';
            }
        }

    echo '</table>';
?>
    <button type="button" id="report-overview-modal-btn" data-toggle="modal" data-target="#myModal" style="display: none;">Open Modal</button>
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog" style="width: 80%;">

            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" id="report-overview-modal-head"></h4>
                </div>
                <div class="modal-body">
                    <div id="report-overview-modal-div" class="table-responsive"></div>
                </div>
                <div class="modal-footer">
<!--                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
                </div>
            </div>

        </div>
    </div>
