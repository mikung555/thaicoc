<?php
use yii\helpers\Url;

/**
 * Created by PhpStorm.
 * User: Mark
 * Date: 29-Mar-16
 * Time: 16:53
 */
//use \yii\bootstrap\Modal;
//Modal::begin([
//    'header' => '<h2>Hello world</h2>',
//    'toggleButton' => ['label' => 'click me'],
//]);
echo $strTable;
//Modal::end();

$this->registerJs('
    $("#btnOut,#btnIn,#btnAll,#btnSearch" ).on("click",function(){
     var startdate = $("#startDate").val().split("/").reverse().join("-");
     var enddate = $("#endDate").val().split("/").reverse().join("-");
     var nowdate = new Date().toJSON().slice(0,10).replace(/-/g,"-");

    if(startdate<"2013-02-09")
        startdate = "2013-02-09";
    if(enddate > nowdate||enddate == "")
        enddate = nowdate;
        
    var formData = $(this).attr("formData");
    var url = "'.Url::to('/timeline-event/get-list-pt/').'";
//    console.log(url+"?"+formData+"&startdate="+startdate+"&enddate="+enddate);
               $.ajax({
				type    : "GET",
				cache   : false,
				url     : url+"?"+formData+"&startdate="+startdate+"&enddate="+enddate,
//				data    : formData,
				success  : function(response) {
					//console.log(response);
					$("#report-overview-modal-div").html(response);
                    $(".drilldown-emr > td.perform-click-emr").attr("title","ดูรายการข้อมูลทั้งหมดของรายนี้").css("cursor","pointer");
					$(".drilldown-emr > td.perform-click-emr").off().click(function(){
                        $(this).parent().find("td:last-child > a")[0].click();
                    });
					//$("#inputProvence").html(response);
				},
				error : function(){
				    $("#report-overview-modal-div").html("การเรียกดูข้อมูลผิดพลาด");
					//$("#inputProvence").html("<option>Error</option>");
				}
			});
                });
        ');
?>
