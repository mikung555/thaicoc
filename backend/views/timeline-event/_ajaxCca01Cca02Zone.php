<?php
/**
 * Created by PhpStorm.
 * User: Mark
 * Date: 3/3/2016
 * Time: 1:03 PM
 */

    use miloschuman\highcharts\Highcharts;

    if(is_null($graphSiteOrder['count']) || is_null($graphSiteOrder2['count']))exit();


        echo '<div class="col-md-6" >';
            echo Highcharts::widget([
                'setupOptions' => [
                    'lang' => [
                        'thousandsSep' => ',',
                        'loading' => 'Loading...',
                    ],
                ],
                'scripts' => [
                    'modules/exporting',
                    'themes/default',
                ],
                'options' => $graphSiteOrder['options'],
                'id'=>'cca01-'.$chartId
            ]);
        echo '</div>';

        echo '<div class="col-md-6" >';
            echo Highcharts::widget([
                'setupOptions' => [
                    'lang' => [
                        'thousandsSep' => ',',
                        'loading' => 'Loading...',
                    ],
                ],
                'scripts' => [
                    'modules/exporting',
                    'themes/default',
                ],
                'options' => $graphSiteOrder2['options'],
                'id'=>'cca02-'.$chartId
            ]);
        echo '</div>';
