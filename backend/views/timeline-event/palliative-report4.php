<?php
/**
 * Created by PhpStorm.
 * User: hackablex
 * Date: 7/12/2016 AD
 * Time: 22:45
 */
use appxq\sdii\widgets\GridView;
use yii\bootstrap\Html;
?>
<div class="row">
    <div class="col-md-6">
<?php
echo GridView::widget([
    'dataProvider'=>$dataProviderReport,
    'id'=>'gd-palliative-report3',
    // 'columns'=>$gridColumns,
    'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => ''],
    'columns'=>[
        [
            'class' => 'yii\grid\SerialColumn',
            'headerOptions' => ['style'=>'text-align: center;'],
            'contentOptions' => ['style'=>'min-width:60px;text-align: center;'],
        ],
        [
            'format' => 'text',
            'label' => 'ชื่อจังหวัด',
            'headerOptions' => ['class' => 'text-center'],
            'contentOptions' => ['class' => 'text-left'],
            'value' => function($model){
                return $model['province'];
            }
        ],
        [
            'format' => 'text',
            'label' => 'จำนวนลงทะเบียน',
            'headerOptions' => ['class' => 'text-center'],
            'contentOptions' => ['class' => 'text-center'],
            'value' => function($model){
                return $model['total'];
            }
        ],
        [
            'format' => 'text',
            'label' => 'จำนวนเข้ารับการรักษา',
            'headerOptions' => ['class' => 'text-center'],
            'contentOptions' => ['class' => 'text-center'],
            'value' => function($model) use ($totalTreatment){
                return $totalTreatment[$model['provincecode']];
            }
        ],
        [
            'label' => 'ใช้ Opioid',
            'format' => 'html',
            'headerOptions' => ['class' => 'text-center'],
            'contentOptions' => ['class' => 'text-center'],
            'value' => function($model) use ($totalSum, $date, $totalTreatment){
                $sql= "SELECT count(*) as total FROM `tbdata_2` as a
 INNER JOIN tbdata_1 as b ON a.ptid = b.ptid
	INNER JOIN all_hospital_thai as c ON c.hcode = b.xsourcex
WHERE (b.create_date BETWEEN  :date_start AND :date_end) AND c.provincecode = :provincecode  AND a.rstat <> 3 AND b.rstat <> 3 and care_event_1=1 AND c.code2 <> 'demo' ";
                if($model['pay_right'] != 0)
                    $sql .= " AND b.var70 = '{$model['pay_right']}';";
                $res = \Yii::$app->db->createCommand($sql,[':date_start' => $date['date_start'], ':date_end' =>$date['date_end'], ':provincecode' => $model['provincecode'], ])->queryOne();
                return $res['total'].'<br>'. round((($res['total'])*100)/($totalTreatment[$model['provincecode']]), 1).'%';
            }
        ],
        [
            'label' => 'ไม่ใช้',
            'format' => 'html',
            'headerOptions' => ['class' => 'text-center'],
            'contentOptions' => ['class' => 'text-center'],
            'value' => function($model) use ($date, $totalTreatment){
                $sql= "SELECT count(*) as total FROM `tbdata_2` as a
 INNER JOIN tbdata_1 as b ON a.ptid = b.ptid
	INNER JOIN all_hospital_thai as c ON c.hcode = b.xsourcex
WHERE (b.create_date BETWEEN  :date_start AND :date_end) AND c.provincecode = :provincecode  AND a.rstat <> 3 AND b.rstat <> 3 and care_event_1=2 AND c.code2 <> 'demo' ";
                if($model['pay_right'] != 0)
                    $sql .= " AND b.var70 = '{$model['pay_right']}';";
                $res = \Yii::$app->db->createCommand($sql,[':date_start' => $date['date_start'], ':date_end' =>$date['date_end'], ':provincecode' => $model['provincecode'], ])->queryOne();
                return $res['total'].'<br>'. round((($res['total'])*100)/($totalTreatment[$model['provincecode']]), 1).'%';
            }
        ],
        [
            'label' => 'ไม่ระบุ',
            'format' => 'html',
            'headerOptions' => ['class' => 'text-center'],
            'contentOptions' => ['class' => 'text-center'],
            'value' => function($model) use ($date, $totalTreatment){
                $sql="SELECT count(*) as total FROM `tbdata_2` as a
 INNER JOIN tbdata_1 as b ON a.ptid = b.ptid
	INNER JOIN all_hospital_thai as c ON c.hcode = b.xsourcex
WHERE (b.create_date BETWEEN  :date_start AND :date_end) AND c.provincecode = :provincecode  AND a.rstat <> 3 AND b.rstat <> 3 and (LENGTH(care_event_1) = 0 or care_event_1 is null) AND c.code2 <> 'demo' ";
                if($model['pay_right'] != 0)
                    $sql .= " AND b.var70 = '{$model['pay_right']}';";
                $res = \Yii::$app->db->createCommand($sql,[':date_start' => $date['date_start'], ':date_end' =>$date['date_end'], ':provincecode' => $model['provincecode'], ])->queryOne();
                return $res['total'].'<br>'. round((($res['total'])*100)/($totalTreatment[$model['provincecode']]), 1).'%';
            }
        ],
    ],
]);
?>
    </div>

        <div class="col-md-6">
<?php
echo GridView::widget([
    'dataProvider'=>$dataProviderReport,
    'id'=>'gd-palliative-report3',
    // 'columns'=>$gridColumns,
    'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => ''],
    'columns'=>[
        [
            'class' => 'yii\grid\SerialColumn',
            'headerOptions' => ['style'=>'text-align: center;'],
            'contentOptions' => ['style'=>'min-width:60px;text-align: center;'],
        ],
        [
            'format' => 'text',
            'label' => 'ชื่อจังหวัด',
            'headerOptions' => ['class' => 'text-center'],
            'contentOptions' => ['class' => 'text-left'],
            'value' => function($model){
                return $model['province'];
            }
        ],
        [
            'format' => 'text',
            'label' => 'จำนวนลงทะเบียน',
            'headerOptions' => ['class' => 'text-center'],
            'contentOptions' => ['class' => 'text-center'],
            'value' => function($model){
                return $model['total'];
            }
        ],
        [
            'format' => 'text',
            'label' => 'จำนวนที่ได้รับการติดตาม',
            'headerOptions' => ['class' => 'text-center'],
            'contentOptions' => ['class' => 'text-center'],
            'value' => function($model) use ($totalFollowup){
                return $totalFollowup[$model['provincecode']];
            }
        ],
        [
            'label' => 'การดูแลต่อเนื่องที่บ้าน',
            'format' => 'html',
            'headerOptions' => ['class' => 'text-center'],
            'contentOptions' => ['class' => 'text-center'],
            'value' => function($model) use ($date, $totalFollowup){
                $sql="SELECT count(*) as total FROM `tbdata_3` as a
INNER JOIN tbdata_1 as b ON a.ptid = b.ptid
INNER JOIN all_hospital_thai as c ON c.hcode = b.xsourcex
WHERE (b.create_date BETWEEN  :date_start AND :date_end) AND c.provincecode = :provincecode AND a.rstat <> 3 AND b.rstat <> 3 and (a.var35_1=1 or a.var35_2=1) AND c.code2 <> 'demo' ";
                if($model['pay_right'] != 0)
                    $sql .= " AND b.var70 = '{$model['pay_right']}';";
                $res = \Yii::$app->db->createCommand($sql,[':date_start' => $date['date_start'], ':date_end' =>$date['date_end'], ':provincecode' => $model['provincecode'], ])->queryOne();
                return $res['total'].'<br>'.round((($res['total'])*100)/($totalFollowup[$model['provincecode']]), 1).'%';
            }
        ],
        [
            'label' => 'ไม่ได้ติดตาม',
            'format' => 'html',
            'headerOptions' => ['class' => 'text-center'],
            'contentOptions' => ['class' => 'text-center'],
            'value' => function($model) use ($date, $totalFollowup){
                $sql="SELECT count(*) as total FROM `tbdata_3` as a
INNER JOIN tbdata_1 as b ON a.ptid = b.ptid
INNER JOIN all_hospital_thai as c ON c.hcode = b.xsourcex
WHERE (b.create_date BETWEEN  :date_start AND :date_end) AND c.provincecode = :provincecode AND a.rstat <> 3 AND b.rstat <> 3 and (a.var35_3=1) and a.var35_1<>1 and a.var35_2<>1 and a.var35_4 <> 1 AND c.code2 <> 'demo' ";
                if($model['pay_right'] != 0)
                    $sql .= " AND b.var70 = '{$model['pay_right']}';";
                $res = \Yii::$app->db->createCommand($sql,[':date_start' => $date['date_start'], ':date_end' =>$date['date_end'], ':provincecode' => $model['provincecode'], ])->queryOne();
                return $res['total'].'<br>'.round((($res['total'])*100)/($totalFollowup[$model['provincecode']]), 1).'%';
            }
        ],
        [
            'label' => 'อื่นๆ',
            'format' => 'html',
            'headerOptions' => ['class' => 'text-center'],
            'contentOptions' => ['class' => 'text-center'],
            'value' => function($model) use ($date, $totalFollowup){
                $sql="SELECT count(*) as total FROM `tbdata_3` as a
INNER JOIN tbdata_1 as b ON a.ptid = b.ptid
INNER JOIN all_hospital_thai as c ON c.hcode = b.xsourcex
WHERE (b.create_date BETWEEN  :date_start AND :date_end) AND c.provincecode = :provincecode AND a.rstat <> 3 AND b.rstat <> 3 and (a.var35_4=1) and a.var35_3<>1 and a.var35_1<>1 and a.var35_2<>1 AND c.code2 <> 'demo' ";
                if($model['pay_right'] != 0)
                    $sql .= " AND b.var70 = '{$model['pay_right']}';";
                $res = \Yii::$app->db->createCommand($sql,[':date_start' => $date['date_start'], ':date_end' =>$date['date_end'], ':provincecode' => $model['provincecode'], ])->queryOne();
                return $res['total'].'<br>'.round((($res['total'])*100)/($totalFollowup[$model['provincecode']]), 1).'%';
            }
        ],
        [
            'label' => 'ไม่ระบุ',
            'format' => 'html',
            'headerOptions' => ['class' => 'text-center'],
            'contentOptions' => ['class' => 'text-center'],
            'value' => function($model) use ($date, $totalFollowup){
                $sql="SELECT count(*) as total FROM `tbdata_3` as a
INNER JOIN tbdata_1 as b ON a.ptid = b.ptid
INNER JOIN all_hospital_thai as c ON c.hcode = b.xsourcex
WHERE (b.create_date BETWEEN  :date_start AND :date_end) AND c.provincecode = :provincecode AND a.rstat <> 3 AND b.rstat <> 3 and (LENGTH(a.var35_1)=0 and LENGTH(a.var35_2)=0 and (LENGTH(a.var35_3)=0 or a.var35_3 is null) and (LENGTH(a.var35_4)=0 or a.var35_4 is null)) AND c.code2 <> 'demo' ";
                if($model['pay_right'] != 0)
                    $sql .= " AND b.var70 = '{$model['pay_right']}';";
                $res = \Yii::$app->db->createCommand($sql,[':date_start' => $date['date_start'], ':date_end' =>$date['date_end'], ':provincecode' => $model['provincecode'], ])->queryOne();
                return $res['total'].'<br>'.round((($res['total'])*100)/($totalFollowup[$model['provincecode']]), 1).'%';
            }
        ],
    ],
]);
?>
            </div>
        </div>
