<?php
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

?>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title" id="itemModalLabel">Detail View</h4>
</div>

<div class="modal-body">
<?php
$columns = [
    [
	'class' => 'yii\grid\SerialColumn',
	'headerOptions' => ['style'=>'text-align: center;'],
	'contentOptions' => ['style'=>'min-width:60px;text-align: center;'],
    ],
];

if(isset(Yii::$app->session['sql_main_fields']['enable_field'])){
    $main_fields = \appxq\sdii\utils\SDUtility::string2Array(Yii::$app->session['sql_main_fields']['enable_field']);
    $fixFields = isset($main_fields['field'])?$main_fields['field']:[];
    $fixLabel = isset($main_fields['label'])?$main_fields['label']:[];
    $fixAlign = isset($main_fields['align'])?$main_fields['align']:[];
    $fixWidth = isset($main_fields['width'])?$main_fields['width']:[];
    $fixType = isset($main_fields['type'])?$main_fields['type']:[];
    $fixId = isset($main_fields['id'])?$main_fields['id']:[];
    $fixOrglabel = isset($main_fields['orglabel'])?$main_fields['orglabel']:[];
//	    \yii\helpers\VarDumper::dump($fixFields,10,true);
//		exit();
    foreach ($fixFields as $keyFix => $valueFix) {
	$fType = $fixType[$keyFix];

	$obj = [];
	$obj['attribute'] = $valueFix;

	if(in_array($fType, [4,6])){
	    $modelChoice = backend\modules\ezforms\models\EzformChoice::find()
		    ->where('ezf_field_id=:id',[':id'=>$fixId[$keyFix]])
		    ->all();
	    if($modelChoice){
		$obj['choice'] = ArrayHelper::map($modelChoice, 'ezf_choicevalue', 'ezf_choicelabel');
	    }
	} elseif ($fType==21) {
	    if($fixOrglabel[$keyFix]==1){
		$data = \backend\modules\inv\classes\InvQuery::getProvince();
		if($data){
		    $obj['choice'] = ArrayHelper::map($data, 'id', 'name');
		}
	    } elseif ($fixOrglabel[$keyFix]==2) {
		$data = \backend\modules\inv\classes\InvQuery::getAmphur();
		if($data){
		    $obj['choice'] = ArrayHelper::map($data, 'id', 'name');
		}
	    } elseif ($fixOrglabel[$keyFix]==3) {
		$data = \backend\modules\inv\classes\InvQuery::getDistrict();
		if($data){
		    $obj['choice'] = ArrayHelper::map($data, 'id', 'name');
		}
	    }
	} elseif ($fType==7) {
	    $obj['type'] = $fType;
	}

	$columns[] = [
		'attribute'=>$valueFix,
		'label'=>$fixLabel[$keyFix],
		'value'=>function ($data) use ($obj) { 

		    if (isset($obj['choice'])) {
			return $obj['choice'][$data[$obj['attribute']]];
		    } elseif (isset ($obj['type'])) {
			if($obj['type']==7){
			    return \common\lib\sdii\components\utils\SDdate::mysql2phpThDateSmall($data[$obj['attribute']]);
			}
		    } else {
			return $data[$obj['attribute']];
		    }
		},
		'headerOptions'=>['style'=>"text-align: {$fixAlign[$keyFix]};"],
		'contentOptions'=>['style'=>"min-width:{$fixWidth[$keyFix]}px; text-align: {$fixAlign[$keyFix]};"],
	    ];
    }

    $columns[] = [
	'header'=>Yii::$app->session['sql_main_fields']['ezf_name'],
	'format'=>'raw',
	'value'=>function ($data){ 
	    if ($data['id']!=null) {
		$icon = 'class="glyphicon glyphicon-ok"';
		$rurl = base64_encode(Yii::$app->request->url);

		return Html::a('<i '.$icon.'></i>', Url::to(['/inputdata/redirect-page', 'ezf_id'=>Yii::$app->session['sql_main_fields']['ezf_id'], 'dataid'=>$data['id'], 'rurl'=>$rurl]), [
		    'class' => 'btn-lg',
		    'data-toggle'=>'tooltip',
		    'title'=>  isset($data['detail_main'])?$data['detail_main']:'แสดงข้อมูล',
		]);
	    } else {
		return '';
	    }
	},
	'filter'=>'',
	'headerOptions'=>['style'=>'text-align: center;'],
	'contentOptions'=>['style'=>'min-width:80px; text-align: center;'],
    ];

    $columns[] = [
	'header'=>$jname,
	'format'=>'raw',
	'value'=>function ($data) use ($rurl, $ezf_id){ 
	    if ($data['id2']!=null) {
		$icon = 'class="glyphicon glyphicon-pencil"';
		
		return Html::a('<i '.$icon.'></i>', Url::to(['/inputdata/redirect-page', 'ezf_id'=>$ezf_id, 'dataid'=>$data['id2'], 'rurl'=>$rurl]), [
		    'class' => 'btn btn-warning btn-sm',
		    'data-toggle'=>'tooltip',
		    'title'=>'แก้ไขข้อมูล',
		]);
	    } else {
		return '';
	    }
	},
	'filter'=>'',
	'headerOptions'=>['style'=>'text-align: center;'],
	'contentOptions'=>['style'=>'min-width:80px; text-align: center;'],
    ];	
}
?>
<?php  Pjax::begin(['id'=>'inv-person-grid-pjax']);?>    
<?= \common\lib\sdii\widgets\SDGridView::widget([
    'id' => 'inv-person-grid',
    'panel' => false,
    'dataProvider' => $dataProvider,
    //'filterModel' => $searchModel,
    'layout' => '{summary}{pager}{items}{pager}',
    'columns' => $columns,
]); ?> 
<?php  Pjax::end();?>    
</div>
