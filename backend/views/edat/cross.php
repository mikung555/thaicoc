<?php
use yii\helpers\Html;
use yii\helpers\VarDumper;
$this->registerCssFile('/css/ezform.css');
$this->registerCssFile('/checkbox/demo/build.css');
 ?>
 <div class="sdbox-header">
     <div class="list-group-item list-group-item-info"> <font size="6">Cross-Tabulation Results</font></div>
 <div class="box box-primary box-solid">
     <div class="box-header with-border">
         <h3 class="box-title"></h3>
         <div class="box-tools pull-right">
         </div>
         </div>
         <div class="box-body" style="display: block;">
           <?php
         //VarDumper::dump($column_total,10,true);

                  $i=0; $j=0; $k=0; $m=0; $l=0;

                    foreach($f_val as $key => $value){
                    echo '<div class="table-responsive">';
                    echo '<table class="table" border="1">';
                    echo '<thead>';
                    echo '<tr class="info">';
                    echo '<th>'.$ddval.'</th>';
                  foreach($ddval_list as $value2){

                    echo '<th colspan="2">'.$value2.'</th>';

                }
                    echo '<th colspan="2">Row Total</th>';
                    echo '</tr>';
                    echo '<tr class="active">';
                    echo '<th>'.$key.'</th>';

                    foreach($ddval_list as $value2){

                      echo '<th>Number</th>';
                      echo '<th>Percent</th>';

                    }
                      echo '<th>Number</th>';
                      echo '<th>Percent</th>';
                    echo '</tr>';
                    echo '</thead>';
                    echo '<tbody>';

                    $totalpercent = ($fre_total[0]/$fre_total[0])*100;
                      foreach($fre_val[$i++] as $value4){
                        $pernumber = ($value4['countval']/$fre_total[0])*100;
                    echo '<tr>';
                    echo '<td>'.$value4['listval'].'</td>';
                    foreach($ddval_list as $value2){

                        $per_cross = ($cross_val[$m++][0]/$column_total[$l++][0])*100;
                    echo '<td>'.$cross_val[$k++][0].'</td>';
                    echo '<td>'.number_format($per_cross,2,'.','').' %</td>';

                }
                    echo '<td>'.$value4['countval'].'</td>';
                    echo '<td>'.number_format($pernumber,2,'.','').' %</td>';
                    echo '</tr>';
                  }

                    echo '<tr class="success">';
                    echo '<td>Column Total</td>';

                    foreach($ddval_list as $value2){

                    echo '<td>'.$column_total[$j++][0].'</td>';
                    echo '<td>'.number_format($totalpercent,2,'.','').' %</td>';

                }
                    echo '<td>'.$fre_total[0].'</td>';
                    echo '<td>'.number_format($totalpercent,2,'.','').' %</td>';
                    echo '</tr>';
                    echo '</tbody>';

                    echo '</table>';
                    echo '<div>';
                    echo '<br>';
                  }
            ?>
         </div><!-- /.box-body -->
     </div>
     </div>
