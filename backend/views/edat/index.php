<?php

use yii\helpers\Html;
use yii\web\JsExpression;
use kartik\grid\GridView;
use yii\bootstrap\ActiveForm;
use common\lib\sdii\widgets\SDModalForm;

$this->registerJsFile('/jqueryloading/jloading.js',['depends' => [\yii\web\JqueryAsset::className()]]);

$this->title = 'EDAT : Exploratory Data Analysis Tools';
$this->params['breadcrumbs'][] = $this->title;
$this->registerCssFile('/css/ezform.css');
$this->registerCssFile('/checkbox/demo/build.css');

$this->registerJs('$(document).ready(function () {
   
    $("#SHOW").hide();
    $("#selectall").click(function () {
        $(".selectedId").prop("checked", this.checked);
    });
    
    $(".selectedId").change(function () {
        var check = ($(".selectedId").filter(":checked").length == $(".selectedId").length);
        $("#selectall").prop("checked", check);
    });
    
  
    

});');

$this->registerJS(' $(document).ready(function(){
        $("#show-var").click(
            function(){
                $(".js-tbvalue").toggle();
            }
        )
        
       $("#btn-dis").click(
           function(){
            $(".js-tbvalue").hide();
           }
           
       )
       
       $("#show4").click(
          function(){
             $("#SHOW").show();
          }
       )
       
        $("#show1").click(
          function(){
             $("#SHOW").hide();
          }
       ) 
       
       $("#show2").click(
          function(){
             $("#SHOW").hide();
          }
       ) 
       
        $("#show3").click(
          function(){
             $("#SHOW").hide();
          }
       )
       
       })');



?>


<div class="edat-index">
  <div class='row'>
    <div class='col-md-2'>
        <button class="btn btn-primary" style="width: 127px;" >Select Type</button>
        <br/><br/>
    </div>
      
    <div class='col-md-7'>
            <label class="radio-custom-label">
                <input type="radio" name="edattype" id="show1" value="1"   checked> 1 Frequency
                
            </label>

            <label class="radio-custom-label">
                <input type="radio" name="edattype" id="show2"    value="2"> 2 Summary
             
            </label>
         
            <label class="radio-custom-labelradio-inline">
                <input type="radio" name="edattype" id="show3"  value="3"> 3 List Records
               
            </label>
         
             <label class="radio-custom-label">
                 <input type="radio" name="edattype" id="show4"  value="4"> 4 Cross-Tabulation With
             </label>  
           
        <label class="btn btn-info" id="SHOW"  >
            <select class="form-control" style="styp:" id="dropdownId">
              <?php
                //$params=Yii::$app->request->queryParams;
                //$ezfid=$params['id'];
                $countfield = Yii::$app->db->createCommand("SELECT COUNT(DISTINCT COLUMN_NAME) AS countfield FROM information_schema.columns WHERE table_name ='$tablename'")->query()->readAll();

                if (count($ezfield)>0) {
                    foreach($ezfield as $key => $form) {
                      foreach ($form as $value) { ?>
                        <option value="<?= $value ?>"><?= $value ?></option>
                      <?php
                      }
                    }
                  }
                  ?>
            </select>
        </label>
       

    </div>
    <div class='col-md-3'>
      <button class="btn btn-success" id="btn-dis">Display Result</button>
   </div>
      
      <div id="progressbar"></div>
</div>
<div class='row'>
  <div class='col-md-2'>
    <!--  <h4>Select variables: </h4>-->
    <button class="btn btn-info" id="show-var">Select Variables</button>
    <br/>
</div>
  <div class='col-md-7'>
        <!--<h5>This table has a total of <?php echo $countfield[0]['countfield'];  ?> variables</h5>-->
      <div class="list-group-item list-group-item-info" >This Table has a total of <?php echo $countfield[0]['countfield'];  ?> Variables </div>
  </div>
    
  <!-- <div class='col-md-3'>
<button type="button" name="Check" class="check btn btn-success" onclick="Checkall()" class="type2">Select All</button>
<button type="button" name="Uncheck" class="btn btn-success" onclick="Uncheckall()" class="type2">Deselect All</button>
</div> -->
</div>
<!-- Select All  -->
<div class="row">
    <div class='col-md-2 '><button class="btn btn-warning js-tbvalue" style="width: 125px;">Select All  <input type="checkbox" class="js-tbvalue" id="selectall"  checked></button> </div>
<div class='col-md-7'></div>
</div>

<hr>
<!-- Select All Test-->
<!-- <div class="row"><div class="col-md-4">test1</div>
<div class="col-md-4">test2</div>
<div class="col-md-4">test3</div></div>
-->
<?php
$num=1;

      //$params=Yii::$app->request->queryParams;
      //$ezfid=$params['id'];
      //$ezfield = Yii::$app->db->createCommand("SELECT COLUMN_NAME FROM information_schema.columns WHERE table_name ='tbdata_".$ezfid."'")->query()->readAll();
      if (count($ezfield)>0) {
              //echo "<input type='checkbox' class='selectall'> ";
          foreach($ezfield as $key => $form) {
            foreach ($form as $value) {
     /*
              echo "<div class='row'>";
              echo "<div class='col-md-6 js-tbvalue'>";

              echo "<input type='checkbox' class='selectedId' name='.$value.' checked> ";

              echo $value;
              echo "<br>";
              echo "</div>";
              echo "</div>";
              # code...

        */
              switch ($num) {
                case '1':
                  echo '<div class="row"><div class="container"><div class="col-md-4 js-tbvalue"><input type="checkbox" class="selectedId" name="'.$value.'" checked>' .$value.' </div>';
                  $num++;
                  break;
                  case '2':
                  echo '<div class="col-md-4 js-tbvalue"><input type="checkbox" class="selectedId" name="'.$value.'" checked>' .$value.' </div>';
                    $num++;
                    break;
                    case '3':
                  echo '<div class="col-md-4 js-tbvalue"><input type="checkbox" class="selectedId" name="'.$value.'" checked>' .$value.' </div></div></div>';
                      $num=1;
                  break;
                
              }//switch

            }//foreach
              //$columns[$aindex]=$form['COLUMN_NAME'];
              //$aindex++;
          }
      }
 ?>
 <div class="result-edat">
 </div>
 <div class="result-summary">
 </div>
 <div class="result-list">
 </div>
 <div class="result-cross">
 </div>
</div>

<?php
  $this->registerJs('
   $( "#btn-dis" ).on( "click", function displayResult() {
        var val = [];
        var val2 = [];
        var index=0;
        var val3 = "'.$tablename.'";
        var ddval=$("#dropdownId").val();
        var sltype=$("input[name=edattype]:checked").val();
           
        $(".js-tbvalue").each(function(){
          if($(this).find(".selectedId").is(":checked")){
            val[index]=$(this).find(".selectedId").is(":checked");
            val2[index]=$(this).text();
            index++;
          }
          
        });
           if(sltype==1){
             $.blockUI({ message: "กำลังโหลดข้อมูล" });
             $.post("/edat/frequency",{val:val, val2:val2, val3:val3},function(frequency_data){
                   $( ".result-summary" ).html("");
                   $( ".result-list" ).html("");
                   $( ".result-cross" ).html("");
                   $( ".result-edat" ).html( frequency_data );
                 $.unblockUI();
             });
          
           }
           if(sltype==2){
              $.blockUI({ message: "กำลังโหลดข้อมูล" });
             $.post("/edat/summary",{val:val, val2:val2, val3:val3},function(summary_data){
                  $(".result-edat").html("");
                  $( ".result-list" ).html("");
                  $( ".result-cross" ).html("");
                  $( ".result-summary" ).html( summary_data );
              $.unblockUI();
           });
           }
           if(sltype==3){
           $.blockUI({ message:"กำลังโหลดข้อมูล"});
             $.post("/edat/list",{val:val, val2:val2, val3:val3},function(list_data){
                  $(".result-edat").html("");
                  $( ".result-summary" ).html("");
                  $( ".result-cross" ).html("");
                  $( ".result-list" ).html( list_data );
              $.unblockUI();
           });
           }
           if(sltype==4){
             $.blockUI({ message:"กำลังโหลดข้อมูล"});
             $.post("/edat/cross",{val:val, val2:val2, val3:val3, ddval:ddval},function(cross_data){
                  $(".result-edat").html("");
                  $( ".result-summary" ).html("");
                  $( ".result-list" ).html("");
                  $( ".result-cross" ).html( cross_data );
             $.unblockUI();    
           });
           }
         });

    ');
?>
