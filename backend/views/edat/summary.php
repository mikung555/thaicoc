<?php
use yii\helpers\Html;
use yii\helpers\VarDumper;
$this->registerCssFile('/css/ezform.css');
$this->registerCssFile('/checkbox/demo/build.css');
 ?>
 <div class="sdbox-header">
     <div class="list-group-item list-group-item-info"> <font size="6">Summary Results</font></div>
 <div class="box box-primary box-solid">
     <div class="box-header with-border">
         <h3 class="box-title">Continues Data ตัวอย่างเช่น[Age->อายุ]</h3>
         <div class="box-tools pull-right">
         </div>
         </div>
         <div class="box-body" style="display: block;">
           <?php $i=0; $j=0;
           //  VarDumper::dump($varstat,10,true);
                        
                       foreach($varname as $key => $value) {
                       echo '<div >';
                       echo '<table class="table" border="1">';
                       echo '<thead>';
                                 echo '<tr class="info">';
                                 echo '<th> Variable Name </th>';
                                 echo '<th> Minimum </th>';
                                 echo '<th> Maximum </th>';
                                 echo '<th> Mean </th>';
                                 echo '<th> Median </th>';
                                 echo '<th>Standard Deviation </th>';
                                 echo '</tr>';
                       echo '</thead>';
                       echo '<tbody>';
                       foreach ($varstat[$i] as $key2 => $value2) {
                       echo '<tr>';
                       echo '<td> '.$key.' </td>';
                       echo '<td> '.$value2['minval'].' </td>';
                       echo '<td> '.$value2['maxval'].' </td>';
                       echo '<td> '.$value2['meanval'].' </td>';
                      // echo '<td> '.number_format($median_val[$j++][0],2,'.','').' </td>';
                       echo '<td> '.$value2['median_val'].' </td>';
                       echo '<td> '.$value2['sdval'].' </td>';
                       echo '</tr>';
                     }
                       echo '</tbody>';
                       echo '</table>';
                       echo '</div>';
                       echo "<br>";
                     $i++;}
                       ?>
           </div><!-- /.box-body -->
       </div>
     
     <div class="box box-primary box-solid">
         <div class="box box-header box-solid">
             <h3 class="box-title">Qualitative Data ตัวอย่างเช่น[v3->เพศ]</h3>
              <div class="box-tools pull-right"></div>
         </div>
         
         <div class="box box-body box-solid" style="display: block;">
              <?php $i=0;
            foreach($f_val as $key1=> $value) {
            echo '<div class="table-responsive">';
            echo '<table class="table" border="1">';
            echo '<thead>';
                      echo '<tr class="info">';
                      echo '<th class="text-center"> Value of variable "'.$key1.'" </th>';
                      echo '<th class="text-center"> Number </th>';
                      echo '<th class="text-center"> Percent </th>';
                      echo '<th class="text-center"> Bar Chart </th>';
                      echo '</tr>';
            echo '</thead>';
            echo '<tbody>';
            $totalpercent = ($fre_total['counttotal']/$fre_total['counttotal'])*100;
            foreach ($fre_val[$i] as $key2 => $value2) {
                $pernumber = ($value2['countval']/$fre_total['counttotal'])*100;

                echo '<tr>';
                //echo '<td>'.$value2.'</td>';
                //VarDumper::dump($value2,10,true);
                //echo "string";
                echo '<td class="text-center">'.$value2['listval'].'</td>';
                
                echo '<td class="text-center">'.$value2['countval'].'</td>';
                
                echo '<td class="text-center">'.number_format($pernumber,2,'.','').' %</td>';
                echo '<td>
                    <div class="progress progress-xs progress-striped active">
                                  <div class="progress-bar progress-bar-primary" style="width: '.number_format($pernumber,2,'.','').'% "></div>
                                </div></td>';
            }
                echo '</tr>';
                echo '<tr class="success">';
                echo '<td class="text-center">Total</td>';
                echo '<td class="text-center">'.$fre_total['counttotal'].'</td>';
                echo '<td class="text-center">'.number_format($totalpercent,2,'.','').' %</td>';
                echo '<td><div class="progress progress-xs progress-striped active">
                                  <div class="progress-bar progress-bar-primary" style="width: '.number_format($totalpercent,2,'.','').'% "></div>
                                </div></td>';
                echo '</tr>';
            echo '</tbody>';
            echo '</table>';
            echo '</div>';
            echo "<br>";
      $i++;}
            ?>
         </div>
         
     </div>
       </div>
