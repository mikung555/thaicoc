<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\BuffeTriggers */

$this->title = 'Buffe Triggers#'.$model->sitecode;
$this->params['breadcrumbs'][] = ['label' => 'Buffe Triggers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="buffe-triggers-view">

    <div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	<h4 class="modal-title" id="itemModalLabel"><?= Html::encode($this->title) ?></h4>
    </div>
    <div class="modal-body">
        <?= DetailView::widget([
	    'model' => $model,
	    'attributes' => [
		'sitecode',
		'Trigger',
		'Event',
		'Table',
		'Statement:ntext',
		'Timing',
		'Created',
		'sql_mode',
		'Definer',
		'character_set_client',
		'collation_connection',
		'Database_Collation',
	    ],
	]) ?>
    </div>
</div>
