<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\UserAllow */

$this->title = 'Update User Allow: ' . ' ' . $model->user_id;
$this->params['breadcrumbs'][] = ['label' => 'User Allows', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->user_id, 'url' => ['view', 'user_id' => $model->user_id, 'sitecode' => $model->sitecode]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="user-allow-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
