<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\UserAllow */

$this->title = 'User Allow#'.$model->user_id;
$this->params['breadcrumbs'][] = ['label' => 'User Allows', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-allow-view">

    <div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	<h4 class="modal-title" id="itemModalLabel"><?= Html::encode($this->title) ?></h4>
    </div>
    <div class="modal-body">
        <?= DetailView::widget([
	    'model' => $model,
	    'attributes' => [
		'user_id',
		'sitecode',
	    ],
	]) ?>
    </div>
</div>
