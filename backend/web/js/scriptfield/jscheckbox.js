   function addCheckboxField(rowItem,form){

       var Url = baseUrlTo+'/checkbox/insertfield?forder='+rowItem;
         $.post(
            Url, //serialize Yii2 form
            form.serialize()
            ).done(function(result){
                 if(result.status == 'success'){
                     location.reload();
                 }else if(result.status == 'uniquevalue'){
                     $.unblockUI();
                     var noty_id = noty({"text":result.message, "type":result.action,"layout":'center'});
                     $('button[type=submit]').removeAttr('disabled');
                     $('button[type=submit]').html('สร้างคำถาม');

                 }else{
                     $.unblockUI();
                     console.log('///');
                 }
            }).fail(function(){
                console.log('server error');
        });
    }
    function delCheckboxField(fieldid){
        $.post(baseUrlTo+'/checkbox/formdelete?id='+fieldid,function(result){
            $.unblockUI();
            $('div[item-id='+result.data+']').remove();
        });
    }
    function updateCheckboxField(ezf_id,form){

        var Url = baseUrlTo+'/checkbox/updatefield?id='+ezf_id;

         $.post(
             Url, //serialize Yii2 form
             form.serialize()
         ).done(function(result){
                 if(result.status == 'success'){
                     location.reload();
                 }else if(result.status == 'uniquevalue'){
                     $.unblockUI();
                     var noty_id = noty({"text":result.message, "type":result.action,"layout":'center'});
                     $('button[type=submit]').removeAttr('disabled');
                     $('button[type=submit]').html('สร้างคำถาม');

                 }else{
                     $.unblockUI();
                     console.log('///');
                 }
         }).fail(function(){
             console.log('server error');
         });
    }