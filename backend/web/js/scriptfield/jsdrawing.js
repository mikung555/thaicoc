   function addDrawingField(rowItem,form){
        var Url = baseUrlTo+'/drawing/insertfield?forder='+rowItem;
        $.post(
            Url, //serialize Yii2 form
            form.serialize()
            ).done(function(result){
                if(result.status == 'success'){
                    //$('#showPanel').hide();
                    //$('#addPanel').hide();
                    //$('#formPanel').prepend().append(result.html);
                    //$('.row .dad').dad();
                    location.reload();
                }
                else {
                    $.unblockUI();
                    var noty_id = noty({"text": result.message, "type": result.status});
                    $('button[type=submit]').removeAttr('disabled');
                    $('button[type=submit]').html('สร้างคำถาม');
                    $('#ezformfields-ezf_field_name').attr('style', 'border-color:1px red solid !important;width:150px;color:red;');
                    $('#errorAddField').html('<span style="color:red">&nbsp;&nbsp;ตัวแปรนี้มีอยู่ในฐานข้อมูลแล้วกรุณากรอกใหม่</span>');
//                console.log(result);
                }
                }).fail(function(){
                console.log('server error');
        });
    }
    function delDrawingField(fieldid){
        $.post(baseUrlTo+'/drawing/formdelete?id='+fieldid,function(result){
	    
	    if(result.status == 'error'){
		var noty_id = noty({"text": result.message, "type": result.status});
		$.unblockUI();
	    } else {
		$.unblockUI();
		$('div[item-id='+result.data+']').remove();
	    }
            
        });
    }
    function updateDrawingField(ezf_id,form){
        var Url = baseUrlTo+'/drawing/updatefield?id='+ezf_id;
        $.post(
            Url, //serialize Yii2 form
            form.serialize()
        ).done(function(result){
             if(result.status == 'success'){
                 location.reload();
             }else if(result.status == 'danger'){
                 $('button[type=submit]').removeAttr('disabled');
                 $('button[type=submit]').html('สร้างคำถาม');
                 $('#ezformfields-ezf_field_name').attr('style','border-color:1px red solid !important;width:150px;color:red;');
                 $('#errorAddField').html('<span style="color:red">&nbsp;&nbsp;ตัวแปรนี้มีอยู่ในฐานข้อมูลแล้วกรุณากรอกใหม่</span>');
             }
        }).fail(function(){
            console.log('server error');
        });
    }