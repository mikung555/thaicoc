    function addRadioField(rowItem,form){
         var Url = baseUrlTo+'/radio/insertfield?forder='+rowItem;
        $.post(
            Url, //serialize Yii2 form
            form.serialize()
            ).done(function(result){
                if(result.status == 'success'){
                    //$('#showPanel').hide();
                    //$('#addPanel').hide();
                    //$('#formPanel').prepend().append(result.html);
                    //$('.row .dad').dad();
                    location.reload();
//                   ". SDNoty::show('result.message', 'result.status') ."
                }else {
                    $.unblockUI();
                    var noty_id = noty({"text": result.message, "type": result.status});
                    $('button[type=submit]').removeAttr('disabled');
                    $('button[type=submit]').html('สร้างคำถาม');
                    $('#ezformfields-ezf_field_name').attr('style', 'border-color:1px red solid !important;width:150px;color:red;');
                    $('#errorAddField').html('<span style="color:red">&nbsp;&nbsp;ตัวแปรนี้มีอยู่ในฐานข้อมูลแล้วกรุณากรอกใหม่</span>');
                }
            }).fail(function(){
                console.log('server error');
        });
    }
    function delRadioField(fieldid){
        $.post('/ezforms/radio/formdelete?id='+fieldid,function(result){
            $.unblockUI();
            ". SDNoty::show('result.message', 'result.status') ."
            $('div[item-id='+result.data+']').remove();
        });
    }
    function updateRadioField(ezf_id, ezf_field_id, form){
        var Url = '/ezforms/radio/updatefield?id='+ezf_field_id+'&ezf_id='+ezf_id;
         $.post(
             Url, //serialize Yii2 form
             form.serialize()
         ).done(function(result){
             if(result.status == 'success'){
                location.reload();
	    }else{
                 $.unblockUI();
                var noty_id = noty({"text":result.message, "type":result.status});
                $('button[type=submit]').removeAttr('disabled');
                $('button[type=submit]').html('สร้างคำถาม');
                $('#ezformfields-ezf_field_name').attr('style','border-color:1px red solid !important;width:150px;color:red;');
                $('#errorAddField').html('<span style="color:red">&nbsp;&nbsp;ตัวแปรนี้มีอยู่ในฐานข้อมูลแล้วกรุณากรอกใหม่</span>');
            }
         }).fail(function(){
             console.log('server error');
         });
    }