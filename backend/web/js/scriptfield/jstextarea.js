   function addTextAreaField(rowItem, form){
        //alert($('#field_row').val());
        //return;
        var Url = baseUrlTo+'/textarea/insertfield?forder=' + rowItem + '&rows=' + $('#field_row').val();
        $.post(
            Url, //serialize Yii2 form
            form.serialize()
            ).done(function(result){
                if(result.status == 'success'){
                    //$('#showPanel').hide();
                    //$('#addPanel').hide();
                    //$('#formPanel').prepend().append(result.html);
                    //$('.row .dad').dad();
                    location.reload();
//                   ". SDNoty::show('result.message', 'result.status') ."
                }elese
                {
                    $.unblockUI();
                    var noty_id = noty({"text": result.message, "type": result.status});
                    $('button[type=submit]').removeAttr('disabled');
                    $('button[type=submit]').html('สร้างคำถาม');
                    $('#ezformfields-ezf_field_name').attr('style', 'border-color:1px red solid !important;width:150px;color:red;');
                    $('#errorAddField').html('<span style="color:red">&nbsp;&nbsp;ตัวแปรนี้มีอยู่ในฐานข้อมูลแล้วกรุณากรอกใหม่</span>');
                }
            }).fail(function(){
                console.log('server error');
        });
    }
    function delTextAreaField(fieldid){
        $.post(baseUrlTo+'/textarea/formdelete?id='+fieldid,function(result){
            $.unblockUI();
            ". SDNoty::show('result.message', 'result.status') ."
            $('div[item-id='+result.data+']').remove();
        });
    }
    function updateTextAreaField(ezf_id, form, rows){
        var Url = baseUrlTo+'/textarea/updatefield?id='+ ezf_id + '&rows=' + rows;
        $.post(
            Url, //serialize Yii2 form
            form.serialize()
        ).done(function(result){
             if(result.status == 'success'){
                     location.reload();
             }else if(result.status == 'danger'){
                 $.unblockUI();
                 var noty_id = noty({"text":result.message, "type":result.status});
                 $('button[type=submit]').removeAttr('disabled');
                 $('button[type=submit]').html('สร้างคำถาม');
                 $('#ezformfields-ezf_field_name').attr('style','border-color:1px red solid !important;width:150px;color:red;');
                 $('#errorAddField').html('<span style="color:red">&nbsp;&nbsp;ตัวแปรนี้มีอยู่ในฐานข้อมูลแล้วกรุณากรอกใหม่</span>');
             }
        }).fail(function(){
            console.log('server error');
        });
    }
