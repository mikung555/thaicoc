<?php
use yii\helpers\Html;

/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\MessageInterface the message bing composed */
/* @var $content string main view render result */
?>
<?php $this->beginPage() ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=<?php echo Yii::$app->charset ?>" />
    <title><?php echo Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<p><img alt="Image result for logo คณะแพทย์มข" src="https://cloud.cascap.in.th/img/gecicca_logo.png" style="border: none;" /></p>
<p>&nbsp;</p>

<p><strong>GeCiCCA</strong> <strong>Enrollment Report</strong></p>

<hr />
<p><strong>Trial name</strong>: A randomized controlled trial of Gemcitabine alone versus Gemcitabine plus Cisplatin as an adjuvant chemotherapy after curative intent resection of cholangiocarcinoma.</p>

<p>&nbsp;</p>

<p><strong>This report generated on</strong><strong>:</strong> <?=$data['update_date']; ?></p>

<p><strong>Site No:</strong> 13777</p>

<p><strong>Visit</strong><strong>:</strong> 1st enrollment visit</p>

<p><strong>Date of enrollment</strong><strong>:</strong> <?=$data['create_date']; ?></p>

<table border="0" cellpadding="0" cellspacing="2">
    <tbody>
    <tr>
        <td style="width:122px">
            <p><strong>Screening ID</strong><strong>:</strong></p>
        </td>
        <td style="width:208px">
            <p><?=$data['hptcode']; ?></p>
        </td>
    </tr>
    <tr>
        <td style="width:122px">
            <p><strong>Subject ID</strong>:</p>
        </td>
        <td style="width:208px">
            <p><?=$data['f1v45_x']; ?></p>
        </td>
    </tr>
    <tr>
        <td style="width:122px">
            <p><strong>Subject Initial</strong>:</p>
        </td>
        <td style="width:208px">
            <p><?=$data['f1_initial']; ?></p>
        </td>
    </tr>
    <tr>
        <td style="width:122px">
            <p><strong>Date of birth</strong><strong>:</strong></p>
        </td>
        <td style="width:208px">
            <p><?=$data['f1v3']; ?></p>
        </td>
    </tr>
    <tr>
        <td style="width:122px">
            <p><strong>Gender:</strong></p>
        </td>
        <td style="width:208px">
            <p><?=$data['f1v5']; ?></p>
        </td>
    </tr>
    </tbody>
</table>

<p><strong>Completion</strong><strong>:</strong> This subject has now been randomized.</p>

<p><strong>Date of randomization confirmation</strong><strong>:</strong> <?=$data['update_date']; ?></p>

<p><strong>Randomization code</strong><strong>:</strong> <?=$data['random']; ?></p>

<p><strong>Treatment group</strong><strong>:</strong> <?=$data['chemo_drugs']; ?></p>

<p><strong>Completed randomization by</strong><strong>:</strong> <?=$data['user_create']; ?></p>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>