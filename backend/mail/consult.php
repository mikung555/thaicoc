<?php
/**
 * Created by PhpStorm.
 * User: hackablex
 * Date: 7/4/2016 AD
 * Time: 02:16
 */

?>

<b>Consult Tools from <?=$data['sender']; ?></b>
<hr>
เรื่อง : <?=$data['title']; ?><br>
ชื่อฟอร์ม : <?=$data['ezf_name']; ?><br>
ประเภท : <?=$data['type']; ?><br>
เมื่อ : <?=$data['create_date']; ?><br>
<?php echo \yii\helpers\Html::a('ลิงค์รายละเอียด [เปิด]', \yii\helpers\Url::toRoute(['/inputdata/redirect-page', 'ezf_id' => $data['ezf_id'], 'dataid'=>$data['data_id']], true), ['target'=>'_blank']); ?><br>
<?=$data['detail']; ?><br>


<hr>
ขอแสดงความนับถือ<br>
<?=$data['from_name']; ?><br>
ชื่อสถานบริการ : <?=$data['from_site']; ?><br>
Phone: <?=$data['from_phone']; ?><br>
Email: <?=$data['from_email']; ?><br>
<br>
<hr>
<?php echo Yii::$app->keyStorage->get('sender');?> <?php echo \yii\helpers\Html::a(Yii::$app->keyStorage->get('frontend.domain'), Yii::$app->keyStorage->get('frontend.domain'), ['target'=>'_blank'])?>
