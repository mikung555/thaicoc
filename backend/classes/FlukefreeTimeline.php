<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace backend\classes;
use yii\data\Pagination;
use yii\base\Component;
/**
 * Description of FulkefreeTimeline
 *
 * @author TOSHIBA
 */
class FlukefreeTimeline {
    
    public function getFlukefreeTimeline($dataPost) {
        $startdate = $dataPost['startdate'];
        $enddate = $dataPost['enddate'];
        
        $sqlM = "SELECT gid,main_ezf_table,gname,gdetail, active, created_by, sitecode, ezf_id, ezf_name, ezf_table, enable_form FROM inv_gen WHERE gid= :gid";
        $queryM = \Yii::$app->db->createCommand($sqlM, [':gid' => $dataPost['module_id']])->queryOne();
        $form_data = \appxq\sdii\utils\SDUtility::string2Array($queryM['enable_form']);
       
        
        foreach ($form_data['form'] as $val) {
            if ($form_lst == null) {
                $form_lst .= "'" . $val . "'";
            } else {
                $form_lst .= ",'" . $val . "'";
            }
        }
        //\appxq\sdii\utils\VarDumper::dump($form_lst);        exit();
        $datalist = [];

        if ($queryM) {


            $sqlEzf = " SELECT ezf_id, ezf_name,ezf_detail,ezf_table, user_create, create_date,field_detail , ezf_timeline 
                    FROM ezform WHERE ezf_id in($form_lst) ";
            $resEzf = \Yii::$app->db->createCommand($sqlEzf)->queryAll();
            //\appxq\sdii\utils\VarDumper::dump($resEzf);
            $sql = "";
                foreach ($resEzf as $tabledata) {
                    if ($tabledata['ezf_timeline'] != "") {
                        //$ezftimeline = $tabledata['ezf_timeline'];
                        $ezftimeline = "CONCAT('มีการบันทึกฟอร์ม : ','{$tabledata['ezf_name']} , ',{$tabledata['ezf_timeline']})";
                    }else if ($tabledata['field_detail'] != "") {
                        //$ezftimeline = $tabledata['field_detail'];
                        $ezftimeline = "CONCAT('มีการบันทึกฟอร์ม : ','{$tabledata['ezf_name']} , ',{$tabledata['field_detail']})";
                    }else{
                        //$ezftimeline = "'{$tabledata['ezf_name']}'";
                        $ezftimeline = "CONCAT('มีการบันทึกฟอร์ม : ','{$tabledata['ezf_name']}')";
                    }                    
                    if ($sql == '') {
                        $sql = "(SELECT `id`,`create_date`,date(create_date) as 'date', time(create_date) as 'time',{$ezftimeline} `data`  FROM `" . $tabledata['ezf_table'] . "` WHERE DATE(create_date)=CURDATE() ORDER BY create_date DESC  limit 20)";
                    } else {
                        $sql .= " UNION (SELECT `id`,`create_date`,date(create_date) as 'date', time(create_date) as 'time',{$ezftimeline} `data` FROM `" . $tabledata['ezf_table'] . "` WHERE DATE(create_date) =CURDATE() ORDER BY create_date DESC  limit 20)";
                    }
                }
                $datalist = \Yii::$app->dbutf8mb4->createCommand($sql)->queryAll();

            $datalist = \appxq\sdii\utils\SortArray::sortingArrayDESC($datalist, 'create_date');           
//            $datalist = \appxq\sdii\utils\SortArray::sortingArrayDESC($datalist, 'date');
            
              
        }
        return $datalist;
    }
    
}
