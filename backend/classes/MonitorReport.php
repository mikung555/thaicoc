<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace backend\classes;

use backend\modules\inv\models\InvGen;

/**
 * Description of MonitorReport
 *
 * @author jokeclancool 
 */
const inStr = '1498130302024554300,1497859920056436400,1496902325091526900,9911
                ,1498622167035527500,148472197302019370,1496678490037398000
                ,1496678203080761900,29,1498457589040399800,1498463555010552900,1484721973020193700';

class MonitorReport {

    public function getReportOfModules() {

        $data = null;
        $dataProvider = [];
        $dataTotal = [];
        $allpatient = 0;
        $sql = " SELECT gid, gname,gdetail, active, created_by, sitecode, ezf_id, ezf_name, ezf_table,main_ezf_table, enable_form "
                . " FROM inv_gen WHERE gid IN(" . inStr . ")  ORDER BY order_module ";
        $result = \Yii::$app->db->createCommand($sql)->queryAll();
        //\appxq\sdii\utils\VarDumper::dump($result);
        if ($result) {
            $x = 0;
            foreach ($result as $val) {
                $table = "";
                $form_data = \appxq\sdii\utils\SDUtility::string2Array($val['enable_form']);

                if ($val['main_ezf_table'] != '') {

                    if ($val['gid'] == '1498622167035527500') {
                        $sqlPatient = " SELECT count(DISTINCT ptid) as pacount, count(ptid) as tcount, count(DISTINCT up.sitecode) as scount,count(DISTINCT up.user_id) as ucount FROM " . $val['main_ezf_table'] . " tb INNER JOIN user_profile up ON tb.sitecode=up.sitecode  WHERE up.sitecode = '93006' ";
                    } else {
                        $sqlPatient = " SELECT count(DISTINCT ptid) as pacount, count(ptid) as tcount, count(DISTINCT up.sitecode) as scount,count(DISTINCT up.user_id) as ucount FROM " . $val['main_ezf_table'] . " tb INNER JOIN user_profile up ON tb.sitecode=up.sitecode  ";
                    }

                    $resPatient = \Yii::$app->db->createCommand($sqlPatient)->queryOne();
                    $table = $val['main_ezf_table'];
                } else {
                    foreach ($form_data as $val2) {
                        $sqlPatient = " SELECT count(DISTINCT ptid) as pacount, count(ptid) as tcount, count(DISTINCT up.sitecode) as scount,count(DISTINCT up.user_id) as ucount FROM " . $val2['ezf_table'] . " tb INNER JOIN user_profile up ON tb.sitecode=up.sitecode  ";
                        $resPatient = \Yii::$app->db->createCommand($sqlPatient)->queryOne();
                        $table = $val2['ezf_table'];
                    }
                }

//                $sqlSCount = " SELECT count(upro.user_id) as scount FROM user_profile upro "
//                        . "INNER JOIN inv_favorite ifa ON upro.user_id=ifa.user_id "
//                        . "WHERE upro.sitecode='" . $resPatient['xsourcex'] . "' AND ifa.gid= '" . $val['gid'] . "' ";
                if ($val['gid'] == '1484721973020193700' || $val['gid'] == '1498463555010552900') {
                    $sqlSCount = " SELECT count(DISTINCT sitecode) as scount,count(DISTINCT user_id) as ucount FROM user_profile ";
                    $resSCount = \Yii::$app->db->createCommand($sqlSCount)->queryOne();
                    $dataProvider[$x]['mod_site'] = $resSCount['scount'] == '' ? 0 : $resSCount['scount'];
                    $dataProvider[$x]['mod_user'] = $resSCount['ucount'] == '' ? 0 : $resSCount['ucount'];
                } else {
                    $dataProvider[$x]['mod_site'] = $resPatient['scount'] == '' ? 0 : $resPatient['scount'];
                    $dataProvider[$x]['mod_user'] = $resPatient['ucount'] == '' ? 0 : $resPatient['ucount'];
                    //$sqlSCount = " SELECT count(DISTINCT sitecode) as scount ,count(DISTINCT user_create) as ucount FROM $table ";
                }

                $dataProvider[$x]['mod_id'] = $val['gid'];
                $dataProvider[$x]['mod_name'] = $val['gname'];
                $dataProvider[$x]['mod_form'] = count($form_data['form']);

                $dataProvider[$x]['mod_patient'] = $resPatient['pacount'];
                $dataProvider[$x]['mod_allpatient'] = $resPatient['allpatient'];
                $dataProvider[$x]['mod_ntime'] = $resPatient['tcount'];

                //  if($dataTotal['mod_form_total']< $dataProvider[$x]['mod_form'])
                $dataTotal['mod_form_total'] += $dataProvider[$x]['mod_form'];

                if ($dataTotal['mod_site_total'] < $dataProvider[$x]['mod_site'])
                    $dataTotal['mod_site_total'] = $dataProvider[$x]['mod_site'];

                if ($dataTotal['mod_user_total'] < $dataProvider[$x]['mod_user'])
                    $dataTotal['mod_user_total'] = $dataProvider[$x]['mod_user'];

                if ($dataTotal['mod_patient_total'] < $dataProvider[$x]['mod_patient'])
                    $dataTotal['mod_patient_total'] = $dataProvider[$x]['mod_patient'];

                if ($dataTotal['mod_allpatient_total'] < $dataProvider[$x]['mod_allpatient']) {
                    $dataTotal['mod_allpatient_total'] = $dataProvider[$x]['mod_allpatient'];

                    $allpatient = $dataProvider[$x]['mod_allpatient'];
                }

                $dataTotal['mod_ntime_total'] += $dataProvider[$x]['mod_ntime'];

                //$dataProvider[$x]['mod_site'] = number_format($dataProvider[$x]['mod_site']);

                $x++;
            }
        }

        //$dataProvider = \appxq\sdii\utils\SortArray::sortingArrayDESC($dataProvider, 'mod_site');
        $dataProvider = new \yii\data\ArrayDataProvider([
            'allModels' => $dataProvider,
            'pagination' => [
                'pageSize' => 50,
            ],
        ]);
        $dataProvider->sort->attributes['mod_name'] = [
            // The tables are the ones our relation are configured to
            // in my case they are prefixed with "tbl_"
            'asc' => ['mod_name' => SORT_ASC],
            'desc' => ['mod_name' => SORT_DESC],
        ];
        return [
            'dataProvider' => $dataProvider,
            'dataTotal' => $dataTotal,
            'allpatient' => $dataTotal['mod_allpatient_total']
        ];
    }

    public function getReportOfForms($module_id, $sitecode = null, $startDate = null, $endDate = null) {
        $data = null;
        $dataProvider = [];
        $sql = " SELECT gid, gname,gdetail, active, created_by, sitecode, ezf_id, ezf_name, ezf_table, enable_form "
                . " FROM inv_gen WHERE gid=:gid  ";

        $result = \Yii::$app->db->createCommand($sql, [':gid' => $module_id])->queryOne();
        $form_data = \appxq\sdii\utils\SDUtility::string2Array($result['enable_form']);

        $form_lst = $form_data['form'];
        if ($result) {
            $x = 0;
            $params = [];
            foreach ($form_lst as $val) {

                $sqlEzf = " SELECT ez.ezf_id, ez.ezf_name,ez.ezf_table, ez.user_create, ez.create_date 
                    FROM ezform ez WHERE ez.ezf_id = :ezf_id ";
                $params = [
                    ':ezf_id' => $val,
                ];
                if ($startDate != null || $endDate != null) {
                    $sqlEzf .= " AND ez.create_date BETWEEN :startDate AND :endDate ";
                    $params = [
                        ':ezf_id' => $val,
                        ':startDate' => $startDate,
                        ':endDate' => $endDate
                    ];
                }


                $resEzf = \Yii::$app->db->createCommand($sqlEzf, $params)->queryOne();

                $tb_data = $resEzf['ezf_table'];
                //\appxq\sdii\utils\VarDumper::dump($resEzf);
                if ($tb_data != null) {
                    $params = [];
                    if ($sitecode != null) {
                        $tbwhere = " WHERE sitecode = :sitecode ";
                        $params = [':sitecode' => $sitecode];
                    }
                    $sqlIndat = " SELECT count(DISTINCT ptid) as form_all 
                        ,SUM(IF(YEAR(create_date) = YEAR(create_date) ,1,0)) as form_year
                        ,SUM(IF(YEAR(NOW()) = YEAR(create_date) AND MONTH(NOW()) = MONTH(create_date) ,1,0)) as form_month
                        ,SUM(IF(YEAR(create_date) = YEAR(CURDATE()) AND MONTH(create_date) = MONTH(CURDATE()) AND WEEKDAY(create_date)= WEEKDAY(CURDATE()) ,1,0)) as form_week
                        ,SUM(IF(DATE(create_date) = CURDATE() ,1,0)) as form_date
                        FROM (SELECT ptid, create_date FROM $tb_data $tbwhere GROUP BY ptid) as tbdata  ";
                    $resIndata = \Yii::$app->db->createCommand($sqlIndat, $params)->queryOne();

                    $dataProvider[$x]['form_id'] = $resEzf['ezf_id'];
                    $dataProvider[$x]['form_name'] = $resEzf['ezf_name'];
                    $dataProvider[$x]['form_all'] = $resIndata['form_all'];
                    $dataProvider[$x]['form_year'] = $resIndata['form_year'];
                    $dataProvider[$x]['form_month'] = $resIndata['form_month'];
                    $dataProvider[$x]['form_week'] = $resIndata['form_week'];
                    $dataProvider[$x]['form_date'] = $resIndata['form_date'];

                    $x++;
                }
            }
            $dataProvider = \appxq\sdii\utils\SortArray::sortingArrayDESC($dataProvider, 'form_all');
        }

        return $dataProvider;
    }

    public function getReportOfSites($module_id) {
        $data = null;
        $dataProvider = [];
        $sql = " SELECT gid, gname,gdetail, active, created_by, sitecode, ezf_id, ezf_name, ezf_table,main_ezf_table, enable_form "
                . " FROM inv_gen WHERE gid =:gid  ORDER BY order_module ";
        $result = \Yii::$app->db->createCommand($sql, [':gid' => $module_id])->queryOne();

        if ($result['main_ezf_table'] != '') {
            if ($result['gid'] == '1498622167035527500') {
                $sqlPatient = " SELECT DISTINCT up.sitecode, aht.`name` FROM (" . $result['main_ezf_table'] . " tb INNER JOIN user_profile up ON tb.sitecode=up.sitecode) INNER JOIN all_hospital_thai aht ON up.sitecode=aht.hcode WHERE up.sitecode = '93006' ";
                $resSite = \Yii::$app->db->createCommand($sqlPatient)->queryAll();
            } else {
                $sqlPatient = " SELECT DISTINCT up.sitecode, aht.`name` FROM (" . $result['main_ezf_table'] . " tb INNER JOIN user_profile up ON tb.sitecode=up.sitecode) INNER JOIN all_hospital_thai aht ON up.sitecode=aht.hcode  ";
                $resSite = \Yii::$app->db->createCommand($sqlPatient)->queryAll();
            }
        }
        //\appxq\sdii\utils\VarDumper::dump($resSite);
        $sites = "";
        foreach ($resSite as $val) {
            if ($sites == '') {
                $sites .= "'" . $val['sitecode'] . "'";
            } else {
                $sites .= ",'" . $val['sitecode'] . "'";
            }
        }
        if ($resSite != null) {
            if ($result['gid'] == '1498622167035527500') {
                $sqlCount = " SELECT sitecode, SUM(AllPatient)as AllPatient, SUM(IF(gid=:gidnp,nPatient,0)) as nPatient, SUM(IF(gid=:gidnt,nTime,0)) as nTime 
                FROM rpt_patient_count_cc WHERE sitecode = '93006' GROUP BY sitecode ORDER BY sitecode";
                $resCount = \Yii::$app->db->createCommand($sqlCount, [':gidnp' => $module_id, ':gidnt' => $module_id])->queryAll();

                $sqlUser = "SELECT sitecode, COUNT(user_id) as users FROM user_profile WHERE sitecode='93006' GROUP BY sitecode ORDER BY sitecode";
                $resUser = \Yii::$app->db->createCommand($sqlUser)->queryAll();
            } else {
                $sqlCount = " SELECT sitecode, SUM(AllPatient)as AllPatient, SUM(IF(gid=:gidnp,nPatient,0)) as nPatient, SUM(IF(gid=:gidnt,nTime,0)) as nTime 
                FROM rpt_patient_count_cc  WHERE sitecode IN($sites) GROUP BY sitecode ORDER BY sitecode";
                $resCount = \Yii::$app->db->createCommand($sqlCount, [':gidnp' => $module_id, ':gidnt' => $module_id])->queryAll();

                $sqlUser = "SELECT sitecode, COUNT(user_id) as users FROM user_profile WHERE sitecode IN($sites) GROUP BY sitecode ORDER BY sitecode";
                $resUser = \Yii::$app->db->createCommand($sqlUser)->queryAll();
            }
        }


        //\appxq\sdii\utils\VarDumper::dump($resUser);
        $x = 0;
        $resTarget = null;
        $resMain = null;
        $target_table = '';
        $main_table = '';
        $x = 0;
        foreach ($resSite as $val) {

            $dataProvider[$x]['sitecode'] = $val['sitecode'];
            $dataProvider[$x]['site_name'] = $val['name'];

            foreach ($resCount as $valC) {
                if ($val['sitecode'] == $valC['sitecode']) {
                    $dataProvider[$x]['form_all'] = $valC['AllPatient'];
                    $dataProvider[$x]['form_patient'] = $valC['nPatient'];
                    $dataProvider[$x]['form_amt'] = $valC['nTime'];
                }
            }

            foreach ($resUser as $valU) {
                if ($val['sitecode'] == $valU['sitecode']) {
                    $dataProvider[$x]['form_user'] = $valU['users'];
                }
            }
            $x++;

            $dataProvider = \appxq\sdii\utils\SortArray::sortingArrayDESC($dataProvider, 'form_all');
        }
        return $dataProvider;
    }

    public function getSiteName($sitecode) {
        $sqlSite = " SELECT `name` FROM all_hospital_thai WHERE hcode=:sitecode";
        $resSite = \Yii::$app->db->createCommand($sqlSite, [':sitecode' => $sitecode])->queryOne();

        return $resSite['name'];
    }

}
