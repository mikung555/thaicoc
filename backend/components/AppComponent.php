<?php

namespace backend\components;

use Yii;
use yii\base\Component;

class AppComponent extends Component {

    public function init() {
	parent::init();
	
	self::currentUsername();
	self::siteURL();
	
    }

    public static function siteURL() {
	$model = Yii::$app->user->identity->userProfile->sitecode;
	$sqlHospital = "SELECT `hcode`,`name` FROM all_hospital_thai WHERE hcode='".($model)."' ";
	$dataHospital = Yii::$app->db->createCommand($sqlHospital)->queryOne();
	$sqlSiteURL = "SELECT code,`url`,`name` FROM site_url WHERE code='".(is_numeric($model) ? $model+0 : $model)."' ";
	$dataSiteURL = Yii::$app->db->createCommand($sqlSiteURL)->queryOne();
	if ($dataSiteURL['code']=="") {
	    $siteURL = new \backend\models\SiteUrl;
	    $siteURL->code=is_numeric($model) ? $model+0 : $model;
	    $siteURL->name=$dataHospital['name'];
	    $siteURL->url=$model;
	    $siteURL->save();
	    $SQLquery=Yii::$app->db->createCommand("REPLACE INTO site_menu (SELECT '".($siteURL->code)."' AS '".($siteURL->code)."', a.* from site_menu_init as a)")->query();
	}
	
    }
    
    public static function currentUsername() {
	$currentUsername = Yii::$app->user->identity->userProfile->user->username;
        setcookie("currentUsername", Yii::$app->user->identity->userProfile->user->username, time()+3600, "/", Yii::$app->keyStorage->get('frontend.domain'), false);
        $delAssets = (Yii::$app->user->can('administrator') || Yii::$app->user->can('adminsite'))?1:0;
        setcookie("delAssets", $delAssets, time()+3600, "/", Yii::$app->keyStorage->get('frontend.domain'), false);
	//echo $currentUsername," ",$_COOKIE['currentUsername'];
        //exit;
	if (($_COOKIE['CloudUserID'] != "") && ($_COOKIE['CloudUserID'] != $currentUsername)) {
	    $LoginForm = new \backend\models\LoginForm();
	    Yii::$app->user->login($LoginForm->searchUser($_COOKIE['CloudUserID']),0);
	    return Yii::$app->getResponse()->redirect(Yii::$app->getHomeUrl());
	}else{
	    if ($currentUsername != "") if ($_COOKIE['CloudUserID']=="") {
		Yii::$app->user->logout();
		setcookie("CloudUserID", "", time()-1, "/", Yii::$app->keyStorage->get('frontend.domain'), false);
		return Yii::$app->getResponse()->redirect(Yii::$app->getHomeUrl());
		//echo "<META http-equiv=\"refresh\" content=\"0;URL=".Yii::getAlias('@frontendUrl')."\">";
		exit;    
	    }
	}
	
    }


}
