<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\ScriptGroup;

/**
 * ScriptGroupSearch represents the model behind the search form about `backend\models\ScriptGroup`.
 */
class ScriptGroupSearch extends ScriptGroup
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['gid', 'passed', 'error', 'total'], 'integer'],
            [['created_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ScriptGroup::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'gid' => $this->gid,
            'passed' => $this->passed,
            'error' => $this->error,
            'total' => $this->total,
            'created_at' => $this->created_at,
        ]);

        return $dataProvider;
    }
}
