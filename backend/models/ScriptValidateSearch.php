<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\ScriptValidate;

/**
 * ScriptValidateSearch represents the model behind the search form about `backend\models\ScriptValidate`.
 */
class ScriptValidateSearch extends ScriptValidate
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sid', 'rating', 'enable'], 'integer'],
            [['title', 'name', 'description', 'sql', 'sql_log', 'template', 'process', 'return'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ScriptValidate::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'sid' => $this->sid,
            'rating' => $this->rating,
            'enable' => $this->enable,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'sql', $this->sql])
            ->andFilterWhere(['like', 'sql_log', $this->sql_log])
            ->andFilterWhere(['like', 'template', $this->template])
            ->andFilterWhere(['like', 'process', $this->process])
            ->andFilterWhere(['like', 'return', $this->return]);

        return $dataProvider;
    }
}
