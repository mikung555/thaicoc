<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "const_district".
 *
 * @property integer $DISTRICT_ID
 * @property string $DISTRICT_CODE
 * @property string $DISTRICT_NAME
 * @property integer $AMPHUR_ID
 * @property integer $PROVINCE_ID
 * @property integer $GEO_ID
 */
class ConstDistrict extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'const_district';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['DISTRICT_CODE', 'DISTRICT_NAME'], 'required'],
            [['AMPHUR_ID', 'PROVINCE_ID', 'GEO_ID'], 'integer'],
            [['DISTRICT_CODE'], 'string', 'max' => 6],
            [['DISTRICT_NAME'], 'string', 'max' => 150]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'DISTRICT_ID' => Yii::t('app', 'District  ID'),
            'DISTRICT_CODE' => Yii::t('app', 'District  Code'),
            'DISTRICT_NAME' => Yii::t('app', 'District  Name'),
            'AMPHUR_ID' => Yii::t('app', 'Amphur  ID'),
            'PROVINCE_ID' => Yii::t('app', 'Province  ID'),
            'GEO_ID' => Yii::t('app', 'Geo  ID'),
        ];
    }
}
