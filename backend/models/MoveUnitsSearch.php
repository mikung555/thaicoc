<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\MoveUnits;

/**
 * MoveUnitsSearch represents the model behind the search form about `backend\models\MoveUnits`.
 */
class MoveUnitsSearch extends MoveUnits
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['muid', 'user_id', 'created_by', 'updated_by'], 'integer'],
            [['sitecode_old', 'sitecode_new', 'status', 'file', 'comment', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MoveUnits::find()
		->select("move_units.*, h1.name as sitecode_old, h2.name as sitecode_new, user_profile.firstname as user_name, user_profile.lastname as user_lname , user.username as user")
		->innerJoin('user', 'user.id=move_units.user_id')
		->innerJoin('user_profile', 'user_profile.user_id=move_units.user_id')
		->leftJoin('all_hospital_thai h1', 'h1.`hcode`=move_units.sitecode_old')
		->innerJoin('all_hospital_thai h2', 'h2.`hcode`=move_units.sitecode_new');
//CONCAT(user_profile.firstname, ' ', user_profile.lastname) as user_id, 
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'move_units.muid' => $this->muid,
            'move_units.user_id' => $this->user_id,
            'move_units.created_by' => $this->created_by,
            'move_units.created_at' => $this->created_at,
            'move_units.updated_by' => $this->updated_by,
            'move_units.updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'move_units.sitecode_old', $this->sitecode_old])
            ->andFilterWhere(['like', 'move_units.sitecode_new', $this->sitecode_new])
            ->andFilterWhere(['like', 'move_units.status', $this->status])
            ->andFilterWhere(['like', 'move_units.file', $this->file])
            ->andFilterWhere(['like', 'move_units.comment', $this->comment]);

        return $dataProvider;
    }
}
