<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "script_group".
 *
 * @property integer $gid
 * @property integer $passed
 * @property integer $error
 * @property integer $total
 * @property string $created_at
 */
class ScriptGroup extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'script_group';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['gid', 'passed', 'error', 'total', 'created_at'], 'required'],
            [['gid', 'passed', 'error', 'total'], 'integer'],
            [['created_at'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'gid' => Yii::t('app', 'Gid'),
            'passed' => Yii::t('app', 'Passed'),
            'error' => Yii::t('app', 'Error'),
            'total' => Yii::t('app', 'Total'),
            'created_at' => Yii::t('app', 'Created At'),
        ];
    }
}
