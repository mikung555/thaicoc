<?php

namespace backend\models;

use common\models\UserProfile;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\QueryRequest;
use yii\data\SqlDataProvider;
use yii\helpers\VarDumper;

/**
 * QueryRequestSearch represents the model behind the search form about `backend\models\QueryRequest`.
 */
class QueryRequestSearch extends QueryRequest {

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
                [['id', 'ezf_id', 'target_id', 'data_id', 'ezf_field_id', 'status', 'user_create', 'user_update', 'approved_by'], 'integer'],
                [['new_val', 'note', 'time_create', 'time_update'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function searchAll($params, $ezf_id = null) {
        $query = QueryRequest::find()->select('query_request.*')->rightJoin('query_list', '`query_list`.`query_id` = `query_request`.`id`');
        $query_by = $params['query_by'];
        $type = $params['type'];

        if ($query_by == 1)
            $query_by = 'byme';
        else if ($query_by == 2)
            $query_by = 'tome';

        //check dama manager
        $queryManager = QueryManager::find()->where('user_id = :user_id AND user_group = :user_group', [':user_id' => (Yii::$app->user->id), ':user_group' => 1])->one();
        //VarDumper::dump($ezf_id, 10, true); exit;
        //view by form
        if ($ezf_id + 0) {
            if ($queryManager['id'] || Yii::$app->user->can('administrator')) {
                $query = $query->where('query_request.ezf_id = :ezf_id', [':ezf_id' => $ezf_id]);
            } else {
                $query = $query->where('query_request.ezf_id = :ezf_id AND query_request.xsourcex = :xsourcex', [':ezf_id' => $ezf_id, ':xsourcex' => Yii::$app->user->identity->userProfile->sitecode]);
            }
            //
            $query = $query->andWhere('query_request.userkey_status = :type', [':type' => $type]);
            if ($query_by == 'byme') {
                $query = $query->andWhere('query_list.user_id_by = :user_id', [':user_id' => Yii::$app->user->id]);
            } else if ($query_by == 'tome') {
                $query = $query->andWhere('query_list.user_id_to = :user_id', [':user_id' => Yii::$app->user->id]);
            }
        } else {
            if ($queryManager['id'] || Yii::$app->user->can('administrator')) {
                //เป็น admin ไม่ต้อง where
            } else {
                $query = $query->where('query_request.xsourcex = :xsourcex', [':xsourcex' => Yii::$app->user->identity->userProfile->sitecode]);
            }
            //
            $query = $query->andWhere('query_request.userkey_status = :type', [':type' => $type]);
            if ($query_by == 'byme') {
                $query = $query->andWhere('query_list.user_id_by = :user_id', [':user_id' => Yii::$app->user->id]);
            } else if ($query_by == 'tome') {
                $query = $query->andWhere('query_list.user_id_to = :user_id', [':user_id' => Yii::$app->user->id]);
            }
        }

        if ($params['query_status']) {
            if ($params['query_status'] == '9') {
                
            } else if ($params['query_status'] == -9) {
                $query = $query->andWhere('query_request.open_status = 1');
            } else if ($params['query_status'] >= 1) {
                $query = $query->andWhere('query_request.status = :status', [':status' => $params['query_status']]);
            }
        }

        if ($params['txtSearch']) {
            $query = $query->andWhere('query_request.xsourcex LIKE :txtSearch', [':txtSearch' => '%' . $params['txtSearch'] . '%']);
        }

        $query = $query->orderBy(['query_request.time_create' => SORT_DESC]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 100,
            ],
        ]);


        $dataProviderTotal = $dataProvider->count;
        for ($i = 0; $i < $dataProviderTotal; $i++) {
            if ($dataProvider->models[$i]->status == 1) {
                $dataProvider->models[$i]->status = 'Waiting';
            } else if ($dataProvider->models[$i]->status == 2) {
                $dataProvider->models[$i]->status = 'Resolve with some change';
            } else if ($dataProvider->models[$i]->status == 3) {
                $dataProvider->models[$i]->status = 'Resolve without any change';
            } else if ($dataProvider->models[$i]->status == 4) {
                $dataProvider->models[$i]->status = 'Unresolvable';
            } else if ($dataProvider->models[$i]->status == 5) {
                $dataProvider->models[$i]->status = 'Remark';
            }

            $dataNew = \yii\helpers\Json::decode($dataProvider->models[$i]->new_val);
            foreach ($dataNew as $kNew => $vNew) {
                $dataNew = $kNew;
                $dataProvider->models[$i]->new_val = $vNew;
            }
            $dataOld = \yii\helpers\Json::decode($dataProvider->models[$i]->old_val);
            foreach ($dataOld as $kOld => $vOld) {
                if ($dataNew == $kOld) {
                    $ezfFieldId = $dataProvider->models[$i]->ezf_field_id;
                    $ezfFieldName = $kOld;
                    $ezfId = $dataProvider->models[$i]->ezf_id;
                    $dataOld = $vOld;
                    $dataProvider->models[$i]->old_val = $vOld;
                } else if($dataNew == null || $dataNew == '') {
                    $dataProvider->models[$i]->old_val = '';
                }
            }
            $dataField = Yii::$app->db->createCommand(
                                    "SELECT * FROM ezform_fields WHERE ezf_field_id = :ezf_field_id AND ezf_id = :ezf_id AND ezf_field_name = :ezf_field_name")
                            ->bindValues([
                                ':ezf_field_id' => $ezfFieldId,
                                ':ezf_id' => $ezfId,
                                ':ezf_field_name' => $ezfFieldName
                            ])->queryOne();

            if ($dataField) {
//                if ($dataField['ezf_field_type'] == 10) {
//                    \appxq\sdii\utils\VarDumper::dump($this->getEzfType10($dataField,$dataProvider->models[$i]->old_val));
//                }
                $dataFieldChoice = Yii::$app->db->createCommand(
                                        "SELECT ezf_choicelabel FROM ezform_choice "
                                        . "WHERE ezf_field_id = :ezf_field_id AND ezf_choicevalue = :ezf_choicevalue")
                                ->bindValues([':ezf_field_id' => $dataField['ezf_field_id'], ':ezf_choicevalue' => $dataOld])->queryOne();

                if ($dataFieldChoice) {
                    $dataProvider->models[$i]->old_val = $dataFieldChoice['ezf_choicelabel'];
                }
                $dataFieldChoice = Yii::$app->db->createCommand(
                                        "SELECT ezf_choicelabel FROM ezform_choice "
                                        . "WHERE ezf_field_id = :ezf_field_id AND ezf_choicevalue = :ezf_choicevalue")
                                ->bindValues([':ezf_field_id' => $dataField['ezf_field_id'], ':ezf_choicevalue' => $vNew])->queryOne();

                if ($dataFieldChoice) {
                    $dataProvider->models[$i]->new_val = $dataFieldChoice['ezf_choicelabel'];
                }
            }

            //
            if ($dataProvider->models[$i]->target_id) {
                //$user = common\models\UserProfile::findOne($model->target);
                //echo $ezf_table_comp; echo'<hr>'; print_r($arr_comp_desc_field_name); exit;
                $comp_name = new \backend\models\Dynamic();
                $ezf_table_comp = \backend\modules\ezforms\components\EzformQuery::checkIsTableComponent($dataProvider->models[$i]->ezf_id);
                $arr_desc = explode(',', $ezf_table_comp['field_id_desc']);
                $arr_comp_desc_field_name = [];
                foreach ($arr_desc AS $val) {
                    $ezf_fields = Yii::$app->db->createCommand("SELECT ezf_field_name FROM ezform_fields WHERE ezf_field_id='$val';")->queryOne();
                    $arr_comp_desc_field_name[] = $ezf_fields['ezf_field_name'];
                }
                $ezf_table_comp = \backend\modules\ezforms\components\EzformQuery::getFormTableName($ezf_table_comp['ezf_id']);
                $comp_name->setTableName($ezf_table_comp->ezf_table);
                $comp_name = $comp_name::findOne($dataProvider->models[$i]->target_id);
                $str = '';
                foreach ($arr_comp_desc_field_name as $val) {
                    $str .= $comp_name->$val . ' ';
                }
                $dataProvider->models[$i]->target_id = $str;
                //return $model->firstname.' '.$model->lastname;
                //return $model->target;
            } else
                $dataProvider->models[$i]->target_id = 'ไม่ระบุเป้าหมาย';
            //
            $ezform = \backend\modules\ezforms\models\Ezform::find()->select('ezf_name')->where('ezf_id = :ezf_id', [':ezf_id' => $dataProvider->models[$i]->ezf_id])->one();
            $dataProvider->models[$i]->ezf_id = $ezform->ezf_name;
            //
            $ezf_fields = Yii::$app->db->createCommand("SELECT ezf_field_name, ezf_field_label FROM ezform_fields WHERE ezf_field_id = :ezf_field_id", [':ezf_field_id' => $dataProvider->models[$i]->ezf_field_id])->queryOne();
            $dataProvider->models[$i]->ezf_field_id = $ezf_fields['ezf_field_label'] . '(' . $ezf_fields['ezf_field_name'] . ')';
            //
            $user = UserProfile::findOne($dataProvider->models[$i]->user_create);
            $dataProvider->models[$i]->user_create = $user->firstname . ' ' . $user->lastname;
        }


        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        //VarDumper::dump($this->validate($params), 10, true); Yii::$app->end();

        $query->andFilterWhere([
            'xsourcex' => $this->xsourcex,
            'ezf_id' => $this->ezf_id,
            'target_id' => $this->target_id,
            'data_id' => $this->data_id,
            'ezf_field_id' => $this->ezf_field_id,
            'status' => $this->status,
            'user_create' => $this->user_create,
            'user_update' => $this->user_update,
            'time_create' => $this->time_create,
            'time_update' => $this->time_update,
            'approved_by' => $this->approved_by,
        ]);

        $query->andFilterWhere(['like', 'new_val', $this->new_val])
                ->andFilterWhere(['like', 'note', $this->note]);



        return $dataProvider;
    }

    public function search() {
        $queryManager = QueryManager::find()->where('user_id = :user_id AND user_group = :user_group', [':user_id' => (Yii::$app->user->id), ':user_group' => 1])->one();
        if ($queryManager['id'] || Yii::$app->user->can('administrator')) {
            $sql = 'select b.ezf_id as ezf_name, b.ezf_id, SUM(b.status=1) AS status_waiting, "" as positions from query_list as a INNER join query_request as b on a.query_id = b.id group by b.ezf_id';
        } else {
            //$sql = 'select ezf_id as ezf_name, ezf_id, SUM(status=1) AS status_waiting, "" as positions from `query_request` group by ezf_id;';
            $sql = 'select b.ezf_id as ezf_name, b.ezf_id, SUM(b.status=1) AS status_waiting, "" as positions from query_list as a left join query_request as b on a.query_id = b.id WHERE b.xsourcex = \'' . Yii::$app->user->identity->userProfile->sitecode . '\' group by b.ezf_id';
            //$sql = 'select *, "" as ezf_name, "" as status_waiting, "" as positions from query_list where user_id_by = "'.Yii::$app->user->id.'" OR user_id_to = "'.Yii::$app->user->id.'" group by ezf_id;';
        }
        $count = Yii::$app->db->createCommand($sql)->query()->count();

        $dataProvider = new SqlDataProvider([
            'sql' => $sql,
            //'params' => [':status' => 1],
            'totalCount' => $count,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'attributes' => [
                //'title',
                //'view_count',
                //'created_at',
                ],
            ],
        ]);

        //VarDumper::dump($dataProvider->models, 10, true); Yii::$app->end();
        $dataProviderTotal = $dataProvider->count;
        $changeData = [];
        $user_id = Yii::$app->user->id;
        for ($i = 0; $i < $dataProviderTotal; $i++) {

            $ezform = \backend\modules\ezforms\models\Ezform::find()->select('ezf_name')->where('ezf_id = :ezf_id', [':ezf_id' => $dataProvider->models[$i]['ezf_id']])->one();
            $changeData[$i]['ezf_name'] = $ezform->ezf_name;
            $changeData[$i]['ezf_id'] = $dataProvider->models[$i]['ezf_id'];
            $changeData[$i]['status_waiting'] = $dataProvider->models[$i]['status_waiting'];
            $QueryManager = QueryManager::find()->where('ezf_id = :ezf_id', [':ezf_id' => $dataProvider->models[$i]['ezf_id']])->andWhere('user_id = :user_id', [':user_id' => $user_id])->one();
            if ($QueryManager->status == 1) {
                $changeData[$i]['positions'] = 'Owner';
            } else if ($QueryManager->status == 2) {
                $changeData[$i]['positions'] = 'Co Dev';
            } else if ($QueryManager->status == 3) {
                $changeData[$i]['positions'] = 'Data Manager';
            } else {
                $changeData[$i]['positions'] = 'User';
            }
        }
        $dataProvider->models = $changeData;

        //VarDumper::dump($dataProvider->models, 10, true); Yii::$app->end();

        return $dataProvider;
    }

    public function searchByForm($params, $ezf_id) {
        return self::searchAll($params, $ezf_id);
    }

    

}
