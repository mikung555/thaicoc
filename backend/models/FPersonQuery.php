<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[FPerson]].
 *
 * @see FPerson
 */
class FPersonQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return FPerson[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return FPerson|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}