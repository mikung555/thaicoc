<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Tbdata;

/**
 * TbdataSearch represents the model behind the search form about `backend\models\Tbdata`.
 */
class TbdataSearch extends Tbdata
{
    protected static $table;
    protected static $ezf_id;
   
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [];
    }

    public static function setTableName($table)
    {
        self::$table = $table;
    }    
    public static function setEZFid($ezf_id)
    {
        self::$ezf_id = $ezf_id;
    }  
    
    public static function setDataExport($dataExport)
    {
        $this->dataExport = $ezf_id;
    }
    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        //$query = Tbdata::find();
        $queryx = new Tbdata();
        $queryx->setTableName(self::$table);
        $query=$queryx->find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'rstat' => $this->rstat,
            'user_create' => $this->user_create,
            'create_date' => $this->create_date,
            'user_update' => $this->user_update,
            'update_date' => $this->update_date,
        ]);
        $ezfield = Yii::$app->db->createCommand('SELECT * FROM `ezform_fields` WHERE ezf_id =\''.self::$ezf_id.'\'')->query()->readAll();
        if (count($ezfield)>0) {
            foreach($ezfield as $key => $form) {
//            echo $form['ezf_field_name'] . "<br>";
                
                //echo $form->ezf_field_name;exit;
                //echo $form['ezf_field_name'];exit;
                //echo $this->var1;
                //print_r($form);
                //echo $form[0]->ezf_field_name; exit;
                ////$this[$form['ezf_field_name']];
                //$this->$form['ezf_field_name'];
            //$query->andFilterWhere(['like', $form['ezf_field_name'], $this[$form['ezf_field_name']]]);
            }
        }
//        \yii\helpers\VarDumper::dump($ezfield);exit;
        $query->andFilterWhere(['like', 'xsourcex', $this->xsourcex]);
//            ->andFilterWhere(['like', 'var1', $this->var1])
//            ->andFilterWhere(['like', 'var2', $this->var2])
//            ->andFilterWhere(['like', 'var3', $this->var3])
//            ->andFilterWhere(['like', 'var4', $this->var4])
//            ->andFilterWhere(['like', 'var5', $this->var5])
//            ->andFilterWhere(['like', 'var6', $this->var6])
//            ->andFilterWhere(['like', 'var7', $this->var7])
//            ->andFilterWhere(['like', 'var8', $this->var8])
//            ->andFilterWhere(['like', 'var9', $this->var9])
//            ->andFilterWhere(['like', 'var10', $this->var10])
//            ->andFilterWhere(['like', 'var11', $this->var11])
//            ->andFilterWhere(['like', 'var12', $this->var12])
//            ->andFilterWhere(['like', 'var13', $this->var13])
//            ->andFilterWhere(['like', 'var14', $this->var14])
//            ->andFilterWhere(['like', 'var15', $this->var15])
//            ->andFilterWhere(['like', 'var16', $this->var16])
//            ->andFilterWhere(['like', 'var17', $this->var17])
//            ->andFilterWhere(['like', 'var18', $this->var18])
//            ->andFilterWhere(['like', 'var19', $this->var19])
//            ->andFilterWhere(['like', 'var20', $this->var20])
//            ->andFilterWhere(['like', 'var21', $this->var21])
//            ->andFilterWhere(['like', 'var22', $this->var22])
//            ->andFilterWhere(['like', 'var23', $this->var23])
//            ->andFilterWhere(['like', 'var24', $this->var24])
//            ->andFilterWhere(['like', 'var25', $this->var25])
//            ->andFilterWhere(['like', 'var26', $this->var26])
//            ->andFilterWhere(['like', 'var27', $this->var27])
//            ->andFilterWhere(['like', 'var28', $this->var28])
//            ->andFilterWhere(['like', 'var29', $this->var29])
//            ->andFilterWhere(['like', 'var30', $this->var30])
//            ->andFilterWhere(['like', 'var31', $this->var31])
//            ->andFilterWhere(['like', 'var32', $this->var32]);

        return $dataProvider;
    }
    
    public function export($params, $owner, $objective, $show, $dataExport)
    {
        //$query = Tbdata::find();
	$sitecode = Yii::$app->user->identity->userProfile->sitecode;
	
        $queryx = new Tbdata();
        $queryx->setTableName(self::$table);
	$query;
	if($objective){
	    if($owner){
		if($show=='all'){
		    $query=$queryx->find()
                    ->where(self::$table.'.rstat NOT IN ("0","3")');
		}elseif ($show=='one') {
		    $query=$queryx->find()
		    //->innerJoin('user_profile', self::$table.'.user_create = user_profile.user_id')
		    ->where(self::$table.'.rstat NOT IN ("0","3") AND '.self::$table.'.hsitecode = :sitecode', [':sitecode'=>$sitecode]);
		} else {
		    $query=$queryx->find()
		    //->innerJoin('user_profile', self::$table.'.user_create = user_profile.user_id')
		    ->where(self::$table.'.rstat NOT IN ("0","3") AND '.self::$table.'.hsitecode = :sitecode', [':sitecode'=>$show]);
		}
		
	    } else {
		$query=$queryx->find()
		    //->innerJoin('user_profile', self::$table.'.user_create = user_profile.user_id')
		    ->where(self::$table.'.rstat NOT IN ("0","3") AND '.self::$table.'.hsitecode = :sitecode', [':sitecode'=>$sitecode]);
	    }
	} else {
	    if($owner){
		if($show=='all'){
		    $query=$queryx->find()->where(self::$table.'.rstat NOT IN ("0","3")');
		}elseif ($show=='one') {
		    $query=$queryx->find()
		    ->innerJoin('user_profile', self::$table.'.user_create = user_profile.user_id')
		    ->where(self::$table.'.rstat NOT IN ("0","3") AND user_profile.user_id = :user_id', [':user_id'=>Yii::$app->user->id]);
		} else {
		    $query=$queryx->find()
		    ->innerJoin('user_profile', self::$table.'.user_create = user_profile.user_id')
		    ->where(self::$table.'.rstat NOT IN ("0","3") AND user_profile.user_id = :user_id', [':user_id'=>$show]);
		}
	    } else {
		$query=$queryx->find()
		    ->innerJoin('user_profile', self::$table.'.user_create = user_profile.user_id')
		    ->where(self::$table.'.rstat NOT IN ("0","3") AND user_profile.user_id = :user_id', [':user_id'=>Yii::$app->user->id]);
	    }
	}
        

	$this->load($params);
	
//	$query->andFilterWhere([
//            'id' => $this->id,
//            'rstat' => $this->rstat,
//            'user_create' => $this->user_create,
//            'create_date' => $this->create_date,
//            'user_update' => $this->user_update,
//            'update_date' => $this->update_date,
//        ]);
	
        //$query->andFilterWhere(['like', 'xsourcex', $this->xsourcex]);
	
	//sql custom 
	$table = self::$table;
	
	if($dataExport){
	    if(trim($dataExport['export_select'])!=''){
		$sql_col = explode('#', $dataExport['export_select']);
		if(is_array($sql_col)){
		    $query->select($sql_col);
		}
	    }
	    
	    if(trim($dataExport['export_join'])!=''){
		$sql_join = explode('@', $dataExport['export_join']);
		if(is_array($sql_join)){
		    foreach ($sql_join as $keyJoin => $valueJoin) {
			$sql_join_items = explode('#', $valueJoin);
			if(is_array($sql_join_items)){
			    $typeJoin = trim($sql_join_items[0]);
			    $tableJoin = trim($sql_join_items[1]);
			    $onJoin = trim($sql_join_items[2]);

			    $query->join($typeJoin, $tableJoin, $onJoin);
			}
		    }
		}
	    }
	    
	    if(trim($dataExport['export_where'])!=''){
		$query->andWhere($dataExport['export_where']);
	    }
	    
	}
	
	//\appxq\sdii\utils\VarDumper::dump($query->createCommand()->rawSql);
	
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        
        return $dataProvider;
    }
}
