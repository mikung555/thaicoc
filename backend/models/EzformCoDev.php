<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "ezform_co_dev".
 *
 * @property string $ezf_id
 * @property string $user_co
 * @property integer $status
 *
 * @property Ezform $ezf
 */
class EzformCoDev extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ezform_co_dev';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ezf_id', 'user_co', 'status'], 'required'],
            [['ezf_id', 'user_co', 'status'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ezf_id' => Yii::t('app', 'รหัสฟอร์ม'),
            'user_co' => Yii::t('app', 'User Co'),
            'status' => Yii::t('app', 'สถานะ'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEzf()
    {
        return $this->hasOne(Ezform::className(), ['ezf_id' => 'ezf_id']);
    }

    /**
     * @inheritdoc
     * @return EzformCoDevQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new EzformCoDevQuery(get_called_class());
    }
}
