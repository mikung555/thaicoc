<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "user_profile".
 *
 * @property string $user_id
 * @property string $firstname
 * @property string $middlename
 * @property string $lastname
 * @property string $avatar_path
 * @property string $avatar_base_url
 * @property string $locale
 * @property integer $gender
 * @property integer $sitecode
 *
 * @property User $user
 */
class UserProfile extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_profile';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
//            [['locale'], 'required'],
//            [['gender'], 'integer'],
//            [['firstname', 'middlename', 'lastname', 'avatar_path', 'avatar_base_url'], 'string', 'max' => 255],
//            [['locale'], 'string', 'max' => 32],
//            [['sitecode'], 'string'],
	    [['user_id', 'gender'], 'integer'],
            [['email'],'required','message'=>'กรุณาระบุ'],
            [['firstname', 'middlename', 'lastname', 'avatar_path', 'avatar_base_url'], 'string', 'max' => 255],
            ['locale', 'default', 'value' => Yii::$app->language],
            ['locale', 'in', 'range' => array_keys(Yii::$app->params['availableLocales'])],
            [['volunteer_status','volunteer', 'status','status_personal','status_manager','status_other'
            ,'department_nation_text','department_area','department_area_text','department_province','department_province_text','department_amphur',
            'department_amphur_text','department_group','activate'
            ], 'safe'],
            [['telephone'],'safe'],
           
        ];
    }
    
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_id' => Yii::t('app', 'User ID'),
            'firstname' => Yii::t('app', 'Firstname'),
            'middlename' => Yii::t('app', 'Middlename'),
            'lastname' => Yii::t('app', 'Lastname'),
            'avatar_path' => Yii::t('app', 'Avatar Path'),
            'cid'=>Yii::t('app','Personal ID'),
            'avatar_base_url' => Yii::t('app', 'Avatar Base Url'),
            'locale' => Yii::t('app', 'Locale'),
            'gender' => Yii::t('app', 'Gender'),
	    'volunteer_status'=>'อาสาสมัคร',
	    'volunteer'=>'เลือกอาสาสมัคร',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
