<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "report_dynamic".
 *
 * @property integer $id
 * @property string $titlename
 * @property string $detail
 * @property string $sqlcommand
 */
class ReportDynamic extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'report_dynamic';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['detail', 'sqlcommand'], 'string'],
            [['titlename'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'titlename' => 'Title name',
            'detail' => 'Detail',
            'sqlcommand' => 'SQL Command',
        ];
    }
}
