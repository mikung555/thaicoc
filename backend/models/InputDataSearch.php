<?php

namespace backend\models;

use backend\modules\ezforms\components\EzformQuery;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\ezforms\models\Ezform;
use yii\helpers\Url;
use yii\helpers\VarDumper;

/**
 * EzformSearch represents the model behind the search form about `backend\modules\ezforms\models\Ezform`.
 */
class InputDataSearch extends Ezform {

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
                [['ezf_id', 'user_create', 'user_update', 'status', 'shared', 'public_listview', 'public_edit', 'public_delete'], 'integer'],
                [['ezf_name', 'ezf_detail', 'ezf_table', 'create_date', 'update_date', 'username'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search() {

        $query = Ezform::find()
                ->select('ezform.*,`ezform_favorite`.userid,`ezform_favorite`.status AS favorite_status, `ezform_favorite`.forder AS favorite_forder')
                ->innerJoin('ezform_favorite', '`ezform`.`ezf_id` = `ezform_favorite`.`ezf_id`')
                ->where('ezform.`status` <> :status AND ezform_favorite.userid = :userid', [':status' => 3, ':userid' => Yii::$app->user->id])
                ->orderBy('`ezform_favorite`.forder DESC');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'totalCount' => $query->count(),
            'pagination' => array(
                'pageSize' => 15,
            ),
        ]);

        return $dataProvider;
    }

    public function searchEMR($target) {
        $defaultTarget = $target;
        $target = base64_decode($target);

        //ดูดข้อมูลของเป้าหมายจาก EMR
        if (Yii::$app->keyStorage->get('frontend.domain') == 'dpmcloud.org') {
            $res = Yii::$app->db->createCommand("SELECT ezf_local, ezf_remote FROM ezform_sync;")->queryAll();
            foreach ($res as $valx) {
                $ezform = Yii::$app->dbcascaputf8->createCommand("SELECT ezf_table FROM `ezform` WHERE  ezf_id = :ezf_id", [':ezf_id' => $valx['ezf_remote']])->queryOne();
                $model = Yii::$app->dbcascaputf8->createCommand("SELECT * FROM `" . $ezform['ezf_table'] . "` WHERE  ptid = :ptid", [':ptid' => $target])->queryAll();
                if ($model[0]['id']) {
                    $keys_tb = '';
                    foreach ($model[0] as $key => $val) {
                        $keys_tb .= "`{$key}`, ";
                    }
                    $keys_tb = substr($keys_tb, 0, -2);

                    foreach ($model as $valy) {
                        $values = '';
                        foreach ($valy as $key => $val) {
                            $values .= "'{$val}', ";
                        }
                        $values = substr($values, 0, -2);
                        //VarDumper::dump($values,10,true); exit;

                        $ezform = Yii::$app->dbcascaputf8->createCommand("SELECT ezf_table FROM `ezform` WHERE  ezf_id = :ezf_id", [':ezf_id' => $valx['ezf_local']])->queryOne();
                        Yii::$app->db->createCommand("replace into `" . $ezform['ezf_table'] . "` ({$keys_tb}) VALUES ({$values});")->execute();

                        //save EMR
                        $ezform_target = Yii::$app->dbcascaputf8->createCommand("SELECT * FROM `ezform_target` WHERE  data_id=:data_id AND ezf_id=:ezf_id;", [':data_id' => $valy['id'], ':ezf_id' => $valx['ezf_local']])->queryOne();
                        if ($ezform_target['data_id']) {
                            $values = '';
                            $keys = '';
                            foreach ($ezform_target as $key => $val) {
                                $values .= "'{$val}', ";
                                $keys .= "`{$key}`, ";
                            }
                            $values = substr($values, 0, -2);
                            $keys = substr($keys, 0, -2);
                            //VarDumper::dump($keys,10,true); exit;
                            Yii::$app->db->createCommand("replace into `ezform_target` ({$keys}) VALUES ({$values});")->execute();
                        }
                    }
                }
            }
        }

        if ($defaultTarget == 'byme' || $defaultTarget == 'skip' || $defaultTarget == 'all') {
            $targetOrder = EzformTarget::find()->where('(target_id = :target AND rstat <>3)', [':target' => $target,])->orderBy(['create_date' => SORT_DESC]);
        } else {
            $tempTabelName = "ezform_target_order" . Yii::$app->user->identity->id . time();

            $tempTable = "CREATE TEMPORARY TABLE {$tempTabelName} (
                        `ezf_id` bigint(20) unsigned NOT NULL COMMENT 'รหัส EZ-Form',
                        `ezf_name` varchar(100) CHARACTER SET utf8 NOT NULL DEFAULT 'ฟอร์มไม่มีชื่อ' COMMENT 'ชื่อฟอร์ม',
                        `data_id` bigint(20) unsigned NOT NULL COMMENT 'รหัสข้อมูล',
                        `comp_id` bigint(20) unsigned DEFAULT NULL COMMENT 'รหัส component',
                        `target_id` bigint(20) unsigned NOT NULL COMMENT 'รหัสเป้าหมาย',
                        `user_create` bigint(20) unsigned DEFAULT NULL COMMENT 'รหัสผู้เชื่อมเป้าหมาย',
                        `user_update` bigint(20) DEFAULT NULL,
                        `create_date` datetime DEFAULT NULL COMMENT 'วันที่สร้าง',
                        `update_date` datetime DEFAULT NULL,
                        `ezf_date_order` datetime DEFAULT NULL,
                        `rstat` tinyint(4) NOT NULL,
                        `xsourcex` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
                        PRIMARY KEY (`ezf_id`,`data_id`,`target_id`)
                      ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci";

            Yii::$app->db->createCommand($tempTable)->execute();

            $targetData = EzformTarget::find()->where('(target_id = :target AND rstat <>3)', [':target' => $target,])->all();
            
            foreach ($targetData as $value) {

                $ezf = Ezform::find()->where(['ezf_id' => $value['ezf_id']])->one();
                
                if ($ezf->ezf_table != '') {
                    $ezf_date_order = Yii::$app->db->createCommand("SELECT {$ezf->ezf_date_order} FROM {$ezf->ezf_table} WHERE id = '" . $value['data_id'] . "'")->queryOne();
                    Yii::$app->db->createCommand("REPLACE INTO {$tempTabelName} VALUES ('" . $value['ezf_id'] . "','" . $ezf['ezf_name'] . "','" . $value['data_id'] . "',"
                            . "'" . $value['comp_id'] . "','" . $value['target_id'] . "','" . $value['user_create'] . "','" . $value['user_update'] . "',"
                            . "'" . $value['create_date'] . "','" . $value['update_date'] . "','" . $ezf_date_order[$ezf->ezf_date_order] . "',{$value['rstat']},"
                            . "'" . $value['xsourcex'] . "')")->query();
                }
            }
            
            //$fieldEzf = Ezform::find()->where(['ezf_name' => 'Register'])->orderBy(['ezf_date_order'=>SORT_DESC])->all();
            $fieldEzf = (new \yii\db\Query())->select(['*'])->from($tempTabelName)->where('ezf_name = :name',['name' => 'Register'])->orderBy(['ezf_date_order'=>SORT_DESC])->all();
            $fieldTxt = "";
            foreach ($fieldEzf as $valueEzf) {
                if($fieldTxt == ""){
                    $fieldTxt = $valueEzf['ezf_id'];
                }else{
                    $fieldTxt .= ",".$valueEzf['ezf_id'];
                }
            }
            //\appxq\sdii\utils\VarDumper::dump($fieldTxt);
            $targetOrder = new \yii\db\Query();
            if(!empty($fieldTxt)){
                $targetOrder = $targetOrder->select(['*'])->from($tempTabelName)->where('(target_id = :target AND rstat <>3)', [':target' => $target,])->orderBy([new \yii\db\Expression("FIELD (ezf_id,{$fieldTxt} )"),'ezf_date_order'=>SORT_DESC]);
            }else{
                $targetOrder = $targetOrder->select(['*'])->from($tempTabelName)->where('(target_id = :target AND rstat <>3)', [':target' => $target,])->orderBy(['ezf_date_order'=>SORT_DESC]);
            }
            
        }




        $dataProvidertarget = new ActiveDataProvider([
            'query' => $targetOrder,
            'totalCount' => $targetOrder->count(),
//            'sort' => [
//                'defaultOrder' => ['ezf_date_order' => SORT_DESC],
//            ],
            'pagination' => array(
                'pageSize' => 10,
            ),
        ]);

        //\appxq\sdii\utils\VarDumper::dump($dataProvidertarget);
        return $dataProvidertarget;
    }

    public function searchTarget($ezf_id = null, $target = null, $rstat = null, $comp_target = null) {

        $ezform = Yii::$app->db->createCommand("SELECT ezf_table FROM ezform WHERE ezf_id=:ezf_id;", [':ezf_id' => $ezf_id])->queryOne();
        $ezf_table = $ezform['ezf_table'];
        $xdepartmentx = Yii::$app->user->identity->userProfile->department_area;
        $xdepartmentx_sql = $xdepartmentx > 0 ? ("AND xdepartmentx ='" . $xdepartmentx . "'") : null;

        if ($target == 'byme') {
            //$tbdatatarget = Yii::$app->db->createCommand("SELECT id, target FROM ".$ezf_table." WHERE LENGTH(target)<=0;")->queryAll();
            //$tbdatatarget = $tbdatatarget['target'];
            $query = new Dynamic();
            $query->setTableName($ezf_table);
            //$sql = "select id, target, create_date, update_date FROM ".$ezf_table." WHERE LENGTH(target)<=0";
            //$query = $query->findBySql($sql)->all();
            // OR trim(target)=\'\'
            $query = $query->find()
                    ->where(' rstat NOT IN (3) AND user_create = :user_create AND xsourcex = :xsourcex ' . $xdepartmentx_sql, [
                ':user_create' => Yii::$app->user->id,
                ':xsourcex' => Yii::$app->user->identity->userProfile->sitecode
            ]);
            $ezform = EzformQuery::checkIsTableComponent($ezf_id);
            if ($ezform['special']) {
                $query = $query->orderBy(['hptcode' => SORT_ASC]);
            }
            //->findByCondition('target<>""');
            //->all();
            //\yii\helpers\VarDumper::dump($query->createCommand()->sql, 10, TRUE);
            //Yii::$app->end();

            $dataProvidertarget = new ActiveDataProvider([
                'query' => $query,
                'totalCount' => $query->count(),
                //'sort' => ['attributes' => ['ezf_id', 'ezf_name']],
                'pagination' => array(
                    'pageSize' => 10,
                ),
            ]);


            return $dataProvidertarget;
        } else if ($target == 'skip') {

            $query = new Dynamic();
            $query->setTableName($ezf_table);

            $query = $query->find()
                    ->where('target is null AND rstat NOT IN (3) AND xsourcex = :xsourcex ' . $xdepartmentx_sql, [':xsourcex' => Yii::$app->user->identity->userProfile->sitecode]);
            $ezform = EzformQuery::checkIsTableComponent($ezf_id);
            if ($ezform['special']) {
                $query = $query->orderBy(['hptcode' => SORT_ASC]);
            }

            $dataProvidertarget = new ActiveDataProvider([
                'query' => $query,
                'totalCount' => $query->count(),
                //'sort' => ['attributes' => ['ezf_id', 'ezf_name']],
                'pagination' => array(
                    'pageSize' => 10,
                ),
            ]);


            return $dataProvidertarget;
        } else if ($target == 'all') {

            $query = new Dynamic();
            $query->setTableName($ezf_table);

            if ($rstat) {
                if ($rstat == 'draft') {
                    $rstat = 1;
                } else if ($rstat == 'newrecord') {
                    $rstat = 0;
                } else if ($rstat == 'submitted') {
                    $rstat = 2;
                }

                $query = $query->find()->where('rstat = :rstat AND xsourcex = :xsourcex ' . $xdepartmentx_sql, [
                    ':rstat' => $rstat,
                    ':xsourcex' => Yii::$app->user->identity->userProfile->sitecode
                ]);
                $ezform = EzformQuery::checkIsTableComponent($ezf_id);
                if ($ezform['special']) {
                    $query = $query->orderBy(['hptcode' => SORT_ASC]);
                }
            } else {
                $query = $query->find()->where('rstat <> 3 AND xsourcex = :xsourcex ' . $xdepartmentx_sql, [
                    ':xsourcex' => Yii::$app->user->identity->userProfile->sitecode
                ]);
                $ezform = EzformQuery::checkIsTableComponent($ezf_id);
                if ($ezform['special']) {
                    $query = $query->orderBy(['hptcode' => SORT_ASC]);
                }
            }

            $dataProvidertarget = new ActiveDataProvider([
                'query' => $query,
                'totalCount' => $query->count(),
                //'sort' => ['attributes' => ['ezf_id', 'ezf_name']],
                'pagination' => array(
                    'pageSize' => 10,
                ),
            ]);

            return $dataProvidertarget;
        } else {
            //หา ข้อมูลจาก target ที่ฟอร์มโดยตรง
            $query = new Dynamic();
            $query->setTableName($ezf_table);
            $ezform = EzformQuery::checkIsTableComponent($ezf_id);
            if ($ezform['special'] == '1') {
                $target = base64_decode($target);
                $query = $query->find()->where('ptid = :target AND rstat NOT IN (3)', [':target' => $target,]);
            } else {
                $target = base64_decode($target);
                //$query = $query->find()->where('target = :target AND xsourcex = :xsourcex AND (rstat <> 3)', [':target' => $target, ':xsourcex' => Yii::$app->user->identity->userProfile->sitecode]);
                $query = $query->find()->where('target = :target AND rstat NOT IN (3)', [':target' => $target]);
            }

            $dataProvidertarget = new ActiveDataProvider([
                'query' => $query,
                'totalCount' => $query->count(),
                //'sort' => ['attributes' => ['ezf_id', 'ezf_name']],
                'sort' => [
                    'defaultOrder' => ['create_date' => SORT_ASC],
                ],
                'pagination' => array(
                    'pageSize' => 10,
                ),
            ]);
            return $dataProvidertarget;
        }
        //------------------------------
    }
    
    public function searchEMRCustom($target, $ezf_list) {
        $defaultTarget = $target;
        $target = base64_decode($target);

        //ดูดข้อมูลของเป้าหมายจาก EMR
        if (Yii::$app->keyStorage->get('frontend.domain') == 'dpmcloud.org') {
            $res = Yii::$app->db->createCommand("SELECT ezf_local, ezf_remote FROM ezform_sync;")->queryAll();
            foreach ($res as $valx) {
                $ezform = Yii::$app->dbcascaputf8->createCommand("SELECT ezf_table FROM `ezform` WHERE  ezf_id = :ezf_id", [':ezf_id' => $valx['ezf_remote']])->queryOne();
                $model = Yii::$app->dbcascaputf8->createCommand("SELECT * FROM `" . $ezform['ezf_table'] . "` WHERE  ptid = :ptid", [':ptid' => $target])->queryAll();
                if ($model[0]['id']) {
                    $keys_tb = '';
                    foreach ($model[0] as $key => $val) {
                        $keys_tb .= "`{$key}`, ";
                    }
                    $keys_tb = substr($keys_tb, 0, -2);

                    foreach ($model as $valy) {
                        $values = '';
                        foreach ($valy as $key => $val) {
                            $values .= "'{$val}', ";
                        }
                        $values = substr($values, 0, -2);
                        //VarDumper::dump($values,10,true); exit;

                        $ezform = Yii::$app->dbcascaputf8->createCommand("SELECT ezf_table FROM `ezform` WHERE  ezf_id = :ezf_id", [':ezf_id' => $valx['ezf_local']])->queryOne();
                        Yii::$app->db->createCommand("replace into `" . $ezform['ezf_table'] . "` ({$keys_tb}) VALUES ({$values});")->execute();

                        //save EMR
                        $ezform_target = Yii::$app->dbcascaputf8->createCommand("SELECT * FROM `ezform_target` WHERE  data_id=:data_id AND ezf_id=:ezf_id;", [':data_id' => $valy['id'], ':ezf_id' => $valx['ezf_local']])->queryOne();
                        if ($ezform_target['data_id']) {
                            $values = '';
                            $keys = '';
                            foreach ($ezform_target as $key => $val) {
                                $values .= "'{$val}', ";
                                $keys .= "`{$key}`, ";
                            }
                            $values = substr($values, 0, -2);
                            $keys = substr($keys, 0, -2);
                            //VarDumper::dump($keys,10,true); exit;
                            Yii::$app->db->createCommand("replace into `ezform_target` ({$keys}) VALUES ({$values});")->execute();
                        }
                    }
                }
            }
        }

        if ($defaultTarget == 'byme' || $defaultTarget == 'skip' || $defaultTarget == 'all') {
            $targetOrder = EzformTarget::find()->where('(target_id = :target AND rstat <>3)', [':target' => $target,])->orderBy(['create_date' => SORT_DESC]);
        } else {
            $tempTabelName = "ezform_target_order" . Yii::$app->user->identity->id . time();

            $tempTable = "CREATE TEMPORARY TABLE {$tempTabelName} (
                        `ezf_id` bigint(20) unsigned NOT NULL COMMENT 'รหัส EZ-Form',
                        `ezf_name` varchar(100) CHARACTER SET utf8 NOT NULL DEFAULT 'ฟอร์มไม่มีชื่อ' COMMENT 'ชื่อฟอร์ม',
                        `data_id` bigint(20) unsigned NOT NULL COMMENT 'รหัสข้อมูล',
                        `comp_id` bigint(20) unsigned DEFAULT NULL COMMENT 'รหัส component',
                        `target_id` bigint(20) unsigned NOT NULL COMMENT 'รหัสเป้าหมาย',
                        `user_create` bigint(20) unsigned DEFAULT NULL COMMENT 'รหัสผู้เชื่อมเป้าหมาย',
                        `user_update` bigint(20) DEFAULT NULL,
                        `create_date` datetime DEFAULT NULL COMMENT 'วันที่สร้าง',
                        `update_date` datetime DEFAULT NULL,
                        `ezf_date_order` datetime DEFAULT NULL,
                        `rstat` tinyint(4) NOT NULL,
                        `xsourcex` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
                        PRIMARY KEY (`ezf_id`,`data_id`,`target_id`)
                      ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci";

            Yii::$app->db->createCommand($tempTable)->execute();

            $targetData = EzformTarget::find()->where('(target_id = :target AND rstat <>3)', [':target' => $target,])->all();
            
            foreach ($targetData as $value) {

                $ezf = Ezform::find()->where(['ezf_id' => $value['ezf_id']])->one();
                
                if ($ezf->ezf_table != '' && in_array($value['ezf_id'], $ezf_list)) {
                    $ezf_date_order = Yii::$app->db->createCommand("SELECT {$ezf->ezf_date_order} FROM {$ezf->ezf_table} WHERE id = '" . $value['data_id'] . "'")->queryOne();
                    Yii::$app->db->createCommand("REPLACE INTO {$tempTabelName} VALUES ('" . $value['ezf_id'] . "','" . $ezf['ezf_name'] . "','" . $value['data_id'] . "',"
                            . "'" . $value['comp_id'] . "','" . $value['target_id'] . "','" . $value['user_create'] . "','" . $value['user_update'] . "',"
                            . "'" . $value['create_date'] . "','" . $value['update_date'] . "','" . $ezf_date_order[$ezf->ezf_date_order] . "',{$value['rstat']},"
                            . "'" . $value['xsourcex'] . "')")->query();
                }
            }
            
            //$fieldEzf = Ezform::find()->where(['ezf_name' => 'Register'])->orderBy(['ezf_date_order'=>SORT_DESC])->all();
            $fieldEzf = (new \yii\db\Query())->select(['*'])->from($tempTabelName)->where('ezf_name = :name',['name' => 'Register'])->orderBy(['ezf_date_order'=>SORT_DESC])->all();
            $fieldTxt = "";
            foreach ($fieldEzf as $valueEzf) {
                if($fieldTxt == ""){
                    $fieldTxt = $valueEzf['ezf_id'];
                }else{
                    $fieldTxt .= ",".$valueEzf['ezf_id'];
                }
            }
            //\appxq\sdii\utils\VarDumper::dump($fieldTxt);
            $targetOrder = new \yii\db\Query();
            if(!empty($fieldTxt)){
                $targetOrder = $targetOrder->select(['*'])->from($tempTabelName)->where('(target_id = :target AND rstat <>3)', [':target' => $target,])->orderBy([new \yii\db\Expression("FIELD (ezf_id,{$fieldTxt} )"),'ezf_date_order'=>SORT_DESC]);
            }else{
                $targetOrder = $targetOrder->select(['*'])->from($tempTabelName)->where('(target_id = :target AND rstat <>3)', [':target' => $target,])->orderBy(['ezf_date_order'=>SORT_DESC]);
            }
            
        }




        $dataProvidertarget = new ActiveDataProvider([
            'query' => $targetOrder,
            'totalCount' => $targetOrder->count(),
//            'sort' => [
//                'defaultOrder' => ['ezf_date_order' => SORT_DESC],
//            ],
            'pagination' => array(
                'pageSize' => 10,
            ),
        ]);

        //\appxq\sdii\utils\VarDumper::dump($dataProvidertarget);
        return $dataProvidertarget;
    }

}
