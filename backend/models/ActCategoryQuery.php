<?php

namespace backend\models;

/**
 * This is the ActiveQuery class for [[ActCategory]].
 *
 * @see ActCategory
 */
class ActCategoryQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return ActCategory[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return ActCategory|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}