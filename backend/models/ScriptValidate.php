<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "script_validate".
 *
 * @property integer $sid
 * @property string $title
 * @property string $name
 * @property string $description
 * @property string $sql
 * @property string $sql_log
 * @property string $template
 * @property string $process
 * @property integer $rating
 * @property integer $enable
 * @property string $return
 */
class ScriptValidate extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'script_validate';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'name', 'description', 'sql', 'template', 'return'], 'required'],
            [['title', 'sql', 'sql_log', 'template', 'process'], 'string'],
            [['rating', 'enable'], 'integer'],
            [['name', 'description'], 'string', 'max' => 200],
            [['return'], 'string', 'max' => 10]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'sid' => Yii::t('app', 'Sid'),
            'title' => Yii::t('app', 'Title'),
            'name' => Yii::t('app', 'Field'),
            'description' => Yii::t('app', 'Table'),
            'sql' => Yii::t('app', 'SQL'),
            'sql_log' => Yii::t('app', 'SQL Log'),
            'template' => Yii::t('app', 'Template'),
            'process' => Yii::t('app', 'Process'),
            'rating' => Yii::t('app', 'Rating'),
            'enable' => Yii::t('app', 'Enable'),
            'return' => Yii::t('app', 'Return'),
        ];
    }
}
