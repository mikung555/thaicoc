<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "script_log".
 *
 * @property integer $aid
 * @property integer $gid
 * @property integer $sid
 * @property integer $active
 * @property integer $error
 * @property string $action_at
 */
class ScriptLog extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'script_log';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['gid', 'sid', 'active', 'error', 'action_at'], 'required'],
            [['gid', 'sid', 'active', 'error'], 'integer'],
            [['action_at'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'aid' => Yii::t('app', 'Aid'),
            'gid' => Yii::t('app', 'Gid'),
            'sid' => Yii::t('app', 'Sid'),
            'active' => Yii::t('app', 'Active'),
            'error' => Yii::t('app', 'Error'),
            'action_at' => Yii::t('app', 'Action At'),
        ];
    }
}
