<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "const_amphur".
 *
 * @property integer $AMPHUR_ID
 * @property string $AMPHUR_CODE
 * @property string $AMPHUR_NAME
 * @property string $POSTCODE
 * @property integer $GEO_ID
 * @property integer $PROVINCE_ID
 */
class ConstAmphur extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'const_amphur';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['AMPHUR_CODE', 'AMPHUR_NAME', 'POSTCODE'], 'required'],
            [['GEO_ID', 'PROVINCE_ID'], 'integer'],
            [['AMPHUR_CODE'], 'string', 'max' => 4],
            [['AMPHUR_NAME'], 'string', 'max' => 150],
            [['POSTCODE'], 'string', 'max' => 5]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'AMPHUR_ID' => Yii::t('app', 'Amphur  ID'),
            'AMPHUR_CODE' => Yii::t('app', 'Amphur  Code'),
            'AMPHUR_NAME' => Yii::t('app', 'Amphur  Name'),
            'POSTCODE' => Yii::t('app', 'Postcode'),
            'GEO_ID' => Yii::t('app', 'Geo  ID'),
            'PROVINCE_ID' => Yii::t('app', 'Province  ID'),
        ];
    }
}
