<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "ezform_assign".
 *
 * @property string $ezf_id
 * @property string $user_id
 * @property integer $status
 *
 * @property Ezform $ezf
 * @property User $user
 */
class EzformAssign extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ezform_assign';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ezf_id', 'user_id'], 'required'],
            [['ezf_id', 'user_id', 'status'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ezf_id' => Yii::t('app', 'รหัสฟอร์ม'),
            'user_id' => Yii::t('app', 'รหัสผู้ใช้'),
            'status' => Yii::t('app', 'active=1, '),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEzf()
    {
        return $this->hasOne(Ezform::className(), ['ezf_id' => 'ezf_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @inheritdoc
     * @return EzformAssignQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new EzformAssignQuery(get_called_class());
    }
}
