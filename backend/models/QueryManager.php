<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "query_manager".
 *
 * @property string $id
 * @property string $user_id
 * @property string $ezf_id
 * @property integer $status
 * @property integer $user_group
 */
class QueryManager extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'query_manager';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'ezf_id', 'status'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Primary key',
            'user_id' => 'Data manager',
            'ezf_id' => 'ชื่อฟอร์ม',
            'status' => 'สถานะ',
            'user_group' => 'user group'
        ];
    }
}
