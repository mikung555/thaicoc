<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "query_request".
 *
 * @property string $id
 * @property string $ezf_id
 * @property string $target_id
 * @property string $data_id
 * @property string $ezf_field_id
 * @property string $new_val
 * @property string $old_val
 * @property string $note
 * @property integer $status
 * @property string $user_create
 * @property string $user_update
 * @property string $time_create
 * @property string $time_update
 * @property string $approved_by
 * @property string approved_note
 * @property string $userkey
 * @property string $userkey_status
 * @property string $new_changed
 * @property string $open_status
 * @property string $xsourcex
 */
class QueryRequest extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'query_request';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ezf_field_id'], 'required', 'message'=>'กรุณาเลือก Field'],
            [['ezf_id', 'target_id', 'data_id', 'ezf_field_id', 'status', 'user_create', 'user_update', 'approved_by'], 'integer'],
            [['ezf_id', 'target_id', 'data_id', 'new_val', 'note'], 'required'],
            [['new_val', 'note', 'approved_note', 'new_changed'], 'string'],
            [['time_create', 'time_update', 'userkey', 'userkey_status', 'open_status', 'old_val', 'xsourcex'], 'safe']
        ];
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ezf_id' => 'ชื่อฟอร์ม',
            'target_id' => 'ชื่อเป้าหมาย',
            'data_id' => 'Data ID',
            'ezf_field_id' => 'ชื่อคำถาม (ชื่อฟิลด์)',
            'new_val' => 'ค่าใหม่ที่ต้องการแก้ไข',
            'old_val' => 'ค่าเดิม',
            'new_changed' => 'แก้ไขให้เป็น',
            'note' => 'สาเหตุที่ต้องการแก้ไข',
            'status' => 'สถานะ',
            'user_create' => 'ขอแก้โดย',
            'user_update' => 'User Update',
            'time_create' => 'ขอเมื่อ',
            'time_update' => 'Update Time',
            'approved_by' => 'Approved By',
            'approved_note' => 'เหตุผล',
            'userkey' => 'User key',
            'userkey_status' => 'User key (status)',
            'open_status' => 'Status of topic',
            'xsourcex' => 'Site'
        ];
    }
}
