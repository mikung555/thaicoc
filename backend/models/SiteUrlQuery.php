<?php

namespace backend\models;

/**
 * This is the ActiveQuery class for [[SiteUrl]].
 *
 * @see SiteUrl
 */
class SiteUrlQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return SiteUrl[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return SiteUrl|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}