<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\ScriptResults;

/**
 * ScriptResultsSearch represents the model behind the search form about `backend\models\ScriptResults`.
 */
class ScriptResultsSearch extends ScriptResults
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['rid', 'gid', 'sid', 'aid', 'rating'], 'integer'],
            [['title', 'name', 'description', 'result', 'result_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ScriptResults::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'rid' => $this->rid,
            'gid' => $this->gid,
            'sid' => $this->sid,
            'aid' => $this->aid,
            'rating' => $this->rating,
            'result_at' => $this->result_at,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'result', $this->result]);

        return $dataProvider;
    }
}
