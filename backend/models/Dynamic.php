<?php

namespace backend\models;



class Dynamic extends \yii\db\ActiveRecord
{
    protected static $table;
    public $field_desc;

    public function _construct($table, $scenario)
    {
        parent::_construct($scenario);
        self::$table = $table;
    }

    public static function tableName()
    {
        return self::$table;
    }

    /* UPDATE */
    public static function setTableName($table)
    {
        self::$table = $table;
    }
}
