<?php

namespace backend\models;


class Edat extends \yii\db\ActiveRecord
{
    protected static $table;
    public $field_desc;

    public function _construct($table, $scenario)
    {
        parent::_construct($scenario);
        self::$table = $table;
    }

    public static function tableName()
    {
        return self::$table;
    }

    /* UPDATE */
    public static function setTableName($table)
    {
        self::$table = $table;
    }

    /**
     * @inheritdoc
     * @return TbdataQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new EdatQuery(get_called_class());
    }
}
