<?php

namespace backend\models;

use Yii;
use yii\bootstrap\Html;

/**
 * This is the model class for table "query_reply".
 *
 * @property string $id
 * @property string $query_id
 * @property string $file_upload
 * @property integer $bookmark
 * @property string $ezf_id
 * @property string $target_id
 * @property string $data_id
 * @property string $ezf_field_id
 * @property string $new_changed
 * @property string $old_val
 * @property string $new_val
 * @property string $note
 * @property integer $status
 * @property string $user_create
 * @property string $user_update
 * @property string $time_create
 * @property string $time_update
 * @property string $reply_note
 * @property string $userkey
 */
class QueryReply extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'query_reply';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['query_id', 'ezf_id', 'target_id', 'data_id', 'ezf_field_id', 'new_val'], 'required'],
            [['query_id', 'bookmark', 'ezf_id', 'target_id', 'data_id', 'ezf_field_id', 'status', 'user_create', 'user_update',], 'integer'],
            [['new_changed', 'old_val', 'new_val', 'note', 'reply_note'], 'string'],
            [['time_create', 'time_update'], 'safe'],
            [['file_upload'], 'string', 'max' => 200]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Primary key',
            'query_id' => 'Query ID',
            'file_upload' => 'การแนบไฟล์',
            'bookmark' => 'Bookmark',
            'ezf_id' => 'ชื่อฟอร์ม',
            'target_id' => 'ชื่อเป้าหมาย',
            'data_id' => 'Data ID',
            'ezf_field_id' => 'ชื่อคำถาม (ชื่อฟิลด์)',
            'new_val' => 'ค่าใหม่ที่ต้องการแก้ไข',
            'old_val' => 'ค่าเดิม',
            'new_changed' => 'แก้ไขให้เป็น',
            'note' => 'สาเหตุที่ต้องการแก้ไข',
            'status' => 'สถานะ',
            'user_create' => 'ตอบโดย',
            'user_update' => 'User Update',
            'time_create' => 'ตอบเมื่อ',
            'time_update' => 'Update Time',
            'reply_note' => 'เหตุผลการอนุมัติ',
        ];
    }
    public function lookUpfile($data){
        $html= '';
        $array = explode(',', $data);
        foreach($array  as $i => $value){
            $url = '/../uploads/query_tools/' . $value;
            $html .= Html::a('เอกสารแนบ '.($i+1), $url, ['target' => '_blank', 'class'=>'', 'title' => 'เปิด',]);
            $html .= '<br>';
        }
        return $html;
    }
}
