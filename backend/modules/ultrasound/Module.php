<?php

namespace backend\modules\ultrasound;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\modules\ultrasound\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
