<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\ultrasound\models\DicomUpload */

$this->title = 'Create Dicom Upload';
$this->params['breadcrumbs'][] = ['label' => 'Dicom Uploads', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dicom-upload-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
