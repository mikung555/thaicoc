<?php

namespace backend\modules\inv\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\inv\models\InvMenu;

/**
 * InvMenuSearch represents the model behind the search form about `backend\modules\inv\models\InvMenu`.
 */
class InvMenuSearch extends InvMenu
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['menu_id', 'gid', 'menu_parent', 'menu_active', 'created_by'], 'integer'],
            [['menu_name', 'menu_content', 'created_at'], 'safe'],
            [['menu_order'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = InvMenu::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'menu_id' => $this->menu_id,
            'gid' => $this->gid,
            'menu_parent' => $this->menu_parent,
            'menu_active' => $this->menu_active,
            'menu_order' => $this->menu_order,
            'created_by' => $this->created_by,
            'created_at' => $this->created_at,
        ]);

        $query->andFilterWhere(['like', 'menu_name', $this->menu_name])
            ->andFilterWhere(['like', 'menu_content', $this->menu_content]);

        return $dataProvider;
    }
}
