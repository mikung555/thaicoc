<?php

namespace backend\modules\inv\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\inv\models\InvMain;

/**
 * InvMainSearch represents the model behind the search form about `backend\modules\inv\models\InvMain`.
 */
class InvMainSearch extends InvMain
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['special', 'created_by', 'mid', 'ezf_id', 'comp_id_target'], 'integer'],
            [['sitecode', 'ezf_name', 'ezf_table', 'pk_field', 'field_name', 'label_field', 'order_field', 'enable_field', 'enable_form', 'enable_report'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = InvMain::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'mid' => $this->mid,
            'ezf_id' => $this->ezf_id,
            'comp_id_target' => $this->comp_id_target,
	    'created_by' => $this->created_by,
	    'special' => $this->special,
        ]);

        $query->andFilterWhere(['like', 'sitecode', $this->sitecode])
            ->andFilterWhere(['like', 'ezf_name', $this->ezf_name])
            ->andFilterWhere(['like', 'ezf_table', $this->ezf_table])
            ->andFilterWhere(['like', 'pk_field', $this->pk_field])
            ->andFilterWhere(['like', 'field_name', $this->field_name])
            ->andFilterWhere(['like', 'label_field', $this->label_field])
            ->andFilterWhere(['like', 'order_field', $this->order_field])
            ->andFilterWhere(['like', 'enable_field', $this->enable_field])
            ->andFilterWhere(['like', 'enable_form', $this->enable_form])
            ->andFilterWhere(['like', 'enable_report', $this->enable_report]);

        return $dataProvider;
    }
}
