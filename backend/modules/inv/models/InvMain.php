<?php

namespace backend\modules\inv\models;

use Yii;

/**
 * This is the model class for table "inv_main".
 *
 * @property integer $mid
 * @property string $sitecode
 * @property integer $ezf_id
 * @property string $ezf_name
 * @property string $ezf_table
 * @property integer $comp_id_target
 * @property string $pk_field
 * @property string $field_name
 * @property string $label_field
 * @property string $order_field
 * @property string $enable_field
 * @property string $enable_form
 * @property string $enable_report
 * @property string $created_by
 * @property string $special
 */
class InvMain extends \yii\db\ActiveRecord
{
    public $pk_join;
    public $enable_target;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'inv_main';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sitecode', 'ezf_id', 'field_name'], 'required'],
            [['created_by', 'special','ezf_id', 'comp_id_target'], 'integer'],
            [['enable_field', 'enable_form', 'enable_report'], 'string'],
            [['sitecode'], 'string', 'max' => 10],
	    [['main_ezf_id', 'main_comp_id_target', 'field_name', 'order_field'], 'safe'],
            [['ezf_name', 'ezf_table', 'main_ezf_name', 'main_ezf_table'], 'string', 'max' => 100],
            [['pk_field', 'label_field'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'mid' => Yii::t('app', 'Mid'),
            'sitecode' => Yii::t('app', 'Sitecode'),
	    'main_ezf_id' => Yii::t('app', 'EzF ID'),
            'main_ezf_name' => Yii::t('app', 'ชื่อฟอร์ม'),
            'main_ezf_table' => Yii::t('app', 'EzF Table'),
            'main_comp_id_target' => Yii::t('app', 'Comp Id Target'),
            'ezf_id' => Yii::t('app', 'ฟอร์ม'),
            'ezf_name' => Yii::t('app', 'Ezf Name'),
            'ezf_table' => Yii::t('app', 'Ezf Table'),
            'comp_id_target' => Yii::t('app', 'Comp Id Target'),
            'pk_field' => Yii::t('app', 'Pk'),
	    'special' => Yii::t('app', 'Special'),
	    'created_by' => Yii::t('app', 'Created By'),
            'field_name' => Yii::t('app', 'เลือกตัวแปรสำหรับแสดงรายการเป้าหมายเพื่อเพิ่มลงในใบกำกับงาน'),
            'label_field' => Yii::t('app', 'เรียงลำดับ'),
            'order_field' => Yii::t('app', 'เลือกตัวแปรสำหรับเรียงลำดับ(ถ้าไม่เลือกจะเรียงตาม วันที่บันทึกของฟอร์มตั้งต้น)'),
            'enable_field' => Yii::t('app', 'Enable Field'),
            'enable_form' => Yii::t('app', 'Enable Form'),
            'enable_report' => Yii::t('app', 'Enable Report'),
        ];
    }
}
