<?php

namespace backend\modules\inv\models;

use Yii;

/**
 * This is the model class for table "inv_favorite".
 *
 * @property integer $fav_id
 * @property integer $gid
 * @property integer $user_id
 */
class InvFavorite extends \yii\db\ActiveRecord
{
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'inv_favorite';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['gid', 'user_id'], 'required'],
            [['gid', 'user_id'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'fav_id' => Yii::t('app', 'Fav ID '),
            'gid' => Yii::t('app', 'Gid'),
            'user_id' => Yii::t('app', 'User ID'),
        ];
    }
}
