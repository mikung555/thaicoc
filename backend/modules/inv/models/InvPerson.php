<?php

namespace backend\modules\inv\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
/**
 * This is the model class for table "inv_person".
 *
 * @property integer $inv_id
 * @property integer $ezf_id
 * @property integer $comp_id_target
 * @property integer $dataid
 * @property integer $target
 * @property string $hsitecode
 * @property string $inv_name
 * @property integer $gid
 * @property integer $created_by
 * @property string $created_at
 * @property integer $updated_by
 * @property string $updated_at
 * @property integer $status
 * @property string $meta
 * @property integer $person_id
 * @property integer $person_ezf_id
 * @property integer $person_comp_target
 * @property integer $person_target
 */
class InvPerson extends \yii\db\ActiveRecord
{
    public function behaviors() {
            return [
                    [
                            'class' => TimestampBehavior::className(),
                            'value' => new Expression('NOW()'),
                    ],
                    [
                            'class' => BlameableBehavior::className(),
                    ],
            ];
    }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'inv_person';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['inv_id'], 'required'],
            [['inv_id', 'ezf_id', 'comp_id_target', 'dataid', 'target', 'gid', 'created_by', 'updated_by', 'status', 'person_id', 'person_ezf_id', 'person_comp_target', 'person_target'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['meta'], 'string'],
            [['hsitecode'], 'string', 'max' => 10],
            [['inv_name'], 'string', 'max' => 200]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'inv_id' => Yii::t('app', 'Inv ID'),
            'ezf_id' => Yii::t('app', 'Ezf ID'),
            'comp_id_target' => Yii::t('app', 'Comp Id Target'),
            'dataid' => Yii::t('app', 'Dataid'),
            'target' => Yii::t('app', 'Target'),
            'hsitecode' => Yii::t('app', 'Hsitecode'),
            'inv_name' => Yii::t('app', 'Inv Name'),
            'gid' => Yii::t('app', 'Gid'),
            'created_by' => Yii::t('app', 'Created By'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'status' => Yii::t('app', 'Status'),
            'meta' => Yii::t('app', 'Meta'),
            'person_id' => Yii::t('app', 'Person ID'),
            'person_ezf_id' => Yii::t('app', 'Person Ezf ID'),
            'person_comp_target' => Yii::t('app', 'Person Comp Target'),
            'person_target' => Yii::t('app', 'Person Target'),
        ];
    }
}
