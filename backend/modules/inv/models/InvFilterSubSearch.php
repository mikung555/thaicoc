<?php

namespace backend\modules\inv\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\inv\models\InvFilterSub;

/**
 * InvFilterSubSearch represents the model behind the search form about `backend\modules\inv\models\InvFilterSub`.
 */
class InvFilterSubSearch extends InvFilterSub
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['filter_order','gid', 'public', 'sub_id', 'urine_status', 'filter_id', 'created_by'], 'integer'],
            [['share', 'sub_name', 'sitecode', 'options'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $comp, $module)
    {
	$sitecode = Yii::$app->user->identity->userProfile->sitecode;
	$userId = Yii::$app->user->id;
	
        $query = InvFilterSub::find()->where("gid=:module AND filter_id=:comp AND (created_by=:created_by || public=1 || $userId IN (`share`) )", [
	    ':comp'=>$comp,
	    ':module'=>$module,
	    ':created_by'=>$userId,
	]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'sub_id' => $this->sub_id,
            'urine_status' => $this->urine_status,
            'filter_id' => $this->filter_id,
            'created_by' => $this->created_by,
	    'public' => $this->public,
	    'gid' => $this->gid,
        ]);

        $query->andFilterWhere(['like', 'sub_name', $this->sub_name])
            ->andFilterWhere(['like', 'sitecode', $this->sitecode])
	    ->andFilterWhere(['like', 'share', $this->share])
	    ->andFilterWhere(['like', 'options', $this->options]);

        return $dataProvider;
    }
}
