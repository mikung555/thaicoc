<?php

namespace backend\modules\inv\models;

use Yii;

/**
 * This is the model class for table "inv_filter_sub".
 *
 * @property integer $sub_id
 * @property string $sub_name
 * @property integer $urine_status
 * @property integer $filter_id
 * @property string $sitecode
 * @property integer $created_by
 * @property integer $public
 * @property string $share
 * @property integer $gid
 *  @property string $options
 * @property string $filter_order
 */
class InvFilterSub extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'inv_filter_sub';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sub_name', 'sitecode'], 'required'],
            [['filter_order','public', 'gid', 'urine_status', 'filter_id', 'created_by'], 'integer'],
            [['sub_name'], 'string', 'max' => 100],
            [['sitecode'], 'string', 'max' => 10],
	    [['share', 'options'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'sub_id' => Yii::t('app', 'ID'),
            'sub_name' => Yii::t('app', 'ใบกำกับงาน'),
            'filter_id' => Yii::t('app', 'รอบการให้บริการ'),
	    'gid' => Yii::t('app', 'โมดูล'),
            'sitecode' => Yii::t('app', 'sitecode'),
	    'urine_status' => Yii::t('app', 'ตรวจ Fecal/Urine'),
	    'public' => Yii::t('app', 'Public'),
	    'share' => Yii::t('app', 'Share'),
	    'options' => Yii::t('app', 'เงื่อนไขแสดงข้อมูล'),
        ];
    }
}
