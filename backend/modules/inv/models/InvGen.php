<?php

namespace backend\modules\inv\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
/**
 * This is the model class for table "inv_gen".
 *
 * @property integer $gid
 * @property string $gname
 * @property string $gdetail
 * @property string $gicon
 * @property integer $public
 * @property string $share
 * @property integer $created_by
 * @property string $created_at
 * @property integer $updated_by
 * @property string $updated_at
 * @property string $sitecode
 * @property integer $ezf_id
 * @property string $ezf_name
 * @property string $ezf_table
 * @property integer $comp_id_target
 * @property integer $special
 * @property string $pk_field
 * @property string $field_name
 * @property string $label_field
 * @property string $order_field
 * @property string $enable_field
 * @property string $enable_form
 * @property string $enable_report
 * @property string $active
 * @property string $approved
 * @property integer $parent_gid
 * @property string $subname
 * @property string $subicon
 */
class InvGen extends \yii\db\ActiveRecord
{
    public $pk_join;
    
    public function behaviors() {
		return [
			[
				'class' => TimestampBehavior::className(),
				'value' => new Expression('NOW()'),
			],
			[
				'class' => BlameableBehavior::className(),
			],
		];
	}
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'inv_gen';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['gname'], 'required'],
            [['glink', 'gtag', 'gdevby', 'gdetail', 'inv_js'], 'string'],
            //[[], 'integer'],
            [['subname','subicon','parent_gid','enable_target', 'main_ezf_id', 'main_comp_id_target', 'gtype', 'gsystem', 'fixcol','approved','field_name', 'order_field', 'enable_field', 'enable_form', 'enable_report', 'active', 'public', 'created_by', 'updated_by', 'ezf_id', 'comp_id_target', 'special', 'share', 'gicon', 'created_at', 'updated_at'], 'safe'],
            [['gname'], 'string', 'max' => 150],
            [['sitecode'], 'string', 'max' => 10],
            [['ezf_name', 'ezf_table', 'main_ezf_name', 'main_ezf_table'], 'string', 'max' => 100],
            [['pk_field', 'label_field'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'gid' => Yii::t('app', 'Gid'),
            'gname' => Yii::t('app', 'ชื่อโมดูล'),
            'gdetail' => Yii::t('app', 'รายละเอียด'),
            'gicon' => Yii::t('app', 'icon'),
            'public' => Yii::t('app', 'เสนอให้เป็นโมดูลสาธารณะ'),
	    'active' => Yii::t('app', 'Active'),
	    'approved' => Yii::t('app', 'Approved'),
            'share' => Yii::t('app', 'เลือกสมาชิกเพื่อมอบสิทธิ์'),
            'created_by' => Yii::t('app', 'Created By'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'sitecode' => Yii::t('app', 'Sitecode'),
	    'main_ezf_id' => Yii::t('app', 'EzF ID'),
            'main_ezf_name' => Yii::t('app', 'ชื่อฟอร์ม'),
            'main_ezf_table' => Yii::t('app', 'EzF Table'),
            'main_comp_id_target' => Yii::t('app', 'Comp Id Target'),
            'ezf_id' => Yii::t('app', 'EzF ID'),
            'ezf_name' => Yii::t('app', 'ชื่อฟอร์ม'),
            'ezf_table' => Yii::t('app', 'EzF Table'),
            'comp_id_target' => Yii::t('app', 'Comp Id Target'),
            'special' => Yii::t('app', 'Special'),
            'pk_field' => Yii::t('app', 'Pk Field'),
            'field_name' => Yii::t('app', 'เลือกตัวแปรสำหรับแสดงรายการเป้าหมายเพื่อเพิ่มลงในใบกำกับงาน'),
            'label_field' => Yii::t('app', 'เรียงลำดับ'),
            'order_field' => Yii::t('app', 'เลือกตัวแปรสำหรับเรียงลำดับ(ถ้าไม่เลือกจะเรียงตาม วันที่บันทึกของฟอร์มตั้งต้น)'),
            'enable_field' => Yii::t('app', 'Enable Field'),
            'enable_form' => Yii::t('app', 'Enable Form'),
            'enable_report' => Yii::t('app', 'Enable Report'),
	    'fixcol'=>Yii::t('app', 'เรียงลำดับ'),
	    'subname'=>Yii::t('app', 'ชื่อโมดูลย่อย'),
            'subicon'=>Yii::t('app', 'icon โมดูลย่อย'),
            
	    'glink'=>Yii::t('app', 'Link'),
	    'gtag'=>Yii::t('app', 'Tag'),
	    'gdevby'=>Yii::t('app', 'พัฒนาโดย'),
	    'gtype'=>Yii::t('app', 'ประเภท'),
	    'gsystem'=>Yii::t('app', 'System Module'),
            'enable_target'=>Yii::t('app', 'เปิดให้เป้าหมายกรอกข้อมูลได้ (เป้าหมายจะต้องเป็น Person profile เท่านั้นและไม่ควรเลือกฟอร์มเกิน 10 ฟอร์ม)'),
        ];
    }
}
