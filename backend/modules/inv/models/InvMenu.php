<?php

namespace backend\modules\inv\models;

use Yii;

/**
 * This is the model class for table "inv_menu".
 *
 * @property integer $menu_id
 * @property integer $gid
 * @property string $menu_name
 * @property integer $menu_parent
 * @property string $menu_content
 * @property integer $menu_active
 * @property double $menu_order
 * @property integer $created_by
 * @property string $created_at
 */
class InvMenu extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'inv_menu';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['gid', 'menu_name'], 'required'],
            [['gid', 'menu_parent', 'menu_active', 'created_by'], 'integer'],
            [['menu_content'], 'string'],
            [['menu_order'], 'number'],
            [['created_at'], 'safe'],
            [['menu_name'], 'string', 'max' => 150]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'menu_id' => Yii::t('app', 'Menu ID'),
            'gid' => Yii::t('app', 'Gid'),
            'menu_name' => Yii::t('app', 'ชื่อเมนู'),
            'menu_parent' => Yii::t('app', 'เป็น Sub Menu ของ'),
            'menu_content' => Yii::t('app', 'เนื้อหา'),
            'menu_active' => Yii::t('app', 'Menu Active'),
            'menu_order' => Yii::t('app', 'เรียงไว้ถัดจาก'),
            'created_by' => Yii::t('app', 'Created By'),
            'created_at' => Yii::t('app', 'Created At'),
        ];
    }
}
