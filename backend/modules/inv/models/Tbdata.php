<?php

namespace backend\modules\inv\models;

use Yii;
/**
 * This is the model class for table "tbdata_1435745159010048800".
 *
 * @property integer $id
 */
class Tbdata extends \yii\db\ActiveRecord
{
    protected static $table;
    protected static $colFieldsAddon;
    
    public $field_desc;

//    public function _construct($table, $scenario)
//    {
//        parent::_construct($scenario);
//        self::$table = $table;
//    }

    public function attributes()
    {
	$attrDB = array_keys(static::getTableSchema()->columns);
	$colFieldsID=[];
	
	$colFieldsID[] = 'detail_main';
	$colFieldsID[] = 'id2';
	
	$fields = Yii::$app->session['sql_fields'];
	
	if(isset($fields) && !empty($fields)){
	    foreach ($fields as $key => $value) {
		$colFieldsID[]=$value['sql_id_name'];
		$colFieldsID[]=$value['sql_idsubmit_name'];
		
		$result = trim($value['result_field']);
		if($result!=''){
		    $colFieldsID[]=$value['sql_result_name'];
		}
		if(isset($value['value_options']) && !empty($value['value_options'])){
		    $cond = $value['value_options'][$value];
		    $cform = $cond['form'];
		    $cfield = $cond['field'];
		    $fixvalue = $cond['value'];
		    foreach ($cform as $ckey => $cvalue) {
			$colFieldsID[]='result_'.$cvalue.'_'.$cfield[$ckey].'_'.$fixvalue[$ckey];
		    }
		}
	    }
	} else {
	    $fields = Yii::$app->session['sql_main_fields'];
	    $main_forms = \appxq\sdii\utils\SDUtility::string2Array($fields['enable_form']);
	    
//	    $modelEzform = \backend\modules\ezforms\models\Ezform::find()
//		->where('ezform.ezf_id=:ezf_id', [':ezf_id'=>$fields->ezf_id])
//		->one();
//	
	    //if(isset($modelEzform->comp_id_target) && !empty($modelEzform->comp_id_target)){
            //\appxq\sdii\utils\VarDumper::dump($fields->main_ezf_id);
//		$dataCompMain = \backend\modules\component\models\EzformComponent::find()->where('comp_id=:comp_id',[':comp_id'=>$fields->comp_id_target])->one();
//		if($dataCompMain && $fields->main_ezf_id!=$fields->ezf_id){
//		   $dataFieldsMain = \backend\modules\inv\classes\InvFunc::getFields($fields->main_ezf_id, 'fxmain_');
//		   $fieldsMain = array_keys($dataFieldsMain);
//		   $colFieldsID = array_merge($colFieldsID, $fieldsMain);
//		   
//		}
                
                if(isset(self::$colFieldsAddon) && !empty(self::$colFieldsAddon) ){
                    $colFieldsID = array_merge($colFieldsID, self::$colFieldsAddon);
                }
                
	    //}
	    
	    if(isset($main_forms) && !empty($main_forms)){
		
		$ffixFields = isset($main_forms['form'])?$main_forms['form']:[];
		$ffixLabel = isset($main_forms['label'])?$main_forms['label']:[];
		$ffixWidth = isset($main_forms['width'])?$main_forms['width']:[];
		$ffixCondition = isset($main_forms['condition'])?$main_forms['condition']:[];
				
		foreach ($ffixFields as $key => $value) {
		    $colFieldsID[]='id_'.$value;
		    $colFieldsID[]='idsubmit_'.$value;
		    $colFieldsID[]='idall_'.$value;
		    $colFieldsID[]='result_'.$value;
		    
		    if(isset($ffixCondition[$value]) && !empty($ffixCondition[$value])){
			$cond = $ffixCondition[$value];
			$cform = $cond['form'];
			$cfield = $cond['field'];
			
			foreach ($cform as $ckey => $cvalue) {
			    $colFieldsID[]='result_'.$cvalue.'_'.$cfield[$ckey].'_'.$ckey;
			}
			
		    }
		}
	    }
	}
	 
        return array_merge($attrDB, $colFieldsID);
    }
    
    public static function tableName()
    {
        return self::$table;
    }

    /* UPDATE */
    public static function setTableName($table)
    {
        self::$table = $table;
    }
    
    public static function colFieldsAddon()
    {
        return self::$colFieldsAddon;
    }

    /* UPDATE */
    public static function setColFieldsAddon($colFields)
    {
        if(isset($colFields)){
            self::$colFieldsAddon = $colFields;
        }
    }

}
