<?php
namespace backend\modules\inv\classes;

use Yii;
use yii\helpers\Url;
use yii\helpers\Html;
use appxq\sdii\utils\SDUtility;
use yii\helpers\ArrayHelper;
use backend\modules\ezforms\models\Ezform;
use backend\modules\ezforms\components\EzformQuery;
use backend\modules\ezforms\components\EzformFunc;
use common\lib\codeerror\helpers\GenMillisecTime;
use backend\modules\inv\models\TbdataAll;
use yii\db\Expression;

use backend\modules\inv\classes\InvQuery;
use backend\modules\component\models\EzformComponent;
use backend\models\EzformTarget;
/**
 * OvccaFunc class file UTF-8
 * @author SDII <iencoded@gmail.com>
 * @copyright Copyright &copy; 2015 AppXQ
 * @license http://www.appxq.com/license/
 * @version 1.0.0 Date: 9 ก.พ. 2559 12:38:14
 * @link http://www.appxq.com/
 * @example 
 */
class InvFuncFix {
    public static function getAlign($align) {
	$str = substr($align, 0, 1);
	$str = strtoupper($str);
	return $str;
    }
    
    public static function createColumnsResult($valueField) {
	$columns = [
	    'header'=>$valueField['header'],
	    'format'=>'raw',
	    'value'=>function ($data) use ($valueField) { 
		if ($data['id']!=null) {
		    $field = $valueField;

		    $rowReturn = '';
		    
		    if ($data[$field['sql_id_name']]!=null) {
			
			$arrStr = explode(',', $data[$field['sql_id_name']]);
			$arrResult = explode(',', $data[$field['sql_result_name']]);
			
			
			foreach ($arrStr as $key => $value) {
			    
			    $icon = '<i class="glyphicon glyphicon-exclamation-sign"></i>';
			    $color = '#999;';
			    
			    
		    
			    foreach ($options['value'] as $keyOp => $valueOp) {
				if($arrResult[$key]==$valueOp){
				    $icon = $options['web'][$keyOp];
				    $color = $options['color'][$keyOp];
				}
			    }
			    $rurl = base64_encode(Yii::$app->request->url);

			    $rowReturn .= Html::a($icon, Url::to(['/inputdata/redirect-page', 'ezf_id'=>$field['ezf_id'], 'dataid'=>$value, 'rurl'=>$rurl]), [
				'style'=>'font-size: 18px; color:'.$color.';'
			    ]).' ';
			}
		    }
		    
		    $rowReturn .= Html::a('<i class="glyphicon glyphicon-plus" style="color:#00a65a;font-size: 18px;"></i>', Url::to(['/inputdata/step4',
			'ezf_id'=>$field['ezf_id'],
			'target'=>base64_encode($data->ptid),
			'comp_id_target'=>$field['comp_id_target'],
			'rurl'=>base64_encode(Yii::$app->request->url),
		    ]));

		    return $rowReturn;
		} else {
		    return '';
		}
	    },
	    'filter'=>'',
	    'headerOptions'=>['style'=>'text-align: center;'],
	    'contentOptions'=>['style'=>"min-width:{$valueField['width']}px; "],
	];
	    
	return $columns;
    }
    
    public static function createColumns($valueField) {
	 
	$columns = [
	    'header'=>$valueField['header'],
	    'format'=>'raw',
	    'value'=>function ($data) use ($valueField) { 
		if ($data['id']!=null) {
		    $field = $valueField;

		    $rowReturn = '';
		   
		    if ($data[$field['sql_id_name']]!=null) {
			$arrStr = explode(',', $data[$field['sql_id_name']]);

			foreach ($arrStr as $value) {
			    $icon = 'class="glyphicon glyphicon-ok" style="font-size: 18px;"';
			    $rurl = base64_encode(Yii::$app->request->url);

			    $rowReturn .= Html::a('<i '.$icon.'></i>', Url::to(['/inputdata/redirect-page', 'ezf_id'=>$field['ezf_id'], 'dataid'=>$value, 'rurl'=>$rurl])).' ';
			}
		    }

		    $rowReturn .= Html::a('<i class="glyphicon glyphicon-plus" style="color:#00a65a;font-size: 18px;"></i>', Url::to(['/inputdata/step4',
			'ezf_id'=>$field['ezf_id'],
			'target'=>base64_encode($data->ptid),
			'comp_id_target'=>$field['comp_id_target'],
			'rurl'=>base64_encode(Yii::$app->request->url),
		    ]));

		    return $rowReturn;
		} else {
		    return '';
		}
	    },
	    'filter'=>'',
	    'headerOptions'=>['style'=>'text-align: center;'],
	    'contentOptions'=>['style'=>"min-width:{$valueField['width']}px; "],
	];
	    
	return $columns;
    }
    
    public static function createColumnsCount($valueField, $special) {
	
	$columns = [
	    'header'=>$valueField['header'],
	    'format'=>'raw',
	    //'visible' => $valueField['visible_field']==1?self::visibleColumn($valueField):true,
	    'value'=>function ($data) use ($valueField, $special) { 
                
		if ($data['id']!=null) {
		    $field = $valueField;
                    $modelFields = Yii::$app->session['sql_main_fields'];
                    
		    $rowReturn = '';
		    $pkJoin = 'id';
                    $pkJoin2 = 'target';
		    if($special==1){
			$pkJoin = 'ptid';
                        $pkJoin2 = 'ptid';
		    }
                    
                    $display = SDUtility::string2Array($field['display_options']);
                    $condition = SDUtility::string2Array($field['value_options']);
                 
                    $result = 'id';
                    $dataCond = [];
                    if($field['show']=='detail'){
                        $result = isset($field['field_detail'])?"CONCAT({$field['field_detail']})":'id';
                        
                        
                    } elseif ($field['show']=='condition') {
                        if(isset($display) && !empty($display)){
                            $result = isset($field['result'])?$field['result']:'id';
                        }
                    } elseif ($field['show']=='custom') {
                        if(isset($display) && !empty($display)){
                            if(isset($display['custom']) && $display['custom']!=''){
                                $result = "CONCAT({$display['custom']})";
                            }
                        }
                    } elseif (isset($condition) && !empty($condition)) {
                        $cform = $condition['form'];
                        $cfield = $condition['field'];
                        
                        foreach ($cform as $ckey => $cvalue) {
//                            $cond_result ='result_'.$cvalue.'_'.$cfield[$ckey].'_'.$ckey;
//                            $ezform = \backend\modules\inv\classes\InvQuery::getEzformById($cvalue);
//		    
//                            $sql_cond = "SELECT et.{$cfield[$ckey]} FROM {$ezform['ezf_table']} et WHERE et.$pkJoin2 = :id AND et.rstat<>0 AND et.rstat<>3 ";
//                            $dataCond[$cond_result] = Yii::$app->db->createCommand($sql_cond, [':id'=>$data[$pkJoin]])->queryColumn();
//                            
                        }
                    }
                    
//                    $sql = "SELECT et.id, et.rstat, $result AS result FROM {$field['ezf_table']} et WHERE et.$pkJoin2 = :id AND et.rstat<>0 AND et.rstat<>3 ";
//	
//                    $dataItems = Yii::$app->db->createCommand($sql, [':id'=>$data[$pkJoin]])->queryAll();
                    $data_all = '';
                    $data_draft = '';
                    $data_submit = '';
                    $data_result = '';
                    if($dataItems){
                        $comma = '';
                    foreach ($dataItems as $keyItem => $valueItem) {
                            $data_all .= $comma.$valueItem['id'];
                            if($valueItem['rstat']==1){
                                $data_draft .= $comma.$valueItem['id'];
                            } elseif($valueItem['rstat']==2) {
                                
                                $data_submit .= $comma.$valueItem['id'];
                            }
                            $data_result .= $comma.$valueItem['result'];
                            
                            $comma = ',';
                        }
                    }
		    //\appxq\sdii\utils\VarDumper::dump($dataCond);
                    
		    $numAll =0;
		    if ($data_all!='') {
			$arrSubmit = [];
			$arrDraft = [];
			$arrId = [];
			$arrResult = [];
			
			if($data_draft!=''){
			    $arrDraft = explode(',', $data_draft);
			}
			if($data_submit!=''){
			    $arrSubmit = explode(',', $data_submit);
			}
			if($data_all!=''){
			    $arrId = explode(',', $data_all);
			}
			if($data_result!=''){
			    $arrResult = explode(',', $data_result);
			}
			
			$result = '';
			$numDraft = count($arrDraft);
			$numSubmit = count($arrSubmit);
			$numAll = $numDraft+$numSubmit;
                        
			if($field['show']=='detail'){
			    
			    if($data_all!='' && $data_result!=''){
				
				foreach ($arrId as $keyId => $valueId) {
				    
				    $rowReturn .= Html::a("<span class=\"label label-default\" style=\"font-size: 13px; padding: 1px 5px; background-color: #DDD;\">{$arrResult[$keyId]}</span> ", NULL, [
					'data-url'=>Url::to(['/inv/inv-person/ezform-print',
					    'ezf_id'=>$field['ezf_id'],
					    'dataid'=>$valueId,
					    'target'=>base64_encode($data[$pkJoin]),
					    'comp_id_target'=>$field['comp_id_target'],
					]),
					'style'=>'cursor: pointer;',
					'data-toggle'=>'tooltip',
					'title'=>'แสดงข้อมูล',
					'class'=>'items-tooltip tooltip-detail open-ezfrom form-'.$field['ezf_id'],
					'data-item'=>$valueId
				    ]);
				    
				    
				}
			    }
			} elseif ($field['show']=='condition') {
			    
			    if(isset($display) && !empty($display)){
				
				if($data_all!='' && $data_result!=''){
				    $dcondition = isset($display['condition'])?$display['condition']:[];
				    $dvalue1 = isset($display['value1'])?$display['value1']:[];
				    $dvalue2 = isset($display['value2'])?$display['value2']:[];
				    $dicon = isset($display['icon'])?$display['icon']:[];
				    $dlabel = isset($display['label'])?$display['label']:[];

				    
				    foreach ($arrId as $keyId => $valueId) {
					
					foreach ($dcondition as $dkey => $dcond) {
					    $condVar = false;
					   
					    if($dcond=='between'){
						$strCond = "'{$arrResult[$keyId]}'>='{$dvalue1[$dkey]}' && '{$arrResult[$keyId]}'<='{$dvalue2[$dkey]}'";
						
						eval("\$condVar = $strCond;");
						
						
					    }elseif($ccond[$ckey]=='func'){
						$template = $dvalue1[$ckey];
						$strCond = strtr($template, ['{value}'=>$arrResult]);
						
						eval("\$condVar = $strCond;");

					    }
					    else { //between
						
						
						$strCond = "'{$arrResult[$keyId]}'".$dcond."'{$dvalue1[$dkey]}'";
						
						eval("\$condVar = $strCond;");
						
					    }
					    
					    if($condVar){
						$rowReturn .= Html::a("<span class=\"btn btn-default btn-xs\" style=\"background-color: #DDD;\">".(isset($dicon[$dkey])?"<i class=\"fa {$dicon[$dkey]}\"></i>":'?')."</span> ", null, [
						    'data-url'=>Url::to(['/inv/inv-person/ezform-print',
							'ezf_id'=>$field['ezf_id'],
							'dataid'=>$valueId,
							'target'=>base64_encode($data[$pkJoin]),
							'comp_id_target'=>$field['comp_id_target'],
						    ]),
						    'style'=>'cursor: pointer;',
						    'data-toggle'=>'tooltip',
						    'title'=> isset($dlabel[$dkey])?$dlabel[$dkey]:'แสดงข้อมูล',
						    'class'=>'items-tooltip tooltip-condition open-ezfrom form-'.$field['ezf_id'],
						    'data-item'=>$valueId
						]);
						
//						$rowReturn .= Html::a("<span class=\"btn btn-default btn-xs\" style=\"background-color: #DDD;\">".(isset($dicon[$dkey])?"<i class=\"fa {$dicon[$dkey]}\"></i>":'?')."</span> ", Url::to(['/inputdata/redirect-page',
//						    'ezf_id'=>$field['ezf_id'],
//						    'dataid'=>$valueId,
//						    'rurl'=>base64_encode(Yii::$app->request->url),
//						]), [
//						    'data-toggle'=>'tooltip',
//						    'title'=> isset($dlabel[$dkey])?$dlabel[$dkey]:'แสดงข้อมูล',
//						    'class'=>'items-tooltip tooltip-condition form-'.$field['ezf_id'],
//						    'data-item'=>$valueId
//						]);
					    }
					}
				    }
				}
			    }
			} elseif ($field['show']=='custom') {
				if($data_all!='' && $data_result!=''){

				    foreach ($arrId as $keyId => $valueId) {
					
					$rowReturn .= Html::a("{$arrResult[$keyId]} ", NULL, [//<span class=\"label label-default\" style=\"font-size: 13px; padding-bottom: 3px;padding-top: 0px; background-color: rgba(255, 255, 255, 0);\">
					    'data-url'=>Url::to(['/inv/inv-person/ezform-print',
						'ezf_id'=>$field['ezf_id'],
						'dataid'=>$valueId,
						'target'=>base64_encode($data[$pkJoin]),
						'comp_id_target'=>$field['comp_id_target'],
					    ]),
					    'style'=>'cursor: pointer;',
					    'data-toggle'=>'tooltip',
					    'title'=>'แสดงข้อมูล',
					    'class'=>'items-tooltip tooltip-custom open-ezfrom form-'.$field['ezf_id'],
					    'data-item'=>$valueId
					]);
					
//					$rowReturn .= Html::a("<span class=\"label label-default\" style=\"font-size: 13px; padding-bottom: 3px;padding-top: 0px; background-color: #DDD;\">{$arrResult[$keyId]}</span> ", Url::to(['/inputdata/redirect-page',
//					    'ezf_id'=>$field['ezf_id'],
//					    'dataid'=>$valueId,
//					    'rurl'=>base64_encode(Yii::$app->request->url),
//					]), [
//					    'data-toggle'=>'tooltip',
//					    'title'=>'แสดงข้อมูล',
//					    'class'=>'items-tooltip tooltip-custom form-'.$field['ezf_id'],
//					    'data-item'=>$valueId
//					]);


				    }
				}
			}
			
			
			
			$rowReturn .= Html::a("<span class=\"label label-warning\" style=\"font-size: 13px;\">$numSubmit / $numDraft</span> ", NULL, [
			    'data-url'=>Url::to(['/inv/inv-person/ezform-emr',
				'ezf_id'=>$field['ezf_id'],
				'target'=>base64_encode($data[$pkJoin]),
				'comp_id_target'=>$field['comp_id_target'],
			    ]),
			    'class'=>'open-ezfrom-emr',
			    'style'=>'cursor: pointer;',
			    'data-toggle'=>'tooltip',
			    'title'=>'(Submit/Save Draft) คลิกเพื่อแสดง EMR',
			]);
			
		    } else {
			$rowReturn .= Html::a("<span class=\"label label-default\" style=\"font-size: 13px;\">0 / 0</span> ", NULL, [
			    'data-url'=>Url::to(['/inv/inv-person/ezform-emr',
				'ezf_id'=>$field['ezf_id'],
				'target'=>base64_encode($data[$pkJoin]),
				'comp_id_target'=>$field['comp_id_target'],
			    ]),
			    'class'=>'open-ezfrom-emr',
			    'style'=>'cursor: pointer;',
			    'data-toggle'=>'tooltip',
			    'title'=>'(Submit/Save Draft) คลิกเพื่อแสดงข้อมูล',
			]);
			
		    }
		    
		    $rowReturnAdd = Html::a('<i class="glyphicon glyphicon-plus" style="color:#00a65a;font-size: 18px;"></i> ', null, [
			'data-url'=>Url::to(['/inv/inv-person/ezform-print',
			    'ezf_id'=>$field['ezf_id'],
			    'target'=>base64_encode($data[$pkJoin]),
			    'comp_id_target'=>$field['comp_id_target'],
			]),
			'class'=>'open-ezfrom',
			'style'=>'cursor: pointer;',
			'data-toggle'=>'tooltip',
			'title'=>'เพิ่มข้อมูล',
		    ]);
//		    $rowReturnAdd = Html::a('<i class="glyphicon glyphicon-plus" style="color:#00a65a;font-size: 18px;"></i> ', Url::to(['/inputdata/insert-record',
//			'insert'=>'url',
//			'ezf_id'=>$field['ezf_id'],
//			'target'=>base64_encode($data[$pkJoin]),
//			'comp_id_target'=>$field['comp_id_target'],
//			'rurl'=>base64_encode(Yii::$app->request->url),
//		    ]), [
//			'data-toggle'=>'tooltip',
//			'title'=>'เพิ่มข้อมูล',
//		    ]);
		    //render-ezf-data
		    
		    if($field['unique_record']==2){
			$numDraft+$numSubmit;
			
			if($numDraft>0){
			    $rr = 1;
			}elseif($numSubmit>0){
			    $rr = 2;
			} elseif($numSubmit>0){
			    $rr = -1;
			}else {
			    $rr = 0;
			}
			
			
			if($numAll>0){
			    $rowReturnAdd = '';
			    
//			    $icon = InvFunc::getStatusIcon($rr);
//			    
//			    foreach ($arrId as $keyId => $valueId) {
//				return Html::a('<i '.$icon.'></i>', NULL, [
//				    'class' => 'btn-lg open-ezfrom',
//				    'data-url'=>Url::to(['/inv/inv-person/ezform-print', 'ezf_id'=>$field['ezf_id'], 'dataid'=>$valueId, 
//					'target'=>  base64_encode($data[$pkJoin]),
//					'comp_id_target'=>$field['comp_id_target'] ]),
//				    'data-toggle'=>'tooltip',
//				    'data-placement'=>'right',
//				    'style'=>'cursor: pointer;',
//				    'title'=> 'แสดงข้อมูล',
//				]);
//			    }
			
			}
		    }
			
			

			if(isset($condition) && !empty($condition)){
			    $cform = isset($condition['form'])?$condition['form']:[];
			    $cfield = isset($condition['field'])?$condition['field']:[];
			    $fixvalue1 = isset($condition['value1'])?$condition['value1']:[];
			    $fixvalue2 = isset($condition['value2'])?$condition['value2']:[];
			    $ccond = isset($condition['cond'])?$condition['cond']:[];
			    $cmore = isset($condition['more'])?$condition['more']:[];

			    $arrCond = [];
			    foreach ($cform as $ckey => $cvalue) {
				$sql_result ='result_'.$cvalue.'_'.$cfield[$ckey].'_'.$ckey;
				if(isset($dataCond[$sql_result]) && $dataCond[$sql_result]!=''){
                                    //\appxq\sdii\utils\VarDumper::dump($dataCond);
				    $arrResultCond = $dataCond[$sql_result];
				    $condVarAll;
				    foreach ($arrResultCond as $keyResult => $valueResult) {
					$condVar = false;

					if($ccond[$ckey]=='between'){
					    $strCond = "'{$valueResult}'>='{$fixvalue1[$ckey]}' && '{$valueResult}'<='{$fixvalue2[$ckey]}'";

					    eval("\$condVar = $strCond;");


					} elseif($ccond[$ckey]=='func'){
					    $template = $fixvalue1[$ckey];
					    $strCond = strtr($template, ['{value}'=>$valueResult]);

					    eval("\$condVar = $strCond;");

					}
					else { 

					    $strCond = "'{$valueResult}'".$ccond[$ckey]."'{$fixvalue1[$ckey]}'";

					    eval("\$condVar = ($strCond);");


					}
					$condVarAll = $condVarAll || $condVar;
    //				    if(isset($condVarAll)){
    //					$condVarAll = $condVarAll || $condVar;
    //				    } else {
    //					$condVarAll = $condVar;
    //				    }

				    }

				    $arrCond[] = [
					     'cond'=> ($condVarAll)?'TRUE':'FALSE',
					     'with'=>$cmore[$ckey],
					 ];
				}

			    }
                            
			    if(!empty($arrCond)){
				$strCond = '';
				$with = '';
				foreach ($arrCond as $ackey => $acvalue) {
				    $strCond .= $with .$acvalue['cond'];
				    $with = " {$acvalue['with']} ";
				}

				eval("\$show = $strCond;");

				if($show){
				    return  Html::tag('div', $rowReturn.$rowReturnAdd, ['data-item'=> base64_encode(\yii\helpers\Json::encode($data)), 'data-field'=> base64_encode(\yii\helpers\Json::encode($valueField)), 'data-special'=> $special, 'class'=>'td-items']);
				} else {
				    if($numAll>0){
					return  Html::tag('div', $rowReturn, ['data-item'=> base64_encode(\yii\helpers\Json::encode($data)), 'data-field'=> base64_encode(\yii\helpers\Json::encode($valueField)), 'data-special'=> $special, 'class'=>'td-items']);
				    } else {
					return Html::tag('div', '', ['data-item'=> base64_encode(\yii\helpers\Json::encode($data)), 'data-field'=> base64_encode(\yii\helpers\Json::encode($valueField)), 'data-special'=> $special, 'class'=>'td-items']);
				    }
				}

			    } else {
				if($numAll>0){
				    return  Html::tag('div', $rowReturn, ['data-item'=> base64_encode(\yii\helpers\Json::encode($data)), 'data-field'=> base64_encode(\yii\helpers\Json::encode($valueField)), 'data-special'=> $special, 'class'=>'td-items']);
				} else {
				    return Html::tag('div', '', ['data-item'=> base64_encode(\yii\helpers\Json::encode($data)), 'data-field'=> base64_encode(\yii\helpers\Json::encode($valueField)), 'data-special'=> $special, 'class'=>'td-items']);
				}
			    }


			} else {
                            
			    return Html::tag('div', $rowReturn.$rowReturnAdd, ['data-item'=> base64_encode(\yii\helpers\Json::encode($data)), 'data-field'=> base64_encode(\yii\helpers\Json::encode($valueField)), 'data-special'=> $special, 'class'=>'td-items']);
			}
		    
                        
		 
		} else {
		    return '';
		}
		 
	    },
	    'filter'=>'',
	    'headerOptions'=>['style'=>'text-align: center;'],
	    'contentOptions'=>['style'=>"min-width:{$valueField['width']}px; text-align: center;"],
	];
	    
	return $columns;
    }
    
    public static function createColumnsCountNone($valueField, $special) {
	
	$columns = [
	    'header'=>$valueField['header'],
	    'format'=>'raw',
	    //'visible' => $valueField['visible_field']==1?self::visibleColumn($valueField):true,
	    'value'=>function ($data) use ($valueField, $special) { 
		if ($data['id']!=null) {
		    $field = $valueField;
                    $modelFields = Yii::$app->session['sql_main_fields'];
                    
		    $rowReturn = '';
		    $pkJoin = 'id';
                    $pkJoin2 = 'target';
		    if($special==1){
			$pkJoin = 'ptid';
                        $pkJoin2 = 'ptid';
		    }
                    
                    $display = SDUtility::string2Array($field['display_options']);
                    $condition = SDUtility::string2Array($field['value_options']);
                 
                    $result = 'id';
                    $dataCond = [];
                    if($field['show']=='detail'){
                        $result = isset($field['field_detail'])?"CONCAT({$field['field_detail']})":'id';
                        
                        
                    } elseif ($field['show']=='condition') {
                        if(isset($display) && !empty($display)){
                            $result = isset($field['result'])?$field['result']:'id';
                        }
                    } elseif ($field['show']=='custom') {
                        if(isset($display) && !empty($display)){
                            if(isset($display['custom']) && $display['custom']!=''){
                                $result = "CONCAT({$display['custom']})";
                            }
                        }
                    } elseif (isset($condition) && !empty($condition)) {
                        $cform = $condition['form'];
                        $cfield = $condition['field'];
                        
                        foreach ($cform as $ckey => $cvalue) {
                            $cond_result ='result_'.$cvalue.'_'.$cfield[$ckey].'_'.$ckey;
                            $ezform = \backend\modules\inv\classes\InvQuery::getEzformById($cvalue);
		    
                            $sql_cond = "SELECT et.{$cfield[$ckey]} FROM {$ezform['ezf_table']} et WHERE et.$pkJoin2 = :id AND et.rstat<>0 AND et.rstat<>3 ";
                            $dataCond[$cond_result] = Yii::$app->db->createCommand($sql_cond, [':id'=>$data[$pkJoin]])->queryColumn();
                            
                        }
                    }
                    
                    $sql = "SELECT et.id, et.rstat, $result AS result FROM {$field['ezf_table']} et WHERE et.$pkJoin2 = :id AND et.rstat<>0 AND et.rstat<>3 ";
	
                    $dataItems = Yii::$app->db->createCommand($sql, [':id'=>$data[$pkJoin]])->queryAll();
                    $data_all = '';
                    $data_draft = '';
                    $data_submit = '';
                    $data_result = '';
                    if($dataItems){
                        $comma = '';
                    foreach ($dataItems as $keyItem => $valueItem) {
                            $data_all .= $comma.$valueItem['id'];
                            if($valueItem['rstat']==1){
                                $data_draft .= $comma.$valueItem['id'];
                            } elseif($valueItem['rstat']==2) {
                                
                                $data_submit .= $comma.$valueItem['id'];
                            }
                            $data_result .= $comma.$valueItem['result'];
                            
                            $comma = ',';
                        }
                    }
		    //\appxq\sdii\utils\VarDumper::dump($dataCond);
                    
		    $numAll =0;
		    if ($data_all!='') {
			$arrSubmit = [];
			$arrDraft = [];
			$arrId = [];
			$arrResult = [];
			
			if($data_draft!=''){
			    $arrDraft = explode(',', $data_draft);
			}
			if($data_submit!=''){
			    $arrSubmit = explode(',', $data_submit);
			}
			if($data_all!=''){
			    $arrId = explode(',', $data_all);
			}
			if($data_result!=''){
			    $arrResult = explode(',', $data_result);
			}
			
			$result = '';
			$numDraft = count($arrDraft);
			$numSubmit = count($arrSubmit);
			$numAll = $numDraft+$numSubmit;
                        
			if($field['show']=='detail'){
			    
			    if($data_all!='' && $data_result!=''){
				
				foreach ($arrId as $keyId => $valueId) {
				    
				    $rowReturn .= Html::a("<span class=\"label label-default\" style=\"font-size: 13px; padding: 1px 5px; background-color: #DDD;\">{$arrResult[$keyId]}</span> ", NULL, [
					'data-url'=>Url::to(['/inv/inv-person/ezform-print',
					    'ezf_id'=>$field['ezf_id'],
					    'dataid'=>$valueId,
					    'target'=>base64_encode($data[$pkJoin]),
					    'comp_id_target'=>$field['comp_id_target'],
					]),
					'style'=>'cursor: pointer;',
					'data-toggle'=>'tooltip',
					'title'=>'แสดงข้อมูล',
					'class'=>'items-tooltip tooltip-detail open-ezfrom form-'.$field['ezf_id'],
					'data-item'=>$valueId
				    ]);
				    
				    
				}
			    }
			} elseif ($field['show']=='condition') {
			    
			    if(isset($display) && !empty($display)){
				
				if($data_all!='' && $data_result!=''){
				    $dcondition = isset($display['condition'])?$display['condition']:[];
				    $dvalue1 = isset($display['value1'])?$display['value1']:[];
				    $dvalue2 = isset($display['value2'])?$display['value2']:[];
				    $dicon = isset($display['icon'])?$display['icon']:[];
				    $dlabel = isset($display['label'])?$display['label']:[];

				    
				    foreach ($arrId as $keyId => $valueId) {
					
					foreach ($dcondition as $dkey => $dcond) {
					    $condVar = false;
					   
					    if($dcond=='between'){
						$strCond = "'{$arrResult[$keyId]}'>='{$dvalue1[$dkey]}' && '{$arrResult[$keyId]}'<='{$dvalue2[$dkey]}'";
						
						eval("\$condVar = $strCond;");
						
						
					    }elseif($ccond[$ckey]=='func'){
						$template = $dvalue1[$ckey];
						$strCond = strtr($template, ['{value}'=>$arrResult]);
						
						eval("\$condVar = $strCond;");

					    }
					    else { //between
						
						
						$strCond = "'{$arrResult[$keyId]}'".$dcond."'{$dvalue1[$dkey]}'";
						
						eval("\$condVar = $strCond;");
						
					    }
					    
					    if($condVar){
						$rowReturn .= Html::a("<span class=\"btn btn-default btn-xs\" style=\"background-color: #DDD;\">".(isset($dicon[$dkey])?"<i class=\"fa {$dicon[$dkey]}\"></i>":'?')."</span> ", null, [
						    'data-url'=>Url::to(['/inv/inv-person/ezform-print',
							'ezf_id'=>$field['ezf_id'],
							'dataid'=>$valueId,
							'target'=>base64_encode($data[$pkJoin]),
							'comp_id_target'=>$field['comp_id_target'],
						    ]),
						    'style'=>'cursor: pointer;',
						    'data-toggle'=>'tooltip',
						    'title'=> isset($dlabel[$dkey])?$dlabel[$dkey]:'แสดงข้อมูล',
						    'class'=>'items-tooltip tooltip-condition open-ezfrom form-'.$field['ezf_id'],
						    'data-item'=>$valueId
						]);
						
//						$rowReturn .= Html::a("<span class=\"btn btn-default btn-xs\" style=\"background-color: #DDD;\">".(isset($dicon[$dkey])?"<i class=\"fa {$dicon[$dkey]}\"></i>":'?')."</span> ", Url::to(['/inputdata/redirect-page',
//						    'ezf_id'=>$field['ezf_id'],
//						    'dataid'=>$valueId,
//						    'rurl'=>base64_encode(Yii::$app->request->url),
//						]), [
//						    'data-toggle'=>'tooltip',
//						    'title'=> isset($dlabel[$dkey])?$dlabel[$dkey]:'แสดงข้อมูล',
//						    'class'=>'items-tooltip tooltip-condition form-'.$field['ezf_id'],
//						    'data-item'=>$valueId
//						]);
					    }
					}
				    }
				}
			    }
			} elseif ($field['show']=='custom') {
				if($data_all!='' && $data_result!=''){

				    foreach ($arrId as $keyId => $valueId) {
					
					$rowReturn .= Html::a("{$arrResult[$keyId]} ", NULL, [//<span class=\"label label-default\" style=\"font-size: 13px; padding-bottom: 3px;padding-top: 0px; background-color: rgba(255, 255, 255, 0);\">
					    'data-url'=>Url::to(['/inv/inv-person/ezform-print',
						'ezf_id'=>$field['ezf_id'],
						'dataid'=>$valueId,
						'target'=>base64_encode($data[$pkJoin]),
						'comp_id_target'=>$field['comp_id_target'],
					    ]),
					    'style'=>'cursor: pointer;',
					    'data-toggle'=>'tooltip',
					    'title'=>'แสดงข้อมูล',
					    'class'=>'items-tooltip tooltip-custom open-ezfrom form-'.$field['ezf_id'],
					    'data-item'=>$valueId
					]);
					
//					$rowReturn .= Html::a("<span class=\"label label-default\" style=\"font-size: 13px; padding-bottom: 3px;padding-top: 0px; background-color: #DDD;\">{$arrResult[$keyId]}</span> ", Url::to(['/inputdata/redirect-page',
//					    'ezf_id'=>$field['ezf_id'],
//					    'dataid'=>$valueId,
//					    'rurl'=>base64_encode(Yii::$app->request->url),
//					]), [
//					    'data-toggle'=>'tooltip',
//					    'title'=>'แสดงข้อมูล',
//					    'class'=>'items-tooltip tooltip-custom form-'.$field['ezf_id'],
//					    'data-item'=>$valueId
//					]);


				    }
				}
			}
			
			
			
			$rowReturn .= Html::a("<span class=\"label label-warning\" style=\"font-size: 13px;\">$numSubmit / $numDraft</span> ", NULL, [
			    'data-url'=>Url::to(['/inv/inv-person/ezform-emr',
				'ezf_id'=>$field['ezf_id'],
				'target'=>base64_encode($data[$pkJoin]),
				'comp_id_target'=>$field['comp_id_target'],
			    ]),
			    'class'=>'open-ezfrom-emr',
			    'style'=>'cursor: pointer;',
			    'data-toggle'=>'tooltip',
			    'title'=>'(Submit/Save Draft) คลิกเพื่อแสดง EMR',
			]);
			
		    } else {
			$rowReturn .= Html::a("<span class=\"label label-default\" style=\"font-size: 13px;\">0 / 0</span> ", NULL, [
			    'data-url'=>Url::to(['/inv/inv-person/ezform-emr',
				'ezf_id'=>$field['ezf_id'],
				'target'=>base64_encode($data[$pkJoin]),
				'comp_id_target'=>$field['comp_id_target'],
			    ]),
			    'class'=>'open-ezfrom-emr',
			    'style'=>'cursor: pointer;',
			    'data-toggle'=>'tooltip',
			    'title'=>'(Submit/Save Draft) คลิกเพื่อแสดงข้อมูล',
			]);
			
		    }
		    
		    $rowReturnAdd = Html::a('<i class="glyphicon glyphicon-plus" style="color:#00a65a;font-size: 18px;"></i> ', null, [
			'data-url'=>Url::to(['/inv/inv-person/ezform-print',
			    'ezf_id'=>$field['ezf_id'],
			    'target'=>base64_encode($data[$pkJoin]),
			    'comp_id_target'=>$field['comp_id_target'],
			]),
			'class'=>'open-ezfrom',
			'style'=>'cursor: pointer;',
			'data-toggle'=>'tooltip',
			'title'=>'เพิ่มข้อมูล',
		    ]);
//		    $rowReturnAdd = Html::a('<i class="glyphicon glyphicon-plus" style="color:#00a65a;font-size: 18px;"></i> ', Url::to(['/inputdata/insert-record',
//			'insert'=>'url',
//			'ezf_id'=>$field['ezf_id'],
//			'target'=>base64_encode($data[$pkJoin]),
//			'comp_id_target'=>$field['comp_id_target'],
//			'rurl'=>base64_encode(Yii::$app->request->url),
//		    ]), [
//			'data-toggle'=>'tooltip',
//			'title'=>'เพิ่มข้อมูล',
//		    ]);
		    //render-ezf-data
		    
		    if($field['unique_record']==2){
			$numDraft+$numSubmit;
			
			if($numDraft>0){
			    $rr = 1;
			}elseif($numSubmit>0){
			    $rr = 2;
			} elseif($numSubmit>0){
			    $rr = -1;
			}else {
			    $rr = 0;
			}
			
			
			if($numAll>0){
			    $rowReturnAdd = '';
			    
//			    $icon = InvFunc::getStatusIcon($rr);
//			    
//			    foreach ($arrId as $keyId => $valueId) {
//				return Html::a('<i '.$icon.'></i>', NULL, [
//				    'class' => 'btn-lg open-ezfrom',
//				    'data-url'=>Url::to(['/inv/inv-person/ezform-print', 'ezf_id'=>$field['ezf_id'], 'dataid'=>$valueId, 
//					'target'=>  base64_encode($data[$pkJoin]),
//					'comp_id_target'=>$field['comp_id_target'] ]),
//				    'data-toggle'=>'tooltip',
//				    'data-placement'=>'right',
//				    'style'=>'cursor: pointer;',
//				    'title'=> 'แสดงข้อมูล',
//				]);
//			    }
			
			}
		    }
			
			

			if(isset($condition) && !empty($condition)){
			    $cform = isset($condition['form'])?$condition['form']:[];
			    $cfield = isset($condition['field'])?$condition['field']:[];
			    $fixvalue1 = isset($condition['value1'])?$condition['value1']:[];
			    $fixvalue2 = isset($condition['value2'])?$condition['value2']:[];
			    $ccond = isset($condition['cond'])?$condition['cond']:[];
			    $cmore = isset($condition['more'])?$condition['more']:[];

			    $arrCond = [];
			    foreach ($cform as $ckey => $cvalue) {
				$sql_result ='result_'.$cvalue.'_'.$cfield[$ckey].'_'.$ckey;
				if(isset($dataCond[$sql_result]) && $dataCond[$sql_result]!=''){
                                    //\appxq\sdii\utils\VarDumper::dump($dataCond);
				    $arrResultCond = $dataCond[$sql_result];
				    $condVarAll;
				    foreach ($arrResultCond as $keyResult => $valueResult) {
					$condVar = false;

					if($ccond[$ckey]=='between'){
					    $strCond = "'{$valueResult}'>='{$fixvalue1[$ckey]}' && '{$valueResult}'<='{$fixvalue2[$ckey]}'";

					    eval("\$condVar = $strCond;");


					} elseif($ccond[$ckey]=='func'){
					    $template = $fixvalue1[$ckey];
					    $strCond = strtr($template, ['{value}'=>$valueResult]);

					    eval("\$condVar = $strCond;");

					}
					else { 

					    $strCond = "'{$valueResult}'".$ccond[$ckey]."'{$fixvalue1[$ckey]}'";

					    eval("\$condVar = ($strCond);");


					}
					$condVarAll = $condVarAll || $condVar;
    //				    if(isset($condVarAll)){
    //					$condVarAll = $condVarAll || $condVar;
    //				    } else {
    //					$condVarAll = $condVar;
    //				    }

				    }

				    $arrCond[] = [
					     'cond'=> ($condVarAll)?'TRUE':'FALSE',
					     'with'=>$cmore[$ckey],
					 ];
				}

			    }

			    if(!empty($arrCond)){
				$strCond = '';
				$with = '';
				foreach ($arrCond as $ackey => $acvalue) {
				    $strCond .= $with .$acvalue['cond'];
				    $with = " {$acvalue['with']} ";
				}

				eval("\$show = $strCond;");

				if($show){
				    return $rowReturn.$rowReturnAdd;
				} else {
				    if($numAll>0){
					return $rowReturn;
				    } else {
					return '';
				    }
				}

			    } else {
				if($numAll>0){
				    return $rowReturn;
				} else {
				    return '';
				}
			    }


			} else {
			    return $rowReturn.$rowReturnAdd;
			}
		    
		 
		} else {
		    return '';
		}
		 
	    },
	    'filter'=>'',
	    'headerOptions'=>['style'=>'text-align: center;'],
	    'contentOptions'=>['style'=>"min-width:{$valueField['width']}px; text-align: center;"],
	];
	    
	return $columns;
    }
    
    public static function createColumnsCountTmp($valueField, $special) {
	
	$columns = [
	    'header'=>$valueField['header'],
	    'format'=>'raw',
	    //'visible' => $valueField['visible_field']==1?self::visibleColumn($valueField):true,
	    'value'=>function ($data) use ($valueField, $special) { 
		if ($data['id']!=null) {
		    $field = $valueField;

		    $rowReturn = '';
		    $pkJoin = Yii::$app->session['sql_main_fields']['pk_join'];
		    if($special==1){
			$pkJoin = 'ptid';
		    }
		    
		    $numAll =0;
		    if ($data[$field['sql_id_all']]!=null) {
			$arrSubmit = [];
			$arrDraft = [];
			$arrId = [];
			$arrResult = [];
			
			if($data[$field['sql_id_name']]!=null){
			    $arrDraft = explode(',', $data[$field['sql_id_name']]);
			}
			if($data[$field['sql_idsubmit_name']]!=null){
			    $arrSubmit = explode(',', $data[$field['sql_idsubmit_name']]);
			}
			if($data[$field['sql_id_all']]!=null){
			    $arrId = explode(',', $data[$field['sql_id_all']]);
			}
			if($data[$field['sql_result_name']]!=null){
			    $arrResult = explode(',', $data[$field['sql_result_name']]);
			}
			
			
						
			$result = '';
			$display = SDUtility::string2Array($field['display_options']);
			$numDraft = count($arrDraft);
			$numSubmit = count($arrSubmit);
			$numAll = $numDraft+$numSubmit;
			
			
			if($field['show']=='detail'){
			    
			    if($data[$field['sql_result_name']]!=null && $data[$field['sql_id_all']]!=null){
				
				foreach ($arrId as $keyId => $valueId) {
				    
				    $rowReturn .= Html::a("<span class=\"label label-default\" style=\"font-size: 13px; padding: 1px 5px; background-color: #DDD;\">{$arrResult[$keyId]}</span> ", NULL, [
					'data-url'=>Url::to(['/inv/inv-person/ezform-print',
					    'ezf_id'=>$field['ezf_id'],
					    'dataid'=>$valueId,
					    'target'=>base64_encode($data[$pkJoin]),
					    'comp_id_target'=>$field['comp_id_target'],
					]),
					'style'=>'cursor: pointer;',
					'data-toggle'=>'tooltip',
					'title'=>'แสดงข้อมูล',
					'class'=>'items-tooltip tooltip-detail open-ezfrom form-'.$field['ezf_id'],
					'data-item'=>$valueId
				    ]);
				    
				    
				}
			    }
			} elseif ($field['show']=='condition') {
			    
			    if(isset($display) && !empty($display)){
				
				if($data[$field['sql_result_name']]!=null && $data[$field['sql_id_all']]!=null){
				    $dcondition = isset($display['condition'])?$display['condition']:[];
				    $dvalue1 = isset($display['value1'])?$display['value1']:[];
				    $dvalue2 = isset($display['value2'])?$display['value2']:[];
				    $dicon = isset($display['icon'])?$display['icon']:[];
				    $dlabel = isset($display['label'])?$display['label']:[];

				    
				    foreach ($arrId as $keyId => $valueId) {
					
					foreach ($dcondition as $dkey => $dcond) {
					    $condVar = false;
					   
					    if($dcond=='between'){
						$strCond = "'{$arrResult[$keyId]}'>='{$dvalue1[$dkey]}' && '{$arrResult[$keyId]}'<='{$dvalue2[$dkey]}'";
						
						eval("\$condVar = $strCond;");
						
						
					    }elseif($ccond[$ckey]=='func'){
						$template = $dvalue1[$ckey];
						$strCond = strtr($template, ['{value}'=>$arrResult]);
						
						eval("\$condVar = $strCond;");

					    }
					    else { //between
						
						
						$strCond = "'{$arrResult[$keyId]}'".$dcond."'{$dvalue1[$dkey]}'";
						
						eval("\$condVar = $strCond;");
						
					    }
					    
					    if($condVar){
						$rowReturn .= Html::a("<span class=\"btn btn-default btn-xs\" style=\"background-color: #DDD;\">".(isset($dicon[$dkey])?"<i class=\"fa {$dicon[$dkey]}\"></i>":'?')."</span> ", null, [
						    'data-url'=>Url::to(['/inv/inv-person/ezform-print',
							'ezf_id'=>$field['ezf_id'],
							'dataid'=>$valueId,
							'target'=>base64_encode($data[$pkJoin]),
							'comp_id_target'=>$field['comp_id_target'],
						    ]),
						    'style'=>'cursor: pointer;',
						    'data-toggle'=>'tooltip',
						    'title'=> isset($dlabel[$dkey])?$dlabel[$dkey]:'แสดงข้อมูล',
						    'class'=>'items-tooltip tooltip-condition open-ezfrom form-'.$field['ezf_id'],
						    'data-item'=>$valueId
						]);
						
//						$rowReturn .= Html::a("<span class=\"btn btn-default btn-xs\" style=\"background-color: #DDD;\">".(isset($dicon[$dkey])?"<i class=\"fa {$dicon[$dkey]}\"></i>":'?')."</span> ", Url::to(['/inputdata/redirect-page',
//						    'ezf_id'=>$field['ezf_id'],
//						    'dataid'=>$valueId,
//						    'rurl'=>base64_encode(Yii::$app->request->url),
//						]), [
//						    'data-toggle'=>'tooltip',
//						    'title'=> isset($dlabel[$dkey])?$dlabel[$dkey]:'แสดงข้อมูล',
//						    'class'=>'items-tooltip tooltip-condition form-'.$field['ezf_id'],
//						    'data-item'=>$valueId
//						]);
					    }
					}
				    }
				}
			    }
			} elseif ($field['show']=='custom') {
				if($data[$field['sql_result_name']]!=null && $data[$field['sql_id_all']]!=null){

				    foreach ($arrId as $keyId => $valueId) {
					
					$rowReturn .= Html::a("<span class=\"label label-default\" style=\"font-size: 13px; padding-bottom: 3px;padding-top: 0px; background-color: #DDD;\">{$arrResult[$keyId]}</span> ", NULL, [
					    'data-url'=>Url::to(['/inv/inv-person/ezform-print',
						'ezf_id'=>$field['ezf_id'],
						'dataid'=>$valueId,
						'target'=>base64_encode($data[$pkJoin]),
						'comp_id_target'=>$field['comp_id_target'],
					    ]),
					    'style'=>'cursor: pointer;',
					    'data-toggle'=>'tooltip',
					    'title'=>'แสดงข้อมูล',
					    'class'=>'items-tooltip tooltip-custom open-ezfrom form-'.$field['ezf_id'],
					    'data-item'=>$valueId
					]);
					
//					$rowReturn .= Html::a("<span class=\"label label-default\" style=\"font-size: 13px; padding-bottom: 3px;padding-top: 0px; background-color: #DDD;\">{$arrResult[$keyId]}</span> ", Url::to(['/inputdata/redirect-page',
//					    'ezf_id'=>$field['ezf_id'],
//					    'dataid'=>$valueId,
//					    'rurl'=>base64_encode(Yii::$app->request->url),
//					]), [
//					    'data-toggle'=>'tooltip',
//					    'title'=>'แสดงข้อมูล',
//					    'class'=>'items-tooltip tooltip-custom form-'.$field['ezf_id'],
//					    'data-item'=>$valueId
//					]);


				    }
				}
			}
			
			
			
			$rowReturn .= Html::a("<span class=\"label label-warning\" style=\"font-size: 13px;\">$numSubmit / $numDraft</span> ", NULL, [
			    'data-url'=>Url::to(['/inv/inv-person/ezform-emr',
				'ezf_id'=>$field['ezf_id'],
				'target'=>base64_encode($data[$pkJoin]),
				'comp_id_target'=>$field['comp_id_target'],
			    ]),
			    'class'=>'open-ezfrom-emr',
			    'style'=>'cursor: pointer;',
			    'data-toggle'=>'tooltip',
			    'title'=>'(Submit/Save Draft) คลิกเพื่อแสดง EMR',
			]);
			
		    } else {
			$rowReturn .= Html::a("<span class=\"label label-default\" style=\"font-size: 13px;\">0 / 0</span> ", NULL, [
			    'data-url'=>Url::to(['/inv/inv-person/ezform-emr',
				'ezf_id'=>$field['ezf_id'],
				'target'=>base64_encode($data[$pkJoin]),
				'comp_id_target'=>$field['comp_id_target'],
			    ]),
			    'class'=>'open-ezfrom-emr',
			    'style'=>'cursor: pointer;',
			    'data-toggle'=>'tooltip',
			    'title'=>'(Submit/Save Draft) คลิกเพื่อแสดงข้อมูล',
			]);
			
		    }
		    
		    $rowReturnAdd = Html::a('<i class="glyphicon glyphicon-plus" style="color:#00a65a;font-size: 18px;"></i> ', null, [
			'data-url'=>Url::to(['/inv/inv-person/ezform-print',
			    'ezf_id'=>$field['ezf_id'],
			    'target'=>base64_encode($data[$pkJoin]),
			    'comp_id_target'=>$field['comp_id_target'],
			]),
			'class'=>'open-ezfrom',
			'style'=>'cursor: pointer;',
			'data-toggle'=>'tooltip',
			'title'=>'เพิ่มข้อมูล',
		    ]);
//		    $rowReturnAdd = Html::a('<i class="glyphicon glyphicon-plus" style="color:#00a65a;font-size: 18px;"></i> ', Url::to(['/inputdata/insert-record',
//			'insert'=>'url',
//			'ezf_id'=>$field['ezf_id'],
//			'target'=>base64_encode($data[$pkJoin]),
//			'comp_id_target'=>$field['comp_id_target'],
//			'rurl'=>base64_encode(Yii::$app->request->url),
//		    ]), [
//			'data-toggle'=>'tooltip',
//			'title'=>'เพิ่มข้อมูล',
//		    ]);
		    //render-ezf-data
		    
		    if($field['unique_record']==2){
			$numDraft+$numSubmit;
			
			if($numDraft>0){
			    $rr = 1;
			}elseif($numSubmit>0){
			    $rr = 2;
			} elseif($numSubmit>0){
			    $rr = -1;
			}else {
			    $rr = 0;
			}
			
			
			if($numAll>0){
			    $rowReturnAdd = '';
			    
//			    $icon = InvFunc::getStatusIcon($rr);
//			    
//			    foreach ($arrId as $keyId => $valueId) {
//				return Html::a('<i '.$icon.'></i>', NULL, [
//				    'class' => 'btn-lg open-ezfrom',
//				    'data-url'=>Url::to(['/inv/inv-person/ezform-print', 'ezf_id'=>$field['ezf_id'], 'dataid'=>$valueId, 
//					'target'=>  base64_encode($data[$pkJoin]),
//					'comp_id_target'=>$field['comp_id_target'] ]),
//				    'data-toggle'=>'tooltip',
//				    'data-placement'=>'right',
//				    'style'=>'cursor: pointer;',
//				    'title'=> 'แสดงข้อมูล',
//				]);
//			    }
			
			}
		    }
			
			$condition = SDUtility::string2Array($field['value_options']);

			if(isset($condition) && !empty($condition)){
			    $cform = isset($condition['form'])?$condition['form']:[];
			    $cfield = isset($condition['field'])?$condition['field']:[];
			    $fixvalue1 = isset($condition['value1'])?$condition['value1']:[];
			    $fixvalue2 = isset($condition['value2'])?$condition['value2']:[];
			    $ccond = isset($condition['cond'])?$condition['cond']:[];
			    $cmore = isset($condition['more'])?$condition['more']:[];

			    $arrCond = [];
			    foreach ($cform as $ckey => $cvalue) {
				$sql_result ='result_'.$cvalue.'_'.$cfield[$ckey].'_'.$ckey;
				if(isset($data[$sql_result]) && $data[$sql_result]!=''){
				    $arrResultCond = explode(',', $data[$sql_result]);
				    $condVarAll;
				    foreach ($arrResultCond as $keyResult => $valueResult) {
					$condVar = false;

					if($ccond[$ckey]=='between'){
					    $strCond = "'{$valueResult}'>='{$fixvalue1[$ckey]}' && '{$valueResult}'<='{$fixvalue2[$ckey]}'";

					    eval("\$condVar = $strCond;");


					} elseif($ccond[$ckey]=='func'){
					    $template = $fixvalue1[$ckey];
					    $strCond = strtr($template, ['{value}'=>$valueResult]);

					    eval("\$condVar = $strCond;");

					}
					else { 

					    $strCond = "'{$valueResult}'".$ccond[$ckey]."'{$fixvalue1[$ckey]}'";

					    eval("\$condVar = ($strCond);");


					}
					$condVarAll = $condVarAll || $condVar;
    //				    if(isset($condVarAll)){
    //					$condVarAll = $condVarAll || $condVar;
    //				    } else {
    //					$condVarAll = $condVar;
    //				    }

				    }

				    $arrCond[] = [
					     'cond'=> ($condVarAll)?'TRUE':'FALSE',
					     'with'=>$cmore[$ckey],
					 ];
				}

			    }

			    if(!empty($arrCond)){
				$strCond = '';
				$with = '';
				foreach ($arrCond as $ackey => $acvalue) {
				    $strCond .= $with .$acvalue['cond'];
				    $with = " {$acvalue['with']} ";
				}

				eval("\$show = $strCond;");

				if($show){
				    return $rowReturn.$rowReturnAdd;
				} else {
				    if($numAll>0){
					return $rowReturn;
				    } else {
					return '';
				    }
				}

			    } else {
				if($numAll>0){
				    return $rowReturn;
				} else {
				    return '';
				}
			    }


			} else {
			    return $rowReturn.$rowReturnAdd;
			}
		    
		 
		} else {
		    return '';
		}
		 
	    },
	    'filter'=>'',
	    'headerOptions'=>['style'=>'text-align: center;'],
	    'contentOptions'=>['style'=>"min-width:{$valueField['width']}px; text-align: center;"],
	];
	    
	return $columns;
    }
    
    public static function getList() {
	$arry = [];
	$fields = Yii::$app->session['sql_fields'];
	if(isset($fields) && !empty($fields)){
	    foreach ($fields as $key => $value) {
		$arry[$value['ezf_id']] = $value['ezf_name'];
	    }
	} else {
	    if(isset(Yii::$app->session['sql_main_fields']['enable_form'])){
		$main_forms = \appxq\sdii\utils\SDUtility::string2Array(Yii::$app->session['sql_main_fields']['enable_form']);
		$ffixFields = isset($main_forms['form'])?$main_forms['form']:[];
		$ffixLabel = isset($main_forms['label'])?$main_forms['label']:[];

		foreach ($ffixFields as $key => $value) {
		    $arry[$value] = $ffixLabel[$key];
		}
	    }
	}
	
	return $arry;
    }
    
    public static function createSqlWeb($value, $modelFields) {
	$sql_col = [];
	
	    $result = isset($value['result_field'])?$value['result_field']:[];

	    $sql_id_all = $value['sql_id_all'];
	    $sql_id_name = $value['sql_id_name'];
	    $sql_idsubmit_name = $value['sql_idsubmit_name'];
	    $sql_result_name = $value['sql_result_name'];
	    
	    $pk = 'id';
	    $pkJoin = 'target';
	    if($modelFields['special']==1){
		$pk = 'ptid';
		$pkJoin = 'ptid';
	    }
	    
//	    if($value['field_detail']!=''){
//		$sql_id_detail = 'detail_'.$value['ezf_id'];
//		$sql_col[] = "SELECT CONCAT({$value['field_detail']}) AS $sql_id_detail FROM {$value['ezf_table']} et";
//	    }
	     
	    $sql_col[] = "(SELECT group_concat(et.id) FROM {$value['ezf_table']} et WHERE et.$pkJoin = {$modelFields['ezf_table']}.$pk AND et.rstat<>0 AND et.rstat<>3 GROUP BY et.$pkJoin) AS $sql_id_all";
	    $sql_col[] = "(SELECT group_concat(et.id) FROM {$value['ezf_table']} et WHERE et.$pkJoin = {$modelFields['ezf_table']}.$pk AND et.rstat=1 GROUP BY et.$pkJoin) AS $sql_id_name";
	    $sql_col[] = "(SELECT group_concat(et.id) FROM {$value['ezf_table']} et WHERE et.$pkJoin = {$modelFields['ezf_table']}.$pk AND et.rstat=2 GROUP BY et.$pkJoin) AS $sql_idsubmit_name";
	    
	    $display = SDUtility::string2Array($value['display_options']);
	    
	    if($value['show']=='detail'){
		
		if(isset($value['field_detail']) && $value['field_detail']!=''){
		    $sql_col[] = "(SELECT group_concat(CONCAT({$value['field_detail']})) FROM {$value['ezf_table']} et WHERE et.$pkJoin = {$modelFields['ezf_table']}.$pk AND et.rstat<>0 AND et.rstat<>3 GROUP BY et.$pkJoin) AS $sql_result_name";
		}
			
	    } elseif ($value['show']=='condition') {
		if(isset($display) && !empty($display)){
		    $dcondition = isset($display['condition'])?$display['condition']:[];
		    $dvalue1 = isset($display['value1'])?$display['value1']:[];
		    $dvalue2 = isset($display['value2'])?$display['value2']:[];
		    $dicon = isset($display['icon'])?$display['icon']:[];
		    $dlabel = isset($display['label'])?$display['label']:[];
		    
		    $sql_col[] = "(SELECT group_concat(et.{$value['result']}) FROM {$value['ezf_table']} et WHERE et.$pkJoin = {$modelFields['ezf_table']}.$pk AND et.rstat<>0 AND et.rstat<>3 GROUP BY et.$pkJoin) AS $sql_result_name";
                    
		}
	    } elseif ($value['show']=='custom') {
		if(isset($display) && !empty($display)){
		    if(isset($display['custom']) && $display['custom']!=''){
			$sql_col[] = "(SELECT group_concat(CONCAT({$display['custom']})) FROM {$value['ezf_table']} et WHERE et.$pkJoin = {$modelFields['ezf_table']}.$pk AND et.rstat<>0 AND et.rstat<>3 GROUP BY et.$pkJoin) AS $sql_result_name";
		    }
		}
	    }
	    
	    
	    $condition = SDUtility::string2Array($value['value_options']);
	    
	    if(isset($condition) && !empty($condition)){
		$cform = $condition['form'];
		$cfield = $condition['field'];
		
		foreach ($cform as $ckey => $cvalue) {
		    $sql_result ='result_'.$cvalue.'_'.$cfield[$ckey].'_'.$ckey;
		    $ezform = \backend\modules\inv\classes\InvQuery::getEzformById($cvalue);
		    
		    $sql_col[] = "(SELECT group_concat(et.{$cfield[$ckey]}) FROM {$ezform['ezf_table']} et WHERE et.$pkJoin = {$modelFields['ezf_table']}.$pk AND et.rstat<>0 AND et.rstat<>3 GROUP BY et.$pkJoin) AS $sql_result";
		}
		
	    }
//	    $result = trim($result);
//	    if($result!=''){
//		$sql_col[] = "(SELECT group_concat(et.$result) FROM {$value['ezf_table']} et WHERE et.$pkJoin = {$modelFields['ezf_table']}.$pk AND et.rstat<>3 GROUP BY et.$pkJoin) AS $sql_result_name";
//	    }
	
	
	return $sql_col;
    }
    
    public static function createSqlReport($value, $modelFields) {
	$sql_col = [];
	
	    $result = isset($value['result_field'])?$value['result_field']:[];

	    $sql_id_name = $value['sql_id_name'];
	    $sql_idsubmit_name = $value['sql_idsubmit_name'];
	    $sql_result_name = $value['sql_result_name'];

	    $pk = 'id';
	    $pkJoin = 'target';
	    if($modelFields['special']==1){
		$pk = 'ptid';
		$pkJoin = 'ptid';
	    }

	    $sql_col[] = "(SELECT group_concat(et.id) FROM {$value['ezf_table']} et WHERE et.$pkJoin = {$modelFields['ezf_table']}.$pk AND et.rstat<>3 GROUP BY et.$pkJoin) AS $sql_id_name";
	    //$sql_col[] = "(SELECT group_concat(et.id) FROM {$value['ezf_table']} et WHERE et.$pkJoin = {$modelFields['ezf_table']}.$pk AND et.rstat=2 GROUP BY et.$pkJoin) AS $sql_idsubmit_name";
//	    $result = trim($result);
//	    if($result!=''){
//		$sql_col[] = "(SELECT group_concat(et.$result) FROM {$value['ezf_table']} et WHERE et.$pkJoin = {$modelFields['ezf_table']}.$pk AND et.rstat<>3 GROUP BY et.$pkJoin) AS $sql_result_name";
//	    }
	
	return $sql_col;
    }
 
    public static function visibleColumn($field){
	$condition = SDUtility::string2Array($field['value_options']);
		    
	if(isset($condition) && !empty($condition)){
	    $cform = $condition['form'];
	    $cfield = $condition['field'];
	    return FALSE;
	    foreach ($cform as $ckey => $cvalue) {
		$sql_result ='result_'.$cvalue.'_'.$cfield[$ckey];

	    }
	} else {
	    return true;
	}
    }
    
    public static function createReportOverall($inv_main, $sitecode){
	$enable_form = [];
	if(isset($inv_main)){
	    $enable_form = \common\lib\sdii\components\utils\SDUtility::string2Array($inv_main['enable_form']);
	} else {
	    $formdata = InvQuery::getEzform();

	    foreach ($formdata as $datakey => $datavalue) {
		$enable_form['form'][] = $datavalue['ezf_id'];
		$enable_form['label'][] = $datavalue['ezf_name'];
		$enable_form['comp_id_target'][] = $datavalue['comp_id_target'];
		$enable_form['date'][] = $datavalue['ezf_date_order'];
	    }
	}
	
	$overviewForm1;
	if(isset($enable_form) && !empty($enable_form)){
	    $cform = $enable_form['form'];
	    $clabel = $enable_form['label'];
	    $cdate = $enable_form['date'];
	    
	    foreach ($cform as $ckey => $cvalue) {
		$ezform = \backend\modules\inv\classes\InvQuery::getEzformById($cvalue);
		$dates = '2013-02-09';
		$datef = isset($cdate[$ckey]) && $cdate[$ckey]!=''?$cdate[$ckey]:'create_date';
		$unique_record = isset($ezform['unique_record']) && $ezform['unique_record']!=''?$ezform['unique_record']:0;
		
		$sql = " REPLACE INTO report_overview_control VALUES('$sitecode',NOW());
			
			REPLACE INTO report_overview_all
			(SELECT
				'{$ezform['ezf_id']}' AS form_name,
				'{$ezform['ezf_table']}' AS form_table,
				'{$ezform['ezf_id']}' AS form_id,
				rstat AS form_stat,
				COUNT(DISTINCT ptid) AS count_all
			FROM {$ezform['ezf_table']}
			WHERE hsitecode IS NOT NULL
			AND $datef BETWEEN $dates AND NOW()
			AND ( rstat <> 0 AND rstat <> 3 )
			GROUP BY rstat);
			
			";
		
		if($unique_record==1){
		    $sql .= " 
			REPLACE INTO report_overview_site
			SELECT 
				'$sitecode' AS sitecode,
				'{$ezform['ezf_id']}' AS form_name,
				'{$ezform['ezf_table']}' AS form_table,
				'{$ezform['ezf_id']}' AS form_id,
				1 AS form_stat,
				COUNT(*) AS count_all,
				SUM( DATE_FORMAT($datef,'%Y') = YEAR(NOW()) ) AS count_year,
				SUM( DATE_FORMAT($datef,'%Y_%m') = DATE_FORMAT(NOW(),'%Y_%m') ) AS count_month,
				SUM( DATE_FORMAT($datef,'%Y_%U') = DATE_FORMAT(NOW(),'%Y_%U') ) AS count_week,
				SUM( DATE_FORMAT($datef,'%Y_%m_%d') = DATE_FORMAT(NOW(),'%Y_%m_%d') ) AS count_today
			FROM 
			(
				SELECT
					ptid,
					$datef
				FROM
					{$ezform['ezf_table']}
				WHERE
					hsitecode = '$sitecode'
				AND rstat = 1
				AND $datef BETWEEN $dates AND NOW()
				GROUP BY ptid
			) AS {$ezform['ezf_table']};


			REPLACE INTO report_overview_site
			SELECT 
				'$sitecode' AS sitecode,
				'{$ezform['ezf_id']}' AS form_name,
				'{$ezform['ezf_table']}' AS form_table,
				'{$ezform['ezf_id']}' AS form_id,
				2 AS form_stat,
				COUNT(*) AS count_all,
				SUM( DATE_FORMAT($datef,'%Y') = YEAR(NOW()) ) AS count_year,
				SUM( DATE_FORMAT($datef,'%Y_%m') = DATE_FORMAT(NOW(),'%Y_%m') ) AS count_month,
				SUM( DATE_FORMAT($datef,'%Y_%U') = DATE_FORMAT(NOW(),'%Y_%U') ) AS count_week,
				SUM( DATE_FORMAT($datef,'%Y_%m_%d') = DATE_FORMAT(NOW(),'%Y_%m_%d') ) AS count_today
			FROM 
			(
				SELECT
					ptid,
					$datef
				FROM
					{$ezform['ezf_table']}
				WHERE
					hsitecode = '$sitecode'
				AND (rstat = 2 OR rstat = 4 OR rstat = 5)
				AND $datef BETWEEN $dates AND NOW()
				GROUP BY ptid
			) AS {$ezform['ezf_table']};
			";
		} else {
		    $sql .= " 
			REPLACE INTO report_overview_site
			SELECT 
				'$sitecode' AS sitecode,
				'{$ezform['ezf_id']}' AS form_name,
				'{$ezform['ezf_table']}' AS form_table,
				'{$ezform['ezf_id']}' AS form_id,
				1 AS form_stat,
				COUNT(*) AS count_all,
				SUM( DATE_FORMAT($datef,'%Y') = YEAR(NOW()) ) AS count_year,
				SUM( DATE_FORMAT($datef,'%Y_%m') = DATE_FORMAT(NOW(),'%Y_%m') ) AS count_month,
				SUM( DATE_FORMAT($datef,'%Y_%U') = DATE_FORMAT(NOW(),'%Y_%U') ) AS count_week,
				SUM( DATE_FORMAT($datef,'%Y_%m_%d') = DATE_FORMAT(NOW(),'%Y_%m_%d') ) AS count_today
			FROM 
				{$ezform['ezf_table']} 
			WHERE hsitecode = '$sitecode'
			AND $datef BETWEEN $dates AND NOW()
			AND rstat = 1;

			
			REPLACE INTO report_overview_site
			SELECT 
				'$sitecode' AS sitecode,
				'{$ezform['ezf_id']}' AS form_name,
				'{$ezform['ezf_table']}' AS form_table,
				'{$ezform['ezf_id']}' AS form_id,
				2 AS form_stat,
				COUNT(*) AS count_all,
				SUM( DATE_FORMAT($datef,'%Y') = YEAR(NOW()) ) AS count_year,
				SUM( DATE_FORMAT($datef,'%Y_%m') = DATE_FORMAT(NOW(),'%Y_%m') ) AS count_month,
				SUM( DATE_FORMAT($datef,'%Y_%U') = DATE_FORMAT(NOW(),'%Y_%U') ) AS count_week,
				SUM( DATE_FORMAT($datef,'%Y_%m_%d') = DATE_FORMAT(NOW(),'%Y_%m_%d') ) AS count_today
			FROM 
				{$ezform['ezf_table']} 
			WHERE hsitecode = '$sitecode'
			AND $datef BETWEEN $dates AND NOW()
			AND (rstat = 2 OR rstat = 4 OR rstat = 5);
			";
		}	
//		\yii\helpers\VarDumper::dump($sql,10,true);
//		exit();
		
		Yii::$app->db->createCommand($sql)->execute();
	    }
	}

    }
    
    public static function getIcon() {
	$icon = [
	    'fa-battery-empty' => 'battery empty',
	    'fa-battery-quarter' => 'battery quarter',
	    'fa-battery-half' => 'battery half',
	    'fa-battery-three-quarters' => 'battery three quarters',
	    'fa-battery-full' => 'battery full',
	    'fa_music'=>'&#xf001;',
	];
	
	return $icon;
    }
    
    public static function getFields($ezf_id, $prefix='') {
	$modelFields = InvQuery::getFields($ezf_id);
	$dataFields=[];
	foreach ($modelFields as $key => $value) {
	    $name = $value['name'];

	    if($value['ezf_field_type']==21){
		if($value['ezf_field_label']==1){
		    $name = 'จังหวัด';
		} elseif ($value['ezf_field_label']==2) {
		    $name = 'อำเภอ';
		} elseif ($value['ezf_field_label']==3) {
		    $name = 'ตำบล';
		}
	    } 
	    $dataFields[$prefix.$value['id']] = $name;
	}
        $dataFields[$prefix.'create_date'] = 'วันที่บันทึกข้อมูล';
	return $dataFields;
    }
    
    public static function setFieldSearch($ezf_comp){
        //
        //$arr = explode(',', $ezform_comp['field_search']);
        try {
            $concatSearch = '';
            $concatLabel = '';
            $modelComp = [];
            $listIdField = $ezf_comp['field_search'];
            if ($ezf_comp['field_search_cid']){
                $listIdField = $ezf_comp['field_search_cid'] . ',' . $ezf_comp['field_search'];
	    }
	    
            $ezf_fields = Yii::$app->db->createCommand("SELECT ezf_field_id, ezf_field_name, ezf_field_label FROM ezform_fields WHERE ezf_field_id in(" . $listIdField . ");")->queryAll();
            $ezf_fields = ArrayHelper::index($ezf_fields, 'ezf_field_id');
	    
            foreach ($ezf_fields AS $key => $val) {
                if ($key <> $ezf_comp['field_search_cid']) {
                    $modelComp['arr_comp_field_search'][] = $val['ezf_field_name'];
                    $concatSearch .= "IFNULL(`" . $val['ezf_field_name'] . "`, ''), ' ', ";
                    $concatLabel .= $val['ezf_field_label'] . ', ';
                }
            }

            $concatSearch = substr($concatSearch, 0, -2);
            $modelComp['concat_search'] = $concatSearch;
            $modelComp['concat_label'] = $concatLabel . $ezf_fields[$ezf_comp['field_search_cid']]['ezf_field_label'];
            return $modelComp;
        }catch (\yii\base\Exception $ex){
             echo 'กำหนดฟิลด์ค้นหาที่คอมโพเนน <code>'. $ezf_comp['comp_name'].'</code> ไม่ถูกต้อง กรุณาตั้งค่าใหม่';
             exit;
             return;
        }
    }
    
    public static function taskSpecial1($out, $string_q, $string_decode, $ezf_id, $ezf_comp)
    {
        
        if (is_numeric($string_q) AND strlen($string_q) < 13 AND strlen($string_q) > 10) {
            //ถ้าเป็นตัวเลข และน้อยกว่า 13 หลัก
            $out['results'] = [['id' => '-999', 'text' => 'เลขบัตรประชาชนยังไม่ครบ 13 หลัก']];
        } else if (is_numeric($string_q) AND strlen($string_q) > 13) {
            //ถ้าเป็นตัวเลขมากกว่า 13 หลัก
            $out['results'] = [['id' => '-999', 'text' => 'เลขบัตรประชาชนมากกว่า 13 หลัก']];
        } else if (is_numeric($string_q) AND strlen($string_q) == 13) {
            //ดูดข้อมูล
            if(Yii::$app->keyStorage->get('frontend.domain') == 'dpmcloud.org'){
                $ezform = Ezform::find()->select('comp_id_target')->where(['ezf_id'=>$ezf_id])->one();
                $ezform = EzformQuery::getTablenameFromComponent($ezform->comp_id_target);
                $res = Yii::$app->db->createCommand("SELECT id FROM `".($ezform->ezf_table)."` WHERE rstat <> 3 AND `cid` LIKE '".$string_q."';")->queryOne();
                //print_r($res); exit;
                //ถ้าหาไม่เจอ
                if(!$res['id']){
                    //ดูดข้อมูล
                    self::copyPatientCascap($ezf_id, $string_q, ($ezform->ezf_table));
                }
            }
            $cid_k = substr($string_q, 0, 1) =='0' ? false : true;
            //ถ้าเป็นตัวเลข 13 หลักแล้ว (ทั้งต่างด้าวที่ขึ้นต้นด้วย 0)
            if (!self::checkCid($string_q) && $cid_k) {
                //กรณีเลขบัตรไม่ถูกต้อง
                $out['results'] = [['id' => '-999', 'text' => 'เลขบัตรประชาชน 13 หลักไม่ถูกต้อง']];
            }
            else {
                
                $ezfField = EzformQuery::getFieldNameByID($ezf_comp['field_search_cid']);
                //กรณีเลขบัตรถูกต้อง
                //ptid=ptid_key คือเลือกเอาเฉพาะตัวมีค่าเท่ากัน
                $string_decode_all = $string_decode . " AND rstat <> 3 AND `".($ezfField->ezf_field_name)."` LIKE '" . $string_q . "' AND id=ptid";
                $ortQuery_ptid = \Yii::$app->db->createCommand($string_decode_all);

                //หาทั้งหมด
                $string_decode_all = $string_decode . " AND rstat <> 3 AND `".($ezfField->ezf_field_name)."` LIKE '" . $string_q . "'";
                $ortQuery_all = \Yii::$app->db->createCommand($string_decode_all);

                $string_decode .= " AND rstat <> 3 AND `".($ezfField->ezf_field_name)."` LIKE '" . $string_q . "' AND xsourcex ='" . Yii::$app->user->identity->userProfile->sitecode . "'";
                $ortQuery = \Yii::$app->db->createCommand($string_decode);

                //เมื่อพบข้อมูลที่ site ตัวเอง
                if ($ortQuery->query()->count()) {
                    $data = $ortQuery->queryAll();
                    $out['results'] = array_values($data);
                } //เจอข้อมูลที่ site อื่นๆ ต้องการเพิ่มใหม่
                else if ($ortQuery_ptid->query()->count() || $ortQuery_all->query()->count()) {
                    $out['results'] = [['id' => '-991', 'text' => 'พบข้อมูลที่หน่วยงานอื่น ต้องการลงทะเบียน "' . $string_q . '" นี้ในหน่วยงานของท่าน หรือไม่?']];
                } //เมื่อไม่พบข้อมูลเลย ให้เพิ่มใหม่
                else if (!$ortQuery_ptid->query()->count() && !$ortQuery_all->query()->count()) {
                    $out['results'] = [['id' => '-990', 'text' => 'เพิ่มข้อมูลใหม่สำหรับ "' . $string_q . '" ในหน่วยงานของท่าน']];
                }

            }
        } else {
            
            //for string
            $string_q = str_replace(' ', ",", trim($string_q));
            $string_q = explode(',', $string_q);
            $xsourcex = Yii::$app->user->identity->userProfile->sitecode;
            $fieldSearch = self::setFieldSearch($ezf_comp);
            $countLenStr = count($string_q);
            if ($countLenStr == 1) {
                //fullcode ค้นได้เฉพาะฟอร์ม CCA-02
                if(($ezf_id == '1437619524091524800' || Yii::$app->user->can('datamanager')) && strlen($string_q[0]) == 10) {
                    //เตรียมข้อมูล
                    $modelComp = self::specialTarget1($ezf_comp);
                    $sqlSearch = "select ptid as id, concat(".$modelComp['concatSearch'].") as text, hsitecode, cid, `name`, surname FROM `".($modelComp['ezf_table_comp'])."` WHERE 1";

                    //หาด้วย codefull (hsitecode + hptcode)
                    //หา site ตัวเองก่อน
                    $string_decodex = " AND rstat <> 3 AND CONCAT(`hsitecode`, `hptcode`) LIKE '".$string_q[0]."' AND hsitecode <> '' AND hptcode <> '' ORDER BY `hptcode` DESC LIMIT 0,1";
                    $ortQuery = \Yii::$app->db->createCommand($sqlSearch.$string_decodex)->queryAll();
                    if ($ortQuery[0]['cid']) {
                        $string_decodex = " AND rstat <> 3 AND `cid` LIKE '" . $ortQuery[0]['cid'] . "' AND xsourcex = :xsourcex";
                        $ortQuery_site = \Yii::$app->db->createCommand($sqlSearch." AND xsourcex = :xsourcex".$string_decodex, [':xsourcex'=>$xsourcex])->queryAll();
                        if ($ortQuery_site[0]['cid']) {
                            $out['results'] = array_values($ortQuery_site);
                            return $out;
                        }else{
                            $string_decode_all = $sqlSearch . " AND rstat <> 3 AND `cid` LIKE '" . $ortQuery[0]['cid'] . "' AND id=ptid";
                            $ortQuery_all = \Yii::$app->db->createCommand($string_decode_all)->queryAll();

                            if(!$ortQuery_all[0]['cid']) {
                                //หาทั้งหมด
                                $string_decode_all = $sqlSearch . " AND rstat <> 3 AND `cid` LIKE '" . $ortQuery[0]['cid'] . "'";
                                $ortQuery_all = \Yii::$app->db->createCommand($string_decode_all)->queryAll();
                            }

                            if ($ortQuery_all[0]['cid']) {
                                $out['results'] = [['id' => '-991', 'text' => 'พบข้อมูล '.$ortQuery_all[0]['name'].' '.$ortQuery_all[0]['surname'].' ที่ Site อื่น ลงทะเบียน "' . $ortQuery[0]['cid'] . '" นี้ในหน่วยงานของท่าน']];
                                return $out;
                            }
                        }
                    }else{
                        
                        $string_decodex = " AND rstat <> 3 AND CONCAT(`hsitecode`, `hptcode`) LIKE '".$string_q[0]."' AND hsitecode <> '' AND hptcode <> '' ORDER BY `hptcode` DESC LIMIT 0,1";
                        $ortQuery = \Yii::$app->db->createCommand($sqlSearch.$string_decodex)->queryOne();
                        if ($ortQuery['cid']) {
                            $out['results'] = [['id' => '-991', 'text' => 'พบข้อมูล '.$ortQuery['name'].' '.$ortQuery['surname'].' ที่หน่วยงานอื่น ลงทะเบียน "' . $ortQuery['cid'] . '" นี้ในหน่วยงานของท่าน']];
                            return $out;
                        }
                    }

                }//data manager

                //หาด้วย hncode
                $frontendDomain = Yii::$app->keyStorage->get('frontend.domain');
                if ($frontendDomain == 'cascap.in.th' || $frontendDomain == 'dpmcloud.org') {
                    $ezform = EzformQuery::getTablenameFromComponent($ezf_comp['comp_id']);
                    $getColumnName = EzformQuery::getColumnName($ezform->ezf_table, 'hncode');
                    if($getColumnName['COLUMN_NAME']) {
                        $string_decodex = " AND rstat <> 3 AND (`hptcode`  LIKE '%" . ($string_q[0]) . "%' OR LOWER(`hncode`) LIKE LOWER('%" . ($string_q[0]) . "%')) AND hncode IS NOT NULL AND hncode <> '' ORDER BY `hptcode` ASC LIMIT 0,20";
                        $ortQuery = \Yii::$app->db->createCommand($string_decode . " AND xsourcex = :xsourcex" . $string_decodex, [':xsourcex' => $xsourcex])->queryAll();
                        if ($ortQuery[0]['cid']) {
                            $out['results'] = array_values($ortQuery);
                            return $out;
                        }
                    }
                }


                //ค้นตาม field_search ที่กำหนดไว้ใน component
                $string_decode .= " AND rstat <> 3 AND xsourcex = '" . $xsourcex . "' AND (CONCAT(".$fieldSearch['concat_search'].") LIKE '%" . ($string_q[0]) . "%') ORDER BY `hptcode` DESC LIMIT 0,20";

            }
            //ค้นกรณีมีการเว้นวรรคคำ 2 คำ ขึ้นไป
            else if ($countLenStr >= 2){
                for($i=0;$i<$countLenStr;$i++){
                    $strSearch = "CONCAT(".$fieldSearch['concat_search'].") LIKE '%" . ($string_q[$i]) . "%' AND ";
                }
                $strSearch = substr($strSearch, 0, -5);
                $string_decode .= " AND rstat <> 3 AND xsourcex = '" . $xsourcex . "' AND (".$strSearch.") ORDER BY `hptcode` DESC LIMIT 0,20";
            }

            if($countLenStr <=2) {
                $ortQuery = \Yii::$app->db->createCommand($string_decode);
                //echo ($ortQuery->rawSql);
                //Yii::$app->end();
                $data = $ortQuery->queryAll();
                $out['results'] = array_values($data);

                if (!$ortQuery->query()->count())
                    $out['results'] = [['id' => '-999', 'text' => 'ไม่พบคำที่ค้นหา! ลองค้นคำอื่นๆ หรือค้นหาจากเลข 13 หลัก']];
            }else{
                $out['results'] = [['id' => '-999', 'text' => 'ไม่พบคำที่ค้นหา! ลองค้นคำอื่นๆ หรือค้นหาจากเลข 13 หลัก']];
            }
        }
        
        return $out;
    }
    
    public static function checkCid($pid)
    {
        if (strlen($pid) != 13) return false;
        for ($i = 0, $sum = 0; $i < 12; $i++)
            $sum += (int)($pid{$i}) * (13 - $i);
        if ((11 - ($sum % 11)) % 10 == (int)($pid{12}))
            return true;
        return false;
    }
    
    public function copyPatientCascap($ezf_id, $cid, $ezf_table){

        //ค้นข้อมูล
        //หาข้อมูลที่สมบูรณ์ก่อน
        $model = Yii::$app->dbcascaputf8->createCommand("SELECT * FROM `tb_data_1` WHERE  `cid` LIKE '" . $cid . "' AND (error IS NULL OR error = '') AND rstat <> 3 AND rstat IS NOT NULL ORDER BY update_date DESC;")->queryOne();
        //หาไม่เจอเอาข้อมูลที่แรกให้
        if(!$model['id']) {
            $model = Yii::$app->dbcascaputf8->createCommand("SELECT * FROM `tb_data_1` WHERE  `cid` LIKE '" . $cid . "' AND id=ptid AND rstat <> 3 AND rstat IS NOT NULL;")->queryOne();
        }

        //หาไม่เจอเลยเอาข้อมูลที่พอมีมาให้
        if(!$model['id']) {
            $model = Yii::$app->dbcascaputf8->createCommand("SELECT * FROM `tb_data_1` WHERE  `cid` LIKE '" . $cid . "' AND rstat <> 3 AND rstat IS NOT NULL;")->queryOne();
        }

        if($model['id']) {
            Yii::$app->db->createCommand()->insert($ezf_table, $model)->execute();

            //save EMR
            $model = Yii::$app->dbcascaputf8->createCommand("SELECT * FROM `ezform_target` WHERE  data_id=:data_id AND ezf_id=:ezf_id;", [':data_id' => $model['id'], ':ezf_id' => $ezf_id])->queryOne();
            Yii::$app->db->createCommand()->insert('ezform_target', $model)->execute();
            //
        }

        return true;
    }
    
    public static function specialTarget1($ezform_comp)
    {
        $ezf_table = EzformQuery::getTablenameFromComponent($ezform_comp['comp_id']);

        // array("hsitecode", 'hptcode', "name", "surname");
        //$arr = explode(',', $ezform_comp['field_id_desc']);
        $concatSearch ='';
        $modelComp = [];
        $ezf_fields = Yii::$app->db->createCommand("SELECT ezf_field_name FROM ezform_fields WHERE ezf_field_id in(".$ezform_comp['field_id_desc'].");")->queryAll();
        foreach ($ezf_fields AS $val) {
            $modelComp['arr_comp_desc_field_name'][] = $val['ezf_field_name'];
            $concatSearch .= "IFNULL(`".$val['ezf_field_name']."`, ''), ' ', ";
        }
        $concatSearch = substr($concatSearch, 0, -2);

        $sqlSearch = "select ptid as id, concat(".$concatSearch.") as text FROM `".($ezf_table->ezf_table)."` WHERE 1";

        //VarDumper::dump($sqlSearch); exit;

        $modelComp['comp_id'] = $ezform_comp['comp_id'];
        $modelComp['concatSearch'] = $concatSearch;
        $modelComp['tagMultiple'] = false;
        $modelComp['sqlSearch'] = $sqlSearch;
        $modelComp['special'] = true;
        //ใช้แสดงชื่อเป้าหมาย ขั้นตอนที่ 3
        $modelComp['ezf_table_comp'] = $ezf_table->ezf_table;

        $modelComp['comp_key_name'] = 'ptid';
        return $modelComp;
    }
    
    public static function getStatusIcon($rstat){
	
	$icon = 'class="glyphicon glyphicon-time"';
	
	if($rstat==2){
	    $icon = 'class="glyphicon glyphicon-ok-sign"';
	} elseif ($rstat==4) {
	    $icon = 'class="fa fa-snowflake-o"';
	} elseif ($rstat==5) {
	    $icon = 'class="glyphicon glyphicon-lock"';
	} else {
	    $icon = 'class="glyphicon glyphicon-time"';
	}
	
	return $icon;
    }
    
    public static function findTargetComponent($ezform_comp)
    {
        /*
         1. ดูว่า EZForm มี comp_id_target อะไร
         2. ไปดูว่า ezform_comp_id นั้น ในตาราง ezf_component มี comp_select = 1 or 2 (select2 นั้น เลือกได้ 1 หรือมากกว่า 1)
         3. ดูว่า ezf_id ชี้ไปที่ ezform ไหน และ ezf_table อะไร
         4. ดูว่า จะ select id,description จาก field ชื่ออะไร (ดูได้จาก field_id_key field_id_desc และ ezf_table)
         5. สรุป select field_id_key as id,concat(field_id_desc1,' ',field_id_desc2) as description from ezf_table;
        */
        $comp_id = $ezform_comp['comp_id'];
        $ezf_id = $ezform_comp['ezf_id'];
        $ezform = Yii::$app->db->createCommand("SELECT ezf_table FROM ezform WHERE ezf_id='$ezf_id';")->queryOne();
        $ezf_table = $ezform['ezf_table'];
	
        //$arr_desc = explode(',', $ezform_comp['field_id_desc']);
        $str_desc = '';
        $arr_comp_desc_field_name = array();
        $whereIsNull = '';
        $ezf_fields = Yii::$app->db->createCommand("SELECT ezf_field_name FROM ezform_fields WHERE ezf_field_id in (".$ezform_comp['field_id_desc'].");")->queryAll();
        foreach ($ezf_fields AS $val) {
            $str_desc .= "IFNULL(`" . $val['ezf_field_name'] . "`, ''),' ',";
            $whereIsNull .= "(`" . $val['ezf_field_name'] . "` LIKE '' OR `" . $val['ezf_field_name'] . "` IS NULL) AND ";
            $arr_comp_desc_field_name[] = $val['ezf_field_name'];
        }
        $str_desc = substr($str_desc, 0, -5);
        $whereIsNull = substr($whereIsNull, 0, -5);
        //echo $str_desc; exit;

        //get field first in component
        //$ezf_fields = Yii::$app->db->createCommand("SELECT ezf_field_name FROM ezform_fields WHERE ezf_field_id='$arr_desc[0]]';")->queryOne();
        //$whereIsNull = $ezf_fields['ezf_field_name'];

        //$field_id_key = $ezform_comp['field_id_key'];
        //$ezf_fields = Yii::$app->db->createCommand("SELECT ezf_field_name FROM ezform_fields WHERE ezf_field_id='$field_id_key';")->queryOne();
        //select concat(ifnull(firstname,''),' ',ifnull(lastname,'')) from user_profile where not((firstname like '' or firstname is null) and (lastname like '' or lastname is null));
        $sql = "select  id, concat(" . $str_desc . ") as text from " . $ezf_table . " WHERE NOT(" . $whereIsNull . ")";
        //" . $ezf_fields['ezf_field_name'] . " as
        //$sql = "select ".$ezf_fields['ezf_field_name']." as id, concat(".$str_desc.") as text from ".$ezf_table." where $whereIsNull IS NOT NULL";
        //echo $sql; exit;
        $modelComp = Yii::$app->db->createCommand($sql)->queryAll();
        //echo ($modelComp); exit;
        //print_r  ($modelComp); exit;
        //$strModel =array();
        //$strModel = \yii\helpers\ArrayHelper::map($modelComp, 'id', 'text');

        if ($ezform_comp['comp_select'] == 1)
            $tagMultiple = false;
        else if ($ezform_comp['comp_select'] == 2)
            $tagMultiple = true;

        //create sql search
        if ($ezform_comp['shared'] == 1) {
            //public
        } else {
            //private
            if ($comp_id == 100000 OR $comp_id == 100001) {
                $sql = $sql . " AND sitecode = '" . Yii::$app->user->identity->userProfile->sitecode . "'";
            } else {
                $sql = $sql . " AND xsourcex = '" . Yii::$app->user->identity->userProfile->sitecode . "'";
            }
        }
        $sqlSearch = $sql . " AND CONCAT(" . $str_desc . ") LIKE '%\$q%' ";

        $modelComp['comp_id'] = $comp_id;
        $modelComp['tagMultiple'] = $tagMultiple;
        $modelComp['sqlSearch'] = $sqlSearch;
        $modelComp['ezf_table_comp'] = $ezf_table;
        $modelComp['arr_comp_desc_field_name'] = $arr_comp_desc_field_name;
        $modelComp['comp_key_name'] = $ezf_fields['ezf_field_name'];
        return $modelComp;
    }
    
    public static function getPersonOldServer($sitecode, $cid, $key, $convert){
        $str = "decode(unhex(person.address),sha2(:key,256)) AS address,
		    decode(unhex(person.cid),sha2(:key,256)) AS cid,
		    decode(unhex(person.hn),sha2(:key,256)) AS hn,
		    decode(unhex(person.pname),sha2(:key,256)) AS pname,
		    decode(unhex(person.fname),sha2(:key,256)) AS fname,
		    decode(unhex(person.lname),sha2(:key,256)) AS lname,";

        if ($convert==1) {
            $str = "convert(decode(unhex(person.address),sha2(:key,256)) using tis620) AS address,
		    convert(decode(unhex(person.cid),sha2(:key,256)) using tis620) AS cid,
		    convert(decode(unhex(person.hn),sha2(:key,256)) using tis620) AS hn,
		    convert(decode(unhex(person.pname),sha2(:key,256)) using tis620) AS pname,
		    convert(decode(unhex(person.fname),sha2(:key,256)) using tis620) AS fname,
		    convert(decode(unhex(person.lname),sha2(:key,256)) using tis620) AS lname,";
        }

        $sql = "SELECT person.khet,
		    person.province,
		    person.amphur,
		    person.tambon,
		    person.hospcode,
		    person.hospname,
		    person.person_id,
		    person.house_id,
		    $str
		    person.sex,
		    person.nationality,
		    person.education,
		    person.type_area,
		    person.religion,
		    person.birthdate,
		    floor(DATEDIFF(CURRENT_DATE, STR_TO_DATE(person.birthdate, '%Y-%m-%d'))/365) AS age,
		    person.village_id,
		    person.village_code,
		    person.village_name,
		    person.pttype,
		    person.pttype_begin_date,
		    person.pttype_expire_date,
		    person.pttype_hospmain,
		    person.pttype_hospsub,
		    person.marrystatus,
		    person.death,
		    person.death_date
	    FROM person
	    WHERE person.sitecode = :hospcode and cidlink like md5(:cid)
	    ";

        //ip 8
        return Yii::$app->dbbot_ip8->createCommand($sql, [':cid'=>$cid, ':hospcode'=>$sitecode, ':key'=>$key])->queryOne();
    }    
    public static function getPersonOneByCid($sitecode, $cid, $key, $convert) {
        //new server
        $str = "
		    decode(unhex(f_person.cid),sha2(:key,256)) AS cid,
		    decode(unhex(f_person.HN),sha2(:key,256)) AS hn,
		    decode(unhex(f_person.Pname),sha2(:key,256)) AS pname,
		    decode(unhex(f_person.Name),sha2(:key,256)) AS fname,
		    decode(unhex(f_person.Lname),sha2(:key,256)) AS lname,";

        if ($convert==1) {
            $str = "
		    convert(decode(unhex(f_person.cid),sha2(:key,256)) using tis620) AS cid,
		    convert(decode(unhex(f_person.HN),sha2(:key,256)) using tis620) AS hn,
		    convert(decode(unhex(f_person.Pname),sha2(:key,256)) using tis620) AS pname,
		    convert(decode(unhex(f_person.Name),sha2(:key,256)) using tis620) AS fname,
		    convert(decode(unhex(f_person.Lname),sha2(:key,256)) using tis620) AS lname,";
        }

        $sql = "SELECT
		    $str
		    f_person.sex,
		    f_person.Birth as birthdate,
		    f_person.ptlink
	    FROM f_person
	    WHERE f_person.sitecode = :hospcode and ptlink like md5(:cid) or decode(unhex(f_person.cid),sha2(:key,256))=:cid
	    ";

        $res = \backend\modules\ckdnet\classes\CkdnetFunc::queryOne($sql, [':cid'=>$cid, ':hospcode'=>$sitecode, ':key'=>$key]);

        $str = "decode(unhex(f_address.HouseNo),sha2(:key,256)) AS address ";

        if ($convert==1) {
            $str = "convert(decode(unhex(f_address.HouseNo),sha2(:key,256)) using tis620) AS address ";
        }

        if($res['cid']) {
            $resAddr = \backend\modules\ckdnet\classes\CkdnetFunc::queryOne("select f_address.Village as village_no, 
        f_address.villaname as village_name,
        f_address.Changwat as changwat,
        f_address.Ampur as ampur,
        f_address.Tambon as tambon, $str from f_address where f_address.sitecode = :hospcode and f_address.ptlink like :ptlink", [':hospcode'=>$sitecode, ':ptlink' => $res['ptlink'], ':key' => $key]);

            $res['address'] = $resAddr['address'];
            $res['village_no'] = $resAddr['village_no'];
            $res['village_name'] = $resAddr['village_name'];
            $res['changwat'] = $resAddr['changwat'];
            $res['ampur'] = $resAddr['ampur'];
            $res['tambon'] = $resAddr['tambon'];
        }else {
            //old server
            $res = self::getPersonOldServer($sitecode, $cid, $key, $convert);
        }

        return $res;
    }   
    
    public static function getPersonTdcOneByCid($sitecode, $cid, $key, $convert) {
        $str = "decode(unhex(address.HouseNo),sha2(:key,256)) AS HouseNo,
		    decode(unhex(person.CID),sha2(:key,256)) AS CID,
		    decode(unhex(person.HN),sha2(:key,256)) AS HN,
		    decode(unhex(person.Pname),sha2(:key,256)) AS Pname,
		    decode(unhex(person.`Name`),sha2(:key,256)) AS `Name`,
		    decode(unhex(person.Lname),sha2(:key,256)) AS Lname,";

        if ($convert==1) {
            $str = "convert(decode(unhex(address.HouseNo),sha2(:key,256)) using tis620) AS HouseNo,
		    convert(decode(unhex(person.CID),sha2(:key,256)) using tis620) AS CID,
		    convert(decode(unhex(person.HN),sha2(:key,256)) using tis620) AS HN,
		    convert(decode(unhex(person.Pname),sha2(:key,256)) using tis620) AS Pname,
		    convert(decode(unhex(person.`Name`),sha2(:key,256)) using tis620) AS `Name`,
		    convert(decode(unhex(person.Lname),sha2(:key,256)) using tis620) AS Lname,";
        }

        $sql = "SELECT person.HOSPCODE,
		    person.PID,
		    person.HID,
		    person.HouseID,
		    person.PreName,
		    $str
		    person.sex,
		    person.Birth,
		    floor(DATEDIFF(CURRENT_DATE, STR_TO_DATE(person.Birth, '%Y%m%d'))/365) AS age,
		    person.Mstatus,
		    person.Occupation_Old,
		    person.Occupation_New,
		    person.Race,
		    person.Nation,
		    person.Religion,
		    person.Education,
		    person.Fstatus,
		    person.Father,
		    person.Mother,
		    person.Couple,
		    person.Vstatus,
		    person.MoveIn,
		    person.Discharge,
		    person.Ddischarge,
		    person.BloodGroup,
		    person.Rh,
		    person.Labor,
		    person.PassPort,
		    person.TypeArea,
		    person.D_Update,
		    address.addresstype,
		    address.housetype,
		    address.House_id,
		    address.roomno,
		    address.condo,
		    address.soisub,
		    address.soimain,
		    address.villaname,
		    address.address,
		    address.Road,
		    address.Village,
		    address.Tambon,
		    address.Ampur,
		    address.Changwat,
		    address.TelePhone,
		    address.Mobile
	    FROM person INNER JOIN address ON person.PID = address.PID
	    WHERE person.HOSPCODE = :hospcode and cidlink like md5(:cid)
	    ";

        return Yii::$app->dbtdc->createCommand($sql, [':cid'=>$cid, ':hospcode'=>$sitecode, ':key'=>$key])->queryOne();
    }
    
    public static function insertTdcAll($sitecode, $table, $tdc_data){
	//นำเข้าข้อมูลใหม่
        $id = GenMillisecTime::getMillisecTime();
        $hpcode = $sitecode;
        $pid_data = Yii::$app->db->createCommand("SELECT MAX(CAST(hptcode AS UNSIGNED))  AS pidcode FROM `$table` WHERE xsourcex = '$hpcode';")->queryOne();
        $pid = str_pad($pid_data['pidcode']+1, 5, "0", STR_PAD_LEFT);
        //$pid = $pid_data['pidcode'];
        $ptcodefull = $hpcode.$pid;

	$tbdata = new TbdataAll();
        $tbdata->setTableName($table);

	//data set
	$tbdata->attributes = $tdc_data;

	//Fix
	$tbdata->id = $id;
	$tbdata->ptid = $id;
	$tbdata->xsourcex = $hpcode;
	$tbdata->target = $id;
	$tbdata->user_create = Yii::$app->user->id;
	$tbdata->create_date = new Expression('NOW()');
	$tbdata->sitecode = $hpcode;
	$tbdata->ptcode = $pid;
	$tbdata->ptcodefull = $ptcodefull;
	$tbdata->hsitecode = $hpcode;
	$tbdata->hptcode = $pid;
	$tbdata->rstat = 1;

	//custom data

	return $tbdata->save();
    }   
    
    public static function insertRecord($params){

        /*post
        ezf_id
        target
        comp_id_target
        */

        $chk_json = false;
        $purl = null;
        $xsourcex = Yii::$app->user->identity->userProfile->sitecode;//hospitalcode
        $xdepartmentx = Yii::$app->user->identity->userProfile->department_area;//xdepartmentx
	$dataid = false;
        //is set params
        if($params){
	    $dataid = $params['dataid'];
            $ezf_id = $params['ezf_id'];
            $target = $params['target'];
            $comp_id_target = $params['comp_id_target'];
            $xsourcex = $xsourcex;
            $chk_json = $chk_json;
        }

        //reset record
        if($dataid){
            
            $modelform = \backend\modules\ezforms\models\EzformFix::findOne($ezf_id);
            Yii::$app->dbwebs1->createCommand("DELETE FROM `".($modelform->ezf_table)."` WHERE id = :data_id;", [':data_id'=>$dataid])->execute();
            Yii::$app->dbwebs1->createCommand("DELETE FROM `ezform_target` WHERE `data_id` = :data_id;", [':data_id'=>$dataid])->execute();
            Yii::$app->dbwebs1->createCommand("DELETE FROM `ezform_data_relation` WHERE `target_ezf_data_id` = :data_id;", [':data_id'=>$dataid])->execute();
            Yii::$app->dbwebs1->createCommand("DELETE FROM `ezform_reply` WHERE `data_id` = :data_id;", [':data_id'=>$dataid])->execute();
            Yii::$app->dbwebs1->createCommand("DELETE FROM `query_request` WHERE `data_id` = :data_id;", [':data_id'=>$dataid])->execute();
            Yii::$app->dbwebs1->createCommand("DELETE FROM `ezform_data_log` WHERE `dataid` = :data_id;", [':data_id'=>$dataid])->execute();

            //กรณี dpmcloud ลบข้อมูล ให้ลบฝั่ง cascap ออกด้วย
            if(Yii::$app->keyStorage->get('frontend.domain') == 'dpmcloud.org'){
                $res = Yii::$app->dbwebs1->createCommand("SELECT * FROM ezform_sync WHERE ezf_local = :ezf_id", [':ezf_id'=>$ezf_id])->queryOne();
                //ดึงข้อมูลที่ Remote server
                $ezform = Yii::$app->dbcascaputf8->createCommand("SELECT ezf_table FROM `ezform` WHERE  ezf_id = :ezf_id", [':ezf_id'=>$res['ezf_remote']])->queryOne();
                if($ezform['ezf_id']){
                    //ถ้าพบข้อมูลให้ลบข้อมูลฝั่งนั้นออก
                    Yii::$app->dbcascaputf8->createCommand("DELETE FROM `".($ezform['ezf_table'])."` WHERE id = :data_id;", [':data_id'=>$dataid])->execute();
                    Yii::$app->dbcascaputf8->createCommand("DELETE FROM `ezform_target` WHERE `data_id` = :data_id;", [':data_id'=>$dataid])->execute();
                    Yii::$app->dbcascaputf8->createCommand("DELETE FROM `ezform_data_relation` WHERE `target_ezf_data_id` = :data_id;", [':data_id'=>$dataid])->execute();
                    Yii::$app->dbcascaputf8->createCommand("DELETE FROM `ezform_reply` WHERE `data_id` = :data_id;", [':data_id'=>$dataid])->execute();
                    Yii::$app->dbcascaputf8->createCommand("DELETE FROM `query_request` WHERE `data_id` = :data_id;", [':data_id'=>$dataid])->execute();
                    Yii::$app->dbcascaputf8->createCommand("DELETE FROM `ezform_data_log` WHERE `dataid` = :data_id;", [':data_id'=>$dataid])->execute();
                }
            }

        }else {
            $dataid = GenMillisecTime::getMillisecTime();
        }
        //VarDumper::dump($_POST, 10, true);
        //Yii::$app->end();

        $model_fields = \backend\modules\ezforms\components\EzformQueryFix::getFieldsByEzf_id($ezf_id);
        $model_form = EzformFunc::setDynamicModelLimit($model_fields);
        $table_name = \backend\modules\ezforms\components\EzformQueryFix::getFormTableName($ezf_id);

        $model = new \backend\modules\ezforms\models\EzformDynamic($table_name->ezf_table);

        //check unique_record
        if($table_name->unique_record==2){
            $model_chk = $model->find()->select('id')->where('ptid = :target AND rstat <>3', [':target' =>base64_decode($target)])->one();
            if($model_chk->id){
                return $model_chk->id;
            }
        }

        //check new record
        $model_chk = $model->find()->select('id')->where('(ptid = :target OR target = :target) AND xsourcex = :xsourcex AND rstat =0 AND user_create=:user', [':target' =>base64_decode($target), ':xsourcex' => $xsourcex, ':user'=>Yii::$app->user->id])->one();
        if($model_chk->id){
            return $model_chk->id;
        }


        //replace data set
//        if($_REQUEST['dataset']) {
//            $dataSet = Json::decode(base64_decode($_REQUEST['dataset']));
//            foreach ($dataSet as $key=>$value){
//                $model_form->{$key} = $value;
//            }
//        }
//        $model->attributes = $model_form->attributes;


        $model->id = $dataid;
        $model->rstat = 0;
        $model->user_create = Yii::$app->user->id;
        $model->create_date = new Expression('NOW()');
        $model->user_update = Yii::$app->user->id;
        $model->update_date = new Expression('NOW()');
        $model->xsourcex = $xsourcex;//hospitalcode
        $model->xdepartmentx = $xdepartmentx;
        if ($target <> 'skip' AND $target <> 'all') {
            $ezform = EzformQuery::checkIsTableComponent($ezf_id);
            // ตาราง Register ของ special Component
            if($ezform['ezf_id'] == $ezf_id){
                //save target
                $model->target = $dataid;
                //save ptid
                $targetx = base64_decode($target);
                $ezform = EzformQuery::getTablenameFromComponent($comp_id_target);
                $targetx = EzformQuery::getIDkeyFromtable($ezform->ezf_table, $targetx);
                $model->ptid = $targetx['ptid'];

                //save sitecode, ptcode. ptcodefull
                $ptcodefull = $targetx['sitecode'].$targetx['ptcode'];
                $model->sitecode = $targetx['sitecode'];
                $model->ptcode = $targetx['ptcode'];
                $model->hsitecode = $targetx['hsitecode'];
                $model->hptcode = $targetx['hptcode'];
                $model->ptcodefull = $ptcodefull;
            }
            // ตาราง ทั่วไป ของ special Component
            else if ($ezform['special']) {
                //save target
                $target_decode = base64_decode($target);
                $ezform = EzformQuery::getTablenameFromComponent($comp_id_target);
                $targetx = EzformQuery::getTargetFromtable($ezform->ezf_table, $target_decode, $xsourcex);
                $model->target = $targetx['id'];
                //save ptid

                $targetx = EzformQuery::getIDkeyFromtable($ezform->ezf_table, $target_decode);
                $model->ptid = $targetx['ptid'];

                //save sitecode, ptcode. ptcodefull
                $ptcodefull = $targetx['sitecode'].$targetx['ptcode'];
                $model->sitecode = $targetx['sitecode'];
                $model->ptcode = $targetx['ptcode'];

                $res = Yii::$app->db->createCommand("select hsitecode, hptcode from `".($ezform->ezf_table)."` where ptid = :target AND hptcode <> '' AND hsitecode <> '' AND xsourcex = :xsourcex AND rstat <>3 order by create_date DESC", [':target' => $targetx['ptid'], ':xsourcex' => $xsourcex])->queryOne();
                $model->hsitecode = $res['hsitecode'];
                $model->hptcode = $res['hptcode'];
                $model->ptcodefull = $ptcodefull;
                //VarDumper::dump($targetx, 10, true);

            } else {
                $model->target = isset($target) ? base64_decode($target) : NULL;
                $model->ptid = isset($target) ? base64_decode($target) : NULL;
            }
        }
        foreach ($model_fields as $key => $value) {

            if ($value['ezf_field_type'] == 7 || $value['ezf_field_type'] == 253) {

                //$model->{$value['ezf_field_name']} = date('Y-m-d');

            }
            else if ($value['ezf_field_type'] == 9) {

                //$model->{$value['ezf_field_name']} = date('Y-m-d H:i:s');

            }
            else if ($value['ezf_field_type'] == 18) {
                //EzformQuery::saveReferenceFields($ezf_field_ref_field, $ezf_field_ref_table, $target, $ezf_id, $ezf_field_id, $dataid);
                $target_decode = base64_decode($target);
                EzformQuery::saveReferenceFields($value['ezf_field_ref_field'], $value['ezf_field_ref_table'], $target_decode, $value['ezf_id'], $value['ezf_field_id'], $dataid);
            }

        }

        if ($model->save()) {
            //save EMR
            $model = new EzformTarget();
            $model->ezf_id = $ezf_id;
            $model->data_id = $dataid;
            isset($target) ? $model->target_id = base64_decode($target) : NULL;
            isset($comp_id_target) ? $model->comp_id = $comp_id_target : NULL;
            $model->user_create = Yii::$app->user->id;
            $model->create_date = new Expression('NOW()');
            $model->user_update = Yii::$app->user->id;
            $model->update_date = new Expression('NOW()');
            $model->rstat = 0;
            $model->xsourcex = $xsourcex;
            $model->save();
            //
        }

        //cascap reference fields CCA01
        if($ezf_id =='1437377239070461302') {
            $targetx = base64_decode($target);
            $res = Yii::$app->db->createCommand("SELECT v2 as birthd, v3 as sex, add1n8code, add1n7code, add1n6code FROM tb_data_1 WHERE ptid = '$targetx';")->queryOne();
            Yii::$app->db->createCommand("UPDATE tb_data_2 SET f1v1a1 = :f1v1a1, f1v1a2 = :f1v1a2, f1v1a3 = :f1v1a3, f1v2 = :f1v2, f1v3 = :f1v3 WHERE id = '$dataid';", [
                'f1v1a1' => $res['add1n8code'],
                'f1v1a2' => $res['add1n7code'],
                'f1v1a3' => $res['add1n6code'],
                'f1v2' => $res['birthd'],
                'f1v3' => $res['sex'],
            ])->query();
        }

        //Palliative reference fields Refer
        if($ezf_id =='1450928555015607100') {
            $targetx = base64_decode($target);
            $res = Yii::$app->db->createCommand("SELECT var27_province, var27_amphur, var27_tumbon FROM  tbdata_1 WHERE ptid = '".$targetx."' AND xsourcex = '".$xsourcex."';")->queryOne();
            Yii::$app->db->createCommand("UPDATE tbdata_4 SET refer_province = :val_province, refer_amphur = :val_amphur, refer_tumbon = :val_tumbon WHERE id = '$dataid';", [
                'val_province' => $res['var27_province'],
                'val_amphur' => $res['var27_amphur'],
                'val_tumbon' => $res['var27_tumbon'],
            ])->query();
        }

        //save next record
//        if(Yii::$app->request->post('addnext')){
//            $dataid_before = Yii::$app->request->post('addnext');
//            $fields = EzformFields::find()->select('ezf_field_name, ezf_field_options')->where('ezf_id = :ezf_id', [':ezf_id' => $ezf_id])->all();
//            $listField ='';
//            $listFieldUpdate = '';
//            foreach($fields as $field){
//                $json = json_decode($field->ezf_field_options, true);
//                if($json['remember_field']){
//                    $listField .= $field->ezf_field_name.', ';
//                    $listFieldUpdate .= $field->ezf_field_name.' = :'.$field->ezf_field_name.', ';
//                }
//            }
//
//            $listField = substr($listField, 0, -2);
//            $listFieldUpdate = substr($listFieldUpdate, 0, -2);
//            if($listField) {
//                $res = Yii::$app->db->createCommand("SELECT " . $listField . " FROM " . $table_name->ezf_table . " WHERE id = '" . $dataid_before . "';")->queryOne();
//                Yii::$app->db->createCommand("UPDATE " . $table_name->ezf_table . " SET " . $listFieldUpdate . " WHERE id = '$dataid';", $res)->query();
//            }
//        }

	return $dataid;

    }
 
    public static function importDataFromTdc($sitecode, $tdc_data, $comp_id=null){

        $comp = EzformComponent::find()
            ->select('ezf_id, comp_id')
            ->where(['comp_id' => $comp_id])
            ->one();
        $ezform = Ezform::find()
            ->select('ezf_id, ezf_table')
            ->where(['ezf_id' => $comp->ezf_id])
            ->one();

        //นำเข้าข้อมูลใหม่
	$table = $ezform->ezf_table;
	$id = GenMillisecTime::getMillisecTime();
        $hpcode = $sitecode;
        $pid_data = Yii::$app->db->createCommand("SELECT MAX(CAST(hptcode AS UNSIGNED))  AS pidcode FROM `$table` WHERE xsourcex = '$hpcode';")->queryOne();
        $pid = str_pad($pid_data['pidcode']+1, 5, "0", STR_PAD_LEFT);
        //$pid = $pid_data['pidcode'];
        $ptcodefull = $hpcode.$pid;

	$tbdata = new TbdataAll();
        $tbdata->setTableName($table);

	//data set
	$tbdata->attributes = $tdc_data;

	//Fix
	$tbdata->id = $id;
	$tbdata->ptid = $id;
	$tbdata->xsourcex = $hpcode;
	$tbdata->target = $id;
	$tbdata->user_create = Yii::$app->user->id;
	$tbdata->create_date = new Expression('NOW()');
	$tbdata->sitecode = $hpcode;
	$tbdata->ptcode = $pid;
	$tbdata->ptcodefull = $ptcodefull;
	$tbdata->hsitecode = $hpcode;
	$tbdata->hptcode = $pid;
	$tbdata->rstat = 1;

	if($tbdata->save()){
            //save EMR
            $modelTarget = new EzformTarget();
            $modelTarget->ezf_id = $ezform->ezf_id;
            $modelTarget->comp_id = $comp_id;
            $modelTarget->data_id = $tbdata['id'];
            $modelTarget->target_id = $tbdata['id'];
            $modelTarget->user_create = Yii::$app->user->id;
            $modelTarget->create_date = new Expression('NOW()');
            $modelTarget->user_update = Yii::$app->user->id;
            $modelTarget->update_date = new Expression('NOW()');
            $modelTarget->rstat = 1;
            $modelTarget->xsourcex = $tbdata['xsourcex'];
            $modelTarget->save();

            $arr = [
                'hsitecode' => $tbdata['hsitecode'],
                'hptcode' => $tbdata['hptcode'],
                'cid' => $tbdata['CID'],
                'ezf_id' => $ezform->ezf_id,
                'dataid' => $tbdata['id'],
		'target' => base64_encode($id),
		'comp_id_target' => $comp_id,
		'ezf_id_main' => Yii::$app->session['ezform_main'],
            ];
            return $arr;

        }
	return FALSE;
    }
    
    public function importDataFromBot($sitecode, $cid, $tccbot, $comp_id=null){

        $comp = EzformComponent::find()
            ->select('ezf_id, comp_id')
            ->where(['comp_id' => $comp_id])
            ->one();
        $ezform = Ezform::find()
            ->select('ezf_id, ezf_table')
            ->where(['ezf_id' => $comp->ezf_id])
            ->one();

        //นำเข้าข้อมูลใหม่
        $id = GenMillisecTime::getMillisecTime();
        $hpcode = $sitecode;
        $user_create = Yii::$app->user->id;
        $create_date = date('Y-m-d H:i:s');
        $pid = Yii::$app->db->createCommand("SELECT MAX(CAST(hptcode AS UNSIGNED))  AS pidcode FROM `".($ezform->ezf_table)."` WHERE xsourcex = '$hpcode';")->queryOne();
        $pid = str_pad($pid['pidcode']+1, 5, "0", STR_PAD_LEFT);
        //$pid = $pid['pidcode'];
        $ptcodefull = $hpcode.$pid;

        if(strlen($tccbot['village_code'])>2){
            //หมู่ที่
            $add1n5=substr($tccbot['village_code'], -2);
            //จังหวัด
            $add1n8code=substr($tccbot['village_code'], 0, 2);
            //อำเภอ
            $add1n7code =substr($tccbot['village_code'], 0, 4);
            //ตำบล
            $add1n6code=substr($tccbot['village_code'], 0, 6);
        }else{
            //หมู่ที่
            $add1n5=$tccbot['village_no'];
            //จังหวัด
            $add1n8code=$tccbot['changwat'];
            //อำเภอ
            $add1n7code =$tccbot['ampur'];
            //ตำบล
            $add1n6code=$tccbot['tambon'];
        }
        //check ptid from system
        //หาข้อมูลล่าสุดก่อนมาให้
        $modelFromSys = Yii::$app->db->createCommand("SELECT ptid FROM `".($ezform->ezf_table)."` WHERE  `cid` LIKE '" . $cid . "' AND rstat <> 3 AND rstat IS NOT NULL ORDER BY update_date DESC;")->queryOne();

        $r = Yii::$app->db->createCommand()->insert($ezform->ezf_table,  [
            'id' => $id,
            'ptid' => $modelFromSys['ptid'] ? $modelFromSys['ptid'] : $id,
            'xsourcex' => $hpcode,
            'target' => $id,
            'user_create' => $user_create,
            'create_date' => $create_date,
            'sitecode' => $hpcode,
            'ptcode' => $pid,
            'ptcodefull' => $ptcodefull,
            'hsitecode' => $hpcode,
            'hptcode' => $pid,
            'cid' => $cid,
            'rstat' => 1,
            //new
            'tccbot'=>1,
            'title' => $tccbot['pname'],
            'name' => $tccbot['fname'],
            'surname' => $tccbot['lname'],
            'v2' => $tccbot['birthdate'],
            'v3' => $tccbot['sex'],
            'age' => \backend\modules\ovcca\classes\OvccaFunc::getAge($tccbot['birthdate']),

            'add1n2' => $tccbot['village_name'],
            'add1n1' => $tccbot['address'],
            'add1n5'=>$add1n5,
            'add1n8code'=>$add1n8code,
            'add1n7code'=>$add1n7code,
            'add1n6code'=>$add1n6code,

            'hn' => $tccbot['hn'],
            'hncode' => $tccbot['hn'],
        ])->execute();

        if($r){
            //save EMR
            $modelTarget = new EzformTarget();
            $modelTarget->ezf_id = $ezform->ezf_id;
            $modelTarget->comp_id = $comp_id;
            $modelTarget->data_id = $id;
            $modelTarget->target_id = $id;
            $modelTarget->user_create = Yii::$app->user->id;
            $modelTarget->create_date = new Expression('NOW()');
            $modelTarget->user_update = Yii::$app->user->id;
            $modelTarget->update_date = new Expression('NOW()');
            $modelTarget->rstat = 1;
            $modelTarget->xsourcex = $hpcode;
            $modelTarget->save();

            $arr = [
                'hsitecode' => $hpcode,
                'hptcode' => $pid,
                'cid' => $cid,
                'ezf_id' => $ezform->ezf_id,
                'dataid' => $id,
		'target' => base64_encode($id),
		'comp_id_target' => $comp_id,
		'ezf_id_main' => Yii::$app->session['ezform_main'],
            ];
            return $arr;

        } else{
        }
    }
    
    public static function getColAddon(){
        $fields = Yii::$app->session['sql_main_fields'];
        
        $fieldsMain = [];
        
        $dataCompMain = \backend\modules\component\models\EzformComponent::find()->where('comp_id=:comp_id',[':comp_id'=>$fields->comp_id_target])->one();
        if($dataCompMain && $fields->main_ezf_id!=$fields->ezf_id){
           $dataFieldsMain = \backend\modules\inv\classes\InvFunc::getFields($fields->main_ezf_id, 'fxmain_');
           $fieldsMain = array_keys($dataFieldsMain);
        }
        
        return $fieldsMain;
    }
    
}
