<?php

namespace backend\modules\inv\controllers;

use Yii;
use backend\modules\inv\models\InvFields;
use backend\modules\inv\models\InvFieldsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\Response;
use appxq\sdii\helpers\SDHtml;
use backend\modules\inv\classes\InvQuery;
use backend\modules\ezforms\models\Ezform;

/**
 * InvFieldsController implements the CRUD actions for InvFields model.
 */
class InvFieldsController extends Controller
{
    public function behaviors()
    {
        return [
	    'access' => [
		'class' => AccessControl::className(),
		'rules' => [
		    [
			'allow' => true,
			'actions' => ['index', 'view'], 
			'roles' => ['?', '@'],
		    ],
		    [
			'allow' => true,
			'actions' => ['view', 'create', 'update', 'delete', 'deletes'], 
			'roles' => ['@'],
		    ],
		],
	    ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public function beforeAction($action) {
	if (parent::beforeAction($action)) {
	    if (in_array($action->id, array('create', 'update'))) {
		
	    }
	    return true;
	} else {
	    return false;
	}
    }
    
    /**
     * Lists all InvFields models.
     * @return mixed
     */
    public function actionIndex($comp, $module, $sub_id)
    {
        $searchModel = new InvFieldsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $sub_id);
	
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
	    'comp' => $comp,
	    'module' => $module,
	    'sub_id'=>$sub_id
        ]);
    }

    /**
     * Displays a single InvFields model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
	if (Yii::$app->getRequest()->isAjax) {
	    return $this->renderAjax('view', [
		'model' => $this->findModel($id),
	    ]);
	} else {
	    return $this->render('view', [
		'model' => $this->findModel($id),
	    ]);
	}
    }

    /**
     * Creates a new InvFields model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($comp, $sub_id)
    {
	if (Yii::$app->getRequest()->isAjax) {
	    $ezform = \backend\modules\inv\classes\InvQuery::getEzformById($comp);
		
	    $model = new InvFields();
	    $model->sub_id = $sub_id;
	    $model->width = 110;
	    $model->report_width = 14;
	    $model->comp = $comp;
	    $model->comp_id_target = $ezform['comp_id_target'];
	    
	    
	    $modelEzform = InvQuery::getEzform();
	    
	    
	    if ($model->load(Yii::$app->request->post())) {
		Yii::$app->response->format = Response::FORMAT_JSON;
		
		$objEzform = Ezform::find()->where('ezf_id=:ezf_id', [':ezf_id'=>$model->ezf_id])->one();
		$model->ezf_name = $objEzform->ezf_name;
		$model->ezf_table = $objEzform->ezf_table;
		$model->comp_id_target = $objEzform->comp_id_target;
		$model->sql_id_name = 'id_'.$objEzform->ezf_id;
		$model->sql_idsubmit_name = 'idsubmit_'.$objEzform->ezf_id;
		$model->sql_result_name = 'result_'.$objEzform->ezf_id;
		$options = '';
		if(isset($_POST['options'])){
		    $options = \appxq\sdii\utils\SDUtility::array2String($_POST['options']);
		}
		$model->value_options = $options;
		
		if ($model->save()) {
		    $result = [
			'status' => 'success',
			'action' => 'create',
			'message' => SDHtml::getMsgSuccess() . Yii::t('app', 'Data completed.'),
			'data' => $model,
		    ];
		    return $result;
		} else {
		    $result = [
			'status' => 'error',
			'message' => SDHtml::getMsgError() . Yii::t('app', 'Can not create the data.'),
			'data' => $model,
		    ];
		    return $result;
		}
	    } else {
		return $this->renderAjax('create', [
		    'model' => $model,
		    'modelEzform'=>$modelEzform
		]);
	    }
	} else {
	    throw new NotFoundHttpException('Invalid request. Please do not repeat this request again.');
	}
    }

    /**
     * Updates an existing InvFields model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
	if (Yii::$app->getRequest()->isAjax) {
	    $model = $this->findModel($id);
	    $modelEzform = InvQuery::getEzform();
	    
	    if ($model->load(Yii::$app->request->post())) {
		Yii::$app->response->format = Response::FORMAT_JSON;
		
		$objEzform = Ezform::find()->where('ezf_id=:ezf_id', [':ezf_id'=>$model->ezf_id])->one();
		$model->ezf_name = $objEzform->ezf_name;
		$model->ezf_table = $objEzform->ezf_table;
		$model->comp_id_target = $objEzform->comp_id_target;
		$model->sql_id_name = 'id_'.$objEzform->ezf_id;
		$model->sql_idsubmit_name = 'idsubmit_'.$objEzform->ezf_id;
		$model->sql_result_name = 'result_'.$objEzform->ezf_id;
		$options = '';
		if(isset($_POST['options'])){
		    $options = \appxq\sdii\utils\SDUtility::array2String($_POST['options']);
		}
		$model->value_options = $options;
		
		if ($model->save()) {
		    $result = [
			'status' => 'success',
			'action' => 'update',
			'message' => SDHtml::getMsgSuccess() . Yii::t('app', 'Data completed.'),
			'data' => $model,
		    ];
		    return $result;
		} else {
		    $result = [
			'status' => 'error',
			'message' => SDHtml::getMsgError() . Yii::t('app', 'Can not update the data.'),
			'data' => $model,
		    ];
		    return $result;
		}
	    } else {
		return $this->renderAjax('update', [
		    'model' => $model,
		    'modelEzform'=>$modelEzform
		]);
	    }
	} else {
	    throw new NotFoundHttpException('Invalid request. Please do not repeat this request again.');
	}
    }

    /**
     * Deletes an existing InvFields model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
	if (Yii::$app->getRequest()->isAjax) {
	    Yii::$app->response->format = Response::FORMAT_JSON;
	    if ($this->findModel($id)->delete()) {
		$result = [
		    'status' => 'success',
		    'action' => 'update',
		    'message' => SDHtml::getMsgSuccess() . Yii::t('app', 'Deleted completed.'),
		    'data' => $id,
		];
		return $result;
	    } else {
		$result = [
		    'status' => 'error',
		    'message' => SDHtml::getMsgError() . Yii::t('app', 'Can not delete the data.'),
		    'data' => $id,
		];
		return $result;
	    }
	} else {
	    throw new NotFoundHttpException('Invalid request. Please do not repeat this request again.');
	}
    }

    public function actionDeletes() {
	if (Yii::$app->getRequest()->isAjax) {
	    Yii::$app->response->format = Response::FORMAT_JSON;
	    if (isset($_POST['selection'])) {
		foreach ($_POST['selection'] as $id) {
		    $this->findModel($id)->delete();
		}
		$result = [
		    'status' => 'success',
		    'action' => 'deletes',
		    'message' => SDHtml::getMsgSuccess() . Yii::t('app', 'Deleted completed.'),
		    'data' => $_POST['selection'],
		];
		return $result;
	    } else {
		$result = [
		    'status' => 'error',
		    'message' => SDHtml::getMsgError() . Yii::t('app', 'Can not delete the data.'),
		    'data' => $id,
		];
		return $result;
	    }
	} else {
	    throw new NotFoundHttpException('Invalid request. Please do not repeat this request again.');
	}
    }
    
    /**
     * Finds the InvFields model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return InvFields the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = InvFields::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
