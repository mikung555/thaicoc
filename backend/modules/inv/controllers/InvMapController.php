<?php
namespace backend\modules\inv\controllers;

use Yii;
use yii\web\Controller;
use backend\modules\inv\classes\InvQuery;
use yii\helpers\ArrayHelper;
use appxq\sdii\helpers\SDHtml;
use backend\modules\inv\models\InvGen;
use backend\modules\inv\classes\InvFunc;
use yii\web\Response;
use backend\modules\inv\models\InvMain;
use backend\modules\ezforms\models\Ezform;
use yii\web\UploadedFile;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of InvMap
 *
 * @author appxq
 */
class InvMapController extends Controller {
    
    public function actionCreateModules()
    {
	$module = isset($_GET['module'])?$_GET['module']:0;
	$userId = Yii::$app->user->id;
	$sitecode = Yii::$app->user->identity->userProfile->sitecode;
	
	
	$model = InvQuery::getModule($module, $userId);
	$ezform = InvQuery::getCompAll();
	
	$dataComp = ArrayHelper::map($ezform, 'ezf_id', 'special');
	$dataComp2 = ArrayHelper::map($ezform, 'ezf_id', 'ezf_field_name');
	
	if($model){
            $model->enable_field = \appxq\sdii\utils\SDUtility::string2Array($model->enable_field);
	    $model->enable_form = \appxq\sdii\utils\SDUtility::string2Array($model->enable_form);
	    $model->share = explode(',', $model->share);
            
	    if(($model->public==1 && Yii::$app->user->can('administrator')) || $model['created_by']==Yii::$app->user->id){

	    } else {
		$userProfile = Yii::$app->user->identity->userProfile;
		Yii::$app->getSession()->setFlash('alert', [
		    'body'=> SDHtml::getMsgError() . Yii::t('app', 'คุณไม่มีสิทธิ์แก้ไขโมดูลนี้กรุณาติดต่อผู้สร้าง  '.'คุณ '.$userProfile->firstname.' '.$userProfile->lastname. ' เบอร์โทร '.$userProfile->telephone),
		    'options'=>['class'=>'alert-danger']
		]);

		return $this->redirect(['/inv/inv-map/index', 'module'=>$module]);
	    }	
	} else {
	    
	    $model = new InvGen();
	    $model->sitecode = $sitecode;
	    $model->active = 1;
	    $model->gsystem = 0;
            $model->enable_target = 0;
            $model->gtype = 2;
            $model->main_ezf_id = 0;
            $model->main_ezf_name='';
            $model->main_ezf_table='';
            $model->main_comp_id_target=0;
            $model->comp_id_target=0;
            $model->special = 0;
            $model->ezf_id = 0;
            $model->ezf_name = '';
            $model->ezf_table = '';
            $model->pk_field = '';
            
	}
	
	
	//\appxq\sdii\utils\VarDumper::dump($dataFields);
	$fromOwn = ArrayHelper::map(InvQuery::getEzformByOwnMap(),'id','name');
	$fromFav = ArrayHelper::map(InvQuery::getEzformByFavMap(),'id','name');
	$fromAssign = ArrayHelper::map(InvQuery::getEzformByAssignMap($sitecode),'id','name');
	
	//$fromAll = array_merge(['ฟอร์มที่ฉันสร้าง'=>$fromOwn], ['ฟอร์มที่ฉันเลือก Favorite'=>$fromFav]);
	$dataSubForm = array_merge(['ฟอร์มที่ฉันสร้าง'=>$fromOwn], ['ฟอร์มที่ฉันเลือก Favorite'=>$fromFav], ['ฟอร์มที่ถูกแชร์มาให้ฉันหรือสาธารณะ'=>$fromAssign]);

        $oldFileName = $model->gicon;
	
	if ($model->load(Yii::$app->request->post())) {

            if(isset($model->share) && !empty($model->share)){
		$model->share = implode(',', $model->share);
	    }
	    
	    if($_POST['action_submit']=='submit'){
		$model->active = 1;
	    } else {
		$model->active = 0;
	    }
	    
	    if($model->public==0){
		$model->approved = 0;
	    } 
	    
	    if (isset($_FILES['InvGen']['name']['gicon']) && $_FILES['InvGen']['name']['gicon']!='') {
		$icon_file = UploadedFile::getInstance($model, 'gicon');
		
		$typeArry = explode('/', $icon_file->type);
		
		$typeName = 'jpg';
		if(isset($typeArry[1])){
		    $typeName = $typeArry[1];
		}
		
		$newFileName = 'icon_'.date("Ymd_His").'_'.\common\lib\codeerror\helpers\GenMillisecTime::getMillisecTime().'.'.$typeName;
		$fullPath = Yii::$app->basePath . '/web/module_icon/' . $newFileName;
		
		$icon_file->saveAs($fullPath);
		
		@unlink(Yii::$app->basePath . '/web/module_icon/' .$oldFileName);
		
		$model->gicon = $newFileName;
	    } else {
		$model->gicon = isset($oldFileName)?$oldFileName:'';
	    }
	    //$model->gid = \common\lib\codeerror\helpers\GenMillisecTime::getMillisecTime();
	    $action = $model->isNewRecord;
            
            $enable_field = '';
            if(isset($_POST['fields'])){
                $enable_field = \appxq\sdii\utils\SDUtility::array2String($_POST['fields']);
            }
            $model->enable_field = $enable_field;

            $enable_form = '';
            if(isset($_POST['forms'])){
                $enable_form = \appxq\sdii\utils\SDUtility::array2String($_POST['forms']);
            }
            $model->enable_form = $enable_form;
            
	    if($model->isNewRecord){
		$model->gid = \common\lib\codeerror\helpers\GenMillisecTime::getMillisecTime();
	    }
	    
	    if ($model->save()) {
		
		Yii::$app->getSession()->setFlash('alert', [
		    'body'=> SDHtml::getMsgSuccess() . Yii::t('app', 'Data completed.'),
		    'options'=>['class'=>'alert-success']
		]);
		
		if($action){
		    return $this->redirect(['/inv/inv-gen/index']);
		} else {
		    return $this->redirect(['/inv/inv-map/create-modules', 'module'=>$model->gid]);
		}
	    } else {
		Yii::$app->getSession()->setFlash('alert', [
		    'body'=> SDHtml::getMsgError() . Yii::t('app', 'Can not create the data.'),
		    'options'=>['class'=>'alert-danger']
		]);
	    }
	}
	
	return $this->render('create-modules', [
	    'model' => $model,
	    'module' => $module,
	    'dataSubForm' => $dataSubForm,
	]);
    }
    
    public function actionIndex()
    {
        //return $this->render('offline');
        
	$module = isset($_GET['module'])?$_GET['module']:0;
	$sub_module = isset($_GET['sub_module'])?$_GET['sub_module']:0;
        
        Yii::$app->session['pjax_reload'] = 'inv-person-map-pjax';
        
	$sitecode = Yii::$app->user->identity->userProfile->sitecode;
	$userId = Yii::$app->user->id;
	
        $show_module = $sub_module>0?$sub_module:$module;
        
	$comp;
	$inv_main;
	if($show_module>0){
	    $inv_main = InvQuery::getModule($show_module, $userId);
	    if($inv_main){
		$comp = $inv_main['ezf_id'];
	    } else {
		Yii::$app->getSession()->setFlash('alert', [
		    'body'=> 'ไม่พบโมดูล',
		    'options'=>['class'=>'alert-danger']
		]);

		return $this->redirect(['/tccbots/my-module']);
	    }
	} else {
	    Yii::$app->getSession()->setFlash('alert', [
                'body'=> 'ไม่พบโมดูล',
                'options'=>['class'=>'alert-danger']
            ]);

            return $this->redirect(['/tccbots/my-module']);
	}
	
	Yii::$app->session['sql_fields'] = false;//InvQuery::getInvFields($ovfilter_sub);
	Yii::$app->session['sql_main_fields'] = $inv_main;
        $comp_id = Yii::$app->keyStorage->get('personprofile.comp_id');
        $enable_form = \appxq\sdii\utils\SDUtility::string2Array(Yii::$app->session['sql_main_fields']['enable_form']);
        Yii::$app->session['emr_form'] = array_values(ArrayHelper::getColumn($enable_form, 'form'));
        
        //\appxq\sdii\utils\VarDumper::dump(Yii::$app->session['emr_form']);
        
        if(isset($comp_id)){
            $sql = "SELECT
            `ezform_component`.*,
            `ezform`.`ezf_table`
            FROM
            `ezform_component`
            JOIN `ezform`
            ON `ezform_component`.`ezf_id` = `ezform`.`ezf_id`
            WHERE
            `ezform_component`.`comp_id` = :comp_id ";
            $ezf_comp = Yii::$app->db->createCommand($sql, [':comp_id'=>$comp_id])->queryOne();
            
            if($ezf_comp){
                Yii::$app->session['sql_ezf_comp'] = $ezf_comp;
                Yii::$app->session['emr_form'] = ArrayHelper::merge(Yii::$app->session['emr_form'], [$ezf_comp['ezf_id']]);
            } else {
                Yii::$app->session['sql_ezf_comp'] = null;
            }
        } else {
            Yii::$app->session['sql_ezf_comp'] = null;
        } 
        
	try {
           
            
	    return $this->render('index', [
		'module' => $module,
                'sub_module' =>$sub_module,
            'show_module'=>$show_module,
		'sitecode' =>$sitecode,
		'inv_main'=>$inv_main,
	    ]);
	} catch (\yii\db\Exception $e) {
	    return $this->render('error', [
		'inv_main'=>$inv_main,
		'module' => $module,
                'sub_module' =>$sub_module,
            'show_module'=>$show_module,
		'sitecode' =>$sitecode,
		'msg'=>$e->getMessage(),
	    ]);
	}
	
    }
    
    public function actionQuerys($q){
        $ezf_table = 'tb_data_1';
        $ezf_comp;
        if(isset(Yii::$app->session['sql_ezf_comp'])){
            $ezf_comp = Yii::$app->session['sql_ezf_comp'];
            $ezf_table = $ezf_comp['ezf_table'];
        }
        $sitecode = Yii::$app->user->identity->userProfile->sitecode;
        
	$sql = "SELECT id, target, concat(IFNULL(`hsitecode`, ''), ' ', IFNULL(`hptcode`, ''), ' ', IFNULL(`title`, ''), IFNULL(`name`, ''), ' ', IFNULL(`surname`, '')) as fullname FROM `$ezf_table` WHERE hsitecode =:sitecode AND concat(`name`, ' ', `surname`) LIKE :q OR `cid` LIKE :q LIMIT 0,10";
        Yii::$app->response->format = Response::FORMAT_JSON;
        $data = Yii::$app->db->createCommand($sql, [':sitecode'=>$sitecode, ':q'=>"%$q%"])->queryAll();

        $json = array();
        if($data){
            foreach($data as $value){
                $json[] = [
                    'id'=>$value['id'],
                    'label'=>$value["fullname"],
                    'ezf_id'=>$ezf_comp['ezf_id'],
                    'comp_id_target'=>$ezf_comp['comp_id'],
                    'target'=>$value["target"],
                    ];
            }
        }
        
        return $json;
    }
    
    public function actionDelete($id)
    {
	if (Yii::$app->getRequest()->isAjax) {
	    Yii::$app->response->format = Response::FORMAT_JSON;
            $model = \backend\modules\inv\models\InvPerson::find()->where('inv_id=:id', [':id'=>$id])->one();
	    if ($model->delete()) {
		$result = [
		    'status' => 'success',
		    'action' => 'update',
		    'message' => SDHtml::getMsgSuccess() . Yii::t('app', 'Deleted completed.'),
		    'data' => $id,
		];
		return $result;
	    } else {
		$result = [
		    'status' => 'error',
		    'message' => SDHtml::getMsgError() . Yii::t('app', 'Can not delete the data.'),
		    'data' => $id,
		];
		return $result;
	    }
	} else {
	    throw new \yii\web\NotFoundHttpException('Invalid request. Please do not repeat this request again.');
	}
    }
    
    public function actionEzformEmrAll() {
	    $ezf_id = isset($_GET['ezf_id'])?$_GET['ezf_id']:0;
	    $dataid = isset($_GET['dataid'])?$_GET['dataid']:NULL;
	    $target = isset($_GET['target'])?$_GET['target']:NULL;
	    $comp_id_target = isset($_GET['comp_id_target'])?$_GET['comp_id_target']:NULL;
	    $addBtn = isset($_GET['addBtn'])?$_GET['addBtn']:1;
	    try {
                Yii::$app->session['emr_url'] = \yii\helpers\Url::current();
                
		//$dataProvidertarget = \backend\models\InputDataSearch::searchTarget($ezf_id, $target, 'draft', $comp_id_target);
                $emr_form = isset(Yii::$app->session['emr_form'])?Yii::$app->session['emr_form']:[];
                
                $dataProvidertarget = \backend\models\InputDataSearch::searchEMRCustom($target, $emr_form);
		
		$modelEzform = Ezform::find()
		->where('ezform.ezf_id=:ezf_id', [':ezf_id'=>$ezf_id])
		->one();
		
		$ezform_comp = Yii::$app->db->createCommand("SELECT * FROM ezform_component WHERE comp_id=:comp_id;", [':comp_id'=>$comp_id_target])->queryOne();

		$dataComponent = [];
		if(isset($comp_id_target)){
		    if ($ezform_comp['special'] == 1) {
			//หาเป้าหมายพิเศษประเภทที่ 1 (CASCAP) (Thaipalliative) (Ageing)
			
			$dataComponent = InvFunc::specialTarget1($ezform_comp);
			
			//$dataProvidertarget = \backend\models\InputDataSearch::searchEMR($target);
		    } else {
			//หาแบบปกติ
			$dataComponent = InvFunc::findTargetComponent($ezform_comp);
			
		    }
		} else {
		    //target skip
		    $dataComponent['sqlSearch'] = '';
		    $dataComponent['tagMultiple'] = FALSE;
		    $dataComponent['ezf_table_comp'] = '';
		    $dataComponent['arr_comp_desc_field_name'] = '';
		}
		
		if(isset($_GET['page'])){
                    return $this->renderPartial('_ezform_emr', [
		    'dataProvidertarget'=>$dataProvidertarget,
		    'ezf_id'=>$ezf_id,
		    'dataid'=>$dataid,
		    'target'=>$target,
                        'addBtn'=>$addBtn,
                        'emrpage'=>2,
		    'comp_id_target'=>$comp_id_target,
		    'dataComponent'=>$dataComponent,
		    'modelEzform' =>$modelEzform,
		]);
                } else {
                    return $this->renderAjax('_ezform_emr', [
		    'dataProvidertarget'=>$dataProvidertarget,
		    'ezf_id'=>$ezf_id,
		    'dataid'=>$dataid,
		    'target'=>$target,
                        'addBtn'=>$addBtn,
                        'emrpage'=>2,
		    'comp_id_target'=>$comp_id_target,
		    'dataComponent'=>$dataComponent,
		    'modelEzform' =>$modelEzform,
		]);
                }
		

	    } catch (\yii\db\Exception $e) {
		
	    }
	    
    }
}
