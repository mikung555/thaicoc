<?php

namespace backend\modules\inv\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use backend\modules\inv\classes\InvQuery;

/**
 * OvPersonController implements the CRUD actions for OvPerson model.
 */
class InvFixController extends Controller
{
    public function behaviors()
    {
        return [
	    'access' => [
		'class' => AccessControl::className(),
		'rules' => [
		    [
			'allow' => true,
			'actions' => ['index', 'view'], 
			'roles' => ['?', '@'],
		    ],
		    [
			'allow' => true,
			'actions' => ['view', 'create', 'update', 'delete', 'deletes', 'import-all', 'import-views', 'ovfilter'], 
			'roles' => ['@'],
		    ],
		],
	    ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public function beforeAction($action) {
	if (parent::beforeAction($action)) {
	    if (in_array($action->id, array('create', 'update'))) {
		
	    }
	    return true;
	} else {
	    return false;
	}
    }
    
    public function actionIndex($module)
    {
	$module = isset($_GET['module'])?$_GET['module']:0;
	$sub_module = isset($_GET['sub_module'])?$_GET['sub_module']:0;
        
	$sitecode = Yii::$app->user->identity->userProfile->sitecode;
	$userId = Yii::$app->user->id;
	
        $show_module = $sub_module>0?$sub_module:$module;
        
	$comp;
	$inv_main;
	if($show_module>0){
	    $inv_main = InvQuery::getModule($show_module, $userId);
	    if($inv_main){
		$comp = $inv_main['ezf_id'];
	    } else {
		if($sub_module>0){
                    Yii::$app->getSession()->setFlash('alert', [
                        'body'=> 'ไม่มีสิทธ์ใช้โมดูลนี้',
                        'options'=>['class'=>'alert-danger']
                    ]);

                    return $this->redirect(['/inv/inv-person/index', 'module'=>$module]);
                }
                
		Yii::$app->getSession()->setFlash('alert', [
		    'body'=> 'ไม่มีสิทธ์ใช้โมดูลนี้',
		    'options'=>['class'=>'alert-danger']
		]);

		return $this->redirect(['/tccbots/my-module']);
	    }
	} else {
	    Yii::$app->getSession()->setFlash('alert', [
		'body'=> 'ไม่พบโมดูล',
		'options'=>['class'=>'alert-danger']
	    ]);

	    return $this->redirect(['/tccbots/my-module']);
	}
	
	$siteconfig = \common\models\SiteConfig::find()->One();
	
	$sqlHospital = "SELECT `hcode`,`name` FROM all_hospital_thai WHERE hcode='".($siteCode)."' ";
	//$dataHospital = Yii::$app->db->createCommand($sqlHospital)->queryOne();

	//Yii::$app->db->createCommand('CALL spOverview("'.$siteCode.'",0)')->execute();

	$table = $this->renderAjax('_ajaxOverviewReport', [
	    'siteconfig' => $siteconfig,
	    'inv_main' => $inv_main,
	    'comp' => $comp,
	]);

	return $this->render('index', [
	    'siteconfig' => $siteconfig,
	    'table'=>$table,
	    'debug'=>$debug,
	    'mysitecode'=>$siteCode,
	    'module' =>$module,
            'sub_module' =>$sub_module,
            'show_module'=>$show_module,
	    'inv_main' => $inv_main,
	    'comp' => $comp,
            
	]);
    }
    
    public function actionReportOverviewRefresh()
    {
	$module = isset($_GET['module'])?$_GET['module']:0;
	
	$userId = Yii::$app->user->id;
	
	$comp;
	$inv_main;
	if($module>0){
	    $inv_main = InvQuery::getModule($module, $userId);
	    if($inv_main){
		$comp = $inv_main['ezf_id'];
	    } else {
		Yii::$app->getSession()->setFlash('alert', [
		    'body'=> 'ไม่พบโมดูล',
		    'options'=>['class'=>'alert-danger']
		]);

		return $this->redirect(['/tccbots/my-module']);
	    }
	} else {
	    Yii::$app->getSession()->setFlash('alert', [
		'body'=> 'ไม่พบโมดูล',
		'options'=>['class'=>'alert-danger']
	    ]);

	    return $this->redirect(['/tccbots/my-module']);
	}
	
	$siteconfig = \common\models\SiteConfig::find()->One();
	
	$sitecode = Yii::$app->user->identity->userProfile->sitecode;
	
	\backend\modules\inv\classes\InvFunc::createReportOverall($inv_main, $sitecode);
	
        return $this->renderAjax('_ajaxOverviewReport', [
            'siteconfig' => $siteconfig,
	    'inv_main' => $inv_main,
	    'comp' => $comp,
        ]);


    }
    
    public function actionReport($module)
    {
	$module = isset($_GET['module'])?$_GET['module']:0;
        $sub_module = isset($_GET['sub_module'])?$_GET['sub_module']:0;
        
	$report = isset($_GET['report'])?$_GET['report']:0;
	Yii::$app->session['pjax_reload'] = 'inv-person-report-pjax';
        
	$sitecode = Yii::$app->user->identity->userProfile->sitecode;
	$userId = Yii::$app->user->id;
	
        $show_module = $sub_module>0?$sub_module:$module;
        
	$comp;
	$inv_main;
	if($show_module>0){
	    $inv_main = InvQuery::getModule($show_module, $userId);
	    if($inv_main){
		$comp = $inv_main['ezf_id'];
	    } else {
		if($sub_module>0){
                    Yii::$app->getSession()->setFlash('alert', [
                        'body'=> 'ไม่มีสิทธ์ใช้โมดูลนี้',
                        'options'=>['class'=>'alert-danger']
                    ]);

                    return $this->redirect(['/inv/inv-person/index', 'module'=>$module]);
                }
                
		Yii::$app->getSession()->setFlash('alert', [
		    'body'=> 'ไม่มีสิทธ์ใช้โมดูลนี้',
		    'options'=>['class'=>'alert-danger']
		]);

		return $this->redirect(['/tccbots/my-module']);
	    }
	} else {
	    Yii::$app->getSession()->setFlash('alert', [
		'body'=> 'ไม่พบโมดูล',
		'options'=>['class'=>'alert-danger']
	    ]);

	    return $this->redirect(['/tccbots/my-module']);
	}
	
	return $this->render('report', [
	    'module' =>$module,
            'sub_module' =>$sub_module,
	    'inv_main' => $inv_main,
	    'comp' => $comp,
	    'report'=>$report,
	    
	]);
    }
    
    public function actionView()
    {
	if (Yii::$app->getRequest()->isAjax) {
	    $gid = isset($_GET['gid'])?$_GET['gid']:0;
	    $jtable = isset($_GET['jtable'])?$_GET['jtable']:'';
	    $jselect = isset($_GET['jselect'])?$_GET['jselect']:'';
	    $jvalue = isset($_GET['jvalue'])?$_GET['jvalue']:'';
	    $jname = isset($_GET['jname'])?$_GET['jname']:'';
	    $rurl = isset($_GET['rurl'])?$_GET['rurl']:'';
	    $ezf_id = isset($_GET['ezf_id'])?$_GET['ezf_id']:'';
	    
	    $sitecode = Yii::$app->user->identity->userProfile->sitecode;
	    $userId = Yii::$app->user->id;
	
	    $comp;
	    $inv_main = InvQuery::getModule($gid, $userId);
	    if($inv_main){
		$comp = $inv_main['ezf_id'];
	    } else {
		Yii::$app->getSession()->setFlash('alert', [
		    'body'=> 'ไม่พบโมดูล',
		    'options'=>['class'=>'alert-danger']
		]);

		return $this->redirect(['/inv/inv-fix/report', 'module'=>$gid]);
	    }
	    
	    Yii::$app->session['sql_fields'] = false;
	    Yii::$app->session['sql_main_fields'] = $inv_main;
	    
	    $searchModel = new \backend\modules\inv\models\TbdataSearch();
	    $dataProvider = $searchModel->searchView(Yii::$app->request->queryParams, $jtable, $jselect, $jvalue);
	    $dataProvider->pagination->pageSize = 100;
	    
	    return $this->renderAjax('_view', [
		'dataProvider'=>$dataProvider,
		'gid'=>$gid,
		'jtable'=>$jtable,
		'jselect'=>$jselect,
		'jvalue'=>$jvalue,
		'jname'=>$jname,
		'rurl'=>$rurl,
		'ezf_id'=>$ezf_id,
	    ]);
	} 
    }
}
