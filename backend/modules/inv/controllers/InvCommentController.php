<?php
namespace backend\modules\inv\controllers;
use yii;
//nut
use backend\modules\comments\models\Cmd;
use backend\modules\comments\models\CmdSearch;

class InvCommentController extends yii\web\Controller {

    //nut
    public $table = "inv_comment";
    public $table_user_profile = "user_profile";
    public $gid = null;

    public function actionIndex() {
        
    }

    public function actionSessionCount() {

        $count = Yii::$app->db->createCommand("select count(*) from inv_comment")->queryScalar();
        Yii::$app->session['count'] = $count;
    }

    public function actionShowRateStart() {

        $count = Yii::$app->db->createCommand("select count(*) from {$this->table}")->queryScalar();
        echo Yii::$app->session['count_s'] = $count;
    }

    public function actionCheckModuleUserAdd() {
        $gid = $_POST['gid'];

        //echo $gids;exit(); 
        $add = Yii::$app->db->createCommand("SELECT * FROM inv_favorite WHERE gid = '" . $gid . "' AND user_id ='" . Yii::$app->user->identity->id . "' ")->queryAll();
        //$add= InvFavorite::find()->where(['gid'=>$gid])->all();
        // echo count($add);exit();
        if (!empty($add) && !is_nan($add)) {
            echo 1; //มีข้อมูลการรับอ app 
        } else {
            $add1 = \backend\modules\inv\models\InvGen::find()->where(['gsystem' => 1, 'gid' => $gid])->one();
            $add1 = Yii::$app->db->createCommand("select * from inv_gen where gid = '" . $gid . "' and gsystem=1")->queryOne();
            if (!empty($add1)) {
                echo 2;
                exit();
            }
            echo 0;
        }
    }

//ตรวจสอบเริ่มต้นว่ารับ app แล้ว หรือยัง หรือ app นั้นมีค่า gsystem =1

    public function actionCreate() { //เพิ่มข้อมูล และ แก้ไข 
        $model = new Cmd();
        // print_r($_POST);exit();
        if (!empty($_POST)) {

            $date = Date('Y-m-d H:i:s');
            $vote = trim($_POST["vote"]);
            $message = trim($_POST["message"]);
            $message_alert = [];

            if ($model->checkValidations($message) == true) {
                $message_alert = [
                    'status' => 404,
                    'message' => 'คุณไม่สามารถกรอก script ได้นะครับ'
                ];
                echo json_encode($message_alert);
                exit();
            }

            $check = Yii::$app->db->createCommand("SELECT count(*) FROM {$this->table} WHERE user_id='" . Yii::$app->user->identity->id . "' AND gid = '" . $_POST['gids'] . "' ")->queryScalar();
            if ($check == 0) {
                $columns = array(
                    'gid' => $_POST['gids'],
                    'user_id' => "" . Yii::$app->user->identity->id,
                    'vote' => $vote,
                    'message' => $message,
                    'created_by' => $date,
                    'created_at' => $date,
                    'updated_by' => $date,
                    'updated_at' => $date,
                );
                $model->save();
                $cmd = Yii::$app->db->createCommand();
                $table = $this->table;
                $cmd->insert($table, $columns);

                if ($cmd->execute()) {
                    $message_alert = [
                        'status' => 200,
                        'message' => 'บันทึกข้อมูลแล้วครับ'
                    ];
                    //unset(Yii::$app->session['gid']);
                    echo json_encode($message_alert);

                    exit();
                }
            } else {
                $sql = "UPDATE {$this->table} SET vote='" . $vote . "',message='" . $message . "',updated_at='" . $date . "' WHERE user_id='" . Yii::$app->user->identity->id . "'";
                $result = Yii::$app->db->createCommand($sql);

                if ($result->execute()) {
                    $message_alert = [
                        'status' => 200,
                        'message' => 'บันทึกข้อมูลแล้วครับ'
                    ];
                    echo json_encode($message_alert);
                    exit();
                }
            }
        }//end Create
    }

    public function actionShowform() {//แสดง form 
        //echo $_POST['gid'];exit();
        //echo 'hello';exit();
        $sql = "
             SELECT  concat(avatar_base_url,'/',avatar_path) as paths FROM user_profile 
             WHERE user_id = " . Yii::$app->user->identity->id . "
           
             LIMIT 1        
        ";
        $img = Yii::$app->db->createCommand($sql)->queryOne();
        //print_r($img);exit();


        $userId = Yii::$app->user->id;
        Yii::$app->session['gid'] = $_POST['gid'];
        $model = \backend\modules\inv\classes\InvQuery::getModule($gid, $userId);

        return $this->renderAjax("/cmd/create", [
                    'img' => $img,
                    'model' => $model,
                    'gid'=>$_POST['gid']
        ]);
    }

//end Showform

    public function actionRealtime() {//เรียวทาม
        $sql = "SELECT * FROM {$this->table} WHERE `real` = 1";
        $model = Yii::$app->db->createCommand($sql)->queryAll();
        // \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $count = Count($model);
        if ($count == 0) {
            $count = '0';
        }
        echo $count;
    }

//Realtime 

    public function actionSaverealtime() {
        $sql = "UPDATE {$this->table} SET `real` = 0";
        $model = Yii::$app->db->createCommand($sql)->execute();
    }

//Saverealtime

    public function actionShowcmd() {
        //$cmd = Cmd::find()->all();
        try {

            $endPage = 5;
            if (!empty($_POST)) {
                $endPage = $_POST["endPage"];
            }
            $sql = "SELECT * FROM {$this->table} ORDER BY id DESC LIMIT 0,$endPage";
            $cmd = Yii::$app->db->createCommand($sql)->queryAll();
        } catch (Exception $e) {
            echo $e->getMessage();
        }

        return $this->render('cmdshow', [
                    'cmd' => $cmd
        ]);
    }

//end Showcomment

    public function actionShowInvUser() {//แสดง comment ของตัวเอง
        $uid = Yii::$app->user->identity->id;
        //echo $uid;exit();
        $gid = $_POST['gid'];
        // echo $gid;exit();
        //echo $uid;exit(); 
        $html = "";

        $sql = "
            SELECT us.username,concat(u.firstname,'  ',u.lastname) as name, 
            concat(u.avatar_base_url,'/',u.avatar_path) as images, i.*
            , u.avatar_base_url,
            u.avatar_path
            FROM inv_comment as i
            INNER JOIN  user_profile as u
            on u.user_id = i.user_id
            INNER JOIN user as us 
            ON i.user_id = us.id
             
            WHERE i.gid = '" . $_POST['gid'] . "' AND i.user_id='" . $uid . "' 
                
        ";


        $cmd = Yii::$app->db->createCommand($sql)->queryAll();

        if (empty($cmd)) {

            exit();
        }
        $arr = array();
        foreach ($cmd as $c) {
            $arr[] = [
                'id' => $c['id'],
                'vote' => $c['vote'],
                'message' => $c['message'],
                'updated_at' => $c['updated_at'],
                'user_id' => $c['user_id'],
                'name' => $c['name'],
                'images' => $c['images'],
                'avatar_path' => $c['avatar_path'],
                'avatar_base_url' => $c['avatar_base_url']
            ];
        }
        $cmds = new Cmd();



        foreach ($arr as $a) {

            $html .= "";
            $html .= "<h5><b>ความเห็นของฉัน</b></h5>";
            $html .= "<div class=\"media\" style='background: #ffffff; padding: 5px; border-radius: 5px;border: 1px solid rgba(9, 9, 35, 0.08);'>";

            $html .= "<div class=\"media-left\">";

            if (!empty($a['avatar_base_url'])) {
                $html .= "<img src='" . $a['avatar_base_url'] . '/' . $a['avatar_path'] . "' class=\"media-object img img-circle\" style=\"width:60px\">";
            } else {
                $html .= "<img src=\"/img/anonymous.jpg\" class=\"media-object img img-circle\" style=\"width:60px\">";
            }


            $html .= "</div>";
            $html .= "<div class=\"media-body\">";
            $html .= "<h5 class=\"media-heading\" ><b>" . $a['name'] . "</b></h5>";

            $html .= "<p>";
            $vote = $a['vote'];
            if (empty($vote)) {
                $vote = 0;
            }
            $vote_arr = [1, 2, 3, 4, 5];
            $voteEnd = 5;
            foreach ($vote_arr as $v) {
                if ($vote == $v) {
                    for ($i = 0; $i < $vote; $i++) {
                        $html .= '<i class="fa fa-star" style="    color: #fec900;"></i>';
                    }

                    $x = 5 - $vote;
                    for ($j = 0; $j < $x; $j++) {
                        $html .= '<i class="fa fa-star-o" style="    color: #fec900;"></i>';
                    }
                }
            }
            $html .= "<span class='pull-right'>"
                    . "<button onclick='return DeleteComment()' id='btnDeleteComment'  class='btn btn-default btn-sm'><i class='glyphicon glyphicon-trash'></i></button>"
                    . "<button onclick='return showFormEdit()' id='btnEditComment' class='btn btn-default btn-sm'><i class='glyphicon glyphicon-pencil'></i></button>"
                    . "</span>";
            $html .= '</p>';

            $html .= "<div style='width: 400px; */
    /* word-wrap: break-word; */
    /* height: 357px; */
    /* background: blue; */
    /* overflow: hidden; */
    display: block;
    width: 450px;
    
    /* border: 1px solid #09C; */
    /* background-color: #CFC; */
    word-wrap: break-word;
    font-size: 12px;
    white-space: pre-wrap;
    margin-top: -10px;
    padding: 3px 1px 3px 1px;'>" . $a['message'] . "</div>";

            $html .= "</div>";

            $html .= "</div>";
        }
        echo $html;
    }

    public function actionShowInv() {//แสดง comment
        $html = "";
        $html .= "<br>";
        $pageSize = 10;
        $gid = $_POST['gid'];
       
        //echo $gid;exit();
        if (!empty($_POST['pageSize'])) {
            @$pageSize = $_POST['pageSize'];
        }
        $uid = Yii::$app->user->identity->id;

        $sql = "
            SELECT us.username,concat(u.firstname,'  ',u.lastname) as name, 
            concat(u.avatar_base_url,'/',u.avatar_path) as images, i.*,
            u.avatar_base_url,
            u.avatar_path
            FROM inv_comment as i
            INNER JOIN  user_profile as u
            on u.user_id = i.user_id
            INNER JOIN user as us 
            ON i.user_id = us.id
            WHERE i.user_id != {$uid} AND gid = '".$gid."'            
            ORDER BY id DESC LIMIT 0,$pageSize
                
        ";


        $cmd = Yii::$app->db->createCommand($sql)->queryAll();
        //print_r($cmd);exit();
      
        if (empty($cmd)) {
            echo "<span style='font-size: 18px;'></span>";
            exit();
        }
        $arr = array();
        foreach ($cmd as $c) {
            $arr[] = [
                'id' => $c['id'],
                'vote' => $c['vote'],
                'message' => $c['message'],
                'updated_at' => $c['updated_at'],
                'user_id' => $c['user_id'],
                'name' => $c['name'],
                'images' => $c['images'],
                'avatar_path' => $c['avatar_path'],
                'avatar_base_url' => $c['avatar_base_url']
            ];
        }
          
        $cmds = new Cmd();
        foreach ($arr as $a) {
            // $html .= "<div class='col-md-6'>"; 

            $html .= "<div class=\"media\" style='    height: suto;background: #ffffff; padding: 5px; border-radius: 5px;border: 1px solid rgba(9, 9, 35, 0.08);'>";

            $html .= "<div class=\"media-left\">";

            if (!empty($a['avatar_base_url'])) {
                $html .= "<img src='" . $a['avatar_base_url'] . '/' . $a['avatar_path'] . "' class=\"media-object img img-circle\" style=\"width:60px\">";
            } else {
                $html .= "<img src=\"/img/anonymous.jpg\" class=\"media-object img img-circle\" style=\"width:60px\">";
            }


            $html .= "</div>";
            $html .= "<div class=\"media-body\">";
            $html .= "<h5 class=\"media-heading\" style='    font-size: 12px;'><b>" . $a['name'] . "</b></h5>";

            $html .= "<p>";
            $vote = $a['vote'];
            if (empty($vote)) {
                $vote = 0;
            }
            $vote_arr = [1, 2, 3, 4, 5];
            $voteEnd = 5;
            foreach ($vote_arr as $v) {
                if ($vote == $v) {
                    for ($i = 0; $i < $vote; $i++) {
                        $html .= '<i class="fa fa-star" style="color:#fec900"></i>';
                    }

                    $x = 5 - $vote;
                    for ($j = 0; $j < $x; $j++) {
                        $html .= '<i class="fa fa-star-o"></i>';
                    }
                }
            }
            $html .= "<span class='pull-right' style='color: #aaa; display: inline-block;font-size: 11px;margin-right: 5%; margin-left: 2px;
    white-space: nowrap;'>" . \common\lib\sdii\components\utils\SDdate::differenceTimer($a['updated_at']) . "</span>";
            $html .= '</p>';

            $html .= "<div style='width: 400px; */
    /* word-wrap: break-word; */
    /* height: 357px; */
    /* background: blue; */
    /* overflow: hidden; */
    display: block;
    width: 450px;
    
    /* border: 1px solid #09C; */
    /* background-color: #CFC; */
    word-wrap: break-word;
    font-size: 12px;
    white-space: pre-wrap;
    margin-top: -10px;
    padding: 3px 1px 3px 1px;'>" . $a['message'] . "</div>";
            $html .= "</div>";

            $html .= "</div>";
            // $html .= "</div>";
        }

        echo $html;
    }

///nut
    public function actionCheckuser() {
        $user_id = Yii::$app->user->identity->id;
        $user_cmd = Cmd::find()->where(["user_id" => $user_id])->one();
        if (!empty($user_cmd)) {
            echo $user_id . " ได้แสดงความคิดเห็นแล้วครับ";
        }
        //echo $user_id;
    }

    public function actionCountOne() {//แสดงการเข้าชมดาว บนสุด

        $сount = Yii::$app->db->createCommand("select count(*) from {$this->table} where gid = '" . $_POST['gid'] . "' ")->queryScalar();
        $sum = Yii::$app->db->createCommand("select sum(vote) as sums from {$this->table} where gid = '" . $_POST['gid'] . "' ")->queryAll();
        $sums = (int) $sum[0]['sums'];
        $counts = round($sums /= $сount, 1);
        $strCounts = explode(".", "" . $counts);

        $html = "";

        //$html .= "<span class=\"rating-num\">".number_format($counts,1)."</span></span>";

        $html .= "<div class=\"rating-starss\">";
        if(empty($сount) || is_nan($сount)){
            for($i=1;$i<=5; $i++){
                $html .= "<apan><i class='fa fa-star-o' style=\"color: #fec900;font-size:14px;\"></i></span>";
            }
        }
        
        // $arr = [1,2,3,4,5];  
        if ($strCounts[1] == 0 || empty($strCounts[1])) {
            $strCounts[1] = 1;
            
        }

        for ($i = 1; $i <= $counts; $i++) {
            if ($i == $strCounts[0]) {
                for ($j = 0; $j < $strCounts[0]; $j++) {
                    $html .= "<apan><i class='fa fa-star' style=\"color: #fec900;font-size:14px;\"></i></span>";
                }//end for 

                $number = 5 - $strCounts[0];

                for ($j = 0; $j < $number; $j++) {
                    if ($strCounts[1] >= 5) {
                        $html .= "<i class=\"fa fa-star-half-o\" style=\"color: #fec900;font-size:14px;\"></i>";
                        $strCounts[1] = 1;
                    } else {

                        $html .= "<apan><i class='fa fa-star-o' style=\"color: #fec900;font-size:14px;\"></i></span>";
                    } //end if
                }//end for
            }//end for 
        }
        $html .= "</div>";
        $html .= "<div class=\"rating-users\">";
        //$html .= "<i class=\"icon-user\"></i> ".$сount ." คน";
        $html .= "</div>";
        echo $html;
    }

//Counts 

    public function actionCounts() { //แสดงการเข้าชมดาว 
        $gid = $_POST['gid'];
        $сount = Yii::$app->db->createCommand("select count(*) from {$this->table} where gid='" . $gid . "' ")->queryScalar();
        $sum = Yii::$app->db->createCommand("select sum(vote) as sums from {$this->table} where gid='" . $gid . "' ")->queryAll();
        $sums = (int) $sum[0]['sums'];
        $counts = round($sums /= $сount, 1);
        if (is_nan($counts)) {
            $counts = 0;
        }
//        if(!isset($counts)){
//            $counts=0;
//        }
        $strCounts = explode(".", "" . $counts);

        $html = "";

        $html .= "<span class=\"rating-num\">" . number_format($counts, 1) . "</span></span>";

        $html .= "<div class=\"rating-stars\">";
        // $arr = [1,2,3,4,5];  
        if ($strCounts[1] == 0 || empty($strCounts[1])) {
            $strCounts[1] = 1;
        }

        for ($i = 1; $i <= $counts; $i++) {
            if ($i == $strCounts[0]) {
                for ($j = 0; $j < $strCounts[0]; $j++) {
                    $html .= "<apan><i class='fa fa-star' style=\"color: #fec900;font-size:14px;\"></i></span>";
                }//end for 

                $number = 5 - $strCounts[0];

                for ($j = 0; $j < $number; $j++) {
                    if ($strCounts[1] >= 5) {
                        $html .= "<i class=\"fa fa-star-half-o\" style=\"color: #fec900;font-size:14px;\"></i>";
                        $strCounts[1] = 1;
                    } else {

                        $html .= "<apan><i class='fa fa-star-o' style=\"color: #fec900;font-size:14px;\"></i></span>";
                    } //end if
                }//end for
            }//end for 
        }
        $html .= "</div>";
        $html .= "<div class=\"rating-users\">";
        $html .= "<i class=\"icon-user\"></i> " . $сount . " คน";
        $html .= "</div>";
        echo $html;
    }

//Counts  

    public function actionCountInv() {
        $сount = Yii::$app->db->createCommand("select count(*) from {$this->table}")->queryScalar();
        echo $сount;
    }

    public function actionCheckModeul_c() {
        $gid = $_POST['gid'];
        echo $gid;
        exit();
        $sql = "SELECT gsystem FROM inv_gen WHERE gid='" . $gid . "' AND gsystem=1";
        $cmd = Yii::$app->db->createCommand($sql)->queryAll();

        if (!empty($cmd)) {
            echo 1;
        } else {
            echo 0;
        }
    }

    public function actionCheckModule() {
        $gid = $_POST['gid'];

        $sql = " 
                SELECT i.gid 
                FROM inv_favorite as i
                WHERE i.gid = '" . $gid . "' 
           ";
        $cmd = Yii::$app->db->createCommand($sql)->queryOne();
        if (!empty($cmd)) {
            echo 1;
        } else {
            $cmd = Yii::$app->db->createCommand("select gid from inv_gen where gid='" . $gid . "' or gsystem=1")->queryOne();
            if (!empty($cmd)) {
                echo 2;
                exit();
            }
            echo 0;
        }
    }

    public function actionDeletes() {
        $uid = Yii::$app->user->identity->id;
        $sql = "DELETE FROM {$this->table} WHERE user_id = {$uid} ";
        $del = Yii::$app->db->createCommand($sql);
        if ($del->execute()) {
            echo "success";
        }
        exit();
    }
    
    public function actionUpdateRate(){
         $votes = 0;  
         $gid = $_POST["gid"];
        //nut
        
         $sqli="SELECT 
	        (select count(vote) from inv_comment as i5 where vote =5 and gid = '".$gid."') as v5,
	        (select count(vote) from inv_comment as i4 where vote =4 and gid = '".$gid."') as v4,
	        (select count(vote) from inv_comment as i3 where vote =3 and gid = '".$gid."') as v3,
	        (select count(vote) from inv_comment as i2 where vote =2 and gid = '".$gid."') as v2,
	        (select count(vote) from inv_comment as i1 where vote =1 and gid = '".$gid."') as v1
	        FROM inv_comment  limit 1";
       $vote = Yii::$app->db->createCommand($sqli)->queryAll();
       
       
       if(is_nan($vote) || empty($vote)){
           $vote2= [0,0,0,0,0];
           //print_r($vote2);
       }else{
            $vote2= [
               'v5' =>  $vote[0]['v5'],
               'v4' =>  $vote[0]['v4'],
               'v3' =>  $vote[0]['v3'],
               'v2' =>  $vote[0]['v2'],
               'v1' =>  $vote[0]['v1'],
              
           ];
            
       }
       
        
        $count = Yii::$app->db->createCommand("select count(*) from {$this->table}")->queryScalar();
        
        echo json_encode($vote2);
	 
        
    }//UpdateReate

//actionCreate ---> Create
}
