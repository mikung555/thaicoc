<?php

namespace backend\modules\inv\controllers;

use Yii;
use backend\modules\inv\models\InvGen;
use backend\modules\inv\models\InvGenSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\Response;
use appxq\sdii\helpers\SDHtml;

/**
 * InvGenController implements the CRUD actions for InvGen model.
 */
class InvGenController extends Controller
{
    public function behaviors()
    {
        return [
	    'access' => [
		'class' => AccessControl::className(),
		'rules' => [
		    [
			'allow' => true,
			'actions' => ['index', 'view'], 
			'roles' => ['?', '@'],
		    ],
		    [
			'allow' => true,
			'actions' => ['view', 'create', 'update', 'delete', 'deletes'], 
			'roles' => ['@'],
		    ],
		],
	    ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public function beforeAction($action) {
	if (parent::beforeAction($action)) {
	    if (in_array($action->id, array('create', 'update'))) {
		
	    }
	    return true;
	} else {
	    return false;
	}
    }
    
    /**
     * Lists all InvGen models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new InvGenSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionAuth()
    {
	
	if(!Yii::$app->user->can('administrator')){
	    Yii::$app->getSession()->setFlash('alert', [
		'body'=> SDHtml::getMsgError() . Yii::t('app', 'คุณไม่มีสิทธิ์ใช้หน้านี้  '),
		'options'=>['class'=>'alert-danger'],
	    ]);

	}
        $searchModel = new InvGenSearch();
        $dataProvider = $searchModel->searchAuth(Yii::$app->request->queryParams);

        return $this->render('auth', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Deletes an existing InvGen model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
	if (Yii::$app->getRequest()->isAjax) {
	    Yii::$app->response->format = Response::FORMAT_JSON;
	    $model = $this->findModel($id);
	    $model->active = 0;
	    if ($model->save()) {
		$result = [
		    'status' => 'success',
		    'action' => 'update',
		    'message' => SDHtml::getMsgSuccess() . Yii::t('app', 'Deleted completed.'),
		    'data' => $id,
		];
		return $result;
	    } else {
		$result = [
		    'status' => 'error',
		    'message' => SDHtml::getMsgError() . Yii::t('app', 'Can not delete the data.'),
		    'data' => $id,
		];
		return $result;
	    }
	} else {
	    throw new NotFoundHttpException('Invalid request. Please do not repeat this request again.');
	}
    }
    
    public function actionManager($id)
    {
	Yii::$app->response->format = Response::FORMAT_JSON;
        
	$model = $this->findModel($id);
	$model->approved = $model->approved==1?0:1;
	
	if ($model->save()) {
	    
	    $result = [
		    'status' => 'success',
		    'action' => 'update',
		    'message' => '<strong><i class="glyphicon glyphicon-ok-sign"></i> Success!</strong> ' . Yii::t('user', 'Success.'),
		    'data' => $id,
	    ];
	    return $result;
        }
    }

    /**
     * Finds the InvGen model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return InvGen the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = InvGen::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
