<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\inv\models\InvFieldsSearch */
/* @var $form yii\bootstrap\ActiveForm */
?>

<div class="inv-fields-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
	'layout' => 'horizontal',
	'fieldConfig' => [
	    'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
	    'horizontalCssClasses' => [
		'label' => 'col-sm-2',
		'offset' => 'col-sm-offset-3',
		'wrapper' => 'col-sm-6',
		'error' => '',
		'hint' => '',
	    ],
	],
    ]); ?>

    <?= $form->field($model, 'if_id') ?>

    <?= $form->field($model, 'sub_id') ?>

    <?= $form->field($model, 'ezf_id') ?>

    <?= $form->field($model, 'ezf_name') ?>

    <?= $form->field($model, 'ezf_table') ?>

    <?php // echo $form->field($model, 'comp_id_target') ?>

    <?php // echo $form->field($model, 'result_field') ?>

    <?php // echo $form->field($model, 'sql_id_name') ?>

    <?php // echo $form->field($model, 'sql_result_name') ?>

    <?php // echo $form->field($model, 'value_options') ?>

    <?php // echo $form->field($model, 'header') ?>

    <?php // echo $form->field($model, 'width') ?>

    <div class="form-group">
	<div class="col-sm-offset-2 col-sm-6">
	    <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
	    <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
	</div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
