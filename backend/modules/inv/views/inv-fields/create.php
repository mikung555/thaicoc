<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\inv\models\InvFields */

$this->title = Yii::t('app', 'Create Inv Fields');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Inv Fields'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="inv-fields-create">

    <?= $this->render('_form', [
        'model' => $model,
	'modelEzform'=>$modelEzform
    ]) ?>

</div>
