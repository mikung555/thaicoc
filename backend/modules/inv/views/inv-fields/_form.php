<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use appxq\sdii\helpers\SDNoty;
use appxq\sdii\helpers\SDHtml;
use backend\modules\inv\classes\InvQuery;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model backend\modules\inv\models\InvFields */
/* @var $form yii\bootstrap\ActiveForm */
backend\assets\ColorAsset::register($this);
?>

<div class="inv-fields-form">

    <?php $form = ActiveForm::begin([
	'id'=>$model->formName(),
    ]); ?>

    <div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	<h4 class="modal-title" id="itemModalLabel">เพิ่มฟอร์มสำหรับกำกับงาน</h4>
    </div>

    <div class="modal-body">
	<?= $form->field($model, 'ezf_id')->dropDownList(\yii\helpers\ArrayHelper::map(InvQuery::getEzformByCom($model->comp_id_target), 'id', 'name')) ?>

	<div class="row">
	    <div class="col-md-6">
		<?= $form->field($model, 'header')->textInput(['maxlength' => true]) ?>
	    </div>
	    <div  class="col-md-3">
		<?= $form->field($model, 'width')->textInput() ?>
	    </div>
	    <div  class="col-md-3">
		<?= $form->field($model, 'report_width')->textInput() ?>
	    </div>
	</div>
	<?php
	$dataFields = ArrayHelper::map(InvQuery::getFields($model->ezf_id),'id','name');
	$dataSubForm = ArrayHelper::map(InvQuery::getEzformByCom($modelEzform->comp_id_target),'id','name');
	$dataSelect = array_merge($dataFields, $dataSubForm);
	
	\yii\helpers\VarDumper::dump($modelEzform,10,true);
	\yii\helpers\VarDumper::dump($dataSubForm,10,true);
	\yii\helpers\VarDumper::dump($dataSelect,10,true);
	?>
	<div class="row">
	    <div class="col-md-9">
		<?= $form->field($model, 'result_field')->dropDownList(['แสดงผล', 'แสดงผลตามเงื่อนไข'],[]) ?>
	    </div>
	    <div  class="col-md-3">
		<?= $form->field($model, 'report_show')->dropDownList(['Y'=>'Yes', 'N'=>'No']) ?>
	    </div>
	</div>
	<div class="row">
    <div class="col-md-3 "><label>Form</label></div>
    <div class="col-md-3 sdbox-col"><label>Field</label></div>
    <div class="col-md-2 sdbox-col"><label>Value</label></div>
    <div class="col-md-2 sdbox-col"><label>Condition</label></div>
    <div class="col-md-2 sdbox-col"><label></label></div>
</div>
	<?php
$options = appxq\sdii\utils\SDUtility::string2Array($model->value_options);
if(isset($options) && is_array($options)){
    $value = isset($options['value'])?$options['value']:[];
    $web = isset($options['web'])?$options['web']:[];
    $convert = isset($options['convert'])?$options['convert']:[];
    $color = isset($options['color'])?$options['color']:[];
    echo '<div class="items-fields">';
    foreach ($value as $key => $var) {
?>
    <div class="row" style="margin-bottom: 15px;">
	<div class="col-md-2 "><input type="text" class="form-control" name="options[value][]" value="<?=$var?>"></div>
	<div class="col-md-3 sdbox-col"><input type="text" class="form-control" name="options[web][]" value="<?=$web[$key]?>"></div>
	<div class="col-md-3 sdbox-col"><input type="text" class="form-control" name="options[convert][]" value="<?=$convert[$key]?>"></div>
	<div class="col-md-2 sdbox-col"><input type="text" class="form-control color" name="options[color][]" value="<?=$color[$key]?>"></div>
	<div class="col-md-2 sdbox-col"><button type="button" class="items-del btn btn-danger"><i class="glyphicon glyphicon-remove"></i></button></div>
    </div>
<?php
 }
 echo '</div>';
} else {
?>
<div class="items-fields">
    <div class="row" style="margin-bottom: 15px;">
	<div class="col-md-2 "><input type="text" class="form-control" name="options[value][]"></div>
	<div class="col-md-3 sdbox-col"><input type="text" class="form-control" name="options[web][]"></div>
	<div class="col-md-3 sdbox-col"><input type="text" class="form-control" name="options[convert][]"></div>
	<div class="col-md-2 sdbox-col"><input type="text" class="form-control color" name="options[color][]" value="#333333"></div>
	<div class="col-md-2 sdbox-col"><button type="button" class="items-del btn btn-danger"><i class="glyphicon glyphicon-remove"></i></button></div>
    </div>
</div>
<?php }?>
<div class="row add-fields">
    <div class="col-md-3 sdbox-col"><input type="text" class="form-control cform-input" disabled="disabled" name="options[result][condition][form][]" value="Form"></div>
    <div class="col-md-3 sdbox-col"><input type="text" class="form-control cfield-input" disabled="disabled" name="options[result][condition][field][]" value="Field"></div>
    <div class="col-md-2 sdbox-col"><input type="text" class="form-control cvalue-input" disabled="disabled" name="options[result][condition][value][]" value="Value"></div>
    <div class="col-md-2 sdbox-col"><input type="text" class="form-control ccond-input" disabled="disabled" name="options[result][condition][cond][]" value="Condition"></div>
    <div class="col-md-2 sdbox-col"><button type="button" data-url="<?=  Url::to(['/inv/inv-person/get-widget', 'view'=>'_widget_form_condition'])?>" class="items-add btn btn-success"><i class="glyphicon glyphicon-plus"></i></button></div>
</div>
	
	<?= $form->field($model, 'sql_id_name')->hiddenInput()->label(false) ?>
	<?= $form->field($model, 'sql_idsubmit_name')->hiddenInput()->label(false) ?>
	<?= $form->field($model, 'sql_result_name')->hiddenInput()->label(false) ?>
	
	<?= $form->field($model, 'sub_id')->hiddenInput()->label(false) ?>
	<?= $form->field($model, 'comp')->hiddenInput()->label(false) ?>
	
	<?= $form->field($model, 'ezf_name')->hiddenInput()->label(false) ?>

	<?= $form->field($model, 'ezf_table')->hiddenInput()->label(false) ?>

	<?= $form->field($model, 'comp_id_target')->hiddenInput()->label(false) ?>
	
	<?= $form->field($model, 'value_options')->hiddenInput()->label(false) ?>

    </div>
    <div class="modal-footer">
	<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	<?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php  
$onload = '';
if($model->isNewRecord){
    //getSubField($('#invfields-ezf_id').find('option:selected').val());
    $onload = "
$('#invfields-header').val($('#invfields-ezf_id').find('option:selected').text());";
}

$this->registerJs("

$onload

$('.color').colorPicker();

$('.items-fields').on('click', '.items-del', function(){
    $(this).parent().parent().remove();
});

$('.inv-fields-form').on('change', '#invfields-ezf_id', function(){
    $('#invfields-header').val($(this).find('option:selected').text());
    //getSubField($(this).find('option:selected').val());
});

//$('.add-fields').on('click', '.items-add', function(){
//    $('.items-fields').append('<div class=\"row\" style=\"margin-bottom: 15px;\"><div class=\"col-md-3 \"><input type=\"text\" class=\"form-control\" name=\"options[form][]\"></div><div class=\"col-md-3 sdbox-col\"><input type=\"text\" class=\"form-control\" name=\"options[field][]\"></div><div class=\"col-md-2 sdbox-col\"><input type=\"text\" class=\"form-control\" name=\"options[condition][]\"></div><div class=\"col-md-2 sdbox-col\"><input type=\"text\" class=\"form-control\" name=\"options[cond][]\" value=\"\"></div><div class=\"col-md-2 sdbox-col\"><button type=\"button\" class=\"items-del btn btn-danger\"><i class=\"glyphicon glyphicon-remove\"></i></button></div></div>');
//    $('.color').colorPicker();
//});

function getSubField(valField){
    if(valField === undefined || valField === null){
    
    } else {
	$.ajax({
	    method: 'POST',
	    url: '".Url::to(['/inv/inv-person/getfields'])."',
	    data: {id:valField},
	    dataType: 'HTML',
	    success: function(result, textStatus) {
		$('#invfields-result_field').html(result);
	    }
	});
    }
}

$('form#{$model->formName()}').on('beforeSubmit', function(e) {
    var \$form = $(this);
    $.post(
	\$form.attr('action'), //serialize Yii2 form
	\$form.serialize()
    ).done(function(result) {
	if(result.status == 'success') {
	    ". SDNoty::show('result.message', 'result.status') ."
	    if(result.action == 'create') {
		$(document).find('#modal-inv-fields').modal('hide');
		$.pjax.reload({container:'#inv-fields-grid-pjax'});
	    } else if(result.action == 'update') {
		$(document).find('#modal-inv-fields').modal('hide');
		$.pjax.reload({container:'#inv-fields-grid-pjax'});
	    }
	} else {
	    ". SDNoty::show('result.message', 'result.status') ."
	} 
    }).fail(function() {
	". SDNoty::show("'" . SDHtml::getMsgError() . "Server Error'", '"error"') ."
	console.log('server error');
    });
    return false;
});

$('.add-fields').on('click', '.items-add', function(){
    getWidget($(this).attr('data-url') , $(this).parent().parent().parent().find('.items-fields'), $(this).parent().parent().parent().parent().find('.items-fields option:selected').val());
});

function getWidget(url, appendId, id=0) {
    $.ajax({
	method: 'POST',
	url: url,
	data: {id:id, data:".\yii\helpers\Json::encode($dataFields).", data_sub:".\yii\helpers\Json::encode($dataSubForm).", data_select:".\yii\helpers\Json::encode($dataSelect)."},
	dataType: 'HTML',
	success: function(result, textStatus) {
	    $(appendId).append(result);
	}
    });
}

");?>