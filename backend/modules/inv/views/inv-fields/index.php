<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\helpers\Url;
use appxq\sdii\widgets\GridView;
use appxq\sdii\widgets\ModalForm;
use appxq\sdii\helpers\SDNoty;
use appxq\sdii\helpers\SDHtml;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\inv\models\InvFieldsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'เพิ่มฟอร์มสำหรับกำกับงาน');
$this->params['breadcrumbs'][] = ['label' => 'แสดงตัวอย่างใบกำกับงาน', 'url' => ['/inv/inv-person/index', 'module'=>$module]];
$this->params['breadcrumbs'][] = ['label' => 'จัดการโมดูล', 'url' => ['/inv/inv-gen/index']];
if($module==0){
    $this->params['breadcrumbs'][] = ['label' => 'Module Maker', 'url' => ['/inv/inv-person/main', 'ezf_id' => $comp, 'module' => $module]];
}
$this->params['breadcrumbs'][] = ['label' => 'ใบกำกับงาน', 'url' => ['/inv/inv-filter-sub/index', 'comp'=>$comp, 'module'=>$module]];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="inv-fields-index">

    <?php
    if($module>0){
	echo $this->render('/inv-person/_menu', ['module'=>$module, 'comp'=>$comp]);
    }
    ?>
    
    <?php  Pjax::begin(['id'=>'inv-fields-grid-pjax']);?>
    <?= GridView::widget([
	'id' => 'inv-fields-grid',
	'panelBtn' => Html::button(SDHtml::getBtnAdd(), ['data-url'=>Url::to(['inv-fields/create', 'comp'=>$comp, 'sub_id'=>$sub_id]), 'class' => 'btn btn-success btn-sm', 'id'=>'modal-addbtn-inv-fields']). ' ' .
		      Html::button(SDHtml::getBtnDelete(), ['data-url'=>Url::to(['inv-fields/deletes']), 'class' => 'btn btn-danger btn-sm', 'id'=>'modal-delbtn-inv-fields', 'disabled'=>true]),
	'dataProvider' => $dataProvider,
	//'filterModel' => $searchModel,
        'columns' => [
	    [
		'class' => 'yii\grid\CheckboxColumn',
		'checkboxOptions' => [
		    'class' => 'selectionInvFieldIds'
		],
		'headerOptions' => ['style'=>'text-align: center;'],
		'contentOptions' => ['style'=>'width:40px;text-align: center;'],
	    ],
	    [
		'class' => 'yii\grid\SerialColumn',
		'headerOptions' => ['style'=>'text-align: center;'],
		'contentOptions' => ['style'=>'width:60px;text-align: center;'],
	    ],
            'ezf_name',
	    'header',
	    'width',
	    'report_width',
	    'report_show',
	    //'result_field',
            //'ezf_table',
            // 'comp_id_target',
            // 'result_field',
            // 'sql_id_name',
            // 'sql_result_name',
            // 'value_options:ntext',
            // 'header',
             

	    [
		'class' => 'appxq\sdii\widgets\ActionColumn',
		'contentOptions' => ['style'=>'width:80px;text-align: center;'],
		'template' => '{update} {delete}',
	    ],
        ],
    ]); ?>
    <?php  Pjax::end();?>

</div>

<?=  ModalForm::widget([
    'id' => 'modal-inv-fields',
    //'size'=>'modal-lg',
]);
?>

<?php  $this->registerJs("
$('#inv-fields-grid-pjax').on('click', '#modal-addbtn-inv-fields', function() {
    modalInvField($(this).attr('data-url'));
});

$('#inv-fields-grid-pjax').on('click', '#modal-delbtn-inv-fields', function() {
    selectionInvFieldGrid($(this).attr('data-url'));
});

$('#inv-fields-grid-pjax').on('click', '.select-on-check-all', function() {
    window.setTimeout(function() {
	var key = $('#inv-fields-grid').yiiGridView('getSelectedRows');
	disabledInvFieldBtn(key.length);
    },100);
});

$('#inv-fields-grid-pjax').on('click', '.selectionInvFieldIds', function() {
    var key = $('input:checked[class=\"'+$(this).attr('class')+'\"]');
    disabledInvFieldBtn(key.length);
});

$('#inv-fields-grid-pjax').on('dblclick', 'tbody tr', function() {
    var id = $(this).attr('data-key');
    modalInvField('".Url::to(['inv-fields/update', 'id'=>''])."'+id);
});	

$('#inv-fields-grid-pjax').on('click', 'tbody tr td a', function() {
    var url = $(this).attr('href');
    var action = $(this).attr('data-action');

    if(action === 'update' || action === 'view') {
	modalInvField(url);
    } else if(action === 'delete') {
	yii.confirm('".Yii::t('app', 'Are you sure you want to delete this item?')."', function() {
	    $.post(
		url
	    ).done(function(result) {
		if(result.status == 'success') {
		    ". SDNoty::show('result.message', 'result.status') ."
		    $.pjax.reload({container:'#inv-fields-grid-pjax'});
		} else {
		    ". SDNoty::show('result.message', 'result.status') ."
		}
	    }).fail(function() {
		". SDNoty::show("'" . SDHtml::getMsgError() . "Server Error'", '"error"') ."
		console.log('server error');
	    });
	});
    }
    return false;
});

function disabledInvFieldBtn(num) {
    if(num>0) {
	$('#modal-delbtn-inv-fields').attr('disabled', false);
    } else {
	$('#modal-delbtn-inv-fields').attr('disabled', true);
    }
}

function selectionInvFieldGrid(url) {
    yii.confirm('".Yii::t('app', 'Are you sure you want to delete these items?')."', function() {
	$.ajax({
	    method: 'POST',
	    url: url,
	    data: $('.selectionInvFieldIds:checked[name=\"selection[]\"]').serialize(),
	    dataType: 'JSON',
	    success: function(result, textStatus) {
		if(result.status == 'success') {
		    ". SDNoty::show('result.message', 'result.status') ."
		    $.pjax.reload({container:'#inv-fields-grid-pjax'});
		} else {
		    ". SDNoty::show('result.message', 'result.status') ."
		}
	    }
	});
    });
}

function modalInvField(url) {
    $('#modal-inv-fields .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
    $('#modal-inv-fields').modal('show')
    .find('.modal-content')
    .load(url);
}

");?>