<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\inv\models\InvFields */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Inv Fields',
]) . ' ' . $model->if_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Inv Fields'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->if_id, 'url' => ['view', 'id' => $model->if_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="inv-fields-update">

    <?= $this->render('_form', [
        'model' => $model,
	'modelEzform'=>$modelEzform
    ]) ?>

</div>
