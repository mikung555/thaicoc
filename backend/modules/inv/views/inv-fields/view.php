<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\inv\models\InvFields */

$this->title = 'เพิ่มฟอร์มสำหรับกำกับงาน#'.$model->if_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Inv Fields'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="inv-fields-view">

    <div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	<h4 class="modal-title" id="itemModalLabel"><?= Html::encode($this->title) ?></h4>
    </div>
    <div class="modal-body">
        <?= DetailView::widget([
	    'model' => $model,
	    'attributes' => [
		'if_id',
		'sub_id',
		'ezf_id',
		'ezf_name',
		'ezf_table',
		'comp_id_target',
		'result_field',
		'sql_id_name',
		'sql_result_name',
		'value_options:ntext',
		'header',
		'width',
	    ],
	]) ?>
    </div>
</div>
