<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;

\backend\modules\comments\assets\CommentAsset::register($this); 
$arr=[1,2,3,4,5];
 
$this->title = Yii::t('app', 'Cmds');
$this->params['breadcrumbs'][] = $this->title;
?>


<div class="col-md-6">

    <div class="col-md-12">
      <div style='font-weight: bold;'>ความคิดเห็นของฉัน</div>
      <div id="showComment_User"></div>
    </div>

  <div class="clearfix"></div>
 <hr>

    <div class="col-md-6" style="font-weight: bold;"><b>ความเห็น</b></div>
    <div class="col-md-6">
        
        <?= Html::a('<i class="glyphicon glyphicon-pencil"></i> เขียนความคิดเห็น', ['#'], 
            [
            'class'=>'btn btn-default pull-right',
            'id'=>'btnComment' 
             
         ]) ?>
    </div>
    <div class="clearfix"></div>
    <div class=""><!-- กล่องใส่กราฟ -->
        <div id="container">
          <div id="inner">
            <div class="rating">
              <span id="Count_rate"></span>
            </div>

        <div class="histo">
             

            <?php for($i=0; $i<5;$i++): ?>
                <?php 
                
                $v=array('bar-five','bar-four','bar-three','bar-two','bar-one');//สี bar
                $num=array($vote[0][v5],$vote[0][v4],$vote[0][v3],$vote[0][v2],$vote[0][v1]); //จำนวน ความคิดเห็น 
                
                
                $n=5;//จำนวนตัวเลข bar
                
                ?>
                <div class="five histo-rate">
                    <span class="histo-star">
                      <i class="active icon-star"></i><?= $n-$i;?></span>
                      <span class="bar-block"> 
                       <span id="<?= $v[$i]; ?>" class="bar">
                         <span><?= number_format($num[$i])?></span>&nbsp;
                     </span> 
                 </span>
             </div>    
             <?php 
                $num[0] = ($num[0] / $count)*100;
                $num[1] = ($num[1] / $count)*100;
                $num[2] = ($num[2] / $count)*100;
                $num[3] = ($num[3] / $count)*100;
                $num[4] = ($num[4] / $count)*100;

             ?>  

         <?php endfor; ?>
     </div>
 </div>
</div>
</div><!-- กล่องใส่กราฟ-->

<div id="showComment"></div>
<div id="showForm"></div>
</div>
 


<?php 
    $this->registerJs("
      var n=1;
      var message='';
      var media = '';
      var pageSizes = 5;
      var count = 0;
      var Count_c = 0;
        setInterval(function(){
          
            realtime();

        },100); 
        var timeOut = setTimeout(function(){
          showComment(pageSizes);
        },100);
        CountInv(); 
        countRate();




        $(document).ready(function() {
          $('.bar span').hide();
          $('#bar-five').animate({
             width:'$num[0]%'    }, 1000);
          $('#bar-four').animate({
              width:'$num[1]%'}, 1000);
          $('#bar-three').animate({
              width:'$num[2]%'}, 1000);
          $('#bar-two').animate({
              width:'$num[3]%'}, 1000);
          $('#bar-one').animate({
              width:'$num[4]%'}, 1000);
          
          setTimeout(function() {
            $('.bar span').fadeIn('slow');
          }, 1000);
          
        });

  
        function countRate()
        {
          $.ajax({
            url:'".Url::to(['/comments/cmd/counts'])."',
            success:function(data)
            {
               $('#Count_rate').html(data);
            }
          })
        }   

        function realtime()
        {
            
             if(Count_c != 0){
                $.ajax({
                  url:'".Url::to(['/comments/cmd/realtime'])."',
                  success:function(count){
                      Count_c = count;
                      console.log(count); 
                      updateData();
                      showComment();
                  }
              });

             } 
            
            
        }//realtime <<<------------------------------------------------------------------------>>>
        function updateData()
        {
            $.ajax({
                url:'".Url::to(['/comments/cmd/saverealtime'])."',
                success:function(count){
                   $('#showComment').load(); 
                   
                   
                }
            });
        }//updateData  <<<------------------------------------------------------------------------>>>
       
        $('#btnComment').click(function(){
            $.ajax({
                 url:'".Url::to(['/comments/cmd/showform'])."',
                 success:function(data){
                    $('#showForm').html(data);
                    $('#showForm').show();
                    $('#showComment').hide();
                    $('#btnLoadmore').hide();
                    $('#container').hide();
                 }
            });
        });//btnComment แสดงกล่องพิมพื <<<------------------------------------------------------------------------>>>

        function showComment(pageSize=5)
        {
         

         $.ajax({
              url:'".Url::to(['/comments/cmd/show-inv'])."',
              type:'POST',
              data:{pageSize:pageSize},
              //dataType:'json',
              success:function(data){
                
                $('#showComment').html(data);
                $('#loadings').hide();
              }

          });
          
        }//showComment <<<------------------------------------------------------------------------>>>


        $('#btnLoadmore').click(function(){
           
           $('#loadings').show();
           showComment(pageSizes+=5);
           CountInv();

        });//LoadMore โหลด 
        function CountInv()
        {
          $.ajax({
            url:'".Url::to(['/comments/cmd/count-inv'])."',
            success:function(data)
            {
              count = data;
              if(pageSizes > count){
                  $('#btnLoadmore').hide();
              }else{
                 $('#btnLoadmore').show();
              }
            }
          })
        }//count

        function showCommentUser()
        {
         

         $.ajax({
              url:'".Url::to(['/comments/cmd/show-inv'])."',
              type:'POST',
              data:{uid:".Yii::$app->user->identity->id."},
              success:function(data){
                
                $('#showComment_User').html(data);
                //console.log(data);
                 
              }

          });
          
        }//showComment <<<------------------------------------------------------------------------>>>
    setTimeout(function(){
        showCommentUser()
    },500);

    ");

?>
 
<div class="clearfix"></div>   
<div class="col-md-6">  
<button id='btnLoadmore' class="btn btn-default btn-block">โหลดเพิ่ม    
<span id="loadings">
  <span style="margin-left:10%;"  class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span> Loading...
</span>
</button>
</div>



 <style>
 #loadings{
  display: none;
 }
 #btnLoadmore{
  display: none;
 }
   .glyphicon-refresh-animate {
    -animation: spin .7s infinite linear;
    -webkit-animation: spin2 .7s infinite linear;
}

@-webkit-keyframes spin2 {
    from { -webkit-transform: rotate(0deg);}
    to { -webkit-transform: rotate(360deg);}
}

@keyframes spin {
    from { transform: scale(1) rotate(0deg);}
    to { transform: scale(1) rotate(360deg);}
}
#showComment{
    height: 300px;overflow-x: hidden;
}
 </style>