 

<div class="col-sm-12 col-md-12">
	
	<?php 
             
		use yii\helpers\Html;
		use yii\helpers\Url;
		use yii\widgets\ActiveForm;
		$select = "";
		$select = "<hr>";
		$select .= "<section class='ratings' style='float:left;'>";
			for($i=1; $i<=5; $i++){
				$select .= "<input type='radio' name='rating' class'rating' value='".$i."'>";
			}
		$select .= "</section>";
		$select .= "<span id='showStarts' style='margin-left:5%;color:rgba(243, 86, 37, 0.95);'></span>";
		echo $select;
                
                //echo $model['gid'];


	?>

	<div class="media">
		<div class="media-left">

			 

			<?php 
                            
				 if(!empty($img)){
                                    
                                      echo Html::img($img['paths'],['class'=>'media-object img-circle','style'=>'width: 60px;height: 60px;']);
			            //echo Html::img($img,['class'=>'media-object','style'=>'width: 60px;height: 60px;']);
			            echo $avatar_base_url;
			        }else{
			            echo Html::img('@web/img/anonymous.jpg',['class'=>'media-object','style'=>'width: 60px;height: 60px;']);
			        }
			?>
		</div>
		<div class="media-body">
			<div class="media-heading">
				<?php 
				$model = new backend\modules\comments\models\Cmd();
				$form = ActiveForm::begin();
				?>
                            <input type="text" hidden="hidden" id="gid" value="<?= Yii::$app->session['gid'];?>">
				<?= $form->field($model,"message")->textArea([
					'rows'=>5	,
					'placeholder'=>"บอกให้ผู้อื่นทราบความคิดเห็นของคุณเกี่ยวกับแอปนี้",
					'id'=>"comment",
					'autofocus'=>true
					])->label(false);?>

					<?php ActiveForm::end();?>

					<!-- ปุ่มส่งความคิดเห็น ครับ  -->

					<?= Html::button('ส่งความคิดเห็น', 
						[
						'class'=>'btn btn-primary',
						'id'=>'btnSend',
						'style'=>'margin-top: 10px;'
						]) ?> 
					<?= Html::button('ยกเลิก', 
						[
						'class'=>'btn btn-default',
						'id'=>'btnCancel',
						'style'=>'margin-top: 10px;'
						]) ?>
					</div>

				</div>
			</div>
</div>
 
<?php 
	$this->registerJs("
		 var vote=0;
		 var message='';
                 var gids = $('#gid').val();
		showStart(); 
		$('#btnSend').click(function(){

			message = $('#comment').val();
			//alert(vote);return false; 
			if(vote == '' || vote == 0)
			{
				alert('กรุณาเลือกดาว');
				return false;
			}else if(message=='' || message==null)
			{
				alert('กรุณากรอกความคิดเห็น');
				return false;
			} 
                             $.ajax({
				url:'".Url::to(['/inv/inv-comment/create'])."',
				type:'POST',
				dataType:'json',
				data:{vote:vote,message:message,gids:gids},
				success:function(data){
					
					if(data.status == '404'){
						alert(data.message);
                                                //$('#myModal').modal();
						return false;
					}else if(data.status == '200')
					{
                                            $('#showForm').html('');
                                                showCommentUser();
                                                $('#container').show();
                                              $.ajax({
                                                url:'".Url::to(['/inv/inv-comment/update-rate'])."',
                                                type:'post',
                                                data:{gid:'".$gid."'},
                                                dataType:'json',    
                                                success:function(data){
                                                    console.log(data);
                                                     setTimeout(function(){
                                                        ShowUpdateRate(data.v5,data.v4,data.v3,data.v2,data.v1);
                                                    },100);
                                                }
                                              }); 
                                              countRate(); 
                                               load_on_check();
                                               $('#container').html('');
                                               ShowRate();
                                               
                                                
					}

					$('#showComment').show();
					$('#showForm').hide();

				} 
            });
            rows =2;
        });//Send <<<----------------------------------------->>>
         
        
        function showStart(){
            
            $('.ratings').rating(function(v, event){
               vote = event.target.title;
               msg(vote);
            });//rating <<<----------------------------------------->>>
        }//showStart
        function msg(v){
            var message = '';
            if(v ==5){
                message='ชอบที่สุด';
            }else if(v ==4){
                message='ชอบ';
            }else if(v ==3){
                message='พอใช้ได้';
            }else if(v ==2){
                message='ไม่ชอบ';
            }else if(v ==1){
                message='ไม่ชอบที่สุด';
            }
            $('#showStarts').html(message);
        }//msg  <<<----------------------------------------->>>

        $('#btnCancel').click(function(){
			$('#showComment').show();
			$('#showForm').hide();
                        $('#container').show();
			
        });//cancel 
        

        


	");
?>
 
 