<?php \yii\widgets\Pjax::begin(['id' => 'nut']); ?>
<div class=""><!-- กล่องใส่กราฟ -->

    <div id="container">
        <div id="inner">
            <div class="rating">
                <span id="Count_rate"></span>
            </div>

            <div class="histo">


                <?php for ($i = 0; $i < 5; $i++): ?>
                    <?php
                    $v = array('bar-five', 'bar-four', 'bar-three', 'bar-two', 'bar-one'); //สี bar
                    $num = array($vote[0][v5], $vote[0][v4], $vote[0][v3], $vote[0][v2], $vote[0][v1]); //จำนวน ความคิดเห็น 


                    $n = 5; //จำนวนตัวเลข bar
                    ?>
                    <div class="five histo-rate">
                        <span class="histo-star">
                            <i class="fa fa-star"></i><?= $n - $i; ?></span>
                        <span class="bar-block"> 
                            <span id="<?= $v[$i]; ?>" class="bar">
                                <span><?= number_format($num[$i]) ?></span>&nbsp;
                            </span> 
                        </span>
                    </div>    
                    <?php
                    $num[0] = ($num[0] / $count) * 100;
                    $num[1] = ($num[1] / $count) * 100;
                    $num[2] = ($num[2] / $count) * 100;
                    $num[3] = ($num[3] / $count) * 100;
                    $num[4] = ($num[4] / $count) * 100;
                    ?>  

                <?php endfor; ?>
            </div>
        </div>
    </div>
</div><!-- กล่องใส่กราฟ-->
<?php \yii\widgets\Pjax::end(); ?>