<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\helpers\Url;
use appxq\sdii\widgets\GridView;
use appxq\sdii\widgets\ModalForm;
use appxq\sdii\helpers\SDNoty;
use appxq\sdii\helpers\SDHtml;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\field\FieldRange;
use backend\modules\inv\classes\InvFunc;
use common\lib\sdii\widgets\SDModalForm;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\ovcca\models\OvPersonSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$inv_gen_main = backend\modules\inv\classes\InvQuery::getModule($module, $userId);
$gname = isset($inv_main['gname'])?$inv_gen_main['gname']:'แสดงตัวอย่างโมดูลที่ได้จากการตั้งค่านี้';
$this->title = Yii::t('app', "$gname");
$this->params['breadcrumbs'][] = $this->title;
$ovfilter_sub = isset($ovfilter_sub) && !empty($ovfilter_sub)?$ovfilter_sub:0;

$op['language'] = 'th';
$q = array_filter($op);
$this->registerJsFile('https://maps.google.com/maps/api/js?key=AIzaSyCq1YL-LUao2xYx3joLEoKfEkLXsEVkeuk&'.http_build_query($q), [
    'position'=>\yii\web\View::POS_HEAD,
    'depends'=>'yii\web\YiiAsset',
]);

\appxq\sdii\assets\leaflet\LeafletAsset::register($this);
?>
<div class="inv-person-index" >
   
    <?php
    if($module>0){
        $gtype = $sub_module>0?4:3;
	echo $this->render('/inv-person/_menu', ['module'=>$module, 'sub_module' => $sub_module, 'gtype'=>$gtype]);
    }
    
    
    ?>
    <?php  Pjax::begin(['id'=>'inv-person-map-pjax', 'timeout' => 1000*60]);?>
    <div class="row" style="margin-top: 10px;margin-bottom: 10px;">
        <?php $form = ActiveForm::begin([
                    'id' => 'jump_menu',
                    'action' => ['index', 'module'=>$module, 'sub_module'=>$sub_module],
                    'method' => 'get',
                    'layout' => 'inline',
                    'options' => ['style'=>'display: inline-block;', 'class'=>'col-md-12']	    
                ]); 
        
        $addmap = isset($_GET['addmap'])?$_GET['addmap']:0;
        $sdate = isset($_GET['sdate'])?$_GET['sdate']:'2017-01-01';
        $edate = isset($_GET['edate'])?$_GET['edate']:date('Y-m-d');
        //'onChange'=>'$("#jump_menu").submit()'
        ?>
        <div class="row">
            <div class="col-md-9 ">
                <b>แสดงผลตามช่วงเวลา</b>
                <?= yii\jui\DatePicker::widget([
                    'name' => 'sdate',
                    'value' => $sdate,
                    'language' => 'th',
                    'dateFormat' => 'yyyy-MM-dd',
                    'options'=>[
                        'id' => 'wx1_datePicker',
                        'class'=>'form-control'
                    ]
                ])?>
                 ถึง 
                <?= yii\jui\DatePicker::widget([
                    'name' => 'edate',
                    'value' => $edate,
                    'language' => 'th',
                    'dateFormat' => 'yyyy-MM-dd',
                    'options'=>[
                        'id' => 'wx2_datePicker',
                        'class'=>'form-control'
                    ]
                ])?>
                <?= Html::hiddenInput('addmap', $addmap)?>
                <button class="btn btn-primary" type="submit"><i class="glyphicon glyphicon-search"></i> ดำเนินการ</button> 
            </div>
            <div class="col-md-3 text-right">
                <?php if($addmap==1):?>
                    <button name="addmap" value="0" class="btn btn-success" type="submit"><i class="glyphicon glyphicon-check"></i> เพิ่มข้อมูล</button>
                <?php else:?>
                    <button name="addmap" value="1" class="btn btn-default" type="submit"><i class="glyphicon glyphicon-ban-circle"></i> เพิ่มข้อมูล</button>
                <?php endif;?>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
    
    <div class="row">
        
        <div class="col-md-12 ">
            <div id="map" style="width: 100%; height: 700px"></div>
        </div>
    </div>
    
    <?php
        $js='';
        $forms = appxq\sdii\utils\SDUtility::string2Array($inv_main['enable_form']);
        $fields = appxq\sdii\utils\SDUtility::string2Array($inv_main['enable_field']);
        $btn_add = '';

        $lat_init = isset($fields['lat_init'])?$fields['lat_init']:'16.0148725';
        $lng_init = isset($fields['lng_init'])?$fields['lng_init']:'101.8819517';
        $zoom_init = isset($fields['zoom_init'])?$fields['zoom_init']:9;
        
        if(is_array($forms)){
            $overlays='{';
            foreach ($forms as $key => $value) {
                //โปรแกรม สร้างแมพ
                $iconArry = explode('-', $value['icon']);
                $prefix = $iconArry[0];
                $icon = str_replace($prefix.'-', '', $value['icon']);
                $color = $value['color'];
                $show = $value['show'];
                
                
                $layerGroup = "map_{$key}";
                $js .= "var $layerGroup = new L.LayerGroup();";
                
                $sedate = null;
                if($value['conddate']==1){
                    $sedate = ['s'=>$sdate, 'e'=>$edate];
                } 
                $var_date = $value['date'];
                        
                $data_map = backend\modules\inv\classes\InvQuery::genMapData($value['ezf_table'], $sitecode, $sedate, $var_date);
                if($data_map){
//                                    var data_set = {
//                                        'sys_lat':e.latlng.lat,
//                                        'sys_lng':e.latlng.lng,
//                                    };
//                                    data_set = btoa(JSON.stringify(data_set));
                    foreach ($data_map as $i => $map) {
                        if($map['sys_lat']!='' && $map['sys_lng']!=''){
                            $eventEdit = '';
                            if($addmap==0){
                                $eventEdit = ".on('click', function(e){
                                        var url = '".Url::to(['/inv/inv-person/ezform-print',
                                                'ezf_id'=>$value['form'],
                                                'dataid'=>$map['dataid'],
                                                'comp_target'=>'skip',
                                                'target'=>null,
                                                'comp_id_target'=>null,
                                                'addperson'=>$value['addperson'],
                                            ])."';

                                        modalEzfrom(url);
                                    })";
                            }
                            $js .= "L.marker([{$map['sys_lat']}, {$map['sys_lng']}], {icon: L.AwesomeMarkers.icon({icon: '$icon', prefix: '$prefix', markerColor: '$color'}) })$eventEdit.addTo($layerGroup);";
                        }
                    }
                }
                if($show==1){
                    $js .= "$layerGroup.addTo(map);";
                }
                
                $overlays .= "'{$value['label']}':$layerGroup,";
                
                if($value['adddata']==1){
                    $btn_add .= Html::a('<i class="fa '.$value['icon'].'"></i> '.$value['label'],null,[
                        'data-url'=>Url::to(['/inv/inv-person/ezform-print',
                            'ezf_id'=>$value['form'],
                            'end'=>1,
                            'comp_target'=>'skip',
                            'comp_id_target'=> NULL,
                            'dataset'=>''
                        ]),
                        'class'=>'btn btn-success print-ezform',
                        'style'=>'cursor: pointer;color: #fff;',
                    ]).' ';
                }
                
            }
            $overlays .= '}';
            
            $js .= "
               
                var grayscale   = L.tileLayer(mbUrl2, {id: 'mapbox.light', attribution: mbAttr2}),
                    streets  = L.tileLayer(mbUrl, {id: 'mapbox.streets',   attribution: mbAttr});   
                streets.addTo(map);
                var baseLayers = {
                        'ดาวเทียม': grayscale,
                        'Streets': streets
                };        
                var overlays = $overlays;
                    
                L.control.layers(baseLayers,overlays, 
                {
                    collapsed: false,
                    autoZIndex: false
                }).addTo(map);
                
               lc = L.control.locate({
                    strings: {
                        title: 'ใช้ตำแหน่งปัจจุบัน'
                    }
                }).addTo(map);

            ";
        }
        
        $eventAdd = '';
        if($addmap==1){
            $btn_add = $btn_add==''?'ไม่อนุญาตให้เพิ่มข้อมูล':$btn_add;
            $eventAdd = "var currentMarker=null;

            map.on('click', function (event) {
             if (currentMarker) {

                    currentMarker._icon.style.transition = 'transform 0.3s ease-out';
                    currentMarker._shadow.style.transition = 'transform 0.3s ease-out';

                    currentMarker.setLatLng(event.latlng);
                    currentMarker.openPopup();

                    setTimeout(function () {
                        currentMarker._icon.style.transition = null;
                        currentMarker._shadow.style.transition = null;
                    }, 300);

                    $('.print-ezform').attr('data-lat',event.latlng.lat);
                    $('.print-ezform').attr('data-lng',event.latlng.lng);

                    return;
                }

                currentMarker = L.marker(event.latlng, {
                    icon: L.AwesomeMarkers.icon({icon: 'star',  prefix: 'fa',markerColor: 'red'}),
                    draggable: true
                }).addTo(map).on('click', function (e) {
                    setTimeout(function () {
                        $('.print-ezform').attr('data-lat',e.latlng.lat);
                        $('.print-ezform').attr('data-lng',e.latlng.lng);
                    }, 300);
                    event.originalEvent.stopPropagation();
                }).bindPopup('$btn_add',{closeButton:false}).openPopup();

                $('.print-ezform').attr('data-lat',event.latlng.lat);
                $('.print-ezform').attr('data-lng',event.latlng.lng);

            });";
        }
        
        $this->registerJs("
	
        var map = L.map('map').setView([$lat_init, $lng_init], $zoom_init);

            var mbUrl2 = 'http://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}';
            var mbAttr2 = 'Map data &copy; <a href=\"http://openstreetmap.org\">OpenStreetMap</a> contributors, ' +
                '<a href=\"https://www.thaicarecloud.org\">Thai Care Cloud</a>, ';

            var mbUrl = 'https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw';
            var mbAttr = 'Map data &copy; <a href=\"http://openstreetmap.org\">OpenStreetMap</a> contributors, ' +
            '<a href=\"https://www.thaicarecloud.org\">Thai Care Cloud</a>, ';


            L.tileLayer(mbUrl, {
              maxZoom: 18,
              attribution: mbAttr,
              id: 'mapbox.streets'
            }).addTo(map);

            // Add geocoder
            var geocoder = L.control.geocoder('search-MKZrG6M').addTo(map);

        $js
        
        $eventAdd
        
        $('.modal-lg').width('90%');	
        
        $('#inv-person-map-pjax').on('click', '.print-ezform', function() {
            var url = $(this).attr('data-url');
            var lat = $(this).attr('data-lat');
            var lng = $(this).attr('data-lng');
            
            var data_set = {
                'sys_lat':lat,
                'sys_lng':lng,
            };
            
            data_set = btoa(JSON.stringify(data_set));
            
            modalEzfrom(url+data_set);
        });
        
        function modalEzfrom(url) {
            $('#modal-print .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
            $('#modal-print').modal('show');
            $.ajax({
                method: 'POST',
                url: url,
                dataType: 'HTML',
                success: function(result, textStatus) {
                    $('#modal-print .modal-content').html(result);
                    return false;
                }
            });
        }

        function modalEzfromEmr(url) {
            $('#modal-inv-emr .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
            $('#modal-inv-emr').modal('show');
            $.ajax({
                method: 'POST',
                url: url,
                dataType: 'HTML',
                success: function(result, textStatus) {
                    $('#modal-inv-emr .modal-content').html(result);
                    return false;
                }
            });
        }

        function modalOvList(url) {
            $('#modal-inv-person .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
            $('#modal-inv-person').modal('show');
            $.ajax({
                method: 'POST',
                url: url,
                data: $('.selectionOvPersonIds:checked[name=\"selection[]\"]').serialize(),
                dataType: 'JSON',
                success: function(result, textStatus) {
                    $('#modal-inv-person .modal-content').html(result.html);
                }
            });
        }


        function modalOvPerson(url) {
            $('#modal-inv-person .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
            $('#modal-inv-person').modal('show')
            .find('.modal-content')
            .load(url);
        }

        function modalOvPersonSmall(url) {
            $('#modal-ovlist .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
            $('#modal-ovlist').modal('show')
            .find('.modal-content')
            .load(url);
        }

        function modalReload(url) {
            $('#modal-reload .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
            $('#modal-reload').modal('show')
            .find('.modal-content')
            .load(url);
        }
    ");
    ?>
    
    <?php Pjax::end();?>
</div>



<?=  ModalForm::widget([
    'id' => 'modal-inv-person',
    'size'=>'modal-lg',
    'tabindexEnable'=>false,
    'clientOptions'=>['backdrop'=>'static'],
    'options'=>['style'=>'overflow-y:scroll;'],
]);
?>

<?=  ModalForm::widget([
    'id' => 'modal-print',
    'size'=>'modal-lg',
    'tabindexEnable'=>false,
    'clientOptions'=>['backdrop'=>'static'],
    'options'=>['style'=>'overflow-y:scroll;']
]);
?>

<?=  ModalForm::widget([
    'id' => 'modal-reload',
    //'size'=>'modal-lg',
    'tabindexEnable'=>false,
    'clientOptions'=>['backdrop'=>'static'],
     'options'=>['style'=>'overflow-y:scroll;']
]);
?>

<?=  ModalForm::widget([
    'id' => 'modal-inv-emr',
    'size'=>'modal-lg',
    'tabindexEnable'=>false,
    //'clientOptions'=>['backdrop'=>'static'],
     'options'=>['style'=>'overflow-y:scroll;']
]);
?>

<?=  ModalForm::widget([
    'id' => 'modal-ezf-view',
    'size'=>'modal-lg',
    'tabindexEnable'=>false,
    'clientOptions'=>['backdrop'=>'static'],
    'options'=>['style'=>'overflow-y:scroll;']
]);
?>

<?php  


$this->registerJs("
    


");?>
