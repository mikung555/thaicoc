<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use appxq\sdii\helpers\SDNoty;
use appxq\sdii\helpers\SDHtml;
use backend\modules\inv\classes\InvQuery;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;
use kartik\depdrop\DepDrop;
use yii\helpers\Url;
use appxq\sdii\widgets\ModalForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\inv\models\InvMain */
/* @var $form yii\bootstrap\ActiveForm */
backend\assets\ColorAsset::register($this);
?>
<?php
if($module>0){
    echo $this->render('/inv-person/_menu', ['module'=>$module, 'gtype'=>3]);
}

$dataMain[$modelEzform->ezf_id] = $modelEzform->ezf_name;
$dataSubFormAll = ArrayHelper::merge($dataMain, $dataSubForm);

?>
<div class="inv-gen-form-map">

    <?php $form = ActiveForm::begin([
	'id'=>$model->formName(),
	'options'=>['enctype'=>'multipart/form-data'],
    ]); ?>

    <div class="modal-body">
	<div class="row">
	    <div class="col-md-6">
		<?= $form->field($model, 'gname')->textInput(['maxlength' => true]) ?>
	    </div>
	    <div class="col-md-3">
		<?php 
		echo $form->field($model, 'gtype')->hiddenInput()->label(FALSE);
		?>
	    </div>
	    <div class="col-md-3" style="margin-top: 20px;">
		<?php
		echo $form->field($model, 'gsystem')->hiddenInput()->label(FALSE);
		?>
	    </div>
	</div>
	
	<?php
	$initialPreview = [];
	if(!$model->isNewRecord && $model->gicon!=''){
	    $initialPreview[] = Html::img(Yii::getAlias('@backendUrl') . '/module_icon/'.$model->gicon, ['width'=>128]);
	}
	?>
	<?= $form->field($model, 'gicon')->widget(kartik\file\FileInput::className(), [
	    'pluginOptions' => [
		'previewFileType' => 'image',
		'initialPreview' => $initialPreview,
		'overwriteInitial' => true,
		'showPreview' => true,
		'showCaption' => true,
		'showRemove' => FALSE,
		'showUpload' => FALSE,
		'allowedFileExtensions' => ['png', 'jpg', 'jpeg'],
		'maxFileSize' => 1000,
	    ]
	])->hint('ควรใช้ภาพขนาด 57x57, 72x72, 114x114, 128x128 <a href="http://www.iconarchive.com/show/flatwoken-icons-by-alecive.html#iconlist" target="_blank">ดาวโหลดไอคอน</a>') ?>
	
        <?= $form->field($model, 'enable_target')->checkbox() ?>
        
	<div id="div-glink">
	    <?= $form->field($model, 'glink')->textInput(['maxlength' => true]) ?>
	</div>
	
	<?= $form->field($model, 'gdetail')->widget(dosamigos\tinymce\TinyMce::className(),[
		'options' => ['rows' => 6],
		'language' => 'th_TH',
		'clientOptions' => [
			'fontsize_formats' => '8pt 9pt 10pt 11pt 12pt 26pt 36pt',
			'plugins' => [
				"advlist autolink lists link image charmap print preview hr anchor pagebreak",
				"searchreplace wordcount visualblocks visualchars code fullscreen",
				"insertdatetime media nonbreaking save table contextmenu directionality",
				"emoticons template paste textcolor colorpicker textpattern",
			],
			'toolbar' => "undo redo | styleselect fontsizeselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media | forecolor backcolor emoticons",
			'content_css' => Yii::getAlias('@backendUrl').'/css/bootstrap.min.css',
			'image_advtab' => true,
			'filemanager_crossdomain' => true,
			'external_filemanager_path' => Yii::getAlias('@storageUrl').'/filemanager/',
			'filemanager_title' => 'Responsive Filemanager',
			'external_plugins' => array('filemanager' => Yii::getAlias('@storageUrl').'/filemanager/plugin.min.js')
		]
	]) ?>
	
	<?= $form->field($model, 'gdevby')->widget(dosamigos\tinymce\TinyMce::className(),[
		'options' => ['rows' => 3],
		'language' => 'th_TH',
		'clientOptions' => [
			'fontsize_formats' => '8pt 9pt 10pt 11pt 12pt 26pt 36pt',
			'plugins' => [
				"advlist autolink lists link image charmap print preview hr anchor pagebreak",
				"searchreplace wordcount visualblocks visualchars code fullscreen",
				"insertdatetime media nonbreaking save table contextmenu directionality",
				"emoticons template paste textcolor colorpicker textpattern",
			],
			'toolbar' => "undo redo | styleselect fontsizeselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media | forecolor backcolor emoticons",
			'content_css' => Yii::getAlias('@backendUrl').'/css/bootstrap.min.css',
			'image_advtab' => true,
			'filemanager_crossdomain' => true,
			'external_filemanager_path' => Yii::getAlias('@storageUrl').'/filemanager/',
			'filemanager_title' => 'Responsive Filemanager',
			'external_plugins' => array('filemanager' => Yii::getAlias('@storageUrl').'/filemanager/plugin.min.js')
		]
	]) ?>
	
	

	<div class="modal-header" style="margin-bottom: 15px;">
		<h4 class="modal-title" id="itemModalLabel">Share</h4>
	    </div>
	
	<?= $form->field($model, 'public')->checkbox() ?>
	<?php
	if($model->public==1){
	    if($model->approved==1){
		echo '<code>Approved.</code><br><br>';
	    } else {
		echo '<code>Waiting for approval.</code><br><br>';
	    }
	}
	?>
	<?php
	$userlist = \backend\modules\ezforms\components\EzformQuery::getIntUserAll(); //explode(",", $model1->assign);
       
	?>
	<?= $form->field($model, 'share')->widget(\kartik\select2\Select2::className(), [
		    'options' => ['placeholder' => 'แชร์', 'multiple' => true],
		    'data' => \yii\helpers\ArrayHelper::map($userlist, 'id', 'text'),
		    'pluginOptions' => [
                        'tokenSeparators' => [',', ' '],
                        'initValueText'=>$userlist_text,
		    ],
		]) ?>
	<div id="div-custom">
	<?= $form->field($model, 'field_name')->hiddenInput()->label(FALSE) ?>
        <?= $form->field($model, 'label_field')->hiddenInput()->label(FALSE) ?>
        <?= $form->field($model, 'order_field')->hiddenInput()->label(FALSE) ?>
	
	<div class="modal-header" style="margin-bottom: 15px;">
		<h4 class="modal-title" id="itemModalLabel">ตั้งค่า</h4>
	    </div>
        <div class="panel panel-primary">
            <div class="panel-heading" ><i class="fa fa-file-text-o" aria-hidden="true"></i> ตั้งค่าแผนที่</div>
            <div class="panel-body">
                
                <div class="row" style="padding-left: 30px;padding-right: 30px;">
                    <div class="col-md-12">
                        <label class="control-label">ตำแหน่งเริ่มต้น</label>
                        <?php
                        $enable_field = $model->enable_field;
                        $lat_init=null;
                        $lng_init=null;
                        $zoom_init=9;

                        if(isset($enable_field) && is_array($enable_field) && !empty($enable_field)){
                            $lat_init = $enable_field['lat_init'];
                            $lng_init = $enable_field['lng_init'];
                            $zoom_init = $enable_field['zoom_init'];
                        }
                            echo \common\lib\sdii\widgets\MapInput::widget([
                                'lat'=>'lat_init',
                                'lng'=>'lng_init',
                                'latValue'=>$lat_init,
                                'lngValue'=>$lng_init,
                            ]);
                            echo Html::hiddenInput('fields[lat_init]', $lat_init, ['id'=>'lat_init']);
                            echo Html::hiddenInput('fields[lng_init]', $lng_init, ['id'=>'lng_init']);
                        ?>
                    </div>
                    
                </div>
                <div class="row" style="padding-left: 30px;padding-right: 30px;margin-top: 15px;">
                    <div class="col-md-12">
                        <label class="control-label">Zoom <code>ตัวเลขยิ่งมากยิ่งซูมมาก</code></label>
                        <?= Html::dropDownList('fields[zoom_init]', $zoom_init, [7=>7,8=>8,9=>9,10=>10,11=>11,12=>12,13=>13,14=>14,15=>15,16=>16,17=>17,18=>18], ['class'=>'form-control'])?>
                    </div>
                </div>
            </div>
        </div>
            
	<div class="panel panel-default" style="border-color: #e08e0b;">
	    <div class="panel-heading" style="background-color: #f39c12; color: #FFF;"><i class="fa fa-file-text-o" aria-hidden="true"></i> เพิ่มฟอร์ม</div>
	    <div class="panel-body">
		
                <div class="row" style="padding-left: 30px;padding-right: 30px;">
				<div class="col-md-2 "><label>Form</label></div>
				<div class="col-md-2 sdbox-col"><label>Label</label></div>
                                <div class="col-md-1 sdbox-col"><label>Date</label></div>
				<div class="col-md-1 sdbox-col"><label>Icon</label></div>
				<div class="col-md-1 sdbox-col"><label>Color</label></div>
			    </div>
                <div id="dad-box" class="forms-items">
		    <?php
		
		    $enable_form = $model->enable_form;
//		    \yii\helpers\VarDumper::dump($enable_form,10,true);
//									    exit();
		    if(isset($enable_form) && is_array($enable_form) && !empty($enable_form)){
			
			foreach ($enable_form as $key_form => $value_form) {
                            $dataDateFieldsResult = ArrayHelper::map(InvQuery::getFields($value_form['form']), 'id', 'name');
			    
			    $dataDateFieldsResult = ArrayHelper::merge(['create_date'=>'Create Date'], $dataDateFieldsResult);
		    ?>
			    <div id="<?=$key_form?>" class="row form-fields dads-children" data-id="<?=$key_form?>" style="margin-bottom: 15px;">
                                <div class="col-md-12 draggable text-center" style="color: #999; padding-bottom: 10px; cursor: -webkit-grab;"><i class="glyphicon glyphicon-resize-vertical"></i><i class="glyphicon glyphicon-resize-vertical"></i><i class="glyphicon glyphicon-resize-vertical"></i></div>
				<div class="col-md-2 "><?= Html::dropDownList("forms[$key_form][form]", $value_form['form'], $dataSubForm, ['class'=>'form-control form-input'])?></div>
				<div class="col-md-2 sdbox-col"><input type="text" class="form-control label-input" name="forms[<?=$key_form?>][label]" value="<?=$value_form['label']?>"></div>
                                <div class="col-md-1 sdbox-col"><?= Html::dropDownList("forms[$key_form][date]", $value_form['date'], $dataDateFieldsResult, ['class'=>'form-control date-input'])?></div>
				<div class="col-md-1 sdbox-col"><div class="input-group iconpicker-container"><input type="text" class="form-control dicon-input icp icp-auto iconpicker-input iconpicker-element" name="forms[<?=$key_form?>][icon]" value="<?=$value_form['icon']?>" ><span class="input-group-addon"><i></i></span></div></div>
                                <div class="col-md-1 sdbox-col"><?=  Html::textInput("forms[$key_form][color]", $value_form['color'], ['class'=>'form-control input-color'])?></div>
                                <div class="col-md-1 sdbox-col"><?= Html::checkbox("forms[$key_form][show]", $value_form['show'], ['label'=>'แสดงทันที'])?></div>
                                <div class="col-md-1 sdbox-col"><?= Html::checkbox("forms[$key_form][adddata]", $value_form['adddata'], ['label'=>'เพิ่มข้อมูลได้'])?></div>
                                <div class="col-md-1 sdbox-col"><?= Html::checkbox("forms[$key_form][conddate]", $value_form['conddate'], ['label'=>'แสดงตามช่วงเวลา'])?></div>
                                <div class="col-md-1 sdbox-col"><?= Html::checkbox("forms[$key_form][addperson]", $value_form['addperson'], ['label'=>'เพิ่มผู้ป่วยได้'])?></div>
                                <div class="col-md-1 sdbox-col">
                                    <button type="button" class="forms-items-del btn btn-danger"><i class="glyphicon glyphicon-remove"></i></button>
                                </div>
				<?=  Html::hiddenInput("forms[$key_form][ezf_name]", $value_form['ezf_name'], ['class'=>'en-input']);?>
				<?=  Html::hiddenInput("forms[$key_form][ezf_table]", $value_form['ezf_table'], ['class'=>'et-input']);?>
				<?=  Html::hiddenInput("forms[$key_form][unique_record]", $value_form['unique_record'], ['class'=>'ur-input']);?>
			    </div>
		    <?php 
			}
		    }
		   ?>
		</div>
		    
		<div class="row add-forms">
		    <div class="col-md-10"></div>
		    <div class="col-md-2 sdbox-col" style="text-align: right;"><button type="button" data-url="<?=  Url::to(['/inv/inv-person/get-widget', 'view'=>'_widget_map'])?>" class="forms-items-add btn btn-success"><i class="glyphicon glyphicon-plus"></i> เพิ่มฟอร์ม</button></div>
		</div>
	    </div>
	</div>
	</div>
	<?= $form->field($model, 'active')->hiddenInput()->label(FALSE) ?>
	<?= $form->field($model, 'created_by')->hiddenInput()->label(FALSE) ?>
	<?= $form->field($model, 'created_at')->hiddenInput()->label(FALSE) ?>
	<?= $form->field($model, 'updated_by')->hiddenInput()->label(FALSE) ?>
	<?= $form->field($model, 'updated_at')->hiddenInput()->label(FALSE) ?>
	<?= $form->field($model, 'special')->hiddenInput()->label(FALSE) ?>
	<?= $form->field($model, 'sitecode')->hiddenInput()->label(FALSE) ?>
	<?= $form->field($model, 'main_ezf_id')->hiddenInput()->label(FALSE) ?>
	<?= $form->field($model, 'main_ezf_name')->hiddenInput()->label(FALSE) ?>
	<?= $form->field($model, 'main_ezf_table')->hiddenInput()->label(FALSE) ?>
	<?= $form->field($model, 'main_comp_id_target')->hiddenInput()->label(FALSE) ?>
	<?= $form->field($model, 'ezf_id')->hiddenInput()->label(FALSE) ?>
	<?= $form->field($model, 'ezf_name')->hiddenInput()->label(FALSE) ?>
	<?= $form->field($model, 'ezf_table')->hiddenInput()->label(FALSE) ?>
	<?= $form->field($model, 'comp_id_target')->hiddenInput()->label(FALSE) ?>
	<?= $form->field($model, 'pk_field')->hiddenInput()->label(FALSE) ?>
	
	<?= $form->field($model, 'enable_field')->hiddenInput()->label(false) ?>
	<?= $form->field($model, 'enable_form')->hiddenInput()->label(false) ?>
	<?= $form->field($model, 'enable_report')->hiddenInput()->label(false) ?>
    </div>
    <div class="modal-footer">
	<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary', 'name'=>'action_submit', 'value'=>'submit']) ?>
	<?php
	    if(!$model->isNewRecord){
		echo Html::submitButton('Delete', ['class' => 'btn btn-danger', 'name'=>'action_submit', 'value'=>'del', 'data' => [
		'confirm' => "คุณแน่ใจที่จะต้องการลบข้อมูลนี้หรือไม่",
		'method' => 'post',
	    ],]);
	    }
	?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?=  ModalForm::widget([
    'id' => 'modal-inv-gen',
    //'size'=>'modal-lg',
]);
?>

<?php  

backend\modules\ezforms2\assets\EzfColorInputAsset::register($this);
backend\assets\IconAsset::register($this);
//\backend\modules\ezforms2\assets\DadAsset::register($this);

$this->registerJs("
var color_options = {
    showInput: true,
    showPalette:true,
    showPaletteOnly: true,
    hideAfterPaletteSelect:true,
    preferredFormat: 'name',
    palette: [
        ['blue','red','darkred','orange','green','darkgreen','purple','darkpuple', 'cadetblue'],
    ]
};

$('.input-color').spectrum(color_options);

$('#dad-box').dad({
    draggable:'.draggable',
    callback:function(e){
	
    }
});

setUi($('#invgen-gtype').val());

$('.forms-items .dicon-input').iconpicker({
    title: 'Using Font Awesome',
    placement:'top',
//    icons: ['bed', 'bug', 'bolt', 'ban', 'book', 'bell', 'birthday-cake', 'bookmark', 'building', 'calculator', 'calendar',
//    'bus', 'camera', 'car', 'check', 'times', 'check-circle', 'check-circle-o', 'circle', 'circle-o', 'clock-o', 'child', 'cloud',
//    'coffee', 'cube', 'cubes', 'cutlery', 'envelope-o', 'diamond', 'exclamation-circle', 'exchange', 'female', 'male', 'flask',
//    'folder-open-o', 'folder-o', 'users', 'user', 'heartbeat', 'heart-o', 'heart', 'gift', 'globe', 'picture-o', 'minus-circle',
//    'phone', 'question-circle', 'plus-circle', 'shield', 'share-alt', 'star', 'star-o', 'thumbs-o-up', 'thumbs-o-down', 'times-circle',
//    'unlock-alt', 'unlock', 'wrench', 'trophy', 'trash', 'tree', 'life-ring', 
//    'shopping-cart', 'paper-plane', 'search', 'retweet', 'random', 'wheelchair', 'user-md', 'stethoscope', 'hospital-o', 'medkit',
//    'h-square', 'ambulance', 'link', 'chain-broken'
//    ],
    iconBaseClass: 'fa',
    iconComponentBaseClass: 'fa',
    iconClassPrefix: 'fa-'
});

$('.forms-items').on('click', '.forms-items-del', function(){
    $(this).parent().parent().remove();
});

$('.forms-items').on('change', '.form-input', function(){
    $(this).parent().parent().find('.label-input').val($(this).find('option:selected').text());
    getSubField($(this).find('option:selected').val(), $(this).parent().parent().find('.date-input'));
    
    getFormType($(this).find('option:selected').val(), $(this).parent().parent().find('.en-input'), $(this).parent().parent().find('.et-input'), $(this).parent().parent().find('.ur-input'));
});


$('.add-forms').on('click', '.forms-items-add', function(){
    getWidget($(this).attr('data-url') ,$('.forms-items'));
});

function setUi(val){
    if(val==1){
	$('#div-glink').show();
	$('#div-custom').hide();
    } else {
	$('#div-glink').hide();
	$('#div-custom').show();
    }
}

function getFormType(id, append1, append2, append3) {
    $.ajax({
	method: 'POST',
	url: '".Url::to(['/inv/inv-person/get-form-type'])."',
	data: {ezf_id:id},
	dataType: 'JSON',
	success: function(result, textStatus) {
	    $(append1).val(result.en);
	    $(append2).val(result.et);
	    $(append3).val(result.ur);
	}
    });
}

function getWidget(url, appendId, id=0) {
    $.ajax({
	method: 'POST',
	url: url,
	data: {id:id, data:".\yii\helpers\Json::encode($dataFields).", data_sub:".\yii\helpers\Json::encode($dataSubForm).", data_select:".\yii\helpers\Json::encode($dataSelect)."},
	dataType: 'HTML',
	success: function(result, textStatus) {
	    $(appendId).append(result);
            $('#dad-box').removeClass('dads-children');
            $('.sp-replacer.sp-light').removeClass('dads-children');
            
	}
    });
}

function modalInvGen(url) {
    $('#modal-inv-gen .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
    $('#modal-inv-gen').modal('show')
    .find('.modal-content')
    .load(url);
}

function getSubField(valField, appendId){
    if(valField === undefined || valField === null){
    
    } else {
	$.ajax({
	    method: 'POST',
	    url: '".Url::to(['/inv/inv-person/getfieldsdate'])."',
	    data: {id:valField},
	    dataType: 'HTML',
	    success: function(result, textStatus) {
		$(appendId).html(result);
	    }
	});
    }
}

");?>