<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\inv\models\InvMain */
$title = 'สร้างแผนที่';
if(isset($model['gname']) && $model['gname']!=''){
    $title = 'ตั้งค่าแผนที่';
}

$this->title = Yii::t('app', $title);
$this->params['breadcrumbs'][] = ['label' => 'จัดการโมดูล', 'url' => ['/inv/inv-gen/index']];
if($module==0){
    $this->params['breadcrumbs'][] = ['label' => 'EzMap', 'url' => ['/inv/inv-map/create-modules', 'module' => $module]];
}

$this->params['breadcrumbs'][] = $this->title;
?>
<div class="inv-map-create">
    
    <?= $this->render('_form_module', [
	    'model' => $model,
	    'module' => $module,
	    'dataSubForm' => $dataSubForm,
    ]) ?>

</div>
