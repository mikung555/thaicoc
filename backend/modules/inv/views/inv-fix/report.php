<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use common\lib\sdii\components\utils\SDUtility;
use yii\helpers\ArrayHelper;
use backend\modules\inv\classes\InvQuery;
use kartik\select2\Select2;
use appxq\sdii\widgets\ModalForm;
use yii\web\JsExpression;
use yii\widgets\Pjax;


/* @var $this yii\web\View */
/* @var $model backend\modules\inv\models\InvMain */
$sitecode = Yii::$app->user->identity->userProfile->sitecode;
$userId = Yii::$app->user->id;
	
$this->title = Yii::t('app', 'รายงาน');
$this->params['breadcrumbs'][] = $this->title;

$op['language'] = 'th';
	
$q = array_filter($op);

$this->registerJsFile('https://maps.google.com/maps/api/js?'.http_build_query($q), [
    'position'=>\yii\web\View::POS_HEAD,
    'depends'=>'yii\web\YiiAsset',
]);

?>
<div class="inv-report">
    <?php
    
    if($module>0){
	
	echo $this->render('/inv-person/_menu', ['module'=>$module, 'sub_module' => $sub_module, 'comp'=>$comp]);
    }
    
    ?>
    <?php  Pjax::begin(['id'=>'inv-person-report-pjax', 'timeout' => 1000*60]);?>
    <div class="inv-report-views" style="padding-top: 15px;">
	<?php $form = ActiveForm::begin([
		    'id' => 'jump_menu',
		    'action' => ['report', 'module'=>$module, 'sub_module'=>$sub_module],
		    'method' => 'get',
		]);
	?>
	
	<?=  Html::radioList('report', $report, ['รายงานอย่างง่าย', 'รายงานขั้นสูง (เร็วๆนี้)'], ['onChange'=>'$("#jump_menu").submit()'])?>
	
	    <div class="panel panel-primary">
	    <div class="panel-heading">
		<h3 class="panel-title"><i class="glyphicon glyphicon-stats"></i> ตั้งค่าการแสดงรายงาน</h3>
	    </div>
	    <div class="panel-body">
	      
	    
	    <?php
	    if($report==0){
		?>
		<div class="row">
		<div class="col-md-3">
		    <h4>เลือกฟอร์มที่ต้องการแสดง</h4>
		</div>
		<div class="col-md-9">
		    <h4>เลือกฟิลด์ที่ต้องการแสดง</h4>
		</div>
	    </div>
		<?php
		$enable_form = SDUtility::string2Array($inv_main['enable_form']);
		$form = isset($enable_form['form'])?$enable_form['form']:[];
		$ezform = InvQuery::getFormsSelect(implode(',', $form));
		foreach ($ezform as $key => $value) {
		    $formItem=null;
		    $fieldItem=null;
		    if(isset($_GET['config'])){
			if(isset($_GET['config'][$value['ezf_id']]['form'])){
			    $formItem = $_GET['config'][$value['ezf_id']]['form'];
			}
			if(isset($_GET['config'][$value['ezf_id']]['field'])){
			    $fieldItem = $_GET['config'][$value['ezf_id']]['field'];
			}
		    }
		?>
		
		<div class="form-group">
		    <div class="row row_<?=$value['ezf_id']?>">
			<div class="col-md-3" style="padding-top: 8px;">
			    <?=Html::checkbox("config[{$value['ezf_id']}][form]", $formItem, ['label'=>$value['ezf_name']])?>
			</div>
			<div class="col-md-9">
			    <?php
			    $modelFields = InvQuery::getFieldsId($value['ezf_id']);
			    $dataFields=[];
			    foreach ($modelFields as $keyF => $valueF) {
				$name = $valueF['name'];

				if($valueF['ezf_field_type']==21){
				    if($valueF['ezf_field_label']==1){
					$name = 'จังหวัด';
				    } elseif ($valueF['ezf_field_label']==2) {
					$name = 'อำเภอ';
				    } elseif ($valueF['ezf_field_label']==3) {
					$name = 'ตำบล';
				    }
				} 
				$dataFields[$valueF['id']] = $name;
			    }
			    ?>
			    <?=  Select2::widget([
				'name' => "config[{$value['ezf_id']}][field]",
				'value' => $fieldItem,	
				'options' => ['placeholder' => 'เลือกฟิลด์', 'multiple' => true],
				'data' => $dataFields,
				'pluginOptions' => [
					'tags' => true,
				],
			    ])?>
			</div>
		    </div>
		</div>


		<?php
		}
		?>
		<div class="form-group">
		    <?= Html::submitButton('<i class="glyphicon glyphicon-eye-open"></i> แสดงรายงาน', ['class' => 'btn btn-primary']) ?>
		</div>
		</div>
	  </div>
	    <?php ActiveForm::end(); ?>
	    <?php
    //	\yii\helpers\VarDumper::dump($ezform,10,true);
    //	exit();
		if(isset($_GET['config'])){
		    $config = $_GET['config'];
		    $ezformMap = ArrayHelper::map($ezform, 'ezf_id', 'ezf_name');
		    $ezformMapTable = ArrayHelper::map($ezform, 'ezf_id', 'ezf_table');
		    
		    $i = 1;
		    foreach ($config as $ezf_id => $value) {
			if(isset($value['form']) && $value['form']==1){
		?>
		    <h4><i class="glyphicon glyphicon-list-alt"></i> ตารางที่ <?=$i?> ข้อมูล <?=$ezformMap[$ezf_id]?></h4>
	
			    <table class="table table-bordered table-hover"> 
				<thead> 
				    <tr> 
					<th>ลักษณะ</th> 
					<th style="width: 200px; text-align: right;">จำนวน</th> 
					<th style="width: 200px; text-align: right;">ร้อยละ</th>
					<th style="width: 200px; text-align: right;"></th>
				    </tr> 
				</thead> 
				<?php
				$select = '';
				if(isset($value['field']) && !empty($value['field'])){
				    $select = implode(',', $value['field']);
				}
				if($select==''){
				    continue;
				}
				$dataFieldsReport = InvQuery::getFieldsSelect($ezf_id, $select);
				?>
				<tbody> 
				    <?php foreach ($dataFieldsReport as $key => $field):?>
				    <?php
				    $nameLabel = $field['ezf_field_label'];
				  
					if($field['ezf_field_type']==21){
					    if($field['ezf_field_label']==1){
						$nameLabel = 'จังหวัด';
					    } elseif ($field['ezf_field_label']==2) {
						$nameLabel = 'อำเภอ';
					    } elseif ($field['ezf_field_label']==3) {
						$nameLabel = 'ตำบล';
					    }
					} 
				
				    ?>
				    <tr> 
					<th><i class="glyphicon glyphicon-chevron-down"></i> <?=$nameLabel?></th> 
					<th style="width: 200px; text-align: right;"></th> 
					<th style="width: 200px; text-align: right;"></th> 
					<th style="width: 200px; text-align: right;"></th>
				    </tr> 
				    <?php
				    $ezf_table = $ezformMapTable[$ezf_id];
				    $fId = $field['ezf_field_id'];
				    $fName = $field['ezf_field_name'];
				    $fType = $field['ezf_field_type'];
				    $fLabel = $field['ezf_field_label'];
				    
				   
				    $dataItems = InvQuery::getFieldsItems($ezf_table, $fName, $inv_main, $sitecode);
				    $dataCount = InvQuery::getFieldsItemsCount($ezf_table, $fName, $inv_main, $sitecode);
				    $select = ArrayHelper::map($dataItems, $fName, 'row_num');
				    ArrayHelper::remove($select, '');
				    
				    $select = implode(',', array_keys($select));
				    
				    $categories = [];
				    $dataChart = [];
				    $dataPie = [];
				    foreach ($dataItems as $keyItem => $item) {
					if(in_array($fType, [4,6])){
					    $choice=[];
					    $modelChoice = \backend\modules\ezforms\models\EzformChoice::find()
						    ->where('ezf_field_id=:id',[':id'=>$fId])
						    ->all();
					    if($modelChoice){
						$choice = ArrayHelper::map($modelChoice, 'ezf_choicevalue', 'ezf_choicelabel');
					    }
					    
					    $choice['']='ว่าง';
					    $choice['null']='Null';
					    $categories[] = $choice[isset($item[$fName])?$item[$fName]:'null'];
					    echo $this->render('_itemReport', [
						'label' => $choice[isset($item[$fName])?$item[$fName]:'null'],
						'num'=>$item['row_num'],
						'count'=>$dataCount,
						'inv_main'=>$inv_main,
						'field'=>$item,
						'jtable'=>$ezf_table,
						'jselect'=>$fName,
						'jvalue'=>isset($item[$fName])?$item[$fName]:'null',
						'jname'=>$ezformMap[$ezf_id],
						'rurl'=>base64_encode(Yii::$app->request->url),
						'ezf_id'=>$ezf_id,
					    ]);
					} else if($fType==21) {
					    $choice=[];
					    if($fLabel==1){
						$data = \backend\modules\inv\classes\InvQuery::getProvinceSelect($select);
						if($data){
						    $choice = ArrayHelper::map($data, 'id', 'name');
						}
					    } elseif ($fLabel==2) {
						$data = \backend\modules\inv\classes\InvQuery::getAmphurSelect($select);
						if($data){
						    $choice = ArrayHelper::map($data, 'id', 'name');
						}
						
					    } elseif ($fLabel==3) {
						$data = \backend\modules\inv\classes\InvQuery::getDistrictSelect($select);
						if($data){
						    $choice = ArrayHelper::map($data, 'id', 'name');
						}
					    }
					    
					    $choice['']='ว่าง';
					    $choice['null']='Null';
					   
					    $label = $choice[isset($item[$fName])?$item[$fName]:'null'];
					    $label = $label==''?"<code>ไม่พบรหัส $item[$fName]</code>":$label;
					    
					    $categories[] = $label;
					    echo $this->render('_itemReport', [
						'label' => $label,
						'num'=>$item['row_num'],
						'count'=>$dataCount,
						'inv_main'=>$inv_main,
						'field'=>$item,
						'jtable'=>$ezf_table,
						'jselect'=>$fName,
						'jvalue'=>isset($item[$fName])?$item[$fName]:'null',
						'jname'=>$ezformMap[$ezf_id],
						'rurl'=>base64_encode(Yii::$app->request->url),
						'ezf_id'=>$ezf_id,
					    ]);
					} elseif ($fType==17) {//Hospital list
					    $choice=[];
					    $data = \backend\modules\inv\classes\InvQuery::getHospitalSelect($select);
					    if($data){
						$choice = ArrayHelper::map($data, 'id', 'name');
					    }
					    
					    $choice['']='ว่าง';
					    $choice['null']='Null';
					   
					    $label = $choice[isset($item[$fName])?$item[$fName]:'null'];
					    $label = $label==''?"<code>ไม่พบรหัส $item[$fName]</code>":$label;
					    
					    $categories[] = $label;
					    echo $this->render('_itemReport', [
						'label' => $label,
						'num'=>$item['row_num'],
						'count'=>$dataCount,
						'inv_main'=>$inv_main,
						'field'=>$item,
						'jtable'=>$ezf_table,
						'jselect'=>$fName,
						'jvalue'=>isset($item[$fName])?$item[$fName]:'null',
						'jname'=>$ezformMap[$ezf_id],
						'rurl'=>base64_encode(Yii::$app->request->url),
						'ezf_id'=>$ezf_id,
					    ]);
					} elseif (in_array($fType, [16, 19])) {
					    $valueFix = isset($item[$fName])?$item[$fName]:'null';
					    $arrFix = [0=>'No', 1=>'Yes', ''=>'ว่าง', 'null'=>'Null'];
					    
					    $categories[] = $arrFix[$valueFix];
					    echo $this->render('_itemReport', [
						'label' => $arrFix[$valueFix],
						'num'=>$item['row_num'],
						'count'=>$dataCount,
						'inv_main'=>$inv_main,
						'field'=>$item,
						'jtable'=>$ezf_table,
						'jselect'=>$fName,
						'jvalue'=>isset($item[$fName])?$item[$fName]:'null',
						'jname'=>$ezformMap[$ezf_id],
						'rurl'=>base64_encode(Yii::$app->request->url),
						'ezf_id'=>$ezf_id,
					    ]);
					} else {
					    $categories[] = isset($item[$fName])?($item[$fName]!=''?$item[$fName]:'ว่าง'):'Null';
					    echo $this->render('_itemReport', [
						'label' => isset($item[$fName])?($item[$fName]!=''?$item[$fName]:'ว่าง'):'Null',
						'num'=>$item['row_num'],
						'count'=>$dataCount,
						'inv_main'=>$inv_main,
						'field'=>$item,
						'jtable'=>$ezf_table,
						'jselect'=>$fName,
						'jvalue'=>isset($item[$fName])?$item[$fName]:'null',
						'jname'=>$ezformMap[$ezf_id],
						'rurl'=>base64_encode(Yii::$app->request->url),
						'ezf_id'=>$ezf_id,
					    ]);
					} 
					$dataChart[] = $item['row_num']+0;
					
					$dataPie[] = ['name'=>$categories[$keyItem] , 'y'=>$item['row_num']+0];
				    }
				    ?>
				    <tr> 
					<th style="text-align: right;">รวม</th> 
					<th style="width: 200px; text-align: right;"><?=number_format($dataCount)?></th> 
					<th style="width: 200px; text-align: right;"></th> 
					<th style="width: 200px; text-align: right;"></th> 
				    </tr> 
				    <tr>
					<td>
					    <?= miloschuman\highcharts\Highcharts::widget([
						'options' => [
						    'title' => ['text' => $nameLabel],
						    'xAxis' => [
						       'categories' => $categories
						    ],
						    'yAxis' => [
						       'title' => ['text' => 'จำนวน']
						    ],
						    'series' => [
						       ['name' => $nameLabel, 'data' => $dataChart]
						    ]
						 ]
					    ]);?>
					</td>
					<td colspan="3">
					    <?= miloschuman\highcharts\Highcharts::widget([
						'options' => [
						    'chart' => [
							'plotBackgroundColor'=> NULL,
							'plotBorderWidth'=> NULL,
							'plotShadow'=> false,
							'type'=> 'pie',
						    ],
						    'title' => ['text' => $nameLabel],
						    'tooltip' => ['pointFormat'=>new JsExpression("'{series.name}: <b>{point.percentage:.1f}%</b>'")],
						    'plotOptions'=> [
							'pie' => [
							    'allowPointSelect' => true,
							    'cursor' => 'pointer',
							    'dataLabels' => [
								'enabled'=>true,
								'format'=>new JsExpression("'<b>{point.name}</b>: {point.percentage:.1f} %'"),
								'style' => [
								    'color' => new JsExpression("(Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'")
								]
							    ]
							]
						    ],
						    'series' => [
						      [
							  'name' => $nameLabel,
							  'colorByPoint' => true,
							  'data' => $dataPie
						      ]
						    ]
						 ]
					    ]);?>
					   
					    
					</td>
				    </tr>
				    <?php endforeach; ?>
				</tbody> 
			    </table>
		<?php
			    $i++;
			}
		    }
		}
	    }//report = 0
	    ?>
	   
	
    </div>
    <?php  Pjax::end();?>
</div>



<?=  ModalForm::widget([
    'id' => 'modal-appoint',
    'size'=>'modal-lg',
    'options'=>['style'=>'overflow-y:scroll;'],
]);
?>

<?=  ModalForm::widget([
    'id' => 'modal-print',
    'size'=>'modal-lg',
    'tabindexEnable'=>false,
    'options'=>['style'=>'overflow-y:scroll;'],
]);
?>

<?php  $this->registerJs("
    $('.modal-lg').width('90%');
    
  $('.btn-views').click(function(){
	var url = $(this).attr('data-url');
	modalAppoint(url);
  });
	
function modalAppoint(url) {
    $('#modal-appoint .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
    $('#modal-appoint').modal('show')
    .find('.modal-content')
    .load(url);
}

$('#inv-person-report-pjax').on('click', '.open-ezfrom', function() {
    modalEzfrom($(this).attr('data-url'));
    
});

function modalEzfrom(url) {
    $('#modal-print .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
    $('#modal-print').modal('show');
    $.ajax({
	method: 'POST',
	url: url,
	dataType: 'HTML',
	success: function(result, textStatus) {
	    $('#modal-print .modal-content').html(result);
	    return false;
	}
    });
}

");?>