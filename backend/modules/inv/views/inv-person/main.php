<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\inv\models\InvMain */

$this->title = Yii::t('app', 'EzModule® (Version 2.0) : '.$modelEzform->ezf_name);
$this->params['breadcrumbs'][] = ['label' => 'แสดงตัวอย่างโมดูลที่ได้จากการตั้งค่านี้', 'url' => ['/inv/inv-person/index', 'module' => $module, 'comp'=>$ezf_id]];
$this->params['breadcrumbs'][] = ['label' => 'จัดการโมดูล', 'url' => ['/inv/inv-gen/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="inv-main-create">
    
    <?= $this->render('_form_main', [
        'sitecode' =>$sitecode,
	    'userId'=>$userId,
	    'model' => $model,
	    'ezform'=>$ezform,
	    'ezf_id'=>$ezf_id,
	    'modelEzformMain'=>$modelEzformMain,
	    'main_ezf_id'=>$main_ezf_id,
	    'module' => $module,
	    'dataFields' => $dataFields,
	    'modelEzform' => $modelEzform,
	    'dataSubForm' => $dataSubForm,
	    'dataSelect' => $dataSelect,
    ]) ?>

</div>
