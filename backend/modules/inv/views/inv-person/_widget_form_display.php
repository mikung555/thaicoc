<?php
use yii\helpers\Html;
use yii\helpers\Url;

$genid = \common\lib\codeerror\helpers\GenMillisecTime::getMillisecTime();

?>
<div id="<?=$genid?>" class="row" style="margin-bottom: 15px;">
    <div class="col-md-2 sdbox-col"><?= Html::dropDownList('forms[display]['.$id.'][condition][]', null, ['=='=>'=', '<'=>'<', '>'=>'>', '<='=>'<=', '>='=>'>=', '!='=>'!=', 'between'=>'Between','func'=>'PHP Function'], ['class'=>'form-control dcond-input'])?></div>
    <div class="col-md-2 sdbox-col"><input type="text" class="form-control dvalue1-input" name="forms[display][<?=$id?>][value1][]" ></div>
    <div class="col-md-2 sdbox-col"><input type="text" class="form-control dvalue2-input" readonly="readonly" name="forms[display][<?=$id?>][value2][]" ></div>
    <div class="col-md-2 sdbox-col"><input type="text" class="form-control dicon-input" name="forms[display][<?=$id?>][icon][]" ></div>
    <div class="col-md-2 sdbox-col"><input type="text" class="form-control dlabel-input" name="forms[display][<?=$id?>][label][]" ></div>
    <div class="col-md-2 sdbox-col"><button type="button" class="forms-display-del btn btn-danger"><i class="glyphicon glyphicon-remove"></i></button></div>
</div>
<?php  
$this->registerJs("
$('.dicon-input').iconpicker({
    title: 'Using Font Awesome',
    icons: ['bed', 'bug', 'bolt', 'ban', 'book', 'bell', 'birthday-cake', 'bookmark', 'building', 'calculator', 'calendar',
    'bus', 'camera', 'car', 'check', 'times', 'check-circle', 'check-circle-o', 'circle', 'circle-o', 'clock-o', 'child', 'cloud',
    'coffee', 'cube', 'cubes', 'cutlery', 'envelope-o', 'diamond', 'exclamation-circle', 'exchange', 'female', 'male', 'flask',
    'folder-open-o', 'folder-o', 'users', 'user', 'heartbeat', 'heart-o', 'heart', 'gift', 'globe', 'picture-o', 'minus-circle',
    'phone', 'question-circle', 'plus-circle', 'shield', 'share-alt', 'star', 'star-o', 'thumbs-o-up', 'thumbs-o-down', 'times-circle',
    'unlock-alt', 'unlock', 'wrench', 'trophy', 'trash', 'tree', 'life-ring', 
    'shopping-cart', 'paper-plane', 'search', 'retweet', 'random', 'wheelchair', 'user-md', 'stethoscope', 'hospital-o', 'medkit',
    'h-square', 'ambulance', 'link', 'chain-broken'
    ],
    iconBaseClass: 'fa',
    iconComponentBaseClass: 'fa',
    iconClassPrefix: 'fa-'
});
");?>