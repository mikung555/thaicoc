<?php

use Yii;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use appxq\sdii\helpers\SDNoty;
use appxq\sdii\helpers\SDHtml;
use yii\web\JsExpression;

/**
 * _ezform_popup file UTF-8
 * @author SDII <iencoded@gmail.com>
 * @copyright Copyright &copy; 2015 AppXQ
 * @license http://www.appxq.com/license/
 * @version 1.0.0 Date: 17 พ.ย. 2559 12:32:13
 * @link http://www.appxq.com/
 */
$xsourcex = Yii::$app->user->identity->userProfile->sitecode;
$xdepartmentx = Yii::$app->user->identity->userProfile->department_area;
$xdepartmentx_sql = $xdepartmentx > 0 ? ("AND xdepartmentx ='" . $xdepartmentx . "'") : null;
$input_ezf_id = Yii::$app->request->get('ezf_id');
$input_target = Yii::$app->request->get('target');
$input_comp_target = Yii::$app->request->get('comp_id_target');
$input_dataid = Yii::$app->request->get('dataid');
$ezform = \backend\models\Ezform::findOne(['ezf_id' => $input_ezf_id]);
?>

<div id="emr-popup" class="fields-form">

    <div class="modal-body" >

        <div class="inv-person-form">

            <?php $form = ActiveForm::begin([
            'id'=>$model->formName(),
             'layout'=>'inline',   
            ]); ?>

            <?php
	    echo $form->field($model, 'inv_name')->widget(\yii\jui\AutoComplete::className(), [
		'clientOptions' => [
		'minChars' => 1,
		'source' => new JsExpression("function(request, response) {
		    $.getJSON('".yii\helpers\Url::to(['/inv/inv-map/querys'])."', {
                        q: request.term
                    }, response);
		}"),
		'select' => new JsExpression("function( event, ui ) {
		    $('#invperson-person_id').val(ui.item.id);
                    $('#invperson-person_ezf_id').val(ui.item.ezf_id);
                    $('#invperson-person_comp_target').val(ui.item.comp_id_target);
                    $('#invperson-person_target').val(ui.item.target);
                    
                    $('#btn-save').show();
		 }"),
		'change' => new JsExpression("function( event, ui ) {
		    if (!ui.item) {
			$('#invperson-person_id').val('');
                        $('#invperson-person_ezf_id').val('');
                        $('#invperson-person_comp_target').val('');
                        $('#invperson-person_target').val('');
                        
                        $('#btn-save').hide();
		    }
		 }"),
		],
	     'options'=>['class'=>'form-control', 'placeholder'=>'ค้นหาผู้ป่วยเพื่อเพิ่มข้อมูล', 'style'=>'width: 400px;'] 
	    ]);
	?>
            
            <?= $form->field($model, 'inv_id')->hiddenInput()->label(false) ?>
            
            <?= $form->field($model, 'person_id')->hiddenInput()->label(false) ?>
            <?= $form->field($model, 'person_ezf_id')->hiddenInput()->label(false) ?>
            <?= $form->field($model, 'person_comp_target')->hiddenInput()->label(false) ?>
            <?= $form->field($model, 'person_target')->hiddenInput()->label(false) ?>
            
            <?= Html::submitButton('เพิ่มผู้ป่วย', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary' ,'id'=>'btn-save']) ?>
            
            <?php ActiveForm::end(); ?>

        </div>
        
        <div class="inv-person-items" style="margin-top: 15px;">
            <table class="table table-hover"> 
                <thead> 
                    <tr> 
                        <th>รายชื่อผู้ป่วย</th> 
                        <th style="min-width: 120px; width: 120px;">วันที่เพิ่มข้อมูล</th> 
                        <th style="min-width: 150px; width: 200px;"></th> 
                    </tr> 
                </thead> 
                <tbody id="box-items"> 
                    <?php
                    
                    $userid = Yii::$app->user->id;
                    if(isset($person_items) && !empty($person_items)){
                        foreach ($person_items as $key => $value) {
                            ?>
                            <tr data-id="<?=$value['inv_id']?>"> 
                                <td><?=$value['inv_name']?></td> 
                                <td><?= isset($value['updated_at'])?\common\lib\sdii\components\utils\SDdate::mysql2phpThDateSmall($value['updated_at']):''?></td> 
                                <td>
                                    <?php
//                                        echo Html::a("EMR", NULL, [
//                                            'data-url'=> \yii\helpers\Url::to(['/inv/inv-map/ezform-emr-all',
//                                                'ezf_id'=>$value['person_ezf_id'],
//                                                'target'=>base64_encode($value['person_target']),
//                                                'comp_id_target'=>$value['person_comp_target'],
//                                            ]),
//                                            'class'=>'btn btn-xs btn-primary open-ezfrom-emr',
//                                            'style'=>'cursor: pointer;',
//                                        ]);
                                    
                                    if($value['created_by'] == $userid){
                                    ?>
                                    
                                    <button class="btn btn-xs btn-danger btn-del" 
                                            data-url="<?= \yii\helpers\Url::to(['/inv/inv-map/delete', 'id'=>$value['inv_id']])?>">นำออก</button>
                                    <?php
                                    }
                                    ?>
                                </td> 
                            </tr> 
                            <?php
                        }
                    }
                    ?>
                    
                    
                </tbody> 
            </table>
        </div>
    </div>

</div>

<?= appxq\sdii\widgets\ModalForm::widget([
    'id' => 'modal-sub-emr',
    'size'=>'modal-lg',
    'tabindexEnable'=>false,
    //'clientOptions'=>['backdrop'=>'static'],
     'options'=>['style'=>'overflow-y:scroll;']
]);
?>

<?php
//window.print();
$this->registerJs("
    $('#btn-save').hide();
    
$('#box-items').on('click', '.btn-del', function() {
    var url = $(this).attr('data-url');
    var ele = this;
    
    yii.confirm('".Yii::t('app', 'Are you sure you want to delete this item?')."', function() {
        $.post(
            url
        ).done(function(result) {
            if(result.status == 'success') {
                ". SDNoty::show('result.message', 'result.status') ."
                $(ele).parent().parent().remove();
            } else {
                ". SDNoty::show('result.message', 'result.status') ."
            }
        }).fail(function() {
            ". SDNoty::show("'" . SDHtml::getMsgError() . "Server Error'", '"error"') ."
            console.log('server error');
        });
    });
});

$('form#{$model->formName()}').on('beforeSubmit', function(e) {
    var \$form = $(this);
    $.post(
    \$form.attr('action'), //serialize Yii2 form
    \$form.serialize()
    ).done(function(result) {
    if(result.status == 'success') {
        ". SDNoty::show('result.message', 'result.status') ."
        if(result.action == 'create') {
            $(\$form).trigger('reset');
            $('#box-items').append(result.html);
            $('#btn-save').hide();
        } else if(result.action == 'update') {
           
        }
    } else {
        ". SDNoty::show('result.message', 'result.status') ."
    } 
    }).fail(function() {
    ". SDNoty::show("'" . SDHtml::getMsgError() . "Server Error'", '"error"') ."
    console.log('server error');
    });
    return false;
});

$('#emr-popup').on('click', '.open-ezfrom', function() {
    modalEzfrom($(this).attr('data-url'));
    
});
function modalEzfrom(url) {
    $('#modal-print .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
    $('#modal-print').modal('show');
    $.ajax({
	method: 'POST',
	url: url,
	dataType: 'HTML',
	success: function(result, textStatus) {
	    $('#modal-print .modal-content').html(result);
	    return false;
	}
    });
}

$('#box-items').on('click', '.open-ezfrom-emr', function() {
    modalEzfromEmr($(this).attr('data-url'));
    
});

function modalEzfromEmr(url) {
    $('#modal-inv-emr .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
    $('#modal-inv-emr').modal('show');
    $.ajax({
	method: 'POST',
	url: url,
	dataType: 'HTML',
	success: function(result, textStatus) {
	    $('#modal-inv-emr .modal-content').html(result);
	    return false;
	}
    });
}

");
?>


