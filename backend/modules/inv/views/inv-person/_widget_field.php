<?php
$genid = \common\lib\codeerror\helpers\GenMillisecTime::getMillisecTime();
$indexArry = array_keys($dataFields);
?>
<div id="<?=$genid?>" class="row" style="margin-bottom: 15px;">
    <div class="col-md-3 "><?=  yii\bootstrap\Html::dropDownList('fields[field][]', null, $dataFields, ['class'=>'form-control field-input'])?></div>
    <div class="col-md-3 sdbox-col"><input type="text" class="form-control label-input" name="fields[label][]" value="<?= isset($dataFields[$indexArry[0]])?$dataFields[$indexArry[0]]:''?>"></div>
    <div class="col-md-1 sdbox-col"><input type="number" class="form-control width-input" name="fields[width][]" value="110"></div>
    <div class="col-md-1 sdbox-col"><input type="number" class="form-control rwidth-input" name="fields[rwidth][]" value="25"></div>
    <div class="col-md-2 sdbox-col"><?=  \yii\helpers\Html::dropDownList('fields[align][]', null, ['left'=>'ชิดซ้าย', 'center'=>'กึ่งกลาง', 'right'=>'ชิดขวา'], ['class'=>'form-control align-input'])?></div>
    <div class="col-md-1 sdbox-col"><?= yii\helpers\Html::dropDownList('fields[report][]', null, ['Y'=>'Yes', 'N'=>'No'], ['class'=>'form-control report-input'])?></div>
    <?= \yii\helpers\Html::hiddenInput('fields[type][]', null, ['class'=>'type-input']);?>
    <?= \yii\helpers\Html::hiddenInput('fields[id][]', null, ['class'=>'id-input']);?>
    <?= \yii\helpers\Html::hiddenInput('fields[orglabel][]', null, ['class'=>'orglabel-input']);?>
    <div class="col-md-1 sdbox-col"><button type="button" class="fields-items-del btn btn-danger"><i class="glyphicon glyphicon-remove"></i></button></div>
</div>
<?php  
$valF = isset($indexArry[0])?$indexArry[0]:0;
$this->registerJs("
$('#$genid .field-input').trigger('change');

");?>