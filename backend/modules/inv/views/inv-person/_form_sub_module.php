<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use appxq\sdii\helpers\SDNoty;
use appxq\sdii\helpers\SDHtml;
use backend\modules\inv\classes\InvQuery;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;
use kartik\depdrop\DepDrop;
use yii\helpers\Url;
use appxq\sdii\widgets\ModalForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\inv\models\InvMain */
/* @var $form yii\bootstrap\ActiveForm */
backend\assets\ColorAsset::register($this);
?>
<?php
if($module>0){
    echo $this->render('/inv-person/_menu', ['module'=>$module, 'sub_module' => $sub_module, 'comp'=>$comp]);
}

$dataMain[$modelEzform->ezf_id] = $modelEzform->ezf_name;
$dataSubFormAll = ArrayHelper::merge($dataMain, $dataSubForm);

?>
<div>
    <?php $formJump = ActiveForm::begin([
		'id' => 'jump_menu',
		'action' => ['sub-modules', 'module'=>$module],
		'method' => 'get',
		//'layout' => 'inline',
		//'options' => ['style'=>'display: inline-block;', 'class'=>'col-md-12']	    
	    ]); 
    
	    ?>
	<div class="modal-body">
	    <div class="row">
		<div class="col-md-5 ">
		    <label>ฟอร์มตั้งต้นที่ถูกเลือก</label>
		</div>
		
	    </div>
	    <div class="row">
		<div class="col-md-12 ">
		    <div class="form-inline">
                        <?=  Html::dropDownList('gtype', $gtype, [0=>'General Module', 2=>'Map Module'], ['class'=>'form-control', 'onChange'=>'$("#jump_menu").submit()', 'disabled'=>!$model->isNewRecord])?>
                        
		    <?=  Html::dropDownList('main_ezf_id', $main_ezf_id, ArrayHelper::map($ezform, 'ezf_id', 'ezf_name'), ['class'=>'form-control', 'onChange'=>'$("#jump_menu").submit()', 'disabled'=>(!$model->isNewRecord || $gtype==2)])?>
                        
		    <?php
                    if($model->isNewRecord){
                        echo Html::button('<i class="glyphicon glyphicon-star"></i> คลิกที่นี่เพื่อเลือกฟอร์มอื่นๆ', ['id'=>'add-form-fav', 'class'=>'btn btn-warning']);
                    }
                    ?>
		    </div>
		</div>
	    </div>
	</div>
     <?php ActiveForm::end(); ?>
</div>
<div class="inv-gen-form">
    
    <?php $form = ActiveForm::begin([
	'id'=>$model->formName(),
	'options'=>['enctype'=>'multipart/form-data'],
    ]); ?>

    <div class="modal-body">
	<div class="row">
	    <div class="col-md-6">
		<?= $form->field($model, 'gname')->textInput(['maxlength' => true]) ?>
	    </div>
	    
	</div>
	
	<?php
	$initialPreview = [];
	if(!$model->isNewRecord && $model->gicon!=''){
	    $initialPreview[] = Html::img(Yii::getAlias('@backendUrl') . '/module_icon/'.$model->gicon, ['width'=>128]);
	}
	?>
	<?= $form->field($model, 'gicon')->widget(kartik\file\FileInput::className(), [
	    'pluginOptions' => [
		'previewFileType' => 'image',
		'initialPreview' => $initialPreview,
		'overwriteInitial' => true,
		'showPreview' => true,
		'showCaption' => true,
		'showRemove' => FALSE,
		'showUpload' => FALSE,
		'allowedFileExtensions' => ['png', 'jpg', 'jpeg'],
		'maxFileSize' => 1000,
	    ]
	])->hint('ควรใช้ภาพขนาด 57x57, 72x72, 114x114, 128x128 <a href="http://www.iconarchive.com/show/flatwoken-icons-by-alecive.html#iconlist" target="_blank">ดาวโหลดไอคอน</a>') ?>
	
        <?= $form->field($model, 'enable_target')->checkbox() ?>
        
	<div id="div-glink">
	    <?= $form->field($model, 'glink')->textInput(['maxlength' => true]) ?>
	</div>
	
	<?= $form->field($model, 'gdetail')->widget(dosamigos\tinymce\TinyMce::className(),[
		'options' => ['rows' => 6],
		'language' => 'th_TH',
		'clientOptions' => [
			'fontsize_formats' => '8pt 9pt 10pt 11pt 12pt 26pt 36pt',
			'plugins' => [
				"advlist autolink lists link image charmap print preview hr anchor pagebreak",
				"searchreplace wordcount visualblocks visualchars code fullscreen",
				"insertdatetime media nonbreaking save table contextmenu directionality",
				"emoticons template paste textcolor colorpicker textpattern",
			],
			'toolbar' => "undo redo | styleselect fontsizeselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media | forecolor backcolor emoticons",
			'content_css' => Yii::getAlias('@backendUrl').'/css/bootstrap.min.css',
			'image_advtab' => true,
			'filemanager_crossdomain' => true,
			'external_filemanager_path' => Yii::getAlias('@storageUrl').'/filemanager/',
			'filemanager_title' => 'Responsive Filemanager',
			'external_plugins' => array('filemanager' => Yii::getAlias('@storageUrl').'/filemanager/plugin.min.js')
		]
	]) ?>
	
	<?= $form->field($model, 'gdevby')->widget(dosamigos\tinymce\TinyMce::className(),[
		'options' => ['rows' => 3],
		'language' => 'th_TH',
		'clientOptions' => [
			'fontsize_formats' => '8pt 9pt 10pt 11pt 12pt 26pt 36pt',
			'plugins' => [
				"advlist autolink lists link image charmap print preview hr anchor pagebreak",
				"searchreplace wordcount visualblocks visualchars code fullscreen",
				"insertdatetime media nonbreaking save table contextmenu directionality",
				"emoticons template paste textcolor colorpicker textpattern",
			],
			'toolbar' => "undo redo | styleselect fontsizeselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media | forecolor backcolor emoticons",
			'content_css' => Yii::getAlias('@backendUrl').'/css/bootstrap.min.css',
			'image_advtab' => true,
			'filemanager_crossdomain' => true,
			'external_filemanager_path' => Yii::getAlias('@storageUrl').'/filemanager/',
			'filemanager_title' => 'Responsive Filemanager',
			'external_plugins' => array('filemanager' => Yii::getAlias('@storageUrl').'/filemanager/plugin.min.js')
		]
	]) ?>
	
	

	<div class="modal-header" style="margin-bottom: 15px;">
		<h4 class="modal-title" id="itemModalLabel">Share</h4>
	    </div>
	
	<?= $form->field($model, 'public')->checkbox() ?>
	<?php
	if($model->public==1){
	    if($model->approved==1){
		echo '<code>Approved.</code><br><br>';
	    } else {
		echo '<code>Waiting for approval.</code><br><br>';
	    }
	}
	?>
	<?php
	$userlist = \backend\modules\ezforms\components\EzformQuery::getIntUserAll(); //explode(",", $model1->assign);
       
	?>
	<?= $form->field($model, 'share')->widget(\kartik\select2\Select2::className(), [
		    'options' => ['placeholder' => 'แชร์', 'multiple' => true],
		    'data' => \yii\helpers\ArrayHelper::map($userlist, 'id', 'text'),
		    'pluginOptions' => [
                        'tokenSeparators' => [',', ' '],
                        'initValueText'=>$userlist_text,
		    ],
		]) ?>
	<div id="div-custom">
	<?php if($gtype == 0):?>
	
	<div class="modal-header" style="margin-bottom: 15px;">
		<h4 class="modal-title" id="itemModalLabel">ตั้งค่า</h4>
	    </div>
	<div class="row">
	    <div class="col-md-5 ">
		
		<?= $form->field($model, 'field_name')->widget(Select2::className(), [
		    'options' => ['placeholder' => 'เลือกฟิลด์', 'multiple' => true],
		    'data' => $dataFields,
		    'pluginOptions' => [
			    'tags' => true,
		    ],
		]) ?>
	    </div>
	    <div class="col-md-5 sdbox-col">
		<?= $form->field($model, 'order_field')->widget(Select2::className(), [
		    'options' => ['placeholder' => 'เลือกฟิลด์', 'multiple' => true],
		    'data' => $dataFields,
		    'pluginOptions' => [
			    'tags' => true,
		    ],
		]) ?>
	    </div>
	   <div class="col-md-2 sdbox-col">
		<?= $form->field($model, 'label_field')->dropDownList(['asc' => 'จากน้อยไปมาก', 'desc' => 'จากมากไปน้อย']) ?>
	    </div>
	</div>
	
	<div class="row">
	    <div class="col-md-5 ">
		<?= $form->field($model, 'fixcol')->textInput(['type'=>'number']) ?>
	    </div>
	</div>
	
	
	<div class="panel panel-primary">
	    <div class="panel-heading"><i class="fa fa-thumb-tack" aria-hidden="true"></i> เพิ่มตัวแปรสำหรับกำกับงาน</div>
	    <div class="panel-body">
		<div class="row">
		    <div class="col-md-3 "><label>Field</label></div>
		    <div class="col-md-3 sdbox-col"><label>Label</label></div>
		    <div class="col-md-1 sdbox-col"><label>Width</label></div>
		    <div class="col-md-1 sdbox-col"><label>PDF Width</label></div>
		    <div class="col-md-2 sdbox-col"><label>Align</label></div>
		    <div class="col-md-1 sdbox-col"><label>Show in PDF?</label></div>
		</div>
		<div class="fields-items">
		<?php
		
		$enable_field = $model->enable_field;
		if(isset($enable_field) && is_array($enable_field) && !empty($enable_field)){
		    $field = isset($enable_field['field'])?$enable_field['field']:[];
		    $label = isset($enable_field['label'])?$enable_field['label']:[];
		    $width = isset($enable_field['width'])?$enable_field['width']:[];
		    $rwidth = isset($enable_field['rwidth'])?$enable_field['rwidth']:[];
		    $align = isset($enable_field['align'])?$enable_field['align']:[];
		    $report = isset($enable_field['report'])?$enable_field['report']:[];
		    $type = isset($enable_field['type'])?$enable_field['type']:[];
		    $id = isset($enable_field['id'])?$enable_field['id']:[];
		    $orglabel = isset($enable_field['orglabel'])?$enable_field['orglabel']:[];
		    
		    foreach ($field as $key_field => $value_field) {
		?>
		    <div class="row" style="margin-bottom: 15px;">
			<div class="col-md-3 "><?= Html::dropDownList('fields[field][]', $value_field, $dataFields, ['class'=>'form-control field-input'])?></div>
			<div class="col-md-3 sdbox-col"><input type="text" class="form-control label-input" name="fields[label][]" value="<?=$label[$key_field]?>"></div>
			<div class="col-md-1 sdbox-col"><input type="number" class="form-control width-input" name="fields[width][]" value="<?=$width[$key_field]?>"></div>
			<div class="col-md-1 sdbox-col"><input type="number" class="form-control rwidth-input" name="fields[rwidth][]" value="<?=$rwidth[$key_field]?>"></div>
			<div class="col-md-2 sdbox-col"><?= Html::dropDownList('fields[align][]', $align[$key_field], ['left'=>'ชิดซ้าย', 'center'=>'กึ่งกลาง', 'right'=>'ชิดขวา'], ['class'=>'form-control align-input'])?></div>
			<div class="col-md-1 sdbox-col"><?= Html::dropDownList('fields[report][]', $report[$key_field], ['Y'=>'Yes', 'N'=>'No'], ['class'=>'form-control report-input'])?></div>
			<div class="col-md-1 sdbox-col"><button type="button" class="fields-items-del btn btn-danger"><i class="glyphicon glyphicon-remove"></i></button></div>
			<?=  Html::hiddenInput('fields[type][]', $type[$key_field], ['class'=>'type-input']);?>
			<?=  Html::hiddenInput('fields[id][]', $id[$key_field], ['class'=>'id-input']);?>
			<?=  Html::hiddenInput('fields[orglabel][]', $orglabel[$key_field], ['class'=>'orglabel-input']);?>
		    </div>
		<?php
		    }
		}
	       ?>
		</div>
		<div class="row add-fields">
		    <div class="col-md-3 "><input type="text" class="form-control" name="fields[field][]" disabled="disabled" value="Field"></div>
		    <div class="col-md-3 sdbox-col"><input type="text" class="form-control" name="fields[label][]" disabled="disabled" value="Label"></div>
		    <div class="col-md-1 sdbox-col"><input type="text" class="form-control" name="fields[width][]" disabled="disabled" value="Width"></div>
		    <div class="col-md-1 sdbox-col"><input type="text" class="form-control" name="fields[rwidth][]" disabled="disabled" value="Report Width"></div>
		    <div class="col-md-2 sdbox-col"><input type="text" class="form-control" name="fields[align][]" disabled="disabled" value="Align"></div>
		    <div class="col-md-1 sdbox-col"><input type="text" disabled="disabled" class="form-control" name="fields[report][]" value="Report"></div>
		    <div class="col-md-1 sdbox-col"><button type="button" data-url="<?=  Url::to(['/inv/inv-person/get-widget', 'view'=>'_widget_field'])?>" class="fields-items-add btn btn-success"><i class="glyphicon glyphicon-plus"></i></button></div>
		</div>
	    </div>
	</div>
	
	<div class="panel panel-default" style="border-color: #e08e0b;">
	    <div class="panel-heading" style="background-color: #f39c12; color: #FFF;"><i class="fa fa-file-text-o" aria-hidden="true"></i> เพิ่มฟอร์มสำหรับกำกับงาน</div>
	    <div class="panel-body">
		
		<div id="dad-box" class="forms-items">
		    <?php
		
		    $enable_form = $model->enable_form;
//		    \yii\helpers\VarDumper::dump($enable_form,10,true);
//									    exit();
		    if(isset($enable_form) && is_array($enable_form) && !empty($enable_form)){
			$formArry = isset($enable_form['form'])?$enable_form['form']:[];
			$label = isset($enable_form['label'])?$enable_form['label']:[];
			$date = isset($enable_form['date'])?$enable_form['date']:[];
			$width = isset($enable_form['width'])?$enable_form['width']:[];
			$rwidth = isset($enable_form['rwidth'])?$enable_form['rwidth']:[];
			$visible = isset($enable_form['visible'])?$enable_form['visible']:[];
			$report = isset($enable_form['report'])?$enable_form['report']:[];
			$condition = isset($enable_form['condition'])?$enable_form['condition']:[];
			$display = isset($enable_form['display'])?$enable_form['display']:[];
			$show = isset($enable_form['show'])?$enable_form['show']:[];
			$result = isset($enable_form['result'])?$enable_form['result']:[];
			
			$fd = isset($enable_form['field_detail'])?$enable_form['field_detail']:[];
			$en = isset($enable_form['ezf_name'])?$enable_form['ezf_name']:[];
			$et = isset($enable_form['ezf_table'])?$enable_form['ezf_table']:[];
			$ct = isset($enable_form['comp_id_target'])?$enable_form['comp_id_target']:[];
			$ur = isset($enable_form['unique_record'])?$enable_form['unique_record']:[];
			
			$bgcolor = '#fcf8e3';
			$border_color = '#faebcc';
			
			foreach ($formArry as $key_form => $value_form) {
			    $visible_value = isset($visible[$key_form])?$visible[$key_form]:'';
			    $dataDateFieldsResult = ArrayHelper::map(InvQuery::getFields($value_form), 'id', 'name');
			    
			    $dataDateFieldsResult = ArrayHelper::merge([''=>'-----เลือกฟิลด์-----'], $dataDateFieldsResult);
                            
                            
                            
                            $listText = $dataSubForm['ฟอร์มที่ฉันสร้าง'][$value_form];
                            if(!$listText){
                                $listText = $dataSubForm['ฟอร์มที่ฉันเลือก Favorite'][$value_form];
                                if(!$listText){
                                    $listText = $dataSubForm['ฟอร์มที่ถูกแชร์มาให้ฉันหรือสาธารณะ'][$value_form];
                                    if(!$listText){
                                        $dataEzform = InvQuery::getEzformById($value_form);
                                        $listText = $dataEzform['ezf_name'];
                                    }
                                }
                            }
                            
                            $dataSubFormfix = ArrayHelper::merge([$value_form=>$listText.' (ปัจจุบัน)'], $dataSubForm);
		    ?>
                   
			<div id="<?=$genid?>" class="well dads-children" style="padding: 15px; background-color: <?=$bgcolor?>; border-color: <?=$border_color?>; ">
			    <div class="row draggable">
				<div class="col-md-2 "><label>Form</label></div>
				<div class="col-md-2 sdbox-col"><label>Label</label></div>
				<div class="col-md-2 sdbox-col"><label>Date Field</label></div>
				<div class="col-md-1 sdbox-col"><label>Width</label></div>
				<div class="col-md-1 sdbox-col"><label>PDF Width</label></div>
				<div class="col-md-2 sdbox-col"><label>Visible Condition</label></div>
				<div class="col-md-1 sdbox-col"><label>Show in PDF?</label></div>
			    </div>
				
			    <div class="row form-fields" style="margin-bottom: 15px;">
				<div class="col-md-2 "><?= Html::dropDownList('forms[form][]', $value_form, $dataSubFormfix, ['class'=>'form-control form-input'])?></div>
				<div class="col-md-2 sdbox-col"><input type="text" class="form-control label-input" name="forms[label][]" value="<?=$label[$key_form]?>"></div>
				<div class="col-md-2 sdbox-col"><?= Html::dropDownList('forms[date][]', $date[$key_form], $dataDateFieldsResult, ['class'=>'form-control date-input'])?></div>
				<div class="col-md-1 sdbox-col"><input type="number" class="form-control width-input" name="forms[width][]" value="<?=$width[$key_form]?>"></div>
				<div class="col-md-1 sdbox-col"><input type="number" class="form-control rwidth-input" name="forms[rwidth][]" value="<?=$rwidth[$key_form]?>"></div>
				<div class="col-md-2 sdbox-col"><?= yii\helpers\Html::dropDownList('forms[visible][]', $visible_value, ['แสดงผล', 'แสดงผลตามเงื่อนไข'], ['class'=>'form-control visible-input'])?></div>
				<div class="col-md-1 sdbox-col"><?= yii\helpers\Html::dropDownList('forms[report][]', $report[$key_form], ['Y'=>'Yes', 'N'=>'No'], ['class'=>'form-control report-input'])?></div>
				<div class="col-md-1 sdbox-col"><button type="button" class="forms-items-del btn btn-danger"><i class="glyphicon glyphicon-remove"></i></button></div>
				<?=  Html::hiddenInput('forms[field_detail][]', $fd[$key_form], ['class'=>'fd-input']);?>
				<?=  Html::hiddenInput('forms[ezf_name][]', $en[$key_form], ['class'=>'en-input']);?>
				<?=  Html::hiddenInput('forms[ezf_table][]', $et[$key_form], ['class'=>'et-input']);?>
				<?=  Html::hiddenInput('forms[comp_id_target][]', $ct[$key_form], ['class'=>'ct-input']);?>
				<?=  Html::hiddenInput('forms[unique_record][]', $ur[$key_form], ['class'=>'ur-input']);?>
			    </div>
			    
			    <div class="condition-view" style="padding: 15px; border-top: 1px solid <?=$border_color?>;">
				<div class="row">
				    <div class="col-md-2 sdbox-col"><label>Form</label></div>
				    <div class="col-md-2 sdbox-col"><label>Field</label></div>
				    <div class="col-md-2 sdbox-col"><label>Condition</label></div>
				    <div class="col-md-2 sdbox-col"><label>Value1</label></div>
				    <div class="col-md-2 sdbox-col"><label>Value2</label></div>
				    <div class="col-md-1 sdbox-col"><label>More</label></div>
				</div>
				
				<div class="forms-condition-items">
				    <?php

				    if(isset($condition) && is_array($condition) && !empty($condition)){
					$condItems = isset($condition[$value_form])?$condition[$value_form]:[];
					
					$formCond = isset($condItems['form'])?$condItems['form']:[];
					$value1Cond = isset($condItems['value1'])?$condItems['value1']:[];
					$value2Cond = isset($condItems['value2'])?$condItems['value2']:[];
					$fieldCond = isset($condItems['field'])?$condItems['field']:[];
					$condCond = isset($condItems['cond'])?$condItems['cond']:[];
					$moreCond = isset($condItems['more'])?$condItems['more']:[];
					
					
					
					foreach ($formCond as $key_cond => $value_cond) {
						$dataFieldsResult = ArrayHelper::map(InvQuery::getFields($value_cond), 'id', 'name');
                                                $dataFieldsResult['rstat']='rstat';
                                                
                                                $listSubText = $dataSubForm['ฟอร์มที่ฉันสร้าง'][$value_cond];
                                                if(!$listSubText){
                                                    $listSubText = $dataSubForm['ฟอร์มที่ฉันเลือก Favorite'][$value_cond];
                                                    if(!$listSubText){
                                                        $listSubText = $dataSubForm['ฟอร์มที่ถูกแชร์มาให้ฉันหรือสาธารณะ'][$value_cond];
                                                        if(!$listSubText){
                                                            $dataEzform = InvQuery::getEzformById($value_cond);
                                                            $listSubText = $dataEzform['ezf_name'];
                                                        }
                                                    }
                                                }
                                                
                                                $dataSubFormAllfix = ArrayHelper::merge([$value_cond=>$listSubText.' (ปัจจุบัน)'], $dataSubFormAll);
				    ?>
					<div class="row" style="margin-bottom: 15px;">
					    <div class="col-md-2 sdbox-col"><?= Html::dropDownList('forms[condition]['.$value_form.'][form][]', $value_cond, $dataSubFormAllfix, ['class'=>'form-control cform-input'])?></div>
					    <div class="col-md-2 sdbox-col"><?= Html::dropDownList('forms[condition]['.$value_form.'][field][]', $fieldCond[$key_cond], $dataFieldsResult, ['class'=>'form-control cfield-input'])?></div>
					    <div class="col-md-2 sdbox-col"><?= Html::dropDownList('forms[condition]['.$value_form.'][cond][]', $condCond[$key_cond], ['=='=>'=', '<'=>'<', '>'=>'>', '<='=>'<=', '>='=>'>=', '!='=>'!=', 'between'=>'Between' ,'func'=>'PHP Function'], ['class'=>'form-control ccond-input'])?></div>
					    <div class="col-md-2 sdbox-col"><input type="text" class="form-control cvalue1-input" name="forms[condition][<?=$value_form?>][value1][]" value="<?=$value1Cond[$key_cond]?>"></div>
					    <div class="col-md-2 sdbox-col"><input type="text" <?=$condCond[$key_cond]!='between'?'readonly="readonly"':'';?> class="form-control cvalue2-input" name="forms[condition][<?=$value_form?>][value2][]" value="<?=$value2Cond[$key_cond]?>"></div>
					    <div class="col-md-1 sdbox-col ccondmore"><?= yii\helpers\Html::dropDownList('forms[condition]['.$value_form.'][more][]', $moreCond[$key_cond], ['||'=>'OR', '&&'=>'AND'], ['class'=>'form-control cmore-input'])?></div>
					    <div class="col-md-1 sdbox-col"><button type="button" class="forms-condition-del btn btn-danger"><i class="glyphicon glyphicon-remove"></i></button></div>
					</div>
				    <?php
					}
				    }
				    ?>
				</div>

				<div class="row" style="margin-bottom: 15px;">
				    <div class="col-md-2 sdbox-col"><input type="text" class="form-control " disabled="disabled" name="forms[result][condition][form][]" value="Form"></div>
				    <div class="col-md-2 sdbox-col"><input type="text" class="form-control " disabled="disabled" name="forms[result][condition][field][]" value="Field"></div>
				    <div class="col-md-2 sdbox-col"><input type="text" class="form-control " disabled="disabled" name="forms[result][condition][cond][]" value="Condition"></div>
				    <div class="col-md-2 sdbox-col"><input type="text" class="form-control " disabled="disabled" name="forms[result][condition][value][]" value="Value1"></div>
				    <div class="col-md-2 sdbox-col"><input type="text" class="form-control " disabled="disabled" name="forms[result][condition][value][]" value="Value2"></div>
				    <div class="col-md-1 sdbox-col"><input type="text" class="form-control " disabled="disabled" name="forms[result][condition][value][]" value="More"></div>
				    <div class="col-md-1 sdbox-col"><button type="button" data-url="<?=  Url::to(['/inv/inv-person/get-widget', 'view'=>'_widget_form_condition'])?>" class="forms-condition-add btn btn-success"><i class="glyphicon glyphicon-plus"></i></button></div>
				</div>
			    </div>
			    
			    <div class="row" style="padding: 15px 0; border-top: 1px solid <?=$border_color?>;">
				<div class="col-md-3 "><label>Show Items</label></div>
				<div class="col-md-3 "><label></label></div>
			    </div>
			    
			    <div class="row form-fields" style="margin-bottom: 15px;">
				<div class="col-md-3 "><?= Html::dropDownList('forms[show][]', $show[$key_form], ['default'=>'แสดงผลแบบมาตรฐาน [#Save Draft/#Submited]+','detail'=>'แสดงผลตามตัวแปรลักษณะหลักของฟอร์ม','condition'=>'แสดงผลแบบเงื่อนไข','custom'=>'แสดงผลแบบกำหนดเอง'], ['class'=>'form-control show-input', 'data-id'=>$value_form])?></div>
				<div class="col-md-3 sdbox-col result-box <?=($show[$key_form]!='condition'?'display-none':'')?>"><?= Html::dropDownList('forms[result][]', $result[$key_form], $dataDateFieldsResult, ['class'=>'form-control result-input '])?></div>
				<div class="col-md-9 varshow"></div>
			    </div>
			    
			    <div class="echo-view <?=(!in_array($show[$key_form], ['condition', 'custom'])?'display-none':'')?>" style="padding: 15px; border-top: 1px solid <?=$border_color?>;">
				
				<?php
				if(isset($display) && is_array($display) && !empty($display)){
					$displayItems = isset($display[$value_form])?$display[$value_form]:[];
					$dcustom = isset($displayItems['custom'])?$displayItems['custom']:[];
					
				    if($show[$key_form]=='condition'){
				?>
				    <div class="display-condition-view" >
					<div class="row">
					    <div class="col-md-2"><label>Condition</label></div>
					    <div class="col-md-2 sdbox-col"><label>Value1</label></div>
					    <div class="col-md-2 sdbox-col"><label>Value2</label></div>
					    <div class="col-md-2 sdbox-col"><label>Icon</label></div>
					    <div class="col-md-2 sdbox-col"><label>Label</label></div>
					</div>

					<div class="forms-display-condition-items">
					    <?php
									    
						$dcond = isset($displayItems['condition'])?$displayItems['condition']:[];
						$dvalue1 = isset($displayItems['value1'])?$displayItems['value1']:[];
						$dvalue2 = isset($displayItems['value2'])?$displayItems['value2']:[];
						$dicon = isset($displayItems['icon'])?$displayItems['icon']:[];
						$dlabel = isset($displayItems['label'])?$displayItems['label']:[];

						foreach ($dcond as $key_dcond => $value_dcond) {
						   
					    ?>
						    <div class="row" style="margin-bottom: 15px;">
							<div class="col-md-2 sdbox-col"><?= Html::dropDownList('forms[display]['.$value_form.'][condition][]', $value_dcond, ['=='=>'=', '<'=>'<', '>'=>'>', '<='=>'<=', '>='=>'>=', '!='=>'!=', 'between'=>'Between','func'=>'PHP Function'], ['class'=>'form-control dcond-input'])?></div>
							<div class="col-md-2 sdbox-col"><input type="text" value="<?=$dvalue1[$key_dcond]?>" class="form-control dvalue1-input" name="forms[display][<?=$value_form?>][value1][]" ></div>
							<div class="col-md-2 sdbox-col"><input type="text" value="<?=$dvalue2[$key_dcond]?>" class="form-control dvalue2-input" <?=$value_dcond!='between'?'readonly="readonly"':'';?> name="forms[display][<?=$value_form?>][value2][]" ></div>
							<div class="col-md-2 sdbox-col"><input type="text" value="<?=$dicon[$key_dcond]?>" class="form-control dicon-input" name="forms[display][<?=$value_form?>][icon][]" ></div>
							<div class="col-md-2 sdbox-col"><input type="text" value="<?=$dlabel[$key_dcond]?>" class="form-control dlabel-input" name="forms[display][<?=$value_form?>][label][]" ></div>
							<div class="col-md-2 sdbox-col"><button type="button" class="forms-display-del btn btn-danger"><i class="glyphicon glyphicon-remove"></i></button></div>
						    </div>
					    <?php }?>
					</div>

					<div class="row" style="margin-bottom: 15px;">
					    <div class="col-md-2"><input type="text" class="form-control" disabled="disabled" value="Condition"></div>
					    <div class="col-md-2 sdbox-col"><input type="text" class="form-control" disabled="disabled" value="Value1"></div>
					    <div class="col-md-2 sdbox-col"><input type="text" class="form-control" disabled="disabled" value="Value2"></div>
					    <div class="col-md-2 sdbox-col"><input type="text" class="form-control" disabled="disabled" value="Icon"></div>
					    <div class="col-md-2 sdbox-col"><input type="text" class="form-control" disabled="disabled" value="Label"></div>
					    <div class="col-md-2 sdbox-col"><button type="button" data-url="<?=  Url::to(['/inv/inv-person/get-widget', 'view'=>'_widget_form_display'])?>" class="forms-display-add btn btn-success"><i class="glyphicon glyphicon-plus"></i></button></div>
					</div>
				    </div>
				<?php } elseif ($show[$key_form]=='custom') { ?>
				    
				    <textarea placeholder="เป็นส่วนของคำสั่ง SQL เช่น 'CCA-02 Ultrasound on ','[',f2v1,']' หากเป็นข้อความให้ใส่ในเครื่องหมาย Single quote " name="forms[display][<?=$value_form?>][custom]" class="form-control dcustom-input" rows="3"><?=$dcustom?></textarea>
				<?php }  
				} ?>
			    </div>
			</div>
		    <?php 
				if($bgcolor=='#fcf8e3'){
				    $bgcolor = '#dff0d8';
				    $border_color = '#d6e9c6';
				} else {
				    $bgcolor = '#fcf8e3';
				    $border_color = '#faebcc';
				}
			}
		    }
		   ?>
		</div>
		    
		<div class="row add-forms">
		    <div class="col-md-10"></div>
		    <div class="col-md-2 sdbox-col" style="text-align: right;"><button type="button" data-url="<?=  Url::to(['/inv/inv-person/get-widget', 'view'=>'_widget_form'])?>" class="forms-items-add btn btn-success"><i class="glyphicon glyphicon-plus"></i> เพิ่มฟอร์ม</button></div>
		</div>
	    </div>
	</div>
	<?php elseif($gtype == 2):?>
            <?= $form->field($model, 'field_name')->hiddenInput()->label(FALSE) ?>
        <?= $form->field($model, 'label_field')->hiddenInput()->label(FALSE) ?>
        <?= $form->field($model, 'order_field')->hiddenInput()->label(FALSE) ?>
	
	<div class="modal-header" style="margin-bottom: 15px;">
		<h4 class="modal-title" id="itemModalLabel">ตั้งค่า</h4>
	    </div>
        <div class="panel panel-primary">
            <div class="panel-heading" ><i class="fa fa-file-text-o" aria-hidden="true"></i> ตั้งค่าแผนที่</div>
            <div class="panel-body">
                
                <div class="row" style="padding-left: 30px;padding-right: 30px;">
                    <div class="col-md-12">
                        <label class="control-label">ตำแหน่งเริ่มต้น</label>
                        <?php
                        $enable_field = $model->enable_field;
                        $lat_init=null;
                        $lng_init=null;
                        $zoom_init=9;

                        if(isset($enable_field) && is_array($enable_field) && !empty($enable_field)){
                            $lat_init = $enable_field['lat_init'];
                            $lng_init = $enable_field['lng_init'];
                            $zoom_init = $enable_field['zoom_init'];
                        }
                            echo \common\lib\sdii\widgets\MapInput::widget([
                                'lat'=>'lat_init',
                                'lng'=>'lng_init',
                                'latValue'=>$lat_init,
                                'lngValue'=>$lng_init,
                            ]);
                            echo Html::hiddenInput('fields[lat_init]', $lat_init, ['id'=>'lat_init']);
                            echo Html::hiddenInput('fields[lng_init]', $lng_init, ['id'=>'lng_init']);
                        ?>
                    </div>
                    
                </div>
                <div class="row" style="padding-left: 30px;padding-right: 30px;margin-top: 15px;">
                    <div class="col-md-12">
                        <label class="control-label">Zoom <code>ตัวเลขยิ่งมากยิ่งซูมมาก</code></label>
                        <?= Html::dropDownList('fields[zoom_init]', $zoom_init, [7=>7,8=>8,9=>9,10=>10,11=>11,12=>12,13=>13,14=>14,15=>15,16=>16,17=>17,18=>18], ['class'=>'form-control'])?>
                    </div>
                </div>
            </div>
        </div>
            
	<div class="panel panel-default" style="border-color: #e08e0b;">
	    <div class="panel-heading" style="background-color: #f39c12; color: #FFF;"><i class="fa fa-file-text-o" aria-hidden="true"></i> เพิ่มฟอร์ม</div>
	    <div class="panel-body">
		
                <div class="row" style="padding-left: 30px;padding-right: 30px;">
				<div class="col-md-2 "><label>Form</label></div>
				<div class="col-md-2 sdbox-col"><label>Label</label></div>
                                <div class="col-md-1 sdbox-col"><label>Date</label></div>
				<div class="col-md-1 sdbox-col"><label>Icon</label></div>
				<div class="col-md-1 sdbox-col"><label>Color</label></div>
			    </div>
                <div id="dad-box" class="forms-items">
		    <?php
		
		    $enable_form = $model->enable_form;
//		    \yii\helpers\VarDumper::dump($enable_form,10,true);
//									    exit();
		    if(isset($enable_form) && is_array($enable_form) && !empty($enable_form)){
			
			foreach ($enable_form as $key_form => $value_form) {
                            $dataDateFieldsResult = ArrayHelper::map(InvQuery::getFields($value_form['form']), 'id', 'name');
			    
			    $dataDateFieldsResult = ArrayHelper::merge(['create_date'=>'Create Date'], $dataDateFieldsResult);
		    ?>
			    <div id="<?=$key_form?>" class="row form-fields dads-children" data-id="<?=$key_form?>" style="margin-bottom: 15px;">
                                <div class="col-md-12 draggable text-center" style="color: #999; padding-bottom: 10px; cursor: -webkit-grab;"><i class="glyphicon glyphicon-resize-vertical"></i><i class="glyphicon glyphicon-resize-vertical"></i><i class="glyphicon glyphicon-resize-vertical"></i></div>
				<div class="col-md-2 "><?= Html::dropDownList("forms[$key_form][form]", $value_form['form'], $dataSubForm, ['class'=>'form-control form-input'])?></div>
				<div class="col-md-2 sdbox-col"><input type="text" class="form-control label-input" name="forms[<?=$key_form?>][label]" value="<?=$value_form['label']?>"></div>
                                <div class="col-md-1 sdbox-col"><?= Html::dropDownList("forms[$key_form][date]", $value_form['date'], $dataDateFieldsResult, ['class'=>'form-control date-input'])?></div>
				<div class="col-md-1 sdbox-col"><div class="input-group iconpicker-container"><input type="text" class="form-control dicon-input icp icp-auto iconpicker-input iconpicker-element" name="forms[<?=$key_form?>][icon]" value="<?=$value_form['icon']?>" ><span class="input-group-addon"><i></i></span></div></div>
                                <div class="col-md-1 sdbox-col"><?=  Html::textInput("forms[$key_form][color]", $value_form['color'], ['class'=>'form-control input-color'])?></div>
                                <div class="col-md-1 sdbox-col"><?= Html::checkbox("forms[$key_form][show]", $value_form['show'], ['label'=>'แสดงทันที'])?></div>
                                <div class="col-md-1 sdbox-col"><?= Html::checkbox("forms[$key_form][adddata]", $value_form['adddata'], ['label'=>'เพิ่มข้อมูลได้'])?></div>
                                <div class="col-md-1 sdbox-col"><?= Html::checkbox("forms[$key_form][conddate]", $value_form['conddate'], ['label'=>'แสดงตามช่วงเวลา'])?></div>
                                <div class="col-md-1 sdbox-col"><?= Html::checkbox("forms[$key_form][addperson]", $value_form['addperson'], ['label'=>'เพิ่มผู้ป่วยได้'])?></div>
                                <div class="col-md-1 sdbox-col">
                                    <button type="button" class="forms-items-del btn btn-danger"><i class="glyphicon glyphicon-remove"></i></button>
                                </div>
				<?=  Html::hiddenInput("forms[$key_form][ezf_name]", $value_form['ezf_name'], ['class'=>'en-input']);?>
				<?=  Html::hiddenInput("forms[$key_form][ezf_table]", $value_form['ezf_table'], ['class'=>'et-input']);?>
				<?=  Html::hiddenInput("forms[$key_form][unique_record]", $value_form['unique_record'], ['class'=>'ur-input']);?>
			    </div>
		    <?php 
			}
		    }
		   ?>
		</div>
		    
		<div class="row add-forms">
		    <div class="col-md-10"></div>
		    <div class="col-md-2 sdbox-col" style="text-align: right;"><button type="button" data-url="<?=  Url::to(['/inv/inv-person/get-widget', 'view'=>'_widget_map'])?>" class="forms-items-add btn btn-success"><i class="glyphicon glyphicon-plus"></i> เพิ่มฟอร์ม</button></div>
		</div>
	    </div>
	</div>
            <?php endif;?>
	</div>
	<?= $form->field($model, 'active')->hiddenInput()->label(FALSE) ?>
	<?= $form->field($model, 'created_by')->hiddenInput()->label(FALSE) ?>
	<?= $form->field($model, 'created_at')->hiddenInput()->label(FALSE) ?>
	<?= $form->field($model, 'updated_by')->hiddenInput()->label(FALSE) ?>
	<?= $form->field($model, 'updated_at')->hiddenInput()->label(FALSE) ?>
	<?= $form->field($model, 'special')->hiddenInput()->label(FALSE) ?>
	<?= $form->field($model, 'sitecode')->hiddenInput()->label(FALSE) ?>
	<?= $form->field($model, 'main_ezf_id')->hiddenInput()->label(FALSE) ?>
	<?= $form->field($model, 'main_ezf_name')->hiddenInput()->label(FALSE) ?>
	<?= $form->field($model, 'main_ezf_table')->hiddenInput()->label(FALSE) ?>
	<?= $form->field($model, 'main_comp_id_target')->hiddenInput()->label(FALSE) ?>
	<?= $form->field($model, 'ezf_id')->hiddenInput()->label(FALSE) ?>
	<?= $form->field($model, 'ezf_name')->hiddenInput()->label(FALSE) ?>
	<?= $form->field($model, 'ezf_table')->hiddenInput()->label(FALSE) ?>
	<?= $form->field($model, 'comp_id_target')->hiddenInput()->label(FALSE) ?>
	<?= $form->field($model, 'pk_field')->hiddenInput()->label(FALSE) ?>
	
        <?= $form->field($model, 'subname')->hiddenInput()->label(false) ?>
	<?= $form->field($model, 'subicon')->hiddenInput()->label(false) ?>
        
	<?= $form->field($model, 'enable_field')->hiddenInput()->label(false) ?>
	<?= $form->field($model, 'enable_form')->hiddenInput()->label(false) ?>
	<?= $form->field($model, 'enable_report')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'parent_gid')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'gsystem')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'gtype')->hiddenInput()->label(false) ?>
        
    </div>
    <div class="modal-footer">
	<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary', 'name'=>'action_submit', 'value'=>'submit']) ?>
	<?php
	    if(!$model->isNewRecord){
		echo Html::submitButton('Delete', ['class' => 'btn btn-danger', 'name'=>'action_submit', 'value'=>'del', 'data' => [
		'confirm' => "คุณแน่ใจที่จะต้องการลบข้อมูลนี้หรือไม่",
		'method' => 'post',
	    ],]);
	    }
	?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?=  ModalForm::widget([
    'id' => 'modal-inv-gen',
    //'size'=>'modal-lg',
]);
?>

<?php  
backend\modules\ezforms2\assets\EzfColorInputAsset::register($this);
backend\assets\IconAsset::register($this);
echo \common\lib\sdii\widgets\SDModalForm::widget([
    'id' => 'modal-query-request',
    'size' => 'modal-lg',
    'tabindexEnable' => false
]);
$this->registerJsFile('/jqueryloading/jloading.js',['depends' => [\yii\web\JqueryAsset::className()]]);

$generalJs = "
$('.echo-view .dicon-input').iconpicker({
    title: 'Using Font Awesome',
    icons: ['bed', 'bug', 'bolt', 'ban', 'book', 'bell', 'birthday-cake', 'bookmark', 'building', 'calculator', 'calendar',
    'bus', 'camera', 'car', 'check', 'times', 'check-circle', 'check-circle-o', 'circle', 'circle-o', 'clock-o', 'child', 'cloud',
    'coffee', 'cube', 'cubes', 'cutlery', 'envelope-o', 'diamond', 'exclamation-circle', 'exchange', 'female', 'male', 'flask',
    'folder-open-o', 'folder-o', 'users', 'user', 'heartbeat', 'heart-o', 'heart', 'gift', 'globe', 'picture-o', 'minus-circle',
    'phone', 'question-circle', 'plus-circle', 'shield', 'share-alt', 'star', 'star-o', 'thumbs-o-up', 'thumbs-o-down', 'times-circle',
    'unlock-alt', 'unlock', 'wrench', 'trophy', 'trash', 'tree', 'life-ring', 
    'shopping-cart', 'paper-plane', 'search', 'retweet', 'random', 'wheelchair', 'user-md', 'stethoscope', 'hospital-o', 'medkit',
    'h-square', 'ambulance', 'link', 'chain-broken'
    ],
    iconBaseClass: 'fa',
    iconComponentBaseClass: 'fa',
    iconClassPrefix: 'fa-'
});

$('.fields-items').on('click', '.fields-items-del', function(){
    $(this).parent().parent().remove();
});

$('.forms-items').on('click', '.forms-items-del', function(){
    $(this).parent().parent().parent().remove();
});

$('.forms-items').on('click', '.forms-condition-del', function(){
    $(this).parent().parent().remove();
});

$('.forms-items').on('click', '.forms-display-del', function(){
    $(this).parent().parent().remove();
});

$('.forms-items').on('change', '.ccond-input', function(){
    if($(this).val()=='between'){
	$(this).parent().parent().find('.cvalue2-input').removeAttr('readonly');
    } else {
	$(this).parent().parent().find('.cvalue2-input').attr('readonly','readonly');
	$(this).parent().parent().find('.cvalue2-input').val('');
    }
});

$('.forms-items').on('change', '.ccond-input', function(){
    if($(this).val()=='between'){
	$(this).parent().parent().find('.cvalue2-input').removeAttr('readonly');
    } else {
	$(this).parent().parent().find('.cvalue2-input').attr('readonly','readonly');
	$(this).parent().parent().find('.cvalue2-input').val('');
    }
});



$('.forms-items').on('change', '.dcond-input', function(){
    if($(this).val()=='between'){
	$(this).parent().parent().find('.dvalue2-input').removeAttr('readonly');
    } else {
	$(this).parent().parent().find('.dvalue2-input').attr('readonly','readonly');
	$(this).parent().parent().find('.dvalue2-input').val('');
    }
});

$('.forms-items').on('change', '.visible-input', function(){
    if($(this).val()==1){
	$(this).parent().parent().parent().find('.condition-view').show();
    } else {
	$(this).parent().parent().parent().find('.condition-view').hide();
	$(this).parent().parent().parent().find('.forms-condition-items').html('');
    }
});

$('.fields-items').on('change', '.field-input', function(){
    $(this).parent().parent().find('.label-input').val($(this).find('option:selected').text());
    $(this).parent().parent().find('.width-input').val(110);
    $(this).parent().parent().find('.rwidth-input').val(25);
    $(this).parent().parent().find('.align-input').val('left');
    
    getType($(this).find('option:selected').val(), $(this).parent().parent().find('.type-input') , $(this).parent().parent().find('.id-input'), $(this).parent().parent().find('.orglabel-input'));
});

$('.forms-items').on('change', '.form-input', function(){
    $(this).parent().parent().find('.label-input').val($(this).find('option:selected').text());
    getSubField($(this).find('option:selected').val(), $(this).parent().parent().find('.date-input'));
    getSubField($(this).find('option:selected').val(), $(this).parent().parent().parent().find('.result-input'));
    
    $(this).parent().parent().parent().find('.forms-condition-items .cform-input').attr('name', 'forms[condition]['+$(this).find('option:selected').val()+'][form][]');
    $(this).parent().parent().parent().find('.forms-condition-items .cfield-input').attr('name', 'forms[condition]['+$(this).find('option:selected').val()+'][field][]');
    $(this).parent().parent().parent().find('.forms-condition-items .cvalue-input').attr('name', 'forms[condition]['+$(this).find('option:selected').val()+'][vaule][]');
    $(this).parent().parent().parent().find('.forms-condition-items .ccond-input').attr('name', 'forms[condition]['+$(this).find('option:selected').val()+'][cond][]');
    
    $(this).parent().parent().parent().find('.show-input').attr('data-id', $(this).find('option:selected').val());
    
    $(this).parent().parent().parent().find('.forms-display-condition-items .dcond-input').attr('name', 'forms[display]['+$(this).find('option:selected').val()+'][condition][]');
    $(this).parent().parent().parent().find('.forms-display-condition-items .dvalue1-input').attr('name', 'forms[display]['+$(this).find('option:selected').val()+'][value1][]');
    $(this).parent().parent().parent().find('.forms-display-condition-items .dvalue2-input').attr('name', 'forms[display]['+$(this).find('option:selected').val()+'][value2][]');
    $(this).parent().parent().parent().find('.forms-display-condition-items .dicon-input').attr('name', 'forms[display]['+$(this).find('option:selected').val()+'][icon][]');
    $(this).parent().parent().parent().find('.forms-display-condition-items .dlabel-input').attr('name', 'forms[display]['+$(this).find('option:selected').val()+'][label][]');
    
    $(this).parent().parent().parent().find('.dcustom-input').attr('name', 'forms[display]['+$(this).find('option:selected').val()+'][custom]');
    
    getFormType($(this).find('option:selected').val(), $(this).parent().parent().find('.fd-input') , $(this).parent().parent().find('.en-input'), $(this).parent().parent().find('.et-input'), $(this).parent().parent().find('.ct-input'), $(this).parent().parent().find('.ur-input'));
});

$('.forms-items').on('change', '.show-input', function(){
    if($( this ).val()=='condition'){
	$(this).parent().parent().find('.result-box').removeClass('display-none');
	$(this).parent().parent().parent().find('.echo-view').removeClass('display-none');
	$(this).parent().parent().parent().find('.echo-view').html('');
	$(this).parent().parent().parent().find('.varshow').html('');
	var url = '".Url::to(['/inv/inv-person/get-widget', 'view'=>'_widget_box_display'])."';
	getWidget(url , $(this).parent().parent().parent().find('.echo-view'));
    } else if($( this ).val()=='custom') {
	$(this).parent().parent().find('.result-box').addClass('display-none');
	$(this).parent().parent().parent().find('.echo-view').removeClass('display-none');
	$(this).parent().parent().parent().find('.varshow').html('');
	$(this).parent().parent().parent().find('.echo-view').html('<textarea placeholder=\"เป็นส่วนของคำสั่ง SQL เช่น \'CCA-02 Ultrasound on \',\'[\',f2v1,\']\' หากเป็นข้อความให้ใส่ในเครื่องหมาย Single quote\" name=\"forms[display]['+$( this ).attr('data-id')+'][custom]\" class=\"form-control dcustom-input\" rows=\"3\"></textarea>');
    } else if($( this ).val()=='detail') {
	$(this).parent().parent().find('.result-box').addClass('display-none');
	$(this).parent().parent().parent().find('.echo-view').removeClass('display-none');
	$(this).parent().parent().parent().find('.echo-view').html('');
	getDetail($(this).parent().parent().parent().find('.form-input option:selected').val(), $(this).parent().parent().parent().find('.varshow'));
	
    } else {
	$(this).parent().parent().find('.result-input').val('');
	$(this).parent().parent().find('.result-box').addClass('display-none');
	$(this).parent().parent().parent().find('.echo-view').addClass('display-none');
	$(this).parent().parent().parent().find('.echo-view').html('');
	$(this).parent().parent().parent().find('.varshow').html('');
    }
});

$('.inv-gen-form').on('change', '#invgen-gtype', function(){
    setUi($(this).val());
});

$('.forms-items').on('change', '.cform-input', function(){
    getSubField($(this).find('option:selected').val(), $(this).parent().parent().find('.cfield-input'));
});

$('.add-fields').on('click', '.fields-items-add', function(){
    getWidget($(this).attr('data-url') ,$('.fields-items'));
});

$('.forms-items').on('click', '.forms-display-add', function(){
    getWidget($(this).attr('data-url') ,$(this).parent().parent().parent().find('.forms-display-condition-items'), $(this).parent().parent().parent().parent().parent().find('.form-fields option:selected').val());
});

$('.add-forms').on('click', '.forms-items-add', function(){
    getWidget($(this).attr('data-url') ,$('.forms-items'));
});

$('.forms-items').on('click', '.forms-condition-add', function(){
    getWidgetAll($(this).attr('data-url') , $(this).parent().parent().parent().find('.forms-condition-items'), $(this).parent().parent().parent().parent().find('.form-fields option:selected').val());
});

function setUi(val){
    if(val==1){
	$('#div-glink').show();
	$('#div-custom').hide();
    } else {
	$('#div-glink').hide();
	$('#div-custom').show();
    }
}

function getType(id, appendType, appendId, appendOrglabel) {
    $.ajax({
	method: 'POST',
	url: '".Url::to(['/inv/inv-person/get-type'])."',
	data: {ezf_id:'".$ezf_id."' , id:id},
	dataType: 'JSON',
	success: function(result, textStatus) {
	    $(appendType).val(result.type);
	    $(appendId).val(result.id);
	    $(appendOrglabel).val(result.orglabel);
	}
    });
}

function getFormType(id, append1, append2, append3, append4, append5) {
    $.ajax({
	method: 'POST',
	url: '".Url::to(['/inv/inv-person/get-form-type'])."',
	data: {ezf_id:id},
	dataType: 'JSON',
	success: function(result, textStatus) {
	    $(append1).val(result.fd);
	    $(append2).val(result.en);
	    $(append3).val(result.et);
	    $(append4).val(result.ct);
	    $(append5).val(result.ur);
	}
    });
}

function getWidget(url, appendId, id=0) {
    $.ajax({
	method: 'POST',
	url: url,
	data: {id:id, data:".\yii\helpers\Json::encode($dataFields).", data_sub:".\yii\helpers\Json::encode($dataSubForm).", data_select:".\yii\helpers\Json::encode($dataSelect)."},
	dataType: 'HTML',
	success: function(result, textStatus) {
	    $(appendId).append(result);
	}
    });
}

function getWidgetAll(url, appendId, id=0) {
    $.ajax({
	method: 'POST',
	url: url,
	data: {id:id, data:".\yii\helpers\Json::encode($dataFields).", data_sub:".\yii\helpers\Json::encode($dataSubFormAll).", data_select:".\yii\helpers\Json::encode($dataSelect)."},
	dataType: 'HTML',
	success: function(result, textStatus) {
	    $(appendId).append(result);
	}
    });
}

function getSubField(valField, appendId){
    if(valField === undefined || valField === null){
    
    } else {
	$.ajax({
	    method: 'POST',
	    url: '".Url::to(['/inv/inv-person/getfields'])."',
	    data: {id:valField},
	    dataType: 'HTML',
	    success: function(result, textStatus) {
		$(appendId).html(result);
	    }
	});
    }
}

function getDetail(formId, appendId){
    if(formId === undefined || formId === null){
    
    } else {
	$.ajax({
	    method: 'POST',
	    url: '".Url::to(['/inv/inv-person/get-detail'])."',
	    data: {id:formId},
	    dataType: 'HTML',
	    success: function(result, textStatus) {
		$(appendId).html(result);
	    }
	});
    }
}

$('.inv-main-form').on('click', '#modal-addbtn-inv-gen', function() {
    modalInvGen($(this).attr('data-url'));
});
$('.forms-items .visible-input').trigger('change');
";

$mapJs = "
var color_options = {
    showInput: true,
    showPalette:true,
    showPaletteOnly: true,
    hideAfterPaletteSelect:true,
    preferredFormat: 'name',
    palette: [
        ['blue','red','darkred','orange','green','darkgreen','purple','darkpuple', 'cadetblue'],
    ]
};

$('.input-color').spectrum(color_options);

$('.forms-items .dicon-input').iconpicker({
    title: 'Using Font Awesome',
    placement:'top',
    iconBaseClass: 'fa',
    iconComponentBaseClass: 'fa',
    iconClassPrefix: 'fa-'
});

$('.forms-items').on('click', '.forms-items-del', function(){
    $(this).parent().parent().remove();
});

$('.forms-items').on('change', '.form-input', function(){
    $(this).parent().parent().find('.label-input').val($(this).find('option:selected').text());
    getSubField($(this).find('option:selected').val(), $(this).parent().parent().find('.date-input'));
    
    getFormType($(this).find('option:selected').val(), $(this).parent().parent().find('.en-input'), $(this).parent().parent().find('.et-input'), $(this).parent().parent().find('.ur-input'));
});


$('.add-forms').on('click', '.forms-items-add', function(){
    getWidget($(this).attr('data-url') ,$('.forms-items'));
});

function setUi(val){
    if(val==1){
	$('#div-glink').show();
	$('#div-custom').hide();
    } else {
	$('#div-glink').hide();
	$('#div-custom').show();
    }
}

function getFormType(id, append1, append2, append3) {
    $.ajax({
	method: 'POST',
	url: '".Url::to(['/inv/inv-person/get-form-type'])."',
	data: {ezf_id:id},
	dataType: 'JSON',
	success: function(result, textStatus) {
	    $(append1).val(result.en);
	    $(append2).val(result.et);
	    $(append3).val(result.ur);
	}
    });
}

function getWidget(url, appendId, id=0) {
    $.ajax({
	method: 'POST',
	url: url,
	data: {id:id, data:".\yii\helpers\Json::encode($dataFields).", data_sub:".\yii\helpers\Json::encode($dataSubForm).", data_select:".\yii\helpers\Json::encode($dataSelect)."},
	dataType: 'HTML',
	success: function(result, textStatus) {
	    $(appendId).append(result);
            $('#dad-box').removeClass('dads-children');
            $('.sp-replacer.sp-light').removeClass('dads-children');
            
	}
    });
}


function getSubField(valField, appendId){
    if(valField === undefined || valField === null){
    
    } else {
	$.ajax({
	    method: 'POST',
	    url: '".Url::to(['/inv/inv-person/getfieldsdate'])."',
	    data: {id:valField},
	    dataType: 'HTML',
	    success: function(result, textStatus) {
		$(appendId).html(result);
	    }
	});
    }
}

";

$jsAddOn = '';
if($gtype==0){
    $jsAddOn = $generalJs;
} elseif ($gtype ==2) {
    $jsAddOn = $mapJs;
}
$this->registerJs("
$('#add-form-fav').on('click', function(){
    modalQueryRequest('/inputdata/favorite-form');
    $('#modal-query-request').on('hidden.bs.modal', function () {
        // do something…
        runBlockUI('รอสักครู่กำลังจัดรายการฟอร์มใหม่...');
        location.reload(true);
    })
});
function modalQueryRequest(url) {
    $('#modal-query-request .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
    $('#modal-query-request').modal('show')
    .find('.modal-content')
    .load(url);
}
function runBlockUI(text){
    $.blockUI({
                 message : '<span style=\"font-size : 25px; font-color:#ffffff;\">'+text+'</span>',
                 css: {
                     border: 'none',
                     padding: '15px',
                     backgroundColor: '#000',
                     '-webkit-border-radius': '10px',
                     '-moz-border-radius': '10px',
                     opacity: 1,
                     color: '#fff'
                 }});
    }
setUi($('#invgen-gtype').val());



$jsAddOn



function modalInvGen(url) {
    $('#modal-inv-gen .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
    $('#modal-inv-gen').modal('show')
    .find('.modal-content')
    .load(url);
}



$('#dad-box').dad({
    draggable:'.draggable',
    callback:function(e){
	
    }
});

");?>