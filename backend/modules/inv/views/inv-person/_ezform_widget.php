<?php

use Yii;
use yii\helpers\Html;
use kartik\select2\Select2;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;
use appxq\sdii\helpers\SDNoty;
use appxq\sdii\helpers\SDHtml;
use yii\bootstrap\Modal;

$this->registerCssFile('/css/ezform.css');

$pjax_reload = 'inv-person-grid-pjax';
if (isset(Yii::$app->session['pjax_reload'])) {
    $pjax_reload = Yii::$app->session['pjax_reload'];
}

$form = backend\modules\ezforms\components\EzActiveForm::begin([
            'id' => $formname,
            'action' => ['/inv/inv-person/ezform-save', 'id' => $dataid,
                'ezf_id' => $ezf_id,
                'dataid' => $dataid,
                'target' => $target,
                'comp_id_target' => $comp_id_target,
                'comp_target' => $comp_target,
            ],
            'options' => ['enctype' => 'multipart/form-data']
        ]);
?>
<?php
backend\assets\EzfGenAsset::register($this);

$modelDynamic = new backend\modules\ezforms\models\EzformDynamic($modelform->ezf_table);
$datamodel_table = $modelDynamic->find()->where('id = :id', [':id' => $dataid])->One();
?>    
<?php
$checkSql = 1;
if ($datamodel_table->error && $datamodel_table->rstat != 0) {
    ?>
    <?php $checkSql = 0;
    ?>

    <div class="alert alert-danger text-left">
        <span class="h2">Data Validation</span><hr><h3>กรุณาระบุข้อต่อไปนี้ (ถ้ากรอกครบแล้ว ให้กดปุ่ม [Save draft] อีกครั้งเพื่อตรวจสอบเงื่อนไขใหม่) :<br><b><?php echo $datamodel_table->error; ?></b></h3>
    </div>
    <hr>
<?php } else if ($datamodel_table->rstat == 3) { ?>
    <div class="alert alert-warning text-left">
        <span class="h2">ข้อความแจ้งเตือน</span><hr><h3>ข้อมูลในหน้านี้ถูกลบแล้ว ดังนั้นจะไม่สามารถแก้ไขข้อมูลนี้ได้ (อ่านอย่างเดียว)</h3>
    </div>
    <hr>
<?php } ?>
<?php if ($sql_announce) { ?>
    <div class="alert alert-warning text-left">
        <span class="h2">Data Announce</span><hr><h3>พบการตรวจสอบเงื่อนไขดังนี้ :<br><b><?php echo $sql_announce; ?></b></h3>
    </div>
    <hr>
<?php } ?>

<?php
echo '<div class="row">';

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                $sql = "SELECT rstat, xsourcex FROM {$modelform['ezf_table']} WHERE id=:id ";
$dataModel = Yii::$app->db->createCommand($sql, [':id' => $dataid])->queryOne();
//set data
$input_target = $target;
$input_ezf_id = $ezf_id;
$input_dataid = $dataid;


//1437377239070461302 f1v3
if (isset($modelform->comp_type) && $modelform->comp_type == 3) {

    $inputLatID = Html::getInputId($model_gen, 'sys_lat');
    $inputLngID = Html::getInputId($model_gen, 'sys_lng');
    $inputLatValue = Html::getAttributeValue($model_gen, 'sys_lat');
    $inputLngValue = Html::getAttributeValue($model_gen, 'sys_lng');
    echo '<div class="col-md-12">';
    echo Html::activeHiddenInput($model_gen, 'sys_lat');
    echo Html::activeHiddenInput($model_gen, 'sys_lng');
    echo \common\lib\sdii\widgets\MapInput::widget([
        'lat' => $inputLatID,
        'lng' => $inputLngID,
        'latValue' => $inputLatValue,
        'lngValue' => $inputLngValue,
    ]);
    echo '</div>';
}

foreach ($modelfield as $field) {

    //reference field
    $options_input = [];
    if ($field->ezf_field_type == 18) {
        //ถ้ามีการกำหนดเป้าหมาย

        if ($field->ezf_field_val == 1) {
            //แสดงอย่างเดียวแก้ไขไม่ได้ (Read-only)
            $modelDynamic = \backend\modules\ezforms\components\EzformQuery::getFormTableName($field->ezf_field_ref_table);
            //set form disable
            $options_input['readonly'] = true;
            $form->attributes['rstat'] = 0;
        } else if ($field->ezf_field_val == 2) {
            //ถ้ามีการแก้ไขค่า จะอัพเดจค่านั้นทั้งตารางต้นทาง - ปลายทาง
            $modelDynamic = \backend\modules\ezforms\components\EzformQuery::getFormTableName($field->ezf_id);
            $form->attributes['rstat'] = $dataModel['rstat'];
        } else if ($field->ezf_field_val == 3) {
            //แก้ไขเฉพาะตารางปลายทาง
            $modelDynamic = \backend\modules\ezforms\components\EzformQuery::getFormTableName($field->ezf_field_ref_table);
            //\yii\helpers\VarDumper::dump($ezfField, 10, true);
            //echo $field->ezf_field_ref_field,'-';
            $options_input['readonly'] = true;
            $form->attributes['rstat'] = 0;
        }
        $ezfTable = $modelDynamic->ezf_table;

        $ezfField = \backend\modules\ezforms\models\EzformFields::find()->select('ezf_field_name')->where('ezf_field_id = :ezf_field_id', [':ezf_field_id' => $field->ezf_field_ref_field])->one();
        if (!$ezfField->ezf_field_name) {
            $ezfField = \backend\modules\ezforms\models\EzformFields::find()->select('ezf_field_name, ezf_field_label')->where('ezf_field_id = :ezf_field_id', [':ezf_field_id' => $field->ezf_field_id])->one();
            $html = '<h1>Error</h1><hr>';
            $html .= '<h3>เนื่องการเชื่อมโยงของประเภทคำถาม Reference field ผิดพลาด การแก้ไขคือ ลบคำถามออกแล้วสร้างใหม่</h3>';
            $html .= '<h4>คำถามที่ผิดพลาดคือ : </h4>' . $ezfField->ezf_field_name . ' (' . $ezfField->ezf_field_label . ')';
            echo $html;
            Yii::$app->end();
        }
        $modelDynamic = new backend\modules\ezforms\models\EzformDynamic($ezfTable);

        //กำหนดตัวแปร
        $field_name = $field->ezf_field_name; //field name ต้นทาง
        $field_name_ref = $ezfField->ezf_field_name; //field name ปลายทาง
        //ดูตาราง Ezform ฟิลด์ target ที่มีเป้าหมายเดียวกัน
        if ($input_target == 'byme' || $input_target == 'skip' || $input_target == 'all') {
            $target = \backend\modules\ezforms\components\EzformQuery::getTargetFormEzf($input_ezf_id, $input_dataid);
            $target = $target['ptid'];
        } else {
            $target = base64_decode($input_target);
        }
        //หา target ใน site ตัวเองก่อน (ก็ไม่แนะนำ)
        $res = $modelDynamic->find()->select($field_name_ref)->where('ptid = :target AND ' . $field_name_ref . ' <> "" AND xsourcex = :xsourcex AND rstat <>3', [':target' => $target, ':xsourcex' => $dataModel['xsourcex']])->orderBy('create_date DESC');

        //echo $target.' -';
        //หาที่ target ก่อน
        if ($res->count()) {
            $model_table = $res->One();
            if ($field->ezf_field_val == 1) {
                //echo 'target 1-1<br>';
                //เปลี่ยนค่า field name ระหว่างต้นทาง และปลายทาง
                $model_gen->$field_name = $model_table->$field_name_ref;
                $model_gen->attributes[$field_name] = $model_table->$field_name_ref;
            } else if ($field->ezf_field_val == 2) {
                
            } else if ($field->ezf_field_val == 3) {
                if ($input_dataid) {
                    //echo 'target 1<br>';
                    $res = \backend\modules\ezforms\components\EzformQuery::getReferenceData($field->ezf_id, $field->ezf_field_id, $input_dataid, $field_name_ref);
                    //\yii\helpers\VarDumper::dump($model_table->attributes, 10,true);
                    if ($res[$field_name_ref]) {
                        $model_table->$field_name_ref = $res[$field_name_ref];
                    }
                    //เปลี่ยนค่า field name ระหว่างต้นทาง และปลายทาง
                    $model_gen->$field_name = $model_table->$field_name_ref;
                    $model_gen->attributes[$field_name] = $model_table->$field_name_ref;
                } else {
                    //\yii\helpers\VarDumper::dump($model_table->attributes, 10,true);
                    // echo 'target else 1<br>';
                    //เปลี่ยนค่า field name ระหว่างต้นทาง และปลายทาง
                    $model_gen->$field_name = $model_table->$field_name_ref;
                    $model_gen->attributes[$field_name] = $model_table->$field_name_ref;
                }
            }
        } else {
            //กรณีหาไม่เจอ ให้ดู primary key
            $res = $modelDynamic->find()->where('id = :id', [':id' => $target]);
            if ($res->count()) {
                $model_table = $res->One();

                if ($field->ezf_field_val == 1) {
                    //echo 'target 2-1<br>';
                    //เปลี่ยนค่า field name ระหว่างต้นทาง และปลายทาง
                    $model_gen->$field_name = $model_table->$field_name_ref;
                    $model_gen->attributes[$field_name] = $model_table->$field_name_ref;
                } else if ($field->ezf_field_val == 2) {
                    
                } else if ($field->ezf_field_val == 3) {
                    if ($input_dataid) {
                        //echo 'target 2<br>';
                        $res = \backend\modules\ezforms\components\EzformQuery::getReferenceData($field->ezf_id, $field->ezf_field_id, $input_dataid, $field_name_ref);
                        if ($res[$field_name_ref]) {
                            $model_table->$field_name_ref = $res[$field_name_ref];
                        }
                        //เปลี่ยนค่า field name ระหว่างต้นทาง และปลายทาง
                        $model_gen->$field_name = $model_table->$field_name_ref;
                        $model_gen->attributes[$field_name] = $model_table->$field_name_ref;
                    } else {
                        //echo 'target else 2<br>';
                        //\yii\helpers\VarDumper::dump($model_table->attributes, 10,true);
                        //เปลี่ยนค่า field name ระหว่างต้นทาง และปลายทาง
                        $model_gen->$field_name = $model_table->$field_name_ref;
                        $model_gen->attributes[$field_name] = $model_table->$field_name_ref;
                    }
                }
            }
        }

        // yii\helpers\VarDumper::dump($model_table, 10, true);
        //Yii::$app->end();
    } else if ($modelEzform->query_tools == 2 && Yii::$app->keyStorage->get('frontend.domain') == "cascap.in.th" && ($input_ezf_id == "1437377239070461301" || $input_ezf_id == "1437377239070461302")) {
        //not doing every thing
    } else if ($modelEzform->query_tools == 2) {
        $form->attributes['rstat'] = $dataModel['rstat'];
    }
    //end reference field

    echo \backend\modules\ezforms\components\EzformFunc::getTypeEprintform($model_gen, $field, $form);
}
echo '</div>';

foreach ($modelfield as $value) {
    $inputId = Html::getInputId($model_gen, $value['ezf_field_name']);
    $inputValue = Html::getAttributeValue($model_gen, $value['ezf_field_name']);

    $dataCond = backend\modules\ezforms\components\EzformQuery::getCondition($value['ezf_id'], $value['ezf_field_name']);



    if ($dataCond) {

        //Edit Html
        $fieldId = Html::getInputId($model_gen, $value['ezf_field_name']);
        if ($value['ezf_field_type'] == 4) {
            $fieldId = $value['ezf_field_name'];
        }
        $enable = TRUE;
        foreach ($dataCond as $index => $cvalue) {
            $dataCond[$index]['cond_jump'] = json_decode($cvalue['cond_jump']);
            $dataCond[$index]['cond_require'] = json_decode($cvalue['cond_require']);

            if ($inputValue == $cvalue['ezf_field_value'] || $inputValue == '') {

                if ($value['ezf_field_type'] == '4' || $value['ezf_field_type'] == '6') {
                    if ($enable) {
                        $enable = false;
                        $jumpArr = json_decode($cvalue['cond_jump']);
                        if (is_array($jumpArr)) {
                            foreach ($jumpArr as $j => $jvalue) {
                                $this->registerJs("
										var fieldIdj = '" . $jvalue . "';
										var inputIdj = '" . $fieldId . "';
										var valueIdj = '" . $inputValue . "';
										var fixValuej = '" . $cvalue['ezf_field_value'] . "';
										var fTypej = '" . $value['ezf_field_type'] . "';
										domHtml(fieldIdj, inputIdj, valueIdj, fixValuej, fTypej, 'none');
									");
                            }
                        }

                        $requireArr = json_decode($cvalue['cond_require']);
                        if (is_array($requireArr)) {
                            foreach ($requireArr as $r => $rvalue) {
                                $this->registerJs("
										var fieldIdr = '" . $rvalue . "';
										var inputIdr = '" . $fieldId . "';
										var valueIdr = '" . $inputValue . "';
										var fixValuer = '" . $cvalue['ezf_field_value'] . "';
										var fTyper = '" . $value['ezf_field_type'] . "';
										domHtml(fieldIdr, inputIdr, valueIdr, fixValuer, fTyper, 'block');
									");
                            }
                        }
                    }
                } else {
                    $jumpArr = json_decode($cvalue['cond_jump']);
                    if (is_array($jumpArr)) {
                        foreach ($jumpArr as $j => $jvalue) {
                            $this->registerJs("
									    var fieldIdj = '" . $jvalue . "';
									    var inputIdj = '" . $fieldId . "';
									    var valueIdj = '" . $inputValue . "';
									    var fixValuej = '" . $cvalue['ezf_field_value'] . "';
									    var fTypej = '" . $value['ezf_field_type'] . "';
									    domHtml(fieldIdj, inputIdj, valueIdj, fixValuej, fTypej, 'block');
								    ");
                        }
                    }

                    $requireArr = json_decode($cvalue['cond_require']);
                    if (is_array($requireArr)) {
                        foreach ($requireArr as $r => $rvalue) {
                            $this->registerJs("
									    var fieldIdr = '" . $rvalue . "';
									    var inputIdr = '" . $fieldId . "';
									    var valueIdr = '" . $inputValue . "';
									    var fixValuer = '" . $cvalue['ezf_field_value'] . "';
									    var fTyper = '" . $value['ezf_field_type'] . "';
									    domHtml(fieldIdr, inputIdr, valueIdr, fixValuer, fTyper, 'none');
								    ");
                        }
                    }
                }
            }
        }

        //Add Event
        if ($value['ezf_field_type'] == 20 || $value['ezf_field_type'] == 0 || $value['ezf_field_type'] == 16) {
            $this->registerJs("
		    var dataCond = '" . yii\helpers\Json::encode($dataCond) . "';
		    var inputId = '" . $inputId . "';
		    eventCheckBox(inputId, dataCond);
		    setCheckBox(inputId, dataCond);
		");
        } else if ($value['ezf_field_type'] == 6) {
            $this->registerJs("
		    var dataCond = '" . yii\helpers\Json::encode($dataCond) . "';
		    var inputId = '" . $inputId . "';
		    eventSelect(inputId, dataCond);
		    setSelect(inputId, dataCond);
		");
        } else if ($value['ezf_field_type'] == 4) {
            $this->registerJs("
		    var dataCond = '" . yii\helpers\Json::encode($dataCond) . "';
		    var inputName = '" . $value['ezf_field_name'] . "';
		    eventRadio(inputName, dataCond);
		    setRadio(inputName, dataCond);
		");
        }
    }
}

if ($readonly == 0) {
    ?>
    <div class="modal-footer">
        <?php
        $rstat = 0;
        if (isset($modelform['unique_record']) && $modelform['unique_record'] == 1 && isset($model_gen->id) && !empty($model_gen->id)) {
            $sql = "SELECT rstat, xsourcex FROM {$modelform['ezf_table']} WHERE id=:id ";

            $dataGen = Yii::$app->db->createCommand($sql, [':id' => $model_gen->id])->queryOne();
            $xsourcex = Yii::$app->user->identity->userProfile->sitecode;
            $rstat = $dataGen['rstat'];
            if ($xsourcex != $dataGen['xsourcex']) {//$dataGen['rstat']>1 || 
            } else {
                if (isset($modelform['query_tools']) && $modelform['query_tools'] == 2) {
                    if ($dataGen['rstat'] != 2) {
                        echo Html::submitButton('Save Draft', ['class' => 'btn btn-outline-primary btn-submit', 'id' => 'save-draft', 'name' => 'submit', 'value' => '1', 'data-loading-text' => 'Loading...']);
                        echo Html::submitButton('Submit', ['class' => 'btn btn-primary btn-submit', 'name' => 'submit', 'value' => '2', 'id' => 'submit', 'data-loading-text' => 'Loading...']);
                        echo Html::submitButton('Delete', ['class' => 'btn btn-danger', 'name' => 'submit', 'value' => '3']);
                    } else {
                        if ((Yii::$app->user->can('administrator') || Yii::$app->user->can('adminsite'))) {
                            echo Html::submitButton('ReSaveDraft', ['class' => 'btn btn-warning btn-submit', 'id' => 'save-draft', 'name' => 'submit', 'value' => '1', 'data-loading-text' => 'Loading...']);
                        } else {
                            echo "หากต้องการแก้ไขข้อมูลที่ Submit ไปแล้ว กรุณาติดต่อ Admin Site ";
                        }
                    }
                } elseif (isset($modelform['query_tools']) && $modelform['query_tools'] == 3) {
                    if ($dataGen['rstat'] != 2) {
                        echo Html::submitButton('Save Draft', ['class' => 'btn btn-outline-primary btn-submit', 'id' => 'save-draft', 'name' => 'submit', 'value' => '1', 'data-loading-text' => 'Loading...']);
                        echo Html::submitButton('Submit', ['class' => 'btn btn-primary btn-submit', 'name' => 'submit', 'value' => '2', 'data-loading-text' => 'Loading...']);
                        echo Html::submitButton('Delete', ['class' => 'btn btn-danger btn-submit', 'name' => 'submit', 'value' => '3', 'data-loading-text' => 'Loading...']);
                    } else {
                        if ((Yii::$app->user->can('administrator') || Yii::$app->user->can('adminsite'))) {
                            echo Html::submitButton('ReSaveDraft', ['class' => 'btn btn-warning btn-submit', 'id' => 'save-draft', 'name' => 'submit', 'value' => '1', 'data-loading-text' => 'Loading...']);
                        } else {
                            echo "หากต้องการแก้ไขข้อมูลที่ Submit ไปแล้ว กรุณาติดต่อ Admin Site ";
                        }
                    }
                } else {
                    echo Html::submitButton('Submit', ['class' => 'btn btn-primary btn-submit', 'name' => 'submit', 'value' => '1', 'data-loading-text' => 'Loading...']);
                    echo Html::submitButton('Delete', ['class' => 'btn btn-danger btn-submit', 'name' => 'submit', 'value' => '3', 'data-loading-text' => 'Loading...']);
                }
            }
        } else {
            if (isset($modelform['query_tools']) && $modelform['query_tools'] == 2) {
                $sql = "SELECT rstat, xsourcex FROM {$modelform['ezf_table']} WHERE id=:id ";

                $dataGen = Yii::$app->db->createCommand($sql, [':id' => $dataid])->queryOne();

                $rstat = $dataGen['rstat'];
                if ($dataGen['rstat'] != 2) {
                    echo Html::submitButton('Save Draft', ['class' => 'btn btn-outline-primary btn-submit', 'id' => 'save-draft', 'name' => 'submit', 'value' => '1', 'data-loading-text' => 'Loading...']);
                    echo Html::submitButton('Submit', ['class' => 'btn btn-primary btn-submit', 'name' => 'submit', 'value' => '2', 'id' => 'submit', 'data-loading-text' => 'Loading...']);
                    echo Html::submitButton('Delete', ['class' => 'btn btn-danger btn-submit', 'name' => 'submit', 'value' => '3', 'data-loading-text' => 'Loading...']);
                } else {
                    if ((Yii::$app->user->can('administrator') || Yii::$app->user->can('adminsite'))) {
                        echo Html::submitButton('ReSaveDraft', ['class' => 'btn btn-warning btn-submit', 'id' => 'save-draft', 'name' => 'submit', 'value' => '1', 'data-loading-text' => 'Loading...']);
                    } else {
                        echo "หากต้องการแก้ไขข้อมูลที่ Submit ไปแล้ว กรุณาติดต่อ Admin Site ";
                    }
                }
            } elseif (isset($modelform['query_tools']) && $modelform['query_tools'] == 3) {
                $sql = "SELECT rstat, xsourcex FROM {$modelform['ezf_table']} WHERE id=:id ";

                $dataGen = Yii::$app->db->createCommand($sql, [':id' => $dataid])->queryOne();

                $rstat = $dataGen['rstat'];
                if ($dataGen['rstat'] != 2) {
                    echo Html::submitButton('Save Draft', ['class' => 'btn btn-outline-primary btn-submit', 'id' => 'save-draft', 'name' => 'submit', 'value' => '1', 'data-loading-text' => 'Loading...']);
                    echo Html::submitButton('Submit', ['class' => 'btn btn-primary btn-submit', 'name' => 'submit', 'value' => '2', 'data-loading-text' => 'Loading...']);
                    echo Html::submitButton('Delete', ['class' => 'btn btn-danger btn-submit', 'name' => 'submit', 'value' => '3', 'data-loading-text' => 'Loading...']);
                } else {
                    if ((Yii::$app->user->can('administrator') || Yii::$app->user->can('adminsite'))) {
                        echo Html::submitButton('ReSaveDraft', ['class' => 'btn btn-warning btn-submit', 'id' => 'save-draft', 'name' => 'submit', 'value' => '1', 'data-loading-text' => 'Loading...']);
                    } else {
                        echo "หากต้องการแก้ไขข้อมูลที่ Submit ไปแล้ว กรุณาติดต่อ Admin Site ";
                    }
                }
            } else {
                echo Html::submitButton('Submit', ['class' => 'btn btn-primary btn-submit', 'name' => 'submit', 'value' => '1', 'data-loading-text' => 'Loading...']);
                echo Html::submitButton('Delete', ['class' => 'btn btn-danger btn-submit', 'name' => 'submit', 'value' => '3', 'data-loading-text' => 'Loading...']);
            }
        }
        ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
        <?= Html::button(Yii::t('app', '<i class="glyphicon glyphicon-ban-circle"></i> Close'), ['class' => 'btn btn-outline-warning', 'data-dismiss' => 'modal']) ?>
    </div>
<?php } backend\modules\ezforms\components\EzActiveForm::end(); ?>

<?php
//window.print();
$query_tools = isset($modelform['query_tools']) ? $modelform['query_tools'] : 0;
$setBtn = '';
if ($rstat != 0) {
    if ($checkSql == 0) {
        if ($rstat != 2) {

            $setBtn = "$('#submit').hide();";
        }
    }
} else {
    $setBtn = "$('#submit').hide();";
}

$disabledInput = '';

if ($rstat == 2) {
    $disabledInput = "$('#ezform-box input, #ezform-box select').attr('disabled', true); $('#ezform-box input[type=\"hidden\"]').attr('disabled', false);";
}

if (isset($formname) && $formname == 'ezform-target') {
    $msg = 'หากท่านต้องการลบข้อมูลการลงทะเบียนนี้ออก ท่านจำเป็นต้องลบข้อมูลจากฟอร์มอื่นๆของเป้าหมายให้หมดก่อนจึงจะสามารถลบฟอร์มลงทะเบียนนี้ได้';
    $delAction = 1;

    if ($model_gen['id'] == $model_gen['ptid']) {
        $delAction = 2;
    }
} else {
    $delAction = 3;
    $msg = 'คุณแน่ใจที่จะลบข้อมูลนี้หรือไม่';
}
?>



<div class="modal fade" id="modal-add" data-keyboard="false" data-backdrop="static" role="dialog">
    <div class="modal-dialog" style="width: 80%;">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <br>
            </div>
            <div class="modal-body">
                <div class="col-md-4">
                    <div class="panel panel-primary">
                        <div class="panel-heading">วันที่จ่ายยา</div>
                        <div class="panel-body">

                            <?php
                            try {
                               
                                    if ($datamodel_table->hptcode) {
                                        $drug_hpcode = $datamodel_table->hptcode;
                                    }
                                    print_r($datamodel_table);
                                    $sql = "SELECT DISTINCT DATE(`DATETIME_ADMIT`) AS DATETIME_ADMIT FROM f_drug_ipd WHERE HOSPCODE = :HOSPCODE AND  ptlink = MD5(:ptlink) AND TYPEDRUG = '2' ORDER BY `DATETIME_ADMIT` DESC";
                                    $dataDrug = \backend\modules\ckdnet\classes\CkdnetFunc::queryAll($sql, [':HOSPCODE' => $datamodel_table->hsitecode, ':ptlink' => $datamodel_table->cid]);
                                        
                                   
                                    foreach ($dataDrug as $value) {

                                        echo Html::a($value['DATETIME_ADMIT'], 'javascript:void(0)', [
                                            'class' => 'dataDrug list-group-item',
                                            'style' => 'width:100%',
                                            'data-url' => Url::to(['/ezforms/ezform/add-drug',
                                                'hospcode' => "'" . $drug_hsitecode . "'",
                                                'pid' => "'" . $drug_hpcode . "'",
                                                'ptlink' => "'" . md5($drug_cid) . "'",
                                                'date_admit' => "'" . $value['DATETIME_ADMIT'] . "'"
                                            ])
                                        ]);
                                    }
                                
                            } catch (\yii\base\Exception $ex) {
                                
                            }
                            ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <div class="col-md-6">ชื่อยา</div>
                            <div class="col-md-3">จำนวน</div>
                            <div class="col-md-3">ปริมาณ</div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="panel-body">
                            <div id="dataDrug">

                            </div>
                        </div>
                    </div>

                </div>


            </div>
            <div class="modal-footer">
                <?php
                if ($dataDrug) {
                    echo Html::button("<i class='glyphicon glyphicon-ok'> </i> เพิ่ม", [
                        'id' => 'btnAdddrug',
                        //'data-url' => Url::to(['/ezforms/ezform/add-drug', 'hospcode' => "'" . $datamodel_table->hsitecode . "'", 'pid' => "'" . $datamodel_table->hptcode . "'", 'ptlink' => "'" . md5($datamodel_table->cid) . "'"]),
                        'class' => 'btn btn-success btnAdddrug',
                    ]);
                }
                ?>
                <?=
                Html::button("<i class='glyphicon glyphicon-remove'> </i> ยกเลิก", [
                    'id' => 'btnCancle',
                    'class' => 'btn btn-danger btnCancle',
                ]);
                ?>
            </div>
        </div>

    </div>
</div>


<?php
$this->registerJs("
        
        $('.dataDrug').on('click',function(){
            $('#btnAdddrug').attr('data-url',$(this).attr('data-url'));
            $('#dataDrug').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
            $.ajax({
                url:$(this).attr('data-url'),
                dataType: 'json',
                success: function (data) {
                    $('#dataDrug').html('');
                    if(data.length > 0){
                        $.each(data, function (key, value) {
                            $('#dataDrug').append('<div class=\'col-md-6\'>'+value.DNAME+'</div> <div class=\'col-md-3\'>'+value.AMOUNT+'</div> <div class=\'col-md-3\'>'+value.UNIT_PACKING+'</div> <div class=\'clearfix\'></div>');
                            
                        });
                    }else{
                        $('#dataDrug').html('<label class=\'alert-danger\'>ไม่พบข้อมูล</label> ');
                    }
                },
                error: function (error) {
                    console.log(error);
                }
            });
            return false;
        });
        
        $('.btnCancle').on('click', function () {
            $('#btnAdddrug').attr('data-url','');
            $('#dataDrug').html('');
            $('#modal-add').modal('hide');
            return false;
        });
        
        ");
?>

<?php
$js = $modelform->js;
$this->registerJs("
  $js
      
$('.btn-submit').attr('type', 'submit');

$('input[type=\"radio\"]').dblclick(function(){
    this.checked = false;
    $(this).trigger('change');
});

var formname = '$formname';
var rstat = '$rstat';    
var query_tools = '$query_tools';    
$setBtn

$disabledInput

$('form#$formname').on('beforeSubmit', function(e) {
    var \$form = $(this);
    var formData = new FormData($(this)[0]);
    var clickValue = $(this).find('input[name=\"submit\"]').val();
    var action = $delAction;
        
    var ezf_id = '$ezf_id';
    var id = '{$dataid}';
    var target = '{$model_gen['target']}';    
    var xsourcex = '{$model_gen['xsourcex']}';
        
    if(clickValue==3){
        if(action==1){
            bootbox.alert('$msg');
        } else if(action==2){
            $.ajax({
                url: '" . Url::to(['/inv/inv-person/check-del']) . "',
                type: 'POST',
                data: {ezf_id:ezf_id, id:id, target:target, xsourcex:xsourcex},
                dataType: 'JSON',
                success: function (result) {
                  if(result.status == 'success') {
                      if(result.num > 0){
                        bootbox.alert('$msg');
                      } else {
                        yii.confirm('คุณแน่ใจที่จะลบข้อมูลนี้หรือไม่', function() {
                            save(\$form, formData);
                        });
                      }
                      
                  } else {
                      " . SDNoty::show('result.message', 'result.status') . "
                  } 
                },
                error: function () {
                  " . SDNoty::show("'" . SDHtml::getMsgError() . "Server Error'", '"error"') . "
                  console.log('server error');
                }
            });
        } else {
            yii.confirm('$msg', function() {
                save(\$form, formData);
            });
        }
        
    } else if(clickValue==2){
        yii.confirm('การ Submit จะทำให้คุณไม่สามารถแก้ไขข้อมูลนี้ได้ คุณแน่ใจที่จะบันทึกข้อมูลนี้หรือไม่', function() {
            save(\$form, formData);
        });
    } else {
        save(\$form, formData);
    }

    
    return false;
    
});

function save(\$form, formData) {
    var btnSubmit = $('.btn-submit').button('loading');
    
    $.ajax({
          url: \$form.attr('action'),
          type: 'POST',
          data: formData,
	  dataType: 'JSON',
	  enctype: 'multipart/form-data',
	  processData: false,  // tell jQuery not to process the data
	  contentType: false,   // tell jQuery not to set contentType
          success: function (result) {
	    if(result.status == 'success') {
                " . SDNoty::show('result.message', 'result.status') . "
                btnSubmit.button('reset');
                var emrload = 0;
                if($( '#modal-inv-emr' ).hasClass('in')){
                    var url = '" . (isset(Yii::$app->session['emr_url']) ? Yii::$app->session['emr_url'] : 0) . "';
                        if(url!=0){
                            modalEzfromEmrUpdate(url);
                        }
                    emrload = 1;
                }
                
                 if(result.data.rstat==3){
                    $(document).find('#modal-print').modal('hide'); 
                    if(emrload==1){
                        $('#emr-preload').val(1);
                    } else {
                        $.pjax.reload({container:'#$pjax_reload'});
                    }
                    return false;
                 }

                js_target = result.target;
                js_target_decode = result.target_decode;
                js_id = result.id;  
                  
                
                 if(result.error==0){
                    
                    if(query_tools=='2'){
                        if(result.data.rstat==2 ){
                           
                            " . (($end == 1) ? "$(document).find('#modal-print').modal('hide'); if(emrload==1){
                                $('#emr-preload').val(1);
                            } else {
                                $.pjax.reload({container:'#$pjax_reload'});
                            } return false;" : "$('#preload').val(1);") . "
                        } else {
                         
                            if(emrload==1){
                                $('#emr-preload').val(1);
                            } else {
                                $('#preload').val(1);
                            }
                            
                            if(formname=='ezform-target'){
                                
                                ezform_target(result.target_decode);
                                return false;
                            } else {
                                js_dataid = result.dataid;
                                ezform_mian(result.dataid, result.id);
                                return false;
                            }
                            
                            $('#submit').show();
                        }
                    } else {
                        " . (($end == 1) ? "$(document).find('#modal-print').modal('hide'); if(emrload==1){
                            $('#emr-preload').val(1);
                        } else {
                            $.pjax.reload({container:'#$pjax_reload'});
                        } return false;" : "$('#preload').val(1);") . "
                    }
                    
                } else {
                    if(emrload==1){
                        $('#emr-preload').val(1);
                    } else {
                        $('#preload').val(1);
                    }
                    
                    if(formname=='ezform-target'){
                        js_dataid = '';
                        ezform_target(result.target_decode);
                        return false;
                    } else {
                        js_dataid = result.dataid;
                        ezform_mian(result.dataid, result.id);
                        return false;
                    }
                }

                menu_main();
                js_end=1;
                //ezform_mian(js_dataid, js_id);

            } else {
                " . SDNoty::show('result.message', 'result.status') . "
            } 
          },
          error: function () {
            " . SDNoty::show("'" . SDHtml::getMsgError() . "Server Error'", '"error"') . "
	    console.log('server error');
          }
      });
}

function modalEzfromEmrUpdate(url) {
    $('#modal-inv-emr .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
    $.ajax({
	method: 'POST',
	url: url,
	dataType: 'HTML',
	success: function(result, textStatus) {
	    $('#modal-inv-emr .modal-content').html(result);
	    return false;
	}
    });
}

");
?>

