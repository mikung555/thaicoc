<?php

use Yii;
use yii\helpers\Html;
use appxq\sdii\helpers\SDNoty;
use yii\helpers\Url;

\backend\modules\comments\assets\CommentAsset::register($this);
     
?>

<div class="inv-form">

    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="itemModalLabel">App Info</h4>
    </div>

    <div class="modal-body">
        <?php
        $linkModule = '';
        if ($model['gtype'] == 1) {
            $linkModule = \yii\helpers\Url::to($model['glink']);
        } else {
            $linkModule = \yii\helpers\Url::to(['/inv/inv-person/index', 'module' => $model['gid']]);
        }

        $linkFav = '<button class="btn btn-success btn-sm  fav-btn" data-gid="' . $model['gid'] . '"><i class="glyphicon glyphicon-cloud-download"></i> ยกเลิก</button>';
        $linkNotFav = '<button class="btn btn-warning btn-sm  fav-btn" data-gid="' . $model['gid'] . '"><i class="glyphicon glyphicon-cloud-download"></i> รับ</button>';

        $gname = Html::encode($model['gname']);

        $userId = Yii::$app->user->id;
        $dataFav = backend\modules\inv\classes\InvQuery::getModuleListNotme($userId);
        $arrayMap = \yii\helpers\ArrayHelper::map($dataFav, 'gid', 'gname');
        $arrayFav = array_keys($arrayMap);

        //appxq\sdii\utils\VarDumper::dump($arrayFav,FALSE);
        ?>
        <div class="media">
            <div class="media-left">
                <a href="<?= $private==1?'#':$linkModule ?>">
                    <img alt="72x72" width="72" height="72" class="media-object img-rounded" src="<?= (isset($model['gicon']) && $model['gicon'] != '') ? Yii::getAlias('@backendUrl') . '/module_icon/' . $model['gicon'] : Yii::getAlias('@backendUrl') . '/module_icon/no_icon.png' ?>" >
                </a>
            </div>
            <div class="media-body">
                <h4 class="media-heading"><?= $gname ?></h4>
                <?php if ($private != 1): ?>
                <div id="btn-box" class="pull-right">
                    
                    <?php if ($model['gsystem'] != 1): ?>
                        <?php if (in_array($model['gid'], $arrayFav)): ?>

                            <?= $linkFav ?>
                        <?php else: ?>
                            <?= $linkNotFav ?>
                        <?php endif; ?>
<?php endif; ?>
                </div>
                <?php endif; ?>
                <span>

                    อัพเดทเมื่อ <?= isset($model['updated_at'])?\common\lib\sdii\components\utils\SDdate::mysql2phpThDateSmall($model['updated_at']):'?' ?>
                    (จำนวนผู้ใช้ <?= number_format($ncount['user']); ?> คน จาก <?= number_format($ncount['org']); ?> หน่วยงาน) <?php if (isset($pcoc)) { ?> ดูแลผู้ป่วยแล้วทั้งสิ้น <?= number_format($pcoc['npatient']); ?> ราย รวม <?= number_format($pcoc['ntime']); ?> ครั้ง <?php } ?>

                </span>

                <div class="rating-view" style="color: #ec971f;">
                    <?php if($private!=1):?><a href="<?= $linkModule ?>" class="btn btn-default btn-xs"><i class="glyphicon glyphicon-link"></i> เปิด </a><?php endif;?>
                        <!-- <i class="glyphicon glyphicon-star"></i>
                        <i class="glyphicon glyphicon-star"></i>
                        <i class="glyphicon glyphicon-star"></i>
                        <i class="glyphicon glyphicon-star-empty"></i>
                        <i class="glyphicon glyphicon-star-empty"></i> -->
                    <span id='Count_user'  ></span>
                </div>

            </div>
        </div>
        <hr>
        <ul id="myTabs" class="nav nav-pills nav-justified">
            <li role="presentation" class="active"><a href="#page1" id="page1-tab" data-toggle="tab">รายละเอียด</a></li>
            <li role="presentation"><a href="#page2" id="page2-tab" data-toggle="tab">บทวิจารณ์</a></li>
        </ul>
        <div class="tab-content" id="myTabContent">
            <div class="tab-pane fade in active" role="tabpanel" id="page1" aria-labelledby="page1-tab">
                <div class="modal-header">
                    <h4 class="modal-title" id="itemModalLabel">คำอธิบาย</h4>
                </div>
                <div class="modal-body">
<?= $model['gdetail'] ?>
                </div>

                <div class="modal-header">
                    <h4 class="modal-title" id="itemModalLabel">ผู้พัฒนา</h4>
                </div>
                <div class="modal-body">
<?= $model['gdevby'] ?>
                </div>

                <div class="modal-header">
                    <h4 class="modal-title" id="itemModalLabel">แอพพลิเคชั่นอื่นๆ</h4>
                </div>
                <div class="modal-body">
                    <div class="row">

                        <?php
                        $dataModules = backend\modules\inv\classes\InvQuery::getModuleListMy($model['created_by'], $model['gid']);

                        if ($dataModules) {
                            ?>
                            <?php foreach ($dataModules as $key => $value): ?>
                                <div class="col-xs-4 col-md-3" style="margin-bottom: 20px;">
                                    <div class="media-left">
                                        <?php
                                        $linkModule = '';
                                        if ($value['gtype'] == 1) {
                                            $linkModule = \yii\helpers\Url::to($value['glink']);
                                        } else {
                                            $linkModule = \yii\helpers\Url::to(['/inv/inv-person/index', 'module' => $value['gid']]);
                                        }

                                        $gname = yii\helpers\Html::encode($value['gname']);
                                        $checkthai = \backend\modules\ovcca\classes\OvccaFunc::checkthai($gname);
                                        $len = 12;
                                        if ($checkthai != '') {
                                            $len = $len * 3;
                                        }
                                        if (strlen($gname) > $len) {
                                            $gname = substr($gname, 0, $len) . '...';
                                        }
                                        ?>
                                        <a href="<?= $linkModule ?>">
                                            <div style="margin: 4px;"><img src="<?= (isset($value['gicon']) && $value['gicon'] != '') ? Yii::getAlias('@backendUrl') . '/module_icon/' . $value['gicon'] : Yii::getAlias('@backendUrl') . '/module_icon/no_icon.png' ?>" class="img-rounded" width="72" height="72"></div>
                                        </a>
                                        <h4 class="media-heading text-center" style="font-size: 13px;"><strong><?= $gname ?></strong></h4>
                                    </div>

                                </div>
                            <?php endforeach; ?>
                        <?php
                        } else {
                            if (count($arrayFav) == 0) {
                                echo '<div class="col-md-4"><cite>ไม่พบโมดูล</cite></div>';
                            }
                        }
                        ?>

                    </div>
                </div>

            </div>
            <div class="tab-pane fade" role="tabpanel" id="page2" aria-labelledby="page2-tab">
                <div class="row">
<?php $arr = [1, 2, 3, 4, 5]; ?>
                    <div class="col-md-12">

                        <div id="show_user"> </div>

                        <div class="clearfix"></div>


                        <div class="col-md-6" style="font-weight: bold;"><b>ความเห็น</b></div>
                        <div class="col-md-6">
                            <button style="width:100%;margin-top: 10px;display:none;" id="btnComment" class="btn btn-success pull-right"><i class="glyphicon glyphicon-pencil"></i> เขียนความคิดเห็น</button>

                        </div>
                        <div class="clearfix"></div>
                        <!---->

                        <div id="container"></div>


                        <div id="showComment"></div>
                        <div id="showForm"></div>
                    </div>

                </div>

                <div class="row">
                    <div class="clearfix"></div>
                    <div class="col-md-12">
                        <button id='btnLoadmore' class="btn btn-default btn-block">โหลดเพิ่ม
                            <span id="loadings">
                                <span style="margin-left:10%;"  class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span> Loading...
                            </span>
                        </button>
                    </div>

                </div>


            </div> <!-- end -->
        </div>
    </div>

</div>
 <?php 
    $vote = Yii::$app->session["vote"];
 ?>
<?php $this->registerJs("
   
     
 function ShowRate(){
    var rows =1; 
    var html ='';
    
    html += '<div id=\"container\">';
       html +=  '<div id=\"inner\">';
          html +=   '<div class=\"rating\">';
               html +=  '<span id=\"Count_rate\"></span>';
            html += '</div>';
            html += '<div class=\"histo\">';
            for (var i = 0; i < 5; i++){
                 var v = ['bar-five', 'bar-four', 'bar-three', 'bar-two', 'bar-one'];
                    var  num = ['".(int)$vote[0]."','".(int)$vote[1]."','".(int)$vote[2]."','".(int)$vote[3]."','".(int)$vote[4]."'];  
                    
                  var n=5;
                   html +='<div class=\"five histo-rate\">';
                         html +='<span class=\"histo-star\">';
                            html += '<i class=\"fa fa-star\"></i>'+ (n-i) +'</span>';
                         html +='<span class=\"bar-block\">' ;
                             html +='<span id='+v[i]+' class=\"bar\">';
                                html += '<span>'+ num[i] +'</span>&nbsp;';
                             html += '</span>';
                         html +='</span>';
                     html +='</div>';
            }
            
                
               
                $.ajax({
                    url:'" . Url::to(['/inv/inv-comment/show-rate-start']) . "',
                     success:function(data){
                       num[0] = (num[0] / data) * 100;
                       num[1] = (num[1] / data) * 100;
                       num[2] = (num[2] / data) * 100;
                       num[3] = (num[3] / data) * 100;
                       num[4] = (num[4] / data) * 100;
                       console.log(data);
                       console.log(num);

                        $('.bar span').hide();
                        $('#bar-five').animate({
                           width:num[0].toFixed(0)+'%'    }, 1000);
                        $('#bar-four').animate({
                            width:num[1].toFixed(0)+'%'}, 1000);
                        $('#bar-three').animate({
                            width:num[2].toFixed(0)+'%'}, 1000);
                        $('#bar-two').animate({
                            width:num[3].toFixed(0)+'%'}, 1000);
                        $('#bar-one').animate({
                            width:num[4].toFixed(0)+'%'}, 1000);

                        setTimeout(function() {
                          $('.bar span').fadeIn('slow');
                        }, 1000);

                     }
                });
             
 
             html +='</div>';
         html +='</div>';
     html +='</div>';
 html +='</div>' ;
 
$('#container').html(html);
}
 


function countID(data){
    if(data =='' || data==0 ||data==null){
        data =0;
    }
                      
}
 setTimeout(function(){
          showComment(pageSizes);
          checkmodule();
          showCommentUser();
          CountInv();
          countRate();
          load_on_check();
          ShowRate();

},100);

   function load_on_check()
   {
     $.ajax({
         url:'" . Url::to(['/inv/inv-comment/check-module-user-add']) . "',
         type:'POST',
         data:{gid:" . $model['gid'] . "},
         success:function(data){
         console.log(data);
            if(data == 1 || data == 2   ){
              $('#showForm').show();
              $('#btnComment').show();
              $('#showComments').show();
            }else {
              $('#showForm').hide();
              $('#btnComment').hide();
              $('#showComments').hide

            }
        }
    });
   }

") ?>
<?php $this->registerJs("
$('.inv-form').on('click', '.fav-btn', function() {
    var gid = $(this).attr('data-gid');
    var icon = $('#btn-box');
    $.ajax({
	method: 'POST',
	url: '" . Url::to(['/inv/inv-person/favorite', 'gid' => '']) . "'+gid,
	dataType: 'JSON',
	success: function(result, textStatus) {
	    if(result.status == 'success') {
		" . SDNoty::show('result.message', 'result.status') . "
                     $('#showForm').show();
                     $('#btnComment').show();
                     $('#showComments').show();
		if(result.active==1){
		    icon.html('$linkFav');
                     $('#showForm').show();
                     $('#btnComment').show();
                     $('#showComments').show();
		} else {
		    icon.html('$linkNotFav');
                     $('#showForm').hide();
                     $('#btnComment').hide();
                     $('#showComments').hide();
		}
	    } else {
		" . SDNoty::show('result.message', 'result.status') . "
	    }
	}
    });
});

      var n=1;
      var message='';
      var media = '';
      var pageSizes = 10;
      var count = 0;
      var Count_c = 0;
        setInterval(function(){

            realtime();

        },100);


        function countRate()
        {
          $.ajax({
            url:'" . Url::to(['/inv/inv-comment/counts']) . "',
            type:'POST',
             data:{gid:" . $model['gid'] . "},
            success:function(data)
            {
               $('#Count_rate').html(data);
            }
          })
        } //แสดงดาว
        function countRate_one()
        {
          $.ajax({
            url:'" . Url::to(['/inv/inv-comment/count-one']) . "',
            type:'POST',
            data:{gid:" . $model['gid'] . "},
            success:function(data)
            {
               $('#Count_user').html(data);
            }
          })
        }//แสดงดาวทั้งหมด
        countRate_one();
        function realtime()
        {
             if(Count_c != 0){
                $.ajax({
                  url:'" . Url::to(['/inv/inv-comment/realtime']) . "',
                  success:function(count){
                      Count_c = count;

                      updateData();
                      showComment();
                  }
              });

             }


        }//realtime <<<------------------------------------------------------------------------>>>
        function updateData()
        {
            $.ajax({
                url:'" . Url::to(['/inv/inv-comment/saverealtime']) . "',
                success:function(count){
                   $('#showComment').load();
                }
            });
        }//updateData  <<<------------------------------------------------------------------------>>>

        $('#btnComment').click(function(){
            showForm();
        });//btnComment แสดงกล่องพิมพื <<<------------------------------------------------------------------------>>>
        function showForm()
        {
            $.ajax({
                 url:'" . Url::to(['/inv/inv-comment/showform']) . "',
                 type:'POST',
                 data:{gid:" . $model['gid'] . "},
                 success:function(data){
                    $('#showForm').html(data);
                    $('#showForm').show();
                    $('#showComment').hide();
                    $('#btnLoadmore').hide();
                    $('#container').hide();

                 }
            });
        }//แสดง form เพิ่ม
        function showFormEdit(){
            showForm();
            return false;
        }//แสดง form แก้ไข
        function showComment(pageSize=10)
        {
         $.ajax({
              url:'" . Url::to(['/inv/inv-comment/show-inv']) . "',
              type:'POST',
              data:{pageSize:pageSize, gid:" . $model['gid'] . "},
              //dataType:'json',
              success:function(data){
                $('#showComment').html(data);
                $('#loadings').hide();
              }

          });//แสดง comment ด้านล่าง

        }//showComment <<<------------------------------------------------------------------------>>>


        $('#btnLoadmore').click(function(){

           $('#loadings').show();
           showComment(pageSizes+=5);
           CountInv();

        });//LoadMore โหลด
        function CountInv()
        {
          $.ajax({
            url:'" . Url::to(['/inv/inv-comment/count-inv']) . "',
            success:function(data)
            {
              count = data;
              if(pageSizes > count){
                  $('#btnLoadmore').hide();
              }else{
                 $('#btnLoadmore').show();
              }
            }
          })
        }//count

        function showCommentUser()
        {
         $.ajax({
              url:'" . Url::to(['/inv/inv-comment/show-inv-user']) . "',
              type:'POST',
              data:{gid:'" . $model['gid'] . "'},
              success:function(data){


                $('#show_user').html(data);

              }

          });

        }//showComment <<<------------------------------------------------------------------------>>>


    function DeleteComment()
    {
        $.ajax({
            url:'" . Url::to(['/inv/inv-comment/deletes']) . "',
            success:function(data){

                if(data=='success')
                {
                    $('#showForm').html('');
                    showCommentUser();
                     $.ajax({
                        url:'".Url::to(['/inv/inv-comment/update-rate'])."',
                        type:'post',
                        data:{gid:'".$model['gid']."'},
                        dataType:'json',    
                        success:function(data){
                            setTimeout(function(){
                                ShowUpdateRate(data.v5,data.v4,data.v3,data.v2,data.v1);
                            },100);
                        }
                       }); 
                     countRate(); 
                     load_on_check();
                }
            }
        });
        return false;
    }//DeleteComment

    function checkmodule()
    {
        $.ajax({
            url:'" . Url::to(['/inv/inv-comment/check-module']) . "',
            data:{gid:" . $model['gid'] . "},
            type:'POST',
            success:function(data){

                if(data == 3 || data == 0)
                {
                     $('#showForm').hide();
                     $('#btnComment').hide();//ไม่แสดง

                     $('#showComments').hide();
                }else if(data==1 || data == 2){
                     $('#showForm').show();
                     $('#btnComment').show();
                     $('#showComments').show();
                }

            }
        });
        return false;
    }//checkmoduel

 
function ShowUpdateRate(v5,v4,v3,v2,v1){
     if(isNaN(v5) || v5==NaN || v5==''){
        v5=0;
     }if(isNaN(v4) || v4==NaN || v4==''){
        v4=0;
     }if(isNaN(v3) || v3==NaN || v3==''){
        v3=0;
     }if(isNaN(v2) || v2==NaN || v2==''){
        v2=0;
     }if(isNaN(v1) || v1==NaN || v1==''){
        v1=0;
     } 
   
        //  howComment(pageSizes);
          checkmodule();
          showCommentUser();
          CountInv();
          countRate();
          load_on_check();
          ShowRate();
          

    var rows =1; 
    var html ='';
    
    html += '<div id=\"container\">';
       html +=  '<div id=\"inner\">';
          html +=   '<div class=\"rating\">';
               html +=  '<span id=\"Count_rate\"></span>';
            html += '</div>';
            html += '<div class=\"histo\">';
            for (var i = 0; i < 5; i++){
                 var v = ['bar-five', 'bar-four', 'bar-three', 'bar-two', 'bar-one'];
                    var  num = [v5,v4,v3,v2,v1];  
                    
                  var n=5;
                   html +='<div class=\"five histo-rate\">';
                         html +='<span class=\"histo-star\">';
                            html += '<i class=\"fa fa-star\"></i>'+ (n-i) +'</span>';
                         html +='<span class=\"bar-block\">' ;
                             html +='<span id='+v[i]+' class=\"bar\">';
                                html += '<span>'+ num[i] +'</span>&nbsp;';
                             html += '</span>';
                         html +='</span>';
                     html +='</div>';
            }
            setTimeout(function(){
 
                $.ajax({
                    url:'" . Url::to(['/inv/inv-comment/show-rate-start']) . "',
                     success:function(data){
                       num[0] = (num[0] / data) * 100;
                       num[1] = (num[1] / data) * 100;
                       num[2] = (num[2] / data) * 100;
                       num[3] = (num[3] / data) * 100;
                       num[4] = (num[4] / data) * 100;
                       console.log(data);
                       console.log(num);

                        $('.bar span').hide();
                        $('#bar-five').animate({
                           width:num[0].toFixed(0)+'%'    }, 1000);
                        $('#bar-four').animate({
                            width:num[1].toFixed(0)+'%'}, 1000);
                        $('#bar-three').animate({
                            width:num[2].toFixed(0)+'%'}, 1000);
                        $('#bar-two').animate({
                            width:num[3].toFixed(0)+'%'}, 1000);
                        $('#bar-one').animate({
                            width:num[4].toFixed(0)+'%'}, 1000);

                        setTimeout(function() {
                          $('.bar span').fadeIn('slow');
                        }, 1000);

                     }
                });
            },100);
 
             html +='</div>';
         html +='</div>';
     html +='</div>';
 html +='</div>' ;
 
$('#container').html(html);
}



    ");
?>

<?php $this->registerCss("

#loadings{
 display: none;
}
#btnLoadmore{
 display: none;
}
  .glyphicon-refresh-animate {
   -animation: spin .7s infinite linear;
   -webkit-animation: spin2 .7s infinite linear;
}

@-webkit-keyframes spin2 {
   from { -webkit-transform: rotate(0deg);}
   to { -webkit-transform: rotate(360deg);}
}

@keyframes spin {
   from { transform: scale(1) rotate(0deg);}
   to { transform: scale(1) rotate(360deg);}
}
#showComment{
   height: 400px;
   overflow-x: hidden;
   margin-top:20px;
}
#showComment::-webkit-scrollbar-track
{
       -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
       border-radius: 10px;
       background-color: #F5F5F5;
}

#showComment::-webkit-scrollbar
{
width: 8px;
   background-color: #F5F5F5;
}

#showComment::-webkit-scrollbar-thumb
{
border-radius: 11px;
   -webkit-box-shadow: inset 0 0 6px rgb(206, 206, 206);
   background-color: #efefef;
}
.rating-starss {
    font-size: 20px;
    color: #E3E3E3;
    margin-bottom: .5em;
 
    /*float: left;*/
    position: relative;
    top: -27px;
    left: 50px;
}
.bar-block {
    margin-left: 5px;
    color: black;
    display: block;
    float: left;
    width: 75%;
    position: relative;
    padding: 1px;
}
.fa-star{
        color: #fec900;
        font-size:14px;
}

") ?>
