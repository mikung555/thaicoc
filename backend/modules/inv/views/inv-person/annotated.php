
<div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Annotated case report form</h4>
      </div>
      <div class="modal-body">
        <?php

use yii\helpers\Html;

$this->title = 'Annotated case report form';

$this->registerCssFile('/css/ezform.css');

?>

<div class='row'>
    <div class='col-lg-12'>
        <h4><?= $modelezform->ezf_detail; ?></h4>
    </div>
</div>
<hr>

<div class="row dad" id="formPanel">
    <?php $form = backend\modules\ezforms\components\EzActiveForm::begin(['action'=>'#', 'options' => ['enctype'=>'multipart/form-data']]); ?>
    <?php
    $form->attributes['rstat'] = 'annotated';// render annotated
    foreach($modelfield as $field){
        echo \backend\modules\ezforms\components\EzformFunc::getTypeEform($model_gen, $field, $form);
    }
    backend\assets\EzfGenAsset::register($this);
    ?>
    <div class='col-lg-12'>
        <p style='color:green;font-size: 15px;'><?php echo $message;?></p>
    </div>
    <?php backend\modules\ezforms\components\EzActiveForm::end();?>
</div>
      </div>
<div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
