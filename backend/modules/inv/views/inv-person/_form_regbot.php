<?php
/**
 * Created by PhpStorm.
 * User: hackablex
 * Date: 6/21/2016 AD
 * Time: 00:58
 */
use yii\bootstrap\Html;
use \yii\bootstrap\ActiveForm;
?>

<div class="modal-content">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h3 class="modal-title" id="ModalUser"><i class="fa fa-plus" aria-hidden="true"></i> เพิ่มข้อมูลจาก TDC</h3>
    </div>


    <!-- content -->
    <div class="modal-body">
            <?php $form = ActiveForm::begin([
                'id'=>'findbot-form',
                'layout'=>'inline',
            ]); ?>

            <div class="alert alert-success h4" id="results_regbot1" style="display: none;"></div>
            <div class="alert alert-danger h4" id="results_regbot2" style="display: none;"></div>

            <div class="form-group">
                <input type="text" name="findbot[cid]" value="<?=$queryParams['target'];?>" class="form-control" placeholder="เลขบัตรประชาชน 13 หลัก" style="width: 250px;">
            </div>
            <div class="form-group">
                <label>กุญแจถอดรหัสข้อมูล</label>
                <input type="password" name="findbot[key]" value="<?=$key?>" class="form-control" placeholder="โปรดกรอกรหัสที่กำหนดใน TDC" style="width: 250px;">
            </div>
            <div class="checkbox">
                <label>
                    <input type="checkbox" name="findbot[convert]" <?=$convert==1?'checked':''?> value="1"> เข้ารหัสแบบ tis620
                </label>
            </div>
            <input type="hidden" name="import_botconfirm" value="0">
            <?php echo Html::hiddenInput('comp_id', $queryParams['comp_id']) ?>
            <?php ActiveForm::end(); ?>
    </div>

    <div class="modal-footer">
        <button id="btn-importFromBot" type="button" class="btn btn-primary"><i class="glyphicon glyphicon-import"></i> ค้นหาข้อมูล</button>
    </div>


</div>

<?php  $this->registerJs("
$('.modal-footer').on('click', '#btn-importFromBot', function(){
    $('#modal-query-request .modal-footer').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');

    var \$form = $('form#findbot-form');
    var formData = new FormData(\$form[0]);
    $.ajax({
          url: \$form.attr('action'),
          type: 'POST',
          data: formData,
	  dataType: 'JSON',
	  //enctype: 'multipart/form-data',
	processData: false,  // tell jQuery not to process the data
	contentType: false,   // tell jQuery not to set contentType
          success: function (result) {
          $('#results_regbot1').hide();
          $('#results_regbot2').hide();
            if(result.status == 'success'){
                $('#results_regbot1').show();
                $('#results_regbot1').html(result.message);
                $(\"input[name=import_botconfirm]\").val(1);
                $('#modal-query-request .modal-footer').html('<button id=\"btn-importFromBotCreate\" type=\"button\" class=\"btn btn-success\"><i class=\"fa fa-plus\"></i> สร้าง PID</button>');
            }else{
                $('#results_regbot2').show();
                $('#results_regbot2').html(result.message);
                $('#modal-query-request .modal-footer').html('<button id=\"btn-importFromBot\" type=\"button\" class=\"btn btn-primary\"><i class=\"glyphicon glyphicon-import\"></i> ค้นหาข้อมูล</button>');
            }
          },
          error: function () {
	    console.log('server error');
          }
      });
    return false;
});

$('.modal-footer').on('click', '#btn-importFromBotCreate', function(){
    $('#modal-query-request .modal-footer').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
    
    var \$form = $('form#findbot-form');
    var formData = new FormData(\$form[0]);
    $.ajax({
          url: \$form.attr('action'),
          type: 'POST',
          data: formData,
	       dataType: 'JSON',
	  //enctype: 'multipart/form-data',
	processData: false,  // tell jQuery not to process the data
	contentType: false,   // tell jQuery not to set contentType
          success: function (result) {
            $('#results_regbot1').hide();
            $('#results_regbot2').hide();
            if(result.import == 'success'){
                $('#results_regbot1').show();
                $('#results_regbot1').html(result.message);
                
                $('#modal-query-request .modal-body').html('<div class=\"h3\">สร้าง PID ด้วย <code id=\"cid\"></code> สำเร็จ! &nbsp;&nbsp;HOSPCODE : <span class=\"text-danger\" id=\"hsitecode\"></span>&nbsp;PID : <span class=\"text-danger\" id=\"hptcode\"></span></div>');
		        $('#hsitecode').text(result.hsitecode);
		        $('#hptcode').text(result.hptcode);
		        $('#cid').text(result.cid);
		        $('#modal-query-request .modal-footer').html('<a class=\"btn btn-success\" href=\"redirect-page?ezf_id='+result.ezf_id+'&dataid='+result.dataid+'\"><i class=\"fa fa-file-text\" aria-hidden=\"true\"></i> เปิดฟอร์ม</a>&nbsp;<button type=\"button\" class=\"btn btn-danger\" data-dismiss=\"modal\"><i class=\"fa fa-times\" aria-hidden=\"true\"></i> ปิด</button>');
		
		$(document).find('#modal-query-request').modal('hide');
		
		$('#menu-box').show();
		
		
		js_target_decode = result.dataid;
		js_dataid = null;
		js_id = result.dataid;
		js_comp_id_target = result.comp_id_target;
		js_target = result.target;
		js_ezf_id = result.ezf_id;
		js_ezf_id_main = result.ezf_id_main;
		
                menu_target();
		//ezform_target(js_target_decode);
		
            }
	        else if(result.status == 'success'){
                $('#results_regbot1').show();
                $('#results_regbot1').html(result.message);
            }else{
                $('#results_regbot2').show();
                $('#results_regbot2').html(result.message);
                $('#modal-query-request .modal-footer').html('<button id=\"btn-importFromBot\" type=\"button\" class=\"btn btn-primary\"><i class=\"glyphicon glyphicon-import\"></i> ค้นหาข้อมูล</button>');
            }
	    
          },
          error: function () {
	    console.log('server error');
          }
      });
    return false;
});

");?>

