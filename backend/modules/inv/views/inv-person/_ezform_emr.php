<?php

use Yii;
use yii\helpers\Html;
use appxq\sdii\widgets\GridView;
use yii\helpers\Url;

/**
 * _ezform_popup file UTF-8
 * @author SDII <iencoded@gmail.com>
 * @copyright Copyright &copy; 2015 AppXQ
 * @license http://www.appxq.com/license/
 * @version 1.0.0 Date: 17 พ.ย. 2559 12:32:13
 * @link http://www.appxq.com/
 */
$xsourcex = Yii::$app->user->identity->userProfile->sitecode;
$xdepartmentx = Yii::$app->user->identity->userProfile->department_area;
$xdepartmentx_sql = $xdepartmentx > 0 ? ("AND xdepartmentx ='".$xdepartmentx."'") : null;
$input_ezf_id = Yii::$app->request->get('ezf_id');
$input_target= Yii::$app->request->get('target');
$input_comp_target =Yii::$app->request->get('comp_id_target');
$input_dataid =Yii::$app->request->get('dataid');
$ezform=  \backend\models\Ezform::findOne(['ezf_id'=>$input_ezf_id]);

$idgen = appxq\sdii\utils\SDUtility::getMillisecTime();
?>

<div id="emr-popup" class="fields-form form-<?=$idgen?>">

    <div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        
	<h4 class="modal-title" id="itemModalLabel">EMR 
            <?php if($addBtn==1):?>
            <a class="open-ezfrom btn btn-success" title="" data-url="<?= Url::to(['/inv/inv-person/ezform-print', 'ezf_id'=>$ezf_id, 'target'=>$target, 'comp_id_target'=>$comp_id_target])?>" style="cursor: pointer;" data-toggle="tooltip" data-original-title="เพิ่มข้อมูล"><i class="glyphicon glyphicon-plus"></i>  <?=$ezform->ezf_name;?></a>
            <?php endif;?>
        </h4>
        
    </div>
    <div class="modal-body" >
        <ul class="nav nav-tabs" style="margin-bottom: 10px;" >
            <li role="presentation" class="<?=($emrpage=='1')?'active':''?>"><a class="emr-show" href="<?= Url::to(['/inv/inv-person/ezform-emr',
                'ezf_id'=>$ezf_id,
                'dataid'=>$dataid,
                'target'=>$target,
                'comp_id_target'=>$comp_id_target,
                'addBtn'=>$addBtn,
                'emrpage'=>$emrpage,
                'page'=> isset($_GET['page'])?$_GET['page']:1,
                ])?>">EMR จากฟอร์มที่เลือก</a></li>
            <li role="presentation" class="<?=($emrpage=='2')?'active':''?>"><a class="emr-show" href="<?= Url::to(['/inv/inv-person/ezform-emr-all',
                'ezf_id'=>$ezf_id,
                'dataid'=>$dataid,
                'target'=>$target,
                'comp_id_target'=>$comp_id_target,
                'addBtn'=>$addBtn,
                'emrpage'=>$emrpage,
                'page'=> isset($_GET['page'])?$_GET['page']:1,
                ])?>">EMR ทั้งหมด</a></li>
      </ul>
	<?=
	GridView::widget([
	    'id' => 'emr-grid',
	    'panelBtn' => '',
	    'dataProvider' => $dataProvidertarget,
	    //'filterModel' => $searchModel,
	    'columns' => [
		    [
			'class' => 'yii\grid\SerialColumn',
			'headerOptions' => ['style' => 'text-align: center;'],
			'contentOptions' => ['style' => 'width:60px;text-align: center;'],
		    ],
		    [
		    'header' => 'ชื่อเป้าหมาย',
		    'format'=>'raw',
		    'value' => function ($model, $key, $index, $widget) use ($dataComponent) {
			
			if (isset($model->ptid) || isset($model->target)) {
                            
			    //$user = common\models\UserProfile::findOne($model->target);
			    //echo $arr_comp_desc_field_name; echo'<hr>'; print_r($arr_comp_desc_field_name); exit;
			    $comp_name = new \backend\models\Dynamic();
			    $comp_name->setTableName($dataComponent['ezf_table_comp']);
			    
			    if ($dataComponent['comp_id'] == 100000 OR $dataComponent['comp_id'] == 100001) {
				$comp_name = $comp_name::find()->where('user_id = :user_id', [':user_id' => $model->target])->one();
			    } else if ($dataComponent['special']) {
				$comp_name = $comp_name::find()->where('rstat <>3 AND xsourcex = :xsourcex AND ptid = :ptid', [':xsourcex' => $model->xsourcex, ':ptid' => $model->ptid])->one();
			    } else {
				$comp_name = $comp_name::find()->where('rstat <>3 AND xsourcex = :xsourcex AND id = :target', [':xsourcex' => $model->xsourcex, ':target' => $model->target])->one();
			    }
			    
			    $str = '';
			    foreach ($dataComponent['arr_comp_desc_field_name'] as $val) {
				$str .= $comp_name->$val . ' ';
			    }
			    
			    return $str;

			    //return $model->target;
			} elseif (isset($model['target_id'])) {
                            
                            $comp_name = new \backend\models\Dynamic();
			    $comp_name->setTableName($dataComponent['ezf_table_comp']);
                            
                            $comp_name = $comp_name::find()->where('rstat <>3 AND xsourcex = :xsourcex AND id = :target', [':xsourcex' => $model['xsourcex'], ':target' => $model['target_id']])->one();

                            $str = '';
			    foreach ($dataComponent['arr_comp_desc_field_name'] as $val) {
				$str .= $comp_name->$val . ' ';
			    }
			    
			    return $str;
                        } else 
			    return 'ไม่ระบุเป้าหมาย';
		    },
			],
			[
			    'header' => 'ลักษณะหลัก',
			    'format'=>'raw',
			    'value' => function ($model, $key, $index, $widget) use ($modelEzform) {
				if (strlen(trim($modelEzform->field_detail)) && !isset($model['ezf_name'])) {
                                    if (isset($model['target_id'])) {
                                        $id = $model['data_id'];
                                    } else {
                                        $id = $model->id;
                                    }
                                    
				    $arr_desc = explode(',', $modelEzform->field_detail);
				    $str_desc = '';
				    foreach ($arr_desc as $val) {
					$str_desc .= $val . ', ';
				    }
				    $str_desc = substr($str_desc, 0, -2);
				    try {
					$res = Yii::$app->db->createCommand("SELECT " . $str_desc . " FROM `" . ($modelEzform->ezf_table) . "` WHERE id = '" . $id . "';")->queryOne();
				    } catch (\yii\db\Exception $e) {
					try {
					    $res = Yii::$app->db->createCommand("SELECT " . $str_desc . " FROM `" . ($modelEzform->ezf_table) . "` WHERE user_id = '" . $id . "';")->queryOne();
					} catch (\yii\db\Exception $e) {
					    
					}
				    }
				    $str_desc = '';
				    foreach ($res as $val) {
					$str_desc .= $val . ' ';
				    }
				} elseif (isset($model['ezf_name'])) {
                                    return $model['ezf_name'];
                                } else {
                                
				    $str_desc = 'ไม่ได้ระบุ';
				}
                                
				return $str_desc;
			    },
			],
			[
			    'header' => 'หน่วยงาน',
			    'format'=>'raw',
			    'value' => function ($model, $key, $index, $widget) {
				$hospital = \backend\modules\ezforms\components\EzformQuery::getHospital($model['xsourcex']);
				$html = '<span class="label label-success" data-toggle="tooltip" data-original-title="' . ($hospital['name'] . ' ต.' . $hospital['tambon'] . ' อ.' . $hospital['amphur'] . ' จ.' . $hospital['province']) . '">' . $hospital['hcode'] . '</span>';
				return $html;
			    },
			],
			[
			    'attribute' => 'update_date',
			    'label' => 'วันที่แก้ไขล่าสุด',
			    'value' => function ($model, $key, $index, $widget) {
				//return Html::a(Html::encode(""), '#');
				if ($model['update_date']) {
				    $date = new DateTime($model['update_date']);
				    return $date->format('d/m/Y (H:i:s)');
				}

				$date = new DateTime($model['create_date']);
				return $date->format('d/m/Y (H:i:s)');
			    },
			],
			[
			    'header' => 'บันทึกโดย',
			    'format'=>'raw',
			    'value' => function ($model, $key, $index, $widget) {
				if ($model['user_update'] == "") {
				    $user = common\models\UserProfile::findOne($model['user_create']);
				} else {
				    $user = common\models\UserProfile::findOne($model['user_update']);
				}
				return '<span class="text-center fa fa-2x fa-user text-warning" data-toggle="tooltip" data-original-title="บันทึกล่าสุดโดย : ' . $user->firstname . ' ' . $user->lastname . '"></span>';

				//return $model->firstname.' '.$model->lastname;
				//return $model->target;
			    },
			],
			[
			    'header' => 'สถานะ',
			    'format'=>'raw',
			    'value' => function ($model, $key, $index, $widget) {
				if ($model['rstat'] == 0) {
				    return Html::a('<i class="fa fa-pencil-square-o"></i> New Record', null, ['data-pjax' => 0, 'class' => 'text-default', 'data-toggle' => 'tooltip', 'data-original-title' => Yii::t('kvgrid', 'รอการกรอกข้อมูล')]);
				} else if ($model['rstat'] == 1) {
				    return Html::a('<i class="fa fa-pencil-square-o"></i> Waiting', null, ['data-pjax' => 0, 'class' => 'text-warning', 'data-toggle' => 'tooltip', 'data-original-title' => Yii::t('kvgrid', 'ข้อมูลยังไม่ถูกส่งเข้าระบบด้วยการคลิก Submitted')]);
				} else if ($model['rstat'] == 2 || $model['rstat'] >= 4) {
				    return Html::a('<i class="fa fa-send"></i> Submitted', null, ['data-pjax' => 0, 'class' => 'text-success', 'data-toggle' => 'tooltip', 'data-original-title' => Yii::t('kvgrid', 'ข้อมูลถูกส่งเข้าระบบเรียบร้อยแล้ว')]);
				}

				//return $model->firstname.' '.$model->lastname;
				//return $model->target;
			    },
			],
			[
                                                    'header' => 'Action',
			    'format'=>'raw',
                                                    'value' => function ($model, $key, $index, $widget) use ($xsourcex, $input_comp_target, $input_ezf_id, $input_target) {
                                                         if (isset($model['target_id'])) {
                                                             $id = $model['data_id'];
                                                             $target_id = $model['target_id'];
                                                             $ezf_id = $model['ezf_id'];
                                                         } else {
                                                             $id = $model->id;
                                                             $target_id = $input_target;
                                                             $ezf_id = $input_ezf_id;
                                                         }
                                                         
							if($model['xsourcex'] !=$xsourcex ){//|| $model->rstat>1
							    $html = Html::a('<i class="fa fa-view"></i> อ่านอย่างเดียว', NULL , ['data-url'=>Url::to(['/inv/inv-person/ezform-print',
							    'ezf_id'=>$ezf_id,
							    'target'=>$target_id,
							    'dataid'=>$id,				
							    'comp_id_target'=>$input_comp_target,
								'readonly'=>1,
							    ]),'data-pjax' => 0, 'style'=>'cursor: pointer;', 'class' => 'open-ezfrom open-form btn btn-warning', 'data-toggle' =>'tooltip', 'data-original-title' => Yii::t('kvgrid', 'แสดงข้อมูลนี้')]);
							    
							}else{
							    
                                                            $html = Html::a('<i class="fa fa-edit"></i> ดู / แก้ไข', NULL, ['data-url'=>Url::to(['/inv/inv-person/ezform-print',
							    'ezf_id'=>$ezf_id,
							    'target'=>$target_id,
							    'dataid'=>$id,				
							    'comp_id_target'=>$input_comp_target,
								'readonly'=>0,
							    ]),'data-pjax' => 0, 'style'=>'cursor: pointer;', 'class' => 'open-ezfrom open-form btn btn-primary', 'data-toggle' =>'tooltip', 'data-original-title' => Yii::t('kvgrid', 'แก้ไขข้อมูลนี้')]);
                                                         }
                                                         return $html;
                                                    },
                                                            
                                                        ],	    
			
	    ],
	]);
	?>

			    </div>

			</div>

			<?php
			//window.print();
			$this->registerJs("

$('.open-ezfrom').on('click', function() {
    modalEzfrom($(this).attr('data-url'));
    return false;
});

$('.pagination a').on('click', function() {
    modalEzfromEmrUpdate($(this).attr('href'));
    return false;
});

$('.emr-show').on('click', function() {
    modalEzfromEmrUpdate($(this).attr('href'));
    return false;
});

function modalEzfrom(url) {
    $('#modal-print .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
    $('#modal-print').modal('show');
    $.ajax({
	method: 'POST',
	url: url,
	dataType: 'HTML',
	success: function(result, textStatus) {
	    $('#modal-print .modal-content').html(result);
	    return false;
	}
    });
}

function modalEzfromEmrUpdate(url) {
    $('#modal-inv-emr .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
    $.ajax({
	method: 'POST',
	url: url,
	dataType: 'HTML',
	success: function(result, textStatus) {
	    $('#modal-inv-emr .modal-content').html(result);
	    return false;
	}
    });
}

");
			?>


