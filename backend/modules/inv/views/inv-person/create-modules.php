<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\inv\models\InvMain */
$title = 'สร้างโมดูล';
$labelHome = 'แสดงตัวอย่างโมดูลที่ได้จากการตั้งค่านี้';
if(isset($model['gname']) && $model['gname']!=''){
    $title = 'ตั้งค่าโมดูล';
    $labelHome = $model['gname'];
}

$this->title = Yii::t('app', $title);
$this->params['breadcrumbs'][] = ['label' => $labelHome, 'url' => ['/inv/inv-person/index', 'module' => $module, 'comp'=>$ezf_id]];
$this->params['breadcrumbs'][] = ['label' => 'จัดการโมดูล', 'url' => ['/inv/inv-gen/index']];
if($module==0){
    $this->params['breadcrumbs'][] = ['label' => 'EzModule', 'url' => ['/inv/inv-person/main', 'ezf_id' => $ezf_id, 'module' => $module]];
}

$this->params['breadcrumbs'][] = $this->title;
?>
<div class="inv-main-create">
    
    <?= $this->render('_form_module', [
	    'model' => $model,
	    'module' => $module,
	    'ezf_id' => $ezf_id,
	    'dataFields' => $dataFields,
	    'modelEzform' => $modelEzform,
	    'dataSubForm' => $dataSubForm,
	    'dataSelect' => $dataSelect,
    ]) ?>

</div>
