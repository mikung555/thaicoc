<?php
use Yii;
use yii\helpers\Html;
use yii\bootstrap\Tabs;
use yii\helpers\Url;
use backend\modules\inv\models\InvMenu;

$moduleID = '';
$controllerID = '';
$actionID = '';

if (isset(Yii::$app->controller->module->id)) {
	    $moduleID = Yii::$app->controller->module->id;
}
if (isset(Yii::$app->controller->id)) {
	    $controllerID = Yii::$app->controller->id;
}
if (isset(Yii::$app->controller->action->id)) {
	    $actionID = Yii::$app->controller->action->id;
}

?>

<?php
$gtype = isset($gtype)?$gtype:0;
$setUrl = 'inv-person';
if($gtype==3){
    $setUrl = 'inv-map';
}
elseif ($gtype==4) {
    $setUrl = 'inv-map';
}

$modelGen = \backend\modules\inv\models\InvGen::find()->where('parent_gid=0 AND gid=:gid', [':gid'=>$module])->one();//, ':created_by'=> Yii::$app->user->id
if($modelGen){
    if((Yii::$app->user->can('administrator')) || $modelGen['created_by']==Yii::$app->user->id){
	echo Html::a('', ["/inv/$setUrl/create-modules", 'module'=>$module], ['class'=>'fa fa-cog fa-2x pull-right',
	    'data-toggle'=>'tooltip',
	    'title'=>'ตั้งค่าโมดูล',
            'style'=>'margin-bottom: 15px;',
	]);
    }
    
    if((Yii::$app->user->can('administrator')) || $modelGen['created_by']==Yii::$app->user->id){
	if(isset($id) && $id>0){

	    echo Html::a('', ['/inv/inv-menu/delete', 'module'=>$module, 'gtype'=>$gtype, 'id'=>$id], ['class'=>'fa fa-trash-o fa-2x pull-right',
		'data-toggle'=>'tooltip',
		'title'=>'ลบเมนู',
		'data' => [
		    'confirm' => "คุณแน่ใจที่จะต้องการลบข้อมูลนี้หรือไม่",
		    'method' => 'post',
		],
                'style'=>'margin-bottom: 15px;',
	    ]);//'data-confirm'=>'คุณแน่ใจที่จะต้องการลบข้อมูลนี้หรือไม่?'
	    echo Html::a('', ['/inv/inv-menu/update', 'module'=>$module, 'gtype'=>$gtype, 'id'=>$id], ['class'=>'fa fa-pencil-square-o fa-2x pull-right',
		'data-toggle'=>'tooltip',
                'style'=>'margin-bottom: 15px;',
		'title'=>'แก้ไขเมนู']);
	}

        echo Html::a('', ['/inv/inv-person/sub-modules', 'module'=>$module], ['class'=>'fa fa-archive fa-2x pull-right',
		'data-toggle'=>'tooltip',
            'style'=>'margin-bottom: 15px;',
		'title'=>'เพิ่มโมดูล'
	    ]);
        
	echo Html::a('', ['/inv/inv-menu/create', 'module'=>$module, 'gtype'=>$gtype], ['class'=>'fa fa-plus fa-2x pull-right',
		'data-toggle'=>'tooltip',
            'style'=>'margin-bottom: 15px;',
		'title'=>'สร้างเมนูใหม่'
	    ]);
    }
}
?>

<?php
$labelHome = isset($modelGen['gname'])?$modelGen['gname']:'Module';




$items = [
	[
	    'label' => 'Dashboard',
	    'url' => Url::to(['/inv/inv-fix/index', 'module'=>$module]),
	    'active'=>$controllerID=='inv-fix' && $actionID=='index',
            'visible'=>$gtype!=3,
	],
	[
	    'label' => 'Workbench',
	    'url' => Url::to(["/inv/$setUrl/index", 'module'=>$module]),
	    'active'=>$controllerID==$setUrl && $actionID=='index', 
	],
        [
	    'label' => 'Assigned Cases',
	    'url' => Url::to(["/inv/inv-person/index2", 'module'=>$module]),
	    'active'=>$controllerID=='inv-person' && $actionID=='index2',
            'visible'=>$gtype!=3,
	],
	[
	    'label' => 'Report',
	    'url' => Url::to(['/inv/inv-fix/report', 'module'=>$module]),
	    'active'=>$controllerID=='inv-fix' && $actionID=='report',
            'visible'=>$gtype!=3,
	],
    ];

$modelmainmenu = InvMenu::find()->where(['gid' => $module, 'menu_parent'=>0])->orderBy('menu_order')->all();
foreach ($modelmainmenu as $key => $value) {
    
    $submenu = InvMenu::find()->where(['gid' => $module, 'menu_parent'=>$value['menu_id']])->orderBy('menu_order')->all();
    if($submenu){
	$subItems = [];
	$subId = [];
	foreach ($submenu as $subKey => $subValue) {
	    $subItems[] = [
		'label' => $subValue['menu_name'],
		'url' => Url::to(['/inv/inv-menu/view', 'module'=>$module, 'gtype'=>$gtype, 'id'=>$subValue['menu_id']]),
		'active'=>$controllerID=='inv-menu' && $_GET['id']==$subValue['menu_id'],
	    ];
	    $subId[] = $subValue['menu_id'];
	}
	
	$items[] = [
	    'label' => $value['menu_name'],
	    'url' => '#',
	    'items' => $subItems,
	    'dropDownOptions'=>['id'=>  common\lib\codeerror\helpers\GenMillisecTime::getMillisecTime()],
	    'active'=>$controllerID=='inv-menu' && in_array($_GET['id'], $subId),
	];
    } else {
	$items[] = [
	    'label' => $value['menu_name'],
	    'url' => Url::to(['/inv/inv-menu/view', 'module'=>$module, 'gtype'=>$gtype, 'id'=>$value['menu_id']]),
	    'active'=>$controllerID=='inv-menu' && $_GET['id']==$value['menu_id'],
	];
    }
}


$submodule;

if($controllerID == 'inv-person' && $actionID == 'index2'){
    $submodule = backend\modules\inv\classes\InvQuery::getSubModuleAllType($module);
} elseif ($controllerID == 'inv-fix') {
    $submodule = backend\modules\inv\classes\InvQuery::getSubModuleAllType($module);
} else {
    $submodule = backend\modules\inv\classes\InvQuery::getSubModuleAll($module);
}

$subModuleItems=[];

$actionPath = $actionID=='sub-modules'?'index':$actionID;

$actionPath = $actionPath=='create-modules'?'index':$actionPath;

if($submodule){
        $imgUrl = (isset($modelGen['subicon']) && $modelGen['subicon'] != '')? Yii::getAlias('@backendUrl').'/module_icon/'.$modelGen['subicon']:Yii::getAlias('@backendUrl').'/module_icon/no_icon.png';
        
        $mname = isset($modelGen['subname']) && $modelGen['subname']!=''?$modelGen['subname']:$modelGen['gname'];
        
        $subModuleItems[] = [
            'label' => Html::img($imgUrl, ['class'=>'img-rounded', 'height'=>25]).' '. $mname,
            'url' => Url::to(["/inv/inv-person/index", 'module'=>$module, 'sub_module'=>0 ]),
            'active'=>($sub_module==0 && $actionID!='sub-modules' && $actionID!='create-modules') && $controllerID!='inv-menu',
        ];
    foreach ($submodule as $mKey => $mValue) {
        $imgUrl = (isset($mValue['gicon']) && $mValue['gicon'] != '')? Yii::getAlias('@backendUrl').'/module_icon/'.$mValue['gicon']:Yii::getAlias('@backendUrl').'/module_icon/no_icon.png';
        
        if($controllerID=='inv-menu'){
            $controllerID = 'inv-person';
            $actionPath = 'index';
        }
        
        $setNewController = ($controllerID=='inv-person' && $mValue['gtype']==2)?'inv-map':$controllerID;
        
        $setNewController = ($controllerID=='inv-map' && $mValue['gtype']==0)?'inv-person':$setNewController;
        
        $subModuleItems[] = [
            'label' => Html::img($imgUrl, ['class'=>'img-rounded', 'height'=>25]).' '. $mValue['gname'],
            'url' => Url::to(["/inv/$setNewController/$actionPath", 'module'=>$module, 'sub_module'=>$mValue['gid'] ]),
            'active'=>$sub_module==$mValue['gid'] && $actionID!='sub-modules',
        ];
    }
}

?>

<?= \yii\bootstrap\Nav::widget([
    'items' => $items,
    'options' => ['class'=>'nav nav-pills', 'id'=>'main_menu'],
]);
?>
<br>

<?php
if(((Yii::$app->user->can('administrator')) || $modelGen['created_by']==Yii::$app->user->id) && $sub_module>0){
        $subModelGen = \backend\modules\inv\models\InvGen::find()->where('gid=:gid', [':gid'=>$sub_module])->one();
        
	echo Html::a('', ["/inv/inv-person/sub-modules", 'module'=>$module, 'sub_module'=>$sub_module, 'main_ezf_id'=>$subModelGen['main_ezf_id'], 'gtype'=>$subModelGen['gtype']], ['class'=>'fa fa-wrench fa-2x pull-right',
	    'data-toggle'=>'tooltip',
            'style'=>'margin-bottom: 18px;',
	    'title'=>'ตั้งค่าโมดูล'
	]);
    }
    if($sub_module>0) {
?>

<a style="cursor: pointer; margin-bottom: 18px;" data-toggle="tooltip" title="รายละเอียดแอพ" data-url="<?=Url::to(['/inv/inv-person/info-app', 'gid'=>$sub_module])?>" class="fa fa-info-circle fa-2x pull-right info-app"></a>
    <?php } ?>
       
<?= \yii\bootstrap\Nav::widget([
    'items' => $subModuleItems,
    'encodeLabels'=>false,
    'options' => ['class'=>'nav nav-tabs', 'id'=>'sub_menu'],
]);
?>

<?=    \appxq\sdii\widgets\ModalForm::widget([
    'id' => 'modal-app',
    //'size'=>'modal-lg',
]);
?>

<?php  $this->registerJs("
$('.info-app').on('click', function() {
    modalApp($(this).attr('data-url'),$(this).attr('id'));
});    

function modalApp(url,directUrl) {
    $('#modal-app .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
    $('#modal-app').modal('show')
    .find('.modal-content')
    .load(url);
}
");?>