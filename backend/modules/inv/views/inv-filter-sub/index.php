<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\helpers\Url;
use appxq\sdii\widgets\GridView;
use appxq\sdii\widgets\ModalForm;
use appxq\sdii\helpers\SDNoty;
use appxq\sdii\helpers\SDHtml;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\inv\models\InvFilterSubSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'ใบกำกับงาน');
$this->params['breadcrumbs'][] = ['label' => 'แสดงตัวอย่างใบกำกับงาน', 'url' => ['/inv/inv-person/index', 'module'=>$module, 'comp'=>$comp]];
$this->params['breadcrumbs'][] = ['label' => 'จัดการโมดูล', 'url' => ['/inv/inv-gen/index']];
if($module==0){
    $this->params['breadcrumbs'][] = ['label' => 'Module Maker', 'url' => ['/inv/inv-person/main', 'ezf_id' => $comp, 'module' => $module]];
}
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="inv-filter-sub-index">

    <?php
    if($module>0){
	echo $this->render('/inv-person/_menu', ['module'=>$module, 'comp'=>$comp]);
    }
    ?>
    
    <?php  Pjax::begin(['id'=>'inv-filter-sub-grid-pjax']);?>
    <?= GridView::widget([
	'id' => 'inv-filter-sub-grid',
	'panelBtn' => Html::button(SDHtml::getBtnAdd(), ['data-url'=>Url::to(['inv-filter-sub/create', 'comp'=>$comp, 'module' => $module]), 'class' => 'btn btn-success btn-sm', 'id'=>'modal-addbtn-inv-filter-sub']). ' ' .
		      Html::button(SDHtml::getBtnDelete(), ['data-url'=>Url::to(['inv-filter-sub/deletes']), 'class' => 'btn btn-danger btn-sm', 'id'=>'modal-delbtn-inv-filter-sub', 'disabled'=>true]),
	'dataProvider' => $dataProvider,
	'filterModel' => $searchModel,
        'columns' => [
	    [
		'class' => 'yii\grid\CheckboxColumn',
		'checkboxOptions' => [
		    'class' => 'selectionInvFilterSubIds'
		],
		'headerOptions' => ['style'=>'text-align: center;'],
		'contentOptions' => ['style'=>'width:40px;text-align: center;'],
	    ],
	    [
		'class' => 'yii\grid\SerialColumn',
		'headerOptions' => ['style'=>'text-align: center;'],
		'contentOptions' => ['style'=>'width:60px;text-align: center;'],
	    ],

            'sub_name',
	    //'urine_status',
	    [
		'attribute'=>'public',
		'value'=>function ($data){ 
		    return $data->public==1?'Yes':'No';
		},
		'filter'=>  Html::activeDropDownList($searchModel, 'public', ['No', 'Yes'], ['class'=>'form-control', 'prompt'=>'All']),
		'headerOptions'=>['style'=>'text-align: center;'],
		'contentOptions'=>['style'=>'min-width:60px; text-align: center;'],
	    ],
	    [
		'attribute'=>'share',
		'value'=>function ($data){ 
		    $arry = [];
		    if(isset($data->share) && $data->share!=''){
			$arry = explode(',', $data->share);
		    }
		    
		    return count($arry);
		},
		'filter'=>  '',
		'headerOptions'=>['style'=>'text-align: center;'],
		'contentOptions'=>['style'=>'min-width:60px; text-align: center;'],
	    ],		
	    [
		'header'=>'n=?',
		'format'=>'raw',
		'value'=>function ($data){ 
		    $count = 'เงื่อนไข';
		    if($data['filter_order']==0){
			$count = backend\modules\inv\models\InvSubList::find()->where('filter_id=:filter_id AND sub_id=:sub_id', ['filter_id'=>$data['filter_id'], 'sub_id'=>$data['sub_id']])->count();
		    }
		    
		    return $count;
		},
		'filter'=>'',
		'headerOptions'=>['style'=>'text-align: center;'],
		'contentOptions'=>['style'=>'min-width:70px; text-align: center;'],
	    ],
//	    [
//		'header'=>'',
//		'format'=>'raw',
//		'value'=>function ($data){ 
//		    $count = \yii\bootstrap\Html::a('<span class="glyphicon glyphicon-plus"></span> เพิ่มฟอร์มสำหรับกำกับงาน', Url::to(['/inv/inv-fields/index', 'comp'=>$data['filter_id'], 'module' => $data['gid'], 'sub_id'=>$data['sub_id']]), ['class'=>'btn btn-success']);
//		    
//		    return $count;
//		},
//		'filter'=>'',
//		'headerOptions'=>['style'=>'text-align: center;'],
//		'contentOptions'=>['style'=>'min-width:70px; text-align: center;'],
//	    ],
	    [
		'class' => 'appxq\sdii\widgets\ActionColumn',
		'contentOptions' => ['style'=>'width:80px;text-align: center;'],
		'template' => '{update} {delete}',
		'buttons'=>[
		    'update' => function ($url, $data, $key) {
			    if(Yii::$app->user->id==$data['created_by']){
				    return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
					'data-action' => 'update',
					'title' => Yii::t('yii', 'Update'),
					'data-pjax' => isset($this->pjax_id)?$this->pjax_id:'0',
				    ]);
			    }
		    },
		    'delete' => function ($url, $data, $key) {
			    if(Yii::$app->user->id==$data['created_by']){
				    return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
				    'data-action' => 'delete',
				    'title' => Yii::t('yii', 'Delete'),
				    'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
				    'data-method' => 'post',
				    'data-pjax' => isset($this->pjax_id)?$this->pjax_id:'0',
				    ]);
			    }
		    }	    
		]
	    ],
        ],
    ]); ?>
    <?php  Pjax::end();?>

</div>

<?=  ModalForm::widget([
    'id' => 'modal-inv-filter-sub',
    'size'=>'modal-lg',
]);
?>

<?php  $this->registerJs("
$('#inv-filter-sub-grid-pjax').on('click', '#modal-addbtn-inv-filter-sub', function() {
    modalInvFilterSub($(this).attr('data-url'));
});

$('#inv-filter-sub-grid-pjax').on('click', '#modal-delbtn-inv-filter-sub', function() {
    selectionInvFilterSubGrid($(this).attr('data-url'));
});

$('#inv-filter-sub-grid-pjax').on('click', '.select-on-check-all', function() {
    window.setTimeout(function() {
	var key = $('#inv-filter-sub-grid').yiiGridView('getSelectedRows');
	disabledInvFilterSubBtn(key.length);
    },100);
});

$('#inv-filter-sub-grid-pjax').on('click', '.selectionInvFilterSubIds', function() {
    var key = $('input:checked[class=\"'+$(this).attr('class')+'\"]');
    disabledInvFilterSubBtn(key.length);
});

$('#inv-filter-sub-grid-pjax').on('dblclick', 'tbody tr', function() {
    var id = $(this).attr('data-key');
    modalInvFilterSub('".Url::to(['inv-filter-sub/update', 'id'=>''])."'+id);
});	

$('#inv-filter-sub-grid-pjax').on('click', 'tbody tr td a', function() {
    var url = $(this).attr('href');
    var action = $(this).attr('data-action');

    if(action === 'update' || action === 'view') {
	modalInvFilterSub(url);
	return false;
    } else if(action === 'delete') {
	yii.confirm('".Yii::t('app', 'Are you sure you want to delete this item?')."', function() {
	    $.post(
		url
	    ).done(function(result) {
		if(result.status == 'success') {
		    ". SDNoty::show('result.message', 'result.status') ."
		    $.pjax.reload({container:'#inv-filter-sub-grid-pjax'});
		} else {
		    ". SDNoty::show('result.message', 'result.status') ."
		}
	    }).fail(function() {
		". SDNoty::show("'" . SDHtml::getMsgError() . "Server Error'", '"error"') ."
		console.log('server error');
	    });
	});
	return false;
    }
    
});

function disabledInvFilterSubBtn(num) {
    if(num>0) {
	$('#modal-delbtn-inv-filter-sub').attr('disabled', false);
    } else {
	$('#modal-delbtn-inv-filter-sub').attr('disabled', true);
    }
}

function selectionInvFilterSubGrid(url) {
    yii.confirm('".Yii::t('app', 'Are you sure you want to delete these items?')."', function() {
	$.ajax({
	    method: 'POST',
	    url: url,
	    data: $('.selectionInvFilterSubIds:checked[name=\"selection[]\"]').serialize(),
	    dataType: 'JSON',
	    success: function(result, textStatus) {
		if(result.status == 'success') {
		    ". SDNoty::show('result.message', 'result.status') ."
		    $.pjax.reload({container:'#inv-filter-sub-grid-pjax'});
		} else {
		    ". SDNoty::show('result.message', 'result.status') ."
		}
	    }
	});
    });
}

function modalInvFilterSub(url) {
    $('#modal-inv-filter-sub .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
    $('#modal-inv-filter-sub').modal('show')
    .find('.modal-content')
    .load(url);
}

");?>