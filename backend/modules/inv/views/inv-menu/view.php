<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\inv\models\InvMenu */
$labelHome = isset($modelGen['gname'])?$modelGen['gname']:'Module';
$this->title = $model->menu_name;
$this->params['breadcrumbs'][] = ['label' => $labelHome, 'url' => ['/inv/inv-person/index', 'module' => $module]];
$this->params['breadcrumbs'][] = $this->title;

$gtype = isset($_GET['gtype'])?$_GET['gtype']:0;
?>

<?php
if($module>0){
    echo $this->render('/inv-person/_menu', ['module'=>$module, 'gtype'=>$gtype, 'id'=>$model->menu_id]);
}
?>

<div class="inv-menu-view">
    <div class="modal-body">
        <?php echo $model->menu_content;?>
    </div>
</div>
