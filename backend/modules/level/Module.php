<?php

namespace backend\modules\level;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\modules\level\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
