<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\level\models\LevelManagerRight */
$user = \common\models\UserProfile::find()->where(['user_id' => $model->user_create])->one();
$this->title = 'การจัดการสิทธิ์การเข้าใช้งานของ ' . $user['firstname'] . ' ' . $user['lastname'];
$this->params['breadcrumbs'][] = ['label' => 'Level Manager Rights', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="level-manager-right-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('< Back', ['/level/level-manager-right', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <hr>
    <table class="table table-striped table-bordered detail-view">
        <tbody>
        <tr>
            <th>ผู้ได้รับสิทธิ์</th>
            <td>
                <?php
                $user = \common\models\UserProfile::find()->where(['user_id' => $model->target])->one();
                echo $user['firstname'] . ' ' . $user['lastname'];
                ?>
            </td>
        </tr>
        <tr>
            <th>สิทธิ์</th>
            <td>
                <?php
                $items = [
                    '1' => 'ผู้ดูแลระดับอำเภอ',
                    '2' => 'ผู้ดูแลระดับจังหวัด'
                ];
                echo $items[$model->p];
                ?>
            </td>
        </tr>
        <tr>
            <th>วันที่มอบสิทธิ์</th>
            <td>
                <?php
                $date = new DateTime($model->create_date);
                echo $date->format('d/m/') . ($date->format('Y') + 543);
                ?>
            </td>
        </tr>
        <tr>
            <th>ผู้ให้สิทธิ์</th>
            <td>
                <?php
                $user = \common\models\UserProfile::find()->where(['user_id' => $model->user_create])->one();
                echo $user['firstname'] . ' ' . $user['lastname'];
                ?>
            </td>
        </tr>
        </tbody>
    </table>

</div>
