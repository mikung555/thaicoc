<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\level\models\LevelManagerRightSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Level Manager Rights';
$this->params['breadcrumbs'][] = $this->title;


if(!Yii::$app->user->can('administrator')){
    echo '<div class="alert alert-danger">คุณไม่มีสิทธ์ใช้งาน Module นี้ กรุณาติดต่อผู้ดูแลระบบ</div>';
    exit;
}
$fieldSearch = $_GET['LevelManagerRightSearch'];

?>
<div class="level-manager-right-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('< Back', ['/level', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Create Level Manager Right', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            //xsourcex',
            //'rstat',
            [
                'attribute' => 'target',
                'label' => 'ผู้ได้รับสิทธิ์',
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-left'],
                'filter' => \kartik\widgets\Select2::widget([
                    'name' => 'LevelManagerRightSearch[target]',
                    'value' => is_null($fieldSearch['target']) ? null : $fieldSearch['target'],
                    'data' => $filterType['target'],
                    'options' => ['placeholder' => 'Select for filter ...'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]),
                'value' => function($model){
                    $user = \common\models\UserProfile::find()->where(['user_id'=>$model->target])->one();
                    return $user['firstname'].' '.$user['lastname'];
                }
            ],
            [
                'attribute' => 'p',
                'label' => 'สิทธิ์',
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-left'],
                'filter' => \kartik\widgets\Select2::widget([
                    'name' => 'LevelManagerRightSearch[p]',
                    'value' => is_null($fieldSearch['p']) ? null : $fieldSearch['p'],
                    'data' => [
                        '1' => 'ผู้ดูแลระดับอำเภอ',
                        '2' => 'ผู้ดูแลระดับจังหวัด',
                        '3' => 'ผู้ดูแลระดับเขต',
                        '4' => 'ผู้ดูแลระดับกระทรวง',
                    ],
                    'options' => ['placeholder' => 'Select for filter ...'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]),
                'value' => function($model){
                    $items = [
                        '1' => 'ผู้ดูแลระดับอำเภอ',
                        '2' => 'ผู้ดูแลระดับจังหวัด',
                        '3' => 'ผู้ดูแลระดับเขต',
                        '4' => 'ผู้ดูแลระดับกระทรวง',
                    ];
                    return $items[$model->p];
                }
            ],
            [
                'attribute' => 'create_date',
                'label' => 'วันที่มอบสิทธิ์',
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'value' => function($model){
                    $date = new DateTime($model->create_date);
                    return $date->format('d/m/').($date->format('Y')+543);
                }
            ],
            [
                'attribute' => 'user_create',
                'label' => 'ผู้ให้สิทธิ์',
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-left'],
                'filter' => \kartik\widgets\Select2::widget([
                    'name' => 'LevelManagerRightSearch[user_create]',
                    'value' => is_null($fieldSearch['user_create']) ? null : $fieldSearch['user_create'],
                    'data' => $filterType['user_create'],
                    'options' => ['placeholder' => 'Select for filter ...'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]),
                'value' => function($model){
                    $user = \common\models\UserProfile::find()->where(['user_id'=>$model->user_create])->one();
                    return $user['firstname'].' '.$user['lastname'];
                }
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
