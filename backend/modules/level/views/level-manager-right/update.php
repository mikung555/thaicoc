<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\level\models\LevelManagerRight */

$this->title = 'Update Level Manager Right: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Level Manager Rights', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="level-manager-right-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'userList' => $userList
    ]) ?>

</div>
