<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\level\models\LevelManagerRightSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="level-manager-right-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'xsourcex') ?>

    <?= $form->field($model, 'rstat') ?>

    <?= $form->field($model, 'user_create') ?>

    <?= $form->field($model, 'create_date') ?>

    <?php // echo $form->field($model, 'user_update') ?>

    <?php // echo $form->field($model, 'update_date') ?>

    <?php // echo $form->field($model, 'target') ?>

    <?php // echo $form->field($model, 'p') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
