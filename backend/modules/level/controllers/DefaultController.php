<?php

namespace backend\modules\level\controllers;

use Yii;
use app\modules\adddoctor\models\AllHospitalThai;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\VarDumper;
use yii\web\Controller;

class DefaultController extends Controller
{
    public function actionIndex()
    {
        $areaUser = AllHospitalThai::find()->select('provincecode, amphurcode')->where(['hcode'=>\Yii::$app->user->identity->userProfile->sitecode])->one();
        $res = \Yii::$app->db->createCommand("SELECT id, p FROM tbdata_coc_right WHERE target=:user_id AND rstat <> 3", [':user_id'=>Yii::$app->user->identity->id])->queryOne();
        $array = [];
        if(($res['id'] && $res['p']==2) || Yii::$app->user->can('administrator')) {
            $array = AllHospitalThai::find()->select('name, hcode, province, amphur')->where('provincecode=:provincecode', [':provincecode'=>$areaUser->provincecode])->all();
            $array = ArrayHelper::map($array, 'hcode', 'name', 'amphur');
        }else  if($res['id'] && $res['p']==1) {
            $array = AllHospitalThai::find()->select('name, hcode, province, amphur')->where('provincecode=:provincecode and amphurcode=:amphurcode', [':provincecode'=>$areaUser->provincecode, ':amphurcode'=>$areaUser->amphurcode])->all();
            $array = ArrayHelper::map($array, 'hcode', 'name', 'amphur');
        }
        return $this->render('index', [
            'array' => $array,
        ]);
    }

    public function actionSwap($hcode)
    {
        $res = \Yii::$app->db->createCommand("SELECT id, p FROM tbdata_coc_right WHERE target=:user_id AND rstat <> 3", [':user_id'=>Yii::$app->user->identity->id])->queryOne();

        if($res['id'] || Yii::$app->user->can('administrator')) {
            \Yii::$app->db->createCommand()->update('user_profile', ['sitecode'=>$hcode], ['user_id'=>\Yii::$app->user->identity->id])->execute();;
        }

        return $this->redirect(Url::to(['/']));
    }
}
