<?php

namespace backend\modules\level\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\level\models\LevelManagerRight;

/**
 * LevelManagerRightSearch represents the model behind the search form about `backend\modules\level\models\LevelManagerRight`.
 */
class LevelManagerRightSearch extends LevelManagerRight
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'rstat', 'user_create', 'user_update', 'target'], 'integer'],
            [['xsourcex', 'create_date', 'update_date', 'p'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = LevelManagerRight::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'rstat' => $this->rstat,
            'user_create' => $this->user_create,
            'create_date' => $this->create_date,
            'user_update' => $this->user_update,
            'update_date' => $this->update_date,
            'target' => $this->target,
        ]);

        $query->andFilterWhere(['like', 'xsourcex', $this->xsourcex])
            ->andFilterWhere(['like', 'p', $this->p]);

        return $dataProvider;
    }
}
