<?php

namespace backend\modules\level\models;

use Yii;

/**
 * This is the model class for table "tbdata_coc_right".
 *
 * @property string $id
 * @property string $xsourcex
 * @property integer $rstat
 * @property string $user_create
 * @property string $create_date
 * @property string $user_update
 * @property string $update_date
 * @property string $target
 * @property string $p
 */
class LevelManagerRight extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbdata_coc_right';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['xsourcex'], 'required'],
            [['rstat', 'user_create', 'user_update', 'target'], 'integer'],
            [['create_date', 'update_date'], 'safe'],
            [['xsourcex'], 'string', 'max' => 20],
            [['p'], 'string', 'max' => 10]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'xsourcex' => 'Xsourcex',
            'rstat' => 'Rstat',
            'user_create' => 'User Create',
            'create_date' => 'Create Date',
            'user_update' => 'User Update',
            'update_date' => 'Update Date',
            'target' => 'ผู้มีสิทธิ์ใช้งาน',
            'p' => 'สิทธิการเข้าใช้',
        ];
    }
}
