<?php

namespace backend\modules\ezforms2;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\modules\ezforms2\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
