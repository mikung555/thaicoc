<?php

namespace backend\modules\ezforms2\classes\widgets;

use Yii;
use yii\base\Object;
use yii\base\InvalidConfigException;
use appxq\sdii\utils\SDUtility;
use yii\web\View;

/**
 * Select2AjaxCore class file UTF-8
 * @author SDII <iencoded@gmail.com>
 * @copyright Copyright &copy; 2015 AppXQ
 * @license http://www.appxq.com/license/
 * @version 1.0.0 Date: 19 ส.ค. 2559 17:27:52
 * @link http://www.appxq.com/
 * @example backend\modules\ezforms2\classes\widgets\TextInput
 */
class Select2AjaxCore extends Object {

    const BEHAVIOR_CLASS_NAME = '';//TextInputBehavior
    
    /**
     * Initializes this TextInput.
     */
    public function init() {
//	if (self::BEHAVIOR_CLASS_NAME === null || self::BEHAVIOR_CLASS_NAME === '') {
//	    throw new InvalidConfigException('TextInput::BEHAVIOR_CLASS_NAME must be set.');
//	}
    }

    public function generateViewEditor($input, $model) {
	$options = SDUtility::string2Array($input['input_option']);
	$specific = SDUtility::string2Array($input['input_specific']);
	
	if (isset($options['class'])) {
	    $options['class'] .= ' form-control';
	} else {
	    $options['class'] = 'form-control';
	}
	
	$view = new View();
	
	return $view->renderAjax('/widgets/select2ajax/_view_editor', [
	    'model'=>$model,
	    'options'=>$options,
	    'specific'=>$specific,
	    'data'=>$data,
	]);
    }
    
    public function generateViewInput($field) {
	$options = SDUtility::string2Array($field['ezf_field_options']);
	$specific = SDUtility::string2Array($field['ezf_field_specific']);
	
	$view = new View();
	
	if (isset($options['class'])) {
	    $options['class'] .= ' form-control';
	} else {
	    $options['class'] = 'form-control';
	}
	
	return $view->renderAjax('/widgets/select2ajax/_view_item', [
	    'field'=>$field,
	    'options'=>$options,
	    'specific'=>$specific,
	    'data'=>$data,
	]);
    }

    public function generateOptions($input, $model) {
	$view = new View();
	
	return $view->renderAjax('/widgets/select2ajax/_options', [
	    'model'=>$model,
	    'input'=>$input,
	]);
    }
    
    public function generateValidations($input, $model) {
	$view = new View();
	
	return $view->renderAjax('/widgets/_default_validations', [
	    'model'=>$model,
	    'input'=>$input,
	]);
    }
    
    public static function initHospital($model, $modelFields) {
        $code = $model[$modelFields['ezf_field_name']];
        $str = '';
        if(isset($code) && !empty($code)){
            $sql = "SELECT lpad(`code`,'5','0') AS `code`,`name` FROM `const_hospital` WHERE `code`=:code";
            $data = Yii::$app->db->createCommand($sql, [':code'=>$code])->queryOne();
            
            $str = $data['code'].' : '. $data['name'];
        }
        
        return $str;
    }
    
    public static function initSnomed($model, $modelFields) {
        $code = $model[$modelFields['ezf_field_name']];
        $str = '';
        if(isset($code) && !empty($code)){
            $sql = "SELECT lpad(`code`,'5','0') AS `code`,`name` FROM `const_hospital` WHERE `code`=:code";
            $data = Yii::$app->db->createCommand($sql, [':code'=>$code])->queryOne();
            
            $str = $data['code'].' : '. $data['name'];
        }
        
        return $str;
    }
    
    public static function initIcd10($model, $modelFields) {
        $code = $model[$modelFields['ezf_field_name']];
        $str = '';
        if(isset($code) && !empty($code)){
            $sql = "SELECT * FROM `icd10` WHERE `code`=:code";
            $data = Yii::$app->db->createCommand($sql, [':code'=>$code])->queryOne();
            
            $str = $data['code'].' : '. $data['name'];
        }
        
        return $str;
    }
    
    public static function initIcd9($model, $modelFields) {
        $code = $model[$modelFields['ezf_field_name']];
        $str = '';
        if(isset($code) && !empty($code)){
            $sql = "SELECT * FROM `icd9` WHERE `code`=:code";
            $data = Yii::$app->db->createCommand($sql, [':code'=>$code])->queryOne();
            
            $str = $data['code'].' : '. $data['name'];
        }
        
        return $str;
    }
    
}
