<?php

namespace backend\modules\ezforms2\classes\widgets;

use Yii;
use yii\base\Object;
use yii\base\InvalidConfigException;
use appxq\sdii\utils\SDUtility;
use yii\web\View;

/**
 * FileUploadCore class file UTF-8
 * @author SDII <iencoded@gmail.com>
 * @copyright Copyright &copy; 2015 AppXQ
 * @license http://www.appxq.com/license/
 * @version 1.0.0 Date: 19 ส.ค. 2559 17:27:52
 * @link http://www.appxq.com/
 * @example backend\modules\ezforms2\classes\widgets\TextInput
 */
class FileUploadCore extends Object {

    const BEHAVIOR_CLASS_NAME = 'backend\modules\ezforms2\classes\behavior\FileUploadBehavior'; //TextInputBehavior

    /**
     * Initializes this TextInput.
     */

    public function init() {
//	if (self::BEHAVIOR_CLASS_NAME === null || self::BEHAVIOR_CLASS_NAME === '') {
//	    throw new InvalidConfigException('TextInput::BEHAVIOR_CLASS_NAME must be set.');
//	}
    }

    public function generateViewEditor($input, $model) {
        $options = SDUtility::string2Array($input['input_option']);
        $specific = SDUtility::string2Array($input['input_specific']);

        if (isset($options['class'])) {
            $options['class'] .= ' form-control';
        } else {
            $options['class'] = 'form-control';
        }

        $view = new View();

        return $view->renderAjax('/widgets/fileupload/_view_editor', [
                    'model' => $model,
                    'options' => $options,
                    'specific' => $specific,
                    'data' => $data,
        ]);
    }

    public function generateViewInput($field) {
        $options = SDUtility::string2Array($field['ezf_field_options']);
        $specific = SDUtility::string2Array($field['ezf_field_specific']);

        $view = new View();

        if (isset($options['class'])) {
            $options['class'] .= ' form-control';
        } else {
            $options['class'] = 'form-control';
        }

        return $view->renderAjax('/widgets/fileupload/_view_item', [
                    'field' => $field,
                    'options' => $options,
                    'specific' => $specific,
                    'data' => $data,
        ]);
    }

    public function generateOptions($input, $model) {
        $view = new View();

        return $view->renderAjax('/widgets/fileupload/_options', [
                    'model' => $model,
                    'input' => $input,
        ]);
    }

    public function generateValidations($input, $model) {
        $view = new View();

        return $view->renderAjax('/widgets/_default_validations', [
                    'model' => $model,
                    'input' => $input,
        ]);
    }

    public static function initial($model, $modelFields) {
        $fileImg = $model[$modelFields['ezf_field_name']];
        $imgPath = Yii::getAlias('@backendUrl/fileinput') . '/'; //Yii::getAlias('@backendUrl') . '/fileinput/';
        $initialPreview = [];
        if (isset($fileImg) && !empty($fileImg)) {
            $active = false;
            $items = [];
            //file_active = 0 new upload
            //file_active = 1 file confirm
            //file_active = 9 file waiting for approve
            //file_active = -9 file disable (query tools)
            //file_active = -10 file disable (input data)
            $fileItemActive = \backend\modules\ezforms2\models\FileUpload::find()->where('tbid=:tbid and ezf_field_id=:ezf_field_id and ezf_id=:ezf_id and mode = :mode AND file_active =1', [':tbid' => $model->id, ':ezf_id' => $modelFields->ezf_id, ':ezf_field_id' => $modelFields->ezf_field_id, ':mode' => 1])->orderBy('created_at desc')->one();
            if ($fileItemActive) {
                $items[] = $fileItemActive['file_name'];
                $active = true;
            } else {
                $fileItem = \backend\modules\ezforms2\models\FileUpload::find()->where('tbid=:tbid and ezf_field_id=:ezf_field_id and ezf_id=:ezf_id and mode = :mode', [':tbid' => $model->id, ':ezf_id' => $modelFields->ezf_id, ':ezf_field_id' => $modelFields->ezf_field_id, ':mode' => 1])->orderBy('created_at desc')->one();
                if ($fileItem) {
                    $items[] = $fileItem['file_name'];
                }
            }

            foreach ($items as $item) {
                $img = $imgPath . $item;
                $img_old = $img;
                $ext = strtolower(pathinfo($item, PATHINFO_EXTENSION));
                if ($ext == 'pdf') {
                    $img = Yii::getAlias('@storageUrl') . '/source/pdf_icon.png';
                }
                $initialPreview[] = \yii\helpers\Html::img($img, ['data-filename' => $img_old, 'class' => 'file-preview-image', 'alt' => $modelFields->ezf_field_label, 'title' => $modelFields->ezf_field_label]);
            }
        }

        return $initialPreview;
    }

}
