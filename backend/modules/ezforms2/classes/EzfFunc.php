<?php

namespace backend\modules\ezforms2\classes;

use Yii;
use appxq\sdii\models\SDDynamicModel;
use backend\modules\ezforms2\classes\EzfQuery;
use appxq\sdii\utils\SDUtility;
use yii\helpers\ArrayHelper;
use yii\web\View;
use backend\modules\ezforms2\models\EzformCondition;
use yii\helpers\Html;
use backend\modules\ezforms2\models\EzformChoice;
use yii\helpers\Json;
use appxq\sdii\helpers\SDHtml;
use backend\modules\ezforms\models\EzformFields;

/**
 * OvccaFunc class file UTF-8
 * @author SDII <iencoded@gmail.com>
 * @copyright Copyright &copy; 2015 AppXQ
 * @license http://www.appxq.com/license/
 * @version 1.0.0 Date: 9 ก.พ. 2559 12:38:14
 * @link http://www.appxq.com/
 * @example 
 */
class EzfFunc {

    public static function setDynamicModel($fields, $table, $annotated=0) {
	$attributes = ['ptid', 'xsourcex', 'xdepartmentx', 'rstat', 'sitecode', 'ptcode', 'ptcodefull', 'hptcode', 'hsitecode', 'user_create', 'create_date', 'user_update', 'update_date', 'target', 'error', 'sys_lat', 'sys_lng'];
	$labels = [];
	$required = [];
	$rules = [];
	//$rulesFields = [];
        $rulesFields['safe'] = ['ptid', 'xsourcex', 'xdepartmentx', 'rstat', 'sitecode', 'ptcode', 'ptcodefull', 'hptcode', 'hsitecode', 'user_create', 'create_date', 'user_update', 'update_date', 'target', 'error', 'sys_lat', 'sys_lng'];
	$condFields = [];
        $behavior = [];
        
	if (!empty($fields)) {
	    foreach ($fields as $value) {
		//Attributes array
		$attributes[$value['ezf_field_name']] = $value['ezf_field_default'];
		//Labels array
		$labels[$value['ezf_field_name']] = isset($value['ezf_field_label']) ? $value['ezf_field_label'] : '';
                if($annotated==1 && $value['table_field_type']!='none' && $value['table_field_type']!='field'){
                    $labels[$value['ezf_field_name']] .= " <code>{$value['ezf_field_name']}</code>";
                }
		//Rule array required
		if ($value['ezf_field_required'] == 1) {
		    $required[] = $value['ezf_field_name'];
		}

		//Rule array validate
		$validateArray = SDUtility::string2Array($value['ezf_field_validate']);
		if (is_array($validateArray)) {
		    $addRule = false;
		    foreach ($validateArray as $keyRule => $valueRule) {
			if (is_array($valueRule)) {
			    $name = self::getRuleName($valueRule);
			    $rulesFields[$name][] = $value['ezf_field_name'];
			    $rules[$name] = $valueRule;
			} else {
			    $addRule = true;
			    break;
			}
		    }

		    if ($addRule) {
			$name = self::getRuleName($validateArray);
			$rulesFields[$name][] = $value['ezf_field_name'];
			$rules[$name] = $validateArray;
		    }
		} 
                
                $rulesFields['safe'][] = $value['ezf_field_name'];
		$rules['safe'] = ['safe'];

                if($value['ezf_condition']==1){
                    $condFields[] = self::getCondition($value['ezf_id'], $value['ezf_field_name']);
                }
                
                if(isset(Yii::$app->session['ezf_input'])){
                    try {
                        $dataInput = EzfFunc::getInputByArray($value->ezf_field_type, Yii::$app->session['ezf_input']);
                        if($dataInput){
                            $inputWidget = Yii::createObject($dataInput['system_class']);
                            if($inputWidget::BEHAVIOR_CLASS_NAME!=''){
                                $behavior[$inputWidget::BEHAVIOR_CLASS_NAME.'_'.$value->ezf_field_name] = [
                                    'class' => $inputWidget::BEHAVIOR_CLASS_NAME,
                                    'ezf_field' => $value->attributes,
                                    'ezf_table' => $table,
                                ];
                            }
                        }
                    } catch (\ReflectionException $e) {

                    }
                }
	    }
	}

        $model = new SDDynamicModel($attributes);
	foreach ($rules as $key => $value) {
	    $options = isset($value[1]) ? $value[1] : [];
	    $model->addRule($rulesFields[$key], $value[0], $options);
	}
        
	$js = '';
	foreach ($condFields as $key => $value) {
	    if (!empty($value)) {
		foreach ($required as $i => $v) {
		    foreach ($value as $k => $data) {
			if ((!empty($data['var_require']) && in_array($v, $data['var_require']))) {//|| (!empty($data['var_jump']) && in_array($v, $data['var_jump']))
			    $js .= "if(attribute.name == '$v') {
				    var r = $('#sddynamicmodel-{$data['ezf_field_name']}:checked').val()=='{$data['ezf_field_value']}';
				    //console.log(r);	
				    return r;	
			    }";
			}
		    }
		}
	    }
	}
//		VarDumper::dump($js, 10, true);
//		exit();

	$model->addRule($required, 'required', ['whenClient' => "function (attribute, value) {
			$js
		}"]);
        
	$model->addLabel($labels);
        
        if(!empty($behavior)){
            foreach ($behavior as $keyBehavior => $valueBehavior) {
                $model->attachBehavior($keyBehavior, $valueBehavior);
            }
        }
        
	return $model;
    }
    
    public static function getBehavior($fields, $table) {
	
        $behavior = [];
        
	if (!empty($fields)) {
	    foreach ($fields as $value) {
		
                if(isset(Yii::$app->session['ezf_input'])){
                    try {
                        $dataInput = EzfFunc::getInputByArray($value->ezf_field_type, Yii::$app->session['ezf_input']);
                        if($dataInput){
                            $inputWidget = Yii::createObject($dataInput['system_class']);
                            if($inputWidget::BEHAVIOR_CLASS_NAME!=''){
                                $behavior[$inputWidget::BEHAVIOR_CLASS_NAME.'_'.$value->ezf_field_name] = [
                                    'class' => $inputWidget::BEHAVIOR_CLASS_NAME,
                                    'ezf_field' => $value->attributes,
                                    'ezf_table' => $table,
                                ];
                            }
                        }
                    } catch (\ReflectionException $e) {

                    }
                }
	    }
	}
        
	return $behavior;
    }

    public static function generateInput($form, $model, $modelFields, $widgets = '/widgets/_view_item') {
	$html = '';
        $view = new View();
        try {
            if ($modelFields['table_field_type'] != 'none' && $modelFields['table_field_type'] != '') {
                $dataInput;

                if (isset(Yii::$app->session['ezf_input'])) {
                    $dataInput = EzfFunc::getInputByArray($modelFields['ezf_field_type'], Yii::$app->session['ezf_input']);
                }

                if ($dataInput) {
                    $specific = SDUtility::string2Array($modelFields['ezf_field_specific']);
                    $options = SDUtility::string2ArrayJs($modelFields['ezf_field_options']);
                    unset($options['specific']);

                    $data = SDUtility::string2Array($modelFields['ezf_field_data']);
                    $label = '';
                    if(isset($modelFields['ezf_field_label']) && $modelFields['ezf_field_label']==''){
                        $label = "->label('')";
                    }
                    //inline, label fix
                    if ($dataInput['input_function'] == 'widget') {
                        if (!empty($data)) {
                            if(isset($data['items'])) {
                                $options['data'] = $data['items'];
                            }
                            
                            if(isset($data['func'])){
                                try {
                                    eval("\$dataItems = {$data['func']};");
                                } catch (\yii\base\Exception $e) {
                                    $dataItems = [];
                                }
                                $options['data'] = $dataItems;
                            }
                            
                            if(isset($data['fields'])) {
                                $options['fields'] = $data['fields'];
                                
                            }
                        }
                        
                        $widget_render = '';
                        if(isset($model[$modelFields['ezf_field_name']]) && !empty($model[$modelFields['ezf_field_name']])){
                            if(isset($options['options']['data-func-set']) && !empty($options['options']['data-func-set'])){
                                $pathStr = [
                                    '{model}'=>  "\$model",
                                    '{modelFields}'=>  "\$modelFields",
                                ];
                                
                                $funcSet = strtr($options['options']['data-func-set'], $pathStr);
                                
                                try {
                                    $initial = eval("return $funcSet;");
                                } catch (\yii\base\Exception $e) {
                                    $initial = FALSE;
                                }
                                
                                if($initial){
                                    if(isset($options['options']['data-name-in'])){
                                        $data_in = $options['options']['data-name-in'];
                                        $data_set = self::addProperty($data_in, $options['options']['data-name-set'], $initial);
                                        $options = ArrayHelper::merge($options, $data_set);
                                    } else {
                                        $options[$options['options']['data-name-set']] = $initial;
                                    }
                                }
                            }
                            
                            if(isset($options['options']['data-data-widget']) && !empty($options['options']['data-data-widget'])){
                                $widget_render = $view->renderAjax($options['options']['data-data-widget'], [
                                    'model' => $model,
                                    'modelFields' => $modelFields,
                                ]);
                            }
                        }
                        
                        $attribute = "\$modelFields['ezf_field_name']";
                        if(isset($options['options']['multiple']) && $options['options']['multiple']==true){
                            $attribute = "\$modelFields['ezf_field_name'].'[]'";
                        }
                        
                        eval("\$html = \$form->field(\$model, $attribute, \$specific)->hint(\$modelFields['ezf_field_hint'])->{$dataInput['input_function']}({$dataInput['input_class']}, \$options)$label;");
                        $html .= $widget_render;
                    } else {
                        if (empty($data)) {

                            eval("\$html = \$form->field(\$model, \$modelFields['ezf_field_name'], \$specific)->hint(\$modelFields['ezf_field_hint'])->{$dataInput['input_function']}(\$options)$label;");
                        } else {
                            if(isset($data['func'])){
                                eval("\$dataItems = {$data['func']};");
                            } else {
                                $dataItems = $data['items'];
                            }
                            eval("\$html = \$form->field(\$model, \$modelFields['ezf_field_name'], \$specific)->hint(\$modelFields['ezf_field_hint'])->{$dataInput['input_function']}(\$dataItems, \$options)$label;");
                        }
                    }
                } else {
                    $html = Html::activeHiddenInput($model, $modelFields['ezf_field_name']);
                }
            } else {
                if (isset(Yii::$app->session['ezf_input'])) {
                    $dataInput = EzfFunc::getInputByArray($modelFields['ezf_field_type'], Yii::$app->session['ezf_input']);
                }

                if ($dataInput) {
                    $class = str_replace('::className()', '', $dataInput['input_class']);
                    $options = SDUtility::string2Array($modelFields['ezf_field_options']);
                    $options['name'] = $modelFields['ezf_field_name'];
                    $options['value'] = $modelFields['ezf_field_label'];

                    if($modelFields['ezf_field_type']=='57'){
                        $html = '';
                    } else {
                        eval("\$html = {$class}::widget(\$options);");
                        if(isset($modelFields['ezf_field_hint'])){
                            $html .= $modelFields['ezf_field_hint'];
                        }
                    }
                }

            }

            $style_color = '';
            if ($modelFields['ezf_field_color'] != '') {
                $style_color = "background-color: {$modelFields['ezf_field_color']};";
            }

            

            return $view->renderAjax($widgets, [
                'field_id' => $modelFields['ezf_field_id'],
                'field_size' => $modelFields['ezf_field_lenght'],
                'style_color' => $style_color,
                'field_item' => $html,
                'hide'=>$modelFields['ezf_field_type']==0?'display: none;':'',
            ]);
        
        } catch (yii\base\Exception $e) {
            return '<code>'.$e->getMessage().'</code>';
        }   
    }
    
    public static function addProperty($obj, $key, $data) {
        foreach ($obj as $i => $value) {
            if(is_array($value)){
                $obj[$i] = self::addProperty($value, $key, $data);
            } else {
                if($i==$key){
                    $obj[$key] = $data;
                }
            }
        }
        return $obj;
    }

    private static function getRuleName($rule) {
	$name = $rule[0];
	if (count($rule) > 1) {
	    $name = '';
	    foreach ($rule as $key => $value) {
		if (is_integer($key)) {
		    $name .= $value;
		} else {
		    $name .= $key . $value;
		}
	    }
	}
	return $name;
    }

    public static function getCondition($ezf_id, $ezf_field_name) {
	$model = EzformCondition::find()
		->where('ezf_id=:ezf_id AND ezf_field_name=:ezf_field_name', [':ezf_id' => $ezf_id, ':ezf_field_name' => $ezf_field_name])
		->all();

	$dataEzf = [];
	if ($model) {
	    $k = 0;
	    foreach ($model as $key => $value) {
		$arr_cond_jump = json_decode($value['cond_jump']);
		$arr_cond_require = json_decode($value['cond_require']);

		if (is_array($arr_cond_jump)) {
		    $cond_jump = implode(',', $arr_cond_jump);
		    $var_jump = ArrayHelper::getColumn(EzfQuery::getConditionFieldsName('ezf_field_name', $cond_jump), 'ezf_field_name');
		}

		if (is_array($arr_cond_require)) {
		    $cond_require = implode(',', $arr_cond_require);
		    $var_require = ArrayHelper::getColumn(EzfQuery::getConditionFieldsName('ezf_field_name', $cond_require), 'ezf_field_name');
		}

		$dataEzf[$k]['ezf_id'] = $ezf_id;
		$dataEzf[$k]['ezf_field_name'] = $value['ezf_field_name'];
		$dataEzf[$k]['ezf_field_value'] = $value['ezf_field_value'];
		$dataEzf[$k]['var_jump'] = isset($var_jump) ? $var_jump : '';
		$dataEzf[$k]['var_require'] = isset($var_require) ? $var_require : '';
		$k++;
	    }
	}

	return $dataEzf;
    }

    public static function generateFieldName($ezf_id) {
	return 'var_' . EzfQuery::getFieldsCountById($ezf_id);
    }

    public static function getInputByArray($id, $input) {
	foreach ($input as $key => $value) {
	    if ($value['input_id'] == $id) {
		return $value;
	    }
	}
	return FALSE;
    }

    public static function setEzfData($dataItems, $ezf_field_id, $ezf_id, $oldName , $newName, $dataType) {
        $dataEzf = Yii::$app->session['ezform'];
        
        if(isset($dataItems['condition'])){
            //\appxq\sdii\utils\VarDumper::dump($dataItems,1,0);
            self::saveCondition($dataItems['condition']);
	    unset($dataItems['condition']);
        }
            
	if(isset($dataItems['builder']) && !empty($dataItems['builder'])){
            
	    foreach ($dataItems['builder'] as $key => $value) {
                
		//set items
                
		if($dataType == 'radio'){
		    $dataItems['items']['data'][$value['value']] = $value['label'];
		    if(isset($value['other'])){
			$dataItems['items']['other'][$value['value']] = $value['other'];
		    }
		} elseif($dataType == 'fields'){
                    
                    if(isset($value['fields'])){
                        $last_xy;
                        
                        foreach ($value['fields'] as $xy => $obj){
                            $dataItems['fields'][$xy] = $obj;
                            $last_xy = $xy;
                        }
                        if(isset($value['other'])){
                            $dataItems['fields'][$last_xy]['other'] = $value['other'];
                        }
                    }
                } elseif($dataType == 'select') {
		    $dataItems['items'][$value['value']] = $value['label'];
		}
		
		//save items
                if($dataType == 'fields'){
                    if(is_array($value['fields'])){
                        foreach ($value['fields'] as $xy => $obj) {
                            if($obj['action']=='update'){
                                $updateField = EzfQuery::updateFields($obj,$dataEzf);
                                if(!$updateField){
                                    $result = [
                                        'status' => 'error',
                                        'message' => SDHtml::getMsgError() . Yii::t('app', 'แก้ไขคอลัมน์ไม่สำเสร็จ "'.$obj['attribute'].'"'),
                                    ];
                                    return $result;
                                }
                            } else {
                                $createField = EzfQuery::addFields($obj, $ezf_id, $ezf_field_id, $dataEzf);
                                if(!$createField){
                                    $result = [
                                        'status' => 'error',
                                        'message' => SDHtml::getMsgError() . Yii::t('app', 'สร้างคอลัมน์ไม่สำเสร็จ "'.$obj['attribute'].'"'),
                                    ];
                                    return $result;
                                }
                            }
                            
                            $dataItems['builder'][$key]['fields'][$xy]['action'] = 'update';
                            
                            if(isset($obj['data']) && is_array($obj['data'])){
                                foreach ($obj['data'] as $key_item => $value_item) {
                                    if($value_item['action']=='update'){
                                        $model_item = EzformChoice::find()->where('ezf_choice_id=:id', [':id'=>$key_item])->one();
                                        $model_item->ezf_choicevalue = $value_item['value'];
                                        $model_item->ezf_choicelabel = $value_item['label'];
                                        $model_item->ezf_choiceetc = null;
                                        $model_item->save();
                                    } else {
                                        $model_item = new EzformChoice();
                                        $model_item->ezf_choice_id = $key_item;
                                        $model_item->ezf_field_id = $ezf_field_id;
                                        $model_item->ezf_choicevalue = $value_item['value'];
                                        $model_item->ezf_choicelabel = $value_item['label'];
                                        $model_item->ezf_choiceetc = null;
                                        $model_item->ezf_choice_col = 12;
                                        $model_item->save();
                                    }

                                    $dataItems['builder'][$key]['fields'][$xy]['data'][$key_item]['action'] = 'update';
                                }
                            }
                        }
                    }
                    
                    
                } elseif(in_array ($dataType, ['select', 'radio'])) {
		    if($value['action']=='update'){
			$model = EzformChoice::find()->where('ezf_choice_id=:id', [':id'=>$key])->one();
			$model->ezf_choicevalue = $value['value'];
			$model->ezf_choicelabel = $value['label'];
                        $model->ezf_choiceetc = isset($value['other']['id'])?$value['other']['id']:null;
                        $model->save();
		    } else {
			$model = new EzformChoice();
			$model->ezf_choice_id = $key;
			$model->ezf_field_id = $ezf_field_id;
			$model->ezf_choicevalue = $value['value'];
			$model->ezf_choicelabel = $value['label'];
			$model->ezf_choiceetc = isset($value['other']['id'])?$value['other']['id']:null;
			$model->ezf_choice_col = 12;
			$model->save();
		    }
		}
                
                if(isset($value['other'])){
                    if($value['other']['action']=='update'){
                        $updateField = EzfQuery::updateFields($value['other'], $dataEzf);
                        if(!$updateField){
                            $result = [
                                'status' => 'error',
                                'message' => SDHtml::getMsgError() . Yii::t('app', 'แก้ไขคอลัมน์ไม่สำเสร็จ "'.$value['other']['attribute'].'"'),
                            ];
                            return $result;
                        }
                    } else {
                        $createField = EzfQuery::addFields($value['other'], $ezf_id, $ezf_field_id, $dataEzf);
                        if(!$createField){
                            $result = [
                                'status' => 'error',
                                'message' => SDHtml::getMsgError() . Yii::t('app', 'สร้างคอลัมน์ไม่สำเสร็จ "'.$value['other']['attribute'].'"'),
                            ];
                            return $result;
                        }
                    }
                }
		
		//commit
                if(isset($dataItems['builder'][$key]['other'])){
                    $dataItems['builder'][$key]['other']['action'] = 'update';
                }
		$dataItems['builder'][$key]['action'] = 'update';
	    }
	    
	    if($oldName!=$newName){
		EzfQuery::deleteCondition($ezf_id, $oldName);
	    }
	    
	    unset($dataItems['func']);
	}
        
        //delete items
        if(isset($dataItems['delete_fields']) && !empty($dataItems['delete_fields'])){
	    foreach ($dataItems['delete_fields'] as $id=>$varname) {
		$model = EzformFields::find()->where('ezf_field_id=:ezf_field_id', [':ezf_field_id' => $id])->one();
                if($model){
                    $alterTable = self::alterDropColumn($dataEzf['ezf_table'], $varname);
                    if(!$alterTable){
                        $result = [
                            'status' => 'error',
                            'message' => SDHtml::getMsgError() . Yii::t('app', 'ลบคอลัมน์ไม่สำเสร็จ "'.$varname.'"'),
                            'data' => $model,
                        ];
                        return $result;
                    }
                    $model->delete();
                } else {
                    $result = [
                        'status' => 'error',
                        'message' => SDHtml::getMsgError() . Yii::t('app', 'ลบคอลัมน์ไม่ได้เนื่องจากไม่พบข้อมูล "'.$varname.'"'),
                        'data' => $model,
                    ];
                    return $result;
                }
	    }
	}
	unset($dataItems['delete_fields']);
        
        
        if(isset($dataItems['delete']) && !empty($dataItems['delete'])){
	    foreach ($dataItems['delete'] as $id) {
                $model = EzformChoice::find()->where('ezf_choice_id=:id', [':id'=>$id])->one();
                $model->delete();
	    }
	}
	unset($dataItems['delete']);
        
	
	return SDUtility::array2String($dataItems);
    }
    
    public static function setEzfFields($dataInput, $dataItems, $unset = true) {
	$tmp_data = SDUtility::string2Array($dataInput);
	$data = isset($dataItems) ? $dataItems : [];
	if ($unset) {
	    foreach ($data as $keyRow => $valueRow) {
		if ($valueRow == '') {
		    unset($data[$keyRow]);
		} elseif (is_array($valueRow)) {
		    foreach ($valueRow as $key => $value) {
			if ($value == '') {
			    unset($data[$keyRow][$key]);
			}
		    }
		}
	    }
	}
	$data = ArrayHelper::merge($tmp_data, $data);
	return SDUtility::array2String($data);
    }

    public static function setSpecific($dataInput, $options) {
	$dataItems = isset($options['specific']) ? $options['specific'] : [];
	$tmp_specific = SDUtility::string2Array($dataInput['input_specific']);
	$specific = [];
	$sColor = '';
        
	if (isset($dataItems['color']) && !empty($dataItems['color'])) {
	    $sColor = 'color:' . $dataItems['color'];
	    $specific['labelOptions'] = ['style' => $sColor];
	}
        
	if (isset($dataItems['icon']) && !empty($dataItems['icon'])) {
	    $specific['template'] = "<i class='fa {$dataItems['icon']}' style='$sColor'></i> {label}\n{input}\n{hint}\n{error}";
	}
        
	$prefix = '';
	if (isset($dataItems['prefix']) && !empty($dataItems['prefix'])) {
	    $prefix = '<span class="input-group-addon">' . $dataItems['prefix'] . '</span>';
	}
        
	$suffix = '';
	if (isset($dataItems['suffix']) && !empty($dataItems['suffix'])) {
	    $suffix = '<span class="input-group-addon">' . $dataItems['suffix'] . '</span>';
	}
        
        unset($dataItems['suffix']);
        unset($dataItems['prefix']);
        unset($dataItems['icon']);
        unset($dataItems['color']);

	if ($prefix != '' || $suffix != '') {
	    $specific['inputTemplate'] = '<div class="input-group">' . $prefix . '{input}' . $suffix . '</div>';
	}
        
        $specific = ArrayHelper::merge($specific, $dataItems);
	$specific = ArrayHelper::merge($tmp_specific, $specific);

	return SDUtility::array2String($specific);
    }

    public static function createChildrenItem($field, $htmlInput) {
	$style_color = '';
	if ($field['ezf_field_color'] != '') {
	    $style_color = "background-color: {$field['ezf_field_color']};";
	}
	$view = new View();

	return $view->renderAjax('/widgets/_dads_children', [
		    'field_id' => $field['ezf_field_id'],
		    'field_size' => $field['ezf_field_lenght'],
		    'field_order' => $field['ezf_field_order'],
		    'style_color' => $style_color,
		    'field_item' => $htmlInput,
	]);
    }

    public static function mergeValidate($validate) {
	$addArry = [];
	foreach ($validate as $row => $items) {
	    foreach ($items as $key => $value) {
		$addArry[$items[0]][$key] = $value;
	    }
	}
	$returnArry = [];
	foreach ($addArry as $key => $value) {
	    $returnArry[] = $value;
	}

	return $returnArry;
    }

    //$index = INDEX, PRIMARY, UNIQUE, FULLTEXT
    //$length 0 = not set
    public static function alterTableAdd($table, $column, $type, $length = 0) {
	$strLen = '';
	$strIndex = '';

	if ($length > 0) {
	    $strLen = "($length)";
	}

	$type = "$type $strLen NULL DEFAULT NULL";

	try {
	    Yii::$app->db->createCommand()->addColumn($table, $column, $type)->execute();
	    return true;
	} catch (\yii\db\Exception $e) {
	    return false;
	}
    }

    public static function alterTableChange($table, $column, $newColumn, $type, $length = 0) {
	$strLen = '';
	$strIndex = '';

	if ($length > 0) {
	    $strLen = "($length)";
	}

	try {
	    $sql = "ALTER TABLE `$table` CHANGE `$column` `$newColumn` $type $strLen NULL DEFAULT NULL";
	    
	    Yii::$app->db->createCommand($sql)->execute();
	    return true;
	} catch (\yii\db\Exception $e) {
	    return false;
	}
    }

    public static function alterDropColumn($table, $column) {
	try {
	   Yii::$app->db->createCommand()->dropColumn($table, $column)->execute();
	    return true;
	} catch (\yii\db\Exception $e) {
	    return false;
	}
    }
    
    public static function saveCondition($data) {
	$condArr = [];
	if (isset($data)) {
	    $condArr = Json::decode($data);

	    foreach ($condArr as $key => $value) {
		$model = EzformCondition::find()
				->where('ezf_id=:ezf_id AND ezf_field_name=:ezf_field_name AND ezf_field_value=:ezf_field_value', [':ezf_id' => $value['ezf_id'], ':ezf_field_name' => $value['ezf_field_name'], ':ezf_field_value' => $value['ezf_field_value']])
				->one();

		if ($model) {
			$model->cond_jump = Json::encode($value['cond_jump']);
			$model->cond_require = Json::encode($value['cond_require']);
		} else {
			$model = new EzformCondition();
			$model->ezf_id = (int)$value['ezf_id'];
			$model->ezf_field_name = (string)$value['ezf_field_name'];
			$model->ezf_field_value = (string)$value['ezf_field_value'];
			$model->cond_jump = ($value['cond_jump']!='')?Json::encode($value['cond_jump']):'';
			$model->cond_require = ($value['cond_require']!='')?Json::encode($value['cond_require']):'';
		}

		if ($model->isNewRecord) {
			if ($value['cond_jump'] != '' || $value['cond_require'] != '') {
				$action = $model->save();
			}
		} else {
			$action = $model->save();
		}
	    }
	}

    //unset($_COOKIE['gen_condition']);
    }
    
    public static function generateCondition($modelTable, $field, $model, $view) {
	//$view = new View();
	$inputId = Html::getInputId($modelTable, $field['ezf_field_name']);
	$inputValue = Html::getAttributeValue($modelTable, $field['ezf_field_name']);

	$dataCond = EzfQuery::getCondition($field['ezf_id'], $field['ezf_field_name']);
	if ($dataCond) {
	    //Edit Html
	    $condition = SDUtility::string2Array($field['ezf_field_options']);
	    
	    $fieldId = $inputId;
	    $dataType = 'none';
            
            $dataInput;
	    if(isset(Yii::$app->session['ezf_input'])){
		$dataInput = EzfFunc::getInputByArray($field['ezf_field_type'], Yii::$app->session['ezf_input']);
	    }

            if ($dataInput) {
                if($dataInput['input_function']=='widget'){
                    if (isset($condition['options']['data-type'])) {
                        $dataType = $condition['options']['data-type'];
                    }
                } else {
                    
                    if (isset($condition['data-type'])) {
                        $dataType = $condition['data-type'];
                    }
                }
            }
            
            if($dataType == 'select' || $dataType == 'radio'){
                $fieldId = $field['ezf_field_name'];
            }
            
	    
            
	    $enable = TRUE;
	    foreach ($dataCond as $index => $cvalue) {
		//if($inputValue == $cvalue['ezf_field_value'] || $inputValue == ''){
		$dataCond[$index]['cond_jump'] = json_decode($cvalue['cond_jump']);
		$dataCond[$index]['cond_require'] = json_decode($cvalue['cond_require']);


		if ($dataType == 'select' || $dataType == 'radio') {
		    if ($inputValue == $cvalue['ezf_field_value'] || $inputValue == '') {
			if ($enable) {
			    $enable = false;
			    $jumpArr = json_decode($cvalue['cond_jump']);
			    if (is_array($jumpArr)) {
				foreach ($jumpArr as $j => $jvalue) {
				    $view->registerJs("
					    var fieldIdj = '" . $jvalue . "';
					    var inputIdj = '" . $fieldId . "';
					    var valueIdj = '" . $inputValue . "';
					    var fixValuej = '" . $cvalue['ezf_field_value'] . "';
					    var fTypej = '" . $dataType . "';
					    domHtml(fieldIdj, inputIdj, valueIdj, fixValuej, fTypej, 'none');
				    ");
				}
			    }

			    $requireArr = json_decode($cvalue['cond_require']);
			    if (is_array($requireArr)) {
				foreach ($requireArr as $r => $rvalue) {
				    $view->registerJs("
					    var fieldIdr = '" . $rvalue . "';
					    var inputIdr = '" . $fieldId . "';
					    var valueIdr = '" . $inputValue . "';
					    var fixValuer = '" . $cvalue['ezf_field_value'] . "';
					    var fTyper = '" . $dataType . "';
					    domHtml(fieldIdr, inputIdr, valueIdr, fixValuer, fTyper, 'block');
				    ");
				}
			    }
			}
		    }
		} else {

		    $jumpArr = json_decode($cvalue['cond_jump']);
		    if (is_array($jumpArr)) {
			foreach ($jumpArr as $j => $jvalue) {
			    $view->registerJs("
				    var fieldIdj = '" . $jvalue . "';
				    var inputIdj = '" . $fieldId . "';
				    var valueIdj = '" . $inputValue . "';
				    var fixValuej = '" . $cvalue['ezf_field_value'] . "';
				    var fTypej = '" . $dataType . "';
				    domHtml(fieldIdj, inputIdj, valueIdj, fixValuej, fTypej, 'block');
			    ");
			}
		    }

		    $requireArr = json_decode($cvalue['cond_require']);
		    if (is_array($requireArr)) {

			foreach ($requireArr as $r => $rvalue) {

			    $view->registerJs("
				    var fieldIdr = '" . $rvalue . "';
				    var inputIdr = '" . $fieldId . "';
				    var valueIdr = '" . $inputValue . "';
				    var fixValuer = '" . $cvalue['ezf_field_value'] . "';
				    var fTyper = '" . $dataType . "';
				    domHtml(fieldIdr, inputIdr, valueIdr, fixValuer, fTyper, 'none');

			    ");
			}
		    }
		}
	    }
	    
	    //Add Event
	    if ($dataType == 'checkbox') {
		$view->registerJs("
			var dataCond = '" . yii\helpers\Json::encode($dataCond) . "';
			var inputId = '" . $inputId . "';
			eventCheckBox(inputId, dataCond);
			setCheckBox(inputId, dataCond);
		    ");
	    } else if ($dataType == 'select') {
                
		$view->registerJs("
			var dataCond = '" . yii\helpers\Json::encode($dataCond) . "';
			var inputId = '" . $inputId . "';
			eventSelect(inputId, dataCond);
			setSelect(inputId, dataCond);
		    ");
	    } else if ($dataType == 'radio') {
		$view->registerJs("
			var dataCond = '" . yii\helpers\Json::encode($dataCond) . "';
			var inputName = '" . $field['ezf_field_name'] . "';
			eventRadio(inputName, dataCond);
			setRadio(inputName, dataCond);
		    ");
	    }
	}
    }
    
    public static function itemAlias($code, $key = NULL) {
	$itemStr['reportItems'] = [
	    'bar_chart' => 'แผนภูมิแท่ง',
	    'pie' => 'แผนภูมิวงกลม',
	    'line_graph' => 'กราฟเส้น',
	];
		
	$return = $itemStr[$code];

	if (isset($key)) {
	    return isset($return[$key]) ? $return[$key] : false;
	} else {
	    return isset($return) ? $return : false;
	}
    }
    
    public static function systemCreateField($column) {
	try {
	    Yii::$app->db->createCommand()->insert('ezform_fields', $column)->execute();
	    return true;
	} catch (\yii\db\Exception $e) {
	    return false;
	}
    }
    
    public static function systemUpdateField($ezf_field_id, $column) {
	try {
	    Yii::$app->db->createCommand()->update('ezform_fields', $column, 'ezf_field_id=:ezf_field_id', [':ezf_field_id'=>$ezf_field_id])->execute();
	    return true;
	} catch (\yii\db\Exception $e) {
	    return false;
	}
    }
    
    public static function systemDeleteField($ezf_field_id) {
	try {
	    Yii::$app->db->createCommand()->delete('ezform_fields', 'ezf_field_id=:ezf_field_id', [':ezf_field_id'=>$ezf_field_id])->execute();
	    return true;
	} catch (\yii\db\Exception $e) {
	    return false;
	}
    }
    
    public static function resetFieldData($ezf_field_id, $table) {
	try {
            EzfQuery::deleteChoice($ezf_field_id);
            
            $model = EzformFields::find()->where('ezf_field_ref=:ezf_field_ref', [':ezf_field_ref' => $ezf_field_id])->all();
            if($model){
                foreach ($model as $key => $value) {
                    EzfFunc::alterDropColumn($table, $value->ezf_field_name);
                }
                EzfQuery::deleteFieldOther($ezf_field_id);
            }
            
	} catch (\yii\db\Exception $e) {
	    return false;
	}
    }
    
    public static function uploadOption($model, $pathFrom, $pathTo) {
        
        if (stristr($model['ezf_field_default'], 'tmp.png') == TRUE && $pathFrom!='' && $pathTo!='') {

            $fileName = $model['ezf_field_default'];
            $newFileName = $fileName;
            $nameEdit = false;
            
            if (stristr($fileName, 'tmp.png') == TRUE) {
                $nameEdit = true;
                $newFileName = \common\lib\codeerror\helpers\GenMillisecTime::getMillisecTime() . '.png';
                @copy(Yii::$app->basePath . $pathFrom . $fileName, Yii::$app->basePath . $pathTo . $newFileName);
                @unlink(Yii::$app->basePath . $pathFrom . $fileName);
            }
            
            $model['ezf_field_default'] = $newFileName;
            $modelTmp = \backend\modules\ezforms2\models\EzformFields::find()->where(['ezf_field_id' => $model['ezf_field_id']])->one();

            if (isset($modelTmp['ezf_field_id'])) {
                $fileName = $modelTmp['ezf_field_default'];
                if ($nameEdit && $fileName != '') {
                    @unlink(Yii::$app->basePath . $pathTo . $fileName);
                }
            }
            return $newFileName;
        }

        return $model['ezf_field_default'];
    }

}
