<?php
namespace backend\modules\ezforms2\classes;

use Yii;

/**
 * OvccaQuery class file UTF-8
 * @author SDII <iencoded@gmail.com>
 * @copyright Copyright &copy; 2015 AppXQ
 * @license http://www.appxq.com/license/
 * @version 1.0.0 Date: 9 ก.พ. 2559 12:38:14
 * @link http://www.appxq.com/
 * @example 
 */
class EzfQuery {
    public static function getIntUserAll() {
	$sql = "SELECT user_id as id, 
		    CONCAT(firstname, ' ', lastname) AS text
		FROM user_profile
		";
	
	return Yii::$app->db->createCommand($sql)->queryAll();
    }
    
    public static function getInputv2All() {
	$sql = "SELECT *
		FROM ezform_input
		WHERE input_version='v2'
		ORDER BY input_order
		";
	
	return Yii::$app->db->createCommand($sql)->queryAll();
    }
    
    public static function getFieldsCountById($id) {
	$sql = "SELECT count(*) AS num
		FROM ezform_fields
		WHERE ezf_id=:id AND ezf_field_ref_field IS NULL AND ezf_field_type<>0
		";
	
	return Yii::$app->db->createCommand($sql, [':id'=>$id])->queryScalar();
    }
    
    public static function getEzformById($id) {
	
	$sql = "SELECT ezf_id, ezf_name, ezf_table, comp_id_target, field_detail, unique_record FROM ezform WHERE ezf_id = :id";
	
	return Yii::$app->db->createCommand($sql, [':id'=>$id])->queryOne();
    }
    
    public static function getConditionFieldsName($field, $cond) {
	$sql = "SELECT $field
		FROM ezform_fields 
		WHERE ezform_fields.ezf_field_id in($cond) ";
	
	if($cond!=''){
	    return Yii::$app->db->createCommand($sql)->queryAll();
	}
	return '';
    }
    
    public static function deleteFieldOther($ezf_field_id) {
	$sql = "DELETE FROM `ezform_fields` WHERE `ezf_field_ref` = :id ";
	
	return Yii::$app->db->createCommand($sql, [':id'=>$ezf_field_id])->execute();
    }
    
    public static function deleteChoice($ezf_field_id) {
	$sql = "DELETE FROM `ezform_choice` WHERE `ezf_field_id` = :id ";
	
	return Yii::$app->db->createCommand($sql, [':id'=>$ezf_field_id])->execute();
    }
    
    public static function deleteCondition($ezf_id, $ezf_field_name) {
	$sql = "DELETE FROM `ezform_condition` WHERE `ezf_id` = :id AND `ezf_field_name` = :name ";
	
	return Yii::$app->db->createCommand($sql, [':id'=>$ezf_id, ':name'=>$ezf_field_name])->execute();
    }
    
    public static function getCondition($ezf_id, $ezf_field_name) {
	$sql = "SELECT *
		FROM ezform_condition
		WHERE ezform_condition.ezf_id = :ezf_id AND ezform_condition.ezf_field_name = :ezf_field_name
		ORDER BY ezform_condition.cond_id;";

	return Yii::$app->db->createCommand($sql, [':ezf_id'=>$ezf_id, ':ezf_field_name'=>$ezf_field_name])->queryAll();
    }
    
    public static function getEzformReportById($id) {
	
	$sql = "SELECT ezform.ezf_id, ezform.ezf_name, ezf_table, comp_id_target, field_detail, unique_record , ezform_config.*
		FROM ezform INNER JOIN ezform_config ON ezform_config.ezf_id = ezform.ezf_id
		WHERE ezform.ezf_id = :id AND ezform_config.config_type = 'report' ";
	
	return Yii::$app->db->createCommand($sql, [':id'=>$id])->queryOne();
    }
    
    public static function getEzformReportByIdAll($id) {
	
	$sql = "SELECT ezform_config.*
		FROM ezform_config
		WHERE ezform_config.ezf_id = :id AND ezform_config.config_type = 'report' ";
	
	return Yii::$app->db->createCommand($sql, [':id'=>$id])->queryAll();
    }
    
    public static function getEzformReportByUser($user_id, $ezf_id) {
	
	$sql = "SELECT ezform_config.*
		FROM ezform_report INNER JOIN ezform_config ON ezform_config.config_id = ezform_report.config_id
		WHERE ezform_report.ezf_id = :id AND ezform_report.user_id = :user_id ";
	
	return Yii::$app->db->createCommand($sql, [':id'=>$ezf_id, ':user_id'=>$user_id])->queryAll();
    }
    
    public static function getProvince(){
        $sql = "SELECT `PROVINCE_ID`, `PROVINCE_CODE`,`PROVINCE_NAME` FROM `const_province`";
        return Yii::$app->db->createCommand($sql)->queryAll();
    }
    
    public static function addFields($obj, $ezf_id, $ezf_field_id, $dataEzf) {
	$column = [
            'ezf_field_id'=> $obj['id'],
            'ezf_id'=>$ezf_id,
            'ezf_field_name'=>$obj['attribute'],
            'ezf_field_label'=>isset($obj['label'])?$obj['label']:$obj['suffix'],
            'ezf_field_type'=>isset($obj['type'])?$obj['type']:0,
            'ezf_field_ref'=>$ezf_field_id,
            'ezf_field_lenght'=>12,
            'ezf_margin_col'=>0,
            'ezf_default_choice'=>12,
            'ezf_field_ref_field'=>$ezf_field_id,
            'table_field_type'=>'VARCHAR',
            'table_field_length'=>150,
            'ezf_field_required'=>0,
            'ezf_field_lenght'=>12,
        ];

        $create_field = EzfFunc::systemCreateField($column);
        $alterTable = true;
        if($create_field){
            $alterTable = EzfFunc::alterTableAdd($dataEzf['ezf_table'], $column['ezf_field_name'], $column['table_field_type'], $column['table_field_length']);
        }
        return $alterTable;
    }
    
    public static function updateFields($obj, $dataEzf) {
        
	$model = \backend\modules\ezforms2\models\EzformFields::findOne(['ezf_field_id' => $obj['id']]);
        $oldNameOther = $model->ezf_field_name;
        
        $model->ezf_field_name = $obj['attribute'];
        $model->ezf_field_label = isset($obj['label'])?$obj['label']:$obj['suffix'];
        $model->ezf_field_type = isset($obj['type'])?$obj['type']:0;

        $alterTable = true;
        if($oldNameOther!=$model->ezf_field_name){
            $alterTable = EzfFunc::alterTableChange($dataEzf['ezf_table'], $oldNameOther, $model->ezf_field_name, $model->table_field_type, $model->table_field_length);
        }
        $model->save();
	
	return $alterTable;
    }
    
}
