<?php

namespace backend\modules\ezforms2\models;

use Yii;

/**
 * This is the model class for table "ezform_export".
 *
 * @property string $tbname
 * @property string $export_sql
 * @property string $export_select
 * @property string $export_join
 * @property string $export_where
 * @property string $lable_extra
 * @property integer $status
 */
class EzformExport extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ezform_export';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tbname'], 'required'],
            [['export_sql', 'export_select', 'export_join', 'export_where', 'lable_extra'], 'string'],
            [['status'], 'integer'],
	    [['export_sql', 'export_select', 'export_join', 'export_where', 'lable_extra', 'status'], 'safe'],
            [['tbname'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'tbname' => Yii::t('app', 'ชื่อตาราง'),
            'export_sql' => Yii::t('app', 'Custom Sql'),
            'export_select' => Yii::t('app', 'ฟิลด์ที่ต้องการแสดงเพิ่มเติม'),
            'export_join' => Yii::t('app', 'การเชื่อมตาราง'),
            'export_where' => Yii::t('app', 'เงื่อนไข'),
            'lable_extra' => Yii::t('app', 'คำอธิบาย'),
            'status' => Yii::t('app', 'Status'),
        ];
    }
}
