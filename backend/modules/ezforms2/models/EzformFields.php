<?php

namespace backend\modules\ezforms2\models;

use Yii;

/**
 * This is the model class for table "ezform_fields".
 *
 * @property string $ezf_field_id
 * @property string $ezf_id
 * @property integer $ezf_field_group
 * @property string $ezf_field_name
 * @property string $ezf_field_label
 * @property string $ezf_field_sub_id
 * @property string $ezf_field_sub_textvalue
 * @property string $ezf_field_default
 * @property string $ezf_field_help
 * @property integer $ezf_field_type
 * @property integer $ezf_field_ref
 * @property double $ezf_field_order
 * @property integer $ezf_field_lenght
 * @property integer $ezf_margin_col
 * @property integer $ezf_default_choice
 * @property integer $ezf_field_rows
 * @property integer $ezf_component
 * @property string $ezf_field_province
 * @property string $ezf_field_amphur
 * @property string $ezf_field_tumbon
 * @property string $ezf_field_val
 * @property integer $ezf_field_head_label
 * @property integer $ezf_field_ref_field
 * @property integer $ezf_field_ref_table
 * @property string $ezf_field_hint
 * @property integer $ezf_field_required
 * @property string $ezf_field_validate
 * @property string $ezf_field_data
 * @property string $ezf_field_specific
 * @property string $ezf_field_options
 * @property string $ezf_field_color
 * @property integer $ezf_field_icon
 * @property integer $ezf_field_text_html
 *@property integer $table_field_type
 * @property integer $table_field_length
 * 
 * @property EzformChoice[] $ezformChoices
 * @property EzformComponent[] $ezformComponents
 * @property Ezform $ezf
 */
class EzformFields extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ezform_fields';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ezf_field_id'], 'required'],
	    ['ezf_field_name','match','pattern'=>'/^[a-z0-9_]+$/i','message'=>'ตัวแปรต้องเป็นภาษาอังกฤษหรือตัวเลขเท่านั้นและห้ามเว้นวรรค'],
            [['ezf_field_id', 'ezf_id', 'ezf_field_group', 'ezf_field_type', 'ezf_field_ref', 'ezf_field_lenght', 'ezf_margin_col', 'ezf_default_choice', 'ezf_field_rows', 'ezf_component', 'ezf_field_head_label', 'ezf_field_ref_field', 'ezf_field_ref_table', 'ezf_field_required', 'ezf_field_icon', 'ezf_field_text_html'], 'integer'],
            [['ezf_field_help', 'ezf_field_hint', 'ezf_field_validate', 'ezf_field_data', 'ezf_field_specific', 'ezf_field_options'], 'string'],
            [['ezf_field_order'], 'number'],
	    [['table_field_length', 'table_field_type', 'ezf_condition'], 'safe'],
            [['ezf_field_name', 'ezf_field_label', 'ezf_field_sub_id', 'ezf_field_sub_textvalue', 'ezf_field_default', 'ezf_field_province', 'ezf_field_amphur', 'ezf_field_tumbon', 'ezf_field_val'], 'string', 'max' => 255],
            [['ezf_field_color'], 'string', 'max' => 20]
        ];
    }

    public function checkValue($attribute,$params){

            if(preg_match('/^[a-z0-9_]+$/i',$this->ezf_field_name)  ){   // เช็คว่าต้องข้อความต้องเป็นอังกฤษหรือตัวเลขเท่านั้น


            }else{

                $this->addError($attribute,'ตัวแปรต้องเป็นภาษาอังกฤษหรือตัวเลขเท่านั้นและห้ามเว้นวรรค');

            }



    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ezf_field_id' => Yii::t('app', 'รหัสของฟิลด์'),
            'ezf_id' => Yii::t('app', 'Ezf ID'),
            'ezf_field_group' => Yii::t('app', 'Ezf Field Group'),
            'ezf_field_name' => Yii::t('app', 'ชื่อตัวแปร'),
            'ezf_field_label' => Yii::t('app', 'ชื่อคำถาม'),
            'ezf_field_sub_id' => Yii::t('app', 'Ezf Field Sub ID'),
            'ezf_field_sub_textvalue' => Yii::t('app', 'Ezf Field Sub Textvalue'),
            'ezf_field_default' => Yii::t('app', 'ค่าที่เลือกอัตโนมัติ'),
            'ezf_field_help' => Yii::t('app', 'ข้อความช่วยเหลือ'),
            'ezf_field_type' => Yii::t('app', 'ประเภทคำถาม'),
            'ezf_field_ref' => Yii::t('app', 'ฟิลด์อ้างอิง'),
            'ezf_field_order' => Yii::t('app', 'ลำดับของคำถาม'),
            'ezf_field_lenght' => Yii::t('app', 'ความยาวของคำถาม'),
            'ezf_margin_col' => Yii::t('app', 'ความกว้างเยื้อง'),
            'ezf_default_choice' => Yii::t('app', 'ค่าปริยายความกว้างตัวเลือก'),
            'ezf_field_rows' => Yii::t('app', 'Ezf Field Rows'),
            'ezf_component' => Yii::t('app', 'Ezf Component'),
            'ezf_field_province' => Yii::t('app', 'Ezf Field Province'),
            'ezf_field_amphur' => Yii::t('app', 'Ezf Field Amphur'),
            'ezf_field_tumbon' => Yii::t('app', 'Ezf Field Tumbon'),
            'ezf_field_val' => Yii::t('app', 'Ezf Field Val'),
            'ezf_field_head_label' => Yii::t('app', 'Ezf Field Head Label'),
            'ezf_field_ref_field' => Yii::t('app', 'Ezf Field Ref Field'),
            'ezf_field_ref_table' => Yii::t('app', 'Ezf Field Ref Table'),
            'ezf_field_hint' => Yii::t('app', 'ข้อความช่วยเหลือ'),
            'ezf_field_required' => Yii::t('app', 'Required'),
            'ezf_field_validate' => Yii::t('app', 'Validate'),
            'ezf_field_data' => Yii::t('app', 'Data'),
            'ezf_field_specific' => Yii::t('app', 'Specific'),
            'ezf_field_options' => Yii::t('app', 'Options'),
            'ezf_field_color' => Yii::t('app', 'สีพื้นหลัง'),
            'ezf_field_icon' => Yii::t('app', 'Ezf Field Icon'),
            'ezf_field_text_html' => Yii::t('app', 'Ezf Field Text Html'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEzformChoices()
    {
        return $this->hasMany(EzformChoice::className(), ['ezf_field_id' => 'ezf_field_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEzformComponents()
    {
        return $this->hasMany(EzformComponent::className(), ['field_id_key' => 'ezf_field_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEzf()
    {
        return $this->hasOne(Ezform::className(), ['ezf_id' => 'ezf_id']);
    }
}
