<?php

namespace backend\modules\ezforms2\models;

use Yii;

/**
 * This is the model class for table "ezform".
 *
 * @property string $ezf_id
 * @property string $ezf_version
 * @property string $ezf_name
 * @property string $ezf_detail
 * @property string $xsourcex
 * @property string $ezf_table
 * @property string $user_create
 * @property string $create_date
 * @property string $user_update
 * @property string $update_date
 * @property integer $status
 * @property integer $shared
 * @property integer $public_listview
 * @property integer $public_edit
 * @property integer $public_delete
 * @property string $comp_id_target
 * @property string $co_dev
 * @property string $assign
 * @property integer $category_id
 * @property string $field_detail
 * @property string $sql
 * @property string $js
 * @property string $error
 * @property integer $query_tools
 * @property integer $unique_record
 * @property integer $reply_tools
 * @property string $consultant_users
 * @property string $telegram
 *
 * @property ActFormRelation[] $actFormRelations
 * @property ActCategory[] $acts
 * @property EzformAssign[] $ezformAssigns
 * @property User[] $users
 * @property EzformCoDev[] $ezformCoDevs
 * @property EzformComponent[] $ezformComponents
 * @property EzformFields[] $ezformFields
 * @property EzformTarget[] $ezformTargets
 */
class Ezform extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ezform';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ezf_id', 'ezf_detail', 'xsourcex', 'ezf_table', 'user_create', 'create_date', 'user_update', 'update_date', 'status', 'co_dev', 'assign', 'field_detail'], 'required'],
            [['ezf_id', 'user_create', 'user_update', 'status', 'shared', 'public_listview', 'public_edit', 'public_delete', 'comp_id_target', 'category_id', 'query_tools', 'unique_record', 'reply_tools'], 'integer'],
            [['ezf_detail', 'co_dev', 'assign', 'sql_condition', 'sql_announce', 'js', 'error', 'consultant_users'], 'string'],
            [['create_date', 'update_date'], 'safe'],
            [['ezf_version', 'telegram'], 'string', 'max' => 50],
            [['ezf_name', 'ezf_table'], 'string', 'max' => 100],
            [['xsourcex'], 'string', 'max' => 20],
            [['field_detail'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ezf_id' => Yii::t('app', 'รหัสฟอร์ม'),
            'ezf_version' => Yii::t('app', 'Ezf Version'),
            'ezf_name' => Yii::t('app', 'ชื่อฟอร์ม'),
            'ezf_detail' => Yii::t('app', 'รายละเอียด'),
            'xsourcex' => Yii::t('app', 'Xsourcex'),
            'ezf_table' => Yii::t('app', 'ตาราง'),
            'user_create' => Yii::t('app', 'ผู้สร้าง'),
            'create_date' => Yii::t('app', 'วันที่สร้าง'),
            'user_update' => Yii::t('app', 'ผู้แก้ไข'),
            'update_date' => Yii::t('app', 'วันที่แก้ไข'),
            'status' => Yii::t('app', 'สถานะ'),
            'shared' => Yii::t('app', 'private = 0, public = 1'),
            'public_listview' => Yii::t('app', 'อนุญาตให้ view List Data table ของฟอร์ม'),
            'public_edit' => Yii::t('app', 'อนุญาตให้ Edit/Update List Data table ของฟอร์ม'),
            'public_delete' => Yii::t('app', 'อนุญาตให้ Remove List Data table ของฟอร์ม'),
            'comp_id_target' => Yii::t('app', 'เป้าหมายถ้าไม่เลือกคือตัวเอง'),
            'co_dev' => Yii::t('app', 'Co Dev'),
            'assign' => Yii::t('app', 'Assign'),
            'category_id' => Yii::t('app', 'Category ID'),
            'field_detail' => Yii::t('app', 'Field Detail'),
            'sql_announce' => Yii::t('app', 'SQL Announce'),
            'sql_condition' => Yii::t('app', 'SQL Condition'),
            'js' => Yii::t('app', 'Js'),
            'error' => Yii::t('app', 'Error'),
            'query_tools' => Yii::t('app', 'Query Tools'),
            'unique_record' => Yii::t('app', 'Unique Record'),
            'reply_tools' => Yii::t('app', 'Reply Tools'),
            'consultant_users' => Yii::t('app', 'Consultant Users'),
            'telegram' => Yii::t('app', 'Telegram'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActFormRelations()
    {
        return $this->hasMany(ActFormRelation::className(), ['ezf_id' => 'ezf_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActs()
    {
        return $this->hasMany(ActCategory::className(), ['act_id' => 'act_id'])->viaTable('act_form_relation', ['ezf_id' => 'ezf_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEzformAssigns()
    {
        return $this->hasMany(EzformAssign::className(), ['ezf_id' => 'ezf_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['id' => 'user_id'])->viaTable('ezform_assign', ['ezf_id' => 'ezf_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEzformCoDevs()
    {
        return $this->hasMany(EzformCoDev::className(), ['ezf_id' => 'ezf_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEzformComponents()
    {
        return $this->hasMany(EzformComponent::className(), ['ezf_id' => 'ezf_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEzformFields()
    {
        return $this->hasMany(EzformFields::className(), ['ezf_id' => 'ezf_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEzformTargets()
    {
        return $this->hasMany(EzformTarget::className(), ['ezf_id' => 'ezf_id']);
    }
}
