<?php

namespace backend\modules\ezforms2\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\ezforms2\models\EzformFields;

/**
 * EzformFieldsSearch represents the model behind the search form about `backend\modules\ezforms2\models\EzformFields`.
 */
class EzformFieldsSearch extends EzformFields
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['table_field_length', 'ezf_field_id', 'ezf_id', 'ezf_field_group', 'ezf_field_type', 'ezf_field_ref', 'ezf_field_lenght', 'ezf_margin_col', 'ezf_default_choice', 'ezf_field_rows', 'ezf_component', 'ezf_field_head_label', 'ezf_field_ref_field', 'ezf_field_ref_table', 'ezf_field_required', 'ezf_field_icon', 'ezf_field_text_html'], 'integer'],
            [[ 'table_field_type', 'ezf_field_name', 'ezf_field_label', 'ezf_field_sub_id', 'ezf_field_sub_textvalue', 'ezf_field_default', 'ezf_field_help', 'ezf_field_province', 'ezf_field_amphur', 'ezf_field_tumbon', 'ezf_field_val', 'ezf_field_hint', 'ezf_field_validate', 'ezf_field_data', 'ezf_field_specific', 'ezf_field_options', 'ezf_field_color'], 'safe'],
            [['ezf_field_order'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = EzformFields::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'ezf_field_id' => $this->ezf_field_id,
            'ezf_id' => $this->ezf_id,
            'ezf_field_group' => $this->ezf_field_group,
            'ezf_field_type' => $this->ezf_field_type,
            'ezf_field_ref' => $this->ezf_field_ref,
            'ezf_field_order' => $this->ezf_field_order,
            'ezf_field_lenght' => $this->ezf_field_lenght,
            'ezf_margin_col' => $this->ezf_margin_col,
            'ezf_default_choice' => $this->ezf_default_choice,
            'ezf_field_rows' => $this->ezf_field_rows,
            'ezf_component' => $this->ezf_component,
            'ezf_field_head_label' => $this->ezf_field_head_label,
            'ezf_field_ref_field' => $this->ezf_field_ref_field,
            'ezf_field_ref_table' => $this->ezf_field_ref_table,
            'ezf_field_required' => $this->ezf_field_required,
            'ezf_field_icon' => $this->ezf_field_icon,
            'ezf_field_text_html' => $this->ezf_field_text_html,
	    'table_field_length' => $this->table_field_length,
        ]);

        $query->andFilterWhere(['like', 'ezf_field_name', $this->ezf_field_name])
            ->andFilterWhere(['like', 'ezf_field_label', $this->ezf_field_label])
            ->andFilterWhere(['like', 'ezf_field_sub_id', $this->ezf_field_sub_id])
            ->andFilterWhere(['like', 'ezf_field_sub_textvalue', $this->ezf_field_sub_textvalue])
            ->andFilterWhere(['like', 'ezf_field_default', $this->ezf_field_default])
            ->andFilterWhere(['like', 'ezf_field_help', $this->ezf_field_help])
            ->andFilterWhere(['like', 'ezf_field_province', $this->ezf_field_province])
            ->andFilterWhere(['like', 'ezf_field_amphur', $this->ezf_field_amphur])
            ->andFilterWhere(['like', 'ezf_field_tumbon', $this->ezf_field_tumbon])
            ->andFilterWhere(['like', 'ezf_field_val', $this->ezf_field_val])
            ->andFilterWhere(['like', 'ezf_field_hint', $this->ezf_field_hint])
            ->andFilterWhere(['like', 'ezf_field_validate', $this->ezf_field_validate])
            ->andFilterWhere(['like', 'ezf_field_data', $this->ezf_field_data])
            ->andFilterWhere(['like', 'ezf_field_specific', $this->ezf_field_specific])
            ->andFilterWhere(['like', 'ezf_field_options', $this->ezf_field_options])
		->andFilterWhere(['like', 'table_field_type', $this->table_field_type])
            ->andFilterWhere(['like', 'ezf_field_color', $this->ezf_field_color]);

        return $dataProvider;
    }
}
