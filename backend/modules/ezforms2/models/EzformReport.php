<?php

namespace backend\modules\ezforms2\models;

use Yii;

/**
 * This is the model class for table "ezform_report".
 *
 * @property integer $report_id
 * @property integer $user_id
 * @property integer $ezf_id
 * @property integer $config_id
 */
class EzformReport extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ezform_report';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['report_id', 'user_id', 'ezf_id', 'config_id'], 'required'],
            [['report_id', 'user_id', 'ezf_id', 'config_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'report_id' => Yii::t('app', 'Report ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'ezf_id' => Yii::t('app', 'Ezf ID'),
            'config_id' => Yii::t('app', 'Config ID'),
        ];
    }
}
