<?php

namespace backend\modules\ezforms2\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use appxq\sdii\helpers\SDHtml;

/**
 * Select2Controller implements the CRUD actions for EzformInput model.
 */
class Select2Controller extends Controller
{
    
    public function actionCreate()
    {
	if (Yii::$app->getRequest()->isAjax) {
	    Yii::$app->response->format = Response::FORMAT_JSON;
	    
	    $row = isset($_POST['row'])?$_POST['row']:0;
	    
	    $html = $this->renderAjax('/widgets/select2/_formitem', [
		'row' => $row,
	    ]);
	    
	    $result = [
		'status' => 'success',
		'action' => 'create',
		'message' => SDHtml::getMsgSuccess() . Yii::t('app', 'Data completed.'),
		'html' => $html,
	    ];
	    return $result;
	} else {
	    throw new NotFoundHttpException('Invalid request. Please do not repeat this request again.');
	}
    }
    
    public function actionHospital($q = null, $id = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            $sql = "SELECT lpad(`code`,'5','0') AS `code`,`name` FROM `const_hospital` WHERE `name` LIKE :q OR `code` LIKE :q LIMIT 0,100";
            $data = Yii::$app->db->createCommand($sql, [':q'=>"%$q%"])->queryAll();
            $i = 0;
            $out=[];
            foreach($data as $value){
                $out["results"][$i] = ['id'=>$value['code'],'text'=>$value["code"]." : ".$value["name"]];
                $i++;
            }
        }
//        if ($id > 0) {
//            $out['results'] = ['id' => $id, 'text' => City::find($id)->name];
//        }
//        
        return $out;
    }
    
    public function actionSnomed($q = null, $id = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $q=urlencode($q);
        $json = file_get_contents("http://www.cascap.in.th:9201/snomed/description/_search?q=TERM:{$q}&size=50&sort=_score:desc");
        $arrayJson = json_decode($json,true);
        $data=$arrayJson['hits']['hits'];
        $i=0;
        foreach ($data as $snomed) {
                $json2 = file_get_contents("http://www.cascap.in.th:9201/snomed/concept/_search?q=".$snomed['_source']['CONCEPTID']);
                $arrayJson2 = json_decode($json2,true);
                $data2=$arrayJson2['hits']['hits']['0']['_source'];
                //print_r($data2);exit;
                $out['results'][$i] = ['id' => $snomed['_source']['DESCRIPTIONID'], 'text' => "<b>" . $snomed['_source']['TERM']."</b> (".$data2['FULLYSPECIFIEDNAME'].")"];
                $i++;
        }
        return  $out;
    }
    

    public function actionIcd10($q = null, $id = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            $sql = "SELECT * FROM `icd10` WHERE CONCAT(`code`, `name`) LIKE :q LIMIT 0,100";
            $data = Yii::$app->db->createCommand($sql, [':q'=>"%$q%"])->queryAll();
            $i = 0;
            $out=[];
            foreach($data as $value){
                $out["results"][$i] = ['id'=>$value['code'],'text'=>$value["code"]." : ".$value["name"]];
                $i++;
            }
        }
//        if ($id > 0) {
//            $out['results'] = ['id' => $id, 'text' => City::find($id)->name];
//        }
//        
        return $out;
    }
    
    public function actionIcd9($q = null, $id = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            $sql = "SELECT * FROM `icd9` WHERE CONCAT(`code`, `name`) LIKE :q LIMIT 0,100";
            $data = Yii::$app->db->createCommand($sql, [':q'=>"%$q%"])->queryAll();
            $i = 0;
            $out=[];
            foreach($data as $value){
                $out["results"][$i] = ['id'=>$value['code'],'text'=>$value["code"]." : ".$value["name"]];
                $i++;
            }
        }
//        if ($id > 0) {
//            $out['results'] = ['id' => $id, 'text' => City::find($id)->name];
//        }
//        
        return $out;
    }
    
    
}
