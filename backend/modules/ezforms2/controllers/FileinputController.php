<?php

namespace backend\modules\ezforms2\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use appxq\sdii\helpers\SDHtml;

/**
 * Select2Controller implements the CRUD actions for EzformInput model.
 */
class FileinputController extends Controller
{
    
    public function actionDelete($id) {
        if (Yii::$app->getRequest()->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $model = \backend\modules\ezforms2\models\FileUpload::findOne($id);
            $file_name = $model['file_name'];
            if ($model->delete()) {
                    @unlink(Yii::$app->basePath . '/../backend/web/fileinput/' . $file_name);
                    $result = [
                            'status' => 'success',
                            'action' => 'update',
                            'message' =>  Yii::t('app', 'Deleted completed.'),
                            'data' => $id,
                    ];
                    return $result;
            } else {
                    $result = [
                            'status' => 'error',
                            'message' => Yii::t('app', 'Can not delete the data.'),
                            'data' => $id,
                    ];
                    return $result;
            }
        } else {
                throw new NotFoundHttpException('Invalid request. Please do not repeat this request again.');
        }
    }
    
    
}
