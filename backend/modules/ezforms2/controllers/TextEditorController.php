<?php

namespace backend\modules\ezforms2\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use appxq\sdii\helpers\SDHtml;

/**
 * Select2Controller implements the CRUD actions for EzformInput model.
 */
class TextEditorController extends Controller
{
    
    public function actions()
    {
        return [
            'image-upload' => [
                'class' => 'vova07\imperavi\actions\UploadAction',
                'url' => Yii::getAlias('@storageUrl') . '/form-upload', // Directory URL address, where files are stored.
                'path' => '@app/../storage/web/form-upload', // Or absolute path to directory where files are stored.
                    'validatorOptions' => [
                      'maxWidth' => 3000,
                      'maxHeight' => 3000
                  ],
            ],
            'file-upload' => [
                'class' => 'vova07\imperavi\actions\UploadAction',
                'url' => Yii::getAlias('@storageUrl') . '/form-upload', // Directory URL address, where files are stored.
                'path' => '@app/../storage/web/form-upload', // Or absolute path to directory where files are stored.
                'uploadOnlyImage' => false,
                'validatorOptions' => [
                    'maxSize' => 500000000
                ]
            ],
            'files-get' => [
                'class' => 'vova07\imperavi\actions\GetAction',
                'url' => Yii::getAlias('@storageUrl') . '/form-upload', // Directory URL address, where files are stored.
                'path' => '@app/../storage/web/form-upload', // Or absolute path to directory where files are stored.
                'type' => \vova07\imperavi\actions\GetAction::TYPE_FILES,
            ],
            'images-get' => [
                'class' => 'vova07\imperavi\actions\GetAction',
                'url' => Yii::getAlias('@storageUrl') . '/form-upload', // Directory URL address, where files are stored.
                'path' => '@app/../storage/web/form-upload', // Or absolute path to directory where files are stored.
                'type' => \vova07\imperavi\actions\GetAction::TYPE_IMAGES,
            ]
        ];
    }

}
