<?php
use yii\web\JsExpression;
use yii\helpers\ArrayHelper;

/**
 * _pie file UTF-8
 * @author SDII <iencoded@gmail.com>
 * @copyright Copyright &copy; 2015 AppXQ
 * @license http://www.appxq.com/license/
 * @version 1.0.0 Date: 5 ต.ค. 2559 19:39:49
 * @link http://www.appxq.com/
 */
$options = appxq\sdii\utils\SDUtility::string2Array($config['config_options']);

$sql = "SELECT {$options['field']} , count(*) AS num
	FROM $ezf_table
	WHERE ptid = :ptid
	group by {$options['field']}
	";

$data = Yii::$app->db->createCommand($sql, [':ptid'=>$target])->queryAll();

$choice = [];
$modelField = \backend\modules\ezforms\models\EzformFields::find()
						    ->where('ezf_id=:id AND ezf_field_name=:field',[':id'=>$ezf_id, ':field'=>$options['field']])
						    ->one();
if($modelField){
    if(in_array($modelField['ezf_field_type'], [4,6])){
	$modelChoice = \backend\modules\ezforms\models\EzformChoice::find()
						    ->where('ezf_field_id=:id',[':id'=>$modelField['ezf_field_id']])
						    ->all();
	if($modelChoice){
	    $choice = ArrayHelper::map($modelChoice, 'ezf_choicevalue', 'ezf_choicelabel');
	}
    }
}


$dataPie=[]; 
$nameLabel = $ezf_name;

if($data){
    foreach ($data as $key => $value) {
	$name = $value[$options['field']];
	if(!empty($choice)){
	    $name = $choice[$value[$options['field']]];
	}
	$dataPie[] = ['name'=> $name, 'y'=>$value['num']+0];
    }
}
?>

<?= miloschuman\highcharts\Highcharts::widget([
    'options' => [
	'chart' => [
	    'plotBackgroundColor'=> NULL,
	    'plotBorderWidth'=> NULL,
	    'plotShadow'=> false,
	    'type'=> 'pie',
	],
	'title' => ['text' => $nameLabel],
	'tooltip' => ['pointFormat'=>new JsExpression("'{series.name}: <b>{point.percentage:.1f}%</b>'")],
	'plotOptions'=> [
	    'pie' => [
		'allowPointSelect' => true,
		'cursor' => 'pointer',
		'dataLabels' => [
		    'enabled'=>true,
		    'format'=>new JsExpression("'<b>{point.name}</b>: {point.percentage:.1f} %'"),
		    'style' => [
			'color' => new JsExpression("(Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'")
		    ]
		]
	    ]
	],
	'series' => [
	  [
	      'name' => $nameLabel,
	      'colorByPoint' => true,
	      'data' => $dataPie
	  ]
	]
     ]
]);?>
