<?php
use yii\web\JsExpression;

/**
 * _line_graph file UTF-8
 * @author SDII <iencoded@gmail.com>
 * @copyright Copyright &copy; 2015 AppXQ
 * @license http://www.appxq.com/license/
 * @version 1.0.0 Date: 5 ต.ค. 2559 19:39:21
 * @link http://www.appxq.com/
 * SELECT AVG(bmi) FROM tbdata_1467908585004525300 WHERE rstat<>3 AND bmi>0
 */

$options = appxq\sdii\utils\SDUtility::string2Array($config['config_options']);

$sql = "SELECT {$options['field']}
	FROM $ezf_table
	WHERE ptid = :ptid
	";

$data = Yii::$app->db->createCommand($sql, [':ptid'=>$target])->queryAll();

$dataChart=[]; 
$categories=[];
$nameLabel = $ezf_name;
$criteria = [];
$group = [];
$avg = 0;
if(isset($options['group']) && $options['group']=='1'){
    $sql = "SELECT AVG({$options['field']}) FROM $ezf_table WHERE rstat<>3 AND {$options['field']}>0
	";

    $avg = Yii::$app->db->createCommand($sql)->queryScalar();
}

if($data){
    foreach ($data as $key => $value) {
	$categories[] = 'ครั้งที่ '.($key+1);
	$dataChart[] = $value[$options['field']]+0;
	
	if(isset($options['criteria']) && $options['criteria']=='1'){
	    $criteria[] = $options['standard']+0;
	}
	
	if(isset($options['group']) && $options['group']=='1'){
	    $group[] = $avg+0;
	}
    }
}
$series = [['name' => $nameLabel,  'data' => $dataChart]];

if(isset($options['criteria']) && $options['criteria']=='1'){
    $series[] = ['name' => 'ค่ามาตรฐาน',  'data' => $criteria];
}

if(isset($options['group']) && $options['group']=='1'){
    $series[] = ['name' => 'อิงกลุ่ม', 'data' => $group];
}

?>

<?= miloschuman\highcharts\Highcharts::widget([
    'options' => [
	'title' => ['text' => $nameLabel],
	'xAxis' => [
	   'categories' => $categories
	],
	'yAxis' => [
	   'title' => ['text' => 'จำนวน']
	],
	'series' => $series
     ]
]);?>
