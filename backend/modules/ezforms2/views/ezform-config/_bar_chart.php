<?php
use yii\web\JsExpression;
use yii\helpers\ArrayHelper;

/**
 * _line_graph file UTF-8
 * @author SDII <iencoded@gmail.com>
 * @copyright Copyright &copy; 2015 AppXQ
 * @license http://www.appxq.com/license/
 * @version 1.0.0 Date: 5 ต.ค. 2559 19:39:21
 * @link http://www.appxq.com/
 * SELECT AVG(bmi) FROM tbdata_1467908585004525300 WHERE rstat<>3 AND bmi>0
 */

$options = appxq\sdii\utils\SDUtility::string2Array($config['config_options']);

$sql = "SELECT {$options['field']} , count(*) AS num
	FROM $ezf_table
	WHERE ptid = :ptid
	group by {$options['field']}
	";

$data = Yii::$app->db->createCommand($sql, [':ptid'=>$target])->queryAll();

$choice = [];
$modelField = \backend\modules\ezforms\models\EzformFields::find()
						    ->where('ezf_id=:id AND ezf_field_name=:field',[':id'=>$ezf_id, ':field'=>$options['field']])
						    ->one();
if($modelField){
    if(in_array($modelField['ezf_field_type'], [4,6])){
	$modelChoice = \backend\modules\ezforms\models\EzformChoice::find()
						    ->where('ezf_field_id=:id',[':id'=>$modelField['ezf_field_id']])
						    ->all();
	if($modelChoice){
	    $choice = ArrayHelper::map($modelChoice, 'ezf_choicevalue', 'ezf_choicelabel');
	}
    }
}

$dataChart=[]; 
$categories=[];
$nameLabel = $ezf_name;

if($data){
    foreach ($data as $key => $value) {
	$name = $value[$options['field']];
	if(!empty($choice)){
	    $name = $choice[$value[$options['field']]];
	}
	$categories[] = $name;
	$dataChart[] = $value['num']+0;
    }
}
$series = [['name' => $nameLabel, 'data' => $dataChart]];

?>

<?= miloschuman\highcharts\Highcharts::widget([
    'options' => [
	'chart' => [
            'type' => 'column'
        ],
	'title' => ['text' => $nameLabel],
	'xAxis' => [
	   'categories' => $categories
	],
	'yAxis' => [
	   'title' => ['text' => 'จำนวน']
	],
	'series' => $series
     ]
]);?>