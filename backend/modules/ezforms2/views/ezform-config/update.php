<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\ezforms2\models\EzformConfig */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Ezform Config',
]) . ' ' . $model->config_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Ezform Configs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->config_id, 'url' => ['view', 'id' => $model->config_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="ezform-config-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
