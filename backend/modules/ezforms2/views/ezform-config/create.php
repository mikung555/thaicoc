<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\ezforms2\models\EzformConfig */

$this->title = Yii::t('app', 'Create Ezform Config');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Ezform Configs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ezform-config-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
