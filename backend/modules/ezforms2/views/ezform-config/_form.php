<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use appxq\sdii\helpers\SDNoty;
use appxq\sdii\helpers\SDHtml;

/* @var $this yii\web\View */
/* @var $model backend\modules\ezforms2\models\EzformConfig */
/* @var $form yii\bootstrap\ActiveForm */
?>

<div class="ezform-config-form">

    <?php $form = ActiveForm::begin([
	'id'=>$model->formName(),
    ]); ?>

    <div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	<h4 class="modal-title" id="itemModalLabel">Ezform Config</h4>
    </div>

    <div class="modal-body">
	
	<div class="row">
	    <div class="col-md-6">
		<?= $form->field($model, 'config_name')->dropDownList(\backend\modules\ezforms2\classes\EzfFunc::itemAlias('reportItems')) ?>
	    </div>
	    <div class="col-md-6" style="padding-top: 20px;">
		<?= $form->field($model, 'config_value')->checkbox() ?>
	    </div>
	</div>
	<div class="row">
	    <div class="col-md-6">
		<?php
		$dataFields = \backend\modules\inv\classes\InvQuery::getFieldsId($model->ezf_id);
		?>
		<?=  Html::label('ฟิลด์ที่ต้องการคำนวณ')?>
		<?=  Html::dropDownList('options[field]', $model->config_options['field'], \yii\helpers\ArrayHelper::map($dataFields, 'ezf_field_name', 'name'),['class'=>'form-control']);?>
	    </div>
	</div>
	<br>
	<div class="row">
	    <div class="col-md-3" style="padding-top: 25px;">
		<?=  Html::checkbox('options[criteria]', $model->config_options['criteria'], ['label'=>'อิงเกณฑ์', 'id'=>'criteria'])?>
	    </div>
	    <div class="col-md-3">
		<?=  Html::label('ค่ามาตรฐาน')?>
		<?=  Html::textInput('options[standard]', $model->config_options['standard'], ['class'=>'form-control', 'type'=>'number', 'id'=>'standard', 'disabled'=>(isset($model->config_options['criteria']) && $model->config_options['criteria']==1)?false:true])?>
	    </div>
	</div>
	<div class="row">
	    <div class="col-md-6">
		<?=  Html::checkbox('options[group]', $model->config_options['group'], ['label'=>'อิงกลุ่ม'])?>
	    </div>
	</div>
	
	<?= $form->field($model, 'config_id')->hiddenInput()->label(false) ?>
	<?= $form->field($model, 'config_type')->hiddenInput()->label(false) ?>
	<?= $form->field($model, 'ezf_id')->hiddenInput()->label(false) ?>
	
    </div>
    <div class="modal-footer">
	<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	<?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php  $this->registerJs("
$('#criteria').change(function() {
    if(this.checked){
	$('#standard').attr('disabled', false);
    } else {
	$('#standard').attr('disabled', true);
    }
});

$('form#{$model->formName()}').on('beforeSubmit', function(e) {
    var \$form = $(this);
    $.post(
	\$form.attr('action'), //serialize Yii2 form
	\$form.serialize()
    ).done(function(result) {
	if(result.status == 'success') {
	    ". SDNoty::show('result.message', 'result.status') ."
	    if(result.action == 'create') {
		$(document).find('#modal-ezform-config').modal('hide');
		$.pjax.reload({container:'#ezform-config-grid-pjax'});
	    } else if(result.action == 'update') {
		$(document).find('#modal-ezform-config').modal('hide');
		$.pjax.reload({container:'#ezform-config-grid-pjax'});
	    }
	} else {
	    ". SDNoty::show('result.message', 'result.status') ."
	} 
    }).fail(function() {
	". SDNoty::show("'" . SDHtml::getMsgError() . "Server Error'", '"error"') ."
	console.log('server error');
    });
    return false;
});

");?>