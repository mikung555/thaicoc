<?php
use Yii;
use yii\helpers\Html;
use yii\helpers\Url;
use common\lib\sdii\components\helpers\SDNoty;

/**
 * _textinput_options file UTF-8
 * @author SDII <iencoded@gmail.com>
 * @copyright Copyright &copy; 2015 AppXQ
 * @license http://www.appxq.com/license/
 * @version 1.0.0 Date: 19 ส.ค. 2559 19:02:45
 * @link http://www.appxq.com/
 * @example  
 */
$model_form = \backend\modules\ezforms2\models\Ezform::find()->where(['ezf_id'=>$modelFields->ezf_id])->One();

$sql = "SELECT * FROM ".$model_form->ezf_table." WHERE id='".$model->id."' ";

$dataFileInput = Yii::$app->db->createCommand($sql)->queryOne();

$getSite = isset($dataFileInput['hsitecode']);
$sitecode = Yii::$app->user->identity->userProfile->sitecode;
if(!$getSite || ($getSite && $getSite == $sitecode)){
    $fileModel = new \backend\modules\ezforms2\models\FileUploadSearch();
    $fileModel->ezf_id = $modelFields->ezf_id;
    $fileModel->ezf_field_id = $modelFields->ezf_field_id;
    $fileModel->tbid = $model->id;
    $fileModel->mode = '1';
    $dataProvider = $fileModel->search();

    //check file upload
if($dataProvider->models[0]->ezf_id) {
	$ezform = \backend\modules\ezforms\components\EzformQuery::getFormTableName($dataProvider->models[0]->ezf_id);
	$res = Yii::$app->db->createCommand("select rstat, xsourcex from `" . $ezform->ezf_table . "` where id = :id;", [':id' => $dataProvider->models[0]->tbid])->queryOne();
	$comp = \backend\modules\ezforms\components\EzformQuery::checkIsTableComponent($dataProvider->models[0]->ezf_id);
}
$visibleDel = true;
if($res['xsourcex'] == Yii::$app->user->identity->userProfile->sitecode){
	if($comp['special']){
		$visibleDel = $res['rstat'] == 0 || $res['rstat'] == 1 ? true : false;
	}else{
		$visibleDel = true;
	}
}else{
	$visibleDel = false;
}

\yii\widgets\Pjax::begin(['id'=>$modelFields->ezf_id.'-pjax-'.$modelFields->ezf_field_id]);
echo \yii\grid\GridView::widget([
    'id' => 'research-grid',
    'dataProvider' => $dataProvider,
    'layout' => "{items}\n{pager}",
    'columns' => [
	//['class' => 'yii\grid\SerialColumn'],
	[
	    'attribute'=>'file_name',
	    'header'=>'',
	    'value'=>function ($data){
		$img = Yii::getAlias('@web/fileinput').'/'.$data['file_name'];
		$img_old = $img;
		$ext = strtolower(pathinfo($data['file_name'], PATHINFO_EXTENSION));
		if($ext=='pdf'){
		    $img = Yii::getAlias('@storageUrl').'/source/pdf_icon.png';
		}
		return '<img class="file-preview-image" src="'.$img.'" data-filename="'.$img_old.'" height="100">';
	    },
	    'format' => 'raw',	    
	    'headerOptions'=>['style'=>'text-align: center;'],
	    'contentOptions'=>['style'=>'width:100px;text-align: center;'],
	],
	'file_name_old:ntext',
	[
	    'attribute'=>'created_at',
	    'value'=>function ($data){return common\lib\sdii\components\utils\SDdate::mysql2phpDate($data['created_at']);},
	    'headerOptions'=>['style'=>'text-align: center;'],
	    'contentOptions'=>['style'=>'width:100px;text-align: center;'],
	    'filter'=>'',
	],
	[
	    'class' => 'common\lib\sdii\widgets\SDActionColumn',
	    'template'=>'{delete}',
		'visible' => $visibleDel,
	    'buttons'=>[
		'delete' => function ($url, $data, $key) {
		    //if(Yii::$app->user->id==$data['created_by']){
			return Html::a('<span class="glyphicon glyphicon-trash"></span>', Url::to(['/ezforms2/fileinput/delete', 'id'=>$data['fid']]), [
			    'data-action' => 'delete',
			    'title' => Yii::t('yii', 'Delete'),
			    'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
			    'data-method' => 'post',
			]);
		    //}
		},
	    ],
	],
    ],
]);
\yii\widgets\Pjax::end();

$this->registerJs("
$('#".$modelFields->ezf_id.'-pjax-'.$modelFields->ezf_field_id."').on('click', 'tbody tr td a', function() {
    var url = $(this).attr('href');
    var action = $(this).attr('data-action');

    if(action === 'update' || action === 'clone'){
    } else if(action === 'delete') {
		yii.confirm('".Yii::t('app', 'Are you sure you want to delete this item?')."', function(){
			$.post(
				url
			).done(function(result){
				if(result.status == 'success'){
					". SDNoty::show('result.message', 'result.status') ."
					$.pjax.reload({container:'#".$modelFields->ezf_id.'-pjax-'.$modelFields->ezf_field_id."'});
				} else {
					". SDNoty::show('result.message', 'result.status') ."
				}
			}).fail(function(){
				". SDNoty::show("'" . "Server Error'", '"error"') ."
				console.log('server error');
			});
		})
    }
    return false;
});
");
?>

<?php } ?>