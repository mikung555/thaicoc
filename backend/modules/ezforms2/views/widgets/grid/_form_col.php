<?php
use yii\helpers\Html;

$key_item = common\lib\codeerror\helpers\GenMillisecTime::getMillisecTime();

?>
<th style="position:relative;">
    <?=  Html::textInput("data[builder][$id][fields][1_1][header][$key_item][label]", 'หัวข้อที่ '.$col, ['class'=>'form-control', 'id'=>"label_item_$key_item" , 'placeholder'=>'หัวข้อ'])?>
    <?=  Html::dropDownList("data[builder][$id][fields][1_1][header][$key_item][type]", 'textinput', ['textinput'=>'Text Input', 'textarea'=>'Textarea', 'datetime'=>'Date Time', 'checkbox'=>'Checkbox'],['class'=>'form-control ', 'id'=>"type_item_$key_item"])?>
    <?= Html::hiddenInput("data[builder][$id][fields][1_1][header][$key_item][col]", $col, ['class'=>'header_col'])?>
    <i class="fa fa-close del-items-col" data-id="<?=$id?>" data-item-id="<?=$key_item?>" data-col="<?=$col?>" data-var="<?=$model->ezf_field_name?>" style="position:absolute;right: 10px;top:10px;cursor:pointer;color:#9F9F9F;"></i>
</th>