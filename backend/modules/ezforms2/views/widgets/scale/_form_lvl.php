<?php
use yii\helpers\Html;

$key_item = common\lib\codeerror\helpers\GenMillisecTime::getMillisecTime();

?>
<th style="position:relative;">
    <?=  Html::textInput("data[builder][$id][fields][1_1][data][$key_item][value]", $col, ['class'=>'form-control', 'id'=>"value_item_$key_item" , 'placeholder'=>'ค่า'])?>
    <?=  Html::textInput("data[builder][$id][fields][1_1][data][$key_item][label]", 'ระดับ '.$col, ['class'=>'form-control ', 'id'=>"label_item_$key_item", 'placeholder'=>'ระดับ'])?>
    <i class="fa fa-close del-items-lvl" data-id="<?=$id?>" data-item-id="<?=$key_item?>" data-col="<?=$col?>" data-var="<?=$model->ezf_field_name?>" style="position:absolute;right: 10px;top:10px;cursor:pointer;color:#9F9F9F;"></i>
    <?= Html::hiddenInput("data[builder][$id][fields][1_1][data][$key_item][action]", 'create')?>
</th>