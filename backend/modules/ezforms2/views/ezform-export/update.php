<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\ezforms2\models\EzformExport */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Ezform Export',
]) . ' ' . $model->tbname;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Ezform Exports'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->tbname, 'url' => ['view', 'id' => $model->tbname]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="ezform-export-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
