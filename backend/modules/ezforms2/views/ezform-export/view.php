<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\ezforms2\models\EzformExport */

$this->title = 'Ezform Export#'.$model->tbname;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Ezform Exports'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ezform-export-view">

    <div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	<h4 class="modal-title" id="itemModalLabel"><?= Html::encode($this->title) ?></h4>
    </div>
    <div class="modal-body">
        <?= DetailView::widget([
	    'model' => $model,
	    'attributes' => [
		'tbname',
		'export_sql:ntext',
		'export_select:ntext',
		'export_join:ntext',
		'export_where:ntext',
		'lable_extra:ntext',
		'status',
	    ],
	]) ?>
    </div>
</div>
