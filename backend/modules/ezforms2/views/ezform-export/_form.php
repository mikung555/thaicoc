<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use appxq\sdii\helpers\SDNoty;
use appxq\sdii\helpers\SDHtml;

/* @var $this yii\web\View */
/* @var $model backend\modules\ezforms2\models\EzformExport */
/* @var $form yii\bootstrap\ActiveForm */
?>

<div class="ezform-export-form">

    <?php $form = ActiveForm::begin([
	'id'=>$model->formName(),
    ]); ?>

    <div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	<h4 class="modal-title" id="itemModalLabel">Ezform Export</h4>
    </div>

    <div class="modal-body">
	<?= $form->field($model, 'tbname')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'export_sql')->textarea(['rows' => 6]) ?>

	<?= $form->field($model, 'export_select')->textarea(['rows' => 6])->hint('ใช้เครื่องหมาย # เป็นตัวคั้นและไม่ต้องเว้นวรรค ตัวอย่าง <code>field1#field2...#fieldN</code>') ?>

	<?= $form->field($model, 'export_join')->textarea(['rows' => 6])->hint('ตัวอย่าง <code>type1#table1#on1@type2#table2#on2...@typeN#tableN#onN</code> หมายเหตุ type=ประเภทการเชื่อมตารางเช่น INNER JOIN, table=ชื่อตาราง, on=เงื่อนไขในการเชื่อมตาราง') ?>

	<?= $form->field($model, 'export_where')->textarea(['rows' => 6])->hint('เงื่อนไข SQL') ?>

	<?= $form->field($model, 'lable_extra')->textarea(['rows' => 6])->hint('ตัวอย่าง <code>Json {"ptid":"new ptid","name":"new name"}</code>') ?>

	<?= $form->field($model, 'status')->checkbox() ?>

    </div>
    <div class="modal-footer">
	<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	<?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php  $this->registerJs("
$('form#{$model->formName()}').on('beforeSubmit', function(e) {
    var \$form = $(this);
    $.post(
	\$form.attr('action'), //serialize Yii2 form
	\$form.serialize()
    ).done(function(result) {
	if(result.status == 'success') {
	    ". SDNoty::show('result.message', 'result.status') ."
	    if(result.action == 'create') {
		$(\$form).trigger('reset');
		$.pjax.reload({container:'#ezform-export-grid-pjax'});
	    } else if(result.action == 'update') {
		$(document).find('#modal-ezform-export').modal('hide');
		$.pjax.reload({container:'#ezform-export-grid-pjax'});
	    }
	} else {
	    ". SDNoty::show('result.message', 'result.status') ."
	} 
    }).fail(function() {
	". SDNoty::show("'" . SDHtml::getMsgError() . "Server Error'", '"error"') ."
	console.log('server error');
    });
    return false;
});

");?>