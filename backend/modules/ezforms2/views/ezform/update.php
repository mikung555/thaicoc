<?php

use Yii;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use common\lib\sdii\widgets\SDModalForm;
use common\lib\sdii\components\helpers\SDNoty;
use kartik\widgets\Select2;
use kartik\tree\TreeViewInput;
use backend\models\TblTree;

\backend\modules\ezforms2\assets\EzfAsset::register($this);

$this->title = Yii::t('backend', 'ฟอร์ม ', [
            'modelClass' => 'Ezform',
        ]) . ':: ' . $model->ezf_name;

$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Ezforms'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('backend', 'จัดการฟอร์ม');

?>

<div class="ezform-update" >
    <?= Html::a('<i class="fa fa-edit"></i>&nbsp;&nbsp;&nbsp;จัดการฟอร์ม', ['/ezforms2/ezform/update', 'id' => $ezf_id], ['class' => 'btn btn-info btn-flat  ']) ?>
    <?= Html::a('<i class="fa fa-comments-o"></i>&nbsp;&nbsp;&nbsp;เพิ่มคำถามพิเศษ', ['/component/ezform-component/index', 'id' => $ezf_id], ['class' => 'btn btn-primary btn-flat']) ?>
    <?= Html::a('<i class="fa fa-file-excel-o"></i>&nbsp;&nbsp;&nbsp;สร้าง EZForm จาก Excel', ['/file-upload', 'id' => $ezf_id], ['class' => 'btn btn-primary btn-flat']) ?>
    <?= Html::a('<i class="fa fa-globe"></i>&nbsp;&nbsp;&nbsp;ดูฟอร์มออนไลน์', ['/ezforms2/ezform/viewform', 'id' => $ezf_id], ['class' => 'btn btn-primary btn-flat']) ?>
    <?= Html::a('<i class="fa fa-table"></i>&nbsp;&nbsp;&nbsp;ดูข้อมูล/ส่งออกข้อมูล', ['/managedata/managedata/view-data', 'id' => $ezf_id], ['class' => 'btn btn-primary btn-flat']) ?>
    <div style='float:right'>
        <?= Html::a('<i class="fa fa-mail-reply"></i>&nbsp;&nbsp;&nbsp;กลับไปหน้าเลือกฟอร์ม', ['/ezforms/ezform/', 'id' => $ezf_id], ['class' => 'btn btn-warning btn-flat']) ?>
    </div>
    <br>
    <br>
    
            <?php $form = ActiveForm::begin(['id' => $model->formName()]); ?>
            <div class='row'>
                <div class='col-lg-6'>
		    <?php
	    ?>
                    <?= Html::activeHiddenInput($model, 'ezf_id'); ?>
                    <?=
                            $form->field($model, 'ezf_name')
                            ->textInput([
                                'maxlength' => true,
//                                'onblur' => "autoSave('ezf_name','" . $model1->ezf_id . "')",
                    ]);
                    ?>
                </div>
                <div class='col-lg-6'>
                    <?php
                    
                    echo $form->field($model, 'user_create')
                            ->textInput([
                                'maxlength' => true,
//                                'onblur'=>"autoSave($(this).val(),'user_create','".$model1->ezf_id."')",
                                'disabled' => true,
                                'value' => $userprofile->firstname . ' ' . $userprofile->lastname,
                    ]);
                    ?>
                </div>
            </div>
       
	    
            <div class='row'>
        <div class='col-lg-12'>
            <p style='cursor:pointer' id='btnSettingGeneral' class="text-left"><i class='fa fa-cog btn-default'></i> แสดง/ซ่อน การตั้งค่าทั่วไป </p>
        </div>
    </div>

     <div class="box box-primary" id='settingGeneral' style='display:none;'>
        <div class="box-body">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#setting1" aria-controls="setting1" role="tab" data-toggle="tab">ตั้งค่าทั่วไป</a></li>
                <li role="presentation"><a href="#setting2" aria-controls="setting2" role="tab" data-toggle="tab">คุณสมบัติ</a></li>
                <li role="presentation"><a href="#setting3" aria-controls="setting3" role="tab" data-toggle="tab">Code เสริม</a></li>
                <li role="presentation"><a href="#setting4" aria-controls="setting4" role="tab" data-toggle="tab">TDC Mapping</a></li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="setting1" style="padding-top: 10px;">

                    <div class="row">
                        <div class="col-md-6">
                            <?= $form->field($model, 'comp_id_target')->widget(Select2::classname(), [
                                'data' => $dataComponent,
                                'options' => ['placeholder' => 'กรุณาเลือกคอมโพเนนท์...'],
                                'pluginOptions' => [
                                    'allowClear' => true,
                                    'multiple' => false,
                                ],
                            ]);
                            ?>
                        </div>

                        <div class="col-md-6">
                            <?= $form->field($model, 'category_id')->widget(TreeViewInput::classname(), [
                                'name' => 'category_id',
                                'id' => 'category_id',
                                'query' => TblTree::find()->where('readonly=1 or userid='.Yii::$app->user->id.' or id IN (select distinct root from tbl_tree where userid='.Yii::$app->user->id.')')->addOrderBy('root, lft'),
                                'headingOptions'=>['label'=>'Categories'],
                                'asDropdown' => true,
                                'multiple' => false,
                                'fontAwesome' => true,
                                'rootOptions' => [
                                    'label'=>'<i class="fa fa-home"></i> '.$siteconfig->sitename,
                                    'class'=>'text-success',
                                    'options' => ['disabled' => false]
                                ],
                            ])
                            ?>
                        </div>
                    </div>

                    <div class="row">
                        <div class='col-lg-6'>
                            <?=
                            $form->field($model, 'ezf_detail')->textarea(['rows' => 7,
                                //    'onblur' => "autoSave($(this).val(),'ezf_detail','" . $model1->ezf_id . "')"
                            ]);
                            ?>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <?php
                                $items = [
                                    '0' => 'ส่วนตัว (Private)',
                                    '2' => 'ระบุคน (Assign)',
                                    '3' => 'ทุกคนใน Site นั้น',
                                    '1' => 'สาธารณะ (Public)',
                                ];
                                echo $form->field($model, 'shared')->radioList($items);
                                ?>
                                <div id="ezform-shared_assign" style="display: <?php echo $model->shared==2 ? ';' : 'none;'; ?>">
                                    <?= $form->field($model, 'assign')->widget(Select2::className(),[
                                        //'initValueText' => $model1->assign, // set the initial display text
                                        'data' => $userlist,
                                        'options' => ['placeholder' => 'กรุณาเลือกผู้ใช้ฟอร์มนี้...', 'multiple' => true],
                                        'pluginOptions' => [
                                            'allowClear' => true,
                                            'tags' => true,
                                            'tokenSeparators' => [',', ' '],
                                        ]
                                    ]) ?>
                                </div>
                            </div>
                            <label>สิทธิในการใช้ฟอร์ม</label><br>
                            <?php
                            $items = [
                                '1' => 'อนุญาตให้ view List Data table ของฟอร์ม',
                                '2' => 'อนุญาตให้ Edit/Update List Data table ของฟอร์ม',
                                '3' => 'อนุญาตให้ Remove List Data table ของฟอร์ม',
                            ];
                            echo \yii\bootstrap\BaseHtml::checkbox('public_listview', ($model->public_listview == 1 ? true : false), ['label' =>$items['1'], 'id'=>'public_listview', 'value' => '1']).'<br>';
                            echo \yii\bootstrap\BaseHtml::checkbox('public_edit', ($model->public_edit == 1 ? true : false), ['label' =>$items['2'], 'id'=>'public_edit', 'value' => '1']).'<br>';
                            echo \yii\bootstrap\BaseHtml::checkbox('public_delete', ($model->public_delete == 1 ? true : false), ['label' =>$items['2'], 'id'=>'public_delete', 'value' => '1']).'<br>';
                            ?>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <?php

                            $arrCoDev = yii\helpers\ArrayHelper::getColumn($modelCoDevs, 'user_co');
			    
                            echo '<label>ผู้สร้างร่วม</label>';
                            echo Select2::widget([
                                'name' => 'ezform-co_dev',
                                'value' => $arrCoDev, // initial value
                                'data' => $userlist,
                                'options' => ['placeholder' => 'Select a color ...', 'multiple' => true, 'class' => 'form-control ezform-co_dev',],
                                'pluginOptions' => [
                                    'tags' => true,
                                    'tokenSeparators' => [',', ' '],
                                ],
                            ]);
                            ?>

                        </div>

                        <div class="col-md-6">
                            <?php
                            echo $form->field($model, 'field_detail')->widget(Select2::classname(), [
                                'data' => \yii\helpers\ArrayHelper::map($modelFields,'ezf_field_name','ezf_field_label'),
                                'options' => ['placeholder' => 'กรุณาเลือกตัวแปร...'],
                                'pluginOptions' => [
                                    'allowClear' => true,
                                    'multiple' => true,
                                ],
                            ]);
                            ?>
                        </div>
                    </div>

                </div>
                <div role="tabpanel" class="tab-pane" id="setting2" style="padding-top: 10px;">
                    <div class="row">
                        <div class="col-md-4">
                            <?php $model->query_tools =='' ? $model->query_tools=1 : null; echo $form->field($model, 'query_tools')->radioList(['1' => 'Disable', '2'=>'Enable'], ['id'=>'ezform-query_tools']); ?>
                        </div>
                        <div class="col-md-4">
                            <?php $model->unique_record =='' ? $model->unique_record=1 : null; echo $form->field($model, 'unique_record')->radioList(['1' => 'Disable', '2'=>'Enable'], ['id'=>'ezform-unique_record'])->label('Unique Record (patient mode)'); ?>
                        </div>
                        <div class="col-md-4">
                            <?php $model->reply_tools =='' ? $model->reply_tools=1 : null; echo $form->field($model, 'reply_tools')->radioList(['1' => 'Disable', '2'=>'Enable'],
                                [
                                    'id'=>'ezform-reply_tools',
                                    'onchange'=>'$(\'#reply_tools_setting\').toggle();'
                                ])->label('Consult Tools');?>
                            <div id="reply_tools_setting" style="display: <?php echo $model->reply_tools==2 ? ';' : 'none;'; ?>">
                                <?=$form->field($model, 'telegram')->textInput()->label('Telegram group');?>
                                <?php
                                $model->consultant_users = explode(',', $model->consultant_users);
                                echo $form->field($model, 'consultant_users')->widget(Select2::className(),[
                                    'id' => 'select-consultant-users',
                                    'data' => $userlist,
                                    'options' => ['placeholder' => 'เลือกผู้ดูแลฟอร์มนี้...', 'multiple' => true],
                                    'pluginOptions' => [
                                        'allowClear' => true,
                                        'tags' => true,
                                        'tokenSeparators' => [',', ' '],
                                    ]
                                ]); ?>
                            </div>
                        </div>
                    </div>

                </div>
                <div role="tabpanel" class="tab-pane" id="setting3" style="padding-top: 10px;">
                    <?php if(Yii::$app->user->can('administrator')) { ?>
                        <div class="row">
                            <div class="col-md-6 ">
                                <?= $form->field($model, 'sql_condition')->textarea(['rows'=>20]) ?>
                            </div>
                            <div class="col-md-6 sdbox-col">
                                <?= $form->field($model, 'js')->textarea(['rows'=>20]) ?>
                            </div>
                            <div class="col-md-6 sdbox-col">
                                <?= $form->field($model, 'sql_announce')->textarea(['rows'=>20]) ?>
                            </div>
                        </div>
                    <?php }else{ ?>
                        <div class="row">
                            <div class="col-md-6 ">
                                &nbsp;
                            </div>
                            <div class="col-md-6 sdbox-col">
                                <?= $form->field($model, 'js')->textarea() ?>
                            </div>
                        </div>
                    <?php } ?>
                </div>

                <div role="tabpanel" class="tab-pane" id="setting4" style="padding-top: 10px;">
                    <?php
                    echo Html::label('ขั้นตอนที่ 1 เลือก Tables จาก TDC');
                    echo Select2::widget([
                        'id' => 'tcc-bot-mapping',
                        'name' => 'tcc-bot-mapping',
                        'data' => $tccTables,
                        'options' => [
                            'placeholder' => 'Please select tables from TDC',
                        ],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]);

                    echo '<br>';
                    echo Html::label('ขั้นตอนที่ 2 Mapping Fields ระหว่าง TDC กับ EzForm');
                    echo '<div id="div-tcc-bot-mapping"></div>';
                    ?>
                </div>
            </div>


	    </div>
    </div>


<?php ActiveForm::end(); ?>

<?= $this->render('_ezf_editor', [
	'model' => $model,
	'ezf_id' => $ezf_id,
	'modelFields' => $modelFields,
	'modelComponents' => $modelComponents,
	'dataComponent' => $dataComponent,
	'userlist' => $userlist,
	'modelCoDevs' => $modelCoDevs,
	'modelDynamic' => $modelDynamic,
	'tccTables' => $tccTables,
	'userprofile'=>$userprofile,
	'siteconfig'=>$siteconfig,
]);?>
    
</div>


<?= SDModalForm::widget([
    'id' => 'modal-ezform',
    'size' => 'modal-lg',
    'tabindexEnable' => false,
]);
?>

<?= SDModalForm::widget([
    'id' => 'modal-condition',
    'size'=>'modal-sm',
]);
?>

<?= SDModalForm::widget([
    'id' => 'modal-gridtype',
    'size'=>'modal-md',
]);
?>

<?php
// AUTO Save EzForm 
$this->registerJs("
$('#settingGeneral').hide();
$('#btnSettingGeneral').click(function(){

    $('#settingGeneral').toggle();
});

$('#modal-ezform').on('hidden.bs.modal', function (e) {
    $('.sp-container').remove();
    $('.redactor-toolbar-tooltip').remove();
})

function modalEzform(url) {
    $('#modal-ezform .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
    $('#modal-ezform').modal('show')
    .find('.modal-content')
    .load(url);
}

function modalCondition(url) {
    $('#modal-condition .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
    $('#modal-condition').modal('show')
    .find('.modal-content')
    .load(url);
}

function modalGridtype(url) {
    $('#modal-gridtype .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
    $('#modal-gridtype').modal('show')
    .find('.modal-content')
    .load(url);
}

$(\"#ezform-query_tools\").on('change', function() {
    var status = $('input[name=\"Ezform[query_tools]\"]:checked').val();
    var id = $('#ezform-ezf_id').val();
    $.post('".Url::to(['/ezforms/ezform/use-ezform-option'])."',{status:status, id:id, action: 'query_tools'},function(result){
        " . SDNoty::show('result.message', 'result.status') . "
    });
});
$(\"#ezform-unique_record\").on('change', function() {
    var status = $('input[name=\"Ezform[unique_record]\"]:checked').val();
    var id = $('#ezform-ezf_id').val();
    $.post('".Url::to(['/ezforms/ezform/use-ezform-option'])."',{status:status, id:id,  action: 'unique_record'},function(result){
        " . SDNoty::show('result.message', 'result.status') . "
    });
});
$(\"#ezform-reply_tools\").on('change', function() {
    var status = $('input[name=\"Ezform[reply_tools]\"]:checked').val();
    var id = $('#ezform-ezf_id').val();
    $.post('".Url::to(['/ezforms/ezform/use-ezform-option'])."',{status:status, id:id,  action: 'reply_tools'},function(result){
        " . SDNoty::show('result.message', 'result.status') . "
    });
});

$(\"input[name='Ezform[shared]']\").on('change',function(){
    var shared = $(\"input[name='Ezform[shared]']:checked\").val();
    if(shared==2){
        $('#ezform-shared_assign').toggle();
    }else $('#ezform-shared_assign').hide();
    var id = $('#ezform-ezf_id').val();
    $.post('".Url::to(['/ezforms/ezform/updateformstatus'])."',{shared:shared,id:id},function(result){
        " . SDNoty::show('result.message', 'result.status') . "
    });
});
$('#ezform-ezf_name').on('blur',function(){
    var value = $(this).val();
    var attribute = $('#ezform-ezf_name').attr('id');
    var field_id = $('#ezform-ezf_id').val();
    $.post('".Url::to(['/ezforms/ezform/formautosave'])."', {
            value: value,
            field: attribute,
            id: field_id
        }, function (result) {
            var noty_id = noty({'text': result.message, 'type': 'success'});
         });
});

$('#ezform-sql').on('blur',function(){
    var value = $(this).val();
    var attribute = $('#ezform-sql').attr('id');
    var field_id = $('#ezform-ezf_id').val();
    $.post('".Url::to(['/ezforms/ezform/formautosave'])."', {
            value: value,
            field: attribute,
            id: field_id
        }, function (result) {
            var noty_id = noty({'text': result.message, 'type': 'success'});
         });
});
$('#ezform-js').on('blur',function(){
    var value = $(this).val();
    var attribute = $('#ezform-js').attr('id');
    var field_id = $('#ezform-ezf_id').val();
    $.post('".Url::to(['/ezforms/ezform/formautosave'])."', {
            value: value,
            field: attribute,
            id: field_id
        }, function (result) {
            var noty_id = noty({'text': result.message, 'type': 'success'});
         });
});

$('#ezform-ezf_detail').on('blur',function(){
    var value = $(this).val();
    var attribute = $('#ezform-ezf_detail').attr('id');
    var field_id = $('#ezform-ezf_id').val();
    $.post('".Url::to(['/ezforms/ezform/formautosave'])."', {
            value: value,
            field: attribute,
            id: field_id
        }, function (result) {
            var noty_id = noty({'text': result.message, 'type': 'success'});
         });
});

$('#tcc-bot-mapping').on('change',function(){

    var val = $(this).val();
    var ezf_id = $('#ezform-ezf_id').val();

    $.post('".Url::to(['/ezforms/ezform/tcc-bot-mapping',])."',{val:val, ezf_id:ezf_id},function(result){
        $('#div-tcc-bot-mapping').html(result);
    });
});

$('#ezform-consultant_users').on('change',function(){

    var val = $(this).val();
    var ezf_id = $('#ezform-ezf_id').val();
    $.post('".Url::to(['/ezforms/ezform/save-consultant', 'type'=>'consultant'])."',{val:val, ezf_id:ezf_id},function(result){
        " . SDNoty::show('result.message', 'result.status') . "
    });
});

$('#ezform-telegram').on('change',function(){

    var val = $(this).val();
    var ezf_id = $('#ezform-ezf_id').val();
    $.post('".Url::to(['/ezforms/ezform/save-consultant', 'type'=>'telegram'])."',{val:val, ezf_id:ezf_id},function(result){
        " . SDNoty::show('result.message', 'result.status') . "
    });
});

$('#ezform-assign').on('change',function(){

    var val = $(this).val();
    var ezf_id = $('#ezform-ezf_id').val();
    $.post('".Url::to(['/ezforms/ezform/saveassign'])."',{val:val, ezf_id:ezf_id},function(result){
        " . SDNoty::show('result.message', 'result.status') . "
    });
});

$('#ezform-comp_id_target').on('change',function(){
    var ezf_id = $('#ezform-ezf_id').val();
    $.post('".Url::to(['/ezforms/ezform/update-component-target'])."',
        {
            form_id: ezf_id,
            comp_id: this.value
        },
        function(result){
            " . SDNoty::show('result.message', 'result.status') . "
        });
});

$( '#category_id' ).change(function() {
        var ezf_id = $('#ezform-ezf_id').val();
        $.post('".Url::to(['/ezforms/ezform/update-category'])."',
        {
            form_id: ezf_id,
            category_id: this.value
        }, function(result){" . SDNoty::show('result.message', 'result.status') . "});
});

$('#ezform-field_detail').on('change',function(){
    var val = $(this).val();
    var ezf_id = $('#ezform-ezf_id').val();
    $.post('".Url::to(['/ezforms/ezform/savefielddetail'])."',{val:val, ezf_id:ezf_id},function(result){
        " . SDNoty::show('result.message', 'result.status') . "
    });
});

$('.ezform-co_dev').on('change',function(){

    var field_id = '';

    $.each( $( '.ezform-co_dev' ).select2('data'), function( key, value ) {
      field_id += value.id + ',';
    });

    var val = $(this).val();
    var ezf_id = $('#ezform-ezf_id').val();
    $.post('".Url::to(['/ezforms/ezform/save-co-dev'])."',{val:field_id.substring(0, (field_id.length - 1)), ezf_id:ezf_id},function(result){
        " . SDNoty::show('result.message', 'result.status') . "
    });
});
");

?>

<?php 
// set var top
$this->registerJs(" 
	var baseUrlTo = '".Url::to(['/ezforms2'])."';
", 1);
?>

<?php 
// set var end
$this->registerJs("
	var eid = '".$ezf_id."';
	var baseUrl = '".Url::to(['/ezforms2/ezform-condition'])."';
	var conditionUrl = '".Url::to(['/ezforms2/ezform-condition/condition'])."';
	var fieldsUrl = '".Url::to(['/ezforms2/ezform-condition/fields', 'id'=>$_GET['id']])."';
", 3);


?>
