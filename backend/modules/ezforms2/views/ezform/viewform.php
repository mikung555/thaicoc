<?php
use Yii;
use backend\modules\ezforms2\classes\EzActiveForm;
use yii\helpers\Html;
use backend\modules\ezforms2\classes\EzfFunc;

/**
 * viewform file UTF-8
 * @author SDII <iencoded@gmail.com>
 * @copyright Copyright &copy; 2015 AppXQ
 * @license http://www.appxq.com/license/
 * @version 1.0.0 Date: 1 ก.ย. 2559 10:41:10
 * @link http://www.appxq.com/
 */

$this->title = Yii::t('backend', 'ฟอร์ม ', [
            'modelClass' => 'Ezform',
        ]) . ':: ' . $model->ezf_name;

$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Ezforms'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('backend', 'ดูฟอร์มออนไลน์');

\backend\modules\ezforms2\assets\EzfAsset::register($this);
\backend\modules\ezforms2\assets\EzfGenAsset::register($this);
?>
    
    <?= Html::a('<i class="fa fa-edit"></i>&nbsp;&nbsp;&nbsp;จัดการฟอร์ม', ['/ezforms2/ezform/update', 'id' => $ezf_id], ['class' => 'btn btn-primary btn-flat  ']) ?>
    <?= Html::a('<i class="fa fa-comments-o"></i>&nbsp;&nbsp;&nbsp;เพิ่มคำถามพิเศษ', ['/component/ezform-component/index', 'id' => $ezf_id], ['class' => 'btn btn-primary btn-flat']) ?>
    <?= Html::a('<i class="fa fa-file-excel-o"></i>&nbsp;&nbsp;&nbsp;สร้าง EZForm จาก Excel', ['/file-upload', 'id' => $ezf_id], ['class' => 'btn btn-primary btn-flat']) ?>
    <?= Html::a('<i class="fa fa-globe"></i>&nbsp;&nbsp;&nbsp;ดูฟอร์มออนไลน์', ['/ezforms2/ezform/viewform', 'id' => $ezf_id], ['class' => 'btn btn-info btn-flat']) ?>
    <?= Html::a('<i class="fa fa-table"></i>&nbsp;&nbsp;&nbsp;ดูข้อมูล/ส่งออกข้อมูล', ['/managedata/managedata/view-data', 'id' => $ezf_id], ['class' => 'btn btn-primary btn-flat']) ?>
    <div style='float:right'>
        <?= Html::a('<i class="fa fa-mail-reply"></i>&nbsp;&nbsp;&nbsp;กลับไปหน้าเลือกฟอร์ม', ['/ezforms/ezform/', 'id' => $ezf_id], ['class' => 'btn btn-warning btn-flat']) ?>
    </div>
    <br>
    <br>
    
<div class="ezform-box">
    <?php $form = EzActiveForm::begin([
	//'action' => '/ezforms/ezform/savedata', 
	'options' => [
	    'enctype' => 'multipart/form-data'
	]
    ]); ?>
    <div class="modal-header">
	<h3 class="modal-title" id="itemModalLabel"><?=$model->ezf_name?> <small><?=$model->ezf_detail?></small></h3>
    </div>
    <div class="modal-body">
	<div id="formPanel" class="row">
	    <?php
	    foreach ($modelFields as $field) {
		if($field['ezf_field_ref']==NULL || empty($field['ezf_field_ref'])){
                    echo EzfFunc::generateInput($form, $modelTable, $field);
                    if($field['ezf_condition']==1){
                        EzfFunc::generateCondition($modelTable, $field, $model, $this);
                    }
                }
	    }
	    ?>
	</div>
    </div>
    <div class="modal-footer">
	<?= Html::submitButton(Yii::t('app', 'บันทึก'), ['class' => 'btn btn-primary']) ?>
	<?= Html::resetButton(Yii::t('app', 'รีเซ็ต'), ['class' => 'btn btn-default']) ?>
    </div>
    <?php EzActiveForm::end(); ?>
</div>



