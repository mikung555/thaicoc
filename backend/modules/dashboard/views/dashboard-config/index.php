<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\helpers\Url;
use appxq\sdii\widgets\GridView;
use appxq\sdii\widgets\ModalForm;
use appxq\sdii\helpers\SDNoty;
use appxq\sdii\helpers\SDHtml;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\dashboard\models\DashboardConfigSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Dashboard Configs');
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="dashboard-config-index">

    
    <?php  Pjax::begin(['id'=>'dashboard-config-grid-pjax']);?>
    <?= GridView::widget([
	'id' => 'dashboard-config-grid',
	'panelBtn' => Html::button(SDHtml::getBtnAdd(), ['data-url'=>Url::to(['dashboard-config/create']), 'class' => 'btn btn-success btn-sm', 'id'=>'modal-addbtn-dashboard-config']). ' ' .
		      Html::button(SDHtml::getBtnDelete(), ['data-url'=>Url::to(['dashboard-config/deletes']), 'class' => 'btn btn-danger btn-sm', 'id'=>'modal-delbtn-dashboard-config', 'disabled'=>true]),
	'dataProvider' => $dataProvider,
	'filterModel' => $searchModel,
        'columns' => [
	    [
		'class' => 'yii\grid\CheckboxColumn',
		'checkboxOptions' => [
		    'class' => 'selectionDashboardConfigIds'
		],
		'headerOptions' => ['style'=>'text-align: center;'],
		'contentOptions' => ['style'=>'width:40px;text-align: center;'],
	    ],
	    [
		'class' => 'yii\grid\SerialColumn',
		'headerOptions' => ['style'=>'text-align: center;'],
		'contentOptions' => ['style'=>'width:60px;text-align: center;'],
	    ],

            'dash_id',
            'dash_name',
            'dash_color',
            'dash_public',
            'dash_share:ntext',
            // 'dash_active',
            // 'dash_approved',
            // 'created_by',
            // 'created_at',
            // 'updated_by',
            // 'updated_at',
            // 'sitecode',
            // 'ezf_id',
            // 'ezf_name',
            // 'ezf_table',
            // 'comp_id',
            // 'pk_field',
            // 'special',
            // 'ezform_config:ntext',
            // 'dash_js:ntext',
            // 'date_config',
            // 'search_config:ntext',
            // 'event_config:ntext',

	    [
		'class' => 'appxq\sdii\widgets\ActionColumn',
		'contentOptions' => ['style'=>'width:80px;text-align: center;'],
		'template' => '{view} {update} {delete}',
	    ],
        ],
    ]); ?>
    <?php  Pjax::end();?>

</div>

<?=  ModalForm::widget([
    'id' => 'modal-dashboard-config',
    'size'=>'modal-lg',
]);
?>

<?php  $this->registerJs("
$('#dashboard-config-grid-pjax').on('click', '#modal-addbtn-dashboard-config', function() {
    modalDashboardConfig($(this).attr('data-url'));
});

$('#dashboard-config-grid-pjax').on('click', '#modal-delbtn-dashboard-config', function() {
    selectionDashboardConfigGrid($(this).attr('data-url'));
});

$('#dashboard-config-grid-pjax').on('click', '.select-on-check-all', function() {
    window.setTimeout(function() {
	var key = $('#dashboard-config-grid').yiiGridView('getSelectedRows');
	disabledDashboardConfigBtn(key.length);
    },100);
});

$('#dashboard-config-grid-pjax').on('click', '.selectionDashboardConfigIds', function() {
    var key = $('input:checked[class=\"'+$(this).attr('class')+'\"]');
    disabledDashboardConfigBtn(key.length);
});

$('#dashboard-config-grid-pjax').on('dblclick', 'tbody tr', function() {
    var id = $(this).attr('data-key');
    modalDashboardConfig('".Url::to(['dashboard-config/update', 'id'=>''])."'+id);
});	

$('#dashboard-config-grid-pjax').on('click', 'tbody tr td a', function() {
    var url = $(this).attr('href');
    var action = $(this).attr('data-action');

    if(action === 'update' || action === 'view') {
	modalDashboardConfig(url);
    } else if(action === 'delete') {
	yii.confirm('".Yii::t('app', 'Are you sure you want to delete this item?')."', function() {
	    $.post(
		url
	    ).done(function(result) {
		if(result.status == 'success') {
		    ". SDNoty::show('result.message', 'result.status') ."
		    $.pjax.reload({container:'#dashboard-config-grid-pjax'});
		} else {
		    ". SDNoty::show('result.message', 'result.status') ."
		}
	    }).fail(function() {
		". SDNoty::show("'" . SDHtml::getMsgError() . "Server Error'", '"error"') ."
		console.log('server error');
	    });
	});
    }
    return false;
});

function disabledDashboardConfigBtn(num) {
    if(num>0) {
	$('#modal-delbtn-dashboard-config').attr('disabled', false);
    } else {
	$('#modal-delbtn-dashboard-config').attr('disabled', true);
    }
}

function selectionDashboardConfigGrid(url) {
    yii.confirm('".Yii::t('app', 'Are you sure you want to delete these items?')."', function() {
	$.ajax({
	    method: 'POST',
	    url: url,
	    data: $('.selectionDashboardConfigIds:checked[name=\"selection[]\"]').serialize(),
	    dataType: 'JSON',
	    success: function(result, textStatus) {
		if(result.status == 'success') {
		    ". SDNoty::show('result.message', 'result.status') ."
		    $.pjax.reload({container:'#dashboard-config-grid-pjax'});
		} else {
		    ". SDNoty::show('result.message', 'result.status') ."
		}
	    }
	});
    });
}

function modalDashboardConfig(url) {
    $('#modal-dashboard-config .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
    $('#modal-dashboard-config').modal('show')
    .find('.modal-content')
    .load(url);
}

");?>