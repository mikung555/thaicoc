<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use appxq\sdii\helpers\SDNoty;
use appxq\sdii\helpers\SDHtml;

/* @var $this yii\web\View */
/* @var $model backend\modules\dashboard\models\DashboardConfig */
/* @var $form yii\bootstrap\ActiveForm */
backend\modules\ezforms2\assets\EzfColorInputAsset::register($this);

?>

<div class="dashboard-config-form">

    <?php $form = ActiveForm::begin([
	'id'=>$model->formName(),
    ]); ?>

    <div class="modal-header">
	<!--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>-->
	<h4 class="modal-title" id="itemModalLabel">Dashboard Config</h4>
    </div>

    <div class="modal-body">
	<div class="row">
	    <div class="col-md-6"><?= $form->field($model, 'dash_name')->textInput(['maxlength' => true]) ?></div>
	    <div class="col-md-6"><?= $form->field($model, 'ezf_id')->textInput() ?></div>
	</div>
	<div class="row">
	    <div class="col-md-6">
		<?php
	$userlist = \backend\modules\ezforms\components\EzformQuery::getIntUserAll(); //explode(",", $model1->assign);
	?>
	<?= $form->field($model, 'dash_share')->widget(\kartik\select2\Select2::className(), [
	    'options' => ['placeholder' => 'แชร์', 'multiple' => true],
	    'data' => \yii\helpers\ArrayHelper::map($userlist, 'id', 'text'),
	    'pluginOptions' => [
		    'tags' => true,
		    'tokenSeparators' => [',', ' '],
	    ],
	]) ?>
		
	    </div>
	    <div class="col-md-3"><?= $form->field($model, 'dash_public')->checkbox() ?></div>
	    <div class="col-md-3"><?= $form->field($model, 'dash_color')->textInput(['maxlength' => true]) ?></div>
	</div>
	
	


	<?= $form->field($model, 'dash_active')->hiddenInput()->label(FALSE) ?>

	<?= $form->field($model, 'dash_approved')->hiddenInput()->label(FALSE) ?>

	<?= $form->field($model, 'created_by')->hiddenInput()->label(FALSE) ?>

	<?= $form->field($model, 'created_at')->hiddenInput()->label(FALSE) ?>

	<?= $form->field($model, 'updated_by')->hiddenInput()->label(FALSE) ?>

	<?= $form->field($model, 'updated_at')->hiddenInput()->label(FALSE) ?>

	<?= $form->field($model, 'sitecode')->hiddenInput()->label(FALSE) ?>

	<?= $form->field($model, 'ezf_name')->hiddenInput()->label(FALSE) ?>

	<?= $form->field($model, 'ezf_table')->hiddenInput()->label(FALSE) ?>

	<?= $form->field($model, 'comp_id')->hiddenInput()->label(FALSE) ?>

	<?= $form->field($model, 'pk_field')->hiddenInput()->label(FALSE) ?>

	<?= $form->field($model, 'special')->hiddenInput()->label(FALSE) ?>

	<?= $form->field($model, 'ezform_config')->hiddenInput()->label(FALSE) ?>

	<?= $form->field($model, 'dash_js')->hiddenInput()->label(FALSE) ?>

	<?= $form->field($model, 'date_config')->hiddenInput()->label(FALSE) ?>

	<?= $form->field($model, 'search_config')->hiddenInput()->label(FALSE) ?>

	<?= $form->field($model, 'event_config')->hiddenInput()->label(FALSE) ?>

    </div>
    <div class="modal-footer">
	<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	<?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php  $this->registerJs("
var color_options = {
    showInput: true,
    showInitial:true,
    allowEmpty:true,
    showPalette:true,
    showSelectionPalette:true,
    hideAfterPaletteSelect:true,
    showAlpha:false,
    preferredFormat:'hex',
    palette: [
        ['#000','#444','#666','#999','#ccc','#eee','#f3f3f3','#fff'],
        ['#f00','#f90','#ff0','#0f0','#0ff','#00f','#90f','#f0f'],
        ['#f4cccc','#fce5cd','#fff2cc','#d9ead3','#d0e0e3','#cfe2f3','#d9d2e9','#ead1dc'],
        ['#ea9999','#f9cb9c','#ffe599','#b6d7a8','#a2c4c9','#9fc5e8','#b4a7d6','#d5a6bd'],
        ['#e06666','#f6b26b','#ffd966','#93c47d','#76a5af','#6fa8dc','#8e7cc3','#c27ba0'],
        ['#c00','#e69138','#f1c232','#6aa84f','#45818e','#3d85c6','#674ea7','#a64d79'],
        ['#900','#b45f06','#bf9000','#38761d','#134f5c','#0b5394','#351c75','#741b47'],
        ['#600','#783f04','#7f6000','#274e13','#0c343d','#073763','#20124d','#4c1130']
    ]
};

$('#dashboardconfig-dash_color').spectrum(color_options);

$('form#{$model->formName()}').on('beforeSubmit', function(e) {
    var \$form = $(this);
    $.post(
	\$form.attr('action'), //serialize Yii2 form
	\$form.serialize()
    ).done(function(result) {
	if(result.status == 'success') {
	    ". SDNoty::show('result.message', 'result.status') ."
	    if(result.action == 'create') {
		$(\$form).trigger('reset');
		$.pjax.reload({container:'#dashboard-config-grid-pjax'});
	    } else if(result.action == 'update') {
		$(document).find('#modal-dashboard-config').modal('hide');
		$.pjax.reload({container:'#dashboard-config-grid-pjax'});
	    }
	} else {
	    ". SDNoty::show('result.message', 'result.status') ."
	} 
    }).fail(function() {
	". SDNoty::show("'" . SDHtml::getMsgError() . "Server Error'", '"error"') ."
	console.log('server error');
    });
    return false;
});

");?>