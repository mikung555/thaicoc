<?php

namespace backend\modules\dashboard\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\dashboard\models\DashboardConfig;

/**
 * DashboardConfigSearch represents the model behind the search form about `backend\modules\dashboard\models\DashboardConfig`.
 */
class DashboardConfigSearch extends DashboardConfig
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['dash_id', 'dash_public', 'dash_active', 'dash_approved', 'created_by', 'updated_by', 'ezf_id', 'comp_id', 'special', 'date_config'], 'integer'],
            [['dash_name', 'dash_color', 'dash_share', 'created_at', 'updated_at', 'sitecode', 'ezf_name', 'ezf_table', 'pk_field', 'ezform_config', 'dash_js', 'search_config', 'event_config'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DashboardConfig::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'dash_id' => $this->dash_id,
            'dash_public' => $this->dash_public,
            'dash_active' => $this->dash_active,
            'dash_approved' => $this->dash_approved,
            'created_by' => $this->created_by,
            'created_at' => $this->created_at,
            'updated_by' => $this->updated_by,
            'updated_at' => $this->updated_at,
            'ezf_id' => $this->ezf_id,
            'comp_id' => $this->comp_id,
            'special' => $this->special,
            'date_config' => $this->date_config,
        ]);

        $query->andFilterWhere(['like', 'dash_name', $this->dash_name])
            ->andFilterWhere(['like', 'dash_color', $this->dash_color])
            ->andFilterWhere(['like', 'dash_share', $this->dash_share])
            ->andFilterWhere(['like', 'sitecode', $this->sitecode])
            ->andFilterWhere(['like', 'ezf_name', $this->ezf_name])
            ->andFilterWhere(['like', 'ezf_table', $this->ezf_table])
            ->andFilterWhere(['like', 'pk_field', $this->pk_field])
            ->andFilterWhere(['like', 'ezform_config', $this->ezform_config])
            ->andFilterWhere(['like', 'dash_js', $this->dash_js])
            ->andFilterWhere(['like', 'search_config', $this->search_config])
            ->andFilterWhere(['like', 'event_config', $this->event_config]);

        return $dataProvider;
    }
}
