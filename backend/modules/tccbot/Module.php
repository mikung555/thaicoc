<?php

namespace backend\modules\tccbot;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\modules\tccbot\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
