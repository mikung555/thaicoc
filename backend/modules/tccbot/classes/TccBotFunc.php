<?php
namespace backend\modules\tccbot\classes;

use Yii;
use yii\helpers\Url;
use yii\helpers\Html;
use appxq\sdii\utils\SDUtility;
use backend\modules\tccbot\models\BuffeTableServer;

/**
 * OvccaFunc class file UTF-8
 * @author SDII <iencoded@gmail.com>
 * @copyright Copyright &copy; 2015 AppXQ
 * @license http://www.appxq.com/license/
 * @version 1.0.0 Date: 9 ก.พ. 2559 12:38:14
 * @link http://www.appxq.com/
 * @example 
 */
class TccBotFunc {
    public static function buffeUpdate($sitecode) {
	$model = BuffeTableServer::find()->where('sitecode=:sitecode', [':sitecode'=>$sitecode])->all();
	if($model){
	    foreach ($model as $key => $value) {
		$tbname = $value['table'];
		$qleft = (int)$value['qleft'];
		if(!isset(Yii::$app->session[$sitecode][$tbname])){
		    self::calBuffeUpdate($sitecode, $value);
		} else {
		    if(Yii::$app->session[$sitecode][$tbname]!=$qleft){
			self::calBuffeUpdate($sitecode, $value);
		    }
		}
	    }
	    return true;
	}
	return FALSE;
    }
    
    private static function calBuffeUpdate($sitecode, $value) {
	$tbname = $value['table'];
	$qleft = (int)$value['qleft'];
	
	Yii::$app->session[$sitecode][$tbname] = $qleft;
	
	try {
	    $sql = "SELECT count(*) AS num
		FROM $tbname
		WHERE hospcode = :hospcode
	    ";

	    $record = Yii::$app->dbbot2->createCommand($sql, [':hospcode'=>$sitecode])->queryScalar();

	    $total = $record+$qleft;

	    $progress = floatval($value['progress']);
	    if($total>0){
		$progress = number_format(($record/$total)*100);
	    }

	    Yii::$app->db->createCommand()->update('buffe_table_server', ['record'=>$record, 'progress'=>$progress], '`sitecode`=:sitecode AND `table`=:table', [':sitecode'=>$sitecode, ':table'=>$value['table']])->execute();
	} catch (\yii\db\Exception $e) {
	    Yii::$app->db->createCommand()->update('buffe_table_server', ['record'=>0, 'progress'=>0], '`sitecode`=:sitecode AND `table`=:table', [':sitecode'=>$sitecode, ':table'=>$value['table']])->execute();
	    return false;
	}
	
    }
}
