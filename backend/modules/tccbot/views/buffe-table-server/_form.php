<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\lib\sdii\components\helpers\SDNoty;

/* @var $this yii\web\View */
/* @var $model backend\modules\tccbot\models\BuffeTableServer */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="buffe-table-server-form">

    <?php $form = ActiveForm::begin(['id'=>$model->formName()]); ?>
	<div class="modal-header">
	    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	    <h4 class="modal-title" id="itemModalLabel">Buffe Table Server</h4>
	</div>

	<div class="modal-body">
		<?= $form->field($model, 'sitecode')->textInput(['maxlength' => true]) ?>

		<?= $form->field($model, 'table')->textInput(['maxlength' => true]) ?>

		<?= $form->field($model, 'record')->textInput() ?>

		<?= $form->field($model, 'qleft')->textInput() ?>

		<?= $form->field($model, 'progress')->textInput(['maxlength' => true]) ?>

	</div>
	<div class="modal-footer">
	    <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
	</div>

    <?php ActiveForm::end(); ?>

</div>

<?php  $this->registerJs("
$('form#{$model->formName()}').on('beforeSubmit', function(e){
    var \$form = $(this);
    $.post(
	\$form.attr('action'), //serialize Yii2 form
	\$form.serialize()
    ).done(function(result){
	if(result.status == 'success'){
	    ". SDNoty::show('result.message', 'result.status') ."
	    if(result.action == 'create'){
		$(\$form).trigger('reset');
		$.pjax.reload({container:'#buffe-table-server-grid-pjax'});
	    } else if(result.action == 'update'){
		$(document).find('#modal-buffe-table-server').modal('hide');
		$.pjax.reload({container:'#buffe-table-server-grid-pjax'});
	    }
	} else{
	    ". SDNoty::show('result.message', 'result.status') ."
	} 
    }).fail(function(){
	console.log('server error');
    });
    return false;
});

");?>