<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\tccbot\models\BuffeTableServerSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="buffe-table-server-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'sitecode') ?>

    <?= $form->field($model, 'table') ?>

    <?= $form->field($model, 'record') ?>

    <?= $form->field($model, 'qleft') ?>

    <?= $form->field($model, 'progress') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
