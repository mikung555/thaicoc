<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\helpers\Url;
use common\lib\sdii\widgets\SDGridView;
use common\lib\sdii\widgets\SDModalForm;
use common\lib\sdii\components\helpers\SDNoty;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\tccbot\models\BuffeTableServerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Buffe Table Servers';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="buffe-table-server-index">

    <div class="sdbox-header">
	<h3><?=  Html::encode($this->title) ?></h3>
    </div>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p style="padding-top: 10px;">
	<span class="label label-primary">Notice</span>
	<?= Yii::t('app', 'You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b> or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.') ?>
    </p>

    <?php  Pjax::begin(['id'=>'buffe-table-server-grid-pjax']);?>
    <?= SDGridView::widget([
	'id' => 'buffe-table-server-grid',
	'panelBtn' => Html::button(Yii::t('app', '<span class="glyphicon glyphicon-plus"></span>'), ['data-url'=>Url::to(['buffe-table-server/create']), 'class' => 'btn btn-success btn-sm', 'id'=>'modal-addbtn-buffe-table-server']),
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'sitecode',
            'table',
            'record',
            'qleft',
            'progress',

            ['class' => 'common\lib\sdii\widgets\SDActionColumn'],
        ],
    ]); ?>
    <?php  Pjax::end();?>

</div>

<?=  SDModalForm::widget([
    'id' => 'modal-buffe-table-server',
    'size'=>'modal-lg',
]);
?>

<?php  $this->registerJs("
$('#buffe-table-server-grid-pjax').on('click', '#modal-addbtn-buffe-table-server', function(){
modalBuffeTableServer($(this).attr('data-url'));
});

$('#buffe-table-server-grid-pjax').on('dblclick', 'tbody tr', function() {
    var id = $(this).attr('data-key');
    modalBuffeTableServer('".Url::to(['buffe-table-server/update', 'id'=>''])."'+id);
});	

$('#buffe-table-server-grid-pjax').on('click', 'tbody tr td a', function() {
    var url = $(this).attr('href');
    var action = $(this).attr('data-action');

    if(action === 'update' || action == 'view'){
	modalBuffeTableServer(url);
    } else if(action === 'delete') {
	yii.confirm('".Yii::t('app', 'Are you sure you want to delete this item?')."', function(){
	    $.post(
		url
	    ).done(function(result){
		if(result.status == 'success'){
		    ". SDNoty::show('result.message', 'result.status') ."
		    $.pjax.reload({container:'#buffe-table-server-grid-pjax'});
		} else {
		    ". SDNoty::show('result.message', 'result.status') ."
		}
	    }).fail(function(){
		console.log('server error');
	    });
	})
    }
    return false;
});
	
function modalBuffeTableServer(url) {
    $('#modal-buffe-table-server .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
    $('#modal-buffe-table-server').modal('show')
    .find('.modal-content')
    .load(url);
}

");?>