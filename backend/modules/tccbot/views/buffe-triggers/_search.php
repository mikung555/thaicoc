<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\tccbot\models\BuffeTriggersSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="buffe-triggers-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'sitecode') ?>

    <?= $form->field($model, 'Trigger') ?>

    <?= $form->field($model, 'Event') ?>

    <?= $form->field($model, 'Table') ?>

    <?= $form->field($model, 'Statement') ?>

    <?php // echo $form->field($model, 'Timing') ?>

    <?php // echo $form->field($model, 'Created') ?>

    <?php // echo $form->field($model, 'sql_mode') ?>

    <?php // echo $form->field($model, 'Definer') ?>

    <?php // echo $form->field($model, 'character_set_client') ?>

    <?php // echo $form->field($model, 'collation_connection') ?>

    <?php // echo $form->field($model, 'Database_Collation') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
