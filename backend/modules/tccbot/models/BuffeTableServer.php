<?php

namespace backend\modules\tccbot\models;

use Yii;

/**
 * This is the model class for table "buffe_table_server".
 *
 * @property string $sitecode
 * @property string $table
 * @property integer $his_record
 * @property integer $record
 * @property integer $qleft
 * @property string $progress
 */
class BuffeTableServer extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'buffe_table_server';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sitecode', 'table'], 'required'],
            [['his_record','record', 'qleft'], 'integer'],
            [['sitecode', 'progress'], 'string', 'max' => 10],
            [['table'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'sitecode' => 'หน่วยบริการ',
            'table' => 'ชื่อตาราง',
            'his_record' => 'จำนวนข้อมูลใน รพ.',
            'record' => 'จำนวนเรคคอร์ด',
            'qleft' => 'จำนวนคิวรอ',
            'progress' => 'ความก้าวหน้า',
        ];
    }

    /**
     * @inheritdoc
     * @return BuffeTableServerQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new BuffeTableServerQuery(get_called_class());
    }
}
