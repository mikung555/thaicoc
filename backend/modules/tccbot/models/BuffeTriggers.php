<?php

namespace backend\modules\tccbot\models;

use Yii;

/**
 * This is the model class for table "buffe_triggers".
 *
 * @property string $sitecode
 * @property string $Trigger
 * @property string $Event
 * @property string $Table
 * @property string $Statement
 * @property string $Timing
 * @property string $Created
 * @property string $sql_mode
 * @property string $Definer
 * @property string $character_set_client
 * @property string $collation_connection
 * @property string $Database_Collation
 */
class BuffeTriggers extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'buffe_triggers';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sitecode', 'Trigger', 'Event', 'Table'], 'required'],
            [['Statement'], 'string'],
            [['sitecode'], 'string', 'max' => 10],
            [['Trigger'], 'string', 'max' => 50],
            [['Event', 'Timing', 'Created', 'sql_mode', 'Definer', 'character_set_client', 'collation_connection', 'Database_Collation'], 'string', 'max' => 20],
            [['Table'], 'string', 'max' => 200]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'sitecode' => 'รหัสหน่วยบริการ',
            'Trigger' => 'ชื่อเรียก',
            'Event' => 'เหตุการณ์',
            'Table' => 'ตาราง',
            'Statement' => 'คำสั่ง',
            'Timing' => 'จังหวะ',
            'Created' => 'สร้างเมื่อ',
            'sql_mode' => 'โหมด',
            'Definer' => 'ผู้สร้าง',
            'character_set_client' => 'รหัสภาษาไคลเอนท์',
            'collation_connection' => 'รหัสภาษาเชื่อมต่อ',
            'Database_Collation' => 'รหัสภาษาฐานข้อมูล',
        ];
    }

    /**
     * @inheritdoc
     * @return BuffeTriggersQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new BuffeTriggersQuery(get_called_class());
    }
}
