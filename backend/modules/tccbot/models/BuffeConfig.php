<?php

namespace backend\modules\tccbot\models;

use Yii;

/**
 * This is the model class for table "buffe_config".
 *
 * @property string $id
 * @property string $buffe_version
 * @property string $his_type
 * @property string $his_version
 * @property double $config_delay
 * @property double $command_delay
 * @property double $constants_delay
 * @property double $template_delay
 * @property double $sync_delay
 * @property string $sync_nrec
 * @property string $his_user
 * @property string $his_password
 * @property string $his_ip
 * @property string $his_port
 * @property string $his_db
 * @property string $service_url
 * @property string $dadd
 * @property string $dupdate
 * @property string $last_ping
 * @property integer $del_log
 * @property string $cpu
 * @property string $ram
 * @property string $cpu_serv
 * @property string $ram_serv
 * @property string $ram_usage
 * @property string $ram_serv_usage
 */
class BuffeConfig extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'buffe_config';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'cpu', 'ram', 'cpu_serv', 'ram_serv', 'ram_usage', 'ram_serv_usage','config_delay', 'command_delay', 'constants_delay', 'template_delay', 'sync_delay', 'sync_nrec'], 'required'],
            [['config_delay', 'command_delay', 'constants_delay', 'template_delay', 'sync_delay'], 'number'],
            [['dadd', 'dupdate', 'last_ping'], 'safe'],
            [['del_log'], 'integer'],
            [['id', 'cpu', 'ram', 'cpu_serv', 'ram_serv','ram_usage', 'ram_serv_usage'], 'string', 'max' => 10],
            [['buffe_version', 'his_type', 'his_version', 'his_port'], 'string', 'max' => 20],
            [['sync_nrec', 'his_db', 'service_url'], 'string', 'max' => 255],
            [['his_user'], 'string', 'max' => 30],
            [['his_password', 'his_ip'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'buffe_version' => Yii::t('app', 'Buffe Version'),
            'his_type' => Yii::t('app', 'His Type'),
            'his_version' => Yii::t('app', 'His Version'),
            'config_delay' => Yii::t('app', 'Config Delay'),
            'command_delay' => Yii::t('app', 'Command Delay'),
            'constants_delay' => Yii::t('app', 'Constants Delay'),
            'template_delay' => Yii::t('app', 'Template Delay'),
            'sync_delay' => Yii::t('app', 'Sync Delay'),
            'sync_nrec' => Yii::t('app', 'Sync Nrec'),
            'his_user' => Yii::t('app', 'His User'),
            'his_password' => Yii::t('app', 'His Password'),
            'his_ip' => Yii::t('app', 'His Ip'),
            'his_port' => Yii::t('app', 'His Port'),
            'his_db' => Yii::t('app', 'His Db'),
            'service_url' => Yii::t('app', 'Service Url'),
            'dadd' => Yii::t('app', 'Dadd'),
            'dupdate' => Yii::t('app', 'Dupdate'),
            'last_ping' => Yii::t('app', 'Last Ping'),
            'del_log' => Yii::t('app', 'Del Log'),
            'cpu' => Yii::t('app', 'Cpu'),
            'ram' => Yii::t('app', 'Ram'),
	    'cpu_serv' => Yii::t('app', 'Cpu'),
            'ram_serv' => Yii::t('app', 'Ram'),
        ];
    }
}
