<?php

namespace backend\modules\tccbot\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\tccbot\models\BuffeTriggers;

/**
 * BuffeTriggersSearch represents the model behind the search form about `backend\modules\tccbot\models\BuffeTriggers`.
 */
class BuffeTriggersSearch extends BuffeTriggers
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sitecode', 'Trigger', 'Event', 'Table', 'Statement', 'Timing', 'Created', 'sql_mode', 'Definer', 'character_set_client', 'collation_connection', 'Database_Collation'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = BuffeTriggers::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere(['like', 'sitecode', $this->sitecode])
            ->andFilterWhere(['like', 'Trigger', $this->Trigger])
            ->andFilterWhere(['like', 'Event', $this->Event])
            ->andFilterWhere(['like', 'Table', $this->Table])
            ->andFilterWhere(['like', 'Statement', $this->Statement])
            ->andFilterWhere(['like', 'Timing', $this->Timing])
            ->andFilterWhere(['like', 'Created', $this->Created])
            ->andFilterWhere(['like', 'sql_mode', $this->sql_mode])
            ->andFilterWhere(['like', 'Definer', $this->Definer])
            ->andFilterWhere(['like', 'character_set_client', $this->character_set_client])
            ->andFilterWhere(['like', 'collation_connection', $this->collation_connection])
            ->andFilterWhere(['like', 'Database_Collation', $this->Database_Collation]);

        return $dataProvider;
    }
}
