<?php

namespace backend\modules\tccbot\models;

use Yii;

/**
 * This is the model class for table "buffe_tables".
 *
 * @property string $sitecode
 * @property string $table
 * @property string $data
 * @property integer $his_rows
 * @property integer $server_rows
 * @property string $dadd
 * @property string $dupdate
 */
class BuffeTables extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'buffe_tables';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sitecode', 'table', 'dadd', 'dupdate'], 'required'],
            [['data'], 'string'],
            [['his_rows', 'server_rows'], 'integer'],
            [['dadd', 'dupdate'], 'safe'],
            [['sitecode'], 'string', 'max' => 10],
            [['table'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'sitecode' => 'รหัสหน่วยบริการ',
            'table' => 'ตาราง',
            'data' => 'โครงสร้าง',
            'his_rows' => 'เรคคอร์ด (HIS)',
            'server_rows' => 'เรคคอร์ด (Server)',
            'qleft' => 'คิวรอ',
            'dadd' => 'วันที่นำเข้า',
            'dupdate' => 'วันที่อัพเดท',
        ];
    }

    public function fields()
    {
        $fields = [
            'sitecode',
            'table',
            'data',
            'his_rows',
            'server_rows' => function () {
                $tbname=$this->table;
                $sitecode=\Yii::$app->user->identity->userProfile->sitecode;
                $table=  \Yii::$app->db->createCommand("select count(*) as nrow from `buffe_data`.`{$tbname}` where sitecode LIKE '$sitecode'")->queryOne();
                return "select count(*) as nrow from `buffe_data`.`{$tbname}` where sitecode LIKE '$sitecode'";
                return $table['nrow'];
            },
            'qleft',
        ];
        return $fields;
    }    
    /**
     * @inheritdoc
     * @return BuffeTablesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new BuffeTablesQuery(get_called_class());
    }
}
