<?php

namespace backend\modules\managedata;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\modules\managedata\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
