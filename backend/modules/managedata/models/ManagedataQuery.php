<?php

namespace backend\modules\managedata\models;

/**
 * This is the ActiveQuery class for [[Managedata]].
 *
 * @see Managedata
 */
class ManagedataQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return Managedata[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Managedata|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}