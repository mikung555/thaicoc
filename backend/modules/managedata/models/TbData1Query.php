<?php

namespace backend\modules\managedata\models;

/**
 * This is the ActiveQuery class for [[TbData1]].
 *
 * @see TbData1
 */
class TbData1Query extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return TbData1[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return TbData1|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}