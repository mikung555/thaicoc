<?php

namespace backend\modules\managedata\models;

use Yii;

/**
 * This is the model class for table "ezform".
 *
 * @property string $ezf_id
 * @property string $ezf_name
 * @property string $ezf_detail
 * @property string $ezf_table
 * @property string $user_create
 * @property string $create_date
 * @property string $user_update
 * @property string $update_date
 * @property integer $status
 * @property integer $shared
 * @property integer $public_listview
 * @property integer $public_edit
 * @property integer $public_delete
 *
 * @property EzformFields[] $ezformFields
 */
class Managedata extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ezform';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ezf_id', 'ezf_name', 'ezf_detail', 'ezf_table', 'user_create', 'create_date', 'user_update', 'update_date', 'status'], 'required'],
            [['ezf_id', 'user_create', 'user_update', 'status', 'shared', 'public_listview', 'public_edit', 'public_delete'], 'integer'],
            [['ezf_detail'], 'string'],
            [['create_date', 'update_date'], 'safe'],
            [['ezf_name', 'ezf_table'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ezf_id' => Yii::t('app', 'รหัสฟอร์ม'),
            'ezf_name' => Yii::t('app', 'ฟอร์ม'),
            'ezf_detail' => Yii::t('app', 'n'),
            'ezf_table' => Yii::t('app', 'เสร็จ'),
            'user_create' => Yii::t('app', 'รอ'),
            'create_date' => Yii::t('app', 'วันที่สร้าง'),
            'user_update' => Yii::t('app', 'ผู้แก้ไข'),
            'update_date' => Yii::t('app', 'วันที่แก้ไข'),
            'status' => Yii::t('app', 'สถานะ'),
            'shared' => Yii::t('app', 'private = 0, public = 1'),
            'public_listview' => Yii::t('app', 'อนุญาตให้ view List Data table ของฟอร์ม'),
            'public_edit' => Yii::t('app', 'อนุญาตให้ Edit/Update List Data table ของฟอร์ม'),
            'public_delete' => Yii::t('app', 'อนุญาตให้ Remove List Data table ของฟอร์ม'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEzformFields()
    {
        return $this->hasMany(EzformFields::className(), ['ezf_id' => 'ezf_id']);
    }

    /**
     * @inheritdoc
     * @return ManagedataQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ManagedataQuery(get_called_class());
    }
}
