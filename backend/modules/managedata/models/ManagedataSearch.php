<?php

namespace backend\modules\managedata\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\ezforms\models\Ezform;
/**
 * ManagedataSearch represents the model behind the search form about `backend\modules\managedata\models\Managedata`.
 */
class ManagedataSearch extends Ezform
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ezf_id', 'user_create', 'user_update', 'status', 'shared', 'public_listview', 'public_edit', 'public_delete'], 'integer'],
            [['ezf_name', 'ezf_detail', 'ezf_table', 'create_date', 'update_date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Ezform::find()
            ->select('ezform.*, ezform_favorite.userid as user_id')
            ->innerJoin('ezform_favorite', '`ezform`.`ezf_id` = `ezform_favorite`.`ezf_id`')
            ->andWhere('ezform.`status` <> :status', [':status' => 3])
            ->andWhere(['ezform_favorite.userid' => Yii::$app->user->id]);
        //->andWhere(['ezform.status' => 1])
//        \yii\helpers\VarDumper::dump($query->createCommand()->sql, 10, TRUE);
//        echo Yii::$app->user->id;
//        Yii::$app->end();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['attributes' => ['ezf_id', 'ezf_name']],
            'pagination' => array(
                'pageSize' => 100,
            ),
        ]);

        //$this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        return $dataProvider;
    }

    public function searchTdc($params)
    {
        $query = Ezform::find()
            ->select('ezform.*')
            ->where("shared = '99' and ezf_name like '(%' ORDER BY ezf_name");
        //->andWhere(['ezform.status' => 1])
//        \yii\helpers\VarDumper::dump($query->createCommand()->sql, 10, TRUE);
//        echo Yii::$app->user->id;
//        Yii::$app->end();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => array(
                'pageSize' => 100,
            ),
        ]);

        //$this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        return $dataProvider;
    }
}
