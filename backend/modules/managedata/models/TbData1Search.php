<?php

namespace backend\modules\managedata\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\managedata\models\TbData1;

/**
 * TbData1Search represents the model behind the search form about `backend\modules\managedata\models\TbData1`.
 */
class TbData1Search extends TbData1
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'rstat', 'user_create', 'user_update', 'edattype'], 'integer'],
            [['create_date', 'update_date', 'value1', 'value2', 'value3'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TbData1::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'rstat' => $this->rstat,
            'user_create' => $this->user_create,
            'create_date' => $this->create_date,
            'user_update' => $this->user_update,
            'update_date' => $this->update_date,
            'edattype' => $this->edattype,
        ]);

        $query->andFilterWhere(['like', 'value1', $this->value1])
            ->andFilterWhere(['like', 'value2', $this->value2])
            ->andFilterWhere(['like', 'value3', $this->value3]);

        return $dataProvider;
    }
}
