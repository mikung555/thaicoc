<?php
namespace backend\modules\managedata\classes;

use Yii;
use backend\modules\managedata\classes\ManagerFunc;
use backend\modules\ckdnet\classes\CkdnetFunc;

/**
 * ManagerFunc class file UTF-8
 * @author SDII <iencoded@gmail.com>
 * @copyright Copyright &copy; 2015 AppXQ
 * @license http://www.appxq.com/license/
 * @version 1.0.0 Date: 16 พ.ย. 2559 14:05:38
 * @link http://www.appxq.com/
 * @example 
 */
class ManagerQuery {
    public static function getZone($sitecode) {
	$sql = "SELECT all_hospital_thai.zone_code
		FROM all_hospital_thai
		WHERE all_hospital_thai.hcode = :sitecode
		";
	return Yii::$app->db->createCommand($sql, [':sitecode'=>$sitecode])->queryScalar();
    }
    
    public static function getDataCountSite($sitecode, $table) {
	$sql = "SELECT count(*) AS row
		FROM $table
		WHERE sitecode = :sitecode
		";
	return CkdnetFunc::queryScalar($sql, [':sitecode'=>$sitecode]);
    }
    
    
    public static function getDataCountAll($ezf_id) {
	$sql = "SELECT tdc_count.total
		FROM tdc_count
		WHERE tdc_count.ezf_id = :ezf_id
		";
	return Yii::$app->dbwebs1->createCommand($sql, [':ezf_id'=>$ezf_id])->queryScalar();
    }
    
    public static function getMapTable($ezf_id) {
	$sql = "SELECT ezform_map_f43.f43
		FROM ezform_map_f43
		WHERE ezform_map_f43.ezf_id = :ezf_id
		";
	return Yii::$app->dbwebs1->createCommand($sql, [':ezf_id'=>$ezf_id])->queryScalar();
    }
    
    public static function getMapEzf($table) {
	$sql = "SELECT ezform_map_f43.ezf_id
		FROM ezform_map_f43
		WHERE ezform_map_f43.f43 = :f43
		";
	return Yii::$app->dbwebs1->createCommand($sql, [':f43'=>$table])->queryScalar();
    }
    
    public static function getColumn($table) {
	$sitecode = Yii::$app->user->identity->userProfile->sitecode;
	$data_con = \backend\modules\ckdnet\classes\CkdnetQuery::getDbConfig($sitecode);
	    
	$sqlColumn = "SELECT COLUMN_NAME AS `column` FROM INFORMATION_SCHEMA.COLUMNS
		WHERE TABLE_NAME = :tbname AND table_schema = :tbschema";//AND table_schema = :tbschema , ':tbschema' => 'tdc_data'
	return CkdnetFunc::queryAll($sqlColumn, [':tbname' => $table, ':tbschema' => $data_con['db']]);
    }
    
    public static function getFields($id) {
	
	$sql = "SELECT *  FROM `ezform_fields` WHERE ezf_field_type<>13 AND `ezf_id` = :id ORDER BY ezf_field_order";
	
	return CkdnetFunc::queryAll($sql, [':id'=>$id]);
    }
	
}
