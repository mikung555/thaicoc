<?php

use yii\helpers\Html;

$this->title = 'Annotated case report form';

$this->registerCssFile('/css/ezform.css');
?>


<div class='row'>
    <div class='col-lg-12'>
        <strong class="text-center" style="padding:10px;color:red;"> * คลิกเลือกชื่อตัวแปรที่ต้องการนำลงรายงาน</strong>
    </div>
</div>
<hr>
<div class="container" style="width:100%;">
    <div class="row dad" id="formPanel">
        <?php $form = backend\modules\ezforms\components\EzActiveForm::begin(['action' => '#', 'options' => ['enctype' => 'multipart/form-data']]); ?>
        <?php
        $form->attributes['rstat'] = 'annotated'; // render annotated
        foreach ($modelfield as $field) {
            echo \backend\modules\ezforms\components\EzformFunc::getTypeEform($model_gen, $field, $form, '', '2');//getTypeEform($model_gen, $field, $form, '2');
        }
        backend\assets\EzfGenAsset::register($this);
        ?>
        <div class='col-lg-12'>
            <p style='color:green;font-size: 15px;'><?php echo $message; ?></p>
        </div>
        <?php backend\modules\ezforms\components\EzActiveForm::end(); ?>
    </div
</div>
<?php 
$this->registerJS("
    $('code').addClass('label label-danger');
    $('code').click(function(){
	var values = $(this).text();
        
        var ezf_id = '".$ezf_id."';
            $.ajax({
                url:'".\yii\helpers\Url::to(['/ezformreports/ezform-custom-report/check-type'])."',
                type:'post',
                data:{ezf_id:ezf_id, ezf_field_name:values},
                success:function(data){
                    tinymce.activeEditor.execCommand('mceInsertContent', false, '{{data.'+data+'}}');
                }
            });

       
    });
    
    $('th ,b').click(function(){
	var values = $(this).text();
        //alert(values);

       tinymce.activeEditor.execCommand('mceInsertContent', false,values);
    });

");

$this->registerCss("
   code{cursor:pointer;} 
");
?>