<?php

use yii\helpers\Html;

$this->title = 'Annotated case report form';

$this->registerCssFile('/css/ezform.css');
if($_GET['print']==1) {
    $this->registerJs('
    window.onload = function () {
        window.print();
    }
    ');
}
?>

<?php if($_GET['print']!=1){ ?>
<div class='row'>
    <div class='col-lg-12'>
        <div class="text-right">
            <?php echo Html::a('<li class="fa fa-chevron-circle-left"></li> ย้อนกลับก่อนหน้านี้', Yii::$app->request->referrer, ['class' => 'btn btn-success'])?>
            <?php echo Html::a('<span class="fa fa-print"></span> Print', ['ezf_id' =>$_GET['ezf_id'], 'print' => 1], ['class' => 'btn btn-danger', 'target'=>'_blank']); ?>
            <hr></div>

        <h2><?= $modelezform->ezf_name; ?></h2>
    </div>
</div>
<?php } ?>

<div class='row'>
    <div class='col-lg-12'>
        <h4><?= $modelezform->ezf_detail; ?></h4>
    </div>
</div>
<hr>

<div class="row dad" id="formPanel">
    <?php $form = backend\modules\ezforms\components\EzActiveForm::begin(['action'=>'#', 'options' => ['enctype'=>'multipart/form-data']]); ?>
    <?php
    $form->attributes['rstat'] = 'annotated';// render annotated
    foreach($modelfield as $field){
        echo \backend\modules\ezforms\components\EzformFunc::getTypeEform($model_gen, $field, $form);
    }
    backend\assets\EzfGenAsset::register($this);
    ?>
    <div class='col-lg-12'>
        <p style='color:green;font-size: 15px;'><?php echo $message;?></p>
    </div>
    <?php backend\modules\ezforms\components\EzActiveForm::end();?>
</div