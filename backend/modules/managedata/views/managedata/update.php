<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\managedata\models\Managedata */

$this->title = Yii::t('backend', 'Update {modelClass}: ', [
    'modelClass' => 'Managedata',
]) . ' ' . $model->ezf_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Managedatas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ezf_id, 'url' => ['view', 'id' => $model->ezf_id]];
$this->params['breadcrumbs'][] = Yii::t('backend', 'Update');
?>
<div class="managedata-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
