<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\helpers\Url;
use common\lib\sdii\widgets\SDGridView;
use common\lib\sdii\widgets\SDModalForm;
use common\lib\sdii\components\helpers\SDNoty;
use common\lib\codeerror\helpers\CheckOwn;
use backend\models\Dynamic;
use backend\modules\managedata\classes\ManagerQuery;
use backend\modules\managedata\classes\ManagerFunc;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\managedata\models\ManagedataSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'จัดการข้อมูล');
$this->params['breadcrumbs'][] = $this->title;
$sitecode = Yii::$app->user->identity->userProfile->sitecode;

?>
<div class="managedata-index">
    <!-- Nav tabs -->
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="<?php echo Yii::$app->controller->action->id == 'index' ? 'active' : null; ?>"><a href="/managedata/managedata" aria-controls="home" role="tab" data-toggle=""><span class="fa fa-database"></span> Data from EzForm</a></li>
        <li role="presentation" class="<?php echo Yii::$app->controller->action->id == 'from-tdc' ? 'active' : null; ?>"><a href="/managedata/managedata/from-tdc" aria-controls="home" role="tab"><span class="fa fa-database"></span> Data from TDC</a></li>
    </ul>

    <!-- Tab panes http://tools.cascap.in.th/purify/download/index.php -->
    <div class="tab-content">
	
        <div role="tabpanel" class="tab-pane active" id="export1" style="padding-top: 10px;">
            <?= SDGridView::widget([
                'id' => 'managedata-grid',
                'panelBtn' => Html::a('<span class="glyphicon glyphicon-tint"></span> Purify', 'http://tools.cascap.in.th/purify/download/index.php', [
                                    'title' => Yii::t('yii', 'Purification Tools'),
				    'target'=>'_blank',
                                    'class'=>'btn btn-warning btn-xs',
                                ]),
                'dataProvider' => $dataProvider,
                //  'filterModel' => $searchModel,
                'columns' => [
                    [
                        'class' => 'yii\grid\SerialColumn',
                        'headerOptions' => ['style'=>'text-align: center;'],
                        'contentOptions' => ['style'=>'min-width:60px;text-align: center;'],
                    ],

                    //'ezf_id',
                    'ezf_name',
                    //'ezf_detail:ntext',
                    //'ezf_table',
//                    [
//                        'label'=>'ข้อมูลทั้งหมด',
//                        'header'=>'ข้อมูลทั้งหมด',
//                        //'format'=>'raw',
//                        'value' => function ($data)
//                        {
//                            try {
//                                //$sitecode = Yii::$app->user->identity->userProfile->sitecode;
//                                $conut = $data['total']; //ManagerQuery::getDataCountAll($data['ezf_id']);
//                            } catch (yii\db\Exception $e) {
//                                $conut = 0;
//                            }
//                            return number_format($conut);
//                        }
//                    ],
                    [
                        'label'=>'เฉพาะไซต์ตัวเอง',
                        'header'=>'เฉพาะไซต์ตัวเอง',
                        //'format'=>'raw',
                        'value' => function ($data)
                        {
                            
                            try {
                                $sitecode = Yii::$app->user->identity->userProfile->sitecode;
				$table = $data['f43'];
				
                                $conut = ManagerQuery::getDataCountSite($sitecode, $table);
                            } catch (yii\db\Exception $e) {
                                $conut = 0;
                            }
                            return number_format($conut);
                        }
                    ],
                    //'user_create',
                    // 'create_date',
                    // 'user_update',
                    // 'update_date',
                    // 'status',
                    // 'shared',
                    // 'public_listview',
                    // 'public_edit',
                    // 'public_delete',

                    [
                        'class' => 'common\lib\sdii\widgets\SDActionColumn' ,
                        'template' => '{export} {edat} {annotated} {datadic}',
                        'contentOptions' => ['style'=>'width:380px;'],
                        'buttons' => [
                            'export' => function ($url, $model){
                                
				    $table = $model['f43'];
                                    return Html::a('<span class="glyphicon glyphicon-share-alt"></span> View/Export', ['view-tdc', 'id' => $model['ezf_id'], 'table'=>$table], [
                                        'data-action' => 'viewexport',
                                        'title' => Yii::t('yii', 'View/Export'),
                                        'class'=>'btn btn-primary btn-xs',
                                        'data-pjax' => isset($this->pjax_id)?$this->pjax_id:'0',
                                    ]);
                                
                            },

                            'edat' => function ($url, $model){

                                return Html::a('<span class="glyphicon glyphicon-tasks"></span> EDAT', ['/edat', 'id' => $model['ezf_id'], 'table'=>$model['ezf_table']], [
                                    'data-action' => 'edat',
                                    'title' => Yii::t('yii', 'Exploratory Data Analysis Tools'),
                                    'class'=>'btn btn-success btn-xs',
                                    'url' => ['/managedata/tb-data1'],
                                ]);

                            },
                            'annotated' => function ($url, $model){

                                return Html::a('<span class="glyphicon glyphicon-book"></span> Annotated CRF', ['/managedata/annotated', 'ezf_id' => $model['ezf_id'],], [
                                    'data-action' => 'Data Dictionary',
                                    'title' => Yii::t('yii', 'Annotated case report form'),
                                    'class'=>'btn btn-danger btn-xs',
                                ]);

                            },
                            'datadic' => function ($url, $model){

                                return Html::a('<span class="glyphicon glyphicon-book"></span> Dictionary', ['/managedata/dictionary', 'ezf_id' => $model['ezf_id']], [
                                    'data-action' => 'Data Dictionary',
                                    'title' => Yii::t('yii', 'Data Dictionary'),
                                    'class'=>'btn btn-info btn-xs',
                                ]);

                            }
                            

                        ]
                    ],
                ],
            ]); ?>
        </div>
        <div role="tabpanel" class="tab-pane" id="export2" style="padding-top: 10px;">
            กำลังดำเนินการ
        </div>
    </div>

</div>

<?=  SDModalForm::widget([
    'id' => 'modal-managedata',
    'size'=>'modal-lg',
]);
?>

<?php  $this->registerJs("
$('#managedata-grid-pjax').on('click', '#modal-addbtn-managedata', function(){
modalManagedatum($(this).attr('data-url'));
});

$('#managedata-grid-pjax').on('dblclick', 'tbody tr', function() {
    var id = $(this).attr('data-key');
    modalManagedatum('".Url::to(['managedata/update', 'id'=>''])."'+id);
});

$('#managedata-grid-pjax').on('click', 'tbody tr td a', function() {
    var url = $(this).attr('href');
    var action = $(this).attr('data-action');

    if(action === 'update' || action == 'viewexport'){
	modalManagedatum(url);
}else if(action === 'delete') {
	yii.confirm('".Yii::t('app', 'Are you sure you want to delete this item?')."', function(){
	    $.post(
		url
	    ).done(function(result){
		if(result.status == 'success'){
		    ". SDNoty::show('result.message', 'result.status') ."
		    $.pjax.reload({container:'#managedata-grid-pjax'});
		} else {
		    ". SDNoty::show('result.message', 'result.status') ."
		}
	    }).fail(function(){
		console.log('server error');
	    });
	})
    }
    return false;
});

function modalManagedatum(url) {
    $('#modal-managedata .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
    $('#modal-managedata').modal('show')
    .find('.modal-content')
    .load(url);
}

");?>
