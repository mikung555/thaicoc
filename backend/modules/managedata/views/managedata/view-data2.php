<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\TbdataSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'จัดการข้อมูล';
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'จัดการข้อมูล'), 'url' => ['index']];
$this->params['breadcrumbs'][] = 'ดูข้อมูล/ส่งออกข้อมูล';
?>
<style>
    a.asc:after {
	content: "\e151";
    }
    a.asc:after, a.desc:after {
	position: relative;
	display: inline-block;
	font-family: 'Glyphicons Halflings';
	font-style: normal;
	font-weight: normal;
	padding-left: 5px;
    }
    :after, :before {
	-webkit-box-sizing: border-box;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
    }
    a.desc:after {
	content: "\e152";
    }
    a.asc:after, a.desc:after {
	position: relative;
	display: inline-block;
	font-family: 'Glyphicons Halflings';
	font-style: normal;
	font-weight: normal;
	padding-left: 5px;
    }
</style>
<div class="tbdata-index">

<?php // echo $this->render('_search', ['model' => $searchModel]);  ?>

<!--    <p>
<?= Html::a('Create Tbdata', ['create'], ['class' => 'btn btn-success']) ?>
    </p>-->
    <br>

    <div style='float:right'> 
<?= Html::a('<i class="fa fa-mail-reply"></i>&nbsp;&nbsp;&nbsp;กลับไปหน้าเลือกฟอร์ม', ['/managedata/managedata/', 'id' => $dataForm["ezf_id"]], ['class' => 'btn btn-warning btn-flat']) ?>
    </div>
    <br>
    <br>

    <div class="box">
	<div class="box-body">
<?php

use kartik\dynagrid\DynaGrid;
use kartik\grid\GridView;

$columns[] = [
    'class' => 'kartik\grid\SerialColumn',
    'headerOptions' => ['style'=>'text-align: center;'],
    'contentOptions' => ['style'=>'min-width:60px;text-align: center;'],
];

$aindex = 1;
$params = Yii::$app->request->queryParams;
$ezfid = $params['id'];

$arrField = ArrayHelper::map($dataField, 'ezf_field_name', 'ezf_field_label');

if (count($dataColumn) > 0) {
    foreach ($dataColumn as $key => $form) {

	if (in_array($form["column"], ['xsourcex', 'target', 'user_create', 'user_update',])) {
	    continue;
	}
	$title = ($arrField[$form["column"]] != '') ? $arrField[$form["column"]] : $form["column"];
	if ($form['column'] == 'rstat') {
	    $columns[$aindex] = [
		'attribute' => $form['column'],
		'label' => $form['column'],
		'value' => function ($data) {
		    $rstat = '';
		    if ($data['rstat'] == 2) {
			$rstat = 'Safe draft';
		    } elseif ($data['rstat'] == 4) {
			$rstat = 'Submitted';
		    } elseif ($data['rstat'] == 3) {
			$rstat = 'Delete';
		    }
		    return $rstat;
		},
		'sortLinkOptions' => ['data-toggle' => 'tooltip', 'title' => $title],
		    //'headerOptions'=>['style'=>'text-align: center; width:100px;'],
		    //'contentOptions'=>['style'=>'width:100px; text-align: center;'],
	    ];
	} elseif ($form['column'] == 'id') {
	    $columns[$aindex] = [
		'attribute' => $form['column'],
		'label' => $form['column'],
		'value' => function ($data) {
		    return '<a data-toggle="tooltip" title="show data" href="' . yii\helpers\Url::to(['/inputdata/redirect-page', 'ezf_id' => $_GET['id'], 'dataid' => $data['id']]) . '">' . $data['id'] . '</a>';
		},
			'format' => 'raw',
			'sortLinkOptions' => ['data-toggle' => 'tooltip', 'title' => $title],
			    //'headerOptions'=>['style'=>'text-align: center; width:100px;'],
			    //'contentOptions'=>['style'=>'width:100px; text-align: center;'],
		    ];
		} else {
		    $columns[$aindex] = [
			'attribute' => $form['column'],
			'label' => $form['column'],
			'sortLinkOptions' => ['data-toggle' => 'tooltip', 'title' => $title],
			    //'headerOptions'=>['style'=>'text-align: center; width:100px;'],
			    //'contentOptions'=>['style'=>'width:100px; text-align: center;'],
		    ];
		}

		$aindex++;
	    }
	}
	$columns[] = [
	    'class' => 'yii\grid\CheckboxColumn',
	    'checkboxOptions' => [
		'class' => 'selectionOvPersonIds'
	    ],
	    'headerOptions' => ['style'=>'text-align: center;'],
	    'contentOptions' => ['style'=>'min-width:40px;text-align: center;'],
	];
	?>
		   
<div class="row" style="margin-bottom: 10px;">
    <div class="col-sm-6">
	<?php
	$form = ActiveForm::begin([
		    'id' => 'jump_menu',
		    'action' => ['view-data', 'id' => $_GET['id'], 'table' => $_GET['table']],
		    'method' => 'get',
		    'layout' => 'horizontal',
	]);
	?>

	<?php
	if ($owner) {
	    echo Html::dropDownList('show', $show, $show_list, ['class' => 'form-control', 'onChange' => '$("#jump_menu").submit()']);
	}
	?>

	<?php ActiveForm::end(); ?>
    </div>
    <div class="col-sm-6 sdbox-col">
	<?php
	echo kartik\export\ExportMenu::widget([
	'dataProvider' => $dataProvider,
	'columns' => $columns,
	'fontAwesome' => true,

	]);
	?>
    </div>

</div>
			    <?php
	$dynagrid = GridView::widget([
	    'dataProvider' => $dataProvider,
	    'columns' => $gridColumns,
	]);
//			    $dynagrid = DynaGrid::begin([
//					'columns' => $columns,
//					'theme' => 'panel-info',
//					'showPersonalize' => true,
//					'storage' => 'cookie',
//					'gridOptions' => [
//					    'dataProvider' => $dataProvider,
//					    'filterModel' => $searchModel,
//					    'showPageSummary' => true,
//					    'floatHeader' => true,
//					    'responsiveWrap' => FALSE,
//					    'export' => true, //Got error at server
//					    'pjax' => true,
//					    'panel' => [
//						'heading' => '<h3 class="panel-title"><i class="glyphicon glyphicon-book"></i>  ชื่อฟอร์ม ' . $dataForm['ezf_name'] . '</h3>',
//						'before' => '<div style="padding-top: 7px;"><em>* ท่านสามารถเรียกดูข้อมูลตามเงื่อนไขได้ทั้งสิ้น ' . $dataProvider->getTotalCount() . ' record. </em> (<em>* ท่านสามารถส่งออกข้อมูลได้โดยการเลือกปุ่มส่งออก ที่ด้านขวาบนของตาราง</em>) </div>',
//						'after' => false
//					    ],
//					    'toolbar' => [
//						['content' =>
//						    Html::a('<span class="fa fa-list"></span> Show All', ['id' => $_GET['id'], 'table' => $_GET['table'], 'show' => 'all'], ['data-pjax' => 0, 'class' => 'btn btn-success', 'title' => 'Show all data'])
//						],
//						['content' => '{dynagrid}'],
//						'{export}',
//					    ],
//					],
//					'options' => ['id' => 'dynagrid-1', 'pageSize' => Yii::$app->request->get('show') == 'all' ? 0 : 50,] // a unique identifier is important
//			    ]);
//
//			    if (substr($dynagrid->theme, 0, 6) == 'simple') {
//				$dynagrid->gridOptions['panel'] = false;
//			    }
//			    DynaGrid::end();
			    ?>

	</div>
    </div></div>