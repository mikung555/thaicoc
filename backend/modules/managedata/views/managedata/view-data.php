<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;
use common\lib\sdii\components\helpers\SDNoty;
use appxq\sdii\widgets\ModalForm;
use yii\helpers\Url;
use appxq\sdii\helpers\SDHtml;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\TbdataSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$column='A';
$column++;
$this->title = 'จัดการข้อมูล';
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'จัดการข้อมูล'), 'url' => ['index']];
$this->params['breadcrumbs'][] = 'ดูข้อมูล/ส่งออกข้อมูล';


$op['language'] = 'th';
	
$q = array_filter($op);

$this->registerJsFile('https://maps.google.com/maps/api/js?key=AIzaSyCq1YL-LUao2xYx3joLEoKfEkLXsEVkeuk&'.http_build_query($q), [
    'position'=>\yii\web\View::POS_HEAD,
    'depends'=>'yii\web\YiiAsset',
]);

?>
<style>
    .grid-view th, .grid-view td {
    white-space: nowrap;
}
a.asc:after {
  content: "\e151";
}
a.asc:after, a.desc:after {
  position: relative;
  display: inline-block;
  font-family: 'Glyphicons Halflings';
  font-style: normal;
  font-weight: normal;
  padding-left: 5px;
}
:after, :before {
  -webkit-box-sizing: border-box;
  -moz-box-sizing: border-box;
  box-sizing: border-box;
}
a.desc:after {
  content: "\e152";
}
a.asc:after, a.desc:after {
  position: relative;
  display: inline-block;
  font-family: 'Glyphicons Halflings';
  font-style: normal;
  font-weight: normal;
  padding-left: 5px;
}
</style>
<div id="main-manager" class="tbdata-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

<!--    <p>
        <?= Html::a('Create Tbdata', ['create'], ['class' => 'btn btn-success']) ?>
    </p>-->
    <br>
    
    <div style='float:right'> 
        <?= Html::a('<i class="fa fa-mail-reply"></i>&nbsp;&nbsp;&nbsp;กลับไปหน้าเลือกฟอร์ม', ['/managedata/managedata/', 'id' => $dataForm["ezf_id"]], ['class' => 'btn btn-warning btn-flat']) ?>
    </div>
    <?php
	echo '<h3 class="panel-title"><i class="glyphicon glyphicon-book"></i>  ชื่อฟอร์ม '.$dataForm['ezf_name'].'</h3>';
	echo '<div style="padding-top: 7px;"><em>* ท่านสามารถเรียกดูข้อมูลตามเงื่อนไขได้ทั้งสิ้น '.$dataProvider->getTotalCount().' record. </em> </div>';
?>
    
    <div class="box">
    <div class="box-body">
<?php
use kartik\dynagrid\DynaGrid;
use kartik\grid\GridView;
//$columns = [
//    ['class'=>'kartik\grid\SerialColumn', 'order'=>DynaGrid::ORDER_FIX_LEFT],
//    'var2',
//    'var3',
//    'var4',
//    'var6',
//    'var7',
//    'var9',
//    'var10',
//    'var11',
//    'var12',
//    'var13',
//    'var14',
//    'var15',
//    'var16',
//    'var17',
//    'var18',
//    'var19',
//    'var20',
//    ['class'=>'kartik\grid\CheckboxColumn',  'order'=>DynaGrid::ORDER_FIX_RIGHT]
//];

	//$columns[0]['class']='kartik\grid\CheckboxColumn';
	$columns[0] = [
		'class' => 'yii\grid\CheckboxColumn',
		'checkboxOptions' => [
		    'class' => 'selectionManagerIds'
		],
		'headerOptions' => ['style'=>'text-align: center;'],
		'contentOptions' => ['style'=>'width:40px;text-align: center;'],
	    ];
	$columns[1] = [
		'header' => '',
		'contentOptions' => ['style'=>'width:80px;text-align: center;'],
		'value' => function ($model)
		    {
			return Html::a('<span class="fa fa-eye"></span> View', NULL, [
				    'data-url'=>Url::to(['/inv/inv-person/ezform-print', 
				    'ezf_id'=>$_GET['id'],
				    'readonly'=>0,
				    'dataid'=>$model['id']]),
                                    'title' => Yii::t('app', 'View'),
                                    'class'=>'btn btn-info btn-xs print-ezform',
			    ]);
		    },
		//'noWrap' => TRUE,
		'format' => 'raw',
			 
	    ];
        $columns[2]['class']='yii\grid\SerialColumn';
	$columns[2]['contentOptions']=['style'=>'min-width:80px;text-align: center;'];
        //$columns[2]['order']=DynaGrid::ORDER_FIX_LEFT;
	
        $aindex=3;
        $params=Yii::$app->request->queryParams;
        $ezfid=$params['id'];
	
	$colFields = backend\modules\inv\classes\InvQuery::getFieldsAll($ezfid);
	$colFields_label = ArrayHelper::map($colFields, 'id', 'name');
	//appxq\sdii\utils\VarDumper::dump($colFields);
        
	$moreColumn = [];
        
	if($dataExport){
	    if(isset($dataExport['lable_extra']) && $dataExport['lable_extra']!=''){
		$lable_extra = \yii\helpers\Json::decode($dataExport['lable_extra']);
		if(is_array($lable_extra)){
		    $moreColumn = $lable_extra;
		    $colKey = ArrayHelper::getColumn($dataColumn, 'column');

		    foreach ($moreColumn as $keyMore => $valueMore) {
			if(!in_array($keyMore, $colKey)){
			    $dataColumn[] = ['column'=>$keyMore];
			}
		    }

		}
	    }
	}
	
	$colFields_label = array_merge($colFields_label, $moreColumn);
	$colFields_label = array_merge($colFields_label, ['user_create'=>'สร้างโดย','create_date'=>'สร้างวันที่','ptid'=>'PTID','hptcode'=>'hptcode','hsitecode'=>'hsitecode']);
	$colFields = array_keys($colFields_label);
	//appxq\sdii\utils\VarDumper::dump($colFields_label);
//	
//	yii\helpers\VarDumper::dump($colFields,10,true);
//	exit();
//	
	$columnTable = ArrayHelper::getColumn($dataColumn, 'column');
	
		$arrField = ArrayHelper::map($dataField, 'ezf_field_name', 'ezf_field_label');
		$arrField = array_merge($arrField, $moreColumn);
//        $ezfield = Yii::$app->db->createCommand('SELECT * FROM `ezform_fields` WHERE ezf_id =\''.$ezfid.'\'')->query()->readAll();
        if (count($dataColumn)>0) {
            foreach($colFields as $key => $field_name) {
				
                if(!in_array($field_name, $columnTable)){
						continue;
		}
				$title = ($arrField[$field_name]!='')?$arrField[$field_name]:$field_name;
				if($field_name=='rstat'){
				    $columns[$aindex]=[
					'attribute'=>$field_name,
					'label'=>$field_name,
					'value' => function ($data)
					    {
                                            
						$rstat = '';
						if($data['rstat']==1){
						    $rstat = 'Save draft';
						} elseif ($data['rstat']==2) {
						    $rstat = 'Submitted';
						} elseif ($data['rstat']==3) {
						    $rstat = 'Delete';
						} elseif ($data['rstat']==4) {
						    $rstat = 'Freeze';
						} else {
						    $rstat = $data['rstat'];
						}
						return $rstat;
					    },
					//'noWrap' => TRUE,
					//'sortLinkOptions' =>['data-toggle'=>'tooltip', 'title'=>$title],
					//'headerOptions'=>['style'=>'text-align: center; width:100px;'],
					//'contentOptions'=>['style'=>'width:100px; text-align: center;'],
				    ];
				} elseif($field_name=='id'){
				    $columns[$aindex]=[
					'attribute'=>$field_name,
					'label'=>$field_name,
					'value' => function ($data)
					    {
						return '<a data-toggle="tooltip" title="show data" href="'.yii\helpers\Url::to(['/inputdata/redirect-page', 'ezf_id'=>$_GET['id'], 'dataid'=>$data['id']]).'">'.$data['id'].'</a>';
					    },
					//'noWrap' => TRUE,
					'format' => 'raw',
					//'sortLinkOptions' =>['data-toggle'=>'tooltip', 'title'=>$title],
					//'headerOptions'=>['style'=>'text-align: center; width:100px;'],
					//'contentOptions'=>['style'=>'width:100px; text-align: center;'],
				    ];
				} else {
				    $columns[$aindex]=[
					'attribute'=>$field_name,
					'label'=>$field_name,
					//'noWrap' => TRUE,
					//'sortLinkOptions' =>['data-toggle'=>'tooltip', 'title'=>$title, 'data-placement'=>'bottom'],
					//'headerOptions'=>['data-toggle'=>'tooltip', 'title'=>'fsdff23432'],
					//'contentOptions'=>['style'=>'width:100px; text-align: center;'],
				    ];
				}
                
                $aindex++;
            }
			
        }
        
        //$columns[$aindex]['order']=DynaGrid::ORDER_FIX_RIGHT;   


        

     ?>
    <?php 
//use kartik\export\ExportMenu;

//$gridColumns = [
//    ['class' => 'yii\grid\SerialColumn'],
//    'id',
//    'name',
//    'color',
//    'publish_date',
//    'status',
//    ['class' => 'yii\grid\ActionColumn'],
//];
//
//        $columns['0']['class']='yii\grid\SerialColumn';
//        $aindex=1;
//        $params=Yii::$app->request->queryParams;
//        $ezfid=$params['id'];
//        
//        $ezfield = Yii::$app->db->createCommand('SELECT * FROM `ezform_fields` WHERE ezf_id =\''.$ezfid.'\'')->query()->readAll();
//        if (count($ezfield)>0) {
//            foreach($ezfield as $key => $form) {
//                $columns[$aindex]=$form['ezf_field_name'];
//                $aindex++;
//            }
//        }
//        $columns[$aindex]['class']='yii\grid\ActionColumn';
   
        
// Renders a export dropdown menu
//echo ExportMenu::widget([
//    'dataProvider' => $dataProvider,
//    'columns' => $columns
//]);

// You can choose to render your own GridView separately
//echo "<div class='table-responsive' style='overflow-x: auto;'>";        
//echo \kartik\grid\GridView::widget([
//    'dataProvider' => $dataProvider,
//    'filterModel' => $searchModel,
//    'columns' => $columns,
//    'headerRowOptions'=>['class'=>'kartik-sheet-style'],
//    'filterRowOptions'=>['class'=>'kartik-sheet-style'],
//    'pjax'=>true, // pjax is set to always true for this demo
//    'beforeHeader'=>[
//        [
//            'columns'=>[
//                ['content'=>'Header Before 1', 'options'=>['colspan'=>5, 'class'=>'text-center warning']], 
//                ['content'=>'Header Before 2', 'options'=>['colspan'=>3, 'class'=>'text-center warning']], 
//                ['content'=>'Header Before 3', 'options'=>['colspan'=>3, 'class'=>'text-center warning']], 
//            ],
//            'options'=>['class'=>'skip-export'] // remove this row from export
//        ]
//    ],
//    // set your toolbar
//    'toolbar'=> [
//        ['content'=>
//            Html::button('<i class="glyphicon glyphicon-plus"></i>', ['type'=>'button', 'title'=>Yii::t('kvgrid', 'Add Book'), 'class'=>'btn btn-success', 'onclick'=>'alert("This will launch the book creation form.\n\nDisabled for this demo!");']) . ' '.
//            Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['grid-demo'], ['data-pjax'=>0, 'class'=>'btn btn-default', 'title'=>Yii::t('kvgrid', 'Reset Grid')])
//        ],
//        '{export}',
//        '{toggleData}',
//    ],
//    'responsive' => true,
//    // set export properties
//    'export'=>[
//        'fontAwesome'=>true
//    ],
//    // parameters from the demo form
//
//]);    
//echo "</div>";
//print_r($columns);exit;
    ?>
	<div class="row" style="margin-bottom: 10px;">
	    <div class="col-sm-6">
		<?php $form = ActiveForm::begin([
	'id' => 'jump_menu',
        'action' => ['view-data', 'id'=>$_GET['id'], 'table'=>$_GET['table']],
        'method' => 'get',
	'layout' => 'horizontal',
    ]); ?>
	
	<?php
	
	    echo Html::dropDownList('show', $show, $show_list, ['class'=>'form-control', 'onChange'=>'$("#jump_menu").submit()', 'disabled'=>!$owner]);
	
	
	?>
	
    <?php ActiveForm::end(); ?>
	    </div>
	    <div class="col-sm-6 sdbox-col">
		<?=Html::button('<span class="glyphicon glyphicon-export"></span> Export All', ['id'=>'reportExel', 'data-url'=>Url::to(['/managedata/managedata/report-exel', 'id'=>$_GET['id'], 'table'=>$_GET['table'], 'show'=>$show]), 'class' => 'btn  btn-default '])?>
	    </div>
	
	</div>
	
	<?php  Pjax::begin(['id'=>'inv-person-grid-pjax']);?>

    <?=	\appxq\sdii\widgets\GridView::widget([
	'id' => 'inv-person-grid',
	'panelBtn' => '',//Html::button(SDHtml::getBtnDelete().' ลบที่เลือก', ['data-url'=>Url::to(['/managedata/managedata/deletes']), 'class' => 'btn btn-danger btn-sm', 'id'=>'modal-delbtn-manager', 'disabled'=>true]),
	'dataProvider' => $dataProvider,
	'filterModel' => $searchModel,
        'columns' => $columns,
    ]); ?>
    <?php  Pjax::end();?>
	
	
</div>
    </div></div>

<?=  ModalForm::widget([
    'id' => 'modal-ov-person',
    'size'=>'modal-lg',
]);
?>

<?=  ModalForm::widget([
    'id' => 'modal-print',
    'size'=>'modal-lg',
    'tabindexEnable'=>false,
    'options'=>['style'=>'overflow-y:scroll;']
]);
?>

<?php  $this->registerJs("
    
$('#inv-person-grid-pjax').on('click', '.select-on-check-all', function() {
    window.setTimeout(function() {
	
	var key = $('#inv-person-grid').yiiGridView('getSelectedRows');
	console.log(key);
	disabledInvFieldBtn(key.length);
    },100);
});

$('#inv-person-grid-pjax').on('click', '.selectionManagerIds', function() {
    var key = $('input:checked[class=\"'+$(this).attr('class')+'\"]');
    disabledInvFieldBtn(key.length);
});

function disabledInvFieldBtn(num) {
    if(num>0) {
	$('#modal-delbtn-manager').attr('disabled', false);
    } else {
	$('#modal-delbtn-manager').attr('disabled', true);
    }
}

$('#inv-fields-grid-pjax').on('click', '#modal-delbtn-manager', function() {
    selectionInvFieldGrid($(this).attr('data-url'));
});

function selectionInvFieldGrid(url) {
    yii.confirm('".Yii::t('app', 'Are you sure you want to delete these items?')."', function() {
	$.ajax({
	    method: 'POST',
	    url: url,
	    data: $('.selectionManagerIds:checked[name=\"selection[]\"]').serialize(),
	    dataType: 'JSON',
	    success: function(result, textStatus) {
		if(result.status == 'success') {
		    ". SDNoty::show('result.message', 'result.status') ."
		    $.pjax.reload({container:'#inv-person-grid-pjax'});
		} else {
		    ". SDNoty::show('result.message', 'result.status') ."
		}
	    }
	});
    });
}

$('#reportExel, #reportExel1').on('click', function() {
    var url = $(this).attr('data-url');
    selectionOvPersonExel(url);
});

$('#inv-person-grid-pjax').on('click', '.print-ezform', function() {
    var url = $(this).attr('data-url');
    modalEzfrom(url);
});

$('#inv-person-grid-pjax').on('dblclick', 'tbody tr', function() {
    var id = $(this).attr('data-key');
    modalEzfrom('".Url::to(['/inv/inv-person/ezform-print', 
		'ezf_id'=>$_GET['id'],
		'readonly'=>0,
		'dataid'=>''])."'+id);
});	

function modalEzfrom(url) {
    $('#modal-print .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
    $('#modal-print').modal('show');
    $.ajax({
	method: 'POST',
	url: url,
	dataType: 'HTML',
	success: function(result, textStatus) {
	    $('#modal-print .modal-content').html(result);
	    return false;
	}
    });
}

function selectionOvPersonExel(url) {
    $('#modal-ov-person .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
    $('#modal-ov-person').modal('show');
    $.ajax({
	method: 'POST',
	url: url,
	dataType: 'JSON',
	success: function(result, textStatus) {
	    if(result.status == 'success') {
		". SDNoty::show('result.message', 'result.status') ."
		$('#modal-ov-person .modal-content').html(result.html);
		
		$('#modal-ov-person').modal('hide');
	    } else {
		". SDNoty::show('result.message', 'result.status') ."
	    }
	}
    });
}

");?>