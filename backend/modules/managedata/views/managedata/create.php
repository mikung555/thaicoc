<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\managedata\models\Managedata */

$this->title = Yii::t('backend', 'Create Managedata');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Managedatas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="managedata-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
