<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\managedata\models\TbData1 */

$this->title = Yii::t('backend', 'Update {modelClass}: ', [
    'modelClass' => 'Tb Data1',
]) . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Tb Data1s'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('backend', 'Update');
?>
<div class="tb-data1-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
