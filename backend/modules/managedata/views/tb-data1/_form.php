<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\lib\sdii\components\helpers\SDNoty;

/* @var $this yii\web\View */
/* @var $model backend\modules\managedata\models\TbData1 */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tb-data1-form">

    <?php $form = ActiveForm::begin(['id'=>$model->formName()]); ?>
	<div class="modal-header">
	    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	    <h4 class="modal-title" id="itemModalLabel">Tb Data1</h4>
	</div>

	<div class="modal-body">
		<?= $form->field($model, 'id')->textInput() ?>

		<?= $form->field($model, 'rstat')->textInput() ?>

		<?= $form->field($model, 'user_create')->textInput() ?>

		<?= $form->field($model, 'create_date')->textInput() ?>

		<?= $form->field($model, 'user_update')->textInput() ?>

		<?= $form->field($model, 'update_date')->textInput() ?>

		<?= $form->field($model, 'value1')->textInput(['maxlength' => true]) ?>

		<?= $form->field($model, 'value2')->textInput(['maxlength' => true]) ?>

		<?= $form->field($model, 'value3')->textInput(['maxlength' => true]) ?>

		<?= $form->field($model, 'edattype')->textInput() ?>

	</div>
	<div class="modal-footer">
	    <?= Html::submitButton($model->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
	</div>

    <?php ActiveForm::end(); ?>

</div>

<?php  $this->registerJs("
$('form#{$model->formName()}').on('beforeSubmit', function(e){
    var \$form = $(this);
    $.post(
	\$form.attr('action'), //serialize Yii2 form
	\$form.serialize()
    ).done(function(result){
	if(result.status == 'success'){
	    ". SDNoty::show('result.message', 'result.status') ."
	    if(result.action == 'create'){
		$(\$form).trigger('reset');
		$.pjax.reload({container:'#tb-data1-grid-pjax'});
	    } else if(result.action == 'update'){
		$(document).find('#modal-tb-data1').modal('hide');
		$.pjax.reload({container:'#tb-data1-grid-pjax'});
	    }
	} else{
	    ". SDNoty::show('result.message', 'result.status') ."
	} 
    }).fail(function(){
	console.log('server error');
    });
    return false;
});

");?>