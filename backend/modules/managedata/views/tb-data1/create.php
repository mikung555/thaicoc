<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\managedata\models\TbData1 */

$this->title = Yii::t('backend', 'Create Tb Data1');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Tb Data1s'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tb-data1-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
