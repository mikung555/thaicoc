<?php
 
namespace backend\modules\ezformreports\controllers;
use backend\models\EzformTarget;
use backend\models\QueryManager;
use backend\models\QueryRequest;
use backend\modules\ckdnet\classes\CkdnetFunc;
use backend\modules\component\models\EzformComponent;
use backend\modules\ezforms\components\EzformFunc;
use backend\modules\ezforms\components\EzformQuery;
use backend\modules\ezforms\models\Ezform;
use backend\modules\ezforms\models\EzformChoice;
use backend\modules\ezforms\models\EzformDynamic;
use backend\modules\ezforms\models\EzformReply;
use backend\modules\ovcca\classes\OvccaFunc;
use common\lib\codeerror\helpers\GenMillisecTime;
use yii\data\ActiveDataProvider;
use yii\db\Exception;
use yii\db\Expression;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\helpers\VarDumper;
use yii\web\Controller;
use backend\models\InputDataSearch;
use yii\web\NotFoundHttpException;
use yii\web\Request;
use Yii;
use backend\modules\ezforms\models\EzformFields;
use yii\web\Response;
use yii\web\UploadedFile;
use backend\modules\inv\models\TbdataAll;
use backend\modules\ezformreports\classets\EzformPrint; 

class InputdataController extends Controller{
  public function actionPrintNew(){
    $dataid = \Yii::$app->request->get('dataid', '');
    $ezf_id = \Yii::$app->request->get('id', '');
    $message = \Yii::$app->request->get('message', '');
    
    $ezform = EzformPrint::getEzform($ezf_id); //select ezform where ezf_id='1437377239070462301'
    $ezf_table = $ezform["ezf_table"]; // table ของการเก็บข้อมูล
    $title = $ezform["ezf_name"]; //ชื่อ ezform
    
    $ezf_field = EzformPrint::getEzformFields($ezf_id);//ezform field
    
    //\appxq\sdii\utils\VarDumper::dump($dataid);
    
    return $this->renderPartial("_print-new",[
       'title'=>$title,
       'ezf_field'=>$ezf_field,
       'table'=>$ezf_table,
       'dataid'=>$dataid
    ]);
  }  
  public function actionPrintform($id, $message = '')
    {
 
        $modelform = InputDataSearch::findOne($id);
         
//        $pkJoin = 'target';    
//        if($ezform['comp_id_target']) {
//            $ezform_comp = Yii::$app->db->createCommand("SELECT * FROM ezform_component WHERE comp_id=:comp_id;", 
//                    [':comp_id'=>$modelform->comp_id_target])->queryOne();
//            if ($ezform_comp['special'] == 1) {
//                $pkJoin = 'ptid';
//            }
//        }
//      echo $modelform->target;
        
        $modelfield = EzformFields::find()
            ->where('ezf_id = :ezf_id', [':ezf_id' => $modelform->ezf_id])
            ->orderBy(['ezf_field_order' => SORT_ASC])
            ->all();
        if (isset($_GET['dataid'])) {
            $modelDynamic = new EzformDynamic($modelform->ezf_table);
            $model_table = $modelDynamic->find()->where('id = :id', [':id' => Yii::$app->request->get('dataid')])->One();
            $model_gen = \backend\modules\ezforms\components\EzformFunc::setDynamicModelLimit($modelfield);
            $model_gen->attributes = $model_table->attributes;
        }else {
            $model_gen = \backend\modules\ezforms\components\EzformFunc::setDynamicModelLimit($modelfield);
        }
        //VarDumper::dump($model_gen->attributes, 10, true);
        //Yii::$app->end();

        $textProvince = 'จ.#area1# อ.#area2# ต.#area3#'; //set value
        foreach($modelfield as $fields){
            $ezf_field_name = $fields->ezf_field_name;
            if($fields->ezf_field_type==0 AND $fields->ezf_field_sub_id != ''){
                $EzformFields = EzformFields::find()->select('ezf_field_type, ezf_field_id, ezf_field_name, ezf_field_label')->where('ezf_field_id = :ezf_field_id', [':ezf_field_id' => $fields['ezf_field_sub_id']])->one();
                if($EzformFields->ezf_field_type==19){
                    //single checkbox
                }
            }else if($fields->ezf_field_type==4 || $fields->ezf_field_type==6){
                //4=radio, 6=select
                $ezformChoice = EzformChoice::find()->where('ezf_field_id = :ezf_field_id AND ezf_choicevalue = :ezf_choicevalue', [':ezf_field_id' => $fields->ezf_field_id, ':ezf_choicevalue' => $model_gen->{$ezf_field_name}])->one();
                //VarDumper::dump($ezformChoice, 10, true);
                //Yii::$app->end();
                $model_gen->{$ezf_field_name} = $ezformChoice->ezf_choicelabel;
            }else if($fields->ezf_field_type==7){
                if($model_gen->{$ezf_field_name}+0) {
                    $explodeDate = explode('-', $model_gen->{$ezf_field_name});
                    $formateDate = $explodeDate[2] . "/" . $explodeDate[1] . "/" . ($explodeDate[0] + 543);
                    $model_gen->{$ezf_field_name} = $formateDate;
                }
            }else if($fields->ezf_field_type==10){
                //conponent
                $comp = EzformComponent::find()->where('comp_id = :comp_id', [':comp_id' => $fields->ezf_component])->one();
                $ezform = EzformQuery::getFormTableName($comp->ezf_id);
                $field_id_key = EzformFields::find()->select('ezf_field_name')->where('ezf_field_id = :ezf_field_id', [':ezf_field_id'=>$comp->field_id_key])->one();
                $field_id_desc = explode(',', $comp->field_id_desc);
                $fields='';
                foreach($field_id_desc as $val){
                    $fieldx = EzformFields::find()->select('ezf_field_name')->where('ezf_field_id = :ezf_field_id', [':ezf_field_id'=>$val])->one();
                    $fields .= $fieldx->ezf_field_name.', ';
                }
                $fields = substr($fields, 0, -2);

                $res = Yii::$app->db->createCommand("SELECT ".$fields." FROM ".$ezform->ezf_table." WHERE ".$field_id_key->ezf_field_name." = '".$model_gen->$ezf_field_name."';'")->queryOne();
                $text = '';
                foreach($res as $val){
                    $text .= $val.' ';
                }
                //VarDumper::dump($text);
                $model_gen->{$ezf_field_name} = $text;
                
                
            }else if($fields->ezf_field_type==11){
                //snomed
            }else if($fields->ezf_field_type==13){
                $fieldx = EzformFields::find()->select('ezf_field_name, ezf_field_label')->where('ezf_field_sub_id = :ezf_field_sub_id', [':ezf_field_sub_id'=>$fields->ezf_field_id])->all();
                 
                foreach($fieldx as $val) {
                    $ezf_field_namex = $val->ezf_field_name;
                    
                    //Province, amphur, tombon
                    if ($val->ezf_field_label == 1) {
                        $sql = "SELECT `PROVINCE_ID`, `PROVINCE_CODE`,`PROVINCE_NAME` FROM `const_province` WHERE PROVINCE_CODE='" . $model_gen->$ezf_field_namex . "'";
                        $res = Yii::$app->db->createCommand($sql)->queryOne();
                         $textProvince = str_replace('#area1#', $res['PROVINCE_NAME'], $textProvince);

                    } else if ($val->ezf_field_label == 2) {
                        $sql = "SELECT AMPHUR_NAME FROM `const_amphur` WHERE AMPHUR_CODE='" . $model_gen->{$ezf_field_namex} . "'";
                        $res = Yii::$app->db->createCommand($sql)->queryOne();
                         $textProvince = str_replace('#area2#', $res['AMPHUR_NAME'], $textProvince);

                    } else if ($val->ezf_field_label == 3) {
                        $sql = "SELECT DISTRICT_NAME FROM `const_district` WHERE DISTRICT_CODE='" . $model_gen->{$ezf_field_namex} . "'";
                        $res = Yii::$app->db->createCommand($sql)->queryOne();
                         $textProvince = str_replace('#area3#', $res['DISTRICT_NAME'], $textProvince);
                        $modelfield->{$ezf_field_namex} = $textProvince;
                        //echo $modelfield->$ezf_field_namex;
                    }
                }
                   $modelfield->$ezf_field_id = $textProvince; 
                 $textProvince = 'จ.#area1# อ.#area2# ต.#area3#';
            }else if($fields->ezf_field_type==14){
                //fileinputValidate
            }else if($fields->ezf_field_type==17){
                //activeHospitalSave
            }
            else if($fields->ezf_field_type==27){
                //activeIcd9Save
                $res = Yii::$app->db->createCommand("SELECT concat(code, ' : ',  name) as name FROM icd9 WHERE code=:code", [':code'=>$model_gen->{$ezf_field_name}])->queryOne();
                $model_gen->{$ezf_field_name} = $res['name'];
            }
            else if($fields->ezf_field_type==28){
                //activeIcd9Save
                $res = Yii::$app->db->createCommand("SELECT concat(code, ' : ',  name) as name FROM icd10 WHERE code=:code", [':code'=>$model_gen->{$ezf_field_name}])->queryOne();
                $model_gen->{$ezf_field_name} = $res['name'];
            }

        }
        
       // \yii\helpers\VarDumper::dump($model_gen->attributes, 10, true);
       
        return $this->renderPartial('print2', [
            'modelform' => $modelform,
            'modelfield' => $modelfield,
            'message' => $message,
            'target'=>$target,
            'id'=>$id,
            'dataid'=>Yii::$app->request->get('dataid'),
            'model_gen' => $model_gen]);
    }
    
    public function actionPrintform2()
    {
       // \appxq\sdii\utils\VarDumper::dump($_GET);
        $id = $_GET['id'];
        $message=$_GET['message'];
        $ezf_id = $_GET['ezf_id'];
        $select = $_GET['select'];
        
        //$modelform = $this->findModelEzform($id);
         $modelform = InputDataSearch::findOne($id);
 
        
        $modelfield = EzformFields::find()
            ->where('ezf_id = :ezf_id', [':ezf_id' => $modelform->ezf_id])
            ->orderBy(['ezf_field_order' => SORT_ASC])
            ->all();
        if (isset($_GET['dataid'])) {
            $modelDynamic = new EzformDynamic($modelform->ezf_table);
            $model_table = $modelDynamic->find()->where('id = :id', [':id' => Yii::$app->request->get('dataid')])->One();
            $model_gen = \backend\modules\ezforms\components\EzformFunc::setDynamicModelLimit($modelfield);
            $model_gen->attributes = $model_table->attributes;
        }else {
            $model_gen = \backend\modules\ezforms\components\EzformFunc::setDynamicModelLimit($modelfield);
        }
     
        $textProvince = 'จ.#area1# อ.#area2# ต.#area3#'; //set value
        foreach($modelfield as $fields){
            $ezf_field_name = $fields->ezf_field_name;
            if($fields->ezf_field_type==0 AND $fields->ezf_field_sub_id != ''){
                $EzformFields = EzformFields::find()->select('ezf_field_type, ezf_field_id, ezf_field_name, ezf_field_label')->where('ezf_field_id = :ezf_field_id', [':ezf_field_id' => $fields['ezf_field_sub_id']])->one();
                if($EzformFields->ezf_field_type==19){
                    //single checkbox
                }
            }else if($fields->ezf_field_type==4 || $fields->ezf_field_type==6){
                //4=radio, 6=select
                $ezformChoice = EzformChoice::find()->where('ezf_field_id = :ezf_field_id AND ezf_choicevalue = :ezf_choicevalue', [':ezf_field_id' => $fields->ezf_field_id, ':ezf_choicevalue' => $model_gen->{$ezf_field_name}])->one();
                //VarDumper::dump($ezformChoice, 10, true);
                //Yii::$app->end();
                $model_gen->{$ezf_field_name} = $ezformChoice->ezf_choicelabel;
            }else if($fields->ezf_field_type==7){
                if($model_gen->{$ezf_field_name}+0) {
                    $explodeDate = explode('-', $model_gen->{$ezf_field_name});
                    $formateDate = $explodeDate[2] . "/" . $explodeDate[1] . "/" . ($explodeDate[0] + 543);
                    $model_gen->{$ezf_field_name} = $formateDate;
                }
            }else if($fields->ezf_field_type==10){
                //conponent
                $comp = EzformComponent::find()->where('comp_id = :comp_id', [':comp_id' => $fields->ezf_component])->one();
                $ezform = EzformQuery::getFormTableName($comp->ezf_id);
                $field_id_key = EzformFields::find()->select('ezf_field_name')->where('ezf_field_id = :ezf_field_id', [':ezf_field_id'=>$comp->field_id_key])->one();
                $field_id_desc = explode(',', $comp->field_id_desc);
                $fields='';
                foreach($field_id_desc as $val){
                    $fieldx = EzformFields::find()->select('ezf_field_name')->where('ezf_field_id = :ezf_field_id', [':ezf_field_id'=>$val])->one();
                    $fields .= $fieldx->ezf_field_name.', ';
                }
                $fields = substr($fields, 0, -2);

                $res = Yii::$app->db->createCommand("SELECT ".$fields." FROM ".$ezform->ezf_table." WHERE ".$field_id_key->ezf_field_name." = '".$model_gen->$ezf_field_name."';'")->queryOne();
                $text = '';
                foreach($res as $val){
                    $text .= $val.' ';
                }
                //VarDumper::dump($text);
                $model_gen->{$ezf_field_name} = $text;
            }else if($fields->ezf_field_type==11){
                //snomed
            }else if($fields->ezf_field_type==13){
                $fieldx = EzformFields::find()->select('ezf_field_name, ezf_field_label')->where('ezf_field_sub_id = :ezf_field_sub_id', [':ezf_field_sub_id'=>$fields->ezf_field_id])->all();
                 
                foreach($fieldx as $val) {
                    $ezf_field_namex = $val->ezf_field_name;
                    
                    //Province, amphur, tombon
                    if ($val->ezf_field_label == 1) {
                        $sql = "SELECT `PROVINCE_ID`, `PROVINCE_CODE`,`PROVINCE_NAME` FROM `const_province` WHERE PROVINCE_CODE='" . $model_gen->$ezf_field_namex . "'";
                        $res = Yii::$app->db->createCommand($sql)->queryOne();
                         $textProvince = str_replace('#area1#', $res['PROVINCE_NAME'], $textProvince);

                    } else if ($val->ezf_field_label == 2) {
                        $sql = "SELECT AMPHUR_NAME FROM `const_amphur` WHERE AMPHUR_CODE='" . $model_gen->{$ezf_field_namex} . "'";
                        $res = Yii::$app->db->createCommand($sql)->queryOne();
                         $textProvince = str_replace('#area2#', $res['AMPHUR_NAME'], $textProvince);

                    } else if ($val->ezf_field_label == 3) {
                        $sql = "SELECT DISTRICT_NAME FROM `const_district` WHERE DISTRICT_CODE='" . $model_gen->{$ezf_field_namex} . "'";
                        $res = Yii::$app->db->createCommand($sql)->queryOne();
                         $textProvince = str_replace('#area3#', $res['DISTRICT_NAME'], $textProvince);
                        $modelfield->{$ezf_field_namex} = $textProvince;
                        //echo $modelfield->$ezf_field_namex;
                    }
                }
                   $modelfield->$ezf_field_id = $textProvince; 
                 $textProvince = 'จ.#area1# อ.#area2# ต.#area3#';
            }else if($fields->ezf_field_type==14){
                //fileinputValidate
            }else if($fields->ezf_field_type==17){
                //activeHospitalSave
            }
            else if($fields->ezf_field_type==27){
                //activeIcd9Save
                $res = Yii::$app->db->createCommand("SELECT concat(code, ' : ',  name) as name FROM icd9 WHERE code=:code", [':code'=>$model_gen->{$ezf_field_name}])->queryOne();
                $model_gen->{$ezf_field_name} = $res['name'];
            }
            else if($fields->ezf_field_type==28){
                //activeIcd9Save
                $res = Yii::$app->db->createCommand("SELECT concat(code, ' : ',  name) as name FROM icd10 WHERE code=:code", [':code'=>$model_gen->{$ezf_field_name}])->queryOne();
                $model_gen->{$ezf_field_name} = $res['name'];
            }

        }
       // \appxq\sdii\utils\VarDumper::dump($_GET);
       // \yii\helpers\VarDumper::dump($model_gen->attributes, 10, true);
        return $this->renderPartial('print3', [
            'modelform' => $modelform,
            'modelfield' => $modelfield,
            'message' => $message,
            'target'=>$target,
            'id'=>$id,
            'dataid'=>Yii::$app->request->get('dataid'),
            'model_gen' => $model_gen]);
    
    }
    
    
    
    //////
    public function actionPrintformV2()
    {
       // \appxq\sdii\utils\VarDumper::dump($_GET);
        $id = $_GET['id'];
        $message=$_GET['message'];
//      \appxq\sdii\utils\VarDumper::dump($_GET);
        //$modelform = $this->findModelEzform($id);
        $modelform = InputDataSearch::findOne($id);
        $modelfield = EzformFields::find()
            ->where('ezf_id = :ezf_id', [':ezf_id' => $modelform->ezf_id])
            ->orderBy(['ezf_field_order' => SORT_ASC])
            ->all();
        if (isset($_GET['dataid'])) {
            $modelDynamic = new EzformDynamic($modelform->ezf_table);
            $model_table = $modelDynamic->find()->where('id = :id', [':id' => Yii::$app->request->get('dataid')])->One();
            $model_gen = \backend\modules\ezforms\components\EzformFunc::setDynamicModelLimit($modelfield);
            $model_gen->attributes = $model_table->attributes;
        }else {
            $model_gen = \backend\modules\ezforms\components\EzformFunc::setDynamicModelLimit($modelfield);
        }
        //VarDumper::dump($model_gen->attributes, 10, true);
        //Yii::$app->end();

        $textProvince = 'จ.#area1# อ.#area2# ต.#area3#'; //set value
        foreach($modelfield as $fields){
            $ezf_field_name = $fields->ezf_field_name;
            if($fields->ezf_field_type==0 AND $fields->ezf_field_sub_id != ''){
                $EzformFields = EzformFields::find()->select('ezf_field_type, ezf_field_id, ezf_field_name, ezf_field_label')->where('ezf_field_id = :ezf_field_id', [':ezf_field_id' => $fields['ezf_field_sub_id']])->one();
                if($EzformFields->ezf_field_type==19){
                    //single checkbox
                }
            }else if($fields->ezf_field_type==4 || $fields->ezf_field_type==6){
                //4=radio, 6=select
                $ezformChoice = EzformChoice::find()->where('ezf_field_id = :ezf_field_id AND ezf_choicevalue = :ezf_choicevalue', [':ezf_field_id' => $fields->ezf_field_id, ':ezf_choicevalue' => $model_gen->{$ezf_field_name}])->one();
                //VarDumper::dump($ezformChoice, 10, true);
                //Yii::$app->end();
                $model_gen->{$ezf_field_name} = $ezformChoice->ezf_choicelabel;
            }else if($fields->ezf_field_type==7){
                if($model_gen->{$ezf_field_name}+0) {
                    $explodeDate = explode('-', $model_gen->{$ezf_field_name});
                    $formateDate = $explodeDate[2] . "/" . $explodeDate[1] . "/" . ($explodeDate[0] + 543);
                    $model_gen->{$ezf_field_name} = $formateDate;
                }
            }else if($fields->ezf_field_type==10){
                //conponent
                $comp = EzformComponent::find()->where('comp_id = :comp_id', [':comp_id' => $fields->ezf_component])->one();
                $ezform = EzformQuery::getFormTableName($comp->ezf_id);
                $field_id_key = EzformFields::find()->select('ezf_field_name')->where('ezf_field_id = :ezf_field_id', [':ezf_field_id'=>$comp->field_id_key])->one();
                $field_id_desc = explode(',', $comp->field_id_desc);
                $fields='';
                foreach($field_id_desc as $val){
                    $fieldx = EzformFields::find()->select('ezf_field_name')->where('ezf_field_id = :ezf_field_id', [':ezf_field_id'=>$val])->one();
                    $fields .= $fieldx->ezf_field_name.', ';
                }
                $fields = substr($fields, 0, -2);

                $res = Yii::$app->db->createCommand("SELECT ".$fields." FROM ".$ezform->ezf_table." WHERE ".$field_id_key->ezf_field_name." = '".$model_gen->$ezf_field_name."';'")->queryOne();
                $text = '';
                foreach($res as $val){
                    $text .= $val.' ';
                }
                //VarDumper::dump($text);
                $model_gen->{$ezf_field_name} = $text;
            }else if($fields->ezf_field_type==11){
                //snomed
            }else if($fields->ezf_field_type==13){
                
                $fieldx = EzformFields::find()->select('ezf_field_name, ezf_field_label')->where('ezf_field_sub_id = :ezf_field_sub_id', [':ezf_field_sub_id'=>$fields->ezf_field_id])->all();

                foreach($fieldx as $val) {
                    $ezf_field_namex = $val->ezf_field_name;
                    //Province, amphur, tombon
                    if ($val->ezf_field_label == 1) {
                        $sql = "SELECT `PROVINCE_ID`, `PROVINCE_CODE`,`PROVINCE_NAME` FROM `const_province` WHERE PROVINCE_CODE='" . $model_gen->$ezf_field_namex . "'";
                        $res = Yii::$app->db->createCommand($sql)->queryOne();
                        $textProvince = str_replace('#area1#', $res['PROVINCE_NAME'], $textProvince);

                    } else if ($val->ezf_field_label == 2) {
                        $sql = "SELECT AMPHUR_NAME FROM `const_amphur` WHERE AMPHUR_CODE='" . $model_gen->{$ezf_field_namex} . "'";
                        $res = Yii::$app->db->createCommand($sql)->queryOne();
                        $textProvince = str_replace('#area2#', $res['AMPHUR_NAME'], $textProvince);

                    } else if ($val->ezf_field_label == 3) {
                        $sql = "SELECT DISTRICT_NAME FROM `const_district` WHERE DISTRICT_CODE='" . $model_gen->{$ezf_field_namex} . "'";
                        $res = Yii::$app->db->createCommand($sql)->queryOne();
                        $textProvince = str_replace('#area3#', $res['DISTRICT_NAME'], $textProvince);
                        $modelfield->{$ezf_field_namex} = $textProvince;
                         
                    }
                }
                //$modelfield->$ezf_field_id = $textProvince;
                $textProvince = 'จ.#area1# อ.#area2# ต.#area3#';
            }else if($fields->ezf_field_type==14){
                //fileinputValidate
            }else if($fields->ezf_field_type==17){
                //activeHospitalSave
            }
            else if($fields->ezf_field_type==27){
                //activeIcd9Save
                $res = Yii::$app->db->createCommand("SELECT concat(code, ' : ',  name) as name FROM icd9 WHERE code=:code", [':code'=>$model_gen->{$ezf_field_name}])->queryOne();
                $model_gen->{$ezf_field_name} = $res['name'];
            }
            else if($fields->ezf_field_type==28){
                //activeIcd9Save
                $res = Yii::$app->db->createCommand("SELECT concat(code, ' : ',  name) as name FROM icd10 WHERE code=:code", [':code'=>$model_gen->{$ezf_field_name}])->queryOne();
                $model_gen->{$ezf_field_name} = $res['name'];
            }

        }
       // \appxq\sdii\utils\VarDumper::dump($_GET);
       // \yii\helpers\VarDumper::dump($model_gen->attributes, 10, true);
        return $this->renderPartial('v3', [
            'modelform' => $modelform,
            'modelfield' => $modelfield,
            'message' => $message,
            'select'=>Yii::$app->request->get('select'),
            'model_gen' => $model_gen]);
    }
}
