<?php
namespace backend\modules\ezformreports\controllers;
use Yii;
use common\models\EzformReportEditor;
use backend\modules\ezformreports\classets\EzformReportClass;

use backend\models\EzformTarget;
use backend\models\QueryManager;
use backend\models\QueryRequest;
use backend\modules\ckdnet\classes\CkdnetFunc;
use backend\modules\component\models\EzformComponent;
use backend\modules\ezforms\components\EzformFunc;
use backend\modules\ezforms\components\EzformQuery;
use backend\modules\ezforms\models\Ezform;
use backend\modules\ezforms\models\EzformChoice;
use backend\modules\ezforms\models\EzformDynamic;
use backend\modules\ezforms\models\EzformReply;
use backend\modules\ovcca\classes\OvccaFunc;
use common\lib\codeerror\helpers\GenMillisecTime;
use yii\data\ActiveDataProvider;
use yii\db\Exception;
use yii\db\Expression;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\helpers\VarDumper;
use yii\web\Controller;
use backend\models\InputDataSearch;
use yii\web\NotFoundHttpException;
use yii\web\Request;
use backend\modules\ezforms\models\EzformFields;
use yii\web\Response;
use yii\web\UploadedFile;
use backend\modules\inv\models\TbdataAll;
use backend\modules\ezformreports\classets\EzformPrint;

use backend\modules\ezformreports\classets\EzformType15;
use backend\modules\ezformreports\classets\EzformType10;
use backend\modules\ezformreports\classets\EzformType13;
use backend\modules\ezformreports\classets\EzformType14;


class EzformCustomReportController extends \yii\web\Controller{
      public function actionEzformVar() {
          
          $ezf_id=\Yii::$app->request->get("ezf_id","");
          $ezform_field =  EzformReportClass::getEzformField($ezf_id);
          return $this->renderAjax("ezform-var", ["model"=>$ezform_field]);
      }//modal variable ezform 
      public function actionSavEzformReportEditor(){
        \appxq\sdii\utils\VarDumper::dump($_POST);
      }
      
      public function actionReportEzformCustom(){
          $ezf_id = \Yii::$app->request->get('ezf_id','');
          $post = \Yii::$app->request->post();
          if(!empty($post)){
              
              $ezf_report_name = $post["ezf_report_name"];//\yii\helpers\Json::encode(["ezf_report_name"=>$post["ezf_report_name"]]);
              $model = EzformReportEditor::find()->where(["ezf_id"=>$ezf_id])->one();
              
              if(!empty($model)){
                 
                  //update
                  if(md5($model->ezf_report_name) != md5($ezf_report_name)){
                      $update = EzformReportClass::getUpdateEzformReport($ezf_id, $ezf_report_name, $model->id);
                        echo EzformReportClass::getResponse($update);
                  }
                  
                  
              }else{
                  //echo $ezf_report_name;
                  $save = EzformReportClass::getSaveEzformReport($ezf_id, $ezf_report_name);
                  echo EzformReportClass::getResponse($save);
              }
              return false;
          }else{
              $model = EzformReportEditor::find()->where(["ezf_id"=>$ezf_id])->one();
              if(empty($model)){
               $model = new EzformReportEditor(); 
              }
              //$model->ezf_report_name = \yii\helpers\Json::decode($modelezf_report_name)["ezf_report_name"];
                //$model->ezf_report_name = base64_decode($model["ezf_report_name"]);
                
                //\appxq\sdii\utils\VarDumper::dump($model);     
              return $this->renderAjax("_form",[
                 "model"=>$model
               ]);
          }
          
         
      }//save ezform custom report to database
      
      public function actionPrint(){
       
        
        $ezf_id = \Yii::$app->request->get("id", "");
        $dataid = \Yii::$app->request->get("dataid", "");
        if(empty($dataid) || $dataid == ""){
            echo "<h1 class='alert alert-danger' style=''>ยังไม่มีข้อมูลที่สามารถปริ้นได้</h1>";
            exit();
        }
        $modelform = \backend\models\InputDataSearch::findOne($ezf_id);

        $modelfield = EzformFields::find()
                ->where('ezf_id = :ezf_id', [':ezf_id' => $modelform->ezf_id])
                ->orderBy(['ezf_field_order' => SORT_ASC])
                ->all();


        if (isset($_GET['dataid'])) {
            $modelDynamic = new EzformDynamic($modelform->ezf_table); //demo tbdata_1467866743058143900
            //demo tbdata_1467866743058143900
            $model_table = $modelDynamic->find()->where('id = :id', [':id' => Yii::$app->request->get('dataid')])->One();

            $model_gen = \backend\modules\ezforms\components\EzformFunc::setDynamicModelLimit($modelfield);
            $model_gen->attributes = $model_table->attributes;
        } else {
            $model_gen = \backend\modules\ezforms\components\EzformFunc::setDynamicModelLimit($modelfield);
        }

        $template = EzformReportClass::getEzformReportEditor($ezf_id);
        $out = [];
        foreach ($modelfield as $field) {
            $ezf_field_name = $field->ezf_field_name;
            if($field->ezf_field_type == 10)//type 10 componetn
            {
                $ezf_component = $field->ezf_component;
                $out = EzformType10::getType10($field->ezf_id, $ezf_component, $out, $dataid,$field->ezf_field_name,$field);
                
                 //\appxq\sdii\utils\VarDumper::dump($out); 
            }

            //7,8,9 data time and time 
            if ($field->ezf_field_type == 1 || $field->ezf_field_type == 15 || 
                $field->ezf_field_type == 3 || $field->ezf_field_type == 7 || 
                $field->ezf_field_type == 8 || $field->ezf_field_type == 9 || 
                $field->ezf_field_type == 12 || $field->ezf_field_type == 26 || 
                $field->ezf_field_type==21 || $field->ezf_field_type==22 || $field->ezf_field_type==5) {
                 $out[$ezf_field_name] = $model_gen->$ezf_field_name;
                 if($field->ezf_field_type == 7){//Date
                    //$ddd = "2017-08-16"; //phpThDate2mysqlDate
                    if (!empty($model_gen->$ezf_field_name) || $model_gen->$ezf_field_name != null) {
                        $data = (string) \common\lib\sdii\components\utils\SDdate::mysql2phpDate($model_gen->$ezf_field_name);
                        $converts = explode("/", $data);
                        $year = (int) $converts[count($converts) - 1] + 543;
                        //echo $converts[0].'/'.$converts[1].'/'.$year;
                        $out[$ezf_field_name] = $converts[0] . '/' . $converts[1] . '/' . $year;
                    }
                    //\common\lib\sdii\components\utils\SDdate::phpThDate2mysqlDate($model_gen->$ezf_field_name);
                }
                if ($field->ezf_field_type == 9) {//dateTime
                    //$ddd = "2017-08-16 12:12:29"; //phpThDate2mysqlDate
                    $data = (string) \common\lib\sdii\components\utils\SDdate::mysql2phpDateTime($model_gen->$ezf_field_name);
                    $converts = explode("/", $data);
                    $converts2 = explode(" ", $converts[2]);
                    $year = (int) $converts2[0] + 543;
                    //appxq\sdii\utils\VarDumper::dump($converts2);
                    //echo $converts[0] . '/' . $converts[1] . '/' . $year . " " . $converts2[1];
                    $out[$ezf_field_name] = $converts[0] . '/' . $converts[1] . '/' . $year . " " . $converts2[1]; 
                }
                 
                 
            }
            if ($field->ezf_field_type == 14) {//FileUpload
                $ezf_field_sub_id = $field->ezf_field_id;
                $out = EzformType14::getType14($field->ezf_id, $field->ezf_field_name, $out, $dataid, 14);
                   
                 
            }
            if($field->ezf_field_type == 24){//Drawing
               $ezf_field_sub_id = $field->ezf_field_id;
               $out = EzformType14::getType14($field->ezf_id,$field->ezf_field_name, $out, $dataid, 24);
                
            }
            
            if ($field->ezf_field_type == 6) {//dropdown list radiobox 
                
                $ezf_field_id = $field->ezf_field_id; //ezf_field_id
                $ezf_choicevalue = $model_gen->$ezf_field_name; //value 
                $value = EzformReportClass::getEzformChoilce($ezf_field_id, $ezf_choicevalue);
                $out[$ezf_field_name] = $value["ezf_choicelabel"];
                
                
            }
//            if ($field->ezf_field_type ==4) {//dropdown list radiobox 
//                
//                $ezf_field_id = $field->ezf_field_id; //ezf_field_id
//                $ezf_choicevalue = $model_gen->$ezf_field_name; //value 
//                $value = EzformReportClass::getEzformChoilce($ezf_field_id, $ezf_choicevalue);
//                $out[$ezf_field_name] = $value["ezf_choicelabel"];
//                
//               // \appxq\sdii\utils\VarDumper::dump($out);
//            }
            if ($field->ezf_field_type == 16 || $field->ezf_field_type == 19) {//checkbox single 
                $html = "";
                if ($model_gen->$ezf_field_name) {

                    $html .= 'ใช่';
                } else {
                    $html .= '...';
                }
                $out[$ezf_field_name] = $html;
            }
            if ($field->ezf_field_type == 18) {//ref field

                //1
                $ref_table = $field->ezf_field_ref_table;
                $ref_field = $field->ezf_field_ref_field;
                //2 
                $ref_varname = EzformReportClass::getEzformRefVarName($ref_field)["ezf_field_name"];

                //3
                $ezf_table = EzformReportClass::getEzform($ref_table)["ezf_table"];

                $input_target = 'skip';
                $input_ezf_id= $ezf_id;
                $input_dataid = $dataid;
                
                $options_input = [];
                if ($field->ezf_field_val == 1) {
                    //แสดงอย่างเดียวแก้ไขไม่ได้ (Read-only)
                    //\appxq\sdii\utils\VarDumper::dump($field->ezf_field_val);
                    $modelDynamic = \backend\modules\ezforms\components\EzformQuery::getFormTableName($field->ezf_field_ref_table);
                    //set form disable
                    $options_input['readonly'] = true;
                    $form->attributes['rstat'] = 0;
                } else if ($field->ezf_field_val == 2) {
                    //ถ้ามีการแก้ไขค่า จะอัพเดจค่านั้นทั้งตารางต้นทาง - ปลายทาง
                    $modelDynamic = \backend\modules\ezforms\components\EzformQuery::getFormTableName($field->ezf_field_ref_table);//ezf_id
                    $form->attributes['rstat'] = $dataModel['rstat'];
                } else if ($field->ezf_field_val == 3) {
                    //แก้ไขเฉพาะตารางปลายทาง
                    $modelDynamic = \backend\modules\ezforms\components\EzformQuery::getFormTableName($field->ezf_field_ref_table);
                    //\yii\helpers\VarDumper::dump($ezfField, 10, true);
                    //echo $field->ezf_field_ref_field,'-';
                    $options_input['readonly'] = true;
                    $form->attributes['rstat'] = 0;
                }
                $ezfTable = $modelDynamic->ezf_table;
                 
                
                $ezfField = \backend\modules\ezforms\models\EzformFields::find()
                        ->select('ezf_field_name')
                        ->where('ezf_field_id = :ezf_field_id', 
                                [':ezf_field_id' => $field->ezf_field_ref_field])
                        ->one();
                
                if (!$ezfField->ezf_field_name) {
                    $ezfField = \backend\modules\ezforms\models\EzformFields::find()->select('ezf_field_name, ezf_field_label')->where('ezf_field_id = :ezf_field_id', [':ezf_field_id' => $field->ezf_field_id])->one();
                    $html = '<h1>Error</h1><hr>';
                    $html .= '<h3>เนื่องการเชื่อมโยงของประเภทคำถาม Reference field ผิดพลาด การแก้ไขคือ ลบคำถามออกแล้วสร้างใหม่</h3>';
                    $html .= '<h4>คำถามที่ผิดพลาดคือ : </h4>' . $ezfField->ezf_field_name . ' (' . $ezfField->ezf_field_label . ')';
                    echo $html;
                    Yii::$app->end();
                }
                $modelDynamic = new \backend\modules\ezforms\models\EzformDynamic($ezfTable);
                
                //กำหนดตัวแปร
                $field_name = $field->ezf_field_name; //field name ต้นทาง 
                $field_name_ref = $ezfField->ezf_field_name; //field name ปลายทาง
                
                  
                //ดูตาราง Ezform ฟิลด์ target ที่มีเป้าหมายเดียวกัน
                if ($input_target == 'byme' || $input_target == 'skip' || $input_target == 'all') {
                    $target = \backend\modules\ezforms\components\EzformQuery::getTargetFormEzf($input_ezf_id, $input_dataid);
                    $target = $target['ptid'];
                } else {
                    $target = base64_decode($input_target);
                }
                //หา target ใน site ตัวเองก่อน (ก็ไม่แนะนำ)
                $res = $modelDynamic->find()->select($field_name_ref)->where('ptid = :target AND ' . $field_name_ref . ' <> "" AND xsourcex = :xsourcex AND rstat <>3', [':target' => $target, ':xsourcex' => $dataModel['xsourcex']])->orderBy('create_date DESC');
                if ($res->count()) {
                    $model_table = $res->One();
                    
                    if ($field->ezf_field_val == 1) {
                        //echo 'target 1-1<br>';
                        //เปลี่ยนค่า field name ระหว่างต้นทาง และปลายทาง
                        $model_gen->$field_name = $model_table->$field_name_ref;
                        $model_gen->attributes[$field_name] = $model_table->$field_name_ref;
                    } else if ($field->ezf_field_val == 2) {
                        
                    } else if ($field->ezf_field_val == 3) {
                        if ($input_dataid) {
                            //echo 'target 1<br>';
                            $res = \backend\modules\ezforms\components\EzformQuery::getReferenceData($field->ezf_id, $field->ezf_field_id, $input_dataid, $field_name_ref);
                            //\yii\helpers\VarDumper::dump($model_table->attributes, 10,true);
                            if ($res[$field_name_ref]) {
                                $model_table->$field_name_ref = $res[$field_name_ref];
                            }
                            //เปลี่ยนค่า field name ระหว่างต้นทาง และปลายทาง
                            $model_gen->$field_name = $model_table->$field_name_ref;
                            $model_gen->attributes[$field_name] = $model_table->$field_name_ref;
                        } else {
                            //\yii\helpers\VarDumper::dump($model_table->attributes, 10,true);
                            // echo 'target else 1<br>';
                            //เปลี่ยนค่า field name ระหว่างต้นทาง และปลายทาง
                            $model_gen->$field_name = $model_table->$field_name_ref;
                            $model_gen->attributes[$field_name] = $model_table->$field_name_ref;
                        }
                    }
                } else {
                    //กรณีหาไม่เจอ ให้ดู primary key
                    $res = $modelDynamic->find()->where('id = :id', [':id' => $target]);
                   
                    if ($res->count()) {
                        $model_table = $res->One();

                        if ($field->ezf_field_val == 1) {
                            //echo 'target 2-1<br>';
                            //เปลี่ยนค่า field name ระหว่างต้นทาง และปลายทาง
                            $model_gen->$field_name = $model_table->$field_name_ref;
                            $model_gen->attributes[$field_name] = $model_table->$field_name_ref;
                        } else if ($field->ezf_field_val == 2) {
                            
                        } else if ($field->ezf_field_val == 3) {
                            if ($input_dataid) {
                                //echo 'target 2<br>';
                                $res = \backend\modules\ezforms\components\EzformQuery::getReferenceData($field->ezf_id, $field->ezf_field_id, $input_dataid, $field_name_ref);
                                if ($res[$field_name_ref]) {
                                    $model_table->$field_name_ref = $res[$field_name_ref];
                                }
                                //เปลี่ยนค่า field name ระหว่างต้นทาง และปลายทาง
                                $model_gen->$field_name = $model_table->$field_name_ref;
                                $model_gen->attributes[$field_name] = $model_table->$field_name_ref;
                            } else {
                                //echo 'target else 2<br>';
                                //\yii\helpers\VarDumper::dump($model_table->attributes, 10,true);
                                //เปลี่ยนค่า field name ระหว่างต้นทาง และปลายทาง
                                $model_gen->$field_name = $model_table->$field_name_ref;
                                $model_gen->attributes[$field_name] = $model_table->$field_name_ref;
                            }
                        }
                    }
                }
                $ezf_field_order = $field->ezf_field_order;
                $ezf_field_id = $field->ezf_field_id;
                $ezf_field_name = $field->ezf_field_name;
                $ezf_field_lenght = $field->ezf_field_lenght;
                
                $model = EzformFields::find()->where(['ezf_field_id' => $field->ezf_field_ref_field])->andWhere('ezf_field_type IS NOT NULL')->one();
		$res = \backend\modules\ezforms\components\EzformQuery::getReferenceData($ezf_id, $field->ezf_field_id, $input_dataid, $field_name_ref);
                    if ($res[$field_name_ref]) {
                         $out[$ref_varname] = $res[$field_name_ref];
                       // echo '<div id="showprint"><input type=\'checkbox\' name=\'select[]\' id=\'selects\' value=' . $field->ezf_field_id .'>' . $field->ezf_field_label . ' : <b>' . $res[$field_name_ref] . '</b></div>';
                    } else {
                         $out[$ref_varname] = $model_gen->$ezf_field_name;
                        //echo '<div id="showprint"><input type=\'checkbox\' name=\'select[]\' id=\'selects\' value='. $field->ezf_field_id . '>' . $field->ezf_field_label . ' : <b>' . $model_gen->$ezf_field_name . '</b></div>';
                    }	
                    
                //$field_item = \backend\modules\ezforms\components\EzformQuery::getInputId($field->ezf_field_type);    
                
                //echo $model->ezf_field_label ." | ".$field->ezf_field_ref_field;
                //Yii::$app->end();
                //echo $target.' -';
                //หาที่ target ก่อน
            }if ($field->ezf_field_type == 23 || $field->ezf_field_type ==4) {//Scale and radio box multi 
                
                $modelchoice = \backend\modules\ezforms\models\EzformChoice::find()
                                ->where('ezf_field_id = :ezf_field_id', [':ezf_field_id' => $field->ezf_field_id])->all();
                $modelsubfield = \backend\modules\ezforms\models\EzformFields::find()
                                ->where('ezf_field_sub_id = :ezf_field_sub_id', [':ezf_field_sub_id' => $field->ezf_field_id])->all();
                $varname = EzformReportClass::getEzformFieldOne($field->ezf_field_id);
              
                $i = 1;
                foreach ($modelsubfield as $subfield) {
                    $modelsubchoice = \backend\modules\ezforms\models\EzformChoice::find()->where('ezf_field_id = :ezf_field_id', [':ezf_field_id' => $subfield->ezf_field_sub_id])->all();
                    $html .= $i . ". " . $subfield["ezf_field_label"];
                    $j = 0;
                    foreach ($modelsubchoice as $subchoice) {
                        $ezf_field_name = $subfield["ezf_field_name"];
                        $html .= ($subchoice["ezf_choicevalue"] == $model_gen->$ezf_field_name ? '(' . $modelchoice[$j]['ezf_choicelabel'] . ')' : '');
                        $j++;
                    }

                    $i++;
                }
                $out[$varname["ezf_field_name"]] = $html;
                 
                if($field->ezf_field_type ==4){//radiobox multi 
                       
                     $table = EzformReportClass::getEzform($ezf_id)["ezf_table"];
                     
                     $data = EzformReportClass::getTBDATA($table, $dataid,$varname["ezf_field_name"]);
                     //SELECT * FROM ezform_choice WHERE ezf_field_id='1501823752003646600' AND ezf_choicevalue='3'
                     $data = EzformReportClass::getEzformChoilce($field->ezf_field_id, $data[$varname["ezf_field_name"]]);
                     
                     $out[$varname["ezf_field_name"]] = $data["ezf_choicelabel"];  
                     
                     //\appxq\sdii\utils\VarDumper::dump($out);
                     //$model_gen->$ezf_field_name;  $field->ezf_field_id
                    
                }
            }
           if ($field->ezf_field_type == 25) {

                //$html = "<table><tr><td>demodsdfsdf</td></tr></table>";
                //$out["vargrid1"] = $html;
                $ezform_fields = EzformType15::getType15EzformField($ezf_id);
                //$model = EzformType15::getType15EzformSubField($ezf_id, $ezf_field_sub_id);
                 
                foreach($ezform_fields as $key=>$value){
                    if($value["ezf_field_type"] == "25"){
                        $ezf_field_sub_id = $value["ezf_field_id"];
                        $modelHeader = EzformType15::getType15EzformSubFieldHead($ezf_id, $ezf_field_sub_id);
                        
                        for($i=0; $i<count($modelHeader); $i++){
                            $modelRows = EzformType15::getType15EzformSubFieldRows($ezf_id, $modelHeader[$i]["ezf_field_id"]);
                            
                            for($j=0; $j<count($modelRows); $j++){
                                 
                                if($modelRows[$j]["ezf_field_type"] == 251 || $modelRows[$j]["ezf_field_type"] == 252 || $modelRows[$j]["ezf_field_type"] == 253){
                                     //text and date
                                  
                                     $out_grid = EzformType15::getType15EzformTbData($ezf_id, $dataid, $modelRows[$j]["ezf_field_name"]);        
                                    //ezf_id ,dataid,  field , 
                                    
                                    $out[$modelRows[$j]["ezf_field_name"]] = $out_grid[$modelRows[$j]["ezf_field_name"]];
                                    //echo $out_grid[$modelRows[$j]["ezf_field_name"]]."<br>";
                                   
                                }else if($modelRows[$j]["ezf_field_type"] == 254){
                                    //single checkbox choices
                                    $out_grid = EzformType15::getType15EzformTbData($ezf_id, $dataid, $modelRows[$j]["ezf_field_name"]);        
                                    //ezf_id ,dataid,  field , 
                                    //$out[$modelRows[$j]["ezf_field_name"]] = $out_grid[$modelRows[$j]["ezf_field_name"]];
                                    $html = "";
                                        if ($out_grid[$modelRows[$j]["ezf_field_name"]]) {

                                            $html .= 'ใช่';
                                        } else {
                                            $html .= '...';
                                        }
                                   $out[$modelRows[$j]["ezf_field_name"]] = $html;
                                   //\appxq\sdii\utils\VarDumper::dump($out);
                                }
                                
                                 
                                
                            }
                        }
                    }
                }
                 
 
            }
            
            if($field->ezf_field_type == 13)//Province Amphur
            {
                 $ezf_field_sub_id = $field->ezf_field_id;
                 $out = EzformType13::getType13($field->ezf_id, $ezf_field_sub_id, $out, $dataid);
                 
            }
            if($field->ezf_field_type == 17)//Hospital list
            {
               $table = EzformReportClass::getEzform($field->ezf_id)["ezf_table"];  
               $host_value = EzformReportClass::getTBDATA($table, $dataid, $field->ezf_field_name);
               $h_value = \backend\modules\ezformreports\classets\EzformAddress::getHospital($host_value[$field->ezf_field_name])["name"];
               $out[$field->ezf_field_name] = $h_value;
  
            }
//            if($field->ezf_field_type==26){
//                //Map
//                
//            }
             if($field->ezf_field_type==27){
                //Icd9
                $res = Yii::$app->db->createCommand("SELECT concat(code, ' : ',  name) as name FROM icd9 WHERE code=:code", [':code'=>$model_gen->{$ezf_field_name}])->queryOne();
                $model_gen->{$ezf_field_name} = $res['name'];
                $out[$field->ezf_field_name]=$model_gen->$ezf_field_name;
                //
            }
             if($field->ezf_field_type==28){
                //Icd10
                $res = Yii::$app->db->createCommand("SELECT concat(code, ' : ',  name) as name FROM icd10 WHERE code=:code", [':code'=>$model_gen->{$ezf_field_name}])->queryOne();
                $model_gen->{$ezf_field_name} = $res['name'];
                $out[$field->ezf_field_name]=$model_gen->$ezf_field_name;
                //\appxq\sdii\utils\VarDumper::dump($out);
            }
            if($field->ezf_field_type==11){
                //Snomed
                $out[$ezf_field_name] = $model_gen->$ezf_field_name;
            }
            
             
        }//end foreach
         //1,3,15,7,8,9,12,6,16,19,18,23,4,25
         // \appxq\sdii\utils\VarDumper::dump($out);
        //$this->layout = "";
    
                 
         
        $title = EzformReportClass::getEzform($ezf_id)["ezf_name"];
        return $this->renderAjax("print", [
                    "title"=>$title,
                    "template" => $template,
                    "out" => Json::encode($out)
        ]);
    }
    
    
    public function actionCheckType(){
        $ezf_id = $_POST['ezf_id']; //ezf_id
        $ezf_field_name = $_POST["ezf_field_name"]; //รับ ezf_field_name มา 
        
        
        $sql="SELECT * FROM ezform_fields WHERE ezf_field_name=:ezf_field_name AND ezf_id=:ezf_id";
        
        $query=\Yii::$app->db->createCommand($sql,[":ezf_field_name"=>$ezf_field_name, ":ezf_id"=>$ezf_id])->queryOne();
        
        if($query["ezf_field_type"] == 18){
            $ezf_field_name = EzformReportClass::getEzformRefVarName($query["ezf_field_ref_field"])["ezf_field_name"];
        }

        
        echo $ezf_field_name;
    }

}
