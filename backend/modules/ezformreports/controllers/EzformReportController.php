<?php

namespace backend\modules\ezformreports\controllers;

use backend\modules\ezforms\components\EzformQuery;
use backend\modules\ezforms\models\EzformFields;
use Yii;
use backend\models\Ezform;
use yii\data\ActiveDataProvider;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;
use backend\modules\inv\classes\InvFunc;

class EzformReportController extends \yii\web\Controller {

    public function actionIndex() {
        return $this->render('index');
    }

    public function actionUser() {
        $user = \common\models\UserProfile::find()->limit(20)->all();

        $users = [];
        foreach ($user as $u) {
            $users[] = $user;
        }
        //\appxq\sdii\utils\VarDumper::dump($users);

        echo json_encode($users);
    }

    public function actionDataManagement($ezfdata) {

        $ezfdata = Yii::$app->db->createCommand("SELECT * FROM ezform_data_manage WHERE id = :id;", [':id' => $ezfdata])->queryOne();
        $searchModel = new \backend\models\search\DataManagementSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $ezfdata);
        $fParams = explode(',', $ezfdata['params']);

        //check component ezfrom
        $ezform = Yii::$app->db->createCommand("SELECT comp_id_target FROM ezform WHERE ezf_id = :ezf_id;", [':ezf_id' => $ezfdata['ezf_id']])->queryOne();

        //map choice type
        foreach ($fParams as $field) {
            //set value
            $ezchoice = [];

            $ezfield = Yii::$app->db->createCommand("select ezf_field_id, ezf_field_sub_id,  ezf_field_name, ezf_field_type, ezf_component from ezform_fields where ezf_id=:ezf_id and ezf_field_name=:ezf_field_name", [':ezf_id' => $ezfdata['ezf_id'], ':ezf_field_name' => $field])->queryOne();
            if ($ezfield['ezf_field_type'] == 4 || $ezfield['ezf_field_type'] == 6) {
                $ezchoice = Yii::$app->db->createCommand("select ezf_choicevalue as value_id, ezf_choicelabel as text from ezform_choice where ezf_field_id=:ezf_field_id", [':ezf_field_id' => $ezfield['ezf_field_id']])->queryAll();
            } else if ($ezfield['ezf_field_type'] == 10) {
                $ezformComponents = EzformQuery::getFieldComponent($ezfield['ezf_component']);
                $ezformComp = EzformQuery::getFormTableName($ezformComponents->ezf_id);
                $field_desc_array = explode(',', $ezformComponents->field_id_desc);
                $field_key = EzformFields::find()->select('ezf_field_name')->where(['ezf_field_id' => $ezformComponents->field_id_key])->one();

                $field_desc_list = '';
                if (count($field_desc_array)) {
                    foreach ($field_desc_array as $value) {
                        if (!empty($value)) {
                            $field_desc = EzformFields::find()->where(['ezf_field_id' => $value])->one();
                            $field_desc_list .= 'COALESCE(' . $field_desc->ezf_field_name . ", '-'), ' ',";
                        }
                    }
                }
                $field_desc_list = substr($field_desc_list, 0, -6);

                //set SQL
                $sql = 'SELECT ' . $field_key->ezf_field_name . ' as value_id, CONCAT(' . $field_desc_list . ') AS text FROM ' . ($ezformComp->ezf_table) . ' WHERE 1 ';
                if ($ezfield['ezf_component'] == 100000 || $ezfield['ezf_component'] == 100001) {
                    $sql .= " AND sitecode = '" . Yii::$app->user->identity->userProfile->sitecode . "'";
                }

                //if find by target
                if ($ezformComponents->find_target AND $ezformComponents->special) {
                    $sql .= " AND ptid = '" . base64_decode($_GET['target']) . "' AND rstat <> 3";
                } else if ($ezformComponents->find_target) {
                    $sql .= " AND target = '" . base64_decode($_GET['target']) . "' AND rstat <> 3";
                }

                //if find by site
                if ($ezformComponents->find_bysite) {
                    $sql .= " AND xsourcex = '" . Yii::$app->user->identity->userProfile->sitecode . "'";
                }
                $ezchoice = Yii::$app->db->createCommand($sql)->queryAll();
            } else if ($ezfield['ezf_field_type'] == 231) {
                $ezchoice = Yii::$app->db->createCommand("select ezf_choicevalue as value_id, ezf_choicelabel as text from ezform_choice where ezf_field_id=:ezf_field_id", [':ezf_field_id' => $ezfield['ezf_field_sub_id']])->queryAll();
            } else if ($ezfield['ezf_field_type'] == 16 || ($ezfield['ezf_field_type'] == 0 && $ezfield['ezf_field_sub_id'])) {
                $ezchoice = [
                    '0' => [
                        'value_id' => '1',
                        'text' => 'ใช่'
                    ],
                    '1' => [
                        'value_id' => '0',
                        'text' => 'ไม่ใช่'
                    ],
                ];
            }
            $filterType[$field] = ArrayHelper::map($ezchoice, 'value_id', 'text');
        }

        //VarDumper::dump($fParams,10,true); exit;
        return $this->render('data-management', [
                    'dataProvider' => $dataProvider,
                    'searchModel' => $searchModel,
                    'fParams' => $fParams,
                    'ezfdata' => $ezfdata,
                    'ezform' => $ezform,
                    'filterType' => $filterType
        ]);
    }

    public function actionEzformPrint() {
        $readonly = isset($_GET['readonly']) ? $_GET['readonly'] : 0;
        $end = isset($_GET['end']) ? $_GET['end'] : 0;
        $ezf_id = isset($_GET['ezf_id']) ? $_GET['ezf_id'] : 0;

        $main_ezf_id = isset($_GET['main_ezf_id']) ? $_GET['main_ezf_id'] : 0;
        $dataid = isset($_GET['dataid']) ? $_GET['dataid'] : NULL;
        $target = isset($_GET['target']) ? $_GET['target'] : NULL;
        $comp_id_target = isset($_GET['comp_id_target']) ? $_GET['comp_id_target'] : NULL;
        $comp_target = isset($_GET['comp_target']) ? $_GET['comp_target'] : '';
        $dataset = isset($_GET['dataset']) ? $_GET['dataset'] : null;

        try {

            $modelform = \backend\modules\ezforms\models\Ezform::find()->where('ezf_id = :ezf_id', [':ezf_id' => $ezf_id])->one();

            $comp_id_target = isset($_GET['comp_id_target']) ? $_GET['comp_id_target'] : $modelform->comp_id_target;

            if (isset($ezf_id) && isset($comp_id_target) && isset($target) && (!isset($dataid) || $dataid == '' || $dataid == 'null')) {
                $dataid = InvFunc::insertRecord([
                            'ezf_id' => $ezf_id,
                            'target' => $target,
                            'comp_id_target' => $comp_id_target,
                ]);
            } elseif (isset($ezf_id) && $comp_target == 'skip' && (!isset($dataid) || $dataid == '' || $dataid == 'null')) {
                $comp_id_target = NULL;
                $dataid = InvFunc::insertRecord([
                            'ezf_id' => $ezf_id,
                            'target' => 'skip',
                            'comp_id_target' => $comp_id_target,
                ]);
            }

            Yii::$app->session['ezform_main'] = $ezf_id;

            $modelfield = \backend\modules\ezforms\models\EzformFields::find()
                    ->where('ezf_id = :ezf_id', [':ezf_id' => $ezf_id])
                    ->orderBy(['ezf_field_order' => SORT_ASC])
                    ->all();

            $table = $modelform->ezf_table;

            $model_gen = \backend\modules\ezforms\components\EzformFunc::setDynamicModelLimit($modelfield);




            $dataComponent = [];
            if ($comp_id_target) {
                $ezform_comp = Yii::$app->db->createCommand("SELECT * FROM ezform_component WHERE comp_id=:comp_id;", [':comp_id' => $comp_id_target])->queryOne();

                if ($ezform_comp['special'] == 1) {
                    //หาเป้าหมายพิเศษประเภทที่ 1 (CASCAP) (Thaipalliative) (Ageing)

                    $dataComponent = InvFunc::specialTarget1($ezform_comp);

                    //$dataProvidertarget = \backend\models\InputDataSearch::searchEMR($target);
                } else {
                    //หาแบบปกติ
                    $dataComponent = InvFunc::findTargetComponent($ezform_comp);
                }
            } else {
                //target skip
                $dataComponent['sqlSearch'] = '';
                $dataComponent['tagMultiple'] = FALSE;
                $dataComponent['ezf_table_comp'] = '';
                $dataComponent['arr_comp_desc_field_name'] = '';
            }
            $sql_announce = false;
            $error = false;
            if (isset($dataid)) {
                $model = new \backend\models\Tbdata();
                $model->setTableName($table);

                $query = $model->find()->where('id=:id', [':id' => $dataid]);

                $data = $query->one();

                if (!isset($target)) {
                    $target = base64_encode($data->target);
                }

                $model_gen->attributes = $data->attributes;

                //SQL Check
                if ($modelform->sql_condition) {
                    $rst = Yii::$app->db->createCommand("select ezf_id from ezform WHERE ezf_id=:ezf_id and (`sql_condition` like '%update%' or `sql_condition` like '%delete%' or `sql_condition` like 'DROP%' or `sql_condition` like 'ALTER%' or `sql_condition` like '%TRUNCATE%' or `sql_condition` like '%remove%')", [':ezf_id' => $ezf_id])->queryScalar();
                    if (!$rst) {
                        $sql = str_replace('DATAID', $dataid, $modelform->sql_condition);
                        $res = Yii::$app->db->createCommand($sql);
                        //
                        try {
                            $chk = $res->query()->count() > 0 ? true : false;
                        } catch (Exception $exception) {
                            echo '<h1>Input data error</h1><hr>';
                            echo '<h2>' . $exception->getName() . '</h2>';
                            echo $exception->getMessage();
                            exit();
                        }
                        //
                        if ($chk) {
                            $res = $res->queryOne();
                            $error = '';
                            foreach ($res as $val) {
                                if (trim($val) != "")
                                    $error .= $val . '<br>';
                            }
                            Yii::$app->db->createCommand("UPDATE `" . $modelform->ezf_table . "` SET `error` = :error WHERE id = :id", ['error' => $error, ':id' => $dataid])->execute();
                        } else {
                            Yii::$app->db->createCommand("UPDATE `" . $modelform->ezf_table . "` SET `error` = '' WHERE id = :id", [':id' => $dataid])->execute();
                        }
                    } else {
                        return 'SQL Condition danger command';
                    }
                }

                //SQL Announce
                if ($modelform->sql_announce) {
                    $rst = Yii::$app->db->createCommand("select ezf_id from ezform WHERE ezf_id=:ezf_id and (`sql_announce` like '%update%' or `sql_announce` like '%delete%' or `sql_announce` like 'DROP%' or `sql_announce` like 'ALTER%' or `sql_announce` like 'TRUNCATE%' or `sql_announce` like 'remove%')", [':ezf_id' => $ezf_id])->queryScalar();
                    if (!$rst) {
                        $sql = str_replace('DATAID', $dataid, $modelform->sql_announce);
                        try {
                            $res = Yii::$app->db->createCommand($sql)->queryOne();
                        } catch (Exception $exception) {
                            echo '<h1>Input data error</h1><hr>';
                            echo '<h2>' . $exception->getName() . '</h2>';
                            echo $exception->getMessage();
                            exit();
                        }

                        if (count($res)) {
                            $sql_announce = '';
                            foreach ($res as $val) {
                                if (trim($val) != "")
                                    $sql_announce .= $val . '<br>';
                            }
                        }
                    }else {
                        return 'SQL Announce danger command';
                    }
                }
            }

            if (isset($dataset) && !empty($dataset)) {
                $dataSet = Json::decode(base64_decode($dataset));
                foreach ($dataSet as $key => $value) {
                    $model_gen->{$key} = $value;
                }
            }

            return $this->renderAjax('_ezform_print', [
                        'modelform' => $modelform,
                        'modelfield' => $modelfield,
                        'model_gen' => $model_gen,
                        'main_ezf_id' => $main_ezf_id,
                        'ezf_id' => $ezf_id,
                        'dataid' => $dataid,
                        'target' => $target,
                        'ezform_comp' => $ezform_comp,
                        'comp_id_target' => $comp_id_target,
                        'dataComponent' => $dataComponent,
                        'end' => $end,
                        'readonly' => $readonly,
                        'comp_target' => $comp_target,
                        'dataset' => $dataset,
                        'sql_announce' => $sql_announce,
                        'error' => $error,
            ]);
        } catch (\yii\db\Exception $e) {
            
        }
    }

    public function actionEzformPrintAuto() {
        $readonly = isset($_GET['readonly']) ? $_GET['readonly'] : 0;
        $end = isset($_GET['end']) ? $_GET['end'] : 0;
        $ezf_id = isset($_GET['ezf_id']) ? $_GET['ezf_id'] : 0;
        $main_ezf_id = isset($_GET['main_ezf_id']) ? $_GET['main_ezf_id'] : 0;
        $dataid = isset($_GET['dataid']) ? $_GET['dataid'] : NULL;
        $target = isset($_GET['target']) ? $_GET['target'] : NULL;
        $comp_id_target = isset($_GET['comp_id_target']) ? $_GET['comp_id_target'] : NULL;

        $params['comp_id'] = Yii::$app->request->get('comp_id_target');
        $params['target'] = self::actionGenForeigner($params['comp_id_target']); //cid
        $params['mode'] = 'gen-foreigner';

        self::actionInsertSpecial($params);

        try {

            $modelform = \backend\modules\ezforms\models\Ezform::find()->where('ezf_id = :ezf_id', [':ezf_id' => $ezf_id])->one();

            $comp_id_target = isset($_GET['comp_id_target']) ? $_GET['comp_id_target'] : $modelform->comp_id_target;

            if (isset($ezf_id) && isset($comp_id_target) && isset($target) && (!isset($dataid) || $dataid == '' || $dataid == 'null')) {
                $dataid = InvFunc::insertRecord([
                            'ezf_id' => $ezf_id,
                            'target' => $target,
                            'comp_id_target' => $comp_id_target,
                ]);
            }

            Yii::$app->session['ezform_main'] = $ezf_id;

            $modelfield = \backend\modules\ezforms\models\EzformFields::find()
                    ->where('ezf_id = :ezf_id', [':ezf_id' => $ezf_id])
                    ->orderBy(['ezf_field_order' => SORT_ASC])
                    ->all();

            $table = $modelform->ezf_table;

            $model_gen = \backend\modules\ezforms\components\EzformFunc::setDynamicModelLimit($modelfield);
            $ezform_comp = Yii::$app->db->createCommand("SELECT * FROM ezform_component WHERE comp_id=:comp_id;", [':comp_id' => $comp_id_target])->queryOne();


            $dataComponent = [];
            if (isset($comp_id_target)) {
                if ($ezform_comp['special'] == 1) {
                    //หาเป้าหมายพิเศษประเภทที่ 1 (CASCAP) (Thaipalliative) (Ageing)

                    $dataComponent = InvFunc::specialTarget1($ezform_comp);

                    //$dataProvidertarget = \backend\models\InputDataSearch::searchEMR($target);
                } else {
                    //หาแบบปกติ
                    $dataComponent = InvFunc::findTargetComponent($ezform_comp);
                }
            } else {
                //target skip
                $dataComponent['sqlSearch'] = '';
                $dataComponent['tagMultiple'] = FALSE;
                $dataComponent['ezf_table_comp'] = '';
                $dataComponent['arr_comp_desc_field_name'] = '';
            }



            if (isset($dataid)) {
                $model = new \backend\models\Tbdata();
                $model->setTableName($table);

                $query = $model->find()->where('id=:id', [':id' => $dataid]);

                $data = $query->one();

                if (!isset($target)) {
                    $target = base64_encode($data->target);
                }

                $model_gen->attributes = $data->attributes;
            }

            return $this->renderAjax('_ezform_print', [
                        'modelform' => $modelform,
                        'modelfield' => $modelfield,
                        'model_gen' => $model_gen,
                        'main_ezf_id' => $main_ezf_id,
                        'ezf_id' => $ezf_id,
                        'dataid' => $dataid,
                        'target' => $target,
                        'ezform_comp' => $ezform_comp,
                        'comp_id_target' => $comp_id_target,
                        'dataComponent' => $dataComponent,
                        'end' => $end,
                        'readonly' => $readonly,
            ]);
        } catch (\yii\db\Exception $e) {
            
        }
    }

    public function actionCheckConfig() {
        $user_id = \Yii::$app->user->identity->id;
        $ezf_id = $_GET['ezf_id'];
        
        $sql = "SELECT * FROM ezform_print_config WHERE user_id=:user_id AND ezf_id = :ezf_id";
        $params = [
            ':user_id' => $user_id,
            ':ezf_id'=>$ezf_id
        ];
        $model = \Yii::$app->db->createCommand($sql, $params)->queryOne();
        $query= \backend\modules\ezformreports\models\EzformPrintConfigSub::find()->where(['ezp_id'=>$model['id']])->all();
        return \yii\helpers\Json::encode($query);
    }

//CheckConfig
    public function geetDeleteConfig($ezf_id, $user_id){
       $model= \backend\modules\ezformreports\models\EzformPrintConfig::find()
               ->where(['ezf_id'=>$ezf_id, 'user_id'=>$user_id])->one();
       if(!empty($model)){
           if(!empty($model->delete())){
               $sql="DELETE FROM ezform_print_config_sub WHERE ezp_id=:ezp_id";
               $query=\Yii::$app->db->createCommand($sql,[':ezp_id'=>$model->id])->execute();
           }
           
       }
    }//Detele Config
 
    
    public function actionSaveConfig() {
        $ezf_id = $_GET['ezf_id'];
        $user_id = \Yii::$app->user->identity->id;
        $status = 0;
        $this->geetDeleteConfig($ezf_id, $user_id);// ลบค่าเก่าก่อนนะครับ
        $conf = $_GET['conf'];
        $check = substr($conf, 0, 3); //on,
        if ($check == 'on,') {
            $conf = substr($conf, 3, strlen($conf));
        }
        $exp = explode(",", $conf);
        //print_r($exp);exit();
        $model = new \backend\modules\ezformreports\models\EzformPrintConfig();

        $model->ezf_id = $ezf_id;
        $model->user_id = $user_id;
        if ($model->save()) {
            $status = 1;

            

            foreach ($exp as $key=>$value) {
                $ezform_print_sub = new \backend\modules\ezformreports\models\EzformPrintConfigSub();
                $ezform_print_sub->ezp_id = $model->id;
                $ezform_print_sub->ezf_id = $ezf_id;
                $ezform_print_sub->user_id = $user_id;
                $ezform_print_sub->name = $value;
                $ezform_print_sub->save();
            }
            // $ezform_print_sub->
        }
        if($status == 1){
            
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
             
            $result = [
                'status' => 'success',
                'action' => 'create',
                'message' => '<strong><i class="glyphicon glyphicon-ok"></i> บันทึกข้อมูลการตั้งค่าเรียบร้อยแล้ว</strong> ' . Yii::t('app', ''),
            ];
            return $result;
        }
    }

//SaveConfig
}
