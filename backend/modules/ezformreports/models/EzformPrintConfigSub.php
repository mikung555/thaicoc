<?php

namespace backend\modules\ezformreports\models;

use Yii;

/**
 * This is the model class for table "ezform_print_config_sub".
 *
 * @property integer $id
 * @property string $ezf_id
 * @property string $user_id
 * @property string $name
 */
class EzformPrintConfigSub extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ezform_print_config_sub';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ezf_id', 'user_id','ezp_id'], 'integer'],
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ezf_id' => 'Ezf ID',
            'user_id' => 'User ID',
            'name' => 'Name',
        ];
    }
}
