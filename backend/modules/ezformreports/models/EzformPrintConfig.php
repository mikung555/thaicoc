<?php

namespace backend\modules\ezformreports\models;

use Yii;

/**
 * This is the model class for table "ezform_print_config".
 *
 * @property integer $id
 * @property string $conf
 * @property string $user_id
 */
class EzformPrintConfig extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    
    public static function tableName()
    {
        return 'ezform_print_config';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id','ezf_id'], 'integer'],
             
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ezf_id' => 'Conf',
            'user_id' => 'User ID',
        ];
    }
}
