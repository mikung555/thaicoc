<?php

namespace backend\modules\ezformreports;

class Ezformreports extends \yii\base\Module
{
    public $controllerNamespace = 'backend\modules\ezformreports\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
