<?php

use Yii;
use yii\helpers\Html;
use yii\helpers\Url;
use backend\models\InputDataSearch;
use backend\modules\ezforms\models\EzformFields;
use backend\modules\ezforms\models\EzformDynamic;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;
use appxq\sdii\helpers\SDNoty;
use appxq\sdii\helpers\SDHtml;
 
$this->title = "ปริ้น Ezform";
$ezf_id = $id;
//echo $ezf_id;
//appxq\sdii\utils\VarDumper::dump($ezf_id);
//$modelform = InputDataSearch::findOne($ezf_id);
//
//
//$modelfield = EzformFields::find()
//        ->where('ezf_id = :ezf_id', [':ezf_id' => $modelform->ezf_id])
//        ->orderBy(['ezf_field_order' => SORT_ASC])
//        ->all();
//if (isset($_GET['dataid'])) {
//    $modelDynamic = new EzformDynamic($modelform->ezf_table);
//    $model_table = $modelDynamic->find()->where('id = :id', [':id' => Yii::$app->request->get('dataid')])->One();
//    $model_gen = \backend\modules\ezforms\components\EzformFunc::setDynamicModelLimit($modelfield);
//    $model_gen->attributes = $model_table->attributes;
//} else {
//    $model_gen = \backend\modules\ezforms\components\EzformFunc::setDynamicModelLimit($modelfield);
//}

//appxq\sdii\utils\VarDumper::dump($model_gen);
$this->title = $modelform->ezf_name;
?> 
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
<div class="container" >
    <div class='col-lg-12'>
        <h2><?= Html::encode($this->title) ?></h2>
        <hr>
        <div id="alerts"></div>
    </div>
    <!-- printform2 -->
    <form action="<?= Url::to(['/ezformreports/inputdata/printform2']) ?>" method="get" id="frm_print">    
        <input type="hidden" id="id" name="id" value="<?= $id ?>">
        <input type="hidden" id="message" name="message" value="<?= $message ?>">
        <input type="hidden" id="dataid" name="dataid" value="<?= $dataid ?>">
        <input type="hidden" id="dataid" name="ezf_id" value="<?= $ezf_id ?>">

        <div id="showprint">
            <input type="checkbox" id="checkAll"><span id="lblAll">เลือกทั้งหมด</span>
            <div class="pull-right">
                <a href="#" class="btn btn-warning" id="btnSetting"><i class='glyphicon glyphicon-cog'></i>  ตั้งค่า</a>
                <a href="#" class="btn btn-primary" id="btnSave"><i class="glyphicon glyphicon-floppy-saved"></i>  บันทึกการตั้งค่า</a>
                <button class="btn btn-danger" id="btnPrint"><i class='glyphicon glyphicon-print'></i>  ปริ้น</button>
            </div>
        </div>
        <?php
        $index = 1;
        $modelDynamic = new backend\modules\ezforms\models\EzformDynamic($modelform->ezf_table);
        
        $datamodel_table = $modelDynamic->find()->where('id = :id', [':id' => $_GET['dataid']])->One();
        $sql = "SELECT rstat, xsourcex FROM {$modelform['ezf_table']} WHERE id=:id ";
        $dataModel = Yii::$app->db->createCommand($sql, [':id'=>$dataid])->queryOne();
        //set data
        
        $input_target = $target;
        $input_ezf_id= $ezf_id;
        $input_dataid = $dataid;
       
       
        foreach ($modelfield as $key => $field):
            ?>
            <?php
            $ezf_field_name = $field->ezf_field_name;
            // echo "type => ".$field->ezf_field_type."<br>";
            
            if ($field->ezf_field_type == 1 || $field->ezf_field_type == 3 || $field->ezf_field_type == 6 || $field->ezf_field_type == 7 || $field->ezf_field_type == 8 || $field->ezf_field_type == 9 || $field->ezf_field_type == 12) {

                echo '<div id="showprint" >' .
                "<input type='checkbox' name='select[]' id='selects' value='" . $field->ezf_field_id . "'> " . $field->ezf_field_label . ': ' . '<b>' . $model_gen->$ezf_field_name . '</b>' . '</div>';
                $index++;
            }  else if ($field->ezf_field_type == 4) {

                echo '<div id="showprint" >' .
                "<input type='checkbox' name='select[]' id='selects' "
                . "value='" . $field->ezf_field_id . "'> "
                . "" . $field->ezf_field_label . ': '
                . '' . '<b>' . $model_gen->$ezf_field_name . '</b>' . '</div>';
                $index++;
            } else if ($field->ezf_field_type == 21) {
//                echo '<div id="showprint">' .
//                "<input type='checkbox' name='select[]' id='selects'  value='" . $field->ezf_field_id . "'> " . $model_gen->$ezf_field_name . '</div>';
//                $index++;
            }
            //file uplodad
            else if ($field->ezf_field_type == 11 || $field->ezf_field_type == 17 || $field->ezf_field_type == 28 || $field->ezf_field_type == 27) {
                echo '<div id="showprint">' .
                "<input type='checkbox' name='select[]' id='selects'  value='" . $field->ezf_field_id . "'> " . $field->ezf_field_label . ' : <b>' . $model_gen->$ezf_field_name . '</b></div>';
                $index++;
            } else if ($field->ezf_field_type == 14) {
                $imagepath = \Yii::getAlias('@storageUrl') . '/source/';
                echo '<div id="showprint">';
                echo "<input type='checkbox' name='select[]' id='selects'  value='" . $field->ezf_field_id . "'> ";
                echo $field->ezf_field_label . ' : <br>';
                if ($model_gen->$ezf_field_name != "" || !empty($model_gen->$ezf_field_name)) {
                    echo "<img src='" . Url::to('/fileinput/' . $model_gen->$ezf_field_name) . "'>";
                }

                echo "</div>";
                $index++;
            } else if ($field->ezf_field_type == 10) {
                
                echo '<div id="showprint">' .
                "<input type='checkbox' name='select[]' id='selects'  value='" .
                $field->ezf_field_id . "'> " . $field->ezf_field_label . ' : <b>' . $model_gen->$ezf_field_name . '</b></div>';
                $index++;
            } else if ($field->ezf_field_type == 16 || $field->ezf_field_type == 19) {
                if ($model_gen->$ezf_field_name) {
                    echo '<div id="showprint">' .
                    "<input type='checkbox' name='select[]' id='selects'  value='" . $field->ezf_field_id . "'> " . $field->ezf_field_label . ' : <b>ใช่</b></div>';
                } else {
                    echo '<div id="showprint">' .
                    "<input type='checkbox' name='select[]' id='selects'  value='" . $field->ezf_field_id . "'> " . $field->ezf_field_label . ' : <b>...</b></div>';
                }
                $index++;
                    
            } else if ($field->ezf_field_type == 18) {
                //ถ้ามีการกำหนดเป้าหมาย
                $options_input = [];
                if ($field->ezf_field_val == 1) {
                    //แสดงอย่างเดียวแก้ไขไม่ได้ (Read-only)
                    //\appxq\sdii\utils\VarDumper::dump($field->ezf_field_val);
                    $modelDynamic = \backend\modules\ezforms\components\EzformQuery::getFormTableName($field->ezf_field_ref_table);
                    //set form disable
                    $options_input['readonly'] = true;
                    $form->attributes['rstat'] = 0;
                } else if ($field->ezf_field_val == 2) {
                    //ถ้ามีการแก้ไขค่า จะอัพเดจค่านั้นทั้งตารางต้นทาง - ปลายทาง
                    $modelDynamic = \backend\modules\ezforms\components\EzformQuery::getFormTableName($field->ezf_field_ref_table);//ezf_id
                    $form->attributes['rstat'] = $dataModel['rstat'];
                } else if ($field->ezf_field_val == 3) {
                    //แก้ไขเฉพาะตารางปลายทาง
                    $modelDynamic = \backend\modules\ezforms\components\EzformQuery::getFormTableName($field->ezf_field_ref_table);
                    //\yii\helpers\VarDumper::dump($ezfField, 10, true);
                    //echo $field->ezf_field_ref_field,'-';
                    $options_input['readonly'] = true;
                    $form->attributes['rstat'] = 0;
                }
                $ezfTable = $modelDynamic->ezf_table;
                 
                
                $ezfField = \backend\modules\ezforms\models\EzformFields::find()
                        ->select('ezf_field_name')
                        ->where('ezf_field_id = :ezf_field_id', 
                                [':ezf_field_id' => $field->ezf_field_ref_field])
                        ->one();
                
                if (!$ezfField->ezf_field_name) {
                    $ezfField = \backend\modules\ezforms\models\EzformFields::find()->select('ezf_field_name, ezf_field_label')->where('ezf_field_id = :ezf_field_id', [':ezf_field_id' => $field->ezf_field_id])->one();
                    $html = '<h1>Error</h1><hr>';
                    $html .= '<h3>เนื่องการเชื่อมโยงของประเภทคำถาม Reference field ผิดพลาด การแก้ไขคือ ลบคำถามออกแล้วสร้างใหม่</h3>';
                    $html .= '<h4>คำถามที่ผิดพลาดคือ : </h4>' . $ezfField->ezf_field_name . ' (' . $ezfField->ezf_field_label . ')';
                    echo $html;
                    Yii::$app->end();
                }
                $modelDynamic = new backend\modules\ezforms\models\EzformDynamic($ezfTable);
                
                //กำหนดตัวแปร
                $field_name = $field->ezf_field_name; //field name ต้นทาง 
                $field_name_ref = $ezfField->ezf_field_name; //field name ปลายทาง
                
                  
                //ดูตาราง Ezform ฟิลด์ target ที่มีเป้าหมายเดียวกัน
                if ($input_target == 'byme' || $input_target == 'skip' || $input_target == 'all') {
                    $target = \backend\modules\ezforms\components\EzformQuery::getTargetFormEzf($input_ezf_id, $input_dataid);
                    $target = $target['ptid'];
                } else {
                    $target = base64_decode($input_target);
                }
                //หา target ใน site ตัวเองก่อน (ก็ไม่แนะนำ)
                $res = $modelDynamic->find()->select($field_name_ref)->where('ptid = :target AND ' . $field_name_ref . ' <> "" AND xsourcex = :xsourcex AND rstat <>3', [':target' => $target, ':xsourcex' => $dataModel['xsourcex']])->orderBy('create_date DESC');

                //echo $target.' -';
                //หาที่ target ก่อน
               
                if ($res->count()) {
                    $model_table = $res->One();
                    
                    if ($field->ezf_field_val == 1) {
                        //echo 'target 1-1<br>';
                        //เปลี่ยนค่า field name ระหว่างต้นทาง และปลายทาง
                        $model_gen->$field_name = $model_table->$field_name_ref;
                        $model_gen->attributes[$field_name] = $model_table->$field_name_ref;
                    } else if ($field->ezf_field_val == 2) {
                        
                    } else if ($field->ezf_field_val == 3) {
                        if ($input_dataid) {
                            //echo 'target 1<br>';
                            $res = \backend\modules\ezforms\components\EzformQuery::getReferenceData($field->ezf_id, $field->ezf_field_id, $input_dataid, $field_name_ref);
                            //\yii\helpers\VarDumper::dump($model_table->attributes, 10,true);
                            if ($res[$field_name_ref]) {
                                $model_table->$field_name_ref = $res[$field_name_ref];
                            }
                            //เปลี่ยนค่า field name ระหว่างต้นทาง และปลายทาง
                            $model_gen->$field_name = $model_table->$field_name_ref;
                            $model_gen->attributes[$field_name] = $model_table->$field_name_ref;
                        } else {
                            //\yii\helpers\VarDumper::dump($model_table->attributes, 10,true);
                            // echo 'target else 1<br>';
                            //เปลี่ยนค่า field name ระหว่างต้นทาง และปลายทาง
                            $model_gen->$field_name = $model_table->$field_name_ref;
                            $model_gen->attributes[$field_name] = $model_table->$field_name_ref;
                        }
                    }
                } else {
                    //กรณีหาไม่เจอ ให้ดู primary key
                    $res = $modelDynamic->find()->where('id = :id', [':id' => $target]);
                   
                    if ($res->count()) {
                        $model_table = $res->One();

                        if ($field->ezf_field_val == 1) {
                            //echo 'target 2-1<br>';
                            //เปลี่ยนค่า field name ระหว่างต้นทาง และปลายทาง
                            $model_gen->$field_name = $model_table->$field_name_ref;
                            $model_gen->attributes[$field_name] = $model_table->$field_name_ref;
                        } else if ($field->ezf_field_val == 2) {
                            
                        } else if ($field->ezf_field_val == 3) {
                            if ($input_dataid) {
                                //echo 'target 2<br>';
                                $res = \backend\modules\ezforms\components\EzformQuery::getReferenceData($field->ezf_id, $field->ezf_field_id, $input_dataid, $field_name_ref);
                                if ($res[$field_name_ref]) {
                                    $model_table->$field_name_ref = $res[$field_name_ref];
                                }
                                //เปลี่ยนค่า field name ระหว่างต้นทาง และปลายทาง
                                $model_gen->$field_name = $model_table->$field_name_ref;
                                $model_gen->attributes[$field_name] = $model_table->$field_name_ref;
                            } else {
                                //echo 'target else 2<br>';
                                //\yii\helpers\VarDumper::dump($model_table->attributes, 10,true);
                                //เปลี่ยนค่า field name ระหว่างต้นทาง และปลายทาง
                                $model_gen->$field_name = $model_table->$field_name_ref;
                                $model_gen->attributes[$field_name] = $model_table->$field_name_ref;
                            }
                        }
                    }
                }
                $ezf_field_order = $field->ezf_field_order;
                $ezf_field_id = $field->ezf_field_id;
                $ezf_field_name = $field->ezf_field_name;
                $ezf_field_lenght = $field->ezf_field_lenght;
                
                $model = EzformFields::find()->where(['ezf_field_id' => $field->ezf_field_ref_field])->andWhere('ezf_field_type IS NOT NULL')->one();
		$res = \backend\modules\ezforms\components\EzformQuery::getReferenceData($ezf_id, $field->ezf_field_id, $input_dataid, $field_name_ref);
                    if ($res[$field_name_ref]) {
                        echo '<div id="showprint"><input type=\'checkbox\' name=\'select[]\' id=\'selects\' value=' . $field->ezf_field_id .'>' . $field->ezf_field_label . ' : <b>' . $res[$field_name_ref] . '</b></div>';
                    } else {
                        echo '<div id="showprint"><input type=\'checkbox\' name=\'select[]\' id=\'selects\' value='. $field->ezf_field_id . '>' . $field->ezf_field_label . ' : <b>' . $model_gen->$ezf_field_name . '</b></div>';
                    }	
                    
                //$field_item = \backend\modules\ezforms\components\EzformQuery::getInputId($field->ezf_field_type);    
                
                //echo $model->ezf_field_label ." | ".$field->ezf_field_ref_field;
                //Yii::$app->end();
            } 
            else if ($field->ezf_field_type == 25) {//Grid
                $modelchoice = \backend\modules\ezforms\models\EzformChoice::find()->where('ezf_field_id = :ezf_field_id', [':ezf_field_id' => $field->ezf_field_id])->all();
                $modelquestion = \backend\modules\ezforms\models\EzformFields::find()->where('ezf_field_sub_id = :ezf_field_id', [':ezf_field_id' => $field->ezf_field_id])->all();

                $html = "<div id='showprint' > ";
                $html .= "<input type='checkbox' name='select[]' id='selects'  value='" . $field->ezf_field_id . "'> ";
                $html .= "<b>" . $field->ezf_field_label . "</b><br>";
                //$html .= $field->ezf_field_help;//หัวข้อย่อย

                $html .= "<table class='table table-responsive table-bordered'>";

                $j = 0;
                $count_modelchoice = count($modelchoice);
                $html .= "<tr>";
                foreach ($modelquestion as $question) {

                    $modelvalue = \backend\modules\ezforms\models\EzformFields::find()->where('ezf_field_sub_id = :ezf_field_id', [":ezf_field_id" => $question["ezf_field_id"]])->all();
                    if ($question["ezf_field_label"]) {
                        $html .= '<span style="padding-right:3px;">' . $question["ezf_field_label"] . '</span>';
                    }
                    while ($j < $count_modelchoice) {
                        $html .= "<th>" . $modelchoice[$j]['ezf_choicelabel'] . "</th>"; //หัวข้อ  
                        $j++;
                    }



                    //$j++;
                }

                $html .= "</tr>";

                foreach ($modelquestion as $question) {
                    $modelvalue = \backend\modules\ezforms\models\EzformFields::find()->where('ezf_field_sub_id = :ezf_field_id', [":ezf_field_id" => $question["ezf_field_id"]])->all();

                    $html .= "<tr>";
                    foreach ($modelvalue as $value) {

                        $ezf_field_name = $value["ezf_field_name"];
                        //\appxq\sdii\utils\VarDumper::dump($modelchoice);



//                        if ($value["ezf_field_type"] == "251" || $value["ezf_field_type"] == "252") {
//                            //คำตอบ
//                            $html .= "<td>(" . $model_gen->$ezf_field_name . ")</td>";
//                        } else if ($value["ezf_field_type"] == "253") {
//                                $data = "";
//                                if(!empty(@$model_gen->$value["ezf_field_name"])){
//                                     $date = @$model_gen->$value["ezf_field_name"];
//                                }
//                               
//                             
//                            if ($date) {
//                                $explodeDate = explode('-', $date);
//                                $formateDate = $explodeDate[2] . "/" . $explodeDate[1] . "/" . ($explodeDate[0] + 543);
//                                if(!empty($formateDate)){
//                                    $model_gen->$value["ezf_field_name"] = $formateDate;
//                                }
//                                
//                            }
//                            //คำตอบ
//                            $html .= "<td>(" . $model_gen->$ezf_field_name . ")</td>";
//                        } else if ($value["ezf_field_type"] == "254") {
//                            if ($model_gen->$ezf_field_name) {
//                                $html .= "<td>(ใช่)</td>";
//                            } else {
//                                $html .= "";
//                            }
//                        }

                        //$j++;
                    }

                    // $html .= '<hr style="margin-top:2px; margin-bottom: 0px;">';
                    $html .= "</tr>";
                }

                $html .= "</table>";
                $html .= "</div>";
                echo $html;
                $index++;
            } else if ($field->ezf_field_type == 23) {
                $modelchoice = \backend\modules\ezforms\models\EzformChoice::find()->where('ezf_field_id = :ezf_field_id', [':ezf_field_id' => $field->ezf_field_id])->all();
                $modelsubfield = \backend\modules\ezforms\models\EzformFields::find()->where('ezf_field_sub_id = :ezf_field_sub_id', [':ezf_field_sub_id' => $field->ezf_field_id])->all();
                $html = "<div>";
                $html .= "<b>" . $field->ezf_field_label . "</b><br>";
                $html .= $field->ezf_field_help;

                $i = 1;
                foreach ($modelsubfield as $subfield) {
                    $modelsubchoice = \backend\modules\ezforms\models\EzformChoice::find()->where('ezf_field_id = :ezf_field_id', [':ezf_field_id' => $subfield->ezf_field_sub_id])->all();


                    $html .= '<span style="padding-right:3px;">' . $i . ". " . $subfield["ezf_field_label"] . '</span>';

                    $j = 0;
                    foreach ($modelsubchoice as $subchoice) {
                        $ezf_field_name = $subfield["ezf_field_name"];
                        $html .= ($subchoice["ezf_choicevalue"] == $model_gen->$ezf_field_name ? '(' . $modelchoice[$j]['ezf_choicelabel'] . ')' : '');
                        $j++;
                    }
                    $html .= '<br>';
                    $i++;
                }
                $html .= "</div>";
                $index++;
                echo $html;
            } else if ($field->ezf_field_type == 13) {

                $fieldx = \backend\modules\ezforms\models\EzformFields::find()->select('ezf_field_name, ezf_field_label')->where('ezf_field_sub_id = :ezf_field_sub_id', [':ezf_field_sub_id' => $field->ezf_field_id])->all();
                $html = "<div id='showprint' > ";
                $html .= "<input type='checkbox' name='select[]' id='selects'  value='" . $field->ezf_field_id . "'> ";
                $html .= $field->ezf_field_label;
                foreach ($fieldx as $val) {
                    $ezf_field_namex = $val->ezf_field_name;

                    //Province, amphur, tombon
                    if ($val->ezf_field_label == 1) {
                        $sql = "SELECT `PROVINCE_ID`, `PROVINCE_CODE`,`PROVINCE_NAME` FROM `const_province` WHERE PROVINCE_CODE='" . $model_gen->$ezf_field_namex . "'";
                        $res = Yii::$app->db->createCommand($sql)->queryOne();
                        $textProvince = str_replace('#area1#', $res['PROVINCE_NAME'], $textProvince);
                        //appxq\sdii\utils\VarDumper::dump();
                        $html .= ": <b>" . $res['PROVINCE_NAME'] . "</b>";
                        //$field->ezf_field_id
                    } else if ($val->ezf_field_label == 2) {
                        $sql = "SELECT AMPHUR_NAME FROM `const_amphur` WHERE AMPHUR_CODE='" . $model_gen->{$ezf_field_namex} . "'";
                        $res = Yii::$app->db->createCommand($sql)->queryOne();
                        $textProvince = str_replace('#area2#', $res['AMPHUR_NAME'], $textProvince);
                        $html .= "อำเภอ: <b>" . $res['AMPHUR_NAME'] . "</b>";
                    } else if ($val->ezf_field_label == 3) {
                        $sql = "SELECT DISTRICT_NAME FROM `const_district` WHERE DISTRICT_CODE='" . $model_gen->{$ezf_field_namex} . "'";
                        $res = Yii::$app->db->createCommand($sql)->queryOne();
                        $textProvince = str_replace('#area3#', $res['DISTRICT_NAME'], $textProvince);
                        $modelfield->{$ezf_field_namex} = $textProvince;
                        $html .= "ตำบล: <b>" . $res['DISTRICT_NAME'] . "</b>";
                        //echo $modelfield->$ezf_field_namex;
                    }
                }
                $modelfield->$ezf_field_id = $textProvince;

                $textProvince = 'จ.#area1# อ.#area2# ต.#area3#';
                $html .= "</div>";
                $index++;
                echo $html;
            }
            ?>

        <?php endforeach; ?>
    </form>
</div>



<style>

    #showprint {

        padding: 2px;
        border: 1px solid rgb(238, 238, 238);
        overflow: hidden;
        width: 1100px;
        margin: 0 auto;
        border-style: dotted;
    }


    /*        input[type=checkbox], input[type=radio] {
                margin: 5px 0 0;
                margin-top: 1px\9;
                line-height: normal;
                 background: blue; 
                -ms-transform: scale(2);
                -moz-transform: scale(2);
                -webkit-transform: scale(2);
                -o-transform: scale(2);
                padding: 10px;
                margin-right: 10px;
            }*/

    input[type="checkbox"]{

        cursor: pointer;
        /* -webkit-appearance: none; */
        appearance: none;
        background: #34495E;
        border-radius: 1px;
        box-sizing: border-box;
        position: relative;
        box-sizing: content-box;
        width: 24px;
        height: 20px;
        border-width: 0;
        transition: all 0.3s linear;
        margin-right: 10px;
        position: relative;
        top: 5px;
    }
    input[type="checkbox"]:checked{
        background-color: #2ECC71;
    }
    input[type="checkbox"]:focus{
        outline: 0 none;
        box-shadow: none;
    }

</style>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>

<script>   

    _show(); //function _show

    $('#checkAll,#lblAll,#selects, #btnPrint, #btnSetting').hide();
    $('#btnSetting').click(function () {
        $('#checkAll,#lblAll,#selects, #btnSave').fadeIn('slow');
        $('#btnPrint').hide();
        $("input[type='checkbox']").attr('disabled', false);
    });//btnSettingr
    $('#btnSave').click(function () {
        _insert();

    });
    function _insert() {
        var i = 0;
        var arr = []; //ตัวแปรสำหรับพิมพ์ ค่า
        $(':checkbox:checked').each(function () {
            arr[i++] = $(this).val();
        });
        var str = "";

        str = arr.join(",");


        $.ajax({
            url: '<?= Url::to(['/ezformreports/ezform-report/save-config']) ?>',
            data: {conf: str, ezf_id: '<?= $ezf_id ?>'},
            type: 'get',
            dataType: 'json',
            success: function (result) {

                if (result.status == 'success') {
                    $('#alerts').html('<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">&times;</a>' + result.message + '</div>');
                    // $('#btnSave').hide();
                    $('#btnPrint').show();
                    //$("input[type='checkbox']").attr('disabled',true);
                }
//                var len = result.length;
//                if (len == 0) {
//                    $('#checkAll,#lblAll,#selects, #btnPrint').hide();
//                }
            }
        });
        return false;
    }//_insert 
    function _show() {
        $.ajax({
            url: '<?= Url::to(['/ezformreports/ezform-report/check-config']) ?>',
            dataType: "JSON",
            data: {ezf_id: '<?= $ezf_id ?>'},
            type: 'get',
            success: function (result) {
                var len = result.length;
                if (len == 0) {
                    //$('#checkAll,#lblAll,#selects, #btnPrint, #btnSave').hide();
                    $('#checkAll,#lblAll,#selects,#btnPrint').fadeIn('slow');
                } else {
                    $('#checkAll,#lblAll,#selects,#btnPrint').fadeIn('slow');

                }
                $.each(result, function (k, v) {

                    $("[value=" + v.name + "]").attr("checked", "checked");
                    // $("input[type='checkbox']").attr('disabled',true);
                });
            }
        });
        return false;
    }//_show

    $("#checkAll").click(function () {
        $('input:checkbox').not(this).prop('checked', this.checked);
    });//Check All

    $('#btnPrint').click(function () {
        var i = 0;
        var arr = []; //ตัวแปรสำหรับพิมพ์ ค่า
        $(':checkbox:checked').each(function () {
            arr[i++] = $(this).val();
        });
        console.log(arr);

    });//Print config

</script>