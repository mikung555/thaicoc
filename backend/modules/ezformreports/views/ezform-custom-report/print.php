
<!doctype html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title><?= $title;?></title>
  
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" >
  <script src="<?= \yii\helpers\Url::to('@web/js/ng/angular.min.js')?>"></script>
  <script src="<?= \yii\helpers\Url::to('@web/js/ng/angular-sanitize.js')?>"></script>
  
  <style>
        .container{width:1200px;margin:0 auto;}
        .text-center{text-align:center;}
  </style>
  
<?php //\appxq\sdii\utils\VarDumper::dump(yii\helpers\Json::decode($out));?>
</head>
<body>
    <div ng-app="bindHtmlReportApp" class="container">
 

     <div ng-controller="CustomReportController">
         
         <div id="template" class="container">
             <h2><?= $title;?></h2><hr>
             <?= $template["ezf_report_name"];?>
             
             
        </div>
    </div>
</div>
    
<script>  
var out = <?php echo $out?>;
angular.module('bindHtmlReportApp', ['ngSanitize'])
  .controller('CustomReportController', ['$scope', function($scope) {
    $scope.myHTML = '<table border=1><tr><td>Demo</td><td>Demo2</td></tr></table>';
    $scope.data = out;

}]);
        
window.print();        
</script>

<?= $this->registerJS("
    $('table').addClass('table table-bordered');
    $('img').addClass('img img-responsive');
")?>
</body>
</html>