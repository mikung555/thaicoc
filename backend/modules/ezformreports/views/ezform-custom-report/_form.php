<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use vova07\imperavi\Widget;
use dosamigos\tinymce\TinyMce;
 //appxq\sdii\utils\VarDumper::dump(base64_decode($model["ezf_report_name"]));
?>
 
<?php

$form = ActiveForm::begin([
            'id' => 'form-custom-report',
            'enableClientValidation' => true,
            'enableAjaxValidation' => false,
            'options' => ['enctype' => 'multipart/form-data']]);
?>
 
 <?php   //$form->field($model, 'ezf_report_name')->textInput()?>

<?= $form->field($model, 'ezf_report_name')->widget(TinyMce::className(), [
    'options' => [
        'rows' => 16,
        'id'=>'nut-editor'
    ],
    'language' => 'th_TH',
   
    'clientOptions' => [
        
        'plugins' => [
            "advlist autolink lists link charmap print preview anchor",
            "searchreplace wordcount visualblocks visualchars code fullscreen",
            "insertdatetime media nonbreaking save table contextmenu directionality",
        ],
        
        'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
    ] 
])->label(false); ?>

<?php
 
?>
 
<?php ActiveForm::end(); ?>
 <?php $this->registerJS("
     
    $('#form-custom-report').hover(function(){
        //form-custom-report
        //console.log(tinymce.activeEditor.getContent());
        //tinymce.activeEditor.execCommand('mceInsertContent', false, '{{data.'+values+'}}');

       setTimeout(function(){
        Save();
       },3000);
    });
    Save=()=>{
      let url = $('#form-custom-report').attr('action');
      let frm = $('#form-custom-report').serialize();      
      let ezf_report_name = tinymce.activeEditor.getContent();//tinymce   
      $.ajax({
        url:url,
        type:'POST',
        data:{ezf_report_name:ezf_report_name},
        dataType:'JSON',
        success:function(data){
            
             
            if(data.status == 'success'){
                " . \common\lib\sdii\components\helpers\SDNoty::show('data.message', 'data.status') . "
            }
            
            
        },
        error:function(error){
            console.log(error);
        }
      });
    }
    

")?>