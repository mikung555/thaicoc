<?php

use yii\helpers\Html;
use yii\helpers\Url;
use common\lib\sdii\widgets\SDModalForm;
use yii\bootstrap\ActiveForm;
?>

 


<?php \yii\widgets\Pjax::begin(['id' => 'nut-pjax'])?>
<div class="col-md-6">
    <div style="margin:0 0 10px 0;">    
        <?php
        Html::button("<i class=\"fa fa-compress\"></i> ตัวแปร", ["style" => "margin-right:5px;", "class" => "btn btn-primary btn-flat", "id" => "btnVar",
            'data-url' => Url::to(['//managedata/annotated/index', 'ezf_id' => $_GET["id"], 'print' => '2'])])
        ?>
    </div>    
<div id="show-ezform-custom-reports"></div>
</div>
<div class="col-md-6">
    <div style="height:300px;overflow: hidden;overflow-y: auto;">
    <div id="showvaliable"></div>
    </div>
</div>
<?php \yii\widgets\Pjax::end(); ?>

<?php
$this->registerJs("
    function showvaliable(){
        var url = '".Url::to(['//managedata/annotated/index', 'ezf_id' => $_GET["id"], 'print' => '2'])."';
        $.ajax({
            url:url,
            success:function(data){
                $('#showvaliable').html(data);
            }
        });
        
    }showvaliable();
     
//  
//    $('#valiables .container #formPanel code').click(function(e){
//       var vals = $(this).text();
//       alert('ok');
//    });

    $('#btnViewDemo').click(()=>{
       loadForm();
    });
   loadForm=()=>{
        $.ajax({
            url:'" . Url::to(['/ezformreports/ezform-custom-report/report-ezform-custom', 'ezf_id' => $_GET["id"]]) . "',
                type:'GET',
                success:function(data){
                    $('#show-ezform-custom-reports').html(data);
                }
        });
    }
    $('#btnVar').click(()=>{
       let url=$('#btnVar').attr('data-url');
       $('.cke_editable cke_editable_themed').append('Demo');
       modalEzformReport(url);
    });
     
    modalEzformReport=(url)=>{
        //$.pjax.reload({container:'#nut-pjax'}); //refresh the grid
        $.ajax({
            url:url,
            success:function(data){
                $('#modal-ezform').modal('show');
                $('#modal-ezform .modal-content').html(data);
            }
        });
        
    } 
    
//auto load
loadForm();

 
    
    
 

");

$this->registerCSS("
    .drawingContent {
    position: absolute;
    z-index: 3000;
    top: 50px;
    left: 10px;
}
");
?>


