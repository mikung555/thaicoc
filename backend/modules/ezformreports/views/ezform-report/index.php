<?php
    use yii\helpers\Url;
    $this->registerJsFile('/js/modules/js/angular.min.js');
    $this->registerJsFile('/js/modules/ngcontrollers/nutController.js');
    
    $this->registerJsFile('/js/modules/js/html2canvas.min.js');
    $this->registerJsFile('/js/modules/js/jspdf.debug.js');
    $this->registerJsFile('/js/modules/js/saveHtmlToPdf.js');
     
  

    
        
?>
<a href="<?= Url::to(['/nut/index'])?>">Test</a>
<div ng-app="nut" ng-controller="nutController">
    <div>
        <form>
            NAME
            <input type="text" ng-model="name">
            <button ng-click="insertdata()">Save</button>
        </form>
    </div>
    
    <button class="btn btn-danger" ng-click="prints()"><i class="fa fa-print"></i> Pdf</button>
    
    <div>
        <table>
            <thead>
                <tr>
                    <th>ID</th>
                    <th>NAME</th>
                </tr>
            </thead>
            <tbody>
                <tr ng-repeat="d in data">
                    <td>{{d.id}}</td>
                    <td>{{d.name}}</td>
                </tr>
            </tbody>
        </table>
    </div>
</div> 