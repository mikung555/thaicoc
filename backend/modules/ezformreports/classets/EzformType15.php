<?php

namespace backend\modules\ezformreports\classets;

use Yii;
use backend\modules\ezformreports\classets\EzformReportClass;

class EzformType15 {

    public static function getType15EzformField($ezf_id) {
        return EzformReportClass::getEzformField($ezf_id);
        
    }
 

    public static function getType15EzformSubFieldHead($ezf_id, $ezf_field_sub_id) {
         
        $query = (new \yii\db\Query())
                ->select(['*'])
                ->from('ezform_fields')
                ->where("(ezf_id=:ezf_id AND ezf_field_sub_id=:ezf_field_sub_id) OR (ezf_id=:ezf_id2 AND ezf_field_type='250')", 
                        [":ezf_id" => $ezf_id, ":ezf_field_sub_id" => $ezf_field_sub_id, ":ezf_id2"=>$ezf_id]);
        return $query->all();
    }
    public static function getType15EzformSubFieldRows($ezf_id, $ezf_field_sub_id){
        //  SELECT * FROM ezform_fields WHERE ezf_id='1501817752036780000' AND ezf_field_sub_id='1501824170041410200';

        $query = (new \yii\db\Query())
                ->select(['*'])
                ->from('ezform_fields')
                ->where("ezf_id=:ezf_id AND ezf_field_sub_id=:ezf_field_sub_id", 
                        [":ezf_id" => $ezf_id, ":ezf_field_sub_id" => $ezf_field_sub_id]);
        return $query->all();
    }
    
    public static function getType15EzformTbData($ezf_id, $dataid, $fields){
        $sql="SELECT `$fields` FROM tbdata_$ezf_id WHERE id=:dataid";
        $params=[":dataid"=>$dataid];
        return Yii::$app->db->createCommand($sql,$params)->queryOne();
    }
    

}
