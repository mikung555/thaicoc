<?php

namespace backend\modules\ezformreports\classets;

use Yii;
use backend\modules\ezformreports\classets\EzformReportClass;

class EzformType10{//component 
    public static function getType10($ezf_id, $ezf_component, $out, $dataid,$ezf_field_name, $fields){
         $data= EzformType10::getModelGens($ezf_id, '', $dataid, $fields);
         $out[$fields->ezf_field_name] = $data[$fields->ezf_field_name];
         return $out;
        //\appxq\sdii\utils\VarDumper::dump($out);
        
    }//step 1 ezform_fields 
    public static function  getModelGens($id, $message = '', $dataid='', $fields){
        $modelform = \backend\models\InputDataSearch::findOne($id);
        $modelfield = \backend\modules\ezforms\models\EzformFields::find()
            ->where('ezf_id = :ezf_id', [':ezf_id' => $modelform->ezf_id])
            ->orderBy(['ezf_field_order' => SORT_ASC])
            ->all();
        if (!empty($dataid)) {
            $modelDynamic = new \backend\modules\ezforms\models\EzformDynamic($modelform->ezf_table);
            $model_table = $modelDynamic->find()->where('id = :id', [':id' => $dataid])->One();
            $model_gen = \backend\modules\ezforms\components\EzformFunc::setDynamicModelLimit($modelfield);
            $model_gen->attributes = $model_table->attributes;
        }else {
            $model_gen = \backend\modules\ezforms\components\EzformFunc::setDynamicModelLimit($modelfield);
        }
        
        //foreach($modelfield as $fields){
            $ezf_field_name = $fields->ezf_field_name;
            
            if($fields->ezf_field_type == 10){
                
                //conponent
                $comp = \backend\modules\component\models\EzformComponent::find()->where('comp_id = :comp_id', [':comp_id' => $fields->ezf_component])->one();
                $ezform = \backend\modules\ezforms\components\EzformQuery::getFormTableName($comp->ezf_id);
                $field_id_key = \backend\modules\ezforms\models\EzformFields::find()->select('ezf_field_name')->where('ezf_field_id = :ezf_field_id', [':ezf_field_id'=>$comp->field_id_key])->one();
                $field_id_desc = explode(',', $comp->field_id_desc);
                $fields='';
                
                foreach($field_id_desc as $val){
                    $fieldx = \backend\modules\ezforms\models\EzformFields::find()->select('ezf_field_name')->where('ezf_field_id = :ezf_field_id', [':ezf_field_id'=>$val])->one();
                    $fields .= $fieldx->ezf_field_name.', ';
                }
                
                $fields = substr($fields, 0, -2);
                
                //SELECT firstname, lastname FROM user_profile WHERE user_id = 
                $res = Yii::$app->db->createCommand("SELECT ".$fields." FROM ".$ezform->ezf_table." WHERE ".$field_id_key->ezf_field_name." = '".$model_gen->$ezf_field_name."';'")->queryOne();
                 
                $text = '';
                foreach($res as $val){
                    $text .= $val.' ';
                }
                //VarDumper::dump($text);
                $model_gen->{$ezf_field_name} = $text;
                $out[$ezf_field_name] = $model_gen->$ezf_field_name;
                
                 
            }
             
        return $out;
        
    }
  
    public static function getTBDATA($field="", $table='',$code){
        $sql="SELECT * FROM doctor_all WHERE doctorcode =:code";
        $params = [":code"=>$code];
        return \Yii::$app->db->createCommand($sql,$params)->queryOne();
    }

    public static function getEzformField($ezf_id){
        return EzformReportClass::getEzformField($ezf_id);
    }//get ezform field
    
    public static function getEzformComponent($ezf_component){
        $sql="SELECT * FROM ezform_component WHERE ezform_component.comp_id=:ezf_component";
        $params=[":ezf_component"=>$ezf_component];
        return \Yii::$app->db->createCommand($sql,$params)->queryOne();
    }//step 2  
    
    public static function getType15EzformTbData($ezf_id, $dataid, $fields){
       return EzformType15::getType15EzformTbData($ezf_id, $dataid, $fields);
    }
    public static function getType10EzformSubField($ezf_field_sub_id){
        return EzformReportClass::getEzformSubField($ezf_field_sub_id);
    }
    public static function getType10EzformFieldQuery($com_ezf_id,$field_id_key,$field_id_desc1,$field_id_desc2){
        $sql="SELECT *
FROM ezform_fields 
WHERE ezf_id=:ezf_id AND (ezf_field_id = $field_id_key OR ezf_field_id =$field_id_desc1 OR ezf_field_id =$field_id_desc2)";
       $params=[":ezf_id"=>$com_ezf_id];
        
        return \Yii::$app->db->createCommand($sql,$params)->queryAll();
         
    }
    public static function getType10EzformTableQuery($table,$field, $id){
        $sql="SELECT `$field` FROM $table WHERE doctorcode =:id";
        $params=[":id"=>$id];
        return \Yii::$app->db->createCommand($sql,$params)->queryOne();
    }
    
    
}
