<?php

namespace backend\modules\ezformreports\classets;

use Yii;
use backend\modules\ezformreports\classets\EzformReportClass;
use backend\modules\ezformreports\classets\EzformAddress;

class EzformType13{
    public static function getType13EzformField($ezf_id){
        return EzformReportClass::getEzformField($ezf_id);
    }//get ezform field
 
    public static function getType13EzformTbData($ezf_id, $dataid, $fields){
       return EzformType15::getType15EzformTbData($ezf_id, $dataid, $fields);
    }
    public static function getType13EzformSubField($ezf_field_sub_id){
        return EzformReportClass::getEzformSubField($ezf_field_sub_id);
    }
    
    public static function getType13($ezf_id, $ezf_field_sub_id, $out, $dataid){
        $modelSubfield = EzformType13::getType13EzformSubField($ezf_field_sub_id);
        $modelTable = EzformReportClass::getEzform($ezf_id)["ezf_table"];
        
        //1 province 2 amphur 3 districe
        foreach($modelSubfield as $subfield){
            $data = EzformReportClass::getTBDATA($modelTable, $dataid, $subfield["ezf_field_name"]);
            if($subfield["ezf_field_label"] == 1){//Province
                $province = EzformAddress::getProvince($data[$subfield["ezf_field_name"]])["PROVINCE_NAME"];
                $out[$subfield["ezf_field_name"]] = $province;
            }
            if($subfield["ezf_field_label"] == 2){//Amphur
                 $province = EzformAddress::getAmphur($data[$subfield["ezf_field_name"]])["AMPHUR_NAME"];
                $out[$subfield["ezf_field_name"]] = $province;
            }
            if($subfield["ezf_field_label"] == 3){//District
                $province = EzformAddress::getDistrict($data[$subfield["ezf_field_name"]])["DISTRICT_NAME"];
                $out[$subfield["ezf_field_name"]] = $province;
            }
        }
        //\appxq\sdii\utils\VarDumper::dump($out);
        return $out;
    }//end 
}
