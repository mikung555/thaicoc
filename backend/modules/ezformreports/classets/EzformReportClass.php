<?php

namespace backend\modules\ezformreports\classets;

use Yii;

class EzformReportClass {

    public static function getEzform($ezf_id) {
        $sql = "SELECT * FROM ezform WHERE ezf_id=:ezf_id";
        $params = [":ezf_id" => $ezf_id];
        $query = \Yii::$app->db->createCommand($sql, $params)->queryOne();
        return $query;
    }
     public static function getDataTable($tablename) {
        $sql = "SELECT * FROM ezform WHERE ezf_id=:ezf_id";
        $params = [":ezf_id" => $ezf_id];
        $query = \Yii::$app->db->createCommand($sql, $params)->queryOne();
        return $query;
    }

    public static function getEzformField($ezf_id) {
        $sql = "SELECT * 
                FROM ezform_fields as ef
                WHERE ef.ezf_id=:ezf_id AND ezf_field_sub_id is null
                #AND ezf_field_head_label IS NULL
                AND ef.ezf_field_label <> ''
                AND ef.ezf_field_type is NOT NULL";
        $params = [":ezf_id" => $ezf_id];
        $query = \Yii::$app->db->createCommand($sql, $params)->queryAll();
        return $query;
    }
    public static function getEzformSubField($ezf_field_sub_id){
        $sql="SELECT * FROM ezform_fields WHERE ezf_field_sub_id=:ezf_field_sub_id";
        $params = [":ezf_field_sub_id" => $ezf_field_sub_id];
        $query = \Yii::$app->db->createCommand($sql, $params)->queryAll();
        return $query;
    }
    public static function getEzformFieldOne($ezf_field_id){
        $sql = "SELECT * FROM ezform_fields WHERE ezf_field_id=:ezf_field_id";
        $params = [":ezf_field_id" => $ezf_field_id];
        $query = \Yii::$app->db->createCommand($sql, $params)->queryOne();
        return $query;
    }
    public static function getEzformChoilce($ezf_field_id, $ezf_choicevalue){
        $sql = "SELECT * from ezform_choice where ezf_field_id =:ezf_field_id AND ezf_choicevalue=:ezf_choicevalue;";
        $params = [":ezf_field_id" => $ezf_field_id, ':ezf_choicevalue'=>$ezf_choicevalue];
        $query = \Yii::$app->db->createCommand($sql, $params)->queryOne();
        return $query;
    }

    public static function getEzformRefVarName($ref_field){
        //SELECT * FROM ezform_fields WHERE ezf_field_id='1446662876036103400';
        $sql="SELECT `ezf_field_name`,`ezf_field_label` FROM ezform_fields WHERE ezf_field_id=:ref_field";
        $params = ["ref_field"=>$ref_field];
        return \Yii::$app->db->createCommand($sql,$params)->queryOne();
    }

    public static function getSaveEzformReport($ezf_id, $ezf_report_name) {
        
       // echo base64_encode($ezf_report_name);
        $sql = "INSERT INTO ezform_report_editor(ezf_id,ezf_report_name) VALUES(:ezf_id,:ezf_report_name)";
        $params = [":ezf_id" => $ezf_id, ":ezf_report_name" => $ezf_report_name];
        $execute = \Yii::$app->db->createCommand($sql, $params)->execute();
        $status=0;
        if($execute){
            $status=1;
        }
        return $status;
    }
    public static function getUpdateEzformReport($ezf_id, $ezf_report_name,$id) {
        $sql = "UPDATE `ezform_report_editor` SET ezf_id=:ezf_id, ezf_report_name=:ezf_report_name WHERE id=:id";
        $params = [":ezf_id" => $ezf_id, ":ezf_report_name" => $ezf_report_name,":id"=>$id];
        $execute = \Yii::$app->db->createCommand($sql, $params)->execute();
        $status=0;
        if($execute){
            $status=1;
        }
        return $status;
    }
    public static function getEzformReportEditor($ezf_id){
        $sql="SELECT * FROM ezform_report_editor WHERE ezf_id=:ezf_id";
        $params=[":ezf_id"=>$ezf_id];
        return \Yii::$app->db->createCommand($sql,$params)->queryOne();
    }

    public static function getResponse($status=0){
//        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        if($status == 1){
            $out = ["message"=>"<i class='fa fa-check-circle'></i> success", "status"=>"success"];
        }else{
            $out = ["message"=>"<i class='fa fa-times-circle'></i> error", "status"=>"error"];
        }
        return \yii\helpers\Json::encode($out);
    }
    
    public static function getTBDATA($ezf_id, $id, $fields){
        $sql="SELECT `$fields` FROM $ezf_id WHERE id=:id";
        $params=[":id"=>$id];
        return \Yii::$app->db->createCommand($sql,$params)->queryOne();
    }

}
