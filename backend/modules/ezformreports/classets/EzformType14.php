<?php

namespace backend\modules\ezformreports\classets;

use Yii;
use backend\modules\ezformreports\classets\EzformReportClass;
use backend\modules\ezformreports\classets\EzformAddress;

class EzformType14{
 //fileInput
    public static function getType14($ezf_id, $ezf_field_name, $out, $dataid, $type){
        $table = EzformReportClass::getEzform($ezf_id)["ezf_table"];
        $tbdata = EzformReportClass::getTBDATA($table, $dataid, $ezf_field_name);
        if($type == 14){
            $img = \yii\helpers\Url::to("@web/fileinput/$tbdata[$ezf_field_name]");
        }
        if($type == 24){
            $path = Yii::getAlias("@storageUrl");
            //."/drawing/bg/bg_20170810_11324615023395663129.png"
            $img_fiel = explode(",", $tbdata[$ezf_field_name]); ///drawing/data/20170810_11521215023407327796.png,''
            $img =  $path."/drawing/data/$img_fiel[0]";
            
        }
         $out[$ezf_field_name] = $img;
         
         return $out;
    }
    
}
