<?php 
namespace backend\modules\ezformreports\classets;

class EzformAddress {
    public static function getProvince($province_code){
        $sql="SELECT * FROM const_province WHERE PROVINCE_CODE = :code";
        $params=[":code"=>$province_code];
        return \Yii::$app->db->createCommand($sql,$params)->queryOne();
    }
    public static function getAmphur($amphur_code){
        $sql="SELECT * FROM const_amphur WHERE AMPHUR_CODE =:code";
        $params=[":code"=>$amphur_code];
        return \Yii::$app->db->createCommand($sql,$params)->queryOne();
    }
    public static function getDistrict($district_code){
        $sql="SELECT * FROM const_district WHERE DISTRICT_CODE =:code";
        $params=[":code"=>$district_code];
        return \Yii::$app->db->createCommand($sql,$params)->queryOne();
    }
    public static function getHospital($code){
        $sql="SELECT * FROM const_hospital WHERE CODE=:code";
        $params=[":code"=>$code];
        return \Yii::$app->db->createCommand($sql,$params)->queryOne();
    }
}
