    
<?php
use miloschuman\highcharts\Highcharts;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;
use backend\modules\ckdpd\classes\LoginForm;
use yii\helpers\Html;
use yii\bootstrap\Tabs;
use kartik\widgets\Select2;
use yii\helpers\ArrayHelper;
use yii\jui\DatePicker;


?>


<?php

echo DatePicker::widget([
    'name'  => 'from_date',
    'value'  => $value,
    //'language' => 'ru',
    'dateFormat' => 'dd-MM-yyyy',
]);
?>

<?php

echo DatePicker::widget([
    'name'  => 'from_date',
    'value'  => $value,
    //'language' => 'ru',
    'dateFormat' => 'dd-MM-yyyy',
]);
?>
<button class="btn btn-success">search</button>
  <?php
// ค่าครีอะตินิน และ eGFR
echo Highcharts::widget([
    'scripts' => [
        'modules/exporting',
        'themes/grid-light',
    ],
    'options' => [
                'chart' => ['renderTo' => 'graph4',],
        'id'=>"graph4",
        'title' => [
            'text' => 'ค่าครีอะตินินและอัตราการกรองของไต',
        ],
        'xAxis' => [
            'categories' => ['6/05/2017', '7/05/2017', '8/05/2017', '9/05/2017', '10/05/2017'],
        ],

        'series' => [
       
             [
                'type' => 'spline',
                'name' => 'ค่าครีอะตินิน',
                 'data' => [['6/05/2017',120], 100, 110,115, 122],
                'marker' => [
                    'lineWidth' => 2,
                    'lineColor' => new JsExpression('Highcharts.getOptions().colors[2]'),
                    'fillColor' => 'white',
                ],
            ],
               [
                'type' => 'spline',
                'name' => 'อัตราการกรองของไต',
                'data' => [90, 80,95, 99, 100],
                'marker' => [
                    'lineWidth' => 2,
                    'lineColor' => new JsExpression('Highcharts.getOptions().colors[1]'),
                    'fillColor' => 'white',
                ],
            ],
 
           
   
        ],
    ]
]);




?>