<?php
use miloschuman\highcharts\Highcharts;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;
use backend\modules\ckdpd\classes\LoginForm;
use yii\helpers\Html;
use yii\bootstrap\Tabs;
use kartik\widgets\Select2;
use yii\helpers\ArrayHelper;
use yii\jui\DatePicker;
use appxq\sdii\widgets\GridView;
?>
  <?php
  
 // echo \appxq\sdii\utils\VarDumper::dump($gdata);
echo Highcharts::widget([
    'scripts' => [
        'highcharts-more',
        'modules/exporting',
        'themes/grid-light',
    ],
    'options' => [
         'chart' => ['renderTo' => 'graphUrin1',],
        'id'=>"graphUrin1",
         'title' => [
            'text' => 'ประมาณปัสสาวะต่อวัน',
        ],
        'xAxis' => [
            'categories' => $datedata,
        ],

        'series' => [

                        [
              'type' => 'column',
                'name' => 'ปริมาณปัสสาวะ',
                'data' =>$gdata,  
                            'plotOptions'=>[
                                
                                'series'=>[
                                    'pointWidth'=>15
                                ]
                            ],
                            'maxPointWidth'=>15,
        
            ],
   
           
   
        ],
    ]
]);
         //  \appxq\sdii\utils\VarDumper::dump($datagrid);

?>
<div id="divgrid3" class="container col-md-12">
<?= GridView::widget([
    'id'=>'grid3',
    'dataProvider' => $datagrid,
    'columns' => [
        ['attribute'=>'ckdpd04_date',
            'label'=>'วันที่'],
             ['attribute'=>'systolic',
            'label'=>'ความดันตัวบน'],
         ['attribute'=>'diastolic',
            'label'=>'ความดันตัวล่าง'],
       ['attribute'=>'bloodsugar_lv_period',
            'label'=>'ระดับนํ้าตาล'],
   
        // ...
    ],
    
    
]) ?>
</div>