<?php

use miloschuman\highcharts\Highcharts;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;
use backend\modules\ckdpd\classes\LoginForm;
use yii\helpers\Html;
use yii\bootstrap\Tabs;
use kartik\widgets\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use kartik\widgets\DatePicker;
?>

<div class="panel panel-primary">
    <div class="panel-heading text-center">
        <h3 class="panel-title">Home Data</h3>
    </div>
    <div class="" id="bodyHomeData">
        <div id="homecontent">

            <div class="container col-md-12">
                <div class="row">
                    <div class="col-md-2 ">
                        <a href="https://backend.thaicarecloud.org/ckdpd/report" class="btn btn-default"  target="_blank">ค้นหาจากรายชื่อ</a>
                    </div>
                    <div class="clearfix"></div>


                </div>


                <!--<div class="container">-->
                <h4>       <div id="con1">    <input type="radio" name="datelength" value="0" checked="true"> กำหนดเอง
                        <input type="radio" name="datelength" value="1"> 7 วัน
                        <input type="radio" name="datelength" value="2"> 15 วัน
                        <input type="radio" name="datelength" value="2"> 30 วัน</div></h4>
                <hr>
                <div class="col-md-5">
                    <?php
                    echo DatePicker::widget([
                        'id' => 'date1',
                        'name' => 'start_date',
                        'value' => $value,
                        'type' => DatePicker::TYPE_COMPONENT_APPEND,
                        'pluginOptions' => [
                            'autoclose' => true,
                            'format' => 'yyyy-mm-dd'
                        ]
                    ]);
                    ?>
                </div>
                <div class="col-md-5">
                    <?php
                    echo DatePicker::widget([
                        'id' => 'date2',
                        'name' => 'end_date',
                        'value' => $value,
                        //'language' => 'ru',
                        'type' => DatePicker::TYPE_COMPONENT_APPEND,
                        'pluginOptions' => [
                            'autoclose' => true,
                            'format' => 'yyyy-mm-dd'
                        ]
                    ]);
                    ?>
                </div>
                <div class="col-md-2">
                    <button id="bSearch" class="btn btn-success" >search</button>
                </div>
                <div class="clearfix"></div>
                <hr>

            </div>
            <p></p>

            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#home">ความดันและระดับนํ้าตาล</a></li>
                <li><a data-toggle="tab" href="#menu1">ความดันและระดับนํ้าตาล (กราฟเส้น)</a></li>
                <li><a data-toggle="tab" href="#menu2">ปริมาตรนํ้ายาล้างไต</a></li>
            </ul>

            <div class="tab-content" id="tab1">
                <div id="home" class="tab-pane fade in active">
                    <h3>ความดันและระดับนํ้าตาล</h3>
                    <div  id="graph1" class="row">
                    </div>
                    <div  id="graph5" class="row">
                    </div>
                    <div  id="graph55" class="row">
                    </div>
                    <div  id="graph11" class="row">

                    </div>
                </div>
                <div id="menu1" class="tab-pane fade">
                    <h3>ความดันและระดับนํ้าตาล (เส้น) </h3>
                    <div  id="graph2" class="row">

                    </div>
                    <div  id="graph6" class="row">
                    </div>
                    <div  id="graph66" class="row">
                    </div>
                    <div  id="graph22" class="row">
                    </div>

                </div>
                <div id="menu2" class="tab-pane fade">
                    <h3>สรุปปริมาตรนํ้ายา</h3>
                    <div  id="graph3" class="row">

                    </div>
                    <div  id="graph33" class="row">

                    </div>
                </div>
                <div id="menu3" class="tab-pane fade">
                    <h3>นํ้าหนัก</h3>
                    <div  id="graphWeight1" class="row">

                    </div>
                    <div  id="graphWeight2" class="row">

                    </div>
                </div>
                <div id="menu4" class="tab-pane fade">
                    <h3>ปัสสาวะ</h3>
                    <div  id="graphUrin1" class="row">

                    </div>
                    <div  id="graphUrin2" class="row">

                    </div>
                </div>

            </div>              
        </div>
    </div>

    <!--</div>-->

    <?php
    $this->registerJs("
  var datelength=0;
  var startdate='';
  var enddate='';
var ptlinkUrl =findGetParameter('ptlink');
var cidUrl =findGetParameter('cid');
var ptidUrl =findGetParameter('ptid');


document.getElementById ('bSearch').addEventListener ('click', callGraphUrl, false);
document.getElementsByName('datelength')[0].addEventListener ('change', changeDatelength0, false);
document.getElementsByName('datelength')[1].addEventListener ('change', changeDatelength7, false);
document.getElementsByName('datelength')[2].addEventListener ('change', changeDatelength15, false);
document.getElementsByName('datelength')[3].addEventListener ('change', changeDatelength30, false);


function startchange(){
startdate=document.getElementsByName('start_date')[0].value;
}

function endchange(){
enddate=document.getElementsByName('end_date')[0].value;
}

function changeDatelength0(){
datelength=0;
}
function changeDatelength7(){
datelength=7;
}
function changeDatelength15(){
datelength=15;
}
function changeDatelength30(){
datelength=30;
}
 function callGraphUrl(){
 console.log('ok');
 startchange();
 endchange();
 showGraph1(ptlinkUrl,ptidUrl,cidUrl);
 showGraph2(ptlinkUrl,ptidUrl,cidUrl);
 showGraph3(ptlinkUrl,ptidUrl,cidUrl);
 showGraph4(ptlinkUrl,ptidUrl,cidUrl);
 showGraph5(ptlinkUrl,ptidUrl,cidUrl);
}
 function callGraph(ptid){
  startchange();
 endchange();
 showGraph1(ptlinkUrl,ptidUrl,cidUrl);
 showGraph2(ptlinkUrl,ptidUrl,cidUrl);
 showGraph3(ptlinkUrl,ptidUrl,cidUrl);
 showGraph4(ptlinkUrl,ptidUrl,cidUrl);
 showGraph5(ptlinkUrl,ptidUrl,cidUrl);

}
    function showGraph1(ptid){
    
         $.ajax({
            url:'" . Url::to(['pressure']) . "?ptlink='+ptlinkUrl+'&daynum=' + datelength+'&startdate='+startdate+'&enddate='+enddate+'&cid='+cidUrl+'&ptid='+ptidUrl,
            method:'GET',
          
            dataType:'HTML',
            success:function(result){
            if(result!=null){
               $('#graph11').html(result); 
               }
             //  showGraph2();
            },error: function (xhr, ajaxOptions, thrownError) {
            }
        });
        }
        
   function showGraph2(ptid){
               $.ajax({
            url:'" . Url::to(['pressure2']) . "?ptlink='+ptlinkUrl+'&daynum=' + datelength+'&startdate='+startdate+'&enddate='+enddate+'&cid='+cidUrl+'&ptid='+ptidUrl,
            method:'GET',
            dataType:'HTML',
            success:function(result){
               $('#graph22').html(result); 
           //    showGraph3();
            },error: function (xhr, ajaxOptions, thrownError) {
            }
        });
        }
        
   function showGraph3(ptid) {
      $.ajax({
            url:'" . Url::to(['sol']) . "?ptlink='+ptlinkUrl+'&daynum=' + datelength+'&startdate='+startdate+'&enddate='+enddate+'&cid='+cidUrl+'&ptid='+ptidUrl,
            method:'GET',
        
            dataType:'HTML',
            success:function(result){
               $('#graph33').html(result); 
           //    showGraph4();
            },error: function (xhr, ajaxOptions, thrownError) {
            }
        });
        }
        
    function showGraph4(ptid){
      $.ajax({
            url:'" . Url::to(['egfr']) . "?ptlink='+ptlinkUrl+'&daynum=' + datelength+'&startdate='+startdate+'&enddate='+enddate+'&cid='+cidUrl+'&ptid='+ptidUrl,
            method:'GET',
            dataType:'HTML',
            success:function(result){
               $('#graph4').html(result); 
            },error: function (xhr, ajaxOptions, thrownError) {
            }
        });
        }
           function showGraph5(ptid){
      $.ajax({
            url:'" . Url::to(['sugar']) . "?ptlink='+ptlinkUrl+'&daynum=' + datelength+'&startdate='+startdate+'&enddate='+enddate+'&cid='+cidUrl+'&ptid='+ptidUrl,
            method:'GET',
            dataType:'HTML',
            success:function(result){
               $('#graph55').html(result); 
            },error: function (xhr, ajaxOptions, thrownError) {
            }
        });
        }

function findGetParameter(parameterName) {
    var result = '',
        tmp = [];
    var items = location.search.substr(1).split('&');
    for (var index = 0; index < items.length; index++) {
        tmp = items[index].split('=');
        if (tmp[0] === parameterName) result = decodeURIComponent(tmp[1]);
    }
    return result;
}

callGraphUrl();
    
");
    ?>
