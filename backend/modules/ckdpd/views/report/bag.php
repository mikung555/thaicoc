    
<?php
use miloschuman\highcharts\Highcharts;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;
use backend\modules\ckdpd\classes\LoginForm;
use yii\helpers\Html;
use yii\bootstrap\Tabs;
use kartik\widgets\Select2;
use yii\helpers\ArrayHelper;
use yii\jui\DatePicker;
use appxq\sdii\widgets\GridView;

//\appxq\sdii\utils\VarDumper::dump($datedata);
?>

<div id="divgrid3" class="container col-md-12">
<?php

try{
    echo GridView::widget([
    'id'=>'grid3',
    'dataProvider' => $dataArr,
    'columns' => [
        ['attribute'=>'pname',
            'label'=>'ชื่อ-นามสกุล'],
             ['attribute'=>'create_date',
            'label'=>'วันที่'],
         ['attribute'=>'ttmedset',
            'label'=>'จำนวนนํ้ายา']
        // ...
    ],
]);
} catch (Exception $ex) {

}

 ?>
</div>

