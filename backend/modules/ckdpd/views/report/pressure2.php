<?php
use miloschuman\highcharts\Highcharts;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;
use backend\modules\ckdpd\classes\LoginForm;
use yii\helpers\Html;
use yii\bootstrap\Tabs;
use kartik\widgets\Select2;
use yii\helpers\ArrayHelper;
use yii\jui\DatePicker;
use appxq\sdii\widgets\GridView;


?>


       <?php
echo Highcharts::widget([
    'scripts' => [
        'modules/exporting',
        'themes/grid-light',
    ],
    'options' => [
         'chart' => ['renderTo' => 'graph2',],
        'id'=>"graph2",
        'title' => [
            'text' => 'กราฟความดันและระดับนํ้าตาล',
        ],
        'xAxis' => [
            'categories' =>$datedata,
        ],

        'series' => [
            
             [
                'type' => 'spline',
                'name' => 'ความดันตัวบน',
                 'data' =>$gdataP1,
                'marker' => [
                    'lineWidth' => 2,
                    'lineColor' => new JsExpression('Highcharts.getOptions().colors[2]'),
                    'fillColor' => 'white',
                ],
            ],
               [
                'type' => 'spline',
                'name' => 'ความดันตัวล่าง',
                'data' => $gdataP2,
                'marker' => [
                    'lineWidth' => 2,
                    'lineColor' => new JsExpression('Highcharts.getOptions().colors[1]'),
                    'fillColor' => 'white',
                ],
            ],
 
           
   
        ],
    ]
]);

?>

<div  id="divgrid2" class="container col-md-12">
<?= GridView::widget([
    'id'=>'grid2',
    'dataProvider' => $datagrid,
    'columns' => [
        ['attribute'=>'ckdpd04_date',
            'label'=>'วันที่'],
             ['attribute'=>'systolic',
            'label'=>'ความดันตัวบน'],
         ['attribute'=>'diastolic',
            'label'=>'ความดันตัวล่าง'],
       ['attribute'=>'bloodsugar_lv_period',
            'label'=>'ระดับนํ้าตาล'],
   
        // ...
    ],
    
    
]) ?>
</div>