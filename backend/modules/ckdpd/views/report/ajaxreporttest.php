<?php

use backend\modules\ckdpd\assets\CkdpdAsset;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

CkdpdAsset::register($this);
?>    
<div class="container">
        <h2>Dynamic Tabs</h2>
        <ul class="nav nav-tabs">
            <li class="active">
                <a data-toggle="tab" href="#home">
                    <i class="glyphicon glyphicon-home"></i> Home</a>
            </li>
            <li>
                <a data-toggle="tab" href="#setting"><i class="glyphicon glyphicon-cog"></i> Setting</a>
            </li>
            <li>
                <a data-toggle="tab" href="#demoTable"><i class="glyphicon glyphicon-table"></i> Table</a>
            </li>
        </ul>

        <div class="tab-content">
            <div id="home" class="tab-pane fade in active">  
                 Home     
            </div>
            <div id="setting" class="tab-pane fade">
                <h3></h3>
                <div id="show-setting">
                    setting
                </div>
            </div>
            <div id="demoTable" class="tab-pane fade">
                <div id="show-dataTable">
                    Demo
                </div>
            </div>
        </div>
    </div>


