    
<?php
use miloschuman\highcharts\Highcharts;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;
use backend\modules\ckdpd\classes\LoginForm;
use yii\helpers\Html;
use yii\bootstrap\Tabs;
use kartik\widgets\Select2;
use yii\helpers\ArrayHelper;
use yii\jui\DatePicker;
use appxq\sdii\widgets\GridView;

//\appxq\sdii\utils\VarDumper::dump($datedata);
?>


  <?php
echo Highcharts::widget([
    'scripts' => [
        'modules/exporting',
        'themes/grid-light',
    ],
    'options' => [
        'chart' => ['renderTo' => 'graph3',],
        'id'=>"graph3",
        'title' => [
            'text' => 'สรุปปริมาตรนํ้ายาล้างไตที่เข้าออกร่างกายต่อวัน',
        ],
        'xAxis' => [
            'categories' => $datedata,
        ],
       
        
               'series' => [
            [
                'type' => 'column',
                'name' => 'นํ้าเข้า',
                'data' => $suminArr,
                "stack"=> 'in',
                     'maxPointWidth'=>15,
            ],
            [
                'type' => 'column',
                'name' => 'นํ้าออก',
                'data' => $sumoutArr,
               "stack"=> 'out',
                     'maxPointWidth'=>15,

            ],
            [
                'type' => 'column',
                'name' => 'สรุป',
                'data' => $sumArr,
                "stack"=> 'sum',
                    'maxPointWidth'=>15,

            ],
        
                   
      
        ],
        
          "plotOptions"=>[
              "column"=>[ "stacking"=> 'normal']
          ]
  
   
    ]
]);

//echo "test". $suminArr;
?> 
<div  id="divgrid1" class="container col-md-12">
<?= GridView::widget([
    'id'=>'grid1',
    'dataProvider' => $datagrid,
    'columns' => [
        
        ['attribute'=>'ckdpd02date','label'=>'วันที่',  'contentOptions' => ['style' => 'width:180px;  min-width:100px;  '],],
        ['attribute'=>'r1invol','label'=>'นํ้าเข้ารอบที่ 1'], 
           ['attribute'=>'r1outvol','label'=>'นํ้าออกรอบที่ 1'], 
        
             ['attribute'=>'r2invol','label'=>'นํ้าเข้ารอบที่ 2'], 
           ['attribute'=>'r2outvol','label'=>'นํ้าออกรอบที่ 2'], 

        
             ['attribute'=>'r3invol','label'=>'นํ้าเข้ารอบที่ 3'], 
           ['attribute'=>'r3outvol','label'=>'นํ้าออกรอบที่ 3'], 

        
             ['attribute'=>'r4invol','label'=>'นํ้าเข้ารอบที่ 4'], 
           ['attribute'=>'r4outvol','label'=>'นํ้าออกรอบที่ 4'], 

        
             ['attribute'=>'r5invol','label'=>'นํ้าเข้ารอบที่ 5'], 
           ['attribute'=>'r5outvol','label'=>'นํ้าออกรอบที่ 5'], 

        
             ['attribute'=>'r6invol','label'=>'นํ้าเข้ารอบที่ 6'], 
           ['attribute'=>'r6outvol','label'=>'นํ้าออกรอบที่ 6'], 

        
             ['attribute'=>'r7invol','label'=>'นํ้าเข้ารอบที่ 7'], 
           ['attribute'=>'r7outvol','label'=>'นํ้าออกรอบที่ 7'], 
            ['attribute'=>'med_inbody','label'=>'สรุปปริมาตรนํ้ายาออก'], 

    ],
    
    
]) ?>
</div>