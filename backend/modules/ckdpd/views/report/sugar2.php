<?php
use miloschuman\highcharts\Highcharts;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;
use backend\modules\ckdpd\classes\LoginForm;
use yii\helpers\Html;
use yii\bootstrap\Tabs;
use kartik\widgets\Select2;
use yii\helpers\ArrayHelper;
use yii\jui\DatePicker;
use appxq\sdii\widgets\GridView;
?>
  <?php
  
 // echo \appxq\sdii\utils\VarDumper::dump($gdata);
echo Highcharts::widget([
    'scripts' => [
        'highcharts-more',
        'modules/exporting',
        'themes/grid-light',
    ],
    'options' => [
         'chart' => ['renderTo' => 'graph55',],
        'id'=>"graph5",
         'title' => [
            'text' => 'กราฟระดับนํ้าตาล',
        ],
        'xAxis' => [
            'categories' => $datedata,
        ],

        'series' => [

      
        [
                'type' => 'spline',
                'name' => 'ระดับนํ้าตาล',
                'data' => $gdata2,
                'marker' => [
                    'lineWidth' => 2,
                    'lineColor' => new JsExpression('Highcharts.getOptions().colors[1]'),
                    'fillColor' => 'white',
                ],
            ]
 
           
   
        ],
    ]
]);
         //  \appxq\sdii\utils\VarDumper::dump($datagrid);

?>


  <?php
  
 // echo \appxq\sdii\utils\VarDumper::dump($gdata);
echo Highcharts::widget([
    'scripts' => [
        'highcharts-more',
        'modules/exporting',
        'themes/grid-light',
    ],
    'options' => [
         'chart' => ['renderTo' => 'graph6',],
        'id'=>"graph6",
         'title' => [
            'text' => 'กราฟระดับนํ้าตาล',
        ],
        'xAxis' => [
            'categories' => $datedata,
        ],

        'series' => [

      
        [
                'type' => 'spline',
                'name' => 'ระดับนํ้าตาล',
                'data' => $gdata2,
                'marker' => [
                    'lineWidth' => 2,
                    'lineColor' => new JsExpression('Highcharts.getOptions().colors[1]'),
                    'fillColor' => 'white',
                ],
            ]
 
           
   
        ],
    ]
]);
         //  \appxq\sdii\utils\VarDumper::dump($datagrid);

?>
