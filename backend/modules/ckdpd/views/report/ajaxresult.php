<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use yii\grid\GridView;
use yii\data\ActiveDataProvider;
use backend\modules\ckdpd\assets\CkdpdAsset;
use yii\helpers\Url;
use yii\widgets\Pjax;

CkdpdAsset::register($this);
//appxq\sdii\utils\VarDumper::dump($countdata);
?>
<div class="container">
    <div class="alert alert-warning report84-desc" role="alert"><b>สรุปยอดข้อมูล</b><br>
        <li>มีผู้ป่วย PD ทั้งหมด <font color="red"><b><?=$countdata['patientCount']?> </b></font> ราย </li> 
        <li>ติดตั้ง CKD Pro <font color="red"><b><?= $countdata['app_install'] ?> </b></font> ราย คิดเป็น  <font color="red"><b><?php echo '' + number_format( ($countdata['app_install']/$countdata['patientCount'])*100,1,'.',' '); ?></b></font> %</li> 
        <li>ยังไม่ติดตั้งติดตั้ง <font color="red"><b>90</b></font> ราย คิดเป็น  <font color="red"><b><?php echo '' + number_format( (($countdata['patientCount']-$countdata['app_install'])/$countdata['patientCount'])*100,1,'.',' '); ?></b></font> %</li> 

    </div>
    <div class="alert alert-warning report84-desc" role="alert">
        <li>จากที่ติดตั้งแอปทั้งหมด <font color="red"><b><?= $countdata['app_install'] ?> </b></font> ราย </li> 
        <li>มีการส่งข้อมูลเข้ามาอย่างน้อย 1 ครั้งต่อวัน <font color="red"><b><?=$countdata['count_send'] ?></b></font> ราย คิดเป็น  <font color="red"><b><?php echo '' + number_format( ($countdata['count_send']/$countdata['app_install'])*100,1,'.',' '); ?></b></font> %</li> 
        <li>ที่ไม่ใช้มี <font color="red"><b>9</b></font> ราย คิดเป็น  <font color="red"><b><?php echo '' + number_format( (($countdata['app_install']-$countdata['count_send'])/$countdata['app_install'])*100,1,'.',' '); ?></b></font> %</li> 

    </div>
    <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#pic">ภาพจากผู้ป่วย</a></li>
        <li><a data-toggle="tab" href="#alert">ข้อความแจ้งเตือน</a></li>
        <li><a data-toggle="tab" href="#bag">เตือนนํ้ายาเหลือน้อย</a></li>
  
    </ul>

    <div class="tab-content">
        <div id="pic" class="tab-pane  active">
                <div class="panel panel-primary">
        <div class="panel-heading">แจ้งเตือนรูป</div>
        <div class="panel-body">
    <?php  Pjax::begin(['id'=>'inv-person-grid-pjax', 'timeout' => 1000*60]);?>
             <?php
    echo GridView::widget([
        'id' => 'inv-person-grid',
        'dataProvider' => $provider,
        'columns' => [
            [
                'label' => 'รหัส',
                'format' => 'raw',
                'contentOptions' => ['class' => 'col-2','align'=>'left'],
                'headerOptions' => ['width' => '15%','style'=>'text-align:center'],
                'value' => function($provider) {
                    return $provider['id'];
                }
            ],
            [
                'label' => 'ชื่อ',
                'format' => 'raw',
                'contentOptions' => ['class' => 'col-5','align'=>'left'],
                'headerOptions' => ['width' => '40%','style'=>'text-align:center'],
                'value' => function($provider) {
                    return $provider['name'];
                }
            ],
            [
                'label' => 'รูป',
                'format' => 'raw',
                'contentOptions' => ['class' => 'col-5','align'=>'center'],
                'headerOptions' => ['width' => '45%','style'=>'text-align:center'],
                'value' => function($provider) {
                    $imgpath = $provider['picture'];
                    return '<a href=' . Url::to('/fileinput/' . $imgpath) . '>'
                            . "<img src='" . Url::to('/fileinput/' . $imgpath) . "'/>"
                            . '</a>';
                }
            ],
        // ...
        ],
    ]);
    ?>
    

            <?php Pjax::end(); ?>
        </div>
    </div>
        </div>
        <div id="alert" class="tab-pane">
                  <div class="panel panel-primary">
        <div class="panel-heading">ข้อความแจ้งเตือน</div>
        <div class="panel-body">
                <?php  Pjax::begin(['id'=>'inv-person-grid-alert-pjax', 'timeout' => 1000*60]);?>
             <?php
           //  echo \appxq\sdii\utils\VarDumper::dump($providerAlert);
    echo GridView::widget([
        'id' => 'inv-person-grid-alert',
        'dataProvider' => $providerAlert,
        'columns' => [
            [
                'label' => 'วันที่',
                'format' => 'raw',
                'contentOptions' => ['class' => 'col-2','align'=>'left'],
                'headerOptions' => ['width' => '15%','style'=>'text-align:center'],
                'value' => function($providerAlert) {
                    return $providerAlert['create_date'];
                }
            ],
            [
                'label' => 'ชื่อ',
                'format' => 'raw',
                'contentOptions' => ['class' => 'col-5','align'=>'left'],
                'headerOptions' => ['width' => '40%','style'=>'text-align:center'],
                'value' => function($providerAlert) {
                    return $providerAlert['pname'];
                }
            ],
          [
                'label' => 'ข้อความแจ้งเตือน',
                'format' => 'raw',
                'contentOptions' => ['class' => 'col-5','align'=>'center'],
                'headerOptions' => ['width' => '45%','style'=>'text-align:center'],
                'value' => function($providerAlert) {
                    $imgpath = $providerAlert['notification'];
                    return  $imgpath ;
                }
            ],
        // ...
        ],
    ]);
    ?>
    
            <?php Pjax::end(); ?>
        </div>
        </div>

        </div>
        <div id="bag" class="tab-pane">
            
                              <div class="panel panel-primary">
        <div class="panel-heading">นํ้ายาเหลือน้อย</div>
        <div class="panel-body">
                <?php  Pjax::begin(['id'=>'inv-person-grid-bag-pjax', 'timeout' => 1000*60]);?>

             <?php
    echo GridView::widget([
        'id' => 'inv-person-grid-bag',
        'dataProvider' => $providerBag,
        'columns' => [
            [
                'label' => 'ชื่อ',
                'format' => 'raw',
                'contentOptions' => ['class' => 'col-2','align'=>'left'],
                'headerOptions' => ['width' => '35%','style'=>'text-align:center'],
                'value' => function($providerBag) {
                    return $providerBag['pname'];
                }
            ],
            [
                'label' => 'วันที่',
                'format' => 'raw',
                'contentOptions' => ['class' => 'col-5','align'=>'left'],
                'headerOptions' => ['width' => '20%','style'=>'text-align:center'],
                'value' => function($providerBag) {
                    return $providerBag['create_date'];
                }
            ],
            [
                'label' => 'ข้อความแจ้งเตือน',
                'format' => 'raw',
                'contentOptions' => ['class' => 'col-5','align'=>'center'],
                'headerOptions' => ['width' => '45%','style'=>'text-align:center'],
                'value' => function($providerBag) {
                    $imgpath = $providerBag['ttmedset'];
                    return  $imgpath ;
                }
            ],
        // ...
        ],
    ]);
    ?>
    
            <?php Pjax::end(); ?>
        </div>
        </div>
            
        </div>

         </div>
    


</div>
<div class="container">

   
</div>