    
<?php
use miloschuman\highcharts\Highcharts;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;
use backend\modules\ckdpd\classes\LoginForm;
use yii\helpers\Html;
use yii\bootstrap\Tabs;
use kartik\widgets\Select2;
use yii\helpers\ArrayHelper;
use yii\jui\DatePicker;
use appxq\sdii\widgets\GridView;

//\appxq\sdii\utils\VarDumper::dump($datedata);
?>


       <?php
echo Highcharts::widget([
    'scripts' => [
        'modules/exporting',
        'themes/grid-light',
    ],
    'options' => [
         'chart' => ['renderTo' => 'graph2',],
        'id'=>"graph2",
        'title' => [
            'text' => 'กราฟความดันและระดับนํ้าตาล',
        ],
        'xAxis' => [
            'categories' =>$datedata,
        ],

        'series' => [
            
             [
                'type' => 'spline',
                'name' => '',
                 'data' =>[],
                'marker' => [
                    'lineWidth' => 2,
                    'lineColor' => new JsExpression('Highcharts.getOptions().colors[2]'),
                    'fillColor' => 'white',
                ],
            ],
              
   
        ],
    ]
]);

?>
