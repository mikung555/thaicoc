<?php
use miloschuman\highcharts\Highcharts;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;
use backend\modules\ckdpd\classes\LoginForm;
use yii\helpers\Html;
use yii\bootstrap\Tabs;
use kartik\widgets\Select2;
use yii\helpers\ArrayHelper;
use yii\jui\DatePicker;
use appxq\sdii\widgets\GridView;
use yii\widgets\Pjax;
use common\lib\sdii\widgets\SDGridView;
use common\lib\sdii\widgets\SDModalForm;
use common\lib\sdii\components\helpers\SDNoty;
use yii\helpers\Url;
?>

    <div class="modal-content">
      <div class="modal-header">
        <h2 class="modal-title">ข้อมูลนํ้าออก</h2>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <p><div id="waternum" align='center'>กำลังดึงข้อมูล...</div></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
 



<?php
$this->registerJs("
 //   localstoage.setData
    var waternumFirst=0;

function getdata(){
var pdcid = $('#pd-modal-ajax').attr('data-id');
$.ajax({
  url: '" .Url::to(['/ckdpd/water'])     .  "/get-num?cid='+pdcid,
  context: document.body
}).done(function(data) {
console.log(data);
  $('#waternum').html('<h1>'+ data + '</h1>');
  waternumFirst=data;
});

}
setInterval(getdata,2000);

");

?>