<?php   
    $this->title="Water";
 
?>
<div id="water-output"></div>


<?php $this->registerJS("
    var count_str = '".$count."';
    function GetWater(){
        $.ajax({
            url:'".\yii\helpers\Url::to(['/ckdpd/water'])."',
            type:'get',
            data:{data:1},
            success:function(res){
                $('#water-output').html(res);
            }
        });
    }GetWater();
     function CheckCount(){
        $.ajax({
            url:'".\yii\helpers\Url::to(['/ckdpd/water/get-count'])."',
            type:'get',
            success:function(res){
               if(parseInt(res) != parseInt(count_str) ){
                    count_str = res;
                    GetWater();
               }  
            }
        });
    }CheckCount();
    setInterval(function(){
        CheckCount();  
    },1000);

")?>