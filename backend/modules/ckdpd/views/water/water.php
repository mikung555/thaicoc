<?php
use miloschuman\highcharts\Highcharts;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;
use backend\modules\ckdpd\classes\LoginForm;
use yii\helpers\Html;
use yii\bootstrap\Tabs;
use kartik\widgets\Select2;
use yii\helpers\ArrayHelper;
use yii\jui\DatePicker;
use appxq\sdii\widgets\GridView;
use yii\widgets\Pjax;
use common\lib\sdii\widgets\SDGridView;
use common\lib\sdii\widgets\SDModalForm;
use common\lib\sdii\components\helpers\SDNoty;
use yii\helpers\Url;
?>
<?php   
    $this->title="ข้อมูลการล้างไต";
?>
<div class="container">
<?= GridView::widget([
    'id'=>'pd-table',
    'dataProvider' => $datagrid,
       'columns' => [ ['attribute'=>'maxdate',
            'label'=> 'เวลาล่าสุด'],
        ['attribute'=>'cid',
            'label'=> 'รหัสบัตรประชาชน'],
          [
                'label'=>'ดูข้อมูล',
              'attribute'=>'pdmode',
                'format'=>'raw',
                'value'=> function ($model){
                    return Html::a('click',"#" ,['class'=>'btn btn-success','id'=>"$model[cid]"]) ;
                }
            ],
                              [
                'label'=>'สถานะ',
              'attribute'=>'pdstatus',
                 'value'=> function ($model){
                    return $model[pdstatus]==2?"เสร็จสิ้น":"กำลังดำเนินการ" ;
                }
          
            ]
      
        // ...
    ],
    
]) ?>
</div>
<?=  SDModalForm::widget([
    'id' => 'pd-modal-ajax',
    'size'=>'modal-lg'
    
]);
?>

<?php
$this->registerJs("
$('#pd-table').on('click', 'a', function() {


//alert($(this).attr('id'));
 $('#pd-modal-ajax').modal('show')
            .find('.modal-content')
            .load('". Url::to(['/ckdpd/water'])   . "/get-water?cid='+$(this).attr('id'));
              $('#pd-modal-ajax').attr('data-id',$(this).attr('id'));


});"
        );
        
        ?>