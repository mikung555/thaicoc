<?php
use yii\web\JsExpression;
use yii\bootstrap\ActiveForm;
use appxq\sdii\helpers\SDNoty;
use appxq\sdii\helpers\SDHtml;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

?>
<div id="assign-user">
        <?php $form = ActiveForm::begin([
	'id'=>'assign-user',
    ]); ?>
    <div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	<h4 class="modal-title" id="itemModalLabel">หมอบหมาย</h4>
    </div>

    <div class="modal-body">
        <?php
         //\appxq\sdii\utils\VarDumper::dump($map_user);
        echo kartik\select2\Select2::widget([
            'attribute'=>'user_name',
            'model'=>$model,
            'options' => ['placeholder' => 'Search ...', 'multiple' => true],
            'initValueText'=>$userlist_text,
            'pluginOptions' => [
                    'tokenSeparators' => [',', ' '],
                    'minimumInputLength' => 0,
                    'ajax' => [
                        'url' => \yii\helpers\Url::to(['/article/research/author-list']),
                        'dataType' => 'json',
                        'data' => new JsExpression('function(params) { return {q:params.term}; }')
                    ],
                    'escapeMarkup' => new JsExpression('function (markup) {  return markup; }'),
                    'templateResult' => new JsExpression('function(param) { return param.text; }'),
                    'templateSelection' => new JsExpression('function (param) {return param.text; }'),
            ],
            'pluginEvents' => [
                "select2:select" => "function(e) { 
                $.ajax({
                    method: 'POST',
                    url: '".Url::to(['/inv/inv-person/assign-add', 
                        'target_id' => $target_id,
                        'ptid' => $ptid,
                        'gid' => $gid,
                        'sitecode' => $sitecode,
                        'user_id'=>''
                    ])."'+e.params.data.id,
                    dataType: 'JSON',
                    success: function(result, textStatus) {
                        if(result.status == 'success') {
                            ". SDNoty::show('result.message', 'result.status') ."
                        } else {
                            ". SDNoty::show('result.message', 'result.status') ."
                        }
                    }
                });
 }",
                "select2:unselect" => "function(e) { 
                $.ajax({
                    method: 'POST',
                    url: '".Url::to(['/inv/inv-person/assign-del', 
                        'target_id' => $target_id,
                        'ptid' => $ptid,
                        'gid' => $gid,
                        'sitecode' => $sitecode,
                        'user_id'=>''
                    ])."'+e.params.data.id,
                    dataType: 'JSON',
                    success: function(result, textStatus) {
                        if(result.status == 'success') {
                            ". SDNoty::show('result.message', 'result.status') ."
                        } else {
                            ". SDNoty::show('result.message', 'result.status') ."
                        }
                    }
                });
 }",

            ]
        ]);?>
    </div>
    <?php ActiveForm::end(); ?>
</div>

<?php  $this->registerJs("

");?>