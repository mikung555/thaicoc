<?php 
use yii\helpers\Html;
use yii\helpers\Url;

$key_form = \common\lib\codeerror\helpers\GenMillisecTime::getMillisecTime();
$indexArry = array_keys($dataSubForm);

$valF = isset($indexArry[0])?$indexArry[0]:0;

$dataDateFieldsResult = ['create_date'=>'Create Date'];
?>
<div id="<?=$key_form?>" class="row form-fields dads-children" data-id="<?=$key_form?>" style="margin-bottom: 15px;">
    <div class="col-md-12 draggable text-center" style="color: #999; padding-bottom: 10px; cursor: -webkit-grab;"><i class="glyphicon glyphicon-resize-vertical"></i><i class="glyphicon glyphicon-resize-vertical"></i><i class="glyphicon glyphicon-resize-vertical"></i></div>
    <div class="col-md-2 "><?= Html::dropDownList("forms[$key_form][form]", '', $dataSubForm, ['class'=>'form-control form-input'])?></div>
    <div class="col-md-2 sdbox-col"><input type="text" class="form-control label-input" name="forms[<?=$key_form?>][label]" value=""></div>
    <div class="col-md-1 sdbox-col"><?= Html::dropDownList("forms[$key_form][date]", $value_form['date'], $dataDateFieldsResult, ['class'=>'form-control date-input'])?></div>
    <div class="col-md-1 sdbox-col"><div class="input-group iconpicker-container"><input type="text" class="form-control dicon-input icp icp-auto iconpicker-input iconpicker-element" name="forms[<?=$key_form?>][icon]" value="fa-star" ><span class="input-group-addon"><i></i></span></div></div>
    <div class="col-md-1 sdbox-col"><?=  Html::textInput("forms[$key_form][color]", '#93c47d', ['class'=>'form-control input-color'])?></div>
    <div class="col-md-1 sdbox-col"><?= Html::checkbox("forms[$key_form][show]", true, ['label'=>'แสดงทันที'])?></div>
    <div class="col-md-1 sdbox-col"><?= Html::checkbox("forms[$key_form][adddata]", true, ['label'=>'เพิ่มข้อมูลได้'])?></div>
    <div class="col-md-1 sdbox-col"><?= Html::checkbox("forms[$key_form][conddate]", true, ['label'=>'แสดงตามช่วงเวลา'])?></div>
    <div class="col-md-1 sdbox-col"><?= Html::checkbox("forms[$key_form][addperson]", true, ['label'=>'เพิ่มผู้ป่วยได้'])?></div>
    <div class="col-md-1 sdbox-col">
        <button type="button" class="forms-items-del btn btn-danger"><i class="glyphicon glyphicon-remove"></i></button>
    </div>
    <?=  Html::hiddenInput("forms[$key_form][ezf_name]", $value_form['ezf_name'], ['class'=>'en-input']);?>
    <?=  Html::hiddenInput("forms[$key_form][ezf_table]", $value_form['ezf_table'], ['class'=>'et-input']);?>
    <?=  Html::hiddenInput("forms[$key_form][unique_record]", $value_form['unique_record'], ['class'=>'ur-input']);?>
</div> 
<?php  
$this->registerJs("
var color_options = {
    showInput: true,
    showPalette:true,
    showPaletteOnly: true,
    hideAfterPaletteSelect:true,
    preferredFormat: 'name',
    palette: [
        ['blue','red','darkred','orange','green','darkgreen','purple','darkpuple', 'cadetblue'],
    ]
};
$('.input-color').spectrum(color_options);

$('.dicon-input').iconpicker({
    title: 'Using Font Awesome',
    placement:'top',
//    icons: ['bed', 'bug', 'bolt', 'ban', 'book', 'bell', 'birthday-cake', 'bookmark', 'building', 'calculator', 'calendar',
//    'bus', 'camera', 'car', 'check', 'times', 'check-circle', 'check-circle-o', 'circle', 'circle-o', 'clock-o', 'child', 'cloud',
//    'coffee', 'cube', 'cubes', 'cutlery', 'envelope-o', 'diamond', 'exclamation-circle', 'exchange', 'female', 'male', 'flask',
//    'folder-open-o', 'folder-o', 'users', 'user', 'heartbeat', 'heart-o', 'heart', 'gift', 'globe', 'picture-o', 'minus-circle',
//    'phone', 'question-circle', 'plus-circle', 'shield', 'share-alt', 'star', 'star-o', 'thumbs-o-up', 'thumbs-o-down', 'times-circle',
//    'unlock-alt', 'unlock', 'wrench', 'trophy', 'trash', 'tree', 'life-ring', 
//    'shopping-cart', 'paper-plane', 'search', 'retweet', 'random', 'wheelchair', 'user-md', 'stethoscope', 'hospital-o', 'medkit',
//    'h-square', 'ambulance', 'link', 'chain-broken'
//    ],
    iconBaseClass: 'fa',
    iconComponentBaseClass: 'fa',
    iconClassPrefix: 'fa-'
});

$('#$key_form .form-input').trigger('change');
");?>