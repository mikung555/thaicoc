<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\ovcca\models\OvFilter */

$this->title = Yii::t('app', 'Create Ov Filter');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Ov Filters'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ov-filter-create">

    <?= $this->render('_form_fileter', [
        'model' => $model,
    ]) ?>

</div>
