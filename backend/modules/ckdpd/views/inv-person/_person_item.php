<tr data-id="<?= $value['inv_id'] ?>"> 
    <td><?= $value['inv_name'] ?></td> 
    <td><?= \common\lib\sdii\components\utils\SDdate::mysql2phpThDateSmall(date('Y-m-d')) ?></td> 
    <td>
        <?php
            echo \yii\helpers\Html::a("EMR", NULL, [
                'data-url' => \yii\helpers\Url::to(['/inv/inv-person/ezform-emr',
                    'ezf_id' => $value['person_ezf_id'],
                    'target' => base64_encode($value['person_target']),
                    'comp_id_target' => $value['person_comp_target'],
                ]),
                'class' => 'btn btn-xs btn-primary open-ezfrom-emr',
                'style' => 'cursor: pointer;',
            ]);
        ?>
        <button class="btn btn-xs btn-danger btn-del" 
                data-url="<?= \yii\helpers\Url::to(['/inv/inv-map/delete', 'id' => $value['inv_id']]) ?>">Delete</button>
    </td> 
</tr> 