<?php
use kartik\form\ActiveForm;
use yii\helpers\Html;
use appxq\sdii\helpers\SDHtml;
use common\lib\sdii\components\helpers\SDNoty;

?>

<?php $form = ActiveForm::begin([
	'id'=>'sublist-form',
    ]); ?>

<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title" id="itemModalLabel">เลือกเป้าหมายเพื่อเพิ่มลงในใบกำกับงาน</h4>
</div>
<?php
//$dataSub = \yii\helpers\ArrayHelper::map($dataOvFilterSub, 'sub_id', 'sub_name');
?>
<div class="modal-body">
    <div class="row">
	<div class="col-md-12">
	    <div class="col-md-5">
		<?=Html::dropDownList('select', $select, $dataOvFilterSub, ['class'=>'form-control', 'data-url'=>  \yii\helpers\Url::to(['inv-person/addlist', 'comp'=>$comp, 'ovfilter_sub'=>$ovfilter_sub]), 'prompt'=>'ทั้งหมด', 'id'=>'selectList'])?>
	    </div>
	    
	</div>
    </div>
    
    <?php
    if($ovfilter_sub!=0){
	$ovsub = \backend\modules\inv\models\InvFilterSub::findOne(['sub_id'=>$ovfilter_sub]);
    }
    ?>
    
    <?= \common\lib\sdii\widgets\DualListBox::widget([
    'model' => $model,
    'attribute' => 'list_person',
    'title' => 'ใบกำกับงาน',
    'data' => $data,
    'data_id'=> 'id',
    'data_value'=> 'name',
	'lngOptions'=>[
	    'available' => 'Source:ทั้งหมด',
	    'selected' => (isset($ovsub))?'Target:'.$ovsub['sub_name']:'ใบกำกับงานย่อย',
	    'showing' => 'จำนวน',
	    'search_placeholder'=>'ค้นหา'
	]
  ]);?>
    
    
</div>
<div class="modal-body">
    <hr>
    <h4>คู่มือ</h4>
    
    <div class="row">
	<div class="col-md-6">
	    <p><button type="button" class="btn btn-default">
		<span class="glyphicon glyphicon-chevron-right"></span> 
		<span style="margin-left: -10px;" class="glyphicon glyphicon-chevron-right"></span>
	    </button> = เลือกทั้งหมด </p>
	    <p><button type="button" class="btn btn-default">
		<span class="glyphicon glyphicon-chevron-right"></span>
	    </button> = เลือกที่เลือก </p>
	    
	    <p><code>Ctrl+Clik</code> = เลือกที่ละรายการ</p>
	    <p><code>Ctrl+A</code> = เลือกทั้งหมด</p>
	    <p><code>Shift+Clik</code> = เลือกเป็นช่วง</p>
	</div>
	<div class="col-md-6">
	    <p><button type="button" class="btn btn-default">
		<span class="glyphicon glyphicon-chevron-left"></span>
	    </button> = คืนที่เลือก </p>
	    <p><button type="button" class="btn btn-default">
		<span class="glyphicon glyphicon-chevron-left"></span>
		<span style="margin-left: -10px;" class="glyphicon glyphicon-chevron-left"></span>
	    </button> = คืนทั้งหมด </p>
	    
	    
	</div>
    </div>
    
    
    
    
    
</div>
<div class="modal-footer">
    <?= Html::submitButton('บันทึก', ['class' => 'btn btn-primary']) ?>
    
</div>

<?php ActiveForm::end(); ?>

<?php  $this->registerJs("
$('#selectList').change(function(){
    var url = $(this).attr('data-url');
    getWidgetListItem(url, this.value);
});

function getWidgetListItem(url, val) {
    $('#modal-inv-person .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
    $.ajax({
	method: 'POST',
	url: url,
	data: {select:val},
	dataType: 'JSON',
	success: function(result, textStatus) {
	    $('#modal-inv-person .modal-content').html(result.html);
	}
    });
}

$('form#sublist-form').on('beforeSubmit', function(e) {
    var \$form = $(this);
    $.post(
	\$form.attr('action'), //serialize Yii2 form
	\$form.serialize()
    ).done(function(result) {
	if(result.status == 'success') {
	    ". SDNoty::show('result.message', 'result.status') ."
	    if(result.action == 'create') {
		$(document).find('#modal-ovlist').modal('hide');
		$.pjax.reload({container:'#inv-person-grid-pjax'});
	    }
	} else {
	    ". SDNoty::show('result.message', 'result.status') ."
	} 
    }).fail(function() {
	". SDNoty::show("'" . SDHtml::getMsgError() . "Server Error'", '"error"') ."
	console.log('server error');
    });
    return false;
});

");?>