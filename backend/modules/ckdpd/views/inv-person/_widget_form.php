<?php 
use yii\helpers\Html;
use yii\helpers\Url;

$genid = \common\lib\codeerror\helpers\GenMillisecTime::getMillisecTime();
$indexArry = array_keys($dataSubForm);

$valF = isset($indexArry[0])?$indexArry[0]:0;
?>
<div id="<?=$genid?>" class="well" style="padding: 15px;">
    <div class="row">
	<div class="col-md-2 "><label>Form</label></div>
	<div class="col-md-2 sdbox-col"><label>Label</label></div>
	<div class="col-md-2 sdbox-col"><label>Date Field</label></div>
	<div class="col-md-1 sdbox-col"><label>Width</label></div>
	<div class="col-md-1 sdbox-col"><label>PDF Width</label></div>
	<div class="col-md-2 sdbox-col"><label>Visible Condition</label></div>
	<div class="col-md-1 sdbox-col"><label>Show in PDF?</label></div>
    </div>
    
    <div class="row form-fields" style="margin-bottom: 15px;">
	<div class="col-md-2 "><?=  yii\helpers\Html::dropDownList('forms[form][]', null, $dataSubForm, ['class'=>'form-control form-input'])?></div>
	<div class="col-md-2 sdbox-col"><input type="text" class="form-control label-input" name="forms[label][]" value="<?= isset($dataSubForm[$indexArry[0]])?$dataSubForm[$indexArry[0]]:''?>"></div>
	<div class="col-md-2 sdbox-col"><?= yii\helpers\Html::dropDownList('forms[date][]', null, [], ['class'=>'form-control date-input'])?></div>
	<div class="col-md-1 sdbox-col"><input type="number" class="form-control width-input" name="forms[width][]" value="110"></div>
	<div class="col-md-1 sdbox-col"><input type="number" class="form-control rwidth-input" name="forms[rwidth][]" value="14"></div>
	<div class="col-md-2 sdbox-col"><?= yii\helpers\Html::dropDownList('forms[visible][]', $visible_value, ['แสดงผล', 'แสดงผลตามเงื่อนไข'], ['class'=>'form-control visible-input'])?></div>
	<div class="col-md-1 sdbox-col"><?= yii\helpers\Html::dropDownList('forms[report][]', null, ['Y'=>'Yes', 'N'=>'No'], ['class'=>'form-control report-input'])?></div>
	<div class="col-md-1 sdbox-col"><button type="button" class="forms-items-del btn btn-danger"><i class="glyphicon glyphicon-remove"></i></button></div>
	<?=  Html::hiddenInput('forms[field_detail][]', $fd[$key_form], ['class'=>'fd-input']);?>
	<?=  Html::hiddenInput('forms[ezf_name][]', $en[$key_form], ['class'=>'en-input']);?>
	<?=  Html::hiddenInput('forms[ezf_table][]', $et[$key_form], ['class'=>'et-input']);?>
	<?=  Html::hiddenInput('forms[comp_id_target][]', $ct[$key_form], ['class'=>'ct-input']);?>
	<?=  Html::hiddenInput('forms[unique_record][]', $ur[$key_form], ['class'=>'ur-input']);?>
    </div>

    <div class="condition-view" style="padding: 15px; border-top: 1px solid #ddd;">
	<div class="row">
	    <div class="col-md-3 sdbox-col"><label>Form</label></div>
	    <div class="col-md-3 sdbox-col"><label>Field</label></div>
	    <div class="col-md-2 sdbox-col"><label>Value</label></div>
	    <div class="col-md-2 sdbox-col"><label>Condition</label></div>
	</div>
	
	<div class="forms-condition-items">
	    
	</div>

	<div class="row" style="margin-bottom: 15px;">
	    <div class="col-md-3 sdbox-col"><input type="text" class="form-control cform-input" disabled="disabled" name="forms[result][condition][form][]" value="Form"></div>
	    <div class="col-md-3 sdbox-col"><input type="text" class="form-control cfield-input" disabled="disabled" name="forms[result][condition][field][]" value="Field"></div>
	    <div class="col-md-2 sdbox-col"><input type="text" class="form-control cvalue-input" disabled="disabled" name="forms[result][condition][value][]" value="Value"></div>
	    <div class="col-md-2 sdbox-col"><input type="text" class="form-control ccond-input" disabled="disabled" name="forms[result][condition][cond][]" value="Condition"></div>
	    <div class="col-md-2 sdbox-col"><button type="button" data-url="<?=  Url::to(['/inv/inv-person/get-widget', 'view'=>'_widget_form_condition'])?>" class="forms-condition-add btn btn-success"><i class="glyphicon glyphicon-plus"></i></button></div>
	</div>
    </div>
    
    <div class="row" style="padding: 15px 0; border-top: 1px solid #ddd;">
	<div class="col-md-3 "><label>Show Items</label></div>
	<div class="col-md-3 "><label></label></div>
    </div>

    <div class="row form-fields" style="margin-bottom: 15px;">
	<div class="col-md-3 "><?= Html::dropDownList('forms[show][]', null, ['default'=>'แสดงผลแบบมาตรฐาน [#Save Draft/#Submited]+','detail'=>'แสดงผลตามตัวแปรลักษณะหลักของฟอร์ม','condition'=>'แสดงผลแบบเงื่อนไข','custom'=>'แสดงผลแบบกำหนดเอง'], ['class'=>'form-control show-input', 'data-id'=>$valF])?></div>
	<div class="col-md-3 sdbox-col result-box "><?= Html::dropDownList('forms[result][]', null, [], ['class'=>'form-control result-input '])?></div>
	<div class="col-md-9 varshow"></div>
    </div>
    
    <div class="echo-view" style="padding: 15px; border-top: 1px solid #ddd;">
    </div>
</div>    
<?php  


$this->registerJs("

getFieldResult('$valF');
    
function getFieldResult(valField){
    if(valField === undefined || valField === null){
    
    } else {
	$.ajax({
	    method: 'POST',
	    url: '".Url::to(['/inv/inv-person/getfields'])."',
	    data: {id:valField},
	    dataType: 'HTML',
	    success: function(result, textStatus) {
		$('#$genid .date-input').html(result);
		$('#$genid .result-input').html(result);    
	    }
	});
    }
}
$('#$genid .visible-input').trigger('change');
$('#$genid .form-input').trigger('change');
$('#$genid .show-input').trigger('change');
");?>