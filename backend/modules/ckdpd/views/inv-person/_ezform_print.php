<?php

use Yii;
use yii\helpers\Html;
use kartik\select2\Select2;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;
use appxq\sdii\helpers\SDNoty;
use appxq\sdii\helpers\SDHtml;
use common\lib\sdii\widgets\SDModalForm;
/**
 * _ezform_popup file UTF-8
 * @author SDII <iencoded@gmail.com>
 * @copyright Copyright &copy; 2015 AppXQ
 * @license http://www.appxq.com/license/
 * @version 1.0.0 Date: 17 พ.ย. 2559 12:32:13
 * @link http://www.appxq.com/
 */
$this->registerJsFile('/jqueryloading/jloading.js',['depends' => [\yii\web\JqueryAsset::className()]]);
$formmian = '';
?>

<div id="main-fields-form">

<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title" id="itemModalLabel"><?=$modelform->ezf_name?></h4>
</div>
    <div class="modal-body" >
    
	<div id="target-box" >
            <?php if($comp_target!='skip'):?>
            
	    <div class="form-group">
	   
	    <?php
            $formmian = $ezf_id != $ezform_comp['ezf_id']?'js_target_decode = $(this).val();js_id = $(this).val();menu_main();ezform_mian(js_dataid, js_id);':'js_target_decode = $(this).val();menu_target();ezform_target($(this).val());';

	    //appxq\sdii\utils\VarDumper::dump($dataComponent['sqlSearch']);
	    $concatLabel = \backend\modules\inv\classes\InvFunc::setFieldSearch($ezform_comp);
	    //$target = '1478357328006962000';
	    $initValueText = NULL;
	    $initdata = NULL;
	    //ezf_table_comp
	    if (isset($target) && $target != '') {
                //appxq\sdii\utils\VarDumper::dump($dataComponent,true);
                $target_decode = base64_decode($target);
                if(!$dataComponent[0]['text']){
                    
                    $sqlSearch = str_replace('$q', '', $dataComponent['sqlSearch']);
                    $sql =  $sqlSearch. " AND id='$target_decode'";
                    
                    try {
                        $initdata = Yii::$app->db->createCommand($sql)->queryOne();
                        if ($initdata) {
                            $initValueText = $initdata['text'];
                        } else {
                            $initValueText = base64_decode($target);
                        }
                    } catch (\yii\db\Exception $e) {
                        $initValueText = base64_decode($target);
                    }
                }else {
                    $initValueText = $dataComponent[0]['text'];
                }
                
            } 
	   if(isset($target)){
               
           } else {
               echo '<label>เลือกเป้าหมาย</label>';
               echo Select2::widget([
                        'name' => 'select2Target',
                        'id' => 'select2Target',
                        'value'=> $initValueText,
                        'size' => Select2::MEDIUM,
                        'language' => 'th',
                        'options' => ['disabled'=>isset($target),'placeholder' => 'สามารถค้นได้จากรายการดังนี้ '.$concatLabel['concat_label'], 'multiple' => FALSE],
                        'pluginOptions' => [
                            'allowClear' => false,
                            'minimumInputLength' => 0,
                            'ajax' => [
                                'url' => Url::to(['/inv/inv-person/lookup-target']),
                                'type' => 'POST',
                                //'delay' => 500,
                                'dataType' => 'json',
                                'data' => new JsExpression("function(params) { $(this).text(''); return {q:params.term, table:'" . $dataComponent['ezf_table_comp'] . "', search:'" . base64_encode($dataComponent['sqlSearch']) . "', ezf_id : '".$ezf_id."', ezf_comp_id : '".$ezform_comp['comp_id']."'}; }"),
                                //'results' => new JsExpression('function(data) { return {results:data}; }'),
                            ],
                        ],
                    ]);
           }
	    
	    ?>
	  </div>
	    
            
	</div>
	<?php
	     $active_target = (isset($target) && $ezf_id == $ezform_comp['ezf_id'])?'btn-success':'btn-default';
	     $active_main = (isset($target) && $ezf_id != $ezform_comp['ezf_id'])?'btn-success':'btn-default';
	     ?>
        
        <ul id="menu-box" class="nav nav-tabs" style=" <?=$active_target == 'btn-default' && $active_main == 'btn-default'?'display: none;':''?>">
            <li role="presentation" class="<?= $active_target=='btn-success'?'active':'' ?>"><a id="btn-menu-target" data-toggle="tab" style="cursor: pointer; ">ฟอร์มเป้าหมาย (<?=$ezform_comp['comp_name']?>)</a></li>
            <?php if($ezf_id != $ezform_comp['ezf_id']){?>
            <li role="presentation" class="<?= $active_main=='btn-success'?'active':'' ?>"><a id="btn-menu-main" data-toggle="tab" style="cursor: pointer; ">ฟอร์มบันทึก (<?=$modelform->ezf_name?>)</a></li>
            <?php } ?>
        </ul>
	
	<div id="ezform-target-box"></div>
	
	<div id="ezform-box">
	    <?php
	    if(isset($target) && $ezf_id != $ezform_comp['ezf_id']){
		?>
		<div class="panel ">
		    <div class="panel-heading clearfix">
                        <h3 class="panel-title" >
                            <?=\yii\bootstrap\Html::a('<span class="fa fa-picture-o fa-2x"></span>', ['/managedata/managedata/ezform-pdf', 'ezf_id' => $ezf_id, 'dataid'=>$dataid], [
                        'class'=>'pull-right',
                        'target'=>'_blank',
                                'style'=>'margin-left: 10px;',
                    ]);?>
                            <?='<a   class="pull-right" href="/inputdata/printform?dataid='.$dataid.'&amp;id='.$ezf_id.'&amp;print=1&pages=2" target="_blank"><span class="fa fa-print fa-2x"></span></a>';?>
                            <?='<a style="color:gray;margin-right:5px;" class="pull-right" href="/inputdata/printform?dataid='.$dataid.'&amp;id='.$ezf_id.'&amp;print=1&pages=1" target="_blank"><span class="fa fa-print fa-2x"></span></a>';?>
                        </h3>
		</div>
		    <div class="panel-body" id="panel-target-body" >
			
	    <?php echo $this->render('_ezform_widget', [
		    'modelform'=>$modelform,
		    'modelfield'=>$modelfield,
		    'model_gen'=>$model_gen,
		    'ezf_id'=>$ezf_id,
		    'dataid'=>$dataid,
		    'target'=>$target,
		    'comp_id_target'=>$comp_id_target,
		    'formname'=>'ezform-main',
		    'end'=>1,
		    'readonly'=>$readonly,
                    'comp_target'=>$comp_target,
                    'dataset'=>$dataset,
                    'sql_announce'=>$sql_announce,
                    'error'=>$error,
		]);
	    ?>
			</div>
	    </div>
			<?php
	    } elseif(isset($target) && $ezf_id == $ezform_comp['ezf_id']) {
	    ?>
	    <?php
//	    $comp_id;
//		
//		$modelform_target = \backend\modules\ezforms\models\Ezform::find()->where('ezf_id = :ezf_id', [':ezf_id' => $ezform_comp['ezf_id']])->one();
//		
//		$modelfield_target = \backend\modules\ezforms\models\EzformFields::find()
//			->where('ezf_id = :ezf_id', [':ezf_id' => $ezform_comp['ezf_id']])
//			->orderBy(['ezf_field_order' => SORT_ASC])
//			->all();
//		
//		$table_target = $modelform_target->ezf_table;
//		$get_target = Yii::$app->db->createCommand("select target from $table_target where id = '$target_decode'")->queryScalar();
//		$model_gen_target = \backend\modules\ezforms\components\EzformFunc::setDynamicModelLimit($modelfield_target);
//		
//		$comp_id = $modelform_target['comp_id_target'];
//		
//		$model_target = new \backend\models\Tbdata();
//		$model_target->setTableName($table_target);
//
//		$query_target = $model_target->find()->where('id=:id', [':id'=>$target_decode]);
//
//		$data_target = $query_target->one();
//
//		$model_gen_target->attributes = $data_target->attributes;
//		
		?>
		<div class="panel">
		    <div class="panel-heading clearfix" >
			<h3 class="panel-title"> <?='<a class="pull-right" href="/inputdata/printform?dataid='.$dataid.'&amp;id='.$ezform_comp['ezf_id'].'&amp;print=1&pages=2" target="_blank"><span class="fa fa-print fa-2x"></span></a>';?></h3>
                        <h3 class="panel-title"> <?='<a style="color:gray;margin-right:5px;" class="pull-right" href="/inputdata/printform?dataid='.$dataid.'&amp;id='.$ezform_comp['ezf_id'].'&amp;print=1&pages=1" target="_blank"><span class="fa fa-print fa-2x"></span></a>';?></h3>
		</div>
		    <div class="panel-body" id="panel-target-body" >
	    <?php
		
	    echo $this->render('_ezform_widget', [
//		    'modelform'=>$modelform_target,
//		    'modelfield'=>$modelfield_target,
//		    'model_gen'=>$model_gen_target,
//		    'ezf_id'=>$ezform_comp['ezf_id'],
//		    'dataid'=>$target_decode,
//		    'target'=>  base64_encode($get_target),
                    'modelform'=>$modelform,
		    'modelfield'=>$modelfield,
		    'model_gen'=>$model_gen,
		    'ezf_id'=>$ezf_id,
		    'dataid'=>$dataid,
		    'target'=>$target,
		    'comp_id_target'=>$comp_id_target,
		    'formname'=>'ezform-target',
		    'end'=>1,
		    'readonly'=>$readonly,
                    'comp_target'=>$comp_target,
                    'dataset'=>$dataset,
                    'sql_announce'=>$sql_announce,
                    'error'=>$error,
		]);
	    ?>
		    
		</div>
	    </div>
	     <?php } ?>
	 </div>
        <?php else:?>
        <?php
        if($addperson==true){
        ?>
        <ul id="menu-box" class="nav nav-tabs">
                
                <li role="presentation" class="active"><a id="btn-menu-skip" data-toggle="tab" style="cursor: pointer; ">ฟอร์มบันทึก (<?=$modelform->ezf_name?>)</a></li>
                <li role="presentation" ><a id="btn-menu-person" data-toggle="tab" style="cursor: pointer; ">เพิ่มรายชื่อผู้ป่วย</a></li>
                
            </ul>
        <?php
        }
        ?>
        <div id="ezform-box">
            
            
            <div class="panel ">
		    <div class="panel-heading clearfix">
			<h3 class="panel-title"><?='<a class="pull-right" href="/inputdata/printform?dataid='.$dataid.'&amp;id='.$ezf_id.'&amp;print=1&pages=2" target="_blank"><span class="fa fa-print fa-2x"></span></a>';?></h3>
                        <h3 class="panel-title"> <?='<a style="color:gray;margin-right:5px;" class="pull-right" href="/inputdata/printform?dataid='.$dataid.'&amp;id='.$ezf_id.'&amp;print=1&pages=1" target="_blank"><span class="fa fa-print fa-2x"></span></a>';?></h3>
		</div>
		    <div class="panel-body" id="panel-target-body" >
			
	    <?php echo $this->render('_ezform_widget', [
		    'modelform'=>$modelform,
		    'modelfield'=>$modelfield,
		    'model_gen'=>$model_gen,
		    'ezf_id'=>$ezf_id,
		    'dataid'=>$dataid,
		    'target'=>$target,
		    'comp_id_target'=>$comp_id_target,
		    'formname'=>'ezform-main',
		    'end'=>1,
		    'readonly'=>$readonly,
                    'comp_target'=>$comp_target,
                    'dataset'=>$dataset,
                    'sql_announce'=>$sql_announce,
                    'error'=>$error,
		]);
	    ?>
			</div>
	    </div>
        </div>
        <?php endif;?>
        
</div>
<?php 
backend\assets\EzfGenAsset::register($this);
 ?>

</div>

<?php  //window.print();


    $this->registerJS("
        function modalQueryRequest(url) {
            $('#modal-query-request .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
            $('#modal-query-request').modal('show')
            .find('.modal-content')
            .load(url);
        }
        
        $('#select2Target').on('change',function(){
                                    var target = $('#select2Target option:selected').val();
                                    var target_text = $('#select2-select2Target-results').on('.select2-results__option--highlighted').text();
                                    //เลือกแบบ special
                                    //เพิ่มกลุ่มเสี่ยงใหม่ใน site ของท่าน (หา site ไหนก็ไม่เจอ)
                                    if(target == '-990'){
                                        
					//console.log(target_text);
                                        var url = '" . Url::to(['/inv/inv-person/step2-confirm', 'task' => 'add-new', 'comp_id' => $ezform_comp['comp_id']]) . "&target='+encodeURIComponent($.trim(target_text));
					
                                        modalQueryRequest(url);
                                        $('#select2Target option:selected').prop(\"selected\", false);
                                        $('#select2-select2Target-container').text($('#select2Target option:selected').text());
                                    //เพิ่มกลุ่มเสี่ยงใหม่ใน site ของท่าน (copy จาก site อื่น)
                                    }else if(target == '-991'){
                                        //runBlockUI('รอสักครู่กำลังเพิ่มข้อมูลใหม่ ...');
                                        var url = '" . Url::to(['/inv/inv-person/step2-confirm', 'task' => 'add-from-site', 'comp_id' => $ezform_comp['comp_id']]) . "&target='+encodeURIComponent($.trim(target_text));
                                        modalQueryRequest(url);
                                        $('#select2Target option:selected').prop(\"selected\", false);
                                        $('#select2-select2Target-container').text($('#select2Target option:selected').text());
                                    //เป้าหมายปกติ หาไม่เจอเพิ่มข้อมูลใหม่
                                    }else if(target == '-992'){
                                        //runBlockUI('รอสักครู่กำลังเพิ่มข้อมูลใหม่ ...');
                                        var url = '" . Url::to(['/inv/inv-person/redirect-page', 'ezf_id' => $ezform_comp['ezf_id'], 'comp_id_target' => $comp_id_target ]) . "';
                                         
					 $.ajax({
					    method: 'POST',
					    url: url,
					    dataType: 'JSON',
					    success: function(result, textStatus) {
						
						$('#menu-box').show();
						menu_target();

						js_target_decode = 0;
						js_dataid = 0;
						js_id = 0;
						js_target = '';
						
						ezform_target(js_target_decode);
					    }
					});
                                    //กรณีเลขบัตรไม่ถูกต้อง
                                    }else if(target == '-999'){
                                        alert('ไม่พบข้อมูล! ลองค้นหาจากคำอื่นๆ');
                                    } else{
                                    //เลือกข้อมูลแบบปกติ
                                    //runBlockUI('รอสักครู่กำลังเปิดแฟ้มข้อมูล ...');
                                    /*
                                        $.post('" . Url::to(['/inv/inv-person/gen-target']) . "' , { target: target, ezf_id : '".$input_ezf_id."' })
                                         .done(function( data ) {
                                           var chk = ".(Yii::$app->request->get('addnext') ? 'true' : 'null').";
                                           if(chk){
                                                var ezf_id = '".$input_ezf_id."';
                                                var comp_id_target = '".$input_comp_target."';
                                                var addnext = '".Yii::$app->request->get('addnext')."';
                                                var rurl = '".Yii::$app->request->get('rurl')."';
                                                $('#addNewRecord').prop(\"disabled\", true);
                                                $('#addNextRecord').prop(\"disabled\", true);

                                                //runBlockUI('รอสักครู่กำลังเปิดฟอร์มใหม่ ...');

                                                $.post(\"".Url::to(['/inv/inv-person/insert-record'])."\", {ezf_id: ezf_id, target: $.trim(data), comp_id_target: comp_id_target, addnext : addnext, rurl : rurl}, function(result){
                                                    $(location).attr('href',result);
                                                });
                                           }else{
                                                //var url = '" . Url::to(['/inv/inv-person/step4', 'comp_id_target' => $input_comp_target, 'ezf_id' => $input_ezf_id, 'dataset'=>$_GET['dataset']]) . "&target='+$.trim(data);
                                                //$(location).attr('href',url);
                                           }
                                         });
                                    */
                                     }
                                 });");
$this->registerJs("
                                        function runBlockUI(text){
                                       $.blockUI({
                                                    message : '<span style=\"font-size : 25px; font-color:#ffffff;\">'+text+'</span>',
                                                    css: {
                                                        border: 'none',
                                                        padding: '15px',
                                                        backgroundColor: '#000',
                                                        '-webkit-border-radius': '10px',
                                                        '-moz-border-radius': '10px',
                                                        opacity: 1,
                                                        color: '#fff'
                                                    }});
                                       }
                                         function step3ChangeForm(val){
                                            //runBlockUI('รอสักครู่กำลังเปิดฟอร์มใหม่ ...');
                                            var url = \"" .Yii::$app->request->url . "&ezf_id=\"+$.trim(val); $(location).attr(\"href\",url);
                                       }
                                      
                                        ", \yii\web\View::POS_HEAD);    
$this->registerJs("
    var js_end = $end;
    var js_readonly = $readonly;
    var js_dataid = '$dataid';
    var js_id;	
    var js_target_decode = '$target_decode';
    var js_comp_id_target = '$comp_id_target';
    var js_comp_target = '$comp_target';
    var js_target = '$target';
    var js_ezf_id = '$ezf_id';
    var js_ezf_id_main = '$ezf_id';
    var js_dataset = '$dataset';
	
$('a[data-toggle=\"tab\"]').on('shown.bs.tab', function (e) {
  
  if(e.target.id=='btn-menu-main'){
        if(js_id==0){
            alert('คุณต้องบันทึกฟอร์มเป้าหมายก่อน');
            menu_target();
            return false;
       }
	js_end=1;
	ezform_mian(js_dataid, js_id);
    } else if(e.target.id=='btn-menu-target'){
        js_end=$end;
	ezform_target(js_target_decode);
    } else if(e.target.id=='btn-menu-skip'){
        js_end=1;
	ezform_skip(js_dataid);
    } else if(e.target.id=='btn-menu-person'){
        js_end=0;
	ezform_person(js_dataid);
    }
    
});


$('#target-box').on('change', '#select2Target', function() {
    if($(this).val()>0){
	$('#menu-box').show();
	$formmian 
    } else {
	$('#menu-box').hide();
	$('#ezform-box').html('');
    }
});

function menu_target() {
    $('#menu-box #btn-menu-target').tab('show');
    
    //$('#btn-menu-target').removeClass('btn-default');
    //$('#btn-menu-main').removeClass('btn-success');

    //$('#btn-menu-target').addClass('btn-success');
    //$('#btn-menu-main').addClass('btn-default');

}

function menu_main() {
    $('#menu-box #btn-menu-main').tab('show');
    
    //$('#btn-menu-main').removeClass('btn-default');
    //$('#btn-menu-target').removeClass('btn-success');

    //$('#btn-menu-main').addClass('btn-success');
    //$('#btn-menu-target').addClass('btn-default');
    
}

function menu_skip() {
    $('#menu-box #btn-menu-skip').tab('show');
    
}

function ezform_target(dataid) {
    $('#ezform-box').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
    $.ajax({
	    method: 'POST',
	    url: '".  \yii\helpers\Url::to(['/inv/inv-person/ezform-target','dataid'=>''])."'+dataid+'&ezf_id='+js_ezf_id+'&target='+js_target+'&comp_id_target='+js_comp_id_target+'&end='+js_end+'&readonly='+js_readonly+'&comp_target='+js_comp_target+'&dataset='+js_dataset,
	    dataType: 'HTML',
	    success: function(result, textStatus) {
		$('#ezform-box').html(result);
	    }
	});
}

function ezform_mian(dataid, id) {
    $('#ezform-box').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
    $.ajax({
	    method: 'POST',
	    url: '".  \yii\helpers\Url::to(['/inv/inv-person/ezform-mian','id'=>''])."'+id+'&dataid='+dataid+'&ezf_id='+js_ezf_id_main+'&target='+js_target+'&comp_id_target='+js_comp_id_target+'&readonly='+js_readonly+'&comp_target='+js_comp_target+'&dataset='+js_dataset,
	    dataType: 'HTML',
	    success: function(result, textStatus) {
		$('#ezform-box').html(result);
	    }
	});
}

function ezform_skip(dataid) {
    $('#ezform-box').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
    $.ajax({
	    method: 'POST',
	    url: '".  \yii\helpers\Url::to(['/inv/inv-person/ezform-skip','dataid'=>''])."'+dataid+'&ezf_id='+js_ezf_id_main+'&target='+js_target+'&comp_id_target='+js_comp_id_target+'&readonly='+js_readonly+'&comp_target='+js_comp_target+'&dataset='+js_dataset,
	    dataType: 'HTML',
	    success: function(result, textStatus) {
		$('#ezform-box').html(result);
	    }
	});
}

function ezform_person(dataid) {
    $('#ezform-box').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
    $.ajax({
	    method: 'POST',
	    url: '".  \yii\helpers\Url::to(['/inv/inv-person/ezform-person','dataid'=>''])."'+dataid+'&ezf_id='+js_ezf_id_main+'&target='+js_target+'&comp_id_target='+js_comp_id_target+'&readonly='+js_readonly+'&comp_target='+js_comp_target+'&dataset='+js_dataset,
	    dataType: 'HTML',
	    success: function(result, textStatus) {
		$('#ezform-box').html(result);
	    }
	});
}

");?>
    
