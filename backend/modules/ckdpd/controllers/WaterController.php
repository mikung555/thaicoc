<?php

namespace backend\modules\ckdpd\controllers;

use yii\web\Controller;
use Yii;
 
class WaterController extends Controller
{


    public function actionIndex() {
        $data = Yii::$app->request->get("data","");
        $count = 0;
        $query = Yii::$app->db->createCommand("SELECT count(*) as count FROM pddata")->queryOne();
        if(!empty($query)){
            $count = $query["count"];
        } 
        if (!empty($data)) {
            $sql="select *,max(updatetime) as maxdate from pddata where cid <> ''  GROUP BY cid order by maxdate desc ";
            $data = Yii::$app->db->createCommand($sql)->queryAll();
            $datagrid = new \yii\data\ArrayDataProvider([
                'allModels' => $data
            ]);

            return $this->renderAjax('water', ['datagrid' => $datagrid]);
        } else {
            return $this->render('index',[
                'count'=>$count
            ]);
        }
    }
    public function actionGetCount() {
        $count = 0;
        $query = Yii::$app->db->createCommand("SELECT count(*) as count FROM pddata")->queryOne();
        if(!empty($query)){
            $count = $query["count"];
        }
        return $count;
    }
    public function actionGetWater() {
        $cid = Yii::$app->request->get('cid');

        $sql = "select * from pddata where cid=:cid  order by updatetime DESC  limit 1";
        $data = Yii::$app->db->createCommand($sql, [':cid' => $cid])->queryAll();


        $datagrid = new \yii\data\ArrayDataProvider([
            'allModels' => $data
        ]);

        return $this->renderAjax('waterdata', ['datagrid' => $datagrid, 'cid' => $cid]);
    }

    public function actionGetNum() {
        $cid = Yii::$app->request->get('cid');

        $sql = "select volumeout as num,pdmode from pddata where cid=:cid  order by updatetime DESC  limit 1";
        $data = Yii::$app->db->createCommand($sql, [':cid' => $cid])->queryOne();
        if ($data['pdmode'] == '4') {

            return "สิ้นสุดการนํ้ายาออก :  " . $data['num'];
        }
        return $data['pdmode'] . "" . $data['num'];
    }

}
