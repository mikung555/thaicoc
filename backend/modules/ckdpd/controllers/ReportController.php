<?php

namespace backend\modules\ckdpd\controllers;

use yii\web\Controller;
use Yii;
use backend\modules\ckdpd\classes\LoginForm;
use backend\modules\ckdpd\classes\PersonInfo;
use frontend\modules\api\v1\classes\LogStash;
use yii\data\SqlDataProvider;
use yii\web\Response;

class ReportController extends Controller {

    public function actionIndex() {

        $request = Yii::$app->request;
        $model = new LoginForm();
        $sql = "select * from inv_user where sitecode=:sitecode";
        $sitecode = Yii::$app->user->identity->userProfile->sitecode;

        $sql = "select tb_data_coc.ptid , concat(tb_data_coc.name,' ',tb_data_coc.surname) as name from inv_user  inner join tb_data_coc on inv_user.id  = tb_data_coc.cid 
        where inv_user.sitecode =:sitecode group by inv_user.id";

        $personList = Yii::$app->db->createCommand($sql, [':sitecode' => $sitecode])->queryAll();
        // \appxq\sdii\utils\VarDumper::dump($personList2);
        $test = $request->post('test1', 'no data');
        return $this->render('index2', ['test' => $test, 'model' => $model, 'personList' => $personList, 'canserach' => '1']);
    }

    public function actionIndexPartial() {

        $request = Yii::$app->request;
        $model = new LoginForm();
        $sql = "select * from inv_user where sitecode=:sitecode";
        $sitecode = Yii::$app->user->identity->userProfile->sitecode;
        $sql = "select tb_data_coc.ptid , concat(tb_data_coc.name,' ',tb_data_coc.surname) as name from inv_user  inner join tb_data_coc on inv_user.id  = tb_data_coc.cid 
        where inv_user.sitecode =:sitecode group by inv_user.id";

        $personList = Yii::$app->db->createCommand($sql, [':sitecode' => $sitecode])->queryAll();
        // \appxq\sdii\utils\VarDumper::dump($personList2);
        $test = $request->post('test1', 'no data');
        return $this->renderPartial('index2', ['test' => $test, 'model' => $model, 'personList' => $personList, 'canserach' => '1']);
    }

    public function actionAjaxReport() {

        $request = Yii::$app->request;
        $model = new LoginForm();
        $sql = "select * from inv_user where sitecode=:sitecode";
        $sitecode = Yii::$app->user->identity->userProfile->sitecode;

        $sql = "select tb_data_coc.ptid , concat(tb_data_coc.name,' ',tb_data_coc.surname) as name from inv_user  inner join tb_data_coc on inv_user.id  = tb_data_coc.cid 
where inv_user.sitecode =:sitecode group by inv_user.id";

        $personList = Yii::$app->db->createCommand($sql, [':sitecode' => $sitecode])->queryAll();
        // \appxq\sdii\utils\VarDumper::dump($personList2);
        return $this->renderAjax('ajaxreport', ['test' => $test, 'model' => $model, 'personList' => $personList, 'canserach' => '1']);
    }

    public function actionHome() {
        $request = Yii::$app->request;

        $model = new LoginForm();
        $sql = "select * from inv_user where sitecode=:sitecode";
        $sitecode = Yii::$app->user->identity->userProfile->sitecode;

        $sql = "select tb_data_coc.ptid , concat(tb_data_coc.name,' ',tb_data_coc.surname) as name from inv_user  inner join tb_data_coc on inv_user.id  = tb_data_coc.cid 
        where inv_user.sitecode =:sitecode group by inv_user.id";

        $personList = Yii::$app->db->createCommand($sql, [':sitecode' => $sitecode])->queryAll();
        // \appxq\sdii\utils\VarDumper::dump($personList2);
        $test = $request->post('test1', 'no data');
        return $this->render('index', ['test' => $test, 'model' => $model, 'personList' => $personList]);
    }

    public function actionHomeAjax() {
        $request = Yii::$app->request;

        $model = new LoginForm();
        $sql = "select * from inv_user where sitecode=:sitecode";
        $sitecode = Yii::$app->user->identity->userProfile->sitecode;

        $sql = "select tb_data_coc.ptid , concat(tb_data_coc.name,' ',tb_data_coc.surname) as name from inv_user  inner join tb_data_coc on inv_user.id  = tb_data_coc.cid 
        where inv_user.sitecode =:sitecode group by inv_user.id";

        return $this->renderAjax('index');
    }

    public function actionPressure() {
        $ptlink = Yii::$app->request->get('ptlink');
        $cid = Yii::$app->request->get('cid');
        $ptid = Yii::$app->request->get('ptid');
        $startDate = Yii::$app->request->get('startdate');
        $endEate = Yii::$app->request->get('enddate');
        $sitecode = Yii::$app->user->identity->userProfile->sitecode;
        $dayNum = Yii::$app->request->get('daynum');
        //get ptid
        if ($ptid == '')
            $ptid = \backend\modules\ckdpd\classes\PersonInfo::getPtid($cid, $ptlink);
        if ($ptlink == '1309da7fd7587c167e47054bad50eb01') {
            $ptid = '1497945602005167900';
        }
        if ($dayNum > 0) {
            $datelistSql = "select ckdpd04_date from tbdata_1484483043088715200 "
                    . "where 1 "
                    . " and ptid=:ptid"
                    . " and ckdpd04_date <> '' " . "  and systolic <> '' and diastolic <> '' "
                    . " group by ckdpd04_date order by ckdpd04_date desc limit :dayNum  ";
            $dataDatelist = Yii::$app->db->createCommand($datelistSql, [':sitecode' => $sitecode, ':ptid' => $ptid, ':dayNum' => intval($dayNum)])->queryAll();
            $startDate = $dataDatelist[sizeof($dataDatelist) - 1]["ckdpd04_date"];
            $sql = "select * from tbdata_1484483043088715200 where  ptid=:ptid"
                    . " and ckdpd04_date <> '' " . "  and systolic <> '' and diastolic <> '' "
                    . " and ckdpd04_date >=  '$startDate ' "
                    . " group by ckdpd04_date order by ckdpd04_date asc";
        } else {
            if ($startDate == '' || $endEate == '') {
                $sql = "select * from tbdata_1484483043088715200 where  ptid=:ptid"
                        . "  and systolic <> '' and diastolic <> '' and ckdpd04_date <> '' "
                        . " group by ckdpd04_date order by ckdpd04_date asc";
            } else {
                $sql = "select * from tbdata_1484483043088715200 where  ptid=:ptid"
                        . " and ckdpd04_date <> '' " . "  and systolic <> '' and diastolic <> '' "
                        . " and ckdpd04_date >=  '$startDate' "
                        . " and ckdpd04_date <=  '$endEate' "
                        . " group by ckdpd04_date order by ckdpd04_date asc";
            }
        }
        //   \appxq\sdii\utils\VarDumper::dump($sql);
        $data = Yii::$app->db->createCommand($sql, [':ptid' => $ptid])->queryAll();
        $gdata = [];
        $datedata = [];
        for ($i = 0; $i < sizeof($data); $i++) {
            $tmparray = [intval($data[$i]["systolic"]), intval($data[$i]["diastolic"])];
            array_push($datedata, $data[$i]["ckdpd04_date"]);
            array_push($gdata, $tmparray);
        }

        $datagrid = new \yii\data\ArrayDataProvider([
            'allModels' => $data
        ]);

        //  if(sizeof($data)==0)return $this->renderAjax('nodata');

        return $this->renderAjax('pressure', ['data' => $data, 'gdata' => $gdata, 'datedata' => $datedata, 'datagrid' => $datagrid]);
    }

    public function actionSugar() {
        $ptlink = Yii::$app->request->get('ptlink');
        $cid = Yii::$app->request->get('cid');
        $ptid = Yii::$app->request->get('ptid');
        $startDate = Yii::$app->request->get('startdate');
        $endEate = Yii::$app->request->get('enddate');
        $sitecode = Yii::$app->user->identity->userProfile->sitecode;
        $dayNum = Yii::$app->request->get('daynum');
        if ($ptid == '')
            $ptid = \backend\modules\ckdpd\classes\PersonInfo::getPtid($cid, $ptlink);
        if ($ptlink == '1309da7fd7587c167e47054bad50eb01') {
            $ptid = '1497945602005167900';
        }

        if ($dayNum > 0) {
            $datelistSql = "select ckdpd04_date from tbdata_1484483043088715200 "
                    . "where 1 "
                    . "and ptid=:ptid"
                    . " and ckdpd04_date <> '' " . " and bloodsugar_lv_period <> ''"
                    . " group by ckdpd04_date order by ckdpd04_date desc limit :dayNum  ";
            $dataDatelist = Yii::$app->db->createCommand($datelistSql, [':sitecode' => $sitecode, ':ptid' => $ptid, ':dayNum' => intval($dayNum)])->queryAll();
            $startDate = $dataDatelist[sizeof($dataDatelist) - 1]["ckdpd04_date"];
            $sql = "select * from tbdata_1484483043088715200 where  ptid=:ptid"
                    . " and ckdpd04_date <> '' "
                    . " and ckdpd04_date >=  '$startDate' "
                    . " group by ckdpd04_date order by ckdpd04_date asc";
        } else {
            if ($startDate == '' || $endEate == '') {
                $sql = "select * from tbdata_1484483043088715200 where  ptid=:ptid"
                        . " and bloodsugar_lv_period <> '' and ckdpd04_date <> '' "
                        . " group by ckdpd04_date order by ckdpd04_date asc";
            } else {
                $sql = "select * from tbdata_1484483043088715200 where  ptid=:ptid"
                        . " and ckdpd04_date <> '' " . " and bloodsugar_lv_period <> ''"
                        . " and ckdpd04_date >=  '$startDate ' "
                        . " and ckdpd04_date <=  '$endEate ' "
                        . " group by ckdpd04_date order by ckdpd04_date asc";
            }
        }


        $data = Yii::$app->db->createCommand($sql, [':sitecode' => $sitecode, ':ptid' => $ptid])->queryAll();
        $gdata = [];
        $gdata2 = [];
        $datedata = [];
        for ($i = 0; $i < sizeof($data); $i++) {
            $tmparray = [intval($data[$i]["systolic"]), intval($data[$i]["diastolic"])];
            array_push($datedata, $data[$i]["ckdpd04_date"]);
            array_push($gdata, $tmparray);
            array_push($gdata2, intval($data[$i]["bloodsugar_lv_period"]));
        }
        $datagrid = new \yii\data\ArrayDataProvider([
            'allModels' => $data
        ]);

        //  if(sizeof($data)==0)return $this->renderAjax('nodata');

        return $this->renderAjax('sugar', ['data' => $data, 'gdata' => $gdata, 'datedata' => $datedata, 'gdata2' => $gdata2, 'datagrid' => $datagrid]);
    }

    public function actionPressure2() {
        $ptlink = Yii::$app->request->get('ptlink');
        $cid = Yii::$app->request->get('cid');
        $ptid = Yii::$app->request->get('ptid');
        $startDate = Yii::$app->request->get('startdate');
        $endEate = Yii::$app->request->get('enddate');
        $sitecode = Yii::$app->user->identity->userProfile->sitecode;
        $dayNum = Yii::$app->request->get('daynum');
        if ($ptid == '')
            $ptid = \backend\modules\ckdpd\classes\PersonInfo::getPtid($cid, $ptlink);
        if ($ptlink == '1309da7fd7587c167e47054bad50eb01') {
            $ptid = '1497945602005167900';
        }

        if ($dayNum > 0) {
            $datelistSql = "select ckdpd04_date from tbdata_1484483043088715200 "
                    . "where 1 "
                    . " and ptid=:ptid"
                    . " and ckdpd04_date <> '' "
                    . " group by ckdpd04_date order by ckdpd04_date desc limit :dayNum  ";
            $dataDatelist = Yii::$app->db->createCommand($datelistSql, [':sitecode' => $sitecode, ':ptid' => $ptid, ':dayNum' => intval($dayNum)])->queryAll();
            $startDate = $dataDatelist[sizeof($dataDatelist) - 1]["ckdpd04_date"];
            $sql = "select * from tbdata_1484483043088715200 where  ptid=:ptid"
                    . " and ckdpd04_date <> '' "
                    . " and ckdpd04_date >=  '$startDate ' "
                    . " group by ckdpd04_date order by ckdpd04_date asc";
        } else {
            if ($startDate == '' || $endEate == '') {

                $sql = "select * from tbdata_1484483043088715200 where  ptid=:ptid"
                        . "  and systolic <> '' and diastolic <> '' and ckdpd04_date <> '' "
                        . " group by ckdpd04_date order by ckdpd04_date asc";
            } else {

                $sql = "select * from tbdata_1484483043088715200 where  ptid=:ptid"
                        . " and ckdpd04_date <> '' "
                        . " and ckdpd04_date >=  '$startDate ' "
                        . " and ckdpd04_date <=  '$endEate ' "
                        . " group by ckdpd04_date order by ckdpd04_date asc";
            }
        }




        $data = Yii::$app->db->createCommand($sql, [':ptid' => $ptid])->queryAll();
        $gdataP1 = [];
        $gdataP2 = [];

        $gdata2 = [];
        $datedata = [];
        for ($i = 0; $i < sizeof($data); $i++) {
            $tmparray = [intval($data[$i]["systolic"]), intval($data[$i]["diastolic"])];
            array_push($datedata, $data[$i]["ckdpd04_date"]);
            array_push($gdataP1, intval($data[$i]["systolic"]));
            array_push($gdataP2, intval($data[$i]["diastolic"]));
            array_push($gdata2, intval($data[$i]["bloodsugar_lv_period"]));
        }
        $datagrid = new \yii\data\ArrayDataProvider([
            'allModels' => $data
        ]);
        // if(sizeof($data)==0)return $this->renderAjax('nodata');

        return $this->renderAjax('pressure2', ['data' => $data, 'gdataP1' => $gdataP1, 'gdataP2' => $gdataP2, 'datedata' => $datedata, 'gdata2' => $gdata2, 'datagrid' => $datagrid]);
    }

    public function actionSol() {
        $ptlink = Yii::$app->request->get('ptlink');
        $cid = Yii::$app->request->get('cid');
        $ptid = Yii::$app->request->get('ptid');
        $startDate = Yii::$app->request->get('startdate');
        $endEate = Yii::$app->request->get('enddate');
        $sitecode = Yii::$app->user->identity->userProfile->sitecode;
        $dayNum = Yii::$app->request->get('daynum');
        if ($ptlink == '1309da7fd7587c167e47054bad50eb01') {
            $ptid = '1497945602005167900';
        }

        if ($ptid == '') {
            $ptid = \backend\modules\ckdpd\classes\PersonInfo::getPtid($cid, $ptlink);
            //\appxq\sdii\utils\VarDumper::dump($ptid);
        }


        if ($dayNum > 0) {
            $datelistSql = "select ckdpd02date from tbdata_1484716303035847800 "
                    . "where 1 "
                    . " and ptid=:ptid"
                    . " and ckdpd02date <> '' "
                    . " group by ckdpd02date order by ckdpd02date desc limit :dayNum  ";
            $dataDatelist = Yii::$app->db->createCommand($datelistSql, [':ptid' => $ptid, ':dayNum' => intval($dayNum)])->queryAll();
            $startDate = $dataDatelist[sizeof($dataDatelist) - 1]["ckdpd02date"];
            $sql = "select * from tbdata_1484716303035847800 where ptid=:ptid"
                    . " and ckdpd02date <> '' "
                    . " and ckdpd02date >=  '$startDate ' "
                    . " group by ckdpd02date order by ckdpd02date asc";
        } else {

            if ($startDate == '' || $endEate == '') {
                $sql = "select * from tbdata_1484716303035847800 where  ptid=:ptid"
                        . " and ckdpd02date <> '' "
                        . " group by ckdpd02date order by ckdpd02date asc";
            } else {
                $sql = "select * from tbdata_1484716303035847800 where  ptid=:ptid"
                        . " and ckdpd02date <> '' "
                        . " and ckdpd02date >=  '$startDate ' "
                        . " and ckdpd02date <=  '$endEate ' "
                        . " group by ckdpd02date order by ckdpd02date asc";
            }
        }

        $data = Yii::$app->db->createCommand($sql, [':ptid' => $ptid])->queryAll();

        $suminArr = [];
        $sumoutArr = [];
        $sumArr = [];
        $datedata = [];
        for ($i = 0; $i < sizeof($data); $i++) {
            $sumin = intval($data[$i]["r1invol"]) + intval($data[$i]["r2invol"]) + intval($data[$i]["r3invol"]) + intval($data[$i]["r4invol"]) + intval($data[$i]["r5invol"]) + intval($data[$i]["r6invol"]) + intval($data[$i]["r7invol"]) + intval($data[$i]["r8invol"]);
            $sumout = intval($data[$i]["r1outvol"]) + intval($data[$i]["r2outvol"]) + intval($data[$i]["r3outvol"]) + intval($data[$i]["r4outvol"]) + intval($data[$i]["r5outvol"]) + intval($data[$i]["r6outvol"]) + intval($data[$i]["r7outvol"]) + intval($data[$i]["r8outvol"]);
            $sum = $sumout - $sumin;

            array_push($datedata, $data[$i]["ckdpd02date"]);

            if (strlen($data[$i]["med_inbody"]) > 0) {
                array_push($sumArr, intval($data[$i]["med_inbody"]));
            } else {
                array_push($sumArr, intval($data[$i]["sum_watervol"]));

                $data[$i]["med_inbody"] = $data[$i]["sum_watervol"];
            }
            array_push($suminArr, $sumin);
            array_push($sumoutArr, $sumout);
        }
        $datagrid = new \yii\data\ArrayDataProvider([
            'allModels' => $data
        ]);
        //    if(sizeof($data)==0)return $this->renderAjax('nodata',['$graphName'=>'สรุปปริมาตรนํ้ายาล้างไตที่เข้าออกร่างกายต่อวัน']);

        return $this->renderAjax('sol', ['data' => $data, 'datedata' => $datedata, 'suminArr' => $suminArr, 'sumoutArr' => $sumoutArr, 'sumArr' => $sumArr, 'datagrid' => $datagrid]);
    }

    public function actionEgfr() {
        $request = Yii::$app->request;
        $ptlink = Yii::$app->request->get('ptid');
        $cid = Yii::$app->request->get('cid');


        return $this->renderAjax('egfr', ['test' => $ptid]);
    }

    public function actionTest() {

        $ptlink = Yii::$app->request->get('ptlink');
        $cid = Yii::$app->request->get('cid');
        $ptid = \backend\modules\ckdpd\classes\PersonInfo::getPtid($cid, $ptlink);
        echo "ptid = $ptid";
    }

    public function actionUrin() {
        $ptlink = Yii::$app->request->get('ptlink');
        $cid = Yii::$app->request->get('cid');
        $ptid = Yii::$app->request->get('ptid');
        $startDate = Yii::$app->request->get('startdate');
        $endEate = Yii::$app->request->get('enddate');
        $sitecode = Yii::$app->user->identity->userProfile->sitecode;
        $dayNum = Yii::$app->request->get('daynum');
        //get ptid
        if ($ptid == '')
            $ptid = \backend\modules\ckdpd\classes\PersonInfo::getPtid($cid, $ptlink);
        if ($ptlink == '1309da7fd7587c167e47054bad50eb01') {
            $ptid = '1497945602005167900';
        }
        if ($dayNum > 0) {
            $datelistSql = "select ckdpd03_date from tbdata_1484472652049461000 "
                    . "where 1 "
                    . " and ptid=:ptid"
                    . " and ckdpd03_date <> '' " . "  and urine_vol <> ''  "
                    . " group by ckdpd03_date order by ckdpd03_date desc limit :dayNum  ";
            $dataDatelist = Yii::$app->db->createCommand($datelistSql, [':ptid' => $ptid, ':dayNum' => intval($dayNum)])->queryAll();
            $startDate = $dataDatelist[sizeof($dataDatelist) - 1]["ckdpd04_date"];
            $sql = "select * from tbdata_1484472652049461000 where  ptid=:ptid"
                    . " and ckdpd03_date <> '' " . "  and urine_vol <> ''  "
                    . " and ckdpd03_date >=  '$startDate ' "
                    . " group by ckdpd03_date order by ckdpd03_date asc";
        } else {
            if ($startDate == '' || $endEate == '') {
                $sql = "select * from tbdata_1484472652049461000 where ptid=:ptid"
                        . " and ckdpd03_date <> '' " . "  and urine_vol <> ''  "
                        . " group by ckdpd03_date order by ckdpd03_date asc";
            } else {
                $sql = "select * from tbdata_1484472652049461000 where  ptid=:ptid"
                        . " and ckdpd03_date <> '' " . "  and urine_vol <> ''  "
                        . " and ckdpd03_date >=  '$startDate' "
                        . " and ckdpd03_date <=  '$endEate' "
                        . " group by ckdpd03_date order by ckdpd03_date asc";
            }
        }
        //   \appxq\sdii\utils\VarDumper::dump($sql);
        $data = Yii::$app->db->createCommand($sql, [':ptid' => $ptid])->queryAll();
        $gdata = [];
        $datedata = [];
        for ($i = 0; $i < sizeof($data); $i++) {
            $tmparray = [intval($data[$i]["urine_vol"])];
            array_push($datedata, $data[$i]["ckdpd03_date"]);
            array_push($gdata, $tmparray);
        }

        $datagrid = new \yii\data\ArrayDataProvider([
            'allModels' => $data
        ]);

        //  if(sizeof($data)==0)return $this->renderAjax('nodata');

        return $this->renderAjax('urin', ['data' => $data, 'gdata' => $gdata, 'datedata' => $datedata, 'datagrid' => $datagrid]);
    }

    public function actionWeight() {
        $ptlink = Yii::$app->request->get('ptlink');
        $cid = Yii::$app->request->get('cid');
        $ptid = Yii::$app->request->get('ptid');
        $startDate = Yii::$app->request->get('startdate');
        $endEate = Yii::$app->request->get('enddate');
        $sitecode = Yii::$app->user->identity->userProfile->sitecode;
        $dayNum = Yii::$app->request->get('daynum');
        //get ptid
        if ($ptid == '')
            $ptid = \backend\modules\ckdpd\classes\PersonInfo::getPtid($cid, $ptlink);
        if ($ptlink == '1309da7fd7587c167e47054bad50eb01') {
            $ptid = '1497945602005167900';
        }
        if ($dayNum > 0) {
            $datelistSql = "select ckdpd03_date from tbdata_1484472652049461000 "
                    . "where 1 "
                    . " and ptid=:ptid"
                    . " and ckdpd03_date <> '' " . "  and weight <> ''  "
                    . " group by ckdpd03_date order by ckdpd03_date desc limit :dayNum  ";
            $dataDatelist = Yii::$app->db->createCommand($datelistSql, [':ptid' => $ptid, ':dayNum' => intval($dayNum)])->queryAll();
            $startDate = $dataDatelist[sizeof($dataDatelist) - 1]["ckdpd04_date"];
            $sql = "select * from tbdata_1484472652049461000 where  ptid=:ptid"
                    . " and ckdpd03_date <> '' " . "  and weight <> ''  "
                    . " and ckdpd03_date >=  '$startDate ' "
                    . " group by ckdpd03_date order by ckdpd03_date asc";
        } else {
            if ($startDate == '' || $endEate == '') {
                $sql = "select * from tbdata_1484472652049461000 where  ptid=:ptid"
                        . " and ckdpd03_date <> '' " . "  and weight <> ''  "
                        . " group by ckdpd03_date order by ckdpd03_date asc";
            } else {
                $sql = "select * from tbdata_1484472652049461000 where  ptid=:ptid"
                        . " and ckdpd03_date <> '' " . "  and weight <> ''  "
                        . " and ckdpd03_date >=  '$startDate' "
                        . " and ckdpd03_date <=  '$endEate' "
                        . " group by ckdpd03_date order by ckdpd03_date asc";
            }
        }
        //   \appxq\sdii\utils\VarDumper::dump($sql);
        $data = Yii::$app->db->createCommand($sql, [':ptid' => $ptid])->queryAll();
        $gdata = [];
        $datedata = [];
        for ($i = 0; $i < sizeof($data); $i++) {
            $tmparray = [intval($data[$i]["weight"])];
            array_push($datedata, $data[$i]["ckdpd03_date"]);
            array_push($gdata, $tmparray);
        }

        $datagrid = new \yii\data\ArrayDataProvider([
            'allModels' => $data
        ]);

        //  if(sizeof($data)==0)return $this->renderAjax('nodata');

        return $this->renderAjax('weight', ['data' => $data, 'gdata' => $gdata, 'datedata' => $datedata, 'datagrid' => $datagrid]);
    }

    public function actionSumalert() {
        $sitecode = Yii::$app->user->identity->userProfile->sitecode;


        $sql = "select count(*) as acount from tbdata_1484715176062828300 where status_msg <> 1 and rstat=1 and hsitecode =:sitecode";
        $countAlert = Yii::$app->db->createCommand($sql, [':sitecode' => $sitecode])->queryOne();

        return $countAlert["acount"];
    }

    public function actionSumbag() {
        $sitecode = Yii::$app->user->identity->userProfile->sitecode;

        $sql = "select count(DISTINCT ptid) as bagalert from tbdata_1484729154090320000 where ttmedset < 5  and rstat=1 " . "and hsitecode =:sitecode";
        $countAlert = Yii::$app->db->createCommand($sql, [':sitecode' => $sitecode])->queryOne();

        return $countAlert["bagalert"];
    }

    public function actionAlertDetail() {

        try {
            $sitecode = Yii::$app->user->identity->userProfile->sitecode;

            $sql = "select DISTINCT tbdata_1484715176062828300.create_date,tbdata_1484715176062828300.notification,tbdata_1484715176062828300.ptid,tbdata_1484715176062828300.hsitecode ,CONCAT(tb_data_coc.name, \" \",tb_data_coc.surname) AS pname from tbdata_1484715176062828300 left join  tb_data_coc on tbdata_1484715176062828300.ptid = tb_data_coc.ptid  where status_msg <> 1 and tbdata_1484715176062828300.hsitecode =:sitecode";
            $dataArr = Yii::$app->db->createCommand($sql, [':sitecode' => $sitecode])->queryAll();

//        $list = array();
//        foreach ($dataArr as $data) {
//            $list[$data['pname']][] = $data;
//        } 
            $datagrid = new \yii\data\ArrayDataProvider([
                'allModels' => $dataArr
            ]);
            //    \appxq\sdii\utils\VarDumper::dump( $dataArr);
            LogStash::Log("CKDReport", var_export($a, true), "");

            $grid = $this->render('alert', ['dataArr' => $datagrid]);
            LogStash::Log("CKDReport", "Show data success.", "");

            return $grid;
        } catch (Exception $e) {

            LogStash::Error("CKDReport", $e, "");

            \appxq\sdii\utils\VarDumper::dump($e);
        } catch (ErrorException $e) {
            LogStash::ErrorEx("CKDReport", $e, "");
            \appxq\sdii\utils\VarDumper::dump($e);
        }
    }

    public function actionBagDetail() {

        try {
            $sitecode = Yii::$app->user->identity->userProfile->sitecode;
            $sql = "SELECT tbdata_1484729154090320000.ptid,tbdata_1484729154090320000.hsitecode ,tbdata_1484729154090320000.create_date,tbdata_1484729154090320000.ttmedset ,CONCAT(tb_data_coc.name, \" \",tb_data_coc.surname) AS pname  FROM tbdata_1484729154090320000
 left join  tb_data_coc on tbdata_1484729154090320000.ptid = tb_data_coc.ptid
WHERE  tbdata_1484729154090320000.hsitecode =:sitecode and  ttmedset <5
and tbdata_1484729154090320000.rstat = 1
group by tbdata_1484729154090320000.ptid
";
            $dataArr = Yii::$app->db->createCommand($sql, [':sitecode' => $sitecode])->queryAll();
            $datagrid = new \yii\data\ArrayDataProvider([
                'allModels' => $dataArr
            ]);
            $grid = $this->render('bag', ['dataArr' => $datagrid]);
            return $grid;
        } catch (Exception $e) {

            LogStash::Error("CKDReport", $e, "");
        } catch (ErrorException $e) {
            LogStash::ErrorEx("CKDReport", $e, "");
        }
    }

    public function actionResultAjax() {
        $sitecode = Yii::$app->user->identity->userProfile->sitecode;
        $count = Yii::$app->db->createCommand("
     SELECT  COUNT(DISTINCT tbdata_1484575078056625200.id) AS 'COUNT(*)'
FROM tbdata_1484575078056625200 
inner join tb_data_coc on tbdata_1484575078056625200.ptid = tb_data_coc.ptid 
WHERE tbdata_1484575078056625200.rstat=2 and picture <> '' and  tbdata_1484575078056625200.xsourcex=:sitecode
", ['sitecode' => $sitecode]
                )->queryScalar();

        $provider = new SqlDataProvider([
            'sql' => "
        SELECT tbdata_1484575078056625200.id ,tbdata_1484575078056625200.picture, concat(tb_data_coc.name,' ',tb_data_coc.surname) as name 
FROM tbdata_1484575078056625200 
inner join tb_data_coc on tbdata_1484575078056625200.ptid = tb_data_coc.ptid 
WHERE tbdata_1484575078056625200.rstat=2 and picture <> '' and  tbdata_1484575078056625200.xsourcex=:sitecode
group by tbdata_1484575078056625200.id
"
            ,
            'params' => ['sitecode' => $sitecode],
            'totalCount' => $count,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'attributes' => [
                    'id'
                ],
            ],
        ]);


        $countAlert = Yii::$app->db->createCommand("
         select count(DISTINCT tbdata_1484715176062828300.create_date) as 'COUNT(*)' from tbdata_1484715176062828300 inner join  tb_data_coc on tbdata_1484715176062828300.ptid = tb_data_coc.ptid  where status_msg <> 1 and tbdata_1484715176062828300.hsitecode =:sitecode

", ['sitecode' => $sitecode]
                )->queryScalar();

        $providerAlert = new SqlDataProvider([
            'sql' => "
               select DISTINCT tbdata_1484715176062828300.create_date,tbdata_1484715176062828300.notification,tbdata_1484715176062828300.ptid,tbdata_1484715176062828300.hsitecode ,CONCAT(tb_data_coc.name, \" \",tb_data_coc.surname) AS pname from tbdata_1484715176062828300 left join  tb_data_coc on tbdata_1484715176062828300.ptid = tb_data_coc.ptid  where status_msg <> 1 and tbdata_1484715176062828300.hsitecode =:sitecode

",
            'params' => ['sitecode' => $sitecode],
            'totalCount' => $countAlert,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'attributes' => [
                    'id'
                ],
            ],
        ]);


        $countBag = Yii::$app->db->createCommand("
SELECT count(tbdata_1484729154090320000.ptid) as 'COUNT(*)'  FROM tbdata_1484729154090320000
 inner join  tb_data_coc on tbdata_1484729154090320000.ptid = tb_data_coc.ptid
WHERE  tbdata_1484729154090320000.hsitecode =:sitecode and  ttmedset <5
and tbdata_1484729154090320000.rstat = 1
group by tbdata_1484729154090320000.ptid
", ['sitecode' => $sitecode]
                )->queryScalar();

        $providerBag = new SqlDataProvider([
            'sql' => "
SELECT tbdata_1484729154090320000.ptid,tbdata_1484729154090320000.hsitecode ,tbdata_1484729154090320000.create_date,tbdata_1484729154090320000.ttmedset ,CONCAT(tb_data_coc.name, \" \",tb_data_coc.surname) AS pname  FROM tbdata_1484729154090320000
 inner join  tb_data_coc on tbdata_1484729154090320000.ptid = tb_data_coc.ptid
WHERE  tbdata_1484729154090320000.hsitecode =:sitecode and  ttmedset <5
and tbdata_1484729154090320000.rstat = 1"
            ,
            'params' => ['sitecode' => $sitecode],
            'totalCount' => $countBag,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'attributes' => [
                    'id'
                ],
            ],
        ]);


        $countSql = \backend\modules\ckdpd\classes\CkdData::getCountPatient($sitecode);
        $countInstallSql = \backend\modules\ckdpd\classes\CkdData::getCountPatientMobile($sitecode);

        $countSendDataSql = \backend\modules\ckdpd\classes\CkdData::getCountPatientMobileSendDayData($sitecode, '2018-01-24');
        $countdata = array('patientCount'=>$countSql['countptid'],'app_install'=> $countInstallSql['countmobile'],'count_send'=> sizeof($countSendDataSql));


// returns an array of data rows
        $models = $provider->getModels();
        return $this->renderAjax("ajaxresult", ['provider' => $provider, 'providerAlert' => $providerAlert
                , 'providerBag' => $providerBag
                ,'countdata'=> $countdata 
                ]);
    }

    public function actionJsonLog() {

        Yii::$app->response->format = Response::FORMAT_JSON;
        $sql = "select data_id,ezf_id,
SUBSTR(sql_log,LOCATE( 'add1n6code', sql_log, 1)+13, 6)  add1n6code,
SUBSTR(sql_log,LOCATE( 'add1n7code', sql_log, 1)+13, 4)  add1n7code,
SUBSTR(sql_log,LOCATE( 'add1n8code', sql_log, 1)+13, 2)  add1n8code

from `tccfixmobile`.`ezform_sql_log` where `ezf_id` = '1437377239070462301'   and sql_log like '%add1n6code%' ";

        $data = Yii::$app->db->createCommand($sql)->queryAll();
        return $data;
    }

}
