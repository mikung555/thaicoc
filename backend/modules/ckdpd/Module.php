<?php

namespace backend\modules\ckdpd;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\modules\ckdpd\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
