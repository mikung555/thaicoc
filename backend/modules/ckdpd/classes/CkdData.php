<?php

namespace backend\modules\ckdpd\classes;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of LoginForm
 *
 * @author engiball
 */
class CkdData {

    public static function getImageList($sitecode, $rstat = 1) {
        $sql = "select * from `tbdata_1484575078056625200` where sitecode=:sitecode and rstat!=0 and rstat<=:rstat";
        $data = \Yii::$app->db->createCommand($sql, [':sitecode' => $sitecode, ':rstat' => $rstat])->queryOne();
        return $data;
    }

    public static function getNotiList($sitecode, $rstat = 1) {
        $sql = "select * from tbdata_1484715176062828300 where sitecode=:sitecode and rstat!=0 and rstat<=:rstat";
        $data = \Yii::$app->db->createCommand($sql, [':sitecode' => $sitecode, ':rstat' => $rstat])->queryOne();
        return $data;
    }

    public static function getInBody() {
        $sql = "
            SELECT *
            FROM tbdata_1484716303035847800
            WHERE update_date IN (
            SELECT MAX(update_date)
            FROM tbdata_1484716303035847800
            where ckdpd02date>=:startDate and ckdpd02date<=:endDate and med_inbody <0
            and sitecode=:sitecode
            GROUP BY ckdpd02date
            );";
        $data = \Yii::$app->db->createCommand($sql, [':sitecode' => $sitecode, ':rstat' => $rstat
                    , ':startDate' => $startDate, ':endDate' => $endDate])->queryAll();
        return $data;
    }

    public static function getCountPatient($sitecode) {
        $sql = "select count(distinct ptid) as countptid from tbdata_1484589286074665600 where sitecode=:sitecode";
        
        $data = \Yii::$app->db->createCommand($sql, [':sitecode' => $sitecode])->queryOne();
        return $data;
    }
    
        public static function getCountPatientMobile($sitecode) {
        $sql = "select count(distinct user_id) as countmobile from inv_user where sitecode=:sitecode and  appinstall =1";
        
        $data = \Yii::$app->db->createCommand($sql, [':sitecode' => $sitecode])->queryOne();
        return $data;
    }

        public static function getCountPatientMobileSendDayData($sitecode,$selectDate) {       
            $datetime1 = date_create($selectDate);
            $datetime2 = date_create();
            $interval = date_diff($datetime1, $datetime2);
            $diffDate = $interval->format("%d");
            $interval = date_diff($datetime1, $datetime2);
            $sql = "SELECT ptid, COUNT(*) as c from ckd_mobile_stat where sitecode=:sitecode and usedate >=:selectDate 
                    GROUP BY ptid
                    HAVING COUNT(*) > :diffDate";        
        $data = \Yii::$app->db->createCommand($sql, [':sitecode' => $sitecode,':selectDate' => $selectDate,":diffDate"=>$diffDate])->queryAll();
        return $data;
    }



}
