<?php

namespace backend\modules\ckdpd\models;

use Yii;

/**
 * This is the model class for table "tbdata_1484575078056625200".
 *
 * @property integer $id
 * @property integer $ptid
 * @property string $xsourcex
 * @property string $xdepartmentx
 * @property integer $rstat
 * @property string $sitecode
 * @property string $ptcode
 * @property string $ptcodefull
 * @property string $hptcode
 * @property string $hsitecode
 * @property integer $user_create
 * @property string $create_date
 * @property integer $user_update
 * @property string $update_date
 * @property integer $target
 * @property string $sys_lat
 * @property string $sys_lng
 * @property string $error
 * @property string $picture
 * @property string $recorder
 * @property string $source_app07
 * @property string $picture_type
 */
class PdImage extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbdata_1484575078056625200';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'xsourcex'], 'required'],
            [['id', 'ptid', 'rstat', 'user_create', 'user_update', 'target'], 'integer'],
            [['create_date', 'update_date'], 'safe'],
            [['error'], 'string'],
            [['xsourcex', 'xdepartmentx', 'ptcodefull', 'sys_lat', 'sys_lng'], 'string', 'max' => 20],
            [['sitecode', 'ptcode', 'hptcode', 'hsitecode', 'picture_type'], 'string', 'max' => 10],
            [['picture', 'recorder', 'source_app07'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ptid' => 'Ptid',
            'xsourcex' => 'Xsourcex',
            'xdepartmentx' => 'Xdepartmentx',
            'rstat' => 'Rstat',
            'sitecode' => 'Sitecode',
            'ptcode' => 'Ptcode',
            'ptcodefull' => 'Ptcodefull',
            'hptcode' => 'Hptcode',
            'hsitecode' => 'Hsitecode',
            'user_create' => 'User Create',
            'create_date' => 'Create Date',
            'user_update' => 'User Update',
            'update_date' => 'Update Date',
            'target' => 'Target',
            'sys_lat' => 'Sys Lat',
            'sys_lng' => 'Sys Lng',
            'error' => 'Error',
            'picture' => 'Picture',
            'recorder' => 'Recorder',
            'source_app07' => 'Source App07',
            'picture_type' => 'Picture Type',
        ];
    }
}
