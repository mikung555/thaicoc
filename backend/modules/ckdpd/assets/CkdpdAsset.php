<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace backend\modules\ckdpd\assets;

use yii\web\AssetBundle;
/**
 * Description of InputManager
 *
 * @author engiball
 */
class CkdpdAsset extends AssetBundle {
     // public $sourcePath='@backend/modules/ckdpd/assets';
public $sourcePath = '@bower/bootstrap/dist';
    public $css = [
    ];
    public $js = [

	'js/bootstrap.js'

    ];
      public $jsOptions = [
	'position' => \yii\web\View::POS_HEAD
    ];
    
    public $depends = [
	'yii\web\YiiAsset',
        'yii\web\JqueryAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
    
      public $publishOptions = [
        'forceCopy'=>true,
      ];

}
