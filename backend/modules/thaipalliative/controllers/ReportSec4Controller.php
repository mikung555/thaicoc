<?php

namespace backend\modules\thaipalliative\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\VarDumper;

class ReportSec4Controller extends Controller
{
    public function actionIndex()
    {
        return $this->render('index');
    }
    public function actionTable32()
    {

$dataall=self::GetDataTable32();
$data=self::GetDataTable32('13777');
      //  exit();
      $render="table32";
      $url=$this->renderAjax($render, [

      'data' => $data,
      'dataall' => $dataall,

      ]);
      return  $url;
    }//test

    public function GetDataTable32($sitecode=null)
    {
      if ($sitecode==null) {
        $datasql="";
      }else{
        $datasql="WHERE sitecode='$sitecode'";
      }
      $sqlControl = "SELECT
SUM(oxigen) AS totaloxigen,
SUM(syringe) AS totalsyrigne,
SUM(bed) AS totalbed,
SUM(edrug) AS totaledrug,
SUM(tank) AS totaltank,
SUM(eqip) AS totaleqip,
SUM(monkeybar) AS totalmonkeybar,
SUM(wheelcar) AS totalwheelcar

FROM
(
SELECT
sitecode,
IF(var33r1c2=1,1,0) AS oxigen,
IF(var33r4c2=1,1,0) AS syringe,
IF(var33r5c2=1,1,0) AS bed,
IF(var33r6c2=1,1,0) AS edrug,
IF(var33r2c2=1,1,0) AS tank,
IF(var33r7c2=1,1,0) AS eqip,
IF(var33r8c2=1,1,0) AS monkeybar,
IF(var33r9c2=1,1,0) AS wheelcar
FROM tbdata_3
)
AS total
$datasql";
    $dataProvider = Yii::$app->db->createCommand($sqlControl)->queryAll();
    return $dataProvider;
  }
  }
 // table 32
