<?
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;


$domain=Url::home();
?>
<div class="table-responsive">
  <h2>ตารางที่ 32 </h2>
  <p>จำนวนและร้อยละผู้ป่วยที่มีการยืมอุปกรณ์การแพทย์ และระยะเวลาที่ยืม
    จำแนกตามประเภทอุปกรณ์ ระหว่าง <b>  <? echo Yii::$app->formatter->asDate($data[0][date_start], 'long')  ?> ถึง <? echo Yii::$app->formatter->asDate($data[0][date_end], 'long')  ?></b></p>
  <table class="table table-bordered">
    <thead>
      <tr>
        <td rowspan="2"><center>การยืมอุปกรณ์</center></td>
        <td colspan="2"><center>จำนวนผู้ป่วย</center></td>
        <td colspan="4" ><center>ระยะเวลาที่ยืม</center></td>
        <tr>
        <td><center>จำนวน</center></td>
        <td><center>ร้อยละ</center></td>
    	  <td><center>Min</center></td>
        <td><center>Max</center></td>
    	  <td><center>Median</center></td>
        <td><center>Mean±SD</center></td>
      </tr>
      </thead>
    <tbody>
      <tr >
        <td>เครื่องผลิตออกซิเจน</td>
        <td align='right'><?= $data[0][totaloxigen] ?></td>
        <td align='right'><?= number_format(($data[0][totaloxigen]/$dataall[0][totaloxigen])*100,1) ?></td>
        <td align='right'></td>
    	 <td align='right'></td>
    	 <td align='right'></td>
    	 <td align='right'></td>
      </tr>
      <tr>
        <td >Syringe driver</td>
        <td align='right'><?= $data[0][totalsyrigne] ?></td>
        <td align='right'><?= number_format(($data[0][totalsyrigne]/$dataall[0][totalsyrigne])*100,1) ?></td>
        <td align='right'>&nbsp;</td>
    	  <td align='right'>&nbsp;</td>
    	  <td align='right'>&nbsp;</td>
    	  <td align='right'>&nbsp;</td>
      </tr>
      <tr>
        <td >เตียง</td>
        <td align='right'><?=  $data[0][totalbed] ?></td>
        <td align='right'><?= number_format(($data[0][totalbed]/$dataall[0][totalbed])*100,1) ?></td>
        <td align='right'>&nbsp;</td>
    	  <td align='right'>&nbsp;</td>
    	  <td align='right'>&nbsp;</td>
    	  <td align='right'>&nbsp;</td>
      </tr>
      <tr>
        <td >เครื่องพ่นยา</td>
        <td align='right'><?= $data[0][totaledrug] ?></td>
        <td align='right'><?= number_format(($data[0][totaledrug]/$dataall[0][totaledrug])*100,1) ?></td>
        <td align='right'>&nbsp;</td>
    	  <td align='right'>&nbsp;</td>
    	  <td align='right'>&nbsp;</td>
    	  <td align='right'>&nbsp;</td>
      </tr>
       <tr>
        <td >ออกซิเจน tank</td>
        <td align='right'><?= $data[0][totaltank] ?></td>
        <td align='right'><?= number_format(($data[0][totaltank]/$dataall[0][totaledrug])*100,1) ?></td>
         <td align='right'>&nbsp;</td>
    	 <td align='right'>&nbsp;</td>
    	 <td align='right'>&nbsp;</td>
    	 <td align='right'>&nbsp;</td>
      </tr>
       <tr>
        <td >เครื่องดูดเสมหะ</td>
        <td align='right'><?= $data[0][totaleqip] ?></td>
        <td align='right'><?= number_format(($data[0][totaleqip]/$dataall[0][totaleqip])*100,1) ?></td>
         <td align='right'>&nbsp;</td>
    	 <td align='right'>&nbsp;</td>
    	 <td align='right'>&nbsp;</td>
    	 <td align='right'>&nbsp;</td>
      </tr>
      <tr>
        <td >Monkey bar</td>
        <td align='right'><?=  $data[0][totalmonkeybar] ?></td>
        <td align='right'><?= number_format(($data[0][totalmonkeybar]/$dataall[0][totalmonkeybar])*100,1) ?></td>
        <td align='right'>&nbsp;</td>
    	  <td align='right'>&nbsp;</td>
    	  <td align='right'>&nbsp;</td>
    	  <td align='right'>&nbsp;</td>
      </tr>
       <tr>
        <td >รถเข็น</td>
        <td align='right'><?= $data[0][totalwheelcar] ?></td>
       <td align='right'><?= number_format(($data[0][totalwheelcar]/$dataall[0][totalwheelcar])*100,1) ?></td>
       <td align='right'>&nbsp;</td>
    	 <td align='right'>&nbsp;</td>
    	 <td align='right'>&nbsp;</td>
    	 <td align='right'>&nbsp;</td>
      </tr>
      </tbody>
      <tfoot>
      <tr>
        <?php $sumtotal=array_sum($data[0]);  ?>
        <td align="center">รวมทั้งหมด</td>
    	  <td align="right"><?= $sumtotal  ?></td>
        <td></td>
        <td></td>
    	  <td></td>
    	  <td></td>
    	  <td></td>
      </tr>
      </tfoot>
    </table>
  </div>
