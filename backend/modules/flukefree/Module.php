<?php

namespace app\modules\flukefree;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\flukefree\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
