<?php

use yii\bootstrap\Modal;

# style sheet
$this->registerCss("

.loader {
  border: 16px solid #f3f3f3;
  border-radius: 50%;
  border-top: 16px solid blue;
  border-right: 16px solid green;
  border-bottom: 16px solid red;
  border-left: 16px solid pink;
  width: 50px;
  height: 50px;
  -webkit-animation: spin 2s linear infinite;
  animation: spin 2s linear infinite;
}

@-webkit-keyframes spin {
  0% { -webkit-transform: rotate(0deg); }
  100% { -webkit-transform: rotate(360deg); }
}

@keyframes spin {
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
}


.panel-table .panel-body{
  padding:0;
}

.panel-table .panel-body .table-bordered{
  border-style: none;
  margin:0;
}

.panel-table .panel-body .table-bordered > thead > tr > th:first-of-type {
    text-align:center;
    width: 100px;
}

.panel-table .panel-body .table-bordered > thead > tr > th:last-of-type,
.panel-table .panel-body .table-bordered > tbody > tr > td:last-of-type {
  border-right: 0px;
}

.panel-table .panel-body .table-bordered > thead > tr > th:first-of-type,
.panel-table .panel-body .table-bordered > tbody > tr > td:first-of-type {
  border-left: 0px;
}

.panel-table .panel-body .table-bordered > tbody > tr:first-of-type > td{
  border-bottom: 0px;
}

.panel-table .panel-body .table-bordered > thead > tr:first-of-type > th{
  border-top: 0px;
}

.panel-table .panel-footer .pagination{
  margin:0; 
}

/*
used to vertically center elements, may need modification if you're not using default sizes.
*/
.panel-table .panel-footer .col{
 line-height: 34px;
 height: 34px;
}

.panel-table .panel-heading .col h3{
 line-height: 30px;
 height: 30px;
}

.panel-table .panel-body .table-bordered > tbody > tr > td{
  line-height: 34px;
}

.modal-dialog.modal-xxl  {
    /* new custom width */
    width: 90%;
    /* must be half of the width, minus scrollbar on the left (30px) */
    }
        
");


$this->registerJs("
$(document).on('click', '*[id^=popupModal]', function(e) {
        $('#visitdata').empty();
        
        e.preventDefault();
        $('#modal').modal('show').find('.modal-content').load($(this).attr('href'));
        

        // call ajax
        $('#visitheader').html('<h1><b>'+$(this).attr('title')+'</b></h1>');
        $('#visitdata').html('<div class=loader></div>');
        // ajax
        var target = $(this).attr('target');
        var detail = $(this).attr('detail');
        var url = $(this).attr('data-url'); //'view-list-of-visit';
        
        $.get(url, {
            provincecode:$(this).attr('provincecode'),
            amphurcode:$(this).attr('amphurcode'),
            tamboncode:$(this).attr('tamboncode'),
        })
        .done(function( data ) {
            $('#visitdata').html(data);
        });
    });
");


Modal::begin([
    'id' =>'modal',
    'header' => '<div id="visitheader"><h2>คนไข้ที่เลือกดู</h2></div>',
    'size' => 'modal-xxl'
    
]);
echo "<div id='visitdata'>";
echo 'คนไข้ที่เลือก';
echo "</div>";

Modal::end();

?>
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<?php

echo $this->render('_list60',
                [
                    'l60' => $l60,
                ]);



?>

