<?php
use yii\grid\GridView;


?>
<p></p>
<p></p>
<div class="panel panel-primary panel-table">
    <div class="panel-heading">
        <div class="row">
            <div class="col col-xs-6">
                <h3 class="panel-title">แสดงโรงเรียนต้อนแบบ</h3>
            </div>
            <div class="col col-xs-6 text-right">
                <?php
//                            $url = Url::to(Url::current());
//                            echo yii\helpers\Html::a('Refresh', $url
//                                    , [
//                                        'class' => 'btn btn-sm btn-primary btn-create',
//                                    ]);
                ?>

            </div>
        </div>
    </div>
    <div class="panel-body">
        <div id="schoolslist">
            <p></p>
            <p></p>
<?php

$models = $l60->getModels();


echo GridView::widget([
    'dataProvider' => $l60,
    'tableOptions' => [
        'class' => 'table table-striped table-bordered'
        ],
    'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => '<font style="color: #d2d6de;font-style: italic;">Null</font>'],
    'columns' => [
        [
            'class' => 'yii\grid\SerialColumn', // <-- here
            // you may configure additional properties here
        ],
        
        // Full code from site
        [
            'attribute'=>'hcode',
            'format' => 'text',
            'label'=>'โรงเรีบนต้นแบบในปี 60',
            'headerOptions' => ['style' => 'text-align:left; min-width: 120px; max-width: 180px;'],
            'contentOptions' => ['style' => 'text-align:left; min-width: 120px; max-width: 180px;'],
//            'headerOptions' => ['class' => 'text-center'],
//            'contentOptions' => ['class' => 'text-center'],
            'value' => function($models){
                $txt = $models['code'].': โรงเรียน'.$models['name'];
                return $txt;
            }
        ],
        [
            'attribute'=>'zone_code',
            'format' => 'raw',
            'label'=>'เขต',
            'headerOptions' => ['style' => 'text-align:left; min-width: 60px; max-width: 100px;'],
            'contentOptions' => ['style' => 'text-align:left; min-width: 60px; max-width: 100px;'],
            'value' => function($models){
                return $models['zone_code'];
            }
        ],
        [
            'attribute'=>'province',
            'format' => 'raw',
            'label'=>'จังหวัด',
            'headerOptions' => ['style' => 'text-align:left; min-width: 100px; max-width: 100px;'],
            'contentOptions' => ['style' => 'text-align:left; min-width: 100px; max-width: 100px;'],
            'value' => function($models){
                $txt = $models['province'];
                return $txt;
            }
        ],
        [
            'attribute'=>'amphur',
            'format' => 'raw',
            'label'=>'อำเภอ',
            'headerOptions' => ['style' => 'text-align:left; min-width: 100px; max-width: 100px;'],
            'contentOptions' => ['style' => 'text-align:left; min-width: 100px; max-width: 100px;'],
            'value' => function($models){
                $txt = $models['amphur'];
                return $txt;
            }
        ],
        [
            'attribute'=>'summarydata',
            'format' => 'raw',
            'label'=>'ตำบล',
            
            'headerOptions' => ['style' => 'text-align:left; min-width: 100px; max-width: 100px;', 'title' => 'คลิ๊กเพื่อแสดงการนำเข้าข้อมูล'],
            'contentOptions' => ['style' => 'text-align:left; min-width: 100px; max-width: 100px; '],
            'value' => function($models){
                $sitecode = Yii::$app->user->identity->userProfile->sitecode;
                if( 1 ){
                    $dataUrl = \yii\helpers\Url::to([
                            'list60-hospital-relate',
                        ]);
                    $txtUrl = yii\helpers\Html::a(
                        $models['tambon'],
                        NULL ,
                        [
                            'class' => ''
                            , 'id' => 'popupModal'
                            , 'title' => 'โรงพยาบาลที่อยู่ในตำบลเดียวกัน'
                            , 'style' => 'cursor:pointer;'
                            , 'data-url' => 'list60-hospital-relate'
                            , 'provincecode' => $models['provincecode']
                            , 'amphurcode' => $models['amphurcode']
                            , 'tamboncode' => $models['tamboncode']
                        ]);
                    $txtPopup = yii\helpers\Html::a(' <span class="glyphicon glyphicon-eye-open" style="font-size:16px;color:blue; cursor:pointer;"></span>'
                        , NULL
                        , [
                            'class' => ''
                            , 'id' => 'popupModal'
                            , 'title' => 'โรงพยาบาลที่อยู่ในตำบลเดียวกัน'
                            , 'style' => 'cursor:pointer;'
                            , 'data-url' => 'list60-hospital-relate'
                            , 'provincecode' => $models['provincecode']
                            , 'amphurcode' => $models['amphurcode']
                            , 'tamboncode' => $models['tamboncode']
                            //, 'detail' => $value['typeofsurgery'].' xtumorsitex '.$value['TumorSite'].' xstagex '.$value['stage']
                        ]);
                }else{
                    $txtUrl = $models['tambon'];
                }
                return $txtUrl.$txtPopup;
            }
        ],
    ],
]);
?>
        </div>
    </div>
</div>
<p></p>
<p></p>
<p></p><div class="container">
    <div class="row">
        <div class="panel panel-primary panel-table">
            <div class="panel-heading">
                <div class="row">
                    <div class="col col-xs-6">
                        <h3 class="panel-title">เพิ่มโรงเรียนเป้าหมาย</h3>
                    </div>
                    <div class="col col-xs-6 text-right">
                        <?php
        //                            $url = Url::to(Url::current());
        //                            echo yii\helpers\Html::a('Refresh', $url
        //                                    , [
        //                                        'class' => 'btn btn-sm btn-primary btn-create',
        //                                    ]);
                        ?>

                    </div>
                </div>
            </div>
            <div class="panel-body">
                <div id="schooladdtolist">
                    <p></p>
                </div>
            </div>
        </div>
    </div>
</div>
<p></p>
<p></p>
<p></p>
        
        
     
        
 
