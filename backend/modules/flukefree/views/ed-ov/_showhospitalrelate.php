<?php

use yii\grid\GridView;
use backend\modules\flukefree\classes\EdOvData;

if( 0 ){
    $h = $register = EdOvData::getSummaryRegisterofHosp('13777');
    echo "<pre align='left'>";
    //var_dump($h);
    print_r($h);
    var_dump($l60gt);
    echo "</pre>";
}


?>
<p></p>
<div class="panel panel-primary panel-table">
    <div class="panel-heading">
        <div class="row">
            <div class="col col-xs-6">
                <h3 class="panel-title">โรงพยาบาลที่อยู่ใน ตำบลเดียวกัน</h3>
            </div>
            <div class="col col-xs-6 text-right">
                <?php
//                            $url = Url::to(Url::current());
//                            echo yii\helpers\Html::a('Refresh', $url
//                                    , [
//                                        'class' => 'btn btn-sm btn-primary btn-create',
//                                    ]);
                ?>

            </div>
        </div>
    </div>
    <div class="panel-body">
<?php
$models = $l60->getModels();
echo GridView::widget([
    'dataProvider' => $l60,
    'tableOptions' => [
        'class' => 'table table-striped table-bordered'
        ],
    'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => '<font style="color: #d2d6de;font-style: italic;">Null</font>'],
    'columns' => [
        [
            'class' => 'yii\grid\SerialColumn', // <-- here
            // you may configure additional properties here
        ],
        
        // Full code from site
        
        [
            'attribute'=>'amphur',
            'format' => 'text',
            'label'=>'อำเภอ',
            'headerOptions' => ['style' => 'text-align:left; min-width: 120px; max-width: 60px;'],
            'contentOptions' => ['style' => 'text-align:left; min-width: 120px; max-width: 60px;'],
//            'headerOptions' => ['class' => 'text-center'],
//            'contentOptions' => ['class' => 'text-center'],
            'value' => function($models){
                $txt = $models['amphur']; //$model['amphurcode'].': '.
                return $txt;
            }
        ],
        [
            'attribute'=>'tambon',
            'format' => 'text',
            'label'=>'ตำบล',
            'headerOptions' => ['style' => 'text-align:left; min-width: 120px; max-width: 60px;'],
            'contentOptions' => ['style' => 'text-align:left; min-width: 120px; max-width: 60px;'],
//            'headerOptions' => ['class' => 'text-center'],
//            'contentOptions' => ['class' => 'text-center'],
            'value' => function($models){
                $txt = $models['tambon']; // $model['tamboncode'].': '.
                return $txt;
            }
        ],
        [
            'attribute'=>'hcode',
            'format' => 'text',
            'label'=>'โรงเรีบนต้นแบบในปี 60',
            'headerOptions' => ['style' => 'text-align:left; min-width: 120px; max-width: 220px;'],
            'contentOptions' => ['style' => 'text-align:left; min-width: 120px; max-width: 220px;'],
//            'headerOptions' => ['class' => 'text-center'],
//            'contentOptions' => ['class' => 'text-center'],
            'value' => function($models){
                $txt = $models['hcode'].': '.$models['name'];
                return $txt;
            }
        ],
        [
            'attribute'=>'register',
            'format' => 'text',
            'label'=>'ลงทะเบียน',
            'headerOptions' => ['style' => 'text-align:right; min-width: 80px; max-width: 60px;'],
            'contentOptions' => ['style' => 'text-align:right; min-width: 80px; max-width: 60px;', 'title'=>'ลงทะเบียนทั้งหมด: Submited / Save draft'],
//            'headerOptions' => ['class' => 'text-center'],
//            'contentOptions' => ['class' => 'text-center'],
            'value' => function($models){
                $register = EdOvData::getSummaryRegisterofHosp($models['hcode']);
                $txt = $register['allreg'].': '.$register['submited'].'/'.$register['waiting'];
                return $txt;
            }
        ],
        [
            'attribute'=>'cca01',
            'format' => 'text',
            'label'=>'CCA-01',
            'headerOptions' => ['style' => 'text-align:right; min-width: 80px; max-width: 60px;'],
            'contentOptions' => ['style' => 'text-align:right; min-width: 80px; max-width: 60px;'],
            'value' => function($models){
                $data = EdOvData::getSummaryCCA01ofHosp($models['hcode']);
                $txt = $data['alldata'].': '.$data['submited'].'/'.$data['waiting'];
                return $txt;
            }
        ],
        [
            'attribute'=>'cca02',
            'format' => 'text',
            'label'=>'Ultrasound',
            'headerOptions' => ['style' => 'text-align:right; min-width: 80px; max-width: 60px;'],
            'contentOptions' => ['style' => 'text-align:right; min-width: 80px; max-width: 60px;'],
            'value' => function($models){
                $data = EdOvData::getSummaryCCA02ofHosp($models['hcode']);
                $txt = $data['alldata'].': '.$data['submited'].'/'.$data['waiting'];
                return $txt;
            }
        ],
        [
            'attribute'=>'contact',
            'format' => 'raw',
            'label'=>'เบอร์ติดต่อ',
            'headerOptions' => ['style' => 'text-align:right; min-width: 80px; max-width: 60px;'],
            'contentOptions' => ['style' => 'text-align:right; min-width: 80px; max-width: 60px;'],
            'value' => function($models){
                $contact = EdOvData::getUserInHospitalRelate($models['hcode']);
                $txt = $contact['txt']['contact']; //'Tel: 089-xxxx-xxx';
                return $txt;
            }
        ],
    ],
]);
?>
    </div>
</div>
<?php     
        
               
$model = $l60gt->getModels();


?>
<p></p>
<p></p>
<div class="panel panel-primary panel-table">
    <div class="panel-heading">
        <div class="row">
            <div class="col col-xs-6">
                <h3 class="panel-title">แสดงภายในอำเภอ เดียวกัน</h3>
            </div>
            <div class="col col-xs-6 text-right">
                <?php
//                            $url = Url::to(Url::current());
//                            echo yii\helpers\Html::a('Refresh', $url
//                                    , [
//                                        'class' => 'btn btn-sm btn-primary btn-create',
//                                    ]);
                ?>

            </div>
        </div>
    </div>
    <div class="panel-body">
<?php
echo GridView::widget([
    'dataProvider' => $l60gt,
    'tableOptions' => [
        'class' => 'table table-striped table-bordered'
        ],
    'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => '<font style="color: #d2d6de;font-style: italic;">Null</font>'],
    'columns' => [
        [
            'class' => 'yii\grid\SerialColumn', // <-- here
            // you may configure additional properties here
        ],
        
        // Full code from site
        [
            'attribute'=>'amphur',
            'format' => 'text',
            'label'=>'อำเภอ',
            'headerOptions' => ['style' => 'text-align:left; min-width: 120px; max-width: 60px;'],
            'contentOptions' => ['style' => 'text-align:left; min-width: 120px; max-width: 60px;'],
//            'headerOptions' => ['class' => 'text-center'],
//            'contentOptions' => ['class' => 'text-center'],
            'value' => function($model){
                $txt = $model['amphur']; //$model['amphurcode'].': '.
                return $txt;
            }
        ],
        [
            'attribute'=>'tambon',
            'format' => 'text',
            'label'=>'ตำบล',
            'headerOptions' => ['style' => 'text-align:left; min-width: 120px; max-width: 60px;'],
            'contentOptions' => ['style' => 'text-align:left; min-width: 120px; max-width: 60px;'],
//            'headerOptions' => ['class' => 'text-center'],
//            'contentOptions' => ['class' => 'text-center'],
            'value' => function($model){
                $txt = $model['tambon']; // $model['tamboncode'].': '.
                return $txt;
            }
        ],
        [
            'attribute'=>'hcode',
            'format' => 'raw',
            'label'=>'โรงเรีบนต้นแบบในปี 60',
            'headerOptions' => ['style' => 'text-align:left; min-width: 120px; max-width: 220px;'],
            'contentOptions' => ['style' => 'text-align:left; min-width: 120px; max-width: 220px;'],
//            'headerOptions' => ['class' => 'text-center'],
//            'contentOptions' => ['class' => 'text-center'],
            'value' => function($model){
                $txtSpano = '';
                $txtSpanc = '';
                $request = Yii::$app->request;
                if( $request->get('amphurcode')==$model['amphurcode'] && $request->get('tamboncode')==$model['tamboncode'] ){
                    $txtSpano = '<span style="color: red;">';
                    $txtSpanc = '</span>';
                }
                $txt = $txtSpano.$model['hcode'].': โรงเรียน'.$model['name'].$txtSpanc;
                return $txt;
            }
        ],
        [
            'attribute'=>'register',
            'format' => 'raw',
            'label'=>'ลงทะเบียน',
            'headerOptions' => ['style' => 'text-align:right; min-width: 80px; max-width: 60px;'],
            'contentOptions' => ['style' => 'text-align:right; min-width: 80px; max-width: 60px;', 'title'=>'ลงทะเบียนทั้งหมด: Submited / Save draft'],
//            'headerOptions' => ['class' => 'text-center'],
//            'contentOptions' => ['class' => 'text-center'],
            'value' => function($model){
                $txtSpano = '';
                $txtSpanc = '';
                $request = Yii::$app->request;
                if( $request->get('amphurcode')==$model['amphurcode'] && $request->get('tamboncode')==$model['tamboncode'] ){
                    $txtSpano = '<span style="color: red;">';
                    $txtSpanc = '</span>';
                }
                $register = EdOvData::getSummaryRegisterofHosp($model['hcode']);
                $txt = $txtSpano.$register['allreg'].': '.$register['submited'].'/'.$register['waiting'].$txtSpanc;
                return $txt;
            }
        ],
        [
            'attribute'=>'cca01',
            'format' => 'raw',
            'label'=>'CCA-01',
            'headerOptions' => ['style' => 'text-align:right; min-width: 80px; max-width: 60px;'],
            'contentOptions' => ['style' => 'text-align:right; min-width: 80px; max-width: 60px;'],
            'value' => function($model){
                $txtSpano = '';
                $txtSpanc = '';
                $request = Yii::$app->request;
                if( $request->get('amphurcode')==$model['amphurcode'] && $request->get('tamboncode')==$model['tamboncode'] ){
                    $txtSpano = '<span style="color: red;">';
                    $txtSpanc = '</span>';
                }
                $data = EdOvData::getSummaryCCA01ofHosp($model['hcode']);
                $txt = $txtSpano.$data['alldata'].': '.$data['submited'].'/'.$data['waiting'].$txtSpanc;
                return $txt;
            }
        ],
        [
            'attribute'=>'cca02',
            'format' => 'raw',
            'label'=>'Ultrasound',
            'headerOptions' => ['style' => 'text-align:right; min-width: 80px; max-width: 60px;'],
            'contentOptions' => ['style' => 'text-align:right; min-width: 80px; max-width: 60px;'],
            'value' => function($model){
                $txtSpano = '';
                $txtSpanc = '';
                $request = Yii::$app->request;
                if( $request->get('amphurcode')==$model['amphurcode'] && $request->get('tamboncode')==$model['tamboncode'] ){
                    $txtSpano = '<span style="color: red;">';
                    $txtSpanc = '</span>';
                }
                $data = EdOvData::getSummaryCCA02ofHosp($model['hcode']);
                $txt = $txtSpano.$data['alldata'].': '.$data['submited'].'/'.$data['waiting'].$txtSpanc;
                return $txt;
            }
        ],
        [
            'attribute'=>'contact',
            'format' => 'raw',
            'label'=>'เบอร์ติดต่อ',
            'headerOptions' => ['style' => 'text-align:right; min-width: 80px; max-width: 60px;'],
            'contentOptions' => ['style' => 'text-align:right; min-width: 80px; max-width: 60px;'],
            'value' => function($model){
                //$data = EdOvData::getSummaryCCA02ofHosp($models['hcode']);
                $contact = EdOvData::getUserInHospitalRelate($model['hcode']);
                $txt = $contact['txt']['contact']; //'Tel: 089-xxxx-xxx';
                return $txt;
            }
        ],
    ],
]);
?>
    </div>
</div>
