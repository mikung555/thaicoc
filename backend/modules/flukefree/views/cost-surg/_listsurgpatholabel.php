<?php

?>
<div id="labeltable">
    <h1>แสดงรายชื่อของ <h2><label id="hospitaltext">โรงพยาบาลที่เลือก</label></h2></h1>
    <p>ข้อมูลที่แสดงทั้งหมด <label id="hospitalpatientcount">0</label> records โดยมีเงื่อนไขว่า ต้องเป็นข้อมูลที่ลงทะเบียนที่ <label id="hospitaltextnormal">โรงพยาบาลที่เลือก</label> <br />ต้องมีการผ่ารตัด (CCA-03) พร้อมผลพยาธิวิทยา (CCA-04)</p>
    <p>หากท่านไม่ได้เป็นบุคลากรในหน่วยบริการนั้น จะไม่สมารถดูข้อมูล พร้อมชื่อและนามสกุลของผู้ป่วยได้</p>
    <h4><label for="earlystagenumber"><i class="material-icons" style="font-size:16px;color:green" title="Advanced Stage">mood</i> Early Stage </label><b>&nbsp;<label id="earlystagenumber">0</label></b></h4>
    <h4><label for="advancestagenumber"><i class="material-icons" style="font-size:16px;color:red" title="Advanced Stage">mood_bad</i> Advance Stage </label><b>&nbsp;<label id="advancestagenumber">0</label></b></h4>
    <h4><label for="waitingstagenumber"><i class="material-icons" style="font-size:16px;color:gray" title="Advanced Stage">help</i> ข้อมูลยังไม่สมบูรณ์ </label><b>&nbsp;<label id="waitingstagenumber">0</label></b></h4>
    <p> </p><p> </p>
</div>

