<?php

use backend\modules\flukefree\classes\Hospital;
use backend\modules\flukefree\classes\EncodeString;
use backend\modules\flukefree\classes\ThaiDate;
use yii\helpers\Url;
use yii\helpers\Html;

$request = Yii::$app->request;
$hospital = Hospital::getHospitalDetail($hsitecode);

?>
<div id="listtable">
        <div class="col-md-12 col-md-offset-0">
            <div class="panel panel-default panel-table">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col col-xs-6">
                            <h3 class="panel-title">พบค้นไข้ที่อยู่ในเงื่อนไข <?php echo count($listSP); ?> คน</h3>
                        </div>
                        <div class="col col-xs-6 text-right">
                            <?php
//                            $url = Url::to(Url::current());
//                            echo yii\helpers\Html::a('Refresh', $url
//                                    , [
//                                        'class' => 'btn btn-sm btn-primary btn-create',
//                                    ]);
                            ?>
                            
                        </div>
                    </div>
                </div>
                <div class="panel-body">
                    <table class="table table-striped table-bordered table-list">
                        <thead>
                            <tr>
                                <th><em class="fa fa-cog"></em></th>
                                <th class="hidden-xs">SiteID</th>
                                <th class="hidden-xs">PID</th>
                                <th>HN</th>
                                <th>ชื่อ สกุล</th>
                                <th>วันที่ผ่าตัด</th>
                                <th style="text-align: right;">จำนวนครั้ง <br/>ที่เข้ารักษา</th>
                                <th>ประเภทการผ่าตัด</th>
                                <th>Tumor Site</th>
                                <th style="text-align: right;">Staging</th>
                            </tr> 
                        </thead>
                        <tbody>
                            <?php
                            $sitecode = Yii::$app->user->identity->userProfile->sitecode;
                            $sumEarlyStage = 0;
                            $sumAdvanceStage = 0;
                            $sumWaitingStage = 0;
                            if( count($listSP)>0 ){
                                foreach($listSP as $key => $value){
                                    // แสดงข้อมูลทั้งหมด ที่มีการผ่าตัด และ Patho
                                    
                                    
                                    if( $sitecode==$value['hsitecode'] ){
                                        $canViewFlag = TRUE;
                                        $canView = 'class="glyphicon glyphicon-eye-open" style="font-size:16px;color:blue"';
                                    }else{
                                        $canViewFlag = FALSE;
                                        $canView = 'class="glyphicon glyphicon-eye-close" style="font-size:16px;color:red"';
                                    }
                                    
                                    if($canViewFlag){
                                        //
                                    }else{
                                        // กรณีเป็น Site อื่น จะไม่สามารถดูข้อมูลได้
                                        //$value['title'] = '***';
                                        $value['name'] = EncodeString::replaceStar($value['name']);
                                        $value['surname'] = EncodeString::replaceStar($value['surname']);
                                        $value['HN'] = EncodeString::replaceStar($value['HN']);
                                    }
                                    
                                    $earlyStage = array('0','I','II');
                                    $advanceStage = array('III','IIIA','IIIB','IVA','IVB','UN');
                                    if(in_array($value['stage'], $earlyStage) ){
                                        $sumEarlyStage++;
                                        $stageColor = 'class="material-icons" style="font-size:16px;color:green" title="Early Stage"'; // 'btn-primary';
                                        $googleFont = "mood";
                                    }else if(in_array($value['stage'], $advanceStage) ){
                                        $sumAdvanceStage++;
                                        $stageColor = 'class="material-icons" style="font-size:16px;color:red" title="Advanced Stage"';
                                        $googleFont = "mood_bad";
                                    }else{
                                        $sumWaitingStage++;
                                        $stageColor = 'class="material-icons" style="font-size:16px;color:gray" title="ยังไม่ระบุ"'; //'btn-danger';
                                        $googleFont = "help";
                                    }
                            ?>
                            <tr>
                                <td align="center">
                                    <?php
                                    echo Html::a('<span '.$canView.'></span>'
                                            , NULL
                                            , [
                                                'class' => ''
                                                , 'id' => 'popupModal'
                                                , 'style' => 'cursor:pointer;'
                                                , 'target' => $value['ptid']
                                                , 'table' => 'tb_data_7'
                                                , 'title' => $value['hsitecode'].$value['hptcode'].': '.$value['title'].$value['name'].' '.$value['surname']
                                                , 'detail' => $value['typeofsurgery'].' xtumorsitex '.$value['TumorSite'].' xstagex '.$value['stage']
                                            ]

                                        );
                                    ?>
                                    <a>
                                        <i <?php echo $stageColor; ?>><?php echo $googleFont; ?></i>
                                    </a>
                                </td>
                                <td class="hidden-xs">
                                    <?php
                                    echo $value['hsitecode'];
                                    ?>
                                </td>
                                <td class="hidden-xs">
                                    <?php
                                    echo $value['hptcode'];
                                    ?>
                                </td>
                                <td>
                                    <?php
                                    echo $value['HN'];
                                    ?>
                                </td>
                                <td>
                                    <?php
                                    echo $value['title'].$value['name'].' '.$value['surname'];
                                    ?>
                                </td>
                                <td>
                                    <?php
                                    echo ThaiDate::dbToBuddhist($value['dofsurgery']);
                                    ?>
                                </td>
                                <td style="text-align: right;">
                                    <?php
                                    echo $value['visit'];
                                    ?>
                                </td>
                                <td>
                                    <?php
                                    echo $value['typeofsurgery'];
                                    ?>
                                </td>
                                <td>
                                    <?php
                                    echo $value['TumorSite'];
                                    ?>
                                </td>
                                <td style="text-align: right;">
                                    <?php
                                    echo $value['stage'];
                                    ?>
                                </td>
                            </tr>
                            <?php
                                }
                            }
                            ?>
                        </tbody>
                    </table>

                </div>
                <div class="panel-footer">
                    <div class="row">
                        <div class="col col-xs-4">จำนวนข้อมูล <?php echo count($listSP); ?>
                        </div>
                        <div class="col col-xs-8">
                            <!--
                            <ul class="pagination hidden-xs pull-right">
                                <li><a href="#">1</a></li>
                                <li><a href="#">2</a></li>
                                <li><a href="#">3</a></li>
                                <li><a href="#">4</a></li>
                                <li><a href="#">5</a></li>
                            </ul>
                            <ul class="pagination visible-xs pull-right">
                                <li><a href="#">«</a></li>
                                <li><a href="#">»</a></li>
                            </ul>
                            -->
                        </div>
                    </div>
                </div>
            </div>

        </div>
</div>
<?php
# ตัวเลขของ Advance Stage
if( strlen($request->get('staging'))==0 ){
    $txtEarlyStage = Html::a(number_format($sumEarlyStage)
            ,NULL
            ,[
                'style' => 'cursor:pointer;',
                'id' => 'sumListbyOnlyEarlyStage',
                'staging' => 'Early',
                'startDate' => $request->get('startDate'),
                'endDate' => $request->get('endDate'),
                'selectHosp' => $request->get('selectHosp'),
            ]);
    $txtAdvanceStage = Html::a(number_format($sumAdvanceStage)
            ,NULL
            ,[
                'style' => 'cursor:pointer;',
                'id' => 'sumListbyOnlyAdvanceStage',
                'staging' => 'Advancd',
                'startDate' => $request->get('startDate'),
                'endDate' => $request->get('endDate'),
                'selectHosp' => $request->get('selectHosp'),
            ]);
    $txtWaitingStage = Html::a(number_format($sumWaitingStage)
            ,NULL
            ,[
                'style' => 'cursor:pointer;',
                'id' => 'sumListbyOnlyWaitingStage',
                'staging' => 'Waiting',
                'startDate' => $request->get('startDate'),
                'endDate' => $request->get('endDate'),
                'selectHosp' => $request->get('selectHosp'),
            ]);
?>
<script lang="javascript">
    document.getElementById('hospitaltext').innerHTML='<?php echo $hsitecode.': '.$hospital['name']; ?>';
    document.getElementById('hospitaltextnormal').innerHTML='<?php echo $hospital['name']; ?>';
    document.getElementById('hospitalpatientcount').innerHTML='<?php echo count($listSP); ?>';
    document.getElementById('earlystagenumber').innerHTML='<?php echo $txtEarlyStage; ?>';
    document.getElementById('advancestagenumber').innerHTML='<?php echo $txtAdvanceStage; ?>';
    document.getElementById('waitingstagenumber').innerHTML='<?php echo $txtWaitingStage; ?>';
</script>
<?php
}

?>
