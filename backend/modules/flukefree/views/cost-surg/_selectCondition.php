<?php

use yii\jui\DatePicker;
use yii\helpers\Url;
use backend\modules\flukefree\classes\Hospital;

$hospitalSurg = Hospital::getHospitalSurgery();

?>
<div class="row" style="text-align:left; margin-left: 10px;">
    
    <!--
    <div class="btn-group" style="width: 200px;">
        <button type="button" class="btn btn-link" style="text-align: left; font-size: 14px; color: #000000;">เลือกโรงพยาบาล</button>
        <button type="button" class="btn btn-link dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="font-size: 14px; color: #000000;">
            <span class="caret" ></span>
            <span class="sr-only">Toggle Dropdown</span>
        </button>
        <ul class="dropdown-menu">
            <li><a href="/cpay" style="text-align: left; font-size: 14px; color: #000000;">13777: โรงพยาบาลศรีนครินทร์ มหาวิทยาลัยขอนแก่น</a></li>
            <li><a href="/cpay/y60" style="text-align: left; font-size: 14px; color: #000000;">10669: โรงพยาบาลสรรพสิทธิประสงค์</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="/cpay" style="text-align: left; font-size: 14px; color: #000000;">10670: โรงพยาบาลขอนแก่น</a></li>
        </ul>
    </div>
    -->
    
    <div class="form-group" style="width: 300px;">
        <select class="form-control" id="selectHosp" required>
            <?php
            if( count($hospitalSurg)>0 ){
                foreach ($hospitalSurg as $key => $value){
                    
            ?>
            <option value="<?php echo $value['sitecode']; ?>" <?php if($hsitecode==$value['sitecode'] ) { echo 'selected="selected"';} ?>><?php echo $value['sitecode'].': '.$value['hospital']; ?></option>
            <?php
                }
            }
            ?>
        </select>
    </div>
    
    <div class="form-group" style="width: 300px;">
        <label for="inputStartDate" class="col-sm-3 col-md-3 control-label">เริ่มวันที่</label>
        <div class="col-sm-9 col-md-9">
        <?php
        if(strlen($dfUSFinding['inputStartDate'])==0){
            $dfUSFinding['inputStartDate']='09/02/2013';
        }
        if(strlen($dfUSFinding['inputEndDate'])==0){
            // $endDate = \DateTime::createFromFormat("d/m/Y", $_GET['endDate']);
            // Yii::$app->formatter->asDate('now', 'php:d/m/Y')
            $dfUSFinding['inputEndDate'] = Date('m/d/Y'); //Yii::$app->formatter->asDate('now', 'php:d/m/Y');
        }
        echo DatePicker::widget([
            'id' => 'inputStartDate',
            'name'  => 'inputStartDate',
            'language' => 'th',
            'dateFormat' => 'dd/MM/yyyy',
            'value' => Yii::$app->formatter->asDate($dfUSFinding['inputStartDate'], 'php:d/m/Y'),
            'options' => [
                'class' => 'form-control',
            ],
            'clientOptions' => [ 
                'defaultDate' => Yii::$app->formatter->asDate('now', 'php:d/m/Y'),
                'minDate' => '09/02/2013',
                'maxDate' => Yii::$app->formatter->asDate('now', 'php:d/m/Y'),
            ],
        ]);

        ?>
        </div>
    </div>
    
    <div class="form-group" style="width: 300px;">
        <label for="inputEndDate" class="col-sm-3 col-md-3 control-label">ถึงวันที่</label>
        <div class="col-sm-9 col-md-9">
        <?php
        echo DatePicker::widget([
            'id' => 'inputEndDate',
            'name'  => 'inputEndDate',
            'language' => 'th',
            'dateFormat' => 'dd/MM/yyyy',
            'value' => Yii::$app->formatter->asDate($dfUSFinding['inputEndDate'], 'php:d/m/Y'),
            'options' => [
                'class' => 'form-control',
            ],
            'clientOptions' => [ 
                'defaultDate' => Yii::$app->formatter->asDate('now', 'php:d/m/Y'),
                'minDate' => '09/02/2013',
                'maxDate' => Yii::$app->formatter->asDate('now', 'php:d/m/Y'),
            ],
        ]);

        ?>
        </div>
    </div>
    
    
    <div class="form-group" style="text-align: center; width: 300px;">
        <p><span style="font-size: 10px;">&nbsp;</span></p>
        <?php
        echo yii\helpers\Html::button('ค้น'
                ,[
                    'id' => 'btnChangeCondition',
                    'class' => 'btn btn-primary',
                    'style' => 'width: 200px; text-align:center;',
                ] );
        ?>
    </div>
</div>

<?php

$this->registerJs('
    
    $(document).on("click", "*[id^=btnChangeCondition]", function() {
    
        $("#listtable").html(\'<div class="row text-center"><i class="fa fa-spinner fa-spin fa-3x" style="margin:50px 0;"></i></div>\');
//        var listPatientTopPosition = jQuery("#listtable").offset().top;
//        jQuery("html, body").animate({scrollTop:listPatientTopPosition}, "slow");
    
        var startDate = $("#inputStartDate").val();
        var endDate = $("#inputEndDate").val();
        var selectHosp = $("#selectHosp").val();
        //alert(startDate+endDate+selectHosp);
        
        // Ajax 
        $.ajax({
            type    : "GET",
            url     : "'.Url::to('report-view-search').'",
            data    : {
                startDate: startDate,
                endDate: endDate,
                selectHosp: selectHosp,
            },
            success  : function(response) {
                $("#listtable").html(response);
                //exportToExcel();
//                var listPatientTopPosition = jQuery("#listtable").offset().top;
//                jQuery("html, body").animate({scrollTop:listPatientTopPosition}, "slow");
            },
            error : function(){
                $("#listtable").html("");
            }
        });
    });
    
    $(document).on("click", "*[id^=sumListbyOnly]", function() {
    
        $("#listtable").html(\'<div class="row text-center"><i class="fa fa-spinner fa-spin fa-3x" style="margin:50px 0;"></i></div>\');
//        var listPatientTopPosition = jQuery("#listtable").offset().top;
//        jQuery("html, body").animate({scrollTop:listPatientTopPosition}, "slow");
    
        var startDate = $("#inputStartDate").val();
        var endDate = $("#inputEndDate").val();
        var selectHosp = $("#selectHosp").val();
        var staging = $(this).attr("staging");
        //alert(startDate+endDate+selectHosp);
        
        // Ajax 
        $.ajax({
            type    : "GET",
            url     : "'.Url::to('report-view-search').'",
            data    : {
                startDate: startDate,
                endDate: endDate,
                selectHosp: selectHosp,
                staging: staging,
            },
            success  : function(response) {
                $("#listtable").html(response);
                //exportToExcel();
//                var listPatientTopPosition = jQuery("#listtable").offset().top;
//                jQuery("html, body").animate({scrollTop:listPatientTopPosition}, "slow");
            },
            error : function(){
                $("#listtable").html("");
            }
        });
    });
');


?>
