<?php

use Yii;
use backend\modules\flukefree\classes\Hospital;
use backend\modules\flukefree\classes\ThaiDate;

$request = Yii::$app->request;
        

# style sheet
$this->registerCss("
.panel-table .panel-body{
  padding:0;
}

.panel-table .panel-body .table-bordered{
  border-style: none;
  margin:0;
}

.panel-table .panel-body .table-bordered > thead > tr > th:first-of-type {
    text-align:center;
    width: 100px;
}

.panel-table .panel-body .table-bordered > thead > tr > th:last-of-type,
.panel-table .panel-body .table-bordered > tbody > tr > td:last-of-type {
  border-right: 0px;
}

.panel-table .panel-body .table-bordered > thead > tr > th:first-of-type,
.panel-table .panel-body .table-bordered > tbody > tr > td:first-of-type {
  border-left: 0px;
}

.panel-table .panel-body .table-bordered > tbody > tr:first-of-type > td{
  border-bottom: 0px;
}

.panel-table .panel-body .table-bordered > thead > tr:first-of-type > th{
  border-top: 0px;
}

.panel-table .panel-footer .pagination{
  margin:0; 
}

/*
used to vertically center elements, may need modification if you're not using default sizes.
*/
.panel-table .panel-footer .col{
 line-height: 34px;
 height: 34px;
}

.panel-table .panel-heading .col h3{
 line-height: 30px;
 height: 30px;
}

.panel-table .panel-body .table-bordered > tbody > tr > td{
  line-height: 34px;
}



");


if( 0 ){
    echo "<pre align='left'>";
    print_r($listVisit);
    echo "</pre>";
}

?>
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet' type='text/css'>

<div class="container">
    <div class="row">
    
    <p></p>
        <p>
            <h2>
                <b>
                    <?php 
                    echo str_replace('xstagex','<br />Staging: ',str_replace('xtumorsitex','<br />Tumor Site: ',$request->get('detail'))); 
                    ?>
                </b>
            </h2>
        </p>
    <p><h3><b>แสดงประวัติการมาเข้ารักษาทั้งหมด ลำดับตามวันที่เข้ารักษา</b></h3></p>
    <p>ทั้งหมด <label style="color: blue;"><?php echo count($listVisit); ?></label> ครั้ง</p>
    <p> </p><p> </p>
    
        <div class="col-md-10 col-md-offset-1">

            <div class="panel panel-default panel-table">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col col-xs-6">
                            <h3 class="panel-title">ประวัติ</h3>
                        </div>
                        <div class="col col-xs-6 text-right">
                            <!-- <button type="button" class="btn btn-sm btn-primary btn-create">Create New</button> -->
                        </div>
                    </div>
                </div>
                <div class="panel-body">
                    <table class="table table-striped table-bordered table-list">
                        <thead>
                            <tr>
                                <th><em class="fa fa-cog"></em></th>
                                <th class="hidden-xs">ลำดับ</th>
                                <th>วันที่เข้ารับการรักษา</th>
                                <th>หัตถการที่ได้รับ</th>
                                <th>โรงพยาบาลที่รักษา</th>
                            </tr> 
                        </thead>
                        <tbody>
                            <?php
                            $sitecode = Yii::$app->user->identity->userProfile->sitecode;
                            $irow = 0;
                            if( count($listVisit)>0 ){
                                foreach ($listVisit as $key => $value){
                                    if( $sitecode==$value['hsitecode'] ){
                                        $canOpen = True;
                                        $canView = 'class="glyphicon glyphicon-eye-open" style="font-size:16px;color:blue"';
                                        $titleMessage = 'สามารถคลิ๊กเพื่อดูข้อมูลที่บันทึกเข้ามา';
                                    }else{
                                        $canOpen = False;
                                        $canView = 'class="glyphicon glyphicon-eye-close" style="font-size:16px;color:red"';
                                        $titleMessage = 'ท่านไม่มีสิทธิ์เข้าดูข้อมูลที่บันทึกเข้ามา';
                                    }
                                    
                                    if( count($hospitalList)==0){
                                        $hospital = Hospital::getHospitalDetail($value['hsitecode']);
                                        $hospitalList[$value['hsitecode']] = $hospital['name'];
                                    }else if(strlen($hospitalList[$value['hsitecode']])==0){
                                        $hospital = Hospital::getHospitalDetail($value['hsitecode']);
                                        $hospitalList[$value['hsitecode']] = $hospital['name'];
                                    }
                            ?>
                            <tr>
                                <td align="center">
                                    <!--
                                    <a class="btn btn-default"><em class="fa fa-pencil"></em></a>
                                    <a class="btn btn-danger"><em class="fa fa-trash"></em></a>
                                    -->
                                    <?php
                                    if( $canOpen ){
                                        echo yii\helpers\Html::a('<span '.$canView.'></span>'
                                                , \yii\helpers\Url::to(['/inputdata/step4'
                                                    ,'comp_id_target'=>'1437725343023632100'
                                                    ,'ezf_id'=>'1451381257025574200'
                                                    ,'target'=> base64_encode($value['target'])
                                                    ,'dataid'=> $value['id']
                                                    ])
                                                , [
                                                    'class' => ''
                                                    , 'id' => 'openForm'
                                                    , 'style' => 'cursor:pointer;'
                                                    , 'target' => '_blank'
                                                    , 'title' => $titleMessage
                                                    , 'detail' => $value['typeofsurgery'].' xtumorsitex '.$value['TumorSite'].' xstagex '.$value['stage']
                                                ]

                                            );
                                    }else{
                                        echo '<span '.$canView.'></span>';
                                    }
                                    ?>
                                </td>
                                <td class="hidden-xs">
                                    <?php
                                    echo count($listVisit)-$irow;
                                    ?>
                                </td>
                                <td>
                                    <?php
                                    echo ThaiDate::dbToBuddhist($value['f3v4dvisit']);
                                    ?>
                                </td>
                                <td>
                                    <?php
                                    echo $value['typeofsurgery'];
                                    ?>
                                </td>
                                <td style="text-align: left;">
                                    <?php
                                    echo $value['hsitecode'];
                                    echo ': ';
                                    echo $hospitalList[$value['hsitecode']];
                                    ?>
                                </td>
                            </tr>
                            <?php
                                    $irow++;
                                }
                            }
                            ?>
                        </tbody>
                    </table>

                </div>
              <div class="panel-footer">
                <div class="row">
                  <div class="col col-xs-4"><!-- Page 1 of 5 -->
                  </div>
                  <div class="col col-xs-8">
                      <!--
                    <ul class="pagination hidden-xs pull-right">
                      <li><a href="#">1</a></li>
                      <li><a href="#">2</a></li>
                      <li><a href="#">3</a></li>
                      <li><a href="#">4</a></li>
                      <li><a href="#">5</a></li>
                    </ul>
                    <ul class="pagination visible-xs pull-right">
                        <li><a href="#">«</a></li>
                        <li><a href="#">»</a></li>
                    </ul>
                      -->
                  </div>
                </div>
              </div>
            </div>

</div></div></div>
