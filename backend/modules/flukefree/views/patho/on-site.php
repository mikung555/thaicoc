<?php
use yii\grid\GridView;
use backend\modules\flukefree\classes\ThaiDate;

if( 0 ){
    echo "<pre align='left'>";
    //print_r($_COOKIE);
    var_dump($provider);
    echo "</pre>";
}

$models = $provider->getModels();


echo GridView::widget([
    'dataProvider' => $provider,
    'tableOptions' => [
        'class' => 'table table-striped table-bordered'
        ],
    'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => '<font style="color: #d2d6de;font-style: italic;">Null</font>'],
    'columns' => [
        [
            'class' => 'yii\grid\SerialColumn', // <-- here
            // you may configure additional properties here
        ],
        
        // Full code from site
        [
            'attribute'=>'hptcode',
            'format' => 'text',
            'label'=>'SiteID PID',
            'headerOptions' => ['class' => 'text-center'],
            'contentOptions' => ['class' => 'text-center'],
            'value' => function($models){
                $txt = $models['hsitecode'].$models['hptcode'];
                return $txt;
            }
        ],
        [
            'attribute'=>'hncode',
            'format' => 'raw',
            'label'=>'HN',
            'headerOptions' => ['style' => 'text-align:left; min-width: 100px; max-width: 100px;'],
            'contentOptions' => ['style' => 'text-align:left; min-width: 100px; max-width: 100px;'],
            'value' => function($models){
                return $models['hncode'];
            }
        ],
        [
            'attribute'=>'name',
            'format' => 'raw',
            'label'=>'ชื่อ นามสกุล',
            'headerOptions' => ['style' => 'text-align:left; min-width: 100px; max-width: 100px;'],
            'contentOptions' => ['style' => 'text-align:left; min-width: 100px; max-width: 100px;'],
            'value' => function($models){
                $txt = $models['title'].$models['name'].' '.$models['surname'];
                return $txt;
            }
        ],
        [
            'attribute'=>'f4v1',
            'format' => 'html',
            'label'=>'Diagnosis',
            'headerOptions' => ['style' => 'text-align:center; min-width: 100px; max-width: 100px;'],
            'contentOptions' => ['style' => 'text-align:center; min-width: 100px; max-width: 100px;'],
            'value' => function($models){
                $sitecode = Yii::$app->user->identity->userProfile->sitecode;
                if( $sitecode == $models['hsitecode'] ){
                    $dataUrl = \yii\helpers\Url::to([
                            '/inputdata/redirect-page',
                            'dataid' => $models['id'] ,
                            'ezf_id' => '1452061550097822200',
                            'rurl' => base64_encode(Yii::$app->request->url),
                        ]);
                    $txtUrl = yii\helpers\Html::a(
                            ThaiDate::dbToBuddhist($models['f4v1']),
                            $dataUrl ,
                            [
                                'id' => 'openForm',
                                'target' => '_blank',
                                'style' => 'cursor:pointer;',
                            ]);
                }else{
                    $txtUrl = ThaiDate::dbToBuddhist($models['f4v1']);
                }
                return $txtUrl;
            }
        ],
        
        [
            'attribute'=>'rstat',
            'format' => 'raw',
            'label'=>'RSTAT',
            'headerOptions' => ['style' => 'text-align:left; min-width: 100px; max-width: 100px;'],
            'contentOptions' => ['style' => 'text-align:left; min-width: 100px; max-width: 100px;'],
            'value' => function($models){
                if( $models['title']=='3' ){
                    $txt = 'ข้อมูลนี้ได้ลบไปแล้ว';
                }else if( $models['rstat']=='2' ){
                    $txt = 'Submited';
                }else if( $models['rstat']=='1' ){
                    $txt = 'Save Draft';
                }else if( $models['rstat']=='0' ){
                    $txt = 'New Record';
                }else{
                    $txt = 'Unknow';
                }
                return $txt;
            }
        ],
        'create_date:datetime',
    ],
]);


        
?>