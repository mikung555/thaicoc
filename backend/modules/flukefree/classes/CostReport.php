<?php
namespace backend\modules\flukefree\classes;

use Yii;

/**
 * Description of CostReport
 *
 * @author chaiwat
 */
class CostReport {
    //put your code here
    public static function listFromSite($hsitecode, $startDate, $endDate, $staging) {
        if(strlen($hsitecode)==0){
            $hsitecode = '13777';
        }
        if(strlen($startDate)==0){
            $startDate = '2013-02-09';
        }else{
            //$startDate = '2013-02-09';
        }
        if(strlen($endDate)==0){
            $endDate = Date('Y-m-d');
        }else{
            //$endDate = Date('Y-m-d');
        }
        $sql ="select n.ptid, n.hsitecode, n.hptcode, n.hncode as 'HN', n.title, n.name, n.surname
, n.dofsurgery
, count(distinct if(surg.rstat not in (0,3),surg.id,null)) as visit 
, n.typeofsurgery, n.TumorSite, n.stage
, n.f3v4dvisit
, n.liver_resec ,n.hilar, n.bypass, n.biopsy, n.whipple, n.f4v4
from
	(select n.ptid,n.hsitecode,n.hptcode,n.hncode,n.title,n.name,n.surname 
	,GROUP_CONCAT(n.f3v4dvisit) as f3v4dvisit
	,case 1 when max(n.liver_resec) is not null then max(n.liver_resec)
	when max(n.hilar) is not null then max(n.hilar)
	when max(n.whipple) is not null then max(n.whipple)
	when max(n.bypass) is not null then max(n.bypass)
	when max(n.biopsy) is not null then max(n.biopsy)
	end as dofsurgery
	,case 1 when max(n.liver_resec) is not null then 'Liver Resection'
	when max(n.hilar) is not null then 'Hilar resection'
	when max(n.whipple) is not null then 'Whipple or PPPD or PD'
	when max(n.bypass) is not null then 'Bypass'
	when max(n.biopsy) is not null then 'Biopsy'
	end as typeofsurgery
	,case 1 when max(n.f4v4)='1' then 'Intra'
	when max(n.f4v4)='2' then 'Perihilar'
	when max(n.f4v4)='3' then 'Distal'
	end as TumorSite
	,if(n.stage is null,'ตอบมาไม่สมบูรณ์',n.stage) as stage
	,max(n.liver_resec) as liver_resec
	,max(n.hilar) as hilar
	,max(n.bypass) as bypass
	,max(n.biopsy) as biopsy
	,max(n.whipple) as whipple
	,GROUP_CONCAT(n.f4v4) as f4v4
	from
	(select
	reg.ptid,reg.hsitecode,reg.hncode,reg.hptcode,reg.title,reg.name,reg.surname
	,surg.f3v4dvisit
	,case 1 when surg.f3v5a1b1='1' then surg.f3v5a1b1dtreat
	end as liver_resec
	,case 1 when surg.f3v5a1b2='1' then surg.f3v5a1b2dtreat
	end as hilar
	,case 1 when surg.f3v5a1b3='1' then surg.f3v5a1b3dtreat
	end as bypass
	,case 1 when surg.f3v5a1b4='1' then surg.f3v5a1b4dtreat
	end as biopsy
	,case 1 when surg.f3v5a1b6='1' then surg.f3v5a1b6dtreat
	end as whipple
	,patho.f4v4
	,case 1 when patho.f4v4='1' then
		case 1 when patho.f4v8a4='0' then '0'
		when patho.f4v8a4='1' then 'I'
		when patho.f4v8a4='2' then 'II'
		when patho.f4v8a4='3' then 'III'
		when patho.f4v8a4='4' then 'IVA'
		when patho.f4v8a4='5' then 'IVB'
		when patho.f4v8a4='6' then 'UN'
		end
	when patho.f4v4='2' then
		case 1 when patho.f4v8a5='0' then '0'
		when patho.f4v8a5='1' then 'I'
		when patho.f4v8a5='2' then 'II'
		when patho.f4v8a5='3' then 'IIIA'
		when patho.f4v8a5='4' then 'IIIB'
		when patho.f4v8a5='5' then 'IVA'
		when patho.f4v8a5='6' then 'IVB'
		when patho.f4v8a5='7' then 'UN'
		end
	when patho.f4v4='3' then
		case 1 when patho.f4v8a6='0' then '0'
		when patho.f4v8a6='1' then 'I'
		when patho.f4v8a6='2' then 'II'
		when patho.f4v8a6='3' then 'IIIA'
		when patho.f4v8a6='4' then 'IIIB'
		when patho.f4v8a6='5' then 'IVA'
		when patho.f4v8a6='6' then 'IVB'
		when patho.f4v8a6='7' then 'UN'
		end
	end as stage
	from tb_data_1 reg
	inner join tb_data_7 surg on surg.ptid=reg.ptid
	inner join tb_data_8 patho on patho.ptid=reg.ptid
	where 
	reg.hsitecode=:hsitecode
	and reg.rstat not in (0,3)
	and (surg.f3v5a1b1='1' or surg.f3v5a1b2='1' or surg.f3v5a1b3='1' or surg.f3v5a1b4='1' or surg.f3v5a1b6='1')
	and (patho.f4v4='1' or patho.f4v4='2' or patho.f4v4='3')
	order by reg.hsitecode,reg.hptcode ) n
	group by n.ptid
	order by dofsurgery desc) n
inner join tb_data_7 surg on surg.ptid=n.ptid
where surg.rstat not in (0,3)
and n.dofsurgery between :startDate and :endDate ";
        if( strlen($staging)>0 ){
            //$earlyStage = array('0','I','II');
            //$advanceStage = array('III','IIIA','IIIB','IVA','IVB','UN');
            if($staging=='Early'){
                $sql.= "and n.stage in ('0','I','II') ";
            }else if($staging=='Advancd'){
                $sql.= "and n.stage in ('III','IIIA','IIIB','IVA','IVB','UN') ";
            }else if($staging=='Waiting'){
                $sql.= "and n.stage not in ('0','I','II','III','IIIA','IIIB','IVA','IVB','UN') ";
            }
        }else{
            //
        }
        $sql.="
group by n.ptid
order by n.dofsurgery desc

";
        $newsql = Yii::$app->db->createCommand($sql
                ,[
                    ':hsitecode'=>$hsitecode,
                    ':startDate'=>$startDate,
                    ':endDate'=>$endDate,
                ])->rawSql;
        //echo $newsql;
        return Yii::$app->db->createCommand($sql
                ,[
                    ':hsitecode'=>$hsitecode,
                    ':startDate'=>$startDate,
                    ':endDate'=>$endDate,
                ])->queryAll();
    }
    
    public static function listOfVisit($ptid){
        $sql = "select * ";
        $sql.= ",case 1 when f3v5a1b1='1' then 'Liver Resection'
	when f3v5a1b2='1' then 'Hilar resection'
	when f3v5a1b6='1' then 'Whipple or PPPD or PD'
	when f3v5a1b3='1' then 'Bypass'
	when f3v5a1b4='1' then 'Biopsy'
        when f3v5a1b5='1' then 'Needle biopsy'
        when f3v5a2='1' then 'Chemotherapy'
        when f3v5a3='1' then 'PTBD'
        when f3v5a4='1' then 'Endoscopic Stent'
        when f3v5a5='1' then 'Medication Treatment'
        when f3v6a1='1' then 'Best supportive Treatment'
        when f3v4a1='1' then 'OPD'
	end as typeofsurgery ";
        $sql.= " from tb_data_7 where ptid=:ptid and rstat not in (0,3) order by f3v4dvisit desc ";
        
        return Yii::$app->db->createCommand($sql
                ,[
                    ':ptid'=>$ptid
                ])->queryAll();
    }
}
