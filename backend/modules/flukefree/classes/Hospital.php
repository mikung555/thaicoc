<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace backend\modules\flukefree\classes;

/**
 * Description of QueryCCA02
 *
 * @author chaiwat
 */
use Yii;

class Hospital {
    //put your code here
    public static function getHospitalDetail($hcode)
    {
        if( strlen($hcode)>0 ){
            $sql = "select * ";
            $sql.= "from all_hospital_thai ";
            $sql.= "where hcode=:hcode ";
            //echo $sql;
            return Yii::$app->db->createCommand($sql,[':hcode'=>$hcode])->queryOne();
        }
    }
    
    public static function getHospitalSurgery()
    {
        if( TRUE ){
            $sql = "select * ";
            $sql.= "from project84_zone_p3 ";
            $sql.= "order by zone, sitecode ";
            //echo $sql;
            return Yii::$app->db->createCommand($sql)->queryAll();
        }
    }
    
    
}