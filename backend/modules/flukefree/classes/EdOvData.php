<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace backend\modules\flukefree\classes;

use Yii;
use yii\data\SqlDataProvider;

/**
 * Description of EdOvData
 *
 * @author chaiwat
 */
class EdOvData {
    //put your code here
    function getList60(){
        
        $query = 'select code, hcode, name, zone_code, province, provincecode, amphur, amphurcode, tambon, tamboncode ';
        $query.= 'from all_hospital_thai where note="1" ';
        #$query.= 'order by provincecode, amphurcode ';
        
        $count = Yii::$app->db->createCommand('
            SELECT COUNT(*) FROM all_hospital_thai where note="1"
        ')->queryScalar();

        $provider = new SqlDataProvider([
            'sql' => $query,
            //'params' => [':hcode' => $hcode],
            'totalCount' => $count,
            'pagination' => [
                'pageSize' => 20,
            ],
            'sort' => [
                'defaultOrder'=>['hcode'=> SORT_DESC],
                'attributes' => [
                    'hcode',
                    'zone_code',
                    'province' => [
                        'asc' => ['zone_code' => SORT_ASC, 'province' => SORT_ASC],
                        'desc' => ['zone_code' => SORT_DESC, 'province' => SORT_DESC],
                        'default' => SORT_DESC,
                        'label' => 'จังหวัด',
                        ],
                    'amphur'=> [
                        'asc' => ['zone_code' => SORT_ASC, 'province' => SORT_ASC, 'amphur' => SORT_ASC],
                        'desc' => ['zone_code' => SORT_DESC, 'province' => SORT_DESC, 'amphur' => SORT_DESC],
                        'default' => SORT_DESC,
                        'label' => 'อำเภอ',
                        ],
                ],
            ],
        ]);
        return $provider;
    }
    
    function getHospitalRelate($pvcode,$ampcode,$tmbcode){
        $param = [
            ':provincecode' => $pvcode,
            ':amphurcode' => $ampcode,
            ':tamboncode' => $tmbcode,
        ];
        $condition = 'name5<>"โรงเรียน"
            and provincecode=:provincecode 
            and amphurcode=:amphurcode
            and tamboncode=:tamboncode';
        $count = Yii::$app->db->createCommand('
            SELECT COUNT(*) FROM all_hospital_thai 
            where 
        '.$condition,$param)->queryScalar();
        
        $query = 'select code, hcode, name, zone_code, province, provincecode, amphur, amphurcode, tambon, tamboncode ';
        $query.= 'from all_hospital_thai where '.$condition;
        $provider = new SqlDataProvider([
            'sql' => $query,
            'params' => $param,
            'totalCount' => $count,
            'pagination' => [
                'pageSize' => 20,
            ],
            'sort' => [
                'defaultOrder'=>['hcode'=> SORT_DESC],
                'attributes' => [
                    'hcode',
                    'zone_code',
                    'province' => [
                        'asc' => ['zone_code' => SORT_ASC, 'province' => SORT_ASC],
                        'desc' => ['zone_code' => SORT_DESC, 'province' => SORT_DESC],
                        'default' => SORT_DESC,
                        'label' => 'จังหวัด',
                        ],
                    'amphur'=> [
                        'asc' => ['zone_code' => SORT_ASC, 'province' => SORT_ASC, 'amphur' => SORT_ASC],
                        'desc' => ['zone_code' => SORT_DESC, 'province' => SORT_DESC, 'amphur' => SORT_DESC],
                        'default' => SORT_DESC,
                        'label' => 'อำเภอ',
                        ],
                ],
            ],
        ]);
        return $provider;
    }
    
    function getSummaryRegisterofHosp($hcode){
        if( strlen($hcode)>0 ){
            $sql = 'select count(distinct ptid) as allreg ';
            $sql.= ',count(distinct if(rstat=2,ptid,null)) as submited ';
            $sql.= ',count(distinct if(rstat=1,ptid,null)) as waiting ';
            $sql.= 'from tb_data_1 ';
            $sql.= 'where hsitecode=:hcode ';
            $sql.= 'and rstat in (1,2) ';
            $result = Yii::$app->db->createCommand($sql,[':hcode'=>$hcode])->queryOne();
            return $result;
        }
    }
    function getSummaryCCA01ofHosp($hcode){
        if( strlen($hcode)>0 ){
            $sql = 'select count(distinct ptid) as alldata ';
            $sql.= ',count(distinct if(rstat=2,ptid,null)) as submited ';
            $sql.= ',count(distinct if(rstat=1,ptid,null)) as waiting ';
            $sql.= 'from tb_data_2 ';
            $sql.= 'where hsitecode=:hcode ';
            $sql.= 'and rstat in (1,2) ';
            $result = Yii::$app->db->createCommand($sql,[':hcode'=>$hcode])->queryOne();
            return $result;
        }
    }
    function getSummaryCCA02ofHosp($hcode){
        if( strlen($hcode)>0 ){
            $sql = 'select count(distinct ptid) as alldata ';
            $sql.= ',count(distinct if(rstat=2,ptid,null)) as submited ';
            $sql.= ',count(distinct if(rstat=1,ptid,null)) as waiting ';
            $sql.= 'from tb_data_3 ';
            $sql.= 'where hsitecode=:hcode ';
            $sql.= 'and rstat in (1,2) ';
            $result = Yii::$app->db->createCommand($sql,[':hcode'=>$hcode])->queryOne();
            return $result;
        }
    }
    
    function getAmphurHospitalRelate($pvcode,$ampcode,$tmbcode){
        $param = [
            ':provincecode' => $pvcode,
            ':amphurcode' => $ampcode,
        ];
        $condition = 'name5<>"โรงเรียน"
            and hosp.provincecode=:provincecode 
            and hosp.amphurcode=:amphurcode ';
        $count = Yii::$app->db->createCommand('
            SELECT COUNT(distinct hosp.provincecode,hosp.amphurcode,hosp.tamboncode,hosp.hcode) 
            from tb_data_1 reg
            inner join all_hospital_thai hosp on hosp.hcode=reg.hsitecode
            where 
        '.$condition.'
            group by hosp.provincecode,hosp.amphurcode,hosp.tamboncode, hosp.hcode
                ',$param)->queryScalar();
        
        $query = 'select count(distinct reg.ptid) as alldata ';
        $query.= ',count(distinct if(reg.rstat=2,reg.ptid,null)) as submited ';
        $query.= ',count(distinct if(reg.rstat=1,reg.ptid,null)) as waiting ';
        $query.= ',hosp.provincecode, hosp.province, hosp.amphurcode, hosp.amphur, hosp.tamboncode, hosp.tambon ';
        $query.= ',hosp.hcode, hosp.name ';
        $query.= 'from tb_data_1 reg ';
        $query.= 'inner join all_hospital_thai hosp on hosp.hcode=reg.hsitecode ';
        $query.= 'where '.$condition;
        $query.= 'group by hosp.provincecode,hosp.amphurcode,hosp.tamboncode, hosp.hcode ';
        $provider = new SqlDataProvider([
            'sql' => $query,
            'params' => $param,
            'totalCount' => $count,
            'pagination' => [
                'pageSize' => 20,
            ],
            'sort' => [
                'defaultOrder'=>['tambon'=> SORT_DESC],
                'attributes' => [
                    'hcode',
                    'zone_code',
                    'province' => [
                        'asc' => ['zone_code' => SORT_ASC, 'province' => SORT_ASC],
                        'desc' => ['zone_code' => SORT_DESC, 'province' => SORT_DESC],
                        'default' => SORT_DESC,
                        'label' => 'จังหวัด',
                        ],
                    'amphur'=> [
                        'asc' => ['zone_code' => SORT_ASC, 'province' => SORT_ASC, 'amphur' => SORT_ASC],
                        'desc' => ['zone_code' => SORT_DESC, 'province' => SORT_DESC, 'amphur' => SORT_DESC],
                        'default' => SORT_DESC,
                        'label' => 'อำเภอ',
                        ],
                    'tambon'=> [
                        'asc' => ['zone_code' => SORT_ASC, 'province' => SORT_ASC, 'amphur' => SORT_ASC, 'tambon' => SORT_ASC, 'hcode' => SORT_ASC],
                        'desc' => ['zone_code' => SORT_DESC, 'province' => SORT_DESC, 'amphur' => SORT_DESC, 'tambon' => SORT_DESC, 'hcode' => SORT_DESC],
                        'default' => SORT_DESC,
                        'label' => 'อำเภอ',
                        ],
                ],
            ],
        ]);
        return $provider;
    }
    function getUserInHospitalRelate($hcode){
        if( strlen($hcode)>0 ){
            $sql = 'select distinct firstname, lastname ';
            $sql.= ',telephone ';
            $sql.= ',email ';
            $sql.= 'from user_profile ';
            $sql.= 'where sitecode=:hcode ';
            //$sql.= 'and rstat in (1,2) ';
            $result = Yii::$app->db->createCommand($sql,[':hcode'=>$hcode])->queryAll();
            if( count($result)>0 ){
                foreach($result as $kvauel){
                    $out['person'][$kvauel['user_id']]=$kvauel;
                    if( strlen(trim($kvauel['telephone']))>8 ){
                        if( strlen($out['txt']['contact'])>0 ){
                            $out['txt']['contact']=$out['txt']['contact'].'<br /> '.$kvauel['telephone'];
                        }else{
                            $out['txt']['contact']=$kvauel['telephone'];
                        }
                    }
                }
            }
            return $out;
        }
    }
}
