<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace backend\modules\flukefree\classes;
use Yii;
use yii\data\SqlDataProvider;
/**
 * Description of PathoData
 *
 * @author chaiwat
 */
class PathoData {
    //put your code here
    
    public static function getAllData($hcode){
        $query = 'SELECT reg.title,reg.name,reg.surname,reg.hncode,cca04.* ';
        $query.= 'FROM tb_data_8 cca04 ';
        $query.= 'inner join tb_data_1 reg on reg.ptid=cca04.ptid ';
        $query.= 'WHERE cca04.rstat not in (3) and cca04.hsitecode=:hcode';
        
        $count = Yii::$app->db->createCommand('
            SELECT COUNT(*) FROM tb_data_8 WHERE rstat not in (3) and hsitecode=:hcode
        ', [':hcode' => $hcode])->queryScalar();

        $provider = new SqlDataProvider([
            'sql' => $query,
            'params' => [':hcode' => $hcode],
            'totalCount' => $count,
            'pagination' => [
                'pageSize' => 20,
            ],
            'sort' => [
                'defaultOrder'=>['f4v1'=> SORT_DESC],
                'attributes' => [
                    'hsitecode',
                    'hptcode',
                    'name',
                    'hncode'=> [
                        'asc' => ['hsitecode' => SORT_ASC, 'hptcode' => SORT_ASC],
                        'desc' => ['hsitecode' => SORT_DESC, 'hptcode' => SORT_DESC],
                        'default' => SORT_DESC,
                        'label' => 'PatientID',
                        ],
                    'f4v1' => [
                        'default' => SORT_DESC,
                        'label' => 'Diagnosis',
                        ],
                    'created_date',
                ],
            ],
        ]);
//        $provider->setSort([
//                'defaultOrder' => [ 'hptcode '=>SORT_ASC ],
//           ]);

        // returns an array of data rows
        //$models = $provider->getModels();
        return $provider;
    }
}
