<?php

namespace app\modules\flukefree\controllers;

use Yii;
use backend\modules\flukefree\classes\CostReport;


class CostSurgController extends \yii\web\Controller
{
    public function actionIndex()
    {
        return $this->render('index');
    }
    
    
    public function actionReport()
    {
        // $userId = Yii::$app->user->id;
        // $sitecode = Yii::$app->user->identity->userProfile->sitecode;
        $request = Yii::$app->request;
        if( strlen($request->get('selectHosp'))==0 ){
            $selectHosp = '13777';
        }else{
            $selectHosp = $request->get('selectHosp');
        }
        
        $listSP = CostReport::listFromSite($selectHosp,$startDate,$endDate,$staging);
        return $this->render('report'
                ,[
                    'title' => 'รายงานจำนวนผู้ป่วยที่มีทั้งการผ่าตัด และผลพยาธิวิทยา',
                    'listSP' => $listSP,
                    'hsitecode' => $selectHosp,
                ]);
    }
    
    public function actionReportView()
    {
        // $userId = Yii::$app->user->id;
        // $sitecode = Yii::$app->user->identity->userProfile->sitecode;
        $request = Yii::$app->request;
        if(strlen($request->get('selectHosp'))==0){
            $selectHosp = '13777';
        }else{
            $selectHosp = $request->get('selectHosp');
        }
        $listSP = CostReport::listFromSite($selectHosp,$startDate,$endDate,$staging);
        return $this->renderAjax('report'
                ,[
                    'title' => 'รายงานจำนวนผู้ป่วยที่มีทั้งการผ่าตัด และผลพยาธิวิทยา',
                    'listSP' => $listSP,
                    'hsitecode' => $selectHosp,
                ]);
    }
    
    public function actionReportViewSearch()
    {
        // $userId = Yii::$app->user->id;
        // $sitecode = Yii::$app->user->identity->userProfile->sitecode;
        $request = Yii::$app->request;
        // converse date
        $startDate = \DateTime::createFromFormat("d/m/Y", $request->get('startDate')); // ::createFromFormat("d/m/Y  H:i:s", '31/01/2015');
        $endDate = \DateTime::createFromFormat("d/m/Y", $request->get('endDate'));
//        $startDate = $request->get('startDate');
//        $endDate = $request->get('endDate');
        $selectHosp = $request->get('selectHosp');
        $staging = $request->get('staging');
//        echo $staging;
        $listSP = CostReport::listFromSite($selectHosp,$startDate->format('Y-m-d'),$endDate->format('Y-m-d'),$staging);
        return $this->renderAjax('_listsurgpathotable'
                ,[
                    'title' => 'รายงานจำนวนผู้ป่วยที่มีทั้งการผ่าตัด และผลพยาธิวิทยา',
                    'listSP' => $listSP,
                    'hsitecode' => $selectHosp,
                ]);
    }
    
    
    
    public function actionViewListOfVisit()
    {
        $request = Yii::$app->request;
        $ptid = $request->get('target');
        $listVisit = CostReport::listOfVisit($ptid);
        return $this->renderAjax('_listsurgpathovisit'
                ,[
                    'title' => 'รายงานจำนวนผู้ป่วยที่มีทั้งการผ่าตัด และผลพยาธิวิทยา',
                    'listVisit' => $listVisit,
                ]);
    }
    
}
