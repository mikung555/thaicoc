<?php

namespace app\modules\flukefree\controllers;

use Yii;
use backend\modules\flukefree\classes\EdOvData;

class EdOvController extends \yii\web\Controller
{
    public function actionIndex()
    {
        return $this->render('index');
    }
    
    public function actionList60()
    {
        $l60 = EdOvData::getList60();
        return $this->render('list60',
                [
                    'l60' => $l60,
                ]);
    }

    public function actionList60View()
    {
        $l60 = EdOvData::getList60();
        return $this->renderAjax('list60',
                [
                    'l60' => $l60,
                ]);
    }
    
    public function actionList60HospitalRelate()
    {
        $request = Yii::$app->request;
        $l60 = EdOvData::getHospitalRelate($request->get('provincecode'), $request->get('amphurcode'), $request->get('tamboncode'));
        $l60gt = EdOvData::getAmphurHospitalRelate($request->get('provincecode'), $request->get('amphurcode'));
        return $this->renderAjax('_showhospitalrelate',
                [
                    'l60' => $l60,
                    'l60gt' => $l60gt,
                ]);
    }
    
    
}
