<?php

namespace app\modules\flukefree\controllers;
use Yii;
use backend\modules\flukefree\classes\PathoData;

class PathoController extends \yii\web\Controller
{
    public function actionIndex()
    {
        return $this->render('index');
    }
    
    public function actionOnSite(){
        $sitecode = Yii::$app->user->identity->userProfile->sitecode;
        $provider = PathoData::getAllData($sitecode);
        return $this->render('on-site'
                ,[
                    'provider' => $provider,
                 ]);
    }

}
