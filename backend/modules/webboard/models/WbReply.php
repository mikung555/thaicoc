<?php

namespace backend\modules\webboard\models;

use Yii;

/**
 * This is the model class for table "wb_reply".
 *
 * @property string $id
 * @property string $ques_id
 * @property string $wb_comment
 * @property string $create_by
 * @property string $create_at
 * @property string $update_at
 * @property integer $rstat
 * @property integer $bookmark
 */
class WbReply extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'wb_reply';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ques_id', 'wb_comment', 'create_by', 'create_at'], 'required'],
            [['ques_id', 'create_by', 'rstat', 'bookmark'], 'integer'],
            [['wb_comment'], 'string'],
            [['create_at', 'update_at'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'primary key',
            'ques_id' => 'Question ID',
            'wb_comment' => 'รายละเอียด',
            'create_by' => 'โดย',
            'create_at' => 'เวลาที่ตั้งกระทู้',
            'update_at' => 'เวลาที่ปรับปรุงล่าสุด',
            'rstat' => 'rstat',
            'bookmark' => 'Bookmark',
        ];
    }
}
