<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\webboard\models\WbQuestion */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="wb-question-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'wb_titile')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'wb_detail')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
