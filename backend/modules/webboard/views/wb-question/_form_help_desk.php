<?php
/**
 * Created by PhpStorm.
 * User: Kongvut Sangkla
 * Date: 10/3/2559
 * Time: 15:31
 * E-mail: kongvut@gmail.com
 */
?>

<?php
use yii\bootstrap\Html;
use \yii\bootstrap\ActiveForm;
?>
<div class="modal-content">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="ModalUser">Help Desk (ขอความช่วยเหลือกับผู้ดูแลระบบ)</h4>
    </div>

    <!-- Nav tabs -->
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active"><a href="#tab1" aria-controls="tab1" role="tab" data-toggle="tab"><i class="fa fa-twitch"></i> Line</a></li>
        <li role="presentation"><a href="#tab2" aria-controls="tab2" role="tab" data-toggle="tab"><i class="fa fa-phone"></i> ติดต่อผ่านโทรศัพท์</a></li>
        <li role="presentation"><a href="#tab3" aria-controls="tab3" role="tab" data-toggle="tab"><i class="fa fa-envelope-o"></i> ขอช่วยเหลือผ่านอีเมล</a></li>
    </ul>

    <!-- Tab panes -->
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="tab1">
            <div class="modal-body">
                <?php
                    $line = json_decode(Yii::$app->keyStorage->get('line_group'));
                    echo Html::a(Html::img('/../img/line/'.$line->img, ['class'=>'img-responsive', 'style'=>'margin: 0 auto;']), $line->url, ['target'=>'_blank']);
                ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">ปิด</button>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane" id="tab2">
            <div class="modal-body">

                <ol>
                    <?php if(Yii::$app->keyStorage->get('frontend.domain') == 'cascap.in.th') { ?>
                    <li>ใบกำกับงาน OV-CCA
                        <ul>
                            <li>ศุภัทร์ มณีวงศ์ (086-8682300)</li>
                            <li>ภาณุวัฒน์ 	ประทุมขำ (080-1841938)</li>
                        </ul>
                    </li>
                    <?php } ?>
                    <li>TCC Bot
                        <ul>
                            <li>คงวุฒิ แสงกล้า (087-5442559)</li>
                            <li>ภาณุวัฒน์ 	ประทุมขำ (080-1841938)</li>
                        </ul>
                    </li>
                    <li>ระบบสมัครสมาชิก
                        <ul>
                            <li>สันติ  ต๊อดแก้ว (091-8632115)</li>
                            <li>จารุวรรณ เถื่อนมั่น (083-1451504)</li>
                        </ul>
                    </li>
                    <li>ระบบบันทึกข้อมูล
                        <ul>
                            <li>คงวุฒิ แสงกล้า (087-5442559)</li>
                            <li>ชัยวัฒน์	ทะวะรุ่งเรือง (089-4178746)</li>
                            <li>ภาณุวัฒน์ 	ประทุมขำ (080-1841938)</li>
                        </ul>
                    </li>
                    <?php if(Yii::$app->keyStorage->get('frontend.domain') == 'cascap.in.th') { ?>
                    <li>การตรวจพยาธิใบไม้ตับ (OV-CCA)
                        <ul>
                            <li>ดร.กุลธิดา โกพลรัตน์ (089-2767916)</li>
                        </ul>
                    </li>
                    <li>ถามตอบข้อสงสัยเกี่ยวกับ ใบทำบัตร, ICF และ Ultrasound
                        <ul>
                            <li>...</li>
                        </ul>
                    </li>
                    <?php } ?>
                    <li>ข้อเสนอแนะ-ติชม
                        <ul>
                            <li>รศ.ดร.บัณฑิต 	ถิ่นคำรพ (085-0011123)</li>
                        </ul>
                    </li>
                </ol>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">ปิด</button>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane" id="tab3">
            <?php $form = ActiveForm::begin(['id'=>$model->formName(), 'action' => \yii\helpers\Url::to('/webboard/wb-question/help-desk')]); ?>
            <div class="modal-body">

                <div class="help-desk-form">

                    <div class="form-group">
                        <?= Html::label('ชื่อ-สกุล') ?>
                        <?= Html::textInput('fullname', Yii::$app->user->identity->userProfile->getFullName(), ['class'=>'form-control']); ?>
                    </div>
                    <div class="form-group">
                        <?= Html::label('E-mail') ?>
                        <?= Html::textInput('email', Yii::$app->user->identity->userProfile->email, ['class'=>'form-control', 'type'=>'email', 'required'=>true]); ?>
                    </div>
                    <div class="form-group">
                        <?= Html::label('เบอร์โทรศัพท์') ?>
                        <?= Html::textInput('phone', Yii::$app->user->identity->userProfile->telephone, ['class'=>'form-control',  'required'=>true]); ?>
                    </div>
                    <div class="form-group">
                        <?= Html::label('URL ที่รายงาน หรือขอความช่วยเหลือ') ?>
                        <?= Html::textInput('help_url', $helpurl, ['class'=>'form-control', 'readonly'=>true]); ?>
                    </div>

                    <?//= $form->field($model, 'wb_titile')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'wb_detail')->textarea(['rows' => 6])->label('รายละเอียดการขอความช่วยเหลือ') ?>

                    <?//= $form->field($model, 'create_by')->textInput(['maxlength' => true]) ?>

                    <?//= $form->field($model, 'create_at')->textInput() ?>

                    <?//= $form->field($model, 'update_at')->textInput() ?>

                    <?//= $form->field($model, 'rstat')->textInput() ?>

                    <?//= $form->field($model, 'hits')->textInput() ?>

                    <?//= $form->field($model, 'bookmark')->textInput() ?>



                </div>

            </div>
            <div class="modal-footer">
                <?= Html::submitButton('ส่งคำช่วยเหลือ', ['class' => 'btn btn-primary']) ?>
                <button type="button" class="btn btn-default" data-dismiss="modal">ยกเลิก</button>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>

</div>

<?php  $this->registerJs("
$('form#{$model->formName()}').on('beforeSubmit', function(e){
    $('#modal-user-panel .modal-footer').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');

    var \$form = $(this);
    var formData = new FormData($(this)[0]);
    $.ajax({
          url: \$form.attr('action'),
          type: 'POST',
          data: formData,
	  dataType: 'JSON',
	  //enctype: 'multipart/form-data',
	processData: false,  // tell jQuery not to process the data
	contentType: false,   // tell jQuery not to set contentType
          success: function (result) {
	    if(result.status == 'success'){
		". \common\lib\sdii\components\helpers\SDNoty::show('result.message', 'result.status') ."
		$('#modal-user-panel .modal-footer').html('<button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">ปิด</button>');
		$('#modal-user-panel .modal-body .help-desk-form').html('<div class=\"alert alert-success\" role=\"alert\"><h4>ทางผู้ดูแลได้รับข้อมูลที่ท่านแจ้งผ่านทางอีเมล์เรียบร้อยแล้ว...  <br>เจ้าหน้าที่จะดำเนินการตรวจสอบข้อมูล และดำเนินการแจ้งรายละเอียดให้ท่านทราบภายใน 48 ชั่วโมงครับ</h4></div>');
		if(result.action == 'create'){
		    //$(document).find('#modal-user-panel').modal('hide');
		} else if(result.action == 'update'){
		    $(document).find('#modal-user-panel').modal('hide');
		}
	    } else{
		". \common\lib\sdii\components\helpers\SDNoty::show('result.message', 'result.status') ."
	    }
          },
          error: function () {
	    console.log('server error');
          }
      });
    return false;
});

");?>

