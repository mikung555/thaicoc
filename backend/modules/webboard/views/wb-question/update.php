<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\webboard\models\WbQuestion */

$this->title = 'Update Wb Question: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Wb Questions', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="wb-question-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
