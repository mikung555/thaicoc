<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\webboard\models\WbQuestion */

$this->title = '';
$this->params['breadcrumbs'][] = ['label' => 'Wb Questions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->wb_titile;
?>
<div class="wb-question-view">

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            'wb_titile',
            'wb_detail:html',
            'create_by',
            'create_at',
            'update_at',
            //'rstat',
            //'bookmark',
        ],
    ]) ?>

</div>
