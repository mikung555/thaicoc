<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\webboard\models\WbReply */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="wb-reply-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'ques_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'wb_comment')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'create_by')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'create_at')->textInput() ?>

    <?= $form->field($model, 'update_at')->textInput() ?>

    <?= $form->field($model, 'rstat')->textInput() ?>

    <?= $form->field($model, 'bookmark')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
