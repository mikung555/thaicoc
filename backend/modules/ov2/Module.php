<?php

namespace backend\modules\ov2;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\modules\ov2\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
