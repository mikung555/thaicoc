<?php
namespace backend\modules\ov2\classes;

use Yii;
use yii\data\SqlDataProvider;
/**
 * OvccaQuery class file UTF-8
 * @author SDII <iencoded@gmail.com>
 * @copyright Copyright &copy; 2015 AppXQ
 * @license http://www.appxq.com/license/
 * @version 1.0.0 Date: 9 ก.พ. 2559 12:38:14
 * @link http://www.appxq.com/
 * @example 
 */
class OvccaQuery {
    public static function getPersonAllDecodeIn($key, $convert, $find, $hospcode='05147') {
	if($key!=''){
	    $str = "decode(unhex(person.address),sha2(:key,256)) AS address,
		    decode(unhex(person.cid),sha2(:key,256)) AS cid,
		    decode(unhex(person.hn),sha2(:key,256)) AS hn,
		    decode(unhex(person.pname),sha2(:key,256)) AS pname,
		    decode(unhex(person.fname),sha2(:key,256)) AS fname,
		    decode(unhex(person.lname),sha2(:key,256)) AS lname,";
	    
	    $fstr = 'decode(unhex(person.cid),sha2(:key,256))';
	    
	    if ($convert==1) {
		$str = "convert(decode(unhex(person.address),sha2(:key,256)) using tis620) AS address,
		    convert(decode(unhex(person.cid),sha2(:key,256)) using tis620) AS cid,
		    convert(decode(unhex(person.hn),sha2(:key,256)) using tis620) AS hn,
		    convert(decode(unhex(person.pname),sha2(:key,256)) using tis620) AS pname,
		    convert(decode(unhex(person.fname),sha2(:key,256)) using tis620) AS fname,
		    convert(decode(unhex(person.lname),sha2(:key,256)) using tis620) AS lname,";
		
		$fstr = 'convert(decode(unhex(person.cid),sha2(:key,256)) using tis620)';
	    }
	}  else {
	    $str = "person.address AS address,
		    person.cid AS cid,
		    person.hn AS hn,
		    person.pname AS pname,
		    person.fname AS fname,
		    person.lname AS lname,";
	    
	    $fstr = 'person.cid';
	}
	
	$sql = "SELECT person.khet, 
		    person.province, 
		    person.amphur, 
		    person.tambon, 
		    person.hospcode, 
		    person.hospname, 
		    person.person_id, 
		    person.house_id, 
		    $str
		    person.sex, 
		    person.nationality, 
		    person.education, 
		    person.type_area, 
		    person.religion, 
		    person.birthdate, 
		    floor(DATEDIFF(CURRENT_DATE, STR_TO_DATE(person.birthdate, '%Y-%m-%d'))/365) AS age,
		    person.village_id, 
		    person.village_code, 
		    person.village_name, 
		    person.pttype, 
		    person.pttype_begin_date, 
		    person.pttype_expire_date, 
		    person.pttype_hospmain, 
		    person.pttype_hospsub, 
		    person.marrystatus, 
		    person.death, 
		    person.cidlink,
		    person.death_date
	    FROM person
	    WHERE person.cidlink IN ($find) AND hospcode = :hospcode
	    ";
	
	return Yii::$app->dbbot->createCommand($sql, [':key'=>$key, ':hospcode'=>$hospcode])->queryAll();
	
    }
    
    public static function getPersonAllDecode($min, $max, $type, $village, $key, $hospcode='05147') {
	$str = "decode(unhex(person.address),sha2(:key,256)) AS address,
		    decode(unhex(person.cid),sha2(:key,256)) AS cid,
		    decode(unhex(person.hn),sha2(:key,256)) AS hn,
		    decode(unhex(person.pname),sha2(:key,256)) AS pname,
		    decode(unhex(person.fname),sha2(:key,256)) AS fname,
		    decode(unhex(person.lname),sha2(:key,256)) AS lname,";
	
	if (isset(Yii::$app->session['convert']) && Yii::$app->session['convert']==1) {
	    $str = "convert(decode(unhex(person.address),sha2(:key,256)) using tis620) AS address,
		    convert(decode(unhex(person.cid),sha2(:key,256)) using tis620) AS cid,
		    convert(decode(unhex(person.hn),sha2(:key,256)) using tis620) AS hn,
		    convert(decode(unhex(person.pname),sha2(:key,256)) using tis620) AS pname,
		    convert(decode(unhex(person.fname),sha2(:key,256)) using tis620) AS fname,
		    convert(decode(unhex(person.lname),sha2(:key,256)) using tis620) AS lname,";
	}
	
	$sql = "SELECT person.khet, 
		    person.province, 
		    person.amphur, 
		    person.tambon, 
		    person.hospcode, 
		    person.hospname, 
		    person.person_id, 
		    person.house_id, 
		    $str
		    person.sex, 
		    person.nationality, 
		    person.education, 
		    person.type_area, 
		    person.religion, 
		    person.birthdate, 
		    floor(DATEDIFF(CURRENT_DATE, STR_TO_DATE(person.birthdate, '%Y-%m-%d'))/365) AS age,
		    person.village_id, 
		    person.village_code, 
		    person.village_name, 
		    person.pttype, 
		    person.pttype_begin_date, 
		    person.pttype_expire_date, 
		    person.pttype_hospmain, 
		    person.pttype_hospsub, 
		    person.marrystatus, 
		    person.death, 
		    person.cidlink,
		    person.death_date
	    FROM person
	    WHERE floor(DATEDIFF(CURRENT_DATE, STR_TO_DATE(person.birthdate, '%Y-%m-%d'))/365) BETWEEN :min AND :max AND death = 'N' AND type_area IN ($type) AND hospcode = :hospcode AND village_code = :village_code
	    ";
	
	return Yii::$app->dbbot->createCommand($sql, [':key'=>$key, ':min'=>$min, ':max'=>$max, ':village_code'=>$village, ':hospcode'=>$hospcode])->queryAll();
    }
    
    public static function getPersonAll($min, $max, $type, $village, $hospcode='05147') {
	$sql = "SELECT person.khet, 
		    person.province, 
		    person.amphur, 
		    person.tambon, 
		    person.hospcode, 
		    person.hospname, 
		    person.person_id, 
		    person.house_id, 
		    person.address, 
		    person.cid, 
		    person.hn, 
		    person.pname, 
		    person.fname, 
		    person.lname, 
		    person.sex, 
		    person.nationality, 
		    person.education, 
		    person.type_area, 
		    person.religion, 
		    person.birthdate, 
		    floor(DATEDIFF(CURRENT_DATE, STR_TO_DATE(person.birthdate, '%Y-%m-%d'))/365) AS age,
		    person.village_id, 
		    person.village_code, 
		    person.village_name, 
		    person.pttype, 
		    person.pttype_begin_date, 
		    person.pttype_expire_date, 
		    person.pttype_hospmain, 
		    person.pttype_hospsub, 
		    person.marrystatus, 
		    person.death, 
		    person.cidlink,
		    person.death_date
	    FROM person
	    WHERE floor(DATEDIFF(CURRENT_DATE, STR_TO_DATE(person.birthdate, '%Y-%m-%d'))/365) BETWEEN :min AND :max AND death = 'N' AND type_area IN ($type) AND hospcode = :hospcode AND village_code = :village_code
	    ";
	
	return Yii::$app->dbbot->createCommand($sql, [':min'=>$min, ':max'=>$max, ':village_code'=>$village, ':hospcode'=>$hospcode])->queryAll();
    }
    
    public static function getPersonAll2($min, $max, $village, $hospcode='05147') {
	$sql = "SELECT tb_data_1.*
	    FROM tb_data_1
	    WHERE floor(DATEDIFF(CURRENT_DATE, STR_TO_DATE(v2, '%Y-%m-%d'))/365) BETWEEN :min AND :max AND hsitecode = :hospcode AND add1n6code = :village_code AND add1n5 REGEXP '[0-9]+'
	    ";
	
	return Yii::$app->db->createCommand($sql, [':min'=>$min, ':max'=>$max, ':village_code'=>$village, ':hospcode'=>$hospcode])->queryAll();
    }
    
    public static function getPersonAll3($min, $max, $village, $hospcode='05147') {
	$sql = "SELECT tb_data_1.*
	    FROM tb_data_1
	    WHERE floor(DATEDIFF(CURRENT_DATE, STR_TO_DATE(v2, '%Y-%m-%d'))/365) BETWEEN :min AND :max AND hsitecode = :hospcode AND CONCAT(add1n6code, add1n5) = :village_code AND add1n5 REGEXP '[0-9]+'
	    ";
	
	return Yii::$app->db->createCommand($sql, [':min'=>$min, ':max'=>$max, ':village_code'=>$village, ':hospcode'=>$hospcode])->queryAll();
    }
    
    public static function getVillagelist($min, $max, $type, $hospcode='05147') {
	$sql = "SELECT village_code, 
			village_name ,
			CONCAT(village_code, ' ', village_name, ' (', count(*), ')') AS text,
			count(*) AS num
		FROM person 
		WHERE floor(DATEDIFF(CURRENT_DATE, STR_TO_DATE(person.birthdate, '%Y-%m-%d'))/365) BETWEEN :min AND :max AND death = 'N' AND hospcode=:hospcode AND type_area IN ($type) 
		GROUP BY village_code
		HAVING count(*) > 10;";
	
	return Yii::$app->dbbot->createCommand($sql, [':min'=>$min, ':max'=>$max, ':hospcode'=>$hospcode])->queryAll();
    }
    
    public static function getVillagelist2($min, $max, $hospcode='05147') {
	$sql = "SELECT add1n6code AS add1n2, 
			CONCAT('ต.', const_district.DISTRICT_NAME, ' (', count(*), ')') AS text,
			count(*) AS num
		FROM tb_data_1 INNER JOIN const_district ON tb_data_1.add1n6code = const_district.DISTRICT_CODE
		WHERE floor(DATEDIFF(CURRENT_DATE, STR_TO_DATE(v2, '%Y-%m-%d'))/365) BETWEEN :min AND :max AND hsitecode=:hospcode AND add1n5 REGEXP '[0-9]+'
		GROUP BY add1n6code
		";//HAVING count(*) > 5;
	
	return Yii::$app->db->createCommand($sql, [':min'=>$min, ':max'=>$max, ':hospcode'=>$hospcode])->queryAll();
    }
    
    public static function getVillagelist3($min, $max, $hospcode='05147') {
	$sql = "SELECT CONCAT(add1n6code, add1n5) AS add1n2, 
			CONCAT('ต.', const_district.DISTRICT_NAME, 'บ.', add1n2, ' หมู่ ', add1n5, ' (', count(*), ')') AS text,
			count(*) AS num
		FROM tb_data_1 INNER JOIN const_district ON tb_data_1.add1n6code = const_district.DISTRICT_CODE
		WHERE floor(DATEDIFF(CURRENT_DATE, STR_TO_DATE(v2, '%Y-%m-%d'))/365) BETWEEN :min AND :max AND hsitecode=:hospcode AND add1n5 REGEXP '[0-9]+'
		GROUP BY add1n6code, add1n5
		";//HAVING count(*) > 5;
	
	return Yii::$app->db->createCommand($sql, [':min'=>$min, ':max'=>$max, ':hospcode'=>$hospcode])->queryAll();
    }
    public static function getRegisterId($sitecode) {
	$sql = "SELECT concat('\'',md5(cid),'\'') as cid FROM tb_data_1 WHERE rstat<>3 AND tb_data_1.hsitecode = :sitecode";
	return Yii::$app->db->createCommand($sql, [':sitecode'=>$sitecode])->queryAll();
    }
    public static function getRegisterCid($sitecode) {
	$sql = "SELECT cid FROM tb_data_1 WHERE rstat<>3 AND tb_data_1.hsitecode = :sitecode";
	return Yii::$app->db->createCommand($sql, [':sitecode'=>$sitecode])->queryAll();
    }
    public static function checkCid($cid, $sitecode) {
	$sql = "SELECT * FROM tb_data_1 WHERE rstat<>3 AND cid = :cid AND tb_data_1.hsitecode = :sitecode";
	return Yii::$app->db->createCommand($sql, [':cid'=>$cid, ':sitecode'=>$sitecode])->queryOne();
    }
    
    public static function checkCidupdate($cid, $sitecode) {
	$sql = "SELECT * FROM tb_data_1 WHERE rstat=1 AND cid = :cid AND tb_data_1.hsitecode = :sitecode";
	return Yii::$app->db->createCommand($sql, [':cid'=>$cid, ':sitecode'=>$sitecode])->queryOne();
    }
    
    public static function checkCidAll($cid) {
	$sql = "SELECT * FROM tb_data_1 WHERE rstat<>3 AND cid = :cid";
	return Yii::$app->db->createCommand($sql, [':cid'=>$cid])->queryOne();
    }
    
    public static function getCCA01($target, $start, $end) {
	$sql = "SELECT * FROM tb_data_2 WHERE rstat<>3 AND target = :target AND f1vdcomp BETWEEN :start AND :end ORDER BY f1vdcomp DESC limit 1";
	return Yii::$app->db->createCommand($sql, [':target'=>$target, ':start'=>$start, ':end'=>$end])->queryOne();
    }
    
    public static function getOv01($target, $table, $start, $end) {
	$sql = "SELECT * FROM $table WHERE rstat<>3 AND target = :target AND vdate BETWEEN :start AND :end ORDER BY vdate DESC limit 1";
	return Yii::$app->db->createCommand($sql, [':target'=>$target, ':start'=>$start, ':end'=>$end])->queryOne();
    }
    
    public static function getOv02($target, $table, $start, $end) {
	$sql = "SELECT * FROM $table WHERE rstat<>3 AND target = :target AND vdate BETWEEN :start AND :end ORDER BY vdate DESC limit 1";
	return Yii::$app->db->createCommand($sql, [':target'=>$target, ':start'=>$start, ':end'=>$end])->queryOne();
    }
    
    public static function getOv03($target, $table, $start, $end) {
	$sql = "SELECT * FROM $table WHERE rstat<>3 AND target = :target AND vdate BETWEEN :start AND :end ORDER BY vdate DESC limit 1";
	return Yii::$app->db->createCommand($sql, [':target'=>$target, ':start'=>$start, ':end'=>$end])->queryOne();
    }
    
    public static function getCca02($target, $table, $start, $end) {
	$sql = "SELECT * FROM $table WHERE rstat<>3 AND target = :target AND fsupdate BETWEEN :start AND :end ORDER BY fsupdate DESC limit 1";
	return Yii::$app->db->createCommand($sql, [':target'=>$target, ':start'=>$start, ':end'=>$end])->queryOne();
    }
    
    public static function getRegister($target, $table) {
	$sql = "SELECT * FROM $table WHERE rstat<>3 AND target = :target limit 1";
	return Yii::$app->db->createCommand($sql, [':target'=>$target])->queryOne();
    }
    
    public static function getOVData($sitecode, $select=null, $order='village_code, address, fname, lname') {
	
	$str = '';
	if(isset($select)){
	    $str = ' AND ov2_person.id IN ('.$select.')';
	}
	
	\backend\modules\ov2\classes\OvccaQuery::dpOvTemporaryAll($sitecode, $select);
	
	$sql = "SELECT ov2_person.id, 
		    ov2_person.khet, 
		    ov2_person.province, 
		    ov2_person.amphur, 
		    ov2_person.tambon, 
		    ov2_person.hospcode, 
		    ov2_person.hospname, 
		    ov2_person.person_id, 
		    ov2_person.house_id, 
		    ov2_person.address, 
		    ov2_person.cid, 
		    ov2_person.hn, 
		    floor(DATEDIFF(CURRENT_DATE, STR_TO_DATE(ov2_person.birthdate, '%Y-%m-%d'))/365) AS age,
		    CONCAT(ov2_person.pname, ov2_person.fname, ' ', ov2_person.lname) AS fullname,
		    ov2_person.pname, 
		    ov2_person.fname, 
		    ov2_person.lname, 
		    ov2_person.sex, 
		    ov2_person.nationality, 
		    ov2_person.education, 
		    ov2_person.type_area, 
		    ov2_person.religion, 
		    ov2_person.birthdate, 
		    ov2_person.village_id, 
		    ov2_person.village_code, 
		    ov2_person.village_name, 
		    ov2_person.pttype, 
		    ov2_person.pttype_begin_date, 
		    ov2_person.pttype_expire_date, 
		    ov2_person.pttype_hospmain, 
		    ov2_person.pttype_hospsub, 
		    ov2_person.marrystatus, 
		    ov2_person.death, 
		    ov2_person.death_date, 
		    ov2_person.moo, 
		    ov2_person.ov01_status, 
		    ov2_person.status_import, 
		    ov2_person.ptcode, 
		    ov2_person.ptid_key, 
		    ov2_person.ptid,
		    tmpov.*
	    FROM ov2_person INNER JOIN tb_data_1 ON ov2_person.cid = tb_data_1.cid AND ov2_person.hospcode = tb_data_1.hsitecode
	    LEFT JOIN tmpov ON tmpov.tmp_id=ov2_person.id
	    WHERE ov2_person.status_import = 1 and ov2_person.hospcode = :sitecode $str
	    group by ov2_person.id
	    order by $order
		";
	return Yii::$app->db->createCommand($sql, [':sitecode'=>$sitecode])->queryAll();
    }
    
    public static function getOVDataSetect($sitecode, $fid, $sid, $select=null, $order='village_code, address, fname, lname') {
	
	$str = '';
	if(isset($select)){
	    $str = ' AND ov2_person.id IN ('.$select.')';
	}
	
	\backend\modules\ov2\classes\OvccaQuery::dpOvTemporaryAll($sitecode, $select, $ovfilter_sub);
	
	$sql = "SELECT ov2_person.id, 
		    ov2_person.khet, 
		    ov2_person.province, 
		    ov2_person.amphur, 
		    ov2_person.tambon, 
		    ov2_person.hospcode, 
		    ov2_person.hospname, 
		    ov2_person.person_id, 
		    ov2_person.house_id, 
		    ov2_person.address, 
		    ov2_person.cid, 
		    ov2_person.hn, 
		    floor(DATEDIFF(CURRENT_DATE, STR_TO_DATE(ov2_person.birthdate, '%Y-%m-%d'))/365) AS age,
		    CONCAT(ov2_person.pname, ov2_person.fname, ' ', ov2_person.lname) AS fullname,
		    ov2_person.pname, 
		    ov2_person.fname, 
		    ov2_person.lname, 
		    ov2_person.sex, 
		    ov2_person.nationality, 
		    ov2_person.education, 
		    ov2_person.type_area, 
		    ov2_person.religion, 
		    ov2_person.birthdate, 
		    ov2_person.village_id, 
		    ov2_person.village_code, 
		    ov2_person.village_name, 
		    ov2_person.pttype, 
		    ov2_person.pttype_begin_date, 
		    ov2_person.pttype_expire_date, 
		    ov2_person.pttype_hospmain, 
		    ov2_person.pttype_hospsub, 
		    ov2_person.marrystatus, 
		    ov2_person.death, 
		    ov2_person.death_date, 
		    ov2_person.moo, 
		    ov2_person.ov01_status, 
		    ov2_person.status_import, 
		    ov2_person.ptcode, 
		    ov2_person.ptid_key, 
		    ov2_person.ptid,
		    tmpov.*
	    FROM ov2_person INNER JOIN ov2_sub_list ON ov2_sub_list.person_id = ov2_person.id
	    INNER JOIN tb_data_1 ON ov2_person.cid = tb_data_1.cid AND ov2_person.hospcode = tb_data_1.hsitecode
	    LEFT JOIN tmpov ON tmpov.tmp_id=ov2_person.id
	    WHERE ov2_person.status_import = 1 and ov2_person.hospcode = :sitecode AND ov2_sub_list.filter_id=:fid AND ov2_sub_list.sub_id=:sid $str
	    group by ov2_person.id
	    order by $order
		";
	return Yii::$app->db->createCommand($sql, [':sitecode'=>$sitecode, ':fid'=>$fid, ':sid'=>$sid])->queryAll();
    }
    
    public static function checkConvert($sitecode) {
	$sql = "SELECT COUNT(*) AS num
		FROM ov2_tmp_person
		WHERE ov2_tmp_person.hospcode = :sitecode AND ov2_tmp_person.pname IN ('นาย', 'นาง', 'น.ส.')";
	return Yii::$app->db->createCommand($sql, [':sitecode'=>$sitecode])->queryScalar();
    }
    
    public static function dpOvTemporary($sitecode, $page=null, $perPage=100, $sort='village_code, address, fname, lname', $ovfilter_sub=null) {
	
	$group = 'ov2_person.hospcode, ov2_person.cid';
	$pageStart = 0;
	if(isset($page)){
	    $count = Yii::$app->db->createCommand("
		SELECT COUNT(*) FROM ov2_person WHERE hospcode = :sitecode
	    ", [':sitecode' => $sitecode])->queryScalar();
	    
	    $pageStart = ( $perPage * $page ) - $perPage;
	    
	} 
	
	$limit = "LIMIT $pageStart, $perPage";
	
	if($sort!='village_code, address, fname, lname'){
	    
	    if(substr($sort, 0,1)=='-'){
		$sort = str_replace('-', '', $sort);
		$sort = 'ov2_person.'.$sort.' DESC';
	    }else {
		$sort = 'ov2_person.'.$sort;
	    }
	}
	$ov2_sub_list = '';
	if($ovfilter_sub!=null){
	    $ov2_sub_list = "INNER JOIN ov2_sub_list ON ov2_sub_list.person_id = ov2_person.id AND ov2_sub_list.sub_id = $ovfilter_sub";
	    $group = 'ov2_person.id';
	}
	
	Yii::$app->db->createCommand("DROP TABLE IF EXISTS tmpov;")->execute();
	//			ov2_person.khet, 
//			ov2_person.province, 
//			ov2_person.amphur, 
//			ov2_person.tambon, 
//			ov2_person.hospcode, 
//			ov2_person.hospname, 
//			ov2_person.person_id, 
//			ov2_person.house_id, 
//			ov2_person.address, 
//			ov2_person.cid, 
//			ov2_person.hn, 
//			ov2_person.pname, 
//			ov2_person.fname, 
//			ov2_person.lname, 
//			ov2_person.sex, 
//			ov2_person.nationality, 
//			ov2_person.education, 
//			ov2_person.type_area, 
//			ov2_person.religion, 
//			ov2_person.birthdate, 
//			ov2_person.village_id, 
//			ov2_person.village_code, 
//			ov2_person.village_name, 
//			ov2_person.pttype, 
//			ov2_person.pttype_begin_date, 
//			ov2_person.pttype_expire_date, 
//			ov2_person.pttype_hospmain, 
//			ov2_person.pttype_hospsub, 
//			ov2_person.marrystatus, 
//			ov2_person.death, 
//			ov2_person.death_date, 
//			ov2_person.moo, 
//			ov2_person.ov01_status, 
//			ov2_person.status_import, 
//			ov2_person.ptcode, 
//			ov2_person.ptcodefull, 
//			ov2_person.ptid_key, 
//			ov2_person.ptid,
	$sql = "CREATE TEMPORARY TABLE tmpov
		SELECT ov2_person.id AS tmp_id, 
			tb_data_1.id AS register_id
		FROM ov2_person INNER JOIN tb_data_1 ON ov2_person.ptid = tb_data_1.ptid
		$ov2_sub_list
		WHERE hospcode = :sitecode
		GROUP BY $group
		ORDER BY $sort
		$limit 
		;";
	Yii::$app->db->createCommand($sql, [':sitecode'=>$sitecode])->execute();
	
	$sql = "ALTER TABLE tmpov ADD `cca01_id` LONGTEXT NULL DEFAULT NULL, 
		ADD `cca01_result` LONGTEXT NULL DEFAULT NULL,
		ADD `ov01k_id` LONGTEXT NULL DEFAULT NULL,
		ADD `ov01k_result` LONGTEXT NULL DEFAULT NULL,
		ADD `ov01p_id` LONGTEXT NULL DEFAULT NULL,
		ADD `ov01p_result` LONGTEXT NULL DEFAULT NULL,
		ADD `ov01f_id` LONGTEXT NULL DEFAULT NULL,
		ADD `ov01f_result` LONGTEXT NULL DEFAULT NULL,
		ADD `ov01u_id` LONGTEXT NULL DEFAULT NULL,
		ADD `ov01u_result` LONGTEXT NULL DEFAULT NULL,
		ADD `ov02_id` LONGTEXT NULL DEFAULT NULL,
		ADD `ov02_result` LONGTEXT NULL DEFAULT NULL,
		ADD `ov03_id` LONGTEXT NULL DEFAULT NULL,
		ADD `ov03_result` LONGTEXT NULL DEFAULT NULL,
		ADD `cca02_id` LONGTEXT NULL DEFAULT NULL,
		ADD `cca02_result` LONGTEXT NULL DEFAULT NULL;";
	Yii::$app->db->createCommand($sql)->execute();
	
	$group = 'tb.hsitecode, tb.hptcode';
	
	if($ovfilter_sub!=null){
	    $group = 'ov2_person.id';
	}
	//cca01
	$sql = "UPDATE tmpov 
		INNER JOIN (
		    SELECT GROUP_CONCAT(tb.id) AS tb_id,
			    GROUP_CONCAT(tb.id) AS tb_result,
			    ov2_person.id
		    FROM ov2_person INNER JOIN tb_data_2 AS tb ON ov2_person.ptid = tb.ptid
		    $ov2_sub_list
		    WHERE hospcode = :sitecode
		    GROUP BY $group
		    ORDER BY $sort
		    $limit 
		) AS cca01 ON tmpov.tmp_id=cca01.id 
		SET tmpov.cca01_id=cca01.tb_id,
		    tmpov.cca01_result=cca01.tb_result;";
	Yii::$app->db->createCommand($sql, [':sitecode'=>$sitecode])->execute();
	
	//ov01k
	$sql = "UPDATE tmpov 
		INNER JOIN (
		    SELECT GROUP_CONCAT(tb.id) AS tb_id,
			    GROUP_CONCAT(tb.results) AS tb_result,
			    ov2_person.id
		    FROM ov2_person INNER JOIN tbdata_21 AS tb ON ov2_person.ptid = tb.ptid
		    $ov2_sub_list
		    WHERE hospcode = :sitecode
		    GROUP BY $group
		    ORDER BY $sort
		    $limit 
		) AS ov01k ON tmpov.tmp_id=ov01k.id 
		SET tmpov.ov01k_id=ov01k.tb_id,
		    tmpov.ov01k_result=ov01k.tb_result;";
	Yii::$app->db->createCommand($sql, [':sitecode'=>$sitecode])->execute();
	
	//ov01p
	$sql = "UPDATE tmpov 
		INNER JOIN (
		    SELECT GROUP_CONCAT(tb.id) AS tb_id,
			    GROUP_CONCAT(tb.results) AS tb_result,
			    ov2_person.id
		    FROM ov2_person INNER JOIN tbdata_22 AS tb ON ov2_person.ptid = tb.ptid
		    $ov2_sub_list
		    WHERE hospcode = :sitecode
		    GROUP BY $group
		    ORDER BY $sort
		    $limit 
		) AS ov01p ON tmpov.tmp_id=ov01p.id 
		SET tmpov.ov01p_id=ov01p.tb_id,
		    tmpov.ov01p_result=ov01p.tb_result;";
	Yii::$app->db->createCommand($sql, [':sitecode'=>$sitecode])->execute();
	
	//ov01f
	$sql = "UPDATE tmpov 
		INNER JOIN (
		    SELECT GROUP_CONCAT(tb.id) AS tb_id,
			    GROUP_CONCAT(tb.results) AS tb_result,
			    ov2_person.id
		    FROM ov2_person INNER JOIN tbdata_23 AS tb ON ov2_person.ptid = tb.ptid
		    $ov2_sub_list
		    WHERE hospcode = :sitecode
		    GROUP BY $group
		    ORDER BY $sort
		    $limit 
		) AS ov01f ON tmpov.tmp_id=ov01f.id 
		SET tmpov.ov01f_id=ov01f.tb_id,
		    tmpov.ov01f_result=ov01f.tb_result;";
	Yii::$app->db->createCommand($sql, [':sitecode'=>$sitecode])->execute();
	
	//ov01u
	$sql = "UPDATE tmpov 
		INNER JOIN (
		    SELECT GROUP_CONCAT(tb.id) AS tb_id,
			    GROUP_CONCAT(tb.results) AS tb_result,
			    ov2_person.id
		    FROM ov2_person INNER JOIN tbdata_24 AS tb ON ov2_person.ptid = tb.ptid
		    $ov2_sub_list
		    WHERE hospcode = :sitecode
		    GROUP BY $group
		    ORDER BY $sort
		    $limit 
		) AS ov01u ON tmpov.tmp_id=ov01u.id 
		SET tmpov.ov01u_id=ov01u.tb_id,
		    tmpov.ov01u_result=ov01u.tb_result;";
	Yii::$app->db->createCommand($sql, [':sitecode'=>$sitecode])->execute();
	
	//ov02
	$sql = "UPDATE tmpov 
		INNER JOIN (
		    SELECT GROUP_CONCAT(tb.id) AS tb_id,
			    GROUP_CONCAT(tb.id) AS tb_result,
			    ov2_person.id
		    FROM ov2_person INNER JOIN tbdata_25 AS tb ON ov2_person.ptid = tb.ptid
		    $ov2_sub_list
		    WHERE hospcode = :sitecode
		    GROUP BY $group
		    ORDER BY $sort
		    $limit 
		) AS ov02 ON tmpov.tmp_id=ov02.id 
		SET tmpov.ov02_id=ov02.tb_id,
		    tmpov.ov02_result=ov02.tb_result;";
	Yii::$app->db->createCommand($sql, [':sitecode'=>$sitecode])->execute();
	
	//ov03
	$sql = "UPDATE tmpov 
		INNER JOIN (
		    SELECT GROUP_CONCAT(tb.id) AS tb_id,
			    GROUP_CONCAT(tb.id) AS tb_result,
			    ov2_person.id
		    FROM ov2_person INNER JOIN tbdata_26 AS tb ON ov2_person.ptid = tb.ptid
		    $ov2_sub_list
		    WHERE hospcode = :sitecode
		    GROUP BY $group
		    ORDER BY $sort
		    $limit 
		) AS ov03 ON tmpov.tmp_id=ov03.id 
		SET tmpov.ov03_id=ov03.tb_id,
		    tmpov.ov03_result=ov03.tb_result;";
	Yii::$app->db->createCommand($sql, [':sitecode'=>$sitecode])->execute();
	
	//cca02
	$sql = "UPDATE tmpov 
		INNER JOIN (
		    SELECT GROUP_CONCAT(tb.id) AS tb_id,
			    GROUP_CONCAT(tb.id) AS tb_result,
			    ov2_person.id
		    FROM ov2_person INNER JOIN tb_data_3 AS tb ON ov2_person.ptid = tb.ptid
		    $ov2_sub_list
		    WHERE hospcode = :sitecode
		    GROUP BY $group
		    ORDER BY $sort
		    $limit 
		) AS cca02 ON tmpov.tmp_id=cca02.id 
		SET tmpov.cca02_id=cca02.tb_id,
		    tmpov.cca02_result=cca02.tb_result;";
	Yii::$app->db->createCommand($sql, [':sitecode'=>$sitecode])->execute();
	
	
	return true;
    }
    
    public static function dpOvTemporaryAll($sitecode, $select, $ovfilter_sub=null) {
	$group = 'ov2_person.hospcode, ov2_person.cid';
	
	$str = '';
	if(isset($select)){
	    $str = ' AND ov2_person.id IN ('.$select.')';
	}
	
	$ov2_sub_list = '';
	if($ovfilter_sub!=null){
	    $ov2_sub_list = "INNER JOIN ov2_sub_list ON ov2_sub_list.person_id = ov2_person.id AND ov2_sub_list.sub_id = $ovfilter_sub";
	    $group = 'ov2_person.id';
	}
	
	Yii::$app->db->createCommand("DROP TABLE IF EXISTS tmpov;")->execute();
	
	$sql = "CREATE TEMPORARY TABLE tmpov
		SELECT ov2_person.id AS tmp_id, 
			tb_data_1.id AS register_id
		FROM ov2_person INNER JOIN tb_data_1 ON ov2_person.ptid = tb_data_1.ptid
		WHERE hospcode = :sitecode $str
		GROUP BY $group
		;";
	Yii::$app->db->createCommand($sql, [':sitecode'=>$sitecode])->execute();
	
	$sql = "ALTER TABLE tmpov ADD `cca01_id` LONGTEXT NULL DEFAULT NULL, 
		ADD `cca01_result` LONGTEXT NULL DEFAULT NULL,
		ADD `ov01k_id` LONGTEXT NULL DEFAULT NULL,
		ADD `ov01k_result` LONGTEXT NULL DEFAULT NULL,
		ADD `ov01p_id` LONGTEXT NULL DEFAULT NULL,
		ADD `ov01p_result` LONGTEXT NULL DEFAULT NULL,
		ADD `ov01f_id` LONGTEXT NULL DEFAULT NULL,
		ADD `ov01f_result` LONGTEXT NULL DEFAULT NULL,
		ADD `ov01u_id` LONGTEXT NULL DEFAULT NULL,
		ADD `ov01u_result` LONGTEXT NULL DEFAULT NULL,
		ADD `ov02_id` LONGTEXT NULL DEFAULT NULL,
		ADD `ov02_result` LONGTEXT NULL DEFAULT NULL,
		ADD `ov03_id` LONGTEXT NULL DEFAULT NULL,
		ADD `ov03_result` LONGTEXT NULL DEFAULT NULL,
		ADD `cca02_id` LONGTEXT NULL DEFAULT NULL,
		ADD `cca02_result` LONGTEXT NULL DEFAULT NULL;";
	Yii::$app->db->createCommand($sql)->execute();
	
	$group = 'tb.hsitecode, tb.hptcode';
	
	if($ovfilter_sub!=null){
	    $group = 'ov2_person.id';
	}
	
	//cca01
	$sql = "UPDATE tmpov 
		INNER JOIN (
		    SELECT GROUP_CONCAT(tb.id) AS tb_id,
			    GROUP_CONCAT(tb.id) AS tb_result,
			    ov2_person.id
		    FROM ov2_person INNER JOIN tb_data_2 AS tb ON ov2_person.ptid = tb.ptid
		    $ov2_sub_list
		    WHERE status_import = 1 AND hospcode = :sitecode $str
		    GROUP BY $group
		) AS cca01 ON tmpov.tmp_id=cca01.id 
		SET tmpov.cca01_id=cca01.tb_id,
		    tmpov.cca01_result=cca01.tb_result;";
	Yii::$app->db->createCommand($sql, [':sitecode'=>$sitecode])->execute();
	
	//ov01k
	$sql = "UPDATE tmpov 
		INNER JOIN (
		    SELECT GROUP_CONCAT(tb.id) AS tb_id,
			    GROUP_CONCAT(tb.results) AS tb_result,
			    ov2_person.id
		    FROM ov2_person INNER JOIN tbdata_21 AS tb ON ov2_person.ptid = tb.ptid
		    $ov2_sub_list
		    WHERE status_import = 1 AND hospcode = :sitecode $str
		    GROUP BY $group
		) AS ov01k ON tmpov.tmp_id=ov01k.id 
		SET tmpov.ov01k_id=ov01k.tb_id,
		    tmpov.ov01k_result=ov01k.tb_result;";
	Yii::$app->db->createCommand($sql, [':sitecode'=>$sitecode])->execute();
	
	//ov01p
	$sql = "UPDATE tmpov 
		INNER JOIN (
		    SELECT GROUP_CONCAT(tb.id) AS tb_id,
			    GROUP_CONCAT(tb.results) AS tb_result,
			    ov2_person.id
		    FROM ov2_person INNER JOIN tbdata_22 AS tb ON ov2_person.ptid = tb.ptid
		    $ov2_sub_list
		    WHERE status_import = 1 AND hospcode = :sitecode $str
		    GROUP BY $group
		) AS ov01p ON tmpov.tmp_id=ov01p.id 
		SET tmpov.ov01p_id=ov01p.tb_id,
		    tmpov.ov01p_result=ov01p.tb_result;";
	Yii::$app->db->createCommand($sql, [':sitecode'=>$sitecode])->execute();
	
	//ov01f
	$sql = "UPDATE tmpov 
		INNER JOIN (
		    SELECT GROUP_CONCAT(tb.id) AS tb_id,
			    GROUP_CONCAT(tb.results) AS tb_result,
			    ov2_person.id
		    FROM ov2_person INNER JOIN tbdata_23 AS tb ON ov2_person.ptid = tb.ptid
		    $ov2_sub_list
		    WHERE status_import = 1 AND hospcode = :sitecode $str
		    GROUP BY $group
		) AS ov01f ON tmpov.tmp_id=ov01f.id 
		SET tmpov.ov01f_id=ov01f.tb_id,
		    tmpov.ov01f_result=ov01f.tb_result;";
	Yii::$app->db->createCommand($sql, [':sitecode'=>$sitecode])->execute();
	
	//ov01u
	$sql = "UPDATE tmpov 
		INNER JOIN (
		    SELECT GROUP_CONCAT(tb.id) AS tb_id,
			    GROUP_CONCAT(tb.results) AS tb_result,
			    ov2_person.id
		    FROM ov2_person INNER JOIN tbdata_24 AS tb ON ov2_person.ptid = tb.ptid
		    $ov2_sub_list
		    WHERE status_import = 1 AND hospcode = :sitecode $str
		    GROUP BY $group
		) AS ov01u ON tmpov.tmp_id=ov01u.id 
		SET tmpov.ov01u_id=ov01u.tb_id,
		    tmpov.ov01u_result=ov01u.tb_result;";
	Yii::$app->db->createCommand($sql, [':sitecode'=>$sitecode])->execute();
	
	//ov02
	$sql = "UPDATE tmpov 
		INNER JOIN (
		    SELECT GROUP_CONCAT(tb.id) AS tb_id,
			    GROUP_CONCAT(tb.id) AS tb_result,
			    ov2_person.id
		    FROM ov2_person INNER JOIN tbdata_25 AS tb ON ov2_person.ptid = tb.ptid
		    $ov2_sub_list
		    WHERE status_import = 1 AND hospcode = :sitecode $str
		    GROUP BY $group
		) AS ov02 ON tmpov.tmp_id=ov02.id 
		SET tmpov.ov02_id=ov02.tb_id,
		    tmpov.ov02_result=ov02.tb_result;";
	Yii::$app->db->createCommand($sql, [':sitecode'=>$sitecode])->execute();
	
	//ov03
	$sql = "UPDATE tmpov 
		INNER JOIN (
		    SELECT GROUP_CONCAT(tb.id) AS tb_id,
			    GROUP_CONCAT(tb.id) AS tb_result,
			    ov2_person.id
		    FROM ov2_person INNER JOIN tbdata_26 AS tb ON ov2_person.ptid = tb.ptid
		    $ov2_sub_list
		    WHERE status_import = 1 AND hospcode = :sitecode $str
		    GROUP BY $group
		) AS ov03 ON tmpov.tmp_id=ov03.id 
		SET tmpov.ov03_id=ov03.tb_id,
		    tmpov.ov03_result=ov03.tb_result;";
	Yii::$app->db->createCommand($sql, [':sitecode'=>$sitecode])->execute();
	
	//cca02
	$sql = "UPDATE tmpov 
		INNER JOIN (
		    SELECT GROUP_CONCAT(tb.id) AS tb_id,
			    GROUP_CONCAT(tb.id) AS tb_result,
			    ov2_person.id
		    FROM ov2_person INNER JOIN tb_data_3 AS tb ON ov2_person.ptid = tb.ptid
		    $ov2_sub_list
		    WHERE status_import = 1 AND hospcode = :sitecode $str
		    GROUP BY $group
		) AS cca02 ON tmpov.tmp_id=cca02.id 
		SET tmpov.cca02_id=cca02.tb_id,
		    tmpov.cca02_result=cca02.tb_result;";
	Yii::$app->db->createCommand($sql, [':sitecode'=>$sitecode])->execute();
	
	
	return true;
    }
    
    public static function getFilterSub($sitecode) {
	$sql = "SELECT ov2_filter_sub.sub_id, 
			ov2_filter_sub.urine_status, 
			ov2_filter_sub.filter_id, 
			ov2_filter_sub.sitecode, 
			ov2_filter_sub.created_by,
			CONCAT(ov2_filter_sub.sub_name, ' (', (SELECT COUNT(*) AS num FROM ov2_sub_list INNER JOIN tb_data_1  ON tb_data_1.id = ov2_sub_list.person_id WHERE ov2_sub_list.sub_id = ov2_filter_sub.sub_id),')') AS sub_name
		FROM ov2_filter_sub
		WHERE ov2_filter_sub.sitecode = :sitecode ";
	return Yii::$app->db->createCommand($sql, [':sitecode'=>$sitecode])->queryAll();
    }
    
    public static function getRegisterList() {
	$sql = "SELECT ptid, CONCAT(title, name, ' ', surname) as fullname FROM tb_data_1 WHERE rstat<>3";
	return Yii::$app->db->createCommand($sql)->queryall();
    }
    
    public static function getSeclectList($sub) {
	$sql = "SELECT ov2_sub_list.person_id
		FROM ov2_filter_sub INNER JOIN ov2_sub_list ON ov2_filter_sub.sub_id = ov2_sub_list.sub_id
		where ov2_sub_list.sub_id=:sub";
	return Yii::$app->db->createCommand($sql, [':sub'=>$sub])->queryColumn();
    }
    
    public static function getTbData($sitecode, $ovfilter_sub, $select=null) {
	
	$str = '';
	if(isset($select)){
	    $str = ' AND tb_data_1.id IN ('.$select.')';
	}
	
	$fromSub = '';
	$selectSub = '';
	if($ovfilter_sub>0){
	    $fromSub = "inner join ov2_sub_list on ov2_sub_list.person_id = tb_data_1.id";
	    $selectSub = " AND ov2_sub_list.sub_id = $ovfilter_sub";
	}
	//floor(DATEDIFF(CURRENT_DATE, STR_TO_DATE(tb_data_1.v2, '%Y-%m-%d'))/365) AS age,
	//CONCAT(tb_data_1.title, tb_data_1.name, ' ', tb_data_1.surname) AS fullname,
	$sql = "SELECT tb_data_1.id, 
		    tb_data_1.cid, 
		    tb_data_1.hn, 
		    tb_data_1.v2,
		    tb_data_1.title,
		    tb_data_1.name,
		    tb_data_1.surname,
		    tb_data_1.add1n5, 
		    tb_data_1.hsitecode, 
		    tb_data_1.hptcode, 
		    tb_data_1.add1n1, 
		    tb_data_1.add1n5, 
		    tb_data_1.add1n6code, 
		    tb_data_1.add1n7code, 
		    tb_data_1.add1n8code, 
		    tb_data_1.age,
		    tb_data_1.ptid,
		    (SELECT group_concat(ov01k.id) FROM tbdata_21 ov01k WHERE ov01k.ptid = tb_data_1.ptid AND ov01k.rstat<>3 GROUP BY ov01k.ptid) AS ov01k_id,
		    (SELECT group_concat(ov01k.results) FROM tbdata_21 ov01k WHERE ov01k.ptid = tb_data_1.ptid AND ov01k.rstat<>3 GROUP BY ov01k.ptid) AS ov01k_result,
		    (SELECT group_concat(ov01p.id) FROM tbdata_22 ov01p WHERE ov01p.ptid = tb_data_1.ptid AND ov01p.rstat<>3 GROUP BY ov01p.ptid) AS ov01p_id,
		    (SELECT group_concat(ov01p.results) FROM tbdata_22 ov01p WHERE ov01p.ptid = tb_data_1.ptid AND ov01p.rstat<>3 GROUP BY ov01p.ptid) AS ov01p_result,
		    (SELECT group_concat(ov01f.id) FROM tbdata_23 ov01f WHERE ov01f.ptid = tb_data_1.ptid AND ov01f.rstat<>3 GROUP BY ov01f.ptid) AS ov01f_id,
		    (SELECT group_concat(ov01f.results) FROM tbdata_23 ov01f WHERE ov01f.ptid = tb_data_1.ptid AND ov01f.rstat<>3 GROUP BY ov01f.ptid) AS ov01f_result,
		    (SELECT group_concat(ov01u.id) FROM tbdata_24 ov01u WHERE ov01u.ptid = tb_data_1.ptid AND ov01u.rstat<>3 GROUP BY ov01u.ptid) AS ov01u_id,
		    (SELECT group_concat(ov01u.results) FROM tbdata_24 ov01u WHERE ov01u.ptid = tb_data_1.ptid AND ov01u.rstat<>3 GROUP BY ov01u.ptid) AS ov01u_result,
		    (SELECT group_concat(ov02.id) FROM tbdata_25 ov02 WHERE ov02.ptid = tb_data_1.ptid AND ov02.rstat<>3 GROUP BY ov02.ptid) AS ov02_id,
		    (SELECT group_concat(ov03.id) FROM tbdata_26 ov03 WHERE ov03.ptid = tb_data_1.ptid AND ov03.rstat<>3 GROUP BY ov03.ptid) AS ov03_id,
		    (SELECT group_concat(cca02.id) FROM tb_data_3 cca02 WHERE cca02.ptid = tb_data_1.ptid AND cca02.rstat<>3 GROUP BY cca02.ptid) AS cca02_id,
		    (SELECT group_concat(cca01.id) FROM tb_data_2 cca01 WHERE cca01.ptid = tb_data_1.ptid AND cca01.rstat<>3 GROUP BY cca01.ptid) AS cca01_id
	    FROM tb_data_1 $fromSub
	    WHERE tb_data_1.rstat<>3 AND tb_data_1.hsitecode = :sitecode $str $selectSub
	    order by add1n8code, add1n7code, add1n6code, add1n5, add1n1, name, surname
		";
	
	return Yii::$app->db->createCommand($sql, [':sitecode'=>$sitecode])->queryAll();
    }
    
    public static function getTbSkData($sitecode, $ovfilter_sub, $select=null) {
	
	$str = '';
	if(isset($select)){
	    $str = ' AND tb_data_1.id IN ('.$select.')';
	}
	
	$fromSub = '';
	$selectSub = '';
	if($ovfilter_sub>0){
	    $fromSub = "inner join ov2_sub_list on ov2_sub_list.person_id = tb_data_1.id";
	    $selectSub = " AND ov2_sub_list.sub_id = $ovfilter_sub";
	}
	
	$sql = "SELECT tb_data_1.id, 
		    tb_data_1.cid, 
		    tb_data_1.hn, 
		    tb_data_1.v2,
		    tb_data_1.title,
		    tb_data_1.name,
		    tb_data_1.surname,
		    tb_data_1.add1n5, 
		    tb_data_1.hsitecode, 
		    tb_data_1.hptcode, 
		    tb_data_1.add1n1, 
		    tb_data_1.add1n5, 
		    tb_data_1.add1n6code, 
		    tb_data_1.add1n7code, 
		    tb_data_1.add1n8code, 
		    tb_data_1.age,
		    tb_data_1.ptid
	    FROM tb_data_1 $fromSub
	    WHERE tb_data_1.rstat<>3 AND tb_data_1.hsitecode = :sitecode $str $selectSub
	    order by hptcode
		";
	return Yii::$app->db->createCommand($sql, [':sitecode'=>$sitecode])->queryAll();
    }
    
    public static function getPersonOneByCid($sitecode, $cid, $key, $convert) {
	$str = "decode(unhex(person.address),sha2(:key,256)) AS address,
		    decode(unhex(person.cid),sha2(:key,256)) AS cid,
		    decode(unhex(person.hn),sha2(:key,256)) AS hn,
		    decode(unhex(person.pname),sha2(:key,256)) AS pname,
		    decode(unhex(person.fname),sha2(:key,256)) AS fname,
		    decode(unhex(person.lname),sha2(:key,256)) AS lname,";
	
	if ($convert==1) {
	    $str = "convert(decode(unhex(person.address),sha2(:key,256)) using tis620) AS address,
		    convert(decode(unhex(person.cid),sha2(:key,256)) using tis620) AS cid,
		    convert(decode(unhex(person.hn),sha2(:key,256)) using tis620) AS hn,
		    convert(decode(unhex(person.pname),sha2(:key,256)) using tis620) AS pname,
		    convert(decode(unhex(person.fname),sha2(:key,256)) using tis620) AS fname,
		    convert(decode(unhex(person.lname),sha2(:key,256)) using tis620) AS lname,";
	}
	
	$sql = "SELECT person.khet, 
		    person.province, 
		    person.amphur, 
		    person.tambon, 
		    person.hospcode, 
		    person.hospname, 
		    person.person_id, 
		    person.house_id, 
		    $str
		    person.sex, 
		    person.nationality, 
		    person.education, 
		    person.type_area, 
		    person.religion, 
		    person.birthdate, 
		    floor(DATEDIFF(CURRENT_DATE, STR_TO_DATE(person.birthdate, '%Y-%m-%d'))/365) AS age,
		    person.village_id, 
		    person.village_code, 
		    person.village_name, 
		    person.pttype, 
		    person.pttype_begin_date, 
		    person.pttype_expire_date, 
		    person.pttype_hospmain, 
		    person.pttype_hospsub, 
		    person.marrystatus, 
		    person.death, 
		    person.cidlink,
		    person.death_date
	    FROM person
	    WHERE person.sitecode = :hospcode and cidlink like md5(:cid) and person.death = 'N'
	    ";
	
	return Yii::$app->dbbot->createCommand($sql, [':cid'=>$cid, ':hospcode'=>$sitecode, ':key'=>$key])->queryOne();
    }
}
