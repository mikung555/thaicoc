<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use appxq\sdii\helpers\SDNoty;
use appxq\sdii\helpers\SDHtml;
use kartik\widgets\DatePicker;

/* @var $this yii\web\View */
/* @var $model backend\modules\ov2\models\OvFilter */
/* @var $form yii\bootstrap\ActiveForm */
?>

<div class="ov-filter-form">

    <?php $form = ActiveForm::begin([
	'id'=>$model->formName(),
    ]); ?>

    <div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	<h4 class="modal-title" id="itemModalLabel">เพิ่มรอบการทำงาน</h4>
    </div>

    <div class="modal-body">
	<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
	
	<?= DatePicker::widget([
	    'model' => $model,
	    'attribute' => 'start',
	    'attribute2' => 'end',
	    'options' => ['placeholder' => 'Start date'],
	    'options2' => ['placeholder' => 'End date'],
	    'type' => DatePicker::TYPE_RANGE,
	    'form' => $form,
	    'pluginOptions' => [
		'format' => 'dd-mm-yyyy',
		'autoclose' => true,
	    ]
	]);?>
    </div>
    <div class="modal-footer">
	<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	<?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php  $this->registerJs("
$('form#{$model->formName()}').on('beforeSubmit', function(e) {
    var \$form = $(this);
    $.post(
	\$form.attr('action'), //serialize Yii2 form
	\$form.serialize()
    ).done(function(result) {
	if(result.status == 'success') {
	    ". SDNoty::show('result.message', 'result.status') ."
	    $(document).find('#modal-ov-person').modal('hide');
	    window.location.href = '".yii\helpers\Url::to(['/ov2/ov-person/index', 'ovfilter'=>''])."'+result.data.id
	    //$.pjax.reload({container:'#ov-person-grid-pjax'});
	} else {
	    ". SDNoty::show('result.message', 'result.status') ."
	} 
    }).fail(function() {
	". SDNoty::show("'" . SDHtml::getMsgError() . "Server Error'", '"error"') ."
	console.log('server error');
    });
    return false;
});

");?>