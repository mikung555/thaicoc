<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\ovcca\models\OvFilterSub */

$this->title = Yii::t('app', 'สร้างใบกำกับงาน');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Ov Filter Subs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ov-filter-sub-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
