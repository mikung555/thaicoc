<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use appxq\sdii\helpers\SDNoty;
use appxq\sdii\helpers\SDHtml;

/* @var $this yii\web\View */
/* @var $model backend\modules\ov2\models\TmpPerson */
/* @var $form yii\bootstrap\ActiveForm */
?>

<div class="tmp-person-form">

    <?php $form = ActiveForm::begin([
	'id'=>$model->formName(),
    ]); ?>

    <div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	<h4 class="modal-title" id="itemModalLabel">Tmp Person</h4>
    </div>

    <div class="modal-body">
	<?= $form->field($model, 'khet')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'province')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'amphur')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'tambon')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'hospcode')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'hospname')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'person_id')->textInput() ?>

	<?= $form->field($model, 'house_id')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'cid')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'hn')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'pname')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'fname')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'lname')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'sex')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'nationality')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'education')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'type_area')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'religion')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'birthdate')->textInput() ?>

	<?= $form->field($model, 'village_id')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'village_code')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'village_name')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'pttype')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'pttype_begin_date')->textInput() ?>

	<?= $form->field($model, 'pttype_expire_date')->textInput() ?>

	<?= $form->field($model, 'pttype_hospmain')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'pttype_hospsub')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'marrystatus')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'death')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'death_date')->textInput() ?>

    </div>
    <div class="modal-footer">
	<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	<?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php  $this->registerJs("
$('form#{$model->formName()}').on('beforeSubmit', function(e) {
    var \$form = $(this);
    $.post(
	\$form.attr('action'), //serialize Yii2 form
	\$form.serialize()
    ).done(function(result) {
	if(result.status == 'success') {
	    ". SDNoty::show('result.message', 'result.status') ."
	    if(result.action == 'create') {
		$(\$form).trigger('reset');
		$.pjax.reload({container:'#tmp-person-grid-pjax'});
	    } else if(result.action == 'update') {
		$(document).find('#modal-tmp-person').modal('hide');
		$.pjax.reload({container:'#tmp-person-grid-pjax'});
	    }
	} else {
	    ". SDNoty::show('result.message', 'result.status') ."
	} 
    }).fail(function() {
	". SDNoty::show("'" . SDHtml::getMsgError() . "Server Error'", '"error"') ."
	console.log('server error');
    });
    return false;
});

");?>