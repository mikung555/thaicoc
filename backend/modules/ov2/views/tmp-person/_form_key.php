<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use appxq\sdii\helpers\SDNoty;
use appxq\sdii\helpers\SDHtml;

/* @var $this yii\web\View */
/* @var $model backend\modules\ov2\models\TmpPerson */
/* @var $form yii\bootstrap\ActiveForm */
?>

<div class="tmp-person-form">

    <?php $form = ActiveForm::begin([
	'id'=>'add-key',
    ]); ?>

    <div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	<h4 class="modal-title" id="itemModalLabel">โปรดกรอกรหัสที่กำหนดใน TCC Bot</h4>
    </div>

    <div class="modal-body">
    <?=  Html::img('/img/add_key_tcc.png', ['class'=>'img-responsive'])?>
    <hr>
    <?=  Html::label('กุญแจถอดรหัสข้อมูล')?>
	<?=  Html::passwordInput('add_key', isset(Yii::$app->session['key_db'])?Yii::$app->session['key_db']:'', ['class'=>'form-control', 'placeholder'=> 'กรุณากรอกกุญแจถอดรหัส', 'required'=>true])?>
	<br>
	<?= Html::checkbox('save_key', isset($_COOKIE['save_key'])?$_COOKIE['save_key']:false, ['label'=>'จดจำคีย์นี้'])?>
	 <?= Html::checkbox('convert', isset(Yii::$app->session['convert'])?Yii::$app->session['convert']:false, ['label'=>'เข้ารหัสแบบ tis620 (กรณีชื่อ-สกุล อ่านไม่ออกให้กาช่องนี้ด้วย)'])?>
    </div>
    
    <div class="modal-footer">
	<?= Html::submitButton(Yii::t('app', 'Submit'), ['class' => 'btn btn-primary']) ?>
	<?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php
 
$this->registerJs("
$('form#add-key').on('beforeSubmit', function(e) {
    var \$form = $(this);
    $.post(
	\$form.attr('action'), //serialize Yii2 form
	\$form.serialize()
    ).done(function(result) {
	if(result.status == 'success') {
	    ". SDNoty::show('result.message', 'result.status') ."
	    $(document).find('#modal-tmp-person').modal('hide');
	    window.location.href = '".yii\helpers\Url::to(['/ov2/tmp-person/drump', 'minage'=>$_GET['minage'], 'maxage'=>$_GET['maxage'], 'type'=>$_GET['type'], 'village'=>$_GET['village']])."';
	} else {
	    ". SDNoty::show('result.message', 'result.status') ."
	} 
    }).fail(function() {
	". SDNoty::show("'" . SDHtml::getMsgError() . "Server Error'", '"error"') ."
	console.log('server error');
    });
    return false;
});

");?>