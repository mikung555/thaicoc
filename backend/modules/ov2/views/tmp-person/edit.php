<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\helpers\Url;
use appxq\sdii\widgets\GridView;
use appxq\sdii\widgets\ModalForm;
use appxq\sdii\helpers\SDNoty;
use appxq\sdii\helpers\SDHtml;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\ov2\models\TmpPersonSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'นำเข้าจาก TCC Bot เพื่อจัดการปรับปรุงข้อมูลใน Isan Cohort');
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="tmp-person-index">
    <?php echo Html::a('<i class="fa fa-reply"></i> กลับไปจัดการใบกำกับงาน', ['/ov2/ov-person/index'], ['class' => 'btn btn-sm btn-default']) ?>
    
    <h4 style="color: #15c">1. ถอดรหัสข้อมูล</h4>
    <div style="border: 1px solid #ddd; padding: 10px;border-radius: 4px;margin-bottom: 15px;">
	<?php $form = ActiveForm::begin([
	    'id'=>'findbot-form',
	    'layout'=>'inline',
	]); ?>
	
	<div class="form-group">
	    <label>กุญแจถอดรหัสข้อมูล</label>
	    <input type="password" name="findbot[key]" value="<?=$key?>" class="form-control" placeholder="โปรดกรอกรหัสที่กำหนดใน TCC Bot" style="width: 250px;">
	</div>
	<div class="checkbox">
	    <label>
		<input type="checkbox" name="findbot[convert]" <?=$convert==1?'checked':''?> value="1"> เข้ารหัสแบบ tis620
	    </label>
	</div>
	
	<button type="submit" class="btn btn-primary"><i class="glyphicon glyphicon-import"></i> ถอดรหัส</button>
	<?php ActiveForm::end(); ?>
    </div>
    
    <?php if(isset(Yii::$app->session['tmp_show']) && Yii::$app->session['tmp_show']){  Pjax::begin(['id'=>'tmp-person-grid-pjax']);?>
    <?= GridView::widget([
	'id' => 'tmp-person-grid',
	'panelBtn' => //Html::button(SDHtml::getBtnAdd(), ['data-url'=>Url::to(['tmp-person/create']), 'class' => 'btn btn-success btn-sm', 'id'=>'modal-addbtn-tmp-person']). ' ' .
			Html::button('<i class="glyphicon glyphicon-random"></i> ปรับปรุงทั้งหมด', ['data-url'=>Url::to(['tmp-person/import-update-all']), 'class' => 'btn btn-primary btn-sm', 'id'=>'modal-updateAll-tmp-person']).
		      ' <span><strong>จำนวนที่เลือก</strong></span>  <font color="#ff0000"><span id="cart-num">0</span></font> '.Html::button('<i class="glyphicon glyphicon-random"></i> ปรับปรุงข้อมูลตามที่เลือก', ['data-url'=>Url::to(['tmp-person/import-update']), 'class' => 'btn btn-success btn-sm', 'id'=>'modal-delbtn-tmp-person', 'disabled'=>true]),
		      
	'dataProvider' => $dataProvider,
	'filterModel' => $searchModel,
        'columns' => [
	    [
		'class' => 'yii\grid\CheckboxColumn',
		'checkboxOptions' => function($model, $key, $index, $widget) {
		    return ['class' => 'selectionTmpPersonIds', "value" => $model->cid];
		},
		'headerOptions' => ['style'=>'text-align: center;'],
		'contentOptions' => ['style'=>'width:40px;text-align: center;'],
	    ],
	    [
		'class' => 'yii\grid\SerialColumn',
		'headerOptions' => ['style'=>'text-align: center;'],
		'contentOptions' => ['style'=>'width:60px;text-align: center;'],
	    ],
	    [
		'attribute'=>'cid',
		'label'=>'CID',
		'headerOptions'=>['style'=>'text-align: center;'],
		'contentOptions'=>['style'=>'width:120px; text-align: center;'],
	    ],
            [
		'attribute'=>'name',
		'label'=>'ชื่อ',
	    ],
	    [
		'attribute'=>'surname',
		'label'=>'นามสกุล',
	    ],
	    [
		'attribute'=>'tmp_id',
		'label'=>'ข้อมูลจาก TDC',
		'filter'=>'',
		'contentOptions'=>['style'=>'width:550px; '],
	    ],
//	    [
//		'class' => 'appxq\sdii\widgets\ActionColumn',
//		'contentOptions' => ['style'=>'width:80px;text-align: center;'],
//		'template' => '{view} {update} {delete}',
//	    ],
        ],
    ]); ?>
    <?php  Pjax::end();}?>
</div>

<?=  ModalForm::widget([
    'id' => 'modal-tmp-person',
    'size'=>'modal-lg',
]);
?>

<?php  $this->registerJs("
$('#tmp-person-grid-pjax').on('click', '.select-on-check-all', function() {
    window.setTimeout(function() {
	var key = $('#tmp-person-grid').yiiGridView('getSelectedRows');
	disabledTmpPersonBtn(key.length);
    },100);
});

$('#tmp-person-grid-pjax').on('click', '.selectionTmpPersonIds', function() {
    var key = $('input:checked[class=\"'+$(this).attr('class')+'\"]');
    disabledTmpPersonBtn(key.length);
});

function disabledTmpPersonBtn(num) {
    if(num>0) {
	$('#modal-delbtn-tmp-person').attr('disabled', false);
    } else {
	$('#modal-delbtn-tmp-person').attr('disabled', true);
    }
    $('#cart-num').html(num);
}

$('#tmp-person-grid-pjax').on('click', '#modal-delbtn-tmp-person', function() {
    selectionTmpPersonGrid($(this).attr('data-url'));
});

$('#tmp-person-grid-pjax').on('click', '#modal-updateAll-tmp-person', function() {
    selectionTmpPersonGrid($(this).attr('data-url'));
});

function selectionTmpPersonGrid(url) {
	$('#modal-tmp-person .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
	$('#modal-tmp-person').modal('show');
	$.ajax({
	    method: 'POST',
	    url: url,
	    data: $('.selectionTmpPersonIds:checked[name=\"selection[]\"]').serialize(),
	    dataType: 'JSON',
	    success: function(result, textStatus) {
		if(result.status == 'success') {
		    ". SDNoty::show('result.message', 'result.status') ."
		    $.pjax.reload({container:'#tmp-person-grid-pjax'});
		    //$('#modal-tmp-person .modal-content').html(result.html);
		    $('#modal-tmp-person').modal('hide');
		    
		} else {
		    ". SDNoty::show('result.message', 'result.status') ."
		}
	    }
	});
}

function modalTmpPerson(url) {
    $('#modal-tmp-person .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
    $('#modal-tmp-person').modal('show')
    .find('.modal-content')
    .load(url);
}

");?>