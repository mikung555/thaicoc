<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\ov2\models\TmpPerson */

$this->title = Yii::t('app', 'Create Tmp Person');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Tmp People'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tmp-person-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
