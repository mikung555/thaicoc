<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\ov2\models\TmpPerson */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Tmp Person',
]) . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Tmp People'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="tmp-person-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
