<?php

namespace backend\modules\cup;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\modules\cup\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
