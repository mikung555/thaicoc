<?php
use yii\helpers\Html;
use yii\helpers\Url;
use kartik\widgets\Select2;
use yii\web\JsExpression;
use kartik\grid\GridView;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
?>
<div class="box-body" style="display: block;">
    <input type="button" value="search" name="search-cup" id="search-cup" />
<?php
    //$concatLabel = \backend\controllers\InputdataController::setFieldSearch($ezform_comp);
    $select2Option = [
        'name' => 'select2Target',
        'id' => 'select2Target',
        'value' => $modelSelectTarget['text'] ? $modelSelectTarget['text'] : null,
        'size' => Select2::MEDIUM,
        'language' => 'th',
        'options' => ['placeholder' => 'สามารถค้นได้จากรายการดังนี้ ' . $concatLabel['concat_label'], 'multiple' => ($dataComponent['tagMultiple']) ? $dataComponent['tagMultiple'] : FALSE],
        'pluginOptions' => [
            'allowClear' => false,
            'minimumInputLength' => 0,
            'ajax' => [
                'url' => Url::to(['lookup-target']),
                'type' => 'POST',
                'delay' => 500,
                'dataType' => 'json',
                'data' => new JsExpression("function(params) { $(this).text(''); return {q:params.term, search:'" . base64_encode($dataComponent['sqlSearch']) . "', ezf_id : '" . $input_ezf_id . "', ezf_comp_id : '" . $ezform_comp['comp_id'] . "'}; }"),
                            
            ],
        ],
    ];
    echo Select2::widget($select2Option);

?>
</div>

<div class="sdbox-header">
        <h3><i class="fa fa-archive" aria-hidden="true"></i> ขั้นที่ 3. ดูแฟ้มข้อมูลทั้งหมดของเป้าหมาย <?php echo $modelSelectTarget['text'] ? ': ' . $modelSelectTarget['text'] : null; ?></h3>
        
            <?php
            //\yii\helpers\VarDumper::dump($dataProvidertarget, 10, TRUE);
            $gridColumnstarget = [
                    ['class' => 'kartik\grid\SerialColumn'],
                    [
                    'attribute' => 'ชื่อเป้าหมาย',
                    'value' => function ($model, $key, $index, $widget) use ($dataComponent) {
                        if ($model->ptid || $model->target) {
                            //$user = common\models\UserProfile::findOne($model->target);
                            //echo $arr_comp_desc_field_name; echo'<hr>'; print_r($arr_comp_desc_field_name); exit;
                            $comp_name = new Dynamic();
                            $comp_name->setTableName($dataComponent['ezf_table_comp']);
                            if ($dataComponent['comp_id'] == 100000 OR $dataComponent['comp_id'] == 100001) {
                                $comp_name = $comp_name::find()->where('user_id = :user_id', [':user_id' => $model->target])->one();
                            } else if ($dataComponent['special']) {
                                $comp_name = $comp_name::find()->where('rstat <>3 AND xsourcex = :xsourcex AND ptid = :ptid', [':xsourcex' => $model->xsourcex, ':ptid' => $model->ptid])->one();
                            } else {
                                $comp_name = $comp_name::find()->where('rstat <>3 AND xsourcex = :xsourcex AND id = :target', [':xsourcex' => $model->xsourcex, ':target' => $model->target])->one();
                            }
                            $str = '';
                            foreach ($dataComponent['arr_comp_desc_field_name'] as $val) {
                                $str .= $comp_name->$val . ' ';
                            }
                            return $str;

                            //return $model->target;
                        } else
                            return 'ไม่ระบุเป้าหมาย';
                    },
                    'filterType' => GridView::FILTER_COLOR,
                    'vAlign' => 'middle',
                    'format' => 'raw',
                    'width' => '150px',
                    'noWrap' => true
                ],
                    [
                    'attribute' => 'ลักษณะหลัก',
                    'value' => function ($model, $key, $index, $widget) use ($modelEzform) {
                        if (strlen(trim($modelEzform->field_detail))) {
                            $arr_desc = explode(',', $modelEzform->field_detail);
                            $str_desc = '';
                            foreach ($arr_desc as $val) {
                                $str_desc .= $val . ', ';
                            }
                            $str_desc = substr($str_desc, 0, -2);
                            try {
                                $res = Yii::$app->db->createCommand("SELECT " . $str_desc . " FROM `" . ($modelEzform->ezf_table) . "` WHERE id = '" . $model->id . "';")->queryOne();
                            } catch (\yii\db\Exception $e) {
                                try {
                                    $res = Yii::$app->db->createCommand("SELECT " . $str_desc . " FROM `" . ($modelEzform->ezf_table) . "` WHERE user_id = '" . $model->id . "';")->queryOne();
                                } catch (\yii\db\Exception $e) {
                                    
                                }
                            }
                            $str_desc = '';
                            foreach ($res as $val) {
                                $str_desc .= $val . ' ';
                            }
                        } else {
                            $str_desc = 'ไม่ได้ระบุ';
                        }
                        return $str_desc;
                    },
                    'filterType' => GridView::FILTER_COLOR,
                    'vAlign' => 'middle',
                    'format' => 'raw',
                    'width' => '250px',
                    'noWrap' => true
                ],
                    [
                    'attribute' => 'หน่วยงาน',
                    'value' => function ($model, $key, $index, $widget) {
                        $hospital = \backend\modules\ezforms\components\EzformQuery::getHospital($model->xsourcex);
                        $html = '<span class="label label-success" data-toggle="tooltip" data-original-title="' . ($hospital['name'] . ' ต.' . $hospital['tambon'] . ' อ.' . $hospital['amphur'] . ' จ.' . $hospital['province']) . '">' . $hospital['hcode'] . '</span>';
                        return $html;
                    },
                    'filterType' => GridView::FILTER_COLOR,
                    'hAlign' => 'center',
                    'vAlign' => 'middle',
                    'format' => 'raw',
                    'width' => '50px',
                    'noWrap' => true
                ],
                    [
                    'attribute' => 'update_date',
                    'label' => 'วันที่แก้ไขล่าสุด',
                    'value' => function ($model, $key, $index, $widget) {
                        //return Html::a(Html::encode(""), '#');
                        if ($model->update_date) {
                            $date = new DateTime($model->update_date);
                            return $date->format('d/m/Y (H:i:s)');
                        }

                        $date = new DateTime($model->create_date);
                        return $date->format('d/m/Y (H:i:s)');
                    },
                    'filterType' => GridView::FILTER_COLOR,
                    'hAlign' => 'center',
                    'vAlign' => 'middle',
                    'format' => 'raw',
                    'width' => '100px',
                    'noWrap' => true
                ],
                    [
                    'attribute' => 'บันทึกโดย',
                    'value' => function ($model, $key, $index, $widget) {
                        if ($model->user_update == "") {
                            $user = common\models\UserProfile::findOne($model->user_create);
                        } else {
                            $user = common\models\UserProfile::findOne($model->user_update);
                        }
                        return '<span class="text-center fa fa-2x fa-user text-warning" data-toggle="tooltip" data-original-title="บันทึกล่าสุดโดย : ' . $user->firstname . ' ' . $user->lastname . '"></span>';

                        //return $model->firstname.' '.$model->lastname;
                        //return $model->target;
                    },
                    'filterType' => GridView::FILTER_COLOR,
                    'hAlign' => 'center',
                    'vAlign' => 'middle',
                    'format' => 'raw',
                    'width' => '50px',
                    'noWrap' => true
                ],
                    [
                    'attribute' => 'สถานะ',
                    'value' => function ($model, $key, $index, $widget) {
                        if ($model->rstat == 0) {
                            return Html::a('<i class="fa fa-pencil-square-o"></i> New Record', null, ['data-pjax' => 0, 'class' => 'text-default', 'data-toggle' => 'tooltip', 'data-original-title' => Yii::t('kvgrid', 'รอการกรอกข้อมูล')]);
                        } else if ($model->rstat == 1) {
                            return Html::a('<i class="fa fa-pencil-square-o"></i> Waiting', null, ['data-pjax' => 0, 'class' => 'text-warning', 'data-toggle' => 'tooltip', 'data-original-title' => Yii::t('kvgrid', 'ข้อมูลยังไม่ถูกส่งเข้าระบบด้วยการคลิก Submitted')]);
                        } else if ($model->rstat == 2 || $model->rstat >= 4) {
                            return Html::a('<i class="fa fa-send"></i> Submitted', null, ['data-pjax' => 0, 'class' => 'text-success', 'data-toggle' => 'tooltip', 'data-original-title' => Yii::t('kvgrid', 'ข้อมูลถูกส่งเข้าระบบเรียบร้อยแล้ว')]);
                        }

                        //return $model->firstname.' '.$model->lastname;
                        //return $model->target;
                    },
                    'filterType' => GridView::FILTER_COLOR,
                    'vAlign' => 'middle',
                    'format' => 'raw',
                    'width' => '50px',
                    'noWrap' => true
                ],
                    [
                    'attribute' => 'Action',
                    'value' => function ($model, $key, $index, $widget) use ($xsourcex, $input_comp_target, $input_ezf_id, $input_target) {
                        if ($model->xsourcex == $xsourcex) {
                            $html = Html::a('<i class="fa fa-edit"></i> ดู / แก้ไข', 'step4?comp_id_target=' . $input_comp_target . '&ezf_id=' . $input_ezf_id . '&target=' . $input_target . '&dataid=' . $model->id, ['data-pjax' => 0, 'class' => 'open-form btn btn-primary', 'data-toggle' => 'tooltip', 'data-original-title' => Yii::t('kvgrid', 'แก้ไขข้อมูลนี้')]);
                        } else {
                            $html = Html::a('<i class="fa fa-view"></i> อ่านอย่างเดียว', 'step4?comp_id_target=' . $input_comp_target . '&ezf_id=' . $input_ezf_id . '&target=' . $input_target . '&dataid=' . $model->id . '&action=read-only', ['data-pjax' => 0, 'class' => 'open-form btn btn-warning', 'data-toggle' => 'tooltip', 'data-original-title' => Yii::t('kvgrid', 'แสดงข้อมูลนี้')]);
                        }
                        return $html;
                    },
                    'hAlign' => 'center',
                    'filterType' => GridView::FILTER_COLOR,
                    'vAlign' => 'middle',
                    'format' => 'raw',
                    'width' => '50px',
                    'noWrap' => true
                ],
            ];
            //\yii\helpers\VarDumper::dump(ArrayHelper::map($dataProvider->models, 'ezf_id', 'ezf_name'),10,true);
            $gridTargetFrom = GridView::widget([
                        'dataProvider' => $dataProvidertarget,
                        'columns' => $gridColumnstarget,
                        'containerOptions' => ['style' => 'overflow: auto'], // only set when $responsive = false
                        'toolbar' => [
                                ['content' => '<button type="button" class="btn btn-success btn-flat">ฟอร์มอื่นๆ <li class="fa fa-hand-o-right"></li></button>'],
                                ['content' => Html::dropDownList('nextForm', $_GET['ezf_id'], ArrayHelper::map($dataProvider->models, 'ezf_id', 'ezf_name'), [
                                    'class' => 'form-control',
                                    'onchange' => 'step3ChangeForm($(this).val());'])
                            ],
                            '{export}',
                            '{toggleData}'
                        ],
                        'pjax' => true,
                        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-gridTargetFrom',], 'enablePushState' => false],
                        'bordered' => true,
                        'striped' => true,
                        'condensed' => true,
                        'responsiveWrap' => false,
                        'responsive' => true,
                        'hover' => True,
                        'showPageSummary' => false,
                        'showFooter' => false,
                        'resizableColumns' => true,
                        'containerOptions' => ['style' => 'overflow: auto'], // only set when $responsive = false
                        'headerRowOptions' => ['class' => 'kartik-sheet-style'],
                        'footerRowOptions' => ['class' => 'kartik-sheet-style'],
                        'filterRowOptions' => ['class' => 'kartik-sheet-style'],
                        'panel' => [
                            'type' => GridView::TYPE_PRIMARY,
                            'heading' => '',
                            'after' => false,
                            'afterOptions' => false,
                        ],
                        'persistResize' => false,
            ]);

// show EMR for component
           
  
        echo $gridTargetFrom;
        ?>



    </div>