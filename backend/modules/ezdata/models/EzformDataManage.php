<?php

namespace backend\modules\ezdata\models;

use Yii;

/**
 * This is the model class for table "ezform_data_manage".
 *
 * @property string $id
 * @property string $ezf_id
 * @property string $params
 * @property string $fsearch
 * @property integer $xsourcex
 */
class EzformDataManage extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ezform_data_manage';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ezf_id', 'params', 'fsearch'], 'required'],
            [['ezf_id', 'xsourcex'], 'integer'],
            [['detail', 'js'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ezf_id' => 'Ezf ID',
            'detail' => 'รายละเอียด (ใช้ HTML ได้)',
            'params' => 'ตัวแปรค่าที่ใช้แสดง',
            'fsearch' => 'ตัวแปรสำหรับช่วงเวลาค้นหา',
            'xsourcex' => 'Xsourcex',
            'js' =>'Java script',
        ];
    }
}
