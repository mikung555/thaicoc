<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\ezdata\models\EzformDataManageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ezform Data Manages';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ezform-data-manage-index">

    <p>
        <?= Html::a('Create Ezform Data Manage', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            [
                'attribute' => 'ezf_id',
                'label' => 'Ezform',
                'value' => function ($model){
                    $ezform = Yii::$app->db->createCommand("SELECT ezf_name FROM ezform WHERE ezf_id = :ezf_id;", [':ezf_id'=>$model->ezf_id])->queryOne();
                    return $ezform['ezf_name'];
                },
                'filter' => true,
                'headerOptions'=>['style'=>'text-align: center;'],
                'contentOptions'=>['style'=>'width:120px;text-align: left;'],
            ],
            'params:ntext',
            'fsearch',
            //'xsourcex',

            ['class' => 'yii\grid\ActionColumn',
                'header'=>'จัดการ',
                'template' => '{view} {update} {delete}',
                'buttons' => [

                    //view button
                    'view' => function ($url, $model) {

                        return Html::a('<span class="fa fa-eye"></span> View', ['/ezdata/input-form/data-management', 'ezfdata' => $model->id], [
                            'title' => Yii::t('app', 'View'),
                            'class'=>'btn btn-info btn-xs',
                            'target'=>'_blank'
                        ]);

                    },
                    'update' => function ($url, $model)  {
                        return Html::a('<span class="fa fa-edit"></span> Edit', $url, [
                            'title' => Yii::t('app', 'Update'),
                            'class'=>'btn btn-warning btn-xs',
                            //'target'=>'_blank'
                        ]);

                    },
                    'delete' => function ($url, $model) {
                        return Html::a('<span class="fa fa-edit"></span> Remove', $url, [
                            'title' => Yii::t('app', 'Remove'),
                            'class'=>'btn btn-danger btn-xs',
                            'data-method'=> 'post',
                            'data-confirm' => 'Are you sure for delete?'
                        ]);

                    },
                ],
                'contentOptions' => ['style' => 'width:250px;text-align: center;']
            ]
        ],
    ]); ?>

</div>
