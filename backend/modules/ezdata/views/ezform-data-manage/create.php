<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\ezdata\models\EzformDataManage */

$this->title = 'Create Ezform Data Manage';
$this->params['breadcrumbs'][] = ['label' => 'Ezform Data Manages', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ezform-data-manage-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'ezf_field_search' =>$ezf_field_search,
        'ezf_field_id' => $ezf_field_id,
        'model' => $model,
        'ezformItems' =>$ezformItems
    ]) ?>

</div>
