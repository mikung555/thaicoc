<?php
/**
 * Created by PhpStorm.
 * User: hackablex
 * Date: 7/26/2016 AD
 * Time: 13:23
 */

use appxq\sdii\widgets\GridView;
use yii\widgets\Pjax;
use kartik\helpers\Html;
use yii\helpers\Url;

$this->registerJS($ezfdata['js']);

$this->registerJsFile('https://maps.google.com/maps/api/js?key=AIzaSyCq1YL-LUao2xYx3joLEoKfEkLXsEVkeuk&'.http_build_query($q), [
    'position'=>\yii\web\View::POS_HEAD,
    'depends'=>'yii\web\YiiAsset',
]);
?>

<?=$ezfdata['detail'];?><hr>
<h4>เลือกช่วงเวลาแสดงข้อมูล</h4>
<div class="row">
    <form id="view-data-range" action="" method="get">
        <div class="col-md-6">
            จาก
            <?php
            echo \common\lib\damasac\widgets\DMSDateWidget::widget([
                //'model'=>$model,
                'id'=>'start_date',
                'value' => $_GET['start_date'] ? $_GET['start_date'] : '01'.(date('-m-').(date('Y')+543)),
                'name' =>'start_date',
            ]);
            ?>
        </div>
        <div class="col-md-6">
            ถึง
            <?php
            echo \common\lib\damasac\widgets\DMSDateWidget::widget([
                //'model'=>$model,
                'id'=>'end_date',
                'value' => $_GET['end_date'] ? $_GET['end_date'] : (date('d-m-').(date('Y')+543)),
                'name' =>'end_date',
            ]);
            ?>
        </div>
        <div class="col-md-12 text-right">
            <br>
            <?php
            echo ' '.Html::a('<span class="fa fa-list"></span> แสดงข้อมูลทั้งหมด',
                Url::to(['/ezdata/input-form/data-management/', 'ezfdata' => $_GET['ezfdata'], 'show'=>'all']),
                [
                    'class' => 'btn btn-danger  btn-md',
                    'type' =>'submit',
                ]);
            ?>
            <?php
            echo Html::hiddenInput('ezfdata', $_GET['ezfdata']);
            echo Html::button('<span class="fa fa-search"></span> ค้นหา', [
                'class' => 'btn btn-primary btn-md',
                'type' =>'submit',
            ]);
            ?>
        </div>
    </form>
</div>
<hr>

<?php  Pjax::begin(['id'=>'inv-person-grid-pjax']);?>
    <?php

    if($_GET['ezfdata'] == '1476696513076161100'){
        $total = Yii::$app->db->createCommand('
            SELECT COUNT(*) FROM `dep_calendar`
        ', [':status' => 1])->queryScalar();

        $dpFix = new \yii\data\SqlDataProvider([
            'sql' => 'SELECT * FROM `dep_calendar`',
            'totalCount' => $total,
            'sort' => [
                'attributes' => [
                    'hday', 'hmonth','hname'
                ],
            ],
            'pagination' => [
                'pageSize' => 50,
            ],
        ]);


        echo GridView::widget([
            'panelBtn' => '',
            'dataProvider' => $dpFix,
            'columns' => [
                [
                    'header'=>'วันที่',
                    'attribute' => 'hday',
                ],
                [
                    'header'=>'วันเดือน',
                    'attribute' => 'hmonth',
                ],
                [
                    'header'=>'วันหยุด',
                    'attribute' => 'hname',
                ]
            ],
        ]);

    }
    
    $pkJoin = 'target';    
    if($ezform['comp_id_target']) {
        $ezform_comp = Yii::$app->db->createCommand("SELECT * FROM ezform_component WHERE comp_id=:comp_id;", [':comp_id'=>$ezform['comp_id_target']])->queryOne();
        if ($ezform_comp['special'] == 1) {
            $pkJoin = 'ptid';
        }
    }
    

    $fieldSearch = $_GET['DataManagementSearch'];
    $columns = [
        [
            'class' => 'yii\grid\SerialColumn',
            'headerOptions'=>['style'=>'text-align: center;'],
            'contentOptions'=>['style'=>'width:50px;text-align: center;'],
        ],
    ];
    foreach ($fParams as $val){
        $columns[] = [
            'attribute' => $val,
            'value' =>  function($model) use($val){
                if($_GET['ezfdata']=='1476364204008401300' && in_array($val, ['hncode', 'name', 'surname'])){
                    //tb_data_1
                    $sql = "SELECT $val FROM tb_data_1 WHERE ptid = :ptid";
                    $data = Yii::$app->db->createCommand($sql, [':ptid'=>$model->ptid])->queryScalar();
                    return $data;
                }
                return $model->{$val} !='' ? $model->{$val} : '';
            },
            'filter' => count($filterType[$val]) ? (
            \kartik\widgets\Select2::widget([
                'name' => 'DataManagementSearch['.$val.']',
                'value' => is_null($fieldSearch[$val]) ? null : $fieldSearch[$val],
                'data' => $filterType[$val],
                'options' => ['placeholder' => 'Select for filter ...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ])
            ) : true,
            //'filterOptions'=> count($filterType[$val]) ? ['style'=>'width: 50px;'] : [],
            'headerOptions'=> count($filterType[$val]) ? ['style'=>'width: 50px; text-align: center;'] : ['style'=>'text-align: center;'],
            'contentOptions'=>['style'=>'text-align: left;', 'field-id'=>$val],
        ];
    }
    $columns[] = ['class' => 'yii\grid\ActionColumn',
            'header'=>'จัดการ',
            'template' => '{view} {print}',
            'buttons' => [

                //view button
                'view' => function ($url, $model) use ($ezfdata, $ezform, $pkJoin) {
                    
                    return Html::a('<span class="fa fa-edit"></span>', NULL, [
                            'data-url'=>Url::to(['/inv/inv-person/ezform-print',
                                'ezf_id'=>$ezfdata['ezf_id'],
                                'dataid'=>$model->id,
                                'comp_target'=>isset($ezform['comp_id_target'])?'':'skip',
                                'target'=>(isset($model[$pkJoin]) && $model[$pkJoin]!='' && $model[$pkJoin]!='null')?base64_encode($model[$pkJoin]):null,
                                'comp_id_target'=> isset($ezform['comp_id_target'])?$ezform['comp_id_target']:NULL,
                            ]),
                            'style'=>'cursor: pointer;',
                            'title' => Yii::t('app', 'View'),
                            'class'=>'btn btn-primary btn-xs print-ezform',
                            
                        ]);
                    
//                    return Html::a('<span class="fa fa-edit"></span>', ['/inputdata/redirect-page', 'ezf_id' => $ezfdata['ezf_id'], 'dataid'=>$model->id, 'rurl' => base64_encode(Yii::$app->request->url)], [
//                        'title' => Yii::t('app', 'View'),
//                        'class'=>'btn btn-primary btn-xs',
//                        'target'=>'_blank'
//                    ]);
		   
                },
		'print' => function ($url, $model) use ($ezfdata) {
                    return Html::a('<span class="glyphicon glyphicon-print"></span>', ['/managedata/managedata/ezform-pdf', 'ezf_id' => $ezfdata['ezf_id'], 'dataid'=>$model->id], [
                        'title' => Yii::t('app', 'View'),
                        'class'=>'btn btn-primary btn-xs',
                        'target'=>'_blank'
                    ]);
                },	
                /*
                'update' => function ($url, $model) use ($ezfdata) {
                    return Html::a('<span class="fa fa-edit"></span> Edit', ['/inputdata/redirect-page', 'ezf_id' => $ezfdata['ezf_id'], 'dataid'=>$model->id, 'rurl' => base64_encode(Yii::$app->request->url)], [
                        'title' => Yii::t('app', 'Update'),
                        'class'=>'btn btn-warning btn-xs',
                        'target'=>'_blank'
                    ]);

                },
                */
            ],
            'headerOptions'=>['style'=>'text-align: center;'],
            'contentOptions' => ['style' => 'width:100px;text-align: center;']
        ];
?>

<?php
    $url = '';
    if($ezform['comp_id_target']) {
        $url = Url::to([
            '/inputdata/step2/',
            'comp_id_target' => $ezform['comp_id_target'],
            'ezf_id' => $ezfdata['ezf_id']
        ]);
    }else{
        $url = Url::to([
            '/inputdata/insert-record/',
            'insert' => 'url',
            'ezf_id' => $ezfdata['ezf_id'],
            'target' => 'skip',
            'rurl' => base64_encode(Yii::$app->request->url)]);
    }
    
    $btn = Html::a('<i class="glyphicon glyphicon-plus"></i> เพิ่มข้อมูล', null, [
			'data-url'=>Url::to(['/inv/inv-person/ezform-print',
			    'ezf_id'=>$ezfdata['ezf_id'],
			    'end'=>1,
                            'comp_target'=>isset($ezform['comp_id_target'])?'':'skip',
			    'comp_id_target'=> isset($ezform['comp_id_target'])?$ezform['comp_id_target']:NULL,
			]),
			'class'=>'btn btn-success print-ezform',
			'style'=>'cursor: pointer;',
			'data-toggle'=>'tooltip',
			'title'=>'เพิ่มข้อมูล',
		    ]);
            
    echo GridView::widget([
        'panelBtn' => $btn,
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $columns,
]); ?>

<?php Pjax::end();?>

<?= \appxq\sdii\widgets\ModalForm::widget([
    'id' => 'modal-print',
    'size'=>'modal-lg',
    'tabindexEnable'=>false,
    'options'=>['style'=>'overflow-y:scroll;']
]);
?>

<?php 
echo \appxq\sdii\widgets\ModalForm::widget([
        'id' => 'modal-query-request',
        'size' => 'modal-lg',
        'tabindexEnable' => false,
        //'clientOptions'=>['backdrop'=>'static'],
	 'options'=>['style'=>'overflow-y:scroll;']
    ]);  
$this->registerJs("
$('.modal-lg').width('90%');

$('#inv-person-grid-pjax').on('click', '.print-ezform', function() {
    var url = $(this).attr('data-url');
    modalEzfrom(url);
});

function modalEzfrom(url) {
    $('#modal-print .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
    $('#modal-print').modal('show');
    $.ajax({
	method: 'POST',
	url: url,
	dataType: 'HTML',
	success: function(result, textStatus) {
	    $('#modal-print .modal-content').html(result);
	    return false;
	}
    });
}


");?>