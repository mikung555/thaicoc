<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 
 */
    use appxq\sdii\widgets\GridView;
    use yii\widgets\Pjax;
    use kartik\helpers\Html;
    use yii\helpers\Url;
    use kartik\select2\Select2;
    use yii\bootstrap\ActiveForm;
   

    
?>
<a href="/dpm/menu/index" class="btn btn-success" role="button"><i class="fa fa-cubes" aria-hidden="true"></i> All Module</a>
<br>
<h2 style="text-align: left;"><span style="color:"#000000";"><span lang="TH">Morbidity&Motality&nbsp;</span></h2> 
<a href='<?= Url::to(['patient/index']) ?>' class="btn btn-info" role="button"> บันทึกจำนวนผู้ป่วยใน และ Abstract ของผู้ป่วย M&M </a>
<a href='<?= Url::to(['data-managementstat/index']) ?>' class="btn btn-info" role="button"> จำนวนผู้ป่วยประจำปี M&M แจกแจงตาม Admission Discharge Morbidity Maltality และ Summary </a>
<a href='<?= Url::to(['nmm-report/index']) ?>' class="btn btn-info" role="button"> จำนวนผู้ป่วยประจำเดือน แจกแจงตาม Cause of complication และ MM Grading </a>
<hr>
<h3>จำนวนผู้ป่วยประจำปี M&M แจกแจงตาม Admission Discharge Morbidity Maltality และ Summary</h3>
<hr>

<h4>เลือกช่วงเวลาแสดงข้อมูล</h4>
<form id="view-data-rangestat" action="" method="get">
    <div class="row">
        <div class="col-md-5">
            <?php 
                echo '<label class="control-label">จำนวน</label>';
                echo Select2::widget([
                    'name' => 'typestat',
                    'data' => [
                        'admission' => "Admission",
                        'discharge' => "Discharge",
                        'morbidity' => "Morbidity", 
                        'mortality' => "Mortality", 
                        'summary' => "Summary"
                    ],
                    'value' => 'admission',
                    'options' => [
                        'placeholder' => 'Select a type ...',
                    ],
                ]);
            ?>
        </div>
        <div class="col-md-3">
            <?php 
            $yearold = date('Y',strtotime('-70 year'));
            $current_year = date('Y');
            $arrayYear=array();
            for($current_year; $current_year >= $yearold; $current_year--) {
                 $arrayYear["$current_year"]=$current_year;
            
            }
            echo '<label class="control-label">ในปี</label>';
            echo kartik\widgets\Select2::widget([
                'name' => 'year',
                'data' =>  $arrayYear,
                'value' => date("Y"),
                'options' => ['placeholder' => 'Select a year ...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
          
        ?>  
        
        </div>
        <div class="col-md-3" style="padding:24px;">
            <?php 
                echo Html::button('<span class="fa fa-search"></span> ค้นหา', [
                    'class' => 'btn btn-primary btn-md',
                    'type' =>'submit',
                ]);
            ?>
        </div>
    </div>
</form>
<hr>

<div class="row">
    <div class="col-md-12">
    <?php 
        
        if(!empty($type)){
        $titleyear = date($years)+543;
        echo '<center>';
        echo Html::label("จำนวนผู้ป่วย M&M (".$type.") ประจำปี พ.ศ.".$titleyear." ",$option,['style'=>'font-size:18px;']);
        echo '</center>';
    ?>
        <table class="table table-hover table-striped table-bordered table-responsive">
            <thead>
                <tr class="active">
                    <th style="width: 15%">Week</th>
                    <th style="width: 30%">Duration</th>
                    <?php 
                        if($type == 'summary'){ 
                        echo '<th >Admission</th><th>Discharge</th><th>Morbidity</th><th>Mortality</th>';
                        }else{ 
                        echo '<th style="width: 35%"></th><th style="width: 20%">Amount</th>'; 
                        }?>
                </tr>
            </thead>
            <tbody>
                <?php 
                $i=1;
                foreach ($query as $value) { 
                $data[$i] = \backend\controllers\DataManagementstatController::getStartAndEndDate($i,$years);
                
                echo "<tr>";
                    echo "<td>".$i."</td><td>".date('j M', strtotime($data[$i][0])).' - '.date('j M', strtotime($data[$i][1]))."</td>";
                    $progress = ($value[0]['Amount']*10)/100;
                    if($type != 'summary'){
                    echo "<td>"
                        . "<div class='progress'>"
                        . "<div class='progress-bar progress-bar-success' role='progressbar' aria-valuenow='$progress' aria-valuemin='0' aria-valuemax='100' style='width: $progress%'></div>"
                        . "</div>"
                        . "</td>";
                    }
                    echo "<td>".$value[0]['Amount']."</td>";
                    if($type == 'summary'){
                        echo "<td>".$value[1]['Amount']."</td><td>".$value[2]['Amount']."</td><td>".$value[3]['Amount']."</td>"; 
                    }
                echo "</tr>"; 
                   $i++;
                } 
                ?>
            </tbody>
        </table> 
        <?php } ?>    
    </div>    
</div>






