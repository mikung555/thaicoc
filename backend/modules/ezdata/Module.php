<?php

namespace backend\modules\ezdata;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\modules\ezdata\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
