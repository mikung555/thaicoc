<?php
namespace backend\modules\ezdata\controllers;

use backend\modules\ezforms\components\EzformQuery;
use backend\modules\ezforms\models\EzformFields;
use Yii;
use backend\models\Ezform;
use yii\data\ActiveDataProvider;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;

class InputFormController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $ezform = new ActiveDataProvider([
            'query' =>Ezform::find()->where(['category_id'=>$_GET['act'],'shared'=>1])->orderBy('ezf_name'),
        ]);
        
        return $this->render('index',['ezform'=>$ezform]);
    }
    public function actionDataManagement($ezfdata){
        Yii::$app->session['pjax_reload'] = 'inv-person-map-pjax';
        $ezfdata = Yii::$app->db->createCommand("SELECT * FROM ezform_data_manage WHERE id = :id;", [':id'=>$ezfdata])->queryOne();
        $searchModel = new \backend\models\search\DataManagementSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $ezfdata);
        $fParams = explode(',', $ezfdata['params']);

        //check component ezfrom
        $ezform = Yii::$app->db->createCommand("SELECT comp_id_target FROM ezform WHERE ezf_id = :ezf_id;", [':ezf_id'=>$ezfdata['ezf_id']])->queryOne();

        //map choice type
        foreach ($fParams as $field){
            //set value
            $ezchoice = [];

            $ezfield = Yii::$app->db->createCommand("select ezf_field_id, ezf_field_sub_id,  ezf_field_name, ezf_field_type, ezf_component from ezform_fields where ezf_id=:ezf_id and ezf_field_name=:ezf_field_name", [':ezf_id'=>$ezfdata['ezf_id'], ':ezf_field_name' => $field])->queryOne();
            if($ezfield['ezf_field_type']==4 || $ezfield['ezf_field_type']==6 ){
                $ezchoice = Yii::$app->db->createCommand("select ezf_choicevalue as value_id, ezf_choicelabel as text from ezform_choice where ezf_field_id=:ezf_field_id", [':ezf_field_id'=>$ezfield['ezf_field_id']])->queryAll();
            }
            else if($ezfield['ezf_field_type']==10){
                $ezformComponents = EzformQuery::getFieldComponent($ezfield['ezf_component']);
                $ezformComp = EzformQuery::getFormTableName($ezformComponents->ezf_id);
                $field_desc_array = explode(',', $ezformComponents->field_id_desc);
                $field_key = EzformFields::find()->select('ezf_field_name')->where(['ezf_field_id' => $ezformComponents->field_id_key])->one();

                $field_desc_list = '';
                if(count($field_desc_array)){
                    foreach ($field_desc_array as $value) {
                        if (!empty($value)) {
                            $field_desc = EzformFields::find()->where(['ezf_field_id' => $value])->one();
                            $field_desc_list .= 'COALESCE('.$field_desc->ezf_field_name.", '-'), ' ',";
                        }
                    }
                }
                $field_desc_list = substr($field_desc_list, 0 ,-6);

                //set SQL
                $sql = 'SELECT '.$field_key->ezf_field_name.' as value_id, CONCAT('.$field_desc_list.') AS text FROM '.($ezformComp->ezf_table).' WHERE 1 ';
                if($ezfield['ezf_component'] == 100000 || $ezfield['ezf_component'] == 100001){
                    $sql .=  " AND sitecode = '" . Yii::$app->user->identity->userProfile->sitecode . "'";
                }

                //if find by target
                if($ezformComponents->find_target AND $ezformComponents->special){
                    $sql .= " AND ptid = '".base64_decode($_GET['target'])."' AND rstat <> 3";
                }else if($ezformComponents->find_target){
                    $sql .= " AND target = '".base64_decode($_GET['target'])."' AND rstat <> 3";
                }else{
                    $sql .= " AND rstat <> 3";
                }

                //if find by site
                if($ezformComponents->find_bysite){
                    $sql .= " AND xsourcex = '".Yii::$app->user->identity->userProfile->sitecode."'";
                }
                $ezchoice = Yii::$app->db->createCommand($sql)->queryAll();

            }
            else if($ezfield['ezf_field_type']==231){
                $ezchoice = Yii::$app->db->createCommand("select ezf_choicevalue as value_id, ezf_choicelabel as text from ezform_choice where ezf_field_id=:ezf_field_id", [':ezf_field_id'=>$ezfield['ezf_field_sub_id']])->queryAll();
            }
            else if($ezfield['ezf_field_type']==16 || ($ezfield['ezf_field_type']==0 && $ezfield['ezf_field_sub_id'])){
                $ezchoice = [
                    '0' => [
                        'value_id' => '1',
                        'text' => 'ใช่'
                    ],
                    '1' => [
                        'value_id' => '0',
                        'text' => 'ไม่ใช่'
                    ],
                ];
            }
            $filterType[$field] = ArrayHelper::map($ezchoice, 'value_id', 'text');
        }

        //VarDumper::dump($fParams,10,true); exit;
        return $this->render('data-management',[
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'fParams' =>$fParams,
            'ezfdata' =>$ezfdata,
            'ezform' => $ezform,
            'filterType' => $filterType
        ]);
    }
    public function actionInsertSpecial($params=null)
    {
        //กำหนดค่าให้เลือกเป้าหมายเดิมหลัง insert ใหม่
        $session = Yii::$app->session;

        if($params){
            //
        }else{
            $params = [
                'comp_id' => Yii::$app->request->post('comp_id'),
                'target' => Yii::$app->request->post('target'), //cid
                'task' => Yii::$app->request->post('task'),
                'mode' => Yii::$app->request->post('mode'),
            ];
        }
	
        //final re check (task add new or add new from other site ?)
        $paramsx = self::actionRegisterFromComponent($params['comp_id'], $params['target']);
        $params['task'] = $paramsx['task'];

        $comp = EzformComponent::find()
            ->select('ezf_id, comp_id, field_search_cid')
            ->where(['comp_id' => $params['comp_id']])
            ->one();
        $ezform = Ezform::find()
            ->select('ezf_table')
            ->where(['ezf_id' => $comp->ezf_id])
            ->one();

        $result_success = [
            'status' => 'success',
            'message' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> Success!</strong> ' . Yii::t('app', 'บันทึกข้อมูลสำเร็จ.'),
        ];
        $result_error = [
            'status' => 'error',
            'message' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> Error!</strong> ' . Yii::t('app', 'ไม่สามารถดำเนินการได้ โปรดลองใหม่ภายหลัง.'),
        ];

        $cid = preg_replace("/[^0-9]/", "", $params['target']);
        if(!InvFunc::checkCid($cid) && $params['mode'] != 'gen-foreigner'){
            $result_error['message'] = '<strong><i class="glyphicon glyphicon-remove-sign"></i> Error!</strong> ' . Yii::t('app', 'ข้อมูลเลขบัตรประชาชนไม่ถูกต้อง.');
            return json_encode($result_error);
        }

        //set field cid
        $fieldCid = EzformQuery::getFieldNameByID($comp->field_search_cid);
        $fieldCid = $fieldCid->ezf_field_name;

        if ($params['task'] == 'add-new') {
            //type of special
            $id = GenMillisecTime::getMillisecTime();
            $hsitecode = Yii::$app->user->identity->userProfile->sitecode;
            $xdepartmentx = Yii::$app->user->identity->userProfile->department_area;
            $user_create = Yii::$app->user->id;
            $pid = Yii::$app->db->createCommand("SELECT MAX(CAST(hptcode AS UNSIGNED))  AS pidcode FROM `".($ezform->ezf_table)."` WHERE xsourcex = :xsourcex;", [':xsourcex'=>$hsitecode])->queryOne();
            $pid = str_pad($pid['pidcode']+1, 5, "0", STR_PAD_LEFT);
            $ptcodefull = $hsitecode.$pid;

            Yii::$app->db->createCommand()->insert($ezform->ezf_table, [
                'id' =>$id,
                'ptid' =>$id,
                'xsourcex' => $hsitecode,
                'xdepartmentx' => $xdepartmentx,
                'target' => $id,
                'user_create' => $user_create,
                'create_date' => new Expression('NOW()'),
                'sitecode' => $hsitecode,
                'ptcode' =>$pid,
                'ptcodefull' =>$ptcodefull,
                'hsitecode' =>$hsitecode,
                'hptcode' =>$pid,
                $fieldCid => $cid,
                'rstat' => '0',
            ])->execute();

            //save EMR
            $model = new EzformTarget();
            $model->ezf_id = $comp->ezf_id;
            $model->data_id = $id;
            $model->target_id = $id;
            $model->comp_id = $comp->comp_id;
            $model->user_create = Yii::$app->user->id;
            $model->create_date = new Expression('NOW()');
            $model->user_update = Yii::$app->user->id;
            $model->update_date = new Expression('NOW()');
            $model->rstat = 0;
            $model->xsourcex = $hsitecode;
            $model->save();
            //

            //set session
            $session->set('ezf_id', $comp->ezf_id);
            $session->set('inputTarget', base64_encode($id));//as base64

            //$url_redirect = Url::to(['/inputdata/step4', 'ezf_id' =>$comp->ezf_id, 'comp_id_target'=>$comp->comp_id, 'target' => base64_encode($id), 'dataid' => $id]);
            //self::redirect($url_redirect, 302);

        } else if ($params['task'] == 'add-from-site') {
            //type of special

            $id = GenMillisecTime::getMillisecTime();
            $hsitecode = Yii::$app->user->identity->userProfile->sitecode;
            $xdepartmentx = Yii::$app->user->identity->userProfile->department_area;
            $user_create = Yii::$app->user->id;
            $pid = Yii::$app->db->createCommand("SELECT MAX(CAST(hptcode AS UNSIGNED))  AS pidcode FROM `".($ezform->ezf_table)."` WHERE xsourcex = :xsourcex;", [':xsourcex'=>$hsitecode])->queryOne();
            $pid = str_pad($pid['pidcode']+1, 5, "0", STR_PAD_LEFT);
            //หาข้อมูลที่สมบูรณ์ก่อน
            $model = Yii::$app->db->createCommand("SELECT * FROM `".($ezform->ezf_table)."` WHERE  `".$fieldCid."` LIKE '" . $cid . "' AND (error IS NULL OR error = '') AND rstat <> 3 AND rstat IS NOT NULL ORDER BY update_date DESC;")->queryOne();
            //หาไม่เจอเอาข้อมูลที่แรกให้
            if(!$model['id'])  $model = Yii::$app->db->createCommand("SELECT * FROM `".($ezform->ezf_table)."` WHERE  `".$fieldCid."` LIKE '" . $cid . "' AND id=ptid AND rstat <> 3 AND rstat IS NOT NULL;")->queryOne();
            //หาไม่เจอเอาข้อมูลที่พบมาให้
            if(!$model['id'])  $model = Yii::$app->db->createCommand("SELECT * FROM `".($ezform->ezf_table)."` WHERE  `".$fieldCid."` LIKE '" . $cid . "' AND rstat <> 3 AND rstat IS NOT NULL;")->queryOne();

            //set values
            $model['id'] = $id;
            $model['xsourcex'] = $hsitecode;
            $model['xdepartmentx'] = $xdepartmentx;
            $model['user_create'] = $user_create;
            $model['create_date'] = new Expression('NOW()');
            $model['update_date'] = new Expression('NOW()');
            if($model['hncode']) $model['hncode'] = null;
            $model['tccbot'] = 0;
            $model['hsitecode'] = $hsitecode;
            $model['hptcode'] = $pid;
            $model['rstat'] = '0';
            $ptid = $model['ptid'];
            $model['id'] = $id;

            Yii::$app->db->createCommand()->insert($ezform->ezf_table, $model)->execute();

            //save EMR
            $model = new EzformTarget();
            $model->ezf_id = $comp->ezf_id;
            $model->data_id = $id;
            $model->target_id = $ptid;
            $model->comp_id = $comp->comp_id;
            $model->user_create = Yii::$app->user->id;
            $model->create_date = new Expression('NOW()');
            $model->user_update = Yii::$app->user->id;
            $model->update_date = new Expression('NOW()');
            $model->rstat = 0;
            $model->xsourcex = $hsitecode;
            $model->save();
            //

            //set session
            $session->set('ezf_id', $comp->ezf_id);
            $session->set('inputTarget', base64_encode($ptid));//as base64
        }

        if(Yii::$app->request->get('redirect')=='url')
            return self::redirect(Url::to(['/inputdata/redirect-page', 'ezf_id' => $comp->ezf_id, 'dataid' => $id]), 302);

        $result_success['dataid'] = $id;
        $result_success['ezf_id'] = $comp->ezf_id;
	$result_success['target'] = base64_encode($id);
	$result_success['comp_id_target'] = Yii::$app->request->post('comp_id');
	$result_success['cid'] = $cid;
        $result_success['hsitecode'] = $hsitecode;
        $result_success['hptcode'] = $pid;
	
	$result_success['ezf_id_main'] = Yii::$app->session['ezform_main'];
	
		return json_encode($result_success);
    }
    public static function actionRegisterFromComponent($comp_id, $cid){

        $ezform_comp = Yii::$app->db->createCommand("SELECT * FROM ezform_component WHERE comp_id=:comp_id;", [':comp_id'=>$comp_id])->queryOne();
        $out = ['results' => ['id' => '', 'text' => '']];
        $modelComp = self::actionSpecialTarget1($ezform_comp);

        $res = InvFunc::taskSpecial1($out, $cid, $modelComp['sqlSearch'], $ezform_comp['ezf_id'], $ezform_comp);
        //VarDumper::dump($res,10,true);

        if($res['results'][0]['id']=='-990'){
            $queryParams['task'] = 'add-new';
            $queryParams['comp_id'] = $comp_id;
            $queryParams['target'] = $cid;
            //VarDumper::dump($res,10,true);
        }else if($res['results'][0]['id']=='-991'){
            $queryParams['task'] = 'add-from-site';
            $queryParams['comp_id'] = $comp_id;
            $queryParams['target'] = $cid;
        }

        //if foreigner
        $cid_k = substr($cid, 0, 1) =='0' ? true : false;
        if($cid_k){
            $queryParams['mode'] = 'gen-foreigner';
        }

        return $queryParams;

    }
    public static function actionSpecialTarget1($ezform_comp)
    {
        $ezf_table = EzformQuery::getTablenameFromComponent($ezform_comp['comp_id']);

        // array("hsitecode", 'hptcode', "name", "surname");
        //$arr = explode(',', $ezform_comp['field_id_desc']);
        $concatSearch ='';
        $modelComp = [];
        $ezf_fields = Yii::$app->db->createCommand("SELECT ezf_field_name FROM ezform_fields WHERE ezf_field_id in(".$ezform_comp['field_id_desc'].");")->queryAll();
        foreach ($ezf_fields AS $val) {
            $modelComp['arr_comp_desc_field_name'][] = $val['ezf_field_name'];
            $concatSearch .= "IFNULL(`".$val['ezf_field_name']."`, ''), ' ', ";
        }
        $concatSearch = substr($concatSearch, 0, -2);

        $sqlSearch = "select ptid as id, concat(".$concatSearch.") as text FROM `".($ezf_table->ezf_table)."` WHERE 1";

        //VarDumper::dump($sqlSearch); 

        $modelComp['comp_id'] = $ezform_comp['comp_id'];
        $modelComp['concatSearch'] = $concatSearch;
        $modelComp['tagMultiple'] = false;
        $modelComp['sqlSearch'] = $sqlSearch;
        $modelComp['special'] = true;
        //ใช้แสดงชื่อเป้าหมาย ขั้นตอนที่ 3
        $modelComp['ezf_table_comp'] = $ezf_table->ezf_table;

        $modelComp['comp_key_name'] = 'ptid';
        return $modelComp;
    }
    //เป้าหมาย (พิเศษ)
    
    
    public function actionRedirectPage(){
       
            $comp_id_target = Yii::$app->request->get('comp_id_target');
            $ezf_id = Yii::$app->request->get('ezf_id');
	    
            $comp = EzformComponent::find()
                ->where(['comp_id' => $comp_id_target])
                ->andWhere('comp_id <> 100000 AND comp_id <> 100001')
                ->one();

            $ezform = Yii::$app->db->createCommand("SELECT comp_id_target FROM ezform WHERE ezf_id = :ezf_id;", [':ezf_id' => $comp->ezf_id])->queryOne();
            $comp_id = $ezform['comp_id_target'];

//	    $insertRecord = self::insertRecord([
//		'ezf_id'=>$comp->ezf_id,
//		'comp_id_target'=>$comp_id_target,
//	    ]);
		$table_name = \backend\modules\ezforms\components\EzformQuery::getFormTableName($ezf_id);
	    
                $model = new \backend\modules\ezforms\models\EzformDynamic($table_name->ezf_table);
	    
	    
//		$xsourcex = Yii::$app->user->identity->userProfile->sitecode;
//		$genid = \common\lib\codeerror\helpers\GenMillisecTime::getMillisecTime();
//		$target = $genid;
//		$ezform = EzformQuery::getTablenameFromComponent($comp_id_target);
//		$targetx = EzformQuery::getIDkeyFromtable($ezform->ezf_table, $target);
//		$pid = Yii::$app->db->createCommand("SELECT MAX(CAST(hptcode AS UNSIGNED)) AS pidcode FROM $ezform->ezf_table WHERE xsourcex = '$xsourcex' ORDER BY id DESC LIMIT 1;")->queryOne();
//		
//		$pid = str_pad($pid['pidcode']+1, 5, "0", STR_PAD_LEFT);
//		
//		$id = $genid;
//		
//                $model->id = $genid;
//                $model->rstat = 0;
//                $model->user_create = Yii::$app->user->id;
//                $model->create_date = new Expression('NOW()');
//                $model->user_update = Yii::$app->user->id;
//                $model->update_date = new Expression('NOW()');
//                $model->target = $genid;
//                $model->xsourcex = $xsourcex;//hospitalcode
//		$model->hsitecode = $xsourcex;
//		$model->hptcode = $pid;
//		$model->sitecode = $xsourcex;
//		$model->ptid = $genid;
//		$model->ptcode = $pid;
//		$model->ptcodefull = $xsourcex.$pid;
//		
//		if ($model->save()) {
//		    //save EMR
//		    $modelTarget = new \backend\models\EzformTarget();
//		    $modelTarget->ezf_id = $ezf_id;
//		    $modelTarget->data_id = $genid;
//		    $modelTarget->target_id = $genid;
//		    $modelTarget->comp_id = $comp_id_target;
//		    $modelTarget->user_create = Yii::$app->user->id;
//		    $modelTarget->create_date = new Expression('NOW()');
//		    $modelTarget->user_update = Yii::$app->user->id;
//		    $modelTarget->update_date = new Expression('NOW()');
//		    $modelTarget->rstat = 0;
//		    $modelTarget->xsourcex = $xsourcex;
//		    $modelTarget->save();
//		    
//		    //
//		}
	    
	    header('Access-Control-Allow-Origin: *');
            header("content-type:text/javascript;charset=utf-8");
            echo json_encode([
		'ezf_id'=>$ezf_id,
		'comp_id_target'=>$comp_id_target,
		//'dataid'=>$genid,
		//'target'=>  base64_encode($genid),
		'ezf_id_main' => Yii::$app->session['ezform_main'],
		    ]);
	    

    }    
     
    public function actionRegFromTdc(){
        $sitecode = Yii::$app->user->identity->userProfile->sitecode;

        if (isset($_POST['findbot'])) {
            $cid = $_POST['findbot']['cid'];
            $key = $_POST['findbot']['key'];
            $convert = isset($_POST['findbot']['convert'])?$_POST['findbot']['convert']:0;
            $comp_id = $_POST['comp_id'];

            $checkCid = \backend\modules\ovcca\classes\OvccaFunc::check_citizen($cid);
            if($checkCid){

                $tdc = InvFunc::getPersonTdcOneByCid($sitecode, $cid, $key, $convert);
                //ดึงข้อมูลจาก tdc
                //เช็คความถูกต้องของข้อมูล
                //นำเข้าข้อมูล

                if(($tdc)){
                    if(\backend\modules\ovcca\classes\OvccaFunc::check_citizen($tdc['CID'])){
                        $checkthaiword = trim(\backend\modules\ovcca\classes\OvccaFunc::checkthai($tdc['Name']));

                        if ($checkthaiword  != '') {

                            if($_POST['import_botconfirm']) {
                                $r = InvFunc::importDataFromTdc($sitecode, $cid, $tdc, $comp_id);
                                if (count($r)) {
                                    $arr = [];
                                    $message = "นำเข้าข้อมูลแล้ว <code>เลขบัตรประชาชน: {$tdc['CID']} ชื่อ: {$tdc['Pname']}{$tdc['Name']} {$tdc['Lname']}</code>";
                                    $arr['message'] = $message;
                                    $arr['status'] = 'success';
                                    $arr['import'] = 'success';
                                    return json_encode(array_merge($arr, $r));
                                } else {
                                    $arr = [];
                                    $message = "ไม่สามารถนำเข้าข้อมูลเข้าสู่ระบบ <code>เลขบัตรประชาชน: {$tdc['CID']} ชื่อ: {$tdc['Pname']}{$tdc['Name']} {$tdc['Lname']}</code>";
                                    $arr['message'] = $message;

                                    return json_encode($arr);
                                }
                            }
                            $arr = [];
                            $message = "พบข้อมูล <code>เลขบัตรประชาชน: {$tdc['CID']}</code> ชื่อ: {$tdc['Pname']}{$tdc['Name']} {$tdc['Lname']}";
                            $arr['message'] = $message;
                            $arr['status'] = 'success';
                            return json_encode($arr);

                        } else {
                            $arr = [];
                            $message = "กรณีชื่อ-สกุล อ่านไม่ออกให้เข้ารหัสแบบ tis620";
                            $arr['message'] = $message;
                            return json_encode($arr);
                        }
                    } else {
                        $arr = [];
                        $message = "Convert ข้อมูลไม่ได้ หรือ เลขบัตรประชาชน ไม่ถูกต้องกรุณาใส่ Key ใหม่เพื่อถอดรหัสให้ถูกต้อง";
                        $arr['message'] = $message;
                        return json_encode($arr);
                    }

                } else {
                    $arr = [];
                    $arr['message'] = 'ไม่พบข้อมูลใน TDC';
                    return json_encode($arr);
                }

            } else {
                $arr = [];
                $arr['message'] = 'เลขบัตรประชาชนไม่ถูกต้อง';
                return json_encode($arr);
            }


        }

        return $this->renderAjax('_form_regtdc', ['queryParams'=>$_POST]);
    }

    

    

     
    public function actionEzformEmr() {
	    $ezf_id = isset($_GET['ezf_id'])?$_GET['ezf_id']:0;
	    $dataid = isset($_GET['dataid'])?$_GET['dataid']:NULL;
	    $target = isset($_GET['target'])?$_GET['target']:NULL;
	    $comp_id_target = isset($_GET['comp_id_target'])?$_GET['comp_id_target']:NULL;
	    
	    try {
		$dataProvidertarget = \backend\models\InputDataSearch::searchTarget($ezf_id, $target, 'draft', $comp_id_target);
		
		$modelEzform = Ezform::find()
		->where('ezform.ezf_id=:ezf_id', [':ezf_id'=>$ezf_id])
		->one();
		
		$ezform_comp = Yii::$app->db->createCommand("SELECT * FROM ezform_component WHERE comp_id=:comp_id;", [':comp_id'=>$comp_id_target])->queryOne();

		$dataComponent = [];
		if(isset($comp_id_target)){
		    if ($ezform_comp['special'] == 1) {
			//หาเป้าหมายพิเศษประเภทที่ 1 (CASCAP) (Thaipalliative) (Ageing)
			
			$dataComponent = InvFunc::specialTarget1($ezform_comp);
			
			//$dataProvidertarget = \backend\models\InputDataSearch::searchEMR($target);
		    } else {
			//หาแบบปกติ
			$dataComponent = InvFunc::findTargetComponent($ezform_comp);
			
		    }
		} else {
		    //target skip
		    $dataComponent['sqlSearch'] = '';
		    $dataComponent['tagMultiple'] = FALSE;
		    $dataComponent['ezf_table_comp'] = '';
		    $dataComponent['arr_comp_desc_field_name'] = '';
		}
		
		
		return $this->renderAjax('_ezform_emr', [
		    'dataProvidertarget'=>$dataProvidertarget,
		    'ezf_id'=>$ezf_id,
		    'dataid'=>$dataid,
		    'target'=>$target,
		    'comp_id_target'=>$comp_id_target,
		    'dataComponent'=>$dataComponent,
		    'modelEzform' =>$modelEzform,
		]);

	    } catch (\yii\db\Exception $e) {
		
	    }
	    
    }
    
    public static function actionLookupTarget()
    {
        $sitecode = Yii::$app->user->identity->userProfile->sitecode;
        
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        $string_q = Yii::$app->request->post('q');
        $table = Yii::$app->request->post('table');
        $string_decode = base64_decode(Yii::$app->request->post('search')); //sql
        $ezf_id = Yii::$app->request->post('ezf_id');
        $ezf_comp = \backend\modules\component\models\EzformComponent::find()
            ->where(['comp_id' => Yii::$app->request->post('ezf_comp_id')])
            ->one();
    
        //หากมีการกำหนด special component
        if ($ezf_comp['special'] == 1) {
            $out = InvFunc::taskSpecial1($out, $string_q, $string_decode, $ezf_id, $ezf_comp);
        } else {
            //ถ้าไม่มีการกำหนด ให้ค้นหาแบบธรรมดา ซ้ำๆตั้ง
            //\appxq\sdii\utils\VarDumper::dump($table,true,false);
            $ortQuery = Yii::$app->db->createCommand(str_replace('$q', $string_q, $string_decode). " AND $table.xsourcex = '$sitecode'  AND $table.rstat not in(0, 3) LIMIT 0,10");
            $data = $ortQuery->queryAll();
            $out['results'] = array_values($data);

            if (!$ortQuery->query()->count())
                $out['results'] = [['id' => '-992', 'text' => 'ไม่พบคำที่ค้นหา! ลองค้นคำอื่นๆ (คลิกที่นี่ถ้าต้องการเพิ่มเป้าหมายใหม่)']];
        }

        echo json_encode($out);
        return;
    }
    
    public function actionEzformSave() {
	if (Yii::$app->getRequest()->isAjax) {
	    
	    $id = isset($_GET['id'])?$_GET['id']:0;
	    $ezf_id = isset($_GET['ezf_id'])?$_GET['ezf_id']:0;
	    $dataid = isset($_GET['dataid']) && $_GET['dataid']>0?$_GET['dataid']:0;
	    $target = isset($_GET['target']) && $_GET['target']!=''?$_GET['target']:'';
	    $comp_id_target = isset($_GET['comp_id_target'])?$_GET['comp_id_target']:'';
	    $comp_target = isset($_GET['comp_target'])?$_GET['comp_target']:'';
            $dataset = isset($_GET['dataset'])?$_GET['dataset']:null;
            
	    $target_decode = $target!=''?base64_decode($target):0;
	    
	    $xsourcex = Yii::$app->user->identity->userProfile->sitecode;
	    $rstat = $_POST['submit'];
	    
	    $model_fields = \backend\modules\ezforms\components\EzformQuery::getFieldsByEzf_id($ezf_id);
            $model_form = \backend\modules\ezforms\components\EzformFunc::setDynamicModelLimit($model_fields);
            $table_name = \backend\modules\ezforms\components\EzformQuery::getFormTableName($ezf_id);
            
	    
	    $model_form->attributes = $_POST['SDDynamicModel'];

	    $model = new \backend\modules\ezforms\models\EzformDynamic($table_name->ezf_table);
	    
	    
	    
	    
	    if (isset($_POST['SDDynamicModel'])) {
                if($id==0 && $target_decode>0){
		//insert new and set $id
		
                $genid = \common\lib\codeerror\helpers\GenMillisecTime::getMillisecTime();
		
		$ezform = EzformQuery::getTablenameFromComponent($comp_id_target);
		$targetx = EzformQuery::getIDkeyFromtable($ezform->ezf_table, $target_decode);
		$pid = Yii::$app->db->createCommand("SELECT MAX(CAST(hptcode AS UNSIGNED)) AS pidcode FROM $ezform->ezf_table WHERE xsourcex = '$xsourcex' ORDER BY id DESC LIMIT 1;")->queryOne();
		
		$pid = str_pad($pid['pidcode']+1, 5, "0", STR_PAD_LEFT);
		
		$id = $genid;
		
                $model->id = $genid;
                $model->rstat = 0;
                $model->user_create = Yii::$app->user->id;
                $model->create_date = new Expression('NOW()');
                $model->user_update = Yii::$app->user->id;
                $model->update_date = new Expression('NOW()');
                $model->target = $target_decode;
                $model->xsourcex = $xsourcex;//hospitalcode
		$model->hsitecode = $xsourcex;
		$model->hptcode = $pid;
		$model->sitecode = $targetx['sitecode'];
		$model->ptid = $targetx['ptid'];
		$model->ptcode = $targetx['ptcode'];
		$model->ptcodefull = $targetx['sitecode'].$targetx['ptcode'];
		
		if ($model->save()) {
		    //save EMR
		    $modelTarget = new \backend\models\EzformTarget();
		    $modelTarget->ezf_id = $ezf_id;
		    $modelTarget->data_id = $genid;
		    $modelTarget->target_id = $target_decode;
		    $modelTarget->comp_id = $comp_id_target;
		    $modelTarget->user_create = Yii::$app->user->id;
		    $modelTarget->create_date = new Expression('NOW()');
		    $modelTarget->user_update = Yii::$app->user->id;
		    $modelTarget->update_date = new Expression('NOW()');
		    $modelTarget->rstat = 0;
		    $modelTarget->xsourcex = $xsourcex;
		    $modelTarget->save();
		    
		    //
		}
	    } elseif($id==0 && $target_decode==0){
		$genid = \common\lib\codeerror\helpers\GenMillisecTime::getMillisecTime();
		$target = $genid;
		$ezform = EzformQuery::getTablenameFromComponent($comp_id_target);
		//$targetx = EzformQuery::getIDkeyFromtable($ezform->ezf_table, $target);
		$pid = Yii::$app->db->createCommand("SELECT MAX(CAST(hptcode AS UNSIGNED)) AS pidcode FROM $ezform->ezf_table WHERE xsourcex = '$xsourcex' ORDER BY id DESC LIMIT 1;")->queryOne();
		
		$pid = str_pad($pid['pidcode']+1, 5, "0", STR_PAD_LEFT);
		
		
		
                $model->id = $genid;
                $model->rstat = 0;
                $model->user_create = Yii::$app->user->id;
                $model->create_date = new Expression('NOW()');
                $model->user_update = Yii::$app->user->id;
                $model->update_date = new Expression('NOW()');
                $model->target = $genid;
                $model->xsourcex = $xsourcex;//hospitalcode
		$model->hsitecode = $xsourcex;
		$model->hptcode = $pid;
		$model->sitecode = $xsourcex;
		$model->ptid = $genid;
		$model->ptcode = $pid;
		$model->ptcodefull = $xsourcex.$pid;
		
		if ($model->save()) {
                    $id = $genid;
                    $target = base64_encode($genid);
                    $target_decode = $genid;

		    //save EMR
		    $modelTarget = new \backend\models\EzformTarget();
		    $modelTarget->ezf_id = $ezf_id;
		    $modelTarget->data_id = $genid;
		    $modelTarget->target_id = $genid;
		    $modelTarget->comp_id = $comp_id_target;
		    $modelTarget->user_create = Yii::$app->user->id;
		    $modelTarget->create_date = new Expression('NOW()');
		    $modelTarget->user_update = Yii::$app->user->id;
		    $modelTarget->update_date = new Expression('NOW()');
		    $modelTarget->rstat = 0;
		    $modelTarget->xsourcex = $xsourcex;
		    $modelTarget->save();
		    
		    //
		}
            } elseif($id==0 && $comp_target == 'skip'){
                $id = InvFunc::insertRecord([
                    'ezf_id'=>$ezf_id,
                    'target'=>'skip',
                    'comp_id_target'=>NULL,
                ]);
                $dataid=$id;
            } 
            
            Yii::$app->response->format = Response::FORMAT_JSON;
		
            $modelOld = $model->find()->where('id = :id', ['id' => $id])->One();

	    //$model_form->attributes = $modelOld->attributes;
	    $model_form->attributes = $_POST['SDDynamicModel'];

            //if click final save (rstat not change)
	    
	    //VarDumper::dump($modelOld->attributes,10,true);
	    //VarDumper::dump($model_form->attributes,10,true);
	    $model->attributes = $modelOld->attributes;
	    
	    if($rstat != 3){
                $model->attributes = $model_form->attributes;
            }
	    
	    $model->hsitecode = $modelOld->hsitecode;
	    $model->hptcode = $modelOld->hptcode;
	    $model->sitecode = $modelOld->sitecode;
	    $model->ptid = $modelOld->ptid;
	    $model->ptcode = $modelOld->ptcode;
	    $model->ptcodefull = $modelOld->ptcodefull;
	    $model->user_create = $modelOld->user_create;
	    $model->create_date = $modelOld->create_date;
	    $model->error = $modelOld->error;
	    $model->xdepartmentx = $modelOld->xdepartmentx;
	    
	    
	    $model->id = $id; //ห้ามเอาออก ไม่งั้นจะ save ไม่ได้
	    $model->rstat = $rstat;
	    $model->xsourcex = $xsourcex;
	    //$model->create_date = new Expression('NOW()');
	    $model->user_update = Yii::$app->user->id;
	    $model->update_date = new Expression('NOW()');
	    //echo $rstat;
            
	    $model->target = $target_decode==0?'':$target_decode;
            
	    foreach ($model_fields as $key => $value) {
                
                    if ($value['ezf_field_type'] == 7 || $value['ezf_field_type'] == 253) {
                        // set Data Sql Format
                        $data = $model[$value['ezf_field_name']];
                        if($data+0) {
                            $explodeDate = explode('/', $data);
                            $formateDate = ($explodeDate[2] - 543) . '-' . $explodeDate[1] . '-' . $explodeDate[0];
                            $model->{$value['ezf_field_name']} = $formateDate;
                        }else{
                            $model->{$value['ezf_field_name']} = new Expression('NULL');
                        }
                    }
                    else if ($value['ezf_field_type'] == 10) {
                        if (count($model->{$value['ezf_field_name']}) > 1)
                            $model->{$value['ezf_field_name']} = implode(',', $model->{$value['ezf_field_name']});
                    }
                    else if ($value['ezf_field_type'] == 14) {
                       
                        if (isset($_FILES['SDDynamicModel']['name'][$value['ezf_field_name']]) && $_FILES['SDDynamicModel']['name'][$value['ezf_field_name']] !='' && is_array($_FILES['SDDynamicModel']['name'][$value['ezf_field_name']])) {
                            $fileItems = $_FILES['SDDynamicModel']['name'][$value['ezf_field_name']];
                            $fileType = $_FILES['SDDynamicModel']['type'][$value['ezf_field_name']];
                            $newFileItems = [];
                            $action = false;
                            foreach ($fileItems as $i => $fileItem) {
                                if($fileItem!=''){
                                    $action = true;
                                    $fileArr = explode('/', $fileType[$i]);
                                    if (isset($fileArr[1])) {
                                        $fileBg = $fileArr[1];

                                        //$newFileName = $fileName;
                                        $newFileName = $value['ezf_field_name'].'_'.($i+1).'_'.$model->id.'_'.date("Ymd_His").(microtime(true)*10000) . '.' . $fileBg;
                                        //chmod(Yii::$app->basePath . '/../backend/web/fileinput/', 0777);
                                        $fullPath = Yii::$app->basePath . '/../backend/web/fileinput/' . $newFileName;
                                        $fieldname = 'SDDynamicModel[' . $value['ezf_field_name'] . '][' . $i . ']';

                                        $file = UploadedFile::getInstanceByName($fieldname);
                                        $file->saveAs($fullPath);
                                        
					if($file){
					    $newFileItems[] = $newFileName;

					    //add file to db
					    $file_db = new \backend\modules\ezforms\models\FileUpload();
					    $file_db->tbid = $model->id;
					    $file_db->ezf_id = $value['ezf_id'];
					    $file_db->ezf_field_id = $value['ezf_field_id'];
					    $file_db->file_active = 0;
					    $file_db->file_name = $newFileName;
					    $file_db->file_name_old = $fileItem;
					    $file_db->target = ($model->ptid ? $model->ptid :  $model->target).'';
					    $file_db->save();
					}
                                    }
                                }
                            }
                            $res = Yii::$app->db->createCommand("SELECT `" . $value['ezf_field_name'] . "` as filename FROM `" . $table_name->ezf_table . "` WHERE id = :dataid", [':dataid' => $_POST['id']])->queryOne();
                            if($action){
                                $model->{$value['ezf_field_name']} = implode(',', $newFileItems);

//				if($res){
//				    $res_items = explode(',', $res['filename']);
//
//				    foreach ($res_items as $dataTmp) {
//					@unlink(Yii::$app->basePath . '/../backend/web/fileinput/' . $dataTmp);
//				    }
//				}
                            } else {
                                if($res){
                                    $model->{$value['ezf_field_name']} = $res['filename'];
                                }
                            }

                        } else {
                            $res = Yii::$app->db->createCommand("SELECT `" . $value['ezf_field_name'] . "` as filename FROM `" . $table_name->ezf_table . "` WHERE id = :dataid", [':dataid' => $_POST['id']])->queryOne();
                            $model->{$value['ezf_field_name']} = $res['filename'];
                        }
                    }
                    else if ($value['ezf_field_type'] == 24) {

                        if (stristr($model[$value['ezf_field_name']], 'tmp.png') == TRUE) {
                            //set data Drawing
                            $fileArr = explode(',', $model[$value['ezf_field_name']]);
                            $fileName = $fileArr[0];
                            $fileBg = $fileArr[1];
                            $newFileName = $fileName;
                            $newFileBg = $fileBg;
                            $nameEdit = false;
                            $bgEdit = false;
                            if (stristr($fileName, 'tmp.png') == TRUE) {
                                $nameEdit = true;
                                $newFileName = date("Ymd_His").(microtime(true)*10000) . '.png';
                                @copy(Yii::$app->basePath . '/../backend/web/drawing/' . $fileName, Yii::$app->basePath . '/../storage/web/drawing/data/' . $newFileName);
                                @unlink(Yii::$app->basePath . '/../backend/web/drawing/' . $fileName);
                            }
                            if (stristr($fileBg, 'tmp.png') == TRUE) {
                                $bgEdit = true;
                                $newFileBg = 'bg_' . date("Ymd_His").(microtime(true)*10000) . '.png';
                                @copy(Yii::$app->basePath . '/../storage/web/drawing/' . $fileBg, Yii::$app->basePath . '/../storage/web/drawing/bg/' . $newFileBg);
                                @unlink(Yii::$app->basePath . '/../storage/web/drawing/' . $fileBg);
                            }

                            $model[$value['ezf_field_name']] = $newFileName . ',' . $newFileBg;
                            $modelTmp = EzformQuery::getDynamicFormById($table_name->ezf_table, $_POST['id']);

                            if (isset($modelTmp['id'])) {
                                $fileArr = explode(',', $modelTmp[$value['ezf_field_name']]);
                                if (count($fileArr) > 1) {
                                    $fileName = $fileArr[0];
                                    $fileBg = $fileArr[1];
                                    if ($nameEdit) {
                                        @unlink(Yii::$app->basePath . '/../storage/web/drawing/data/' . $fileName);
                                    }
                                    if ($bgEdit) {
                                        @unlink(Yii::$app->basePath . '/../storage/web/drawing/bg/' . $fileBg);
                                    }
                                }
                            }
                        }
                    } else if ($value['ezf_field_type'] == 30) {
			$fileName = $model[$value['ezf_field_name']];
			$newFileName = $fileName;
                        $nameEdit = false;
			
			$foder = 'sddynamicmodel-'.$value['ezf_field_name'].'_'.Yii::$app->user->id;
			
			if (stristr($fileName, 'fileNameAuto') == TRUE) {
			    $nameEdit = true;
			    $newFileName = date("Ymd_His").(microtime(true)*10000) . '.mp3';
			    @copy(Yii::$app->basePath . "/../storage/web/audio/$foder/" . $fileName, Yii::$app->basePath . '/../storage/web/audio/' . $newFileName);
			    @unlink(Yii::$app->basePath . "/../storage/web/audio/$foder/" . $fileName);
			}
			
			$model[$value['ezf_field_name']] = $newFileName;
			
			$modelTmp = EzformQuery::getDynamicFormById($table_name->ezf_table, $_POST['id']);

			if (isset($modelTmp['id'])) {
			    
				$fileName = $modelTmp[$value['ezf_field_name']];
				//sddynamicmodel-
				if ($nameEdit) {
				    @unlink(Yii::$app->basePath . "/../storage/web/audio/" . $fileName);
				}
			}
		    }
                }
		//track change
                
                //send email to user key
                
		if ($model->update()) {
                    //save reverse
                    
                    //save EMR
                    
                    //save sql log
                    
                    //save track change
                    
                    //mailing
                    \backend\modules\ezforms\controllers\EzformController::actionMailing($ezf_id, $model->id);
                    
                    //update register for cascap
                    
                    //update cca01 for cascap
                    
		    $result = [
			'status' => 'success',
			'action' => 'update',
			'message' => SDHtml::getMsgSuccess() . Yii::t('app', 'Data completed.'),
			'data' => $model,
                        'id'=>$id,
                        'target'=>$target,
		    ];
		    return $result;
		} else {
		    $result = [
			'status' => 'error',
			'message' => SDHtml::getMsgError() . Yii::t('app', 'Can not update the data.'),
			'data' => $model,
		    ];
		    return $result;
		}
	    } 
	} else {
	    throw new NotFoundHttpException('Invalid request. Please do not repeat this request again.');
	}
    }
    
    public function actionEzformMian() {
	    $readonly = isset($_GET['readonly'])?$_GET['readonly']:0;
	    $ezf_id = isset($_GET['ezf_id'])?$_GET['ezf_id']:0;
	    $dataid = isset($_GET['dataid']) && $_GET['dataid']>0?$_GET['dataid']:0;
	    $target = isset($_GET['target']) && $_GET['target']!=''?$_GET['target']:'';
	    $id = isset($_GET['id'])?$_GET['id']:0;
	    $comp_id_target = isset($_GET['comp_id_target'])?$_GET['comp_id_target']:0;
            $comp_target = isset($_GET['comp_target'])?$_GET['comp_target']:'';
            $dataset = isset($_GET['dataset'])?$_GET['dataset']:null;
	    $target_decode = base64_decode($target);
	    $targetOld = $target_decode;
            //\appxq\sdii\utils\VarDumper::dump($id);
	    try {
		
		
		$modelform = \backend\modules\ezforms\models\Ezform::find()->where('ezf_id = :ezf_id', [':ezf_id' => $ezf_id])->one();
		
		$modelfield = \backend\modules\ezforms\models\EzformFields::find()
			->where('ezf_id = :ezf_id', [':ezf_id' => $ezf_id])
			->orderBy(['ezf_field_order' => SORT_ASC])
			->all();
		
		$table = $modelform->ezf_table;
		
		$model_gen = \backend\modules\ezforms\components\EzformFunc::setDynamicModelLimit($modelfield);
		$ezform_comp = Yii::$app->db->createCommand("SELECT * FROM ezform_component WHERE comp_id=:comp_id;", [':comp_id'=>$comp_id_target])->queryOne();
		//\appxq\sdii\utils\VarDumper::dump(!(isset($target) && $target!=''));
		if($target==''){
		    $modelezform_target = \backend\modules\ezforms\models\Ezform::find()->where('ezf_id = :ezf_id', [':ezf_id' => $ezform_comp['ezf_id']])->one();

		    $model = new \backend\modules\ezforms\models\EzformDynamic($modelezform_target->ezf_table);

		    $modelOld = $model->find()->where('id = :id', ['id' => $id])->One();
		    
		    $ftarget = 'id';
		    if ($ezform_comp['special'] == 1) {
			$ftarget = 'ptid';
		    } 
		    $target_decode = $modelOld[$ftarget];
		    
		    $target = base64_encode($target_decode);
		}
		
		if(isset($ezf_id) && isset($comp_id_target) && isset($target) && $dataid==0) {
		    $dataid = InvFunc::insertRecord([
			'ezf_id'=>$ezf_id,
			'target'=>$target,
			'comp_id_target'=>$comp_id_target,
		    ]);
		    
		}
		
		$dataComponent = [];
		if(isset($comp_id_target)){
		    if ($ezform_comp['special'] == 1) {
			//หาเป้าหมายพิเศษประเภทที่ 1 (CASCAP) (Thaipalliative) (Ageing)
			
			$dataComponent = InvFunc::specialTarget1($ezform_comp);
			
			//$dataProvidertarget = \backend\models\InputDataSearch::searchEMR($target);
		    } else {
			//หาแบบปกติ
			$dataComponent = InvFunc::findTargetComponent($ezform_comp);
			
		    }
		} else {
		    //target skip
		    $dataComponent['sqlSearch'] = '';
		    $dataComponent['tagMultiple'] = FALSE;
		    $dataComponent['ezf_table_comp'] = '';
		    $dataComponent['arr_comp_desc_field_name'] = '';
		}
		
		if(isset($dataid) && $dataid>0){
		    $model = new \backend\models\Tbdata();
		    $model->setTableName($table);

		    $query = $model->find()->where('id=:id', [':id'=>$dataid]);

		    $data = $query->one();

		    $model_gen->attributes = $data->attributes;
		}

                if(isset($dataset)  && !empty($dataset)) {
                    $dataSet = Json::decode(base64_decode($dataset));
                    foreach ($dataSet as $key=>$value){
                        $model_gen->{$key} = $value;
                    }
                }
                //ฟอร์มบันทึก '.$modelform['ezf_name'].'
		$prefix = '<div class="panel">
		    <div class="panel-heading clearfix" id="panel-ezform-box">
			<h3 class="panel-title"><a class="pull-right" href="/inputdata/printform?dataid='.$dataid.'&amp;id='.$ezf_id.'&amp;print=1" target="_blank"><span class="fa fa-print fa-2x"></span></a>'.'</h3>
		</div>
		    <div class="panel-body" id="panel-ezform-body">';
		                

		return $prefix.$this->renderAjax('_ezform_widget', [
		    'modelform'=>$modelform,
		    'modelfield'=>$modelfield,
		    'model_gen'=>$model_gen,
		    'ezf_id'=>$ezf_id,
		    'dataid'=>$dataid,
		    'target'=>$target,
		    'comp_id_target'=>$comp_id_target,
		    'formname'=>'ezform-main',
		    'end'=>1,
                    'readonly'=>$readonly,
                    'comp_target'=>$comp_target,
                    'dataset'=>$dataset,
		]).'</div></div>';

	    } catch (\yii\db\Exception $e) {
		
	    }
	    
    }
    
    public function actionEzformTarget() {
	    $readonly = isset($_GET['readonly'])?$_GET['readonly']:0;
	    $end = isset($_GET['end'])?$_GET['end']:0;
	    $ezf_id = isset($_GET['ezf_id'])?$_GET['ezf_id']:0;
	    $dataid = isset($_GET['dataid']) && $_GET['dataid']>0?$_GET['dataid']:0;
	    $target = isset($_GET['target']) && $_GET['target']!=''?$_GET['target']:'';
	    $comp_id_target = isset($_GET['comp_id_target'])?$_GET['comp_id_target']:0;
            $comp_target = isset($_GET['comp_target'])?$_GET['comp_target']:'';
            $dataset = isset($_GET['dataset'])?$_GET['dataset']:null;
	    $target_decode = base64_decode($target);
	    $targetOld = $target_decode;
	    try {
		
		$ezform_comp = Yii::$app->db->createCommand("SELECT * FROM ezform_component WHERE comp_id=:comp_id;", [':comp_id'=>$comp_id_target])->queryOne();
		$modelform = \backend\modules\ezforms\models\Ezform::find()->where('ezf_id = :ezf_id', [':ezf_id' => $ezform_comp['ezf_id']])->one();
		
		$modelfield = \backend\modules\ezforms\models\EzformFields::find()
			->where('ezf_id = :ezf_id', [':ezf_id' => $ezform_comp['ezf_id']])
			->orderBy(['ezf_field_order' => SORT_ASC])
			->all();
		
		$table = $modelform->ezf_table;
		
		$model_gen = \backend\modules\ezforms\components\EzformFunc::setDynamicModelLimit($modelfield);
		
		$ftarget='id';
                $get_target = null;
		if($dataid>0){
//		    $model = new \backend\models\Tbdata();
//		    $model->setTableName($table);
		    
		    $model = new \backend\modules\ezforms\models\EzformDynamic($table);
		    $data = $model->find()->where('id = :id', ['id' => $dataid])->One();
	    
//		    $query = $model->find()->where('id=:id', [':id'=>$dataid]);
//
//		    $data = $query->one();

		    $model_gen->attributes = $data->attributes;
		    
		    if ($ezform_comp['special'] == 1) {
			$target_decode = $data->ptid;
			$ftarget='ptid';
		    } else {
			$target_decode = $data->id;
		    }
		 
                    
		}
		$get_target = isset($target) && $target!=''?$targetOld:Yii::$app->db->createCommand("select $ftarget from $table where id = '$target_decode'")->queryScalar();
		//$get_target = Yii::$app->db->createCommand("select $ftarget from $table where id = '$target'")->queryScalar();
		
		$dataComponent = [];
		if(isset($comp_id_target)){
		    if ($ezform_comp['special'] == 1) {
			//หาเป้าหมายพิเศษประเภทที่ 1 (CASCAP) (Thaipalliative) (Ageing)
			
			$dataComponent = InvFunc::specialTarget1($ezform_comp);
			
			//$dataProvidertarget = \backend\models\InputDataSearch::searchEMR($target);
		    } else {
			//หาแบบปกติ
			$dataComponent = InvFunc::findTargetComponent($ezform_comp);
			
		    }
		} else {
		    //target skip
		    $dataComponent['sqlSearch'] = '';
		    $dataComponent['tagMultiple'] = FALSE;
		    $dataComponent['ezf_table_comp'] = '';
		    $dataComponent['arr_comp_desc_field_name'] = '';
		}
                
                if(isset($dataset)  && !empty($dataset)) {
                    $dataSet = Json::decode(base64_decode($dataset));
                    foreach ($dataSet as $key=>$value){
                        $model_gen->{$key} = $value;
                    }
                }
                //ฟอร์มเป้าหมาย '.$modelform['ezf_name'].'
		$prefix = '<div class="panel ">
		    <div class="panel-heading clearfix" >
			<h3 class="panel-title"><a class="pull-right" href="/inputdata/printform?dataid='.$dataid.'&amp;id='.$ezform_comp['ezf_id'].'&amp;print=1" target="_blank"><span class="fa fa-print fa-2x"></span></a>'.'</h3>
		</div>
		    <div class="panel-body" id="panel-target-body">';
		

		return $prefix.$this->renderAjax('_ezform_widget', [
		    'modelform'=>$modelform,
		    'modelfield'=>$modelfield,
		    'model_gen'=>$model_gen,
		    'ezf_id'=>$ezform_comp['ezf_id'],
		    'dataid'=>$dataid,
		    'target'=> $get_target?base64_encode($get_target):'',
		    'comp_id_target'=>$comp_id_target,
		    'formname'=>'ezform-target',
		    'end'=>$end,
		    'readonly'=>$readonly,
                    'comp_target'=>$comp_target,
                    'dataset'=>$dataset,
		]).'</div></div>';

	    } catch (\yii\db\Exception $e) {
		//\appxq\sdii\utils\VarDumper::dump($e);
	    }
	    
    }    
}
