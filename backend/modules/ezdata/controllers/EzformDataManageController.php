<?php

namespace backend\modules\ezdata\controllers;

use backend\modules\ezforms\models\EzformFields;
use common\lib\codeerror\helpers\GenMillisecTime;
use Yii;
use backend\modules\ezdata\models\EzformDataManage;
use backend\modules\ezdata\models\EzformDataManageSearch;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * EzformDataManageController implements the CRUD actions for EzformDataManage model.
 */
class EzformDataManageController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all EzformDataManage models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new EzformDataManageSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single EzformDataManage model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new EzformDataManage model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new EzformDataManage();

        if ($model->load(Yii::$app->request->post())) {
            $model->id = GenMillisecTime::getMillisecTime();
            $EzformDataManage = $_POST['EzformDataManage'];
            $model->params = implode(',', $EzformDataManage['params']);
            $model->save();
            return $this->redirect(['index']);
        } else {
            $ezformItems = Yii::$app->db->createCommand("select ezf_id, ezf_name from `ezform` WHERE ezf_id in(select ezf_id from `ezform_favorite` WHERE userid=:userid)", [':userid'=>Yii::$app->user->identity->id])->queryAll();
            $ezformItems = ArrayHelper::map($ezformItems, 'ezf_id', 'ezf_name');
            //VarDumper::dump($ezformFav); exit;
            $ezf_field_id = \Yii::$app->db->createCommand("select ezf_field_name, ezf_field_label from `ezform_fields` WHERE ezf_field_type not in(2,13,15) and ezf_field_name <> 'id' and ezf_field_type is not null and  ezf_id = :ezf_id;", [':ezf_id'=>$model->ezf_id])->queryAll();
            $ezf_field_search = \Yii::$app->db->createCommand("select ezf_field_name, ezf_field_label from `ezform_fields` WHERE ezf_field_type in(7,9) and ezf_id = :ezf_id;", [':ezf_id'=>$model->ezf_id])->queryAll();
            //VarDumper::dump($ezf_field_search,10,true); exit;
            $ezf_field_search[] =[
                'ezf_field_name' => 'create_date',
                'ezf_field_label' => 'create_date : วันที่บันทึกข้อมูล',
            ];
            $ezf_field_search[] =[
                'ezf_field_name' => 'update_date',
                'ezf_field_label' => 'update_date : วันที่แก้ไขข้อมูลล่าสุด',
            ];
            return $this->render('create', [
                'ezf_field_search' =>$ezf_field_search,
                'ezf_field_id' => $ezf_field_id,
                'model' => $model,
                'ezformItems' =>$ezformItems
            ]);
        }
    }

    /**
     * Updates an existing EzformDataManage model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $EzformDataManage = $_POST['EzformDataManage'];
            $model->params = implode(',', $EzformDataManage['params']);
            $model->save();
            return $this->redirect(['index']);
        } else {
            $ezformItems = Yii::$app->db->createCommand("select ezf_id, ezf_name from `ezform` WHERE ezf_id in(select ezf_id from `ezform_favorite` WHERE userid=:userid)", [':userid'=>Yii::$app->user->identity->id])->queryAll();
            $ezformItems = ArrayHelper::map($ezformItems, 'ezf_id', 'ezf_name');
            $ezf_field_id = \Yii::$app->db->createCommand("select ezf_field_name, ezf_field_label from `ezform_fields` WHERE ezf_field_type not in(2,13,15) and ezf_field_name <> 'id' and ezf_field_type is not null and  ezf_id = :ezf_id;", [':ezf_id'=>$model->ezf_id])->queryAll();
            $ezf_field_search = \Yii::$app->db->createCommand("select ezf_field_name, ezf_field_label from `ezform_fields` WHERE ezf_field_type in(7,9) and ezf_id = :ezf_id;", [':ezf_id'=>$model->ezf_id])->queryAll();
            //VarDumper::dump($ezf_field_search,10,true); exit;
            $ezf_field_search[] =[
                    'ezf_field_name' => 'create_date',
                    'ezf_field_label' => 'create_date : วันที่บันทึกข้อมูล',
            ];
            $ezf_field_search[] =[
                'ezf_field_name' => 'update_date',
                'ezf_field_label' => 'update_date : วันที่แก้ไขข้อมูลล่าสุด',
            ];
            return $this->render('update', [
                'ezf_field_search' =>$ezf_field_search,
                'ezf_field_id' => $ezf_field_id,
                'model' => $model,
                'ezformItems' =>$ezformItems
            ]);
        }
    }

    /**
     * Deletes an existing EzformDataManage model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the EzformDataManage model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return EzformDataManage the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = EzformDataManage::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
