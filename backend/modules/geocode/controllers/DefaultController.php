<?php

namespace backend\modules\geocode\controllers;

use yii\web\Controller;

use backend\modules\ezforms\models\Ezform;
use backend\modules\component\models\EzformComponent;
use backend\models\ConstAmphur;
use backend\models\ConstDistrict;
use backend\models\ConstProvince;

class DefaultController extends Controller
{
    public function actionIndex()
    {
        return $this->render('index');
    }
    
    public function actionGetAddress () 
    {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $address = "";
            return [
                'address' => $address,
            ];
        if (\Yii::$app->request->isAjax) {
            $compid = \Yii::$app->keyStorage->get('personprofile.comp_id');
            $ezfid = EzformComponent::findOne(['comp_id'=>$compid])->ezf_id;
            $ezform = \backend\models\Ezform::findOne(['ezf_id'=>$ezfid]);
            $ezftable = $ezform->ezf_table;
            $sql = "SELECT count(*) from `{$ezftable}` where sys_lat is null";
            $limit = rand(0,\Yii::$app->db->createCommand($sql)->queryScalar());            
            
            $sql = "SELECT id,add1n5,add1n2,add1n6code,add1n7code,add1n8code from `{$ezftable}` where sys_lat is null limit {$limit},1";
            
            $data = \Yii::$app->db->createCommand($sql)->queryOne();
            if ($data['id']>0) {
                $sql = "UPDATE `{$ezftable}` SET sys_lat='',sys_lng='' WHERE id=".$data['id'];
                \Yii::$app->db->createCommand($sql)->query();
            }
            if ($data['add1n5'] != "") {
                $moo = "1 หมู่ " . ($data['add1n5']+0);
            }else{
                $moo = "";
            }
            if ($data['add1n2'] != "") {
                $ban = "บ้าน".$data['add1n2'];
            }else{
                $ban = "";
            }
            if ($data['add1n6code'] != "") {
                $tmb = $data['add1n6code'];
                $amp = $data['add1n7code'];
                $prov = $data['add1n8code'];
                $tambonname = ConstDistrict::findOne(['DISTRICT_CODE'=>$tmb])->DISTRICT_NAME;
                $amphurname = ConstAmphur::findOne(['AMPHUR_CODE'=>$amp])->AMPHUR_NAME;
                $provincename = ConstProvince::findOne(['PROVINCE_CODE'=>$prov])->PROVINCE_NAME;

                $tambon = $tambonname == "" ? "" : " ตำบล".$tambonname;
                $amphur = $amphurname == "" ? "" : " อำเภอ".$amphurname;
                $province = $provincename == "" ? "" : " จังหวัด".$provincename;
            }
            
            $address = "{$moo}{$tambon}{$amphur}{$province}";            
            

            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return [
                'table' => $ezftable,
                'dataid' => $data['id'],
                'address' => $address,
            ];
        }
    }
    public function actionSavelocation() {
        
        if (\Yii::$app->request->isAjax) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            
            $data = \Yii::$app->request->post("locationdata");
            $table = $data['table'];
            $dataid = $data['dataid'];
            $lat = $data['lat'];
            $lng = $data['lng'];
            
            $sql = "UPDATE `{$table}` SET sys_lat='{$lat}'+((-1)*rand()+rand())*0.00500,sys_lng='{$lng}'+((-1)*rand()+rand())*0.00500 WHERE id='{$dataid}'";
            $data = \Yii::$app->db->createCommand($sql)->query();
            
            unset($data);
            $sql = "SELECT add1n5,add1n6code FROM `{$table}` WHERE id='{$dataid}'";
            $addr = \Yii::$app->db->createCommand($sql)->queryOne();
            $moo = $addr['add1n5'];
            $tmb = $addr['add1n6code'];
            if ($moo != "" && $tmb != "") {
                $sql = "UPDATE `{$table}` SET sys_lat='{$lat}'+((-1)*rand()+rand())*0.00500,sys_lng='{$lng}'+((-1)*rand()+rand())*0.00500 WHERE sys_lat is null and add1n5='{$moo}' and add1n6code='{$tmb}'";
                $data = \Yii::$app->db->createCommand($sql)->query();            
            }
            
            return [
                'result' => "OK",
            ];
        }
    } 
}
