<?php

namespace backend\modules\geocode;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\modules\geocode\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
