<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace backend\modules\linkmap\controllers;
use backend\modules\linkmap\classes\Map;

use Yii;
use yii\web\Controller;
use yii\web;
use yii\helpers\Url;

/**
 * Description of Map
 *
 * @author engiball
 */
class MapController extends Controller {

    //put your code here
   public function beforeAction($action) {
        if (empty(Yii::$app->user->identity->id)) {
            return $this->layout = "@frontend/views/layouts/mainreport";
        }
        return parent::beforeAction($action);
    }

    
    public function actionIndex() {

        return $this->render('_map');
    }

    public function actionMapJs() {

        return $this->renderPartial('js');
    }

    public function actionMapJs2() {

        return $this->renderPartial('js2');
    }
      public function actionLeafjs() {

        return $this->renderPartial('leafjs');
    }


    public function actionCurve() {

        return $this->renderPartial('curvejs');
    }

    public function actionAnim() {

        return $this->renderPartial('animjs');
    }
    public function actionBIcon() {

        return $this->renderPartial('bicon');
    }
      public function actionBMarker() {

        return $this->renderPartial('bmarker');
    }
       public function actionLabel() {

        return $this->renderPartial('label');
    }
    public function actionPopup() {
               return $this->renderPartial('popup');

    }
        public function actionFullscreen() {
               return $this->renderPartial('fullscreen');

    }
         public function actionFullscreencss() {
               return $this->renderPartial('fullscreencss');

    }
    
    
    


    public function actionGetNewPoints() {

        return "[[16.957551,102.8309166],[16.757551,102.8409166],[16.732551,102.5609166]]";
    }

    public function actionGetPeoplePoint() {
        $datas = array();
        $sitecode = Yii::$app->request->get('sitecode');
        $sql = "select sys_lat as lat,sys_lng as lng from tb_data_1 where xsourcex =:sitecode  and sys_lat is not null and  sys_lat <> '' order by rand() limit 500";
        $data = \Yii::$app->db->createCommand($sql, [':sitecode' => $sitecode])->queryAll();
        $data0 = ["lat" => $data[0]["lat"], "lng" => $data[0]["lng"]];
        array_push($datas, $data0, $data);
        return json_encode($datas);
    }

    public function actionGetPlace() {
        $hcode = \Yii::$app->request->get('hcode');
        $datas = array();
        $sql = "SELECT DISTINCT hsitecode as hcode from tb_data_1 where sitecode=:hcode  and xsourcex <>  sitecode and  xsourcex <> '' UNION SELECT DISTINCT sitecode as hcode from tb_data_1 where xsourcex =:hcode  and  xsourcex<>  sitecode  and sitecode <>''";
        $data = \Yii::$app->db->createCommand($sql, [':hcode' => $hcode])->queryAll();

        return json_encode($data);
    }

    public function actionHosDetail() {
        $hcode = \Yii::$app->request->get('hcode');
         $sitecode = \Yii::$app->request->get('sitecode');

        $mapData = Map::hDetail($hcode,$sitecode);
        return json_encode($mapData);
    }
    
    

}
