<?php

namespace backend\modules\linkmap\controllers;

use yii\web\Controller;

class DefaultController extends Controller
{
    
       public function beforeAction($action) {
        if (empty(Yii::$app->user->identity->id)) {
            return $this->layout = "@frontend/views/layouts/mainreport";
        }
        return parent::beforeAction($action);
    }

    public function actionIndex()
    {
        return $this->render('index');
    }
}
