<?php

use backend\modules\linkmap\classes\Map;
use yii\helpers\Url;


$sitecode = Yii::$app->user->identity->userProfile->sitecode;


$mapData = Map::getPoints($sitecode);

$mapData0 = $mapData[0];
//var_dump($mapData0);

$i = 0;
?>


var mymap = L.map('mapid').setView([<?= $mapData[0]["lat"] ?>,<?= $mapData[0]["lng"] ?>], 12).on('click', L.bind(onClick, null,1234));
var markers= L.layerGroup().addTo(mymap);
var paths= L.layerGroup().addTo(mymap);
L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
maxZoom: 18,
id: 'mapbox.streets'
}).addTo(mymap);

<?php
//$mapData0 = array('16.757551','102.8309166');

echo "var marker0 =L.marker([$mapData0[lat],$mapData0[lng]]).addTo(markers)
    .bindPopup('โรงพยาบาล')
    .openPopup();";

echo "markers.addLayer(marker0);";
foreach ($mapData[1] as &$value) {

    $c1 = ($mapData0[lng] + $value[lng]) / 2;
    $c2 = ($mapData0[lng] + $value[lng]) / 2;

    echo "var path$value[0] = L.curve(['M',[$mapData0[lat], $mapData0[lng]],
					   'Q',[$value[lat],$c2],
						 [$value[lat],$value[lng]],
					   ], {dashArray: 10, animate: {duration: 5000, iterations: Infinity}}).addTo(paths);";

      echo "marker$value[0] =  L.marker([$value[lat],$value[lng]]).addTo(markers)
    .bindPopup('โรงพยาบาล')
    .openPopup();";
}
?>
var greenIcon = new L.Icon({
  iconUrl: 'https://storage.thaicarecloud.org/img/pdimg/www_pd_1497608234032356700.png',
  shadowUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-shadow.png',
  iconSize: [25, 25],
  iconAnchor: [10, 10],
  popupAnchor: [0,0],
  shadowSize: [0, 0]
});


function onClick(id) {
mymap.removeLayer(markers);
mymap.removeLayer(paths);
markers =null;
paths =null;
markers= L.layerGroup().addTo(mymap); 
 paths = L.layerGroup().addTo(mymap);
console.log(id);
getNewPoints(id)
}

function getNewPoints(hcode){

$.ajax({
url: "<?=Url::to('linkmap/map',true) ?>/get-people-point?sitecode=10668"
})
.done(function( data ) {
if ( console && console.log ) {

var pointArrayAll = eval("["+data + "]");
console.log(pointArrayAll[0][0]["lat"]);
var pointArray =pointArrayAll[0][1];
console.log(pointArray[0]["lat"]);

var marker5555 =L.marker([pointArrayAll[0][0]["lat"],pointArrayAll[0][0]["lng"]]).addTo(markers).on('click', L.bind(onClick, null,1234));
mymap.panTo(new L.LatLng(pointArrayAll[0][0]["lat"], pointArrayAll[0][0]["lng"]));
for(var i =1 ; i < pointArray.length;i++){
//console.log(pointArray[i]["lat"]+":" +pointArray[i]["lng"]);
var marker5555 =L.marker([pointArray[i]["lat"],pointArray[i]["lng"]], {icon: greenIcon}).addTo(markers).on('click', L.bind(onClick,null, 1234));

var cLng = (parseFloat(pointArray[i]["lng"])+parseFloat(pointArrayAll[0][0]["lng"]))/2;
console.log(pointArray[i]["lng"]+ " / "  +pointArrayAll[0][0]["lng"] );

var path = L.curve(['M',[pointArrayAll[0][0]["lat"], pointArrayAll[0][0]["lng"]],
'Q',[pointArray[i]["lat"],cLng],
[pointArray[i]["lat"],pointArray[i]["lng"]],
], {dashArray: 10, animate: {duration: 10000, iterations: Infinity}}).addTo(paths);

}

}
});

}


