<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace backend\modules\linkmap\classes;

/**
 * Description of Map
 *
 * @author engiball
 */
class Map {
// cascap xsourcex, hsitecode


    public static function getPoints() {


        $hcode = \Yii::$app->request->get('hcode');
        $datas = array();
        $sql = "select sys_lat as lat,sys_lng as lng from tb_data_1 where xsourcex =:sitecode  and sys_lat is not null and  sys_lat <> '' ";
        $data = \Yii::$app->db->createCommand($sql, [':sitecode' => $hcode])->queryAll();
        $data0 = ["lat" => $data[0]["lat"], "lng" => $data[0]["lng"]];
        array_push($datas, $data0, $data);

        return $datas;
    }

    public static function getHosPoint() {
        $hcode = \Yii::$app->request->get('hcode');
        $cTable = \Yii::$app->request->get('ctable');
        $cCol = \Yii::$app->request->get('cCol');


        $datas = array();
        $sql = "select sys_lat as lat,sys_lng as lng from :ctable where :cCol =:sitecode  and sys_lat is not null and  sys_lat <> '' ";
        $data = \Yii::$app->db->createCommand($sql, [':sitecode' => $hcode,':ctable'=>$ctable,':cCol'=>$cCol])->queryAll();
        $data0 = ["lat" => $data[0]["lat"], "lng" => $data[0]["lng"]];
        array_push($datas, $data0, $data);

        return $datas;
    }

    public static function getPeoplePoint() {
        $hcode = \Yii::$app->request->get('hcode');
        $datas = array();
        $sql = "select sys_lat as lat,sys_lng as lng from tb_data_1 where xsourcex =:sitecode  and sys_lat is not null and  sys_lat <> '' ";
        $data = \Yii::$app->db->createCommand($sql, [':sitecode' => $hcode])->queryAll();
        $data0 = ["lat" => $data[0]["lat"], "lng" => $data[0]["lng"]];
        array_push($datas, $data0, $data);
        return $datas;
    }


        public static function getPlaceCustom($cTable,$cCenterCol,$cCeterCode,$cEndCol) {

       $sql = "SELECT DISTINCT :cCenterCal as hcode from :cTable where :cEndCol=:hcode  and :cCenterCal <>  :cEndCol and  :cCenterCal <> '' UNION SELECT DISTINCT :cEndCol as hcode from :cTable where :cCenterCol =:cCenterCode  and  :cCenterCal <>  :cEndCol and :cEndCol <>''";
       //$sql="select hsitecode,sitecode as hcode,count(*) from tb_data_1 where (hsitecode=:hcode or sitecode=:hcode ) and rstat<>3 and sitecode<>''  group by hsitecode,sitecode having count(*) > 50;";
        $data = \Yii::$app->db->createCommand($sql, [':cTable'=>$cTable,':cCenterCal'=>$cCenterCol,':cCenterCode'=> $cCeterCode,':cEndCol'=>$cEndCol])->queryAll();
         array_unshift($data,["centerCode"=>$cCeterCode]);
        return $data;
    }



}
