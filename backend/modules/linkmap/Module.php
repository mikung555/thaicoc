<?php

namespace backend\modules\linkmap;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\modules\linkmap\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
