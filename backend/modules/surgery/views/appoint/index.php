<?php
use Yii;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use appxq\sdii\widgets\ModalForm;
use yii\helpers\Url;
use yii\helpers\VarDumper;
use yii\widgets\Pjax;

//\yii\helpers\VarDumper::dump($events, 10, true);
$this->registerJsFile('https://maps.google.com/maps/api/js?'.http_build_query($q), [
    'position'=>\yii\web\View::POS_HEAD,
    'depends'=>'yii\web\YiiAsset',
]);
?>
<h3>สมุดนัดหมาย</h3>
<hr>
<?php  Pjax::begin(['id'=>'inv-person-grid-pjax']);?>
<?php $form = ActiveForm::begin([
		'id' => 'jump_menu',
		'action' => ['index'],
		'method' => 'get',
		'layout' => 'inline',
		'options' => ['style'=>'display: inline-block;']	    
	    ]); 
	    $datasetArr = [
		'ptype' => $person,
		'division' => $dept,
	    ];
	    $dataset = base64_encode(\yii\helpers\Json::encode($datasetArr));
            
            $dateParse = date_parse($now_date);
            $datasetArr2 = [
		'ptype' => $person,
		'division' => $dept,
                'mm'=>$dateParse["month"],
                'yyyy'=>$dateParse["year"],
	    ];
	    $dataset2 = base64_encode(\yii\helpers\Json::encode($datasetArr2));
	    ?>
            <?=Html::a('<i class="glyphicon glyphicon-plus"></i> เพิ่มข้อมูลนัดหมาย', null, [
                'data-url'=>Url::to(['/inv/inv-person/ezform-print',
                    'ezf_id'=>'1464033435067331400',
                    'end'=>1,
                    'dataset'=>$dataset,
                    'comp_id_target'=>'1437725343023632100',
                ]),
                'class'=>'btn btn-success print-ezform',
                'style'=>'cursor: pointer;',
                'data-toggle'=>'tooltip',
                'title'=>'เพิ่มข้อมูล',
            ]);?>

	    <?=Html::a('<i class="glyphicon glyphicon-plus"></i> เพิ่มหมายเหตุ', null, [
                'data-url'=>Url::to(['/inv/inv-person/ezform-print',
                    'ezf_id'=>'1469513470040385000',
                    'end'=>1,
                    'dataset'=>$dataset2,
                    'comp_target'=>'skip',
                ]),
                'class'=>'btn btn-primary print-ezform',
                'style'=>'cursor: pointer;',
                'data-toggle'=>'tooltip',
                'title'=>'เพิ่มข้อมูล',
            ]);?>

            <?=Html::a('<i class="glyphicon glyphicon-plus"></i> เพิ่มวันหยุดประจำปี', null, [
                'data-url'=>Url::to(['/inv/inv-person/ezform-print',
                    'ezf_id'=>'1469514376009269700',
                    'end'=>1,
                    'comp_target'=>'skip',
                ]),
                'class'=>'btn btn-warning print-ezform',
                'style'=>'cursor: pointer;',
                'data-toggle'=>'tooltip',
                'title'=>'เพิ่มข้อมูล',
            ]);?>
		<?php
                $dataDept = ArrayHelper::merge([0=>'All'], $dataDept);
                $mlist = common\lib\sdii\components\utils\SDdate::$thaimonthFull;
                ?>   
	    <?= Html::dropDownList('person', $person, [1=>'Admit', 2=>'Endoscopy', 3=>'OR Minor', 4=>'ESWL'], ['class'=>'form-control', 'onChange'=>'$("#jump_menu").submit()'])?>
	    <?= Html::dropDownList('dept', $dept, $dataDept, ['class'=>'form-control', 'onChange'=>'$("#jump_menu").submit()'])?>
	    <?= Html::dropDownList('mm', $mm-1, $mlist, ['class'=>'form-control', 'onChange'=>'$("#jump_menu").submit()'])?>
            <?= Html::textInput('yy', $yy, ['class'=>'form-control', 'onChange'=>'$("#jump_menu").submit()'])?>

<?php ActiveForm::end(); ?>
<div class="pull-right"></div>

<?php

$datasetArr = [
		'ptype' => $person,
		'division' => $dept,
		'ddate' => $date,
	    ];
	    $dataset = base64_encode(\yii\helpers\Json::encode($datasetArr));
	    
?>
<div class="row">
    <div class="col-md-8">
	<?= \yii2fullcalendar\yii2fullcalendar::widget([
	    'options'       => [
		  'id'      => 'calendar',

	      ],
	    'events'=> $events,
	    'clientOptions' => [
		'lang' => 'th',
		'navLinks'=>true,
		'dayClick'=>new \yii\web\JsExpression("
		      function(date, jsEvent, view) {
			   var dataset = window.btoa('{\"ptype\":\"$person\", \"division\":\"$dept\", \"ddate\":\"'+date.format()+'\"}');
			   var url = '".Url::to(['/inv/inv-person/ezform-print',
                               'ezf_id'=>'1464033435067331400',
                                'end'=>1,
                                'comp_id_target'=>'1437725343023632100',
                                'dataset'=>'',
			       ])."'+dataset;
                                   
                               $('#modal-print .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
                                $('#modal-print').modal('show');
                                $.ajax({
                                    method: 'POST',
                                    url: url,
                                    dataType: 'HTML',
                                    success: function(result, textStatus) {
                                        $('#modal-print .modal-content').html(result);
                                        return false;
                                    }
                                });
                            
		      }
		  "),
		//'ajaxEvents' => Url::to(['/timetrack/default/jsoncalendar']),
		'header'=>[
		    'left'=>'prev,next today',
		    'center'=>'title',
		    'right'=>'',
		],
		  'defaultDate'=>$now_date,
		  'eventOrder'=>'id',
		  'loading' => new \yii\web\JsExpression("
		      function(bool) {
			  $('#loading').toggle(bool);
			  console.log(bool);
		      }
		  "),
		  'eventClick' => new \yii\web\JsExpression("
		      function(calEvent, jsEvent, view) {
			  var str = calEvent.id;
			  if(str.search('all_')!=-1){
			      var url = '".yii\helpers\Url::to(['/surgery/appoint/view-all', 'person'=>$person, 'dept'=>$dept, 'date'=>''])."'+calEvent.start._i;
			      modalAppoint(url);    
			  } else if(str.search('other_')!=-1){
			    
			  } else if(str.search('stop_')!=-1){
			    
			  } else {
			      var url = '".yii\helpers\Url::to(['/surgery/appoint/view', 'person'=>$person, 'dept'=>$dept, 'date'=>''])."'+calEvent.start._i;
			      modalAppoint(url);
			  }

		      }
		  "),
	      ],
	]);
	?>
    </div>
    <div class="col-md-4">
	<br>
	<h4><code>หมายเหตุ</code></h4>
	
	<?php
	if($commtEvents){
	    foreach ($commtEvents as $key => $value) {
		?>
	<code><?=$value['comment']?></code><br>
	<?php
	    }
	}
	?>
    </div>
</div>

<?php Pjax::end();?>

<input id="preload" type="hidden" value="0"/>
<input id="emr-preload" type="hidden" value="0"/>

<?=ModalForm::widget([
    'id' => 'modal-appoint',
    'size'=>'modal-lg modal-xl',
]);?>

<?= \appxq\sdii\widgets\ModalForm::widget([
    'id' => 'modal-print',
    'size'=>'modal-lg',
    'tabindexEnable'=>false,
    'options'=>['style'=>'overflow-y:scroll;']
]);
?>


<?php  
Yii::$app->session['pjax_reload'] = 'inv-person-grid-pjax';
$this->registerJs("
var person = '$person';
var dept = '$dept';    

        
  $('.fc-prev-button, .fc-next-button, .fc-today-button').click(function(){
    var moment = $('#calendar').fullCalendar('getDate');
    
    window.location.href = '".yii\helpers\Url::to(['/surgery/appoint/index', 'person'=>$person, 'dept'=>$dept, 'date'=>''])."'+moment.format('YYYY-MM-DD');
    
    return false;
  });
	
function modalAppoint(url) {
    $('#modal-appoint .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
    $('#modal-appoint').modal('show')
    .find('.modal-content')
    .load(url);
}


$('.print-ezform').on('click', function() {
    var url = $(this).attr('data-url');
    modalEzfrom(url);
});

function modalEzfrom(url) {
    $('#modal-print .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
    $('#modal-print').modal('show');
    $.ajax({
	method: 'POST',
	url: url,
	dataType: 'HTML',
	success: function(result, textStatus) {
	    $('#modal-print .modal-content').html(result);
	    return false;
	}
    });
}
");?>

