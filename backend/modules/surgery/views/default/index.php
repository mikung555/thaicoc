<div class="surgery-default-index">
    <h1><?= $this->context->action->uniqueId ?></h1>
    <p>
        This is the view content for action "<?= $this->context->action->id ?>".
        The action belongs to the controller "<?= get_class($this->context) ?>"
        in the "<?= $this->context->module->id ?>" module.
    </p>
    <p>
        You may customize this page by editing the following file:<br>
        <code><?= __FILE__ ?></code>
    </p>
</div>
<?php
$cal = isset($_GET['cal'])?$_GET['cal']:1;
$color = 1;
?>
<?=  \appxq\calendar\SDCalendar::widget([
    'readonly' => FALSE,
    'popupId' => 'myModal', // modal ID
    'addFunc' => 'addEditForm', //javascript external function
    'updateFunc' => 'updateEditForm',//javascript external function
    'linkFunc' => 'linkTo',
    'options' => [
	    'view' => "week", //day, week , month
	    'theme' => $color, //0-22
	    'autoload' => true,
	    'url' => \yii\helpers\Url::to('/surgery/default/feed', ['method' => 'list', 'cal'=>$cal]),
	    'quickAddUrl' => \yii\helpers\Url::to('/surgery/default/feed', ['method' => 'add', 'cal'=>$cal]),
	    'quickUpdateUrl' => \yii\helpers\Url::to('/surgery/default/feed', ['method' => 'update']),
	    'quickDeleteUrl' => \yii\helpers\Url::to('/surgery/default/feed', ['method' => 'remove']),
    ],
]);
?>

