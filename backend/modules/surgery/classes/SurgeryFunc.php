<?php
namespace backend\modules\surgery\classes;

use Yii;

/**
 * OvccaFunc class file UTF-8
 * @author SDII <iencoded@gmail.com>
 * @copyright Copyright &copy; 2015 AppXQ
 * @license http://www.appxq.com/license/
 * @version 1.0.0 Date: 9 ก.พ. 2559 12:38:14
 * @link http://www.appxq.com/
 * @example 
 */
class SurgeryFunc {
    public static function getAlign($align) {
	$str = substr($align, 0, 1);
	$str = strtoupper($str);
	return $str;
    }
    
    public static function getIcon() {
	$icon = [
	    'fa-battery-empty' => 'battery empty',
	    'fa-battery-quarter' => 'battery quarter',
	    'fa-battery-half' => 'battery half',
	    'fa-battery-three-quarters' => 'battery three quarters',
	    'fa-battery-full' => 'battery full',
	    'fa_music'=>'&#xf001;',
	];
	
	return $icon;
    }
}
