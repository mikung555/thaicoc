<?php
namespace backend\modules\surgery\classes;
use Yii;

class DateCal {

	public static function js2PhpTime($jsdate) {
		if (preg_match('@(\d+)/(\d+)/(\d+)\s+(\d+):(\d+)@', $jsdate, $matches) == 1) {
			$ret = mktime($matches[4], $matches[5], 0, $matches[1], $matches[2], $matches[3]);
			//echo $matches[4] ."-". $matches[5] ."-". 0  ."-". $matches[1] ."-". $matches[2] ."-". $matches[3];
		} else if (preg_match('@(\d+)/(\d+)/(\d+)@', $jsdate, $matches) == 1) {
			$ret = mktime(0, 0, 0, $matches[1], $matches[2], $matches[3]);
			//echo 0 ."-". 0 ."-". 0 ."-". $matches[1] ."-". $matches[2] ."-". $matches[3];
		}
		return $ret;
	}
	
	public static function th2PhpTime($jsdate) {
		if (preg_match('@(\d+)/(\d+)/(\d+)\s+(\d+):(\d+)@', $jsdate, $matches) == 1) {
			$ret = mktime($matches[4], $matches[5], 0, $matches[2], $matches[1], $matches[3]);
			//echo $matches[4] ."-". $matches[5] ."-". 0  ."-". $matches[1] ."-". $matches[2] ."-". $matches[3];
		} else if (preg_match('@(\d+)/(\d+)/(\d+)@', $jsdate, $matches) == 1) {
			$ret = mktime(0, 0, 0, $matches[2], $matches[1], $matches[3]);
			//echo 0 ."-". 0 ."-". 0 ."-". $matches[1] ."-". $matches[2] ."-". $matches[3];
		}
		return $ret;
	}

	public static function php2JsTime($phpDate) {
		return date("m/d/Y H:i", $phpDate);
	}
	
	public static function php2ThTime($phpDate) {
		return date("d/m/Y H:i", $phpDate);
	}

	public static function php2MySqlTime($phpDate) {
		return date("Y-m-d H:i:s", $phpDate);
	}

	public static function mySql2PhpTime($sqlDate) {
		$arr = date_parse($sqlDate);
		return mktime($arr["hour"], $arr["minute"], $arr["second"], $arr["month"], $arr["day"], $arr["year"]);
	}

	public static function itemTime($range=30) {
		$arr=array();
		for ($i = 0; $i < 24; $i++) {
			for($x='00';$x<60;$x=$x+$range){
				array_push($arr,array('id' => ((strlen($i)==1)?'0'.$i:$i).':'.$x, 'text' => ((strlen($i)==1)?'0'.$i:$i).':'.$x));
			}
		}
		return \yii\helpers\ArrayHelper::map($arr, 'id', 'text');
	}

	public static function colorItem($len=10,$strEnable=false) {
		$arr = array();
		for($i=0;$i<$len;$i++){
			if($strEnable){
				array_push($arr,array('id'=>$i, 'text'=>$i));
			}
			else{
				array_push($arr,array('id'=>$i, 'text'=>''));
			}
		}
		return \yii\helpers\ArrayHelper::map($arr, 'id', 'text');
	}
	
	public static function colorTheme($theme=0) {
		$c = ($theme<0 || $theme>22)?0:$theme;
		return array(self::colorList($c, 0), self::colorList($c, 1), self::colorList($c, 2), self::colorList($c, 3));
	}

	public static function colorList($theme, $i){
		$d = "A64232A64232F83A22F83A22D02424D02424FA573CFA573Ce77c6be77c6bFC8976FC8976BB5517BB5517FF7537FF7537CB7403CB7403FFAD46FFAD46BF9608BF9608FAD165FAD165BDB634BDB634FBE983FBE98350B68E50B68E42D69242D6924DB8104DB8107BD1487BD14893C00B93C00BB3DC6CB3DC6C33B69433B69492E1C092E1C02C70D12C70D14986E74986E7373AD7373AD79A9CFF9A9CFF1587BD1587BD9FC6E79FC6E70BBCB20BBCB29FE1E79FE1E76733DD6733DDB99AFFB99AFF9C3CE49C3CE4A47AE2A47AE2CA2AE6CA2AE6CD74E6CD74E6D21E5BD21E5BF691B2F691B2f06eaaf06eaaf49ac1f49ac1979797979797C2C2C2C2C2C2717171717171CABDBFCABDBF75481E75481EAC725EAC725E924420924420D06B64D06B648A404D8A404DCCA6ACCCA6AC";
		return "#". substr($d, (($theme * 4) + $i) * 6, 6);
	}
}

?>
