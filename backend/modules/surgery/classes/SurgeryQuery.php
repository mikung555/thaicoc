<?php
namespace backend\modules\surgery\classes;

use Yii;

/**
 * OvccaQuery class file UTF-8
 * @author SDII <iencoded@gmail.com>
 * @copyright Copyright &copy; 2015 AppXQ
 * @license http://www.appxq.com/license/
 * @version 1.0.0 Date: 9 ก.พ. 2559 12:38:14
 * @link http://www.appxq.com/
 * @example 
 */
class SurgeryQuery {
    
    public static function getDept() {
	
	$sql = "SELECT id, name FROM tbdata_1464024004038379500 WHERE type=1 AND rstat<>0 AND rstat<>3";
	
	return Yii::$app->db->createCommand($sql)->queryAll();
    }
    
    public static function getEvent($ptype, $division, $now_date) {
	$y = substr($now_date, 0, 4);
	$m = substr($now_date, 5, 2);
	$sitecode = Yii::$app->user->identity->userProfile->sitecode;
        
        $my = "SUM(1) AS my,
                    SUM(0) AS other";
	if($division>0){
            $my = "SUM(IF(division=$division,1,0)) AS my,
			SUM(IF(division<>$division,1,0)) AS other";
        }
        $show = "";
        if($ptype>1){
            $show = "ptype=$ptype AND ";
        }
	$sql = "SELECT ddate,
			$my
		FROM tbdata_1464033435067331400 
		WHERE $show YEAR(ddate)=:yy AND MONTH(ddate)=:mm AND rstat<>0 AND rstat<>3 AND hsitecode=:hsitecode
		GROUP BY ddate
		ORDER BY ddate ";
	
	return Yii::$app->db->createCommand($sql, [':yy'=>$y, ':mm'=>$m, ':hsitecode'=>$sitecode])->queryAll();//, ':division'=>$division
    }
    
    public static function getComment($ptype, $division, $now_date) {
	$y = substr($now_date, 0, 4);
	$m = (int)substr($now_date, 5, 2);
	
	$sql = "SELECT comment
		FROM tbdata_1469513470040385000 commt 
		WHERE ptype=:ptype AND division=:division AND yyyy=:yy AND mm=:mm AND rstat<>0 AND rstat<>3
		order by yyyy, mm
		";
	
	return Yii::$app->db->createCommand($sql, [':ptype'=>$ptype, ':division'=>$division, ':yy'=>$y, ':mm'=>$m])->queryAll();
    }
    
    public static function getEventStopCustom($y, $m) {
	
	$sql = "SELECT ddate,
		sdate as hname
		FROM tbdata_1469514376009269700 
		WHERE YEAR(ddate)=:yy AND MONTH(ddate)=:mm AND rstat<>0 AND rstat<>3
		order by ddate
		";
	
	return Yii::$app->db->createCommand($sql, [':yy'=>$y, ':mm'=>$m])->queryAll();
    }
    
    public static function getEventStop($y) {
	
	$sql = "SELECT CONCAT('$y', '-', lpad(hmonth,2,0), '-', lpad(hday,2,0)) AS ddate,
		hname
		FROM dep_calendar 
		";
	
	return Yii::$app->db->createCommand($sql)->queryAll();
    }
    
    
    public static function getDp($ptype, $division, $date) {
	$sitecode = Yii::$app->user->identity->userProfile->sitecode;
	
        $showSql = "";
        
        if($ptype>1){
            $showSql .= "a.ptype=$ptype AND ";
        }
	if($division>0){
            $showSql .= "a.division=$ptype AND ";
        }
        
        
	$count = Yii::$app->db->createCommand('
	    SELECT COUNT(*) FROM tbdata_1464033435067331400 a
		LEFT JOIN tb_data_1 p ON a.ptid = p.ptid
		LEFT JOIN user_profile t ON a.staff = t.user_id
		WHERE '.$showSql.' a.ddate=:date AND a.rstat<>0 AND a.rstat<>3 AND p.hsitecode=:hsitecode
	', [':date' => $date, ':hsitecode'=>$sitecode])->queryScalar();

	$sql = "SELECT a.division,
			a.ptype,
			a.ddate,
			p.id AS pid,
			p.ptid AS pptid,
			a.id AS aid,
			a.ptid AS aptid,
			p.hncode,
			CONCAT(p.name, ' ', p.surname) AS name,
			p.v2,
			floor(DATEDIFF(a.ddate, STR_TO_DATE(p.v2, '%Y-%m-%d'))/365) AS age,
			p.mobile,
			a.diagnosis,
			a.plan,
			a.staff,
			a.bed,
			CONCAT(t.firstname, ' ', t.lastname) AS tname,
			a.opd,
			a.postpone
		FROM tbdata_1464033435067331400 a
		LEFT JOIN tb_data_1 p ON a.ptid = p.ptid
		LEFT JOIN user_profile t ON a.staff = t.user_id
		WHERE $showSql a.ddate=:date AND a.rstat<>0 AND a.rstat<>3 AND p.hsitecode=:hsitecode
		ORDER BY a.ddate ASC";
	
	$dataProvider = new \yii\data\SqlDataProvider([
	    'sql' => $sql,
	    'params' => [':date' => $date, ':hsitecode'=>$sitecode],
	    'totalCount' => $count,
	    'sort' => [
		'attributes' => [p.telephone,
			a.diagnosis,
			a.plan,
			a.staff,
			a.bed,
			a.opd,
		],
	    ],
	    'pagination' => [
		'pageSize' => 50,
	    ],
	]);
	
	return $dataProvider;
    }
    
    public static function getDpAll($ptype, $division, $date) {
	$sitecode = Yii::$app->user->identity->userProfile->sitecode;
	
        $showSql = "";
        
        if($ptype>1){
            $showSql .= "a.ptype=$ptype AND ";
        }
	
	$count = Yii::$app->db->createCommand('
	    SELECT COUNT(*) FROM tbdata_1464033435067331400 a
		LEFT JOIN tb_data_1 p ON a.ptid = p.ptid
		LEFT JOIN user_profile t ON a.staff = t.user_id
		WHERE '.$showSql.' a.ddate=:date AND a.rstat<>0 AND a.rstat<>3 AND p.hsitecode=:hsitecode
	', [':date' => $date, ':hsitecode'=>$sitecode])->queryScalar();
	
	$sql = "SELECT a.division,
			a.ptype,
			a.ddate,
			p.id AS pid,
			p.ptid AS pptid,
			a.id AS aid,
			a.ptid AS aptid,
			p.hncode,
			CONCAT(p.name, ' ', p.surname) AS name,
			p.v2,
			floor(DATEDIFF(a.ddate, STR_TO_DATE(p.v2, '%Y-%m-%d'))/365) AS age,
			p.mobile,
			a.diagnosis,
			a.plan,
			a.staff,
			a.bed,
			CONCAT(t.firstname, ' ', t.lastname) AS tname,
			a.opd,
			a.postpone
		FROM tbdata_1464033435067331400 a
		LEFT JOIN tb_data_1 p ON a.ptid = p.ptid
		LEFT JOIN user_profile t ON a.staff = t.user_id
		WHERE $showSql a.ddate=:date AND a.rstat<>0 AND a.rstat<>3 AND p.hsitecode=:hsitecode
		ORDER BY a.ddate ASC";
	
	$dataProvider = new \yii\data\SqlDataProvider([
	    'sql' => $sql,
	    'params' => [':date' => $date, ':hsitecode'=>$sitecode],
	    'totalCount' => $count,
	    'sort' => [
		'attributes' => [
		],
	    ],
	    'pagination' => [
		'pageSize' => 50,
	    ],
	]);
	
	return $dataProvider;
    }
}
