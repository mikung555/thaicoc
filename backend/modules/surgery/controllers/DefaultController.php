<?php

namespace backend\modules\surgery\controllers;

use yii\web\Controller;
use backend\modules\surgery\classes\DateCal;

class DefaultController extends Controller
{
    public function actionIndex()
    {
        return $this->render('index');
    }
    
    public function actionFeed()
    {
        $method = $_GET["method"];
	switch ($method) {
		case "add":
			$ret = $this->addCalendar($_POST["CalendarStartTime"], $_POST["CalendarEndTime"], $_POST["CalendarTitle"], $_POST["IsAllDayEvent"]);
			break;
		case "list":
			$ret = $this->listCalendar($_POST["showdate"], $_POST["viewtype"]);//$_POST["viewtype"] บังคับให้โหลดแบบเดือน เพราะ ถ้าอยู่หน้าสับดา ข้อมูลบ้างตัวจะไม่ขึ้น ยังไม่ทราบสาเหตุ
			break;
		case "update":
			$ret = $this->updateCalendar($_POST["calendarId"], $_POST["CalendarStartTime"], $_POST["CalendarEndTime"]);
			break;
		case "remove":
			$ret = $this->removeCalendar($_POST["calendarId"]);
			break;
		case "adddetails":
			$model = new Calendar;
			$model->attributes = $_POST['Calendar'];
			$st = $model->stpartdate . " " . $model->stparttime;
			$et = $model->etpartdate . " " . $model->etparttime;
			if (strlen($_POST['Calendar']['cal_id'])>0) {
				$ret = $this->updateDetailedCalendar($_POST['Calendar']['cal_id'], $st, $et, $model->subject, ($model->isalldayevent) ? 1 : 0, $model->description, $model->location, $model->color, $model->timezone, $model);
			} else {
				$ret = $this->addDetailedCalendar($st, $et, $model->subject, ($model->isalldayevent) ? 1 : 0, $model->description, $model->location, $model->color, $model->timezone, $model);
			}
			break;
	}
	echo \yii\helpers\Json::encode($ret);
    }
    
    public function listCalendar($day, $type) {
		$phpTime = DateCal::js2PhpTime($day);
		//echo $phpTime . "+" . $type.'=';
		switch ($type) {
			case "month":
				$st = mktime(0, 0, 0, date("m", $phpTime), 1, date("Y", $phpTime));
				$et = mktime(0, 0, -1, date("m", $phpTime) + 1, 1, date("Y", $phpTime));
				break;
			case "week":
				//suppose first day of a week is monday 
				$monday = date("d", $phpTime) - date('w', $phpTime);
		
				$st = mktime(0, 0, 0, date("m", $phpTime), $monday, date("Y", $phpTime));
				$et = mktime(0, 0, -1, date("m", $phpTime), $monday + 7, date("Y", $phpTime));
				break;
			case "day":
				$st = mktime(0, 0, 0, date("m", $phpTime), date("d", $phpTime), date("Y", $phpTime));
				$et = mktime(0, 0, -1, date("m", $phpTime), date("d", $phpTime) + 1, date("Y", $phpTime));
				break;
		}
		//echo date('Y-m-d', $st).' - '.date('Y-m-d', $et);
		return $this->listCalendarByRange($st, $et);
	}
	
	public function listCalendarByRange($sd, $ed) {
		
		$ret = array();
		$ret['events'] = array();
		$ret["issort"] = true;
		$ret["start"] = DateCal::php2JsTime($sd);
		$ret["end"] = DateCal::php2JsTime($ed);
		$ret['error'] = null;
		try {
		    $cal = isset($_GET['cal'])?$_GET['cal']:1;
		    
		    $mycalData = array();
		    
		} catch (\yii\db\Exception $e) {
			$ret['error'] = $e->getMessage();
		}
		return $ret;
	}
}
