<?php

namespace backend\modules\surgery;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\modules\surgery\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
