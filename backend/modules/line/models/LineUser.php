<?php

namespace backend\modules\line\models;

use Yii;

/**
 * This is the model class for table "line_user".
 *
 * @property integer $user_id
 * @property string $pincode
 * @property string $line_id
 * @property string $line_name
 * @property string $dupdate
 */
class LineUser extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'line_user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'pincode'], 'required'],
            [['user_id'], 'integer'],
            [['dupdate'], 'safe'],
            [['pincode'], 'string', 'max' => 10],
            [['line_id', 'line_name'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_id' => 'รหัสผู้ใช้',
            'pincode' => 'PINCODE',
            'line_id' => 'ไอดีไลน์',
            'line_name' => 'ชื่อผู้ใช้ไลน์',
            'dupdate' => 'วันที่ตรวจสอบ',
        ];
    }

    /**
     * @inheritdoc
     * @return LineUserQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new LineUserQuery(get_called_class());
    }
}
