
<div class="line-default-index">
    <div class="col-md-6 col-lg-5 col-sm-12 col-xs-12">
        <div class="panel panel-primary"> 
            <div class="panel-heading"> 
                <h3 class="panel-title">Line setup</h3> 
            </div> 
            <div class="panel-body">
                <div class="col-md-4 col-sm-6 col-xs-6">
                    <img src="images/igz6369n.png" class="img-responsive">
                </div>
                <div class="col-md-8 col-sm-6 col-xs-6">
                    <?php if ($lineid == "") { ?>
                    <div class="page-header">
                        <h4>
                            <i class="fa fa-cog" aria-hidden="true"></i> วิธีการ Setup Line Messenger.
                        </h4>
                    </div>                    
                    <ul class="list-group">
                      <li class="list-group-item">เปิดโปรแกรม Line และเพิ่มเพื่อนจาก QRCODE หลังจากนั้น ให้ใส่รหัส OTP (One-time password) โดยพิมพ์หมายเลขด้านล่างนี้</li>
                      <li class="list-group-item"><center><h3 style="display: inline-block;"><span class="label label-danger"><?=$pincode;?></h5</span></h3></center></li>
                    </ul>
                    <a class="btn btn-primary" href="/line">เสร็จแล้ว</a>
                    <?php }else{ ?>
                    <div class="page-header">
                        <h4>
                            <i class="fa fa-cog" aria-hidden="true"></i> การเชื่อมต่อกับ Line สำเร็จ.
                        </h4>
                    </div>                    
                    <div class="col-md-6 col-sm-6 col-xs-6">
                        <img src="<?php echo Yii::$app->user->identity->userProfile->getAvatar() ?: '/img/anonymous.jpg' ?>" alt="" class="center-block img-thumbnail img-responsive">
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-6">
                        <img src="<?php echo $lineimg; ?>" alt="" class="center-block img-thumbnail img-responsive">
                    </div>                        
                    <?php } ?>
                </div>                
            </div> 
        </div>        
    </div>
</div>
