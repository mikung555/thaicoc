<?php

namespace backend\modules\test\classes;

use Yii;

/**
 * newPHPClass class file UTF-8
 * @author SDII <iencoded@gmail.com>
 * @copyright Copyright &copy; 2015 AppXQ
 * @license http://www.appxq.com/license/
 * @version 1.0.0 Date: 29 มิ.ย. 2559 1:06:13
 * @link http://www.appxq.com/
 * @example 
 */
class TestQuery {

    public static function getProvinceAll() {
	$sql = "SELECT const_province.PROVINCE_CODE, 
			const_province.PROVINCE_NAME, 
			const_province.PROVINCE_ID
		FROM const_province
	";
	return Yii::$app->db->createCommand($sql)->queryAll();
    }
    
    public static function getProvinceByID($id) {
	$sql = "SELECT const_province.PROVINCE_CODE, 
			const_province.PROVINCE_NAME, 
			const_province.PROVINCE_ID
		FROM const_province
		WHERE PROVINCE_ID=:id
	";
	return Yii::$app->db->createCommand($sql, [':id'=>$id])->queryOne();
    }

}
