<?php
use backend\assets\NutAsset;
NutAsset::register($this);
?>
<h1>Drag and Drop</h1>
<div class="container-fluid">
    <div class="grid-stack"></div>
</div>

<?php
$this->registerJs("
    var waitForFinalEvent=function(){var b={};return function(c,d,a){a||(a='I am a banana!');b[a]&&clearTimeout(b[a]);b[a]=setTimeout(c,d)}}();
    var fullDateString = new Date();
    function isBreakpoint(alias) {
       return $('.device-' + alias).is(':visible');
    }  
    
    var options = {
        float: false
    };
    $('.grid-stack').gridstack(options);
    function resizeGrid() {
        var grid = $('.grid-stack').data('gridstack');
        if (isBreakpoint('xs')) {
           $('#grid-size').text('One column mode');
        } else if (isBreakpoint('sm')) {
            grid.setGridWidth(3);
            $('#grid-size').text(3);
        } else if (isBreakpoint('md')) {
            grid.setGridWidth(6);
            $('#grid-size').text(6);
        } else if (isBreakpoint('lg')) {
            grid.setGridWidth(12);
            $('#grid-size').text(12);
        }
    };//resizeGrid
    
    $(window).resize(function () {
        waitForFinalEvent(function() {
          resizeGrid();
        }, 300, fullDateString.getTime());
    });//resize
    
    new function () {
        this.serializedData = [
            {x: 0, y: 0, width: 4, height: 2,name:'PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi'},
            {x: 3, y: 1, width: 4, height: 2,name:'d3d3LnczLm9yZy8yMDAwL3N2ZyIg'},
            {x: 4, y: 1, width: 4, height: 1,name:'2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD'},
            {x: 2, y: 3, width: 8, height: 1,name:'U3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIwJSI'},
            {x: 0, y: 4, width: 4, height: 1,name:'kZWRlZCIgc3RvcC1vcGFjaXR5PSIxI'},
            {x: 0, y: 3, width: 4, height: 1,name:'mc2V0PSIwJSIgc3RvcC1jb2xvcj0iI2'},
            {x: 2, y: 4, width: 4, height: 1,name:'Y29sb3I9IiNmZmZmZmYiIHN0b'},
            {x: 2, y: 5, width: 4, height: 1,name:'hY2l0eT0iMSIvPgogIDwvbGluZWFyR3'},
            {x: 0, y: 6, width: 12, height: 1,name:'IjEiIGhlaWdodD0iMSIgZmlsbD0idXJsKCNn'}
        ];
        
        this.grid = $('.grid-stack').data('gridstack'); //class grid-stack
        
        this.loadGrid = function () {
            this.grid.removeAll();
            var items = GridStackUI.Utils.sort(this.serializedData);
            _.each(items, function (node, i) {
                this.grid.addWidget($('<div><div class=\'grid-stack-item-content\'>' + node.name + '</div></div>'),
                            node.x, node.y, node.width, node.height);
            }, this);
            return false;
        }.bind(this);

        this.loadGrid();
        resizeGrid();
    }//new function
");
?>