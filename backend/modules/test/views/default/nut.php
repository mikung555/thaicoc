<?php
    use yii\helpers\Url;
?>
<?= \yii2assets\pdfjs\PdfJs::widget([
  'url'=> Url::base().'/ckdreport.pdf',
    'buttons'=>[
    'presentationMode' => false,
    'openFile' => false,
    'print' => true,
    'download' => true,
    'viewBookmark' => false,
    'secondaryToolbarToggle' => false
  ]
]); ?>
 