<div class="test-default-index">
    <h1><?= $this->context->action->uniqueId ?></h1>
    <?php
    
    ?>
    <div class="row">
	<div class="col-md-6 ">
	    <div class="panel panel-primary"> 
		<div class="panel-heading"> 
		    <div class="row">
			<div class="col-md-6"><h3 class="panel-title">Vitalsign</h3></div>
			<div class="col-md-6 text-right">
				<button type="button" class="btn btn-success btn-sm"><span class="glyphicon glyphicon-plus"></span></button> 
				<button type="button" class="btn btn-warning btn-sm"><span class="glyphicon glyphicon-pencil"></span></button>
				<button type="button" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-th-list"></span></button>
			</div>
		    </div>
		</div> 
		<div class="panel-body"> 
		    <table class="table table-condensed">
			<tr>
			    <td>BP</td>
			    <th>77</th>
			    <td>/ mmHg</td>
			    <td></td>
			    <td>Pulse</td>
			    <th>180</th>
			    <td>/min</td>
			</tr>
			<tr>
			    <td>RR</td>
			    <th>77</th>
			    <td>/min</td>
			    <td></td>
			    <td>Temp</td>
			    <th>30</th>
			    <td>°C</td>
			</tr>
		    </table>
		</div> 
	    </div>
	</div>
	<div class="col-md-6 sdbox-col">
	    <div class="panel panel-primary"> 
		<div class="panel-heading"> 
		    <div class="row">
			<div class="col-md-6"><h3 class="panel-title">BMI = ?</h3> </div>
			<div class="col-md-6 text-right">
				<button type="button" class="btn btn-success btn-sm"><span class="glyphicon glyphicon-plus"></span></button> 
				<button type="button" class="btn btn-warning btn-sm"><span class="glyphicon glyphicon-pencil"></span></button>
				<button type="button" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-th-list"></span></button>
			</div>
		    </div>
		</div> 
		<div class="panel-body"> 
		    <table class="table table-condensed">
			<tr>
			    <td>น้ำหนัก</td>
			    <th>77</th>
			    <td>ก.ก.</td>
			    <td></td>
			    <td>ส่วนสูง</td>
			    <th>180</th>
			    <td>ซ.ม.</td>
			</tr>
		    </table>
		</div> 
	    </div>
	</div>
    </div>
</div>
