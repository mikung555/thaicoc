<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\test\models\DataTest */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Data Test',
]) . ' ' . $model->dt_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Data Tests'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->dt_id, 'url' => ['view', 'id' => $model->dt_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="data-test-update">

    <?= $this->render('_form', [
        'model' => $model,
	'dpProv' => $dpProv,
	'dpInput' => $dpInput,
    ]) ?>

</div>
