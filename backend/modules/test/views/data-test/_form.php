<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use appxq\sdii\helpers\SDNoty;
use appxq\sdii\helpers\SDHtml;

/* @var $this yii\web\View */
/* @var $model backend\modules\test\models\DataTest */
/* @var $form yii\bootstrap\ActiveForm */
?>

<div class="data-test-form">

    <?php $form = ActiveForm::begin([
	'id'=>$model->formName(),
    ]); ?>

    <div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	<h4 class="modal-title" id="itemModalLabel">Data Test</h4>
    </div>

    <div class="modal-body">
	<div class="row">
		<div class="col-md-6 "><?= $form->field($model, 'dt_name')->textInput(['maxlength' => true]) ?></div>
		<div class="col-md-6 sdbox-col"><?= $form->field($model, 'dt_province')->widget(\kartik\select2\Select2::className(), [
		    'options' => ['placeholder' => 'เลือกจังหวัด'],
		    'data' => yii\helpers\ArrayHelper::map($dpProv, 'PROVINCE_ID', 'PROVINCE_NAME'),
		    'pluginOptions' => [
		    ],
		]) ?></div>
	</div>

	<?= $form->field($model, 'dt_text')->textarea(['rows' => 3]) ?>

	<?= $form->field($model, 'dt_enable')->radioList(['No', 'Yes']) ?>

	<?= $form->field($model, 'dt_tel')->widget(\yii\widgets\MaskedInput::className(), [
	    'mask' => '999-999-9999']) 
	    
	    ?>

	<?= $form->field($model, 'dt_email')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'dt_num')->textInput(['type'=>'number']) ?>

	<?= $form->field($model, 'dt_public')->checkbox() ?>

	<?= $form->field($model, 'dt_like')->checkboxList(['สำต้ม'=>'สำต้ม', 'ปลาเผา'=>'ปลาเผา', 'สำต้ม2'=>'สำต้ม2', 'ปลาเผา2'=>'ปลาเผา2']) ?>

	<?= $form->field($model, 'dt_input')->checkboxList(yii\helpers\ArrayHelper::map($dpInput, 'input_id', 'input_name')) ?>

	<?= $form->field($model, 'dt_geography')->textInput() ?>

	<?= $form->field($model, 'created_at')->hiddenInput()->label(false) ?>

	<?= $form->field($model, 'created_by')->hiddenInput()->label(false) ?>

	<?= $form->field($model, 'updated_at')->hiddenInput()->label(false) ?>

	<?= $form->field($model, 'updated_by')->hiddenInput()->label(false) ?>

    </div>
    <div class="modal-footer">
	<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	<?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php  $this->registerJs("
$('form#{$model->formName()}').on('beforeSubmit', function(e) {
    var \$form = $(this);
    $.post(
	\$form.attr('action'), //serialize Yii2 form
	\$form.serialize()
    ).done(function(result) {
	if(result.status == 'success') {
	    ". SDNoty::show('result.message', 'result.status') ."
	    if(result.action == 'create') {
		$(\$form).trigger('reset');
		$.pjax.reload({container:'#data-test-grid-pjax'});
	    } else if(result.action == 'update') {
		$(document).find('#modal-data-test').modal('hide');
		$.pjax.reload({container:'#data-test-grid-pjax'});
	    }
	} else {
	    ". SDNoty::show('result.message', 'result.status') ."
	} 
    }).fail(function() {
	". SDNoty::show("'" . SDHtml::getMsgError() . "Server Error'", '"error"') ."
	console.log('server error');
    });
    return false;
});

");?>