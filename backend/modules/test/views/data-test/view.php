<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\test\models\DataTest */

$this->title = 'Data Test#'.$model->dt_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Data Tests'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="data-test-view">

    <div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	<h4 class="modal-title" id="itemModalLabel"><?= Html::encode($this->title) ?></h4>
    </div>
    <div class="modal-body">
        <?= DetailView::widget([
	    'model' => $model,
	    'attributes' => [
		'dt_id',
		'dt_name',
		'dt_text:ntext',
		'dt_province',
		'dt_enable',
		'dt_tel',
		'dt_email:email',
		'dt_num',
		'dt_public',
		'dt_like:ntext',
		'dt_input:ntext',
		'dt_geography',
		'created_at',
		'created_by',
		'updated_at',
		'updated_by',
	    ],
	]) ?>
    </div>
</div>
