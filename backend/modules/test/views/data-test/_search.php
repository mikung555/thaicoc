<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\test\models\DataTestSearch */
/* @var $form yii\bootstrap\ActiveForm */
?>

<div class="data-test-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
	'layout' => 'horizontal',
	'fieldConfig' => [
	    'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
	    'horizontalCssClasses' => [
		'label' => 'col-sm-2',
		'offset' => 'col-sm-offset-3',
		'wrapper' => 'col-sm-6',
		'error' => '',
		'hint' => '',
	    ],
	],
    ]); ?>

    <?= $form->field($model, 'dt_id') ?>

    <?= $form->field($model, 'dt_name') ?>

    <?= $form->field($model, 'dt_text') ?>

    <?= $form->field($model, 'dt_province') ?>

    <?= $form->field($model, 'dt_enable') ?>

    <?php // echo $form->field($model, 'dt_tel') ?>

    <?php // echo $form->field($model, 'dt_email') ?>

    <?php // echo $form->field($model, 'dt_num') ?>

    <?php // echo $form->field($model, 'dt_public') ?>

    <?php // echo $form->field($model, 'dt_like') ?>

    <?php // echo $form->field($model, 'dt_input') ?>

    <?php // echo $form->field($model, 'dt_geography') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'created_by') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <?php // echo $form->field($model, 'updated_by') ?>

    <div class="form-group">
	<div class="col-sm-offset-2 col-sm-6">
	    <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
	    <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
	</div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
