<?php

namespace backend\modules\test\controllers;

use yii\web\Controller;

class DefaultController extends Controller
{
    public function actionIndex()
    {
        return $this->render('index');
    }
    
    public function actionIndexTest()
    {
        return $this->render('index_1');
    }
    public function actionNut(){
        return $this->render("nut");
    }
}
