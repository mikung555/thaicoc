<?php

namespace backend\modules\test\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\test\models\DataTest;

/**
 * DataTestSearch represents the model behind the search form about `backend\modules\test\models\DataTest`.
 */
class DataTestSearch extends DataTest
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['dt_id', 'dt_province', 'dt_enable', 'dt_public', 'dt_geography', 'created_by', 'updated_by'], 'integer'],
            [['dt_name', 'dt_text', 'dt_tel', 'dt_email', 'dt_like', 'dt_input', 'created_at', 'updated_at'], 'safe'],
            [['dt_num'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DataTest::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'dt_id' => $this->dt_id,
            'dt_province' => $this->dt_province,
            'dt_enable' => $this->dt_enable,
            'dt_num' => $this->dt_num,
            'dt_public' => $this->dt_public,
            'dt_geography' => $this->dt_geography,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'dt_name', $this->dt_name])
            ->andFilterWhere(['like', 'dt_text', $this->dt_text])
            ->andFilterWhere(['like', 'dt_tel', $this->dt_tel])
            ->andFilterWhere(['like', 'dt_email', $this->dt_email])
            ->andFilterWhere(['like', 'dt_like', $this->dt_like])
            ->andFilterWhere(['like', 'dt_input', $this->dt_input]);

        return $dataProvider;
    }
}
