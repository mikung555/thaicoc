<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\helpers\Url;
use appxq\sdii\widgets\GridView;
use appxq\sdii\widgets\ModalForm;
use appxq\sdii\helpers\SDNoty;
use appxq\sdii\helpers\SDHtml;
use kartik\popover\PopoverX;
/* @var $this yii\web\View */
/* @var $searchModel backend\modules\tccadmin\models\BuffeDataTableSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'จัดการ HIS View สำหรับตาราง ' . $table;
$this->params['breadcrumbs'][] = $this->title;

global $tbname;

$tbname=$table;

?>
<div class="buffe-data-table-view-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php  Pjax::begin(['id'=>'buffe-data-table-view-grid-pjax']);?>
    <?= GridView::widget([
	'id' => 'buffe-data-table-view-grid',
	'dataProvider' => $dataprovider,
        'panelBtn' => Html::a('<span class="fa fa-arrow-circle-o-left"></span> กลับไปหน้าจัดการตาราง', Url::to(['buffe-data-table/']), [
                                    'title' => Yii::t('app', 'Go back'),
                                    'class'=>'btn btn-success btn-sm',
			    ]),
        'columns' => [
	    [
		'class' => 'yii\grid\SerialColumn',
		'headerOptions' => ['style'=>'text-align: center;'],
		'contentOptions' => ['style'=>'width:60px;text-align: center;'],
	    ],
            [
                'attribute'=>'sitecode',
                'label' => 'เลขหน่วยบริการ',
            ],   
            [
                'attribute'=>'his',
                'label' => 'ชื่อ HIS',
                'value'=> function ($model) {
                    $his=  Yii::$app->db->createCommand("select name from all_hospital_thai where hcode=".$model['sitecode'])->queryOne();
                    return $his['name'];
                }
            ],   
            [
                'label' => 'จำนวน',
                'attribute'=>'nrecs',
                'contentOptions'=>[ 'style'=>'width: 300px','align'=>'left'], 
            ],
              
        ],
    ]); 
                      
?>
    
    <?php  Pjax::end();?>

</div>

<?=  ModalForm::widget([
    'id' => 'modal-buffe-data-table-view',
    'size'=>'modal-lg',
]);
?>

<?php  $this->registerJs("
$('#buffe-data-table-view-grid-pjax').on('click', '#modal-addbtn-buffe-data-table-view', function() {
    modalBuffeDataTable($(this).attr('data-url'));
});

$('#buffe-data-table-view-grid-pjax').on('click', '#modal-delbtn-buffe-data-table-view', function() {
    selectionBuffeDataTableGrid($(this).attr('data-url'));
});

$('#buffe-data-table-view-grid-pjax').on('click', '.select-on-check-all', function() {
    window.setTimeout(function() {
	var key = $('#buffe-data-table-view-grid').yiiGridView('getSelectedRows');
	disabledBuffeDataTableBtn(key.length);
    },100);
});

$('#buffe-data-table-view-grid-pjax').on('click', '.selectionBuffeDataTableIds', function() {
    var key = $('input:checked[class=\"'+$(this).attr('class')+'\"]');
    disabledBuffeDataTableBtn(key.length);
});

$('#buffe-data-table-view-grid-pjax').on('click', 'tbody tr td a', function() {
    var url = $(this).attr('href');
    var action = $(this).attr('data-action');

    if(action === 'updateview' || action === 'createview') {
	modalBuffeDataTable(url);
        return false;
    } else if(action === 'delete') {
	yii.confirm('".Yii::t('app', 'Are you sure you want to delete this item?')."', function() {
	    $.post(
		url
	    ).done(function(result) {
		if(result.status == 'success') {
		    ". SDNoty::show('result.message', 'result.status') ."
		    $.pjax.reload({container:'#buffe-data-table-view-grid-pjax'});
		} else {
		    ". SDNoty::show('result.message', 'result.status') ."
		}
	    }).fail(function() {
		". SDNoty::show("'" . SDHtml::getMsgError() . "Server Error'", '"error"') ."
		console.log('server error');
	    });
	});
        return false;
    }
    //return false;
});

function disabledBuffeDataTableBtn(num) {
    if(num>0) {
	$('#modal-delbtn-buffe-data-table-view').attr('disabled', false);
    } else {
	$('#modal-delbtn-buffe-data-table-view').attr('disabled', true);
    }
}

function selectionBuffeDataTableGrid(url) {
    yii.confirm('".Yii::t('app', 'Are you sure you want to delete these items?')."', function() {
	$.ajax({
	    method: 'POST',
	    url: url,
	    data: $('.selectionBuffeDataTableIds:checked[name=\"selection[]\"]').serialize(),
	    dataType: 'JSON',
	    success: function(result, textStatus) {
		if(result.status == 'success') {
		    ". SDNoty::show('result.message', 'result.status') ."
		    $.pjax.reload({container:'#buffe-data-table-view-grid-pjax'});
		} else {
		    ". SDNoty::show('result.message', 'result.status') ."
		}
	    }
	});
    });
}

function modalBuffeDataTable(url) {
    $('#modal-buffe-data-table-view .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
    $('#modal-buffe-data-table-view').modal('show')
    .find('.modal-content')
    .load(url);
}

");?>