<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\helpers\Url;
use appxq\sdii\widgets\GridView;
use appxq\sdii\widgets\ModalForm;
use appxq\sdii\helpers\SDNoty;
use appxq\sdii\helpers\SDHtml;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\tccadmin\models\BuffeDataTableSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'จัดการตารางข้อมูล (เฉพาะที่ Trigger มา)';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="buffe-data-table-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php  Pjax::begin(['id'=>'buffe-data-table-grid-pjax']);?>
    <?= GridView::widget([
	'id' => 'buffe-data-table-grid',
	'panelBtn' => Html::button(SDHtml::getBtnAdd(), ['data-url'=>Url::to(['buffe-data-table/create']), 'class' => 'btn btn-success btn-sm', 'id'=>'modal-addbtn-buffe-data-table']),
	'dataProvider' => $dataProvider,
	'filterModel' => $searchModel,
        'columns' => [
	    [
		'class' => 'yii\grid\SerialColumn',
		'headerOptions' => ['style'=>'text-align: center;'],
		'contentOptions' => ['style'=>'width:60px;text-align: center;'],
	    ],
            'tbname',
            'title',
            //'description:ntext',
            [
                'label' => 'View',
                'format'=>'html',
                'contentOptions' => ['style' => 'width:100px;'], 
                'value'=>function($model, $key, $index, $column) use ($siteconfig) {
                  $view = \backend\modules\tccadmin\models\BuffeDataTableView::findOne(['his'=>$siteconfig['his_type'],'tbname'=>$model->tbname,'sitecode'=>$siteconfig['id']]);
                  
                  
                  if (trim($view->viewsql) != "") return '<i class="fa fa-file-text-o" aria-hidden="true"></i>';
                  return trim($model->description)!="" ? '<i class="fa fa-file-text-o" aria-hidden="true"></i>':"";
                }                
            ],
            [
                'attribute' => 'enable',
                'format'=>'html',
                'value'=>function($model, $key, $index, $column){
                  //return $model->enable==1 ? "Active":"Inactive";
		    if ($model->enable==1) {
			return Html::button('<i class="glyphicon glyphicon-ok"></i>', [
			    'class' => 'manager-btn btn btn-xs btn-primary',
			    'data-id' => $model->id,
			    'data-url' => yii\helpers\Url::to(['enable', 'id' => $model->id, 'setto'=>'0'])
			]);
		    } else {
			return Html::button('<i class="glyphicon " style="padding-right: 6px; padding-left: 6px;"></i>',[
			    'class' => 'manager-btn btn btn-xs btn-default',
			    'data-id' => $model->id,
			    'data-url' => yii\helpers\Url::to(['enable', 'id' => $model->id, 'setto'=>'1'])
			]);
		    }                
                },
                'contentOptions' => ['style'=>'width:70px;text-align: center;'],
            ],

	    [
		'class' => 'appxq\sdii\widgets\ActionColumn',
		'contentOptions' => ['style'=>'width:80px;text-align: center;'],
		'template' => '{update} {fields} {view} {report}',
                'buttons' => [
                    'update' => function ($url, $model) {
			    return Html::a('<span class="fa fa-edit"></span> Edit', $url, [
                                    'title' => Yii::t('app', 'Update'),
                                    'class'=>'btn btn-success btn-xs',
                                    'data-action'=>'update',
                                    'data-pjax'=>'0',
			    ]);
			
                    },
                    'fields' => function ($url, $model) {
                            if ($model->enable==0) return;
			    return Html::a('<span class="fa fa-building"></span> Fields', Url::to(['buffe-data-table/fields?table='.$model->tbname]), [
                                    'title' => Yii::t('app', 'Update'),
                                    'class'=>'btn btn-info btn-xs',
			    ]);
			
                    },
                    'view' => function ($url, $model) {
                            if ($model->enable==0) return;
			    return Html::a('<span class="fa fa-cog fa-spin fa-fw"></span> SQL View', Url::to(['buffe-data-table/views?table='.$model->tbname]), [
                                    'title' => Yii::t('app', 'Update'),
                                    'class'=>'btn btn-primary btn-xs',
			    ]);
			
                    },                            
                    'report' => function ($url, $model) {
                            if ($model->enable==0) return;
			    return Html::a('<span class="fa fa-pie-chart fa-spin fa-fw"></span> Report', Url::to(['buffe-data-table/report?table='.$model->tbname]), [
                                    'title' => Yii::t('app', 'Update'),
                                    'class'=>'btn btn-primary btn-xs',
			    ]);
			
                    },                                                        
                ],
                'contentOptions' => ['style' => 'width:300px;']                
	    ],
        ],
    ]); ?>
    <?php  Pjax::end();?>

</div>

<?=  ModalForm::widget([
    'id' => 'modal-buffe-data-table',
    'size'=>'modal-lg',
]);
?>

<?php  $this->registerJs("
$('#buffe-data-table-grid-pjax').on('click', '#modal-addbtn-buffe-data-table', function() {
    modalBuffeDataTable($(this).attr('data-url'));
});

$('#buffe-data-table-grid-pjax').on('click', '#modal-delbtn-buffe-data-table', function() {
    selectionBuffeDataTableGrid($(this).attr('data-url'));
});

$('#buffe-data-table-grid-pjax').on('click', '.select-on-check-all', function() {
    window.setTimeout(function() {
	var key = $('#buffe-data-table-grid').yiiGridView('getSelectedRows');
	disabledBuffeDataTableBtn(key.length);
    },100);
});

$('#buffe-data-table-grid-pjax').on('click', '.selectionBuffeDataTableIds', function() {
    var key = $('input:checked[class=\"'+$(this).attr('class')+'\"]');
    disabledBuffeDataTableBtn(key.length);
});

$('#buffe-data-table-grid-pjax').on('dblclick', 'tbody tr', function() {
    var id = $(this).attr('data-key');
    modalBuffeDataTable('".Url::to(['buffe-data-table/update', 'id'=>''])."'+id);
});	

$('#buffe-data-table-grid-pjax').on('click', 'tbody tr td a', function() {
    var url = $(this).attr('href');
    var action = $(this).attr('data-action');

    if(action === 'update' || action === 'view') {
	modalBuffeDataTable(url);
        return false;
    } else if(action === 'delete') {
	yii.confirm('".Yii::t('app', 'Are you sure you want to delete this item?')."', function() {
	    $.post(
		url
	    ).done(function(result) {
		if(result.status == 'success') {
		    ". SDNoty::show('result.message', 'result.status') ."
		    $.pjax.reload({container:'#buffe-data-table-grid-pjax'});
		} else {
		    ". SDNoty::show('result.message', 'result.status') ."
		}
	    }).fail(function() {
		". SDNoty::show("'" . SDHtml::getMsgError() . "Server Error'", '"error"') ."
		console.log('server error');
	    });
	});
        return false;
    }
    //return false;
});

function disabledBuffeDataTableBtn(num) {
    if(num>0) {
	$('#modal-delbtn-buffe-data-table').attr('disabled', false);
    } else {
	$('#modal-delbtn-buffe-data-table').attr('disabled', true);
    }
}

function selectionBuffeDataTableGrid(url) {
    yii.confirm('".Yii::t('app', 'Are you sure you want to delete these items?')."', function() {
	$.ajax({
	    method: 'POST',
	    url: url,
	    data: $('.selectionBuffeDataTableIds:checked[name=\"selection[]\"]').serialize(),
	    dataType: 'JSON',
	    success: function(result, textStatus) {
		if(result.status == 'success') {
		    ". SDNoty::show('result.message', 'result.status') ."
		    $.pjax.reload({container:'#buffe-data-table-grid-pjax'});
		} else {
		    ". SDNoty::show('result.message', 'result.status') ."
		}
	    }
	});
    });
}

function modalBuffeDataTable(url) {
    $('#modal-buffe-data-table .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
    $('#modal-buffe-data-table').modal('show')
    .find('.modal-content')
    .load(url);
}

");?>