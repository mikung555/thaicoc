<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use appxq\sdii\helpers\SDNoty;
use appxq\sdii\helpers\SDHtml;
use kartik\helpers\Enum;

/* @var $this yii\web\View */
/* @var $model backend\modules\tccadmin\models\BuffeDataTable */
/* @var $form yii\bootstrap\ActiveForm */
if ($model->tbname=="" & $tbname != "") {
    $model->tbname=$tbname;
    $model->his=$his;
}

$listhis=\Yii::$app->db->createCommand("SELECT id,his_name from buffe_his where status<>0 order by id")->queryAll();
$hisitems = \yii\helpers\ArrayHelper::map($listhis, 'id', 'his_name');
?>

<div class="buffe-data-table-view-form">

    <?php $form = ActiveForm::begin([
	'id'=>$model->formName(),
    ]); ?>

    <div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	<h4 class="modal-title" id="itemModalLabel">จัดการ SQL View</h4>
    </div>

    <div class="modal-body">
	<div class="row">
            <div class="col-md-4">
        <?= $form->field($model, 'sitecode')->textInput(['maxlength' => true,'readonly'=> true]) ?>
            </div>
            <div class="col-md-4">
        <?= $form->field($model, 'tbname')->textInput(['maxlength' => true,'readonly'=> true]) ?>
            </div>
            <div class="col-md-4">
        <?= $form->field($model, 'his')->dropDownList($hisitems,["readonly"=>"true"]) ?>
            </div>
        </div>
        <?= $form->field($model, 'viewsql')->textArea(['rows' => '10']) ?>        
        <div class="col-md-8">
            <div class="row">
                <div class="col-md-6">
                    <?= $form->field($model, 'tbtrigger')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-6">
                            <p style="margin-top: 10px;"><b>Trigger ON</b></p>
                        </div>                        
                        <div class="col-md-6">
                            <?= $form->field($model, 'tinsert')->checkbox() ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <?= $form->field($model, 'tupdate')->checkbox() ?>
                        </div>
                        <div class="col-md-6">
                            <?= $form->field($model, 'tdelete')->checkbox() ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <?= $form->field($model, 'lfield1')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-md-6">
                    <?= $form->field($model, 'rfield1')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-md-6">
                    <?= $form->field($model, 'lfield2')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-md-6">
                    <?= $form->field($model, 'rfield2')->textInput(['maxlength' => true]) ?>
                </div>                
                <div class="col-md-6">
                    <?= $form->field($model, 'lfield3')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-md-6">
                    <?= $form->field($model, 'rfield3')->textInput(['maxlength' => true]) ?>
                </div>                
            </div>
        </div>
        <div class="col-md-4">
            <b>สัญลักษณ์พิเศษที่ใช้เป็นการเฉพาะ</b>
        <?php
            $data = [
                ['สัญลักษณ์พิเศษ' => '_SITECODE_', 'ความมหาย' => 'รหัสหน่วยบริการ'],
                ['_SECRETKEY_', 'กุญแจถอดรหัส'],
                ['_KHET_', 'เขต'],
                ['_BUFFE_DB_','ฐานข้อมูล TCC'],
            ];
            echo Enum::array2table($data,false,false,false);
        ?>
        </div>
    </div>
    <div class="modal-footer">
	<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Update') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	<?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php  $this->registerJs("
$('form#{$model->formName()}').on('beforeSubmit', function(e) {
    var \$form = $(this);
    $.post(
	\$form.attr('action'), //serialize Yii2 form
	\$form.serialize()
    ).done(function(result) {
	if(result.status == 'success') {
	    ". SDNoty::show('result.message', 'result.status') ."
	    if(result.action == 'createview') {
                $(document).find('#modal-buffe-data-table-view').modal('hide');
		$(\$form).trigger('reset');
		$.pjax.reload({container:'#buffe-data-table-view-grid-pjax'});
	    } else if(result.action == 'updateview') {
		$(document).find('#modal-buffe-data-table-view').modal('hide');
		$.pjax.reload({container:'#buffe-data-table-view-grid-pjax'});
	    }
	} else {
	    ". SDNoty::show('result.message', 'result.status') ."
	} 
    }).fail(function() {
	". SDNoty::show("'" . SDHtml::getMsgError() . "Server Error'", '"error"') ."
	console.log('server error');
    });
    return false;
});

");?>