<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\tccadmin\models\BuffeDataTable */

$this->title = 'Update Buffe Data Table: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Buffe Data Tables', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="buffe-data-table-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
