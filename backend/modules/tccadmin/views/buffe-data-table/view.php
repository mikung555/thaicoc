<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\tccadmin\models\BuffeDataTable */

$this->title = 'Buffe Data Table#'.$model->id;
$this->params['breadcrumbs'][] = ['label' => 'Buffe Data Tables', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="buffe-data-table-view">

    <div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	<h4 class="modal-title" id="itemModalLabel"><?= Html::encode($this->title) ?></h4>
    </div>
    <div class="modal-body">
        <?= DetailView::widget([
	    'model' => $model,
	    'attributes' => [
		'id',
		'tborder',
		'tbname',
		'description:ntext',
		'enable',
	    ],
	]) ?>
    </div>
</div>
