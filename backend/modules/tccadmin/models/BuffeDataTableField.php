<?php

namespace backend\modules\tccadmin\models;

use Yii;

/**
 * This is the model class for table "buffe_data_table_field".
 *
 * @property string $tbname
 * @property string $fname
 * @property string $caption
 * @property string $description
 */
class BuffeDataTableField extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'buffe_data_table_field';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tbname', 'fname'], 'required'],
            [['description'], 'string'],
            [['tbname', 'fname', 'caption'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'tbname' => 'ชื่อตาราง',
            'fname' => 'ชื่อฟิลด์',
            'caption' => 'ความหมาย',
            'description' => 'รายละเอียด',
        ];
    }

    /**
     * @inheritdoc
     * @return BuffeDataTableFieldQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new BuffeDataTableFieldQuery(get_called_class());
    }
}
