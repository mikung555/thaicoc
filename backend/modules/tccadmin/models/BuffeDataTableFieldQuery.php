<?php

namespace backend\modules\tccadmin\models;

/**
 * This is the ActiveQuery class for [[BuffeDataTableField]].
 *
 * @see BuffeDataTableField
 */
class BuffeDataTableFieldQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return BuffeDataTableField[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return BuffeDataTableField|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}