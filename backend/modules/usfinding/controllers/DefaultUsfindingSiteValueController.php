<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace backend\modules\usfinding\controllers;

use yii\helpers\VarDumper;
use yii\web\Controller;
use Yii;

/**
 * Description of DefaultUsfindingSiteValue
 *
 * @author chaiwat
 */
class DefaultUsfindingSiteValueController extends Controller {
    //put your code here
    
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }
    
    public function GetDefaultUSFinding(){
        $siteCode = Yii::$app->user->identity->userProfile->sitecode;
        $user_id=Yii::$app->user->identity->userProfile->user_id;
        $hospital = self::GetHospitalDetail($siteCode);                 // ดึงรายละเอียด ของข้อมูลในโรงพยาบาลนั้นๆ
        $usdet = self::GetUSDetail($siteCode);                          // รายละเอียด เกี่ยวกับการ Ultrasound
        $out['sitecode']=$siteCode;
        $out['userid']=$user_id;
        $out['zone_code'] = $hospital['zone_code'];
        $out['provincecode'] = $hospital['provincecode'];
        $out['amphurcode'] = (strlen($hospital['amphurcode'])==0 ? '01' : $hospital['amphurcode']);
        $out['amphur'] = $hospital['amphur'];
        $out['amphurlist'] = self::GetAmphurListFromProvince($out['provincecode']);
        $out['tamboncode'] = $hospital['tamboncode'];
        $out['hospitalname'] = $hospital['name'];
        $out['hospitallist'] = self::GetHospitalListFromAmphur($out['provincecode'],$out['amphurcode']);
        $out['inputStartDate'] = ( (strlen($usdet['sf2v1'])==0 || $usdet['sf2v1']=='0000-00-00') ? '2013-02-09' : $usdet['sf2v1']);
        $out['inputEndDate'] = ( (strlen($usdet['ef2v1'])==0 || $usdet['ef2v1']=='0000-00-00') ? date('Y-m-d') : $usdet['ef2v1']);
        return $out;
    }
    
    public function GetHospitalDetail($hsitecode){
        $sql = "select * from all_hospital_thai where hcode='".$hsitecode."' ";
        $out = Yii::$app->db->createCommand($sql)->queryOne();
        return $out;
    }
    
    public function GetAmphurListFromProvince($provincecode){
        $sql = "select distinct amphurcode,amphur from all_hospital_thai where provincecode='".$provincecode."' order by amphurcode ";
        $out = Yii::$app->db->createCommand($sql)->queryAll();
        return $out;
    }
    
    public function GetHospitalListFromAmphur($provincecode,$amphurcode){
        $sql = "select distinct hcode,name from all_hospital_thai where provincecode='".$provincecode."' and amphurcode='".$amphurcode."' order by hcode ";
        $out = Yii::$app->db->createCommand($sql)->queryAll();
        return $out;
    }
    
    public function GetUSDetail($hsitecode){
        $sql = "select min(f2v1) as sf2v1, max(f2v1) as ef2v1 ";
        $sql.= "from tb_data_3 ";
        $sql.= "where hsitecode='".$hsitecode."' or sitecode='".$hsitecode."' ";
        $out = Yii::$app->db->createCommand($sql)->queryOne();
        return $out;
    }
}
