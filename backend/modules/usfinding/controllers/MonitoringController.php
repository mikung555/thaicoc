<?php

namespace backend\modules\usfinding\controllers;

use yii\web\Controller;
use Yii;
use frontend\controllers\classes\ModulesReport;
use backend\modules\usfinding\classes\QueryMonitor;
use backend\modules\ezforms\components\EzformQuery;
use backend\modules\usfinding\classes\SuspectedController;
use yii\helpers\Url;
use yii\jui\DatePicker;
use appxq\sdii\utils\ToDate;

class MonitoringController extends Controller {

    public function actionIndex() {

        $qryProvince = $this->getProvince();
        $qryUsTour = $this->getUsTour();
        $qryZone = $this->getZone();
        $qryUsSite = $this->getUsSite();
        $worklistno = isset($_SESSION['worklistno']) ? $_SESSION['worklistno'] : null;
        
        $session = \Yii::$app->session;
        $table_us = $session['table_us'];
        $refresh_time = $session['refresh_time'];
        $auto_reload = $session['auto_reload'];
        if($table_us ==''){
            $table_us = "tb_data_3";
            $_SESSION['table_us'];
        }
        
        if($refresh_time == ''){
            $refresh_time = '5';
            $_SESSION['refresh_time'] = $refresh_time;
        }
        
        $startDate = NULL;
        $endDate = NULL;
        $sitecode = \Yii::$app->user->identity->userProfile->sitecode;
        if ($worklistno == '' || $worklistno == NULL) {
            $workilistid = " SELECT * FROM worklist WHERE sitecode=:sitecode ORDER BY id DESC LIMIT 1 ";
            $resWork = \Yii::$app->db->createCommand($workilistid, [':sitecode' => $sitecode])->queryOne();
            
            $worklistno = $resWork['id'];
            $_SESSION['worklistno'] = $resWork['id'];
            $startDate = $resWork['start_date'];
            $endDate = $resWork['end_date'];
        }else if($worklistno != $sitecode){
            $workilistid = " SELECT * FROM worklist WHERE id=:id ";
            $resWork = \Yii::$app->db->createCommand($workilistid, [':id' => $worklistno])->queryOne();
            
            $_SESSION['worklistno'] = $resWork['id'];
            $worklistno = $resWork['id'];
            $startDate = $resWork['start_date'];
            $endDate = $resWork['end_date'];
        }else if($worklistno == $sitecode){
            $checkDate = " SELECT update_date FROM tb_data_3 WHERE hsitecode='$sitecode' ORDER BY update_date DESC LIMIT 1  ";
            $resCheck = \Yii::$app->db->createCommand($checkDate)->queryOne();
            
            $date = strtotime($resCheck['update_date']);
            $startDate = date('Y-m-d',$date);
            $endDate = date('Y-m-d');
        }
        
        $checkMon = " SELECT COUNT(id) FROM worklist WHERE  id=:id AND NOW() BETWEEN start_date AND end_date ";
        $resChk = \Yii::$app->db->createCommand($checkMon,[':id'=>$worklistno])->queryScalar();
        //\appxq\sdii\utils\VarDumper::dump($resChk);
        $isMonitor = 'false';
        if($auto_reload == 'true' || $auto_reload == true){
            $isMonitor = 'true';
        }
        $dfUSFinding = DefaultUsfindingSiteValueController::GetDefaultUSFinding();

        return $this->render('index', [
                    'dfUSFinding' => $dfUSFinding,
                    'zone' => $qryZone,
                    'province' => $qryProvince,
                    'usTour' => $qryUsTour,
                    'usSite' => $qryUsSite,
                    'worklist' => $worklistno,
                    'startDate' => $startDate,
                    'endDate' => $endDate,
                    'isMonitor' => $isMonitor
        ]);
    }

    private function getUsTour() {
        $sqlUsTour = "SELECT * FROM `history_us_tour` ORDER BY `times` DESC";

        $qryUsTour = Yii::$app->dbcascap->createCommand($sqlUsTour)->queryAll();

        return $qryUsTour;
    }

    private function getUsSite() {
        $sqlUsTour = "SELECT ussite.No, ussite.dateatsite, ussite.hcode, ussite.hospitalname ";
        $sqlUsTour .= ",hospital.zone_code as zonecode ";
        $sqlUsTour .= ",hospital.provincecode as provcode ";
        $sqlUsTour .= ",concat(hospital.provincecode,hospital.amphurcode) as ampcode ";
        $sqlUsTour .= ",NOW() as edate ";
        $sqlUsTour .= "from history_us_site ussite ";
        $sqlUsTour .= "left join all_hospital_thai hospital ";
        $sqlUsTour .= "on hospital.hcode=ussite.hcode ";
        $sqlUsTour .= "order by ussite.dateatsite ";

        $qryUsTour = Yii::$app->db->createCommand($sqlUsTour)->queryAll();

        return $qryUsTour;
    }

    private function getZone() {
        $sqlZone = "SELECT zone_code, zone_name FROM `cascapcloud`.`all_hospital_thai` WHERE zone_code IS NOT NULL " .
                "GROUP BY zone_code ORDER BY zone_code";

        $qryZone = Yii::$app->db->createCommand($sqlZone)->queryAll();

        return $qryZone;
    }

    private function getProvince($zoneCode = null) {
        if (is_null($zoneCode) || $zoneCode == 0) {
            $sqlProvince = "SELECT provincecode as PROVINCE_CODE, province as PROVINCE_NAME, zone_code " .
                    "FROM `cascapcloud`.`all_hospital_thai` " .
                    "WHERE provincecode IS NOT NULL " .
                    "GROUP BY provincecode " .
                    "ORDER BY province";
        } else {
            $sqlProvince = "SELECT provincecode as PROVINCE_CODE, province as PROVINCE_NAME, zone_code " .
                    "FROM `cascapcloud`.`all_hospital_thai` " .
                    "WHERE provincecode IS NOT NULL " .
                    "AND zone_code LIKE '%$zoneCode%' " .
                    "GROUP BY PROVINCE_CODE " .
                    "ORDER BY PROVINCE_NAME ";
        }

        $qryProvince = Yii::$app->db->createCommand($sqlProvince)->queryAll();

        return $qryProvince;
    }

    private function getAmphur($provinceCode) {
        $sqlAmphur = "SELECT `code6`, provincecode as PROVINCE_CODE, province as PROVINCE_NAME, amphurcode as AMPHUR_CODE, amphur as AMPHUR_NAME, zone_code " .
                "FROM `cascapcloud`.`all_hospital_thai` " .
                "WHERE provincecode LIKE '%$provinceCode%' " .
                "AND all_hospital_thai.amphurcode NOT LIKE '' " .
                "GROUP BY amphurcode " .
                "ORDER BY amphur";

        $qryAmphur = Yii::$app->db->createCommand($sqlAmphur)->queryAll();

        return $qryAmphur;
    }

    private function getHospital($provinceCode, $amphurCode) {
        $amphurCode = substr($amphurCode, 2, 2);
        $sqlAllHospitalThai = "SELECT hcode, name, code6, provincecode, province, amphurcode, amphur, zone_code " .
                "FROM `cascapcloud`.`all_hospital_thai` " .
                "WHERE provincecode LIKE '%$provinceCode%' " .
                "AND amphurcode LIKE '%$amphurCode%' " .
                "GROUP BY hcode ORDER BY name";

        $qryAllHospitalThai = Yii::$app->db->createCommand($sqlAllHospitalThai)->queryAll();

        return $qryAllHospitalThai;
    }
    
    public function actionOpenForm(){
        $ezf_id = \Yii::$app->request->get('ezf_id');
        $dataid = \Yii::$app->request->get('dataid');
        $ptid = \Yii::$app->request->get('ptid');
        $url="/inputdata/redirect-page?dataid=$dataid&ezf_id=$ezf_id";
        return $this->redirect([$url]);
    }
    
    public function actionAddUsroom(){
        $sitecode = \Yii::$app->user->identity->userProfile->sitecode;
        $user_id = \Yii::$app->user->identity->userProfile->user_id;
        $worklist_id = \Yii::$app->request->post('worklist_id');
        $room = " SELECT MAX(room_name) as room_name FROM us_room WHERE room_type='us-room' AND  worklist_id=:worklist_id ";
        $resRoom = \Yii::$app->db->createCommand($room,[':worklist_id'=>$worklist_id])->queryOne();
        $new_room = $resRoom['room_name'] + 1;
        
        $insert =" INSERT INTO us_room(room_name, room_type, worklist_id, user_create, create_date)
            VALUES('$new_room', 'us-room', '$worklist_id', '$user_id',NOW())"; 
        $resInsert = \Yii::$app->db->createCommand($insert)->execute();
        
        return true;
    }
    
    public function actionAddExitnurse(){
        $sitecode = \Yii::$app->user->identity->userProfile->sitecode;
        $user_id = \Yii::$app->user->identity->userProfile->user_id;
        $worklist_id = \Yii::$app->request->post('worklist_id');
        $room = " SELECT MAX(room_name) as room_name FROM us_room WHERE room_type='exit-nurse' AND worklist_id=:worklist_id ";
        $resRoom = \Yii::$app->db->createCommand($room,[':worklist_id'=>$worklist_id])->queryOne();
        $new_room = $resRoom['room_name'] + 1;
        
        $insert =" INSERT INTO us_room(room_name, room_type, worklist_id, user_create, create_date)
            VALUES('$new_room', 'exit-nurse', '$worklist_id', '$user_id',NOW())"; 
        $resInsert = \Yii::$app->db->createCommand($insert)->execute();
        
        return true;
    }
    
    public function actionAddRefer(){
        $sitecode = \Yii::$app->user->identity->userProfile->sitecode;
        $user_id = \Yii::$app->user->identity->userProfile->user_id;
        $worklist_id = \Yii::$app->request->post('worklist_id');
        $room = " SELECT MAX(room_name) as room_name FROM us_room WHERE worklist_id=:worklist_id AND  room_type='refer' ";
        $resRoom = \Yii::$app->db->createCommand($room,[':worklist_id'=>$worklist_id])->queryOne();
        $new_room = $resRoom['room_name'] + 1;
        
        $insert =" INSERT INTO us_room(room_name, room_type, worklist_id, user_create, create_date)
            VALUES('$new_room', 'refer', '$worklist_id', '$user_id',NOW())"; 
        $resInsert = \Yii::$app->db->createCommand($insert)->execute();
        
        return true;
    }
    
    public function actionAddOther(){
        $sitecode = \Yii::$app->user->identity->userProfile->sitecode;
        $user_id = \Yii::$app->user->identity->userProfile->user_id;
        $worklist_id = \Yii::$app->request->post('worklist_id');
        $room = " SELECT MAX(room_name) as room_name FROM us_room WHERE worklist_id=:worklist_id AND  room_type='other' ";
        $resRoom = \Yii::$app->db->createCommand($room,[':worklist_id'=>$worklist_id])->queryOne();
        $new_room = $resRoom['room_name'] + 1;
        
        $insert =" INSERT INTO us_room(room_name, room_type, worklist_id, user_create, create_date)
            VALUES('$new_room', 'other', '$worklist_id', '$user_id',NOW())"; 
        $resInsert = \Yii::$app->db->createCommand($insert)->execute();
        
        return true;
    }

    public function actionUltrasoundData() {
        $startDate = \Yii::$app->request->post('startDate');
        $endDate = \Yii::$app->request->post('endDate');
        $zone = \Yii::$app->request->post('zone');
        $province = \Yii::$app->request->post('province');
        $amphur = \Yii::$app->request->post('amphur');
        $hospital = \Yii::$app->request->post('hospital');
        $xsourcex = \Yii::$app->user->identity->userProfile->sitecode;
        
        $table_us = $_SESSION['table_us'];
        $refresh_time = $_SESSION['refresh_time'];
        if($table_us ==''){
            $table_us = "tb_data_3";
            $_SESSION['table_us'] =$table_us ;
        }
        
        if($refresh_time == ''){
            $refresh_time = '5';
            $_SESSION['refresh_time'] = $refresh_time;
        }
        $worklistno = isset($_SESSION['worklistno']) ? $_SESSION['worklistno'] : '';
        $drilldownState = true;
        
        if ( $worklistno=='') {
            $drilldownState = false;
        }
        if ($zone != null || $province != null || $amphur != null || $hospital != null) {
            $drilldownState = false;
        }

        $sitename = ModulesReport::getSiteName($xsourcex);
        
        
        $sqlEz = " SELECT ezf_id FROM ezform WHERE ezf_table=:ezf_table";
        $resEz = \Yii::$app->db->createCommand($sqlEz, [':ezf_table'=>$table_us])->queryOne();
        
        $doctorList = QueryMonitor::UltrasoundRoomData($startDate, $endDate, $zone, $province, $amphur, $hospital, $worklistno);
        $nurseList = QueryMonitor::ExitNurseData($worklistno);
        $referList = QueryMonitor::ReferData($worklistno);
        $otherList = QueryMonitor::OtherData($worklistno);
        
        $session = \Yii::$app->session;
        $auto_reload = $session['auto_reload'];
        
        $isMonitor = 'false';
        if($auto_reload == 'true' || $auto_reload == true){
            $isMonitor = 'true';
        }
        //\appxq\sdii\utils\VarDumper::dump($nurseList);
        return $this->renderAjax('monitoring', [
                    'doctorList' => $doctorList,
                    'nurseList'=>$nurseList,
                    'referList' => $referList,
                    'otherList'=>$otherList,
                    'drilldownState' => $drilldownState,
                    'sitename' => $sitename,
                    'worklistno' => $worklistno,
                    'startDate' => $startDate,
                    'endDate' => $endDate,
                    'ezf_id'=>$resEz['ezf_id'],
                    'isMonitor' => $isMonitor
        ]);
    }

    public function actionDoctorUltrasound() {
        $xsourcex = \Yii::$app->user->identity->userProfile->sitecode;
        $sqlDoctor = " SELECT doctorcode, doctorfullname FROM doctor_all WHERE doctorcode=:doctorcode LIMIT 0,50";
        $doctor_code = \Yii::$app->request->post('doctor_code');
        $room = \Yii::$app->request->post('room_name');
        $room_type = \Yii::$app->request->post('room_type');
        $resDoctor = \Yii::$app->db->createCommand($sqlDoctor, [':doctorcode' => $doctor_code])->queryOne();
        return $this->renderAjax('doctor-ultrasound', [
                    'doctor' => $resDoctor,
                    'room_name' => $room,
                    'room_type'=>$room_type
        ]);
    }

    public function actionPatientInspected() {
        $user_id = \Yii::$app->request->post('user_id');
        $worklist_id = \Yii::$app->request->post('worklist_id');
        $room_name = \Yii::$app->request->post('room_name');
        $inspect_type = \Yii::$app->request->post('inspect_type');
        $startDate = \Yii::$app->request->post('startDate');
        $endDate = \Yii::$app->request->post('endDate');
        $patientList = QueryMonitor::PatientUltrasound($startDate,$endDate,$user_id, $inspect_type,$room_name,$worklist_id);
        
        return $this->renderAjax('patient-inspected', [
                    'patientList' => $patientList
        ]);
    }

    public function actionDoctorList($q = null) {
        
        $sqlDoctor = " SELECT doctorcode, doctorfullname FROM doctor_all WHERE CONCAT(IFNULL(doctorcode,''), '', IFNULL(doctorfullname,'')) LIKE '%" . $q . "%' LIMIT 50";
        $resDoctor = \Yii::$app->db->createCommand($sqlDoctor)->queryAll();
        //\appxq\sdii\utils\VarDumper::dump($resSite);
        $out = [];
        $i = 0;
        foreach ($resDoctor as $value) {
            $out["results"][$i] = ['id' => $value['doctorcode'], 'text' => $value["doctorfullname"]];
            $i++;
        }

        return json_encode($out);
    }
    
    public function actionDoctorChange() {
        $doctor_code = \Yii::$app->request->post('doctor_code');
        $room_name = \Yii::$app->request->post('room_name');
        $worklist_id = \Yii::$app->request->post('worklist_id');
        
        $update = " UPDATE us_room SET doctor_code=:doctor_code WHERE room_name=:room_name AND worklist_id=:worklist_id ";
        $result = \Yii::$app->db->createCommand($update, [':doctor_code' => $doctor_code
                , ':room_name' => $room_name, ':worklist_id'=>$worklist_id])->execute();
        if ($result) {
            $txt = "เปลี่ยนแพทย์ประจำห้องตรวจเรียบร้อยแล้ว";
        } else {
            $txt = "มีข้อผิดพลาด! ไม่สามารถเปลี่ยนแพทย์ประจำห้องตรวจได้";
        }
        echo $txt;
    }
    
    public function actionUserUltrasound() {

        $sqlUser = " SELECT user_id, firstname, lastname FROM user_profile WHERE user_id=:user_id LIMIT 0,50";
        $user_id = \Yii::$app->request->post('user_id');
        $room = \Yii::$app->request->post('room_name');
        $room_type = \Yii::$app->request->post('room_type');
        $resUser = \Yii::$app->db->createCommand($sqlUser, [':user_id' => $user_id])->queryOne();
        return $this->renderAjax('user-ultrasound', [
                    'user_us' => $resUser,
                    'room_name' => $room,
                    'room_type' => $room_type
        ]);
    }
    
    public function actionUserList($q = null) {
       
        $xsourcex = \Yii::$app->user->identity->userProfile->sitecode;
        $params = [':sitecode'=>$xsourcex];
        $sqlUser = " SELECT user_id, firstname, lastname FROM user_profile WHERE CONCAT(`user_id`, ' ', `firstname`,' ',`lastname`) LIKE '%" . $q . "%' AND sitecode=:sitecode  LIMIT 0,50";
        $resUser = \Yii::$app->db->createCommand($sqlUser, $params)->queryAll();
        //\appxq\sdii\utils\VarDumper::dump($xsourcex);
        $out = [];
        $i = 0;
        foreach ($resUser as $value) {
            $out["results"][$i] = ['id' => $value['user_id'], 'text' => $value["firstname"].' '.$value["lastname"]];
            $i++;
        }

        return json_encode($out);
    }

    public function actionUserChange() {
        $user_id = \Yii::$app->request->post('user_id');
        $room_name = \Yii::$app->request->post('room_name');
        $worklist_id = \Yii::$app->request->post('worklist_id');
        $room_type = \Yii::$app->request->post('room_type');
        
        $update = " UPDATE us_room SET user_id=:user_id WHERE room_name=:room_name AND worklist_id=:worklist_id AND room_type=:room_type ";
        $result = \Yii::$app->db->createCommand($update, [':user_id' => $user_id
                , ':room_name' => $room_name, ':worklist_id'=>$worklist_id, ':room_type'=>$room_type])->execute();
        if ($result) {
            $txt = "เปลี่ยนเจ้าหน้าที่ประจำห้องตรวจเรียบร้อยแล้ว";
        } else {
            $txt = "มีข้อผิดพลาด! ไม่สามารถเปลี่ยนเจ้าหน้าที่ประจำห้องตรวจได้";
        }
        echo $txt;
    }
    
    public function actionRemoveUsroom(){
        $room_name = \Yii::$app->request->post('room_name');
        $worklist_id = \Yii::$app->request->post('worklist_id');
        $room_type = \Yii::$app->request->post('room_type');
        $delete = " DELETE FROM us_room WHERE room_name=:room_name AND worklist_id=:worklist_id AND room_type=:room_type ";
        $result = \Yii::$app->db->createCommand($delete,['room_name'=>$room_name, 'worklist_id'=>$worklist_id, 'room_type'=>$room_type])->execute();
        
        return true;
    }
    

    public function actionSetting() {
        $worklistno = $_SESSION['worklistno'];
        if (!$worklistno == '' || !$worklistno == NULL) {
            $where = " AND id = '$worklistno' ";
        }
        $sitecode = \Yii::$app->user->identity->userProfile->sitecode;
        $sqlWork = " SELECT id,title,sitecode FROM worklist WHERE CONCAT(id, ' ',`title`, `sitecode`) LIKE '%" . $q . "%' $where AND sitecode=:sitecode ORDER BY id DESC LIMIT 1";
        $resWork = \Yii::$app->db->createCommand($sqlWork, [':sitecode' => $sitecode])->queryOne();
        //\appxq\sdii\utils\VarDumper::dump($worklistno);
        if (!$resWork) {
            $resWork['id'] = $sitecode;
            $resWork['title'] = ': เลือกทั้งหมด';
            $resWork['sitecode'] = '';
        }
        return $this->renderAjax('monitor-setting', ['resWorkDefault' => $resWork]);
    }

    public function actionSetSetting() {
        $request = \Yii::$app->request;
        $table_us = $request->post('table_us');
        $refresh_time = $request->post('refresh_time');
        $worklistno = $request->post('worklistno');
        $auto_reload = $request->post('auto_reload');
        //\appxq\sdii\utils\VarDumper::dump($auto_reload);
        $_SESSION['worklistno'] = $worklistno;
        $_SESSION['table_us'] = $table_us;
        $_SESSION['refresh_time'] = $refresh_time;
        $_SESSION['auto_reload'] = $auto_reload;
    }

    public function actionPatientView() {
        $provider = QueryMonitor::getPatients();
        return $this->renderAjax('patient-view.php', ['provider' => $provider]);
    }

    public function actionReferList() {
        $provider = QueryMonitor::getListSuspected();
        //\appxq\sdii\utils\VarDumper::dump($provider);
        return $this->renderAjax('refer-list.php', ['provider' => $provider]);
    }

}
