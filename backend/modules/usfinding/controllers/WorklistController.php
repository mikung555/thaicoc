<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace backend\modules\usfinding\controllers;

use yii\web\Controller;
use Yii;
use \frontend\modules\api\v1\classes\WorklistQuery;
use backend\modules\usfinding\classes\QueryMonitor;
use backend\modules\ezforms\components\EzformQuery;
use backend\modules\usfinding\classes\SuspectedController;
use yii\data\SqlDataProvider;
use yii\data\ActiveDataProvider;

class WorklistController extends Controller {

    public function actionIndex() {
        $worklist_id = \Yii::$app->request->post('worklist_id');
        $sitecode = $sitecode = \Yii::$app->user->identity->userProfile->sitecode;
        if ($worklist_id == '') {
            $sqlWID = " SELECT max(id) as worklist_id FROM worklist WHERE sitecode=:sitecode ";
            $resWID = \Yii::$app->db->createCommand($sqlWID, [':sitecode' => $sitecode])->queryOne();
            $worklist_id = $resWID['worklist_id'];
        }
        
        $sqlPatient = " SELECT * FROM  worklist_map_patient WHERE worklist_id=:worklist_id";
        $resPatient = \Yii::$app->db->createCommand($sqlPatient,[':worklist_id'=>$worklist_id])->queryAll();
        
        $sqlUser = " SELECT * FROM worklist_map_user WHERE worklist_id=:worklist_id ";
        $resUser = \Yii::$app->db->createCommand($sqlUser,[':worklist_id'=>$worklist_id])->queryAll();
        
        $sqlWork = " SELECT CONCAT(id,' : ',sitecode,' ',`title`) as `fulltext`, id, `name`, DATE(start_date) as start_date, DATE(end_date) as end_date 
            FROM worklist wl INNER JOIN all_hospital_thai al ON wl.sitecode=al.hcode
            WHERE sitecode=:sitecode AND id=:worklist_id";
        $resWork = \Yii::$app->db->createCommand($sqlWork,[':worklist_id'=>$worklist_id,':sitecode'=>$sitecode])->queryOne();
        //\appxq\sdii\utils\VarDumper::dump($resWork);
        return $this->render('index',[
            'resatient'=>$resPatient,
            'resUser'=>$resUser,
            'resWork'=>$resWork
        ]);
    }
    
    public function  actionWorklistData(){
        $worklist_id = \Yii::$app->request->post('worklist_id');
        $sitecode = \Yii::$app->user->identity->userProfile->sitecode;
        $sqlWID = " SELECT * FROM worklist WHERE sitecode=:sitecode AND id=:worklist_id ";
        $resWID = \Yii::$app->db->createCommand($sqlWID, [':sitecode' => $sitecode,':worklist_id'=>$worklist_id])->queryOne();
        
        $arr = [];
        $arr['start_date'] = date('Y-m-d',strtotime($resWID['start_date']));
        $arr['end_date'] = date('Y-m-d', strtotime($resWID['end_date']));
        $arr['worklist_id'] = $resWID['worklist_id'];
        
        return json_encode($arr);
    }

    public function actionPatientWorklist() {
        $worklist_id = \Yii::$app->request->post('worklist_id');
        $sitecode = \Yii::$app->user->identity->userProfile->sitecode;
        if ($worklist_id == '') {
            $sqlWID = " SELECT max(id) as worklist_id FROM worklist WHERE sitecode=:sitecode ";
            $resWID = \Yii::$app->db->createCommand($sqlWID, [':sitecode' => $sitecode])->queryOne();
            $worklist_id = $resWID['worklist_id'];
        }

        $patientWorklist = WorklistQuery::GetPatientFullByWorklistId($worklist_id);
        //\appxq\sdii\utils\VarDumper::dump($patientWorklist);
        $dataProvider = new \yii\data\ArrayDataProvider([
            'allModels' => $patientWorklist,
            'pagination' => [
                'pageSize' => 100,
            ],
        ]);

        return $this->renderAjax('patient-worklist', ['provider' => $dataProvider, 'worklist_id' => $worklist_id]);
    }

    public function actionUserWorklist() {
        $worklist_id = \Yii::$app->request->post('worklist_id');
        $sitecode = $sitecode = \Yii::$app->user->identity->userProfile->sitecode;
        if ($worklist_id == '') {
            $sqlWID = " SELECT max(id) as worklist_id FROM worklist WHERE sitecode=:sitecode ";
            $resWID = \Yii::$app->db->createCommand($sqlWID, [':sitecode' => $sitecode])->queryOne();
            $worklist_id = $resWID['worklist_id'];
        }
        
        $userWorklist = WorklistQuery::GetUserFullByWorklistId($worklist_id);
        
        $dataProvider = new \yii\data\ArrayDataProvider([
            'allModels' => $userWorklist,
            'pagination' => [
                'pageSize' => 100,
            ],
        ]);

        return $this->renderAjax('user-worklist', ['provider' => $dataProvider]);
    }

    public function actionGetPatients($q = null, $zone = null, $province = null, $amphur = null) {
       $sitecode = $sitecode = \Yii::$app->user->identity->userProfile->sitecode;
        $sqlPatient = " SELECT ptid,cid,hptcode,ptcodefull,title,`name`,surname FROM tb_data_1 WHERE CONCAT(hptcode, cid,`title`, `name`,' ', surname) LIKE '%" . $q . "%' AND hsitecode=:sitecode LIMIT 0,50 ";
        $resPatient = \Yii::$app->db->createCommand($sqlPatient,[':sitecode'=>$sitecode])->queryAll();

        $out = [];
        $i = 0;
        foreach ($resPatient as $value) {
            $out["results"][$i] = ['id' => $value['ptid'], 'text' => $value['hptcode'] . ' : ' . $value["title"] . $value["name"] . " " . $value["surname"]];
            $i++;
        }

        return json_encode($out);
    }
    
     public function actionGetWorklistNumber($q = null, $zone = null, $province = null, $amphur = null) {
       $sitecode  = \Yii::$app->user->identity->userProfile->sitecode;
        $sqlWork = " SELECT id,title,sitecode FROM worklist WHERE CONCAT(id, ' ',`title`, `sitecode`) LIKE '%" . $q . "%' AND sitecode=:sitecode ORDER BY id DESC LIMIT 0,50";
        $resWork = \Yii::$app->db->createCommand($sqlWork,[':sitecode'=>$sitecode])->queryAll();
        $resWork2 = array();
        $resWork2[0]['id'] = $sitecode;
        $resWork2[0]['title'] = 'เลือกทั้งหมด';
        $resWork2[0]['sitecode'] = '';
        $x =1;
        foreach ($resWork as $val){
            $resWork2[$x] = $val;
            $x +=1 ;
        }

        $out = [];
        $i = 0;
        foreach ($resWork2 as $value) {
            $out["results"][$i] = ['id' => $value['id'], 'text' => $value['id'] . ' : ' . $value["sitecode"].' ' . $value["title"] ];
            $i++;
        }

        return json_encode($out);
    }

    public function actionGetUserSite($q = null, $zone = null, $province = null, $amphur = null) {
        $sitecode  = \Yii::$app->user->identity->userProfile->sitecode;
        $sqlUser = " SELECT user_id,firstname,`middlename`,lastname FROM user_profile WHERE sitecode=:sitecode AND CONCAT(user_id,cid,IFNULL(firstname,''), IFNULL(lastname,'')) LIKE '%" . $q . "%' LIMIT 50";
        $resUser = \Yii::$app->db->createCommand($sqlUser,[':sitecode'=>$sitecode])->queryAll();

        $out = [];
        $i = 0;
        foreach ($resUser as $value) {
            $out["results"][$i] = ['id' => $value['user_id'], 'text' => $value['user_id'] . ' : ' . $value["firstname"] . $value["middlename"] . " " . $value["lastname"]];
            $i++;
        }

        return json_encode($out);
    }

    public function actionCheckWorklist() {
        $result = false;
        $sitecode = \Yii::$app->user->identity->userProfile->sitecode;
        $sqlChk = " SELECT count(*) as `count` FROM worklist WHERE sitecode=:sitecode";
        $resChk = \Yii::$app->db->createCommand($sqlChk, [':sitecode' => $sitecode])->queryOne();
        if ($resChk['count'] > 0) {
            $result = true;
        }

        return $result;
    }

    public function actionCreateWorklist() {
        $sitecode = \Yii::$app->user->identity->userProfile->sitecode;
        $user_id = \Yii::$app->user->identity->userProfile->user_id;
        $startdate = \Yii::$app->request->post('startdate');
        $enddate = \Yii::$app->request->post('enddate');
        $sitename = \backend\classes\MonitorReport::getSiteName($sitecode);
        $worklist_id = WorklistQuery::CreateWorklist($user_id, $sitename, $sitecode, $startdate, $enddate, true);

        return $worklist_id;
    }
    
    public function actionWorklistAll(){
        $sitecode = \Yii::$app->user->identity->userProfile->sitecode;
        $user_id = \Yii::$app->user->identity->userProfile->user_id;
        $user_data = [];
        $sitename = \backend\classes\MonitorReport::getSiteName($sitecode);
        $worklist = " SELECT * FROM worklist WHERE sitecode=:sitecode ";
        $result = \Yii::$app->db->createCommand($worklist, [':sitecode'=>$sitecode])->queryAll();
        $dataList= [];
        foreach ($result as $key=> $val){
            $user_data = QueryMonitor::getUserData($val['create_user']);
            $dataList[$key]['id'] = $val['id'];
            $dataList[$key]['title'] = $val['title'];
            $dataList[$key]['user_name'] = $user_data['firstname'].' '.$user_data['lastname'];
            $dataList[$key]['create_date'] = $val['create_date'];
            
        }
        
        $dataProvider = new \yii\data\ArrayDataProvider([
            'allModels' => $dataList,
            'pagination' => [
                'pageSize' => 100,
            ],
        ]);
        
        
        return $this->renderAjax('worklist-all', ['provider' => $dataProvider, 'sitename'=>$sitename,'user_data'=>$user_data]);
    }
    
    public function actionPatientAll(){
        $sitecode = \Yii::$app->user->identity->userProfile->sitecode;
        $user_id = \Yii::$app->user->identity->userProfile->user_id;
        $worklist_id = \Yii::$app->request->post('worklist_id');
        
        $count = Yii::$app->db->createCommand('
            SELECT count(DISTINCT ptid)  FROM tb_data_1 WHERE sitecode=:sitecode
        ', [':sitecode' => $sitecode])->queryScalar();

          $patient = " SELECT 
              DISTINCT ptid, ptcodefull, title, name, surname, age, v3 as gender ,icf_upload1, icf_upload2 
              ,(SELECT count(DISTINCT tb_data_2.ptid) FROM tb_data_2  WHERE tb_data_2.ptid=tb_data_1.ptid AND rstat < 3 ) as cca01
              ,(SELECT count(DISTINCT wmp.ptid) FROM worklist_map_patient wmp WHERE wmp.ptid=tb_data_1.ptid AND rstat < 3 AND wmp.worklist_id='$worklist_id') as checked
              FROM tb_data_1 WHERE hsitecode=:sitecode ORDER BY ptid ";

        $provider = new SqlDataProvider([
            'sql' => $patient,
            'params' => [':sitecode' => $sitecode],
            'totalCount' => $count,
            'pagination' => [
                'pageSize' => 100,
            ],
            'sort' => [
                'attributes' => [
                    'ptcodefull',
                    'ptid',
                ],
            ],
        ]);
        
        $sitename = \backend\classes\MonitorReport::getSiteName($sitecode);
        
        return $this->renderAjax('patient-all', ['provider' => $provider, 'sitename'=>$sitename,'worklist_id'=>$worklist_id]);
    }

    public function actionAddPatient() {
        $worklist_id = \Yii::$app->request->post('worklist_id');
        $ptid = \Yii::$app->request->post('ptid');
        $user_id = \Yii::$app->user->identity->userProfile->user_id;
        $sitecode = \Yii::$app->user->identity->userProfile->sitecode;
        if ($worklist_id == '') {
            $sqlWID = " SELECT max(id) as worklist_id FROM worklist WHERE sitecode=:sitecode ";
            $resWID = \Yii::$app->db->createCommand($sqlWID, [':sitecode' => $sitecode])->queryOne();
            $worklist_id = $resWID['worklist_id'];
        }
        $result = WorklistQuery::AddPatientById($user_id, $worklist_id, $ptid, true);

        return $result;
    }

    public function actionAddUsersite() {
        $worklist_id = \Yii::$app->request->post('worklist_id');
        $userid = \Yii::$app->request->post('userid');
        $user_id = \Yii::$app->user->identity->userProfile->user_id;
        $sitecode = \Yii::$app->user->identity->userProfile->sitecode;
        if ($worklist_id == '') {
            $sqlWID = " SELECT max(id) as worklist_id FROM worklist WHERE sitecode=:sitecode ";
            $resWID = \Yii::$app->db->createCommand($sqlWID, [':sitecode' => $sitecode])->queryOne();
            $worklist_id = $resWID['worklist_id'];
        }
        //\appxq\sdii\utils\VarDumper::dump($userid);
        $result = WorklistQuery::AddUserById($user_id, $worklist_id, $userid, 0, true);

        return $result;
    }
    
    public function actionUpdateUsersite() {
        $worklist_id = \Yii::$app->request->post('worklist_id');
        $userid = \Yii::$app->request->post('userid');
        $permiss = \Yii::$app->request->post('permiss');
        $user_id = \Yii::$app->user->identity->userProfile->user_id;
        $sitecode = \Yii::$app->user->identity->userProfile->sitecode;
        if ($worklist_id == '') {
            $sqlWID = " SELECT max(id) as worklist_id FROM worklist WHERE sitecode=:sitecode ";
            $resWID = \Yii::$app->db->createCommand($sqlWID, [':sitecode' => $sitecode])->queryOne();
            $worklist_id = $resWID['worklist_id'];
        }
        //\appxq\sdii\utils\VarDumper::dump($userid);
        $result = WorklistQuery::UpdateUserInWorklist($user_id, $worklist_id, $userid, ['permission'=>$permiss,'room_name'=>'','doctor_code'=>''], true,true);

        return $result;
    }

    public function actionRemovePatient() {
        $worklist_id = \Yii::$app->request->post('worklist_id');
        $ptid = \Yii::$app->request->post('ptid');
        $user_id = \Yii::$app->user->identity->userProfile->user_id;
        $sitecode = \Yii::$app->user->identity->userProfile->sitecode;
        if ($worklist_id == '') {
            $sqlWID = " SELECT max(id) as worklist_id FROM worklist WHERE sitecode=:sitecode ";
            $resWID = \Yii::$app->db->createCommand($sqlWID, [':sitecode' => $sitecode])->queryOne();
            $worklist_id = $resWID['worklist_id'];
        }

        $result = WorklistQuery::RemovePatientByPtid($user_id, $worklist_id, $ptid, true);
        return $result;
    }

    public function actionRemoveUser() {
        $worklist_id = \Yii::$app->request->post('worklist_id');
        $userid = \Yii::$app->request->post('user_id');
        $user_id = \Yii::$app->user->identity->userProfile->user_id;
        $sitecode = \Yii::$app->user->identity->userProfile->sitecode;
        if ($worklist_id == '') {
            $sqlWID = " SELECT max(id) as worklist_id FROM worklist WHERE sitecode=:sitecode ";
            $resWID = \Yii::$app->db->createCommand($sqlWID, [':sitecode' => $sitecode])->queryOne();
            $worklist_id = $resWID['worklist_id'];
        }

        $result = WorklistQuery::RemoveUserInWorklist($user_id, $worklist_id, $userid, true);
        return $result;
    }

}
