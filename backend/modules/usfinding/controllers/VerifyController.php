<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace backend\modules\usfinding\controllers;

use yii\helpers\VarDumper;
use yii\web\Controller;
use Yii;
use backend\modules\usfinding\classes\QueryCID;
use backend\modules\usfinding\classes\QueryCCA02;
use backend\modules\usfinding\classes\QueryUS;
use backend\modules\usfinding\classes\PIDManagement;

/**
 * THE CONTROLLER ACTION
 */
class VerifyController extends Controller
{
    public function actionIndex()
    {
        
        $page=1;
        return $this->render('index', [
            'page' => $page,
        ]);
    }
    
    public function actionCid()
    {
        
        $page=1;
        return $this->render('cid', [
            'page' => $page,
        ]);
    }
    
    public function actionUs(){
        $request = Yii::$app->request;
        
        if( strlen($request->get('dstart'))==0 ){
            $start = date('Y-m-d');
        }else{
            $start = $request->get('dstart');
        }
        if( strlen($request->get('dend'))==0 ){
            $end = date('Y-m-d');
        }else{
            $end = $request->get('dend');
        }
        
        
        # data
        $data = QueryUS::getDataList('create_date', $start, $end);
        //echo count($data);
        return $this->render('us',[
            'data' => $data,
            'start' => $start,
            'end' => $end,
        ]);
    }
    
    public function actionUsdupdate(){
        $request = Yii::$app->request;
        
        if( strlen($request->get('dstart'))==0 ){
            $start = date('Y-m-d');
        }else{
            $start = $request->get('dstart');
        }
        if( strlen($request->get('dend'))==0 ){
            $end = date('Y-m-d');
        }else{
            $end = $request->get('dend');
        }
        
        
        # data
        $data = QueryUS::getDataList('update_date', $start, $end);
        //echo count($data);
        return $this->render('us',[
            'data' => $data,
            'start' => $start,
            'end' => $end,
        ]);
    }
    
    public function actionData(){
        $request = Yii::$app->request;
        
        # วันที่
        if( strlen($request->get('dstart'))==0 ){
            $start = date('Y-m-d');
        }else{
            $start = $request->get('dstart');
        }
        if( strlen($request->get('dend'))==0 ){
            $end = date('Y-m-d');
        }else{
            $end = $request->get('dend');
        }
        # ICF
        if( strlen($request->get('icf'))==0 ){
            $icf = 'cca01';
        }else{
            $icf = $request->get('icf');
        }
        # Data
        if( $request->get('icf')=='cca01' ){
            $data = "";
        }
        # Render
        return $this->render('data',[
            'data' => $data,
            'start' => $start,
            'end' => $end,
        ]);
    }

    public function actionGetDataSearchCid($cid='')
    {
        $page=1;
        #$request = Yii::$app->request;
        $hlist=QueryCID::historyUpdateCID($cid);
        $dlist=QueryCID::getListPatienByCID($cid);
        return $this->renderAjax('_listdatasearchcidresult', [
            'page' => $page,
            'dlist' => $dlist,
            'hlist' => $hlist,
        ]);
    }
    
    public function actionUpdateCid($newcid='',$oldcid=''){
        # กระบวนการคือ
        #   ตรวจสอบสิทธิ์ ว่าอยู่ในกลุ่ม Super admin หรือ Admin ส่วนกลาง หรือไม่
        if( Yii::$app->user->can('administrator')==TRUE ){
            
            // check ข้อมูลเก่า
            $checkExist = QueryCID::checkNewCIDExist($newcid);
            if( $checkExist==true ){
                QueryCID::updateCIDByPTID($newcid,$oldcid);
                // แก้ไขได้ ปกติ
                $msgerr="<font style='color: blue;' >&nbsp; เปลี่ยนสำเร็จแล้ว</font>";
                $dlist=QueryCID::getListPatienByCID($newcid);
                return $this->renderAjax('_listdatasearchcidresult', [
                    'dlist' => $dlist,
                    'msgerr' => $msgerr,
                ]);
            }else{
                // เป็นข้อมูลซ้ำ
                $msgerr="<font style='color: red;' >&nbsp; ไม่สามารถแก้ไขได้</font>";
                $dlist=QueryCID::getListPatienByCID($newcid);
                return $this->renderAjax('_listdatasearchcidresult', [
                    'dlist' => $dlist,
                    'msgerr' => $msgerr,
                ]);
            }
            
//            QueryCID::updateCIDByPTID($newcid,$oldcid);
//            // after update get new value from update
//            $dlist=QueryCID::getListPatienByCID($newcid);
//            return $this->renderAjax('_listdatasearchcidresult', [
//                'dlist' => $dlist,
//            ]);
        }
    }
    
    
    
    # สำหรับการทำงานของส่วน การลง CCA-02 ซ้ำ
    public function actionCca02Dup()
    {
        $duplist= QueryCCA02::getListDuplicate60();
        //$page=1;
        return $this->render('cca02dup', [
            'duplist' => $duplist,
        ]);
    }
    
    public function actionCca02Visit()
    {
        //$duplist= QueryCCA02::getListDuplicate60();
        
        $request = Yii::$app->request;
        $duplist= QueryCCA02::getListVisit($request->get('hsitecode'), $request->get('hptcode'), $request->get('f2v1'));
        $page=1;
        return $this->renderAjax('_cca02visit', [
            'duplist' => $duplist,
            'page' => $page,
        ]);
    }
    
    public function actionHospitalShow() {
        $request = Yii::$app->request;
        $hospital = $request->get('hsitecode');
        return $this->renderAjax('_usshow', [
            'hospital' => $hospital,
        ]);
    }


    public function actionPidManagement(){
        $hsitecode = Yii::$app->user->identity->userProfile->sitecode;
        $data = PIDManagement::getListPatient($hsitecode);
        return $this->render('pid-management',[
            'hsitecode' => $hsitecode,
            'data' => $data,
        ]);
    }
    
    public function actionPidManagementEdit(){
        return $this->renderAjax('_ptidsettonewvalue');
    }
    
    public function actionPidManagementEditFromChange(){
        
        //return "เปลี่ยนค่าสำเร็จ";
//        $data = ['currentvalue'=>'cuxxxxx','btnEdit'=>'btn'];
//        $data['currentvalue'] = 'cuxxxxx';
//        $data['btnEdit'] = 'btn';
//        $datareturn[0] = $data;
//        return json_encode($data);
        $request = Yii::$app->request;
        $arg['id'] = $request->get('dataid');
        $arg['datatable'] = $request->get('datatable');
        $arg['datavarname'] = $request->get('datavarname');
        $arg['dataoldvalue'] = $request->get('dataoldvalue');
        $arg['datanewvalue'] = $request->get('datanewvalue');
        $state = PIDManagement::setNewValueForChange($arg);
        if($state==TRUE){
            $data = ''.$request->get('datanewvalue').'||'.'เปลี่ยนค่าสำเร็จ'.'||1';
        }else{
            $data = ''.$request->get('dataoldvalue').'||'.'แก้ไขไม่สำเร็จ'.'||0';
        }
        echo $data;
    }
    
    // doctor
    public function actionDoctor(){
        $request = Yii::$app->request;
        $hsitecode = Yii::$app->user->identity->userProfile->sitecode;
        $data[] = '';
        return $this->render('doctor',[
            'hsitecode' => $hsitecode,
            'data' => $data,
        ]);
    }

}

