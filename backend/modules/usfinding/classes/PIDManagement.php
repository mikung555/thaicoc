<?php


namespace backend\modules\usfinding\classes;


use Yii;
use backend\modules\usfinding\classes\QueryRegister;
use backend\modules\usfinding\classes\QueryCCA01;
use backend\modules\usfinding\classes\QueryCCA02;
use backend\modules\usfinding\classes\QueryCCA02p1;
use backend\modules\usfinding\classes\QueryCCA03;
use backend\modules\usfinding\classes\QueryCCA04;
use backend\modules\usfinding\classes\QueryCCA05;

class PIDManagement {
    
    public static function getListPatient($hsitecode = null)
    {
        if(strlen(trim($hsitecode))>0 ){
            $sql = "select distinct cid ";
            $sql.= "from tb_data_1 reg ";
            $sql.= "where hsitecode=:hsitecode ";
            $sql.= "and rstat<>3 ";
            $sql.= "order by hptcode asc ";
            //echo $sql;
            $dataProvider = Yii::$app->db->createCommand($sql,[":hsitecode"=>$hsitecode])->queryAll();
            return $dataProvider;
        }
    }
    
    public static function getPTIDDetByCID($cid = null){
        if(strlen(trim($cid))>0 ){
            $sql = "select distinct cid,ptid ";
            $sql.= "from tb_data_1 reg ";
            $sql.= "where cid=:cid ";
            $sql.= "and rstat<>3 ";
            $sql.= "order by hsitecode, hptcode asc ";
            //echo $sql;
            $dataProvider = Yii::$app->db->createCommand($sql,[":cid"=>$cid])->queryAll();
            return $dataProvider;
        }
    }
    public static function getPatientDetByPTID($ptid = null){
        if(strlen(trim($ptid))>0 ){
            $sql = "select * ";
            $sql.= "from tb_data_1 reg ";
            $sql.= "where ptid=:ptid ";
            $sql.= "and rstat<>3 ";
            $sql.= "order by substring(1,10,ptid) asc ";
            //echo $sql;
            $dataProvider = Yii::$app->db->createCommand($sql,[":ptid"=>$ptid])->queryAll();
            return $dataProvider;
        }
    }
    
    
    public static function getPatientDetByCID($cid = null){
        if(strlen(trim($cid))>0 ){
            $sql = "select *,FROM_UNIXTIME(substr(id,1,10)) as dateofaddrecord ";
            $sql.= "from tb_data_1 reg ";
            $sql.= "where cid=:cid ";
            $sql.= "and rstat<>3 ";
            $sql.= "order by substring(1,10,id) asc ";
            //echo $sql;
            $dataProvider = Yii::$app->db->createCommand($sql,[":cid"=>$cid])->queryAll();
            return $dataProvider;
        }
    }
    
    public static function countCCA01ByPTID($ptid = null){
        if(strlen(trim($ptid))>0 ){
            $sql = "select count(*) as recs ";
            $sql.= "from tb_data_2 ";
            $sql.= "where ptid=:ptid ";
            $sql.= "and rstat<>3 ";
            
            $dataProvider = Yii::$app->db->createCommand($sql,[":ptid"=>$ptid])->queryOne();
            $count = $dataProvider['recs'];
            return $count;
        }
    }
    
    public static function countCCA02ByPTID($ptid = null){
        if(strlen(trim($ptid))>0 ){
            $sql = "select count(*) as recs ";
            $sql.= "from tb_data_3 ";
            $sql.= "where ptid=:ptid ";
            $sql.= "and rstat<>3 ";
            
            $dataProvider = Yii::$app->db->createCommand($sql,[":ptid"=>$ptid])->queryOne();
            $count = $dataProvider['recs'];
            return $count;
        }
    }


    public static function getRegisterDetail($arg, $org){
        $_det['title'] = 'ข้อมูล Register ใน Record ที่เลือก ['.$arg['id'].'] ';
        if( strlen($arg['ptidzero'])>0 ){
            // load detail init
        }
        $data = QueryRegister::getDetByID($arg['id']);
        $_det['sys']['ezf_id'] = '1437377239070461301';
        $_det['sys']['datatable'] = 'tb_data_1';
        $_det['sys']['data_id'] = $data['id'];
        $_det['sys']['target_id'] = $data['ptid'];
        
        $_det['view']['id']['var'] = 'id';
        $_det['view']['id']['mastervalue'] = $org['id'];
        $_det['view']['id']['currentvalue'] = $data['id'];
        $_det['view']['id']['canchange'] = FALSE;
        $_det['view']['ptid']['var'] = 'ptid';
        $_det['view']['ptid']['mastervalue'] = $org['ptid'];
        $_det['view']['ptid']['currentvalue'] = $data['ptid'];
        $_det['view']['ptid']['canchange'] = TRUE;
        $_det['view']['sitecode']['var'] = 'sitecode';
        $_det['view']['sitecode']['mastervalue'] = $org['sitecode'];
        $_det['view']['sitecode']['currentvalue'] = $data['sitecode'];
        $_det['view']['sitecode']['canchange'] = TRUE;
        $_det['view']['ptcode']['var'] = 'ptcode';
        $_det['view']['ptcode']['mastervalue'] = $org['ptcode'];
        $_det['view']['ptcode']['currentvalue'] = $data['ptcode'];
        $_det['view']['ptcode']['canchange'] = TRUE;
        $_det['view']['ptcodefull']['var'] = 'ptcodefull';
        $_det['view']['ptcodefull']['mastervalue'] = $org['ptcodefull'];
        $_det['view']['ptcodefull']['currentvalue'] = $data['ptcodefull'];
        $_det['view']['ptcodefull']['canchange'] = TRUE;
        $_det['view']['hsitecode']['var'] = 'hsitecode';
        $_det['view']['hsitecode']['mastervalue'] = $org['hsitecode'];
        $_det['view']['hsitecode']['currentvalue'] = $data['hsitecode'];
        $_det['view']['hsitecode']['canchange'] = FALSE;
        $_det['view']['hptcode']['var'] = 'hptcode';
        $_det['view']['hptcode']['mastervalue'] = $org['hptcode'];
        $_det['view']['hptcode']['currentvalue'] = $data['hptcode'];
        $_det['view']['hptcode']['canchange'] = FALSE;
        
        return $_det;
    }
    
    public static function getCCA01Detail($arg, $org){
        $_det['title'] = 'ข้อมูล CCA01 ใน Record ที่เลือก ['.$arg['id'].'] ';
        if( strlen($arg['ptidzero'])>0 ){
            // load detail init
        }
        $data = QueryCCA01::getDetByID($arg['id']);
        $_det['sys']['ezf_id'] = '1437377239070461302';
        $_det['sys']['datatable'] = 'tb_data_2';
        $_det['sys']['data_id'] = $data['id'];
        $_det['sys']['target_id'] = $data['ptid'];
        
        $_det['view']['id']['var'] = 'id';
        $_det['view']['id']['mastervalue'] = $org['id'];
        $_det['view']['id']['currentvalue'] = $data['id'];
        $_det['view']['id']['canchange'] = FALSE;
        $_det['view']['ptid']['var'] = 'ptid';
        $_det['view']['ptid']['mastervalue'] = $org['ptid'];
        $_det['view']['ptid']['currentvalue'] = $data['ptid'];
        $_det['view']['ptid']['canchange'] = TRUE;
        $_det['view']['sitecode']['var'] = 'sitecode';
        $_det['view']['sitecode']['mastervalue'] = $org['sitecode'];
        $_det['view']['sitecode']['currentvalue'] = $data['sitecode'];
        $_det['view']['sitecode']['canchange'] = TRUE;
        $_det['view']['ptcode']['var'] = 'ptcode';
        $_det['view']['ptcode']['mastervalue'] = $org['ptcode'];
        $_det['view']['ptcode']['currentvalue'] = $data['ptcode'];
        $_det['view']['ptcode']['canchange'] = TRUE;
        $_det['view']['ptcodefull']['var'] = 'ptcodefull';
        $_det['view']['ptcodefull']['mastervalue'] = $org['ptcodefull'];
        $_det['view']['ptcodefull']['currentvalue'] = $data['ptcodefull'];
        $_det['view']['ptcodefull']['canchange'] = TRUE;
        $_det['view']['hsitecode']['var'] = 'hsitecode';
        $_det['view']['hsitecode']['mastervalue'] = $org['hsitecode'];
        $_det['view']['hsitecode']['currentvalue'] = $data['hsitecode'];
        $_det['view']['hsitecode']['canchange'] = FALSE;
        $_det['view']['hptcode']['var'] = 'hptcode';
        $_det['view']['hptcode']['mastervalue'] = $org['hptcode'];
        $_det['view']['hptcode']['currentvalue'] = $data['hptcode'];
        $_det['view']['hptcode']['canchange'] = FALSE;
        
        return $_det;
    }
    
    public static function getCCA02Detail($arg, $org){
        $_det['title'] = 'ข้อมูล CCA02 ใน Record ที่เลือก ['.$arg['id'].'] ';
        if( strlen($arg['ptidzero'])>0 ){
            // load detail init
        }
        $data = QueryCCA02::getDetByID($arg['id']);
        $_det['sys']['ezf_id'] = '1437619524091524800';
        $_det['sys']['datatable'] = 'tb_data_3';
        $_det['sys']['data_id'] = $data['id'];
        $_det['sys']['target_id'] = $data['ptid'];
        
        $_det['view']['id']['var'] = 'id';
        $_det['view']['id']['mastervalue'] = $org['id'];
        $_det['view']['id']['currentvalue'] = $data['id'];
        $_det['view']['id']['canchange'] = FALSE;
        $_det['view']['ptid']['var'] = 'ptid';
        $_det['view']['ptid']['mastervalue'] = $org['ptid'];
        $_det['view']['ptid']['currentvalue'] = $data['ptid'];
        $_det['view']['ptid']['canchange'] = TRUE;
        $_det['view']['sitecode']['var'] = 'sitecode';
        $_det['view']['sitecode']['mastervalue'] = $org['sitecode'];
        $_det['view']['sitecode']['currentvalue'] = $data['sitecode'];
        $_det['view']['sitecode']['canchange'] = TRUE;
        $_det['view']['ptcode']['var'] = 'ptcode';
        $_det['view']['ptcode']['mastervalue'] = $org['ptcode'];
        $_det['view']['ptcode']['currentvalue'] = $data['ptcode'];
        $_det['view']['ptcode']['canchange'] = TRUE;
        $_det['view']['ptcodefull']['var'] = 'ptcodefull';
        $_det['view']['ptcodefull']['mastervalue'] = $org['ptcodefull'];
        $_det['view']['ptcodefull']['currentvalue'] = $data['ptcodefull'];
        $_det['view']['ptcodefull']['canchange'] = TRUE;
        $_det['view']['hsitecode']['var'] = 'hsitecode';
        $_det['view']['hsitecode']['mastervalue'] = $org['hsitecode'];
        $_det['view']['hsitecode']['currentvalue'] = $data['hsitecode'];
        $_det['view']['hsitecode']['canchange'] = FALSE;
        $_det['view']['hptcode']['var'] = 'hptcode';
        $_det['view']['hptcode']['mastervalue'] = $org['hptcode'];
        $_det['view']['hptcode']['currentvalue'] = $data['hptcode'];
        $_det['view']['hptcode']['canchange'] = FALSE;
        
        return $_det;
    }
    
    public static function getCCA02p1Detail($arg, $org){
        $_det['title'] = 'ข้อมูล CCA02.1 ใน Record ที่เลือก ['.$arg['id'].'] ';
        if( strlen($arg['ptidzero'])>0 ){
            // load detail init
        }
        $data = QueryCCA02p1::getDetByID($arg['id']);
        $_det['sys']['ezf_id'] = '1454041742064651700';
        $_det['sys']['datatable'] = 'tb_data_4';
        $_det['sys']['data_id'] = $data['id'];
        $_det['sys']['target_id'] = $data['ptid'];
        
        $_det['view']['id']['var'] = 'id';
        $_det['view']['id']['mastervalue'] = $org['id'];
        $_det['view']['id']['currentvalue'] = $data['id'];
        $_det['view']['id']['canchange'] = FALSE;
        $_det['view']['ptid']['var'] = 'ptid';
        $_det['view']['ptid']['mastervalue'] = $org['ptid'];
        $_det['view']['ptid']['currentvalue'] = $data['ptid'];
        $_det['view']['ptid']['canchange'] = TRUE;
        $_det['view']['sitecode']['var'] = 'sitecode';
        $_det['view']['sitecode']['mastervalue'] = $org['sitecode'];
        $_det['view']['sitecode']['currentvalue'] = $data['sitecode'];
        $_det['view']['sitecode']['canchange'] = TRUE;
        $_det['view']['ptcode']['var'] = 'ptcode';
        $_det['view']['ptcode']['mastervalue'] = $org['ptcode'];
        $_det['view']['ptcode']['currentvalue'] = $data['ptcode'];
        $_det['view']['ptcode']['canchange'] = TRUE;
        $_det['view']['ptcodefull']['var'] = 'ptcodefull';
        $_det['view']['ptcodefull']['mastervalue'] = $org['ptcodefull'];
        $_det['view']['ptcodefull']['currentvalue'] = $data['ptcodefull'];
        $_det['view']['ptcodefull']['canchange'] = TRUE;
        $_det['view']['hsitecode']['var'] = 'hsitecode';
        $_det['view']['hsitecode']['mastervalue'] = $org['hsitecode'];
        $_det['view']['hsitecode']['currentvalue'] = $data['hsitecode'];
        $_det['view']['hsitecode']['canchange'] = FALSE;
        $_det['view']['hptcode']['var'] = 'hptcode';
        $_det['view']['hptcode']['mastervalue'] = $org['hptcode'];
        $_det['view']['hptcode']['currentvalue'] = $data['hptcode'];
        $_det['view']['hptcode']['canchange'] = FALSE;
        
        return $_det;
    }
    
    public static function getCCA03Detail($arg, $org){
        $_det['title'] = 'ข้อมูล CCA03 ใน Record ที่เลือก ['.$arg['id'].'] ';
        if( strlen($arg['ptidzero'])>0 ){
            // load detail init
        }
        $data = QueryCCA03::getDetByID($arg['id']);
        $_det['sys']['ezf_id'] = '1451381257025574200';
        $_det['sys']['datatable'] = 'tb_data_7';
        $_det['sys']['data_id'] = $data['id'];
        $_det['sys']['target_id'] = $data['ptid'];
        
        $_det['view']['id']['var'] = 'id';
        $_det['view']['id']['mastervalue'] = $org['id'];
        $_det['view']['id']['currentvalue'] = $data['id'];
        $_det['view']['id']['canchange'] = FALSE;
        $_det['view']['ptid']['var'] = 'ptid';
        $_det['view']['ptid']['mastervalue'] = $org['ptid'];
        $_det['view']['ptid']['currentvalue'] = $data['ptid'];
        $_det['view']['ptid']['canchange'] = TRUE;
        $_det['view']['sitecode']['var'] = 'sitecode';
        $_det['view']['sitecode']['mastervalue'] = $org['sitecode'];
        $_det['view']['sitecode']['currentvalue'] = $data['sitecode'];
        $_det['view']['sitecode']['canchange'] = TRUE;
        $_det['view']['ptcode']['var'] = 'ptcode';
        $_det['view']['ptcode']['mastervalue'] = $org['ptcode'];
        $_det['view']['ptcode']['currentvalue'] = $data['ptcode'];
        $_det['view']['ptcode']['canchange'] = TRUE;
        $_det['view']['ptcodefull']['var'] = 'ptcodefull';
        $_det['view']['ptcodefull']['mastervalue'] = $org['ptcodefull'];
        $_det['view']['ptcodefull']['currentvalue'] = $data['ptcodefull'];
        $_det['view']['ptcodefull']['canchange'] = TRUE;
        $_det['view']['hsitecode']['var'] = 'hsitecode';
        $_det['view']['hsitecode']['mastervalue'] = $org['hsitecode'];
        $_det['view']['hsitecode']['currentvalue'] = $data['hsitecode'];
        $_det['view']['hsitecode']['canchange'] = FALSE;
        $_det['view']['hptcode']['var'] = 'hptcode';
        $_det['view']['hptcode']['mastervalue'] = $org['hptcode'];
        $_det['view']['hptcode']['currentvalue'] = $data['hptcode'];
        $_det['view']['hptcode']['canchange'] = FALSE;
        
        return $_det;
    }
    
    public static function getCCA04Detail($arg, $org){
        $_det['title'] = 'ข้อมูล CCA04 ใน Record ที่เลือก ['.$arg['id'].'] ';
        if( strlen($arg['ptidzero'])>0 ){
            // load detail init
        }
        $data = QueryCCA04::getDetByID($arg['id']);
        $_det['sys']['ezf_id'] = '1452061550097822200';
        $_det['sys']['datatable'] = 'tb_data_8';
        $_det['sys']['data_id'] = $data['id'];
        $_det['sys']['target_id'] = $data['ptid'];
        
        $_det['view']['id']['var'] = 'id';
        $_det['view']['id']['mastervalue'] = $org['id'];
        $_det['view']['id']['currentvalue'] = $data['id'];
        $_det['view']['id']['canchange'] = FALSE;
        $_det['view']['ptid']['var'] = 'ptid';
        $_det['view']['ptid']['mastervalue'] = $org['ptid'];
        $_det['view']['ptid']['currentvalue'] = $data['ptid'];
        $_det['view']['ptid']['canchange'] = TRUE;
        $_det['view']['sitecode']['var'] = 'sitecode';
        $_det['view']['sitecode']['mastervalue'] = $org['sitecode'];
        $_det['view']['sitecode']['currentvalue'] = $data['sitecode'];
        $_det['view']['sitecode']['canchange'] = TRUE;
        $_det['view']['ptcode']['var'] = 'ptcode';
        $_det['view']['ptcode']['mastervalue'] = $org['ptcode'];
        $_det['view']['ptcode']['currentvalue'] = $data['ptcode'];
        $_det['view']['ptcode']['canchange'] = TRUE;
        $_det['view']['ptcodefull']['var'] = 'ptcodefull';
        $_det['view']['ptcodefull']['mastervalue'] = $org['ptcodefull'];
        $_det['view']['ptcodefull']['currentvalue'] = $data['ptcodefull'];
        $_det['view']['ptcodefull']['canchange'] = TRUE;
        $_det['view']['hsitecode']['var'] = 'hsitecode';
        $_det['view']['hsitecode']['mastervalue'] = $org['hsitecode'];
        $_det['view']['hsitecode']['currentvalue'] = $data['hsitecode'];
        $_det['view']['hsitecode']['canchange'] = FALSE;
        $_det['view']['hptcode']['var'] = 'hptcode';
        $_det['view']['hptcode']['mastervalue'] = $org['hptcode'];
        $_det['view']['hptcode']['currentvalue'] = $data['hptcode'];
        $_det['view']['hptcode']['canchange'] = FALSE;
        
        return $_det;
    }
    
    public static function getCCA05Detail($arg, $org){
        $_det['title'] = 'ข้อมูล CCA05 ใน Record ที่เลือก ['.$arg['id'].'] ';
        if( strlen($arg['ptidzero'])>0 ){
            // load detail init
        }
        $data = QueryCCA05::getDetByID($arg['id']);
        $_det['sys']['ezf_id'] = '1440515302053265900';
        $_det['sys']['datatable'] = 'tb_data_11';
        $_det['sys']['data_id'] = $data['id'];
        $_det['sys']['target_id'] = $data['ptid'];
        
        $_det['view']['id']['var'] = 'id';
        $_det['view']['id']['mastervalue'] = $org['id'];
        $_det['view']['id']['currentvalue'] = $data['id'];
        $_det['view']['id']['canchange'] = FALSE;
        $_det['view']['ptid']['var'] = 'ptid';
        $_det['view']['ptid']['mastervalue'] = $org['ptid'];
        $_det['view']['ptid']['currentvalue'] = $data['ptid'];
        $_det['view']['ptid']['canchange'] = TRUE;
        $_det['view']['sitecode']['var'] = 'sitecode';
        $_det['view']['sitecode']['mastervalue'] = $org['sitecode'];
        $_det['view']['sitecode']['currentvalue'] = $data['sitecode'];
        $_det['view']['sitecode']['canchange'] = TRUE;
        $_det['view']['ptcode']['var'] = 'ptcode';
        $_det['view']['ptcode']['mastervalue'] = $org['ptcode'];
        $_det['view']['ptcode']['currentvalue'] = $data['ptcode'];
        $_det['view']['ptcode']['canchange'] = TRUE;
        $_det['view']['ptcodefull']['var'] = 'ptcodefull';
        $_det['view']['ptcodefull']['mastervalue'] = $org['ptcodefull'];
        $_det['view']['ptcodefull']['currentvalue'] = $data['ptcodefull'];
        $_det['view']['ptcodefull']['canchange'] = TRUE;
        $_det['view']['hsitecode']['var'] = 'hsitecode';
        $_det['view']['hsitecode']['mastervalue'] = $org['hsitecode'];
        $_det['view']['hsitecode']['currentvalue'] = $data['hsitecode'];
        $_det['view']['hsitecode']['canchange'] = FALSE;
        $_det['view']['hptcode']['var'] = 'hptcode';
        $_det['view']['hptcode']['mastervalue'] = $org['hptcode'];
        $_det['view']['hptcode']['currentvalue'] = $data['hptcode'];
        $_det['view']['hptcode']['canchange'] = FALSE;
        
        return $_det;
    }
    
    
    # action submit
    public static function setNewValueForChange($arg){
        $state = FALSE;
        # User ที่สามารถแก้ไขข้อมูลได้
        $_usercanedit[] = '1435745159010043375';
        $user_id = Yii::$app->user->identity->userProfile->user_id;
        if( in_array($user_id, $_usercanedit) ){
            # แก้ไข เพียงตารางข้อมูล
            $_varcanchange[] = 'sitecode';
            $_varcanchange[] = 'ptcode';
            $_varcanchange[] = 'ptcodefull';
            # log
            $sql = 'insert into `cascap_log`.`tb_data_log_change` ';
            $sql.= 'set `dadd`=NOW() ';
            $sql.= ', `datatable`=:datatable ';
            $sql.= ', `datavarname`=:datavarname ';
            $sql.= ', `dataid`=:dataid ';
            $sql.= ', `value_new`=:value_new ';
            $sql.= ', `value_old`=:value_old ';
            $sql.= ', `user_change`=:user_change ';
            #
            #
            Yii::$app->db->createCommand($sql,[
                ":datatable"=>$arg['datatable'],
                ":datavarname"=>$arg['datavarname'],
                ":dataid"=>$arg['id'],
                ":value_new"=>$arg['datanewvalue'],
                ":value_old"=>$arg['dataoldvalue'],
                ":user_change"=>Yii::$app->user->identity->userProfile->user_id,
                ])->execute();
            #
            # real update
            if(in_array($arg['datavarname'], $_varcanchange) && strlen($arg['datatable'])>0 ){
                # 
                $sql = 'update `'.addslashes($arg['datatable']).'` ';
                $sql.= 'set `'.addslashes($arg['datavarname']).'`=:valuechange ';
                $sql.= 'where id=:dataid ';
                Yii::$app->db->createCommand($sql,[
                    ":valuechange"=>$arg['datanewvalue'],
                    ":dataid"=>$arg['id'],
                        ])->execute();
                $state = TRUE;
            }
            #
            # system
            $_varsystem[] = 'ptid';
            if(in_array($arg['datavarname'], $_varsystem) && strlen($arg['datatable'])>0 ){
                
                # tb_data 
                $sql = 'update `'.addslashes($arg['datatable']).'` ';
                $sql.= 'set `'.addslashes($arg['datavarname']).'`=:valuechange ';
                $sql.= 'where id=:dataid ';
                Yii::$app->db->createCommand($sql,[
                    ":valuechange"=>$arg['datanewvalue'],
                    ":dataid"=>$arg['id'],
                        ])->execute();
                
                # ezfrom_target
                $sql = 'update `ezform_target` ';
                $sql.= 'set `target_id`=:valuechange ';
                $sql.= 'where `data_id`=:dataid ';
                Yii::$app->db->createCommand($sql,[
                    ":valuechange"=>$arg['datanewvalue'],
                    ":dataid"=>$arg['id'],
                        ])->execute(); 
                
                $state = TRUE;
            }
            #
            # system
            $_datadelete[] = 'rstat';
            if(in_array($arg['datavarname'], $_datadelete) && strlen($arg['datatable'])>0 ){
                
                # tb_data 
                $sql = 'update `'.addslashes($arg['datatable']).'` ';
                $sql.= 'set `'.addslashes($arg['datavarname']).'`=:valuechange ';
                $sql.= 'where id=:dataid ';
                Yii::$app->db->createCommand($sql,[
                    ":valuechange"=>$arg['datanewvalue'],
                    ":dataid"=>$arg['id'],
                        ])->execute();
                
                # ezfrom_target
                $sql = 'update `ezform_target` ';
                $sql.= 'set `rstat`=:valuechange ';
                $sql.= 'where `data_id`=:dataid ';
                Yii::$app->db->createCommand($sql,[
                    ":valuechange"=>$arg['datanewvalue'],
                    ":dataid"=>$arg['id'],
                        ])->execute(); 
                
                $state = TRUE;
            }
        }
        return $state;
    }
    
}
