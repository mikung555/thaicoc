<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace backend\modules\usfinding\classes;

/**
 * Description of QueryUS
 *
 * @author chaiwat
 */
use Yii;

class QueryUS {
    //put your code here
    
    public static function getDataList($dcheck = 'create_date', $start = null, $end = null)
    {
        if( strlen($start)==0 ){
            $start = date('Y-m-d');
        }
        if( strlen($start)==0 ){
            $end = date('Y-m-d');
        }
        if( TRUE ){
            $sql = "select id,ptid ";
            $sql.= ",create_date, update_date, rstat, f2v1 ";
            $sql.= ",sys_usimagepath ";
            $sql.= ",hsitecode,hptcode ";
            $sql.= ",case 1 when f2v6a3b1='1' and f2v6a3='1' then '<font color=red>Suspected CCA</font><br />' ";
            $sql.= "when f2v6a3='1' and (f2v6a3b1<>'1' or f2v6a3b1 is null) then '<font color=#f49542>ส่งรักษาต่อที่โรงพยาบาล</font><br />' ";
            $sql.= "when f2v2a1='1' and (f2v2a1b2 in (1,2,3)) then concat('<font color=blue>PDF ',if(f2v2a1b2='1','(PDF 1)',''),if(f2v2a1b2='2','(PDF 2)',''),if(f2v2a1b2='3','(PDF 3)',''),'</font><br />') "; //,if(f2v2a1b2='1','(PDF 1)',''),if(f2v2a1b2='2','(PDF 2)',if(f2v2a1b2='3','(PDF 3)',''),'')
            $sql.= "when f2v3a3='1' then 'Gallstone' ";
            $sql.= "when f2v3a2='1' then 'Wall' ";
            $sql.= "when f2v4a2='1' then 'Renal cyst' ";
            $sql.= "when f2v4a3='1' then 'Parenchymal change' ";
            $sql.= "when f2v4a4='1' then 'Renal stone' ";
            $sql.= "when f2v2a1='1' and (f2v2a1b1 in (1,2,3)) then 'Fatty liver' ";
            $sql.= "when f2v2a1='1' and (f2v2a1b3 ='1') then 'Cirrhosis' ";
            $sql.= "when f2v2a1='1' and (f2v2a1b4 ='1') then 'Parenchymal change' ";
            $sql.= "end as usresults ";
            $sql.= "from tb_data_3 cca02 ";
            $sql.= "left join all_hospital_thai hospital on hospital.hcode=cca02.hsitecode ";
            if( $dcheck=='update_date' ){
                $sql.= "where cca02.update_date between '".$start." 00:00:00' and '".$end." 23:59:59' ";
            }else{
                $sql.= "where cca02.create_date between '".$start." 00:00:00' and '".$end." 23:59:59' ";
            }
            $sql.= "and cca02.rstat in ('1','2','4') ";
            //$sql.= "order by cca02.create_date desc ";
            $sql.= "order by cca02.f2v1 desc ";
            $sql.= "limit 0,3000 ";     # ป้องกันข้อมูลเยอะเกิน  
            
            $dataProvider = Yii::$app->db->createCommand($sql)->queryAll();
            return $dataProvider;
        }
    }
}
