<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace backend\modules\usfinding\classes;

/**
 * Description of QueryCCA02
 *
 * @author chaiwat
 */
use Yii;

class QueryUrine {
    //put your code here
    public static function getSummray($times)
    {
        if( TRUE ){
            $sql = "select count(*) ";
            $sql.= ",count(distinct if(urine.urine_result in ('0','1'),urine.ptid,null)) as urine ";
            $sql.= ",count(distinct if(urine.urine_result='0',urine.ptid,null)) as urine_negative ";
            $sql.= ",count(distinct if(urine.urine_result='1',urine.ptid,null)) as urine_positive ";
            $sql.= ",count(distinct if(cca02.f2v2a1b2 in ('1','2','3') and urine.urine_result in ('0','1'),cca02.ptid,null)) as urinePDF ";
            $sql.= ",count(distinct if(cca02.f2v2a1b2 in ('1','2','3'),cca02.ptid,null)) as pdf ";
            $sql.= ",count(distinct if(cca02.f2v2a1b2 in ('1','2','3') and urine.urine_result='0',urine.ptid,null)) as urine_negative_pdf ";
            $sql.= ",count(distinct if(cca02.f2v2a1b2 in ('1','2','3') and urine.urine_result='1',urine.ptid,null)) as urine_positive_pdf ";
            $sql.= ",count(distinct if(cca02.f2v6a3='1' and cca02.f2v6a3b1='1',cca02.ptid,null)) as suspected ";
            $sql.= ",count(distinct if((cca02.f2v6a3='1' and cca02.f2v6a3b1='1') and urine.urine_result in ('0','1'),cca02.ptid,null)) as urineSuspected ";
            $sql.= ",count(distinct if((cca02.f2v6a3='1' and cca02.f2v6a3b1='1') and urine.urine_result='0',urine.ptid,null)) as urine_negative_Suspected ";
            $sql.= ",count(distinct if((cca02.f2v6a3='1' and cca02.f2v6a3b1='1') and urine.urine_result='1',urine.ptid,null)) as urine_positive_Suspected ";
            $sql.= "from tb_data_3 cca02 ";
            $sql.= "left join tbdata_1490689887006825700 urine on urine.ptid=cca02.ptid ";
            $sql.= "where cca02.usmobile=:times ";
            $sql.= "and cca02.rstat in ('1','2') ";
            $sql.= "and cca02.f2v1 is not null ";
            //echo $sql;
            $dataProvider = Yii::$app->db->createCommand($sql,[':times'=>$times])->queryAll();
            return $dataProvider;
        }
    }
    
    public static function getHospitalSummry($times)
    {
        if( TRUE ){
            $sql = "select count(*) as usall ";
            $sql.= ",urine.hsitecode ";
            $sql.= ",count(distinct if(urine.urine_result in ('0','1') ,urine.ptid,NULL)) as urine_checked ";
            $sql.= ",count(distinct if(urine.urine_result='0',urine.ptid,null)) as urine_negative ";
            $sql.= ",count(distinct if(urine.urine_result='1',urine.ptid,null)) as urine_positive ";
            $sql.= ",count(distinct if(cca02.f2v2a1b2 in ('1','2','3'),cca02.ptid,null)) as pdf ";
            $sql.= ",count(distinct if(cca02.f2v2a1b2 in ('1','2','3') and urine.urine_result='0',urine.ptid,null)) as urine_negative_pdf ";
            $sql.= ",count(distinct if(cca02.f2v2a1b2 in ('1','2','3') and urine.urine_result='1',urine.ptid,null)) as urine_positive_pdf ";
            $sql.= "from tb_data_3 cca02 ";
            $sql.= "left join tbdata_1490689887006825700 urine on urine.ptid=cca02.ptid ";
            $sql.= "where cca02.usmobile=:times ";
            $sql.= "and cca02.rstat in ('1','2') ";
            $sql.= "and cca02.f2v1 is not null ";
            $sql.= "group by urine.hsitecode ";
            //echo $sql;
            $dataProvider = Yii::$app->db->createCommand($sql,[':times'=>$times])->queryAll();
            return $dataProvider;
        }
    }
    
    
}
