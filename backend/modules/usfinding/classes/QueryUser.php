<?php


namespace backend\modules\usfinding\classes;

use Yii;
/**
 * Description of QueryUser
 *
 * @author chaiwat
 */
class QueryUser {
    public static function getDetByID($userid = null){
        if(strlen(trim($userid))>0 ){
            $sql = "select * ";
            $sql.= "from user_profile ";
            $sql.= "where user_id=:userid ";
            $dataProvider = Yii::$app->db->createCommand($sql,[":userid"=>$userid])->queryOne();
            return $dataProvider;
        }
    }
}
