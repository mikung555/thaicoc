<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace backend\modules\usfinding\classes;

use Yii;

/**
 * Description of QueryCCA03
 *
 * @author chaiwat
 */
class QueryCCA03 {
    //put your code here
    # function for PID Management
    public static function getDetByID($id = null){
        if(strlen($id)>0){
            $sql = "select *,FROM_UNIXTIME(substr(id,1,10)) as dateofaddrecord ";
            $sql.= "from tb_data_7 ";
            $sql.= "where id=:id ";
            $sql.= "and rstat<>3 ";
            //echo $sql;
            $dataProvider = Yii::$app->db->createCommand($sql,[":id"=>$id])->queryOne();
            return $dataProvider;
        }
    }
    
    public static function getIDOfByPTID($ptid = null){
        if(strlen(trim($ptid))>0 ){
            $sql = "select id,ptid,rstat ";
            $sql.= "from tb_data_7 ";
            $sql.= "where ptid=:ptid ";
            $sql.= "and rstat<>3 ";
            
            $dataProvider = Yii::$app->db->createCommand($sql,[":ptid"=>$ptid])->queryAll();
            return $dataProvider;
        }
    }
}
