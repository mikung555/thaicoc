<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace backend\modules\usfinding\classes;

/**
 * Description of QueryCCA02
 *
 * @author chaiwat
 */
use Yii;

class QueryHospital {
    //put your code here
    public static function getSummray($strHospitalList)
    {
        if( strlen($strHospitalList)>0 ){
            $sql = "select hcode,name ";
            $sql.= "from all_hospital_thai ";
            $sql.= "where (".$strHospitalList.") ";
            //echo $sql;
            $data = Yii::$app->db->createCommand($sql)->queryAll();
            if(count($data)>0){
                foreach ($data as $kvalue){
                    $dataProvider[$kvalue['hcode']]=$kvalue['name'];
                }
            }
            return $dataProvider;
        }
    }
    
    public static function getHospitalDet($hsitecode = null)
    {
        if( strlen($hsitecode)>0 ){
            $sql = "select * ";
            $sql.= "from all_hospital_thai ";
            $sql.= "where hcode=:hsitecode ";
            //echo $sql;
            $data = Yii::$app->db->createCommand($sql,['hsitecode'=>$hsitecode])->queryOne();
            
            return $data;
        }
    }
    
    
    
    
}
