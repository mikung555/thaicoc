<?php

namespace backend\modules\usfinding\classes;

use yii\data\SqlDataProvider;
use yii\data\ActiveDataProvider;
use Yii;

class QueryMonitor {

    public function UltrasoundRoomData($startDate = null, $endDate = null, $zone = null, $province = null, $amphur = null, $hospital = null, $worklistno = null) {
        $sDate = \DateTime::createFromFormat("d/m/Y", $startDate); // ::createFromFormat("d/m/Y  H:i:s", '31/01/2015');
        $eDate = \DateTime::createFromFormat("d/m/Y", $endDate);
        $sDate->format('Y-m-d');
        $eDate->format('Y-m-d');
        $sDate = \Yii::$app->formatter->asDate($sDate, "php:Y-m-d");
        $eDate = \Yii::$app->formatter->asDate($eDate, "php:Y-m-d");
        $xsourcex = $hospital;
        
        if ($xsourcex == '')
            $xsourcex = \Yii::$app->user->identity->userProfile->sitecode;
        
        if($worklistno == '') $worklistno=$xsourcex;
        $where = "";
        $param = [];
        if ($zone != null) {
            $where = " AND aht.zone_code=:zonecode ";
            $subwhere = " AND ahti.zone_code= '$zone' ";
            $param = [':zonecode' => $zone];
        } else
        if ($province != null) {
            $where = " AND aht.provincecode=:provincecode ";
            $subwhere = " AND ahti.provincecode='$province' ";
            $param = [':provincecode' => $province];
        } else
        if ($amphur != null) {
            $where = " AND aht.amphurcode=:amphurcode ";
            $subwhere = " AND ahti.amphurcode='$amphur' ";
            $param = [':amphurcode' => $amphur];
        } else
        if ($hospital != null) {
            $where = " AND tb3.hsitecode=:xsourcex ";
            $subwhere = " AND tbd3.xsourcex='$hospital' ";
            $param = [':xsourcex' => $hospital];
        }
        
        if ($worklistno==$xsourcex) {
            $where = " AND tb3.hsitecode=:xsourcex ";
            $subwhere = " AND tbd3.xsourcex='$xsourcex' ";
            $param = [':xsourcex' => $xsourcex];
        }

        
        $doctorList = [];
        if (($worklistno != $xsourcex && $hospital == NULL) && ( $zone==null && $province==null && $amphur==null )) {
            $sql = " SELECT * FROM us_room WHERE room_type='us-room' AND worklist_id=:worklist_id ";
            $result = \Yii::$app->db->createCommand($sql, [':worklist_id' => $worklistno])->queryAll();
            
            foreach ($result as $key => $val) {
                $doctorname = self::getDoctorData($val['doctor_code']);
                $userData = self::getUserData($val['user_id']);
                $select = " SELECT
                dataid
                ,ptid
                ,user_update
                ,isdoctor
                ,f2doctorcode
                ,rstat
                ,min(sys_usimagecount) as usmin
                ,ceil(avg(sys_usimagecount)) as usavg
                ,max(sys_usimagecount) as usmax
                ,sys_usimagecount as usimage
                ,sum(if((rstat=2 OR rstat=1) AND (IFNULL(error,0)=0 AND IFNULL(f2doctorcode,0)<>0 ) ,1,0))as inspected 
                ,sum(if(rstat=1 AND ( IFNULL(error,0)<>0 OR IFNULL(f2doctorcode,0)=0 ) ,1,0))as inspecting 
                ,sum(if(rstat=0,1,0))as startinspect 
                ,sum(IF((rstat=2 OR rstat=1 OR rstat=0 ) AND IFNULL(error,0)<>0 , 1,0)) as mistake 

            FROM(
                SELECT 
                    tb3.id as dataid
                    ,tb3.ptid
                    ,tb3.user_update
                    ,tb3.isdoctor
                    ,tb3.f2doctorcode
                    ,tb3.error
                    ,tb3.rstat
                    ,wmp.room_name
                    ,tb3.sys_usimagecount
                FROM tb_data_3 tb3 INNER JOIN worklist_map_patient wmp ON tb3.ptid=wmp.ptid
                WHERE tb3.rstat < 3 AND tb3.f2v1 BETWEEN '$sDate' AND '$eDate' AND wmp.worklist_id='$worklistno' AND wmp.room_name='".$val['room_name']."' GROUP BY tb3.ptid 
            ) as usmonitor GROUP BY room_name ";
                $resultDat = \Yii::$app->db->createCommand($select, $param)->queryOne();
               //\appxq\sdii\utils\VarDumper::dump($resultDat);
                $doctorList[$key]['user_id'] = $val['user_id'];
                $doctorList[$key]['doctor_name'] = $doctorname['doctorfullname'];
                $doctorList[$key]['doctor_code'] = $val['doctor_code'];
                $doctorList[$key]['user_fullname'] = $userData['firstname'] . ' ' . $userData['lastname'];
                $doctorList[$key]['user_image'] = $userData['avatar_base_url'] . '/' . $userData['avatar_path'];
                $doctorList[$key]['username'] = $resultDat['user_id'];
                $doctorList[$key]['room_name'] = $val['room_name'];
                $doctorList[$key]['inspected'] = $resultDat['inspected']== '' ? '0' : $resultDat['inspected'];
                $doctorList[$key]['inspecting'] = $resultDat['inspecting']== '' ? '0' : $resultDat['inspecting'];
                $doctorList[$key]['startinspect'] = $resultDat['startinspect']== '' ? '0' : $resultDat['startinspect'];
                $doctorList[$key]['status'] = 'doctor';
                $doctorList[$key]['usimage'] = $resultDat['usimage'] == '' ? '0' : $resultDat['usimage'];
                $doctorList[$key]['usmin'] = $resultDat['usmin'] == '' ? '0' : $resultDat['usmin'];
                $doctorList[$key]['usavg'] = $resultDat['usavg'] == '' ? '0' : $resultDat['usavg'];
                $doctorList[$key]['usmax'] = $resultDat['usmax'] == '' ? '0' : $resultDat['usmax'];
                $doctorList[$key]['mistake'] = $resultDat['mistake'] == '' ? '0' : $resultDat['mistake'];
                // set status as follows doctor, nurse, other
            }
        } else {
            $select = " 
            SELECT
                dataid
                ,ptid
                ,user_update
                ,isdoctor
                ,user_id
                ,f2doctorcode
                ,avatar_path
                ,avatar_base_url
                ,firstname
                ,lastname
                ,rstat
                ,min(sys_usimagecount) as usmin
                ,ceil(avg(sys_usimagecount)) as usavg
                ,max(sys_usimagecount) as usmax
                ,sys_usimagecount as usimage
                ,sum(if((rstat=2 OR rstat=1) AND (IFNULL(error,0)=0 AND IFNULL(f2doctorcode,0)<>0 ) ,1,0))as inspected 
                ,sum(if(rstat=1 AND ( IFNULL(error,0)<>0 OR IFNULL(f2doctorcode,0)=0 ) ,1,0))as inspecting 
                ,sum(if(rstat=0,1,0))as startinspect 
                ,sum(IF((rstat=2 OR rstat=1 OR rstat=0 ) AND IFNULL(error,0)<>0 , 1,0)) as mistake 

            FROM(
                SELECT 
                    tb3.id as dataid
                    ,tb3.ptid
                    ,tb3.user_update
                    ,tb3.isdoctor
                    ,tb3.f2doctorcode
                    ,tb3.error
                    ,upr.user_id
                    ,upr.avatar_path
                    ,upr.avatar_base_url
                    ,upr.firstname
                    ,upr.lastname
                    ,tb3.rstat
                    ,tb3.sys_usimagecount

                FROM (tb_data_3 tb3 INNER JOIN user_profile upr ON tb3.user_update=upr.user_id)
                INNER JOIN all_hospital_thai aht ON tb3.hsitecode=aht.hcode 
                WHERE tb3.rstat < 3 AND tb3.f2v1 BETWEEN '$sDate' AND '$eDate' $where GROUP BY tb3.ptid 
            ) as usmonitor GROUP BY user_update ";
            $result = \Yii::$app->db->createCommand($select, $param)->queryAll();
            \frontend\modules\api\v1\classes\LogStash::Log("UltrasoundRoomData#1", var_export($param,true), "");
            //\appxq\sdii\utils\VarDumper::dump($result);
            foreach ($result as $key => $val) {
                $doctorname = self::getDoctorData($val['f2doctorcode']);
                $doctorList[$key]['user_id'] = $val['user_id'];
                $doctorList[$key]['doctor_name'] = $doctorname['doctorfullname'];
                $doctorList[$key]['doctor_code'] = $val['f2doctorcode'];
                $doctorList[$key]['user_fullname'] = $val['firstname'] . ' ' . $val['lastname'];
                $doctorList[$key]['user_image'] = $val['avatar_base_url'] . '/' . $val['avatar_path'];
                $doctorList[$key]['username'] = $val['user_id'];
                $doctorList[$key]['room_name'] = $val['room_name'];
                $doctorList[$key]['inspected'] = $val['inspected'];
                $doctorList[$key]['inspecting'] = $val['inspecting'];
                $doctorList[$key]['startinspect'] = $val['startinspect'];
                $doctorList[$key]['status'] = 'doctor';
                $doctorList[$key]['usimage'] = $val['usimage'] == '' ? '0' : $val['usimage'];
                $doctorList[$key]['usmin'] = $val['usmin'] == '' ? '0' : $val['usmin'];
                $doctorList[$key]['usavg'] = $val['usavg'] == '' ? '0' : $val['usavg'];
                $doctorList[$key]['usmax'] = $val['usmax'] == '' ? '0' : $val['usmax'];
                $doctorList[$key]['mistake'] = $val['mistake'] == '' ? '0' : $val['mistake'];
                // set status as follows doctor, nurse, other
            }
        }

        return $doctorList;
    }
    
    public function ExitNurseData($worklistno) {
        
        $xsourcex = \Yii::$app->user->identity->userProfile->sitecode;
        $nurseList = [];

        if ($worklistno != $xsourcex) {
            $sql = " SELECT * FROM us_room WHERE room_type='exit-nurse' AND worklist_id=:worklist_id ";
            $result = \Yii::$app->db->createCommand($sql, [':worklist_id' => $worklistno])->queryAll();
            
            foreach ($result as $key => $val) {
                $doctorname = self::getDoctorData($val['doctor_code']);
                $userData = self::getUserData($val['user_id']);
                $nurseList[$key]['user_fullname'] = $userData['firstname'] . ' ' . $userData['lastname'];
                $nurseList[$key]['user_image'] = $userData['avatar_base_url'] . '/' . $userData['avatar_path'];
                $nurseList[$key]['user_id'] = $val['user_id'];
                $nurseList[$key]['doctor_code'] = $val['doctor_code'];
                $nurseList[$key]['room_name'] = $val['room_name'];
                $nurseList[$key]['room_type'] = $val['room_type'];
            }
        
        }

        return $nurseList;
    }
    
    public function ReferData($worklistno) {
        
        $xsourcex = \Yii::$app->user->identity->userProfile->sitecode;
        $nurseList = [];

        if ($worklistno != $xsourcex) {
            $sql = " SELECT * FROM us_room WHERE room_type='refer' AND worklist_id=:worklist_id ";
            $result = \Yii::$app->db->createCommand($sql, [':worklist_id' => $worklistno])->queryAll();
            
            foreach ($result as $key => $val) {
                $doctorname = self::getDoctorData($val['doctor_code']);
                $userData = self::getUserData($val['user_id']);
                $nurseList[$key]['user_fullname'] = $userData['firstname'] . ' ' . $userData['lastname'];
                $nurseList[$key]['user_image'] = $userData['avatar_base_url'] . '/' . $userData['avatar_path'];
                $nurseList[$key]['user_id'] = $val['user_id'];
                $nurseList[$key]['doctor_code'] = $val['doctor_code'];
                $nurseList[$key]['room_name'] = $val['room_name'];
                $nurseList[$key]['room_type'] = $val['room_type'];
            }
        
        }

        return $nurseList;
    }
    
    public function OtherData($worklistno) {
        
        $xsourcex = \Yii::$app->user->identity->userProfile->sitecode;
        $nurseList = [];

        if ($worklistno != $xsourcex) {
            $sql = " SELECT * FROM us_room WHERE room_type='other' AND worklist_id=:worklist_id ";
            $result = \Yii::$app->db->createCommand($sql, [':worklist_id' => $worklistno])->queryAll();
            
            foreach ($result as $key => $val) {
                $doctorname = self::getDoctorData($val['doctor_code']);
                $userData = self::getUserData($val['user_id']);
                $nurseList[$key]['user_fullname'] = $userData['firstname'] . ' ' . $userData['lastname'];
                $nurseList[$key]['user_image'] = $userData['avatar_base_url'] . '/' . $userData['avatar_path'];
                $nurseList[$key]['user_id'] = $val['user_id'];
                $nurseList[$key]['doctor_code'] = $val['doctor_code'];
                $nurseList[$key]['room_name'] = $val['room_name'];
                $nurseList[$key]['room_type'] = $val['room_type'];
            }
        
        }

        return $nurseList;
    }

    public function PatientUltrasound($startDate = null, $endDate = null, $user_id, $inspect_type, $room_name, $worklist_id) {
        $xsourcex = \Yii::$app->user->identity->userProfile->sitecode;
        $rstat = 0;
        $sDate = \DateTime::createFromFormat("d/m/Y", $startDate); // ::createFromFormat("d/m/Y  H:i:s", '31/01/2015');
        $eDate = \DateTime::createFromFormat("d/m/Y", $endDate);
        $sDate->format('Y-m-d');
        $eDate->format('Y-m-d');
        $sDate = \Yii::$app->formatter->asDate($sDate, "php:Y-m-d");
        $eDate = \Yii::$app->formatter->asDate($eDate, "php:Y-m-d");

        $join_worklist = "";
        $where_worklist = "";
        if ($worklistno != $xsourcex) {
            $join_worklist = "  INNER JOIN worklist_map_patient wp ON tb3.ptid=wp.ptid ";
            $where_worklist = " AND worklist_id='$worklistno' AND tb3.user_update=wp.create_user";
        }

        if ($inspect_type == 'inspected') {
            $status = 2;
            $where = " (tb3.rstat=2 OR tb3.rstat=1) AND (IFNULL(tb3.error,0)=0 AND IFNULL(tb3.f2doctorcode,0)<>0) ";
        } else if ($inspect_type == 'inspecting') {
            $status = 1;
            $where = " tb3.rstat=1 AND ( IFNULL(tb3.error,0)<>0 OR IFNULL(tb3.f2doctorcode,0)=0  ) ";
        } else if ($inspect_type == 'startinspect') {
            $status = 0;
            $where = "  tb3.rstat='$status' ";
        } else if ($inspect_type == 'mistake') {
            $status = -1;
            $where = "  (tb3.rstat=2 OR tb3.rstat=1 OR tb3.rstat=0 ) AND IFNULL(tb3.f2doctorcode,0)=0 OR (tb3.sys_usimagecount='0' OR IFNULL(tb3.sys_usimagecount,0)=0) ";
        }
        
        
        if($worklist_id ==$xsourcex){
            //\appxq\sdii\utils\VarDumper::dump($user_id);
            $sql = " SELECT tb3.id as dataid_02
            ,(SELECT tb2.id FROM tb_data_2 tb2 WHERE tb2.ptid=tb3.ptid AND tb2.hsitecode='$xsourcex' LIMIT 1) as dataid_01
            ,tb1.ptid
            ,tb1.ptcodefull
            ,tb1.title
            ,tb1.`name`
            ,tb1.surname
            ,(SELECT tb1.confirm FROM tb_data_1 WHERE tb_data_1.ptid=tb3.ptid AND IFNULL(icf_upload1,'0')<>'0' LIMIT 1  )as icf_upload1
            ,(SELECT icf_upload2 FROM tb_data_1 WHERE tb_data_1.ptid=tb3.ptid AND IFNULL(icf_upload2,'0')<>'0' LIMIT 1  ) as icf_upload2
            ,tb3.f2doctorcode
            ,tb3.error
            ,f2v6a3 as refer
            ,CONCAT(IF(IFNULL(tb3.f2v2a1b1,0)<>0,'Fatty',''),' ',IF(IFNULL(tb3.f2v2a1b2,0)<>0,'PDF',''),tb3.f2v2a1b2 ) as result_inspect
            ,'$status' as `status`
            ,tb3.sys_usimagecount as usimage
            ,(SELECT count(tb2.ptid) as cca01 FROM tb_data_2 tb2 WHERE tb2.ptid=tb3.ptid AND tb2.hsitecode='$xsourcex' LIMIT 1) as cca01
            , '1' as cca02 
            FROM tb_data_1 tb1 INNER JOIN tb_data_3 tb3 ON tb1.ptid=tb3.ptid 
            WHERE tb3.user_update=:user_id AND tb3.rstat < 3 AND tb3.f2v1 BETWEEN '$sDate' AND '$eDate'
            AND ($where) AND tb3.hsitecode=:xsourcex GROUP BY tb3.ptid LIMIT 150 ";
            //\appxq\sdii\utils\VarDumper::dump($sDate);
            $result = \Yii::$app->db->createCommand($sql, [':xsourcex' => $xsourcex,':user_id'=>$user_id])->queryAll();
            
            
        }else{
            
            $sql = " SELECT tb3.id as dataid_02
            ,(SELECT tb2.id FROM tb_data_2 tb2 WHERE tb2.ptid=tb3.ptid AND tb2.hsitecode='$xsourcex' LIMIT 1) as dataid_01
            ,tb1.ptid
            ,tb1.ptcodefull
            ,tb1.title
            ,tb1.`name`
            ,tb1.surname
            ,(SELECT tb1.confirm FROM tb_data_1 WHERE tb_data_1.ptid=tb3.ptid AND IFNULL(icf_upload1,'0')<>'0' LIMIT 1  )as icf_upload1
            ,(SELECT icf_upload2 FROM tb_data_1 WHERE tb_data_1.ptid=tb3.ptid AND IFNULL(icf_upload2,'0')<>'0' LIMIT 1  ) as icf_upload2
            ,tb3.f2doctorcode
            ,tb3.error
            ,f2v6a3 as refer
            ,CONCAT(IF(IFNULL(tb3.f2v2a1b1,0)<>0,'Fatty',''),' ',IF(IFNULL(tb3.f2v2a1b2,0)<>0,'PDF',''),tb3.f2v2a1b2 ) as result_inspect
            ,'$status' as `status`
            ,tb3.sys_usimagecount as usimage
            ,(SELECT count(tb2.ptid) as cca01 FROM tb_data_2 tb2 WHERE tb2.ptid=tb3.ptid AND tb2.hsitecode='$xsourcex' LIMIT 1) as cca01
            , '1' as cca02 
            FROM tb_data_1 tb1 INNER JOIN tb_data_3 tb3 ON tb1.ptid=tb3.ptid 
            INNER JOIN worklist_map_patient wmp ON tb3.ptid=wmp.ptid
	    INNER JOIN us_room usr ON wmp.room_name=usr.room_name
            WHERE wmp.worklist_id='$worklist_id' AND tb3.rstat < 3 AND usr.room_name='$room_name'
            AND ($where) AND tb3.hsitecode=:xsourcex GROUP BY tb3.ptid LIMIT 150";
            //\appxq\sdii\utils\VarDumper::dump($sql);
            $result = \Yii::$app->db->createCommand($sql, [':xsourcex' => $xsourcex])->queryAll();
        }

        return $result;
    }

    public function getDoctorData($doctorcode) {
        $sql = "SELECT doctorcode, doctorfullname FROM doctor_all WHERE id=:id";
        $result = \Yii::$app->db->createCommand($sql, [':id' => $doctorcode])->queryOne();
        return $result;
    }
    
    public function getUserData($user_id) {
        $sql = "SELECT firstname, lastname,avatar_path,avatar_base_url FROM user_profile WHERE user_id=:id";
        $result = \Yii::$app->db->createCommand($sql, [':id' => $user_id])->queryOne();
        return $result;
    }

    public function getListSuspected() {
        $xsourcex = \Yii::$app->user->identity->userProfile->sitecode;
        $count = Yii::$app->db->createCommand('
            SELECT COUNT(*)  FROM tb_data_1 tb1 INNER JOIN tb_data_3 tb3 ON tb1.ptid=tb3.ptid
            WHERE (tb3.f2v6a3b1=1 OR tb3.f2v2a1=1) 
            AND tb1.xsourcex=:xsourcex
        ', [':xsourcex' => $xsourcex])->queryScalar();

        $sql = " SELECT  tb1.ptid as register,tb1.xsourcex as hospcode
            ,tb1.title
            ,tb1.name
            ,tb1.surname
            ,(SELECT ICF FROM tb_data_3 tb3 WHERE tb3.ptid=tb1.ptid GROUP BY tb3.ptid) as icf
            ,tb1.v3 as gender
            ,tb1.age
            ,(SELECT DISTINCT IF(ptid<>'',1,0)  FROM tb_data_2 tb2 WHERE tb2.ptid=tb1.ptid )as cca01
            ,IF(tb3.ptid<>'',1,0) as cca02
            FROM tb_data_1 tb1 INNER JOIN tb_data_3 tb3 ON tb1.ptid=tb3.ptid
            WHERE (tb3.f2v6a3b1=1 OR tb3.f2v2a1=1) 
            AND tb1.xsourcex=:xsourcex
            ";
        $provider = new SqlDataProvider([
            'sql' => $sql,
            'params' => [':xsourcex' => $xsourcex],
            'totalCount' => $count,
            'pagination' => [
                'pageSize' => 100,
            ],
            'sort' => [
                'attributes' => [
                    'hsitecode',
                    'hptcode',
                    'f2v1',
                ],
            ],
        ]);

        // returns an array of data rows
        //$models = $provider->getModels;
        return $provider;
    }

    public function getPatients() {
        $xsourcex = \Yii::$app->user->identity->userProfile->sitecode;
        $count = Yii::$app->db->createCommand('
            SELECT COUNT(DISTINCT tb1.ptid) FROM tb_data_1  tb1 WHERE tb1.xsourcex=:xsourcex
        ', [':xsourcex' => $xsourcex])->queryScalar();

        $sql = " SELECT DISTINCT tb1.ptid as register, tb1.xsourcex as hospcode
            ,tb1.title
            ,tb1.name
            ,tb1.surname
            ,(SELECT ICF FROM tb_data_3 tb3 WHERE tb3.ptid=tb1.ptid GROUP BY tb3.ptid) as icf
            ,tb1.v3 as gender
            ,tb1.age
            ,(SELECT DISTINCT IF(ptid<>'',1,0)  FROM tb_data_2 tb2 WHERE tb2.ptid=tb1.ptid )as cca01
            ,(SELECT DISTINCT IF(ptid<>'',1,0)  FROM tb_data_3 tb3 WHERE tb3.ptid=tb1.ptid ) as cca02
            FROM tb_data_1 tb1 
            WHERE tb1.xsourcex=:xsourcex
            ";

        $provider = new SqlDataProvider([
            'sql' => $sql,
            'params' => [':xsourcex' => $xsourcex],
            'totalCount' => $count,
            'pagination' => [
                'pageSize' => 100,
            ],
            'sort' => [
                'attributes' => [
                    'hsitecode',
                    'hptcode',
                    'f2v1',
                ],
            ],
        ]);

        // returns an array of data rows
        //$models = $provider->getModels;
        return $provider;
    }

}
