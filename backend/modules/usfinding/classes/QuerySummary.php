<?php
namespace backend\modules\usfinding\classes;

use yii\data\SqlDataProvider;
use yii\data\ActiveDataProvider;
use Yii;
global $db_name;
$db_name = "cascap_report1."; //config Database name. dpm(NULL) is defalut.

class QuerySummary {
    
    public function getCca01($startDate,$endDate ,$hospitalCode, $state) {
        $sDateTemp = \DateTime::createFromFormat("d/m/Y", $startDate)->format('Y-m-d'); // ::createFromFormat("d/m/Y  H:i:s", '31/01/2015');
        $eDateTemp = \DateTime::createFromFormat("d/m/Y", $endDate)->format('Y-m-d');
        $sDate = Yii::$app->formatter->asDate($sDateTemp, "php:Y-m-d");
        $eDate = Yii::$app->formatter->asDate($eDateTemp, "php:Y-m-d");

        $sql = "SELECT
count(DISTINCT cca02.ptid) AS all_person
,count(DISTINCT IF(cca01.f1v4 IN ('1'),cca02.ptid,null)) AS non_edu
,count(DISTINCT IF(cca01.f1v4 IN ('2'),cca02.ptid,null)) AS primary_edu
,count(DISTINCT IF(cca01.f1v4 IN ('3'),cca02.ptid,null)) AS junior_edu
,count(DISTINCT IF(cca01.f1v4 IN ('4'),cca02.ptid,null)) AS senior_edu
,count(DISTINCT IF(cca01.f1v4 IN ('5'),cca02.ptid,null)) AS vocational_edu
,count(DISTINCT IF(cca01.f1v4 IN ('6'),cca02.ptid,null)) AS bachelor_edu
,count(DISTINCT IF(cca01.f1v4 IN ('7'),cca02.ptid,null)) AS master_edu
,count(DISTINCT IF(cca01.f1v4 IN ('1','2','3','4','5','6','7'),cca02.ptid,null)) AS total_edu
,count(DISTINCT IF(cca01.f1v5 IN ('1'),cca02.ptid,null)) AS non_job
,count(DISTINCT IF(cca01.f1v5 IN ('2'),cca02.ptid,null)) AS farmer_job
,count(DISTINCT IF(cca01.f1v5 IN ('3'),cca02.ptid,null)) AS emp_job
,count(DISTINCT IF(cca01.f1v5 IN ('4'),cca02.ptid,null)) AS business_job
,count(DISTINCT IF(cca01.f1v5 IN ('5'),cca02.ptid,null)) AS gov_job
,count(DISTINCT IF(cca01.f1v5 IN ('6'),cca02.ptid,null)) AS other_job
,count(DISTINCT IF(cca01.f1v5 IN ('1','2','3','4','5','6'),cca02.ptid,null)) AS total_work
,count(DISTINCT IF(cca01.f1v6 IN ('0'),cca02.ptid,null)) AS non_f1v6
,count(DISTINCT IF(cca01.f1v6 IN ('1'),cca02.ptid,null)) AS first_f1v6
,count(DISTINCT IF(cca01.f1v6 IN ('2'),cca02.ptid,null)) AS second_f1v6
,count(DISTINCT IF(cca01.f1v6 IN ('3'),cca02.ptid,null)) AS third_f1v6
,count(DISTINCT IF(cca01.f1v6 IN ('4'),cca02.ptid,null)) AS more_f1v6
,count(DISTINCT IF(cca01.f1v6 IN ('5'),cca02.ptid,null)) AS forget_f1v6
,count(DISTINCT IF(cca01.f1v6 IN ('0','1','2','3','4','5'),cca02.ptid,null)) AS total_f1v6
,count(DISTINCT IF(cca01.f1v7 IN ('0'),cca02.ptid,null)) AS non_f1v7
,count(DISTINCT IF(cca01.f1v7 IN ('1'),cca02.ptid,null)) AS non_meet_f1v7
,count(DISTINCT IF(cca01.f1v7 IN ('2'),cca02.ptid,null)) AS meet_f1v7
,count(DISTINCT IF(cca01.f1v7 IN ('3'),cca02.ptid,null)) AS forget_f1v7
,count(DISTINCT IF(cca01.f1v7 IN ('0','1','2','3'),cca02.ptid,null)) AS total_f1v7
,count(DISTINCT IF(cca01.f1v8 IN ('0'),cca02.ptid,null)) AS non_f1v8
,count(DISTINCT IF(cca01.f1v8 IN ('1'),cca02.ptid,null)) AS first_f1v8
,count(DISTINCT IF(cca01.f1v8 IN ('2'),cca02.ptid,null)) AS second_f1v8
,count(DISTINCT IF(cca01.f1v8 IN ('3'),cca02.ptid,null)) AS third_f1v8
,count(DISTINCT IF(cca01.f1v8 IN ('4'),cca02.ptid,null)) AS more_f1v8
,count(DISTINCT IF(cca01.f1v8 IN ('5'),cca02.ptid,null)) AS forget_f1v8
,count(DISTINCT IF(cca01.f1v8 IN ('0','1','2','3','4','5'),cca02.ptid,null)) AS total_f1v8
,count(DISTINCT IF(cca01.f1v9 IN ('0'),cca02.ptid,null)) AS no_f1v9
,count(DISTINCT IF(cca01.f1v9 IN ('1'),cca02.ptid,null)) AS yes_f1v9
,count(DISTINCT IF(cca01.f1v9 IN ('0','1'),cca02.ptid,null)) AS total_f1v9
,count(DISTINCT IF(cca01.f1v9a1b1 IN ('1'),cca02.ptid,null)) AS f1v9a1b1
,count(DISTINCT IF(cca01.f1v9a1b2 IN ('1'),cca02.ptid,null)) AS f1v9a1b2
,count(DISTINCT IF(cca01.f1v9a1b3 IN ('1'),cca02.ptid,null)) AS f1v9a1b3
,count(DISTINCT IF(cca01.f1v9a1b4 IN ('1'),cca02.ptid,null)) AS f1v9a1b4
,count(DISTINCT IF(cca01.f1v9a1b5 IN ('1'),cca02.ptid,null)) AS f1v9a1b5
,count(DISTINCT IF(cca01.f1v9a1b6 IN ('1'),cca02.ptid,null)) AS f1v9a1b6
,count(DISTINCT IF(cca01.f1v9a1b7 IN ('1'),cca02.ptid,null)) AS f1v9a1b7
,count(DISTINCT IF(cca01.f1v9a1b8 IN ('1'),cca02.ptid,null)) AS f1v9a1b8
,count(DISTINCT IF(cca01.f1v9a1b9 IN ('1'),cca02.ptid,null)) AS f1v9a1b9
,count(DISTINCT IF(cca01.f1v10 IN ('0'),cca02.ptid,null)) AS no_f1v10
,count(DISTINCT IF(cca01.f1v10 IN ('1'),cca02.ptid,null)) AS yes_f1v10
,count(DISTINCT IF(cca01.f1v10 IN ('0','1'),cca02.ptid,null)) AS total_f1v10
,count(DISTINCT IF(cca01.f1v11 IN ('0'),cca02.ptid,null)) AS no_f1v11
,count(DISTINCT IF(cca01.f1v11 IN ('1'),cca02.ptid,null)) AS yes_f1v11
,count(DISTINCT IF(cca01.f1v11 IN ('0','1'),cca02.ptid,null)) AS total_f1v11
,count(DISTINCT IF(cca01.f1v12 IN ('0'),cca02.ptid,null)) AS no_f1v12
,count(DISTINCT IF(cca01.f1v12 IN ('1'),cca02.ptid,null)) AS yes_f1v12
,count(DISTINCT IF(cca01.f1v12 IN ('0','1'),cca02.ptid,null)) AS total_f1v12
,count(DISTINCT IF(cca01.f1v13 IN ('0'),cca02.ptid,null)) AS no_f1v13
,count(DISTINCT IF(cca01.f1v13 IN ('1'),cca02.ptid,null)) AS yes_f1v13
,count(DISTINCT IF(cca01.f1v13 IN ('0','1'),cca02.ptid,null)) AS total_f1v13
,count(DISTINCT IF(cca01.f1v14a0 IN ('1'),cca02.ptid,null)) AS f1v14a0
,count(DISTINCT IF(cca01.f1v14a1 IN ('1'),cca02.ptid,null)) AS f1v14a1
,count(DISTINCT IF(cca01.f1v14a2 IN ('1'),cca02.ptid,null)) AS f1v14a2
,count(DISTINCT IF(cca01.f1v14a3 IN ('1'),cca02.ptid,null)) AS f1v14a3
,count(DISTINCT IF(cca01.f1v14a4 IN ('1'),cca02.ptid,null)) AS f1v14a4
FROM
(SELECT ptid,hsitecode FROM ".$GLOBALS['db_name']."tb_data_3 WHERE f2v1 BETWEEN :sDate AND :eDate) cca02
INNER JOIN ".$GLOBALS['db_name']."tb_data_2 cca01 ON cca01.ptid=cca02.ptid
WHERE cca01.rstat IN (1,2,4)";

         if($state == 0){
            $sql = $sql."AND cca01.f1v3 IN ('1','2')";
        }
        if($state == 1){
            $sql = $sql."AND cca01.f1v3 IN ('1','2')"."AND cca01.hsitecode = ".$hospitalCode ;
        }
        if($state == 2){
            $sql = $sql."AND cca01.f1v3 = '1'"."AND cca01.hsitecode = ".$hospitalCode;
        }
        if($state == 3){
            $sql = $sql."AND cca01.f1v3 ='2'"."AND cca01.hsitecode = ".$hospitalCode;
        }
        
        $result = \Yii::$app->db->createCommand($sql,[':sDate' => $sDate,':eDate' => $eDate])->queryOne();
        //\appxq\sdii\utils\VarDumper::dump($result);
        return $result;
    }
    
     public function getCca01Age($startDate,$endDate ,$hospitalCode, $state) {
        $sDateTemp = \DateTime::createFromFormat("d/m/Y", $startDate)->format('Y-m-d'); // ::createFromFormat("d/m/Y  H:i:s", '31/01/2015');
        $eDateTemp = \DateTime::createFromFormat("d/m/Y", $endDate)->format('Y-m-d');
        $sDate = Yii::$app->formatter->asDate($sDateTemp, "php:Y-m-d");
        $eDate = Yii::$app->formatter->asDate($eDateTemp, "php:Y-m-d");
        
        $data ="(SELECT f1v3,cca01.ptid,cca01.hsitecode FROM (SELECT ptid FROM ".$GLOBALS['db_name']."tb_data_3 WHERE f2v1 BETWEEN :sDate AND :eDate) cca02
INNER JOIN ".$GLOBALS['db_name']."tb_data_2 cca01 ON cca01.ptid=cca02.ptid WHERE cca01.rstat IN (1,2,4)) cca INNER JOIN ".$GLOBALS['db_name']."tb_data_1 ON tb_data_1.ptid=cca.ptid ";
        
        $sql ="SELECT count(DISTINCT(tb_data_1.ptid)) AS person 
,count(DISTINCT IF( age <= ('29') ,tb_data_1.ptid,null)) AS age_20
,count(DISTINCT IF( age >= ('30') AND age <= ('39') ,tb_data_1.ptid,null)) AS age_30
,count(DISTINCT IF( age >= ('40') AND age <= ('49') ,tb_data_1.ptid,null)) AS age_40
,count(DISTINCT IF( age >= ('50') AND age <= ('59') ,tb_data_1.ptid,null)) AS age_50
,count(DISTINCT IF( age >= ('60') AND age <= ('69') ,tb_data_1.ptid,null)) AS age_60
,count(DISTINCT IF( age >= ('70') AND age <= ('79') ,tb_data_1.ptid,null)) AS age_70
,count(DISTINCT IF( age >= ('80') ,tb_data_1.ptid,null)) AS age_80
,AVG(age) AS avg, STDDEV(age) AS sd, MIN(age) AS min, MAX(age) AS max FROM ".$data;
        if($state == 0){
            $sql = $sql."WHERE cca.f1v3 IN ('1','2') and age >0 and age <200 ";
        }
        if($state == 1){
            $sql = $sql."WHERE tb_data_1.hsitecode and cca.hsitecode=".$hospitalCode." and cca.f1v3 IN ('1','2') and age >0 and age <200";
        }
        if($state == 2){
            $sql = $sql."WHERE tb_data_1.hsitecode and cca.hsitecode=".$hospitalCode." and cca.f1v3 = ('1') and age >0 and age <200";
        }
        if($state == 3){
            $sql = $sql."WHERE tb_data_1.hsitecode and cca.hsitecode=".$hospitalCode." and cca.f1v3 = ('2') and age >0 and age <200";
        }
        if($state == 4){ $sql = null;
            $total_age ="(SELECT DISTINCT(tb_data_1.ptid),age FROM ".$data."WHERE cca.f1v3 IN ('1','2') AND tb_data_1.hsitecode and cca.hsitecode=".$hospitalCode.")";
            $sql = "SELECT age AS median FROM (SELECT a1.ptid, a1.age, COUNT(a1.age) Rank FROM ".$total_age." a1, ".$total_age." a2 
WHERE a1.age < a2.age OR (a1.age=a2.age AND a1.ptid <= a2.ptid) group by a1.ptid, a1.age order by a1.age desc) a3 WHERE Rank = (SELECT (COUNT(*)+1) DIV 2 FROM ".$total_age." total_age)";
        }
        if($state == 5){ $sql = null;
            $total_age ="(SELECT DISTINCT(tb_data_1.ptid),age FROM ".$data."WHERE cca.f1v3 IN ('1','2') AND tb_data_1.hsitecode and cca.hsitecode=".$hospitalCode.")";
            $sql = "SELECT age AS median FROM (SELECT a1.ptid, a1.age, COUNT(a1.age) Rank FROM ".$total_age." a1, ".$total_age." a2 
WHERE a1.age < a2.age OR (a1.age=a2.age AND a1.ptid <= a2.ptid) group by a1.ptid, a1.age order by a1.age desc) a3 WHERE Rank = (SELECT (COUNT(*)+1) DIV 2 FROM ".$total_age." total_age)";
        }
        if($state == 6){ $sql = null;
            $total_age ="(SELECT DISTINCT(tb_data_1.ptid),age FROM ".$data."WHERE cca.f1v3 = ('1') AND tb_data_1.hsitecode and cca.hsitecode=".$hospitalCode.")";
            $sql = "SELECT age AS median FROM (SELECT a1.ptid, a1.age, COUNT(a1.age) Rank FROM ".$total_age." a1, ".$total_age." a2 
WHERE a1.age < a2.age OR (a1.age=a2.age AND a1.ptid <= a2.ptid) group by a1.ptid, a1.age order by a1.age desc) a3 WHERE Rank = (SELECT (COUNT(*)+1) DIV 2 FROM ".$total_age." total_age)";
        }
        if($state == 7){ $sql = null;
            $total_age ="(SELECT DISTINCT(tb_data_1.ptid),age FROM ".$data."WHERE cca.f1v3 = ('2') AND tb_data_1.hsitecode and cca.hsitecode=".$hospitalCode.")";
            $sql = "SELECT age AS median FROM (SELECT a1.ptid, a1.age, COUNT(a1.age) Rank FROM ".$total_age." a1, ".$total_age." a2 
WHERE a1.age < a2.age OR (a1.age=a2.age AND a1.ptid <= a2.ptid) group by a1.ptid, a1.age order by a1.age desc) a3 WHERE Rank = (SELECT (COUNT(*)+1) DIV 2 FROM ".$total_age." total_age)";
        }
         $result = \Yii::$app->db->createCommand($sql,[':sDate' => $sDate,':eDate' => $eDate])->queryOne();
        //\appxq\sdii\utils\VarDumper::dump($result);
        return $result;
     }
    
     public function getDoctor($startDate,$endDate,$hospitalCode,$gender) {
        $sDateTemp = \DateTime::createFromFormat("d/m/Y", $startDate)->format('Y-m-d'); // ::createFromFormat("d/m/Y  H:i:s", '31/01/2015');
        $eDateTemp = \DateTime::createFromFormat("d/m/Y", $endDate)->format('Y-m-d');
        $sDate = Yii::$app->formatter->asDate($sDateTemp, "php:Y-m-d");
        $eDate = Yii::$app->formatter->asDate($eDateTemp, "php:Y-m-d");
        if($gender=='1'){
        $sql = " SELECT count(DISTINCT cca02.ptid) AS person,count(DISTINCT IF(cca01.f1v3 IN ('1','2'),cca02.ptid,null)) "
                . "AS person_sex,count(DISTINCT IF(cca01.f1v3='1',cca02.ptid,null)) AS person_sex_male,count(DISTINCT IF(cca01.f1v3='2',cca02.ptid,null)) "
                . "AS person_sex_female FROM (SELECT ptid,hsitecode FROM ".$GLOBALS['db_name']."tb_data_3 WHERE hsitecode=:hospitalCode AND f2v1 BETWEEN :sDate AND :eDate) "
                . "cca02 INNER JOIN ".$GLOBALS['db_name']."tb_data_2 cca01 ON cca01.ptid=cca02.ptid WHERE cca01.rstat IN (1,2,4)";
        $result = \Yii::$app->db->createCommand($sql,[':hospitalCode' => $hospitalCode ,':sDate' => $sDate, ':eDate' => $eDate])->queryOne();
        } else {
        $sql = " SELECT doctorfullname , numpatient,doctorcode FROM (SELECT  f2doctorcode, COUNT(DISTINCT ptid) as numpatient FROM tb_data_3 WHERE rstat IN (1,2,4) "
                . "AND hsitecode=:hospitalCode AND f2v1 BETWEEN :sDate AND :eDate GROUP BY f2doctorcode ) doctor "
                . "INNER JOIN doctor_all ON doctor.f2doctorcode = doctor_all.doctorcode";
        $result = \Yii::$app->db->createCommand($sql,[':hospitalCode' => $hospitalCode ,':sDate' => $sDate, ':eDate' => $eDate])->queryAll();
        }
        //\appxq\sdii\utils\VarDumper::dump($result);
        return $result;
    }
    
    public function getCca02($startDate,$endDate,$hospitalCode,$data,$state) {
        
        $sDateTemp = \DateTime::createFromFormat("d/m/Y", $startDate)->format('Y-m-d'); // ::createFromFormat("d/m/Y  H:i:s", '31/01/2015');
        $eDateTemp = \DateTime::createFromFormat("d/m/Y", $endDate)->format('Y-m-d');
        $sDate = Yii::$app->formatter->asDate($sDateTemp, "php:Y-m-d");
        $eDate = Yii::$app->formatter->asDate($eDateTemp, "php:Y-m-d");
        
       $sql = " SELECT COUNT(DISTINCT(ptid)) as count FROM ".$GLOBALS['db_name']."tb_data_3 WHERE hsitecode=:hospitalCode AND ".$data." = :state AND f2v1 BETWEEN :sDate AND :eDate";
       
        $result = \Yii::$app->db->createCommand($sql,[':sDate' => $sDate,':eDate' => $eDate,':hospitalCode' => $hospitalCode,':state' => $state])->queryOne();
          //      \appxq\sdii\utils\VarDumper::dump($result);
        return $result;
    }
    
        public function getCca02Drilldown($startDate,$endDate,$hospitalCode,$data,$cca02) {
        
        $sDateTemp = \DateTime::createFromFormat("d/m/Y", $startDate)->format('Y-m-d'); // ::createFromFormat("d/m/Y  H:i:s", '31/01/2015');
        $eDateTemp = \DateTime::createFromFormat("d/m/Y", $endDate)->format('Y-m-d');
        $sDate = Yii::$app->formatter->asDate($sDateTemp, "php:Y-m-d");
        $eDate = Yii::$app->formatter->asDate($eDateTemp, "php:Y-m-d");

        $sql ="SELECT id as id_cca02 ,ptid,hsitecode,hptcode,f2v1,".$data." as data FROM ".$GLOBALS['db_name']."tb_data_3 
WHERE hsitecode=:hospitalCode AND (".$data.") in (".$cca02.") AND f2v1 BETWEEN :sDate AND :eDate GROUP BY ptid" ;
        //\appxq\sdii\utils\VarDumper::dump($sql);
        $result = \Yii::$app->db->createCommand($sql,[':sDate' => $sDate,':eDate' => $eDate,':hospitalCode' => $hospitalCode])->queryAll();
       
        return $result;
    }
    
     public function getCca01Drilldown($startDate,$endDate,$hospitalCode,$data,$gender,$cca01) {
        
        $sDateTemp = \DateTime::createFromFormat("d/m/Y", $startDate)->format('Y-m-d'); // ::createFromFormat("d/m/Y  H:i:s", '31/01/2015');
        $eDateTemp = \DateTime::createFromFormat("d/m/Y", $endDate)->format('Y-m-d');
        $sDate = Yii::$app->formatter->asDate($sDateTemp, "php:Y-m-d");
        $eDate = Yii::$app->formatter->asDate($eDateTemp, "php:Y-m-d");
         
        $sql ="SELECT cca.id as id_cca01,cca.ptid,cca.hsitecode,cca.hptcode,cca.f2v1,cca.".$data." as data,tb_data_1.confirm FROM
(SELECT tb_data_2.id,tb_data_2.ptid,tb_data_2.hsitecode,tb_data_2.hptcode,cca02.f2v1,tb_data_2.".$data." FROM
(SELECT ptid,hsitecode,f2v1 FROM ".$GLOBALS['db_name']."tb_data_3 WHERE f2v1 BETWEEN :sDate AND :eDate) cca02 INNER JOIN ".$GLOBALS['db_name']."tb_data_2 ON tb_data_2.ptid=cca02.ptid
WHERE tb_data_2.rstat IN (1,2,4) AND tb_data_2.hsitecode = :hospitalCode AND tb_data_2.f1v3 IN (".$gender.") AND (tb_data_2.".$data." IN (".$cca01."))) cca
INNER JOIN ".$GLOBALS['db_name']."tb_data_1 WHERE tb_data_1.ptid = cca.ptid GROUP BY cca.ptid";
        
        $result = \Yii::$app->db->createCommand($sql,[':sDate' => $sDate,':eDate' => $eDate,':hospitalCode' => $hospitalCode])->queryAll();
       //\appxq\sdii\utils\VarDumper::dump($sql);
        return $result;
    }
    
    public function getSummaryDrilldown($startDate,$endDate,$hospitalCode,$doctorcode,$gender,$state) {
        
        $sDateTemp = \DateTime::createFromFormat("d/m/Y", $startDate)->format('Y-m-d'); // ::createFromFormat("d/m/Y  H:i:s", '31/01/2015');
        $eDateTemp = \DateTime::createFromFormat("d/m/Y", $endDate)->format('Y-m-d');
        $sDate = Yii::$app->formatter->asDate($sDateTemp, "php:Y-m-d");
        $eDate = Yii::$app->formatter->asDate($eDateTemp, "php:Y-m-d");
        if($state =='gender') {
             $sql = "SELECT id as id_regis,pt.ptid,pt.hsitecode,pt.hptcode,pt.f1v3,pt.f2v1 FROM (SELECT tb_data_2.ptid,tb_data_2.hsitecode,tb_data_2.hptcode,tb_data_2.f1v3,ptdate.f2v1 FROM
(SELECT ptid,f2v1 FROM ".$GLOBALS['db_name']."tb_data_3 WHERE f2v1 BETWEEN :sDate AND :eDate AND hsitecode =:hospitalCode) ptdate
INNER JOIN ".$GLOBALS['db_name']."tb_data_2 ON ptdate.ptid = tb_data_2.ptid AND tb_data_2.f1v3 ".$gender." and tb_data_2.rstat IN (1,2,4)) pt INNER JOIN tb_data_1 ON pt.ptid = tb_data_1.ptid GROUP BY pt.ptid";
    $result = \Yii::$app->db->createCommand($sql,[':sDate' => $sDate,':eDate' => $eDate,':hospitalCode' => $hospitalCode])->queryAll();   
        } else {
        $sql = "SELECT gender.id as id_cca02,tb_data_1.ptid,tb_data_1.hsitecode,tb_data_1.hptcode,tb_data_1.age,gender.f1v3,gender.f2v1 FROM
(SELECT pt_doctor.id,pt_doctor.ptid,pt_doctor.f2v1,tb_data_2.f1v3 from
(SELECT id,ptid,f2v1 FROM ".$GLOBALS['db_name']."tb_data_3 WHERE rstat IN (1,2,4) AND hsitecode =:hospitalCode
 AND f2v1 BETWEEN :sDate AND :eDate AND f2doctorcode=:doctorcode) pt_doctor
INNER JOIN ".$GLOBALS['db_name']."tb_data_2 ON pt_doctor.ptid = tb_data_2.ptid) gender
INNER JOIN ".$GLOBALS['db_name']."tb_data_1 ON gender.ptid = tb_data_1.ptid AND tb_data_1.hsitecode =:hospitalCode GROUP BY tb_data_1.ptid";
        $result = \Yii::$app->db->createCommand($sql,[':sDate' => $sDate,':eDate' => $eDate,':hospitalCode' => $hospitalCode,':doctorcode' => $doctorcode,':gender' => $gender])->queryAll();
    }
        return $result;
    }
    
    public function getCca01Diagnose($startDate,$endDate,$hospitalCode,$gender) {
        
        $sDateTemp = \DateTime::createFromFormat("d/m/Y", $startDate)->format('Y-m-d'); // ::createFromFormat("d/m/Y  H:i:s", '31/01/2015');
        $eDateTemp = \DateTime::createFromFormat("d/m/Y", $endDate)->format('Y-m-d');
        $sDate = Yii::$app->formatter->asDate($sDateTemp, "php:Y-m-d");
        $eDate = Yii::$app->formatter->asDate($eDateTemp, "php:Y-m-d");
        
        if($hospitalCode==null){
            $sql ="SELECT count(DISTINCT(cca.ptid)) AS person  FROM
(SELECT tb_data_2.ptid,cca02.f2v1,tb_data_2.f1v14a0,tb_data_2.f1v14a1,tb_data_2.f1v14a2,tb_data_2.f1v14a3,tb_data_2.f1v14a4 FROM
(SELECT ptid,hsitecode,f2v1 FROM ".$GLOBALS['db_name']."tb_data_3 WHERE f2v1 BETWEEN :sDate AND :eDate) cca02
INNER JOIN ".$GLOBALS['db_name']."tb_data_2 ON tb_data_2.ptid=cca02.ptid
WHERE tb_data_2.rstat IN (1,2,4) AND tb_data_2.f1v3 IN (".$gender.") AND (tb_data_2.f1v14a0 or tb_data_2.f1v14a1 or tb_data_2.f1v14a2 or tb_data_2.f1v14a3 or tb_data_2.f1v14a4 IN ('1'))) cca
INNER JOIN ".$GLOBALS['db_name']."tb_data_1 WHERE tb_data_1.ptid = cca.ptid ";
        $result = \Yii::$app->db->createCommand($sql,[':sDate' => $sDate,':eDate' => $eDate])->queryOne();
        } else {
            $sql ="SELECT cca.ptid,tb_data_1.hsitecode,tb_data_1.hptcode,'1' as data,tb_data_1.confirm FROM
(SELECT tb_data_2.ptid,cca02.f2v1,tb_data_2.f1v14a0,tb_data_2.f1v14a1,tb_data_2.f1v14a2,tb_data_2.f1v14a3,tb_data_2.f1v14a4,tb_data_2.hsitecode FROM
(SELECT ptid,hsitecode,f2v1 FROM ".$GLOBALS['db_name']."tb_data_3 WHERE f2v1 BETWEEN :sDate AND :eDate) cca02 INNER JOIN ".$GLOBALS['db_name']."tb_data_2 ON tb_data_2.ptid=cca02.ptid
WHERE tb_data_2.rstat IN (1,2,4) and tb_data_2.hsitecode = :hospitalCode AND tb_data_2.f1v3 IN (".$gender.") 
    AND (tb_data_2.f1v14a0 or tb_data_2.f1v14a1 or tb_data_2.f1v14a2 or tb_data_2.f1v14a3 or tb_data_2.f1v14a4 IN ('1'))) cca
INNER JOIN ".$GLOBALS['db_name']."tb_data_1 WHERE tb_data_1.ptid = cca.ptid AND tb_data_1.hsitecode = :hospitalCode GROUP BY cca.ptid";          
        $result = \Yii::$app->db->createCommand($sql,[':sDate' => $sDate,':eDate' => $eDate,':hospitalCode' => $hospitalCode])->queryAll();
        }  
       //\appxq\sdii\utils\VarDumper::dump($aa);
        return $result;
    }
    
     public function getRelationDrilldown($startDate,$endDate,$hospitalCode,$gender) {
        
        $sDateTemp = \DateTime::createFromFormat("d/m/Y", $startDate)->format('Y-m-d'); // ::createFromFormat("d/m/Y  H:i:s", '31/01/2015');
        $eDateTemp = \DateTime::createFromFormat("d/m/Y", $endDate)->format('Y-m-d');
        $sDate = Yii::$app->formatter->asDate($sDateTemp, "php:Y-m-d");
        $eDate = Yii::$app->formatter->asDate($eDateTemp, "php:Y-m-d");
        
        if($hospitalCode==null){
            $sql ="SELECT count(DISTINCT (cca.ptid)) as person FROM (SELECT tb_data_2.ptid,cca02.f2v1,tb_data_2.f1v9a1b1,tb_data_2.f1v9a1b2,tb_data_2.f1v9a1b3,tb_data_2.f1v9a1b4,
tb_data_2.f1v9a1b5,tb_data_2.f1v9a1b6,tb_data_2.f1v9a1b7,tb_data_2.f1v9a1b8,tb_data_2.f1v9a1b9,tb_data_2.hsitecode FROM (SELECT ptid,hsitecode,f2v1 FROM ".$GLOBALS['db_name']."tb_data_3 WHERE f2v1 
BETWEEN :sDate AND :eDate) cca02 INNER JOIN ".$GLOBALS['db_name']."tb_data_2 ON tb_data_2.ptid=cca02.ptid WHERE tb_data_2.rstat IN (1,2,4) AND tb_data_2.f1v3 IN ('2') AND 
(tb_data_2.f1v9a1b1 or tb_data_2.f1v9a1b2 or tb_data_2.f1v9a1b3 or tb_data_2.f1v9a1b4 or tb_data_2.f1v9a1b5 or tb_data_2.f1v9a1b6 or tb_data_2.f1v9a1b7 or tb_data_2.f1v9a1b8 or tb_data_2.f1v9a1b9 IN ('1'))) cca
INNER JOIN ".$GLOBALS['db_name']."tb_data_1 WHERE tb_data_1.ptid = cca.ptid";
        $result = \Yii::$app->db->createCommand($sql,[':sDate' => $sDate,':eDate' => $eDate])->queryOne();
        } else {
            $sql ="SELECT cca.ptid,tb_data_1.hsitecode,tb_data_1.hptcode,'1' as data,tb_data_1.confirm FROM (SELECT tb_data_2.ptid,cca02.f2v1,tb_data_2.f1v9a1b1,tb_data_2.f1v9a1b2,tb_data_2.f1v9a1b3,
tb_data_2.f1v9a1b4,tb_data_2.f1v9a1b5,tb_data_2.f1v9a1b6,tb_data_2.f1v9a1b7,tb_data_2.f1v9a1b8,tb_data_2.f1v9a1b9,tb_data_2.hsitecode FROM (SELECT ptid,hsitecode,f2v1 FROM ".$GLOBALS['db_name']."tb_data_3 
WHERE f2v1 BETWEEN :sDate AND :eDate) cca02 INNER JOIN ".$GLOBALS['db_name']."tb_data_2 ON tb_data_2.ptid=cca02.ptid WHERE tb_data_2.rstat IN (1,2,4) and tb_data_2.hsitecode = :hospitalCode AND tb_data_2.f1v3 IN (".$gender.") AND 
(tb_data_2.f1v9a1b1 or tb_data_2.f1v9a1b2 or tb_data_2.f1v9a1b3 or tb_data_2.f1v9a1b4 or tb_data_2.f1v9a1b5 or tb_data_2.f1v9a1b6 or tb_data_2.f1v9a1b7 or tb_data_2.f1v9a1b8 or tb_data_2.f1v9a1b9 IN ('1'))) cca
INNER JOIN ".$GLOBALS['db_name']."tb_data_1 WHERE tb_data_1.ptid = cca.ptid AND tb_data_1.hsitecode = :hospitalCode GROUP BY cca.ptid";          
        $result = \Yii::$app->db->createCommand($sql,[':sDate' => $sDate,':eDate' => $eDate,':hospitalCode' => $hospitalCode])->queryAll();
        }  
       //\appxq\sdii\utils\VarDumper::dump($aa);
        return $result;
    }
    
    public function getAgeDrilldown($startDate,$endDate,$hospitalCode,$gender,$ageInterval) {
        
        $sDateTemp = \DateTime::createFromFormat("d/m/Y", $startDate)->format('Y-m-d'); // ::createFromFormat("d/m/Y  H:i:s", '31/01/2015');
        $eDateTemp = \DateTime::createFromFormat("d/m/Y", $endDate)->format('Y-m-d');
        $sDate = Yii::$app->formatter->asDate($sDateTemp, "php:Y-m-d");
        $eDate = Yii::$app->formatter->asDate($eDateTemp, "php:Y-m-d");
        $part = explode(",", $ageInterval);
        $sAge = $part[0];  $eAge = $part[1]; 
       
        $sql ="SELECT cca.id as id_cca01,cca.ptid,cca.hsitecode,tb_data_1.hptcode,'1' as data,tb_data_1.confirm FROM (SELECT id,f1v3,cca01.ptid,cca01.hsitecode,cca02.f2v1 FROM 
(SELECT ptid,hsitecode,f2v1 FROM ".$GLOBALS['db_name']."tb_data_3 WHERE f2v1 BETWEEN :sDate AND :eDate) cca02 INNER JOIN ".$GLOBALS['db_name']."tb_data_2 cca01 ON cca01.ptid=cca02.ptid WHERE cca01.rstat IN (1,2,4)) cca
INNER JOIN ".$GLOBALS['db_name']."tb_data_1 ON tb_data_1.ptid = cca.ptid WHERE cca.f1v3 IN (".$gender.") and age >= :sAge and age <= ".$eAge." AND tb_data_1.hsitecode and cca.hsitecode = :hospitalCode GROUP BY tb_data_1.ptid";

        $result = \Yii::$app->db->createCommand($sql,[':sDate' => $sDate,':eDate' => $eDate,':hospitalCode' => $hospitalCode,':sAge' => $sAge])->queryAll(); 
       //\appxq\sdii\utils\VarDumper::dump($aa);
        return $result;
    }
    
    public function getTestD($id) {

        $sql ="SELECT * FROM ".$GLOBALS['db_name']."tba WHERE id=:id";

        $result = \Yii::$app->db->createCommand($sql,[':id' => $id])->queryAll(); 
     //  \appxq\sdii\utils\VarDumper::dump($result);
        return $result;
    }
}

?>