<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace backend\modules\usfinding\classes;

/**
 * Description of QueryCCA02
 *
 * @author chaiwat
 */
use Yii;

class QueryCCA02 {
    //put your code here
    public static function getListDuplicate60()
    {
        if( TRUE ){
            $sql = "select hsitecode,hptcode,f2v1,count(*) as recs ";
            $sql.= "from tb_data_3 ";
            $sql.= "where f2v1 between '2016-10-01' and NOW() ";
            $sql.= "and rstat in ('1','2') ";
            $sql.= "group by hsitecode,hptcode,f2v1 having recs>1 ";
            $sql.= "order by hsitecode,hptcode,f2v1 ";
            //echo $sql;
            $dataProvider = Yii::$app->db->createCommand($sql)->queryAll();
            return $dataProvider;
        }
    }
    
    public static function getListVisit($hsitecode, $hptcode, $f2v1)
    {
        if( TRUE ){
            $ezf_id = "1437619524091524800";
            $sql = "select id,ptid,target,hsitecode,hptcode,f2v1,'$ezf_id' as ezf_id,rstat ";
            $sql.= "from tb_data_3 ";
            $sql.= "where f2v1 = :f2v1 ";
            $sql.= "and rstat in ('1','2') ";
            $sql.= "and hsitecode = :hsitecode ";
            $sql.= "and hptcode = :hptcode ";
            $sql.= "order by hsitecode,hptcode,f2v1 ";
            //echo $sql;
            $dataProvider = Yii::$app->db->createCommand($sql,[":hsitecode"=>$hsitecode, ":hptcode"=>$hptcode, ":f2v1"=>$f2v1])->queryAll();
            return $dataProvider;
        }
    }
    
    
    # function for PID Management
    public static function getDetByID($id = null){
        if(strlen($id)>0){
            $sql = "select *,FROM_UNIXTIME(substr(id,1,10)) as dateofaddrecord ";
            $sql.= "from tb_data_3 ";
            $sql.= "where id=:id ";
            $sql.= "and rstat<>3 ";
            //echo $sql;
            $dataProvider = Yii::$app->db->createCommand($sql,[":id"=>$id])->queryOne();
            return $dataProvider;
        }
    }
    
    public static function getIDOfByPTID($ptid = null){
        if(strlen(trim($ptid))>0 ){
            $sql = "select id,ptid,rstat ";
            $sql.= "from tb_data_3 ";
            $sql.= "where ptid=:ptid ";
            $sql.= "and rstat<>3 ";
            
            $dataProvider = Yii::$app->db->createCommand($sql,[":ptid"=>$ptid])->queryAll();
            return $dataProvider;
        }
    }
    
    
}
