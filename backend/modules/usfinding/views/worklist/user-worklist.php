<?php

use yii\widgets\ActiveForm;
use kartik\widgets\Select2;
use yii\web\JsExpression;
use kartik\grid\GridView;
use yii\helpers\Url;
use yii\helpers\Html;
use \frontend\modules\api\v1\classes\MainQuery;
?>

<div id="input-patient">
    <div class="clearfix"></div>
    <div class="col-md-12">
        <label style="padding-top: 5px;font-size:16px;" class="section-text" id="label-select">เพิ่ม Admin site :</label>
    </div>
    <div class="col-md-3">
        <?php
        echo Select2::widget([
            'name' => 'user',
            'id' => 'user',
            'options' => ['placeholder' => 'ใส่รหัส หรือ ชื่อ-นามสกุล'],
            'pluginOptions' => [
                'allowClear' => true,
                'minimumInputLength' => 3, //ต้องพิมพ์อย่างน้อย 3 อักษร ajax จึงจะทำงาน
                'ajax' => [
                    'url' => '/usfinding/worklist/get-user-site',
                    'dataType' => 'json', //รูปแบบการอ่านคือ json
                    'data' => new JsExpression('function(params) { 
                            return {q:params.term}; 
                            }
                         '),
                ],
                'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                'templateResult' => new JsExpression('function(city) { return city.text; }'),
                'templateSelection' => new JsExpression('function (city) { return city.text; }'),
            ]
        ]);
        ?>
    </div>
    <div class="col-md-1" >
        <button id="btn-addpatient" class="btn btn-primary" style="font-size:16px;"><strong>Submit</strong></button>
    </div>
    <div class="col-md-1" >
        <!--<button id="btn-user-all" class="btn btn-warning" style="font-size:16px;"><strong>ผู้ใช้ที่อยู่ในหน่วยงานทั้งหมด</strong></button>-->
    </div>
    <div class="clearfix"></div>
</div>
<br/>
<div class="panel-info" >
    <div class="panel-heading " style="border-radius: 10px 10px 0 0;">
        <table>
            <tr>
                <td rowspan="2">
                    <i class="fa fa-users" style="font-size:40px;padding-right:15px;"></i>
                </td>
                <td>
                    <label style="font-size:22px"><strong>Admin site (เจ้าหน้าที่ ที่มีสิทธิ์จัดการข้อมูล)</strong></label><br/>
                </td>
            </tr>
        </table>
    </div>
</div>
<div id="modal-addwarning" class="modal fade" role="dialog" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="background: green;color:#fff;">
                <h4 class="modal-title"><strong>Warning !</strong></h4>
            </div>
            <div class="modal-body" style="padding-left: 20px;">

            </div>

            <div class="modal-footer">
                <button type="button" id="btn-cancel" class="btn btn-primary" data-dismiss="modal"><strong>OK</strong></button>
            </div>

        </div>
    </div>
</div>
<?php
//\appxq\sdii\utils\VarDumper::dump($provider);
echo GridView::widget([
    'dataProvider' => $provider,
    'id' => 'sites-report',
    'panel' => [
        'type' => \Yii::$app->request->get('action') ? Gridview::TYPE_SUCCESS : Gridview::TYPE_SUCCESS,
    ],
    'columns' => [
//                            [
//                            'class' => 'yii\grid\SerialColumn',
//                            'headerOptions' => ['style' => 'text-align: center;'],
//                            'contentOptions' => ['style' => 'width:5px;text-align: center;'],
//                        ],
            [
            'header' => 'ลำดับที่',
            'class' => 'yii\grid\SerialColumn',
            'headerOptions' => ['style' => 'text-align: center;'],
            'contentOptions' => ['style' => 'width:4%;text-align: center;'],
        ],
            [
            'header' => 'เลขที่ Woklist',
            'value' => function ($model) {
                return $model['worklist_id'];
            },
            'headerOptions' => ['style' => 'text-align: center;'],
            'contentOptions' => ['style' => 'width:15%;text-align: center;'],
        ],
            [
            'header' => 'รหัสผู้ใช้',
            'attribute' => 'user_id',
            'value' => function ($model) {
                return $model['user_id'];
            },
            'headerOptions' => ['style' => 'text-align: center;'],
            'contentOptions' => ['style' => 'width:30px;text-align: right;'],
        ], [
            'header' => 'ชื่อ-นามสกุล',
            'value' => function ($model) {
                return $model['firstname'] . ' ' . $model['lastname'];
            },
            'headerOptions' => ['style' => 'text-align: center;'],
            'contentOptions' => ['style' => 'width:30px;text-align: right;'],
        ], [
            'header' => 'permission',
            'attribute' => 'permission',
            'format' => 'raw',
            'value' => function ($model) {//
                $select = $model['permission'] == 0 ? 'selected' : '';
                $select1 = $model['permission'] == 1 ? 'selected' : '';
                $select2 = $model['permission'] == 2 ? 'selected' : '';
                return "<select name='permission-dd' onchange=onChangePermiss('$model[user_id]','$model[worklist_id]') class='form-control' id='permission-dd$model[user_id]'>
                        <option value='0' $select>User</option>
                        <option value='1' $select1>Adminsite</option>
                        <option value='2' $select2>Administrator</option>
                    </select>";
            },
            'headerOptions' => ['style' => 'text-align: center;'],
            'contentOptions' => ['style' => 'width:40px;text-align: right;'],
        ], [
            'header' => 'เพิ่มโดย',
            'value' => function ($model) {
                return MainQuery::GetUserProfileById($model['create_user']);
            },
            'headerOptions' => ['style' => 'text-align: center;'],
            'contentOptions' => ['style' => 'width:30px;text-align: right;'],
        ], [
            'format' => 'raw',
            'header' => '#',
            'value' => function ($model) {

                $btn_delete = Html::button("ลบ", ['onclick' => 'onDeleteUser("' . $model['user_id'] . '")', 'class' => 'btn btn-danger']);
                return $btn_edit . $btn_delete;
            },
            'headerOptions' => ['style' => 'text-align: center;'],
            'contentOptions' => ['style' => 'width:30px;text-align: center;'],
        ]
    ],
    'pjax' => false
]);

$this->registerJs("
    $(function(){
        $('#btn-addpatient').on('click', function(){
            var userid = $('#user').val();
            var worklist_id = $('#worklistno').val();
                var warning = $('#modal-addwarning');
             $.ajax({
                url:'" . Url::to('/usfinding/worklist/add-usersite/') . "',
                method:'POST',
                type:'html',
                data:{
                    userid:userid,
                    worklist_id:worklist_id
                },
                success:function(result){
                    if(result=='success'){
                        showUser(worklist_id);
                    }else{
                        warning.modal();
                        warning.find('.modal-body').html('<label style=\'font-size:18px\'>'+result+'</label>');
                    }
                }
            });
        });
        
        
    });
    function onChangePermiss(user_id, worklist_id){
            var permiss = $('#permission-dd'+user_id).val(); 
            console.log(permiss);
             var warning = $('#modal-addwarning');
             $.ajax({
                url:'" . Url::to('/usfinding/worklist/update-usersite/') . "',
                method:'POST',
                type:'html',
                data:{
                    userid:user_id,
                    worklist_id:worklist_id,
                    permiss:permiss
                },
                success:function(result){
                    console.log(result);
                }
            });
        }

      function onDeleteUser(id){
            bootbox.confirm({
                title: \"ยืนยันข้อตกลง ?\",
                        message: \"<label>ท่านยืนยันที่จะลบข้อมูลนี้หรือไม่ ?</label>\",
                        buttons: {
                            cancel: {
                                label: '<i class=\"fa fa-times\"></i> Cancel'
                            },
                            confirm: {
                                label: '<i class=\"fa fa-check\"></i> Confirm'
                            }
                        },
                        callback: function (result) {
                            if(result){
                                onConfirmRemoveUser(id); 
                            }
                        }
            });
        }
        
        function onConfirmRemoveUser(id){
            var worklist_id = '$worklist_id';
            $.ajax({
                url:'" . Url::to('/usfinding/worklist/remove-user/') . "',
                method:'POST',
                data:{
                    user_id:id,
                    worklist_id:worklist_id
                },
                type:'HTML',
                success : function(result){
                    if(result=='success'){
                        showUser(worklist_id);
                    }else{
                        warning.modal();
                        warning.find('.modal-body').html('<label style=\'font-size:18px\'>'+result+'</label>');
                    }
                }
            });
        }
         function showUser(worklist_id){
        var divshow = $('#show-worklist');
         //divshow.html('<div style=\'text-align:center;\'><i class=\"fa fa-spinner fa-pulse fa-fw fa-3x\"></i></div>');
        $.ajax({
            url:'" . Url::to('/usfinding/worklist/user-worklist/') . "',
            method:'POST',
            type:'html',
            data:{
                workllist_id:worklist_id
            },
            success:function(result){
                divshow.html(result);
            }
        });
    }
    ");
?>



