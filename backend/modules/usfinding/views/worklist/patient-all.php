<?php

use kartik\widgets\Select2;
use yii\web\JsExpression;
use kartik\grid\GridView;
use yii\helpers\Url;
?>
<style>
    .checkbox {
        padding-left: 20px; }
    .checkbox label {
        display: inline-block;
        position: relative;
        padding-left: 5px; }
    .checkbox label::before {
        content: "";
        display: inline-block;
        position: absolute;
        width: 25px;
        height: 25px;
        left: 0;
        margin-left: -25px;
        border: 1px solid #cccccc;
        border-radius: 3px;
        background-color: #fff;
        -webkit-transition: border 0.15s ease-in-out, color 0.15s ease-in-out;
        -o-transition: border 0.15s ease-in-out, color 0.15s ease-in-out;
        transition: border 0.15s ease-in-out, color 0.15s ease-in-out; }
    .checkbox label::after {
        display: inline-block;
        position: absolute;
        width: 25px;
        height: 25px;
        left: 0;
        top: 0;
        margin-left: -27px;
        padding-left: 3px;
        padding-top: 5px;
        font-size: 11px;
        color: #555555; }
    .checkbox input[type="checkbox"] {
        opacity: 0; }
    .checkbox input[type="checkbox"]:focus + label::before {
        outline: thin dotted;
        outline: 5px auto -webkit-focus-ring-color;
        outline-offset: -2px; }
    .checkbox input[type="checkbox"]:checked + label::after {
        font-family: 'FontAwesome';
        content: "\f00c"; }
    .checkbox input[type="checkbox"]:disabled + label {
        opacity: 0.65; }
    .checkbox input[type="checkbox"]:disabled + label::before {
        background-color: #eeeeee;
        cursor: not-allowed; }
    .checkbox.checkbox-circle label::before {
        border-radius: 50%; }
    .checkbox.checkbox-inline {
        margin-top: 0; }

    .checkbox-primary input[type="checkbox"]:checked + label::before {
        background-color: #428bca;
        border-color: #428bca; }
    .checkbox-primary input[type="checkbox"]:checked + label::after {
        color: #fff; }

    .checkbox-danger input[type="checkbox"]:checked + label::before {
        background-color: #d9534f;
        border-color: #d9534f; }
    .checkbox-danger input[type="checkbox"]:checked + label::after {
        color: #fff; }

    .checkbox-info input[type="checkbox"]:checked + label::before {
        background-color: #5bc0de;
        border-color: #5bc0de; }
    .checkbox-info input[type="checkbox"]:checked + label::after {
        color: #fff; }

    .checkbox-warning input[type="checkbox"]:checked + label::before {
        background-color: #f0ad4e;
        border-color: #f0ad4e; }
    .checkbox-warning input[type="checkbox"]:checked + label::after {
        color: #fff; }

    .checkbox-success input[type="checkbox"]:checked + label::before {
        background-color: #5cb85c;
        border-color: #5cb85c; }
    .checkbox-success input[type="checkbox"]:checked + label::after {
        color: #fff; }

</style>
<div class="modal-header" style="border-radius:5px 5px 0px 0px ;background: #00A21E;color:#fff;">
    <button type="button" class="close" onclick="onReload()" data-dismiss="modal">&times;</button>
    <h4 class="modal-title"><strong>ผู้ป่วยที่ลงทะเบียนทั้งหมดของหน่วยงาน</strong></h4>
</div>
<div class="modal-body"  >
    <label style="font-size:20px;"><?= $sitename ?></label>
    <?php
    //\appxq\sdii\utils\VarDumper::dump($provider);
    echo GridView::widget([
        'dataProvider' => $provider,
        'id' => 'sites-report',
        'panel' => [
            'type' => \Yii::$app->request->get('action') ? Gridview::TYPE_SUCCESS : Gridview::TYPE_SUCCESS,
        ],
        'columns' => [
                [
                'header' => 'เลือกผู้ป่วย',
                'attribute' => 'checked',
                'format' => 'raw',
                'value' => function ($model) {
                    $checked = $model['checked'] >0 ? 'checked' : '';
                    $ptcode = $model['ptcodefull'];
                    $input = '
                            <div  class="checkbox checkbox-success">
                                <input onchange="onChecked(this)" data-val="' . $model['ptid'] . '" id="checkbox3' . $ptcode . '" type="checkbox" name="select[]" '.$checked.'>
                                <label for="checkbox3' . $ptcode . '">
                            </div>
                    
                    ';
                    return $input;
                },
                'headerOptions' => ['style' => 'text-align: center;'],
                'contentOptions' => ['style' => 'width:5%;text-align: center;'],
            ],
                [
                'header' => 'รหัสผู้ป่วย',
                'attribute' => 'ptcodefull',
                'value' => function ($model) {
                    return ($model['ptcodefull']);
                },
                'headerOptions' => ['style' => 'text-align: center;'],
                'contentOptions' => ['style' => 'width:20%;text-align: left;'],
            ],
                [
                'header' => 'ชื่อ-สกุล',
                'value' => function ($model) {
                    return $model['title'] . $model['name'] . ' ' . $model['surname'];
                },
                'headerOptions' => ['style' => 'text-align: center;'],
                'contentOptions' => ['style' => 'width:20%;text-align: left;'],
            ], [
                'header' => 'เพศ',
                'value' => function ($model) {
                    return $model['gender'] == 1 ? 'ชาย' : 'หญิง';
                },
                'headerOptions' => ['style' => 'text-align: center;'],
                'contentOptions' => ['style' => 'width:7%;text-align: left;'],
            ], [
                'header' => 'อายุ',
                'value' => function ($model) {
                    return $model['age'];
                },
                'headerOptions' => ['style' => 'text-align: center;'],
                'contentOptions' => ['style' => 'width:7%;text-align: left;'],
            ],
                [
                'format' => 'raw',
                'header' => 'ICF',
                'attribute' => 'icf',
                'value' => function ($model) {
                    return $model['icf_upload1'] == '0' || $model['icf_upload1'] == '' ? "<center><i class='glyphicon glyphicon-remove-circle' aria-hidden='true' style='color:red;font-size:16px;'></i></center>" : "<center><i class='glyphicon glyphicon-ok-circle' aria-hidden='true' style='color:green;font-size:16px;'></i></center>";
                },
                'headerOptions' => ['style' => 'text-align: center;'],
                'contentOptions' => ['style' => 'width:10%;text-align: center;'],
            ], [
                'format' => 'raw',
                'header' => 'ภาพบัตร',
                'attribute' => 'icf_upload2',
                'value' => function ($model) {
                    return $model['icf_upload2'] == '' ? "<center><i class='glyphicon glyphicon-remove-circle' aria-hidden='true' style='color:red;font-size:16px;'></i></center>" : "<center><i class='glyphicon glyphicon-ok-circle' aria-hidden='true' style='color:green;font-size:16px;'></i></center>";
                    ;
                },
                'headerOptions' => ['style' => 'text-align: center;'],
                'contentOptions' => ['style' => 'width:10%;text-align: center;'],
            ], [
                'format' => 'raw',
                'header' => 'CCA-01',
                'attribute' => 'cca01',
                'value' => function ($model) {
                    return $model['cca01'] == '0' || $model['cca01'] == '' ? "<center><i class='glyphicon glyphicon-remove-circle' aria-hidden='true' style='color:red;font-size:16px;'></i></center>" : "<center><i class='glyphicon glyphicon-ok-circle' aria-hidden='true' style='color:green;font-size:16px;'></i></center>";
                    ;
                },
                'headerOptions' => ['style' => 'text-align: center;'],
                'contentOptions' => ['style' => 'width:10%;text-align: center;'],
            ],
        ],
        'pjax' => true
    ]);
    ?>

</div>
<div class="modal-footer">
    <button type="button" class="btn btn-success" onclick="onReload()" data-dismiss="modal">Close And Reload</button>
</div>


<?php
$this->registerJs("
    function onChecked(checkbox){
        var checked = $(checkbox).is(':checked');
        var ptid = $(checkbox).attr('data-val');
        
        if(checked===true){
            addPatient(ptid);
        }else{
            removePatient(ptid);
        }
    }
    
    function addPatient(ptid){
        var worklist_id = '$worklist_id';
        var warning = $('#modal-warning-all');
        $.ajax({
                url:'" . Url::to('/usfinding/worklist/add-patient/') . "',
                method:'POST',
                type:'html',
                data:{
                    ptid:ptid,
                    worklist_id:worklist_id
                },
                success:function(result){
                    if(result=='success'){
                        console.log('Add Success');
                    }else{
                        warning.modal();
                        warning.find('.modal-body').html('<label style=\'font-size:18px\'>'+result+'</label>');
                    }
                }
            });
    }
    
    function removePatient(ptid){
        var worklist_id = '$worklist_id';
        var warning = $('#modal-warning-all');
        $.ajax({
                url:'" . Url::to('/usfinding/worklist/remove-patient/') . "',
                method:'POST',
                data:{
                    ptid:ptid,
                    worklist_id:worklist_id
                },
                type:'HTML',
                success : function(result){
                    
                    if(result=='success'){
                        console.log(result);
                    }else{
                        warning.modal();
                        warning.find('.modal-body').html('<label style=\'font-size:18px\'>'+result+'</label>');
                    }
                }
            });
    }
    function onReload(){
            var worklist_id = '$worklist_id';
            showPatients(worklist_id);
    }
    $('#modal-patient-all').on('hidden.bs.modal', function (e) {
        var worklist_id = '$worklist_id';
        showPatients(worklist_id);
    })

");
?>