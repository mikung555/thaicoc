<?php

use yii\widgets\ActiveForm;
use kartik\widgets\Select2;
use yii\web\JsExpression;
use kartik\grid\GridView;
use yii\helpers\Url;
use \frontend\modules\api\v1\classes\MainQuery;
use yii\helpers\Html;
?>

<div id="input-patient">
    <div class="clearfix"></div>
    <div class="col-md-12">
        <label style="padding-top: 5px;font-size:16px;" class="section-text" id="label-select">เพิ่มผู้ป่วยเข้า Worklist :</label>
    </div>
    <div class="col-md-4">
        <?php
        echo Select2::widget([
            'name' => 'patient',
            'id' => 'patient',
            'options' => ['placeholder' => 'ใส่รหัส หรือ ชื่อ-นามสกุล'],
            'pluginOptions' => [
                'allowClear' => true,
                'minimumInputLength' => 3, //ต้องพิมพ์อย่างน้อย 3 อักษร ajax จึงจะทำงาน
                'ajax' => [
                    'url' => '/usfinding/worklist/get-patients',
                    'dataType' => 'json', //รูปแบบการอ่านคือ json
                    'data' => new JsExpression('function(params) { 
                            return {q:params.term}; 
                            }
                         '),
                ],
                'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                'templateResult' => new JsExpression('function(city) { return city.text; }'),
                'templateSelection' => new JsExpression('function (city) { return city.text; }'),
            ]
        ]);
        ?>
    </div>

    <div class="col-md-6" >
        <button id="btn-addpatient" class="btn btn-primary" style="font-size:16px;"><i class="glyphicon glyphicon-save"></i> <strong>Submit</strong></button>
        <button id="btn-patient-all" class="btn btn-primary" style="font-size:16px;"><i class="glyphicon glyphicon-th-list"></i> <strong>เพิ่มผู้ป่วยมากกว่า 1 ราย</strong></button>
        <button id="btn-patient-all" class="btn btn-success" style="font-size:16px;"><strong><i class="glyphicon glyphicon-import"></i> นำเข้ารายการผู้ป่วยจาก Excel</strong></button>
    </div>

    <div class="col-md-3" >
        <div id="message-alert"></div>
    </div>
    <div class="clear-fix"></div>
    <br/>
</div>

<br/>
<br/>
<br/>
<div class="panel-success" >
    <div class="panel-heading " style="border-radius: 10px 10px 0 0;">
        <table>
            <tr>
                <td rowspan="2">
                    <i class="fa fa-users" style="font-size:40px;padding-right:15px;"></i>
                </td>
                <td>
                    <label style="font-size:22px"><strong>Patients</strong></label><br/>
                </td>
            </tr>
        </table>
    </div>
</div>
<div id="modal-addwarning" class="modal fade" role="dialog" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="background: green;color:#fff;">
                <h4 class="modal-title"><strong>Warning !</strong></h4>
            </div>
            <div class="modal-body" style="padding-left: 20px;">

            </div>

            <div class="modal-footer">
                <button type="button" id="btn-cancel" class="btn btn-primary" data-dismiss="modal"><strong>OK</strong></button>
            </div>

        </div>
    </div>
</div>
<?php
//\appxq\sdii\utils\VarDumper::dump($provider);
echo GridView::widget([
    'dataProvider' => $provider,
    'id' => 'sites-report',
    'panel' => [
        'type' => \Yii::$app->request->get('action') ? Gridview::TYPE_SUCCESS : Gridview::TYPE_SUCCESS,
    ],
    'columns' => [
//                            [
//                            'class' => 'yii\grid\SerialColumn',
//                            'headerOptions' => ['style' => 'text-align: center;'],
//                            'contentOptions' => ['style' => 'width:5px;text-align: center;'],
//                        ],
            [
            'header' => 'ลำดับที่',
            'class' => 'yii\grid\SerialColumn',
            'headerOptions' => ['style' => 'text-align: center;'],
            'contentOptions' => ['style' => 'width:4%;text-align: center;'],
        ],
            [
            'header' => 'เลขที่ Worklist',
            'attribute' => 'worklist_id',
            'value' => function ($model) {
                return $model['worklist_id'];
            },
            'headerOptions' => ['style' => 'text-align: center;'],
            'contentOptions' => ['style' => 'width:15%;text-align: center;'],
        ],
            [
            'header' => 'รหัสผู่ป่วย',
            'attribute' => 'ptcodefull',
            'value' => function ($model) {
                return ($model['ptcodefull']);
            },
            'headerOptions' => ['style' => 'text-align: center;'],
            'contentOptions' => ['style' => 'width:30px;text-align: right;'],
        ], [
            'header' => 'ชื่อ-นามสกุล',
            'value' => function ($model) {
                return ($model['title'] . $model['name'] . ' ' . $model['surname']);
            },
            'headerOptions' => ['style' => 'text-align: center;'],
            'contentOptions' => ['style' => 'width:30px;text-align: right;'],
        ],
            [
            'header' => 'User Create',
            'attribute' => 'create_user',
            'value' => function ($model) {
                return MainQuery::GetUserProfileById($model['create_user']);
            },
            'headerOptions' => ['style' => 'text-align: center;'],
            'contentOptions' => ['style' => 'width:30px;text-align: right;'],
        ], [
            'format' => 'raw',
            'header' => '#',
            'value' => function ($model) {

                $btn_delete = Html::button("ลบ", ['onclick' => 'onDeletePatient("' . $model['ptid'] . '")', 'class' => 'btn btn-danger']);
                return $btn_edit . $btn_delete;
            },
            'headerOptions' => ['style' => 'text-align: center;'],
            'contentOptions' => ['style' => 'width:30px;text-align: center;'],
        ]
    ],
    'pjax' => false
]);

$this->registerJs("
    $(function(){
    $('#btn-patient-all').on('click',function(){
            var modal_patient = $('#modal-patient-all');
            var worklist_id = '$worklist_id';
            modal_patient.modal();
            $.ajax({
                url:'" . Url::to('/usfinding/worklist/patient-all/') . "',
                method:'post',
                type:'HTML',
                data:{
                    worklist_id:worklist_id
                },
                success:function(result){
                    $('#modal-patient-all .modal-dialog .modal-content').html(result);
                }
            });
        });
        
        $('#btn-addpatient').on('click', function(){
            var ptid = $('#patient').val();
            var worklist_id = '$worklist_id';
                var warning = $('#modal-addwarning');
             $.ajax({
                url:'" . Url::to('/usfinding/worklist/add-patient/') . "',
                method:'POST',
                type:'html',
                data:{
                    ptid:ptid,
                    worklist_id:worklist_id
                },
                success:function(result){
                    if(result=='success'){
                        showPatients(worklist_id);
                    }else{
                        warning.modal();
                        warning.find('.modal-body').html('<label style=\'font-size:18px\'>'+result+'</label>');
                    }
                }
            });
        });
        
        
    });
    function onDeletePatient(id){
            bootbox.confirm({
                title: \"ยืนยันข้อตกลง ?\",
                        message: \"<label>ท่านยืนยันที่จะลบข้อมูลนี้หรือไม่ ?</label>\",
                        buttons: {
                            cancel: {
                                label: '<i class=\"fa fa-times\"></i> Cancel'
                            },
                            confirm: {
                                label: '<i class=\"fa fa-check\"></i> Confirm'
                            }
                        },
                        callback: function (result) {
                            if(result){
                                onConfirmDeletePatient(id); 
                            }
                        }
            });
        }
        
        function onConfirmDeletePatient(id){
            var worklist_id = '$worklist_id';
            $.ajax({
                url:'" . Url::to('/usfinding/worklist/remove-patient/') . "',
                method:'POST',
                data:{
                    ptid:id,
                    worklist_id:worklist_id
                },
                type:'HTML',
                success : function(result){
                    
                    if(result=='success'){
                        showPatients(worklist_id);
                    }else{
                        warning.modal();
                        warning.find('.modal-body').html('<label style=\'font-size:18px\'>'+result+'</label>');
                    }
                }
            });
        }
 function showPatients(worklist_id){
        var divshow = $('#show-worklist');
         //divshow.html('<div style=\'text-align:center;\' class=\'col-md-12\'><i class=\"fa fa-spinner fa-pulse fa-fw fa-3x\"></i></div>');
        $.ajax({
            url:'" . Url::to('/usfinding/worklist/patient-worklist/') . "',
            method:'POST',
            type:'html',
            data:{
                worklist_id:worklist_id
            },
            success:function(result){
                divshow.html(result);
            }
        });
    }
     
    ");
?>

