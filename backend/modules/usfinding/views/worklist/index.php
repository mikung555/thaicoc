<?php

use yii\widgets\ActiveForm;
use kartik\widgets\Select2;
use yii\web\JsExpression;
use kartik\grid\GridView;
use yii\helpers\Url;
use \frontend\modules\api\v1\classes\MainQuery;
use yii\helpers\Html;
?>

<?php
$this->registerJs("
    var worklist_id = '';
    $(function(){
    
        $.ajax({
            url:'" . Url::to('/usfinding/worklist/check-worklist/') . "',
            method:'POST',
            type:'HTML',
            data:{
            
            },
            success:function(result){
                if(result==true){
                    showPatients();
                }else{
                    bootbox.confirm({
                        title: \"ยืนยันข้อตกลง ?\",
                        message: \"<div class = 'alert alert-danger h3'>หน่วยงานของท่านยังไม่มีข้อมูล Worklist ท่านต้องการสร้างใหม่หรือไม่?</div>\",
                        buttons: {
                            cancel: {
                                label: '<i class=\"fa fa-times\"></i> Cancel'
                            },
                            confirm: {
                                label: '<i class=\"fa fa-check\"></i> Confirm'
                            }
                        },
                        callback: function (result) {
                            if(result){
                                $('#modal-create-worklist').modal();
                               
                            }else{
                                window.history.back();
                            }
                        }
                    });
                }
            }
        });
        $('#btn-cancel').on('click',function(){
            window.history.back();
        });
        $('#btn-create').on('click', function(){
            $('#sub-tab-three').removeClass('active');
            $('#sub-tab-one').addClass('active');
            var divModal = $('#modal-create-worklist .modal-dialog .modal-content .modal-body');
            divModal.empty();
             divModal.html('<label>รอสักครู่กำลังสร้าง Worklist ใหม่ ...</label>');
             var startdate = $('#startdate').val();
             var enddate = $('#enddate').val();
            $.ajax({
                url:'" . Url::to('/usfinding/worklist/create-worklist/') . "',
                method:'POST',
                type:'html',
                data:{
                    startdate:startdate,
                    enddate:enddate
                },
                success:function(result){
                    worklist_id = result;
                    $('#modal-create-worklist').modal('hide');
                    showPatients(worklist_id);
                }
            });
        });
        
        $('#sub-tab3').on('click', function(){
            $('#modal-create-worklist').modal();
        });
    });
    var tab_curr = 'tab1';
    
    $('#sub-tab1').click(function(){
        tab_curr ='tab1';
        showPatients(worklist_id);
    });
    $('#sub-tab2').click(function(){
        tab_curr ='tab2';
        showUser(worklist_id);
    });
    
    function showPatients(worklist_id){
        var divshow = $('#show-worklist');
         divshow.html('<div style=\'text-align:center;\'><i class=\"fa fa-spinner fa-pulse fa-fw fa-3x\"></i></div>');
        $.ajax({
            url:'" . Url::to('/usfinding/worklist/patient-worklist/') . "',
            method:'POST',
            type:'html',
            data:{
                workllist_id:worklist_id
            },
            success:function(result){
                divshow.html(result);
            }
        });
    }
    
    function showUser(worklist_id){
        var divshow = $('#show-worklist');
         divshow.html('<div style=\'text-align:center;\'><i class=\"fa fa-spinner fa-pulse fa-fw fa-3x\"></i></div>');
        $.ajax({
            url:'" . Url::to('/usfinding/worklist/user-worklist/') . "',
            method:'POST',
            type:'html',
            data:{
                workllist_id:worklist_id
            },
            success:function(result){
                divshow.html(result);
            }
        });
    }
    

");
?>


<style>
    .panel-success2 .panel-heading2{
        background: #00A21E;
        color: #fff;
        padding-top:20px;
        padding-bottom:20px;
        padding-left:20px;
        padding-right:20px;
    }
    .nav nav-pills .nav-justified .active{
        background: #00AA3B;
    }
    table tr th{
        background: #00A21E;
    }
</style>

<ul class="nav nav-pills nav-justified">
    <li id="tab-one" style="font-size:18px;" ><a  href="<?= Url::to(['/usfinding/']) ?>" id="tab1"><i class="fa fa-line-chart fa-lg"></i> US Finding</a></li>
    <li id="tab-three" style="font-size:18px;" class="active"><a data-toggle="tab" href="#modules" id="tab3"><i class="glyphicon glyphicon-tasks fa-lg"></i> Worklist</a></li>
    <li id="tab-two" style="font-size:18px;" ><a  href="<?= Url::to(['/usfinding/monitoring']) ?>" id="tab2"><i class="fa fa-desktop fa-lg"></i> Monitoring System</a></li>

</ul>
<br/>
<div class="panel-success2" >
    <div class="panel-heading2 " style="border-radius: 10px 10px 0 0;">
        <table>
            <tr>
                <td rowspan="2">
                    <i class="fa fa-users" style="font-size:70px;padding-right:15px;"></i>
                </td>
                <td>
                    <label style="font-size:30px"><strong>Worklist</strong><br/>
                        หน่วยงาน <?= $resWork['name'] ?>
                    </label>
                </td>
            </tr>
        </table>
    </div>
</div>
<br/>
<!-- Modal create worklist -->
<div id="modal-create-worklist" class="modal fade" role="dialog" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="background: green;color:#fff;">
                <h4 class="modal-title"><strong>Create New Worklist</strong></h4>
            </div>
            <div class="modal-body" style="padding-left: 20px;">
                <div class="col-md-12">
                    <label class="control-label">เริ่มวันที่ :</label>
                    <div class="input-group date">
                        <span class="input-group-addon kv-date-calendar" title="Select date"><i class="glyphicon glyphicon-calendar"></i></span>
                        <input type="date" id="startdate" class="form-control" name="startdate"  data-datepicker-type="2" data-krajee-datepicker="datepicker_b6ea203c">
                    </div>

                </div>
                <br/>
                <div class="col-md-12">
                    <label class="control-label">ถึงวันที่ :</label>
                    <div class="input-group date">
                        <span class="input-group-addon kv-date-calendar" title="Select date"><i class="glyphicon glyphicon-calendar"></i></span>
                        <input type="date" id="endstart" class="form-control" name="enddate"  data-datepicker-type="2" data-krajee-datepicker="datepicker_b6ea203c">
                    </div> 
                </div>
                <br/><br/>
            </div>

            <div class="modal-footer">
                <button type="button" id="btn-cancel" class="btn btn-warning" data-dismiss="modal"><strong>Cancel</strong></button>
                <button type="button" id="btn-create" class="btn btn-primary" data-dismiss="modal"><strong>Create</strong></button>
            </div>

        </div>
    </div>
</div>
<ul class="nav nav-tabs">
    <li id="sub-tab-one" style="font-size:18px;" class="active"><a data-toggle="tab" href="#" id="sub-tab1"><i class="fa fa-user fa-lg"></i> ผู้ป่วย</a></li>
    <li id="sub-tab-two" style="font-size:18px;" ><a data-toggle="tab" href="#" id="sub-tab2"><i class="fa fa-user-md fa-lg"></i> เจ้าหน้าที่</a></li>
    <li id="sub-tab-three" style="font-size:18px;" ><a data-toggle="tab" href="#" id="sub-tab3"><i class="fa fa-edit fa-lg"></i> สร้าง Worklist ใหม่</a></li>
</ul>
<br/>
<div class="col-md-6">
    <label class="control-label">เลขที่ Worklist :</label>
    <?php
    echo Select2::widget([
        'name' => 'worklistno',
        'id' => 'worklistno',
        'options' => ['placeholder' => $resWork['fulltext'], 'data-id' => $resWork['id']],
        'pluginOptions' => [
            'allowClear' => true,
            'minimumInputLength' => 0, //ต้องพิมพ์อย่างน้อย 3 อักษร ajax จึงจะทำงาน
            'ajax' => [
                'url' => '/usfinding/worklist/get-worklist-number',
                'dataType' => 'json', //รูปแบบการอ่านคือ json
                'data' => new JsExpression('function(params) { 
                            return {q:params.term}; 
                            }
                         '),
            ],
            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
            'templateResult' => new JsExpression('function(city) { return city.text; }'),
            'templateSelection' => new JsExpression('function (city) { return city.text; }'),
        ]
    ]);
    ?>
</div>
<div class="col-md-2" style="padding-top:25px;text-align: left;">
    <button id="btn-worklist-all" class="btn btn-info" style="font-size:16px;"><i class="fa fa-list-alt"></i> <strong>Worklist ทั้งหมด</strong></button>
</div>
<div class="clearfix"></div>

<div class="col-md-3">
    <label class="control-label">เริ่มวันที่ :</label>
    <div class="input-group date">
        <span class="input-group-addon kv-date-calendar" title="Select date"><i class="glyphicon glyphicon-calendar"></i></span>
        <input type="date" id="start-date" class="form-control" value="<?= $resWork['start_date'] ?>" name="start-date"  data-datepicker-type="2" data-krajee-datepicker="datepicker_b6ea203c">
        <a href="worklist-all.php"></a>
    </div>

</div>


<div class="col-md-3">
    <label class="control-label">ถึงวันที่ :</label>
    <div class="input-group date">
        <span class="input-group-addon kv-date-calendar" title="Select date"><i class="glyphicon glyphicon-calendar"></i></span>
        <input type="date" id="end-date" class="form-control" value="<?= $resWork['end_date'] ?>" name="nd-start"  data-datepicker-type="2" data-krajee-datepicker="datepicker_b6ea203c">
    </div> 
</div>
<div class="col-md-4" style="padding-top:25px;text-align: left;">
    <button id="btn-worklist-all" class="btn btn-success" style="font-size:16px;"><i class="fa fa-edit"></i> <strong>บันทึก</strong></button>
    <code>*กดบันทึก เมื่อต้องการแก้ไขวันที่</code>
</div>
<div class="clearfix"></div>
<hr/>

<div id="show-worklist"></div>
<!-- Modal choose personal-->
<div id="modal-worklist-all" class="modal fade" role="dialog" >
    <div class="modal-dialog" style="width:60%">
        <div class="modal-content">
            
        </div>
    </div>
</div>
<div id="modal-patient-all" class="modal fade" role="dialog" >
    <div class="modal-dialog" style="width:70%">
        <div class="modal-content">
            
        </div>
    </div>
</div>

<div id="modal-warning-all" class="modal fade " role="dialog" data-backdrop="static">
    <div class="modal-dialog" >
        <div class="modal-content">
            <div class="modal-header" style="background: green;color:#fff;">
                <h4 class="modal-title"><strong>Warning !</strong></h4>
            </div>
            <div class="modal-body" style="padding-left: 20px;">

            </div>

            <div class="modal-footer">
                <button type="button" id="btn-cancel" class="btn btn-primary" data-dismiss="modal"><strong>OK</strong></button>
            </div>

        </div>
    </div>
</div>
<?php
$this->registerJs("
    $(function(){
        $('#btn-worklist-all').on('click',function(){
            var modal_worklist = $('#modal-worklist-all');
            modal_worklist.modal();
            $.ajax({
                url:'".Url::to('/usfinding/worklist/worklist-all/')."',
                method:'post',
                type:'HTML',
                data:{
                    
                },
                success:function(result){
                    $('#modal-worklist-all .modal-dialog .modal-content').html(result);
                }
            });
        });
        
        
        
        $('#worklistno').on('change',function(e){
            e.preventDefault();
            var divshow = $('#show-worklist');
            divshow.html('<div style=\'text-align:center;\' class=\'col-md-12\'><i class=\"fa fa-spinner fa-pulse fa-fw fa-3x\"></i></div>');
            var start_date = $('#start-date');
            var end_date = $('#end-date');
            var worklist_id = $('#worklistno').val();
            
            $.ajax({
                   url:'" . Url::to('/usfinding/worklist/worklist-data/') . "',
                   method:'POST',
                   type:'json',
                   data:{
                       worklist_id:worklist_id
                   },
                   success:function(result){
                       var arr_wl = JSON.parse(result);
                       //console.log(end_date);
                       end_date.val(arr_wl.end_date);
                       start_date.val(arr_wl.start_date);
                       
                   }
            });
               
            if(tab_curr=='tab1'){
                $.ajax({
                   url:'" . Url::to('/usfinding/worklist/patient-worklist/') . "',
                   method:'POST',
                   type:'html',
                   data:{
                       start_date:start_date.val(),
                       end_date:end_date.val(),
                       worklist_id:worklist_id
                   },
                   success:function(result){
                           divshow.html(result);

                   }
               });
            }else if(tab_curr=='tab2'){
                $.ajax({
                   url:'" . Url::to('/usfinding/worklist/user-worklist/') . "',
                   method:'POST',
                   type:'html',
                   data:{
                       start_date:start_date,
                       end_date:end_date,
                       worklist_id:worklist_id
                   },
                   success:function(result){
                           divshow.html(result);

                   }
               });
            }
        });
        
        $('#btn-addpatient').on('click', function(){
            var ptid = $('#patient').val();
            var worklist_id = $('#worklistno').val();
                var warning = $('#modal-addwarning');
             $.ajax({
                url:'" . Url::to('/usfinding/worklist/add-patient/') . "',
                method:'POST',
                type:'html',
                data:{
                    ptid:ptid,
                    worklist_id:worklist_id
                },
                success:function(result){
                    if(result=='success'){
                        showPatients(worklist_id);
                    }else{
                        warning.modal();
                        warning.find('.modal-body').html('<label style=\'font-size:18px\'>'+result+'</label>');
                    }
                }
            });
        });
    });
    

");
?>
