<?php
use kartik\widgets\Select2;
use yii\web\JsExpression;
use kartik\grid\GridView;
?>
<div class="modal-header" style="border-radius:5px 5px 0px 0px ;background: #00A21E;color:#fff;">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <h4 class="modal-title"><strong>Worklist ทั้งหมดของหน่วยงาน</strong></h4>
</div>
<div class="modal-body" >
    <label style="font-size:20px;"><?= $sitename ?></label>
        <?php
    //\appxq\sdii\utils\VarDumper::dump($provider);
    echo GridView::widget([
        'dataProvider' => $provider,
        'id' => 'sites-report',
        'panel' => [
            'type' => \Yii::$app->request->get('action') ? Gridview::TYPE_SUCCESS : Gridview::TYPE_SUCCESS,
        ],
        'columns' => [
                
                [
                'header' => 'Worklist ID.',
                'attribute' => 'id',
                'value' => function ($model) {
                    return ($model['id']);
                },
                'headerOptions' => ['style' => 'text-align: center;'],
                'contentOptions' => ['style' => 'width:5%;text-align: left;'],
            ],
                [
                'header' => 'Worklist Name',
                'value' => function ($model) {
                    return $model['title'] ;
                },
                'headerOptions' => ['style' => 'text-align: center;'],
                'contentOptions' => ['style' => 'width:25%;text-align: left;'],
            ], [
                'header' => ' สร้างโดย/เจ้าหน้าที่ ',
                'value' => function ($model) {
                    return $model['user_name'];
                },
                'headerOptions' => ['style' => 'text-align: center;'],
                'contentOptions' => ['style' => 'width:15%;text-align: left;'],
            ], [
                'header' => 'ถูกสร้างเมื่อ',
                'value' => function ($model) {
                    return $model['create_date'];
                },
                'headerOptions' => ['style' => 'text-align: center;'],
                'contentOptions' => ['style' => 'width:15%;text-align: left;'],
            ],
        ],
        'pjax' => true
    ]);
    ?>
    
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
</div>

<?php
$this->registerJs("
    

");
?>