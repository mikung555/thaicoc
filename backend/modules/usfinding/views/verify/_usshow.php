<?php
use Yii;
use backend\modules\usfinding\classes\QueryHospital;
use backend\modules\usfinding\classes\QueryRegister;
use backend\modules\usfinding\classes\QueryCCA02;
use backend\modules\usfinding\classes\QueryUser;

$request = Yii::$app->request;

$hospital = QueryHospital::getHospitalDet($request->get('hsitecode'));
?>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title" id="myModalLabel"><b><?php echo $hospital['name']; ?></b></h4>
</div>
<div class="modal-body">
    <div class="panel panel-primary" style="width: 100%;">
        <div class="panel-heading"><h3><b>แสดงโรงพยาบาลที่เลือก</b></h3></div>
        <div class="panel-body">
            <p>รายละเอียด</p>
        </div>

        <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th style="text-align: right"><h4><b>โรงพยาบาล:</b></h4></th>
                        <th style="text-align: left"><h4><b><?php echo $hospital['name']; ?></b></h4></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td style="text-align: right"><b>รหัส 5 หลัก:</b></td>
                        <td style="text-align: left"><?php echo $hospital['hcode']; ?></td>
                    </tr>
                    <tr>
                        <td style="text-align: right"><b>จังหวัด:</b></td>
                        <td style="text-align: left"><?php echo $hospital['province']; ?></td>
                    </tr>
                    <tr>
                        <td style="text-align: right"><b>อำเภอ:</b></td>
                        <td style="text-align: left"><?php echo $hospital['amphur']; ?></td>
                    </tr>
                    <tr>
                        <td style="text-align: right"><b>ตำบล:</b></td>
                        <td style="text-align: left"><?php echo $hospital['tambon']; ?></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <?php 
    $reg = QueryRegister::getDetByID($request->get('ptid'));
    echo $this->render('_dataregister',[
        'hospital' => $hospital,
        'reg' => $reg,
    ]);
    ?>
    <?php 
    $cca02 = QueryCCA02::getDetByID($request->get('id'));
    $ucca02 = QueryUser::getDetByID($cca02['user_update']); 
    echo $this->render('_dataultrasound',[
        'hospital' => $hospital,
        'reg' => $reg,
        'cca02' => $cca02,
        'ucca02' => $ucca02,
    ]);
    ?>
    <div class="panel panel-primary" style="width: 100%;">
        <div class="panel-heading"><h3><b>ภาพอัลตราซาวด์</b></h3></div>
        <p><div item-id="imgUsDet"></div></p>
        <div class="table-responsive">
            <div item-id="1448875228013015000"></div>
        </div>
    </div>
    
    
    <?php
$ptid=$request->get('ptid');
$id=$request->get('id');
$js = <<< JS

$.ajax({
    method: 'GET',
    url:'https://tools.cascap.in.th/api/us/imgjson02.php?ptid=$ptid&id=$id',
    dataType: 'JSON',
    success: function(result, textStatus) {
        console.log( "success" );
        var icount = 0;
	if(result!=null){
	    $.each(result, function (index, value) {
                console.log( "success"+value );
                icount++;
		$('div[item-id="1448875228013015000"]').append('<div class="col-md-12"><img class="img-responsive center" src="' + value + '"><br><br></div>');
	    });
	}
        console.log( "count"+icount );
        if( icount==0 ){
            $('div[item-id="imgUsDet"]').append('<p style="color:red;"><b>  ไม่มีการ Upload ภาพอัลตราซาวด์</b></p>');
        }else{
            $('div[item-id="imgUsDet"]').append('<p style="color:blue;"><b>  จำนวนภาพที่ของที่ Upload ขึ้นทั้งหมด '+icount+' ภาพ</b></p>');
        }
    }
});
        
JS;
$this->registerJs($js, \yii\web\View::POS_END);
    ?>
</div>
<?php
?>