<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use Yii;
use yii\helpers\Html;
use yii\helpers\Url;

$request = Yii::$app->request; 
//echo $request->getUrl();
//echo $request->baseUrl;
//echo $request->scriptFile;
//echo $request->queryString;
//echo $request->scriptUrl;
//echo $_SERVER['REDIRECT_URL'];
//echo $request->url;
//echo $request->pathInfo;

if( $request->pathInfo == 'usfinding/verify/cid'){
    $btnClass   = 'btn btn-info';
}else{
    $btnClass   = 'btn btn-default';
}
echo Html::a('<i class="glyphicon glyphicon-ok"></i>แก้ไขเลขบัตร'
    ,'/usfinding/verify/cid' 
    ,[
        'id'        => 'openUser'.$kuser,
        'data-url'  => '/cpay/y60/show-detail',
        'class'     => $btnClass,
    ]);

echo "&nbsp;";


if( $request->pathInfo == 'usfinding/verify/cca02-dup'){
    $btnClass   = 'btn btn-info';
}else{
    $btnClass   = 'btn btn-default';
}
echo Html::a('<i class="glyphicon glyphicon-wrench"></i>CCA-02ซ้ำ'
    ,'/usfinding/verify/cca02-dup' 
    ,[
        'id'        => 'openUser'.$kuser,
        'data-url'  => '/cpay/y60/show-detail',
        'class'     => $btnClass,
    ]);

echo "&nbsp;";

if( $request->pathInfo == 'usfinding/verify/us'){
    $btnClass   = 'btn btn-info';
}else{
    $btnClass   = 'btn btn-default';
}
echo Html::a('<i class="glyphicon glyphicon-wrench"></i>ลงข้อมูล ultrasound วันนี้'
    ,Url::to(['/usfinding/verify/us']) 
    ,[
        'id'        => 'usDCreate',
        'class'     => $btnClass,
    ]);

echo "&nbsp;";

if( $request->pathInfo == 'usfinding/verify/usdupdate'){
    $btnClass   = 'btn btn-info';
}else{
    $btnClass   = 'btn btn-default';
}
echo Html::a('<i class="glyphicon glyphicon-wrench"></i>แก้ไขข้อมูลวันนี้'
    , Url::to(['/usfinding/verify/usdupdate']) 
    ,[
        'id'        => 'usDUpdate',
        'class'     => $btnClass,
    ]);

echo "&nbsp;";

if( $request->pathInfo == 'usfinding/verify/pid-management'){
    $btnClass   = 'btn btn-info';
}else{
    $btnClass   = 'btn btn-default';
}
echo Html::a('<i class="glyphicon glyphicon-wrench"></i>ตรวจสอบข้อมูลตาม CID'
    , Url::to(['/usfinding/verify/pid-management']) 
    ,[
        'id'        => 'usPidManagement',
        'class'     => $btnClass,
    ]);

?>

