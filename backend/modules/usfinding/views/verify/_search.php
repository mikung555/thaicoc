<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use yii\helpers\Html;


//$form = ActiveForm::begin(['action' => 'forum-post/create', 'id' => 'forum_post', 'method' => 'post',]);
?>
<div class="container-fluid">
  <h1>
      ค้นหาตามเลข บัตรประจำตัวประชาชน
      <?php
            echo Html::a("Clear"
                ,['cid']
                ,['class' =>'btn btn-default btn-md','title'=>'Refresh page']
                );
      ?>
  </h1>
  <div class="row">
    <div class="col-xs-6 col-sm-6" style="background-color: #000\9;">
      ใส่เลขบัตรประจำตัวประชาชนเดิม:
      <?php
            echo Html::input('text'
                    , '_cidsearch', '', ['id'=>'_cidsearch','maxlength'=>13]);
      ?>
      <?php
            echo Html::submitButton('<span class="glyphicon glyphicon-search"> </span> Search'
                    , ['id'=>'btnSearch', 'class' =>'btn btn-info btn-sm','title'=>'ค้น']
                    );
      ?> 
    </div>
    <div class="col-xs-6 col-sm-2"></div>
    <!-- Add clearfix for only the required viewport -->
    <div class="clearfix visible-xs"></div>
    <div class="col-xs-6 col-sm-2"></div>
    <div class="col-xs-6 col-sm-2"></div>
  </div>
</div>
        <center>
            <button type="button" id="waiting" data-loading-text="กำลังประมวลผล  กรุณารอซักครู่..." class="btn btn-primary"></button>
        </center>  
<div id="table_resultsearch"></div>
<div id="table_newcid"></div>
