<?php


?>
    <div class="panel panel-primary" style="width: 100%;">
        <div class="panel-heading"><h4><b><?php echo $_det['title']; ?></b></h4></div>
        <div class="panel-body">
            <p><?php echo $_det['detail']; ?></p>
        </div>

        <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>ตัวแปร</th>
                        <th>ข้อมูลต้นแบบ</th>
                        <th>ข้อมูลขณะนี้</th>
                        <th>คลิกเพื่อเปลี่ยน</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if(count($_det['view'])>0){
                        foreach($_det['view'] as $kview => $vview){
                    ?>
                    <tr>
                        <td><?php echo $vview['var']; ?></td>
                        <td><?php echo $vview['mastervalue']; ?></td>
                        <td id="<?php echo 'currentvalue'.$_det['sys']['data_id'].$vview['var']; ?>"><?php echo $vview['currentvalue']; ?></td>
                        <td>
                            <?php
                            if( $vview['canchange']==FALSE ){
                                $canchange = 'disabled';
                                $class = 'btn btn-default';
                                $btnvalue = 'ไม่สามารถแก้ไข';
                                $setdisable = '1';
                            }else{
                                $canchange = 'canchange';
                                if( $vview['mastervalue'] !== $vview['currentvalue'] ){
                                    $class = 'btn btn-warning';
                                    $btnvalue = 'คลิกเพื่อแก้ไข';
                                    $setdisable = '0';
                                }else{
                                    $class = 'btn btn-primary';
                                    $btnvalue = 'ค่าตรงกันแล้ว';
                                    $setdisable = '1';
                                }
                            }
                            echo \yii\helpers\Html::button($btnvalue, [
                                'id' => 'btnEdit'.$_det['sys']['data_id'].$vview['var'],
                                'dataid' => $_det['sys']['data_id'],
                                'datavarname' => $vview['var'],
                                'dataoldvalue' => $vview['currentvalue'],
                                'datanewvalue' => $vview['mastervalue'],
                                'datatable' => $_det['sys']['datatable'],
                                'tagdiv' => $_arg['div'],
                                'setdisable' => $setdisable,
                                'btnidchange' => 'btnEdit'.$_det['sys']['data_id'].$vview['var'],
                                'currentvalue' => 'currentvalue'.$_det['sys']['data_id'].$vview['var'],
                                'class' => $class,
                                $canchange => $canchange,
                            ]);
                            ?>
                        </td>
                    </tr>
                    <?php
                        }
                    }
                    ?>
                    
                </tbody>
            </table>
        </div>
    </div>
<?php
?>