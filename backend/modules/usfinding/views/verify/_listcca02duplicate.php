<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
use yii\helpers\Html;

if(count($duplist)>0){
?>
<table class="kv-grid-table table table-bordered table-striped">
    <thead>
        <tr style="text-align:center">
            <th class="kv-align-center kv-align-middle" style="min-width:100px;text-align:right;">
                ลำดับที่
            </th>
            <th class="kv-align-center kv-align-middle" style="min-width:100px;text-align:left;">
                Site ID
            </th>
            <th class="kv-align-center kv-align-middle" style="min-width:100px;text-align:left;">
                PID
            </th>
            <th class="kv-align-center kv-align-middle" style="min-width:100px;text-align:left;">
                Exam Date
            </th>
            <th class="kv-align-center kv-align-middle" style="min-width:100px;text-align:left;">
                จำนวนที่ซ้ำ
            </th>
            <th class="kv-align-center kv-align-middle" style="min-width:100px;text-align:left;">
                การจัดการข้อมูล
            </th>
        </tr>
    </thead>
    <tbody>
    <?php
    $num=1;
    foreach ($duplist as $k => $v){
    ?>
        <tr style="text-align:right">
            <td class="kv-align-center kv-align-middle"
                style="text-align:right;">
                    <?php 
                        echo $num 
                    ?>
            </td>
            <td class="kv-align-center kv-align-middle"
                style="text-align:left;">
                <?php
                echo $duplist[$k]['hsitecode'];
                ?>
            </td>
            <td class="kv-align-center kv-align-middle"
                style="text-align:left;">
                <?php
                echo $duplist[$k]['hptcode'];
                ?>
            </td>
            <td class="kv-align-center kv-align-middle"
                style="text-align:left;"> 
                <?php
                $id='btnpopup'.$num;
//                echo Html::a($duplist[$k]['f2v1']
//                        , ''
//                        , ['id'=>$id,
//                            'data-url'=>'/usfinding/verify/cca02-visit',
//                            'hsitecode'=>$duplist[$k]['hsitecode'],
//                            'hptcode'=>$duplist[$k]['hptcode'],
//                            'f2v1'=>$duplist[$k]['f2v1']
//                        ]);
                //echo $duplist[$k]['f2v1'];
                
                echo Html::button($duplist[$k]['f2v1']
                        , ['id'=>$id,
                            'class'=>'btn btn-link btn-xs',
                            'data-url'=>'/usfinding/verify/cca02-visit',
                            'hsitecode'=>$duplist[$k]['hsitecode'],
                            'hptcode'=>$duplist[$k]['hptcode'],
                            'f2v1'=>$duplist[$k]['f2v1']
                        ]);
                ?>
                <a name="#<?php echo $id; ?>"></a>
            </td>
            <td class="kv-align-center kv-align-middle"
                style="text-align:left;">
                <?php
                echo $duplist[$k]['recs'];
                ?>
            </td>
            <td class="kv-align-center kv-align-middle"
                style="text-align:left;">
                <?php
                echo $duplist[$k][''];
                ?>
            </td>
        </tr>
    <?php
        $num++;
    }
    ?> 
    </tbody>
</table>
<?php
}