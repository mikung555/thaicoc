<?php


use backend\modules\usfinding\classes\PIDManagement;

?>
<div class="panel panel-default">
    <div class="panel-heading">แสดงข้อมูล ตามเลขบัตรประจำตัวประชาชน ของโรงพยาบาล: <?php echo $hsitecode; ?>: {hospital_name}</div>
    <div class="panel-body">
        <p>
            แนวทางสำหรับการใช้งานโปรแกรม
        </p>
    </div>

    <div class="table-responsive">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>#</th>
                    <th>CID</th>
                    <th>ptid</th>
                    <th>id</th>
                    <th>rstat</th>
                    <th>sitecode</th>
                    <th>ptcode</th>
                    <th>ptcodefull</th>
                    <th>hsitecode</th>
                    <th>hptcode</th>
                    <th title="ลำดับการลงทะเบียน">Order</th>
                    <th>วันที่</th>
                    <th>ICF</th>
                    <th>CCA-01</th>
                    <th>CCA-02</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $irow = 1;
                if( count($data)>0 ){
                    foreach ($data as $key => $value ){
                        
//                        $lsptid = PIDManagement::getPTIDDetByCID($value['cid']);
//                        $rowscheck = 0;
//                        if(count($lsptid)>0){
//                            foreach($lsptid as $kptid => $vptid){
//                                $lsreg[$vptid['ptid']] = PIDManagement::getPatientDetByPTID($vptid['ptid']); 
//                                $rowscheck = $rowscheck+count($lsreg[$vptid['ptid']]);
//                                //echo '('.count($lsreg[$vptid['ptid']]).') '.$vptid['ptid'].'<br />';
//                            }
//                        }
                        //$rowscheck = 0;
                        $lsreg = PIDManagement::getPatientDetByCID($value['cid']);
                        $rowscheck = count($lsreg);
                ?>
                <tr>
                    <td rowspan="<?php echo $rowscheck; ?>">
                        <?php echo $irow; ?>
                    </td>
                    <td rowspan="<?php echo $rowscheck; ?>">
                        <?php echo $value['cid']; ?>
                        <?php //echo $lsreg[0]['ptid']; ?>
                    </td>
                    <?php
                        $row = 0;
                        echo $this->render('_ptlistreg',[
                            'lsreg' => $lsreg,
                            'ptid' => $lsreg[$row]['ptid'],
                            'row' => $row,
                        ]);
                    ?>
                </tr>
                <?php
                        for($i=1; $i<$rowscheck; $i++){
                ?>
                <tr>
                    <?php
                            $row = $i;
                            echo $this->render('_ptlistreg',[
                                'lsreg' => $lsreg,
                                'ptid' => $lsreg[$row]['ptid'],
                                'row' => $row,
                            ]);
                    ?>
                </tr>
                <?php
                        }
                        $irow++;
                    }
                }
                ?>
            </tbody>
        </table>
    </div>
</div>
<?php
// Modal reg
echo \common\lib\sdii\widgets\SDModalForm::widget([
    'id' => 'modal-popupreg',
    'size' => 'modal-lg',
    'tabindexEnable' => false
]);

$js = <<< JS
        

$(document).on("click", "*[id^=btnEdit]", function() {
    
    var url = "/usfinding/verify/pid-management-edit-from-change";
    var btntxt = $(this).attr('btnidchange');
    var currentvalue = $(this).attr('currentvalue');
    //alert(currentvalue);
    console.log(currentvalue);
    if( $(this).attr('setdisable')=='0' ){
        $.get(url, {
            div:$(this).attr('tagdiv'),
            dataid:$(this).attr('dataid'),
            datatable:$(this).attr('datatable'),
            datavarname:$(this).attr('datavarname'),
            dataoldvalue:$(this).attr('dataoldvalue'),
            datanewvalue:$(this).attr('datanewvalue'),
        })
        .done(function( data, status ) {
            console.log(data);
            var str = data;
            var res = str.split("||");
            if(res.length==3){
                $("#"+currentvalue).html(res[0]);
                $("#"+btntxt).html(res[1]);
                if( res[2]=='1' ){
                    $("#"+btntxt).attr('class','btn btn-primary');
                }
                $("#"+btntxt).attr('setdisable',res[2]);
            }
        });
    }else{
        alert('ค่าตรงกันแล้ว');
    }
});

        
$("#btn-popupreg").on("click", function() {
    modalShowPopup($(this).attr('data-url'));
});
//$("#btn-pop").on("click", function() {
$(document).on("click", "*[id^=btn-pop]", function() {
    modalShowPopup($(this).attr('data-url'));
});
function modalShowPopup(url) {
    //$('#modal-popupreg .modal-content').html('<div class="sdloader "><i class="sdloader-icon"></i></div>');
    $('#modal-popupreg').modal('show')
    .find('.modal-content')
    .load(url);
}
JS;
// register your javascript
$this->registerJs($js, \yii\web\View::POS_END);
