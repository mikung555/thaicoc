<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//echo "xxx";
if( 0 ){
    echo "<pre align='left'>";
    print_r($dlist);
    echo "</pre>";
        
}

if(count($dlist)>0){
?>
<table class="kv-grid-table table table-bordered table-striped">
    <thead>
        <tr style="text-align:center">
            <th class="kv-align-center kv-align-middle" style="min-width:100px;text-align:right;">
                ลำดับที่
            </th>
            <th class="kv-align-center kv-align-middle" style="min-width:100px;text-align:left;">
                ชื่อ-สกุล
            </th>
            <th class="kv-align-center kv-align-middle" style="min-width:100px;text-align:left;">
                Site ID
            </th>
            <th class="kv-align-center kv-align-middle" style="min-width:100px;text-align:left;">
                PID
            </th>
            <th class="kv-align-center kv-align-middle" style="min-width:100px;text-align:left;">
                CID
            </th>
            <th class="kv-align-center kv-align-middle" style="min-width:100px;text-align:left;">
                สถานะ
            </th>
            <th class="kv-align-center kv-align-middle" style="min-width:100px;text-align:left;">
                ใบยินยอม
            </th>
            <th class="kv-align-center kv-align-middle" style="min-width:100px;text-align:left;">
                วันเพิ่มข้อมูล
            </th>
            <th class="kv-align-center kv-align-middle" style="min-width:100px;text-align:left;">
                แก้ไข
            </th>
        </tr>
    </thead>
    <tbody>
    <?php
    $num=1;
    foreach ($dlist as $k => $v){
        $oldcid=$dlist[$k]['cid'];
    ?>
        <tr style="text-align:right">
            <td class="kv-align-center kv-align-middle"
                style="text-align:right;">
                    <?php 
                        echo $num 
                    ?>
            </td>
            <td class="kv-align-center kv-align-middle"
                style="text-align:left;">
                <?php
                echo $dlist[$k]['title']." ".$dlist[$k]['name']." ".$dlist[$k]['surname'];
                ?>
            </td>
            <td class="kv-align-center kv-align-middle"
                style="text-align:left;">
                <?php
                echo $dlist[$k]['hsitecode'];
                ?>
            </td>
            <td class="kv-align-center kv-align-middle"
                style="text-align:left;">
                <?php
                echo $dlist[$k]['hptcode'];
                ?>
            </td>
            <td class="kv-align-center kv-align-middle"
                style="text-align:left;">
                <?php
                echo $dlist[$k]['cid'];
                ?>
            </td>
            <td class="kv-align-center kv-align-middle"
                style="text-align:left;">
                <?php
                echo $dlist[$k]['rstat'];
                ?>
            </td>
            <td class="kv-align-center kv-align-middle"
                style="text-align:left;">
                <?php
                echo $dlist[$k]['icf_check'];
                ?>
            </td>
            <td class="kv-align-center kv-align-middle"
                style="text-align:left;">
                <?php
                echo $dlist[$k]['create_date'];
                ?>
            </td>
            <td class="kv-align-center kv-align-middle"
                style="text-align:left;">
                <?php
                echo $dlist[$k]['update_date'];
                ?>
            </td>
        </tr>
    <?php
        $num++;
    }
    ?> 
    </tbody>
</table>

<?php
    if( count($hlist)>0 ){
        if( 0 ){
            echo "<pre align='left'>";
            print_r($hlist);
            echo "</pre>";
        }
?>
<div class="container-fluid">
    <div style="text-align: center;">
        <h3 style="color: blue;">ประวัติการเปลี่ยนแปลง</h3>
    </div>
    <div class="row">
        <div class="col-xs-2 col-sm-2" style="background-color: #000\9;"></div>
        <div class="col-xs-8 col-sm-8" style="background-color: #000\9;">
<table class="kv-grid-table table table-bordered table-striped">
    <thead>
        <tr style="text-align:center">
            <th class="kv-align-center kv-align-middle" style="min-width:100px;text-align:right;">
                ครั้งที่
            </th>
            <th class="kv-align-center kv-align-middle" style="min-width:100px;text-align:left;">
                วันที่
            </th>
            <th class="kv-align-center kv-align-middle" style="min-width:100px;text-align:left;">
                ค่าที่เปลี่ยนแปลง
            </th>
            <th class="kv-align-center kv-align-middle" style="min-width:100px;text-align:left;">
                เปลี่ยนโดย
            </th>
        </tr>
    </thead>
    <tbody>
    <?php
        $num=1;
        foreach ($hlist as $k => $v){
        ?>
        <tr style="text-align:right">
            <td class="kv-align-center kv-align-middle"
                style="text-align:right;">
                    <?php 
                        echo (count($hlist)+1)-$num;
                    ?>
            </td>
            <td class="kv-align-center kv-align-middle"
                style="text-align:left;">
                <?php
                echo $hlist[$k]['dchange'];
                ?>
            </td>
            <td class="kv-align-center kv-align-middle"
                style="text-align:left;">
                <?php
                echo $hlist[$k]['cid']." => ".$hlist[$k]['cid_new'];
                ?>
            </td>
            <td class="kv-align-center kv-align-middle"
                style="text-align:left;">
                <?php
                echo $hlist[$k]['user_name'];
                ?>
            </td>
        </tr>
        <?php
            $num++;
        }
    ?>
    </tbody>
</table>
        </div>
        <div class="col-xs-2 col-sm-2" style="background-color: #000\9;"></div>
    </div>
</div>
    <?php
    }
    echo $this->render('_updatecid',
        [
            'oldcid'=>$oldcid,
            'msgerr'=>$msgerr,
        ]);
}

