<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
use yii\helpers\Html;

?>
<div class="container-fluid">
  <h2>
      ส่วนบันทึก
      
  </h2>
  <div class="row">
    <div class="col-xs-6 col-sm-6" style="background-color: #000\9;">
      ใส่เลขบัตรประจำตัวประชาชนใหม่ที่ต้องการ:
      <?php
            echo Html::input('text'
                    , '_newcid', $oldcid, ['id'=>'_newcid','maxlength'=>13]);
            echo Html::hiddenInput('_oldcid', $oldcid, ['id'=>'_oldcid']);
      ?>
      <?php
            if( Yii::$app->user->can('administrator')==TRUE ){
                echo Html::submitButton('<span class="glyphicon glyphicon-edit"> </span> บันทึก CID ใหม่'
                        , ['id'=>'btnSave', 'class' =>'btn btn-info btn-sm','title'=>'บันทึก CID ใหม่']
                        );
            }else{
                echo "ท่านไม่มีสิทธิ์แก้ไข";
            }
            
            
            if( strlen(trim($msgerr))>0 ){
                echo "<font style='color: red;' >";
                echo $msgerr;
                echo "</font>";
            }
      ?>
    </div>
    <div class="col-xs-6 col-sm-2"></div>
    <div class="clearfix visible-xs"></div>
    <div class="col-xs-6 col-sm-2"></div>
    <div class="col-xs-6 col-sm-2"></div>
  </div>
</div>

