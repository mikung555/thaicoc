<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
use yii\helpers\Html;

$this->title = Yii::t('', 'จัดการข้อมูลการลง CCA-02 ซ้ำ');

echo $this->render('_menuVerify');

echo "<br />";
// Modal reg
echo \common\lib\sdii\widgets\SDModalForm::widget([
    'id' => 'modal-popupreg',
    'size' => 'modal-lg',
    'tabindexEnable' => false
]);
$js = <<< JS
    //$("#btn-popupreg").on("click", function() {
    $(document).on("click", "*[id^=btnpopup]", function() {
        modalShowPopup($(this).attr('data-url')+'?hsitecode='+$(this).attr('hsitecode')+'&hptcode='+$(this).attr('hptcode')+'&f2v1='+$(this).attr('f2v1'));
    });
    $(document).on("click", "*[id^=popup]", function() {
        modalShowPopup($(this).attr('data-url')+'?hsitecode='+$(this).attr('hsitecode')+'&hptcode='+$(this).attr('hptcode')+'&f2v1='+$(this).attr('f2v1'));
    });
    function OnClick(obj){
        modalShowPopup($(obj).attr('data-url')+'?hsitecode='+$(obj).attr('hsitecode')+'&hptcode='+$(obj).attr('hptcode')+'&f2v1='+$(obj).attr('f2v1'));
    }
    function modalShowPopup(url) {
        $('#modal-popupreg .modal-content').html('<div class="sdloader "><i class="sdloader-icon"></i></div>');
        $('#modal-popupreg').modal('show')
        .find('.modal-content')
        .load(url);
    }
JS;
// register your javascript
$this->registerJs($js, \yii\web\View::POS_END);
//echo Html::button('AJax', ['id'=>'btn-popupreg','data-url'=>'/usfinding/verify/cca02-visit','hsitecode'=>'00313','hptcode'=>'00246','f2v1'=>'2017-01-26']);

echo $this->render('_listcca02duplicate',
        [
            'duplist'=>$duplist,
        ]);




if( 0 ){
    echo "<pre align='left'>";
    print_r($duplist);
    echo "</pre>";
}

