<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$this->title = Yii::t('', 'ตรวจสอบ เลขบัตรประจำตัวประชาชน');


echo $this->render('_menuVerify');

echo $this->render('_search',
        [
            'reccord'=>$reccord,
            'perpage'=>$perpage,
            'curentpage'=>$curentpage,
            'tabdet' => $tabdet,
        ]);


$jsAdd = <<< JS
    init();
    function init()
    {
        waitingoff();
    }
    function waitingon()
    {
        $('#waiting').css('display', 'block');//not show
        $("#waiting").button('loading');
    }
    function waitingoff()
    {
        $('#waiting').css('display', 'none');//not show
    }
    function getdata(cid,div,refresh)
    {
        $("#table_resultsearch").empty();
        var url = "/usfinding/verify/get-data-search-cid";
        var sec = "1";
        $.get(url
            , {
                cid:cid,
                sec:sec,
                div:div,
                refresh:refresh,
            }
        )
        .done(function( data ) {
            switch (sec) {
                case "1":
                    $("#table_resultsearch").empty();
                    $("#table_resultsearch").html(data);
                    waitingoff();
                    break;
                }
            }
        );
    }
    function savenewcid(newcid,oldcid,refresh)
    {
        $("#table_resultsearch").empty();
        var url = "/usfinding/verify/update-cid";
        var sec = "1";
        $.get(url
            , {
                newcid:newcid,
                oldcid:oldcid,
                refresh:refresh,
            }
        )
        .done(function( data ) {
            switch (sec) {
                case "1":
                    $("#table_resultsearch").empty();
                    $("#table_resultsearch").html(data);
                    break;
                }
            }
        );
    }
    $(document).on("click","#btnSearch", function() {
        //alert('search');
        var cid= $("#_cidsearch").val();
        waitingon();
        //alert(cid);
        getdata(cid,'','yes');
    });
        
    $(document).on("click","#btnSave", function() {
        //alert('save');
        var newcid= $("#_newcid").val();
        var oldcid= $("#_oldcid").val();
        //alert(cid);
        savenewcid(newcid,oldcid,'yes');
    });
JS;
$this->registerJs($jsAdd);

