<?php

use Yii;
use yii\helpers\Html;
use yii\helpers\Url;

$request = Yii::$app->request; 

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

?>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<?php

$js = <<< JS
    function reloadData(code) {
        //code
        var url = '';
        var txt = '';
        var txtad = '';
        
        if ( code == $('#lastUpdate').attr('datarecord') ) {
            //
        }else{
            $('#lastUpdate').attr('datarecord',code);
            var noRec = $('#noRec'+code).html();
            var imgUrl = 'https://tools.cascap.in.th/api/us/imglist.php?id=';
            $('#noRec'+code).html('');
            $('#imgShow'+code).html('');
            $('#noRec'+code).append('<i id="spinerForXML" class="fa fa-spinner fa-spin" style="font-size:16px"></i>');
            $('#imgShow'+code).append('<i id="spinerForXML" class="fa fa-spinner fa-spin" style="font-size:16px"></i>');
            $.ajax({
                type: 'GET',    
                url: 'https://tools.cascap.in.th/api/us/loadimage.php', 
                data: { id: code }, 
                dataType: 'json',
                success: function (data) {
                    $('#imgShow'+code).html('<img src="'+imgUrl+code+'" width="40" border="0">');
                    $('#noRec'+code).html($('#noRec'+code).attr('datanoRec'));
                }
            });
        }
    }
JS;
// register your javascript
$this->registerJs($js, \yii\web\View::POS_END);
?>
<div class="panel panel-default">
    <div class="panel-heading">ผลการดูการบันทึกข้อมูล อัลตราซาวด์เข้ามา</div>
    <div class="panel-body">
        <p>ข้อมูลเริ่มบันทึกตั้งแต่
            <?php
                $selectStart = new DateTime($start);
                $selectEnd = new DateTime($end);
                //echo $selectStart->format('l');
                echo $start.' ('.$selectStart->format('l').') '.' ถึง '.$end.' ('.$selectEnd->format('l').') ';
            ?>
        </p>
        <p>พบข้อมูลทั้งหมด
            <?php
                echo number_format(count($data));
            ?>
        <div id="table_resultsearch"></div>
        </p>
        <p>
            <?php
                $btnClass   = 'btn btn-info';
                
                $url = '/'.$request->pathInfo;
                
                //$selectStart->add( 1 ); 
                $one_days = new DateInterval( "P1D" );
                $selectStart->sub( $one_days ); 
                $nextDate = new DateTime($start);
                $nextDate->add( $one_days );
                echo Html::a('<i class="glyphicon glyphicon-wrench"></i><< ย้อนไปอีก 1 วัน ('.$selectStart->format('Y-m-d').') ('.$selectStart->format('l').')'
                    , Url::to([ $url, 'dstart'=>$selectStart->format('Y-m-d'), 'dend'=>$selectStart->format('Y-m-d'),'imgreload'=>$request->get('imgreload') ]) 
                    ,[
                        'id'        => 'preDate',
                        'class'     => $btnClass,
                    ]);
                
                echo "&nbsp;|&nbsp;";
                
                echo Html::a('<i class="glyphicon glyphicon-wrench"></i>วันต่อไป 1 วัน >> ('.$nextDate->format('Y-m-d').') ('.$nextDate->format('l').')'
                    , Url::to([ $url, 'dstart'=>$nextDate->format('Y-m-d'), 'dend'=>$nextDate->format('Y-m-d'),'imgreload'=>$request->get('imgreload') ]) 
                    ,[
                        'id'        => 'preDate',
                        'class'     => $btnClass,
                    ]);
            ?>
        </p>
    </div>

    <div id="lastUpdate" datarecord=""></div>
    <p></p>
    <div class="table-responsive">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>#</th>
                    <th>วันที่บันทึก</th>
                    <th>แก้ไขล่าสุด</th>
                    <th>สถานะ</th>
                    <th>Exam date</th>
                    <th>ภาพอัลตราซาวด์</th>
                    <th>ผล</th>
                    <th>โรงพยาบาล</th>
                    <th>PID</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    $irow=1;
                    if( count($data)>0 ){
                        foreach($data as $key => $value ){
                            
                ?>
                <tr id="recordid<?php echo $value['id']; ?>" onmouseover="reloadData('<?php echo $value['id']; ?>');" >
                    <td id="noRec<?php echo $value['id']; ?>" datanoRec="<?php echo $irow; ?>"><?php echo $irow; ?></td>
                    <td><?php echo $value['create_date']; ?></td>
                    <td><?php echo $value['update_date']; ?></td>
                    <td>
                        <?php 
                            if( $value['rstat']=='1' ){
                                echo "Waiting";
                            }else if($value['rstat']=='2' ){
                                echo "Submited";
                            } 
                        ?>
                    </td>
                    <td>
                        
                        <?php 
                            echo Html::a($value['f2v1']
                                , Url::to([
                                    '/teleradio/suspected/open-form',
                                    'ezf_id' => '1437619524091524800',
                                    'dataid' =>$value['id'],
                                    'ptid'   => $value['ptid'], //base64_encode($value['ptid']),
                                        ]) 
                                ,[
                                    'id'        => 'preDate',
                                    'class'     => 'btn btn-link btn-xs',
                                    'target'    => '_blank',
                                ]);
                        ?>
                    </td>
                    <td id="imgShow<?php echo $value['id']; ?>">
                        <?php
                        if( $request->get('imgreload')=='1' ){
                        ?>
                        <img src="https://tools.cascap.in.th/api/us/imglist.php?id=<?php echo $value['id']; ?>" width="40" border="0">
                        <?php
                        }else{
                        ?>
                        <img src="https://tools.cascap.in.th<?php echo $value['sys_usimagepath']; ?>" width="40" border="0">
                        <?php
                        }
                        ?>
                    </td>
                    <td><?php echo $value['usresults']; ?></td>
                    <td>
                        <?php 
                        //echo $value['hsitecode']; 
                        $url = [
                            '/usfinding/verify/hospital-show',
                            'hsitecode' => $value['hsitecode'],
                            'ptid' => $value['ptid'],
                            'id' => $value['id'],
                        ];
                        $dataurl = Url::to($url);
                        echo Html::button($value['hsitecode']." <span class='glyphicon glyphicon-list-alt' style='color:blue'></span>", [
                                        'data-toggle' => 'tooltip', 'data-original-title' => 'แก้ปัญหาข้อมูล',
                                        'data-pjax' => '0',
                                        'id' => 'btn-pop' . $value['ptid'],
                                        'data-url' => $dataurl,
                                        'title' => 'แก้ปัญหาข้อมูล',
                                        'class' => 'btn btn-link btn-sm',
                                        'style' => 'color:black;',
                            ]);
                        ?>
                    </td>
                    <td><?php echo $value['hptcode']; ?></td>
                </tr>
                <?php
                            $irow++;
                        }
                    }
                ?>
            </tbody>
        </table>
    </div>
</div>
<?php
// Modal reg

echo \common\lib\sdii\widgets\SDModalForm::widget([
    'id' => 'modal-popupus',
    'size' => 'modal-lg',
    'tabindexEnable' => false
]);

$js = <<< JS
$("#btn-popupreg").on("click", function() {
    modalShowPopup($(this).attr('data-url'));
});
        
$(document).on("click", "*[id^=btn-pop]", function() {
    modalShowPopup($(this).attr('data-url'));
});
        
function modalShowPopup(url) {
    $('#modal-popupus').modal('show')
    .find('.modal-content')
    .load(url);
}
JS;
// register your javascript
$this->registerJs($js, \yii\web\View::POS_END);
?>