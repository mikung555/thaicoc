<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

?>
<div class="panel panel-primary" style="width: 100%;">
        <div class="panel-heading"><h3><b>ผลอัลตราซาวด​์</b></h3></div>
        <div class="panel-body">
            <p>รายละเอียด</p>
        </div>

        <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th style="text-align: right"><h4><b>วันที่ตรวจ:</b></h4></th>
                        <th style="text-align: left"><h4><b><?php echo $cca02['f2v1']; ?></b></h4></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td style="text-align: right"><b>Site ID:</b></td>
                        <td style="text-align: left"><?php echo $cca02['hsitecode']; ?></td>
                    </tr>
                    <tr>
                        <td style="text-align: right"><b>PID</b></td>
                        <td style="text-align: left"><?php echo $cca02['hptcode']; ?></td>
                    </tr>
                    <tr>
                        <td style="text-align: right"><b>ผู้บันทึกข้อมูล:</b></td>
                        <td style="text-align: left"><?php echo $ucca02['firstname'].' '.$ucca02['lastname']; ?></td>
                    </tr>
                    <tr>
                        <td style="text-align: right"><b>เบอร์ติดต่อ:</b></td>
                        <td style="text-align: left"><?php echo $ucca02['telephone']; ?></td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="panel-body">
            <p></p>
        </div>
    </div>
