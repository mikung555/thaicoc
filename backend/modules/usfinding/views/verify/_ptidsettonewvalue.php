<?php
use backend\modules\usfinding\classes\PIDManagement;
use backend\modules\usfinding\classes\QueryRegister;
use backend\modules\usfinding\classes\QueryCCA01;
use backend\modules\usfinding\classes\QueryCCA02;
use backend\modules\usfinding\classes\QueryCCA02p1;
use backend\modules\usfinding\classes\QueryCCA03;
use backend\modules\usfinding\classes\QueryCCA04;
use backend\modules\usfinding\classes\QueryCCA05;

$request = Yii::$app->request;

$org = QueryRegister::getDetByID($request->get('idzero'));
?>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title" id="myModalLabel"><b><?php echo $request->get('title'); ?></b></h4>
</div>
<div class="modal-body">
    <div class="panel panel-primary" style="width: 60%;">
        <div class="panel-heading"><h4><b>ข้อมูลต้นแบบ</b></h4></div>
        <div class="panel-body">
            <p>ระบบจะใช้ข้อมูลหลัก เป็นตัวกำกับในการแก้ไขข้อมูล (ptid)</p>
        </div>

        <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>ตัวแปร</th>
                        <th>ข้อมูลต้นแบบ</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>ptid</td>
                        <td><?php echo $org['ptid']; ?></td>
                    </tr>
                    <tr>
                        <td>sitecode</td>
                        <td><?php echo $org['sitecode']; ?></td>
                    </tr>
                    <tr>
                        <td>ptcode</td>
                        <td><?php echo $org['ptcode']; ?></td>
                    </tr>
                    <tr>
                        <td>ptcodefull</td>
                        <td><?php echo $org['ptcodefull']; ?></td>
                    </tr>
                    <tr>
                        <td>fullname</td>
                        <td><?php echo $org['title'].$org['name'].' '.$org['surname']; ?></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div id="deleteReg">
        <?php
        if( $org['ptid'] != $request->get('ptidrow') ){
            $class = 'btn btn-danger';
            $btnvalue = 'คลิ๊กเพื่อ ลบข้อมูลที่ Register ซ้ำ';
            $setdisable = '0';
        }else{
            $class = 'btn btn-primary';
            $btnvalue = 'ข้อมูล ptid ถูกต้องแล้ว และเป็นข้อมูลไม่ซ้ำ';
            $setdisable = '1';
        }
        echo yii\helpers\Html::button($btnvalue, [
            'id' => 'btnEditRstat'.$request->get('id').'rstat',
            'dataid' => $request->get('id'),
            'datavarname' => 'rstat',
            'dataoldvalue' => $org['rstat'],
            'datanewvalue' => '3',
            'datatable' => 'tb_data_1',
            'tagdiv' => 'deleteReg',
            'setdisable' => $setdisable,
            'btnidchange' => 'btnEditRstat'.$request->get('id').'rstat',
            'currentvalue' => 'currentvalueRstat'.$request->get('id').'rstat',
            'class' => $class,
        ]);
        ?>
        <p></p>
    </div>
    <?php
    $arg['ptidzero'] = $request->get('ptidzero');
    $arg['ptidrow'] = $request->get('ptidrow');
    $arg['id'] = $request->get('id');
    $arg['title'] = $request->get('title');
    $arg['div'] = 'divReg'.$arg['id'];
    $_det = PIDManagement::getRegisterDetail($arg,$org);
    echo '<div id="'.$arg['div'].'">';
    echo $this->render('_ptidpanelview',[
        '_det' => $_det,
        '_arg' => $arg,
    ]);
    echo '</div>';
    
    # CCA01
    $cca01 = QueryCCA01::getIDOfCCA01ByPTID($request->get('ptidrow'));
    if( count($cca01)>0 ){
        foreach ($cca01 as $kcca01 => $vcca01 ){
            $arg['ptidzero'] = $request->get('ptidzero');
            $arg['ptidrow'] = $request->get('ptidrow');
            $arg['id'] = $vcca01['id'];
            $arg['title'] = $request->get('title');
            $arg['div'] = 'div'.$vcca01['id'];
            $_det = PIDManagement::getCCA01Detail($arg,$org);
            echo '<div id="'.$arg['div'].'">';
            echo $this->render('_ptidpanelview',[
                '_det' => $_det,
                '_arg' => $arg,
            ]);
            echo '</div>';
        }
    }
    
    # CCA-02
    $cca02 = QueryCCA02::getIDOfByPTID($request->get('ptidrow'));
    //echo count($cca02);
    if( count($cca02)>0 ){
        foreach ($cca02 as $kcca02 => $vcca02 ){
            $arg['ptidzero'] = $request->get('ptidzero');
            $arg['ptidrow'] = $request->get('ptidrow');
            $arg['id'] = $vcca02['id'];
            $arg['title'] = $request->get('title');
            $arg['div'] = 'div'.$vcca02['id'];
            $_det = PIDManagement::getCCA02Detail($arg,$org);
            echo '<div id="'.$arg['div'].'">';
            echo $this->render('_ptidpanelview',[
                '_det' => $_det,
                '_arg' => $arg,
            ]);
            echo '</div>';
        }
    }else{
        echo '<div id="cca02">CCA-02: ไม่มีข้อมูล</div>';
    }
    
    # CCA-02.1
    $cca02p1 = QueryCCA02p1::getIDOfByPTID($request->get('ptidrow'));
    //echo count($cca02p1);
    if( count($cca02p1)>0 ){
        foreach ($cca02p1 as $kcca02p1 => $vcca02p1 ){
            $arg['ptidzero'] = $request->get('ptidzero');
            $arg['ptidrow'] = $request->get('ptidrow');
            $arg['id'] = $vcca02p1['id'];
            $arg['title'] = $request->get('title');
            $arg['div'] = 'div'.$vcca02p1['id'];
            $_det = PIDManagement::getCCA02p1Detail($arg,$org);
            echo '<div id="'.$arg['div'].'">';
            echo $this->render('_ptidpanelview',[
                '_det' => $_det,
                '_arg' => $arg,
            ]);
            echo '</div>';
        }
    }else{
        echo '<div id="cca02">CCA-02.1: ไม่มีข้อมูล</div>';
    }
    
    # CCA-03
    $cca03 = QueryCCA03::getIDOfByPTID($request->get('ptidrow'));
    //echo count($cca03);
    if( count($cca03)>0 ){
        foreach ($cca03 as $kcca03 => $vcca03 ){
            $arg['ptidzero'] = $request->get('ptidzero');
            $arg['ptidrow'] = $request->get('ptidrow');
            $arg['id'] = $vcca03['id'];
            $arg['title'] = $request->get('title');
            $arg['div'] = 'div'.$vcca03['id'];
            $_det = PIDManagement::getCCA03Detail($arg,$org);
            echo '<div id="'.$arg['div'].'">';
            echo $this->render('_ptidpanelview',[
                '_det' => $_det,
                '_arg' => $arg,
            ]);
            echo '</div>';
        }
    }else{
        echo '<div id="cca02">CCA-03: ไม่มีข้อมูล</div>';
    }
    
    # CCA-04
    $cca04 = QueryCCA04::getIDOfByPTID($request->get('ptidrow'));
    //echo count($cca04);
    if( count($cca04)>0 ){
        foreach ($cca04 as $kcca04 => $vcca04 ){
            $arg['ptidzero'] = $request->get('ptidzero');
            $arg['ptidrow'] = $request->get('ptidrow');
            $arg['id'] = $vcca04['id'];
            $arg['title'] = $request->get('title');
            $arg['div'] = 'div'.$vcca04['id'];
            $_det = PIDManagement::getCCA04Detail($arg,$org);
            echo '<div id="'.$arg['div'].'">';
            echo $this->render('_ptidpanelview',[
                '_det' => $_det,
                '_arg' => $arg,
            ]);
            echo '</div>';
        }
    }else{
        echo '<div id="cca02">CCA-04: ไม่มีข้อมูล</div>';
    }
    
    # CCA-05
    $cca05 = QueryCCA05::getIDOfByPTID($request->get('ptidrow'));
    //echo count($cca05);
    if( count($cca05)>0 ){
        foreach ($cca05 as $kcca05 => $vcca05 ){
            $arg['ptidzero'] = $request->get('ptidzero');
            $arg['ptidrow'] = $request->get('ptidrow');
            $arg['id'] = $vcca05['id'];
            $arg['title'] = $request->get('title');
            $arg['div'] = 'div'.$vcca05['id'];
            $_det = PIDManagement::getCCA05Detail($arg,$org);
            echo '<div id="'.$arg['div'].'">';
            echo $this->render('_ptidpanelview',[
                '_det' => $_det,
                '_arg' => $arg,
            ]);
            echo '</div>';
        }
    }else{
        echo '<div id="cca02">CCA-05: ไม่มีข้อมูล</div>';
    }
    
    
        if( 0 ){
            echo "<pre align='left'>";
            //print_r($_GET);
            echo "</pre>";
        }
    ?>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
</div>
<?php


?>

