<?php
use \yii\helpers\Html;

use backend\modules\usfinding\classes\PIDManagement;

if( $ptid != $lsreg[0]['ptid'] ){
    $ptidtxt = '<span style="color: red;">'.$ptid.'</span>';
    $id = '<span style="color: red;">'.$lsreg[$row]['id'].'</span>';
}else{
    $id = $lsreg[$row]['id'];
    $ptidtxt = $ptid;
}


if( $lsreg[$row]['ptcodefull'] != $lsreg[0]['ptcodefull'] ){
    $ptcodefulltxt = '<span style="color: red;">'.$lsreg[$row]['ptcodefull'].'</span>';
}else{
    $ptcodefulltxt = $lsreg[$row]['ptcodefull'];
}
?>
                    <td>
                        <?php
                            echo $ptidtxt;
                            $dataurl = \yii\helpers\Url::to(['/usfinding/verify/pid-management-edit',
                                'ptidzero'=>$lsreg[0]['ptid'],
                                'ptidrow'=>$lsreg[$row]['ptid'],
                                'id'=>$lsreg[$row]['id'],
                                'idzero'=>$lsreg[0]['id'],
                                'title'=>'แก้ไขข้อมูล'
                                ]);
                            
                            echo Html::button(" <span class='glyphicon glyphicon-wrench' style='color:blue'></span>", [
                                        'data-toggle' => 'tooltip', 'data-original-title' => 'แก้ปัญหาข้อมูล',
                                        'data-pjax' => '0',
                                        'id' => 'btn-pop' . $lsreg[$row]['ptid'],
                                        'data-url' => $dataurl,
                                        'title' => 'แก้ปัญหาข้อมูล',
                                        'class' => 'btn btn-link btn-sm',
                            ]);
                        ?>
                    </td>
                    <td>
                        <?php 
                        echo $id; 
                        ?>
                    </td>
                    <td>
                        <?php 
                        echo $lsreg[$row]['rstat']; 
                        ?>
                    </td>
                    <td>
                        <?php 
                            //echo '('.count($lsreg).') '.$lsreg[$row]['ptid'];
                        echo $lsreg[$row]['sitecode'];
                        ?>
                    </td>
                    <td>
                        <?php 
                        echo $lsreg[$row]['ptcode']; 
                        ?>
                    </td>
                    <td>
                        <?php 
                        echo $ptcodefulltxt; 
                        ?>
                    </td>
                    <td>
                        <?php 
                        echo $lsreg[$row]['hsitecode']; 
                        ?>
                    </td>
                    <td>
                        <?php 
                        echo $lsreg[$row]['hptcode']; 
                        ?>
                    </td>
                    <td><?php echo $row; ?></td>
                    <td><?php echo $lsreg[$row]['dateofaddrecord']; ?></td>
                    <td>
                        <?php 
                        if( $lsreg[$row]['confirm']>0 ){
                            echo '1';
                        }else{
                            echo $lsreg[$row]['confirm'];
                        }
                        ?>
                    </td>
                    <td><?php echo PIDManagement::countCCA01ByPTID($ptid); ?></td>
                    <td><?php echo PIDManagement::countCCA02ByPTID($ptid); ?></td>

