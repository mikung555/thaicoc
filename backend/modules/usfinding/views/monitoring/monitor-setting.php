<?php

use yii\widgets\ActiveForm;
use kartik\widgets\Select2;
use yii\web\JsExpression;
use kartik\grid\GridView;
use yii\helpers\Url;
use \frontend\modules\api\v1\classes\MainQuery;
use yii\helpers\Html;

$session = \Yii::$app->session;
$table_us = $session['table_us'];
$refresh_time = $session['refresh_time'];
$auto_reload = $session['auto_reload'];


if ($table_us == '')
    $table_us = 'tb_data_3';
if ($refresh_time == '')
    $refresh_time = '5';

if($auto_reload == null){
    $auto_reload = 'false';
}
//var_dump($auto_reload);
$checked = $auto_reload=='false'?'':'checked';
?>
<div class="modal-header" style="background: green;color:#fff;">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <h4 class="modal-title"><strong>Monitor Setting</strong></h4>
</div>
<div class="modal-body" style="padding-left: 20px;">
    <table width="80%" border="0">
        <tr>
            <td width="35%" style="text-align: right;padding-left: 20px"><div class="col-md-12"> <label>Time Reload : </label> </div></td>
            <td width="40%"> <div class="col-md-12"><input class="form-control" type="number" name="refresh_time" id="refresh_time" value="<?= $refresh_time ?>"></div> </td>
            <td width="20%"> <div class="col-md-12"><label> วินาที/ครั้ง </label></div> </td>
        </tr>
        <tr>
            <td width="35%" style="text-align: right;padding-left: 20px"><div class="col-md-12"> <label>Auto Reload: </label> </div></td>
            <td width="40%"> <div class="col-md-12"><input  type="checkbox" name="auto_reload" id="auto_reload" <?=$checked?>></div> </td>
            <td width="20%"> <div class="col-md-12"><label> </label></div> </td>
        </tr>
        <tr>
            <td width="35%" style="text-align: right;"><div class="col-md-12"> <label>Table Ultrasound : </label></div> </td>
            <td width="40%"> <div class="col-md-12"><input class="form-control" type="text" name="table_us" id="table_us" value="<?= $table_us ?>"> </div></td>
            <td width="20%"> <div class="col-md-8"><label>  </label> </div></td>
        </tr>
        <tr>
            <td width="35%" style="text-align: right;">
                <div class="col-md-12"> <label class="control-label">เลขที่ Worklist :</label></div>
                </td>
            <td width="40%" colspan="2" >
                <div class="col-md-12">
                    <?php
                    
                    echo Select2::widget([
                        'name' => 'worklistno',
                        'id' => 'worklistno',
                        'options' => ['placeholder' =>$resWorkDefault['id'] . ' : ' . $resWorkDefault["sitecode"].' ' . $resWorkDefault["title"] , 'data-id' => $resWorkDefault['id']],
                        'pluginOptions' => [
                            'allowClear' => true,
                            'minimumInputLength' => 0, //ต้องพิมพ์อย่างน้อย 3 อักษร ajax จึงจะทำงาน
                            'ajax' => [
                                'url' => '/usfinding/worklist/get-worklist-number',
                                'dataType' => 'json', //รูปแบบการอ่านคือ json
                                'data' => new JsExpression('function(params) { 
                            return {q:params.term}; 
                            }
                         '),
                            ],
                            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                            'templateResult' => new JsExpression('function(city) { return city.text; }'),
                            'templateSelection' => new JsExpression('function (city) { return city.text; }'),
                        ]
                    ]);
                    ?>
                </div>
            </td>

        </tr>
    </table>

</div>
<div class="modal-footer">
    <button type="button" class="btn btn-success" id="btn-save-setting">บันทึก</button>
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
</div>

<?php
$this->registerJs("
   $('#btn-save-setting').on('click', function(){
       var table_us = $('#table_us').val();
       var refresh_time = $('#refresh_time').val();
       var worklistno= $('#worklistno').val();
       var auto_reload= $('#auto_reload').is(':checked');
       if(worklistno == ''){
            worklistno= $('#worklistno').attr('data-id');
        }
       
       $.ajax({
            url:'" . Url::to('/usfinding/monitoring/set-setting') . "',
            method:'POST',
            data:{
                table_us : table_us,
                refresh_time:refresh_time,
                worklistno:worklistno,
                auto_reload:auto_reload
            },
            type:'HTML',
            success:function(result){
                showMonitoringAfterSetting(worklistno);
               $('#modal-setting').modal('hide');
            },
            error:function(){
            
            }
        });
   });
   
   function showMonitoringAfterSetting(worklistno){
        var monDiv = $('#contain-show');
        var startdate = $('#inputStartDate').val();
        var enddate = $('#inputEndDate').val();
        
        monDiv.html('<div style=\'text-align:center;\'><i class=\"fa fa-spinner fa-pulse fa-fw fa-3x\"></i></div>');
        $.ajax({
            url:'" . Url::to('/usfinding/monitoring/ultrasound-data') . "',
            method:'post',
            data:{
                startDate:startdate,
                endDate:enddate,
                worklistno:worklistno
            },
            type:'HTML',
            success:function(result){
                monDiv.empty();
                monDiv.html(result);
            }
        });
    }
");
?>
