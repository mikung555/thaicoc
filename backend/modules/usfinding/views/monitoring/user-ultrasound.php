<?php
use kartik\widgets\Select2;
use yii\web\JsExpression;
?>
<div class="modal-header" style="border-radius:5px 5px 0px 0px ;background: #00A21E;color:#fff;">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <h4 class="modal-title"><strong>เจ้าหน้าที่ลงข้อมูลประจำห้องตรวจ</strong></h4>
</div>
<div class="modal-body" >
    <i class="glyphicon glyphicon-flag fa-2x"></i><label style="font-size:20px;">ห้องตรวจ Ultrasound ที่ <?=($room_name)?></label>
<?php
//         $resDoctor2 = yii\helpers\ArrayHelper::map($resDoctor, 'doctorcode', 'doctorfullname');
        echo Select2::widget([
                'name' => 'user-us',
                'id' => 'user-us',
                'value'=>$user_us['firstname'].' '.$user_us['firstname'],
                'options' => ['placeholder' => 'เลือกแพทย์ประจำห้องตรวจ...','data-id'=>$user_us['user_id']],
                'pluginOptions' => [
                    'allowClear' => true,
                    'minimumInputLength' => 0, //ต้องพิมพ์อย่างน้อย 3 อักษร ajax จึงจะทำงาน
                    'ajax' => [
                        'url' => '/usfinding/monitoring/user-list',
                        'dataType' => 'json', //รูปแบบการอ่านคือ json
                        'data' => new JsExpression('function(params) { 
                            return {q:params.term}; 
                            }
                         '),
                    ],
                    'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                    'templateResult' => new JsExpression('function(city) { return city.text; }'),
                    'templateSelection' => new JsExpression('function (city) { return city.text; }'),
                ]
        ]);
        ?>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    <button type="button" id="confirm-user" class="btn btn-primary" data-dismiss="modal">Confirm</button>
</div>

<?php
$this->registerJs("
    $('#confirm-user').on('click', function(){
        console.log($('#user-us').val());
        var room_name = '$room_name';
        var room_type = '$room_type';
        onUserChange($('#user-us').val(), room_name,room_type);
    });

");
?>