<?php

use yii\helpers\Url;
use yii\helpers\Html;

if ($state == 'doctor') {
    ?>
    <div class="col-md-12">
        <table class="table table-bordered">
            <thead class="table-default">
                <tr>
                    <th style="text-align: center;">No.</th>
                    <th style="text-align: center;">Site ID</th>
                    <th style="text-align: center;">Participant ID</th>
                    <th style="text-align: center;">Age</th>
                    <th style="text-align: center;">Gender</th>
                    <th style="text-align: center;">Exam Date</th>
                    <th style="text-align: center;">Value</th>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($patientList as $key => $value) {
                    ?>
                    <tr>
                        <th style="text-align: center;">
                            <?php echo ($key + 1) ?>
                        </th>
                        <th style="text-align: center;">
                            <?php echo $value['hsitecode'] ?>
                        </th>
                        <th style="text-align: center;">
                            <?php echo $value['hptcode'] ?>
                        </th>
                        <th style="text-align: center;">
                            <?php echo $value['age'] ?>
                        </th>
                        <th style="text-align: center;">
                            <?php
                            if ($value['f1v3'] == 1)
                                echo "ชาย";
                            else if ($value['f1v3'] == 2)
                                echo "หญิง";
                            ?>
                        </th>
                        <th  style="text-align: center;"> 
                            <a href='javascript:void(0)' id="ccaModal<?= $drilldown ?>" title="เปิดอ่านเท่านั้น"><?= date_format(date_create($value['f2v1']), "d/m/Y") ?>
                                <input id="ezf_id" type="hidden" value="1437619524091524800">
                                <input id="dataid" type="hidden" value="<?= $value['id_cca02'] ?>">
                                <input id="comp_target" type="hidden" value="1437725343023632100">
                                <input id="target" type="hidden" value="<?= base64_encode($value['ptid']) ?>">
                                <input id="comp_id_target" type="hidden" value="1437725343023632100">
                                <input id="readonly" type="hidden" value="1"> </a>&nbsp;
                            <?php
                            echo Html::a('<span class="glyphicon glyphicon-edit"></span>'
                                    , Url::to([
                                        '/teleradio/suspected/open-form',
                                        'ezf_id' => '1437619524091524800',
                                        'dataid' => $value['id_cca02'],
                                        'ptid' => $value['ptid'],
                                    ])
                                    , [
                                'title' => Yii::t('app', 'เปิดเพื่อแก้ไข และดูภาพ'),
                                'target' => '_blank',
                            ]);
                            ?>                 
                        </th>
                        <th style="text-align: center;">
                            <?php echo $doctorname ?>
                        </th>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
<?php } ?>
<?php if ($state == 'gender') { ?>
    <div class="col-md-12">
        <table class="table table-bordered">
            <thead class="table-default">
                <tr>
                    <th style="text-align: center;">No.</th>
                    <th style="text-align: center;">Site ID</th>
                    <th style="text-align: center;">Participant ID</th>
                    <th style="text-align: center;">DVISIT</th>
                    <th style="text-align: center;">Value</th>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($patientGender as $key => $value) {
                    ?>
                    <tr>
                        <th style="text-align: center;">
                            <?php echo ($key + 1) ?>
                        </th>
                        <th style="text-align: center;">
                            <?php echo $value['hsitecode'] ?>
                        </th>
                        <th style="text-align: center;">
                            <?php echo $value['hptcode'] ?>
                        </th>
                        <th style="text-align: center;">
                            <a href='javascript:void(0)' id="ccaModal<?= $drilldown ?>" title="เปิดอ่านเท่านั้น"><?= date_format(date_create($value['f2v1']), "d/m/Y") ?>
                                <input id="ezf_id" type="hidden" value="1437377239070461301">
                                <input id="dataid" type="hidden" value="<?= $value['id_cca02'] ?>">
                                <input id="comp_target" type="hidden" value="1437725343023632100">
                                <input id="target" type="hidden" value="<?= base64_encode($value['ptid']) ?>">
                                <input id="comp_id_target" type="hidden" value="1437725343023632100">
                                <input id="readonly" type="hidden" value="1"> </a>&nbsp;
                            <?php
                            echo Html::a('<span class="glyphicon glyphicon-edit"></span>'
                                    , Url::to([
                                        '/teleradio/suspected/open-form',
                                        'ezf_id' => '1437377239070461301',
                                        'dataid' => $value['id_regis'],
                                        'ptid' => $value['ptid'],
                                    ])
                                    , [
                                'title' => Yii::t('app', 'เปิดเพื่อแก้ไข และดูภาพ'),
                                'target' => '_blank',
                            ]);
                            ?>
                        </th>
                        <th style="text-align: center;">
                            <?php echo $value['f1v3'] ?>
                        </th>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
<?php } ?>
<?php if ($state == 'cca02') { ?>
    <div class="col-md-12">
        <table class="table table-bordered">
            <thead class="table-default">
                <tr>
                    <th style="text-align: center;">No.</th>
                    <th style="text-align: center;">Site ID</th>
                    <th style="text-align: center;">Participant ID</th>
                    <th style="text-align: center;">Exam Date</th>
                    <th style="text-align: center;">Value</th>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($cca02List as $key => $value) {
                    ?>
                    <tr>
                        <th style="text-align: center;">
                            <?php echo ($key + 1) ?>
                        </th>
                        <th style="text-align: center;">
                            <?php echo $value['hsitecode'] ?>
                        </th>
                        <th style="text-align: center;">
                            <?php echo $value['hptcode'] ?>
                        </th>
                        <th  style="text-align: center;">
                            <a href='javascript:void(0)' id="ccaModal<?= $drilldown ?>" title="เปิดอ่านเท่านั้น"><?= date_format(date_create($value['f2v1']), "d/m/Y") ?>
                                <input id="ezf_id" type="hidden" value="1437619524091524800">
                                <input id="dataid" type="hidden" value="<?= $value['id_cca02'] ?>">
                                <input id="comp_target" type="hidden" value="1437725343023632100">
                                <input id="target" type="hidden" value="<?= base64_encode($value['ptid']) ?>">
                                <input id="comp_id_target" type="hidden" value="1437725343023632100">
                                <input id="readonly" type="hidden" value="1"> </a>&nbsp;
                            <?php
                            echo Html::a('<span class="glyphicon glyphicon-edit"></span>'
                                    , Url::to([
                                        '/teleradio/suspected/open-form',
                                        'ezf_id' => '1437619524091524800',
                                        'dataid' => $value['id_cca02'],
                                        'ptid' => $value['ptid'],
                                    ])
                                    , [
                                'title' => Yii::t('app', 'เปิดเพื่อแก้ไข และดูภาพ'),
                                'target' => '_blank',
                            ]);
                            ?>
                        </th>
                        <th style="text-align: center;">
                            <?php echo $value['data'] ?>
                        </th>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
<?php } ?>
<?php if ($state == 'cca01') { ?>
    <div class="col-md-12">
        <table class="table table-bordered">
            <thead class="table-default">
                <tr>
                    <th colspan="6"><?= $doctorname ?> </th>
                </tr>
                <tr>
                    <th style="text-align: center;">No.</th>
                    <th style="text-align: center;">Site ID</th>
                    <th style="text-align: center;">Participant ID</th>
                    <th style="text-align: center;">วันที่ Form Completed CCA-01 (Date)</th>
                    <th style="text-align: center;">Value</th>
                    <th style="text-align: center;">ผลการตรวจสอบ</th>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($cca01List as $key => $value) {
                    ?>
                    <tr>
                        <th style="text-align: center;">
                            <?php echo ($key + 1) ?>
                        </th>
                        <th style="text-align: center;">
                            <?php echo $value['hsitecode'] ?>
                        </th>
                        <th style="text-align: center;">
                            <?php echo $value['hptcode'] ?>
                        </th>
                        <th  style="text-align: center;">
                            <a href='javascript:void(0)' id="ccaModal<?= $drilldown ?>" title="เปิดอ่านเท่านั้น"><?= date_format(date_create($value['f2v1']), "d/m/Y") ?>
                                <input id="ezf_id" type="hidden" value="1437377239070461302">
                                <input id="dataid" type="hidden" value="<?= $value['id_cca02'] ?>">
                                <input id="comp_target" type="hidden" value="1437725343023632100">
                                <input id="target" type="hidden" value="<?= base64_encode($value['ptid']) ?>">
                                <input id="comp_id_target" type="hidden" value="1437725343023632100">
                                <input id="readonly" type="hidden" value="1"> </a>&nbsp;
                            <?php
                            echo Html::a('<span class="glyphicon glyphicon-edit"></span>'
                                    , Url::to([
                                        '/usfinding/monitoring/open-form',
                                        'ezf_id' => '1437377239070461302',
                                        'dataid' => $value['id_cca01'],
                                        'ptid' => $value['ptid'],
                                    ])
                                    , [
                                'title' => Yii::t('app', 'เปิดเพื่อแก้ไข และดูภาพ'),
                                'target' => '_blank',
                            ]);
                            ?>
                        </th>
                        <th style="text-align: center;">
                            <?php echo $value['data'] ?>
                        </th>
                        <th style="text-align: center;">
                            <?php
                            if ($value['confirm'] == 0)
                                echo "ไม่ผ่านการตรวจสอบ";
                            else if ($value['confirm'] > 0)
                                echo "ผ่านการตรวจสอบ";
                            else
                                echo "ข้อมูลยังไม่สมบูรณ์";
                            ?>
                        </th>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
<?php } ?>
<div id="cca-modal" class="modal fade" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-lg" role="document" style="width:90%">
        <div class="modal-content">
            <div class="modal-body" id="body-cca">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<?php
$this->registerJs("
$('tr').on('click','#ccaModal',function(){
    $('#body-cca').empty();
    var ezf_id = $(this).children('#ezf_id').val();
    var dataid = $(this).children('#dataid').val();
    var comp_target = $(this).children('#comp_target').val();
    var target = $(this).children('#target').val();
    var comp_id_target = $(this).children('#comp_id_target').val();
    var readonly = $(this).children('#readonly').val();
        
        $('#body-cca').html('<div style=\'text-align:center;color:#fff;\'><i class=\"fa fa-circle-o-notch fa-spin fa-fw fa-3x\"></i></div>');
         $.ajax({
                url:'" . Url::to('/inv/inv-person/ezform-print') . "',
                method:'GET',
                data:{
                    ezf_id : ezf_id,
                    dataid : dataid,
                    comp_target : comp_target,
                    target : target,
                    comp_id_target : comp_id_target,
                    readonly : readonly,
                },
                success:function(result){
                   $('#body-cca').html(result);
                   $('#cca-modal').modal();
                }
            });
    });
");
?>                      