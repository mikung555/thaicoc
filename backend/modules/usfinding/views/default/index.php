<!-- /*
 * Module US Finding.
 * Developed by Mark.
 * Date : 2016-03-01
*/ -->
<style>

</style>
<?php

use yii\helpers\Url;
use yii\jui\DatePicker;

//$this->title = Yii::t('', 'US Finding');
//echo Yii::$app->getRequest()->url;
if (Yii::$app->getRequest()->url == '/usfinding/default/usfinding') {
//    $this->registerCssFile('/css/usfinding.css');
//    $this->registerCssFile('/assets/931d41cc/css/bootstrap.css');
//    $this->registerCssFile('/assets/c77899e1/themes/smoothness/jquery-ui.css');
//    $this->registerCssFile('/assets/382c7ca3/css/font-awesome.min.css');
//    $this->registerCssFile('/assets/5da5963e/css/AdminLTE.min.css');
//    $this->registerCssFile('/assets/5da5963e/css/skins/_all-skins.min.css');
//    $this->registerCssFile('/assets/7da9ecf3/css/jquery.noty.css');
//    $this->registerCssFile('/assets/7da9ecf3/css/noty_theme_twitter.css');
    $this->registerCssFile('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css');
}

$loadIconData = '\'<i class="fa fa-spinner fa-spin fa-3x" style="margin:50px 0;"></i>\'';
$this->registerCssFile('/css/usfinding.css');
$this->registerJsFile('/js/jsdad/jquery.min.js', ['position' => \yii\web\View::POS_BEGIN]);
$this->registerJsFile('/js/FileSaver.js', ['position' => \yii\web\View::POS_BEGIN]);
$this->registerJsFile('/js/jquery.wordexport.js', ['position' => \yii\web\View::POS_BEGIN]);
$this->registerJsFile('/js/excellentexport.js', ['position' => \yii\web\View::POS_BEGIN]);
$this->registerJs('
    
    function getValueDiv(){
        $("[divReportUSFinding]").click(function(){
            $("#listPatient").html(\'<div class="row text-center"><i class="fa fa-spinner fa-spin fa-3x" style="margin:50px 0;"></i></div>\');
            var listPatientTopPosition = jQuery("#listPatient").offset().top;
            jQuery("html, body").animate({scrollTop:listPatientTopPosition}, "slow");
            
            var keystore = $(this).attr("keyStore");
            var startdate = $(this).attr("startdate");
            var enddate = $(this).attr("enddate");
            var zone = $(this).attr("zone");
            var province = $(this).attr("province");
            var amphur = $(this).attr("amphur");
            var hospital = $(this).attr("hospital");
            $.ajax({
                type    : "GET",
                cache   : false,
                url     : "' . Url::to('/usfinding/default/show-list-patient-report') . '",
                data    : {
                    keystore: keystore,
                    startdate: startdate,
                    enddate: enddate,
                    zone: zone,
                    province: province,
                    amphur: amphur,
                    hospital: hospital
                },
                success  : function(response) {
                    $("#listPatient").html(response);
                    exportToExcel();
                    var listPatientTopPosition = jQuery("#listPatient").offset().top;
                    jQuery("html, body").animate({scrollTop:listPatientTopPosition}, "slow");
                },
                error : function(){
                    $("#listPatient").html("");
                }
            });
        });
    }

    function getValueSpanResult(){
        $("[spanReportResultUSFinding]").click(function(){
            $("#listPatient").html(\'<div class="row text-center"><i class="fa fa-spinner fa-spin fa-3x" style="margin:50px 0;"></i></div>\');
            var listPatientTopPosition = jQuery("#listPatient").offset().top;
            jQuery("html, body").animate({scrollTop:listPatientTopPosition}, "slow");
            
            var keystore = $(this).attr("keyStore");
            var startdate = $(this).attr("startdate");
            var enddate = $(this).attr("enddate");
            var zone = $(this).attr("zone");
            var province = $(this).attr("province");
            var amphur = $(this).attr("amphur");
            var hospital = $(this).attr("hospital");
            $.ajax({
                type    : "GET",
                cache   : false,
                url     : "' . Url::to('/usfinding/default/show-list-patient-report-result') . '",
                data    : {
                    keystore: keystore,
                    startdate: startdate,
                    enddate: enddate,
                    zone: zone,
                    province: province,
                    amphur: amphur,
                    hospital: hospital
                },
                success  : function(response) {
                    $("#listPatient").html(response);
                    exportToExcel();
                    var listPatientTopPosition = jQuery("#listPatient").offset().top;
                    jQuery("html, body").animate({scrollTop:listPatientTopPosition}, "slow");
                },
                error : function(){
                    $("#listPatient").html("");
                }
            });
        });
    }
    
    function exportToExcel(){
        $(".exportToExcel").click(function(){
            var tableClone = $("#tableClone");
            var tableLP = $("#table-listPatiant");
            var copy = tableLP.clone();
            copy.attr(\'id\', \'tableClone\');
            tableClone.replaceWith(copy);
            $("#tableClone").hide();
            var usimg = $("img#usimage");
            var atag = $("a#atag");
            
            for(var i=0;i<usimg.length/2;i++){
                usimg[i].remove();
            }
            for(var i=0;i<atag.length/2;i++){
                atag[i].replaceWith(atag[i].text)
            }
            this.download = "usfinding-report.xls";
            ExcellentExport.excel(this,tableClone.attr(\'id\'), \'US Finding Report\');
 
        });
    }

    function resetTableResultUSFinding(){
        $("#listPatient").html("");
    }

    function getProvince(zoneCode,value){
        $.ajax({
            type    : "GET",
            cache   : false,
            url     : "' . Url::to('/usfinding/default/province') . '",
            data    : {
                zoneCode: zoneCode
            },
            success  : function(response) {
                $("#inputProvence").html(response);
                if(value!=""){
                    $("#inputProvence").val(value);
                }
            },
            error : function(){
                $("#inputProvence").html("");
            }
        });
    }

    function getAmphur(provinceCode,value){
        $.ajax({
            type    : "GET",
            cache   : false,
            url     : "' . Url::to('/usfinding/default/amphur') . '",
            data    : {
                provinceCode: provinceCode
            },
            success  : function(response) {
                $("#inputAmphur").html(response);
                if(value!=""){
                    $("#inputAmphur").val(value);
                }
            },
            error : function(){
                $("#inputAmphur").html("");
            }
        });
    }

    function getHospital(provinceCode,amphurCode, value){
        $.ajax({
            type    : "GET",
            cache   : false,
            url     : "' . Url::to('/usfinding/default/all-hospital-thai') . '",
            data    : {
                provinceCode: provinceCode,
                amphurCode: amphurCode
            },
            success  : function(response) {
                $("#inputHospital").html(response);
                if(value!=""){
                    $("#inputHospital").val(value);
                }
            },
            error : function(){
                $("#inputHospital").html("");
            }
        });
    }
', yii\web\View::POS_BEGIN);
$this->registerJs('
    $("#inputZone").change(function(){
        if( $("#inputZone").val() != "" ){
            getProvince($("#inputZone").val(),"");
        }
    });

    $("#inputProvence").change(function(){
        if( $("#inputProvence").val() != "" ){
            getAmphur($("#inputProvence").val(),"");
        }
    });

    $("#inputAmphur").change(function(){
        if( $("#inputProvence").val()!="" && $("#inputAmphur").val()!="" ){
            getHospital($("#inputProvence").val(),$("#inputAmphur").val(),"");
        }
    });

    $("#btnShowReportByZone").click(function(){
        var reportUSFindingTopPosition = jQuery("#reportUSFinding").offset().top;
        jQuery("html, body").animate({scrollTop:reportUSFindingTopPosition}, "slow");
        var startDate = $("#inputStartDate").val();
        var endDate = $("#inputEndDate").val();
        var zoneCode = $("#inputZone").val();
        if( zoneCode==""){
            alert("กรุณาเลือก เขต");
        }else if((startDate != "") && (endDate != "") && (zoneCode != "")){
            $("#reportUSFinding").html(\'<div class="row text-center"><i class="fa fa-spinner fa-spin fa-3x" style="margin:50px 0;"></i></div>\');
            $.ajax({
                type    : "GET",
                cache   : false,
                url     : "' . Url::to('/usfinding/default/show-report') . '",
                data    : {
                    startDate: startDate,
                    endDate: endDate,
                    zoneCode: zoneCode
                },
                success  : function(response) {
                    $("#reportUSFinding").html(response);
                    getValueDiv();
                    getValueSpanResult();
                    resetTableResultUSFinding();

                    var reportUSFindingTopPosition = jQuery("#reportUSFinding").offset().top;
                    jQuery("html, body").animate({scrollTop:reportUSFindingTopPosition}, "slow");
                },
                error : function(){
                    $("#reportUSFinding").html("Error");
                }
            });
        }
    });

    $("#btnShowReportByProvince").click(function(){
        var reportUSFindingTopPosition = jQuery("#reportUSFinding").offset().top;
        jQuery("html, body").animate({scrollTop:reportUSFindingTopPosition}, "slow");
        var startDate = $("#inputStartDate").val();
        var endDate = $("#inputEndDate").val();
        var provinceCode = $("#inputProvence").val();
        if( provinceCode == "" ){
            alert("กรุณาเลือกจังหวัด");
        }else if((startDate != "") && (endDate != "") && (provinceCode != "")){
            $("#reportUSFinding").html(\'<div class="row text-center"><i class="fa fa-spinner fa-spin fa-3x" style="margin:50px 0;"></i></div>\');
            $.ajax({
                type    : "GET",
                cache   : false,
                url     : "' . Url::to('/usfinding/default/show-report') . '",
                data    : {
                    startDate: startDate,
                    endDate: endDate,
                    provinceCode: provinceCode
                },
                success  : function(response) {
                    $("#reportUSFinding").html(response);
                    getValueDiv();
                    getValueSpanResult();
                    resetTableResultUSFinding();

                    var reportUSFindingTopPosition = jQuery("#reportUSFinding").offset().top;
                    jQuery("html, body").animate({scrollTop:reportUSFindingTopPosition}, "slow");
                },
                error : function(){
                    $("#reportUSFinding").html("Error");
                }
            });
        }
    });

    $("#btnShowReportByAmphur").click(function(){
        var reportUSFindingTopPosition = jQuery("#reportUSFinding").offset().top;
        jQuery("html, body").animate({scrollTop:reportUSFindingTopPosition}, "slow");
        var startDate = $("#inputStartDate").val();
        var endDate = $("#inputEndDate").val();
        var provinceCode = $("#inputProvence").val();
        var amphurCode = $("#inputAmphur").val();
        if( amphurCode == "" ){
            alert("กรุณาเลือก อำเภอที่ต้องการดูรายงาน ");
        }else if((startDate != "") && (endDate != "") && (provinceCode != "") && (amphurCode != "")){
            $("#reportUSFinding").html(\'<div class="row text-center"><i class="fa fa-spinner fa-spin fa-3x" style="margin:50px 0;"></i></div>\');
            $.ajax({
                type    : "GET",
                cache   : false,
                url     : "' . Url::to('/usfinding/default/show-report') . '",
                data    : {
                    startDate: startDate,
                    endDate: endDate,
                    provinceCode: provinceCode,
                    amphurCode: amphurCode
                },
                success  : function(response) {
                    $("#reportUSFinding").html(response);
                    getValueDiv();
                    getValueSpanResult();
                    resetTableResultUSFinding();

                    var reportUSFindingTopPosition = jQuery("#reportUSFinding").offset().top;
                    jQuery("html, body").animate({scrollTop:reportUSFindingTopPosition}, "slow");
                },
                error : function(){
                    $("#reportUSFinding").html("Error");
                }
            });
        }
    });

    function showReportWithChangeOrClick(){
        var reportUSFindingTopPosition = jQuery("#reportUSFinding").offset().top;
        jQuery("html, body").animate({scrollTop:reportUSFindingTopPosition}, "slow");
        var startDate = $("#inputStartDate").val();
        var endDate = $("#inputEndDate").val();
        var provinceCode = $("#inputProvence").val();
        var amphurCode = $("#inputAmphur").val();
        var hospitalCode = $("#inputHospital").val();
        if( hospitalCode == ""){
            // "";
            alert("กรุณาเลือก โรงพยาบาล");
        }else if((startDate != "") && (endDate != "") && (provinceCode != "") && (amphurCode != "") && (hospitalCode != "")){
            $("#reportUSFinding").html(\'<div class="row text-center"><i class="fa fa-spinner fa-spin fa-3x" style="margin:50px 0;"></i></div>\');
            $.ajax({
                type    : "GET",
                cache   : false,
                url     : "' . Url::to('/usfinding/default/show-report') . '",
                data    : {
                    startDate: startDate,
                    endDate: endDate,
                    provinceCode: provinceCode,
                    amphurCode: amphurCode,
                    hospitalCode: hospitalCode
                },
                success  : function(response) {
                    $("#reportUSFinding").html(response);
                    getValueDiv();
                    getValueSpanResult();
                    resetTableResultUSFinding();

                    var reportUSFindingTopPosition = jQuery("#reportUSFinding").offset().top;
                    jQuery("html, body").animate({scrollTop:reportUSFindingTopPosition}, "slow");
                },
                error : function(){
                    $("#reportUSFinding").html("Error");
                }
            });
        }
    }

    $("#btnShowReport").click(function(){
        showReportWithChangeOrClick();
    });

    $("#inputHospital").change(function(){
        showReportWithChangeOrClick();
    });

    $("#selectUsTour").change(function(){
        $("#reportUSFinding").html(\'<div class="row text-center"><i class="fa fa-spinner fa-spin fa-3x" style="margin:50px 0;"></i></div>\');
        var reportUSFindingTopPosition = jQuery("#reportUSFinding").offset().top;
        jQuery("html, body").animate({scrollTop:reportUSFindingTopPosition}, "slow");
        var hSiteCode = $("#selectUsTour").val();
        //var times = $("#selectUsTour").val(allValue.attr("times"));
        //alert(times);
        if(hSiteCode!=""){
            var thisOption = $(this).val();
            var allValue = $(this).children("option[value=\'"+thisOption+"\']");

            //$("#inputStartDate").val(allValue.attr("sdate").split(" ")[0]);
            //$("#inputEndDate").val(allValue.attr("edate").split(" ")[0]);
            $("#inputStartDate").val(allValue.attr("sdatex").split(" ")[0]);
            $("#inputEndDate").val(allValue.attr("edatex").split(" ")[0]);

            $("#inputZone").val(allValue.attr("zonecode"));

            getProvince(allValue.attr("zonecode"),allValue.attr("provcode"));
            getAmphur(allValue.attr("provcode"), allValue.attr("ampcode"));
            getHospital(allValue.attr("provcode"), allValue.attr("ampcode"), allValue.attr("hsitecode"));

            $.ajax({
                type    : "GET",
                cache   : false,
                url     : "' . Url::to('/usfinding/default/show-report-in-us-tour') . '",
                data    : {
                    hSiteCode: hSiteCode,
                },
                success  : function(response) {
                    $("#reportUSFinding").html(response);
                    getValueDiv();
                    getValueSpanResult();
                    resetTableResultUSFinding();

                    var reportUSFindingTopPosition = jQuery("#reportUSFinding").offset().top;
                    jQuery("html, body").animate({scrollTop:reportUSFindingTopPosition}, "slow");
                },
                error : function(){
                    $("#reportUSFinding").html("Error");
                }
            });
        }
    });
    $("#selectSiteUSDist").change(function(){
        $("#reportUSFinding").html(\'<div class="row text-center"><i class="fa fa-spinner fa-spin fa-3x" style="margin:50px 0;"></i></div>\');
        var reportUSFindingTopPosition = jQuery("#reportUSFinding").offset().top;
        jQuery("html, body").animate({scrollTop:reportUSFindingTopPosition}, "slow");
        var hSiteCode = $("#selectSiteUSDist").val();
        if(hSiteCode!=""){
            var thisOption = $(this).val();
            var allValue = $(this).children("option[value=\'"+thisOption+"\']");

            //$("#inputStartDate").val(allValue.attr("sdate").split(" ")[0]);
            //$("#inputEndDate").val(allValue.attr("edate").split(" ")[0]);
            $("#inputStartDate").val(allValue.attr("sdatex").split(" ")[0]);
            $("#inputEndDate").val(allValue.attr("edatex").split(" ")[0]);

            $("#inputZone").val(allValue.attr("zonecode"));
            getProvince(allValue.attr("zonecode"),allValue.attr("provcode"));
            getAmphur(allValue.attr("provcode"), allValue.attr("ampcode"));
            getHospital(allValue.attr("provcode"), allValue.attr("ampcode"), allValue.attr("hsitecode"));
            $.ajax({
                type    : "GET",
                cache   : false,
                url     : "' . Url::to('/usfinding/default/show-report-in-us-site') . '",
                data    : {
                    hSiteCode: hSiteCode
                },
                success  : function(response) {
                    $("#reportUSFinding").html(response);
                    getValueDiv();
                    getValueSpanResult();
                    resetTableResultUSFinding();

                    var reportUSFindingTopPosition = jQuery("#reportUSFinding").offset().top;
                    jQuery("html, body").animate({scrollTop:reportUSFindingTopPosition}, "slow");
                },
                error : function(){
                    $("#reportUSFinding").html("Error selectSiteUSDist");
                }
            });
        }
        
    });
    
    $(document).on("click", "*[id^=ovlistpatientreport]", function() {
    //alert("x");
        //console.log("x");
        $("#listPatient").html(\'<div class="row text-center"><i class="fa fa-spinner fa-spin fa-3x" style="margin:50px 0;"></i></div>\');
        var listPatientTopPosition = jQuery("#listPatient").offset().top;
        jQuery("html, body").animate({scrollTop:listPatientTopPosition}, "slow");

        var keystore = $(this).attr("keyStore");
        var startdate = $(this).attr("startdate");
        var enddate = $(this).attr("enddate");
        var zone = $(this).attr("zone");
        var province = $(this).attr("province");
        var amphur = $(this).attr("amphur");
        var hospital = $(this).attr("hospital");
        var ovuhospital = $(this).attr("ovuhospital");

        //console.log(keystore);

        $.ajax({
            type    : "GET",
            cache   : false,
            url     : "' . Url::to('/usfinding/default/show-list-patient-report') . '",
            data    : {
                keystore: keystore,
                startdate: startdate,
                enddate: enddate,
                zone: zone,
                province: province,
                amphur: amphur,
                hospital: hospital,
                ovuhospital:ovuhospital,
            },
            success  : function(response) {
                //console.log(response);
                $("#listPatient").html(response);
                exportToExcel();
                var listPatientTopPosition = jQuery("#listPatient").offset().top;
                jQuery("html, body").animate({scrollTop:listPatientTopPosition}, "slow");
            },
            error : function(){
                $("#listPatient").html("");
            }
        });
    });
    
    $("#btnUsSummary").click(function(){
        showSummaryReport();
    });
    
    $("#btnCca01").click(function(){
        showCca01Report();
    });
    
    function showSummaryReport(){
        var reportUSFindingTopPosition = jQuery("#reportUSFinding").offset().top;
        jQuery("html, body").animate({scrollTop:reportUSFindingTopPosition}, "slow");
        var startDate = $("#inputStartDate").val();
        var endDate = $("#inputEndDate").val();
        var zoneCode = $("#inputZone").val();
        var provinceCode = $("#inputProvence").val();
        var amphurCode = $("#inputAmphur").val();
        var hospitalCode = $("#inputHospital").val();
    //    console.log(startDate+" | "+endDate+" | "+zoneCode+" | "+provinceCode+" | "+amphurCode+" | "+hospitalCode);
        if( hospitalCode == ""){
            // "";
            alert("กรุณาเลือก โรงพยาบาล");
        }else if((startDate != "") && (endDate != "") && (provinceCode != "") && (amphurCode != "") && (hospitalCode != "")){
            $("#reportUSFinding").html(\'<div class="row text-center"><i class="fa fa-spinner fa-spin fa-3x" style="margin:50px 0;"></i></div>\');
            $.ajax({
                type    : "GET",
                cache   : false,
                url     : "' . Url::to('/usfinding/default/summary-report') . '",
                data    : {
                    startDate: startDate,
                    endDate: endDate,
                    zoneCode: zoneCode,
                    provinceCode: provinceCode,
                    amphurCode: amphurCode,
                    hospitalCode: hospitalCode
                },
                success  : function(response) {
                    $("#reportUSFinding").html(response);
                   

                    var reportUSFindingTopPosition = jQuery("#reportUSFinding").offset().top;
                    jQuery("html, body").animate({scrollTop:reportUSFindingTopPosition}, "slow");
                },
                error : function(){
                    $("#reportUSFinding").html("Error");
                }
            });
        } 
    }
    
    function showCca01Report(){
        var reportUSFindingTopPosition = jQuery("#reportUSFinding").offset().top;
        jQuery("html, body").animate({scrollTop:reportUSFindingTopPosition}, "slow");
        var startDate = $("#inputStartDate").val();
        var endDate = $("#inputEndDate").val();
        var zoneCode = $("#inputZone").val();
        var provinceCode = $("#inputProvence").val();
        var amphurCode = $("#inputAmphur").val();
        var hospitalCode = $("#inputHospital").val();
     //   console.log(startDate+" | "+endDate+" | "+zoneCode+" | "+provinceCode+" | "+amphurCode+" | "+hospitalCode);
        if( hospitalCode == ""){
            // "";
            alert("กรุณาเลือก โรงพยาบาล");
        }else if((startDate != "") && (endDate != "") && (provinceCode != "") && (amphurCode != "") && (hospitalCode != "")){
            $("#reportUSFinding").html(\'<div class="row text-center"><i class="fa fa-spinner fa-spin fa-3x" style="margin:50px 0;"></i></div>\');
            $.ajax({
                type    : "GET",
                cache   : false,
                url     : "' . Url::to('/usfinding/default/cca01-report') . '",
                 data    : {
                    startDate: startDate,
                    endDate: endDate,
                    zoneCode: zoneCode,
                    provinceCode: provinceCode,
                    amphurCode: amphurCode,
                    hospitalCode: hospitalCode
                },
                success  : function(response) {
                    $("#reportUSFinding").html(response);
            

                    var reportUSFindingTopPosition = jQuery("#reportUSFinding").offset().top;
                    jQuery("html, body").animate({scrollTop:reportUSFindingTopPosition}, "slow");
                },
                error : function(){
                    $("#reportUSFinding").html("Error");
                }
            });
        }
    }
');

if (0) {
    echo "<pre align='left'>";
    //print_r($_GET);

    print_r($dfUSFinding);
    echo "</pre>";
}
?>
<?php 

$userId = \Yii::$app->user->identity->userProfile->user_id;
if($userId == "1435745159010043375" || $userId == "149362211072317500" || $userId == "1435745159010043377"){
?>
<ul class="nav nav-pills nav-justified">
    <li id="tab-one" style="font-size:18px;" class="active"><a  href="<?= Url::to(['/usfinding/']) ?>" id="tab1"><i class="fa fa-line-chart fa-lg"></i> US Finding</a></li>
     <li id="tab-three" style="font-size:18px;"><a  href="<?= Url::to(['/usfinding/worklist']) ?>" id="tab3"><i class="glyphicon glyphicon-tasks fa-lg"></i> Worklist</a></li>
    <li id="tab-two" style="font-size:18px;" ><a  href="<?= Url::to(['/usfinding/monitoring']) ?>" id="tab2"><i class="fa fa-desktop fa-lg"></i> Monitoring System</a></li>
   
</ul>
<?php }?>
<div class="row formUsFinding">
    <h1><p class="text-center">US Finding</p></h1>
    <form id="formUSFindingReport" class="form-horizontal">
        <div class="col-md-6">
            <h3><p class="text-center">ทำการคัดกรองระหว่างวันที่</p></h3>
            <!--div class="form-group">
                <label for="inputStartDate" class="col-sm-3 col-md-3 control-label">เริ่มวันที่</label>
                <div class="col-sm-9 col-md-9">
                    <input type="date" class="form-control" id="inputStartDated" min="2013-02-09" max="<?= date("Y-m-d"); ?>" value="<?= $dfUSFinding['inputStartDate']; //2013-02-09  ?>" required >
                </div>
                
            </div-->
            <div class="form-group">
                <label for="inputStartDate" class="col-sm-3 col-md-3 control-label">เริ่มวันที่</label>
                <div class="col-sm-9 col-md-9">
                    <?php
                    echo DatePicker::widget([
                        'id' => 'inputStartDate',
                        'name' => 'inputStartDate',
                        'language' => 'th',
                        'dateFormat' => 'dd/MM/yyyy',
                        'value' => Yii::$app->formatter->asDate($dfUSFinding['inputStartDate'], 'php:d/m/Y'),
                        'options' => [
                            'class' => 'form-control',
                        ],
                        'clientOptions' => [
                            'defaultDate' => Yii::$app->formatter->asDate('now', 'php:d/m/Y'),
                            'minDate' => '09/02/2013',
                            'maxDate' => Yii::$app->formatter->asDate('now', 'php:d/m/Y'),
                        ],
                    ]);
                    ?>
                </div>
            </div>
            <!--div class="form-group">
                <label for="inputEndDate" class="col-sm-3 col-md-3 control-label">ถึงวันที่</label>
                <div class="col-sm-9 col-md-9">
                    <input type="date" class="form-control" id="inputEndDate" min="2013-02-09" max="<?= date("Y-m-d") ?>" value="<?= $dfUSFinding['inputEndDate']; //date("Y-m-d")  ?>" required>
                </div>
            </div-->
            <div class="form-group">
                <label for="inputEndDate" class="col-sm-3 col-md-3 control-label">ถึงวันที่</label>
                <div class="col-sm-9 col-md-9">
<?php
echo DatePicker::widget([
    'id' => 'inputEndDate',
    'name' => 'inputEndDate',
    'language' => 'th',
    'dateFormat' => 'dd/MM/yyyy',
    'value' => Yii::$app->formatter->asDate($dfUSFinding['inputEndDate'], 'php:d/m/Y'),
    'options' => [
        'class' => 'form-control',
    ],
    'clientOptions' => [
        'defaultDate' => Yii::$app->formatter->asDate('now', 'php:d/m/Y'),
        'minDate' => '09/02/2013',
        'maxDate' => Yii::$app->formatter->asDate('now', 'php:d/m/Y'),
    ],
]);
?>
                </div>
            </div>
            <div class="form-group">
                <label for="inputZone" class="col-sm-3 col-md-3 control-label">เขต</label>
                <div class="col-sm-5 col-md-5">
                    <select class="form-control" id="inputZone">
                        <option value="">เลือกเขต</option>
                        <option value="0">ทุกเขต</option>
<?php
foreach ($zone as $item) {
    if ($dfUSFinding['zone_code'] == $item['zone_code']) {
        echo '<option value="' . $item['zone_code'] . '" selected>เขต ' . $item['zone_code'] . ' : ' . $item['zone_name'] . '</option>';
    } else {
        echo '<option value="' . $item['zone_code'] . '">เขต ' . $item['zone_code'] . ' : ' . $item['zone_name'] . '</option>';
    }
}
?>
                    </select>
                </div>
                <div class="col-sm-4 col-md-4">
                    <button type="button" class="btn btn-default form-control" id="btnShowReportByZone">แสดงรายงานในเขต</button>
                </div>
            </div>
            <div class="form-group">
                <label for="inputProvence" class="col-sm-3 col-md-3 control-label">จังหวัด</label>
                <div class="col-sm-5 col-md-5">
                    <select class="form-control" id="inputProvence">
                        <option value="">เลือกจังหวัด</option>
<?php
foreach ($province as $item) {
    if ($dfUSFinding['provincecode'] == $item['PROVINCE_CODE']) {
        echo '<option value="' . $item['PROVINCE_CODE'] . '" selected>' . trim($item['PROVINCE_NAME']) . '</option>';
    } else {
        echo '<option value="' . $item['PROVINCE_CODE'] . '">' . trim($item['PROVINCE_NAME']) . '</option>';
    }
}
?>
                    </select>
                </div>
                <div class="col-sm-4 col-md-4">
                    <button type="button" class="btn btn-default form-control" id="btnShowReportByProvince">แสดงรายงานในเขตจังหวัด</button>
                </div>
            </div>
            <div class="form-group">
                <label for="inputAmphur" class="col-sm-3 col-md-3 control-label">อำเภอ</label>
                <div class="col-sm-5 col-md-5">
                    <select class="form-control" id="inputAmphur" required>
                        <option value="">เลือกอำเภอ</option>
<?php
if (count($dfUSFinding['amphurlist']) > 0) {
    foreach ($dfUSFinding['amphurlist'] as $key => $value) {
        ?>
                                <option value="<?= $dfUSFinding['provincecode'] . $dfUSFinding['amphurlist'][$key]['amphurcode']; ?>" <?php if ($dfUSFinding['amphurcode'] == $dfUSFinding['amphurlist'][$key]['amphurcode']) echo "selected"; ?> >
                                <?= $dfUSFinding['amphurlist'][$key]['amphur']; ?>
                                </option>
                                    <?php
                                }
                            }
                            ?>
                    </select>
                </div>
                <div class="col-sm-4 col-md-4">
                    <button type="button" class="btn btn-default form-control" id="btnShowReportByAmphur">แสดงรายงานในเขตอำเภอ</button>
                </div>
            </div>
            <div class="form-group">
                <label for="inputHospital" class="col-sm-3 col-md-3 control-label">หน่วยบริการ</label>
                <div class="col-sm-9 col-md-9">
                    <select class="form-control" id="inputHospital" required>
                        <option value="">เลือกหน่วยบริการ</option>
<?php
if (count($dfUSFinding['hospitallist']) > 0) {
    foreach ($dfUSFinding['hospitallist'] as $key => $value) {
        ?>
                                <option value="<?= $dfUSFinding['hospitallist'][$key]['hcode']; ?>" <?php if ($dfUSFinding['sitecode'] == $dfUSFinding['hospitallist'][$key]['hcode']) echo "selected"; ?> >
                                <?= $dfUSFinding['hospitallist'][$key]['hcode'] . ": " . $dfUSFinding['hospitallist'][$key]['name']; ?>
                                </option>
                                    <?php
                                }
                            }
                            ?>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label for="inputHospital" class="col-sm-3 col-md-3 control-label"></label>
                <div class="col-sm-9 col-md-9">
                    <button type="button" class="btn btn-primary form-control" id="btnShowReport">Show report</button>
                </div>
            </div>
            <?php if($userId == "1435745159010043375" || $userId == "149362211072317500" || $userId == "1435745159010043377") { ?>
            <div class="form-group">
                    <label class="col-sm-3 col-md-3 control-label"></label>
                    <div class="col-sm-3 col-md-3">
                        <button type="button" class="btn btn-primary form-control" id="btnUsSummary">US Summary</button>
                    </div>
                    <div class="col-sm-3 col-md-3">
                        <button type="button" class="btn btn-primary form-control" id="btnCca01">CCA01</button>
                    </div>
                </div>
            <?php } ?>
        </div>
        <div class="col-md-6">
<?php
if (0) {
    echo "<pre align='left'>";
    print_r($usTour);
    echo "</pre>";
}
?>
            <h3><p class="text-center">เลือกตามการออกสัญจร</p></h3>
            <div class="form-group">
                <label for="selectUsTour" class="col-sm-3 col-md-3 control-label">การออกสัญจร</label>
                <div class="col-sm-9 col-md-9">
                    <select class="form-control" id="selectUsTour" >
                        <option value="">สามารถเลือกได้ตามครั้งการออกสัญจร</option>
<?php
foreach ($usTour as $item) {
    echo '<option value="' . $item['hcode'] . ':' . $item['times'];
    echo '" times="' . $item['times'];
    echo '" hsitecode="' . $item['hcode'];
    echo '" zonecode="' . $item['zonecode'];
    echo '" provcode="' . $item['provcode'];
    echo '" ampcode="' . $item['ampcode'];
    echo '" sdate="' . $item['sdate'];
    echo '" sdatex="' . Yii::$app->formatter->asDate($item['sdate'], 'php:d/m/Y');
    echo '" edate="' . $item['edate'];
    echo '" edatex="' . Yii::$app->formatter->asDate($item['edate'], 'php:d/m/Y');
    echo '">';
    echo iconv('tis620', 'UTF-8', $item['name']);
    echo '</option>';
}
?>
                    </select>
                </div>
            </div>
<?php
if (Yii::$app->user->can('doctorcascap') == TRUE || Yii::$app->user->can('administrator') == TRUE || Yii::$app->user->can('sitemanager') == TRUE
):
    ?>
                <h3><p class="text-center">หน่วยบริการที่ได้รับสนับสนุนเครื่อง US</p></h3>
                <div class="form-group">
                    <label for="selectSiteUSDist" class="col-sm-3 col-md-3 control-label">หน่วยบริการ</label>
                    <div class="col-sm-9 col-md-9">
                        <select class="form-control" id="selectSiteUSDist" >
                            <option value="">เลือกหน่วยบริการ</option>
    <?php
    $irow = 1;
    foreach ($usSite as $item) {
        echo '<option value="' . $item['hcode'] . ':' . $item['No'];
        echo '" hsitecode="' . $item['hcode'];
        echo '" zonecode="' . $item['zonecode'];
        echo '" provcode="' . $item['provcode'];
        echo '" ampcode="' . $item['ampcode'];
        echo '" sdate="' . $item['dateatsite'];
        echo '" sdatex="' . Yii::$app->formatter->asDate($item['dateatsite'], 'php:d/m/Y');
        echo '" edate="' . substr($item['edate'], 0, 10);
        echo '" edatex="' . Yii::$app->formatter->asDate(substr($item['edate'], 0, 10), 'php:d/m/Y');
        echo '">';
        echo $irow . '. ' . $item['hcode'] . ': ' . $item['hospitalname'] . ' ';
        echo '</option>';

        $irow++;
    }
    ?>
                        </select>
                    </div>
                </div>
                <div style="alignment-adjust: right;">
                    <center>
                        <button style="alignment-adjust: central;" type="button" class="btn btn-info" onclick="javascript:window.open('/teleradio/siteus/list', '_self')">
                            <i class="fa fa-file-excel-o" aria-hidden="true" style="color: green"></i>
                            แสดงสถิติภาพรวม
                        </button>

                        <button style="alignment-adjust: central;" type="button" class="btn btn-info" onclick="javascript:window.open('/teleradio/suspected/list-suspected', '_self')">
                            <i class="glyphicon glyphicon-pushpin" aria-hidden="true" style="color: red"></i>
                            แสดงข้อมูล Suspected ทั้งหมด
                        </button>

                        <button style="alignment-adjust: central;" type="button" class="btn btn-info" onclick="javascript:window.open('/teleradio/suspected/data', '_blank')">
                            <i class="glyphicon glyphicon-compressed" aria-hidden="true" style="color: blue"></i>
                            จัดการข้อมูล
                        </button>
                    </center>
                </div>
    <?php
endif;
?>
        </div>
    </form>
</div>
<div class="allReport" id="allReport">
    <div class="row reportUSFinding" id="reportUSFinding">
        <!--    <div class="valueOfUltrasonoTableFinding">-->
        <!--        <div class="tableReportUltrasonoTableFinding">-->
        <!--            <table border="1" class="table-hover reportUltrasonoTableFinding">-->
        <!--                <thead class="tHeadReportUltrasonoTableFinding">-->
        <!--                <tr>-->
        <!--                    <th rowspan="2">รูปแบบที่</th>-->
        <!--                    <th colspan="3">ครั้งที่ตรวจ</th>-->
        <!--                    <th rowspan="2">จำนวน</th>-->
        <!--                </tr>-->
        <!--                <tr>-->
        <!--                    <th>1</th>-->
        <!--                    <th>2</th>-->
        <!--                    <th>3</th>-->
        <!--                </tr>-->
        <!--                </thead>-->
        <!--                <tbody class="tBodyReportUltrasonoTableFinding">-->
        <!--                <tr>-->
        <!--                    <td>1</td>-->
        <!--                    <td>-</td>-->
        <!--                    <td>-</td>-->
        <!--                    <td>-</td>-->
        <!--                    <td>-</td>-->
        <!--                </tr>-->
        <!--                <tr>-->
        <!--                    <td>2</td>-->
        <!--                    <td>-</td>-->
        <!--                    <td>-</td>-->
        <!--                    <td>-</td>-->
        <!--                    <td>-</td>-->
        <!--                </tr>-->
        <!--                <tr>-->
        <!--                    <td>3</td>-->
        <!--                    <td>-</td>-->
        <!--                    <td>-</td>-->
        <!--                    <td>-</td>-->
        <!--                    <td>-</td>-->
        <!--                </tr>-->
        <!--                </tbody>-->
        <!--            </table>-->
        <!--        </div>-->
        <!--        <div class="annotation">-->
        <!--            <dl class="dl-horizontal">-->
        <!--                <dt>No :</dt><dd>Normal</dd>-->
        <!--                <dt>F1 :</dt><dd>Mild fatty liver (Abnormal)</dd>-->
        <!--                <dt>F2 :</dt><dd>Moderate fatty liver (Abnormal)</dd>-->
        <!--                <dt>F3 :</dt><dd>Severe fatty liver (Abnormal)</dd>-->
        <!--                <dt>P1 :</dt><dd>PDF1 (Abnormal)</dd>-->
        <!--                <dt>P2 :</dt><dd>PDF2 (Abnormal)</dd>-->
        <!--                <dt>P3 :</dt><dd>PDF3 (Abnormal)</dd>-->
        <!--                <dt> C :</dt><dd>Cirrhosis (Abnormal)</dd>-->
        <!--                <dt>Pa :</dt><dd>Parenchymal change (Abnormal)</dd>-->
        <!--            </dl>-->
        <!--        </div>-->
        <!--    </div>-->
    </div>
    <div class="row listPatientOfUltrasonoGraphicFinding" id="listPatientOfUltrasonoGraphicFinding">
        <div class="btnControl"></div>
        <div class="listPatient" id="listPatient"></div>
    </div>
</div>
