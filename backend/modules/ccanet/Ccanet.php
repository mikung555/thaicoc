<?php

namespace backend\modules\ccanet;

class Ccanet extends \yii\base\Module
{
    public $controllerNamespace = 'backend\modules\ccanet\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
