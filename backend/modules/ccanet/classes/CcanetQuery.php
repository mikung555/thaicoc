<?php
namespace backend\modules\ccanet\classes;

use Yii;

/**
 * newPHPClass class file UTF-8
 * @author SDII <iencoded@gmail.com>
 * @copyright Copyright &copy; 2015 AppXQ
 * @license http://www.appxq.com/license/
 * @version 1.0.0 Date: 4 พ.ย. 2559 7:53:01
 * @link http://www.appxq.com/
 * @example
 */
class CcanetQuery {

   

    public static function getDbConfig($sitecode) {
	$sql = "SELECT db_config_province.province,
			db_config_province.zonecode,
			db_config_province.`server`,
			db_config_province.`user`,
			db_config_province.passwd,
			db_config_province.`port`,
			db_config_province.db,
			db_config_province.webservice
		FROM db_config_province INNER JOIN all_hospital_thai ON db_config_province.province = all_hospital_thai.provincecode
		WHERE all_hospital_thai.hcode = :sitecode
		";
	return Yii::$app->dbbot_ip8->createCommand($sql, [':sitecode'=>$sitecode])->queryOne();
    }

    public static function getColumn($table) {
	$sql = "SELECT COLUMN_NAME AS `column` FROM INFORMATION_SCHEMA.COLUMNS
		WHERE TABLE_NAME = :tbname AND table_schema = :tbschema";
	return CcanetFunc::queryAll($sql, [':tbname' => $table, ':tbschema' => 'tdc_data']);
    }

    public static function genSelect() {
        $convert = isset(Yii::$app->session['convert_ckd']) ? Yii::$app->session['convert_ckd'] : 0;
        $key = isset(Yii::$app->session['key_ckd']) ?Yii::$app->session['key_ckd'] : '';

        $selectKey = "`f_person`.HOSPCODE,
                `f_person`.PID,
                `f_person`.sitecode,
                `f_person`.ptlink,
                `f_person`.CID,
                `f_person`.`Pname`,
                `f_person`.`Name`,
                `f_person`.`Lname`";

        if($key!=''){
            $selectKey = "`f_person`.hospcode,
                `f_person`.pid,
                `f_person`.sitecode,
                `f_person`.ptlink,
                decode(unhex(f_person.CID),sha2('$key',256)) AS CID,
                decode(unhex(f_person.Pname),sha2('$key',256)) AS Pname,
                decode(unhex(f_person.`Name`),sha2('$key',256)) AS Name,
                decode(unhex(f_person.Lname),sha2('$key',256)) AS Lname";

            if ($convert==1) {
                $selectKey = "`f_person`.hospcode,
                `f_person`.pid,
                `f_person`.sitecode,
                `f_person`.ptlink,
                convert(decode(unhex(f_person.CID),sha2('$key',256)) using tis620) AS CID,
                convert(decode(unhex(f_person.Pname),sha2('$key',256)) using tis620) AS Pname,
                convert(decode(unhex(f_person.`Name`),sha2('$key',256)) using tis620) AS Name,
                convert(decode(unhex(f_person.Lname),sha2('$key',256)) using tis620) AS Lname";

            }
        }

        return $selectKey;
    }
    public static function getAll($sitecode, $sdate, $edate, $dp=false) {
	$sql = "SELECT
                    COUNT(*)
                FROM
                    `patient_profile_hospital`
                WHERE
                    `hospcode` = :sitecode
                ";
        if($dp){
            $selectKey = self::genSelect();

            $sqlDp = "SELECT
                    $selectKey
                FROM
                    `patient_profile_hospital`
                    INNER JOIN `f_person`
                    ON `patient_profile_hospital`.`hospcode` = `f_person`.`HOSPCODE`
                    AND `patient_profile_hospital`.`pid` = `f_person`.`PID`
                WHERE
                    `patient_profile_hospital`.`hospcode` = :sitecode
                    AND `f_person`.`sitecode` = :sitecode
                ";

            return [
                'sql' =>$sqlDp,
                'param'=>[':sitecode'=>$sitecode],
                'keyID'=>'ptlink',
                'total'=>CcanetFunc::queryScalar($sql, [':sitecode'=>$sitecode]),
            ];
        }

	return CcanetFunc::queryScalar($sql, [':sitecode'=>$sitecode]);//':sdate' => $sdate, ':edate' => $edate,
    }
    
    public static function  getTotalPop($sitecode, $sdate, $edate) {
        $sql = "SELECT COUNT(*) FROM cca_diagram where type_area in ('1','3') and death <> 1  and hospcode = :sitecode";
        return  CcanetFunc::queryScalar($sql, [':sitecode' => $sitecode]);
    }
    
    public static function  getNoSym($sitecode, $sdate, $edate) {
        $sql = "SELECT COUNT(*) FROM cca_diagram where type_area in ('1','3') and death <> 1 and symptoms is null and hospcode = :sitecode";
        return  CcanetFunc::queryScalar($sql, [':sitecode' => $sitecode]);
    }
    
    public static function  getSym($sitecode, $sdate, $edate) {
        $sql = "SELECT COUNT(*) FROM cca_diagram where type_area in ('1','3') and death <> 1 and symptoms = 1 and hospcode = :sitecode";
        return  CcanetFunc::queryScalar($sql, [':sitecode' => $sitecode]);
    }
    
    public static function  getOpd($sitecode, $sdate, $edate) {
        $sql = "SELECT COUNT(*) FROM cca_diagram where type_area in ('1','3') and death <> 1 and an is null and symptoms = 1 and hospcode = :sitecode";
        return  CcanetFunc::queryScalar($sql, [':sitecode' => $sitecode]);
    }
    
    public static function  getIpd($sitecode, $sdate, $edate) {
        $sql = "SELECT COUNT(*) FROM cca_diagram where type_area in ('1','3') and death <> 1 and an is NOT null and symptoms = 1 and hospcode = :sitecode";
        return  CcanetFunc::queryScalar($sql, [':sitecode' => $sitecode]);
    }
    
    public static function  getOpdNoSymp($sitecode, $sdate, $edate) {
        $sql = "SELECT COUNT(*) FROM cca_diagram where type_area in ('1','3') and death <> 1 and an is  null and symptoms = 1 and symp_diagnosis NOT IN('k75','k803','k804','k805')  and hospcode = :sitecode";
        return  CcanetFunc::queryScalar($sql, [':sitecode' => $sitecode]);
    }
    
    public static function  getOpdSymp($sitecode, $sdate, $edate) {
        $sql = "SELECT COUNT(*) FROM cca_diagram where type_area in ('1','3') and death <> 1 and an is  null and symptoms = 1 and symp_diagnosis in ('k75','k803','k804','k805') and hospcode = :sitecode";
        return  CcanetFunc::queryScalar($sql, [':sitecode' => $sitecode]);
    }
    
    public static function  getIpdNoSymp($sitecode, $sdate, $edate) {
        $sql = "SELECT COUNT(*) FROM cca_diagram where type_area in ('1','3') and death <> 1 and an is not null and symptoms = 1 and symp_diagnosis NOT IN('k75','k803','k804','k805')  and hospcode = :sitecode";
        return  CcanetFunc::queryScalar($sql, [':sitecode' => $sitecode]);
    }
    
    public static function  getIpdSymp($sitecode, $sdate, $edate) {
        $sql = "SELECT COUNT(*) FROM cca_diagram where type_area in ('1','3') and death <> 1 and an is not null and symptoms = 1 and symp_diagnosis in ('k75','k803','k804','k805') and hospcode = :sitecode";
        return  CcanetFunc::queryScalar($sql, [':sitecode' => $sitecode]);
    }
    
    public static function  getOpdNoSympNocca($sitecode, $sdate, $edate) {
        $sql = "SELECT COUNT(*) FROM cca_diagram where type_area in ('1','3') and death <> 1 and an is  null and symptoms = 1 and symp_diagnosis NOT IN('k75','k803','k804','k805') and cca_code is null and hospcode = :sitecode";
        return  CcanetFunc::queryScalar($sql, [':sitecode' => $sitecode]);
    }
    
    public static function  getOpdNoSympCca($sitecode, $sdate, $edate) {
        $sql = "SELECT COUNT(*) FROM cca_diagram where type_area in ('1','3') and death <> 1 and an is  null and symptoms = 1 and symp_diagnosis NOT IN('k75','k803','k804','k805') and cca_code is not null and hospcode = :sitecode";
        return  CcanetFunc::queryScalar($sql, [':sitecode' => $sitecode]);
    }
    
    public static function  getOpdSympNocca($sitecode, $sdate, $edate) {
        $sql = "SELECT COUNT(*) FROM cca_diagram where type_area in ('1','3') and death <> 1 and an is  null and symptoms = 1 and symp_diagnosis IN('k75','k803','k804','k805') and cca_code is null and hospcode = :sitecode";
        return  CcanetFunc::queryScalar($sql, [':sitecode' => $sitecode]);
    }
    
    public static function  getOpdSympCca($sitecode, $sdate, $edate) {
        $sql = "SELECT COUNT(*) FROM cca_diagram where type_area in ('1','3') and death <> 1 and an is  null and symptoms = 1 and symp_diagnosis IN('k75','k803','k804','k805') and cca_code is not null and hospcode = :sitecode";
        return  CcanetFunc::queryScalar($sql, [':sitecode' => $sitecode]);
    }
    
    public static function  getIpdNoSympNocca($sitecode, $sdate, $edate) {
        $sql = "SELECT COUNT(*) FROM cca_diagram where type_area in ('1','3') and death <> 1 and an is not null and symptoms = 1 and symp_diagnosis NOT IN('k75','k803','k804','k805') and cca_code is null and hospcode = :sitecode";
        return  CcanetFunc::queryScalar($sql, [':sitecode' => $sitecode]);
    }
    
    public static function  getIpdNoSympCca($sitecode, $sdate, $edate) {
        $sql = "SELECT COUNT(*) FROM cca_diagram where type_area in ('1','3') and death <> 1 and an is not null and symptoms = 1 and symp_diagnosis NOT IN('k75','k803','k804','k805') and cca_code is not null and hospcode = :sitecode";
        return  CcanetFunc::queryScalar($sql, [':sitecode' => $sitecode]);
    }
    
    public static function  getIpdSympNocca($sitecode, $sdate, $edate) {
        $sql = "SELECT COUNT(*) FROM cca_diagram where type_area in ('1','3') and death <> 1 and an is not null and symptoms = 1 and symp_diagnosis IN('k75','k803','k804','k805') and cca_code is null and hospcode = :sitecode";
        return  CcanetFunc::queryScalar($sql, [':sitecode' => $sitecode]);
    }
    
    public static function  getIpdSympCca($sitecode, $sdate, $edate) {
        $sql = "SELECT COUNT(*) FROM cca_diagram where type_area in ('1','3') and death <> 1 and an is not null and symptoms = 1 and symp_diagnosis IN('k75','k803','k804','k805') and cca_code is not null and hospcode = :sitecode";
        return  CcanetFunc::queryScalar($sql, [':sitecode' => $sitecode]);
    }




}
