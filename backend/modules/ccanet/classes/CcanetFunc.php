<?php

namespace backend\modules\ccanet\classes;

use Yii;

/**
 * newPHPClass class file UTF-8
 * @author SDII <iencoded@gmail.com>
 * @copyright Copyright &copy; 2015 AppXQ
 * @license http://www.appxq.com/license/
 * @version 1.0.0 Date: 4 พ.ย. 2559 7:53:01
 * @link http://www.appxq.com/
 * @example 
 */
class CcanetFunc {

    public static function getDb() {

        if (isset(Yii::$app->session['dynamic_connection']) && !empty(Yii::$app->session['dynamic_connection'])) {
            $sitecode = Yii::$app->user->identity->userProfile->sitecode;
            //unset(Yii::$app->session['dynamic_connection']);
            if (Yii::$app->session['dynamic_connection']['sitecode'] != $sitecode) {
                $data_con = CcanetQuery::getDbConfig($sitecode);

                $obj = [
                    'sitecode' => $sitecode,
                    'db' => $data_con
                ];
                Yii::$app->session['dynamic_connection'] = $obj;
            }
        } else {
            $sitecode = Yii::$app->user->identity->userProfile->sitecode;
            $data_con = CcanetQuery::getDbConfig($sitecode);

            $obj = [
                'sitecode' => $sitecode,
                'db' => $data_con
            ];
            Yii::$app->session['dynamic_connection'] = $obj;
        }

        $data_config = Yii::$app->session['dynamic_connection']['db'];

        //\appxq\sdii\utils\VarDumper::dump(Yii::$app->session['dynamic_connection']);

        if (isset($data_config) && !empty($data_config)) {
            $dsn = "mysql:host={$data_config['server']};port={$data_config['port']};dbname={$data_config['db']}";

            $db = new \yii\db\Connection([
                'dsn' => $dsn,
                'username' => $data_config['user'],
                'password' => $data_config['passwd'],
                'charset' => 'utf8',
                    // 'enableSchemaCache' => true,
                    // 'schemaCacheDuration' => 3600,
            ]);
            //$db->open();

            return $db;
        }

        return FALSE;
    }

    public static function rawSql($sql, $param = []) {
        $db = self::getDb();
        if ($db) {
            return $db->createCommand($sql, $param)->rawSql;
        }
        return FALSE;
    }

    public static function execute($sql, $param = []) {
        $db = self::getDb();
        if ($db) {
            return $db->createCommand($sql, $param)->execute();
        }
        return FALSE;
    }

    public static function queryAll($sql, $param = []) {
        $db = self::getDb();
        //\appxq\sdii\utils\VarDumper::dump($db);
        if ($db) {
            return $db->createCommand($sql, $param)->queryAll();
        }
        return FALSE;
    }

    public static function queryOne($sql, $param = []) {
        $db = self::getDb();
        if ($db) {
            return $db->createCommand($sql, $param)->queryOne();
        }
        return FALSE;
    }

    public static function queryScalar($sql, $param = []) {
        $db = self::getDb();
        if ($db) {
            return $db->createCommand($sql, $param)->queryScalar();
        }
        return FALSE;
    }

    public static function convertColumn($colArr) {
        $colArrReturn = [];
        foreach ($colArr as $key => $value) {
            $colArrReturn[$key] = strtolower($value);
        }
        return $colArrReturn;
    }

    public static function getCcaDiagram($sitecode, $sdate, $edate) {
        try {
            $count = CcanetQuery::getAll($sitecode, $sdate, $edate);
        } catch (\yii\db\Exception $e) {
            $count = 0;
        }
        $obj[] = [
            'id' => 0,
            'label' => "All ($count)",
            'value' => $count,
        ];
        
        try {
            $count = CcanetQuery::getTotalPop($sitecode, $sdate, $edate);
        } catch (\yii\db\Exception $e) {
            $count = 0;
        }
        // echo count($count);exit();
        //\appxq\sdii\utils\VarDumper::dump($count);
        $obj[] = [
            'id' => 1,
            'label' => "1) All ($count)",
            'value' => $count,
        ];
        
        try {
            $count = CcanetQuery::getNoSym($sitecode, $sdate, $edate);
        } catch (\yii\db\Exception $e) {
            $count = 0;
        }
        // echo count($count);exit();
        //\appxq\sdii\utils\VarDumper::dump($count);
        $obj[] = [
            'id' => 2,
            'label' => "2) No Symptoms ($count)",
            'value' => $count,
        ];
        
        try {
            $count = CcanetQuery::getSym($sitecode, $sdate, $edate);
        } catch (\yii\db\Exception $e) {
            $count = 0;
        }
        // echo count($count);exit();
        //\appxq\sdii\utils\VarDumper::dump($count);
        $obj[] = [
            'id' => 3,
            'label' => "3) Symptoms ($count)",
            'value' => $count,
        ];
        
        try {
            $count = CcanetQuery::getOpd($sitecode, $sdate, $edate);
        } catch (\yii\db\Exception $e) {
            $count = 0;
        }
        // echo count($count);exit();
        //\appxq\sdii\utils\VarDumper::dump($count);
        $obj[] = [
            'id' => 4,
            'label' => "4) OPD CCA ($count)",
            'value' => $count,
        ];
        
        try {
            $count = CcanetQuery::getIpd($sitecode, $sdate, $edate);
        } catch (\yii\db\Exception $e) {
            $count = 0;
        }
        // echo count($count);exit();
        //\appxq\sdii\utils\VarDumper::dump($count);
        $obj[] = [
            'id' => 5,
            'label' => "5) IPD CCA ($count)",
            'value' => $count,
        ];
        
        try {
            $count = CcanetQuery::getOpdNoSymp($sitecode, $sdate, $edate);
        } catch (\yii\db\Exception $e) {
            $count = 0;
        }
        // echo count($count);exit();
        //\appxq\sdii\utils\VarDumper::dump($count);
        $obj[] = [
            'id' => 61,
            'label' => "6.1) opd nosym ($count)",
            'value' => $count,
        ];
        
        try {
            $count = CcanetQuery::getOpdSymp($sitecode, $sdate, $edate);
        } catch (\yii\db\Exception $e) {
            $count = 0;
        }
        // echo count($count);exit();
        //\appxq\sdii\utils\VarDumper::dump($count);
        $obj[] = [
            'id' => 62,
            'label' => "6.2) opd sym ($count)",
            'value' => $count,
        ];
        
        try {
            $count = CcanetQuery::getIpdNoSymp($sitecode, $sdate, $edate);
        } catch (\yii\db\Exception $e) {
            $count = 0;
        }
        // echo count($count);exit();
        //\appxq\sdii\utils\VarDumper::dump($count);
        $obj[] = [
            'id' => 63,
            'label' => "6.3) ipd nosym ($count)",
            'value' => $count,
        ];
        
        try {
            $count = CcanetQuery::getIpdSymp($sitecode, $sdate, $edate);
        } catch (\yii\db\Exception $e) {
            $count = 0;
        }
        // echo count($count);exit();
        //\appxq\sdii\utils\VarDumper::dump($count);
        $obj[] = [
            'id' => 64,
            'label' => "6.4) ipd sym ($count)",
            'value' => $count,
        ];
        
        try {
            $count = CcanetQuery::getOpdNoSympNoCca($sitecode, $sdate, $edate);
        } catch (\yii\db\Exception $e) {
            $count = 0;
        }
        // echo count($count);exit();
        //\appxq\sdii\utils\VarDumper::dump($count);
        $obj[] = [
            'id' => 71,
            'label' => "6.1) opd nosym ($count)",
            'value' => $count,
        ];
        
        try {
            $count = CcanetQuery::getOpdNoSympCca($sitecode, $sdate, $edate);
        } catch (\yii\db\Exception $e) {
            $count = 0;
        }
        // echo count($count);exit();
        //\appxq\sdii\utils\VarDumper::dump($count);
        $obj[] = [
            'id' => 72,
            'label' => "6.1) opd nosym ($count)",
            'value' => $count,
        ];
        
        try {
            $count = CcanetQuery::getOpdSympCca($sitecode, $sdate, $edate);
        } catch (\yii\db\Exception $e) {
            $count = 0;
        }
        // echo count($count);exit();
        //\appxq\sdii\utils\VarDumper::dump($count);
        $obj[] = [
            'id' => 73,
            'label' => "6.2) opd sym ($count)",
            'value' => $count,
        ];     
                
        try {
            $count = CcanetQuery::getOpdSympNocca($sitecode, $sdate, $edate);
        } catch (\yii\db\Exception $e) {
            $count = 0;
        }
        // echo count($count);exit();
        //\appxq\sdii\utils\VarDumper::dump($count);
        $obj[] = [
            'id' => 74,
            'label' => "6.2) opd sym ($count)",
            'value' => $count,
        ];
        
        try {
            $count = CcanetQuery::getIpdNoSympNoCca($sitecode, $sdate, $edate);
        } catch (\yii\db\Exception $e) {
            $count = 0;
        }
        // echo count($count);exit();
        //\appxq\sdii\utils\VarDumper::dump($count);
        $obj[] = [
            'id' => 75,
            'label' => "6.1) opd nosym ($count)",
            'value' => $count,
        ];
        
        try {
            $count = CcanetQuery::getIpdNoSympCca($sitecode, $sdate, $edate);
        } catch (\yii\db\Exception $e) {
            $count = 0;
        }
        // echo count($count);exit();
        //\appxq\sdii\utils\VarDumper::dump($count);
        $obj[] = [
            'id' => 76,
            'label' => "6.1) opd nosym ($count)",
            'value' => $count,
        ];
        
        try {
            $count = CcanetQuery::getIpdSympCca($sitecode, $sdate, $edate);
        } catch (\yii\db\Exception $e) {
            $count = 0;
        }
        // echo count($count);exit();
        //\appxq\sdii\utils\VarDumper::dump($count);
        $obj[] = [
            'id' => 77,
            'label' => "6.2) opd sym ($count)",
            'value' => $count,
        ];     
                
        try {
            $count = CcanetQuery::getIpdSympNocca($sitecode, $sdate, $edate);
        } catch (\yii\db\Exception $e) {
            $count = 0;
        }
        // echo count($count);exit();
        //\appxq\sdii\utils\VarDumper::dump($count);
        $obj[] = [
            'id' => 78,
            'label' => "6.2) opd sym ($count)",
            'value' => $count,
        ];

        return $obj;
    }

   

}
