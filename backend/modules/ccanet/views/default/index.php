
<?php
use yii\bootstrap\Html;
$this->title = Yii::t('backend', 'สรุปสถานการณ์โรคมะเร็งท่อน้ำดีทบทวนชาร์จผู้ป่วย');
$this->params['breadcrumbs'][] = ['label'=>'Modules','url'=>  yii\helpers\Url::to('/tccbots/my-module')];
$this->params['breadcrumbs'][] = Yii::t('backend', 'CCANET');
$this->registerCss('
.primary{
     cursor: pointer;
}

.p1 {
    left: 18%;
    top: 5.5%;
    width: 24%;
    height: 6%;
    position: absolute;
}

.p2 {
    left: 6%;
    top: 20.7%;
    width: 24%;
    height: 6%;
    position: absolute;
}

.p3 {
    left: 34%;
    top: 20.7%;
    width: 24%;
    height: 6%;
    position: absolute;
}

.p4 {
    left: 12.9%;
    top: 34%;
    width: 24%;
    height: 6%;
    position: absolute;
}

.p5 {
    left: 46%;
    top: 34%;
    width: 24%;
    height: 6%;
    position: absolute;
}

.p6_opd_nosym {
    left: 3.8%;
    top: 51.6%;
    width: 24%;
    height: 6%;
    position: absolute;
}

.p6_opd_sym {
    left: 21.2%;
    top: 51.6%;
    width: 24%;
    height: 6%;
    position: absolute;
}

.p6_ipd_nosym {
    left: 39%;
    top: 51.6%;
    width: 24%;
    height: 6%;
    position: absolute;
}

.p6_ipd_sym {
    left: 56.8%;
    top: 51.6%;
    width: 24%;
    height: 6%;
    position: absolute;
}

.p7_opd_nosym_noicd {
    left: -1%;
    top: 71.2%;
    width: 24%;
    height: 6%;
    position: absolute;
}

.p7_opd_nosym_icd {
    left: 8%;
    top: 71.2%;
    width: 24%;
    height: 6%;
    position: absolute;
}

.p7_opd_sym_noicd {
    left: 16.7%;
    top: 71.2%;
    width: 24%;
    height: 6%;
    position: absolute;
}

.p7_opd_sym_icd {
    left: 25.3%;
    top: 71.2%;
    width: 24%;
    height: 6%;
    position: absolute;
}

.p7_ipd_nosym_noicd {
    left: 34.1%;
    top: 71.2%;
    width: 24%;
    height: 6%;
    position: absolute;
}

.p7_ipd_nosym_icd {
    left: 43.2%;
    top: 71.2%;
    width: 24%;
    height: 6%;
    position: absolute;
}

.p7_ipd_sym_noicd {
    left: 52%;
    top: 71.3%;
    width: 24%;
    height: 6%;
    position: absolute;
}

.p7_ipd_sym_icd {
    left: 60.6%;
    top: 71.3%;
    width: 24%;
    height: 6%;
    position: absolute;
}


#btnSetting{
    margin-left: 0px;float: right;
}

');
$ckdDiagram = backend\modules\ccanet\classes\CcanetFunc::getCcaDiagram(Yii::$app->user->identity->userProfile->sitecode, '', '');
$mapDiagram = yii\helpers\ArrayHelper::map($ckdDiagram, 'id', 'value');

?>
<!--<div class="ccanet-default-index">
    <img src="<?= \yii\helpers\Url::to(['@web/img/ccanet2.png'])?>" class="img img-response" style="width:100%">
</div>-->

<div class="col-lg-10 col-md-10 col-sm-10 col-xs-10" style="max-width:1000 px">
        <img class="img-rounded img-responsive" src="<?= \yii\helpers\Url::to(['@web/img/ccanet2.png']) ?>">
        <a class="p1 primary text-center" href="<?= \yii\helpers\Url::to(['#'=>1]) ?>"><table style="height: 100%;width: 100%;"><tbody><tr><td class="text-center"  style="vertical-align: bottom !important;color: red;"><?= number_format($mapDiagram[1])?></td></tr></tbody></table></a>
        <a class="p2 primary text-center" href="<?= \yii\helpers\Url::to(['#'=>1]) ?>"><table style="height: 100%;width: 100%;"><tbody><tr><td class="text-center"  style="vertical-align: bottom !important;color: red;"><?= number_format($mapDiagram[2])?></td></tr></tbody></table></a>
        <a class="p3 primary text-center" href="<?= \yii\helpers\Url::to(['#'=>1]) ?>"><table style="height: 100%;width: 100%;"><tbody><tr><td class="text-center"  style="vertical-align: bottom !important;color: red;"><?= number_format($mapDiagram[3])?></td></tr></tbody></table></a>
        <a class="p4 primary text-center" href="<?= \yii\helpers\Url::to(['#'=>1]) ?>"><table style="height: 100%;width: 100%;"><tbody><tr><td class="text-center"  style="vertical-align: bottom !important;color: red;"><?= number_format($mapDiagram[4])?></td></tr></tbody></table></a>
        <a class="p5 primary text-center" href="<?= \yii\helpers\Url::to(['#'=>1]) ?>"><table style="height: 100%;width: 100%;"><tbody><tr><td class="text-center"  style="vertical-align: bottom !important;color: red;"><?= number_format($mapDiagram[5])?></td></tr></tbody></table></a>
        <a class="p6_opd_nosym primary text-center" href="<?= \yii\helpers\Url::to(['#'=>1]) ?>"><table style="height: 100%;width: 100%;"><tbody><tr><td class="text-center"  style="vertical-align: bottom !important;color: red;"><?= number_format($mapDiagram[61])?></td></tr></tbody></table></a> 
        <a class="p6_opd_sym primary text-center" href="<?= \yii\helpers\Url::to(['#'=>1]) ?>"><table style="height: 100%;width: 100%;"><tbody><tr><td class="text-center"  style="vertical-align: bottom !important;color: red;"><?= number_format($mapDiagram[62])?></td></tr></tbody></table></a> 
        <a class="p6_ipd_nosym primary text-center" href="<?= \yii\helpers\Url::to(['#'=>1]) ?>"><table style="height: 100%;width: 100%;"><tbody><tr><td class="text-center"  style="vertical-align: bottom !important;color: red;"><?= number_format($mapDiagram[63])?></td></tr></tbody></table></a> 
        <a class="p6_ipd_sym primary text-center" href="<?= \yii\helpers\Url::to(['#'=>1]) ?>"><table style="height: 100%;width: 100%;"><tbody><tr><td class="text-center"  style="vertical-align: bottom !important;color: red;"><?= number_format($mapDiagram[64])?></td></tr></tbody></table></a> 
        <a class="p7_opd_nosym_noicd primary text-center" href="<?= \yii\helpers\Url::to(['#'=>1]) ?>"><table style="height: 100%;width: 100%;"><tbody><tr><td class="text-center"  style="vertical-align: bottom !important;color: red;"><?= number_format($mapDiagram[71])?></td></tr></tbody></table></a> 
        <a class="p7_opd_nosym_icd primary text-center" href="<?= \yii\helpers\Url::to(['#'=>1]) ?>"><table style="height: 100%;width: 100%;"><tbody><tr><td class="text-center"  style="vertical-align: bottom !important;color: red;"><?= number_format($mapDiagram[72])?></td></tr></tbody></table></a> 
        <a class="p7_opd_sym_noicd primary text-center" href="<?= \yii\helpers\Url::to(['#'=>1]) ?>"><table style="height: 100%;width: 100%;"><tbody><tr><td class="text-center"  style="vertical-align: bottom !important;color: red;"><?= number_format($mapDiagram[73])?></td></tr></tbody></table></a> 
        <a class="p7_opd_sym_icd primary text-center" href="<?= \yii\helpers\Url::to(['#'=>1]) ?>"><table style="height: 100%;width: 100%;"><tbody><tr><td class="text-center"  style="vertical-align: bottom !important;color: red;"><?= number_format($mapDiagram[74])?></td></tr></tbody></table></a> 
        <a class="p7_ipd_nosym_noicd primary text-center" href="<?= \yii\helpers\Url::to(['#'=>1]) ?>"><table style="height: 100%;width: 100%;"><tbody><tr><td class="text-center"  style="vertical-align: bottom !important;color: red;"><?= number_format($mapDiagram[75])?></td></tr></tbody></table></a> 
        <a class="p7_ipd_nosym_icd primary text-center" href="<?= \yii\helpers\Url::to(['#'=>1]) ?>"><table style="height: 100%;width: 100%;"><tbody><tr><td class="text-center"  style="vertical-align: bottom !important;color: red;"><?= number_format($mapDiagram[76])?></td></tr></tbody></table></a> 
        <a class="p7_ipd_sym_noicd primary text-center" href="<?= \yii\helpers\Url::to(['#'=>1]) ?>"><table style="height: 100%;width: 100%;"><tbody><tr><td class="text-center"  style="vertical-align: bottom !important;color: red;"><?= number_format($mapDiagram[77])?></td></tr></tbody></table></a> 
        <a class="p7_ipd_sym_icd primary text-center" href="<?= \yii\helpers\Url::to(['#'=>1]) ?>"><table style="height: 100%;width: 100%;"><tbody><tr><td class="text-center"  style="vertical-align: bottom !important;color: red;"><?= number_format($mapDiagram[78])?></td></tr></tbody></table></a> 
        
</div>
