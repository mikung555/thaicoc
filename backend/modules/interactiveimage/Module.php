<?php

namespace backend\modules\interactiveimage;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\modules\interactiveimage\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
