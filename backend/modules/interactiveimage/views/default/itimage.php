<?php

use yii\helpers\Url;
?>

<div id='mainCanvasDiv' width="100%" align="center" >

    <canvas id="myCanvas" width="1400px" height="800"></canvas>
</div>

    <?php
//appxq\sdii\utils\VarDumper::dump($jstext);
    $jstext = "
        var imageRatio=2;
        var canvas = document.getElementById('myCanvas');
             var  elem =canvas;
      var context = canvas.getContext('2d');
      var imageObj = new Image();

      imageObj.onload = function() {
        context.drawImage(imageObj, 0, 0,canvas.width,canvas.width/imageRatio );
      };
      imageObj.src = 
      '"
            . Url::to('/images/flukefree', true)
            . "/ff.jpg';";

    $jstext = $jstext . "
          
var myGamePiece;
var isClick =false;
function collides(rects, x, y) {
    var isCollision = false;
    for (var i = 0, len = rects.length; i < len; i++) {
        var left = rects[i].x, right = rects[i].x+rects[i].w;
        var top = rects[i].y, bottom = rects[i].y+rects[i].h;
        if (right >= x
            && left <= x
            && bottom >= y
            && top <= y) {
            isCollision = rects[i];
        }
    }
    return isCollision;
}
setClickPoint();
function setClickPoint(){
//context.clearRect(0, 0, canvas.width, canvas.height);
if (elem ) {
    // list of rectangles to render
    //ratio =2
    // 1400
    var rectRatio =canvas.width/1400;
    rects=null;
    var rects = [
                {x: 91*rectRatio,y: 255*rectRatio, w: 150*rectRatio, h: 80*rectRatio,id:1},
                 {x: 272*rectRatio, y: 197*rectRatio, w: 150*rectRatio, h: 80*rectRatio,id:2},
                 {x: 273*rectRatio, y: 376*rectRatio, w: 150*rectRatio, h: 80*rectRatio,id:3},
                 {x: 494*rectRatio, y: 130*rectRatio, w: 150*rectRatio, h: 80*rectRatio,id:4},
                 {x: 495*rectRatio, y: 423*rectRatio, w: 150*rectRatio, h: 80*rectRatio,id:5},
                 {x: 736*rectRatio, y: 211*rectRatio, w: 150*rectRatio, h: 80*rectRatio,id:6},
                 {x: 727*rectRatio, y: 375*rectRatio, w: 150*rectRatio, h: 80*rectRatio,id:7},
                 {x: 918*rectRatio, y: 255*rectRatio, w: 150*rectRatio, h: 80*rectRatio,id:8},
                 {x: 1059*rectRatio, y: 234*rectRatio, w: 150*rectRatio, h: 80*rectRatio,id:9},
                 {x: 1060*rectRatio, y: 305*rectRatio, w: 150*rectRatio, h: 80*rectRatio,id:10},
                 {x: 25*rectRatio, y: 25*rectRatio, w: 250*rectRatio, h: 100*rectRatio,id:11}                 
                 
];
  // get context
  var context = elem.getContext('2d');
elem.addEventListener('mouseup', function(e) {
isClick=false;
}, false);
    // listener, using W3C style for example    
    elem.addEventListener('mousedown', function(e) {
    if(isClick)return;
        console.log('click: ' + e.offsetX + '/' + e.offsetY);
        var rect = collides(rects, e.offsetX, e.offsetY);
        var count=1;
        if (rect) {
            console.log('collision: ' + rect.x + '/' + rect.y +'/' +rect.id );
            var id = rect.id;
            var url='';
            switch (parseInt(id)) {
        
                case 1:
              url  = 'https://cloudbackend.cascap.in.th/inv/inv-map/index?module=1498130302024554300';
                    break;
                case 2:
                url  = 'https://cloudbackend.cascap.in.th/inv/inv-map/index?module=1497859920056436400';

                    break;
                case 3:
                    url  = 'https://cloudbackend.cascap.in.th/inv/inv-person/index?module=1496902325091526900';

                    break;
                case 4:
                      url  = 'https://cloudbackend.cascap.in.th/inv/inv-map/index?module=1498457589040399800';
                    break;
                case 5:
                      url  = 'https://cloud.cascap.in.th/project84/report';

                    break;
                case 6:
                     url  = 'https://cloudbackend.cascap.in.th/inv/inv-person/index?module=1498622167035527500';

                    break;
                case 7:
                     url  = 'https://cloudbackend.cascap.in.th/inv/inv-person/index?module=1484721973020193700';
                
                    break;
                case 8:
                      url  = 'https://cloudbackend.cascap.in.th/inv/inv-person/index?module=29';
               
                    break;
                case 9:
                     url  = 'https://cloudbackend.cascap.in.th/inv/inv-person/index?module=1496678203080761900';
              
                break;
                    
                  case 10:
                        url  = 'https://cloudbackend.cascap.in.th/inv/inv-person/index?module=1496678490037398000';
                     break;
                            
                  case 11:
                        url  = 'https://cloudbackend.cascap.in.th/tccbots/monitor-report';
                     break;
                }
            console.log('url ' + url);
           // window.location =url;
        window.open(url, '_blank', 'toolbar=yes, location=yes, status=yes, menubar=yes, scrollbars=yes');
isClick=true;

        } else {
        }
    }, false);
    


////// mouse over

    elem.addEventListener('mouseover', function(e) {
        var rect = collides(rects, e.offsetX, e.offsetY);
        if (rect) {

               myCanvas.style.cursor = 'pointer';


        } else {
                 myCanvas.style.cursor = 'default';

        }
    }, false);
    
    elem.addEventListener('mousemove', function(e) {
        var rect = collides(rects, e.offsetX, e.offsetY);
        if (rect) {

               myCanvas.style.cursor = 'pointer';


        } else {
                 myCanvas.style.cursor = 'default';

        }
    }, false);
    
////////////end mouse over
}

}


 var c = $('#myCanvas');
    var ct = c.get(0).getContext('2d');
    var container = $(c).parent();

    //Run function when browser resizes
    $(window).resize( respondCanvas );

  function respondCanvas(){
        c.attr('width', $(container).width() ); //max width
        c.attr('height', $(container).height() ); //max height
imageObj.onload();
//var newCanvas = c.cloneNode(true);
//c.parentNode.replaceChild(newCanvas, c);
setClickPoint();
        //Call a function to redraw other content (texts, images etc)
    }

    //Initial call
    respondCanvas();

";

    $this->registerJs($jstext);
    ?>


