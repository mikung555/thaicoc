<?php
/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\helpers\Url;
use appxq\sdii\widgets\GridView;
use appxq\sdii\widgets\ModalForm;
use appxq\sdii\helpers\SDNoty;
use appxq\sdii\helpers\SDHtml;
?>
  <link href="https://netdna.bootstrapcdn.com/font-awesome/4.0.0/css/font-awesome.css" rel="stylesheet">
  <link rel="stylesheet" href="/leaflet/leaflet.awesome-markers.css">
  <link rel="stylesheet" href="/leaflet/leaflet.css" />
  <!--[if lte IE 8]>
      <link rel="stylesheet" href="http://cdn.leafletjs.com/leaflet-0.6.4/leaflet.ie.css" />
  <![endif]-->
    <div class="alert alert-danger" style="font-size:20px;">
        <i class="fa fa-wrench" aria-hidden="true"></i> กำลังพัฒนาระบบ  
    </div>
    <div class="row">
        <div class="col-md-6">
            <div id="map" style="width: 100%; height: 600px"></div>
        </div>
        <div class="col-md-3" style="height: 600px">
        
    <?php  Pjax::begin(['id'=>'helper-gis-grid-pjax']);?>
    <?= GridView::widget([
	'id' => 'helper-gis-grid',
//	'panelBtn' => Html::button(SDHtml::getBtnAdd(), ['data-url'=>Url::to(['helper-gis/create']), 'class' => 'btn btn-success btn-sm', 'id'=>'modal-addbtn-helper-gis']). ' ' .
//		      Html::button(SDHtml::getBtnDelete(), ['data-url'=>Url::to(['helper-gis/deletes']), 'class' => 'btn btn-danger btn-sm', 'id'=>'modal-delbtn-helper-gis', 'disabled'=>true]),
	'dataProvider' => $dataProvider,
	'filterModel' => $searchModel,
        'columns' => [
//	    [
//		'class' => 'yii\grid\CheckboxColumn',
//		'checkboxOptions' => [
//		    'class' => 'selectionHelperGiIds'
//		],
//		'headerOptions' => ['style'=>'text-align: center;'],
//		'contentOptions' => ['style'=>'width:40px;text-align: center;'],
//	    ],
//	    [
//		'class' => 'yii\grid\SerialColumn',
//		'headerOptions' => ['style'=>'text-align: center;'],
//		'contentOptions' => ['style'=>'width:60px;text-align: center;'],
//	    ],

            'patient_name',
            //'volunteer',
            // 'volunteer_id',
            // 'address',
            // 'lat',
            // 'lng',
            //'tel',

	    [
		'class' => 'appxq\sdii\widgets\ActionColumn',
		'contentOptions' => ['style'=>'width:80px;text-align: center;'],
		'template' => '{view}',
	    ],
        ],
    ]); ?>
    <?php  Pjax::end();?>

</div>
        <div class="col-md-3" style="height: 600px">
        
    <?php  Pjax::begin(['id'=>'helper-gis2-grid-pjax']);?>
    <?= GridView::widget([
	'id' => 'helper-gis2-grid',
//	'panelBtn' => Html::button(SDHtml::getBtnAdd(), ['data-url'=>Url::to(['helper-gis/create']), 'class' => 'btn btn-success btn-sm', 'id'=>'modal-addbtn-helper-gis']). ' ' .
//		      Html::button(SDHtml::getBtnDelete(), ['data-url'=>Url::to(['helper-gis/deletes']), 'class' => 'btn btn-danger btn-sm', 'id'=>'modal-delbtn-helper-gis', 'disabled'=>true]),
	'dataProvider' => $dataProvider2,
	'filterModel' => $searchModel2,
        'columns' => [
//	    [
//		'class' => 'yii\grid\CheckboxColumn',
//		'checkboxOptions' => [
//		    'class' => 'selectionHelperGiIds'
//		],
//		'headerOptions' => ['style'=>'text-align: center;'],
//		'contentOptions' => ['style'=>'width:40px;text-align: center;'],
//	    ],
//	    [
//		'class' => 'yii\grid\SerialColumn',
//		'headerOptions' => ['style'=>'text-align: center;'],
//		'contentOptions' => ['style'=>'width:60px;text-align: center;'],
//	    ],

            //'patient_name',
            'volunteer',
            // 'volunteer_id',
            // 'address',
            // 'lat',
            // 'lng',
            //'tel',

	    [
		'class' => 'appxq\sdii\widgets\ActionColumn',
		'contentOptions' => ['style'=>'width:80px;text-align: center;'],
		'template' => '{view}',
	    ],
        ],
    ]); ?>
    <?php  Pjax::end();?>

</div>
<?=  ModalForm::widget([
    'id' => 'modal-helper-gis',
    'size'=>'modal-lg',
]);
?>
<?=  ModalForm::widget([
    'id' => 'modal-helper-gis2',
    'size'=>'modal-lg',
]);
?>        

<?php  $this->registerJs("
$('#helper-gis-grid-pjax').on('click', '#modal-addbtn-helper-gis', function() {
    modalHelperGi($(this).attr('data-url'));
});

$('#helper-gis-grid-pjax').on('click', '#modal-delbtn-helper-gis', function() {
    selectionHelperGiGrid($(this).attr('data-url'));
});

$('#helper-gis-grid-pjax').on('click', '.select-on-check-all', function() {
    window.setTimeout(function() {
	var key = $('#helper-gis-grid').yiiGridView('getSelectedRows');
	disabledHelperGiBtn(key.length);
    },100);
});

$('#helper-gis-grid-pjax').on('click', '.selectionHelperGiIds', function() {
    var key = $('input:checked[class=\"'+$(this).attr('class')+'\"]');
    disabledHelperGiBtn(key.length);
});

$('#helper-gis-grid-pjax').on('dblclick', 'tbody tr', function() {
    var id = $(this).attr('data-key');
    modalHelperGi('".Url::to(['helper-gis/update', 'id'=>''])."'+id);
});	

$('#helper-gis-grid-pjax').on('click', 'tbody tr td a', function() {
    var url = $(this).attr('href');
    var action = $(this).attr('data-action');

    if(action === 'update' || action === 'view') {
	modalHelperGi(url);
    } else if(action === 'delete') {
	yii.confirm('".Yii::t('app', 'Are you sure you want to delete this item?')."', function() {
	    $.post(
		url
	    ).done(function(result) {
		if(result.status == 'success') {
		    ". SDNoty::show('result.message', 'result.status') ."
		    $.pjax.reload({container:'#helper-gis-grid-pjax'});
		} else {
		    ". SDNoty::show('result.message', 'result.status') ."
		}
	    }).fail(function() {
		". SDNoty::show("'" . SDHtml::getMsgError() . "Server Error'", '"error"') ."
		console.log('server error');
	    });
	});
    }
    return false;
});

function disabledHelperGiBtn(num) {
    if(num>0) {
	$('#modal-delbtn-helper-gis').attr('disabled', false);
    } else {
	$('#modal-delbtn-helper-gis').attr('disabled', true);
    }
}

function selectionHelperGiGrid(url) {
    yii.confirm('".Yii::t('app', 'Are you sure you want to delete these items?')."', function() {
	$.ajax({
	    method: 'POST',
	    url: url,
	    data: $('.selectionHelperGiIds:checked[name=\"selection[]\"]').serialize(),
	    dataType: 'JSON',
	    success: function(result, textStatus) {
		if(result.status == 'success') {
		    ". SDNoty::show('result.message', 'result.status') ."
		    $.pjax.reload({container:'#helper-gis-grid-pjax'});
		} else {
		    ". SDNoty::show('result.message', 'result.status') ."
		}
	    }
	});
    });
}

function modalHelperGi(url) {
    $('#modal-helper-gis .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
    $('#modal-helper-gis').modal('show')
    .find('.modal-content')
    .load(url);
}

");?>

<?php  $this->registerJs("
$('#helper-gis2-grid-pjax').on('click', '#modal-addbtn-helper-gis', function() {
    modalHelperGi($(this).attr('data-url'));
});

$('#helper-gis2-grid-pjax').on('click', '#modal-delbtn-helper-gis', function() {
    selectionHelperGiGrid($(this).attr('data-url'));
});

$('#helper-gis2-grid-pjax').on('click', '.select-on-check-all', function() {
    window.setTimeout(function() {
	var key = $('#helper-gis2-grid').yiiGridView('getSelectedRows');
	disabledHelperGiBtn(key.length);
    },100);
});

$('#helper-gis2-grid-pjax').on('click', '.selectionHelperGiIds', function() {
    var key = $('input:checked[class=\"'+$(this).attr('class')+'\"]');
    disabledHelperGiBtn(key.length);
});

$('#helper-gis2-grid-pjax').on('dblclick', 'tbody tr', function() {
    var id = $(this).attr('data-key');
    modalHelperGi('".Url::to(['helper-gis/update', 'id'=>''])."'+id);
});	

$('#helper-gis2-grid-pjax').on('click', 'tbody tr td a', function() {
    var url = $(this).attr('href');
    var action = $(this).attr('data-action');

    if(action === 'update' || action === 'view') {
	modalHelperGi(url);
    } else if(action === 'delete') {
	yii.confirm('".Yii::t('app', 'Are you sure you want to delete this item?')."', function() {
	    $.post(
		url
	    ).done(function(result) {
		if(result.status == 'success') {
		    ". SDNoty::show('result.message', 'result.status') ."
		    $.pjax.reload({container:'#helper-gis2-grid-pjax'});
		} else {
		    ". SDNoty::show('result.message', 'result.status') ."
		}
	    }).fail(function() {
		". SDNoty::show("'" . SDHtml::getMsgError() . "Server Error'", '"error"') ."
		console.log('server error');
	    });
	});
    }
    return false;
});

function disabledHelperGiBtn(num) {
    if(num>0) {
	$('#modal-delbtn-helper-gis').attr('disabled', false);
    } else {
	$('#modal-delbtn-helper-gis').attr('disabled', true);
    }
}

function selectionHelperGiGrid(url) {
    yii.confirm('".Yii::t('app', 'Are you sure you want to delete these items?')."', function() {
	$.ajax({
	    method: 'POST',
	    url: url,
	    data: $('.selectionHelperGiIds:checked[name=\"selection[]\"]').serialize(),
	    dataType: 'JSON',
	    success: function(result, textStatus) {
		if(result.status == 'success') {
		    ". SDNoty::show('result.message', 'result.status') ."
		    $.pjax.reload({container:'#helper-gis2-grid-pjax'});
		} else {
		    ". SDNoty::show('result.message', 'result.status') ."
		}
	    }
	});
    });
}

function modalHelperGi(url) {
    $('#modal-helper-gis2 .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
    $('#modal-helper-gis2').modal('show')
    .find('.modal-content')
    .load(url);
}

");?>        
        </div>
    </div>
  
  
  <script src="/leaflet/leaflet.js"></script>
  <script src="/leaflet/leaflet.awesome-markers.js"></script>
  <script>

      
    var map = L.map('map').setView([16.0148725,101.8819517], 9);

    var mbUrl = 'https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw';
    var mbAttr = 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, ' +
        '<a href="https://www.thaicarecloud.org">Thai Care Cloud</a>, ';
    L.tileLayer(mbUrl, {
      maxZoom: 18,
      attribution: mbAttr,
      id: 'mapbox.streets'
    }).addTo(map);

    var hospital = new L.LayerGroup();
    <?php
        if (count($hospital)>0) foreach($hospital as $key => $value) {
    ?>
        L.marker([<?=$value['lat']+0.001;?>,<?=$value['lng']+0.001;?>], {icon: L.AwesomeMarkers.icon({icon: 'hospital-o', prefix: 'fa', markerColor: 'blue'}) }).bindPopup("<b>หน่วยบริการสุขภาพ</b><br><?=$value['name'];?>").addTo(hospital);
    <?php
        }
    ?>

    var patient = new L.LayerGroup();
    <?php
        if (count($patient)>0) foreach($patient as $key => $value) {
    ?>
        L.marker([<?=$value['lat']+0.001;?>,<?=$value['lng']+0.001;?>], {icon: L.AwesomeMarkers.icon({icon: 'user', prefix: 'fa', markerColor: 'red'}) }).bindPopup("<b>ผู้ป่วย</b><br><?=$value['patient_name'];?><br><b>ที่อยู่ </b><?=$value['address'];?><br><b>โทร </b><?=$value['tel'];?>").addTo(patient);
    <?php
        }
    ?>
    var volunteer = new L.LayerGroup();
    <?php
        if (count($volunteer)>0) foreach($volunteer as $key => $value) {
    ?>
        L.marker([<?=$value['lat'];?>,<?=$value['lng'];?>], {icon: L.AwesomeMarkers.icon({icon: 'user-md', prefix: 'fa', markerColor: 'green'}) }).bindPopup("<b>อาสาสมัคร</b><br><?=$value['volunteer'];?><br><b>ที่อยู่ </b><?=$value['address'];?><br><b>โทร </b><?=$value['tel'];?>").addTo(volunteer);
    <?php
        }
    ?>      
    //patient.addTo(map);
    volunteer.addTo(map);
    var grayscale   = L.tileLayer(mbUrl, {id: 'mapbox.light', attribution: mbAttr}),
        streets  = L.tileLayer(mbUrl, {id: 'mapbox.streets',   attribution: mbAttr});   
    streets.addTo(map);
    var baseLayers = {
            "Grayscale": grayscale,
            "Streets": streets
    };        
    var overlays = {
            "ผู้ป่วย": patient,
            "อาสาสมัคร": volunteer,
            "โรงพยาบาล": hospital
    };
    
    L.control.layers(baseLayers,overlays, 
    {
        collapsed: false,
        autoZIndex: false
    }).addTo(map);        
//    var group = new L.featureGroup([marker]);
//    map.fitBounds(group.getBounds());
  </script>
