<?php
use yii\bootstrap\Html;
use yii\bootstrap\ButtonDropdown;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;


$this->title = Yii::t('backend', 'CHRONIC KIDNEY DISEASE PREVENTION IN THE NORTHEAST OF THAILAND');
$this->params['breadcrumbs'][] = ['label'=>'Modules','url'=>  yii\helpers\Url::to('/tccbots/my-module')];
$this->params['breadcrumbs'][] = Yii::t('backend', 'CKDNET');
$this->registerCss('
.primary{
     cursor: pointer;
}
.p1 {
    left: 15.2%;
    top: 1.5%;
    width: 24%;
    height: 6%;
    position: absolute;
}
.p2 {
    left: 38%;
    top: 13.5%;
    width: 16%;
    height: 6%;
    position: absolute;
}
.p3 {
    left: 18%;
    top: 26.8%;
    width: 15%;
    height: 6%;
    position: absolute;
}
.p4 {
    left: 52.5%;
    top: 26.8%;
    width: 24%;
    height: 6%;
    position: absolute;
}  
.p5 {
    left: 36.6%;
    top: 37.9%;
    width: 26.5%;
    height: 8%;
    position: absolute;
}
.p6 {
    left: 38.2%;
    top: 47.9%;
    width: 23%;
    height: 6%;
    position: absolute;
}
.p7 {
    left: 38.2%;
    top: 55.4%;
    width: 23%;
    height: 6.5%;
    position: absolute;
}
.p8 {
    left: 38.2%;
    top: 63.9%;
    width: 23%;
    height: 6%;
    position: absolute;
}
.p9 {
    left: 62.4%;
    top: 38.5%;
    width: 27.5%;
    height: 7.6%;
    position: absolute;
}
.p10 {
    left: 66.6%;
    top: 47.5%;
    width: 23.5%;
    height: 8.6%;
    position: absolute;
}
.p11 {
    left: 66.6%;
    top: 58.5%;
    width: 23.5%;
    height: 5.6%;
    position: absolute;
}
.p12 {
    left: 66.6%;
    top: 66.5%;
    width: 22.5%;
    height: 5.6%;
    position: absolute;
}
.p13 {
    left: 1.5%;
    top: 14.5%;
    width: 17.5%;
    height: 5.6%; 
    position: absolute;
}
.p14 {
    left: 1.5%;
    top: 26.8%;
    width: 16%;
    height: 6%;
    position: absolute;
}
.p15 {
    left: 1.5%;
    top: 47.7%;
    width: 15.5%;
    height: 8.6%;
    position: absolute;
}
.p16 {
    left: 18%;
    top: 47.5%;
    width: 15.5%;
    height: 8.6%;
    position: absolute;
}
.p17 {
    left: 4.5%;
    top: 65.5%;
    width: 31.5%;
    height: 6.6%;
    position: absolute;
}
.p18 {
    left: 36.5%;
    top: 86.5%;
    width: 26.5%;
    height: 8.6%;
    position: absolute;
}
.p19 {
    left: 4%;
    top: 81.5%;
    width: 35%;
    height: 3.5%;
    position: absolute;
}

.p20 {
    left: 4%;
    top: 85%;
    width: 35%;
    height: 2.5%;
    position: absolute;
}
.p21 {
    left: 4%;
    top: 87.5%;
    width: 35%;
    height: 2.5%;
    position: absolute;
}

.p22 {
    left: 4%;
    top: 90%;
    width: 35%;
    height: 2.5%;
    position: absolute;
}

.p23 {
    left: 4%;
    top: 92.5%;
    width: 35%;
    height: 3%;
    position: absolute;
}

.p24 {
    left: 4%;
    top: 95.5%;
    width: 35%;
    height: 3.5%;
    position: absolute;
}

.p25 {
    left: 71.5%;
    top: 83.6%;
    width: 22.5%;
    height: 5.6%;
    position: absolute;
}
.p26 {
    left: 71.5%;
    top: 86.7%;
    width: 22.5%;
    height: 5.6%;
    position: absolute;
}
.p27 {
    left: 71.5%;
    top: 89.8%;
    width: 22.5%;
    height: 5.6%;
    position: absolute;
}
.p28 {
    left: 71.5%;
    top: 93%;
    width: 22.5%;
    height: 5.6%;
    position: absolute;
}
#btnSetting{
    margin-left: 0px;float: right;    
}
//a.info-app.pull-left.btn-help {
//    margin-left: -22%;
//}
//a.info-app.pull-left.btn-help > i{
//    color:#f0ad4e;
//}
a.info-app.btn-help > i{
    float: inherit;
}
.app-popover-fw{
    width: 300px;
}
.app-popover-fw-setting{
    width: 150px;
}

@media  (min-width: 1024px){
    .statustdc{
        margin-left:71%
    } 
}
@media  (max-width:768px){
    .statustdc{
        margin-left:34%
    } 
}

#div-settinginfo{
   padding: 10px;
}
#hr-setting{
    margin-top: 10px;
    margin-bottom: 10px;
    
}
');
$type = Yii::$app->session['type_ckd'];
$idcen = Yii::$app->session['mode_ckd'];
//echo "SESSION mode_ckd >> ".Yii::$app->session['mode_ckd'];
//echo "</br> idcen >> ".$idcen;
//echo "</br> Cookie >> ".$_COOKIE['save_modeckd'];
$ckdDiagram = backend\modules\ckdnet\classes\CkdnetFunc::getCkdDiagram(Yii::$app->user->identity->userProfile->sitecode, '', '',$idcen,$type);
$mapDiagram = yii\helpers\ArrayHelper::map($ckdDiagram, 'id', 'value');

//appxq\sdii\utils\VarDumper::dump($mapDiagram);

?>
<div class="ckd-default-index">
    
    <!--
    <div class="row">
        <div class="col-md-12">
            <?php
            echo Html::checkbox('search_year1', false, ['id'=>'search_year1', 'style'=>' transform: scale(2);']);
            echo Html::label('&nbsp;&nbsp;&nbsp;ปีงบประมาณปัจจุบัน', 'search_year1');
            echo '<br>';
            echo Html::checkbox('search_year2', false, ['id'=>'search_year2', 'style'=>' transform: scale(2);']);
            echo Html::label('&nbsp;&nbsp;&nbsp;ปีงบประมาณอื่นๆ', 'search_year2');
            echo '<br>';
            echo Html::checkbox('search_year3', false, ['id'=>'search_year3', 'style'=>' transform: scale(2);']);
            echo Html::label('&nbsp;&nbsp;&nbsp;เลือกตามช่วงเวลา', 'search_year3');
            ?>
            <div class="row">
                <div class="col-md-6">
                    ระบุช่วงเวลา <?php echo Html::checkbox('set_between', $_GET['set_between']); ?>
                    <?php
                    echo \common\lib\damasac\widgets\DMSDateWidget::widget([
                        //'model'=>$model,
                        'id'=>'start_date',
                        'value' => $_GET['start_date'] ? $_GET['start_date'] : '01'.(date('-m-').(date('Y')+543)),
                        'name' =>'start_date',
                    ]);
                    ?>
                </div>
                <div class="col-md-6">
                    ถึง
                    <?php
                    echo \common\lib\damasac\widgets\DMSDateWidget::widget([
                        //'model'=>$model,
                        'id'=>'end_date',
                        'value' => $_GET['end_date'] ? $_GET['end_date'] : (date('d-m-').(date('Y')+543)),
                        'name' =>'end_date',
                    ]);
                    ?>
                </div>
            </div>
            <hr>
        </div>
    </div>
    -->
    
    <div class="col-md-8 col-sm-8 col-xs-8" style="max-width:1051px;">
        <img class="img-rounded img-responsive" src="<?= \yii\helpers\Url::to(['@web/images/ckd/ckd-diagram_1.jpg']) ?>" alt="" >
        <a class="p1 primary text-center" href="<?= \yii\helpers\Url::to(['inv/index', 'state'=>1,'idcen'=>$idcen,'type'=>$type]) ?>" 
            <?php
                if($idcen == 'central'){
                   echo "data-toggle=\"popover\" data-trigger=\"hover\"  data-content=\"มีจำนวนประชากรที่เสียชีวิตแล้วในระดับ Central : ".number_format($mapDiagram[1]['death_central'])." คน\" "; 
                }
            ?>
        >
            <table style="height: 100%;width: 100%;">
                <tbody><tr><td class="text-center"  style="vertical-align: bottom !important;color: red;"><?= number_format($mapDiagram[1]['count'])?></td></tr></tbody>
            </table>
        </a>
        <a class="p2 primary " href="<?= \yii\helpers\Url::to(['inv/index', 'state'=>2,'idcen'=>$idcen]) ?>"><table style="height: 100%;width: 100%;"><tbody><tr><td class="text-center"  style="vertical-align: bottom !important;color: red;"><?= number_format($mapDiagram[2])?></td></tr></tbody></table></a>
        <a class="p3 primary" href="<?= \yii\helpers\Url::to(['inv/index', 'state'=>3,'idcen'=>$idcen]) ?>"><table style="height: 100%;width: 100%;"><tbody><tr><td class="text-center"  style="vertical-align: bottom !important;color: red;"><?= number_format($mapDiagram[3])?></td></tr></tbody></table></a>
        <a class="p4 primary" href="<?= \yii\helpers\Url::to(['inv/index', 'state'=>4,'idcen'=>$idcen]) ?>"><table style="height: 100%;width: 100%;"><tbody><tr><td class="text-center"  style="vertical-align: bottom !important;color: red;"><?= number_format($mapDiagram[4])?></td></tr></tbody></table></a>
        <a class="p5 primary" href="<?= \yii\helpers\Url::to(['inv/index', 'state'=>5,'idcen'=>$idcen]) ?>"><table style="height: 100%;width: 100%;"><tbody><tr><td class="text-center"  style="vertical-align: bottom !important;color: red;"><?= number_format($mapDiagram[5])?></td></tr></tbody></table></a>
        <a class="p6 primary" href="<?= \yii\helpers\Url::to(['inv/index', 'state'=>6,'idcen'=>$idcen]) ?>"><table style="height: 100%;width: 100%;"><tbody><tr><td class="text-center"  style="vertical-align: bottom !important;color: red;"><?= number_format($mapDiagram[6])?></td></tr></tbody></table></a>
        <a class="p7 primary" href="<?= \yii\helpers\Url::to(['inv/index', 'state'=>7,'idcen'=>$idcen]) ?>"><table style="height: 100%;width: 100%;"><tbody><tr><td class="text-center"  style="vertical-align: bottom !important;color: red;"><?= number_format($mapDiagram[7])?></td></tr></tbody></table></a>
        <a class="p8 primary" href="<?= \yii\helpers\Url::to(['inv/index', 'state'=>8,'idcen'=>$idcen]) ?>"><table style="height: 100%;width: 100%;"><tbody><tr><td class="text-center"  style="vertical-align: bottom !important;color: red;"><?= number_format($mapDiagram[8])?></td></tr></tbody></table></a>
        <a class="p9 primary" href="<?= \yii\helpers\Url::to(['inv/index', 'state'=>9,'idcen'=>$idcen]) ?>"><table style="height: 100%;width: 100%;"><tbody><tr><td class="text-center"  style="vertical-align: bottom !important;color: red;"><?= number_format($mapDiagram[9])?></td></tr></tbody></table></a>
        <a class="p10 primary" href="<?= \yii\helpers\Url::to(['inv/index', 'state'=>10,'idcen'=>$idcen]) ?>"><table style="height: 100%;width: 100%;"><tbody><tr><td class="text-center"  style="vertical-align: bottom !important;color: red;"><?= number_format($mapDiagram[10])?></td></tr></tbody></table></a>
        <a class="p11 primary" href="<?= \yii\helpers\Url::to(['inv/index', 'state'=>11,'idcen'=>$idcen]) ?>"><table style="height: 100%;width: 100%;"><tbody><tr><td class="text-center"  style="vertical-align: bottom !important;color: red;"><?= number_format($mapDiagram[11])?></td></tr></tbody></table></a>
        <a class="p12 primary" href="<?= \yii\helpers\Url::to(['inv/index', 'state'=>12,'idcen'=>$idcen]) ?>"><table style="height: 100%;width: 100%;"><tbody><tr><td class="text-center"  style="vertical-align: bottom !important;color: red;"><?= number_format($mapDiagram[12])?></td></tr></tbody></table></a>
        <a class="p13 primary" href="<?= \yii\helpers\Url::to(['inv/index', 'state'=>13,'idcen'=>$idcen]) ?>"><table style="height: 100%;width: 100%;"><tbody><tr><td class="text-center"  style="vertical-align: bottom !important;color: red;"><?= number_format($mapDiagram[13])?></td></tr></tbody></table></a>
        <a class="p14 primary" href="<?= \yii\helpers\Url::to(['inv/index', 'state'=>14,'idcen'=>$idcen]) ?>"><table style="height: 100%;width: 100%;"><tbody><tr><td class="text-center"  style="vertical-align: bottom !important;color: red;"><?= number_format($mapDiagram[14])?></td></tr></tbody></table></a>
        <a class="p15 primary" href="<?= \yii\helpers\Url::to(['inv/index', 'state'=>15,'idcen'=>$idcen]) ?>"><table style="height: 100%;width: 100%;"><tbody><tr><td class="text-center"  style="vertical-align: bottom !important;color: red;"><?= number_format($mapDiagram[15])?></td></tr></tbody></table></a>
        <a class="p16 primary" href="<?= \yii\helpers\Url::to(['inv/index', 'state'=>16,'idcen'=>$idcen]) ?>"><table style="height: 100%;width: 100%;"><tbody><tr><td class="text-center"  style="vertical-align: bottom !important;color: red;"><?= number_format($mapDiagram[16])?></td></tr></tbody></table></a>
        <a class="p17 primary" href="<?= \yii\helpers\Url::to(['inv/index', 'state'=>17,'idcen'=>$idcen]) ?>"><table style="height: 100%;width: 100%;"><tbody><tr><td class="text-center"  style="vertical-align: bottom !important;color: red;"><?= number_format($mapDiagram[17])?></td></tr></tbody></table></a>
        <a class="p18 primary" href="<?= \yii\helpers\Url::to(['inv/index', 'state'=>18,'idcen'=>$idcen]) ?>"><table style="height: 100%;width: 100%;"><tbody><tr><td class="text-center"  style="vertical-align: middle !important; color: red;"><?= number_format($mapDiagram[18])?></td></tr></tbody></table></a>
    <?php 
//        $ckdStageDiagram = \backend\modules\ckdnet\classes\CkdnetFunc::getCkdStageDiagram(Yii::$app->user->identity->userProfile->sitecode,'','',$idcen,$type);
//        $mapStageDiagram = yii\helpers\ArrayHelper::map($ckdStageDiagram,'id','value');    
        
        $cssnum=19;
        $number=19;
        $i=1;
        for($i;$i<7;$i++){
           echo '<a class="p'.$cssnum.' primary" href="/ckd/inv/index?state='.$number.".".$i.'"><table style="height: 100%;width: 100%;"><tbody><tr><td class="text-center"  style="vertical-align: middle !important;color: red;">'. number_format($mapDiagram[$number.".".$i]) .'</td></tr></tbody></table></a>';
           
//           foreach($value as $keystage => $stage){
//                echo '<a class="p'.$i.' primary" href="/ckd/inv/index?state='.$i.'"><table style="height: 100%;width: 100%;"><tbody><tr><td class="text-center"  style="vertical-align: middle !important;color: red;">'. number_format($stage) .'</td></tr></tbody></table></a>';
//            $i++;
//            }
            $cssnum++;
        }
    ?>
         <a class="p25 primary" href="<?= \yii\helpers\Url::to(['../inv/inv-person/index', 'module'=>1484731832014481200]) ?>"><table style="height: 100%;width: 100%;"><tbody><tr><td class="text-center"  style="vertical-align: middle !important; color: red;"><?= number_format($mapDiagram[25])?></td></tr></tbody></table></a>
         <a class="p26 primary" href="<?= \yii\helpers\Url::to(['inv/index', 'state'=>20.2,'idcen'=>$idcen]) ?>"><table style="height: 100%;width: 100%;"><tbody><tr><td class="text-center"  style="vertical-align: middle !important; color: red;"><?= number_format($mapDiagram[20.2])?></td></tr></tbody></table></a>
         <a class="p27 primary" href="<?= \yii\helpers\Url::to(['inv/index', 'state'=>20.3,'idcen'=>$idcen]) ?>"><table style="height: 100%;width: 100%;"><tbody><tr><td class="text-center"  style="vertical-align: middle !important; color: red;"><?= number_format($mapDiagram[20.3])?></td></tr></tbody></table></a>
         <a class="p28 primary" href="<?= \yii\helpers\Url::to(['../inv/inv-menu/view', 'module'=>46,'gtype'=>0,'id'=>44]) ?>"><table style="height: 100%;width: 100%;"><tbody><tr><td class="text-center"  style="vertical-align: middle !important; color: red;"><?= number_format($mapDiagram[28])?></td></tr></tbody></table></a>
    </div> 
    
    <div class="col-md-4 col-sm-4 col-xs-4">
        <div class="row">
            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                <?= \yii\helpers\Html::a('<i class="fa fa-cog fa-lg"></i>',NULL,[
                    'class'=>'btn btn-default pull-right',
                    'id'=>'btn-settinginfo'
                ]);?>
            </div>
            <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
                <div id="div-settinginfo" class="panel panel-primary" style="display:none;"><!-- style="display:none;" -->
                    <div class="row">
                        <div class="col-md-12">
                            <?php IF($tdc['status'] == '1'){?>
                            <label> สถานะ :: <span class="badge" style='background-color:green'> TDC Connected</span></label> 
                            <?php }ELSE IF($tdc['status'] == '0'){ ?>
                            <label> สถานะ :: <span class="badge" style='background-color:red'> TDC Disconnected</span></label> 
                            <?php }ELSE IF($tdc['status'] == NULL){
                                echo "สถานะ :: <center> - </center>";
                            }?>
                            <br>
                            <label>ส่งข้อมูลจาก TDC ล่าสุดเข้ามาเมื่อ <span class="text-primary">
                            <?php 
                            if($tdc['last_sync_time'] != '' || $tdc['last_sync_time'] != NULL){
                                echo common\lib\sdii\components\utils\SDdate::mysql2phpThDateSmall($tdc['last_sync_time']); 
                                echo substr($tdc['last_sync_time'],10);    
                            } else{
                                echo " - ";
                            }  
                            ?>
                            </span></label>           
                        </div>
                        
                        <div class="col-md-12">
                            <?php
                                if (Yii::$app->session['mode_ckd'] == 'central') { $data_select = 'CUP Mode'; } else if(Yii::$app->session['mode_ckd'] == 'hospital') { $data_select = 'Hospital Mode';}else{ $data_select = 'CUP Mode'; }
                                echo "<div class=\"alert alert-info\" id=\"alert-mode\">  ขณะนี้คุณอยู่ในโหมด : ";
                                echo ButtonDropdown::widget([
                                    'label' => $data_select,
                                    'dropdown' => [
                                        'items' => [
                                            ['label' => 'Hospital Mode', 'url' => yii\helpers\Url::to(['/ckd', 'idcen' => 'hospital']),'options' => ['class'=>'data-li']],
                                            ['label' => 'CUP Mode', 'url' => yii\helpers\Url::to(['/ckd', 'idcen' => 'central']),'options' => ['class'=>'data-li']],
                                        ],
                                    ],
                                    'options' => [
                                        'class' => 'btn btn-primary',
                                    ],
                                ]);
                            ?>
                            <a style="cursor:pointer" class="info-app btn-help">
                                <i class="fa fa-question-circle fa-lg" aria-hidden="true"></i>
                            </a>
                            <?php
                                echo "</div>";
                            ?>
                            <hr id="hr-setting">
                            <div class="row">
                                <div class="col-md-12">
                                    
                                    <div id="btnSetting" data-toggle="popover" data-trigger="hover" data-placement="bottom"  data-content="เซ็ตการตั้งค่า LAB ">
                                    <?php
                                    echo Html::button("<i class='fa fa-cog fa-lg'></i> <font size='3' style='vertical-align: middle;'>LAB</font> ", [
                                        'data-url' => yii\helpers\Url::to(['map-lap/index']),
                                        //'value' => yii\helpers\Url::to(['map-lap/index'] ),
                                        'class' => 'btn btn-success pull-right',
                                        'id' => 'btn-modal-maplap',
                                        'title' => "Mapping Lab"
                                    ]);
                                    ?>
                                    </div>
                                </div>
                            </div>
                            
                            <hr id="hr-setting">
                            <?php $form = ActiveForm::begin(['id' => 'ckdTypesearch','action'=> Url::to(['/ckd'])]); ?>
                            <dl class="dl-horizontal">
                                <dt><p class="text-left"><?= \yii\helpers\Html::label('<i class="fa fa-cog"></i> Population Type');?></p></dt>
                                <dd>
                                <?php   
                                    $listType = ['1' => 'Type 1', '2' => 'Type 2', '3' => 'Type 3', '4' => 'Type 4','5' => 'Type 5']; 
                                    echo Html::checkboxList('listType',explode(',',Yii::$app->session['type_ckd']), $listType,['id'=>'listType','class'=>'listTypes','separator'=>'<br/>']);
                                    echo Html::hiddenInput("save_typeckd",1);
                                    echo Html::hiddenInput("save_modeckd",Yii::$app->session['mode_ckd']);
                                    //isset(Yii::$app->session['save_typeckd'])? Yii::$app->session['save_typeckd']:1
                                ?> 
                                </dd>
                            </dl>
                            <hr id="hr-setting">
                            <?= Html::a('<i class="fa fa-refresh fa-lg" aria-hidden="true"></i> Default',NULL,
                                    ['class'=>'btn btn-default btn-sm','id'=>'btnresetckdtype']);?>
                            <?= Html::submitButton('<i class="fa fa-search fa-lg" aria-hidden="true"></i> Submit',['class'=>'btn btn-primary btn-sm pull-right','id'=>'btn-ckdTypesearch']);?>
                            <?php ActiveForm::end(); ?>
                        </div> <!-- col-md-10 -->
                        
                    </div>  <!-- row  -->
                </div><!-- div-settinginfo -->
            </div><!-- col  -->
        </div><!-- row  -->
    </div>
    
</div>
<div class="row">
<div class="col-md-8 col-sm-8 col-xs-8" style="max-width:1051px;">
<?php
        
    if (Yii::$app->user->can('administrator')) {
	?>
    <p class="text-right">
        <?= Html::a('&nbsp;&nbsp;&nbsp;', ['update-report', 'code' => $model->code, 'id' => $model->id], ['class' => 'fa fa-pencil-square-o fa-2x']) ?>
    </p>
    <?php
    }   
?>
    <?php echo Yii::$app->keyStorage->get('ckd.report_desc1');?>
    </div>
</div>    
<?=  appxq\sdii\widgets\ModalForm::widget([
    'id' => 'modal-lg',
    'size'=>'modal-lg',
    'tabindexEnable' => false,
]);
?>

<?php  
if ($_SESSION['showsettinginfo'] != "1") {
    $hidesetting = "$('#div-settinginfo').hide();";
}else{
    $hidesetting = "$('#div-settinginfo').show();";
    $_SESSION['showsettinginfo']=0;
}

$this->registerJs("
$('#btn-modal-maplap').on('click', function() {
    $('#modal-lg .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
    $('#modal-lg').modal('show')
    .find('.modal-content')
    .load($(this).attr('data-url'));
});

//=========== Template alert ==============
var newPopoverTemplate = '<div class=\"popover app-popover-fw\" role=\"popover\"><div class=\"arrow\"></div><h3 class=\"popover-title\"></h3><div class=\"popover-content\"></div></div>';
var newPopoverSetting = '<div class=\"popover app-popover-fw-setting\" role=\"popover\"><div class=\"arrow\"></div><h3 class=\"popover-title\"></h3><div class=\"popover-content\"></div></div>';
//=========== JS alert TotalPop Death central ==============
    $('.p1').popover({template: newPopoverTemplate });
//=========== JS alert Help Hospital & Centrl ==============
    //$('#btnSetting').popover({template: newPopoverSetting });
//=========== JS alert Help Hospital & Centrl ==============
    $('.btn-help').hover(function () {
        $(this).popover({
            title: \"<i class='fa fa-certificate fa-lg text-warning'></i> <strong>คำแนะนำ</strong>\",
            content: \"<code>Hopital Mode</code> คือ จำนวนประชากรผู้รับบริการทั้งหมด ในหน่วยบริการ Hospital Based  <br /><br /> <code>Central Mode</code> คือ จำนวนประชากรผู้รับบริการทั้งหมด ในหน่วยบริการ Hospital Based โดยผลการตรวจจะสามารถดูได้ในระดับหน่วยบริการปฐมภูมิ \",
            html: true,
            placement: \"bottom\",
            container: \"body\",
        }).popover('show');
    }, function () {
        $(this).popover('hide');
    });
    
    
    $('#btn-settinginfo').click(function(){
        $('#div-settinginfo').toggle();
    });
     
    $('#btnresetckdtype').click(function(){
        $('#btnresetckdtype').html('<i class=\"fa fa-refresh fa-spin fa-lg\" aria-hidden=\"true\"></i> Default');
        var url = '".Url::to(['/ckd'])."';
        var reset = 1;
        $.ajax({
            url:url,
            data:{btnreset:reset},
            success:function(result){
                location.reload();
            },error: function (xhr, ajaxOptions, thrownError) {
                console.log(xhr);
            }

        });

    });


");?>
<!--/////////////START.css ปุ่มselect central---hospital/////////////////////-->
<style>
#alert-mode{
    padding: 5px;
    text-align: center;
    margin-bottom: 0px; 
}
.dropdown-menu>li.data-li>a{
        text-decoration: none !important;
}
</style>
<!--/////////////END.css ปุ่มselect central---hospital/////////////////////-->