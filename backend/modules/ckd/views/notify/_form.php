<?php

use yii\helpers\Html;
use kartik\select2\Select2;
use yii\web\JsExpression;
use yii\web\View;

$format = <<< SCRIPT
    function format(state) {
            if(state.id==1){
                return '<font color="green">'+state.text+'</font>';
            }
            if(state.id==2){
                return '<font color="red">'+state.text+'</font>';
            }
            if(state.id==3){
                return '<b>'+state.text+'</b>';
            }

    }
        
    $('.rootSearch').on('click','.btnbtn-remove',function(){
        $(this).parents('.rootSearch').remove();
    });
SCRIPT;
$escape = new JsExpression("function(m) { return m; }");
$this->registerJs($format, View::POS_HEAD);

 unset($results[$no]);
?>

<div class="rootSearch">
    <div class="clearfix"></div>
    <hr>
    <div class="col-sm-7">
        <?=
        Select2::widget([
            'id' => 'field'.$numId,
            'name' => 'field[]',
//            'options' => ['placeholder' => 'เลือกการแจ้งเตือน ...'],
            'data' => $results,
//                    'value'=> $no
        ])
        ?>
    </div>
    <div class="col-sm-2">
        <?=
        Select2::widget([
            'id' => 'status'.$numId,
            'name' => 'status[]',
            'options' => ['placeholder' => 'เลือกสถานะ ...'],
            'value' => "2",
            'data' => ['1' => "<i class='fa fa-circle'></i>", '2' => "<i class='fa fa-circle'></i>", '3' => 'N/A'],
            'pluginOptions' => [
                'templateResult' => new JsExpression('format'),
                'templateSelection' => new JsExpression('format'),
                'escapeMarkup' => $escape
            ],
        ])
        ?>
    </div>
    <div class="col-sm-2">
        <?=
        Select2::widget([
            'id' => 'and-or'.$numId,
            'name' => 'and-or[]',
            'data' => ['AND' => 'AND', 'OR' => 'OR']
        ])
        ?>
    </div>
    <div class="col-sm-1">
        <?= Html::button("<i class='glyphicon glyphicon-remove'></i>", ['class' => 'btn btn-danger btnbtn-remove']); ?>
    </div>
</div>

