<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;
use yii\web\JsExpression;
use yii\web\View;
use kartik\select2\Select2;

//$field = $value;
//\appxq\sdii\utils\VarDumper::dump($result);
$format = <<< SCRIPT
function format(state) {
        if(state.id==1){
            return '<font color="green">'+state.text+'</font>';
        }
        if(state.id==2){
            return '<font color="red">'+state.text+'</font>';
        }
        if(state.id==3){
            return '<b>'+state.text+'</b>';
        }
    
}
SCRIPT;
$escape = new JsExpression("function(m) { return m; }");
$this->registerJs($format, View::POS_HEAD);



//$field = [
//    'c_bp_140_90' => 'BP ≤ 140/90 mmHg',
//    'c_acei_arbs' => 'ได้รับ ACEI /ARBs',
//    'egfr_sl' => 'มีค่า Slope ของ eGFR ต่อระยะเวลา ≥ 4',
//    'c_hb' => 'Hb  ≥ 10 g/dl',
//    'c_hba1c' => 'HbA1C  < 7 %',
//    'c_ldl' => 'LDL cholesterol < 100 mg/dl',
//    'c_srp' => 'มีค่า serum potassium < 5.5 mEq/L',
//    'c_srb' => 'มีค่า serum bicarbonate > 22 mEq/L',
//    'q9' => 'ได้รับการประเมิน Urine Protien Strip  (รพ.ระดับ F-M)',
//    'c_upcraer' => 'ได้รับการประเมิน urine protein creatinine ratio (UPCR) หรือ urine protein 24 hrs',
//    'c_upcr_aer_500' => 'UPCR < 500 mg/g  หรือ Urine  protein 24 hrs < 500 mg/day',
//    'c_spp_45' => 'Serum phosphate < 4.5 mg/L',
//    'c_srh' => 'Serum parathyroid hormone (PTH)  อยู่ในเกณฑ์ปกติ',
//    'q14' => 'ได้รับการเตรียม AVF พร้อม ก่อนเริ่ม hemodialysis',
//    'q15' => 'ได้เข้าร่วม educational class ในหัวข้อต่างๆครบ',
//    'q16' => 'แจ้งค่า eGFR และระยะของ CKD  ครั้งนี้ และภายใน 3 เดือนที่ผ่านมา',
//    'c_egfr_60' => 'แจ้งค่า eGFR < 60 ควรพบอายุรแพทย์',
//    'q18' => 'มีอัตราการลดลงของ GFR มากกว่าปีละ  5 ml/min/1.73 m2 ',
//    'c_egfr_30' => 'eGFR < 30 ได้รับการให้คำปรึกษาเรื่อง RRT',
//    'c_metfn_egfr_30' => 'eGFR < 30 ยังได้รับยา metformin',
//    'c_acei_arbs_aer30' => 'ผู้ป่วยเบาหวานที่มี albuminuria > 30 mg/day และไม่ได้รับยา ACEI หรือ ARB',
//    'c_acei_arbs_aer300' => 'ไม่ได้เป็นเบาหวานมี albuminuria > 300 mg/day และไม่ได้รับยา ACEI หรือ ARB',
//    'x23' => 'ได้รับ ACEi/ARB  ระวังการเกิด AKI และ hyperkalemia ควรได้รับคำแนะนำการปฏิบัติตัว',
//    'x24' => 'เคยได้รับยา ACEI หรือ ARB แต่ต้องหยุดยาเพราะมี adverse event',
//    'x25' => 'มี ปริมาณโปรตีนในปัสสาวะ ≥ 1+',
//    'x26' => 'ผู้ป่วยเบาหวานที่ไม่ได้ตรวจ urine albumin อย่างน้อย 2 ครั้งต่อปี',
//    'x27' => 'ไม่ได้เป็น CKD และได้รับยา NSAIDs นานกว่า 2 สัปดาห์',
//    'c_nsaids' => 'ผู้ป่วย CKD ที่ได้รับยา NSAIDs ',
//    'x29' => 'เวลาสั่งจ่ายยาในโปรแกรม ถ้ามียาที่ต้องปรับตาม CrCl ถ้าสั่งผิดให้มีการเตือนทุกครั้ง หรือถ้าไม่มีการปรับตาม CrCl ให้เตือนทุกครั้ง',
//    'x30' => 'ประวัติการเคยวินิจฉัยว่ามี acute kidney injury ล่าสุด วัน เดือน ปี เท่าไร ',
//    'x31' => 'มีโรคหรือภาวะที่เสี่ยงต่อการเกิด CKD ได้รับการตรวจ serum creatinine และ urine protein หรือ albumin ปีละ 1 ครั้ง (เบาหวาน, ความดันโลหิตสูง, เก๊าท์, SLE, อายุมากกว่า 60 ปี, ได้รับยาที่ nephrotoxic, ติดเชื้อทางเดินปัสสาวะส่วนบน ≥ 3 ครั้งต่อปี, มีโรคหัวใจและหลอดเลือด, polycystic kidney disease, โรคไตตั้งแต่กำเนิด, มีประวัติโรคไตในครอบครัว)',
//    'c_egfr_sl_02' => 'ตรวจ UA พบโปรตีนหรือหรือเม็ดเลือดแดง ในปัสสาวะพิจารณาส่งปรึกษาแพทย์',
//    'c_egfr_sl_03' => 'Serum creatinine เพิ่มขึ้นมากกว่าหรือเท่ากับ 0.3 mg/dL: acute kidney injury ควรหาสาเหตุ',
//    'c_egfr_30x' => 'eGFR < 30 ml/min/1.73m2 พิจารณาส่งปรึกษาอายุรแพทย์โรคไต',
//    'c_egfr_15' => 'eGFR < 15 ml/min/1.73m2 ควรส่งประเมินเตรียมความพร้อมในการทำบำบัดทดแทนไต ',
//    'c_k_nsaids' => 'ACI หรือ Potassium >5'
//];
//unset($field[$no]);
?>

<div class="panel-body viewNotify">
    <div style="margin:10px 10px 10px 10px;" >
        <h4>หมายเหตุ  </h4> <font color='green'><i class='fa fa-circle'> </i></font> = อยู่ในเกณฑ์ &nbsp; | &nbsp; <font color='red'><i class='fa fa-circle'> </i></font>  = ไม่อยู่ในเกณฑ์ &nbsp; | &nbsp; N/A = ยังไม่ได้รับการตรวจ

        <?php
        $form = ActiveForm::begin([
                    'id' => 'add-key',
                    'action' => Url::to(['/ckd/report/add-key']),
                    'options' => ['data-pjax' => true]
        ]);
        ?>



        <?php
//                    Html::button("<i class='glyphicon glyphicon-arrow-left'></i> ย้อนกลับ", [
//                        'id' => 'btn-back',
//                        'onClick' => "backDrilldown('" . Url::to($urlBackDrilldown) . "')",
//                        'class' => 'btn btn-primary',
//                        'style' => 'margin-right:5px;'
//                    ]);
        ?>
        <hr>
        <label>กุญแจถอดรหัสข้อมูล</label><br/>

        <?=
        Html::passwordInput('key_ckd', Yii::$app->session['key_ckd_report'] ? Yii::$app->session['key_ckd_report'] : $key_ckd_report, [
            'class' => 'form-control inline',
            'style' => 'width:40%',
            'placeholder' => 'กรุณากรอกกุญแจถอดรหัส'
        ]);
        ?>

        <?=
        Html::submitButton("<i class='fa fa-key' id='spin'></i> ถอดรหัส", [
            'id' => 'btn-key',
            'class' => 'btn btn-success',
            'style' => 'margin-left:5px;'
        ]);
        ?>
        <?=
        Html::Button("<i class='fa fa-cog' id='spin'></i> ปรับแต่งรายงาน", [
            'id' => 'btn-setting',
            'class' => 'btn btn-primary',
            'style' => 'margin-left:5px;'
        ]);
        ?>

        <br><br>
        <?= Html::checkbox('save_key', Yii::$app->session['save_ckd_key'] != '' ? true : false, ['label' => 'จดจำคีย์นี้', 'id' => 'save_key']) ?>
        <?= Html::checkbox('convert', Yii::$app->session['convert_ckd_char'] != '' ? true : false, ['label' => 'เข้ารหัสแบบ tis620 (กรณีชื่อ-สกุล อ่านไม่ออกให้กาช่องนี้ด้วย)', 'id' => 'convert']) ?>



        <?php ActiveForm::end(); ?>
        <div class="clearfix"></div>
        <hr>
        <div id="dataGrid">

        </div>
    </div>
</div>

<div class="modal fade" id="modalSetting" ><!--data-keyboard="false" data-backdrop="static" role="dialog">-->
    <div class="modal-dialog" style="width: 70%;">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
<font color='green'><i class='fa fa-circle'> </i></font> = อยู่ในเกณฑ์ &nbsp; | &nbsp; <font color='red'><i class='fa fa-circle'> </i></font>  = ไม่อยู่ในเกณฑ์ &nbsp; | &nbsp; N/A = ยังไม่ได้รับการตรวจ
            </div>
            <div class="modal-body">
                <div class="row">
                    <?php
                    ActiveForm::begin([
                        'id' => 'searchForm',
                        'action' => Url::to(['notify/notify']),
                        'options' => ['data-pjax' => true]
                    ]);
                    ?>
                    
<!--                    <div class="col-sm-3">
                        <b>เลือกโหมดการแสดงข้อมูล</b>
                     <?php 
//                     echo   Select2::widget([
//                                    'id' => 'mode',
//                                    'name' => 'mode',
//                                    'hideSearch' => true,
//                                    'options' => ['placeholder' => 'เลือกสถานะ ...'],
//                                    'value' => "hospital",
//                                    'data' => ['hospital' => "Hospital", 'central' => "Central"],
//                                ])
                                ?>
                    </div>
                    <div class="col-sm-9">
                        <span class="text-danger">Hospital Mode </span>คือ จำนวนประชากรผู้รับบริการทั้งหมด ในหน่วยบริการ Hospital Based
                        <br>
                        <span class="text-danger">Central Mode </span>คือ จำนวนประชากรผู้รับบริการทั้งหมด ในหน่วยบริการ Hospital Based โดยผลการตรวจจะสามารถดูได้ในหน่วยบริการปฐมภูมิ
                    </div>
                    
                    <div class="clearfix"></div>
                    <hr>-->

                    <div class="col-sm-7">
                        <b> หัวข้อการแจ้งเตือน</b>
                    </div>
                    <div class="col-sm-2">
                        <b>สถานะ</b>
                    </div>
                    <div class="col-sm-2">
                        <b> การเชื่อมโยง</b>
                    </div>
                    <div class="col-sm-1">
                        <?= Html::button("<i class='glyphicon glyphicon-plus'></i>", ['class' => 'btn btn-success btnbtn-add']); ?>
                    </div>
                    <div class="clearfix"></div>
                    <hr>
                    <div class="form">


                        <div class="rootSearch">
                            <div class="col-sm-7">
                                <?php
                                echo Html::input('text', '', $results[$no], [
                                    'class' => 'form-control',
                                    'readonly' => true
                                ]);
                                echo Html::hiddenInput('field[]', $no);
//                                echo Select2::widget([
//                                    'id' => 'field0',
//                                    'name' => 'field[]',
//                                    'options' => ['placeholder' => 'เลือกการแจ้งเตือน ...', 'readonly' => true],
//                                    'data' => $field,
//                                    'value' => $no
//                                ]);
                                ?>
                            </div>
                            <div class="col-sm-2">
                                <?=
                                Select2::widget([
                                    'id' => 'status0',
                                    'name' => 'status[]',
                                    'options' => ['placeholder' => 'เลือกสถานะ ...'],
                                    'value' => "2",
                                    'data' => ['1' => "<i class='fa fa-circle'></i>", '2' => "<i class='fa fa-circle'></i>", '3' => 'N/A'],
                                    'pluginOptions' => [
                                        'templateResult' => new JsExpression('format'),
                                        'templateSelection' => new JsExpression('format'),
                                        'escapeMarkup' => $escape
                                    ],
                                ])
                                ?>
                            </div>
                            <div class="col-sm-2">
                                <?php
                                echo Html::input('text', 'and-or[]','AND', [
                                    'id' => 'and-or0',
                                    'class' => 'form-control',
                                    'readonly' => true
                                ]);
//                                Select2::widget([
//                                    'id' => 'and-or0',
//                                    'name' => 'and-or[]',
//                                    'data' => ['AND' => 'AND', 'OR' => 'OR']
//                                ])
                                ?>
                            </div>
                            <div class="col-sm-1">
                                <?php // echo Html::button("<i class='glyphicon glyphicon-remove'></i>", ['class' => 'btn btn-danger btnbtn-remove']); ?>
                            </div>
                        </div>

                    </div>
                    <div class="clearfix"></div>
                    <hr>


                    <?php ActiveForm::end(); ?>
                </div>
            </div>
            <div class="modal-footer" id="drugFooter">

                <?=
                Html::submitButton("<i class='glyphicon glyphicon-search' id='spin'></i> ตกลง", [
                    'id' => 'btn-search',
                    'class' => 'btn btn-success',
                    'style' => 'margin-left:5px;'
                ]);
                ?>
                <?=
                Html::button("<i class='glyphicon glyphicon-remove'> </i> ยกเลิก", [
                    'id' => 'btnCancle',
                    'class' => 'btn btn-danger btnCancle',
                ]);
                ?>
            </div>
        </div>

    </div>
</div>

<?php
$this->registerJs("
    
//    $(document).on('ready',function(){
        $('#dataGrid').html('<div class=\"sdloader \"><i class=\"fa fa-spinner fa-pulse fa-fw fa-3x\"></i></div>');
        $.ajax({
            url:'/ckd/notify/notify?no=" . $no . "',
            dataType:'HTML',
            success:function(result){
                $('#dataGrid').html(result);
            },error: function (xhr, ajaxOptions, thrownError) {
                $('#dataGrid').html('<div class=\'alert alert-danger\'>เกิดข้อผิดพลาด!</div>');
            }
        });
//    });
    
    var numId = 0;
    $('#btn-setting').on('click',function(){
//        $('#modalSetting .modal-body').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
        $('#modalSetting').modal('show');
//            $('#modalSetting')
//            .find('.modal-body')
//            .load('notify/setting?no=" . $no . "');
    });
    
    
    $('#btnCancle').on('click',function(){
        $('#modalSetting').modal('hide');
//        $('.error').remove();
//        for(var i=1;i<=numId;i++){
//            $('#field'+i).parents('.rootSearch').remove();
//        }
          $('.error').remove();
          $('.divSpin').remove();
//        numId = 0;
    });
    
    $('#modalSetting').on('hidden.bs.modal', function () {
        $('.error').remove();
        $('.divSpin').remove();
    });
    
    $('.btnbtn-add').on('click',function(){
        $('.form').append('<div class=\"rootSearch divSpin\"> <div class=\"clearfix\"></div><div class=\"sdloader\"><i class=\"fa fa-spinner fa-pulse fa-fw fa-3x\"></i></div></div>');
        numId++;
        $.ajax({
            url:'/ckd/notify/load-form?no=" . $no . "&numId='+numId,
            dataType:'HTML',
            success:function(result){
                $('.error').remove();
                $('.divSpin').remove();
                $('.form').append(result);
            },error: function (xhr, ajaxOptions, thrownError) {
                $('.divSpin').remove();
                $('.form').append('<div class=\'clearfix error\'></div><hr class=\'error\'><div class=\'alert alert-danger error\'>เกิดข้อผิดพลาด!</div>');
            }
        });
    });
    

    $('#btn-search').on('click',function(){
//        console.log($('#searchForm').attr('action'));return false;
        $('#modalSetting').modal('hide');
        var \$form = $('#searchForm');
//        setTimeout(function(){
            var rootNotify = $('.viewNotify');
            rootNotify = rootNotify.parents('.notify');
            var notify = rootNotify.children('.collapse');
            $('#dataGrid').html('<div class=\"sdloader \"><i class=\"fa fa-spinner fa-pulse fa-fw fa-3x\"></i></div>');
            $.ajax({
                url:\$form.attr('action')+'?no=" . $no . "',
                data:\$form.serialize(),
//                dataType:'HTML',
//                method:'POST',
                success:function(result){
                    $('#dataGrid').html(result);
                },error: function (xhr, ajaxOptions, thrownError) {
                    $('#dataGrid').html('<div class=\'alert alert-danger\'>เกิดข้อผิดพลาด!</div>');
                }
            });
//        },500);
       
    });
    
    
    $('form#add-key').on('beforeSubmit', function(e) {
            var rootNotify = $(this).parents('.viewNotify');
            rootNotify = rootNotify.parents('.notify');
            var notify = rootNotify.children('.collapse');
            $('#spin').addClass('fa fa-spinner fa-pulse fa-1x fa-fw');
            var \$form = $(this);
            var \$form2 = $('#searchForm');
            $.post(
                \$form.attr('action'), //serialize Yii2 form
                \$form.serialize()
            ).done(function(result) {
                if(result.status == 'success') {
                    $.ajax({
                        url:'/ckd/notify/notify?no=" . $no . "'+'&page='+$('.page').val()+'&per-page='+$('.per-page').val(),
//                        dataType:'HTML',
//                        method:'POST',
                        data:\$form2.serialize(),
                        success:function(result){
                        " . appxq\sdii\helpers\SDNoty::show('"ถอดรหัสสำเร็จ !"', '"success"') . "
                             $('#spin').removeClass('fa fa-spinner fa-pulse fa-1x fa-fw');
                             $('#spin').addClass('fa fa-key');
                            $('#dataGrid').html(result);
                        },error: function (xhr, ajaxOptions, thrownError) {
                            $('#dataGrid').html('<div class=\'alert alert-danger\'>เกิดข้อผิดพลาด!</div>');
                        }
                    });
                } else {
                    " . \appxq\sdii\helpers\SDNoty::show('"ถอดรหัสผิดพลาด !"', '"error"') . "
                } 
            }).fail(function() {
                " . \appxq\sdii\helpers\SDNoty::show("'" . \appxq\sdii\helpers\SDHtml::getMsgError() . "Server Error'", '"error"') . "
                $('#spin').removeClass('fa fa-spinner fa-pulse fa-1x fa-fw');
                console.log('server error');
            });
            
            return false;
        });
        
        $('#save_key').on('change',function(){
            var \$form = $('#add-key');
            $.post(
                \$form.attr('action'), //serialize Yii2 form
                \$form.serialize()
            )
            
            return false;
        });
        
        $('#convert').on('change',function(){
            var \$form = $('#add-key');
            $.post(
                \$form.attr('action'), //serialize Yii2 form
                \$form.serialize()
            )
            
            return false;
        });


    ");
?>

