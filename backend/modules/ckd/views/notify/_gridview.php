<?php

yii\widgets\Pjax::begin(['id' => 'w0-pjax', 'timeout' => false,
    'enablePushState' => false])
?>
<?php

echo kartik\grid\GridView::widget([
    'id' => 'gridNotify',
    'dataProvider' => $dataProvider,
    'layout' => '<div class="pull-right">{export}
        {toggleData}</div><div class="clearfix"></div>{items}<center>{pager}</center>',
    'columns' => $gridColumns,
    'summary' => false,
    'pjax' => true,
    'toolbar' => [
    ],
    'bordered' => $bordered,
    'striped' => false,
    'condensed' => $condensed,
    'responsive' => true,
    'hover' => true,
]);
?>
<?php yii\widgets\Pjax::end(); ?>
