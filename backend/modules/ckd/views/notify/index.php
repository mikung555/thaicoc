<?php

use kartik\tabs\TabsX;
use yii\helpers\Url;
use yii\helpers\Html;

/* @var $this yii\web\View */
$this->title = Yii::t('backend', 'Module: CKDNET');
$this->params['breadcrumbs'][] = ['label' => 'Modules', 'url' => Url::to('/notify/index')];
$this->params['breadcrumbs'][] = Yii::t('backend', 'สิ่งแจ้งเตือน');




?>



<div class="ckdnet-default-index" enableAjaxValidation="true" data-pjax="true">
    

    <div class="panel-group" id="accordion">

        <?php
//        
//        \appxq\sdii\utils\VarDumper::dump($result);
        $i = 1;
        foreach ($results as $key => $value) {
            ?>
            <div class="panel panel-default notify" data-url="<?= Url::to(['notify/show-notify', 'no'=>$key]) ?>">
                <div class="panel-heading headNotify">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse<?= $i ?>"><?php echo $i . "." . $value; ?></a>
                    </h4>
                </div>

                <div id="collapse<?= $i ?>" class="panel-collapse collapse">
                   
                </div>
            </div>
            <?php
            $i++;
        }
        ?>

    </div> 


</div>

<?php
$this->registerJs("
   
    $('.notify').on('click','.headNotify',function(){
        $('.notify').children('.collapse').html('');
        var notify = $(this).parent('.notify');
        notify = notify.children('.collapse');
        if(!notify.hasClass('collapse in')){
            $('html,body').animate({
                scrollTop: $(this).offset().top -100
              }, 500);
            notify.html('<div class=\"sdloader \"><i class=\"fa fa-spinner fa-pulse fa-fw fa-3x\"></i></div>');
            $.ajax({
                url:$(this).parent().attr('data-url'),
                dataType:'HTML',
                success:function(result){
                    notify.html(result);
                },error: function (xhr, ajaxOptions, thrownError) {
                    notify.html('<div class=\'alert alert-danger\'>เกิดข้อผิดพลาด!</div>');
                }
            });
        }
        
    })


    ");
?>

