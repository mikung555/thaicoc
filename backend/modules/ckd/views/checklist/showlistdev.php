<?php
/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\helpers\Url;
use appxq\sdii\widgets\GridView;
use appxq\sdii\widgets\ModalForm;
use appxq\sdii\helpers\SDNoty;
use appxq\sdii\helpers\SDHtml;

$checkClass = ['1'=>"fa fa-check",'0'=>"fa fa-close"];
$checkClassTr = ['1'=>"trcheck",'0'=>"trclose",'2'=>"trna"];
$colorStyle = ['1'=>"blue",'0'=>"red"];
     ///  \appxq\sdii\utils\VarDumper::dump($checkClass[$data[0]['q1']]);

?>

<div>
  <div class="modal-header">
<button type="button" class="btn btn-success btn-sm pull-right" data-dismiss="modal" aria-hidden="true">ปิด</button>
<a class="btn btn-danger" href="#">รายการแจ้งเตือนรายบุคคล</a>
</div>
    <div class="modal-body">
        <h3 align='center'>
        <i class="fa fa-check" style="color:<?= $colorStyle['1'];?>"></i> ผ่าน ,  <i class="fa fa-close" style="color:<?= $colorStyle['0'];?>"></i> ไม่เป็นไปตามรายการที่ระบุ
</h3>
        
<table class="table table-bordered table-striped" id="myTable">
<tbody>
<th>
<td width="71">ที่</td>
<td width="868">รายการแจ้งเตือนรายบุคคล</td>
<td width="71">ผล</td>
<td width="320">ค่าจริง</td>
</th>
<tr class="trsearch">
<td width="71"></td>
<td width="868">ค้นประเภทข้อมูล</td>
<td ><input type="text" id="filterInput"  /></td>
<td style='color:<?= $colorStyle[$data[0]['c_bp_140_90']] ?>'><?php echo $data[0]['c_bp_140_90'] != '' ? $data[0]['sbp']: '' ; ?>  <?php echo $data[0]['c_bp_140_90'] != '' ? $data[0]['dbp']: '' ; ?>  <?php echo $data[0]['c_bp_140_90'] != '' ? $data[0]['bp_date']: '' ; ?></td>
</tr>
<tr class="trcheck <?php echo $data[0]['c_bp_140_90'] != '' ? $checkClassTr[$data[0]['c_bp_140_90']]: 'trna' ; ?>">
<td width="71">1</td>
<td width="868">BP &le; 140/90 mmHg</td>
<td ><i id="q1check" class="<?php echo $data[0]['c_bp_140_90'] != '' ? $checkClass[$data[0]['c_bp_140_90']]: 'na' ; ?>" style='color:<?= $colorStyle[$data[0]['c_bp_140_90']] ?>'></i></td>
<td style='color:<?= $colorStyle[$data[0]['c_bp_140_90']] ?>'><?php echo $data[0]['c_bp_140_90'] != '' ? $data[0]['sbp']: '' ; ?>  <?php echo $data[0]['c_bp_140_90'] != '' ? $data[0]['dbp']: '' ; ?>  <?php echo $data[0]['c_bp_140_90'] != '' ? $data[0]['bp_date']: '' ; ?></td>
</tr>
<tr class="trcheck <?php echo   $data[0]['c_acei_arbs'] != '' ? $checkClassTr[$data[0]['c_acei_arbs']]: 'trna' ; ?>">
<td width="71">2</td>
<td width="868">ได้รับ ACEI /ARBs</td>
<td ><i id="q1check" class="<?php echo   $data[0]['c_acei_arbs'] != '' ? $checkClassTr[$data[0]['c_acei_arbs']]: 'tr' ; ?>" style='color:<?= $colorStyle[$data[0]['c_acei_arbs']] ?>'></i></td>
<td style='color:<?= $colorStyle[$data[0]['c_acei_arbs']] ?>'><?php echo $data[0]['c_acei_arbs'] != '' ? $data[0]['acei']: '' ; ?><?php echo $data[0]['c_acei_arbs'] != '' ? $data[0]['acei_date']: '' ; ?>  <?php echo $data[0]['c_acei_arbs'] != '' ? $data[0]['arbs_date']: '' ; ?></td>

</tr>
<tr class="trcheck <?php echo $data[0]['c_egfr_sl_0'] != '' ? $checkClassTr[$data[0]['c_egfr_sl_0']]: 'tr' ; ?>">
<td width="71">3</td>
<td width="868">มีค่า Slope ของ eGFR ต่อระยะเวลา &ge; 0</td>
<td ><i id="q1check" class="<?php echo $data[0]['c_egfr_sl_0'] != '' ? $checkClass[$data[0]['c_egfr_sl_0']]: 'na' ; ?>" style='color:<?= $colorStyle[$data[0]['c_egfr_sl_0']] ?>'></i></td>
<td style='color:<?= $colorStyle[$data[0]['c_egfr_sl_0']] ?>'><?php echo $data[0]['c_egfr_sl_0'] != '' ? $data[0]['egfr_sl']: '' ; ?>  <?php echo $data[0]['c_egfr_sl_0'] != '' ? $data[0]['egfr_date']: '' ; ?></td>

</tr>
<tr class="trcheck <?php echo $data[0]['c_hb'] != '' ? $checkClassTr[$data[0]['c_hb']]: 'trna' ; ?>">
<td width="71">4</td>
<td width="868">Hb&nbsp; &ge; 10 g/dl</td>
<td ><i id="q1check" class="<?php echo $data[0]['c_hb'] != '' ? $checkClass[$data[0]['c_hb']]: 'na' ; ?> " style='color:<?= $colorStyle[$data[0]['c_hb']] ?>'></i></td>
<td style='color:<?= $colorStyle[$data[0]['c_hb']] ?>'><?php echo $data[0]['c_hb'] != '' ? $data[0]['hb']: '' ; ?>  <?php echo $data[0]['c_hb'] != '' ? $data[0]['hb_date']: '' ; ?></td>

</tr>
<tr class="trcheck <?php echo $data[0]['c_hba1c'] != '' ? $checkClassTr[$data[0]['c_hba1c']]: 'trna' ; ?>">
<td width="71">5</td>
<td width="868">HbA1C&nbsp; &lt; 7 %</td>
<td ><i id="q1check" class="<?php echo $data[0]['c_hba1c'] != '' ? $checkClass[$data[0]['c_hba1c']]: 'na' ; ?>" style='color:<?= $colorStyle[$data[0]['c_hba1c']] ?>'></i></td>
<td style='color:<?= $colorStyle[$data[0]['c_hba1c']] ?>'><?php echo $data[0]['c_hba1c'] != '' ? $data[0]['hba1c']: '' ; ?>  <?php echo $data[0]['c_hba1c'] != '' ? $data[0]['hba1c_date']: '' ; ?></td>

</tr>
<tr class="trcheck <?php echo $data[0]['c_ldl'] != '' ? $checkClassTr[$data[0]['c_ldl']]: 'trna' ; ?>">
<td width="71">6</td>
<td width="868">LDL cholesterol &lt; 100 mg/dl</td>
<td ><i id="q1check" class="<?php echo $data[0]['c_ldl'] != '' ? $checkClass[$data[0]['c_ldl']]: 'na' ; ?>" style='color:<?= $colorStyle[$data[0]['c_ldl']] ?>'></i></td>
<td style='color:<?= $colorStyle[$data[0]['c_ldl']] ?>'><?php echo $data[0]['c_ldl'] != '' ? $data[0]['ldl']: '' ; ?>  <?php echo $data[0]['c_ldl'] != '' ? $data[0]['ldl_date']: '' ; ?></td>

</tr>
<tr class="trcheck <?php echo $data[0]['c_srp'] != '' ? $checkClassTr[$data[0]['c_srp']]: 'trna' ; ?>">
<td width="71">7</td>
<td width="868">มีค่า serum potassium &lt; 5.5 mEq/L</td>
<td ><i id="q1check" class="<?php echo $data[0]['c_srp'] != '' ? $checkClass[$data[0]['c_srp']]: 'na' ; ?>" style='color:<?= $colorStyle[$data[0]['c_srp']] ?>'</i></td>
<td style='color:<?= $colorStyle[$data[0]['c_srp']] ?>'><?php echo $data[0]['c_srp'] != '' ? $data[0]['srp']: '' ; ?>  <?php echo $data[0]['c_srp'] != '' ? $data[0]['srp']: '' ; ?></td>

</tr>
<tr class="trcheck <?php echo $data[0]['c_srb'] != '' ? $checkClassTr[$data[0]['c_srb']]: 'trna' ; ?>">
<td width="71">8</td>
<td width="868">มีค่า serum bicarbonate &gt; 22 mEq/L</td>
<td ><i id="q1check" class="<?php echo $data[0]['c_srb'] != '' ? $checkClass[$data[0]['c_srb']]: 'na' ; ?>" style='color:<?= $colorStyle[$data[0]['c_srb']] ?>'></i></td>
<td style='color:<?= $colorStyle[$data[0]['c_srb']] ?>'><?php echo $data[0]['c_srb'] != '' ? $data[0]['srb']: '' ; ?>  <?php echo $data[0]['c_srb'] != '' ? $data[0]['srb_date']: '' ; ?></td>

</tr>
<tr class="trcheck <?php echo $data[0]['q9'] != '' ? $checkClassTr[$data[0]['q9']]: 'trna' ; ?>">
<td width="71">9</td>
<td width="868">ได้รับการประเมิน Urine Protien Strip&nbsp; (รพ.ระดับ F-M)</td>
<td ><i id="q1check" class="<?php echo $data[0]['q9'] != '' ? $checkClass[$data[0]['q9']]: 'na' ; ?>" style='color:<?= $colorStyle[$data[0]['q9']] ?>'></i></td>
<td style='color:<?= $colorStyle[$data[0]['q9']] ?>'><?php echo $data[0]['q9'] != '' ? $data[0]['q9x']: '' ; ?></td>

</tr>
<tr class="trcheck <?php echo $data[0]['c_upcr'] != '' ? $checkClassTr[$data[0]['c_upcr']]: 'trna' ; ?>">
<td width="71">10</td>
<td width="868">ได้รับการประเมิน urine protein creatinine ratio (UPCR) หรือ urine protein 24 hrs</td>
<td ><i id="q1check" class="<?php echo $data[0]['c_upcr'] != '' ? $checkClass[$data[0]['c_upcr']]: 'na' ; ?>" style='color:<?= $colorStyle[$data[0]['c_upcr']] ?>'></i></td>
<td style='color:<?= $colorStyle[$data[0]['c_upcr']] ?>'><?php echo $data[0]['c_upcr'] != '' ? $data[0]['upc_dater']: '' ; ?></td>

</tr>
<tr class="trcheck <?php echo $data[0]['c_upcr_aer_500'] != '' ? $checkClassTr[$data[0]['c_upcr_aer_500']]: 'trna' ; ?>">
<td width="71">11</td>
<td width="868">UPCR &lt; 500 mg/g&nbsp; หรือ Urine&nbsp; protein 24 hrs &lt; 500 mg/day</td>
<td ><i id="q1check" class="<?php echo $data[0]['c_upcr_aer_500'] != '' ? $checkClass[$data[0]['c_upcr_aer_500']]: 'na' ; ?>" style='color:<?= $colorStyle[$data[0]['c_upcr_aer_500']] ?>'></i></td>
<td style='color:<?= $colorStyle[$data[0]['c_upcr_aer_500']] ?>'><?php echo $data[0]['c_upcr_aer_500'] != '' ? $data[0]['upcr_aer_500']: '' ; ?></td>

</tr>
<tr class="trcheck <?php echo $data[0]['c_spp_45'] != '' ? $checkClassTr[$data[0]['c_spp_45']]: 'trna' ; ?>" >
<td width="71">12</td>
<td width="868">Serum phosphate &lt; 4.5 mg/L</td>
<td ><i id="q1check" class="<?php echo $data[0]['c_spp_45'] != '' ? $checkClass[$data[0]['c_spp_45']]: 'na' ; ?>" style='color:<?= $colorStyle[$data[0]['c_spp_45']] ?>'></i></td>
<td style='color:<?= $colorStyle[$data[0]['c_spp_45']] ?>'><?php echo $data[0]['c_spp_45'] != '' ? $data[0]['spp']: '' ; ?> <?php echo $data[0]['c_spp_45'] != '' ? $data[0]['spp_date']: '' ; ?></td>

</tr>
<tr class="trcheck <?php echo $data[0]['q13'] != '' ? $checkClassTr[$data[0]['q13']]: 'trna' ; ?>">
<td width="71">13</td>
<td width="868">Serum parathyroid hormone (PTH)&nbsp; อยู่ในเกณฑ์ปกติ</td>
<td ><i id="q1check" class="<?php echo $data[0]['q13'] != '' ? $checkClass[$data[0]['q13']]: 'na' ; ?>" style='color:<?= $colorStyle[$data[0]['q13']] ?>'></i></td>
<td style='color:<?= $colorStyle[$data[0]['q13']] ?>'><?php echo $data[0]['q13'] != '' ? $data[0]['srh']: '' ; ?> <?php echo $data[0]['q13'] != '' ? $data[0]['srh_date']: '' ; ?></td>

</tr>
<tr class="trcheck <?php echo $data[0]['q14'] != '' ? $checkClassTr[$data[0]['q14']]: 'trna' ; ?>">
<td width="71">14</td>
<td width="868">ได้รับการเตรียม AVF พร้อม ก่อนเริ่ม hemodialysis</td>
<td ><i id="q1check" class="<?php echo $data[0]['q14'] != '' ? $checkClass[$data[0]['q14']]: 'na' ; ?>" style='color:<?= $colorStyle[$data[0]['q14']] ?>'></i></td>
<td style='color:<?= $colorStyle[$data[0]['q14']] ?>'><?php echo $data[0]['q14'] != '' ? $data[0]['q14x']: '' ; ?></td>

</tr>
<tr class="trcheck <?php echo $data[0]['q15'] != '' ? $checkClassTr[$data[0]['q15']]: 'trna' ; ?>">
<td width="71">15</td>
<td width="868">ได้เข้าร่วม educational class ในหัวข้อต่างๆครบ</td>
<td ><i id="q1check" class="<?php echo $data[0]['q15'] != '' ? $checkClass[$data[0]['q15']]: 'na' ; ?>" style='color:<?= $colorStyle[$data[0]['q15']] ?>'></i></td>
<td style='color:<?= $colorStyle[$data[0]['q15']] ?>'><?php echo $data[0]['q15'] != '' ? $data[0]['q15x']: '' ; ?></td>

</tr>
<tr class="trcheck <?php echo $data[0]['q16'] != '' ? $checkClass[$data[0]['q16']]: 'na' ; ?>">
<td width="71">16</td>
<td width="868">แจ้งค่า eGFR และระยะของ CKD&nbsp; ครั้งนี้ และภายใน 3 เดือนที่ผ่านมา</td>
<td ><i id="q1check" class="<?php echo $data[0]['q16'] != '' ? $checkClass[$data[0]['q16']]: 'na' ; ?>" style='color:<?= $colorStyle[$data[0]['q16']] ?>'></i></td>
<td style='color:<?= $colorStyle[$data[0]['q16']] ?>'><?php echo $data[0]['q16'] != '' ? $data[0]['q16x']: '' ; ?></td>

</tr>
<tr class="trcheck <?php echo $data[0]['c_egfr_60'] != '' ? $checkClassTr[$data[0]['c_egfr_60']]: 'trna' ; ?>">
<td width="71">17</td>
<td width="868">แจ้งค่า eGFR &lt; 60 ควรพบอายุรแพทย์</td>
<td ><i id="q1check" class="<?php echo $data[0]['c_egfr_60'] != '' ? $checkClass[$data[0]['c_egfr_60']]: 'na' ; ?>" style='color:<?= $colorStyle[$data[0]['c_egfr_60']] ?>'></i></td>
<td style='color:<?= $colorStyle[$data[0]['c_egfr_60']] ?>'><?php echo $data[0]['c_egfr_60'] != '' ? $data[0]['egfr']: '' ; ?> <?php echo $data[0]['c_egfr_60'] != '' ? $data[0]['egfr_date']: '' ; ?></td>

</tr>
<tr class="trcheck <?php echo $data[0]['q18'] != '' ? $checkClassTr[$data[0]['q18']]: 'trna' ; ?>">
<td width="71">18</td>
<td width="868">มีอัตราการลดลงของ GFR มากกว่าปีละ&nbsp; 5 ml/min/1.73 m2&nbsp;</td>
<td ><i id="q1check" class="<?php echo $data[0]['q18'] != '' ? $checkClass[$data[0]['q18']]: 'na' ; ?>" style='color:<?= $colorStyle[$data[0]['q18']] ?>'></i></td>
<td style='color:<?= $colorStyle[$data[0]['q18']] ?>'><?php echo $data[0]['q18'] != '' ? $data[0]['q18x']: '' ; ?></td>

</tr>
<tr class="trcheck <?php echo $data[0]['c_egfr_30'] != '' ? $checkClassTr[$data[0]['c_egfr_30']]: 'trna' ; ?>">
<td width="71">19</td>
<td width="868"> eGFR &lt; 30 ได้รับการให้คำปรึกษาเรื่อง RRT</td>
<td ><i id="q1check" class="<?php echo $data[0]['c_egfr_30'] != '' ? $checkClass[$data[0]['c_egfr_30']]: 'na' ; ?>" style='color:<?= $colorStyle[$data[0]['c_egfr_30']] ?>'></i></td>
<td style='color:<?= $colorStyle[$data[0]['c_egfr_30']] ?>'><?php echo $data[0]['c_egfr_30'] != '' ? $data[0]['egfr']: '' ; ?></td>

</tr>
<tr class="trcheck <?php echo $data[0]['c_metfn_egfr_30'] != '' ? $checkClassTr[$data[0]['c_metfn_egfr_30']]: 'trna' ; ?>">
<td width="71">20</td>
<td> eGFR &lt; 30 ยังได้รับยา metformin</td>
<td ><i id="q1check" class="<?php echo $data[0]['c_metfn_egfr_30'] != '' ? $checkClass[$data[0]['c_metfn_egfr_30']]: 'na' ; ?>" style='color:<?= $colorStyle[$data[0]['c_metfn_egfr_30']] ?>'></i></td>
<td style='color:<?= $colorStyle[$data[0]['c_metfn_egfr_30']] ?>'><?php echo $data[0]['c_metfn_egfr_30'] != '' ? $data[0]['egfr']: '' ; ?> <?php echo $data[0]['c_metfn_egfr_30'] != '' ? $data[0]['metfn']: '' ; ?></td>

</tr>
<tr class="trcheck <?php echo $data[0]['c_acei_arbs_aer30'] != '' ? $checkClassTr[$data[0]['c_acei_arbs_aer30']]: 'trna' ; ?>">
<td width="71">21</td>
<td width="868">ผู้ป่วยเบาหวานที่มี albuminuria &gt; 30 mg/day และไม่ได้รับยา ACEI หรือ ARB</td>
<td ><i id="q1check" class="<?php echo $data[0]['c_acei_arbs_aer30'] != '' ? $checkClass[$data[0]['c_acei_arbs_aer30']]: 'na' ; ?>" style='color:<?= $colorStyle[$data[0]['c_acei_arbs_aer30']] ?>'></i></td>
<td style='color:<?= $colorStyle[$data[0]['c_acei_arbs_aer30']] ?>'><?php echo $data[0]['c_acei_arbs_aer30'] != '' ? $data[0]['acei']: '' ; ?> <?php echo $data[0]['c_acei_arbs_aer30'] != '' ? $data[0]['arbs']: '' ; ?> <?php echo $data[0]['c_acei_arbs_aer30'] != '' ? $data[0]['aer']: '' ; ?></td> 

</tr>
<tr class="trcheck <?php echo $data[0]['c_acei_arbs_aer300'] != '' ? $checkClassTr[$data[0]['c_acei_arbs_aer300']]: 'trna' ; ?>">
<td width="71">22</td>
<td width="868">ไม่ได้เป็นเบาหวานมี albuminuria &gt; 300 mg/day และไม่ได้รับยา ACEI หรือ ARB</td>
<td ><i id="q1check" class="<?php echo $data[0]['c_acei_arbs_aer300'] != '' ? $checkClass[$data[0]['c_acei_arbs_aer300']]: 'na' ; ?>" style='color:<?= $colorStyle[$data[0]['22']] ?>'></i></td>
<td style='color:<?= $colorStyle[$data[0]['c_acei_arbs_aer300']] ?>'><?php echo $data[0]['c_acei_arbs_aer300'] != '' ? $data[0]['aer']: '' ; ?> <?php echo $data[0]['c_acei_arbs_aer300'] != '' ? $data[0]['aer_date']: '' ; ?></td>

</tr>
<tr class="trcheck <?php echo $data[0]['x23'] != '' ? $checkClassTr[$data[0]['x23']]: 'trna' ; ?>">
<td width="71">23</td>
<td width="868">ได้รับ ACEi/ARB&nbsp; ระวังการเกิด AKI และ hyperkalemia ควรได้รับคำแนะนำการปฏิบัติตัว</td>
<td ><i id="q1check" class="<?php echo $data[0]['x23'] != '' ? $checkClass[$data[0]['x23']]: 'na' ; ?>" style='color:<?= $colorStyle[$data[0]['x23']] ?>'></i></td>
<td style='color:<?= $colorStyle[$data[0]['x23']] ?>'><?php echo $data[0]['x23'] != '' ? $data[0]['x23x']: '' ; ?></td>

</tr>
<tr class="trcheck <?php echo $data[0]['x24'] != '' ? $checkClassTr[$data[0]['x24']]: 'trna' ; ?>">
<td width="71">24</td>
<td width="868">เคยได้รับยา ACEI หรือ ARB แต่ต้องหยุดยาเพราะมี adverse event</td>
<td ><i id="q1check" class="<?php echo $data[0]['x24'] != '' ? $checkClass[$data[0]['x24']]: 'na' ; ?>" style='color:<?= $colorStyle[$data[0]['x24']] ?>'></i></td>
<td style='color:<?= $colorStyle[$data[0]['x24']] ?>'><?php echo $data[0]['x24'] != '' ? $data[0]['x24x']: '' ; ?></td>

</tr>
<tr class="trcheck <?php echo $data[0]['x25'] != '' ? $checkClassTr[$data[0]['x25']]: 'trna' ; ?>">
<td width="71">25</td>
<td width="868">มี ปริมาณโปรตีนในปัสสาวะ &ge; 1+</td>
<td ><i id="q1check" class="<?php echo $data[0]['x25'] != '' ? $checkClass[$data[0]['x25']]: 'na' ; ?>" style='color:<?= $colorStyle[$data[0]['x25']] ?>'></i></td>
<td style='color:<?= $colorStyle[$data[0]['x25']] ?>'><?php echo $data[0]['x25'] != '' ? $data[0]['x25x']: '' ; ?></td>

</tr>
<tr class="trcheck <?php echo $data[0]['x26'] != '' ? $checkClassTr[$data[0]['x26']]: 'trna' ; ?>">
<td width="71">26</td>
<td width="868">ผู้ป่วยเบาหวานที่ไม่ได้ตรวจ urine albumin อย่างน้อย 2 ครั้งต่อปี</td>
<td ><i id="q1check" class="<?php echo $data[0]['x26'] != '' ? $checkClass[$data[0]['x26']]: 'na' ; ?>" style='color:<?= $colorStyle[$data[0]['x26']] ?>'></i></td>
<td style='color:<?= $colorStyle[$data[0]['x26']] ?>'><?php echo $data[0]['x26'] != '' ? $data[0]['x26x']: '' ; ?></td>

</tr>
<tr class="trcheck <?php echo $data[0]['x27'] != '' ? $checkClassTr[$data[0]['x27']]: 'trna' ; ?>">
<td width="71">27</td>
<td width="868">ไม่ได้เป็น CKD และได้รับยา NSAIDs นานกว่า 2 สัปดาห์</td>
<td ><i id="q1check" class="<?php echo $data[0]['x27'] != '' ? $checkClass[$data[0]['x27']]: 'na' ; ?>" style='color:<?= $colorStyle[$data[0]['x27']] ?>'></i></td>
<td style='color:<?= $colorStyle[$data[0]['x27']] ?>'><?php echo $data[0]['x27'] != '' ? $data[0]['x27x']: '' ; ?></td>

</tr>
<tr class="trcheck <?php echo $data[0]['c_nsaids'] != '' ? $checkClassTr[$data[0]['c_nsaids']]: 'trna' ; ?>">
<td width="71">28</td>
<td width="868">ผู้ป่วย CKD ที่ได้รับยา NSAIDs&nbsp;</td>
<td ><i id="q1check" class="<?php echo $data[0]['c_nsaids'] != '' ? $checkClass[$data[0]['c_nsaids']]: 'na' ; ?>" style='color:<?= $colorStyle[$data[0]['c_nsaids']] ?>'></i></td>
<td style='color:<?= $colorStyle[$data[0]['c_nsaids']] ?>'><?php echo $data[0]['c_nsaids'] != '' ? $data[0]['nsaids']: '' ; ?> <?php echo $data[0]['c_nsaids'] != '' ? $data[0]['nsaids_date']: '' ; ?></td>

</tr>
<tr class="trcheck <?php echo $data[0]['x29'] != '' ? $checkClassTr[$data[0]['x29']]: 'trna' ; ?>">
<td width="71">29</td>
<td width="868">เวลาสั่งจ่ายยาในโปรแกรม ถ้ามียาที่ต้องปรับตาม CrCl ถ้าสั่งผิดให้มีการเตือนทุกครั้ง หรือถ้าไม่มีการปรับตาม CrCl ให้เตือนทุกครั้ง</td>
<td ><i id="q1check" class="<?php echo $data[0]['x29'] != '' ? $checkClass[$data[0]['x29']]: 'na' ; ?>" style='color:<?= $colorStyle[$data[0]['x29']] ?>'></i></td>
<td style='color:<?= $colorStyle[$data[0]['x29']] ?>'><?php echo $data[0]['x29'] != '' ? $data[0]['x29x']: '' ; ?></td>

</tr>
<tr class="trcheck <?php echo $data[0]['c_egfr_sl_00'] != '' ? $checkClassTr[$data[0]['c_egfr_sl_00']]: 'trna' ; ?>">
<td width="71">30</td>
<td width="868">&nbsp;ประวัติการเคยวินิจฉัยว่ามี acute kidney injury ล่าสุด วัน เดือน ปี เท่าไร&nbsp;</td>
<td ><i id="q1check" class="<?php echo $data[0]['c_egfr_sl_00'] != '' ? $checkClass[$data[0]['c_egfr_sl_00']]: 'na' ; ?>" style='color:<?= $colorStyle[$data[0]['c_egfr_sl_00']] ?>'></i></td>
<td style='color:<?= $colorStyle[$data[0]['c_egfr_sl_00']] ?>'><?php echo $data[0]['c_egfr_sl_00'] != '' ? $data[0]['c_egfr_sl_00x']: '' ; ?></td>

</tr>
<tr class="trcheck <?php echo $data[0]['c_egfr_sl_01'] != '' ? $checkClassTr[$data[0]['c_egfr_sl_01']]: 'trna' ; ?>">
<td width="71">31</td>
<td width="868">มีโรคหรือภาวะที่เสี่ยงต่อการเกิด CKD ได้รับการตรวจ serum creatinine และ urine protein หรือ albumin ปีละ 1 ครั้ง (เบาหวาน, ความดันโลหิตสูง, เก๊าท์, SLE, อายุมากกว่า 60 ปี, ได้รับยาที่ nephrotoxic, ติดเชื้อทางเดินปัสสาวะส่วนบน &ge; 3 ครั้งต่อปี, มีโรคหัวใจและหลอดเลือด, polycystic kidney disease, โรคไตตั้งแต่กำเนิด, มีประวัติโรคไตในครอบครัว)</td>
<td ><i id="q1check" class="<?php echo $data[0]['c_egfr_sl_01'] != '' ? $checkClass[$data[0]['c_egfr_sl_01']]: 'na' ; ?>" style='color:<?= $colorStyle[$data[0]['c_egfr_sl_01']] ?>'></i></td>
<td style='color:<?= $colorStyle[$data[0]['c_egfr_sl_01']] ?>'><?php echo $data[0]['c_egfr_sl_01'] != '' ? $data[0]['c_egfr_sl_01x']: '' ; ?></td>

</tr>
<tr class="trcheck <?php echo $data[0]['c_egfr_sl_02'] != '' ? $checkClassTr[$data[0]['c_egfr_sl_02']]: 'trna' ; ?>">
<td width="71">32</td>
<td width="868">ตรวจ UA พบโปรตีนหรือหรือเม็ดเลือดแดง ในปัสสาวะพิจารณาส่งปรึกษาแพทย์</td>
<td ><i id="q1check" class="<?php echo $data[0]['c_egfr_sl_02'] != '' ? $checkClass[$data[0]['c_egfr_sl_02']]: 'na' ; ?>" style='color:<?= $colorStyle[$data[0]['c_egfr_sl_02']] ?>'></i></td>
<td style='color:<?= $colorStyle[$data[0]['c_egfr_sl_02']] ?>'><?php echo $data[0]['c_egfr_sl_02'] != '' ? $data[0]['c_egfr_sl_02x']: '' ; ?></td>

</tr>
<tr class="trcheck <?php echo $data[0]['c_egfr_sl_03'] != '' ? $checkClassTr[$data[0]['c_egfr_sl_03']]: 'trna' ; ?>">
<td width="71">33</td>
<td width="868">Serum creatinine เพิ่มขึ้นมากกว่าหรือเท่ากับ 0.3 mg/dL: acute kidney injury ควรหาสาเหตุ</td>
<td ><i id="q1check" class="<?php echo $data[0]['c_egfr_sl_03'] != '' ? $checkClass[$data[0]['c_egfr_sl_03']]: 'na' ; ?>" style='color:<?= $colorStyle[$data[0]['c_egfr_sl_03']] ?>'></i></td>
<td style='color:<?= $colorStyle[$data[0]['c_egfr_sl_03']] ?>'><?php echo $data[0]['c_egfr_sl_03'] != '' ? $data[0]['c_egfr_sl_03x']: '' ; ?></td>

</tr>
<tr class="trcheck <?php echo $data[0]['c_egfr_30'] != '' ? $checkClassTr[$data[0]['c_egfr_30']]: 'trna' ; ?>">
<td width="71">34</td>
<td width="868">GFR &lt; 30 ml/min/1.73m<sup>2 </sup>พิจารณาส่งปรึกษาอายุรแพทย์โรคไต</td>
<td ><i id="q1check" class="<?php echo $data[0]['c_egfr_30'] != '' ? $checkClass[$data[0]['c_egfr_30']]: 'na' ; ?>" style='color:<?= $colorStyle[$data[0]['c_egfr_30']] ?>'></i></td>
<td style='color:<?= $colorStyle[$data[0]['c_egfr_30']] ?>'><?php echo $data[0]['c_egfr_30'] != '' ? $data[0]['egfr']: '' ; ?> <?php echo $data[0]['c_egfr_30'] != '' ? $data[0]['egfr_date']: '' ; ?></td>

</tr>
<tr class="trcheck <?php echo $data[0]['c_egfr_15'] != '' ? $checkClassTr[$data[0]['c_egfr_15']]: 'trna' ; ?>">
<td width="71">35</td>
<td>GFR &lt; 15 ml/min/1.73m2 ควรส่งประเมินเตรียมความพร้อมในการทำบำบัดทดแทนไต&nbsp;</td>
<td ><i id="q1check" class="<?php echo $data[0]['c_egfr_15'] != '' ? $checkClass[$data[0]['c_egfr_15']]: 'na' ; ?>" style='color:<?= $colorStyle[$data[0]['c_egfr_15']] ?>'></i></td>
<td style='color:<?= $colorStyle[$data[0]['c_egfr_15']] ?>'><?php echo $data[0]['c_egfr_15'] != '' ? $data[0]['egfr']: '' ; ?> <?php echo $data[0]['c_egfr_15'] != '' ? $data[0]['egfr_date']: '' ; ?></td>

</tr>
<tr class="trcheck <?php echo $data[0]['c_k_nsaids'] != '' ? $checkClassTr[$data[0]['c_k_nsaids']]: 'trna' ; ?>">
<td width="71">36</td>
<td>ACI หรือ Potassium >5</td>
<td ><i id="q1check" class="<?php echo $data[0]['c_k_nsaids'] != '' ? $checkClass[$data[0]['c_k_nsaids']]: 'na' ; ?>" style='color:<?= $colorStyle[$data[0]['c_k_nsaids']] ?>'></i></td>
<td style='color:<?= $colorStyle[$data[0]['c_k_nsaids']] ?>'><?php echo $data[0]['c_k_nsaids'] != '' ? $data[0]['srp']: '' ; ?> <?php echo $data[0]['c_k_nsaids'] != '' ? $data[0]['nsaids']: '' ; ?></td>

</tr>
</tbody>
</table>
    </div>
        </div>


<?php /* $this->registerJs("
    
$(document).ready(function(){

$.ajax({
  url: '".Url::to(['/ckd/checklist/check']) ."',
  context: document.body
}).done(function(html) {
  $( '#q1check' ).addClass( html );
});


});


");
 */
  ?>


<?php 

$this->registerCss("");



$this->registerJs("
    
$('.na').html( 'NA' );
function filterText(fText){

//$('<tr>').hasClass('trcheck').hide();
$('#myTable').tablesorter(); 

}

");
 

  ?>

    <script src="<?php echo Url::to(['/ckd/checklist/sortjs']); ?>" ></script>

 
 