<?php
/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\helpers\Url;
use appxq\sdii\widgets\GridView;
use appxq\sdii\widgets\ModalForm;
use appxq\sdii\helpers\SDNoty;
use appxq\sdii\helpers\SDHtml;

$checkClass = ['1' => "fa fa-check", '0' => "fa fa-close", '5' => "fa fa-check", '6' => "fa fa-close"];
$colorStyle = ['1' => "blue", '0' => "red", '5' => 'blue', '6' => 'red', '9' => 'ee99'];
//       \appxq\sdii\utils\VarDumper::dump($checkClass[$data[0]["0"]]);
// \appxq\sdii\utils\VarDumper::dump($item['result']) 
?>

<div>
    <div class="modal-header">
        <button type="button" class="btn btn-success btn-sm pull-right" data-dismiss="modal" aria-hidden="true">ปิด</button>
        <a class="btn btn-danger" href="#">รายการแจ้งเตือนรายบุคคล</a>
    </div>
    <div class="modal-body">
        <h3 align='center'>
            <i class="fa fa-circle" style="color:<?= $colorStyle['1']; ?>"></i> ผ่าน ,  <i class="fa fa-circle" style="color:<?= $colorStyle['0']; ?>"></i> ไม่เป็นไปตามรายการที่ระบุ
        </h3>
        <div class="pull-right">
            <b>เรียงลำดับตาม</b>  &nbsp;
            <input class="checkSearch" style="width: 20px; height: 20px;" type="checkbox" name="checkSearch[]" <?=$checkVal == '0' ? 'checked' : ''?> value="0" /> <i class="fa fa-circle" style="color:red"></i> 
<!--            &nbsp;
            <input class="checkSearch" style="width: 20px; height: 20px;" type="checkbox" name="checkSearch[]" <?=$checkVal == '1' ? 'checked' : ''?> value="1" /> <i class="fa fa-circle" style="color:blue"></i> 
            &nbsp;
            <input class="checkSearch" style="width: 20px; height: 20px;" type="checkbox" name="checkSearch[]" <?=$checkVal == '9' ? 'checked' : ''?> value="9" /> <i class="fa fa-circle" ></i>  <br>-->
        </div>
        <div class="clearfix"></div>
        <table class="table table-bordered table-striped">
            <tbody>
                <tr>
                    <td width="71">ที่</td>
                    <td width="868">รายการแจ้งเตือนรายบุคคล</td>
                    <td width="71">ผล</td>
                    <td width="320">ค่าจริง</td>
                </tr>
                <?php
//        $i = 0; 
//        $result = [];
//        foreach ($item as $item) {
//            
//            $i++;
//            $res = [];
//            $txt = "";
//            $fileds = explode(',', $item['result']); 
//            $txts = explode(',', $item['txt_result']);
//            $edit = explode(',', $item['edit_code']);
//            
//                foreach ($fileds as $key => $val){
//                    if (preg_match('/^[0-9]+(\.[0-9]+)?$/', $data[0][$val])){
//                     $data[0][$val] =   number_format($data[0][$val],2);
//                    }
//                    else {$data[0][$val] =$data[0][$val];       
//                    }
//                    if ($data[0][$val] == "" or $data[0][$val] == null){
//                        $txts[$key] = "";
//                    }
//                    else{
//                        $txts[$key] = $txts[$key];       
//                    }
//                    
//                    $res[] = $txts[$key]." ".$data[0][$val]." ";
//                    $res_edit[] = $edit[$val];
//                }
//                
//            switch ($item['check']) {
//            case "" :
//             echo eval($item['edit_code']);  
//             array_push($result, ['items'=>$item['item'],'check'=>$check,'text'=>JOIN('  ',$res)]);
////             appxq\sdii\utils\VarDumper::dump(JOIN('  ',$res));   
//            break;
//            }
////             \appxq\sdii\utils\VarDumper::dump($data[0]['sbp']) 
//        }
                foreach ($result as $key => $value) {
                    ?>

                    <tr>
                        <td width="71"><?php echo $key +=1 ?></td>
                        <td width="868"><?php echo $value['items'] ?></td>
                        <td> <i id="q1check" class="fa fa-circle" style='color:<?=$colorStyle[$value['check']]?>'></i></td>
                        <td style='color:'>
                            <?php
                            echo $value['text'];
                            ?></td>

                    </tr>  
                    <?php
                }
//                \appxq\sdii\utils\VarDumper::dump($result);
                ?>
            </tbody>
        </table>  


    </div>
</div>


<?php /* $this->registerJs("

  $(document).ready(function(){

  $.ajax({
  url: '".Url::to(['/ckd/checklist/check']) ."',
  context: document.body
  }).done(function(html) {
  $( '#q1check' ).addClass( html );
  });


  });


  ");
 */
?>



<?php $this->registerJs("
    
        $('.checkSearch').click(function(){
            var url = '".Url::to(['/ckd/checklist/showlist?pid='.$pid."&".'ptlink=' .$_GET['ptlink']."&".'cid=' .$_GET['cid']  ])."';
            console.log($(this).prop('checked'));
            if($(this).prop('checked') == true){
                url += '&checkVal='+$(this).val();  
            }else{
                url += '&checkVal=2';
            }
            $('#modal-checklist').find('.modal-content').load(url);
        });
        $('.na').html( 'NA' );

");
?>
 
