<?php
use backend\modules\ckdnet\classes\CkdnetQuery;
use backend\modules\ckdnet\classes\CkdnetFunc;
use miloschuman\highcharts\Highcharts;
use miloschuman\highcharts\HighchartsAsset;
use yii\helpers\Url;
use yii\helpers\Html;
 

Yii::$app->session['graph']=$graph;
$series=[];
$highcharts_config = [
                    'setupOptions' => [
                        'lang'=> [
                            'shortMonths'=> ['ม.ค.','ก.พ.','มี.ค.', 'เม.ย.', 'พ.ค.', 'มิ.ย.', 'ก.ค.', 'ส.ค.', 'ก.ย.', 'ต.ค.', 'พ.ย.', 'ธ.ค.'],
                            'shortWeekdays'=>['อา.', 'จ.', 'อ.', 'พ.', 'พฤ.', 'ศ.', 'ส.' ],
                            'weekdays'=>['อาทิตย์', 'จันทร์', 'อังคาร', 'พุธ', 'พฤหัสบดี', 'ศุกร์', 'เสาร์' ],
                        ],                        
                    ],
                    'options' => [
                        'title' => ['text' => 'กราฟ'],
                        'xAxis' => [
                            'type' => 'datetime',
                            'dateTimeLabelFormats' => [
                                'day'=>"%e %b %Y",
                                'month'=>'%e %b %Y',
                                'year'=>'%e %b %Y'
                            ],
                            'title' => [
                                'text' => 'Visit Date'
                            ],
                            'labels' => [
                                'rotation'=> -45,
                            ],
                        ],
                        'yAxis' => [
                            'title' => ['text' => 'Result'],
                            
                        ],
                        'tooltip' => [
                            'shared'=>true,
                            'useHTML'=>true,
                            'headerFormat' => '<small><b>{point.x:%A, %e %b %Y}</b></small><table><tr><td>',
//                            'pointFormat' => '<tr><td style="color: {series.color}">{series.name}: </td><td style="text-align: right"><b>{point.y:.2f} </b></td></tr>',
                            'footerFormat' => '</td></tr></table>',
                        ],
                        'plotOptions'=> [
                            'line' => [
                                'dataLabels'=> [
                                    'enabled'=> false,
                                    'color'=>'gray',
                                    'y'=>-5,
                                ],
                            ]
                        ],
                        'series' => []
                    ]
                ];

    if($graph==2){
        $ptid = CkdnetQuery::getPerson($cid);
        $pd_g = CkdnetQuery::getUrine($ptid);
        $dataLab = CkdnetFunc::getLabItemHighcharts($pd_g);
        if($dataLab){
            $series[] = ['name' => 'Urine', 'id'=>1, 'data' => new \yii\web\JsExpression($dataLab)];

        } else {
            $series[] = ['name' => 'Urine', 'id'=>1, 'data' => []];
        }
        
        $pd_g = CkdnetQuery::getBw($ptid);
        $dataLab = CkdnetFunc::getLabItemHighcharts($pd_g);
        if($dataLab){
            $series[] = ['name' => 'BW', 'id'=>2, 'data' => new \yii\web\JsExpression($dataLab)];

        } else {
            $series[] = ['name' => 'BW', 'id'=>2, 'data' => []];
        }
        
        $pd_g = CkdnetQuery::getUf($ptid);
        $dataLab = CkdnetFunc::getLabItemHighcharts($pd_g);
        if($dataLab){
            $series[] = ['name' => 'UF', 'id'=>3, 'data' => new \yii\web\JsExpression($dataLab)];
        } else {
            $series[] = ['name' => 'UF', 'id'=>3, 'data' => []];
        }
        
        ?>


 
<?php
       
    } else {
        $series = CkdnetFunc::getLab($ptlink, $hospcode, Yii::$app->session['emr_pid']);
       
        //appxq\sdii\utils\VarDumper::dump($series);
        if (!empty($series)) {
        
        $group_id = $graph;    
        $list_g = CkdnetQuery::getListGraph2($group_id,$id_graph);
        
        echo '<div class="row">';
        foreach ($list_g as $key => $value) {
            
            echo '<div class="col-md-12">';
           
        
            $findStr = ",{$value['tdc_lab_item_code']},";
            $highcharts_config['options']['title']['text'] = $value['graph_name'];
            $highcharts_config['options']['series'] = $series;
            $highcharts_config['options']['chart']['events']['load'] = new yii\web\JsExpression("
                function(event) {
                    this.series.forEach(function(d,i){
                        var find_id = ','+d.options.id+',';
                        var find_in_set = '$findStr';
                        
                        if(!find_in_set.includes(find_id)){
                           d.hide();
                        }
                    });
                }
                    ");
            
            echo Highcharts::widget($highcharts_config);
            
            echo '</div>';
        }
        echo '</div>';
        }
    }
 

 
 
  
  
?>
 