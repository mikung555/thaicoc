<?php
    use yii\helpers\Url;
    use yii\helpers\Html;
    use yii\bootstrap\ActiveForm;
    $this->title = "เพิ่มเมนูกราฟ";
 
///echo 
?>

<?php $form = ActiveForm::begin([
    'id' => $model->formName(),
    // 'enableAjaxValidation' => true,
    
     
]);?>
<div class="row" style="background: white;">

    <div class="modal-header" style=" ">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title"><?= $this->title; ?></h4>
    </div>
    

    <div class="modal-body" style="padding-left:20px;">
       
        
        <?= $form->field($model, "group")->textInput()->label("ชื่อกลุ่มกราฟ") ?>
        
        <?= $form->field($model, "numbers")->dropDownList($resort)->label('ตำแหน่ง') ?>
      
    </div> 
    <div class="modal-footer">
        <span id="spiner"> </span>
         <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'เพิ่ม') : Yii::t('app', 'แก้ไข'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
</div>
<?php ActiveForm::end(); ?>  

<?php 
 $this->registerJs("
   $('form#{$model->formName()}').on('beforeSubmit', function(e){
      var \$form =$(this);
      $('#spiner').html('<span class=\"sdloader \"><i class=\"sdloader-icon\"></i></span>');
      $.post(
        \$form.attr('action'),
        \$form.serialize()
      ).done(function(result){
        " . \common\lib\sdii\components\helpers\SDNoty::show('result.message', 'result.status') . ";
          
         $('#spiner').html('');
          $('#modal-lg').modal('hide');
          $(\$form).trigger('reset');
        
          forder();
          
         
      });
      $('#spiner').html('');
        return false;
   }); 
   
    function forder(){
        $.ajax({
            url:'".Url::to(['/ckd/graph/forder'])."',
            success:function(){
                console.log('success');
                location.reload();
            }
        });
    }
 ");
?>

























