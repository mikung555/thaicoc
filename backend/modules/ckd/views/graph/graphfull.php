<?php

use yii\helpers\Url;
use yii\helpers\Html;
use miloschuman\highcharts\HighchartsAsset;

$list_g = backend\modules\ckdnet\classes\CkdnetQuery::getListGraph($graph);
//appxq\sdii\utils\VarDumper::dump($list_g);
HighchartsAsset::register($this);
?>
<?php
$list_g = backend\modules\ckdnet\classes\CkdnetQuery::getListGraph($graph);
//appxq\sdii\utils\VarDumper::dump($list_g);
HighchartsAsset::register($this)->withScripts('highcharts');
?> 
<div id="showgraph_load">
    <?php
    $list_g = backend\modules\ckdnet\classes\CkdnetQuery::getListGraph($graph);
    //appxq\sdii\utils\VarDumper::dump($list_g);
    HighchartsAsset::register($this)->withScripts('highcharts');
    ?>






    <div class="row" id="items-add">
        <?php foreach ($list_g as $lg): ?>

            <?php
            Yii::$app->session['cols'] = $lg['cols'];
            $lgs = (int) $lg['id'] . '1';
            ?>
            <div id="<?= $lgs ?>"  class="col-md-<?= Yii::$app->session['cols'] ?> col-sm-<?= Yii::$app->session['cols'] ?> col-lg-<?= Yii::$app->session['cols'] ?>">
                <div class="pull-right" >
                    <?php
                    echo "<button   class='btn btn-success btn-sm'   onclick='return PopupGraph(" . $lg['id'] . ")'><i class=\"fa fa-eye\"></i></button> ";
                    echo "<button   class='btn btn-info btn-sm'   onclick='return EditGraph(" . $lg['id'] . ")'><i class=\"fa fa-pencil\"></i></button> ";
                    echo "<button  class='btn btn-danger btn-sm'   onclick='return DeleteGraphList(" . $lg['id'] . ")'><i class=\"fa fa-minus\"></i></button> ";
                    ?>
                </div> 
                <div data-gid="<?= $lg['id'] ?>" id="<?= $lg['id'] ?>" class="items-g">

                </div>
            </div>


        <?php endforeach; ?>
        <?php $gson = json_encode($list_g); ?> 
    </div>     






</div>

<?php
$this->registerJs("
  
$('#showgraph_load .items-g').each(function( index ) {
    
$(this).html($(this).attr('data-gid'));
    var ele = $(this);
    var id = $(this).attr('data-gid');
   // $(ele).html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
      
    ShowGraphAll(id);
    
  }); 

 function PopupGraph(id){
    $(\"#myModal\").modal(); 
    $('#ShowGraphOnes').html(id);
  
                                  
 }//PopupGraph

function ShowGraphAll(id){
                                   
   //$('#'+id).html('<div class=\"sdloader\"><i class=\"sdloader-icon\"></i></div>');
    $.ajax({
        url:'" . Url::to(['/ckd/graph/popup-graph']) . "',
        method:'GET',
        data:{
        hospcode    :   '" . $hospcode . "',
        ptlink      :   '" . $ptlink . "',
        pid         :   '" . Yii::$app->session['emr_pid'] . "',
        graph       :   '" . $graph . "',
        cid         :   '" . $cid . "',
        id          :   id
    },
    dataType:'HTML',
    success:function(result){                         
        $('#'+id).append(result);
    },error: function (xhr, ajaxOptions, thrownError) {
        console.log(xhr);
      }
 });  
 
}//ShowGraphAll
function EditGraph(id){

        var id = id;                            
        var url = '" . Url::to(['/ckd/graph/add']) . "';
        var urls = url+'?id='+id+'&graph=" . $graph . "&status=2';
        console.log(urls);
        modalUser(urls); 

};//btnEditGraph
                                
                                function DeleteGraphList(id){
                                  
                                    var gname = '';
                                    var id = id;
                                    var url = '" . Url::to(['/ckd/graph/delete-graph']) . "'; 

                                     $.each(" . $gson . ", function(k,v){
                                        if(id == v.id){
                                            gname = v.graph_name; 
                                        }   
                                    });
                                   
                                    
                                 krajeeDialog.confirm('<b>คุณต้องการลบกราฟ '+gname+' หรือไม่</b>', function (result) {
                                         if (result) {        
                                                 $.ajax({
                                                     url:url,
                                                     type:'POST',
                                                     data:{id:id}, 
                                                     dataType:'JSON',
                                                     success:function(result){
                                                         
                                                        " . \common\lib\sdii\components\helpers\SDNoty::show('result.message', 'result.status') . ";
                                                          
                                                      
                                                       for(var i=1; i<=100; i++){
                                                            $('#'+id+''+1).remove();
                                                            
                                                       }
                                                        
                                                     }
                                                 });
                                         } else {
                                             $('#modal-lg').modal('hide');
                                         }

                                     });
                                     return false;
                                 }//DeleteGraphList


")?>