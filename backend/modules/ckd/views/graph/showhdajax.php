<?php

use miloschuman\highcharts\Highcharts;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use backend\modules\ckdnet\classes\CkdnetQuery;
use backend\modules\ckdnet\classes\CkdnetFunc;

$series=[];
$highcharts_config = [
                    'setupOptions' => [
                        'lang'=> [
                            'shortMonths'=> ['ม.ค.','ก.พ.','มี.ค.', 'เม.ย.', 'พ.ค.', 'มิ.ย.', 'ก.ค.', 'ส.ค.', 'ก.ย.', 'ต.ค.', 'พ.ย.', 'ธ.ค.'],
                            'shortWeekdays'=>['อา.', 'จ.', 'อ.', 'พ.', 'พฤ.', 'ศ.', 'ส.' ],
                            'weekdays'=>['อาทิตย์', 'จันทร์', 'อังคาร', 'พุธ', 'พฤหัสบดี', 'ศุกร์', 'เสาร์' ],
                        ],                        
                    ],
                    'options' => [
                        'title' => ['text' => 'กราฟ'],
                        'xAxis' => [
                            'type' => 'datetime',
                            'dateTimeLabelFormats' => [
                                'day'=>"%e %b %Y",
                                'month'=>'%e %b %Y',
                                'year'=>'%e %b %Y'
                            ],
                            'title' => [
                                'text' => 'Visit Date'
                            ],
                            'labels' => [
                                'rotation'=> -45,
                            ],
                        ],
                        'yAxis' => [
                            'title' => ['text' => 'Result'],
                        ],
                        'tooltip' => [
                            'shared'=>true,
                            'useHTML'=>true,
                            'headerFormat' => '<small><b>{point.x:%A, %e %b %Y}</b></small><table><tr><td>',
//                            'pointFormat' => '<tr><td style="color: {series.color}">{series.name}: </td><td style="text-align: right"><b>{point.y:.2f} </b></td></tr>',
                            'footerFormat' => '</td></tr></table>',
                        ],
                        'plotOptions'=> [
                            'line' => [
                                'dataLabels'=> [
                                    'enabled'=> false,
                                    'color'=>'gray',
                                    'y'=>-5,
                                ],
                            ]
                        ],
                        'series' => []
                    ]
                ];
?>

<div class="panel panel-primary">
    <div class="panel-heading text-center">
        <h3 class="panel-title">HD Data</h3>
    </div>
    <div class="" id="bodyHDData">
        <div id="hdcontent">
            <div class="col-md-12">
<?php
        $ptid = $ptid == "" ? CkdnetQuery::getPerson($cid) : $ptid; //1485763331065108700;
        $idwg_config = $highcharts_config;
                
        $dw = CkdnetQuery::getDw($ptid);
        $datadw = CkdnetFunc::getLabItemHighcharts($dw);

        if($datadw){
            $series1[] = ['name' => 'Dry Weight', 'id'=>1, 'data' => new \yii\web\JsExpression($datadw)];
        } else {
            $series1[] = ['name' => 'Dry Weight', 'id'=>1, 'data' => []];
        }        
        $idwg = CkdnetQuery::getIdwg($ptid);
        $dataLab = CkdnetFunc::getLabItemHighcharts($idwg);

        if($dataLab){
            $series2[] = ['name' => 'IDWG', 'id'=>2, 'data' => new \yii\web\JsExpression($dataLab)];
        } else {
            $series2[] = ['name' => 'IDWG', 'id'=>2, 'data' => []];
        }
        
        $seriesx1 = array_merge($series1,$series2);
        
        $sbp = CkdnetQuery::getHdSBP($ptid);
        $datasbp = CkdnetFunc::getLabItemHighcharts($sbp);

        if($datasbp){
            $series3[] = ['name' => 'HOS SBP', 'id'=>3, 'data' => new \yii\web\JsExpression($datasbp)];
        } else {
            $series3[] = ['name' => 'HOS SBP', 'id'=>3, 'data' => []];
        }   
        $dbp = CkdnetQuery::getHdDBP($ptid);
        $datadbp = CkdnetFunc::getLabItemHighcharts($dbp);

        if($datadbp){
            $series4[] = ['name' => 'HOS DBP', 'id'=>4, 'data' => new \yii\web\JsExpression($datadbp)];
        } else {
            $series4[] = ['name' => 'HOS DBP', 'id'=>4, 'data' => []];
        }        
        
        $tdcsbp = CkdnetQuery::getHOSSBP($ptid);
        $datatdcsbp = CkdnetFunc::getLabItemHighcharts($tdcsbp);
        if($datatdcsbp){
            $series5[] = ['name' => 'Home SBP', 'id'=>5, 'data' => new \yii\web\JsExpression($datatdcsbp)];
        } else {
            $series5[] = ['name' => 'Home SBP', 'id'=>5, 'data' => []];
        } 
        $tdcdbp = CkdnetQuery::getHOSDBP($ptid);
       
        $datatdcdbp = CkdnetFunc::getLabItemHighcharts($tdcdbp);
        if($datatdcdbp){
            $series6[] = ['name' => 'Home SBP', 'id'=>6, 'data' => new \yii\web\JsExpression($datatdcdbp)];
        } else {
            $series6[] = ['name' => 'Home SBP', 'id'=>6, 'data' => []];
        }       
        
        $seriesbp = array_merge($series5,$series6);
        
        $seriesx2 = array_merge($series3,$series4);
        $seriesx = array_merge($seriesx1,$seriesx2);
        $series = array_merge($seriesx,$seriesbp);
        
        $idwg_config['options']['title']['text'] = "InterDialytic Weight Gain (IDWG) ";
        $idwg_config['options']['series'] = $series;   
        $findStr = ",1,2,";
        $idwg_config['options']['chart']['events']['load'] = new yii\web\JsExpression("
            function(event) {
                this.series.forEach(function(d,i){
                    var find_id = ','+d.options.id+',';
                    var find_in_set = '$findStr';

                    if(!find_in_set.includes(find_id)){
                       d.hide();
                    }
                });
            }
                ");        
        echo Highcharts::widget($idwg_config);
?>
            </div>
            <div class="col-md-12">
<?php
        
        $allseries = array_merge($series,$seriesbp);
        
        $bp_config = $highcharts_config;
                
        $bp_config['options']['title']['text'] = "Facility-Based Blood Pressure";
        $bp_config['options']['series'] = $series;     
        $findStr = ",1,3,4,";
        $bp_config['options']['chart']['events']['load'] = new yii\web\JsExpression("
            function(event) {
                this.series.forEach(function(d,i){
                    var find_id = ','+d.options.id+',';
                    var find_in_set = '$findStr';

                    if(!find_in_set.includes(find_id)){
                       d.hide();
                    }
                });
            }
                ");        
        echo Highcharts::widget($bp_config);
        
        $hbp_config = $highcharts_config;
                
        $hbp_config['options']['title']['text'] = "Home Blood Pressure";
        $hbp_config['options']['series'] = $series;     
        $findStr = ",1,5,6,";
        $hbp_config['options']['chart']['events']['load'] = new yii\web\JsExpression("
            function(event) {
                this.series.forEach(function(d,i){
                    var find_id = ','+d.options.id+',';
                    var find_in_set = '$findStr';

                    if(!find_in_set.includes(find_id)){
                       d.hide();
                    }
                });
            }
                ");        
        echo Highcharts::widget($hbp_config);        
?>
            </div>            
        </div>
    </div>
</div>
