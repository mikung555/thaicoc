<?php

use yii\helpers\Url;
use yii\helpers\Html;

$this->title = "เลือกตำแหน่งที่ต้องการเรียงลำดับ";
//appxq\sdii\utils\VarDumper::dump($forder);
?>

<div class="row" style="background: white;">

    <div class="modal-header" style=" ">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title"><i class="fa fa-sort"></i> <?= $this->title; ?></h4>
    </div>


    <div class="modal-body" style="padding-left:20px;">
        <?php
        $ors="";
        if(empty($group_id)){$ors=" OR group_id = 1 OR group_id =99";}
        if(@$group_id=="99"){$ors=" OR group_id != 99";}
        $defaults = [
                ['id' => '', 'graph_name' => '---- เลือกตำแหน่งที่ต้องการเรียงลำดับ ----'],
                ['id' => '0', 'graph_name' => 'หน้าสุด']
            ];
        $graph_ = \backend\modules\ckdnet\classes\CkdnetFunc::queryAll("SELECT * FROM lab_graph_list_site WHERE hospcode=:hospcode AND  group_id =:group_id $ors  AND id != :id order by forder asc",
                [':hospcode'=>Yii::$app->session['dynamic_connection']['sitecode'],':group_id'=>$group_id,':id'=>$id]);
        $graph_ = array_merge($defaults, $graph_);
        $l = \yii\helpers\ArrayHelper::map($graph_, 'id', 'graph_name');
        ?>

<!--        <label>เลือกตำแหน่งที่ต้องการเรียงลำดับ</label>-->
        <?php
        echo Html::dropDownList('select_graph', [], $l, [
            'class' => 'form-control',
            'id' => 'sorts',
            'readonly' => false,
            'onchange' => 'Sort($(this).val())',
        ]);
        ?> 
        <br>

    </div> 

</div>
<?php
$this->registerJs("
     Sort = function(forder){
     //console.log(forder);return false;
      $.ajax({
      url:'" . Url::to(['/ckd/graph/shorter']) . "',
      type:'POST',
      data:{
          id:'".$id."',
          forder:forder    
       },
      success:function(result){
         //console.log(result);return false;
          " . \common\lib\sdii\components\helpers\SDNoty::show('result.message', 'result.status') . ";
          $('#modal-lg').modal('hide');
         // $.pjax.reload({container:'#graph-pjax'});
          //location.reload();
         _load(4);
         _show('');
         
      }
    });
      return false;
    }       

");
?>
 