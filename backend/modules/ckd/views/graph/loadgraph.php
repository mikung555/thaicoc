<div class="row" id="rows">
    <?php foreach($list_g as $l): ?>
    <div id="g<?= $l['id']?>" class="col-md-<?= $l['cols']?> col-sm-<?= $l['cols']?> col-lg-<?= $l['cols']?>">
        <div class="pull-right">
          <button title='ดูกราฟ' onClick="_showone(<?= $l['id']?>)" class='btn btn-success btn-sm'><i class='fa fa-eye'></i></button>
           <button title='เรียงลำดับ' onClick="_sorts(<?= $l['id']?>)" class='btn btn-default btn-sm'><i class='fa fa-sort'></i></button>
           <button title='แก้ไข' onClick="_update(<?= $l['id']?>)" class='btn btn-info btn-sm'><i class='fa fa-pencil'></i></button>
          <button title='ลบ' onClick="_delete(<?= $l['id']?>)" class='btn btn-danger btn-sm'><i class='fa fa-minus'></i></button>
          <button onClick="_removeEle(<?= $l['id']?>)" class='btn btn-warning btn-sm'><i class='fa fa-trash'></i></button>
        </div><!-- button -->
        <div class='items-g' data-gid='<?= $l['id']?>' id='<?= $l['id']?>'></div>
    </div>
    <?php endforeach;?>
</div>
 