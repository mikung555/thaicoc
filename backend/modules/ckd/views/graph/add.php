<?php

use yii\helpers\Url;
use yii\helpers\Html;
use kartik\select2\Select2;
$this->title = "เพิ่มกราฟ";

 //echo 'hello =>'.$graph;

///echo 
?>

<div class="row" style="background: white;">

  <div class="modal-header" style=" ">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title"><?= $this->title; ?></h4>
  </div>


  <div class="modal-body" style="padding-left:20px;">
    <?php
    $defaults = [['id' => '0000', 'graph_name' => 'สร้างเอง']];

    $graph_ = \backend\modules\ckdnet\classes\CkdnetFunc::queryAll("SELECT * FROM lab_graph_list_site WHERE group_id=99 GROUP BY graph_name ");

//        if($graph== "3"){
//            $graphs_ = \backend\modules\ckdnet\classes\CkdnetFunc::queryAll("SELECT id, graph_name as graph_name FROM lab_graph_list WHERE group_id='3'");
//            $graph_ = \backend\modules\ckdnet\classes\CkdnetFunc::queryAll("SELECT ls.* FROM 
//                lab_graph_list as ls 
//                WHERE ls.group_id='99'");
//            $graph_ = array_merge($graph_, $graphs_);
//        }
        //$graph_ = \backend\modules\ckdnet\classes\CkdnetFunc::queryAll("SELECT * FROM lab_graph_list_site WHERE group_id=99");
    $graph_ = array_merge($defaults, $graph_);




    $l = \yii\helpers\ArrayHelper::map($graph_, 'id', 'graph_name');
    ?>

    <label>กรุณาเลือกกราฟจากกลุ่ม (All) หรือ เลือกสร้างเอง</label>
    <?php
    if($cstatus == 2){
                //update
      echo Html::dropDownList('select_graph', [], $l, [
        'class' => 'form-control',
        'readonly'=>true,
        'onchange' => 'setHideInput($(this).val())',
      ]);
      // echo Select2::widget([
      //   'name' => 'select_graph',
      //   'data' => $l,
      //   'options' => [
      //     'placeholder' => 'กรุณาเลือกกราฟจากกลุ่ม (All) หรือ เลือกสร้างเอง ...',
      //     'multiple' => false
      //   ],
      // ]);
    }else{
      echo Html::dropDownList('select_graph', [], $l, [
        'class' => 'form-control',
        'readonly'=>false,
        'onchange' => 'setHideInput($(this).val())',
    ]);
    //  echo Select2::widget([
    //   'name' => 'select_graph',
    //   'data' => $l,
    //   'options' => [
    //     'placeholder' => 'กรุณาเลือกกราฟจากกลุ่ม (All) หรือ เลือกสร้างเอง ...',
    //     'multiple' => false
    //   ],
    // ]);
   }

   ?> 
   <br>

   <div id="ShowAddGraphs"></div> 
   <div id="ShowUpdateGraphs"> </div>

 </div> 

</div>

<?php 

if($status == 2){
 $ids = $id;
}else{
 $ids='0000';
}

?>
<?php $this->registerJs("

  var input1 = 'input[name=\"Labgraplists[select_graph]\"]';
  // console.log('".$ids."');
  setHideInput('".$ids."');


  function setHideInput(val)
  { 
   //alert(val);return false;
    $('#ShowUpdateGraphs').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>'); 
    $.ajax({
      url:'" . Url::to(['/ckd/graph/get-form']) . "',
      type:'get',
      data:{
        select_graph:val,//graph_id
        graph:'" . $graph . "',//group_id
        id:val,
        'cstatus':'".$cstatus."'
      },
      
      success:function(data){
        $('#ShowUpdateGraphs').html(''); 
        $('#ShowUpdateGraphs').html(data);
        
      }
    });
  }





  "); ?>
<style>
.select2-search__field{
  width: 194px;
}
</style>