<?php

use yii\helpers\Html;
use yii\bootstrap\Modal;
use miloschuman\highcharts\Highcharts;
use yii\helpers\ArrayHelper;
use backend\modules\ckdnet\classes\CkdnetQuery;
use common\lib\sdii\components\utils\SDdate;
use yii\helpers\Url;
use backend\modules\ckd\classes\NutDialog;
use yii\web\JsExpression;
use appxq\sdii\widgets\ModalForm;
use appxq\sdii\helpers\SDNoty;
use appxq\sdii\helpers\SDHtml;
use yii\widgets\Pjax;
use backend\modules\ckdnet\classes\CkdnetFunc;
use miloschuman\highcharts\HighchartsAsset;

$graph = $group_id;
$ptlink = $_GET['ptlink'];
//\appxq\sdii\utils\VarDumper::dump($returnArr);
?>
<div class="pull-right" style="margin: 7px 9px 0px 0px;">
    <?php if (Yii::$app->user->can('administrator') && Yii::$app->user->can('adminsite')): ?>  
        <button title="รีเฟรช กราฟ" class="btn btn-default" onclick="return _show('')"><i class="fa fa-refresh"></i></button>      
        <?php echo Html::button('<i class="fa fa-pencil"></i>', ['data-url' => Url::to(['/ckd/graph/graph-update-menu', 'hpcode' => Yii::$app->session['dynamic_connection']['sitecode'], 'id' => $graph]), 'class' => 'btn btn-info', 'id' => 'btnUpdateGraphMenu']); ?>
        <?php echo Html::button('<i class="fa fa-minus"></i>', ['data-url' => Url::to(['/ckd/graph/delete']), 'class' => 'btn btn-danger', 'id' => 'btnDeleteGrapAll']); ?>
        <button title="กลับไปยังจุดเริ่มต้น" class="btn btn-warning" id="btnClearGraph" data-url="<?= Url::to(['/ckd/graph/clear-graph']) ?>" data-hospcode='<?= Yii::$app->session['dynamic_connection']['sitecode'] ?>'><i class="fa fa-retweet"></i></button>
        <?php $this->registerJS("
                    $('#btnClearGraph').click(function(){
                    var url = $(this).attr('data-url');
                    var hospcode = $(this).attr('data-hospcode');
                     
                    $.get(url,{hospcode:hospcode}, function(data){
                        console.log(data);
                       location.reload();
                    });
                    
                });
                

        ") ?>



    <?php endif; ?>
</div>
<div style="    border-left: 1px solid #dddddd;padding-top: 10px; border-right: 1px solid #dddddd;">

    <?php
    $cgraph = \backend\modules\ckd\models\Graph::find()->where(['id' => $graph, 'hospcode' => Yii::$app->session['dynamic_connection']['sitecode']])->one();
    ?>
    <?php if (!empty($cgraph)): ?>  
        <span style="margin:10px;">

            <?php
            if (Yii::$app->user->can('doctorckd') || Yii::$app->user->can('administrator')) {
                echo Html::button('<i class="fa fa-plus"></i> เพิ่มกราฟ', ['class' => 'btn btn-success', 'id' => 'btnAddGraphWhere']);
//                                       ['data-url'=>Url::to(['/ckd/graph/add','graph'=>$graph]),
//                                       'class' => 'btn btn-success', 'id'=>'btnAddGraphWhere']); 
            }
            ?> 

        </span>    
    <?php endif; ?>

</div>
<div class="clearfix" style="border-left: 1px solid #dddddd;"></div>
<div style="border-bottom: 1px solid #ddd; border-right: 1px solid #ddd; border-left: 1px solid #ddd; margin-bottom: 15px; padding: 5px; " >

    <div id="shograph_ones"></div>
    <div id="showgraph_load"> </div>
    <div class="clearfix"></div>
    <div class="row" id="btnReadmore" style="display:none;margin-top:10px;">
        <div class="col-md-3"></div>
        <div class="col-md-6" style="display: none;"><button  class='btn btn-info btn-lg btn-block' id="readMore">โหลดกราฟเพิ่ม...</button> </div>
        <div class="col-md-3"></div>
    </div> 



    <?php
    $this->registerJs("
        /********************* แสดงปุ่มแก้ไข กราฟ ***********************/ 
        $('#btnUpdateGraphMenu').on('click', function() { 
            modalUser($(this).attr('data-url')); 
             return false;
        });   
        
        /*********************  ปุ่มลบ กราฟ ***********************/ 
        $('#btnDeleteGrapAll').click(function(){
            var url = $(this).attr('data-url');
           krajeeDialog.confirm(\"<b>คุณต้องการลบเมนูกราฟหรือไม่</b>\", function (result) {
                if (result) {
                   
                        $.ajax({
                            url:url,
                            type:'POST',
                            data:{
                                hospcode:'" . Yii::$app->session['dynamic_connection']['sitecode'] . "',
                                'graph':'" . $graph . "' 
                            },
                            dataType:'json',
                            success:function(result){
                                //console.log(result);
                                ///$.pjax.reload({container:'#graph-pjax'});
                                 " . \common\lib\sdii\components\helpers\SDNoty::show('result.message', 'result.status') . ";
                                location.reload();
                            }
                        });
                } else {
                    $('#modal-lg').modal('hide');
                }
                
            });
        });//Delete graph all menu 
        
/*********************  โหลดหน้าเพิ่มกราฟ ***********************/
        $('#btnAddGraphWhere').click(function(){
            var id = id; 
            var url = '" . Url::to(['/ckd/graph/add']) . "';
            var urls = url+'?graph=" . $graph . "&status=1';
           // console.log(urls);
            modalUser(urls); 
           //  modal-graph
        });//btnAddGraphWhere
                    
                            
                            //end showtab
                        
                                var docker = '" . Yii::$app->user->can('doctorckd') . "';
                                var adminsite = '" . Yii::$app->user->can('administrator') . "';
                                
                                    
                                var numbers=1 , b=10 , e=10; 
                                
                                $('#readMore').click(function(){
                                   
                                    _loadMore();
                                   
                                });
                                 _load(numbers);
                                 
                          
                          setTimeout(function(){
                            _checkMore();
                          },500);
                          function _checkMore(){
                           
                            $.ajax({
                                        url:'" . Url::to(['/ckd/graph/countgraph']) . "',
                                        method:'GET',
                                        data:{
                                            hospcode    :   '" . $hospcode . "',
                                            ptlink      :   '" . $ptlink . "',
                                            pid         :   '" . Yii::$app->session['emr_pid'] . "',
                                            graph       :   '" . $graph . "',
                                            cid         :   '" . $cid . "',
                                            b           :   b,
                                            e           :   e,
                                        },
                                        dataType:'json',
                                        success:function(result){
                                            //console.log(result.length);
                                            //console.log('Begin => '+b);
                                           // console.log('End => '+e);
                                            if(result.length < 1){
                                                $('#btnReadmore').fadeOut();
                                            }else{
                                                $('#btnReadmore').fadeIn();
                                            }

                                        }
                                    });//Ajax
                          }
                          function _loadMore(){
                                    $.ajax({
                                        url:'" . Url::to(['/ckd/graph/loadgraph']) . "',
                                        method:'GET',
                                        data:{
                                            hospcode    :   '" . $hospcode . "',
                                            ptlink      :   '" . $ptlink . "',
                                            pid         :   '" . Yii::$app->session['emr_pid'] . "',
                                            graph       :   '" . $graph . "',
                                            cid         :   '" . $cid . "',
                                            b           :   b,
                                            e           :   e,
                                        },
                                        dataType:'json',
                                        success:function(result){
                                            //console.log(result);
                                            if(result.length <= 1){
                                                $('#btnReadmore').fadeOut();
                                            }else{
                                                $('#btnReadmore').fadeIn();
                                            }
                                            $.each(result, function(k,v){
                                                if(v.id != ''){
                                                    _readmores(v.id);
                                                }
                                                
                                            });
                                            _checkMore();
                                               
                                        }
                                         
                                    });//Ajax
                                    
                                  b+=10;
                                   
                                 
                             }//_loadMore 
                              function _readmores(id){
                                    
                                    var cols = 6;
                                    $.ajax({
                                        url:'" . Url::to(['/ckd/graph/max-id']) . "',
                                        type:'POST',
                                        data:{id:id},
                                        dataType:'JSON',    
                                        success:function(data){
                                            
                                             var id = 0;
                                             id = data[0];
                                             cols = data[5];
                                              var _div = '';
                                              if(cols == '6'){
                                                _width = 435;
                                              }else{
                                                _width = 899;
                                              }
                                            
                                                _div += '<div id=\'g'+id+'\' class=\'col-md-'+cols+' col-sm-'+cols+' col-lg-'+cols+'\'>';

                                                   //button
                                                if(adminsite || docker){  
                                                    _div +='<div class=\'pull-right\' >';
                                                     _div +='<button onClick=\'_showone('+id+')\' class=\'btn btn-success btn-sm\'><i class=\'fa fa-eye\'></i></button> ';
                                                     _div += '<button title=\'เรียงลำดับ\' onClick=\'_sorts('+id+')\' class=\'btn btn-default btn-sm\'><i class=\'fa fa-sort\'></i></button> ';
                                                    _div +='<button onClick=\'_update('+id+')\' class=\'btn btn-info btn-sm\'><i class=\'fa fa-pencil\'></i></button> ';
                                                     _div +='<button onClick=\'_delete('+id+')\' class=\'btn btn-danger btn-sm\'><i class=\'fa fa-minus\'></i></button> ';
                                                    _div +='</div>';
                                                }

                                                    //show graph 
                                                _div += '<div class=\'items-g\' data-gid='+id+' id='+id+'></div>';
                                                _div += '</div>';
                                                //.prepend()
                                                 $('#showgraph_load').append(_div);
                                                 _show(id);
                                                
                                        }
                                    });
                                    
                                 
                                    return false;

                                }//_readmores 
                                 
     /************************************ แสดงกราฟ ****************************************************************/                           
                                function _load(number=''){
                                      
                                    $.ajax({
                                        url:'" . Url::to(['/ckd/graph/load_']) . "',
                                        method:'GET',
                                        data:{
                                            hospcode    :   '" . $hospcode . "',
                                            ptlink      :   '" . $ptlink . "',
                                            pid         :   '" . Yii::$app->session['emr_pid'] . "',
                                            graph       :   '" . $graph . "',
                                            cid         :   '" . $cid . "',
                                            number      :   10, 
                                        },
                                        dataType:'JSON',
                                        success:function(result){
                                            //console.log(result);
                                            var _div = '';
                                            
                                            _div += '<dir class=\'row\' id=\'rows\'>';
                                           $.each(result, function(k,v){
                                                _div += '<div id=\'g'+v.id+'\' class=\'col-md-'+v.cols+' col-sm-'+v.cols+' col-lg-'+v.cols+'\'>';
                                                    
                                                    //button
                                                    if(adminsite || docker){
                                                        _div +='<div class=\'pull-right\' >';
                                                          _div +='<button title=\'ดูกราฟ\' onClick=\'_showone('+v.id+')\' class=\'btn btn-success btn-sm\'><i class=\'fa fa-eye\'></i></button> ';
                                                          _div += '<button title=\'เรียงลำดับ\' onClick=\'_sorts('+v.id+')\' class=\'btn btn-default btn-sm\'><i class=\'fa fa-sort\'></i></button> ';
                                                          _div += '<button title=\'แก้ไข\' onClick=\'_update('+v.id+')\' class=\'btn btn-info btn-sm\'><i class=\'fa fa-pencil\'></i></button> ';
                                                          _div +='<button title=\'ลบ\' onClick=\'_delete('+v.id+')\' class=\'btn btn-danger btn-sm\'><i class=\'fa fa-minus\'></i></button> ';
                                                          //_removeEle
                                                          // _div +='<button onClick=\'_removeEle('+v.id+')\' class=\'btn btn-warning btn-sm\'><i class=\'fa fa-trash\'></i></button> ';
                                                        _div +='</div>';
                                                     }
                                                       
                                                    //show graph 
                                                    _div += '<div class=\'items-g\' data-gid='+v.id+' id='+v.id+'></div>';
                                                    

                                                _div += '</div>';
                                              
                                                
                                           });//each
                                           _div == '</div>';
                                           
                                           $('#showgraph_load').html(_div);
                                            _show('');
                                        }
                                    });//Ajax
                                 
                                }//_load 
                                function _showone(id){
                                 
                                 _width = $('#'+id).width();
                                  $('#modal-lg').modal('show');
                                  var modals='';
                                        
                                        modals += '<div class=\'modal-header\'>';
                                        modals += '<p style=\'display:inline;font-size:14pt;\'><i class=\'fa fa-arrows-alt\'></i> แสดงกราฟแบบเต็มความกว้าง</p>';
                                        modals += '<button  type=\'button\' class=\'close\' data-dismiss=\'modal\' aria-hidden=\'true\'>&times;<br></button>';
                                        modals +='</div>';
                                        modals += '<div class=\'modal-body\' style=\'padding:1px;\'>';
                                        modals += '<div id=\'modalgraph\'>';
                                        modals += '<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div></div>';
                                        modals += '</div>';
                                        
                                         
                                        modals += '</div>';
                                 
                                  $('#modal-lg .modal-content').html(modals);
                                        
                          
                                                var _div = '';
                                                $.ajax({
                                                           url:'" . Url::to(['/ckd/graph/popup-graph']) . "',
                                                           method:'GET',
                                                           data:{
                                                           hospcode    :   '" . $hospcode . "',
                                                           ptlink      :   '" . $ptlink . "',
                                                           pid         :   '" . Yii::$app->session['emr_pid'] . "',
                                                           graph       :   '" . $graph . "',
                                                           cid         :   '" . $cid . "',
                                                           id          :   id,
                                                           renderid          :   'modalgraph',
                                                           widths      :   899,
                                                    },
                                                      dataType:'HTML',
                                                      success:function(result){
                                                       
                                                        $('#modal-lg .modal-content #modalgraph').html(result);
                                                        
                                                    } 
                                                 });


                                }//_showone
                                function _sorts(id){
                                    //console.log(" . $graph . "); 
                                    var id = id; 
                                    var url = '" . Url::to(['/ckd/graph/shorter']) . "';
                                    var urls = url+'?id='+id+'&group_id=" . $graph . "';
                                
                                    modalUser(urls); 
                                }
                                function _show(id){
                                     //console.log($('#'+id).width());  
                                   
                                    if(id != ''){
                                            //console.log(_width);
                                                $('#'+id).html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
                                                var _div = '';
                                                $.ajax({
                                                           url:'" . Url::to(['/ckd/graph/popup-graph']) . "',
                                                           method:'GET',
                                                           data:{
                                                           hospcode    :   '" . $hospcode . "',
                                                           ptlink      :   '" . $ptlink . "',
                                                           pid         :   '" . Yii::$app->session['emr_pid'] . "',
                                                           graph       :   '" . $graph . "',
                                                           cid         :   '" . $cid . "',
                                                           id          :   id,
                                                           widths      :   _width
                                                           
                                                    },
                                                      dataType:'HTML',
                                                      success:function(result){
//                                                      //console.log(result);
                                                                $('#g'+id).removeClass('col-md-6');
                                                                $('#g'+id).removeClass('col-sm-6'); 
                                                                $('#g'+id).removeClass('col-lg-6');
                                                                $('#g'+id).removeClass('col-md-12');
                                                                $('#g'+id).removeClass('col-sm-12'); 
                                                                $('#g'+id).removeClass('col-lg-12');
                                                        $.ajax({
                                                            url:'" . Url::to(['/ckd/graph/max-id']) . "',
                                                            type:'POST',
                                                            data:{id:id},
                                                            dataType:'JSON',    
                                                            success:function(data){
                                                                $('#g'+id).addClass('col-md-'+data[5]+' col-sm-'+data[5]+' col-lg-'+data[5]+'');
                                                            }
                                                        });
                                                        
                                                        $('#'+id).html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>'); 
                                                        $('#'+id).html(result);
                                                         
                                                        
                                                    } 
                                                 });
                                    


                                    }else{
                                        var delar = 200;
                                        $('#showgraph_load .items-g').each(function( index ) {
                                              
                                            $(this).html($(this).attr('data-gid'));
                                                var ele = $(this);
                                                var id = $(this).attr('data-gid');
                                                //$(ele).html('');
                                                $(ele).html('<div class=\"sdloader\"><i class=\"sdloader-icon\"></i></div>');

                                                    $.ajax({
                                                                url:'" . Url::to(['/ckd/graph/popup-graph']) . "',
                                                                method:'GET',
                                                                data:{
                                                                hospcode    :   '" . $hospcode . "',
                                                                ptlink      :   '" . $ptlink . "',
                                                                pid         :   '" . Yii::$app->session['emr_pid'] . "',
                                                                graph       :   '" . $graph . "',
                                                                cid         :   '" . $cid . "',
                                                                id          :   id,
                                                                widths      :   $('#'+id).width()
                                                            },
                                                            dataType:'HTML',
                                                            success:function(result){
                                                            $('.sdloader').html('');
                                                            $('#'+id).append('<div id=\'load'+id+'\'>กำลังเตรียมข้อมูล...</div>');
                                                              setTimeout(function(){
                                                                $('.highcharts-background').addClass('resize');
                                                                 $('#load'+id).html('');
                                                                 $('#'+id).append(result);
                                                                
                                                              },delar); 
                                                                
                                                               delar += 2000;
                                                            },error: function (xhr, ajaxOptions, thrownError) {
                                                                
                                                               
                                                        }//sucess
                                                 });  

                                         }); 
                                         
                                    }//else
                                    return false;
                                     
                                }//_show
                               
                                function _create(id){
                              
                                  
                                   var cols = 6;
                                    $.ajax({
                                        url:'" . Url::to(['/ckd/graph/max-id']) . "',
                                        type:'POST',
                                        data:{id:id},
                                        dataType:'JSON',    
                                        success:function(data){
                                             //console.log(data);
                                             var id = 0;
                                            
                                             id = data[0];
                                             cols = data[5];
                                              var _div = '';
                                            
                                                _div += '<div id=\'g'+id+'\' class=\'col-md-'+cols+' col-sm-'+cols+' col-lg-'+cols+'\'>';

                                                   //button
                                                if(adminsite || docker){   
                                                    _div +='<div class=\'pull-right\' >';
                                                     _div +='<button onClick=\'_showone('+id+')\' class=\'btn btn-success btn-sm\'><i class=\'fa fa-eye\'></i></button> ';
                                                     _div += '<button title=\'เรียงลำดับ\' onClick=\'_sorts('+id+')\' class=\'btn btn-default btn-sm\'><i class=\'fa fa-sort\'></i></button> ';
                                                    _div +='<button onClick=\'_update('+id+')\' class=\'btn btn-info btn-sm\'><i class=\'fa fa-pencil\'></i></button> ';
                                                     _div +='<button onClick=\'_delete('+id+')\' class=\'btn btn-danger btn-sm\'><i class=\'fa fa-minus\'></i></button> ';
                                                    _div +='</div>';
                                                }

                                                    //show graph 
                                                _div += '<div class=\'items-g\' data-gid='+id+' id='+id+'></div>';
                                                _div += '</div>';
                                                //.prepend()
                                                 $('#showgraph_load').prepend(_div);
                                                 _show(id);
                                                
                                        }
                                    });
                                    
                                 
                                    return false;
                                }//_create
                                
                               function _updateG(id){
                              
                                  
                                   var cols = 6;
                                    $.ajax({
                                        url:'" . Url::to(['/ckd/graph/max-id']) . "',
                                        type:'POST',
                                        data:{id:id},
                                        dataType:'JSON',    
                                        success:function(data){
                                            // console.log(data);
                                             var id = 0;
                                            
                                             id = data[0];
                                             cols = data[5];
                                              var _div = '';
                                            
                                                _div += '<div id=\'g'+id+'\' class=\'col-md-'+cols+' col-sm-'+cols+' col-lg-'+cols+'\'>';

                                                   //button
                                                if(adminsite || docker){   
                                                    _div +='<div class=\'pull-right\' >';
                                                     _div +='<button onClick=\'_showone('+id+')\' class=\'btn btn-success btn-sm\'><i class=\'fa fa-eye\'></i></button> ';
                                                    _div +='<button onClick=\'_update('+id+')\' class=\'btn btn-info btn-sm\'><i class=\'fa fa-pencil\'></i></button> ';
                                                     _div +='<button onClick=\'_delete('+id+')\' class=\'btn btn-danger btn-sm\'><i class=\'fa fa-minus\'></i></button> ';
                                                    _div +='</div>';
                                                }

                                                    //show graph 
                                                _div += '<div class=\'items-g\' data-gid='+id+' id='+id+'></div>';
                                                _div += '</div>';
                                                //.prepend()
                                                 $('#showgraph_load').html(_div);
                                                 _show(id);
                                                
                                        }
                                    });
                                    
                                 
                                    return false;
                                }//_create
                                var _width = 435;
                                function _update(id){

                                    var id = id;  
                                    
                                    var url = '" . Url::to(['/ckd/graph/add']) . "';
                                    var urls = url+'?id='+id+'&graph=" . $graph . "&status=2';
                                    //console.log(urls);
                                    modalUser(urls); 

                            };//_update
                            
                            
                            function _delete(id){
                                  
                                 
                                  var gname = '';
                                  var id = id;
                                  var url = '" . Url::to(['/ckd/graph/delete-graph']) . "'; 
                                  
                                  $.ajax({
                                        url:'" . Url::to(['/ckd/graph/max-id']) . "',
                                        type:'POST',
                                        data:{id:id},
                                        dataType:'JSON',    
                                        success:function(data){
                                            krajeeDialog.confirm('<b>คุณต้องการลบกราฟ '+data[3]+' หรือไม่</b>', function (result) {
                                         if (result) {        
                                                 $.ajax({
                                                     url:url,
                                                     type:'POST',
                                                     data:{id:id}, 
                                                     dataType:'JSON',
                                                     success:function(result){
                                                         
                                                        " . \common\lib\sdii\components\helpers\SDNoty::show('result.message', 'result.status') . ";
                                                          
                                                      
                                                         for(var i=1; i<=100; i++){
                                                            $('#'+id).remove();
                                                            $('#g'+id).remove();
                                                         }   
                                                        
                                                        
                                                     }
                                                 });
                                         } else {
                                             $('#modal-lg').modal('hide');
                                         }

                                     });
                                            
                                        }
                                   });  
  
                                 
                                     return false;
                                 }//_delete
                                 
                                 function _removeEle(id){
                                    for(var i=1; i<=100; i++){
                                        $('#'+id).hide();
                                        $('#g'+id).hide();
                                    } 
                                    //console.log(id);
                                 }
                                    

                                 function AddGraphAll(){
                                     
                                    $('#showgraph_load').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');  
                                    $.ajax({
                                        url:'" . Url::to(['/ckd/graph/graph-full']) . "',
                                        method:'GET',
                                        data:{
                                            hospcode    :   '" . $hospcode . "',
                                            ptlink      :   '" . $ptlink . "',
                                            pid         :   '" . Yii::$app->session['emr_pid'] . "',
                                            graph       :   '" . $graph . "',
                                            cid         :   '" . $cid . "',
                                             
                                        },                                         
                                        success:function(result){
                                           
                                           $('#showgraph_load').html(result);
                                        }
                                    });  
                                 }//Add graph
              
                               //end
                                    

                                
                            ");
    ?>

</div>