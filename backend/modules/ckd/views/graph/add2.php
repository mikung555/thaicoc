 
 <?php
    use yii\helpers\Url;
    use yii\helpers\Html;
    use yii\bootstrap\ActiveForm;
    $this->title = "เพิ่มกราฟ";
    use backend\modules\ckd\models\Labgraplists;
    
    
   // $labmap = \yii\helpers\ArrayHelper::map($tdc_l, 'rr', 'rr'); 
    //appxq\sdii\utils\VarDumper::dump($tdc_l);
    
?>
<?php $form = ActiveForm::begin([
    'id' => $model->formName(),
     'enableAjaxValidation'      => true,//เปิดการใช้งาน AjaxValidation
    'enableClientValidation'    => false,
    'validateOnChange'          => true,//กำหนดให้ตรวจสอบเมื่อมีการเปลี่ยนแปลงค่า
    'validateOnSubmit'          => true,//กำหนดให้ตรวจสอบเมื่อส่งข้อมูล
    'validateOnBlur'            => false,
 
]);?>
    
    <?= $form->field($model,'graph_name')->textInput();?>
    <?php
        $sql2="SELECT  tdc_lab_item_code as id, graph_name as graph_label  FROM lab_graph_list WHERE group_id=99";
        $lab_graph_l = \backend\modules\ckdnet\classes\CkdnetFunc::queryAll($sql2,[':hospcode'=>Yii::$app->session['dynamic_connection']['sitecode']]);
        
        $maps = yii\helpers\ArrayHelper::map($lab_graph_l, 'id','graph_label');
        
        $labmap = yii\helpers\ArrayHelper::merge($maps, $labmap);
       
            echo $form->field($model, 'tdc_lab_item_code')->widget(kartik\select2\Select2::className(), [
             //'initValueText' => $model1->assign, // set the initial display text
             'data' => $labmap,
             'options' => ['placeholder' => 'กรุณาเลือกเส้นกราฟ...', 'multiple' => true  ],
             'pluginOptions' => [
                 'allowClear' => true,
                 //'tags' => true,
                 'tokenSeparators' => [',', ' '],
             ]
         ])->label("เส้นกราฟ (Lab)");
    ?>
<div>
    <label>ความกว้าง</label>
    <?php $model->cols='6';?>
    <?= $form->field($model, 'cols')->inline()->radioList($model->getGraphWidth())->label(FALSE);?>
    <?php 
        $model->cstatus = $cstatus;
        $model->cid =  Yii::$app->session['cid_'];  
        echo $form->field($model, 'cstatus')->hiddenInput()->label(FALSE);
        echo $form->field($model, 'cid')->hiddenInput()->label(FALSE);
        //$cstatus
    ?>
</div>
<div class="modal-footer">
    <span id="spiner"></span>
<?= Html::submitButton('บันทึก',['class'=>'btn btn-primary btn-lg']) ?>

</div>
 

<?php ActiveForm::end();?>

<?php $this->registerJs("

  
$('form#{$model->formName()}').on('beforeSubmit', function(e) {
    var \$form = $(this);
    $('#spiner').html('<span class=\"sdloader \"><i class=\"sdloader-icon\"></i></span>'); 
    $.post(
	\$form.attr('action'), //serialize Yii2 form
	\$form.serialize()
    ).done(function(result) {
	 
         " . \common\lib\sdii\components\helpers\SDNoty::show('result.message', 'result.status') . ";
              $('#modal-lg').modal('hide');
          $(\$form).trigger('reset');
          $('#spiner').html('');
            //console.log(result);
            if(result.cstatus == 'create'){
                 _width = result._width;
                _create(result.maxid); //create 
            }else{
                 //console.log(result._width);
                 _width = result._width;
                _show(result.id);//update
                

            }
             
             
            //$('#items-add').reload();
          
          //setTimeout(function(){ showgraph_load(); },1000);
    });
    $('#spiner').html(''); 
    return false;
});

"); ?>
<style>
  .select2-search__field{
        width: 194px;
  }
</style>