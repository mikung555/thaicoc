<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\ckd\models\HelperGis */

$this->title = 'Helper Gis#'.$model->title;
$this->params['breadcrumbs'][] = ['label' => 'Helper Gis', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="helper-gis-view">

    <div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	<h4 class="modal-title" id="itemModalLabel"><?= Html::encode($this->title) ?></h4>
    </div>
    <div class="modal-body">
        <?= DetailView::widget([
	    'model' => $model,
	    'attributes' => [
		'id',
		'title',
		'patient_name',
		'hn',
		'patient_id',
		'volunteer',
		'volunteer_id',
		'address',
		'lat',
		'lng',
		'tel',
	    ],
	]) ?>
    </div>
</div>
