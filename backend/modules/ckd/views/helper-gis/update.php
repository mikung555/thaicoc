<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\ckd\models\HelperGis */

$this->title = 'Update Helper Gis: ' . ' ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Helper Gis', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="helper-gis-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
