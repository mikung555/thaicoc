<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\helpers\Url;
use appxq\sdii\widgets\GridView;
use appxq\sdii\widgets\ModalForm;
use appxq\sdii\helpers\SDNoty;
use appxq\sdii\helpers\SDHtml;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\ckd\models\HelperGisSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Helper Gis';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="helper-gis-index">

    <div class="sdbox-header">
	<h3><?=  Html::encode($this->title) ?></h3>
    </div>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p style="padding-top: 10px;">
	<span class="label label-primary">Notice</span>
	<?= Yii::t('app', 'You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b> or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.') ?>
    </p>

    <?php  Pjax::begin(['id'=>'helper-gis-grid-pjax']);?>
    <?= GridView::widget([
	'id' => 'helper-gis-grid',
	'panelBtn' => Html::button(SDHtml::getBtnAdd(), ['data-url'=>Url::to(['helper-gis/create']), 'class' => 'btn btn-success btn-sm', 'id'=>'modal-addbtn-helper-gis']). ' ' .
		      Html::button(SDHtml::getBtnDelete(), ['data-url'=>Url::to(['helper-gis/deletes']), 'class' => 'btn btn-danger btn-sm', 'id'=>'modal-delbtn-helper-gis', 'disabled'=>true]),
	'dataProvider' => $dataProvider,
	'filterModel' => $searchModel,
        'columns' => [
	    [
		'class' => 'yii\grid\CheckboxColumn',
		'checkboxOptions' => [
		    'class' => 'selectionHelperGiIds'
		],
		'headerOptions' => ['style'=>'text-align: center;'],
		'contentOptions' => ['style'=>'width:40px;text-align: center;'],
	    ],
	    [
		'class' => 'yii\grid\SerialColumn',
		'headerOptions' => ['style'=>'text-align: center;'],
		'contentOptions' => ['style'=>'width:60px;text-align: center;'],
	    ],

            'id',
            'title',
            'patient_name',
            'hn',
            'patient_id',
            // 'volunteer',
            // 'volunteer_id',
            // 'address',
            // 'lat',
            // 'lng',
            // 'tel',

	    [
		'class' => 'appxq\sdii\widgets\ActionColumn',
		'contentOptions' => ['style'=>'width:80px;text-align: center;'],
		'template' => '{view} {update} {delete}',
	    ],
        ],
    ]); ?>
    <?php  Pjax::end();?>

</div>

<?=  ModalForm::widget([
    'id' => 'modal-helper-gis',
    'size'=>'modal-lg',
]);
?>

<?php  $this->registerJs("
$('#helper-gis-grid-pjax').on('click', '#modal-addbtn-helper-gis', function() {
    modalHelperGi($(this).attr('data-url'));
});

$('#helper-gis-grid-pjax').on('click', '#modal-delbtn-helper-gis', function() {
    selectionHelperGiGrid($(this).attr('data-url'));
});

$('#helper-gis-grid-pjax').on('click', '.select-on-check-all', function() {
    window.setTimeout(function() {
	var key = $('#helper-gis-grid').yiiGridView('getSelectedRows');
	disabledHelperGiBtn(key.length);
    },100);
});

$('#helper-gis-grid-pjax').on('click', '.selectionHelperGiIds', function() {
    var key = $('input:checked[class=\"'+$(this).attr('class')+'\"]');
    disabledHelperGiBtn(key.length);
});

$('#helper-gis-grid-pjax').on('dblclick', 'tbody tr', function() {
    var id = $(this).attr('data-key');
    modalHelperGi('".Url::to(['helper-gis/update', 'id'=>''])."'+id);
});	

$('#helper-gis-grid-pjax').on('click', 'tbody tr td a', function() {
    var url = $(this).attr('href');
    var action = $(this).attr('data-action');

    if(action === 'update' || action === 'view') {
	modalHelperGi(url);
    } else if(action === 'delete') {
	yii.confirm('".Yii::t('app', 'Are you sure you want to delete this item?')."', function() {
	    $.post(
		url
	    ).done(function(result) {
		if(result.status == 'success') {
		    ". SDNoty::show('result.message', 'result.status') ."
		    $.pjax.reload({container:'#helper-gis-grid-pjax'});
		} else {
		    ". SDNoty::show('result.message', 'result.status') ."
		}
	    }).fail(function() {
		". SDNoty::show("'" . SDHtml::getMsgError() . "Server Error'", '"error"') ."
		console.log('server error');
	    });
	});
    }
    return false;
});

function disabledHelperGiBtn(num) {
    if(num>0) {
	$('#modal-delbtn-helper-gis').attr('disabled', false);
    } else {
	$('#modal-delbtn-helper-gis').attr('disabled', true);
    }
}

function selectionHelperGiGrid(url) {
    yii.confirm('".Yii::t('app', 'Are you sure you want to delete these items?')."', function() {
	$.ajax({
	    method: 'POST',
	    url: url,
	    data: $('.selectionHelperGiIds:checked[name=\"selection[]\"]').serialize(),
	    dataType: 'JSON',
	    success: function(result, textStatus) {
		if(result.status == 'success') {
		    ". SDNoty::show('result.message', 'result.status') ."
		    $.pjax.reload({container:'#helper-gis-grid-pjax'});
		} else {
		    ". SDNoty::show('result.message', 'result.status') ."
		}
	    }
	});
    });
}

function modalHelperGi(url) {
    $('#modal-helper-gis .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
    $('#modal-helper-gis').modal('show')
    .find('.modal-content')
    .load(url);
}

");?>