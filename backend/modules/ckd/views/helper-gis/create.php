<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\ckd\models\HelperGis */

$this->title = 'Create Helper Gis';
$this->params['breadcrumbs'][] = ['label' => 'Helper Gis', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="helper-gis-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
