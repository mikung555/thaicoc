<?php

use yii\helpers\Html;
use yii\helpers\Url;
?>
<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
    <?php
    $tabcontent = [
        '1. บทสรุปผู้บริหาร',
        '2. สรุปจำนวนหน่วยบริการใน Thai Care Cloud',
        '3. สรุปจำนวนประชากร ผู้รับบริการ ความชุก และผู้ป่วยโรคไตเรื้อรัง',
        '4. Survival Analysis'
    ];


    for ($i = 0; $i < count($tabcontent); $i++) {
        ?>
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="heading<?= $i; ?>">
                <h4 class="panel-title">
                    <a class="collapsed" id="reportpage<?= $i; ?>" role="button" data-toggle="collapse" data-parent="#accordion" href="#report<?= $i; ?>" aria-expanded="false" aria-controls="report<?= $i; ?>">
                        <?php echo $tabcontent[$i]; ?> 
                        <?php
                        if ($i != 1) {
                            echo $text;
                        }
                        ?>
                    </a>
                </h4>
            </div>
            <div id="report<?= $i; ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading<?= $i; ?>">
                <div class="panel-body" >
                    <?php
                    //if(in_array($province, array("","10","12","13","23","27","30","31","32","33","34","35","36","37","38","39","40","41","42","43","44","45","46","47","48","49","50","51","52","54","55","57","58","63","67","73","84","90","92"))){
           
                        echo $this->render("_ckdreport_f" . $i, [
                            'zone' => $zone,
                            'province' => $province,
                            'hospital' => $hospital,
                            'linkpage_f0' => $linkpage_f0,
                            'linkpage_f2' => $linkpage_f2,
                            'linkpage_f3' => $linkpage_f3,
                        ]);
                

//                    }else{
//                        echo "<div class='alert alert-warning' style='text-align:center;'><strong> ไม่พบข้อมูลในระบบ Thai Care Cloud </strong></div>";
//                    }
//                    if ($province == 11) {
//                        echo "<div class='alert alert-warning' style='text-align:center;'><strong> จังหวัดนี้ยังไม่ได้เข้าสู่ระบบ Thai Care Cloud </strong></div>";
//                    } else {
//                        echo $this->render("_ckdreport_f". $i, [
//                            'zone' => $zone,
//                            'province' => $province,
//                            'hospital' => $hospital,
//                            'linkpage_f0' => $linkpage_f0,
//                            'linkpage_f2' => $linkpage_f2,
//                            'linkpage_f3' => $linkpage_f3,
//                        ]);
//                    }
                    ?> 
                </div>
            </div>
        </div>
<?php } ?>
</div>


