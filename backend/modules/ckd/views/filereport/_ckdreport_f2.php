<?php

use yii\helpers\Html;
//echo "F2_" . $hospital . " - " . Yii::$app->user->identity->userProfile->sitecode;
?>
<!--<div class="row">
    <div class="col-md-12">
        <div class="alert alert-warning">
            <strong> กำลังพัฒนาระบบ </strong> 
        </div>
    </div>
</div>-->

<?php
if ($hospital != NULL) {
    if ($hospital == Yii::$app->user->identity->userProfile->sitecode) {
        echo Html::hiddenInput('linkckd_f2', $linkpage_f2, ['id' => 'linkckd_f2']);
        $file = file_get_contents($linkpage_f2);
        if ($file != '' || $file != null) {
            echo '<div class="embed-responsive embed-responsive-16by9">
                    <iframe class="embed-responsive-item" src="" id="reportckd_f2" ></iframe>
                </div>';

            $this->registerJS("
            var linkckd_f2 = $('#linkckd_f2').val();
            $('#reportckd_f2').attr('src',linkckd_f2);

        ");
        } else {
            echo "<div class='alert alert-warning' style='text-align:center;'><strong> ไม่พบข้อมูลในระบบ Thai Care Cloud </strong></div>";
        } // end if chk file
    } else {
        echo "<div class='alert alert-warning' style='text-align:center;'><strong> ไม่อนุญาตการเข้าถึงรายงานของหน่วยบริการอื่นที่ไม่ใช่หน่วยงานที่ท่านสังกัด ท่านสามารถเลือกดูข้อมูลในระดับจังหวัดและข้อมูลในระดับประเทศได้ </strong></div>";
    } //end if chk sitecode = identity sitecode
    
} else {
    
    echo Html::hiddenInput('linkckd_f2', $linkpage_f2, ['id' => 'linkckd_f2']);
    $file = file_get_contents($linkpage_f2);
    if ($file != '' || $file != null) {
        echo '<div class="embed-responsive embed-responsive-16by9">
                <iframe class="embed-responsive-item" src="" id="reportckd_f2" ></iframe>
            </div>';

        $this->registerJS("
            var linkckd_f2 = $('#linkckd_f2').val();
            $('#reportckd_f2').attr('src',linkckd_f2);

        ");
    } else {
        echo "<div class='alert alert-warning' style='text-align:center;'><strong> ไม่พบข้อมูลในระบบ Thai Care Cloud </strong></div>";
    } // end if chk file
} // end if chk sitecode != NULL
?>