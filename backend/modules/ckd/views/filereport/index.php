<?php

use kartik\tabs\TabsX;
use yii\helpers\Url;
use kartik\select2\Select2;
use kartik\widgets\DepDrop;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;

/* @var $this yii\web\View */
$this->title = Yii::t('backend', 'Module: CKDNET');
$this->params['breadcrumbs'][] = ['label' => 'Modules', 'url' => yii\helpers\Url::to('/tccbots/my-module')];
$this->params['breadcrumbs'][] = Yii::t('backend', 'CKDNET');
//\backend\assets\AngularjsAsset::register($this);
?>


<div class="report">
    <div class="col-lg-12">
        <div style="padding-bottom: 10px;">
            <?php ActiveForm::begin(['id' => 'formSearch']); //,'action' => 'filereport/get-report'  ?> 
            <?php //Html::hiddenInput('page_number', $_GET['page_number'])  ?>
            <br/>
            <div class="col-md-5">
                <h5 class="section-text">ปีงบประมาณ</h5>
                <?=
                Select2::widget([
                    'name' => 'year',
                    'id' => 'year',
                    'disabled' => true,
//                    'hideSearch' => true,
                    'value' => (date('m')>9?date("Y")+1:date("Y")),
                    'data' => $itemYear,
                    'options' => [
                        'prompt' => '-----ทั้งหมด-----',
                    ],
                    'pluginOptions' => [
                        'allowClear' => true,
                    ],
                ]);
                ?>
            </div>
            <div class="col-md-5">
                <h5 class="section-text">ดูรายงานจาก :</h5>
                <?php
                echo Select2::widget([
                    'name' => 'lab',
                    'id' => 'lab',
                    'data' => ['1' => 'ดูรายงานจากที่มีผลวินิจฉัยทั้งหมด', '2' => 'ดูรายงานจากผลที่แพทย์ยืนยันแล้ว'], //,'2'=>'ดูรายงานจากผลที่แพทย์ยืนยันแล้ว'
                    'disabled' => true,
                    'pluginOptions' => [
                        'allowClear' => true,
                    ],
                ]);
                ?>
                <?= Html::HiddenInput('lab', '1') ?>
            </div>


            <div class="clearfix"></div>
            <div class="col-md-5" id="div-zone-choice">
                <h5 class="section-text">เขต :</h5>
                <?=               
                Select2::widget([
                    'name' => 'zone',
                    'id' => 'zone',
                    'value' => $zone_select,
                    'hideSearch' => true,
                    'data' => $itemZone,
                    'options' => [
                        'prompt' => 'เขตสุขภาพทั้งหมด',
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
                ?>
            </div>
            <div class="col-md-5" id="div-province">
                <h5 class="section-text">จังหวัด :</h5>
                <?=
                DepDrop::widget([
                    'type' => DepDrop::TYPE_SELECT2,
                    'data' => [$select_province => 'selected'],
                    'name' => 'province',
                    'options' => ['id' => 'province'],
                    'select2Options' => ['pluginOptions' => ['allowClear' => false]],
                    'pluginOptions' => [
                        'depends' => ['zone'],
                        'initialize' => true,
                        'initDepends' => ['zone'],
                        'params' => ['zone'],
                        'placeholder' => '-----ทั้งหมด-----',
                        'url' => '/ckd/filereport/get-province',
                        'allowClear' => true,
                    ]
                ]);
                
                ?>
            </div>
            <div class="clearfix"></div>
            <div class="col-md-5" id="div-hospital">
                <h5 class="section-text" id="label-select">โรงพยาบาล :</h5>
                <?php 
                echo DepDrop::widget([
                    'type' => DepDrop::TYPE_SELECT2,
                    'data' => [$hospital=>$hospital . " - " . $hospital_name],
                    'name' => 'hospital',
                    'select2Options' => ['pluginOptions' => ['allowClear' => true]],
                    'pluginOptions' => [
                        'depends' => ['province'],
                        'initialize' => false,
                        'initDepends' => ['province'],
                        'params' => ['province'],
                        'placeholder' => '-----ทั้งหมด-----',
                        'url' => '/ckd/filereport/get-hospital',
                        'allowClear' => true,
                    ]
                ]);
                                          
                ?>
            </div>

            <div class="clearfix"></div>
            <br/>
            <div class="col-md-4">
                <button type="submit" id="searchData" class="btn btn-primary"><i class="glyphicon glyphicon-search"></i> แสดงรายงาน</button>
            </div>
            <div class="clearfix"></div>
            <?php ActiveForm::end() ?>
        </div>

        <hr>

        <div class="row" id="showcontent">
            <div class="col-md-12">
                <!--                <div id="search-content"></div>-->
                <?php // echo $province."  ".$province_name ."/ ".$hospital . " - " . $hospital_name;
//                echo $linkpage_f0."<br>".$linkpage_f2."<br>".$linkpage_f3;
                echo $this->render('_filereport_list', [
                    'zone' => $zone,
                    'province' => $province,
                    'hospital' => $hospital,
                    'text' => $text,
                    'linkpage_f0' => $linkpage_f0,
                    'linkpage_f2' => $linkpage_f2,
                    'linkpage_f3' => $linkpage_f3,
                ]);
                ?>


            </div>
        </div>




        <?php
        $this->registerJs("
        
        
//        $('#searchData').click(function(){
//            
//            $('#search-content').html('<div class=\"sdloader \"><i class=\"fa fa-spinner fa-pulse fa-fw fa-3x\"></i></div>');
//            $.ajax({
//                url:'" . Url::to(['filereport/search-report']) . "',
//                method:'POST',
//                data:$('#formSearch').serialize(),
//                //dataType:'HTML',
//                success:function(result){
//                   $('#search-content').html(result);
//                },error: function (xhr, ajaxOptions, thrownError) {
//                   console.log(xhr);
//                }
//            });
//            return false;
//        });
    ");
        ?>