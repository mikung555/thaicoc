<?php

use yii\helpers\Html;
//echo "F3_".$hospital." - ".Yii::$app->user->identity->userProfile->sitecode;
?>

<?php

if ($hospital != NULL) {
    if ($hospital == Yii::$app->user->identity->userProfile->sitecode) {
        echo Html::hiddenInput('linkckd_f3', $linkpage_f3, ['id' => 'linkckd_f3']);
        $file = file_get_contents($linkpage_f3);
        if ($file != '' || $file != null) {
            echo '<div class="embed-responsive embed-responsive-16by9">
                <iframe class="embed-responsive-item" src="" id="reportckd_f3" ></iframe>
            </div>';

            $this->registerJS("
            var linkckd_f3 = $('#linkckd_f3').val();
            $('#reportckd_f3').attr('src',linkckd_f3);

        ");
        } else {
            echo "<div class='alert alert-warning' style='text-align:center;'><strong> ไม่พบข้อมูลในระบบ Thai Care Cloud </strong></div>";
        } // end if chk file
    } else {
        echo "<div class='alert alert-warning' style='text-align:center;'><strong> ไม่อนุญาตการเข้าถึงรายงานของหน่วยบริการอื่นที่ไม่ใช่หน่วยงานที่ท่านสังกัด ท่านสามารถเลือกดูข้อมูลในระดับจังหวัดและข้อมูลในระดับประเทศได้ </strong></div>";
    } //end if chk sitecode = identity sitecode
} else {

    echo Html::hiddenInput('linkckd_f3', $linkpage_f3, ['id' => 'linkckd_f3']);
    $file = file_get_contents($linkpage_f3);
    if ($file != '' || $file != null) {
        echo '<div class="embed-responsive embed-responsive-16by9">
                <iframe class="embed-responsive-item" src="" id="reportckd_f3" ></iframe>
            </div>';

        $this->registerJS("
            var linkckd_f3 = $('#linkckd_f3').val();
            $('#reportckd_f3').attr('src',linkckd_f3);

        ");
    } else {
        echo "<div class='alert alert-warning' style='text-align:center;'><strong> ไม่พบข้อมูลในระบบ Thai Care Cloud </strong></div>";
    } // end if chk file
} // end if chk sitecode != NULL
?>