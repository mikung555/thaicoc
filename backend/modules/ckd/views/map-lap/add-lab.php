<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;
use backend\modules\guide\models\GuideList;
use kartik\select2\Select2;
use appxq\sdii\widgets\ModalForm;
use \yii\helpers\Url;
use unclead\multipleinput\MultipleInput;
use unclead\multipleinput\MultipleInputColumn;
use unclead\multipleinput\TabularColumn;
use unclead\multipleinput\TabularInput;

/* @var $this yii\web\View */
/* @var $model backend\modules\guide\models\GuideField */
/* @var $form yii\bootstrap\ActiveForm */

//appxq\sdii\utils\VarDumper::dump($model->new_items);
$this->title = "Maplab";
?>




<div>

    <?php
    $form = ActiveForm::begin([
//                'action' => yii\helpers\Url::to('map-lap/index'),
                'id' => $model->formName(),
    ]);
    ?>

    <div class="">

        <div class="col-md-12" style="    margin: 10px 0 0 0;">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>  
                <h4 class="modal-title">Add Lab Items</h4>
            </div>
            <div class="modal-body" style="padding-left:20px;">
                <div class="">
                    <?=
                    $form->field($model, 'new_items')->label(false)->widget(MultipleInput::className(), [
                        'id' => 'dynamic',
                        'max' => 100,
                        'allowEmptyList' => false,
                        'min' => 1,
                        'addButtonPosition' => MultipleInput::POS_HEADER,
                        'addButtonOptions' => [
                            'label' => "<i class='glyphicon glyphicon-plus'></i> New Lab",
                            'class' => 'btn-success',
                        ],
                        'columns' => [
                                [
                                'name' => 'tdc_lab_name',
                                'title' => 'TDC LAB NAME',
                                'type' => MultipleInputColumn::TYPE_TEXT_INPUT,
                                'options' => [
                                    'placeholder' => 'new lab anme...',
                                    'required'=>true,
                                ],
                                'headerOptions' => [
                                    'style' => 'width: 25%;',
                                ]
                            ],
                                [
                                'name' => 'graph_label',
                                'title' => 'GRAPH LABEL',
                                'type' => MultipleInputColumn::TYPE_TEXT_INPUT,
                                'options' => [
                                    'placeholder' => 'new graph label...',
                                    'required'=>true,
                                ],
                                'headerOptions' => [
                                    'style' => 'width: 25%;',
                                ]
                            ],
                                [
                                'name' => 'tdc_lab_unit',
                                'title' => 'LAB UNIT',
                                'type' => MultipleInputColumn::TYPE_TEXT_INPUT,
                                'options' => [
                                    'placeholder' => 'new lab unit...',
                                    'required'=>true,
                                ],
                                'headerOptions' => [
                                    'style' => 'width: 25%;',
                                ]
                            ],
                                [
                                'name' => 'note',
                                'title' => 'NOTE',
                                'type' => MultipleInputColumn::TYPE_TEXT_INPUT,
                                'options' => [
                                    'placeholder' => 'new note...',
                                    'required'=>true,
                                ],
                                'headerOptions' => [
                                    'style' => 'width: 25%;',
                                ]
                            ],
                        ]
                    ]);
                    ?>
                </div>



            </div>
            <div class="modal-footer">
                <div>     
                    <?=
                    Html::submitButton("<i class='glyphicon glyphicon-download-alt'> </i> Save", [
                        'class' => 'btn btn-primary']);
                    ?>

                    <?=
                    Html::button("<i class='glyphicon glyphicon-remove'> </i> Back", [
                        'data-url' => yii\helpers\Url::to(['map-lap/index']),
                        'class' => 'btn btn-warning',
                        'id' => 'btn-modal-back']);
                    ?>

                </div>
            </div>


        </div>




    </div>
    <?php ActiveForm::end(); ?>
    <div class="clearfix"></div> 
</div>



<?php $this->registerJs("
    

$('form#{$model->formName()}').on('beforeSubmit', function(e) {
    
    var \$form = $(this);
    $.post(
	\$form.attr('action'),
	\$form.serialize()
    ).done(function(result) {
	 console.log(result);
         " . \common\lib\sdii\components\helpers\SDNoty::show('result.message', 'result.status') . ";
            $('#modal-lg').modal('show')
                .find('.modal-content')
                .load($('#btn-modal-back').attr('data-url'));
             $('#modal-lg .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
               
    });
    return false;
});

$('#btn-modal-back').on('click', function() {
    $('#modal-lg .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
    $('#modal-lg').modal('show')
    .find('.modal-content')
    .load($(this).attr('data-url'));
});




"); ?>
