<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;
use backend\modules\guide\models\GuideList;
use kartik\select2\Select2;
use appxq\sdii\widgets\ModalForm;
use \yii\helpers\Url;
use unclead\multipleinput\MultipleInput;
use unclead\multipleinput\MultipleInputColumn;
use unclead\multipleinput\TabularColumn;
use unclead\multipleinput\TabularInput;
use appxq\sdii\helpers\SDNoty;
use appxq\sdii\helpers\SDHtml;

/* @var $this yii\web\View */
/* @var $model backend\modules\guide\models\GuideField */
/* @var $form yii\bootstrap\ActiveForm */

//appxq\sdii\utils\VarDumper::dump($model->new_items);
$this->title = "Mapping Lab";
?>




<div>

    <?php
    $form = ActiveForm::begin([
//                'action' => yii\helpers\Url::to('map-lap/index'),
                'id' => $model->formName()
    ]);
    ?>

    <div>

        <div class="col-md-12" style="    margin: 10px 0 0 0;">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>  
                <h4 class="modal-title">
                    <?php echo (Yii::$app->user->can('administrator') || Yii::$app->user->can('adminsite')) ? '' : "<label class='alert-danger'>สิทธิ์เฉพาะแอดมินเท่านั้น!!</label>" ?> 
                </h4>
            </div>
            <div class="modal-body" style="padding-left:20px;">
                <div class="">
                    <?php
//                    echo $form->field($model, 'items')->label(false)->widget(MultipleInput::className(), [
//                        'id' => 'dynamic',
//                        'max' => 100,
//                        'allowEmptyList' => false,
//                        'min' => 0,
//                        'addButtonOptions' => [
//                            'class' => 'hide',
//                        ],
//                        'removeButtonOptions' => [
//                            'class' => (Yii::$app->user->can('administrator') || Yii::$app->user->can('adminsite')) ? 'btn btn-danger' : 'hide',
//                        ],
//                        'columns' => [
//                                [
//                                'name' => 'tdc_lab_items_code',
//                                'title' => 'items code',
//                                'type' => MultipleInputColumn::TYPE_HIDDEN_INPUT,
////                                'options' => [
//                            //'value' => $data['map_items'],
////                                    'data' => $mapItems,
////                                    'class' => 'col-md-6',
////                                    'pluginOptions' => [
////                                        'placeholder' => 'select items...',
////                                        'readonly' => true,
////                                    ],
////                                ]
//                            ],
//                                [
//                                'name' => 'map_items',
//                                'title' => 'Lab items requried by TDC',
//                                'type' => MultipleInputColumn::TYPE_STATIC,
//                            ],
//                                [
//                                'name' => 'old_labcode',
//                                'title' => 'Odl Code',
//                                'type' => MultipleInputColumn::TYPE_HIDDEN_INPUT,
//                            ],
//                                [
//                                'name' => 'LABCODE',
//                                'title' => 'Lab items from HIS',
//                                'type' => Select2::className(),
//                                'options' => [
//                                    'data' => $labItems,
//                                    'pluginOptions' => [
//                                        'placeholder' => 'select lab...',
//                                        'allowClear' => true,
//                                        'multiple' => true,
//                                        'tags' => true,
//                                        'tokenSeparators' => [',', ' '],
//                                        'disabled' => (Yii::$app->user->can('administrator') || Yii::$app->user->can('adminsite')) ? '' : 'disabled'
//                                    ],
//                                ],
////                                'headerOptions' => [
////                                    'style' => 'width: 50%;',
////                                ]
//                            ],
//                        ]
//                    ]);
//                    
//                    
                    ?>

                    <div class="col-lg-12" id="field-items">
                        <div class="col-lg-5" style="font-size: large; color: black;"><b>Lab items requried by TDC</b></div>
                        <div class="col-lg-7" style="text-align: center; font-size: large; color: black;"><b>Lab items from HIS</b></div>
                        <div class="clearfix"></div>
                        <hr>
                        <?php foreach ($model->items as $key => $value) { ?>

                            <div id="<?= $key ?>">
                                <div class="col-lg-5">
                                    <?php echo Html::hiddenInput("LabMap[items][" . $key . "][tdc_lab_items_code]", $value['tdc_lab_items_code']) ?>
                                    <?php echo Html::label($value['map_items']) ?>
                                    <div style="display:none"> 
                                        <?php
                                        echo Select2::widget([
                                            'name' => "LabMap[items][" . $key . "][old_labcode]",
                                            'value' => $value['LABCODE'],
                                            'data' => $labItems,
                                            'options' => [
                                                'id' => 'st3-' . $key,
                                                'placeholder' => 'select lab...',
                                                'multiple' => true,
                                            ],
                                            'pluginOptions' => [
                                                'allowClear' => true
                                            ],
                                        ])
                                        ?>
                                    </div>
                                    <?php // echo Html::hiddenInput("LabMap[items][" . $key . "][old_labcode]", $value['LABCODE']) ?>
                                </div>     
                                <div class="col-lg-7">
                                    <div class="col-lg-11"> 
                                        <?php
                                        echo Select2::widget([
                                            'name' => "LabMap[items][" . $key . "][LABCODE]",
                                            'value' => $value['LABCODE'],
                                            'data' => $labItems,
                                            'options' => [
                                                'id' => 'st2-' . $key,
                                                'placeholder' => 'select lab...',
                                                'multiple' => true,
                                            ],
                                            'pluginOptions' => [
                                                'allowClear' => true
                                            ],
                                        ])
                                        ?>
                                    </div>
                                    <div class="col-lg-1">
                                        <?php echo Html::button("<i class='glyphicon glyphicon-remove'></i>", ['data-url' => $key, 'class' => 'btn btn-danger btn-del-field']) ?>

                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <hr>
                            </div>


                        <?php } ?>
                    </div>

                </div>

            </div>
            <div class="modal-footer">
                <div style="padding-bottom: 10px">
                    <?php
                    echo (Yii::$app->user->can('administrator') || Yii::$app->user->can('adminsite')) ?
                            Html::button("<i class='glyphicon glyphicon-plus'></i> Add Lab", [
                                'data-url' => yii\helpers\Url::to(['map-lap/add-lab']),
                                'class' => 'btn btn-success',
                                'id' => 'btn-modal-add']) .
                            Html::submitButton("<i id='spin' class='glyphicon glyphicon-download-alt'> </i> Save", [
                                'class' => 'btn btn-primary']) :
                            Html::button("<i class='glyphicon glyphicon-remove'> </i> Cancel", [
                                'class' => 'btn btn-warning',
                                'data-dismiss' => 'modal',
                                'aria-hidden' => 'true'
                    ]);
                    ?>

                </div>
            </div>


        </div>




    </div>
    <?php ActiveForm::end(); ?>
    <div class="clearfix"></div> 
</div>



<?php $this->registerJs("
    
$('#btn-modal-add').on('click', function() {
    $('#modal-lg .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
    $('#modal-lg').modal('show')
    .find('.modal-content')
    .load($(this).attr('data-url'));
});

$('form#{$model->formName()}').on('beforeSubmit', function(e) {
    $('#spin').addClass('fa fa-spinner fa-pulse fa-1x fa-fw');
    var \$form = $(this);
    $.post(
	\$form.attr('action'),
	\$form.serialize()
    ).done(function(result) {
	 console.log(result);
         " . \common\lib\sdii\components\helpers\SDNoty::show('result.message', 'result.status') . ";
              $('#modal-lg').modal('hide');
    }).fail(function() {
                " . SDNoty::show("'" . SDHtml::getMsgError() . "'", '"error"') . "
                $('#modal-lg').modal('hide');
    });
    return false;
});

$('#field-items').on('click','.btn-del-field',function(){
    $(this).parent().parent().parent().remove();
});



"); ?>
