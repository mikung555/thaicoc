 <?php
use yii\helpers\Html;
use yii\helpers\Url;
//if (Yii::$app->user->can('doctorgen')) {

    $html = "";
    $html .= "<label> ยืนยันโดยแพทย์: </label> ";
    echo $html;
    echo "<span id='ShowBtn2'></span> ";
    
    echo Html::button('<i class="fa fa-history"></i> แสดงสถานะทั้งหมด', 
            ['data-url' => Url::to(['/ckd/doctor-confirm/history','hospcode' => $hospcode]),
                'class' => 'btn btn-primary btn-sm', 
                'id' => 'ShowConfirmAll']); 
 
//}//endif
?>
<?php 
    $this->registerJs("
        showBtn2();
        
       $('#ShowConfirmAll').click(function(){
          $('.modal-footer').hide();
          modalUser($(this).attr('data-url')); 
       });// 
       function showBtn2(){
         $.ajax({
                url:'" . Url::to(['/ckd/doctor-confirm/check-confirm']) . "',
                method:'POST',
                data:{
                    pid:'".\Yii::$app->session['emr_pid']."',
                    hospcode:'".$hospcode."',
                    doctor_type2:'2'
                },
                success:function(data){
                   $('#ShowBtn2').html(data);  
                }
            });     
       }// 
       function loadButton(){
        var btn = '';
            $.ajax({
                url:'" . Url::to(['/ckd/doctor-confirm/check-confirm']) . "',
                method:'POST',
                data:{
                    pid:'".\Yii::$app->session['emr_pid']."',
                    hospcode:'".$hospcode."',
                    doctor_type2:'2'
                },
                success:function(data){
                   if(data != ''){
                       $('#btnConfirm').hide();
                       $('#showbtn2').html(data);  
                       showckd();
                   }
                }
            });     
       }
        showckd(); 
       function showckd(){
            $.ajax({
                url:'" . Url::to(['/ckd/doctor-confirm/showckd2',
                  'pid' => Yii::$app->session['emr_pid'], 
                  'hpcode' => Yii::$app->session['dynamic_connection']['sitecode']]) . "',
                success:function(data){
                   $('#shoConfirm').html(data);  
                }
            });
            return false;
        }//showCKD        
");
?>