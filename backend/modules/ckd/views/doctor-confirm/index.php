<?php
    use yii\helpers\Url;
    use yii\helpers\Html;
    $form = yii\bootstrap\ActiveForm::begin();
    $this->title = "การยืนยันโดยแพทย์";
    
     
    // $ptlink=$_GET['ptlink'];
     
      
///echo 
?>
<div class="row" style="background: white;">
<div class="modal-header" style=" 
    border-bottom-color: #f4f4f4;
    background: #337ab7;
    color:#fff;
 ">
    <button type="button" class="btn btn-danger pull-right" data-dismiss="modal">ปิด</button>
    <h4 class="modal-title"><?= Html::encode($this->title)?></h4>
</div>
 
    
<div class="modal-body" style="padding-left:20px;">
    
    <div class="col-md-6">
        <div class="panel panel-primary">
            <div class="panel-heading"><i class="fa fa-binoculars"></i> ผลการวินิฉัยข้อมูลทั้งหมด</div>
            <div class="panel-body">
                 <?php 
                    
                    echo $outputs;
                ?>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="panel panel-primary">
            <div class="panel-heading"><i class="fa fa-binoculars"></i> ผลแล็ปในห้องปฎิบัติการที่เกี่ยวข้องกับ CKD</div>
            <div class="panel-body">
                <?php 
                    $c = $out['cf_lab'];
                    if($c == '1'){
                        $pr="เป็น CKD";
                    }
                    if($c == '0'){
                        $pr="ไม่เป็น CKD";
                    }
                    if($c == null){
                        $pr="ยังไม่มีผลแล็ป";
                    }
                    
                    echo "<b>ผล :  ".$pr."</b>";
                     
                ?>
            </div>
        </div>
    </div>
    <div class="col-md-6">
          <?php
        $items = [
            'null' => 'ยังไม่ยืนยัน',
            '1' => 'N181 : CKD Stage 1',
            '2' => 'N182 : CKD Stage 2',
            '3' => 'N183 : CKD Stage 3',
            '4' => 'N184 : CKD Stage 4',
            '5' => 'N185 : CKD Stage 5',
            '9' => 'N189 : CKD Unknown Stage',
            '0' => 'Not CKD'
        ];
        
        echo $form->field($model, 'items')->radioList($items)->label(FALSE);
         ?>
    </div> 
    <hr>
        <?php yii\bootstrap\ActiveForm::end(); ?>      
        
    </div>
</div>
<?php 
    $this->registerJs("
       
        $(\"input[name='FPerson[items]']\").on('change',function(){
             var item = $(\"input[name='FPerson[items]']:checked\").val();
            
             $.ajax({
                url:'".Url::to(['/ckd/doctor-confirm/save'])."',
                type:'POST',
                data:{
                    item:item,
                    pid:'".$pid."' ,hpcode:'".$hpcode."'},
                success:function(result){
                    console.log(result);
                    " . \common\lib\sdii\components\helpers\SDNoty::show('result.message', 'result.status') . ";
                         
                            $('#modal-lg').modal('hide');
                            showConfirmCKD('".$pid."'); //showConfirmCKD     
                        
                }        
             });
             return false;
        });
        
        function showConfirmCKD(id){
            $.ajax({
                url:'".Url::to(['/ckd/doctor-confirm/showckd', 'pid'=>$pid,'hpcode'=>$hpcode])."',
                success:function(data){
                    $('#shoConfirm').html(data);
                     
                    console.log(data);
                }
            });
            return false;
        }//showCKD
        
       
        
    ");
?>
 

