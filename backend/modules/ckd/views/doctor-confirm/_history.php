<?php 
    use yii\helpers\Url;
    $table='tbdata_1493269244023122200_triger';
    $check = \backend\modules\ckd\classes\ConfirmCheck::getCid($table,$hospcode,\Yii::$app->session['emr_pid'])->queryAll();
    $check2 = \backend\modules\ckd\classes\ConfirmCheck::getCid($table,$hospcode,\Yii::$app->session['emr_pid'])->queryAll();

?>
 

<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title" id="itemModalLabel">ประวัติการยืนยันโดยแพทย์</h4>
</div>

<div class="modal-body">
   
    <table class="table table-bordered table-responsive">
        <thead>
            <tr>
                <th>สถานะการยืนยัน (CKD,DM,HT)</th>
                <th>วันที่ยืนยัน</th>
            </tr>
        </thead>
        <tbody>
            <?php if(empty($check)):?>
            <tr>
                <td colspan="2"><span class="label label-danger">ไม่พบข้อมูล</span></td>
            </tr>
            <?php endif; ?>
            <?php foreach($check as $check): ?>
            <tr>
                <td>
                    <?php 
                        $btnShow="";        
                        if(!empty($check)){
                            if($check['fckd'] == 1){
                                if($check['ckdtype'] == 1){
                                    $btnShow .= "<code>Not CKD</code> ";
                                }else{
                                    $btnShow .= "<code> CKD stage ".($check['ckdtype']-1)."</code> ";

                                }
                            }else{
                                $btnShow .= "<code>ยังไม่ได้ยืนยัน CKD</code> ";
                            } 
                            if($check['dm'] == 1){
                                if($check['dmtype'] == 1){
                                    $btnShow .= "<code>Not DM</code> ";
                                }else if($check['dmtype'] == 2){
                                    $btnShow .= "<code>GDM</code> ";  
                                }else if($check['dmtype'] == 3){
                                    $btnShow .= "<code>Diabetes mellitus type 1</code> ";  
                                }else if($check['dmtype'] == 4){
                                    $btnShow .= "<code>Diabetes mellitus type 2</code> ";  
                                }
                            }else{
                                $btnShow .= "<code>ยังไม่ได้ยืนยัน DM</code> ";
                            } //dm
                             if($check['ht'] == 1){
                                  if($check['dmtype'] == 1){
                                     $btnShow .= "<code>Not hypertension</code> "; 
                                  }
                                   if($check['dmtype'] == 2){
                                     $btnShow .= "<code>Hypertension</code> ";  
                                   }
                             }else{
                                $btnShow .= "<code>ยังไม่ได้ยืนยัน HT</code> ";
                            }    //ht
                        }else{
                            $btnShow = "<code>ยังไม่ได้ยืนยัน CKD</code> <code>ยังไม่ได้ยืนยัน DM</code> <code>ยังไม่ได้ยืนยัน HT</code>";
                        } 
                        echo $btnShow;
                    ?>
                    
                </td>
                <td><?= $check['update_date']; ?></td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <hr>
    <h3>ประวัติการยืนยันโดย ICD</h3>
    <table class="table table-bordered table-responsive">
        <thead>
            <tr>
                <th>สถานะการยืนยัน (CKD,DM,HT)</th>
                <th>วันที่ยืนยัน</th>
            </tr>
        </thead>
        <tbody>
            <?php if(empty($check2)):?>
            <tr>
                <td colspan="2"><span class="label label-danger">ไม่พบข้อมูล</span></td>
            </tr>
            <?php endif; ?>
            <?php foreach($check2 as $check): ?>
            <tr>
                <td>
                    <?php 
                        $btnShow="";        
                        if(!empty($check)){
                            if($check['fckd'] == 1){
                                if($check['ckdtype'] == 1){
                                    $btnShow .= "<code>Not CKD</code> ";
                                }else{
                                    $btnShow .= "<code> CKD stage ".($check['ckdtype']-1)."</code> ";

                                }
                            }else{
                                $btnShow .= "<code>ยังไม่ได้ยืนยัน CKD</code> ";
                            } 
                            if($check['dm'] == 1){
                                if($check['dmtype'] == 1){
                                    $btnShow .= "<code>Not DM</code> ";
                                }else if($check['dmtype'] == 2){
                                    $btnShow .= "<code>GDM</code> ";  
                                }else if($check['dmtype'] == 3){
                                    $btnShow .= "<code>Diabetes mellitus type 1</code> ";  
                                }else if($check['dmtype'] == 4){
                                    $btnShow .= "<code>Diabetes mellitus type 2</code> ";  
                                }
                            }else{
                                $btnShow .= "<code>ยังไม่ได้ยืนยัน DM</code> ";
                            } //dm
                             if($check['ht'] == 1){
                                  if($check['dmtype'] == 1){
                                     $btnShow .= "<code>Not hypertension</code> "; 
                                  }
                                   if($check['dmtype'] == 2){
                                     $btnShow .= "<code>Hypertension</code> ";  
                                   }
                             }else{
                                $btnShow .= "<code>ยังไม่ได้ยืนยัน HT</code> ";
                            }    //ht
                        }else{
                            $btnShow = "<code>ยังไม่ได้ยืนยัน CKD</code> <code>ยังไม่ได้ยืนยัน DM</code> <code>ยังไม่ได้ยืนยัน HT</code>";
                        } 
                        echo $btnShow;
                    ?>
                    
                </td>
                <td><?= $check['update_date']; ?></td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
    
</div>
