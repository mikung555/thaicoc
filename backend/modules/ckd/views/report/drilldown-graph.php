<?php

use miloschuman\highcharts\Highcharts;
use miloschuman\highcharts\HighchartsAsset;
use appxq\sdii\widgets\GridView;
use yii\helpers\Url;
use yii\helpers\Html;
use appxq\sdii\widgets\ModalForm;

if ($year == '') {
    $year = '2560';
}
$this->registerCss('
        .table > thead.showdatatitle >tr >th {
            text-align: center;
            vertical-align: middle;
        }
        tbody.showdata {
            text-align: right;
        }
        hr.hrtext{
            margin-top: 5px;
            margin-bottom: 5px;
            border: 0;
            border-top: 1px solid #eee;
        }
    ');

HighchartsAsset::register($this)->withScripts(['highstock', 'modules/exporting', 'modules/drilldown', 'highcharts-more']);
Highcharts::widget([
    'options' => [
        'chart' => [
            'renderTo' => 'drilldown-graph-show'
        ],
        'title' => ['text' => $labelForGraph['graph']],
        'xAxis' => [
            'categories' => $dataNumber
        ],
        'yAxis' => [
            'title' => ['text' => 'ร้อยละ(100%)'],
            'min' => 0,
            'max' => 100,
            'plotLines' => [
                    [
                    'label' => [
                        'text' => ("KPI " . $toValStart . ".0 %"), // Content of the label. 
                        'align' => 'center' // Positioning of the label. 
                    ],
                    'color' => 'red', // Color value
                    'dashStyle' => 'longdashdot', // Style of the plot line. Default to solid
                    'value' => $toValStart, // Value of where the line will appear
                    'width' => 2
                ]
            ]
        ],
        'series' => [
                [
                'name' => 'ร้อยละ',
                'type' => 'column',
                'data' => $serieData
            ],
        ], 'plotOptions' => [
            'series' => [
                'pointPadding' => 0.2,
                'borderWidth' => 0,
                'dataLabels' => [
                    'enabled' => true,
                //'format' => '{point.y}%'
                ]
            ]
        ],
        'drilldown' => [
            'series' => [[
            'name' => 'Microsoft Internet Explorer',
            'id' => 'Microsoft Internet Explorer',
            'data' => [
                    [
                    'v11.0',
                    24.13
                ],
                    [
                    'v8.0',
                    17.2
                ],
                    [
                    'v9.0',
                    8.11
                ],
                    [
                    'v10.0',
                    5.33
                ],
                    [
                    'v6.0',
                    1.06
                ],
                    [
                    'v7.0',
                    0.5
                ]
            ]
                ]]
        ]
    ]
]);



$this->registerJs("
     Highcharts.chart('drilldown-guage-show', {

    chart: {
        type: 'gauge',
        plotBackgroundColor: null,
        plotBackgroundImage: null,
        plotBorderWidth: 0,
        plotShadow: false
    },

    title: {
        text: '$labelForGraph[gauge]'
    },

    pane: {
        startAngle: -140,
        endAngle: 140,
        background: [{

            backgroundColor: '#FFF',
            borderWidth: 0,
            outerRadius: '105%',
            innerRadius: '103%'
        },
        {

            backgroundColor: '#ccc',
            borderWidth: 0,
            outerRadius: '105%',
            innerRadius: '103%'
        }]
    },

    // the value axis
    yAxis: {
        min: 0,
        max: 100,

        minorTickInterval: 'auto',
        minorTickWidth: 1,
        minorTickLength: 10,
        minorTickPosition: 'inside',
        minorTickColor: '#666',

        tickPixelInterval: 30,
        tickWidth: 2,
        tickPosition: 'inside',
        tickLength: 10,
        tickColor: '#666',
        labels: {
            step: 2,
            rotation: 'auto'
        },
        title: {
            text: 'ร้อยละ $totalSum'
        },
        plotBands: [{
            from: '$fromValStart',
            to: '$toValStart',
            color: '#FF0000' // green
        }, {
            from: '$fromVal2End',
            to: '$toVal2End',
            color: '#00FF00' // red
        }]
    },

    series: [{
        name: 'Value',
        data: [$totalSum],
        tooltip: {
            valueSuffix: ' ร้อยละ'
        }
    }]

},
// Add some life
function (chart) {
 
});
");
echo '<div id="drilldown-guage-show"class="col-md-3"  style="min-width: 270px; max-width: 300px; height: 270px; margin: 0 auto"></div>';
echo "<div class='col-md-9' id='drilldown-graph-show'></div>";

$textDetail = "";

if ($report_num == 1) {
    $textAB = "A หมายถึง จำนวนผู้ป่วย DM และ/หรือ HT ที่ไม่เคยได้รับการวินิจฉัยว่าเป็นโรคไตเรื้อรังในเขตรับผิดชอบ จากหน่วยบริการภายใน CUP <hr class='hrtext'>"
            . "B หมายถึง จำนวนผู้ป่วย DM และ/หรือ HT ที่ไม่เคยได้รับการวินิจฉัยว่าเป็นโรคไตเรื้อรังในเขตรับผิดชอบ ที่ได้รับการตรวจคัดกรอง จากหน่วยบริการภายใน CUP ";
} else if ($report_num == 2) {
    $textAB = "A หมายถึง จำนวนผู้ป่วย DM และ/หรือ HT ที่ไม่เคยได้รับการวินิจฉัยว่าเป็นโรคไตเรื้อรังในเขตรับผิดชอบ ที่ได้รับการตรวจคัดกรอง จากหน่วยบริการภายใน CUP <hr class='hrtext'>"
            . "B หมายถึง จำนวนผู้ป่วย DM และ/หรือ HT ที่ไม่เคยได้รับการวินิจฉัยว่าเป็นโรคไตเรื้อรังในเขตรับผิดชอบ ที่ได้รับการตรวจคัดกรองและผลตามนิยามเป็นโรคไตเรื้อรังรายใหม่ จากหน่วยบริการภายใน CUP ";
} else if ($report_num == 3) {
    $textAB = "A หมายถึง จำนวนผู้ป่วยโรคไตเรื้อรัง Stage 1-4 สัญชาติไทยที่มารับบริการในเขตรับผิดชอบ จากหน่วยบริการภายใน CUP <hr class='hrtext'>"
            . "B หมายถึง จำนวนผู้ป่วยโรคไตเรื้อรัง Stage 1-4 สัญชาติไทยที่มารับบริการในเขตรับผิดชอบและได้รับการวัดความดันโลหิต ที่ความดันโลหิตต่ำกว่า 140/90 mmHg จากหน่วยบริการภายใน CUP ";
} else if ($report_num == 4) {
    $textAB = "A หมายถึง จำนวนผู้ป่วยโรคไตเรื้อรัง Stage 1-4 สัญชาติไทยที่มารับบริการในเขตรับผิดชอบ จากหน่วยบริการภายใน CUP <hr class='hrtext'>"
            . "B หมายถึง จำนวนผู้ป่วยโรคไตเรื้อรัง Stage 1-4 สัญชาติไทยที่มารับบริการในเขตรับผิดชอบที่ได้รับ ACEi/ARB จากหน่วยบริการภายใน CUP ";
} else if ($report_num == 5) {
    $textAB = "A หมายถึง จำนวนผู้ป่วยโรคไตเรื้อรัง Stage 3-4 สัญชาติไทยที่มารับบริการในเขตรับผิดชอบได้รับการตรวจ creatinine/มีผล eGFR ≥ 2 ค่า จากหน่วยบริการภายใน CUP <hr class='hrtext'>"
            . "B หมายถึง จำนวนผู้ป่วยโรคไตเรื้อรัง Stage 3-4 สัญชาติไทยที่มารับบริการในเขตรับผิดชอบได้รับการตรวจ creatinine/มีผล eGFR ≥ 2 ค่า และมีค่าเฉลี่ยการเปลี่ยนแปลง < 4 จากหน่วยบริการภายใน CUP ";
} else if ($report_num == 6) {
    $textAB = "A หมายถึง จำนวนผู้ป่วยโรคไตเรื้อรัง Stage 1-4 สัญชาติไทยที่มารับบริการในเขตรับผิดชอบ จากหน่วยบริการภายใน CUP <hr class='hrtext'>"
            . "B หมายถึง จำนวนผู้ป่วยโรคไตเรื้อรัง Stage 1-4 สัญชาติไทยที่มารับบริการในเขตรับผิดชอบที่ได้รับการตรวจและมีระดับ Hb > 10 gm/dl หรือ Hct > 33% จากหน่วยบริการภายใน CUP ";
} else if ($report_num == 7) {
    $textAB = "A หมายถึง จำนวนผู้ป่วยโรคไตเรื้อรัง Stage 1-4 เฉพาะที่มีเบาหวานร่วม สัญชาติไทยที่มารับบริการในเขตรับผิดชอบ จากหน่วยบริการภายใน CUP <hr class='hrtext'>"
            . "B หมายถึง จำนวนผู้ป่วยโรคไตเรื้อรัง Stage 1-4 เฉพาะที่มีเบาหวานร่วม สัญชาติไทยที่มารับบริการในเขตรับผิดชอบที่ได้รับการตรวจ HbA1c และมีค่าผลการตรวจตั้งแต่ 6.5% ถึง 7.5% จากหน่วยบริการภายใน CUP ";
} else if ($report_num == 8) {
    $textAB = "A หมายถึง จำนวนผู้ป่วยโรคไตเรื้อรัง Stage 3-4 ที่มีอายุ ≥ 50 สัญชาติไทยที่มารับบริการในเขตรับผิดชอบ จากหน่วยบริการภายใน CUP <hr class='hrtext'>"
            . "B หมายถึง จำนวนผู้ป่วยโรคไตเรื้อรัง Stage 3-4 ที่มีอายุ ≥ 50 สัญชาติไทยที่มารับบริการในเขตรับผิดชอบได้รับยากลุ่ม Statin จากหน่วยบริการภายใน CUP ";
} else if ($report_num == 9) {
    $textAB = "A หมายถึง จำนวนครั้งของการตรวจ K ของผู้ป่วยโรคไตเรื้อรัง Stage 3-4 สัญชาติไทยที่มารับบริการในเขตรับผิดชอบ จากหน่วยบริการภายใน CUP <hr class='hrtext'>"
            . "B หมายถึง จำนวนครั้งของการตรวจ K ของผู้ป่วยโรคไตเรื้อรัง Stage 3-4 สัญชาติไทยที่มารับบริการในเขตรับผิดชอบและมีค่าผลการตรวจ < 5.5 mEq/L จากหน่วยบริการภายใน CUP ";
} else if ($report_num == 10) {
    $textAB = "A หมายถึง จำนวนผู้ป่วยโรคไตเรื้อรัง Stage 3-4 สัญชาติไทยที่มารับบริการในเขตรับผิดชอบที่ได้รับการตรวจ serum HCO3 จากหน่วยบริการภายใน CUP <hr class='hrtext'>"
            . "B หมายถึง จำนวนผู้ป่วยโรคไตเรื้อรัง Stage 3-4 สัญชาติไทยที่มารับบริการในเขตรับผิดชอบได้รับการตรวจ serum HCO3 และมีค่าผลตรวจ > 22 mEq/L จากหน่วยบริการภายใน CUP ";
} else if ($report_num == 11) {
    $textAB = "A หมายถึง จำนวนผู้ป่วยโรคไตเรื้อรัง Stage 1-4 สัญชาติไทยที่มารับบริการในเขตรับผิดชอบ จากหน่วยบริการภายใน CUP <hr class='hrtext'>"
            . "B หมายถึง จำนวนผู้ป่วยโรคไตเรื้อรัง Stage 1-4 สัญชาติไทยที่มารับบริการในเขตรับผิดชอบได้รับการตรวจ urine protein จากหน่วยบริการภายใน CUP ";
} else if ($report_num == 12) {
    $textAB = "A หมายถึง จำนวนผู้ป่วยโรคไตเรื้อรัง Stage 1-4 สัญชาติไทยที่มารับบริการที่โรงพยาบาลระดับ A,S,M1 จากหน่วยบริการภายใน CUP <hr class='hrtext'> "
            . "B หมายถึง จำนวนผู้ป่วยโรคไตเรื้อรัง Stage 1-4 สัญชาติไทยที่มารับบริการที่โรงพยาบาลระดับ A,S,M1 ได้รับการตรวจ UPCR จากหน่วยบริการภายใน CUP ";
} else if ($report_num == 13) {
    $textAB = "A หมายถึง จำนวนผู้ป่วยโรคไตเรื้อรัง Stage 1-4 สัญชาติไทยที่มารับบริการในเขตรับผิดชอบระดับ A,S,M1 ได้รับการตรวจ UPCR<hr class='hrtext'> จากหน่วยบริการภายใน CUP "
            . "B หมายถึง จำนวนผู้ป่วยโรคไตเรื้อรัง Stage 1-4 สัญชาติไทยที่มารับบริการในเขตรับผิดชอบระดับ A,S,M1 ได้รับการตรวจและมีมีค่า UPCR < 500 mg/g จากหน่วยบริการภายใน CUP ";
} else if ($report_num == 14) {
    $textAB = "A หมายถึง จำนวนผู้ป่วยโรคไตเรื้อรัง Stage 3-4 สัญชาติไทยที่มารับบริการในเขตรับผิดชอบระดับ A,S และ M1 ได้รับการตรวจ Serum PO4<hr class='hrtext'> จากหน่วยบริการภายใน CUP "
            . "B หมายถึง จำนวนผู้ป่วยโรคไตเรื้อรัง Stage 3-4 สัญชาติไทยที่มารับบริการในเขตรับผิดชอบระดับ A,S และ M1ได้รับการตรวจ Serum PO4 และมีค่าผลตรวจ ≤ 4.6 mg% จากหน่วยบริการภายใน CUP ";
} else if ($report_num == 15) {
    $textAB = "A หมายถึง จำนวนผู้ป่วยโรคไตเรื้อรัง สัญชาติไทยที่มารับบริการในเขตรับผิดชอบระดับ A,S และ M1 ที่ eGFR < 45 และ ≥ 15 และตรวจ serum iPTH ทั้งหมด<hr class='hrtext'> จากหน่วยบริการภายใน CUP "
            . "B หมายถึง จำนวนผู้ป่วยโรคไตเรื้อรัง สัญชาติไทยที่มารับบริการในเขตรับผิดชอบระดับ A,S และ M1 ที่ eGFR < 45 และ ≥ 15 ได้รับการตรวจ Serum iPTH อยู่ในเกณฑ์ที่เหมาะสม (<500) จากหน่วยบริการภายใน CUP ";
}
?>

<div class="col-md-12">
    <div class="panel panel-info">
        <div class="panel-heading" style="font-weight: bold">
            <?php echo $textAB; ?>     
        </div>
    </div>
</div>
<div class="col-md-12" align="center">
    
</div>
<div class="col-md-12">
    <?php
            if($selectCode[0]['provincecode'] != ''){
            echo Html::a('<i class="fa fa-arrow-left" aria-hidden="true"></i> ย้อนกลับ ', 'javascript:void(0)', [
                            'id' => 'modal-view',
                            'class' => 'btn btn-primary pull-left',
                            'onclick' => "getGraphDrilldown('".Url::to([
                                'graph',
                                'zone_select' => $dataBackward[0]['zonecode'],
                                'province_select' => $dataBackward[0]['provincecode'],
                                'amphur_select' => $dataBackward[0]['amphurcode'],
                                'hospital_select' => $dataBackward[0]['hospcode'],
                                'sitecode_select' => $dataBackward[0]['hospcode'],
                                'view_render' => 'drilldown-graph',
                                'report_num' => $report_num,
                                'textAB' => $textAB
                            ])."')"
                        ]);
        
            }
            
        
    ?>
    <h4 align="center"><?=$dataBackward[0]['text']?></h4>
</div>
<div class="col-md-12"></div>
<div id="grid-table" class="col-md-12">
    <?php if ($report_num == 5) { ?>
        <table class="table table-responsive table-striped table-bordered">
            <thead class="showdatatitle">
                <tr>
                    <th style="width: 30%">เขตสุขภาพ</th><th style="width: 20%">A</th><th style="width: 20%">B</th><th style="width: 30%">อัตรา (100)</th>
                </tr>
            </thead>
            <tbody class="showdata">
                <?php
                for ($i = 0; $i < count($dataQuery); $i++) {
                    $amtATotal += $dataQuery[$i]['amtA'];
                    $amtBTotal += $dataQuery[$i]['amtB'];
                    ?>  
                    <tr>
                        <td style="text-align: left;">
                            <?=
                            Html::a($dataNumber[$i], '#', [
                                'id' => 'modal-view',
                                'data-url' => Url::to([
                                    'graph',
                                    'zone_select' => $selectCode[$i]['zonecode'],
                                    'province_select' => $selectCode[$i]['provincecode'],
                                    'amphur_select' => $selectCode[$i]['amphurcode'],
                                    'hospital_select' => $selectCode[$i]['hospcode'],
                                    'view_render' => 'drilldown-list',
                                    'report_num' => $report_num,
                                    'textAB' => $textAB
                                ])
                            ]);
                            // \appxq\sdii\utils\VarDumper::dump($selectCode[$i]['hospcode']);
                            ?>
                        </td>
                        <td><?= number_format($dataQuery[$i]['amtA']) ?></td>
                        <td><?= number_format($dataQuery[$i]['amtB']) ?></td>
                        <td><?= number_format($dataQuery[$i]['amtsum'], 1) ?></td>
                    </tr>
                    <?php
                }
                $amtSumTotal = ($amtBTotal / $amtATotal) * 100;
                ?>  
            <td style="text-align: left;"><b>รวมทั้งหมด</b></td>
            <td><b><?= number_format($amtATotal) ?></b></td>
            <td><b><?= number_format($amtBTotal) ?></b></td>
            <td><b><?= number_format($amtSumTotal, 1) ?></b></td>
            </tbody>
        </table>
    <?php } else { ?>

        <table class="table table-responsive table-striped table-bordered" > 
            <thead class="showdatatitle">
                <tr class="active">
                    <th rowspan="3" style="width: 20%">เขตสุขภาพ</th>
                    <th rowspan="3">A</th>
                    <th rowspan="3">B</th>
                    <th rowspan="3">อัตรา (100)</th>
                    <th colspan="8">ปีงบประมาณ <?= $year ?></th>
                </tr>
                <tr class="active">
                    <th colspan="2">ไตรมาส 1</th>
                    <th colspan="2">ไตรมาส 2</th>
                    <th colspan="2">ไตรมาส 3</th>
                    <th colspan="2">ไตรมาส 4</th>
                </tr>
                <tr class="active">
                    <th >จำนวน</th>
                    <th >อัตรา</th>
                    <th >จำนวน</th>
                    <th >อัตรา</th>
                    <th >จำนวน</th>
                    <th >อัตรา</th>
                    <th >จำนวน</th>
                    <th >อัตรา</th>
                </tr>
            </thead>
            <tbody class="showdata">     
                <?php
                for ($i = 0; $i < count($dataQuery); $i++) {
                    $amtATotal += $dataQuery[$i]['amtA'];
                    $amtBTotal += $dataQuery[$i]['amtB'];
                    $amtQ1Total += $dataQuery[$i]['amtq1'];
                    $amtQ2Total += $dataQuery[$i]['amtq2'];
                    $amtQ3Total += $dataQuery[$i]['amtq3'];
                    $amtQ4Total += $dataQuery[$i]['amtq4'];
                    ?>  
                    <tr>
                        <td class="text-left">
                            <?php
                
                                echo Html::a($dataNumber[$i], 'javascript:void(0)', [
                                    'id' => 'modal-view',
                                    'onclick' => "getGraphDrilldown('" . Url::to([
                                        'graph',
                                        'zone_select' => $selectCode[$i]['zonecode'],
                                        'province_select' => $selectCode[$i]['provincecode'],
                                        'amphur_select' => $selectCode[$i]['amphurcode'],
                                        'hospital_select' => $selectCode[$i]['hospcode'],
                                        'view_render' => 'drilldown-graph',
                                        'report_num' => $report_num,
                                        'textAB' => $textAB
                                    ]) . "')"
                                ]);
                            
                            // \appxq\sdii\utils\VarDumper::dump($selectCode[$i]['hospcode']);
                            ?>
                        </td>
                        <td><?php 
                        if($selectCode[$i]['hospcode'] != null && $dataQuery[$i]['amtA'] > 0){
                        echo Html::a(number_format($dataQuery[$i]['amtA']), 'javascript:void(0)', [
                                            'id' => 'modal-view',
                                            'data-url' => Url::to([
                                                'graph',
                                                'zone_select' => $selectCode[$i]['zonecode'],
                                                'province_select' => $selectCode[$i]['provincecode'],
                                                'amphur_select' => $selectCode[$i]['amphurcode'],
                                                'hospital_select' => $selectCode[$i]['hospcode'],
                                                'sitecode_select' => $selectCode[$i]['hospcode'],
                                                'view_render' => 'drilldown-list',
                                                'report_num' => $report_num,
                                                'textAB' => $textAB,
                                                'kpiA' => $dataQuery[$i]['amtA']
                                            ])
                                        ]);
                        }else{
                            echo number_format($dataQuery[$i]['amtA']);
                        }
                            ?>
                            
                        </td>
                        <td><?php 
                        if($selectCode[$i]['hospcode'] && $dataQuery[$i]['amtB'] > 0){
                        echo Html::a(number_format($dataQuery[$i]['amtB']), 'javascript:void(0)', [
                                            'id' => 'modal-view',
                                            'data-url' => Url::to([
                                                'graph',
                                                'zone_select' => $selectCode[$i]['zonecode'],
                                                'province_select' => $selectCode[$i]['provincecode'],
                                                'amphur_select' => $selectCode[$i]['amphurcode'],
                                                'hospital_select' => $selectCode[$i]['hospcode'],
                                                'sitecode_select' => $selectCode[$i]['hospcode'],
                                                'view_render' => 'drilldown-list',
                                                'report_num' => $report_num,
                                                'textAB' => $textAB,
                                                'kpiB' => $dataQuery[$i]['amtB']
                                            ])
                                        ]); 
                        }else{
                            echo number_format($dataQuery[$i]['amtB']);
                        }
                        
                        ?>
                        
                        </td>
                        <td><?= number_format($dataQuery[$i]['amtsum'], 1) ?></td>
                        <td><?= number_format($dataQuery[$i]['amtq1']) ?></td>
                        <td><?= number_format($dataQuery[$i]['amtq1sum'], 1) ?></td>
                        <td><?= number_format($dataQuery[$i]['amtq2']) ?></td>
                        <td><?= number_format($dataQuery[$i]['amtq2sum'], 1) ?></td>
                        <td><?= number_format($dataQuery[$i]['amtq3']) ?></td>
                        <td><?= number_format($dataQuery[$i]['amtq3sum'], 1) ?></td>
                        <td><?= number_format($dataQuery[$i]['amtq4']) ?></td>
                        <td><?= number_format($dataQuery[$i]['amtq4sum'], 1) ?></td>
                    </tr>
                    <?php
                }
                $amtSumTotal = ($amtBTotal / $amtATotal) * 100;
                $amtQ1SumTotal = ($amtQ1Total / $amtATotal) * 100;
                $amtQ2SumTotal = ($amtQ2Total / $amtATotal) * 100;
                $amtQ3SumTotal = ($amtQ3Total / $amtATotal) * 100;
                $amtQ4SumTotal = ($amtQ4Total / $amtATotal) * 100;
                ?>    
                <tr>
                    <td class="text-left"><b>รวมทั้งหมด</b></td>
                    <td><b><?= number_format($amtATotal) ?></b></td>
                    <td><b><?= number_format($amtBTotal) ?></b></td>
                    <td><b><?= number_format($amtSumTotal, 1) ?></b></td>
                    <td><b><?= number_format($amtQ1Total) ?></b></td>
                    <td><b><?= number_format($amtQ1SumTotal, 1) ?></b></td>
                    <td><b><?= number_format($amtQ2Total) ?></b></td>
                    <td><b><?= number_format($amtQ2SumTotal, 1) ?></b></td>
                    <td><b><?= number_format($amtQ3Total) ?></b></td>
                    <td><b><?= number_format($amtQ3SumTotal, 1) ?></b></td>
                    <td><b><?= number_format($amtQ4Total) ?></b></td>
                    <td><b><?= number_format($amtQ4SumTotal, 1) ?></b></td>
                </tr>
            </tbody>
        </table>

    <?php } ?>
</div>
<div class='clearfix'></div>

<?=
ModalForm::widget([
    'id' => 'modal-drilldown',
    'size' => 'modal-lg',
]);
?>  


<?php $this->registerJS("  
    $('.showdata').on('click', '#modal-view', function() {
        modalGuideField($(this).attr('data-url'));
    });
    function modalGuideField(url) {
        $('#modal-drilldown .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
        $('#modal-drilldown').modal('show')
        .find('.modal-content')
        .load(url);
        return false;
    }
    
        function getGraphDrilldown(url){
            $('#graph-show').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
            $('#yearSelect').html($('#year').val());
            $('#graph-show').load(url);
            return false;
        }
    
"); ?>