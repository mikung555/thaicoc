<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use backend\modules\ckd\controllers\ReportController;
use yii\helpers\Url;
use appxq\sdii\widgets\ModalForm;
use yii\widgets\LinkPager;
use yii\widgets\ActiveForm;
use appxq\sdii\helpers\SDNoty;
use appxq\sdii\helpers\SDHtml;
?>

<div class="drilldown-list">

    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="itemModalLabel"><?= Html::tag('label', $dataBackward[0]['text']); ?></h4>
    </div>
    <?php // \appxq\sdii\utils\VarDumper::dump($dataCurrent);   ?>

    <div class="modal-body"> 
        <div class="row">
            <?php
            if ($dataBackward[0]['zonecode'] != '') {
                ?>
                <div class="col-md-12">
                    <div class="showdata">
                        <?=
                        Html::a('<i class="fa fa-arrow-left" aria-hidden="true"></i> ย้อนกลับ', '#', [
                            'id' => 'modal-view',
                            'class' => 'btn btn-primary pull-left',
                            'data-url' => Url::to([
                                'graph',
                                'zone_select' => $dataBackward[0]['zonecode'],
                                'province_select' => $dataBackward[0]['provincecode'],
                                'amphur_select' => $dataBackward[0]['amphurcode'],
                                'hospital_select' => $dataBackward[0]['hospcode'],
                                'sitecode_select' => $dataBackward[0]['hospcode'],
                                'view_render' => 'drilldown-list',
                                'report_num' => $report_num,
                                'textAB' => $textAB
                            ])
                        ]);
                        ?>
                    </div>
                </div> 
            <?php
            }
            if ($report_num == 1) {
                $textAB = "A หมายถึง ผู้ป่วย DM และ/หรือ HT ที่ไม่เคยได้รับการวินิจฉัยว่าเป็นโรคไตเรื้อรังในเขตรับผิดชอบ จากหน่วยบริการภายใน CUP <hr class='hrtext'>"
                        . "B หมายถึง ผู้ป่วย DM และ/หรือ HT ที่ไม่เคยได้รับการวินิจฉัยว่าเป็นโรคไตเรื้อรังในเขตรับผิดชอบ ที่ได้รับการตรวจคัดกรอง จากหน่วยบริการภายใน CUP ";
            } else if ($report_num == 2) {
                $textAB = "A หมายถึง ผู้ป่วย DM และ/หรือ HT ที่ไม่เคยได้รับการวินิจฉัยว่าเป็นโรคไตเรื้อรังในเขตรับผิดชอบ ที่ได้รับการตรวจคัดกรอง จากหน่วยบริการภายใน CUP <hr class='hrtext'>"
                        . "B หมายถึง ผู้ป่วย DM และ/หรือ HT ที่ไม่เคยได้รับการวินิจฉัยว่าเป็นโรคไตเรื้อรังในเขตรับผิดชอบ ที่ได้รับการตรวจคัดกรองและผลตามนิยามเป็นโรคไตเรื้อรังรายใหม่ จากหน่วยบริการภายใน CUP ";
            } else if ($report_num == 3) {
                $textAB = "A หมายถึง ผู้ป่วยโรคไตเรื้อรัง Stage 1-4 สัญชาติไทยที่มารับบริการในเขตรับผิดชอบ<hr class='hrtext'>"
                        . "B หมายถึง ผู้ป่วยโรคไตเรื้อรัง Stage 1-4 สัญชาติไทยที่มารับบริการในเขตรับผิดชอบและได้รับการวัดความดันโลหิต ที่ความดันโลหิตต่ำกว่า 140/90 mmHg จากหน่วยบริการภายใน CUP ";
            } else if ($report_num == 4) {
                $textAB = "A หมายถึง ผู้ป่วยโรคไตเรื้อรัง Stage 1-4 สัญชาติไทยที่มารับบริการในเขตรับผิดชอบ จากหน่วยบริการภายใน CUP <hr class='hrtext'>"
                        . "B หมายถึง ผู้ป่วยโรคไตเรื้อรัง Stage 1-4 สัญชาติไทยที่มารับบริการในเขตรับผิดชอบที่ได้รับ ACEi/ARB จากหน่วยบริการภายใน CUP ";
            } else if ($report_num == 5) {
                $textAB = "A หมายถึง ผู้ป่วยโรคไตเรื้อรัง Stage 3-4 สัญชาติไทยที่มารับบริการในเขตรับผิดชอบได้รับการตรวจ creatinine/มีผล eGFR ≥ 2 ค่า จากหน่วยบริการภายใน CUP <hr class='hrtext'>"
                        . "B หมายถึง ผู้ป่วยโรคไตเรื้อรัง Stage 3-4 สัญชาติไทยที่มารับบริการในเขตรับผิดชอบได้รับการตรวจ creatinine/มีผล eGFR ≥ 2 ค่า และมีค่าเฉลี่ยการเปลี่ยนแปลง < 4 จากหน่วยบริการภายใน CUP";
            } else if ($report_num == 6) {
                $textAB = "A หมายถึง ผู้ป่วยโรคไตเรื้อรัง Stage 1-4 สัญชาติไทยที่มารับบริการในเขตรับผิดชอบ จากหน่วยบริการภายใน CUP<hr class='hrtext'>"
                        . "B หมายถึง ผู้ป่วยโรคไตเรื้อรัง Stage 1-4 สัญชาติไทยที่มารับบริการในเขตรับผิดชอบที่ได้รับการตรวจและมีระดับ Hb > 10 gm/dl หรือ Hct > 33% จากหน่วยบริการภายใน CUP";
            } else if ($report_num == 7) {
                $textAB = "A หมายถึง ผู้ป่วยโรคไตเรื้อรัง Stage 1-4 เฉพาะที่มีเบาหวานร่วม สัญชาติไทยที่มารับบริการในเขตรับผิดชอบ จากหน่วยบริการภายใน CUP<hr class='hrtext'>"
                        . "B หมายถึง ผู้ป่วยโรคไตเรื้อรัง Stage 1-4 เฉพาะที่มีเบาหวานร่วม สัญชาติไทยที่มารับบริการในเขตรับผิดชอบที่ได้รับการตรวจ HbA1c และมีค่าผลการตรวจตั้งแต่ 6.5% ถึง 7.5% จากหน่วยบริการภายใน CUP";
            } else if ($report_num == 8) {
                $textAB = "A หมายถึง ผู้ป่วยโรคไตเรื้อรัง Stage 3-4 ที่มีอายุ ≥ 50 สัญชาติไทยที่มารับบริการในเขตรับผิดชอบ จากหน่วยบริการภายใน CUP<hr class='hrtext'>"
                        . "B หมายถึง ผู้ป่วยโรคไตเรื้อรัง Stage 3-4 ที่มีอายุ ≥ 50 สัญชาติไทยที่มารับบริการในเขตรับผิดชอบได้รับยากลุ่ม Statin จากหน่วยบริการภายใน CUP";
            } else if ($report_num == 9) {
                $textAB = "A หมายถึง การตรวจ K ของผู้ป่วยโรคไตเรื้อรัง Stage 3-4 สัญชาติไทยที่มารับบริการในเขตรับผิดชอบ จากหน่วยบริการภายใน CUP<hr class='hrtext'>"
                        . "B หมายถึง การตรวจ K ของผู้ป่วยโรคไตเรื้อรัง Stage 3-4 สัญชาติไทยที่มารับบริการในเขตรับผิดชอบและมีค่าผลการตรวจ < 5.5 mEq/L จากหน่วยบริการภายใน CUP";
            } else if ($report_num == 10) {
                $textAB = "A หมายถึง ผู้ป่วยโรคไตเรื้อรัง Stage 3-4 สัญชาติไทยที่มารับบริการในเขตรับผิดชอบที่ได้รับการตรวจ serum HCO3 จากหน่วยบริการภายใน CUP<hr class='hrtext'>"
                        . "B หมายถึง ผู้ป่วยโรคไตเรื้อรัง Stage 3-4 สัญชาติไทยที่มารับบริการในเขตรับผิดชอบได้รับการตรวจ serum HCO3 และมีค่าผลตรวจ > 22 mEq/L จากหน่วยบริการภายใน CUP";
            } else if ($report_num == 11) {
                $textAB = "A หมายถึง ผู้ป่วยโรคไตเรื้อรัง Stage 1-4 สัญชาติไทยที่มารับบริการในเขตรับผิดชอบ จากหน่วยบริการภายใน CUP<hr class='hrtext'>"
                        . "B หมายถึง ผู้ป่วยโรคไตเรื้อรัง Stage 1-4 สัญชาติไทยที่มารับบริการในเขตรับผิดชอบได้รับการตรวจ urine protein จากหน่วยบริการภายใน CUP";
            } else if ($report_num == 12) {
                $textAB = "A หมายถึง ผู้ป่วยโรคไตเรื้อรัง Stage 1-4 สัญชาติไทยที่มารับบริการที่โรงพยาบาลระดับ A,S,M1 จากหน่วยบริการภายใน CUP<hr class='hrtext'>"
                        . "B หมายถึง ผู้ป่วยโรคไตเรื้อรัง Stage 1-4 สัญชาติไทยที่มารับบริการที่โรงพยาบาลระดับ A,S,M1 ได้รับการตรวจ UPCR จากหน่วยบริการภายใน CUP";
            } else if ($report_num == 13) {
                $textAB = "A หมายถึง ผู้ป่วยโรคไตเรื้อรัง Stage 1-4 สัญชาติไทยที่มารับบริการในเขตรับผิดชอบระดับ A,S,M1 ได้รับการตรวจ UPCR จากหน่วยบริการภายใน CUP<hr class='hrtext'>"
                        . "B หมายถึง ผู้ป่วยโรคไตเรื้อรัง Stage 1-4 สัญชาติไทยที่มารับบริการในเขตรับผิดชอบระดับ A,S,M1 ได้รับการตรวจและมีมีค่า UPCR < 500 mg/g จากหน่วยบริการภายใน CUP";
            } else if ($report_num == 14) {
                $textAB = "A หมายถึง ผู้ป่วยโรคไตเรื้อรัง Stage 3-4 สัญชาติไทยที่มารับบริการในเขตรับผิดชอบระดับ A,S และ M1 ได้รับการตรวจ Serum PO4 จากหน่วยบริการภายใน CUP<hr class='hrtext'>"
                        . "B หมายถึง ผู้ป่วยโรคไตเรื้อรัง Stage 3-4 สัญชาติไทยที่มารับบริการในเขตรับผิดชอบระดับ A,S และ M1ได้รับการตรวจ Serum PO4 และมีค่าผลตรวจ ≤ 4.6 mg% จากหน่วยบริการภายใน CUP";
            } else if ($report_num == 15) {
                $textAB = "A หมายถึง ผู้ป่วยโรคไตเรื้อรัง สัญชาติไทยที่มารับบริการในเขตรับผิดชอบระดับ A,S และ M1 ที่ eGFR < 45 และ ≥ 15 และตรวจ serum iPTH ทั้งหมด จากหน่วยบริการภายใน CUP<hr class='hrtext'>"
                        . "B หมายถึง ผู้ป่วยโรคไตเรื้อรัง สัญชาติไทยที่มารับบริการในเขตรับผิดชอบระดับ A,S และ M1 ที่ eGFR < 45 และ ≥ 15 ได้รับการตรวจ Serum iPTH อยู่ในเกณฑ์ที่เหมาะสม (<500) จากหน่วยบริการภายใน CUP";
            }
            ?>
            <hr>
            <div class="col-md-12 list-grid" id="list-grid-pjax">
                <div class="panel panel-info">
                    <div class="panel-heading" style="font-weight: bold">
<?php echo $textAB; ?>     
                    </div>
                </div>

<?php if (empty($dataPatient)) { ?>

                    <table class="table table-responsive table-striped table-bordered" > 
                        <thead class="showdatatitle">
                            <tr class="active">
                                <th rowspan="3" style="width: 40%">เขตสุขภาพ</th>
                                <th rowspan="3" style="width: 20%">A</th>
                                <th rowspan="3" style="width: 20%">B</th>
                                <th rowspan="3" style="width: 20%">อัตรา (100)</th>
                            </tr>
                        </thead>
                        <tbody class="showdata">   
                            <?php
                            for ($i = 0; $i < count($dataQuery); $i++) {
                                $amtA += $dataQuery[$i]['amtA'];
                                $amtB += $dataQuery[$i]['amtB'];
                                ?>    
                                <tr>
                                    <td class="text-left">
                                        <?=
                                        Html::a($dataNumber[$i], '#', [
                                            'id' => 'modal-view',
                                            'data-url' => Url::to([
                                                'graph',
                                                'zone_select' => $selectCode[$i]['zonecode'],
                                                'province_select' => $selectCode[$i]['provincecode'],
                                                'amphur_select' => $selectCode[$i]['amphurcode'],
                                                'hospital_select' => $selectCode[$i]['hospcode'],
                                                'sitecode_select' => $selectCode[$i]['hospcode'],
                                                'view_render' => 'drilldown-list',
                                                'report_num' => $report_num,
                                                'textAB' => $textAB
                                            ])
                                        ]);
                                        ?>
                                    </td>
                                    <td><?=
                                        Html::a(number_format($dataQuery[$i]['amtA']), '#', [
                                            'id' => 'modal-view',
                                            'data-url' => Url::to([
                                                'graph',
                                                'zone_select' => $selectCode[$i]['zonecode'],
                                                'province_select' => $selectCode[$i]['provincecode'],
                                                'amphur_select' => $selectCode[$i]['amphurcode'],
                                                'hospital_select' => $selectCode[$i]['hospcode'],
                                                'sitecode_select' => $selectCode[$i]['hospcode'],
                                                'view_render' => 'drilldown-list',
                                                'report_num' => $report_num,
                                                'textAB' => $textAB,
                                                'kpiA' => $dataQuery[$i]['amtA']
                                            ])
                                        ]);
                                        ?></td>
                                    <td><?=
                                        Html::a(number_format($dataQuery[$i]['amtB']), '#', [
                                            'id' => 'modal-view',
                                            'data-url' => Url::to([
                                                'graph',
                                                'zone_select' => $selectCode[$i]['zonecode'],
                                                'province_select' => $selectCode[$i]['provincecode'],
                                                'amphur_select' => $selectCode[$i]['amphurcode'],
                                                'hospital_select' => $selectCode[$i]['hospcode'],
                                                'sitecode_select' => $selectCode[$i]['hospcode'],
                                                'view_render' => 'drilldown-list',
                                                'report_num' => $report_num,
                                                'textAB' => $textAB,
                                                'kpiB' => $dataQuery[$i]['amtB']
                                            ])
                                        ]);
                                        ?></td>
                                    <td><?= number_format($dataQuery[$i]['amtsum'], 1) ?></td>
                                </tr>

                            <?php
                            }
                            $amtsum += ($amtB / $amtA) * 100;
                            ?>    
                            <tr>
                                <td align="left"><b>รวมทั้งหมด</b></td>
                                <td><b><?= number_format($amtA) ?></b></td>
                                <td><b><?= number_format($amtB) ?></b></td>
                                <td><b><?= number_format($amtsum, 1) ?></b></td>
                            </tr>
                        </tbody>
                    </table>
                <?php } else { ?>
                    <?php
                    //\appxq\sdii\utils\VarDumper::dump( $selectCode[0]['hospcode']."  ".Yii::$app->session['dynamic_connection']['sitecode']);
                    if ($selectCode[0]['hospcode'] != Yii::$app->user->identity->userProfile->sitecode) {
                        echo "<div align='center'><button class='btn btn-danger' active=false><b>ท่านไม่มีสิทธิ์เข้าดูข้อมูล กรุณาติดต่อเจ้าหน้าที่</b></button></div>";
                    } else {
                        $form = ActiveForm::begin([
                                    'id' => 'add-key',
                                    'action' => Url::to(['/ckd/report/add-key'])
                        ]);
                        ?>
                        <hr>
                        <label>กุญแจถอดรหัสข้อมูล</label><br/>

                        <?=
                        Html::passwordInput('key_ckd', Yii::$app->session['key_ckd_report'] ? Yii::$app->session['key_ckd_report'] : $key_ckd_report, [
                            'class' => 'form-control inline',
                            'style' => 'width:40%',
                            'placeholder' => 'กรุณากรอกกุญแจถอดรหัส'
                        ]);
                        ?>

                        <?=
                        Html::submitButton("<i class='fa fa-key' id='spin'></i> ถอดรหัส", [
                            'id' => 'btn-key',
                            'class' => 'btn btn-success',
                            'style' => 'margin-left:5px;'
                        ]);
                        ?>

                        <br><br>
        <?= Html::checkbox('save_key', Yii::$app->session['save_ckd_key'] != '' ? true : false, ['label' => 'จดจำคีย์นี้', 'id' => 'save_key']) ?>
        <?= Html::checkbox('convert', Yii::$app->session['convert_ckd_char'] != '' ? true : false, ['label' => 'เข้ารหัสแบบ tis620 (กรณีชื่อ-สกุล อ่านไม่ออกให้กาช่องนี้ด้วย)', 'id' => 'convert']) ?>



                        <?php ActiveForm::end(); ?>
                        <?php
                        echo GridView::widget([
                            'dataProvider' => $dataPatient,
                            'columns' => [
                                    [
                                    'class' => 'yii\grid\SerialColumn',
                                    'headerOptions' => ['style' => 'text-align: center;'],
                                    'contentOptions' => ['style' => 'width:5px;text-align: center;'],
                                ],
                                    [
                                    'label' => 'HN',
                                    'attribute' => 'pid',
                                    'headerOptions' => ['style' => 'text-align: center;'],
                                    'contentOptions' => ['style' => 'width:15px;text-align: center;'],
                                ],
                                    [
                                    'label' => 'ชื่อ',
                                    'attribute' => 'Name',
                                    'headerOptions' => ['style' => 'text-align: center;'],
                                    'contentOptions' => ['style' => 'width:30px;text-align: center;'],
                                ],
                                    [
                                    'label' => 'นามสกุล',
                                    'attribute' => 'Lname',
                                    'headerOptions' => ['style' => 'text-align: center;'],
                                    'contentOptions' => ['style' => 'width:40px;text-align: center;'],
                                ],
                                    [
                                    'label' => 'B',
                                    'attribute' => 'KPI_B',
                                    'format' => 'raw',
                                    'value' => function($dataPatient) {
                                        try {
                                            return ReportController::getKpiAb($dataPatient['KPI_B']);
                                        } catch (\yii\db\Exception $e) {
                                            return null;
                                        }
                                    },
                                    'headerOptions' => ['style' => 'text-align: center;'],
                                    'contentOptions' => ['style' => 'width:5px;text-align: center;'],
                                ],
                                            [
                                    'header' => 'EMR',
                                    'format' => 'raw',
                                    'value' => function($dataPatient) {
                                        return Html::a('EMR', Url::to(['/ckd/emr/index', 'ptlink'=>$dataPatient['ptlink']]),['target'=>'_blank',"data-pjax"=>"0"]);
                                    },
                                    'headerOptions' => ['style' => 'text-align: center;'],
                                    'contentOptions' => ['style' => 'width:5px;text-align: center;'],
                                ],
                            ],
                            'pjax' => true
                        ]);
                        ?>


                        <?php
                    }
                }
                $hospital = Yii::$app->request->get('hospital');
                $url = Url::to([
                            'graph',
                            'zone_select' => $selectCode[0]['zonecode'],
                            'province_select' => $selectCode[0]['provincecode'],
                            'amphur_select' => $selectCode[0]['amphurcode'],
                            'hospital_select' => $selectCode[0]['hospcode'],
                            'sitecode_select' => $selectCode[0]['hospcode'],
                            'view_render' => 'drilldown-list',
                            'report_num' => $report_num,
                            'textAB' => $textAB,
                            'kpiB' => $kpiBFilture,
                            'kpi' => $kpiAFilture
                ]);
                ?>
            </div> 
        </div>
    </div>

</div>

<?php ?>

<?php $this->registerJS("   
   

    $('.showdata').on('click', '#modal-view', function() {
        modalGuideField($(this).attr('data-url'));
    });
    

    function modalGuideField(url) {
        $('#modal-drilldown .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
        $('#modal-drilldown').modal('show')
        .find('.modal-content')
        .load(url);
    }

        $('form#add-key').on('beforeSubmit', function(e) {

               $('#spin').addClass('fa fa-spinner fa-pulse fa-1x fa-fw');
               var \$form = $(this);
               $.post(
                   \$form.attr('action'), //serialize Yii2 form
                   \$form.serialize()
               ).done(function(result) {
                   if(result.status == 'success') {
                       var urlKey = '" . $url . "';
                       urlKey = urlKey+'&page='+$('#page').val()+'&per-page='+$('#per-page').val();
                       
                     $('#modal-drilldown').find('.modal-content').load(urlKey,function(){
                        " . SDNoty::show('result.message', 'result.status') . "
//                             $('#spin').removeClass('fa fa-spinner fa-pulse fa-1x fa-fw');
//                             $('#spin').addClass('fa fa-key');
                             
                    });
//                       " . SDNoty::show('result.message', 'result.status') . "
//                       $('#spin').removeClass('fa fa-spinner fa-pulse fa-1x fa-fw');
                   } else {
                       " . SDNoty::show('result.message', 'result.status') . "
                   } 
               }).fail(function() {
                   " . SDNoty::show("'" . SDHtml::getMsgError() . "Server Error'", '"error"') . "
                   $('#spin').removeClass('fa fa-spinner fa-pulse fa-1x fa-fw');
                   console.log('server error');
               });
               return false;
           });
           
           $('#save_key').on('change',function(){
            var \$form = $('#add-key');
            $.post(
                \$form.attr('action'), //serialize Yii2 form
                \$form.serialize()
            )
            
            return false;
        });
        
        $('#convert').on('change',function(){
            var \$form = $('#add-key');
            $.post(
                \$form.attr('action'), //serialize Yii2 form
                \$form.serialize()
            )
            
            return false;
        });
    
"); ?>