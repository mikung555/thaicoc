<?php

use miloschuman\highcharts\Highcharts;
use miloschuman\highcharts\HighchartsAsset;
use kartik\grid\GridView;
use kartik\export\ExportMenu;
use yii\helpers\Html;
use yii\helpers\Url;
?> 

<div class="panel panel-info">
    <div class="panel-heading">
        <h4>เขตสุขภาพ</h4>
    </div>
    <div class="panel-body">
        <div id="showGraph">
            <?php
            Highcharts::widget($graph_config);
            ?>
        </div>
    </div>
</div>



<?php
//$h = Yii::$app->request->get();
$dataModel = $dataProvider->getModels();

$pSt1 = is_nan(round(($agv['sumAllStage1'] * 100) / $agv['sumAll'], 2)) ? 0 : round(($agv['sumAllStage1'] * 100) / $agv['sumAll'], 1);
$pSt2 = is_nan(round(($agv['sumAllStage2'] * 100) / $agv['sumAll'], 1)) ? 0.0 : round(($agv['sumAllStage2'] * 100) / $agv['sumAll'], 1);
$pSt3 = is_nan(round(($agv['sumAllStage3'] * 100) / $agv['sumAll'], 1)) ? 0.0 : round(($agv['sumAllStage3'] * 100) / $agv['sumAll'], 1);
$pSt4 = is_nan(round(($agv['sumAllStage4'] * 100) / $agv['sumAll'], 1)) ? 0.0 : round(($agv['sumAllStage4'] * 100) / $agv['sumAll'], 1);
$pSt5 = is_nan(round(($agv['sumAllStage5'] * 100) / $agv['sumAll'], 1)) ? 0.0 : round(($agv['sumAllStage5'] * 100) / $agv['sumAll'], 1);


$gridColumns = [
        [
//        'attribute' => 'zone',
        'label' => $titleColumn == '' ? 'เขตสุขภาพ' : $titleColumn,
        'format' => 'raw',
        'value' => function($model, $lavel) {
            return Html::a($model['zone'], "javascript:void(0)", [
                        'id' => 'btn-drilldown',
                        'name' => $model['zone'],
                        'onclick' => "modalDrilldown('" . Url::to([
                            'get-graph',
                            'drilldown' => '1',
                            'page_number' => $model['page_number'],
                            'zone' => $model['zonecode'],
                            'province' => $model['provincecode'],
                            'amphur' => $model['amphurcode'],
                            'hospital' => $model['hospcode'],
                            'lavel' => $model['lavel'],
                            'stage' => 'ALL'
                        ]) . "')",
//                        'class' => 'btn-link',
                        'style' => 'text-align:justify'
            ]);
        },
        'pageSummary' => 'รวม',
//        'contentOptions' => ['style' => 'text-align:justify'],
//        'headerOptions' => ['style' => 'width:30%']
    ],
        [
        'attribute' => 'count',
        'label' => 'รวมทุกStage',
        'format' => 'raw',
        'value' => function($model) {
            $url = Url::to([
                        'get-graph',
                        'drilldown' => '1',
                        'page_number' => $model['page_number'],
                        'zone' => $model['zonecode'],
                        'province' => $model['provincecode'],
                        'amphur' => $model['amphurcode'],
                        'hospital' => $model['hospcode'],
                        'lavel' => $model['lavel'],
                        'stage' => 'ALL'
            ]);

            if ($model['lavel'] == '5') {
                return Html::a(number_format($model['count']), 'javascript:void(0)', [
                            'id' => 'btn-drilldown',
                            'name' => $model['zone'],
                            'onclick' => "modalDrilldown('" . $url . "')",
//                            'class' => 'btn-link',
                            'style' => 'text-align:justify'
                ]);
            } else {
                return number_format($model['count']);
            }
        },
        'pageSummary' => "<center>" . number_format($agv['sumAll']) . "</center>",
        'headerOptions' => ['style' => 'text-align:center'],
        'contentOptions' => ['style' => 'text-align:center'],
    ],
        [
        'attribute' => 'stage1',
        'label' => 'Stage1',
        'format' => 'raw',
        'value' => function($model) {
            $url = Url::to([
                        'get-graph',
                        'drilldown' => '1',
                        'page_number' => $model['page_number'],
                        'zone' => $model['zonecode'],
                        'province' => $model['provincecode'],
                        'amphur' => $model['amphurcode'],
                        'hospital' => $model['hospcode'],
                        'lavel' => $model['lavel'],
                        'stage' => 'N181'
            ]);

            if ($model['lavel'] == '5') {
                return Html::a(number_format($model['stage1']), 'javascript:void(0)', [
                            'id' => 'btn-drilldown',
                            'name' => $model['zone'],
                            'onclick' => "modalDrilldown('" . $url . "')",
//                            'class' => 'btn-link',
                            'style' => 'text-align:justify'
                ]);
            } else {
                return number_format($model['stage1']);
            }
        },
        'pageSummary' => "<center>" . number_format($agv['sumAllStage1'] ). "</center>",
        'headerOptions' => ['style' => 'text-align:center'],
        'contentOptions' => ['style' => 'text-align:center'],
    ],
        [
        'attribute' => 'p_stage1',
        'label' => 'ร้อยละ',
        'format' => 'raw',
        'value' => function($model) {
            $url = Url::to([
                        'get-graph',
                        'drilldown' => '1',
                        'page_number' => $model['page_number'],
                        'zone' => $model['zonecode'],
                        'province' => $model['provincecode'],
                        'amphur' => $model['amphurcode'],
                        'hospital' => $model['hospcode'],
                        'lavel' => $model['lavel'],
                        'stage' => 'N181'
            ]);

            if ($model['lavel'] == '5') {
                return Html::a(number_format($model['p_stage1']), 'javascript:void(0)', [
                            'id' => 'btn-drilldown',
                            'name' => $model['zone'],
                            'onclick' => "modalDrilldown('" . $url . "')",
//                            'class' => 'btn-link',
                            'style' => 'text-align:justify'
                ]);
            } else {
                return number_format($model['p_stage1']);
            }
        },
        'pageSummary' => "<center>".number_format($pSt1)."</center>",
        'headerOptions' => ['style' => 'text-align:center'],
        'contentOptions' => ['style' => 'text-align:center'],
    ],
        [
        'attribute' => 'stage2',
        'label' => 'Stage2',
        'format' => 'raw',
        'value' => function($model) {
            $url = Url::to([
                        'get-graph',
                        'drilldown' => '1',
                        'page_number' => $model['page_number'],
                        'zone' => $model['zonecode'],
                        'province' => $model['provincecode'],
                        'amphur' => $model['amphurcode'],
                        'hospital' => $model['hospcode'],
                        'lavel' => $model['lavel'],
                        'stage' => 'N182'
            ]);

            if ($model['lavel'] == '5') {
                return Html::a(number_format($model['stage2']), 'javascript:void(0)', [
                            'id' => 'btn-drilldown',
                            'name' => $model['zone'],
                            'onclick' => "modalDrilldown('" . $url . "')",
//                            'class' => 'btn-link',
                            'style' => 'text-align:justify'
                ]);
            } else {
                return number_format($model['stage2']);
            }
        },
        'pageSummary' => "<center>" . number_format($agv['sumAllStage2'] ). "</center>",
        'headerOptions' => ['style' => 'text-align:center'],
        'contentOptions' => ['style' => 'text-align:center'],
    ],
        [
        'attribute' => 'p_stage2',
        'label' => 'ร้อยละ',
        'format' => 'raw',
        'value' => function($model) {
            $url = Url::to([
                        'get-graph',
                        'drilldown' => '1',
                        'page_number' => $model['page_number'],
                        'zone' => $model['zonecode'],
                        'province' => $model['provincecode'],
                        'amphur' => $model['amphurcode'],
                        'hospital' => $model['hospcode'],
                        'lavel' => $model['lavel'],
                        'stage' => 'N182'
            ]);

            if ($model['lavel'] == '5') {
                return Html::a(number_format($model['p_stage2']), 'javascript:void(0)', [
                            'id' => 'btn-drilldown',
                            'name' => $model['zone'],
                            'onclick' => "modalDrilldown('" . $url . "')",
//                            'class' => 'btn-link',
                            'style' => 'text-align:justify'
                ]);
            } else {
                return number_format($model['p_stage2']);
            }
        },
        'pageSummary' => "<center>".number_format($pSt2)."</center>",
        'headerOptions' => ['style' => 'text-align:center'],
        'contentOptions' => ['style' => 'text-align:center'],
    ],
        [
        'attribute' => 'stage3',
        'label' => 'Stage3',
        'format' => 'raw',
        'value' => function($model) {
            $url = Url::to([
                        'get-graph',
                        'drilldown' => '1',
                        'page_number' => $model['page_number'],
                        'zone' => $model['zonecode'],
                        'province' => $model['provincecode'],
                        'amphur' => $model['amphurcode'],
                        'hospital' => $model['hospcode'],
                        'lavel' => $model['lavel'],
                        'stage' => 'N183',
            ]);

            if ($model['lavel'] == '5') {
                return Html::a(number_format($model['stage3']), 'javascript:void(0)', [
                            'id' => 'btn-drilldown',
                            'name' => $model['zone'],
                            'onclick' => "modalDrilldown('" . $url . "')",
//                            'class' => 'btn-link',
                            'style' => 'text-align:justify'
                ]);
            } else {
                return number_format($model['stage3']);
            }
        },
        'pageSummary' => "<center>" . number_format($agv['sumAllStage3']) . "</center>",
        'headerOptions' => ['style' => 'text-align:center'],
        'contentOptions' => ['style' => 'text-align:center'],
    ],
        [
        'attribute' => 'p_stage3',
        'label' => 'ร้อยละ',
        'format' => 'raw',
        'value' => function($model) {
            $url = Url::to([
                        'get-graph',
                        'drilldown' => '1',
                        'page_number' => $model['page_number'],
                        'zone' => $model['zonecode'],
                        'province' => $model['provincecode'],
                        'amphur' => $model['amphurcode'],
                        'hospital' => $model['hospcode'],
                        'lavel' => $model['lavel'],
                        'stage' => 'N183'
            ]);

            if ($model['lavel'] == '5') {
                return Html::a(number_format($model['p_stage3']), 'javascript:void(0)', [
                            'id' => 'btn-drilldown',
                            'name' => $model['zone'],
                            'onclick' => "modalDrilldown('" . $url . "')",
//                            'class' => 'btn-link',
                            'style' => 'text-align:justify'
                ]);
            } else {
                return $model['p_stage3'];
            }
        },
        'pageSummary' => "<center>".number_format($pSt3)."</center>",
        'headerOptions' => ['style' => 'text-align:center'],
        'contentOptions' => ['style' => 'text-align:center'],
    ],
        [
        'attribute' => 'stage4',
        'label' => 'Stage4',
        'format' => 'raw',
        'value' => function($model) {
            $url = Url::to([
                        'get-graph',
                        'drilldown' => '1',
                        'page_number' => $model['page_number'],
                        'zone' => $model['zonecode'],
                        'province' => $model['provincecode'],
                        'amphur' => $model['amphurcode'],
                        'hospital' => $model['hospcode'],
                        'lavel' => $model['lavel'],
                        'stage' => 'N184'
            ]);

            if ($model['lavel'] == '5') {
                return Html::a(number_format($model['stage4']), 'javascript:void(0)', [
                            'id' => 'btn-drilldown',
                            'name' => $model['zone'],
                            'onclick' => "modalDrilldown('" . $url . "')",
//                            'class' => 'btn-link',
                            'style' => 'text-align:justify'
                ]);
            } else {
                return number_format($model['stage4']);
            }
        },
        'pageSummary' => "<center>".number_format($agv['sumAllStage4'])."<center>",
        'headerOptions' => ['style' => 'text-align:center'],
        'contentOptions' => ['style' => 'text-align:center'],
    ],
        [
        'attribute' => 'p_stage4',
        'label' => 'ร้อยละ',
        'format' => 'raw',
        'value' => function($model) {
            $url = Url::to([
                        'get-graph',
                        'drilldown' => '1',
                        'page_number' => $model['page_number'],
                        'zone' => $model['zonecode'],
                        'province' => $model['provincecode'],
                        'amphur' => $model['amphurcode'],
                        'hospital' => $model['hospcode'],
                        'lavel' => $model['lavel'],
                        'stage' => 'N184'
            ]);

            if ($model['lavel'] == '5') {
                return Html::a(number_format($model['p_stage4']), 'javascript:void(0)', [
                            'id' => 'btn-drilldown',
                            'name' => $model['zone'],
                            'onclick' => "modalDrilldown('" . $url . "')",
//                            'class' => 'btn-link',
                            'style' => 'text-align:justify'
                ]);
            } else {
                return number_format($model['p_stage4']);
            }
        },
        'pageSummary' => "<center>".number_format($pSt4)."</center>",
        'headerOptions' => ['style' => 'text-align:center'],
        'contentOptions' => ['style' => 'text-align:center'],
    ],
        [
        'attribute' => 'stage5',
        'label' => 'Stage5',
        'format' => 'raw',
        'value' => function($model) {
            $url = Url::to([
                        'get-graph',
                        'drilldown' => '1',
                        'page_number' => $model['page_number'],
                        'zone' => $model['zonecode'],
                        'province' => $model['provincecode'],
                        'amphur' => $model['amphurcode'],
                        'hospital' => $model['hospcode'],
                        'lavel' => $model['lavel'],
                        'stage' => 'N185'
            ]);

            if ($model['lavel'] == '5') {
                return Html::a(number_format($model['stage5']), 'javascript:void(0)', [
                            'id' => 'btn-drilldown',
                            'name' => $model['zone'],
                            'onclick' => "modalDrilldown('" . $url . "')",
//                            'class' => 'btn-link',
                            'style' => 'text-align:justify'
                ]);
            } else {
                return number_format($model['stage5']);
            }
        },
        'pageSummary' => "<center>".number_format($agv['sumAllStage5'])."<center>",
        'headerOptions' => ['style' => 'text-align:center'],
        'contentOptions' => ['style' => 'text-align:center'],
    ],
        [
        'attribute' => 'p_stage5',
        'label' => 'ร้อยละ',
        'format' => 'raw',
        'value' => function($model) {
            $url = Url::to([
                        'get-graph',
                        'drilldown' => '1',
                        'page_number' => $model['page_number'],
                        'zone' => $model['zonecode'],
                        'province' => $model['provincecode'],
                        'amphur' => $model['amphurcode'],
                        'hospital' => $model['hospcode'],
                        'lavel' => $model['lavel'],
                        'stage' => 'N185'
            ]);

            if ($model['lavel'] == '5') {
                return Html::a(number_format($model['p_stage5']), '#', [
                            'id' => 'btn-drilldown',
                            'name' => $model['zone'],
                            'onclick' => "modalDrilldown('" . $url . "')",
//                            'class' => 'btn-link',
                            'style' => 'text-align:justify'
                ]);
            } else {
                return number_format($model['p_stage5']);
            }
        },
        'pageSummary' => "<center>".number_format($pSt5)."</center>",
        'headerOptions' => ['style' => 'text-align:center'],
        'contentOptions' => ['style' => 'text-align:center'],
    ],
];



echo $grid = GridView::widget([
//                'class' => 'ta'
    'dataProvider' => $dataProvider,
    'layout' => '{items}{pager}',
    'columns' => $gridColumns,
    'summary' => false,
    'pjax' => true,
    'toolbar' => [
//            ['content' =>
//            'จำนวนผู้ป่วยโรคไตเรื้อรังที่มารับบริการที่โรงพยาบาล จำแนกตาม Stage'
//        ],
        '{export}',
//        '{toggleData}',
    ],
    'bordered' => $bordered,
    'striped' => false,
    'condensed' => $condensed,
    'responsive' => $responsive,
    'hover' => true,
    'panel' => [
        'type' => GridView::TYPE_SUCCESS,
        'heading' => '<h4>จำนวน' . $title . '</h4>',
    ],
    'showPageSummary' => $dataModel[0]['lavel'] == '5' ? false : true,
    'persistResize' => false,
]);
?>




<?php
$this->registerJs("
    
         function modalDrilldownPatient(url){
         
            $('#modal-drilldown .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
            $('#modal-drilldown').modal('show')
            .find('.modal-content')
            .load(url);
            return false;
        } 

    function modalDrilldown(url){
        if($('#lavel').val() == 5){
            modalDrilldownPatient(url+'&year='+$('#year').val());
        }else{
            getLoading();
            $.ajax({
               url:url+'&year='+$('#year').val(),
               method:'POST',
   //            data:$('#formSearch').serialize(),
               dataType:'HTML',
               success:function(result){
                  console.log(result);
                  $('#showData').html(result);
               },error: function (xhr, ajaxOptions, thrownError) {
                  console.log(xhr);
               }
           });
        }
        return false;
           
    }
    function getLoading(){
        var _html ='';
        _html += \"<div class='panel panel-info'>\";
        _html +=    \"<div class='panel-heading'>\";
                 _html +=       \"<h4>เขตสุขภาพ</h4>\";
                _html +=    \"</div>\";
                _html +=    \"<div class='panel-body'>\";
                _html +=        \"<div id='showGraph'>\";
                          
                _html +=        \"</div>\";
                 _html +=   \"</div>\";
                _html += \"</div>\";

               _html +=\" <div class='panel panel-success'>\";
                 _html +=\"   <div class='panel-heading'>\";
                _html +=        \"<h4>จำนวนผู้ป่วยโรคไตเรื้อรังที่มารับบริการที่โรงพยาบาล จำแนกตาม Stage</h4>\"
                _html +=\"    </div>\";
                _html +=\"    <div class='panel-body'>\";
                 _html +=\"       <div id='showTable'>\";

                    _html +=\"    </div>\";
                 _html +=\"   </div>\";
               _html +=\" </div>\";
               
        $('#showData').html(_html);
        $('#showGraph').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
        $('#showTable').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
//        $('#dateCalculate').html('กำลังประมวลผล  กรุณารอซักครู่...');
    }
        
        ");
?>

