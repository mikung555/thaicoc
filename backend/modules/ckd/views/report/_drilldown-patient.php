<?php

use kartik\grid\GridView;
use kartik\export\ExportMenu;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use appxq\sdii\helpers\SDNoty;
use appxq\sdii\helpers\SDHtml;


?>

<div >
    <?php
    $h = Yii::$app->request->get('hospital');

//     $btnBack = Html::button("<i class='glyphicon glyphicon-arrow-left'></i> ย้อนกลับ", [
//                'id' => 'btn-back',
//                'onClick' => "backDrilldown('" . Url::to($urlBackDrilldown) . "')",
//                'class' => 'btn btn-primary',
//                'style' => 'margin-right:5px;'
//    ]);
   
//\appxq\sdii\utils\VarDumper::dump($h);
    if (Yii::$app->user->identity->userProfile->sitecode == $h) {
        ?>

        <div class="panel panel-success" style="padding: 10px 10px 10px 10px;">
            <div class="panel-heading">
                <h4><button type="button" class="close" data-dismiss="modal">&times;</button>
                    <br/>จำนวน<?= $title ?></h4>
            </div>
            <div class="panel-body">
                <div style="margin:10px 10px 10px 10px;" >

                    <?php
                    $form = ActiveForm::begin([
                                'id' => 'add-key',
                                'action' => Url::to(['/ckd/report/add-key'])
                    ]);
                    ?>

                    <?php
//                    Html::button("<i class='glyphicon glyphicon-arrow-left'></i> ย้อนกลับ", [
//                        'id' => 'btn-back',
//                        'onClick' => "backDrilldown('" . Url::to($urlBackDrilldown) . "')",
//                        'class' => 'btn btn-primary',
//                        'style' => 'margin-right:5px;'
//                    ]);
                    ?>
                    <hr>
                    <label>กุญแจถอดรหัสข้อมูล</label><br/>

                    <?=
                    Html::passwordInput('key_ckd', Yii::$app->session['key_ckd_report'] ? Yii::$app->session['key_ckd_report'] : $key_ckd_report, [
                        'class' => 'form-control inline',
                        'style' => 'width:40%',
                        'placeholder' => 'กรุณากรอกกุญแจถอดรหัส'
                    ]);
                    ?>

                    <?=
                    Html::submitButton("<i class='fa fa-key' id='spin'></i> ถอดรหัส", [
                        'id' => 'btn-key',
                        'class' => 'btn btn-success',
                        'style' => 'margin-left:5px;'
                    ]);
                    ?>

                    <br><br>
                    <?= Html::checkbox('save_key', Yii::$app->session['save_ckd_key'] != '' ? true : false, ['label' => 'จดจำคีย์นี้', 'id' => 'save_key']) ?>
                    <?= Html::checkbox('convert', Yii::$app->session['convert_ckd_char'] != '' ? true : false, ['label' => 'เข้ารหัสแบบ tis620 (กรณีชื่อ-สกุล อ่านไม่ออกให้กาช่องนี้ด้วย)', 'id' => 'convert']) ?>



                    <?php ActiveForm::end(); ?>
                    <div class="clearfix"></div>
                    <hr>

                    <?=
                    GridView::widget([
                        'dataProvider' => $dataStage[$dataStage['stage']->allModels],
                        'layout' => '{items}<center>{pager}</center>',
                        'summary' => false,
                        'columns' => [
                                [
                                'attribute' => 'pid',
                                'label' => 'HN',
                                'value' => function($model, $urlBackDrilldown, $i = 1) {
                                    if ($i == 1) {
                                        echo Html::hiddenInput('page', $_GET['page'], ['id' => 'page']);
                                        echo Html::hiddenInput('per-page', $_GET['per-page'], ['id' => 'per-page']);
                                    }

                                    return $model['pid'];
                                }
                            ],
                                    [
                                'attribute' => 'cid',
                                'label' => 'เลขบัตรประชาชน'
                            ],
                                [
                                'attribute' => 'fname',
                                'label' => 'ชื่อ'
                            ],
                                [
                                'attribute' => 'lname',
                                'label' => 'นามสกุล'
                            ],
                                [
                                'attribute' => 'stage',
                                'label' => 'Stage'
                            ]
                        ],
                        'pjax' => true,
                        'toolbar' => [
                        ],
                        'bordered' => $bordered,
                        'striped' => false,
                        'condensed' => $condensed,
                        'responsive' => true,
                        'hover' => true,
//                    'showPageSummary' => true,
                        'persistResize' => false,
                    ]);
                    ?>
                </div>
            </div>
        </div>

        <?php
    } else {
        ?>


        <div class="panel panel-success" style="padding: 10px 10px 10px 10px;">
            <div class="panel-heading">
                <h4><button type="button" class="close" data-dismiss="modal">&times;</button>
                    <br/>จำนวน<?= $title ?></h4>

            </div>
            <div class="panel-body">
                <div style="margin:10px 10px 10px 10px;" >
                    <?php
                    echo $btnBack . " <div class='clearfix'></div><hr>";
                    echo "<center><label class='alert alert-danger' style='padding-top:10px;'>คุณไม่มีสิทธิ์ในการดูข้อมูล</label></center><div class='clearfix'></div><hr>";
                    ?>
                </div>
            </div>
        </div>
    <?php } ?>
</div>




<?php
$urlBackDrilldown['lavel'] = '6';
$urlBackDrilldown['stage'] = $dataStage['stage']->allModels;
$url = Url::to($urlBackDrilldown);
$this->registerJs("
        
            
//         function backDrilldown(url){
//            $('#modal-drilldown .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
//            $('#modal-drilldown')
//            .find('.modal-content')
//            .load(url);
//        }      
        function backDrilldown(url){
         getLoading();
         $.ajax({
            url:url,
            method:'POST',
//            data:$('#formSearch').serialize(),
            dataType:'HTML',
            success:function(result){
               console.log(result);
               $('#showData').html(result);
//               $('#dateCalculate').html($('#dateCal').val());
            },error: function (xhr, ajaxOptions, thrownError) {
               console.log(xhr);
            }
        });
        return false;
    }
    
    function getLoading(){
        var _html ='';
        _html += \"<div class='panel panel-info'>\";
        _html +=    \"<div class='panel-heading'>\";
                 _html +=       \"<h4>เขตสุขภาพ</h4>\";
                _html +=    \"</div>\";
                _html +=    \"<div class='panel-body'>\";
                _html +=        \"<div id='showGraph'>\";
                          
                _html +=        \"</div>\";
                 _html +=   \"</div>\";
                _html += \"</div>\";

               _html +=\" <div class='panel panel-success'>\";
                 _html +=\"   <div class='panel-heading'>\";
                _html +=        \"<h4>จำนวนผู้ป่วยโรคไตเรื้อรังที่มารับบริการที่โรงพยาบาล จำแนกตาม Stage</h4>\"
                _html +=\"    </div>\";
                _html +=\"    <div class='panel-body'>\";
                 _html +=\"       <div id='showTable'>\";

                    _html +=\"    </div>\";
                 _html +=\"   </div>\";
               _html +=\" </div>\";
               
        $('#showData').html(_html);
        $('#showGraph').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
        $('#showTable').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
//        $('#dateCalculate').html('กำลังประมวลผล  กรุณารอซักครู่...');
    }
        
        
        $('form#add-key').on('beforeSubmit', function(e) {
       
            $('#spin').addClass('fa fa-spinner fa-pulse fa-1x fa-fw');
            var \$form = $(this);
            $.post(
                \$form.attr('action'), //serialize Yii2 form
                \$form.serialize()
            ).done(function(result) {
                if(result.status == 'success') {
                    var urlKey = '" . $url . "';
                    urlKey = urlKey+'&page='+$('#page').val()+'&per-page='+$('#per-page').val();
                    $('#modal-drilldown')
                    .find('.modal-content')
                    .load(urlKey,function(){
                        " . SDNoty::show('result.message', 'result.status') . "
                             $('#spin').removeClass('fa fa-spinner fa-pulse fa-1x fa-fw');
                             $('#spin').addClass('fa fa-key');
                             
                    });
                    
                } else {
                    " . SDNoty::show('result.message', 'result.status') . "
                } 
            }).fail(function() {
                " . SDNoty::show("'" . SDHtml::getMsgError() . "Server Error'", '"error"') . "
                $('#spin').removeClass('fa fa-spinner fa-pulse fa-1x fa-fw');
                console.log('server error');
            });
            return false;
        });
        
        $('#save_key').on('change',function(){
            var \$form = $('#add-key');
            $.post(
                \$form.attr('action'), //serialize Yii2 form
                \$form.serialize()
            )
            
            return false;
        });
        
        $('#convert').on('change',function(){
            var \$form = $('#add-key');
            $.post(
                \$form.attr('action'), //serialize Yii2 form
                \$form.serialize()
            )
            
            return false;
        });
        
        ");
?>