<?php

use kartik\tabs\TabsX;
use yii\helpers\Url;

/* @var $this yii\web\View */
$this->title = Yii::t('backend', 'Module: CKDNET');
$this->params['breadcrumbs'][] = ['label' => 'Modules', 'url' => Url::to('/tccbots/my-module')];
$this->params['breadcrumbs'][] = Yii::t('backend', 'รายงาน KPI');

$kpiSum[] = $result['kpi1B']==0?0:($result['kpi1B']/$result['kpi1A']) * 100;
$kpiSum[] = $result['kpi2B']==0?0:($result['kpi2B']/$result['kpi2A']) * 100;
$kpiSum[] = $result['kpi3B']==0?0:($result['kpi3B']/$result['kpi3A']) * 100;
$kpiSum[] = $result['kpi4B']==0?0:($result['kpi4B']/$result['kpi4A']) * 100;
$kpiSum[] = $result['kpi5B']==0?0:($result['kpi5B']/$result['kpi5A']) * 100;
$kpiSum[] = $result['kpi6B']==0?0:($result['kpi6B']/$result['kpi6A']) * 100;
$kpiSum[] = $result['kpi7B']==0?0:($result['kpi7B']/$result['kpi7A']) * 100;
$kpiSum[] = $result['kpi8B']==0?0:($result['kpi8B']/$result['kpi8A']) * 100;
$kpiSum[] = $result['kpi9B']==0?0:($result['kpi9B']/$result['kpi9A']) * 100;
$kpiSum[] = $result['kpi10B']==0?0:($result['kpi10B']/$result['kpi10A']) * 100;
$kpiSum[] = $result['kpi11B']==0?0:($result['kpi11B']/$result['kpi11A']) * 100;
$kpiSum[] = $result['kpi12B']==0?0:($result['kpi12B']/$result['kpi12A']) * 100;
$kpiSum[] = $result['kpi13B']==0?0:($result['kpi13B']/$result['kpi13A']) * 100;
$kpiSum[] = $result['kpi14B']==0?0:($result['kpi14B']/$result['kpi14A']) * 100;
$kpiSum[] = $result['kpi15B']==0?0:($result['kpi15B']/$result['kpi15A']) * 100;

$kpi[]=90;
$kpi[]=0;
$kpi[]=80;
$kpi[]=60;
$kpi[]=65;
$kpi[]=60;
$kpi[]=40;
$kpi[]=60;
$kpi[]=80;
$kpi[]=80;
$kpi[]=80;
$kpi[]=40;
$kpi[]=40;
$kpi[]=50;
$kpi[]=50;

$urlReport[0]['url']= ['report-kpi', 'report_num' => \yii\helpers\Html::encode(1)];
$urlReport[0]['label']= "1.KPI CKD 1.1 ร้อยละของผู้ป่วย DM และ/หรือ HT ที่ได้รับการค้นหาและคัดกรองโรคไตเรื้อรัง";

$urlReport[1]['url']= ['report-kpi', 'report_num' => \yii\helpers\Html::encode(2)];
$urlReport[1]['label']= "2.KPI CKD 1.2  ร้อยละของผู้ป่วย DM, HT เป็นโรคไตเรื้อรังรายใหม่ (ในปีงบประมาณ) (พิจารณาเฉพาะผู้ป่วยที่วันที่ได้รับการคัดกรองเกิดก่อนวันที่ได้รับการวินิจฉัยว่าเป็น CKD)";

$urlReport[2]['url']= ['report-kpi', 'report_num' => \yii\helpers\Html::encode(3)];
$urlReport[2]['label']= "3.KPI CKD 2.1 การชะลอความเสื่อมของไต ผู้ป่วย CKD ในเขตรับผิดชอบ ที่ตรวจวัด BP &lt; 140/90 mmHg";

$urlReport[3]['url']= ['report-kpi', 'report_num' => \yii\helpers\Html::encode(4)];
$urlReport[3]['label']= "4.KPI CKD 2.2 การชะลอความเสื่อมของไต ผู้ป่วย CKD ในเขตรับผิดชอบได้รับ ACEi/ARB";

$urlReport[4]['url']= ['report-kpi', 'report_num' => \yii\helpers\Html::encode(5)];
$urlReport[4]['label']= "5.KPI CKD 2.3 การชะลอความเสื่อมของไต ผู้ป่วย CKD มีอัตราการลดลงของ eGFR &lt; 4 ml/min/1.73 m2/yr";

$urlReport[5]['url']= ['report-kpi', 'report_num' => \yii\helpers\Html::encode(6)];
$urlReport[5]['label']= "6.KPI CKD 2.4 การชะลอความเสื่อมของไต ผู้ป่วย CKD ในเขตรับผิดชอบที่ได้รับการตรวจและมีระดับ Hb &gt; 10 gm/dl หรือ Hct &gt; 33%";

$urlReport[6]['url']= ['report-kpi', 'report_num' => \yii\helpers\Html::encode(7)];
$urlReport[6]['label']= "7.KPI CKD 2.5 การชะลอความเสื่อมของไต ผู้ป่วย CKD (เฉพาะที่มีเบาหวานร่วม)ที่มารับบริการในเขตรับผิดชอบได้รับการตรวจ HbA1c และมีค่าผลการตรวจตั้งแต่ 6.5% ถึง 7.5%";

$urlReport[7]['url']= ['report-kpi', 'report_num' => \yii\helpers\Html::encode(8)];
$urlReport[7]['label']= "8.KPI CKD 2.6 การชะลอความเสื่อมของไต ผู้ป่วย CKD กลุ่มเสี่ยงต่อโรคหลอดเลือดและหัวใจได้รับยากลุ่ม Statin";

$urlReport[8]['url']= ['report-kpi', 'report_num' => \yii\helpers\Html::encode(9)];
$urlReport[8]['label']= "9.KPI CKD 2.7 การชะลอความเสื่อมของไต ผู้ป่วย CKD ได้รับการตรวจ serum K และมีค่าผลการตรวจ &lt; 5.5 mEq/L";

$urlReport[9]['url']= ['report-kpi', 'report_num' => \yii\helpers\Html::encode(10)];
$urlReport[9]['label']= "10.KPI CKD 2.8 การชะลอความเสื่อมของไต ผู้ป่วย CKD ได้รับการตรวจ serum HCO3 และมีค่าผลตรวจ &gt; 22 mEq/L";

$urlReport[10]['url']= ['report-kpi', 'report_num' => \yii\helpers\Html::encode(11)];
$urlReport[10]['label']= "11.KPI CKD 2.9 การชะลอความเสื่อมของไต ผู้ป่วย CKD ได้รับการตรวจ urine protein";

$urlReport[11]['url']= ['report-kpi', 'report_num' => \yii\helpers\Html::encode(12)];
$urlReport[11]['label']= "12.KPI CKD 2.10 การชะลอความเสื่อมของไต ผู้ป่วย CKD ได้รับการประเมิน UPCR";

$urlReport[12]['url']= ['report-kpi', 'report_num' => \yii\helpers\Html::encode(13)];
$urlReport[12]['label']= "13.KPI CKD 2.11 การชะลอความเสื่อมของไต ผู้ป่วย CKD ได้รับการประเมิน UPCR และมีผลการประเมิน &lt; 500 mg/g cr";

$urlReport[13]['url']= ['report-kpi', 'report_num' => \yii\helpers\Html::encode(14)];
$urlReport[13]['label']= "14.KPI CKD 2.12 การชะลอความเสื่อมของไต ผู้ป่วย CKD ได้รับการตรวจ Serum PO4 และมีผลการตรวจ ≤  4.6 mg%";

$urlReport[14]['url']= ['report-kpi', 'report_num' => \yii\helpers\Html::encode(15)];
$urlReport[14]['label']= "15.KPI CKD 2.13 การชะลอความเสื่อมของไต ผู้ป่วย CKD ได้รับการตรวจ Serum iPTH และผลอยู่ในเกณฑ์ที่เหมาะสม(&lt; 500)";
?>
<div class="ckdnet-default-index">

    <table class="table table-hover">
        <thead >
            <tr >
                <th style="font-size: 18px;background-color: #666666;color:white;">ชื่อรายงาน </th>
                <th style="font-size: 18px;background-color: #666666;"> <label style="text-align:center;min-width:70px;" class="label label-primary col-md-8">เป้าหมาย</label> </th>
                <th style="font-size: 18px;background-color: #666666;"> <label style="text-align:center;min-width:70px;" class="label label-warning col-md-8">ผลงาน</label> </th>
                <th style="font-size: 18px;background-color: #666666;"> <label style="text-align:center;min-width:70px;" class="label label-danger col-md-8">อัตรา</label></th>
            </tr>
        </thead>

        <tbody>
            <?php foreach ($urlReport as $key => $value){ 
                $keyKpiA = "kpi".($key+1)."A";
                $keyKpiB = "kpi".($key+1)."B";
                
                $stateKpi = $kpiSum[$key]>=$kpi[$key]?"success":"danger";
                ?>
            <tr>
                <td style="font-size: 14px; color: #56af45;">
                    <a href="<?=Url::to($value['url']) ?>"><?=$value['label']?></a>
                   
                </td>
                <td style="font-size: 18px;"><label style="text-align:center;min-width:70px;" class="label label-primary col-md-8 "><?=number_format($result[$keyKpiA])?></label></td>
                <td style="font-size: 18px;"><label style="text-align:center;min-width:70px;" class="label label-warning col-md-8 "><?=number_format($result[$keyKpiB])?></label></td>
                <td style="font-size: 18px;"><label style="text-align:center;min-width:70px;" class="label label-<?=$stateKpi?> col-md-8"><?=number_format($kpiSum[$key],2)?></label></td>
            </tr>                                        
            <?php } ?>

<!--        <tbody>
            <tr>
                <td style="font-size: 14px; color: #56af45;">
                    <a href="">16.จำนวนผู้ป่วยโรคไตเรื้อรังที่มารับบริการที่โรงพยาบาล จำแนกตาม Stage </a>                                </td>
            </tr>                                        
        </tbody>-->

        
            <tr>
                <td style="font-size: 14px; color: #56af45;">
                    <a href="<?=Url::to(['report-state','page_number'=>'17']) ?>">16.จำนวนผู้ป่วย CKD ในเขตรับผิดชอบ จำแนกตาม Stage </a>                                </td>
            </tr>                                        
       
            <tr>
                <td style="font-size: 14px; color: #56af45;">
                    <a href="<?=Url::to(['report18']) ?>">17.Stage Change ผู้ป่วย CKD จำแนกตามโรงพยาบาลที่รักษา</a>                               
                </td>
            </tr>                                        
            </tbody>
    </table>

</div>
