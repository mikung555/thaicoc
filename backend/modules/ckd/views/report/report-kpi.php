<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use kartik\widgets\DepDrop;
use yii\widgets\ActiveForm;
use kartik\widgets\Select2;
use yii\web\JsExpression;

$this->title = Yii::t('backend', 'Module: CKDNET');
$this->params['breadcrumbs'][] = ['label' => 'Modules', 'url' => Url::to('/tccbots/my-module')];
$this->params['breadcrumbs'][] = ['label' => 'รายงาน KPI', 'url' => Url::to('/ckd/report')];
$this->params['breadcrumbs'][] = Yii::t('backend', 'Report');

echo '<div style="padding-left: 10px;">';
$report_num = \yii\helpers\Html::encode($_GET['report_num']);

?>
<div class="alert alert-danger" style="font-size:25px;">
     <i class="fa fa-wrench" aria-hidden="true"></i> กำลังพัฒนาระบบ  
</div>
<?php
$itemZone = [
        ['id' => '01', 'name' => 'เขตสุขภาพที่ 1'],
        ['id' => '02', 'name' => 'เขตสุขภาพที่ 2'],
        ['id' => '03', 'name' => 'เขตสุขภาพที่ 3'],
        ['id' => '04', 'name' => 'เขตสุขภาพที่ 4'],
        ['id' => '05', 'name' => 'เขตสุขภาพที่ 5'],
        ['id' => '06', 'name' => 'เขตสุขภาพที่ 6'],
        ['id' => '07', 'name' => 'เขตสุขภาพที่ 7'],
        ['id' => '08', 'name' => 'เขตสุขภาพที่ 8'],
        ['id' => '09', 'name' => 'เขตสุขภาพที่ 9'],
        ['id' => '10', 'name' => 'เขตสุขภาพที่ 10'],
        ['id' => '11', 'name' => 'เขตสุขภาพที่ 11'],
        ['id' => '12', 'name' => 'เขตสุขภาพที่ 12'],
];
if ($report_num == 1) {
    $headText = " KPI CKD 1.1 ร้อยละของผู้ป่วย DM และ/หรือ HT ที่ได้รับการค้นหาและคัดกรองโรคไตเรื้อรัง";
} else if ($report_num == 2) {
    $headText = " KPI CKD 1.2 ร้อยละของผู้ป่วย DM, HT เป็นโรคไตเรื้อรังรายใหม่ (ในปีงบประมาณ) (พิจารณาเฉพาะผู้ป่วยที่วันที่ได้รับการคัดกรองเกิดก่อนวันที่ได้รับการวินิจฉัยว่าเป็น CKD)";
} else if ($report_num == 3) {
    $headText = " KPI CKD 2.1 การชะลอความเสื่อมของไต ผู้ป่วย CKD ที่มารับบริการ BP < 140/90 mmHg";
} else if ($report_num == 4) {
    $headText = " CKD 2.2 การชะลอความเสื่อมของไต ผู้ป่วยที่มารับบริการโรงพยาบาลได้รับ ACEi/ARB";
} else if ($report_num == 5) {
    $headText = " KPI CKD 2.3 การชะลอความเสื่อมของไต ผู้ป่วยมีอัตราการลดลงของ eGFR < 4 ml/min/1.73 m2/yr";
} else if ($report_num == 6) {
    $headText = " KPI CKD 2.4 การชะลอความเสื่อมของไต ผู้ป่วยที่มารับบริการโรงพยาบาลได้รับการตรวจและมีระดับ Hb > 10 gm/dl หรือ Hct > 33%";
} else if ($report_num == 7) {
    $headText = " KPI CKD 2.5 การชะลอความเสื่อมของไต ผู้ป่วย(เฉพาะที่มีเบาหวานร่วม)ที่มารับบริการโรงพยาบาลได้รับการตรวจ HbA1c และมีค่าผลการตรวจตั้งแต่ 6.5% ถึง 7.5%";
} else if ($report_num == 8) {
    $headText = " KPI CKD 2.6 การชะลอความเสื่อมของไต ผู้ป่วยกลุ่มเสี่ยงต่อโรคหลอดเลือดและหัวใจได้รับยากลุ่ม Statin";
} else if ($report_num == 9) {
    $headText = " KPI CKD 2.7 การชะลอความเสื่อมของไต ผู้ป่วยได้รับการตรวจ serum K และมีค่าผลการตรวจ < 5.5 mEq/L";
} else if ($report_num == 10) {
    $headText = " KPI CKD 2.8 การชะลอความเสื่อมของไต ผู้ป่วยได้รับการตรวจ serum HCO3 และมีค่าผลตรวจ > 22 mEq/L";
} else if ($report_num == 11) {
    $headText = " KPI CKD 2.9 การชะลอความเสื่อมของไต ผู้ป่วยได้รับการตรวจ urine protein";
} else if ($report_num == 12) {
    $headText = " KPI CKD 2.10 การชะลอความเสื่อมของไต ผู้ป่วยได้รับการประเมิน UPCR";
} else if ($report_num == 13) {
    $headText = " KPI CKD 2.11 การชะลอความเสื่อมของไต ผู้ป่วยได้รับการประเมิน UPCR และมีผลการประเมิน < 500 mg/g cr";
} else if ($report_num == 14) {
    $headText = " KPI CKD 2.12 การชะลอความเสื่อมของไต ผู้ป่วยได้รับการตรวจ Serum PO4 และมีผลการตรวจ ≤ 4.6 mg%";
} else if ($report_num == 15) {
    $headText = " KPI CKD 2.13 การชะลอความเสื่อมของไต ผู้ป่วยได้รับการตรวจ Serum iPTH และผลอยู่ในเกณฑ์ที่เหมาะสม(< 500)";
}

//echo "<h3>" . $headText . "</h3>";
$divControls = Html::tag(
                'div', '<br/>
            <div class="col-md-4">
            <h5 class="section-text">ปีงบประมาณ</h5>
                <select class="form-control" id="year">
                    <option value="2560">2560</option>
                    <option value="2559">2559</option>
                    <option value="2558">2558</option>
                    <option value="2557">2557</option>
                </select>
            </div>
            <div class="col-md-4" id="zone-chioce">
           <h5 class="section-text">เขต :</h5>
            <select class="form-control" id="ov01-level">
                    <option value="0">ทั้งหมด</option>
                </select>    
            </div>
        <br/><br/><br/><br/>
        
            <div class="col-md-4" >
            <h5 class="section-text">จังหวัด :</h5>
                <select class="form-control" id="province">
                    <option value="0">ทั้งหมด</option>
                </select>
            </div>
            <div class="col-md-4" id="hospital">
                <h5 class="section-text" id="label-select">โรงพยาบาล :</h5>
                 <select class="form-control" id="ov01-level">
                    <option value="0">ทั้งหมด</option>
                </select>
            </div>
        <br/><br/><br/><br/>
        <div class="col-md-2">
           <button type="button" id="kpiRenderAjax" class="btn btn-info form-control">ตกลง</button>
        </div>
            <br/><br/><br/><br/>
    ');
//echo $divControls;
?>
<div id="frm_controller">

    <?php ActiveForm::begin(['id' => 'formSearch']) ?>
    <input type="hidden" name="report_num" id="report_num" value="<?=$report_num?>">
    <?php //Html::hiddenInput('report_num', $report_num) ?>
    <?= Html::hiddenInput('view_render', 'drilldown-graph') ?>
    <input type="hidden" name="action" id="action" value="refresh">
    <br/>
    <?php
    //\appxq\sdii\utils\VarDumper::dump($report_num);
    ?>
    
    <div class="col-md-10">
        <h5 class="section-text">รายงาน KPI</h5>
        <select name="report_num" id="text_head" class="form-control" style="font-size: 16px;font-weight: bold;">
            <option value="1" <?=$report_num=='1'?'selected':''?> >KPI CKD 1.1 ร้อยละของผู้ป่วย DM และ/หรือ HT ที่ได้รับการค้นหาและคัดกรองโรคไตเรื้อรัง</option>
            <option value="2" <?=$report_num=='2'?'selected':''?> >KPI CKD 1.2 ร้อยละของผู้ป่วย DM, HT เป็นโรคไตเรื้อรังรายใหม่ (ในปีงบประมาณ) (พิจารณาเฉพาะผู้ป่วยที่วันที่ได้รับการคัดกรองเกิดก่อนวันที่ได้รับการวินิจฉัยว่าเป็น CKD)</option>
            <option value="3" <?=$report_num=='3'?'selected':''?> >KPI CKD 2.1 การชะลอความเสื่อมของไต ผู้ป่วย CKD ที่มารับบริการ BP < 140/90 mmHg</option>
            <option value="4" <?=$report_num=='4'?'selected':''?>>CKD 2.2 การชะลอความเสื่อมของไต ผู้ป่วยที่มารับบริการโรงพยาบาลได้รับ ACEi/ARB</option>
            <option value="5" <?=$report_num=='5'?'selected':''?>>KPI CKD 2.3 การชะลอความเสื่อมของไต ผู้ป่วยมีอัตราการลดลงของ eGFR < 4 ml/min/1.73 m2/yr</option>
            <option value="6" <?=$report_num=='6'?'selected':''?>>KPI CKD 2.4 การชะลอความเสื่อมของไต ผู้ป่วยที่มารับบริการโรงพยาบาลได้รับการตรวจและมีระดับ Hb > 10 gm/dl หรือ Hct > 33%</option>
            <option value="7" <?=$report_num=='7'?'selected':''?>>KPI CKD 2.5 การชะลอความเสื่อมของไต ผู้ป่วย(เฉพาะที่มีเบาหวานร่วม)ที่มารับบริการโรงพยาบาลได้รับการตรวจ HbA1c และมีค่าผลการตรวจตั้งแต่ 6.5% ถึง 7.5%</option>
            <option value="8" <?=$report_num=='8'?'selected':''?>>KPI CKD 2.6 การชะลอความเสื่อมของไต ผู้ป่วยกลุ่มเสี่ยงต่อโรคหลอดเลือดและหัวใจได้รับยากลุ่ม Statin</option>
            <option value="9" <?=$report_num=='9'?'selected':''?>>KPI CKD 2.7 การชะลอความเสื่อมของไต ผู้ป่วยได้รับการตรวจ serum K และมีค่าผลการตรวจ < 5.5 mEq/L</option>
            <option value="10" <?=$report_num=='10'?'selected':''?>>KPI CKD 2.8 การชะลอความเสื่อมของไต ผู้ป่วยได้รับการตรวจ serum HCO3 และมีค่าผลตรวจ > 22 mEq/L</option>
            <option value="11" <?=$report_num=='11'?'selected':''?>>KPI CKD 2.9 การชะลอความเสื่อมของไต ผู้ป่วยได้รับการตรวจ urine protein</option>
            <option value="12" <?=$report_num=='12'?'selected':''?>>KPI CKD 2.10 การชะลอความเสื่อมของไต ผู้ป่วยได้รับการประเมิน UPCR</option>
            <option value="13" <?=$report_num=='13'?'selected':''?>>KPI CKD 2.11 การชะลอความเสื่อมของไต ผู้ป่วยได้รับการประเมิน UPCR และมีผลการประเมิน < 500 mg/g cr</option>
            <option value="14" <?=$report_num=='14'?'selected':''?>>KPI CKD 2.12 การชะลอความเสื่อมของไต ผู้ป่วยได้รับการตรวจ Serum PO4 และมีผลการตรวจ ≤ 4.6 mg%</option>
            <option value="15" <?=$report_num=='15'?'selected':''?> >KPI CKD 2.13 การชะลอความเสื่อมของไต ผู้ป่วยได้รับการตรวจ Serum iPTH และผลอยู่ในเกณฑ์ที่เหมาะสม(< 500)</option>
            
        </select>
    </div>
    <div class="col-md-5">
        <h5 class="section-text">ปีงบประมาณ</h5>
        <?=
        Select2::widget([
            'name' => 'year',
            'id' => 'year',
//                    'hideSearch' => true,
            'value' => (date('m')>9?date("Y")+1:date("Y")),
            'data' => $itemYear,
            'options' => [
                'prompt' => '-----ทั้งหมด-----',
            ],
            'pluginOptions' => [
                'allowClear' => true,
            ],
        ]);
        ?>
    </div>
    <div class="col-md-5" id="div-zone-choice">
        <h5 class="section-text">เขต :</h5>
        <?php 
                
        echo 
        Select2::widget([
            'name' => 'zone-choice',
            'id' => 'zone-choice',
            'value' => $zone_select,
            'hideSearch' => true,
            'data' => ArrayHelper::map($itemZone, 'id', 'name'),
            'options' => [
                'prompt' => '-----ทั้งหมด-----',
            ],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]);
        ?>
    </div>
    <div class="clearfix"></div>
    <div class="col-md-5" id="div-province">
        <h5 class="section-text">จังหวัด :</h5>
        <?php 
        //\appxq\sdii\utils\VarDumper::dump($province_select.$amphur_select);
        echo
        DepDrop::widget([
            'type' => DepDrop::TYPE_SELECT2,
            'name' => 'province',
            'data' => [$province_select => 'selected'],
            'options' => ['id' => 'province'],
            'select2Options' => ['pluginOptions' => ['allowClear' => true]],
            'pluginOptions' => [
                'depends' => ['zone-choice'],
                'initialize' => true,
                'initDepends' => ['zone-choice'],
                'params' => ['zone-choice'],
                'placeholder' => '-----ทั้งหมด-----',
                'url' => '/ckd/report/get-province/',
                'allowClear' => true,
            ]
        ]);
        ?>
    </div>

    <div class="col-md-5" id="div-amphur">
        <h5 class="section-text" id="label-select">อำเภอ :</h5>
        <?php
        
        echo DepDrop::widget([
            'type' => DepDrop::TYPE_SELECT2,
            'name' => 'amphur',
            'data' => [$province_select.$amphur_select => 'selected'],
            'options' => ['id' => 'amphur'],
            'select2Options' => ['pluginOptions' => ['allowClear' => true]],
            'pluginOptions' => [
                'depends' => ['province'],
                'initialize' => true,
                'initDepends' => ['province'],
                
                'placeholder' => '-----ทั้งหมด-----',
                'url' => Url::to(['/ckd/report/get-amphur', 'code'=>$province_select.$amphur_select]),
                'params' => ['province'],
                'allowClear' => true,
            ]
        ]);
        ?>
    </div>

    <div class="col-md-5" id="div-hospital">
        <h5 class="section-text" id="label-select">โรงพยาบาล :</h5>
        <?php
//        echo DepDrop::widget([
//            'type' => DepDrop::TYPE_SELECT2,
//            'name' => 'hospital',
//            'id' => 'hospital',
//            'select2Options' => ['pluginOptions' => ['allowClear' => true]],
//            'pluginOptions' => [
//                'depends' => ['amphur'],
//                'initialize' => true,
//                'initDepends' => ['amphur'],
//                'params' => ['province', 'amphur'],
//                'placeholder' => '-----ทั้งหมด-----',
//                'url' => '/ckd/report/get-hospital',
//            ]
//        ]);
        ?>
        <?php
        echo Select2::widget([
                'name' => 'hospital',
                'id' => 'hospital',
                'options' => ['placeholder' => 'เลือกหน่วยงาน...'],
                'pluginOptions' => [
                    'allowClear' => true,
                    'minimumInputLength' => 0, //ต้องพิมพ์อย่างน้อย 3 อักษร ajax จึงจะทำงาน
                    'ajax' => [
                        'url' => '/ckd/report/get-hospital2',
                        'dataType' => 'json', //รูปแบบการอ่านคือ json
                        'data' => new JsExpression('function(params) { 
                            var codes = $("#amphur").val();
                            var zone = $("#zone-choice").val();
                            var amphur =codes.substring(2,4);
                            var province =$("#province").val();
                            //console.log(zonecode+" "+amphur+" "+province);
                            return {q:params.term, zone, province, amphur}; 
                            }
                         '),
                    ],
                    'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                    'templateResult' => new JsExpression('function(city) { return city.text; }'),
                    'templateSelection' => new JsExpression('function (city) { return city.text; }'),
                ]
        ]);
        ?>
    </div>
    <br/><br/><br/><br/><br/><br/><br/><br/>
    <div class="col-md-5">
        <button type="button" id="searchData" class="btn btn-primary"><i class="glyphicon glyphicon-search"></i> ตกลง</button>
    </div>
    <div class="clearfix"></div>
    <?php ActiveForm::end() ?>
</div>
<br/> 
<br/> 
<?php
?>
<div style="padding-left: 10px; padding-right: 10px;">
    <div id="showData">
        <div class="panel panel-info">
            <div class="panel-heading">
                <label style="font-size: 18px;" id="headText"><?= $headText . " ปีงบประมาณ " ?></label> <label style="font-size: 18px;" id='yearSelect'></label>
            </div>
            <div class="panel-body">
                <div id="graph-show">
                    <div id="drilldown-guage-show"class="col-md-2"  style="min-width: 260px; max-width: 400px; height: 260px; margin: 0 auto"></div>
                    <div class='col-md-9' id='drilldown-graph-show'></div><br/>
                </div>
            </div>
        </div>

    </div>
</div>
<?php
$this->registerJs("
          $(document).ready(function(){
          
//        $('#tumbon').attr('disabled','disabled');
//        $('#div-network-service').hide();
        $('#drilldown-guage-show').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
        $('#drilldown-graph-show').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
        
        getGraph('refresh');
        
    });
     $('#searchData').on('click',function(){
        $('#headText').html($('#text_head option:selected').text()+' '+'ปีงบประมาณ ');
        $('#drilldown-guage-show').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
        $('#drilldown-graph-show').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
        getGraph('search');
     });
    
    function getGraph(act){
        $('#yearSelect').html((parseInt($('#year').val())+543));
        $('#action').val(act);
         $.ajax({
            url:'" . Url::to(['report/graph']) . "',
            method:'POST',
            data:$('#formSearch').serialize(),
            dataType:'HTML',
            success:function(result){
               //console.log(result);
               $('#graph-show').html(result);
            },error: function (xhr, ajaxOptions, thrownError) {
               console.log(xhr);
            }
        });
        return false;
    }
      $('#text_head').on('change', function(e){
        
       //var search = document.location.search;
        //var param = search.split('=');
        //document.location.search = param[0]+'='+$('#text_head').val();
        
        //$('#report_num').val($('#text_head').val());
        //$('#drilldown-guage-show').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
        //$('#drilldown-graph-show').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
        //getGraph();
      });
           
     ");
?>

