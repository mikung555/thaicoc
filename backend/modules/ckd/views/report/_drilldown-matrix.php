<?php

use kartik\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use appxq\sdii\helpers\SDNoty;
use appxq\sdii\helpers\SDHtml;
?>

<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title" id="itemModalLabel"><?php echo Html::tag('label', $htxt); ?></h4>
</div>
<div class="modal-body">
    <?php if ($zone != '') { ?>
        <div class="row">
            <div class="col-md-12">
                <?php
                echo Html::button("<i class='glyphicon glyphicon-arrow-left'></i> ย้อนกลับ", [
                    'id' => 'btn-back',
                    'onClick' => "backDrilldown('" . Url::to($urlBackDrilldown) . "')",
                    'class' => 'btn btn-primary',
                    'style' => 'margin-right:5px;'
                ]);
                ?>

            </div>
        </div>
        <hr>
    <?php } ?>
    <div class="row">
        <div class="col-md-12">
            <?php
            if ($zone == NULL) {
                echo GridView::widget([
                    'dataProvider' => $dataprovider,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                        [
                            //  'attribute' => 'zone',
                            'label' => 'เขตสุขภาพ',
                            'format' => 'raw',
                            'value' => function($dataprovider) {
                                return Html::a('เขตสุขภาพ ' . $dataprovider['zone'], "javascript:void(0)", [
                                            'id' => 'btn-drilldown',
                                            'name' => $dataprovider['zone'],
                                            'onclick' => "modalDrilldown('" . Url::to([
                                                'drilldown-mat',
                                                'id' => $dataprovider['id'],
                                                'drilldown' => $dataprovider['drilldown'],
                                                'zone' => $dataprovider['zone'],
                                                'province' => $dataprovider['provincecode'],
                                                'amphur' => $dataprovider['amphurcode'],
                                                'hospital' => $dataprovider['hcode'],
                                                'stage' => $dataprovider['stage']
                                            ]) . "')",
                                            'style' => 'text-align:justify'
                                ]);
                            }
                        ],
                        [
                            'attribute' => 'amount',
                            'label' => 'จำนวน',
                            'format' => ['decimal', 0]
                        ]
                    ],
                    //'pjax' => true,
                    'responsive' => true,
                    'hover' => true
                ]);
            } else {
                if ($province == NULL) {
                    echo GridView::widget([
                        'dataProvider' => $dataprovider,
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],
                            [
//                    'attribute' => 'province',
                                'label' => 'จังหวัด',
                                'format' => 'raw',
                                'value' => function($dataprovider) {
                                    return Html::a($dataprovider['province'], "javascript:void(0)", [
                                                'id' => 'btn-drilldown',
                                                'name' => $dataprovider['province'],
                                                'onclick' => "modalDrilldown('" . Url::to([
                                                    'drilldown-mat',
                                                    'id' => $dataprovider['id'],
                                                    'drilldown' => $dataprovider['drilldown'],
                                                    'zone' => $dataprovider['zone_code'],
                                                    'province' => $dataprovider['provincecode'],
                                                    'amphur' => $dataprovider['amphurcode'],
                                                    'hospital' => $dataprovider['hcode'],
                                                    'stage' => $dataprovider['stage']
                                                ]) . "')",
                                                //                        'class' => 'btn-link',
                                                'style' => 'text-align:justify'
                                    ]);
                                }
                            ],
                            [
                                'attribute' => 'amount',
                                'label' => 'จำนวน',
                                'format' => ['decimal', 0]
                            ]
                        ],
                        //'pjax' => true,
                        'responsive' => true,
                        'hover' => true
                    ]);
                } else {
                    if ($amphur == 0) {
                        echo GridView::widget([
                            'dataProvider' => $dataprovider,
                            'columns' => [
                                ['class' => 'yii\grid\SerialColumn'],
                                [
//                                    'attribute' => 'amphur',
                                    'label' => 'อำเภอ',
                                    'format' => 'raw',
                                    'value' => function($dataprovider) {
                                        return Html::a($dataprovider['amphur'], "javascript:void(0)", [
                                                    'id' => 'btn-drilldown',
                                                    'name' => $dataprovider['amphur'],
                                                    'onclick' => "modalDrilldown('" . Url::to([
                                                        'drilldown-mat',
                                                        'id' => $dataprovider['id'],
                                                        'drilldown' => $dataprovider['drilldown'],
                                                        'zone' => $dataprovider['zone_code'],
                                                        'province' => $dataprovider['provincecode'],
                                                        'amphur' => $dataprovider['amphurcode'],
                                                        'hospital' => $dataprovider['hcode'],
                                                        'stage' => $dataprovider['stage']
                                                    ]) . "')",
                                                    //                        'class' => 'btn-link',
                                                    'style' => 'text-align:justify'
                                        ]);
                                    }
                                ],
                                [
                                    'attribute' => 'amount',
                                    'label' => 'จำนวน',
                                    'format' => ['decimal', 0]
                                ]
                            ],
                            //'pjax' => true,
                            'responsive' => true,
                            'hover' => true
                        ]);
                    } else {
                        if ($hospital == NULL) {
                            echo GridView::widget([
                                'dataProvider' => $dataprovider,
                                'columns' => [
                                    ['class' => 'yii\grid\SerialColumn'],
                                    [
//                                        'attribute' => 'hname',
                                        'label' => 'โรงพยาบาล',
                                        'format' => 'raw',
                                        'value' => function($dataprovider) {
                                            return Html::a($dataprovider['hname'], "javascript:void(0)", [
                                                        'id' => 'btn-drilldown',
                                                        'name' => $dataprovider['hname'],
                                                        'onclick' => "modalDrilldown('" . Url::to([
                                                            'drilldown-mat',
                                                            'id' => $dataprovider['id'],
                                                            'drilldown' => $dataprovider['drilldown'],
                                                            'zone' => $dataprovider['zone_code'],
                                                            'province' => $dataprovider['provincecode'],
                                                            'amphur' => $dataprovider['amphurcode'],
                                                            'hospital' => $dataprovider['hcode'],
                                                            'hname' => $dataprovider['hname'],
                                                            'stage' => $dataprovider['stage']
                                                        ]) . "')",
                                                        //                        'class' => 'btn-link',
                                                        'style' => 'text-align:justify'
                                            ]);
                                        }
                                    ],
                                    [
                                        'attribute' => 'amount',
                                        'label' => 'จำนวน',
                                        'format' => ['decimal', 0]
                                    ]
                                ],
                                //'pjax' => true,
                                'responsive' => true,
                                'hover' => true
                            ]);
                        } else {
                            if ($hospital != Yii::$app->session['dynamic_connection']['sitecode']) {
                                echo "<div align='center'><button class='btn btn-danger' active=false><b>ท่านไม่มีสิทธิ์เข้าดูข้อมูล กรุณาติดต่อเจ้าหน้าที่</b></button></div>";
                            } else {
                                $form = ActiveForm::begin([
                                            'id' => 'add-key',
                                            'action' => Url::to(['/ckd/report/add-key'])
                                ]);

                                echo "<label>กุญแจถอดรหัสข้อมูล</label><br/>";

                                echo Html::passwordInput('key_ckd', Yii::$app->session['key_ckd_report'] ? Yii::$app->session['key_ckd_report'] : $key_ckd_report, [
                                    'class' => 'form-control inline',
                                    'style' => 'width:40%',
                                    'placeholder' => 'กรุณากรอกกุญแจถอดรหัส'
                                ]);

                                echo Html::submitButton("<i class='fa fa-key' id='spin'></i> ถอดรหัส", [
                                    'id' => 'btn-key',
                                    'class' => 'btn btn-success',
                                    'style' => 'margin-left:5px;'
                                ]);
                                echo "<br><br>";
                                echo Html::checkbox('save_key', Yii::$app->session['save_ckd_key'] != '' ? true : false, ['label' => 'จดจำคีย์นี้', 'id' => 'save_key']);
                                echo Html::checkbox('convert', Yii::$app->session['convert_ckd_char'] != '' ? true : false, ['label' => 'เข้ารหัสแบบ tis620 (กรณีชื่อ-สกุล อ่านไม่ออกให้กาช่องนี้ด้วย)', 'id' => 'convert']);

                                ActiveForm::end();

                                echo GridView::widget([
                                    'dataProvider' => $dataprovider,
                                    'columns' => [
                                        ['class' => 'yii\grid\SerialColumn'],
                                        [
                                            'attribute' => 'pid',
                                            'label' => 'PID',
                                            'value' => function($model, $urlBackDrilldown, $i = 1) {
                                                if ($i == 1) {
                                                    echo Html::hiddenInput('page', $_GET['page'], ['id' => 'page']);
                                                    echo Html::hiddenInput('per-page', $_GET['per-page'], ['id' => 'per-page']);
                                                }

                                                return $model['pid'];
                                            }
                                        ],
                                        [
                                            'attribute' => 'prename',
                                            'label' => 'คำนำหน้า'
                                        ],
                                        [
                                            'attribute' => 'fname',
                                            'label' => 'ชื่อ'
                                        ],
                                        [
                                            'attribute' => 'lname',
                                            'label' => 'สกุล'
                                        ]
                                    ],
                                    'pjax' => true,
//                                    'responsive' => true,
                                    'hover' => true
                                ]);
                            }
                        }
                    }
                }
            }
            ?>

        </div>
    </div>
</div>
<?php
$url = Url::to($urlBackDrilldown);
$this->registerJs("
        
        function backDrilldown(url){
            $('#modal-drilldown .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
            $('#modal-drilldown')
            .find('.modal-content')
            .load(url);
        }  
        
        $('form#add-key').on('beforeSubmit', function(e) {

               $('#spin').addClass('fa fa-spinner fa-pulse fa-1x fa-fw');
               var \$form = $(this);
               $.post(
                   \$form.attr('action'), //serialize Yii2 form
                   \$form.serialize()
               ).done(function(result) {
                   if(result.status == 'success') {
                       var urlKey = '" . $url . "&hospital={$hospital}&hname={$hname}';
                       urlKey = urlKey+'&page='+$('#page').val()+'&per-page='+$('#per-page').val();
                       console.log(urlKey);
                     $('#modal-drilldown').find('.modal-content').load(urlKey,function(){
                        " . SDNoty::show('result.message', 'result.status') . "
//                             $('#spin').removeClass('fa fa-spinner fa-pulse fa-1x fa-fw');
//                             $('#spin').addClass('fa fa-key');
                             
                    });
//                       " . SDNoty::show('result.message', 'result.status') . "
//                       $('#spin').removeClass('fa fa-spinner fa-pulse fa-1x fa-fw');
                   } else {
                       " . SDNoty::show('result.message', 'result.status') . "
                   } 
               }).fail(function() {
                   " . SDNoty::show("'" . SDHtml::getMsgError() . "Server Error'", '"error"') . "
                   $('#spin').removeClass('fa fa-spinner fa-pulse fa-1x fa-fw');
                   console.log('server error');
               });
               return false;
           });
           
           $('#save_key').on('change',function(){
            var \$form = $('#add-key');
            $.post(
                \$form.attr('action'), //serialize Yii2 form
                \$form.serialize()
            )
            
            return false;
        });
        
        $('#convert').on('change',function(){
            var \$form = $('#add-key');
            $.post(
                \$form.attr('action'), //serialize Yii2 form
                \$form.serialize()
            )
            
            return false;
        });
        
    ");
?>