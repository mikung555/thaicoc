<?php

use kartik\grid\GridView;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
?>

<div class="pull-right">
    <button class="btn btn-success" onclick="fnWordExport();"><i class="fa fa-file-word-o" aria-hidden="true"></i> ดาวน์โหลด MS-Word</button>
</div>
<div id="docx">
  <div class="WordSection1">
<h4><i class="fa fa-table"></i> การเปลี่ยนแปลงระยะของโรคไตเรื้อรัง <?= $zone ?> ปีงบประมาณ <?= $year_begin+543 ?> เปรียบเทียบกับปีงบประมาณ <?= ($year_begin-1)+543 ?></h4>
<!--<p class="text-primary">การวินิจฉัยโรคโดย : <?php echo \yii\helpers\Html::encode($itemDiag[$diagsource]['name']); ?></p>-->
<p class="text-warning">(คลิกที่ "ตัวเลข" เพื่อดูรายละเอียดของข้อมูล)</p>
<br>

<?php
//    appxq\sdii\utils\VarDumper::dump($arr_parm);
?>
<!--grid report stage change-->
<?php $this->registerCss("
        tr > td > a {
            color: #333;
        } 
    ");
?>
<center>
<table id="matrix_ckdnet" class="table table-bordered" style="width: 75%;margin-left: 5%;margin-right: 5%;">
    <thead>
        <tr>
            <th style="background-color: #9ab3e5;height: 50px;width:150px;vertical-align: middle;">Stage ORG/NEW</th>
            <th style="background-color: #9ab3e5;height: 50px;width:100px;text-align: center;vertical-align: middle;">Stage1</th>
            <th style="background-color: #9ab3e5;height: 50px;width:100px;text-align: center;vertical-align: middle;">Stage2</th>
            <th style="background-color: #9ab3e5;height: 50px;width:100px;text-align: center;vertical-align: middle;">Stage3</th>
            <th style="background-color: #9ab3e5;height: 50px;width:100px;text-align: center;vertical-align: middle;">Stage4</th>
            <th style="background-color: #9ab3e5;height: 50px;width:100px;text-align: center;vertical-align: middle;">Stage5</th>
        </tr>
    </thead>
    <tbody style="text-align: center">

        <tr>
            <th style="background-color: #9ab3e5;height: 50px;vertical-align: middle;">Stage1</th>
            <td id="showdata1" data-url="<?= yii\helpers\Url::to(['report/drilldown-mat', 'zone' => $zone, 'province' => $province, 'amphur' => $amphur, 'hospital' => $hospital, 'hname' => $hname[0]['hname'], 'id' => $queryCol[106]]) ?>" style="background-color: #d9d9d9;vertical-align: middle;"><?php echo Html::a(number_format($queryData[0]['N181N181'])); ?></td><!-- center-->
            <td id="showdata2" data-url="<?= yii\helpers\Url::to(['report/drilldown-mat', 'zone' => $zone, 'province' => $province, 'amphur' => $amphur, 'hospital' => $hospital, 'hname' => $hname[0]['hname'], 'id' => $queryCol[107]]) ?>" style="background-color: #ffe6cc;vertical-align: middle;"><?php echo Html::a(number_format($queryData[0]['N181N182'])); ?></td><!-- 12-->
            <td id="showdata3" data-url="<?= yii\helpers\Url::to(['report/drilldown-mat', 'zone' => $zone, 'province' => $province, 'amphur' => $amphur, 'hospital' => $hospital, 'hname' => $hname[0]['hname'], 'id' => $queryCol[108]]) ?>" style="background-color: #ffd9b3;vertical-align: middle;"><?php echo Html::a(number_format($queryData[0]['N181N183'])); ?></td><!-- 13-->
            <td id="showdata4" data-url="<?= yii\helpers\Url::to(['report/drilldown-mat', 'zone' => $zone, 'province' => $province, 'amphur' => $amphur, 'hospital' => $hospital, 'hname' => $hname[0]['hname'], 'id' => $queryCol[109]]) ?>" style="background-color: #ffcc99;vertical-align: middle;"><?php echo Html::a(number_format($queryData[0]['N181N184'])); ?></td><!-- 14-->
            <td id="showdata5" data-url="<?= yii\helpers\Url::to(['report/drilldown-mat', 'zone' => $zone, 'province' => $province, 'amphur' => $amphur, 'hospital' => $hospital, 'hname' => $hname[0]['hname'], 'id' => $queryCol[110]]) ?>" style="background-color: #ffb399;vertical-align: middle;"><?php echo Html::a(number_format($queryData[0]['N181N185'])); ?></td><!-- 15-->

        </tr>
        <tr>
            <th style="background-color: #9ab3e5;height: 50px;vertical-align: middle;">Stage2</th>
            <td id="showdata6" data-url="<?= yii\helpers\Url::to(['report/drilldown-mat', 'zone' => $zone, 'province' => $province, 'amphur' => $amphur, 'hospital' => $hospital, 'hname' => $hname[0]['hname'], 'id' => $queryCol[111]]) ?>" style="background-color: #d6f5d6;vertical-align: middle;"><?php echo Html::a(number_format($queryData[0]['N182N181'])); ?></td><!-- 21-->
            <td id="showdata7" data-url="<?= yii\helpers\Url::to(['report/drilldown-mat', 'zone' => $zone, 'province' => $province, 'amphur' => $amphur, 'hospital' => $hospital, 'hname' => $hname[0]['hname'], 'id' => $queryCol[112]]) ?>" style="background-color: #d9d9d9;vertical-align: middle;"><?php echo Html::a(number_format($queryData[0]['N182N182'])); ?></td><!-- center-->
            <td id="showdata8" data-url="<?= yii\helpers\Url::to(['report/drilldown-mat', 'zone' => $zone, 'province' => $province, 'amphur' => $amphur, 'hospital' => $hospital, 'hname' => $hname[0]['hname'], 'id' => $queryCol[113]]) ?>" style="background-color: #ffe6cc;vertical-align: middle;"><?php echo Html::a(number_format($queryData[0]['N182N183'])); ?></td><!-- 23-->
            <td id="showdata9" data-url="<?= yii\helpers\Url::to(['report/drilldown-mat', 'zone' => $zone, 'province' => $province, 'amphur' => $amphur, 'hospital' => $hospital, 'hname' => $hname[0]['hname'], 'id' => $queryCol[114]]) ?>" style="background-color: #ffd9b3;vertical-align: middle;"><?php echo Html::a(number_format($queryData[0]['N182N184'])); ?></td><!-- 24-->
            <td id="showdata10" data-url="<?= yii\helpers\Url::to(['report/drilldown-mat', 'zone' => $zone, 'province' => $province, 'amphur' => $amphur, 'hospital' => $hospital, 'hname' => $hname[0]['hname'], 'id' => $queryCol[115]]) ?>" style="background-color: #ffcc99;vertical-align: middle;"><?php echo Html::a(number_format($queryData[0]['N182N185'])); ?></td><!-- 25-->
        </tr>
        <tr>
            <th style="background-color: #9ab3e5;height: 50px;vertical-align: middle;">Stage3</th>
            <td id="showdata11" data-url="<?= yii\helpers\Url::to(['report/drilldown-mat', 'zone' => $zone, 'province' => $province, 'amphur' => $amphur, 'hospital' => $hospital, 'hname' => $hname[0]['hname'], 'id' => $queryCol[116]]) ?>" style="background-color: #adebad;vertical-align: middle;"><?php echo Html::a(number_format($queryData[0]['N183N181'])); ?></td><!-- 31-->
            <td id="showdata12" data-url="<?= yii\helpers\Url::to(['report/drilldown-mat', 'zone' => $zone, 'province' => $province, 'amphur' => $amphur, 'hospital' => $hospital, 'hname' => $hname[0]['hname'], 'id' => $queryCol[117]]) ?>" style="background-color: #d6f5d6;vertical-align: middle;"><?php echo Html::a(number_format($queryData[0]['N183N182'])); ?></td><!-- 32-->
            <td id="showdata13" data-url="<?= yii\helpers\Url::to(['report/drilldown-mat', 'zone' => $zone, 'province' => $province, 'amphur' => $amphur, 'hospital' => $hospital, 'hname' => $hname[0]['hname'], 'id' => $queryCol[118]]) ?>" style="background-color: #d9d9d9;vertical-align: middle;"><?php echo Html::a(number_format($queryData[0]['N183N183'])); ?></td><!-- center-->
            <td id="showdata14" data-url="<?= yii\helpers\Url::to(['report/drilldown-mat', 'zone' => $zone, 'province' => $province, 'amphur' => $amphur, 'hospital' => $hospital, 'hname' => $hname[0]['hname'], 'id' => $queryCol[119]]) ?>" style="background-color: #ffe6cc;vertical-align: middle;"><?php echo Html::a(number_format($queryData[0]['N183N184'])); ?></td><!-- 34-->
            <td id="showdata15" data-url="<?= yii\helpers\Url::to(['report/drilldown-mat', 'zone' => $zone, 'province' => $province, 'amphur' => $amphur, 'hospital' => $hospital, 'hname' => $hname[0]['hname'], 'id' => $queryCol[120]]) ?>" style="background-color: #ffd9b3;vertical-align: middle;"><?php echo Html::a(number_format($queryData[0]['N183N185'])); ?></td><!-- 35-->
        </tr>
        <tr>
            <th style="background-color: #9ab3e5;height: 50px;vertical-align: middle;">Stage4</th>
            <td id="showdata16" data-url="<?= yii\helpers\Url::to(['report/drilldown-mat', 'zone' => $zone, 'province' => $province, 'amphur' => $amphur, 'hospital' => $hospital, 'hname' => $hname[0]['hname'], 'id' => $queryCol[121]]) ?>" style="background-color: #85e085;vertical-align: middle;"><?php echo Html::a(number_format($queryData[0]['N184N181'])); ?></td><!-- 41-->
            <td id="showdata17" data-url="<?= yii\helpers\Url::to(['report/drilldown-mat', 'zone' => $zone, 'province' => $province, 'amphur' => $amphur, 'hospital' => $hospital, 'hname' => $hname[0]['hname'], 'id' => $queryCol[122]]) ?>" style="background-color: #adebad;vertical-align: middle;"><?php echo Html::a(number_format($queryData[0]['N184N182'])); ?></td><!-- 42-->
            <td id="showdata18" data-url="<?= yii\helpers\Url::to(['report/drilldown-mat', 'zone' => $zone, 'province' => $province, 'amphur' => $amphur, 'hospital' => $hospital, 'hname' => $hname[0]['hname'], 'id' => $queryCol[123]]) ?>" style="background-color: #d6f5d6;vertical-align: middle;"><?php echo Html::a(number_format($queryData[0]['N184N183'])); ?></td><!-- 43-->
            <td id="showdata19" data-url="<?= yii\helpers\Url::to(['report/drilldown-mat', 'zone' => $zone, 'province' => $province, 'amphur' => $amphur, 'hospital' => $hospital, 'hname' => $hname[0]['hname'], 'id' => $queryCol[124]]) ?>" style="background-color: #d9d9d9;vertical-align: middle;"><?php echo Html::a(number_format($queryData[0]['N184N184'])); ?></td><!-- center-->
            <td id="showdata20" data-url="<?= yii\helpers\Url::to(['report/drilldown-mat', 'zone' => $zone, 'province' => $province, 'amphur' => $amphur, 'hospital' => $hospital, 'hname' => $hname[0]['hname'], 'id' => $queryCol[125]]) ?>" style="background-color: #ffe6cc;vertical-align: middle;"><?php echo Html::a(number_format($queryData[0]['N184N185'])); ?></td><!-- 45-->
        </tr>
        <tr>
            <th style="background-color: #9ab3e5;height: 50px;vertical-align: middle;">Stage5</th>
            <td id="showdata21" data-url="<?= yii\helpers\Url::to(['report/drilldown-mat', 'zone' => $zone, 'province' => $province, 'amphur' => $amphur, 'hospital' => $hospital, 'hname' => $hname[0]['hname'], 'id' => $queryCol[126]]) ?>" style="background-color: #80cc33;vertical-align: middle;"><?php echo Html::a(number_format($queryData[0]['N185N181'])) ?></td><!-- 51-->
            <td id="showdata22" data-url="<?= yii\helpers\Url::to(['report/drilldown-mat', 'zone' => $zone, 'province' => $province, 'amphur' => $amphur, 'hospital' => $hospital, 'hname' => $hname[0]['hname'], 'id' => $queryCol[127]]) ?>" style="background-color: #85e085;vertical-align: middle;"><?php echo Html::a(number_format($queryData[0]['N185N182'])) ?></td><!-- 52-->
            <td id="showdata23" data-url="<?= yii\helpers\Url::to(['report/drilldown-mat', 'zone' => $zone, 'province' => $province, 'amphur' => $amphur, 'hospital' => $hospital, 'hname' => $hname[0]['hname'], 'id' => $queryCol[128]]) ?>" style="background-color: #adebad;vertical-align: middle;"><?php echo Html::a(number_format($queryData[0]['N185N183'])); ?></td><!-- 53-->
            <td id="showdata24" data-url="<?= yii\helpers\Url::to(['report/drilldown-mat', 'zone' => $zone, 'province' => $province, 'amphur' => $amphur, 'hospital' => $hospital, 'hname' => $hname[0]['hname'], 'id' => $queryCol[129]]) ?>" style="background-color: #d6f5d6;vertical-align: middle;"><?php echo Html::a(number_format($queryData[0]['N185N184'])); ?></td><!-- 54-->
            <td id="showdata25" data-url="<?= yii\helpers\Url::to(['report/drilldown-mat', 'zone' => $zone, 'province' => $province, 'amphur' => $amphur, 'hospital' => $hospital, 'hname' => $hname[0]['hname'], 'id' => $queryCol[130]]) ?>" style="background-color: #d9d9d9;vertical-align: middle;"><?php echo Html::a(number_format($queryData[0]['N185N185'])); ?></td><!-- center-->

        </tr>

    </tbody>
</table>
</center>

<div id="summary_matrix_ckdnet" class="panel panel-info">
    <div class="panel-heading">อัตราการเปลี่ยนเเปลงระยะของโรคไตเรื้อรัง</div>
    <div class="panel-body">
        <p class="text-success"><span class="glyphicon glyphicon-stats"></span> การเปลี่ยนเเปลงของผู้ป่วยโรคไตเรื้อรังเป็นไปในทางที่ดีขึ้น (ชะลอไตเสื่อม) :  
            <span id="showdata26" data-url="<?= yii\helpers\Url::to(['report/drilldown-mat', 'drilldown' => '1', 'stage' => 'stage_d', 'zone' => $zone, 'province' => $province, 'amphur' => $amphur, 'hospital' => $hospital, 'hname' => $hname[0]['hname']]) ?>"><?php echo Html::a(number_format($sum_better)); ?> &nbsp; <span class="glyphicon glyphicon-new-window"></span></span> &nbsp;คน</p>
        <p><span class="glyphicon glyphicon-stats"></span> การเปลี่ยนเเปลงของผู้ป่วยโรคไตเรื้อรังคงที่ หรือ ไม่เปลี่ยนแปลง (ชะลอไตเสื่อม) :  
            <span id="showdata27" data-url="<?= yii\helpers\Url::to(['report/drilldown-mat', 'drilldown' => '1', 'stage' => 'stage_m', 'zone' => $zone, 'province' => $province, 'amphur' => $amphur, 'hospital' => $hospital, 'hname' => $hname[0]['hname']]) ?>"><?php echo Html::a(number_format($sum_stable)); ?> &nbsp; <span class="glyphicon glyphicon-new-window"></span></span> &nbsp;คน</p>
        <p class="text-danger"><span class="glyphicon glyphicon-stats"></span> การเปลี่ยนเเปลงของผู้ป่วยโรคไตเรื้อรังเป็นไปในทางที่แย่ลง :  
            <span id="showdata28" data-url="<?= yii\helpers\Url::to(['report/drilldown-mat', 'drilldown' => '1', 'stage' => 'stage_b', 'zone' => $zone, 'province' => $province, 'amphur' => $amphur, 'hospital' => $hospital, 'hname' => $hname[0]['hname']]) ?>"><?php echo Html::a(number_format($sum_worst)); ?> &nbsp; <span class="glyphicon glyphicon-new-window"></span></span> &nbsp;คน</p>

        <br>

        <p><strong>หมายเหตุ:</strong> จำนวนผู้ป่วยโรคไตเรื้อรังแต่ละสถานะ หมายถึง ผู้ป่วยรายเก่าและรายใหม่ที่มีชีวิต ในปีงบประมาณที่ถูกเลือกจากรายการ ที่มีค่า eGFR (ล่าสุด)ในปีงบประมาณก่อนหน้านี้ และมีค่า eGFR (ล่าสุด)ในปีงบประมาณในปีนี้</p>
        <ul>
            <li>รหัส N181 หมายถึง ผู้ป่วยโรคไตเรื้อรัง ระยะที่ 1 (stage 1)</li>
            <li>รหัส N182 หมายถึง ผู้ป่วยโรคไตเรื้อรัง ระยะที่ 2 (stage 2)</li>
            <li>รหัส N183 หมายถึง ผู้ป่วยโรคไตเรื้อรัง ระยะที่ 3 (stage 3)</li>
            <li>รหัส N184 หมายถึง ผู้ป่วยโรคไตเรื้อรัง ระยะที่ 4 (stage 4)</li>
            <li>รหัส N185 หมายถึง ผู้ป่วยโรคไตเรื้อรัง ระยะที่ 5 (stage 5)</li>
        </ul>
    </div>
</div></div>
</div>

<?php
$this->registerJs("
    
    var x=0;
    var i=0;

    while(i<28){

        x++;

        $('#showdata'+x).css('cursor', 'pointer');

        $('#showdata'+x).click(function () {
            console.log($(this).attr('id'));
            console.log($(this).attr('data-url'));
            modalDrilldown($(this).attr('data-url'));                                 
        }) 
        i++;

   } 

    function modalDrilldown(url){
            $('#modal-drilldown .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
            $('#modal-drilldown').modal('show')
            .find('.modal-content')
            .load(url);
            return false;
    }
    
    function fnWordExport()
    {
        var html, link, blob, url, css;

           css = (
             '<style>' +
             '@page WordSection1{size: 21cm 29.7cm;mso-page-orientation: portrait;}' +
             'div.WordSection1 {page: WordSection1;}' +
             'table{border-collapse:collapse;}th{border:1px gray solid;width:5em;padding:2px;}tr{border:1px gray solid;width:5em;padding:2px;}td{border:1px gray solid;width:5em;padding:2px;}'+
             '</style>'
           );

           html = window.docx.innerHTML;
           blob = new Blob(['\ufeff', css + html], {
             type: 'application/msword'
           });
           url = URL.createObjectURL(blob);
           link = document.createElement('A');
           link.href = url;
           // Set default file name. 
           // Word will append file extension - do not add an extension here.
           link.download = 'Reportmatrix_stagechangeCKD';   
           document.body.appendChild(link);
           if (navigator.msSaveOrOpenBlob ) navigator.msSaveOrOpenBlob( blob, 'Reportmatrix_stagechangeCKD.doc'); // IE10-11
                        else link.click();  // other browsers
           document.body.removeChild(link);
    }
             
");
?>