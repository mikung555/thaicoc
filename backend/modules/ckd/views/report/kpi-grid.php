          
<div id="frm">
            <?php ActiveForm::begin(['id' => 'formSearch']) ?>
            <?= Html::hiddenInput('report_num',$report_num)?>
            <br/>
            <div class="col-md-5">
                <h5 class="section-text">ปีงบประมาณ</h5>
                <?=
                Select2::widget([
                    'name' => 'year',
                    'id' => 'year',
//                    'hideSearch' => true,
                    'value' => date('Y'),
                    'data' => ArrayHelper::map($itemYear, 'id', 'name'),
                    'options' => [
                        'prompt' => '-----ทั้งหมด-----',
                    ],
                    'pluginOptions' => [
                        'allowClear' => true,
                    ],
                ]);
                ?>
            </div>
            <div class="col-md-5" id="div-zone-choice">
                <h5 class="section-text">เขต :</h5>
                <?=
                Select2::widget([
                    'name' => 'zone-choice',
                    'id' => 'zone-choice',
                    'hideSearch' => true,
                    'data' => ArrayHelper::map($itemZone, 'id', 'name'),
                    'options' => [
                        'prompt' => '-----ทั้งหมด-----',
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
                ?>
            </div>
            <div class="clearfix"></div>
            <div class="col-md-5" id="div-province">
                <h5 class="section-text">จังหวัด :</h5>
                <?=
                DepDrop::widget([
                    'type' => DepDrop::TYPE_SELECT2,
                    'name' => 'province',
                    'options' => ['id' => 'province'],
                    'select2Options' => ['pluginOptions' => ['allowClear' => true]],
                    'pluginOptions' => [
                        'depends' => ['zone-choice'],
                        'initialize' => true,
                        'initDepends' => ['zone-choice'],
                        'params' => ['zoneChoice'],
                        'placeholder' => '-----ทั้งหมด-----',
                        'url' => '/ckd/report/get-province',
                        'allowClear' => true,
                    ]
                ]);
                ?>
            </div>

            <div class="col-md-5" id="div-hospital">
                <h5 class="section-text" id="label-select">โรงพยาบาล :</h5>
                <?php
                echo DepDrop::widget([
                    'type' => DepDrop::TYPE_SELECT2,
                    'name' => 'hospital',
                    'id' => 'hospital',
                    'select2Options' => ['pluginOptions' => ['allowClear' => true]],
                    'pluginOptions' => [
                        'depends' => ['province'],
                        'initialize' => true,
                        'initDepends' => ['province'],
                        'params' => ['province', 'zone-choice'],
                        'placeholder' => '-----ทั้งหมด-----',
                        'url' => '/ckd/report/get-select',
                    ]
                ]);
                ?>
            </div>


            <br/><br/><br/><br/>
            <div class="col-md-2">
                <button type="button" id="searchData" class="btn btn-primary"><i class="glyphicon glyphicon-search"></i> ตกลง</button>
            </div>
            <div class="clearfix"></div>
            <?php ActiveForm::end() ?>
        </div>
        <br/> 