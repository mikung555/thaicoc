<?php

use Yii;
use kartik\tabs\TabsX;
use yii\helpers\Url;
use kartik\depdrop\DepDrop;
use yii\helpers\ArrayHelper;
use appxq\sdii\helpers\SDHtml;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use kartik\widgets\Select2;
use appxq\sdii\widgets\ModalForm;
use kartik\grid\GridView;
use yii\web\JsExpression;

/* @var $this yii\web\View */
$this->title = Yii::t('backend', 'Module: CKDNET');
$this->params['breadcrumbs'][] = ['label' => 'Modules', 'url' => Url::to('/tccbots/my-module')];
$this->params['breadcrumbs'][] = ['label' => 'รายงาน KPI', 'url' => Url::to('/ckd/report')];
$this->params['breadcrumbs'][] = Yii::t('backend', 'Report');
//appxq\sdii\utils\VarDumper::dump($mat['n181181']);
?>
<div class="alert alert-danger" style="font-size:25px;">
    <i class="fa fa-wrench" aria-hidden="true"></i> กำลังพัฒนาระบบ  
</div>
<?php
$itemZone = [
        ['id' => '01', 'name' => 'เขตสุขภาพที่ 1'],
        ['id' => '02', 'name' => 'เขตสุขภาพที่ 2'],
        ['id' => '03', 'name' => 'เขตสุขภาพที่ 3'],
        ['id' => '04', 'name' => 'เขตสุขภาพที่ 4'],
        ['id' => '05', 'name' => 'เขตสุขภาพที่ 5'],
        ['id' => '06', 'name' => 'เขตสุขภาพที่ 6'],
        ['id' => '07', 'name' => 'เขตสุขภาพที่ 7'],
        ['id' => '08', 'name' => 'เขตสุขภาพที่ 8'],
        ['id' => '09', 'name' => 'เขตสุขภาพที่ 9'],
        ['id' => '10', 'name' => 'เขตสุขภาพที่ 10'],
        ['id' => '11', 'name' => 'เขตสุขภาพที่ 11'],
        ['id' => '12', 'name' => 'เขตสุขภาพที่ 12'],
];

$itemDiag = [
        ['id' => '0', 'name' => '-----ทั้งหมด-----'],
        ['id' => '1', 'name' => 'แพทย์วินิจฉัย'],
        ['id' => '2', 'name' => 'ผล LAB'],
        ['id' => '3', 'name' => 'รหัสโรค ICD-10'],
];
?>

<div class="ckdnet-default-index">
    <?php ActiveForm::begin(['id' => 'formSearch']) ?>  

    <h4>เลือกรายการ :</h4>
    <div class="container" style="float:left;">
        <div class="row">
            <div class="col-6 col-md-4">
                <h5 class="section-text">ปีงบประมาณเริ่มต้น :</h5>
                <?=
                Select2::widget([
                    'name' => 'year_begin',
//                    'hideSearch' => true,
                    'value' => (date('m')>9?date("Y")+1:date("Y")),
                    'data' => $itemYear,
                    'options' => [
                        'prompt' => '-----ทั้งหมด-----',
                    ],
                    'pluginOptions' => [
                        'allowClear' => true,
                    ],
                ]);
                ?>
            </div>
            <!--            <div class="col-6 col-md-4">
                            <h5 class="section-text">ปีงบประมาณสิ้นสุด :</h5>
                            <?
            //                Select2::widget([
            //                    'name' => 'year_end',
            //                    'value' => date('Y'),
            //                    'data' => $year_range,
            //                    'options' => [
            //                        'prompt' => '-----ทั้งหมด-----',
            //                    ],
            //                    'pluginOptions' => [
            //                        'allowClear' => true,
            //                    ],
            //                ]);
                            ?>
                        </div>-->
        </div>
        <div class="row">
            <div class="col-6 col-md-4" id="div-zone-choice">

                <h5 class="section-text">เขตสุขภาพ :</h5>
                <?=
                Select2::widget([
                    'name' => 'zone-choice',
                    'id' => 'zone-choice',
                    'hideSearch' => true,
                    'value' => $zone_select,
                    'hideSearch' => true,
                    //'data' => $itemZone,
                    'data' => ArrayHelper::map($itemZone, 'id', 'name'),
                    'options' => [
                        'prompt' => '-----ทั้งหมด-----',
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
                ?>
            </div>
            <div class="col-6 col-md-4" id="div-province">
                <h5 class="section-text">จังหวัด :</h5>
                <?=
                DepDrop::widget([
                    'type' => DepDrop::TYPE_SELECT2,
                    'name' => 'province',
                    'options' => ['id' => 'province'],
                    'data' => [$province_select => 'selected'],
                    'select2Options' => ['pluginOptions' => ['allowClear' => true]],
                    'pluginOptions' => [
                        'depends' => ['zone-choice'],
                        'initialize' => true,
                        'initDepends' => ['zone-choice'],
                        'params' => ['zoneChoice'],
                        'placeholder' => '-----ทั้งหมด-----',
                        'url' => '/ckd/report/get-province',
                        'allowClear' => true,
                    ]
                ]);
                ?>
            </div>
        </div>
        <div class="row">
            <div class="col-6 col-md-4" id="div-amphur">
                <h5 class="section-text">อำเภอ :</h5>
                <?php 
                echo DepDrop::widget([
                    'type' => DepDrop::TYPE_SELECT2,
                    'name' => 'amphur',
                    'id' => 'amphur',
                    'data' => [$province_select.$amphur_select => 'selected'],
                    'select2Options' => ['pluginOptions' => ['allowClear' => true]],
                    'pluginOptions' => [
                        'depends' => ['province'],
                        'initialize' => true,
                        'initDepends' => ['province'],
                        'params' => ['province'],
                        'placeholder' => '-----ทั้งหมด-----',
                        'url' => Url::to(['/ckd/report/get-amphur', 'code'=>$province_select.$amphur_select]),
                        'allowClear' => true,
                    ]
                ]);
                ?>
            </div>
            <div class="col-6 col-md-4" id="div-hospital">

                <h5 class="section-text">โรงพยาบาล :</h5>
                <?php
//                echo DepDrop::widget([
//                    'type' => DepDrop::TYPE_SELECT2,
//                    'name' => 'hospital',
//                    'id' => 'hospital',
//                    'select2Options' => ['pluginOptions' => ['allowClear' => true]],
//                    'pluginOptions' => [
//                        'depends' => ['amphur'],
//                        'initialize' => true,
//                        'initDepends' => ['amphur'],
//                        'params' => ['province', 'amphur'],
//                        'placeholder' => '-----ทั้งหมด-----',
//                        'url' => '/ckd/report/get-hospital',
//                    ]
//                ]);
                echo Select2::widget([
                    'name' => 'hospital',
                    'id' => 'hospital',
                    'options' => ['placeholder' => 'เลือกหน่วยงาน...'],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'minimumInputLength' => 0, //ต้องพิมพ์อย่างน้อย 3 อักษร ajax จึงจะทำงาน
                        'ajax' => [
                            'url' => '/ckd/report/get-hospital2',
                            'dataType' => 'json', //รูปแบบการอ่านคือ json
                            'data' => new JsExpression('function(params) { 
                            var codes = $("#amphur").val();
                            var zone = $("#zone-choice").val();
                            var amphur =codes.substring(2,4);
                            var province =$("#province").val();
                            //console.log(zonecode+" "+amphur+" "+province);
                            return {q:params.term, zone, province, amphur}; 
                            }
                         '),
                        ],
                        'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                        'templateResult' => new JsExpression('function(city) { return city.text; }'),
                        'templateSelection' => new JsExpression('function (city) { return city.text; }'),
                    ]
                ]);
                ?>
            </div>
            <!--            <div class="col-6 col-md-4">
                            <h5 class="section-text">เลือกดูการวินิจฉัยโรคโดย :</h5>
                            <?
            //                Select2::widget([
            //                    'name' => 'diagsource',
            //                    'value' => ArrayHelper::getValue($itemDiag, '0'),
            //                    'data' => ArrayHelper::map($itemDiag, 'id', 'name'),
            //                    'options' => [
            //                        'prompt' => '-----ทั้งหมด-----',
            //                    ],
            //                    'pluginOptions' => [
            //                        'allowClear' => true,
            //                    ],
            //                ]);
                            ?>
                        </div>-->
        </div>
        <div class="row">
            <div class="col-6 col-md-4" id="searchData">
                <br>
                <?php
                echo Html::button('<span class="fa fa-search"></span> แสดงรายงาน', [
                    'class' => 'btn btn-primary',
                    'id' => 'btnSearch',
                ]);
                ?>
                <br>

            </div>
        </div>
        <?php ActiveForm::end() ?>
    </div>

    <br>
    <div class="clearfix"></div>
    <hr>

    <br>

    <div id="showmat"></div>

    <br>
    <!--Modal-->
    <?php
    echo ModalForm::widget([
        'id' => 'modal-drilldown',
        'size' => 'modal-lg',
    ]);
    ?>
</div>

<?php
$this->registerJs("
    
    $(document).ready(function(){
        $('#showmat').html('<div class=\"sdloader \"><b class=\"alert alert-info\">กำลังโหลดข้อมูล...</b></div>');        
        Mat();
               
    });
        
    function Mat(){
    
         $.ajax({
            url:'" . Url::to(['report/report-matrix']) . "',
            method:'POST',
            data:$('#formSearch').serialize(),
            dataType:'HTML',
            success:function(result){
               //console.log(result);
               $('#showmat').html(result); 
            },error: function (xhr, ajaxOptions, thrownError) {
               console.log(xhr);
            }
        });
        return false;
    }
 
    $('#btnSearch').on('click',function(){
        $('#showmat').html('<div class=\"sdloader \" ><b class=\"alert alert-info\">กำลังโหลดข้อมูล...</b></div>');
        Mat();
        return false;
    });
 
    
");
?>