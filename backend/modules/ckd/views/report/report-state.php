<?php

use miloschuman\highcharts\Highcharts;
use miloschuman\highcharts\Highstock;
use miloschuman\highcharts\HighchartsAsset;
use miloschuman\highcharts\SeriesDataHelper;
use appxq\sdii\widgets\ModalForm;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use kartik\widgets\DepDrop;
use yii\widgets\ActiveForm;
use kartik\widgets\Select2;
use yii\web\JsExpression;

$this->title = Yii::t('backend', 'Module: CKDNET');
$this->params['breadcrumbs'][] = ['label' => 'Modules', 'url' => Url::to('/tccbots/my-module')];
$this->params['breadcrumbs'][] = ['label' => 'รายงาน KPI', 'url' => Url::to('/ckd/report')];
$this->params['breadcrumbs'][] = Yii::t('backend', 'Report');
?>
<div class="alert alert-danger" style="font-size:25px;">
    <i class="fa fa-wrench" aria-hidden="true"></i> กำลังพัฒนาระบบ  
</div>
<div class="report">

    <div class="col-lg-12">
        <div style="padding-bottom: 10px;">
            <?php ActiveForm::begin(['id' => 'formSearch']) ?>
            <?php // echo Html::hiddenInput('calculate', '0', ['id' => 'calculate'])  ?>
            <?= Html::hiddenInput('page_number', $_GET['page_number']) ?>
            <br/>
            <div class="col-md-5">
                <h5 class="section-text">ปีงบประมาณ</h5>
                <?=
                Select2::widget([
                    'name' => 'year',
                    'id' => 'year',
//                    'hideSearch' => true,
                    'value' => (date('m')>9?date("Y")+1:date("Y")),
                    'data' => $itemYear,
                    'options' => [
                        'prompt' => '-----ทั้งหมด-----',
                    ],
                    'pluginOptions' => [
                        'allowClear' => true,
                    ],
                ]);
                ?>
            </div>
            <!--            <div class="clearfix"></div>
                        <div class="col-md-5" id="div-zone">
                            <h5 class="section-text">เขตพื้นที่/Service Plan :</h5>-->
            <?php
//                echo Select2::widget([
//                    'name' => 'zone',
//                    'id' => 'zone',
//                    'hideSearch' => true,
//                    'value' => '1',
//                    'data' => ['0' => 'เขตพื้นที่', '1' => 'Service Plan', '2' => 'รายโรงพยาบาล'],
//                    'options' => [
//                        'prompt' => '-----ทั้งหมด-----',
//                    ],
//                ]);
            ?>
            <!--</div>-->

            <!--            <div class="col-md-5" id="div-service-plan">
                            <h5 class="section-text">Service Plan Level :</h5>-->
            <?php
//                echo Select2::widget([
//                    'name' => 'service-plan',
//                    'id' => 'service-plan',
//                    'hideSearch' => true,
//                    'data' => ['A' => 'A', 'S' => 'S', 'M1' => 'M1', 'M2' => 'M2', 'F1' => 'F1', 'F2' => 'F2', 'F3' => 'F3'],
//                    'options' => [
//                        'prompt' => '-----ทั้งหมด-----',
//                    ],
//                    'pluginOptions' => [
//                        'allowClear' => true
//                    ],
//                ]);
            ?>
            <!--            </div>-->

            <!--            <div class="clearfix"></div>-->
            <!--            <div class="col-md-5" id="div-zone-province">
                            <h5 class="section-text">เขต/จังหวัด :</h5>-->
            <?php
//                echo Select2::widget([
//                    'name' => 'zone-province',
//                    'id' => 'zone-province',
//                    'hideSearch' => true,
//                    'data' => ['0' => 'เขตสุขภาพ', '1' => 'รายจังหวัด'],
//                    'options' => [
//                        'prompt' => '-----ทั้งหมด-----',
//                    ],
//                ]);
            ?>
            <!--            </div>-->
            <div class="col-md-5" id="div-zone-choice">
                <h5 class="section-text">เขต :</h5>
                <?=
                Select2::widget([
                    'name' => 'zone',
                    'id' => 'zone',
                    'value' => $zone_select,
                    'hideSearch' => true,
                    'data' => $itemZone,
                    'options' => [
                        'prompt' => '-----ทั้งหมด-----',
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
                ?>
            </div>
            <div class="clearfix"></div>
            <div class="col-md-5" id="div-province">
                <h5 class="section-text">จังหวัด :</h5>
                <?=
                DepDrop::widget([
                    'type' => DepDrop::TYPE_SELECT2,
                    'name' => 'province',
                    'data' => [$province_select => 'selected'],
                    'options' => ['id' => 'province'],
                    'select2Options' => ['pluginOptions' => ['allowClear' => true]],
                    'pluginOptions' => [
                        'depends' => ['zone'],
                        'initialize' => true,
                        'initDepends' => ['zone'],
                        'params' => ['zone'],
                        'placeholder' => '-----ทั้งหมด-----',
                        'url' => '/ckd/report/get-province',
                        'allowClear' => true,
                    ]
                ]);
                ?>
            </div>
            <div class="col-md-5" id="div-amphur">
                <h5 class="section-text" id="label-select">อำเภอ :</h5>
                <?php
                echo DepDrop::widget([
                    'type' => DepDrop::TYPE_SELECT2,
                    'name' => 'amphur',
                    'id' => 'amphur',
                    'data' => [$province_select . $amphur_select => 'selected'],
                    'select2Options' => ['pluginOptions' => ['allowClear' => true]],
                    'pluginOptions' => [
                        'depends' => ['province'],
                        'initialize' => true,
                        'initDepends' => ['province'],
                        'placeholder' => '-----ทั้งหมด-----',
                        'url' => Url::to(['/ckd/report/get-amphur', 'code' => $province_select . $amphur_select]),
                        'params' => ['province'],
                        'allowClear' => true,
                    ]
                ]);
                ?>
            </div>

            <div class="col-md-5" id="div-hospital">
                <h5 class="section-text" id="label-select">โรงพยาบาล :</h5>
                <?php
//echo DepDrop::widget([
//    'type' => DepDrop::TYPE_SELECT2,
//    'name' => 'hospital',
//    'id' => 'hospital',
//    'select2Options' => ['pluginOptions' => ['allowClear' => true]],
//    'pluginOptions' => [
//        'depends' => ['amphur'],
//        'initialize' => true,
//        'initDepends' => ['amphur'],
//        'params' => ['province', 'amphur'],
//        'placeholder' => '-----ทั้งหมด-----',
//        'url' => '/ckd/report/get-hospital',
//    ]
//]);
                ?>
                <?php
                echo Select2::widget([
                    'name' => 'hospital',
                    'id' => 'hospital',
                    'options' => ['placeholder' => 'เลือกหน่วยงาน...'],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'minimumInputLength' => 0, //ต้องพิมพ์อย่างน้อย 3 อักษร ajax จึงจะทำงาน
                        'ajax' => [
                            'url' => '/ckd/report/get-hospital2',
                            'dataType' => 'json', //รูปแบบการอ่านคือ json
                            'data' => new JsExpression('function(params) { 
                            var codes = $("#amphur").val();
                            var zone = $("#zone-choice").val();
                            var amphur =codes.substring(2,4);
                            var province =$("#province").val();
                            //console.log(zonecode+" "+amphur+" "+province);
                            return {q:params.term, zone, province, amphur}; 
                            }
                         '),
                        ],
                        'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                        'templateResult' => new JsExpression('function(city) { return city.text; }'),
                        'templateSelection' => new JsExpression('function (city) { return city.text; }'),
                    ]
                ]);
                ?>
            </div>
            <!--            <div class="clearfix"></div>
                        <div class="col-md-5" id="div-tumbon-service" style="display: none">
                            <h5 class="section-text">ตำบล/หน่วยบริการ :</h5>
            <?php
//                echo Select2::widget([
//                    'name' => 'tumbon-service',
//                    'id' => 'tumbon-service',
//                    'hideSearch' => true,
//                    'value' => '0',
//                    'data' => ['0' => 'หน่วยบริการ', '1' => 'ตำบล'],
////                    'options' => [
////                        'prompt' => '-----ทั้งหมด-----',
////                    ],
////                    'pluginOptions' => [
////                        'allowClear' => true
////                    ],
//                ]);
            ?>
                        </div>-->

            <!--            <div class="col-md-5" id="div-tumbon" style="display: none">
                            <h5 class="section-text">ตำบล :</h5>
            <?php
//                echo DepDrop::widget([
//                    'type' => DepDrop::TYPE_SELECT2,
//                    'name' => 'tumbon',
//                    'id' => 'tumbon',
//                    'select2Options' => ['pluginOptions' => ['allowClear' => true]],
//                    'pluginOptions' => [
//                        'depends' => ['amphur','tumbon-service'],
//                        'initialize' => true,
//                        'initDepends' => ['amphur','tumbon-service'],
//                        'params' => ['province', 'amphur','tumbon-service'],
//                        'placeholder' => '-----ทั้งหมด-----',
//                        'url' => '/ckd/report/get-tumbon-service',
//                    ]
//                ]);
            ?>
                        </div>-->

            <!--            <div class="clearfix"></div>
                        <div class="col-md-5" id="div-network-service" style="display: none">
                            <h5 class="section-text">เครือข่ายบริการ :</h5>
            <?php
//                echo DepDrop::widget([
//                    'type' => DepDrop::TYPE_SELECT2,
//                    'name' => 'network-service',
//                    'id' => 'network-service',
//                    'select2Options' => ['pluginOptions' => ['allowClear' => true]],
//                    'pluginOptions' => [
//                        'depends' => ['province'],
//                        'initialize' => true,
//                        'initDepends' => ['province'],
//                        'params' => ['province'],
//                        'placeholder' => '-----ทั้งหมด-----',
//                        'url' => '/ckd/report/get-network',
//                    ]
//                ]);
            ?>
            
                        </div>
                        <div class="col-md-5" id="div-service" style="display: none">
                            <h5 class="section-text">สถานบริการ :</h5>
            <?php
//                echo DepDrop::widget([
//                    'type' => DepDrop::TYPE_SELECT2,
//                    'name' => 'service',
//                    'id' => 'service',
//                    'select2Options' => ['pluginOptions' => ['allowClear' => true]],
//                    'pluginOptions' => [
//                        'depends' => ['amphur','tumbon-service'],
//                        'initialize' => true,
//                        'initDepends' => ['amphur','tumbon-service'],
//                        'params' => ['province', 'amphur','tumbon-service'],
//                        'placeholder' => '-----ทั้งหมด-----',
//                        'url' => '/ckd/report/get-tumbon-service',
//                    ]
//                ]);
            ?>
            
                        </div>-->

            <div class="clearfix"></div>
            <br/>
            <div class="col-md-12">
                <label id="dateCalculate"></label>
            </div>
            <br/>
            <div class="clearfix"></div>
            <div class="col-md-4">
                <button type="button" id="searchData" class="btn btn-primary"><i class="glyphicon glyphicon-search"></i> แสดงรายงาน</button>
                <!--<button type="button" id="mySearchData" class="btn btn-success"><i class="glyphicon glyphicon-search"></i> หน่วยงานของฉัน</button>-->
            </div>
            <div class="clearfix"></div>
            <?php ActiveForm::end() ?>
        </div>
        <br/>

        <div style="padding-left: 10px; padding-right: 10px; padding-top: 20px;">
            <div id="showData">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h4>เขตสุขภาพ</h4>
                    </div>
                    <div class="panel-body">
                        <div id="showGraph">

                        </div>
                    </div>
                </div>

                <div class="panel panel-success">
                    <div class="panel-heading">
                        <h4>จำนวนผู้ป่วยโรคไตเรื้อรังที่มารับบริการที่โรงพยาบาล จำแนกตาม Stage</h4>
                    </div>
                    <div class="panel-body">
                        <div id="showTable">

                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <p style="color: red;"> หมายเหตุ ::</p> 
                <p>ผู้ป่วยโรคไตเรื้อรัง หมายถึง ผู้ป่วยจากแฟ้ม DIAGNOSIS_OPD ที่มีรหัสโรคเป็น (N181-184, (N189 ที่ไม่มี eGFR หรือมี eGFR>=15)) หรือ ( (E102, E112, E122, E132, E142 หรือ I12*, I13*, I151) ที่มี eGFR>= 15 )</p>
                <p> <?php
                    if ($_GET['page'] == '16') {
                        echo'********* ผู้ป่วย 1 คน สามารถเป็นผู้รับริการได้มากกว่า 1 โรงพยาบาล ******';
                    }
                    if ($_GET['page'] == '17') {
                        echo'********* ผู้ป่วย 1 คน จะมีเพียง 1 Record จากการตัดความซ้ำซ้อนด้วยเลขบัตรประชาชนและ Typearea ****** ';
                    }
                    ?> </p>

                <p>การแบ่ง Stage แบ่งจากค่า cretinine ครั้งสุดท้ายที่แปลงเป็น egfr /egfr ครั้งสุดท้ายที่ตรวจ ตามช่วงดังนี้ </p>
                <p>- stage 1 >= 90 <p>
                <p>- stage 2 = 60-89.99 <p>
                <p>- stage 3 = 30-59.99 <p>
                <p>- stage 4= 15-29.99 <p>
                <p>- stage 5 น้อยกว่า 15<p>
            </div>
        </div>

        <!--        <div style="padding-left: 30px; padding-right: 30px;">
                    <b>
                        <p>หมายเหตุ :: </p>
                        <p>ผู้ป่วยโรคไตเรื้อรัง หมายถึง ผู้ป่วยจากแฟ้ม DIAGNOSIS_OPD ที่มีรหัสโรคเป็น (N181-184, (N189 ที่ไม่มี eGFR หรือมี eGFR>=15)) หรือ ( (E102, E112, E122, E132, E142 หรือ I12*, I13*, I151) ที่มี eGFR>= 15 )</p>
                        <p>   ********* ผู้ป่วย 1 คน สามารถเป็นผู้รับริการได้มากกว่า 1 โรงพยาบาล ****** </p>
        
                        <p>การแบ่ง Stage แบ่งจากค่า cretinine ครั้งสุดท้ายที่แปลงเป็น egfr /egfr ครั้งสุดท้ายที่ตรวจ ตามช่วงดังนี้ </p>
                        <p>- stage 1 >= 90 </p>
                        <p>- stage 2 = 60-89.99 </p>
                        <p>- stage 3 = 30-59.99 </p>
                        <p>- stage 4= 15-29.99 </p>
                        <p>- stage 5 น้อยกว่า 15 </p>
                        <p>วันที่ประมวลผล :: 19 เมษายน 2560 </p>
                    </b>
                </div>
            </div>-->
        <?php
        echo ModalForm::widget([
            'id' => 'modal-drilldown',
            'size' => 'modal-lg'
        ]);
        ?>

        <?php
        $this->registerJs("
    

    $(document).ready(function(){
        
//        $('#showGraph').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
//        $('#showTable').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
//        getGraph();
        
    });
    
    function getGraph(){
     
         $.ajax({
            url:'" . Url::to(['report/get-graph']) . "',
            method:'POST',
            data:$('#formSearch').serialize(),
            dataType:'HTML',
            success:function(result){
               console.log(result);
               $('#showData').html(result);
//               $('#dateCalculate').html($('#dateCal').val());
            },error: function (xhr, ajaxOptions, thrownError) {
               console.log(xhr);
            }
        });
        return false;
    }
    
    function getLoading(){
        var _html ='';
        _html += \"<div class='panel panel-info'>\";
        _html +=    \"<div class='panel-heading'>\";
                 _html +=       \"<h4>เขตสุขภาพ</h4>\";
                _html +=    \"</div>\";
                _html +=    \"<div class='panel-body'>\";
                _html +=        \"<div id='showGraph'>\";
                          
                _html +=        \"</div>\";
                 _html +=   \"</div>\";
                _html += \"</div>\";

               _html +=\" <div class='panel panel-success'>\";
                 _html +=\"   <div class='panel-heading'>\";
                _html +=        \"<h4>จำนวนผู้ป่วยโรคไตเรื้อรังที่มารับบริการที่โรงพยาบาล จำแนกตาม Stage</h4>\"
                _html +=\"    </div>\";
                _html +=\"    <div class='panel-body'>\";
                 _html +=\"       <div id='showTable'>\";

                    _html +=\"    </div>\";
                 _html +=\"   </div>\";
               _html +=\" </div>\";
               
        $('#showData').html(_html);
        $('#showGraph').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
        $('#showTable').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
//        $('#dateCalculate').html('กำลังประมวลผล  กรุณารอซักครู่...');
    }
    
    $('#searchData').on('click',function(){
//        $('#calculate').val('0');
        getLoading();
        getGraph();
    });
    
 $('#mySearchData').on('click',function(){
//        $('#calculate').val('0');
        getLoading();
        getGraph();
    });
    
    
    $('#zone').on('change',function(){
        if($(this).val() == null || $(this).val() == ''){
            $('#province').val(null).trigger('change');
            $('#hospital').val(null).trigger('change');
            $('#amphur').val(null).trigger('change');
            $('#province').attr('disabled','disabled');
//            $('#hospital').attr('disabled','disabled');
            $('#amphur').attr('disabled','disabled');
        }else{
            $('#hospital').val(null).trigger('change');
        }
    });
    
     $('#province').on('change',function(){
        if($(this).val() != null || $(this).val() != ''){
            $('#hospital').val(null).trigger('change');
        }
     });
     $('#amphur').on('change',function(){
        if($(this).val() != null || $(this).val() != ''){
        getLoading();
        getGraph();
            $('#hospital').val(null).trigger('change');
        }
    });
    

  
    
");
        ?>