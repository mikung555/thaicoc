<?php

use kartik\tabs\TabsX;
use kartik\widgets\Select2;
use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\helpers\Url;
use appxq\sdii\widgets\GridView;
use appxq\sdii\widgets\ModalForm;
use appxq\sdii\helpers\SDNoty;
use appxq\sdii\helpers\SDHtml;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
$this->title = Yii::t('backend', 'Module: CKDNET');
$this->params['breadcrumbs'][] = ['label' => 'Modules', 'url' => yii\helpers\Url::to('/tccbots/my-module')];
$this->params['breadcrumbs'][] = Yii::t('backend', 'CKDNET');
?>
<div class="ckdnet-default-index">
<?php
echo TabsX::widget([
    'items' => $items,
    'position' => TabsX::POS_ABOVE,
    'encodeLabels' => false
]);
?>
<?php
$form = ActiveForm::begin([
	    'id' => 'jump_menu',
	    'action' => ['index', 'state'=>$state],
	    'method' => 'get',
	    'layout' => 'horizontal',
	]);
?>

    <div class="row" style="margin-bottom: 15px;">
	<div class="col-sm-6">
	    <?=  Html::label('กุญแจถอดรหัสข้อมูล')?>
	    <?=  Html::passwordInput('key', isset(Yii::$app->session['key_ckd'])?Yii::$app->session['key_ckd']:'', ['class'=>'form-control', 'placeholder'=> 'กรุณากรอกกุญแจถอดรหัส', 'required'=>true])?>
	</div>
    </div>

	<?= Html::checkbox('save_keyckd', isset(Yii::$app->session['save_keyckd'])?Yii::$app->session['save_keyckd']:false, ['label'=>'จดจำคีย์นี้'])?>
	<?= Html::checkbox('convert', isset(Yii::$app->session['convert_ckd'])?Yii::$app->session['convert_ckd']:false, ['label'=>'เข้ารหัสแบบ tis620 (กรณีชื่อ-สกุล อ่านไม่ออกให้กาช่องนี้ด้วย)'])?>

    <div class="row" style="margin-bottom: 15px;">
	<div class="col-sm-6">
	    <?= Html::submitButton(Yii::t('app', 'Submit'), ['class' => 'btn btn-primary']) ?>
	</div>
    </div>

    <div class="row" style="margin-bottom: 15px;">
	<div class="col-sm-6">
		<label class="control-label">Workspace</label>

		<?php

		echo Select2::widget([
		    'name' => 'state',
		    'value' => $state,
		    'data' => $dataList,
		    'options' => [
			'class' => 'form-control',
			'multiple' => false,
			'placeholder' => 'Select workspace ...',
			//'prompt' => 'All',
			'onChange' => '$("#jump_menu").submit()'
		    ]
		]);
		?>


	</div>

    </div>

    <?php ActiveForm::end(); ?>

    <?php  Pjax::begin(['id'=>'fperson-grid-pjax']);?>
    <?= GridView::widget([
	'id' => 'fperson-grid',
	'panelBtn' => '',
	'dataProvider' => $dataProvider,
	//'filterModel' => $searchModel,
        'columns' => [
	    [
		'class' => 'yii\grid\CheckboxColumn',
		'checkboxOptions' => [
		    'class' => 'selectionFPersonIds'
		],
		'headerOptions' => ['style'=>'text-align: center;'],
		'contentOptions' => ['style'=>'width:40px;text-align: center;'],
	    ],

	    [
		'class' => 'yii\grid\SerialColumn',
		'headerOptions' => ['style'=>'text-align: center;'],
		'contentOptions' => ['style'=>'width:60px;text-align: center;'],
	    ],
	    [
		'attribute'=>'CID',
		'headerOptions'=>['style'=>'text-align: center;'],
		'contentOptions'=>['style'=>'width:120px; text-align: center;'],
	    ],
	    [
		'attribute'=>'Pname',
		'headerOptions'=>['style'=>'text-align: center;'],
		'contentOptions'=>['style'=>'width:80px; '],
	    ],
	    [
		'attribute'=>'Name',
		'headerOptions'=>['style'=>'text-align: center;'],
		'contentOptions'=>['style'=>'width:200px; '],
	    ],
	    [
		'attribute'=>'Lname',
		'headerOptions'=>['style'=>'text-align: center;'],
		'contentOptions'=>['style'=>'width:200px; '],
	    ],

            [
		'header'=>'EMR',
		'format'=>'raw',
		'value'=>function ($data){
		    return Html::a('EMR', Url::to(['/ckd/emr/index', 'ptlink'=>$data['ptlink'], 'hpcode'=>$data['hospcode'],'pid'=>$data['pid']]));
		},
		'headerOptions'=>['style'=>'text-align: center;'],
		'contentOptions'=>['style'=>'width:80px; text-align: center;'],
	    ],
	[
		'header'=>'Last eGFR',
		'format'=>'raw',
                'value' =>function ($data){
//                    \appxq\sdii\utils\VarDumper::dump($data['cva_code']);
                    if($data['egfr'] == null){
                        return " ";
                    }else{
                        return $data['egfr'];  
                    }
                }, 
//		'value'=>function ($data){
//                    
//		    try {                        
//                        $egfr =  backend\modules\ckdnet\classes\CkdnetQuery::getPatientProfileHospitalFieldOne($data['HOSPCODE']==null?$data['hospcode']:$data['HOSPCODE'], $data['PID']==null?$data['pid']:$data['PID'], 'egfr');
//                        return number_format($egfr['egfr'],1);
//                    } catch (\yii\db\Exception $e) {
//                        return null;
//                    }
//		},
		'headerOptions'=>['style'=>'text-align: center;'],
		'contentOptions'=>['style'=>'width:80px; text-align: right;'],
	    ],
	[
		'header'=>'CVD Risk',
		'format'=>'raw',
                'value' =>function ($data){
//                    \appxq\sdii\utils\VarDumper::dump($data['cva_code']);
                    if($data['cvd_risk'] == null){
                        return " ";
                    }else{
                        return $data['cvd_risk'];  
                    }
                }, 
//		'value'=>function ($data){
//		    try {
//                        $cvd_risk = backend\modules\ckdnet\classes\CkdnetQuery::getPatientProfileHospitalFieldOne($data['HOSPCODE']==null?$data['hospcode']:$data['HOSPCODE'], $data['PID']==null?$data['pid']:$data['PID'], 'cvd_risk');
//                        return number_format($cvd_risk['cvd_risk'],1);
//                    } catch (\yii\db\Exception $e) {
//                        return null;
//                    }
//		},
		'headerOptions'=>['style'=>'text-align: center;'],
		'contentOptions'=>['style'=>'width:80px; text-align: right;'],
	    ],
	[
		'header'=>'CVA',
		'format'=>'raw',
                'value' =>function ($data){
//                    \appxq\sdii\utils\VarDumper::dump($data['cva_code']);
                    if($data['cva_code'] == null){
                        return " ";
                    }else{
                        return $data['cva_code'];  
                    }
                }, 
//		'value'=>function ($data){
//		    try {
//                        $cva =  backend\modules\ckdnet\classes\CkdnetQuery::getPatientProfileHospitalFieldOne($data['HOSPCODE']==null?$data['hospcode']:$data['HOSPCODE'],  $data['PID']==null?$data['pid']:$data['PID'], 'cva_code');
//                        return $cva['cva_code'];
//                    } catch (\yii\db\Exception $e) {
//                        return null;
//                    }
//		},
		'headerOptions'=>['style'=>'text-align: center;'],
		'contentOptions'=>['style'=>'width:80px; text-align: right;'],
	    ],
	[
		'header'=>'CVA DATE',
		'format'=>'raw',
                'value' =>function  ($data){
                    return substr($data['cva_date'],0,10);
                },
//                'value'=>function ($data){
//		    try {
//                        $cva_date = backend\modules\ckdnet\classes\CkdnetQuery::getPatientProfileHospitalFieldOne($data['HOSPCODE'], $data['PID'], 'cva_date');
//                        return substr($cva_date['cva_date'],0,10);
//                    } catch (\yii\db\Exception $e) {
//                        return null;
//                    }
//		},
		'headerOptions'=>['style'=>'text-align: center;'],
		'contentOptions'=>['style'=>'width:80px; text-align: right;'],
	    ],


//	    [
//		'header'=>'บันทึกผล',
//		'format'=>'raw',
//		'value'=>function ($data){
//		    $rowReturn = '';
//
//		    //<span class=\"label label-default\" style=\"font-size: 13px;\">0 / 0</span>
//		    //<span class=\"label label-warning\" style=\"font-size: 13px;\">0 / 0</span>
//		    $rowReturn .= Html::a("<span class=\"label label-default\" style=\"font-size: 13px;\">0 / 0</span> ", Url::to(['/inputdata/step4',
//			'ezf_id'=>'ezf_id',
//			'target'=>base64_encode('target'),
//			'comp_id_target'=>'comp_id_target',
//			'rurl'=>base64_encode(Yii::$app->request->url),
//		    ]), [
//			'data-toggle'=>'tooltip',
//			'title'=>'(Submit/Save Draft) คลิกเพื่อแสดง EMR',
//		    ]);
//
//		    $rowReturnAdd = Html::a('<i class="glyphicon glyphicon-plus" style="color:#00a65a;font-size: 18px;"></i> ', Url::to(['/inputdata/insert-record',
//			'insert'=>'url',
//			'ezf_id'=>'ezf_id',
//			'target'=>base64_encode('target'),
//			'comp_id_target'=>'comp_id_target',
//			'rurl'=>base64_encode(Yii::$app->request->url),
//		    ]), [
//			'data-toggle'=>'tooltip',
//			'title'=>'เพิ่มข้อมูล',
//		    ]);
//
//		    return $rowReturn.$rowReturnAdd;
//		},
//		'headerOptions'=>['style'=>'text-align: center;'],
//		'contentOptions'=>['style'=>'width:120px; text-align: center;'],
//	    ],

	// Last eGFR	CVD Risk	CVA	Urine Protine
//            'HN',
            // 'sex',
            // 'Birth',
            // 'Mstatus',
            // 'Occupation_Old',
            // 'Occupation_New',
            // 'Race',
            // 'Nation',
            // 'Religion',
            // 'Education',
            // 'Fstatus',
            // 'Father',
            // 'Mother',
            // 'Couple',
            // 'Vstatus',
            // 'MoveIn',
            // 'Discharge',
            // 'Ddischarge',
            // 'ABOGROUP',
            // 'RHGROUP',
            // 'Labor',
            // 'PassPort',
            // 'TypeArea',
            // 'D_Update',
            // 'ptlink',

//	    [
//		'class' => 'appxq\sdii\widgets\ActionColumn',
//		'contentOptions' => ['style'=>'width:80px;text-align: center;'],
//		'template' => '{view} {update} {delete}',
//	    ],
        ],
    ]); ?>
    <?php  Pjax::end();?>

</div>

<?=  ModalForm::widget([
    'id' => 'modal-fperson',
    'size'=>'modal-lg',
]);
?>

<?php  $this->registerJs("
$('#fperson-grid-pjax').on('click', '#modal-addbtn-fperson', function() {
    modalFPerson($(this).attr('data-url'));
});

$('#fperson-grid-pjax').on('click', '#modal-delbtn-fperson', function() {
    selectionFPersonGrid($(this).attr('data-url'));
});

$('#fperson-grid-pjax').on('click', '.select-on-check-all', function() {
    window.setTimeout(function() {
	var key = $('#fperson-grid').yiiGridView('getSelectedRows');
	disabledFPersonBtn(key.length);
    },100);
});

$('#fperson-grid-pjax').on('click', '.selectionFPersonIds', function() {
    var key = $('input:checked[class=\"'+$(this).attr('class')+'\"]');
    disabledFPersonBtn(key.length);
});

function disabledFPersonBtn(num) {
    if(num>0) {
	$('#modal-delbtn-fperson').attr('disabled', false);
    } else {
	$('#modal-delbtn-fperson').attr('disabled', true);
    }
}

$('#fperson-grid-pjax').on('click', '.btn-view', function() {
    var param = jQuery.parseJSON($(this).parent().parent().attr('data-key'));
    modalView(param, '".Url::to(['/managedata/managedata/ezform', 'table'=>'f_person', 'ezf_id'=>'1470198830071534100'])."');
});

$('#fperson-grid-pjax').on('dblclick', 'tbody tr', function() {
    var param = jQuery.parseJSON($(this).attr('data-key'));
    modalView(param, '".Url::to(['/managedata/managedata/ezform', 'table'=>'f_person', 'ezf_id'=>'1470198830071534100'])."');
});

function selectionFPersonGrid(url) {
    yii.confirm('".Yii::t('app', 'Are you sure you want to delete these items?')."', function() {
	$.ajax({
	    method: 'POST',
	    url: url,
	    data: $('.selectionFPersonIds:checked[name=\"selection[]\"]').serialize(),
	    dataType: 'JSON',
	    success: function(result, textStatus) {
		if(result.status == 'success') {
		    ". SDNoty::show('result.message', 'result.status') ."
		    $.pjax.reload({container:'#fperson-grid-pjax'});
		} else {
		    ". SDNoty::show('result.message', 'result.status') ."
		}
	    }
	});
    });
}

function modalView(param, url) {
    $('#modal-fperson .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
    $('#modal-fperson').modal('show');
    $.ajax({
	method: 'POST',
	url: url,
	data: param,
	dataType: 'JSON',
	success: function(result, textStatus) {
	    if(result.status == 'success') {
		". SDNoty::show('result.message', 'result.status') ."
		$('#modal-fperson .modal-content').html(result.html);
	    } else {
		". SDNoty::show('result.message', 'result.status') ."
	    }
	}
    });
}

function modalFPerson(url) {
    $('#modal-fperson .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
    $('#modal-fperson').modal('show')
    .find('.modal-content')
    .load(url);
}

");?>
