<?php
use yii\helpers\Html;
?>

<div class="detoxthai-timeline-form">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="itemModalLabel"><?= Html::tag('label','CVA'); ?></h4>
    </div>

    <div class="modal-body"> 
        <div class="row">
            <div class="col-md-12 cva">
                <?php
                    echo $this->render('_cva_item', [
                        'cvaall' =>  $cvaall,
                        'hospcode' => $hospcode, 
                        'ptlink' =>$ptlink, 
                        'nameHospital' =>$nameHospital
                    ]);
                ?>
            </div> 
            <div class="row" id="progress-diag" style="display: none;">
                    <div class="col-md-12 text-center">
                        <div class="sdloader" ><i class="sdloader-icon"></i></div>
                    </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <div class="text-center">
            <button class="btn btn-success" id="get-more" data-url="<?= yii\helpers\Url::to(['cva-item',
            'hospcode' => $hospcode, 
            'ptlink' =>$ptlink, 
            ])?>">แสดงเพิ่มเติม</button>
        </div>
    </div>
   
</div>

<?php  $this->registerJs("

$('#get-more').on('click', function() {
    var url = $(this).attr('data-url');
    getItemDrug(url);
});

function getItemDrug(url) {
    $('#progress-diag').show();
    
    $.ajax({
        method: 'POST',
        url:url,
        dataType: 'HTML',
        success: function(result) {
            $('.cva').append(result);
            console.log(result);
            $('#progress-diag').hide();

        }
    });
}

");?>