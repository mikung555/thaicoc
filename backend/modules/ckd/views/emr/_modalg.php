<?php 
    //print_r($_GET);
?>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <br>
</div>
<div class="modal-body">
    <div class="row">
<?php echo
    $this->renderAjax("/emr/modal_graph", [
        'hospcode' => $hospcode,
        'ptlink' => $ptlink,
        'pid' => Yii::$app->session['emr_pid'],
        'graph' => $graph,
        'cid' => $cid,
        'id'=>$id
    ]);
 ?>
    </div>
</div>