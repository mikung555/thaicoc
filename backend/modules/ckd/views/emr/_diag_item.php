<?php
if($data){
    $color = 'info';
    foreach ($data as $key => $value) {
        $typeout = backend\modules\ckdnet\classes\CkdnetFunc::itemAlias('typeout', $value['typeout']);
        ?>
<div class="panel panel-<?=$color?>">
  <div class="panel-heading">
      <h4><?= \common\lib\sdii\components\utils\SDdate::mysql2phpThDate($value['date_serv'].' '.$value['time_serv'])?> ( <?=$value['HOSPCODE']?> <?=$nameHospital?> : แผนก <?=$typeout?> )</h4>
  </div>
  <div class="panel-body">
    <?php echo $this->render('_diagnose', [
                                    'one_serv'      =>  $value,
                                    'hospcode'      =>  $hospcode,
                                    'ptlink'        =>  $ptlink,
                                    'drugallergy'  =>  $drugallergy,
                                ]);
    if($color=='info'){
        $color='warning';
    } else {
        $color = 'info';
    }
    ?>
  </div>
</div>
       
<?php
    }
}
?>