<?php
use yii\bootstrap\Modal;
use kartik\tabs\TabsX;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use appxq\sdii\widgets\ModalForm;
use backend\modules\ckd\classes\NutDialog;
/* @var $this yii\web\View */
$this->title = Yii::t('backend', 'CHRONIC KIDNEY DISEASE PREVENTION IN 
THE NORTHEAST OF THAILAND');
$this->params['breadcrumbs'][] = ['label'=>'Modules','url'=>  Url::to('/tccbots/my-module')];
$this->params['breadcrumbs'][] = Yii::t('backend', 'CKDNET');

//yii\helpers\VarDumper::dump($patient,10,true);exit;
//echo \Yii::$app->request->getUrl() . Url::to('/ckd/emr/ckd?ptlink='.$patient->ptlink.'&hospcode='.$hospcode);exit;
//$items2 = [
//    [
//        'label'=>'<i class="fa fa-user"></i> EMR',
//        'content'=>$this->renderAjax('ckd',[
//            'hospcode'      => $hospcode,
//            'ptlink'        => $ptlink,
//            'patient'=>$patient,
//            'cid' => $cid,
//            ]),
//        'active'=>$tab=='ckd',
//        'linkOptions'=>['data-url'=>Url::to(['/ckd/emr/ckd', 'tab'=>'ckd', 
//            'hospcode'      => $hospcode,
//            'ptlink'        => $ptlink,
//            'cid' => $cid,
//        ])]
//    ],
////    [
////        'label'=>'<i class="fa fa-user-plus"></i> PHR',
////        'content'=>$this->renderAjax('phr',['cid'=>$cidhex, 'ptlink'=>$ptlink, 'hospcode'=>$hospcode]),
////        'active'=>$tab=='phr',
////        'linkOptions'=>['data-url'=>Url::to(['/ckd/emr/phr', 'tab'=>'phr', 'ptlink'=>$ptlink, 'hospcode'=>$hospcode])]
////    ],
//];


?>
<!--<div>
    <button id="btnTest" data-url="<?= Url::to(['/ckd/graph/test'])?>" class="btn btn-primary">Test</button>
</div>-->
<?php $this->registerJS("
//   $('#btnTest').click(function(){
//        var url = $(this).attr('data-url');
//        $.ajax({url:url, 
//            success:function(data){
//                alert(data);
//            }
//        })
//   }); 
")?>


<div class="ckdnet-default-index">

<!--<div class="text-right">  
    <?php
        $form = ActiveForm::begin([
	    'id' => 'jump_menu',
	    'action' => ['index'],
	    'method' => 'get',
	    'layout' => 'inline',
	]);
?>
    <div class="form-group">
        <div class="input-group margin-bottom-sm">
            <span class="input-group-addon"><i class="fa fa-user fa-fw"></i></span>
            <input class="form-control" type="text" name="cid" value="<?=$cid?>" placeholder="เลขที่บัตรประชาชน">
          </div> 
    </div>
    
    <?= Html::submitButton('<i class="fa fa-search"></i> ค้นหา', ['class' => 'btn btn-primary']) ?>
    <?=Html::button('<i class="fa fa-key"></i> ถอดรหัส', ['data-url'=>Url::to(['/ckdnet/emr/addkey']), 'class' => 'btn btn-warning', 'id'=>'modal-decode-btn'])?>
    <?php ActiveForm::end(); ?>
    
</div>-->
    
<?php   
//\appxq\sdii\utils\VarDumper::dump("hospcode: ".$hospcode." & ptlink:".$ptlink." & patient:".$patient." & cid: ".$cid." & tab: ".$tab." & tab_idemr: ".$tab_idemr);
echo $this->render('index1',[
    'hospcode' => $hospcode,
    'ptlink' => $ptlink,
    'patient' =>$patient,
    'cid' => $cid,
    'tab' => $tab,
    'tab_idemr' => $tab_idemr,
    'nameHospital' =>$nameHospital
]);
//if($patient){
//    echo TabsX::widget([
//        'id'=>'items2',
//        'items'=>$items2,
//        'position'=>TabsX::POS_ABOVE,
//        'encodeLabels'=>false
//    ]);
//} else {
//    //echo '<code>โรงพยาบาลที่ท่านเข้ารับบริการยังไม่เข้าร่วม Thai Care Cloud</code>';
//    echo '<code>ไม่พบข้อมูลผู้ป่วย</code>';
//}
 

?>

</div>

 