<?php
use backend\modules\ckdnet\classes\CkdnetQuery;
use backend\modules\ckdnet\classes\CkdnetFunc;
//appxq\sdii\utils\VarDumper::dump($one_serv);
?>

<table class="table ">
    <tbody>
        <tr>
            <td>BP:</td>
            <th><?= $one_serv['sbp'] ?>/<?= $one_serv['dbp'] ?></th>
            <td>Pulse:</td>
            <th><?= $one_serv['pr'] ?></th>
        </tr>
        <tr>
            <td>RR:</td>
            <th><?= $one_serv['rr'] ?></th>
            <td>Temp:</td>
            <th><?= $one_serv['btemp'] ?></th>
        </tr>
        <tr>
            <?php
            $ww = $one_serv['BW'];
            $hh = $one_serv['HEIGHT'];
            ?>
            <td>น้ำหนัก/ส่วนสูง:</td>
            <th><?= number_format($one_serv['BW'], 2) ?>/<?= number_format($one_serv['HEIGHT']) ?></th>
            <td>BMI:</td>

            <th><?= number_format(($ww / pow(($hh / 100), 2)), 2) ?></th>
        </tr>
    </tbody>
</table>
<p>
    <strong>CC: </strong> <?= $one_serv['chiefcomp'] ?>
</p>

<h4><span class="label label-warning">ลงผลการวินิจฉัย</span></h4>

        <?php
                    
        $data_diag = CkdnetQuery::getDiagOne($one_serv['date_serv'], $ptlink, $hospcode);
        if($data_diag){
            $type = '';
            foreach ($data_diag as $key => $value) {
                $data_icd10 = CkdnetQuery::getIcd10($value['diag_code']);
            ?>
                <p>
                    <?php
                    if($value['DiagType']!=$type){
                        echo '<strong>'.CkdnetFunc::itemAlias('DiagType', $value['DiagType']).'</strong><br>';
                        $type = $value['DiagType'];
                    }
                    ?>
                    &nbsp; &nbsp; &nbsp; <?=$value['diag_code']?> : <?=$data_icd10['name']?>
                </p>

        <?php
        }
        
            } else {
            echo 'ไม่พบข้อมูล';
        }
        
        ?>
    

