<?php

use yii\helpers\Html;
use yii\bootstrap\Modal;
use miloschuman\highcharts\Highcharts;
use yii\helpers\ArrayHelper;
use backend\modules\ckdnet\classes\CkdnetQuery;
use common\lib\sdii\components\utils\SDdate;
use yii\helpers\Url;
use backend\modules\ckd\classes\NutDialog;
use yii\web\JsExpression;
use appxq\sdii\widgets\ModalForm;
use appxq\sdii\helpers\SDNoty;
use appxq\sdii\helpers\SDHtml;
use yii\widgets\Pjax;
use backend\modules\ckdnet\classes\CkdnetFunc;
use miloschuman\highcharts\HighchartsAsset;
use kartik\tabs\TabsX;

HighchartsAsset::register($this)->withScripts('highcharts');
Pjax::begin(['id' => 'graph-pjax']);
\Yii::$app->session["ptlinks"] = $ptlink;

//echo $hospcode;
//yii\helpers\VarDumper::dump($patient,10,true);
if ($patient) {

    $ptname = $patient['Name'];
    $ptsurname = $patient['Lname'];
    $ptcid = $patient['CID'];
    $ptdupdate = str_replace('T', ' ', $patient['D_Update']);
    $ptbirth = substr($patient['Birth'], 0, 10);
    $ptsex = $patient['sex'] == 1 ? "ชาย" : "หญิง";
    $ptabogroup = $patient['ABOGROUP']; //iconv("tis-620", "utf-8", $patient['ABOGROUP'] );
    $pthn = $patient['HN'];

    $addressCode = $patient['CHANGWAT'] . $patient['AMPUR'] . $patient['TAMBON'];
    $addressArr = CkdnetQuery::getProvinceAmphur($addressCode);



    $addressStr = '';
    if ($addressArr) {
        $addressStr = ' ต.<code>' . $addressArr['district'] . '</code> อ.<code>' . $addressArr['amphur'] . '</code> จ.<code>' . $addressArr['province'] . '</code> ' . $addressArr['zipcode'];
    }

//\appxq\sdii\utils\VarDumper::dump($addressArr);

    if ($ptbirth != "") {
        $ptagey = DateTime::createFromFormat('Y-m-d', $ptbirth)
                        ->diff(new DateTime('now', $tz))
                ->y;
        $ptagem = DateTime::createFromFormat('Y-m-d', $ptbirth)
                        ->diff(new DateTime('now', $tz))
                ->m;
        $ptaged = DateTime::createFromFormat('Y-m-d', $ptbirth)
                        ->diff(new DateTime('now', $tz))
                ->d;
    }

    // == Move_data_service check visit >> ckd_visit == 
    
    $graph = isset($_GET['graph']) ? $_GET['graph'] : Yii::$app->session['emr_grah'];
    Yii::$app->session['emr_grah'] = $graph;

//\appxq\sdii\utils\VarDumper::dump($data_service);
    $xcid = $cid == "" ? $ptcid : $cid;
    ?>
     <!-- Move_Show visit >> ckd_visit ==  -->
        
        <div class="col-md-9 col-sm-9 col-lg-9">
            <div class="panel panel-primary">
                <div class="panel-heading">

                    <h3 class="panel-title"><b>ข้อมูลเบื้องต้น </b>
                        <?php if($tab == 'ccanet'){ echo "<div class=\"tabcca_regis pull-right\"></div>";  }?>
<!--                        <div class="tabcca_regis pull-right"></div>-->
                    </h3>
                </div>
                <div class="panel-body">
                    <table width="100%" class="table table-striped  table-condensed">
                        <tbody><tr>
                                <td width="40%"><span class="style7">ชื่อ-สกุล : <?= $ptname; ?> <?= $ptsurname; ?></span></td>
                                <td width="32%" colspan="2"><span class="style7">CID : <?= $ptcid; ?></span></td>
                            </tr>
                            <tr>
                                <td><span class="style7">วันเกิด : <code><?php if ($ptbirth != '' ||$ptbirth != NULL){ echo \common\lib\sdii\components\utils\SDdate::mysql2phpThDateSmall($ptbirth);} ?></code> อายุ : <code><?= $ptagey; ?> ปี <?= $ptagem; ?> เดือน <?= $ptaged; ?> วัน </code></span></td>
                                <td><span class="style7">กรุ๊ปเลือด : <?= $ptabogroup; ?></span></td>
                                <td><span class="style7">เพศ : <?= $ptsex; ?></span></td>
                            </tr>
                            <tr>
                                <td><span class="style7">บ้านเลขที่ : <code><?= $patient['HouseNo']; ?></code></span> <span class="style7"> หมู่ : <code><?= $patient['Village']; ?></code></span> <span class="style7">หมู่บ้าน : <code><?= $patient['villaname']; ?></code></span></td>
                                <td colspan="2"><span class="style7">HN : <?= $pthn; ?> </span></td>
                            </tr>
                            <tr>
                                <td><span class="style7"><?= $addressStr ?> </span></td>
                                <td> <span class="style7">โทรศัพท์ : <code><?= $patient['TelePhone']; ?></code></span> <span class="style7"> มือถือ : <code><?= $patient['Mobile']; ?></code></span></td>
                            </tr>
                            <tr>
                                <td colspan="3">


                                    สถานะ <span class="label label-default label-lg">ตรวจแล้ว</span>


                                    <a class="btn btn-success" id="alertmodal">แจ้งเตือนรายบุคคล</a>

                                    <div style="float:right"> 


                                        <?php
                                        echo $this->renderAjax('../doctor-confirm/_confirm', [
                                            'hospcode' => Yii::$app->session['dynamic_connection']['sitecode']
                                        ]);
                                        //nuttaphon chanpan
                                        ?>

                                    </div>    
                                </td>

                            </tr>

                        </tbody></table>
                    
                </div>
            </div>
            <?php //if (Yii::$app->user->can('doctorgen')): ?>
            <!-- doctorgen สร้าง role ใหม่ -->
            <div class="panel panel-primary">
               <div class="panel-heading"> <h3 class="panel-title"><b>สำหรับแพทย์</b></h3></div>

                <div class="panel-body">
                    <?php Pjax::begin(['id' => 'inv-person-grid-pjax']) ?>
                    <?php
                    echo $this->renderAjax('../doctor-confirm/_doctorgen', [
                        'hospcode' => Yii::$app->session['dynamic_connection']['sitecode']
                    ]);
                    //nuttaphon chanpan
                    ?>
                    <?php Pjax::end(); ?>
                </div>
            </div>


            <div class="row">
                <div class="col-md-6 col-sm-6 col-lg-6">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-md-8 col-sm-8 col-lg-8"> <h3 class="panel-title"><b>การวินิจฉัย  <?php
                                        $dshow = '';

                                        $data_dm = CkdnetQuery::getDiagDM($ptlink, $hospcode);
                                        if ($data_dm) {
                                            echo $dshow = 'DM';
                                            $dshow = ', ';
                                        }

                                        $data_ht = CkdnetQuery::getDiagHT($ptlink, $hospcode);
                                        if ($data_ht) {
                                            echo $dshow . 'HT';
                                            $dshow = ', ';
                                        }
                                        $data_hepatitis = CkdnetQuery::getDiagHepatitis($ptlink, $hospcode);
                                        if ($data_hepatitis) {
                                            echo $dshow . 'Hepatitis';
                                        }
                                        
                                       
                                         ?> </b></h3>
                                        <b>จาก</b>  <?php if($hospcode_cup == ''){ echo $hospcode; }else{ echo $hospcode_cup; } ?> <?= $nameHospital ?>
                                        <br><b>วันที่</b> : <?= ($one_serv == '' ? 'ไม่ระบุ' : \common\lib\sdii\components\utils\SDdate::mysql2phpThDate($one_serv)); ?>
                                        </div>
                                <div class="col-md-4 col-sm-4 col-lg-4 text-right">
                                    <a id="showall-diag" class="btn btn-default btn-sm" data-url="<?=
                                    Url::to(['diag-all',
                                        'hospcode' => $hospcode,
                                        'ptlink' => $ptlink,
                                    ])
                                    ?>" style="cursor: pointer;"><i class="fa fa-bars" aria-hidden="true"></i> All</a>
                                </div>
                            </div>
                        </div>
                        <div class="panel-body">
                            <?php
                            $drugallergy = CkdnetQuery::getDrugallergy($ptlink, $hospcode);
                            ?>
                            <?=
                            $this->render('_diagnose', [
                                'one_serv' => $one_serv_data,
                                'hospcode' => $hospcode,
                                'ptlink' => $ptlink,
                                'drugallergy' => $drugallergy,
                            ]);
                            ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <?php                       
                        //echo $tab_idemr."//".$tab;
                        $itemtab = [
                            [
                                'label'=>'<i class="fa fa-list"></i> CKD',
                                'content'=>$this->renderAjax('_tab_ckd',[
                                    'hospcode_cup'=>$hospcode_cup,
                                    'hospcode'=>$hospcode,
                                    'one_serv'=>$one_serv,
                                    'ptlink'=>$ptlink,
                                    'patient'=>$patient,
                                    'nameHospital'=>$nameHospital
                                    
                                ]),
                                'active'=>($tab=='ckd' ? true : false),
                            ],
                            [
                                'label'=>'<i class="fa fa-list"></i> CCA',
                                'content'=>$this->renderAjax('_tab_cca',[
                                    'querycca'=>$querycca
                                ]),
                                'active'=>($tab=='ccanet' ? true : false) ,
                            ],
                        ];
                       
                        echo TabsX::widget([
                            'id'=>'itemtab',
                            'items'=>$itemtab,
                            'position'=>TabsX::POS_ABOVE,
                            'encodeLabels'=>false
                        ]);
                    ?>
                    
                </div>    
                
            </div>




            <div class="row" >

                <div class="col-md-12 col-sm-12 col-lg-12">

                    <div class="pull-right" style="margin:0 0 6px 0;">
                        <?php if (Yii::$app->user->can('administrator') && Yii::$app->user->can('adminsite')): ?>
                            <?php echo Html::button('<i class="fa fa-plus"></i>', ['data-url' => Url::to(['/ckd/graph', 'hpcode' => Yii::$app->session['dynamic_connection']['sitecode']]), 'class' => 'btn btn-success', 'id' => 'btnAddGraph']); ?>
                            <?php
                            echo Html::button("<i class='glyphicon glyphicon-cog'></i>", [
                                'data-url' => yii\helpers\Url::to(['map-lap/index']),
                                //'value' => yii\helpers\Url::to(['map-lap/index'] ),
                                'class' => 'btn btn-warning',
                                'id' => 'btn-modal-maplap'
                            ]);
                            ?>
                        <?php endif; ?>
                    </div>

                    <ul class="nav nav-tabs" id="memnuGraph">
                        <?php
                        $gg = backend\modules\ckdnet\classes\CkdnetQuery::getGraph();
//                        CkdnetFunc::execute("
//                                 set @i=0;
//                                            insert ignore into lab_graph_group_site
//                                            SELECT
//                                            lab_graph_group.id,
//                                            '" . Yii::$app->session['dynamic_connection']['sitecode'] . "' hospcode,
//                                            lab_graph_group.`group`,
//                                            (@i:=ifnull(@i,0)+1)*10 forder
//                                            FROM
//                                            lab_graph_group
//                                ");


                        foreach ($gg as $key_gg => $value_gg) {
                            ?>
                            <li role="presentation" id="tab<?= $value_gg['id'] ?>">
                                <a href="#" id="tab<?= $value_gg['id'] ?>" onclick="return showtab(<?= $value_gg['id'] ?>)">
                                    <?= Html::encode($value_gg['group']) ?>

                                </a>
                            </li>
                            <?php
                        }
                        ?>

                    </ul>
                    <?php
                    if (!empty(\Yii::$app->session["session_tabs"])) {
                        $lg_id = \Yii::$app->session["session_tabs"];
                    } else {
                        $lg_id = 1;
                    }

                    if (!empty(\Yii::$app->session['group_id2'])) {
                        $lg_id = \Yii::$app->session['group_id2'];
                    }
                    //echo \Yii::$app->session["session_tabs"];
                    if(!empty($tab)){
                       if($tab == 'ckd'){
                           $lg_id = 1;
                       }else if($tab == 'ccanet'){
                           $lg_id = 4;
                       }else if ($tab == 'dr'){
                           $lg_id = 5;
                       }
                    }else{
                        $lg_id = 1;
                    }
                    //appxq\sdii\utils\VarDumper::dump($lg_id." //// ".\Yii::$app->session["session_tabs"]."///".\Yii::$app->session['group_id2']);
                    ?>
                    <div id="showGraph_hd">
                    </div>
                    <div id="showGraph_new">
                        <div id="showGraph_new_old"></div>

                        <div id="showGraph_new_home"></div>
                    </div>

                    <?php $this->registerJs("
                        /************ เพิ่ม tab และกราฟ อัตโนมัติ **************/
                            loadTemplateLabGraphGroupSite();
                            loadTemplateLabGraphListSite();
                            function loadTemplateLabGraphGroupSite(){
                                $.ajax({
                                    url:'" . Url::to(['/ckd/graph/save-lab-graph-group-site']) . "',
                                    success:function(data){
                                        //console.log(data);
                                        //console.log('Save Template Lab graph group site');
                                    }
                                });
                            }
                            function saveTemplate(group_id,graph_name,tdc_lab_item_code,cols){
                               $.ajax({
                                    url:'" . Url::to(['/ckd/graph/save-lab-graph-list-site-template']) . "',
                                    type:'post',
                                    data:{
                                        group_id:group_id,
                                        graph_name:graph_name,
                                        tdc_lab_item_code:tdc_lab_item_code,
                                        cols:cols
                                    },
                                    success:function(data){
                                       // console.log(data);
                                       // console.log('Save Template Success');
                                    }
                                }); 
                            }
                            function loadTemplateLabGraphListSite(){
                                $.ajax({
                                    url:'" . Url::to(['/ckd/graph/save-lab-graph-list-site']) . "',
                                    //dataType:'json',    
                                    success:function(data){
                                        //$.each(data,function(k,v){
                                            //saveTemplate(v.group_id,v.graph_name,v.tdc_lab_item_code,v.cols);
                                        //});
                                      //console.log('success');  
                                         
                                    }
                                });
                            }
                        /************ end **************/
                        showtab(" . $lg_id . ");
                        var clickk=0;
                        function getSession(names, values){
                            $.ajax({
                                url:'" . Url::to(['/ckd/graph/set-session']) . "',
                                type:'get',
                                data:{names:names, values:values},
                                success:function(data){
                                   //console.log(data);
                                }
                            });
                        }
                        function showtab(group_id){ 
                             getSession('session_tabs', group_id);
                              var count = " . count($gg) . ";
                              for(var i=1; i<=9999; i++){
                                 $('#tab'+i).removeClass('active');
                              }
                              $('#tab'+group_id).attr('class', 'disable active');
                              if(clickk != group_id){
                                 clickk = group_id;
                                 if(group_id==6){


                                $.ajax({
                                    url:'" . Url::to(['/ckd/graph/showlab']) . "',
                                    type:'GET',
                                    data:{
                                        cid:'" . $cid . "',
                                        ptlink:'" . $ptlink . "',
                                        serv:'" . $serv . "',
                                        group_id:group_id
                                    },
                                    success:function(result){
                                       // console.log(result);
                                        $('#showGraph_new').html(result);

                                    }
                                });//ajax

                                    $.ajax({
                                        url:'" . Url::to(['/ckdpd/report/home-ajax']) . "?ptlink=$ptlink' ,
                                        method:'GET',
                                        dataType:'HTML',
                                        success:function(result){
                                           $('#showGraph_new').append(result);
                                        },error: function (xhr, ajaxOptions, thrownError) {
                                           $('#showGraph_new').append('<span class=\'label label-danger\'>Error:'+'ไม่มีข้อมูล'+'</span>');

                                        }
                                    });

                                 }else if (group_id=='98') {
                                    $.ajax({
                                        url:'" . Url::to(['/ckd/graph/hd-ajax']) . "?ptlink=$ptlink&cid=$xcid' ,
                                        method:'GET',
                                        dataType:'HTML',
                                        success:function(result){
                                           $('#showGraph_new').html(result);
                                        },error: function (xhr, ajaxOptions, thrownError) {
                                           $('#showGraph_new').html('<span class=\'label label-danger\'>Error:'+'ไม่มีข้อมูล'+'</span>');

                                        }
                                    });                                       
                                 }else{
                                            $.ajax({
                                    url:'" . Url::to(['/ckd/graph/showlab']) . "',
                                    type:'GET',
                                    data:{
                                        cid:'" . $cid . "',
                                        ptlink:'" . $ptlink . "',
                                        serv:'" . $serv . "',
                                        group_id:group_id
                                    },
                                    success:function(result){
                                       // console.log(result);
                                        $('#showGraph_new').html(result);

                                    }
                                });//ajax
                                    }

                              }

                                return false;
                            }
                    ") ?>



                </div>

            </div>

            <!--            <div class="row">
                            <div class="panel panel-primary">
                                <div class="panel-heading text-center">
                                    <h3 class="panel-title">Home Data</h3>
                                </div>
                                <div class="" id="bodyHomeData">
                                    <div id="homecontent">ไม่พบข้อมูล</div>
                                </div>
                            </div>
            
                        </div>-->



        </div>


<?php
} else {
    echo '<code>ไม่พบข้อมูล</code>';
}
?>
<?php Pjax::end(); ?>

<?php
$this->registerJs("


      function showgraph_load(){

      //$('#showgraph_load').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
         $.ajax({
                url:'" . Url::to(['/ckd/graph/graph-full']) . "',
                type:'POST',

                data:{
                    hospcode    :   '" . $hospcode . "',
                    ptlink      :   '" . $ptlink . "',
                    pid         :   '" . Yii::$app->session['emr_pid'] . "',
                    graph       :   '" . $graph . "',
                    cid         :   '" . $cid . "',
                },
                success:function(data){
                    $('#showgraph_load').html(data);

                },
                error: function(err) {
                    $('#showgraph_load').html('<span class=\'label label-danger\'>Error:'+err+'</span>');
                }
            });


          return false;
      }
        $('#btn-modal-maplap').on('click', function() {
            $('#modal-lg .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
            $('#modal-lg').modal('show')
            .find('.modal-content')
            .load($(this).attr('data-url'));

        });




        $('#modal-addbtn-user').on('click', function() {
            modalUser($(this).attr('data-url'));
            $.ajax({
                url:'" . Url::to(['/ckd/doctor-confirm/showckdcheck', 'pid' => Yii::$app->session['emr_pid'], 'hpcode' => Yii::$app->session['dynamic_connection']['sitecode']]) . "',
                success:function(data){
                    $(\"input[name='FPerson[items]']\").filter('[value='+data+']').prop('checked', true);

                 }
             });
            return false;
        });
         //btnAddGraph
         $('#btnAddGraph').on('click', function() {
            modalUser($(this).attr('data-url'));
             return false;
        });





        function modalUser(url) {
            $('#modal-lg .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
            $('#modal-lg').modal('show')
            .find('.modal-content')
            .load(url);
        }




");
?>






<?php $this->registerJs("

$('#showall-diag').on('click', function() {
    var url = $(this).attr('data-url');
    modalLg(url);
});

$('#showall-drug').on('click', function() {
    var url = $(this).attr('data-url');
    modalLg(url);
});

$('#showall-cvd').on('click', function() {
    var url = $(this).attr('data-url');
    modalLg(url);
});

$('#showall-cva').on('click', function() {
    var url = $(this).attr('data-url');
    modalLg(url);
});

$('#showall-mi').on('click', function() {
    var url = $(this).attr('data-url');
    modalLg(url);
});

function modalLg(url) {
    $('#modal-lg .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
    $('#modal-lg').modal('show')
    .find('.modal-content')
    .load(url);
}

$('#graph-pjax').on('click', '.open-ezfrom', function() {
    modalEzfrom($(this).attr('data-url'));

});

function modalEzfrom(url) {
    $('#modal-print .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
    $('#modal-print').modal('show');
    $.ajax({
	method: 'POST',
	url: url,
	dataType: 'HTML',
	success: function(result, textStatus) {
	    $('#modal-print .modal-content').html(result);
	    return false;
	}
    });
}

$('#modal-print').on('hidden.bs.modal', function () {
showBtn(); //เมือปิด modal
});



"); ?>


<?php
$this->registerCss("
    .resize{
        width:auto;height:auto;
    }
        .highcharts-container{width:100%;height:100%;}
");
?>

<?php $this->registerJs("
        if(1==2){
            $.ajax({
                url:'" . Url::to(['/ckdpd/report/home-ajax']) . "?ptlink=$ptlink' ,
                method:'GET',
                dataType:'HTML',
                success:function(result){
                   $('#homecontent').html(result);
                },error: function (xhr, ajaxOptions, thrownError) {
                   $('#homecontent').html('<span class=\'label label-danger\'>Error:'+'ไม่มีข้อมูล'+'</span>');

                }
            });
        }
        
        $('.tabcca_regis').ready(function(){
            //alert('CCA test');
            $.ajax({
                url:'" .Url::to('/ccanet/emr/checkcca'). "',
                data:{
                    ptlink:'".$ptlink."',
                    cid:'".$ptcid."',
                    tab:'".$tab."',
                },
                success:function(result){
                    //console.log(result);
                    $('.tabcca_regis').html(result);
                },function (xhr, ajaxOptions, thrownError) {
                    $('.tabcca_regis').html(result);
                    //console.log(err);
                }
            });
        });
    ");
?>
