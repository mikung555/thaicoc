<?php
    use yii\helpers\Url;
    use backend\modules\ckdnet\classes\CkdnetQuery;
?>

<div class="col-md-6 col-sm-6 col-lg-6">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <div class="row">
                <div class="col-md-9 col-sm-6 col-lg-7"><h3 class="panel-title"><b>CVD Risk</b></h3></div>
                <div class="col-md-3 col-sm-6 col-lg-4 text-right">
                    <a id="showall-cvd" class="btn btn-default btn-sm" data-url="<?=
                    Url::to(['cvd-risk',
                        'hospcode' => $hospcode,
                        'ptlink' => $ptlink,
                    ])
                    ?>" style="cursor: pointer;"><i class="fa fa-bars" aria-hidden="true"></i> All</a>
                </div>

            </div>
        </div>
        <div class="panel-body text-center text-danger">
            <h4><?php
                $cvd_risk = CkdnetQuery::getCvdrisk($patient['PID'], $hospcode, $one_serv, 'cvd_risk')['cvd_risk'];
                if ($cvd_risk == '') {
                    echo 'ไม่พบข้อมูล';
                } else {
                    echo $cvd_risk . '%';
                    if ($cvd_risk > 0 && $cvd_risk < 10) {
                        $color_cvd = "#00ff80";
                        $group_cvd = "(จัดอยู่ในกลุ่มเสี่ยงน้อย)";
                    } else if ($cvd_risk >= 10 && $cvd_risk < 20) {
                        $color_cvd = "#ffff00";
                        $group_cvd = "(จัดอยู่ในกลุ่มเสี่ยงปานกลาง)";
                    } else if ($cvd_risk >= 20 && $cvd_risk < 30) {
                        $color_cvd = "#ff8000";
                        $group_cvd = "(จัดอยู่ในกลุ่มเสี่ยงสูง)";
                    } else if ($cvd_risk >= 30) {
                        $color_cvd = "red";
                        $group_cvd = "(จัดอยู่ในกลุ่มเสี่ยงสูงมาก)";
                    }
                    ?>
                    <i class="fa fa-square fa-lg" aria-hidden="true" data-toggle="tooltip" title="<?php echo $group_cvd; ?>" data-placement='bottom' style="color:<?= $color_cvd; ?>" ></i>
                <?php } ?>
            </h4>
        </div>
    </div>
</div> <!--close col-md-6 cvdrisk-->
<div class="col-md-6 col-sm-6 col-lg-6">
    <div class="panel panel-primary">
        <div class="panel-heading ">
            <div class="row">
                <div class="col-md-8 col-sm-4 col-lg-7"><h3 class="panel-title"><b>CVA</b></h3></div>
                <div class="col-md-4 col-sm-4 col-lg-4 text-right">

                    <a id="showall-cva" class="btn btn-default btn-sm" data-url="<?=
                    Url::to(['cva-all',
                        'hospcode' => $hospcode,
                        'ptlink' => $ptlink,
                    ])
                    ?>" style="cursor: pointer;"><i class="fa fa-bars" aria-hidden="true"></i> All</a>
                </div>
            </div>
        </div>
        <div class="panel-body text-center text-danger">
            <?php
            $cva_code = CkdnetQuery::getPatientProfileHospitalFieldOne($hospcode, $patient['PID'], 'cva_code')['cva_code'];
            $cva_date = CkdnetQuery::getPatientProfileHospitalFieldOne($hospcode, $patient['PID'], 'cva_date')['cva_date'];
            if ($cva_code <> NULL) {
                $check = "class=\"fa fa-check fa-2x\"";
            } else {
                $check = "class=\"fa fa-times fa-2x\"";
                $textcvd = "ไม่ได้รับวินิจฉัย";
            }
            // echo $check."<br>".$cva_code;
            ?>
            <h4><i <?php echo $check ?> aria-hidden="true"></i> <?php echo substr($cva_date, 0, 10) . $textcvd; ?></h4>
        </div>
    </div>
</div> <!--close col-md-6 cva-->
<div class="col-md-6 col-sm-6 col-lg-6">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <div class="row">
                <div class="col-md-6 col-sm-4 col-lg-7"><h3 class="panel-title"><b>MI</b></h3></div>
                <div class="col-md-4 col-sm-4 col-lg-4 text-right">

                    <a id="showall-mi" class="btn btn-default btn-sm" data-url="<?=
                    Url::to(['mi-all',
                        'hospcode' => $hospcode,
                        'ptlink' => $ptlink,
                    ])
                    ?>" style="cursor: pointer;"><i class="fa fa-bars" aria-hidden="true"></i> All</a>
                </div>
            </div>
        </div>
        <div class="panel-body text-center text-danger">
            <?php
            $mi_code = CkdnetQuery::getPatientProfileHospitalFieldOne($hospcode, $patient['PID'], 'mi_code')['mi_code'];
            $mi_date = CkdnetQuery::getPatientProfileHospitalFieldOne($hospcode, $patient['PID'], 'mi_date')['mi_date'];
            if ($mi_code <> NULL) {
                $check = "class=\"fa fa-check fa-2x\"";
            } else {
                $check = "class=\"fa fa-times fa-2x\"";
                $textmi = "ไม่ได้รับวินิจฉัย";
            }
            // echo $mi_code;
            ?>
            <h4><i <?php echo $check ?> aria-hidden="true"></i><?php echo substr($mi_date, 0, 10) . $textmi; ?></h4>
        </div>
    </div>
</div> <!--close col-md-3 mi-->
<?php
if ($patient['DEATH'] == 'Y') {
    ?>
    <div class="col-md-6 col-sm-6 col-lg-6">
        <div class="panel panel-primary">
            <div class="panel-heading text-center">
                <h3 class="panel-title">สถานะ</h3>
            </div>
            <div class="panel-body text-center text-danger">
                เสียชีวิตแล้ว เมื่อ<br> <?php echo $patient['DDEATH']; ?>
            </div>
        </div>
    </div>
    <?php
}
?>

<div class="col-md-12 col-sm-12 col-lg-12">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <div class="row">
                <div class="col-md-9">
                    <h3 class="panel-title"><b>ยาที่ได้รับ</b></h3>
                    <b>จาก</b>  <?php if ($hospcode_cup == '') {
                        echo $hospcode;
                    } else {
                        echo $hospcode_cup;
                    } ?> <?= $nameHospital ?>
                    <br><b>วันที่</b> : <?= ($one_serv == '' ? 'ไม่ระบุ' : \common\lib\sdii\components\utils\SDdate::mysql2phpThDate($one_serv)); ?>
                    <br><span class="label label-warning">หมายเหตุ : ไม่ได้รับยาจะไม่แสดงข้อมูล</span>

                </div>
                <div class="col-md-3 text-right">
                    <a id="showall-drug" class="btn btn-default btn-sm" data-url="<?=
                       Url::to(['drug-all',
                           'hospcode' => $hospcode,
                           'ptlink' => $ptlink,
                       ])
                       ?>" style="cursor: pointer;"><i class="fa fa-bars" aria-hidden="true"></i> All</a>
                </div>
            </div>
        </div>
        <div class="panel-body">
            <?php
            $data_drug_opd = CkdnetQuery::getDrugOpdAll($ptlink, $hospcode);
            $data_drug_ipd = CkdnetQuery::getDrugIpdAll($ptlink, $hospcode);
            $drug = \yii\helpers\ArrayHelper::merge($data_drug_opd, $data_drug_ipd); //เอาข้อมูลIPDกับOPDมารวมกัน
            $data = \appxq\sdii\utils\SortArray::sortingArrayDESC($drug, 'DATE_SERV');
            $dateData = [];
            foreach ($data as $value) {
                //if ($one_serv['date_serv'] == $value['DATE_SERV']) {
                if ($one_serv == $value['DATE_SERV']) {
                    $dateData[$value['DATE_SERV']] = [];
                    array_push($dateData[$value['DATE_SERV']], $value['DNAME'] . '  <code>' . $value['AMOUNT'] . ' ' . $value['UNIT_PACKING'] . '</code> ');

                    echo "<p>" . $dateData[$value['DATE_SERV']][0] . "</p>";
                }
            }
            ?>
        </div>
    </div>
</div><!--close col-md-6 drug-->
