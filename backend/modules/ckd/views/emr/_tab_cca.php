<?php
    use common\lib\sdii\components\utils\SDdate;
    $this->registerCSS("
        .icon-green{ color: green;}
        .icon-red{ color: red;}
    ");

?>
<div class="row">
    <div class="col-md-6">
        <div class="panel panel-primary">
            <div class="panel-heading"><h3 class="panel-title"><b>Symptoms</b></h3></div>
            <div class="panel-body"><center>
                <center>
                    <?php 
                    if($querycca['symptoms'] == null){
                        $textcolor = "text-danger";
                        $iconcheck = "fa fa-times fa-2x text-danger";
                        $datasymptoms = "ไม่ได้รับวินิจฉัย";
                    }else{
                        $textcolor = "text-success";
                        $iconcheck = "fa fa-check fa-2x icon-green";
                        $datasymptoms = "".SDdate::mysql2phpThDateSmall($querycca['symptoms_date'])." ".substr($querycca['symptoms_date'],10)."";
                    }
                    ?>
                    <i class="<?=$iconcheck?>"></i><b class="<?=$textcolor;?>"><?=$datasymptoms;?></b>
                </center>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="panel panel-primary">
            <div class="panel-heading"><h3 class="panel-title"><b>IPD/OPD</b></h3></div>
            <div class="panel-body">
                <center>
                    <?php 
                    if($querycca['an'] == null){
                        echo $datasymptoms = "<b><span class=\"label label-warning\" style=\"font-size: 14px;\"> OPD </span></b>";
                    }else{
                        echo $datasymptoms = "<b>AN : ".$querycca['an']." <span class=\"label label-warning\" style=\"font-size: 14px;\"> IPD </span></b>";
                    }
                    ?>
                </center>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="panel panel-primary">
            <div class="panel-heading"><h3 class="panel-title"><b>Diagnosed CCA</b></h3></div>
            <div class="panel-body">
                <center>
                    <?php 
                    if($querycca['cca_code'] == null){
                        $textcolor = "text-danger";
                        $iconcheck = "fa fa-times fa-2x text-danger";
                        $datasymptoms = "ไม่ได้รับวินิจฉัย";
                    }else{
                        $textcolor = "icon-green";
                        $iconcheck = "fa fa-check fa-2x icon-green";
                        $datasymptoms = "".$querycca['cca_code']."</br>".SDdate::mysql2phpThDateSmall($querycca['cca_code_date'])." ".substr($querycca['cca_code_date'],10)."";
                    }
                    ?>
                    <i class="<?=$iconcheck?>" aria-hidden="true"></i><b class="<?=$textcolor;?>"> <?=$datasymptoms;?></b>
                </center>
            </div>
        </div>
    </div>
</div>