<?php
use backend\modules\ckdnet\classes\CkdnetQuery;
use common\lib\sdii\components\utils\SDdate;
use yii\helpers\Url;
use yii\web\JsExpression;
//appxq\sdii\utils\VarDumper::dump($querycca);
$data_service = CkdnetQuery::getService($ptlink, $hospcode);

if ($data_service) {
    $serv = isset($_GET['serv']) ? $_GET['serv'] : $data_service[0]['date_serv'];
} else {
    $serv = NULL;
}     
if ($patient) {     
?>

<div class="row">
    <div class="col-md-3 col-sm-3 col-lg-3">
        <div class="list-group">
            <?php 
            
            
            $one_serv = [];
            if (isset($data_service) && !empty($data_service)) {
                foreach ($data_service as $key_ser => $value_ser) {
                    $key = isset(Yii::$app->session['key_ckd']) ? Yii::$app->session['key_ckd'] : '';
                    $save_keyckd = isset(Yii::$app->session['save_keyckd']) ? Yii::$app->session['save_keyckd'] : false;
                    $convert = isset(Yii::$app->session['convert_ckd']) ? Yii::$app->session['convert_ckd'] : false;

                    if ($value_ser['date_serv'] == $serv) {
                        $one_serv = $value_ser;
                    }

                    $typeout = backend\modules\ckdnet\classes\CkdnetFunc::itemAlias('typeout', $value_ser['typeout']);
                    ?>

                    <a href="javascript:void(0);" onclick="checkCup('<?=$hospcode?>','<?=$value_ser['HOSPCODE']?>','<?=$value_ser['date_serv']?>');"
                       class="list-group-item <?= $value_ser['date_serv'] == $serv ? 'active' : '' ?> btn-visit" >
                        <?= SDdate::mysql2phpDateTime($value_ser['date_serv'] . ' ' . $value_ser['time_serv']) ?>, <?= $value_ser['HOSPCODE'] ?> : <?= $typeout ?>
                    </a> 
                    
                   
            <?php 
                }
                
            } else {
                echo 'ยังไม่ได้รับบริการ';
            }
            ?>
                
        </div>
        
    </div>
    <div id="ckd_emr">
        <?php // appxq\sdii\utils\VarDumper::dump($one_serv); 
        echo $this->render("ckd",[
            'hospcode'      => $hospcode,
            'ptlink'        => $ptlink,
            'patient'=>$patient,
            'cid' => $cid,
            'hospcode_cup' =>$value_ser['HOSPCODE'],
            'one_serv'=>$one_serv['date_serv'],
            'one_serv_data'=>$one_serv,
            'nameHospital' =>$nameHospital,
            'tab_idemr' => $tab_idemr,
            'tab'=>$tab,
            'querycca'=>$querycca
        ]); ?>
    </div>
        
    
</div>

<?php
} else {
    echo '<code>ไม่พบข้อมูล</code>';
}
?>

<?php
    $this->registerJs("
       
    function checkCup(hospcode,hospcode_cup,one_serv){
    
        $('#ckd_emr').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
        $.ajax({
            url:'" . Url::to(['/ckd/emr/ckdvisit']) . "',
            method:'POST',
            beforeSend:function(data){
             console.log('กำลังโหลดข้อมูล');
            },
            data: {
                hospcode:hospcode,
                hospcode_cup:hospcode_cup,
                ptlink:'".$ptlink."',    
                cid:'".$cid."',
                nameHospital:'".$nameHospital."',
                one_serv:one_serv,
                one_serv_data:".yii\helpers\Json::encode($one_serv).",
                tab_idemr:'".$tab_idemr."',
                tab:'".$tab."',
                querycca:".yii\helpers\Json::encode($querycca).",
            },
            //dataType:'JSON',
            success:function(result){
            //console.log(result);
                //console.log('sent data success');
                $('#ckd_emr').html(result); 
            },error: function (xhr, ajaxOptions, thrownError) {
               console.log(xhr);
            }
        });
    }
    ");


?>