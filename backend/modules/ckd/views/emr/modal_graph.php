<?php
use backend\modules\ckdnet\classes\CkdnetQuery;
use backend\modules\ckdnet\classes\CkdnetFunc;
use miloschuman\highcharts\Highcharts;
use yii\helpers\Url;
use yii\helpers\Html;
Yii::$app->session['graph']=$graph;
$series=[];
$highcharts_config = [
                    'setupOptions' => [
                        'lang'=> [
                            'shortMonths'=> ['ม.ค.','ก.พ.','มี.ค.', 'เม.ย.', 'พ.ค.', 'มิ.ย.', 'ก.ค.', 'ส.ค.', 'ก.ย.', 'ต.ค.', 'พ.ย.', 'ธ.ค.'],
                            'shortWeekdays'=>['อา.', 'จ.', 'อ.', 'พ.', 'พฤ.', 'ศ.', 'ส.' ],
                            'weekdays'=>['อาทิตย์', 'จันทร์', 'อังคาร', 'พุธ', 'พฤหัสบดี', 'ศุกร์', 'เสาร์' ],
                        ],                        
                    ],
                    'options' => [
                        'title' => ['text' => 'กราฟ'],
                        'xAxis' => [
                            'type' => 'datetime',
                            'dateTimeLabelFormats' => [
                                'day'=>"%e %b %Y",
                                'month'=>'%e %b %Y',
                                'year'=>'%e %b %Y'
                            ],
                            'title' => [
                                'text' => 'Visit Date'
                            ],
                            'labels' => [
                                'rotation'=> -45,
                            ],
                        ],
                        'yAxis' => [
                            'title' => ['text' => 'Result'],
                            
                        ],
                        'tooltip' => [
                            'shared'=>true,
                            'useHTML'=>true,
                            'headerFormat' => '<small><b>{point.x:%A, %e %b %Y}</b></small><table><tr><td>',
//                            'pointFormat' => '<tr><td style="color: {series.color}">{series.name}: </td><td style="text-align: right"><b>{point.y:.2f} </b></td></tr>',
                            'footerFormat' => '</td></tr></table>',
                        ],
                        'plotOptions'=> [
                            'line' => [
                                'dataLabels'=> [
                                    'enabled'=> false,
                                    'color'=>'gray',
                                    'y'=>-5,
                                ],
                            ]
                        ],
                        'series' => []
                    ]
                ];

    if($graph==2){
        $ptid = CkdnetQuery::getPerson($cid);
        $pd_g = CkdnetQuery::getUrine($ptid);
        $dataLab = CkdnetFunc::getLabItemHighcharts($pd_g);
        if($dataLab){
            $series[] = ['name' => 'Urine', 'id'=>1, 'data' => new \yii\web\JsExpression($dataLab)];

        } else {
            $series[] = ['name' => 'Urine', 'id'=>1, 'data' => []];
        }
        
        $pd_g = CkdnetQuery::getBw($ptid);
        $dataLab = CkdnetFunc::getLabItemHighcharts($pd_g);
        if($dataLab){
            $series[] = ['name' => 'BW', 'id'=>2, 'data' => new \yii\web\JsExpression($dataLab)];

        } else {
            $series[] = ['name' => 'BW', 'id'=>2, 'data' => []];
        }
        
        $pd_g = CkdnetQuery::getUf($ptid);
        $dataLab = CkdnetFunc::getLabItemHighcharts($pd_g);
        if($dataLab){
            $series[] = ['name' => 'UF', 'id'=>3, 'data' => new \yii\web\JsExpression($dataLab)];
        } else {
            $series[] = ['name' => 'UF', 'id'=>3, 'data' => []];
        }
        
        ?>


<div class="row">
    <div class="col-md-6 col-sm-6 col-lg-6">
        <?php
            
            $highcharts_config['options']['title']['text'] = 'Urine';
            $highcharts_config['options']['series'] = $series;
            $highcharts_config['options']['chart']['events']['load'] = new yii\web\JsExpression("
                function(event) {
                    this.series.forEach(function(d,i){
                        
                        if(d.options.id!=1){
                           d.hide();
                        }
                    });
                }
                    ");
            
            echo Highcharts::widget($highcharts_config);
        ?>
    </div>
    <div class="col-md-6 col-sm-6 col-lg-6">
        <?php
            
            $highcharts_config['options']['title']['text'] = 'BW';
            $highcharts_config['options']['series'] = $series;
            $highcharts_config['options']['chart']['events']['load'] = new yii\web\JsExpression("
                function(event) {
                    this.series.forEach(function(d,i){
                        
                        if(d.options.id!=2){
                           d.hide();
                        }
                    });
                }
                    ");
            echo Highcharts::widget($highcharts_config);
        ?>
    </div>
    <div class="col-md-6 col-sm-6 col-lg-6">
        <?php
            
            $highcharts_config['options']['title']['text'] = 'UF';
            $highcharts_config['options']['series'] = $series;
            $highcharts_config['options']['chart']['type'] = 'column';
            $highcharts_config['options']['chart']['events']['load'] = new yii\web\JsExpression("
                function(event) {
                    this.series.forEach(function(d,i){
                        
                        if(d.options.id!=3){
                           d.hide();
                        }
                    });
                }
                    ");
            echo Highcharts::widget($highcharts_config);
        ?>
    </div>
</div>
<?php
       
    } else {
        $series = CkdnetFunc::getLab($ptlink, $hospcode, Yii::$app->session['emr_pid']);
       
        //appxq\sdii\utils\VarDumper::dump($series);
        if (!empty($series)) {
            
        $list_g = CkdnetQuery::getListGraphOne($graph,$id);
        echo '<div class="row">';
        foreach ($list_g as $key => $value) {
            
            echo '<div class="col-md-12 col-sm-12 col-lg-12">';
           
       
            if(Yii::$app->user->can('administrator') && Yii::$app->session['dynamic_connection']['sitecode'] == $hospcode){
                echo "<div style='background:#ffffff;float:right;padding:5px;'>";
                echo "<div class='clearfix'></div>";
                echo "<button class='btn btn-info btn-sm'   onclick='return EditGraph(".$value['id'].")'><i class=\"fa fa-pencil\"></i></button> ";
                
      
                echo "<button class='btn btn-danger btn-sm'   onclick='return DeleteGraphList(".$value['id'].")'><i class=\"fa fa-minus\"></i></button> ";
                echo " ";

                echo "</div>"; 
            }
             
            
            $findStr = ",{$value['tdc_lab_item_code']},";
            $highcharts_config['options']['title']['text'] = $value['graph_name'];
            $highcharts_config['options']['series'] = $series;
            $highcharts_config['options']['chart']['events']['load'] = new yii\web\JsExpression("
                function(event) {
                    this.series.forEach(function(d,i){
                        var find_id = ','+d.options.id+',';
                        var find_in_set = '$findStr';
                        
                        if(!find_in_set.includes(find_id)){
                           d.hide();
                        }
                    });
                }
                    ");
            
            echo Highcharts::widget($highcharts_config);
            echo '</div>';
        }
        echo '</div>';
        }
    }
    
 $gson = json_encode($list_g);
  
?>

<?php 
    $this->registerJS(" 
       
       function DeleteGraphList(id){
       
           var gname = '';
           var id = id;
           var url = '".Url::to(['/ckd/graph/delete-graph'])."'; 
           
            $.each(".$gson.", function(k,v){
               if(id == v.id){
                   gname = v.graph_name; 
               }   
           });
        
        krajeeDialog.confirm('<b>คุณต้องการลบกราฟ '+gname+' หรือไม่</b>', function (result) {
                if (result) {        
                        $.ajax({
                            url:url,
                            type:'POST',
                            data:{id:id}, 
                            dataType:'JSON',
                            success:function(result){
                                console.log('test');
                                ///$.pjax.reload({container:'#graph-pjax'});
                                 " . \common\lib\sdii\components\helpers\SDNoty::show('result.message', 'result.status') . ";
                                ///location.reload();
                                showgraph_load();
                            }
                        });
                } else {
                    $('#modal-lg').modal('hide');
                }
                
            });
            return false;
        }//DeleteGraphList
        
        function EditGraph(id){
           
            var id = id; 
           // console.log(id);
            var url = '".Url::to(['/ckd/graph/add'])."';
            var urls = url+'?id='+id+'&graph=".$graph."';
            console.log(urls);
            modalUser(urls); 
           
        };//btnEditGraph
        
        $('#btnUpdateGraphWhere').click(function(){
            
            $('#modal-graph .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>'); 
            $('#modal-graph').modal('show') 
            .find('.modal-content') 
            .load($(this).attr('data-url')); 

           //  modal-graph
        });//btnAddGraphWhere

    ");
?>