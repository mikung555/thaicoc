<?php

use common\lib\sdii\components\utils\SDdate;
use backend\modules\ckdnet\classes\CkdnetFunc;
?>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title" id="itemModalLabel">ประวัติการรักษา</h4>
</div>

<div class="modal-body">

    <div class="panel panel-danger">
        <div class="panel-heading">
            <h4>แพ้ยา</h4>
        </div>
        <div class="panel-body">
            <table class="table">
                <tbody>
                    <?php
                    if ($drugallergy) {
                        foreach ($drugallergy as $key_da => $value_da) {
                            ?>
                            <tr>
                                <td><?= $value_da['DATERECORD'] ?></td>
                                <td><?= $value_da['DNAME'] ?></td>
                                <td><?= CkdnetFunc::itemAlias('TYPEDX', $value_da['TYPEDX']); ?></td>
                                <td><?= CkdnetFunc::itemAlias('ALEVEL', $value_da['ALEVEL']); ?></td>
                            </tr>
                            <?php
                        }
                    } else {
                        echo 'ไม่พบข้อมูล';
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </div>       

<?php if ($data) {
        echo '<div id="diag-items">';
        $color = 'info';
        foreach ($data as $key => $value) {
            $typeout = backend\modules\ckdnet\classes\CkdnetFunc::itemAlias('typeout', $value['typeout']);
            ?>

            <div class="panel panel-<?= $color ?>">
                <div class="panel-heading">
                    <h4><?= SDdate::mysql2phpThDate($value['date_serv']) ?> ( <?= $value['HOSPCODE'] ?> <?= $nameHospital ?> : แผนก <?= $typeout ?> )</h4>
                </div>
                <div class="panel-body">
                    <?php
                    echo $this->render('_diagnose', [
                        'one_serv' => $value,
                        'hospcode' => $hospcode,
                        'ptlink' => $ptlink,
                        'drugallergy' => $drugallergy,
                    ]);
                    if ($color == 'info') {
                        $color = 'warning';
                    } else {
                        $color = 'info';
                    }
                    ?>
                </div>
            </div>
        <?php   } ?>
    </div> 
    <div class="row" id="progress-diag" style="display: none;">
        <div class="col-md-12 text-center">
            <div class="sdloader" ><i class="sdloader-icon"></i></div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 text-center">
            <button class="btn btn-success" id="get-more" data-url="<?=
                    yii\helpers\Url::to(['diag-item',
                        'hospcode' => $hospcode,
                        'ptlink' => $ptlink,
                    ])
                    ?>">แสดงเพิ่มเติม</button>
        </div>
    </div>
<?php } else {
    echo '<div class="panel panel-danger">'
            . '<div class="panel-heading"><h4> การวินิจฉัย </h4></div>'
            . '<div class="panel-body"> ไม่พบข้อมูล </div>'
        . '</div>';
} ?>
</div>

<?php $this->registerJs("

$('#get-more').on('click', function() {
    var url = $(this).attr('data-url');
    getItemDrug(url);
});

function getItemDrug(url) {
    $('#progress-diag').show();
    $.ajax({
	    method: 'POST',
	    url:url,
	    dataType: 'HTML',
	    success: function(result, textStatus) {
		$('#diag-items').append(result);
                $('#progress-diag').hide();
	    }
    });
}

"); ?>

