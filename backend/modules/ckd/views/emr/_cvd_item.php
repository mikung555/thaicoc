<?php 
use common\lib\sdii\components\utils\SDdate;

if($cvdall){
    $color = 'info';

$i=0;

foreach ($cvdall as $key => $cvdrisk){ 
   
?>
<div class="panel panel-<?=$color?>">    
    <div class="panel-heading">
        <h4><?= SDdate::mysql2phpThDate($cvdrisk['date_serv']);?> ( <?=$hospcode?> <?=$nameHospital?> )</h4>  
    </div>
    <div class="panel-body">
       <table class="table table-striped"> 
            <tbody> 
                <tr> 
                    <td width="50%">CVD :
                        <?php
                            if($cvdrisk['cvd_risk'] < 10){ $color_cvd = "#00ff80"; $group_cvd = "(จัดอยู่ในกลุ่มเสี่ยงน้อย)"; }
                            else if ($cvdrisk['cvd_risk'] >= 10 && $cvdrisk['cvd_risk'] < 20){ $color_cvd = "#ffff00"; $group_cvd = "(จัดอยู่ในกลุ่มเสี่ยงปานกลาง)";}
                            else if ($cvdrisk['cvd_risk'] >= 20 && $cvdrisk['cvd_risk'] < 30){ $color_cvd = "#ff8000"; $group_cvd = "(จัดอยู่ในกลุ่มเสี่ยงสูง)"; }
                            else if ($cvdrisk['cvd_risk'] >= 30 ){ $color_cvd = "red"; $group_cvd = "(จัดอยู่ในกลุ่มเสี่ยงสูงมาก)";}
                        ?>
                        <b><?php echo $cvdrisk['cvd_risk']." % "; ?>&nbsp;<?php echo $group_cvd; ?></b>
                    </td> 
                    <td><i class="fa fa-square fa-lg" aria-hidden="true" style="color:<?=$color_cvd;?>"></i></td>
                </tr> 
                <tr> 
                    <td width="50%">Height : <b><?php echo $cvdrisk['height'] ?></b></td> 
                    <td>Waist : <b><?php echo $cvdrisk['waist'] ?></b></td> 
                </tr> 
                <tr> 
                    <td width="50%">BPS : <b><?php echo $cvdrisk['bps'] ?></b></td> 
                    <td>Col : <b><?php echo $cvdrisk['tc'] ?></b></td> 
                </tr> 
                <tr> 
                    <td width="50%">HDL : <b><?php echo $cvdrisk['hdl'] ?></b></td> 
                    <td>LDL : <b><?php echo $cvdrisk['ldl'] ?></b></td> 
                </tr> 
                <tr> 
                    <td width="50%">DM : <b><?php if (stripos($cvdrisk['dm'],'e') !== FALSE ){echo 'เป็นโรคเบาหวาน';}  
                                else {echo '';}?></b></td> 
                    <td>HT : <b><?php if (stripos($cvdrisk['ht'],'i1') !== FALSE ){echo 'เป็นโรคความดันโลหิต';}  
                    else {echo '';}?></b></td> 
                </tr> 
                <tr> 
                    <td colspan="2">Smoking : <b><?php 
                    if($cvdrisk['smoking'] == '2'){ echo 'สูบบุหรี่';} 
                    else {echo 'ไม่สูบบุหรี่';}?></b></td> 
                </tr> 
            </tbody> 
        </table>
        
<!--        <h4><span class="label label-warning">ข้อแนะนำเบื้องต้น</span></h4>
        <div class="row">
            <div class="col-md-12">
                <?php 
                    
                    $sug[$i];
                    if($cvdrisk['smoking'] == '2' ){ $sug[$i] = $sug[$i] . " เลิกสูบบุหรี่";}
                    if(stripos($cvdrisk['dm'],'e1') !== FALSE ){ $sug[$i] = $sug[$i] . " รักษาระดับน้ำตาลในเลือดให้อยู่ในเกณฑ์ปกติ"; }
                    if($cvdrisk['bps'] >= '140'){ $sug[$i] = $sug[$i] . " ควบคุมระดับความดันโลหิตให้ดี" ; } 
                                    
                    if($cvdrisk['status'] == 1){
                        if($cvdrisk['tc'] >= 220 || $cvdrisk['ldl'] >= 190) { $sug[$i] = $sug[$i] . ' เข้ารับการรักษาเพื่อลดโคเรสเตอรอลในเลือด'; }
                    }else if ($cvdrisk['status'] == 2){
                        if(($cvdrisk['waist'] >= 38 && $cvdrisk['sex'] == 1) || ($cvdrisk['waist'] > 32 && $cvdrisk['sex'] == 2)) { $sug[$i] = $sug[$i] . ' ลดน้ำหนักให้อยู่ในเกณฑ์ปกติ' ;}
                    }
                    
                    if($cvdrisk['cvd_risk'] < 10 ){
                        echo "เพื่อป้องกันการเกิดโรคหลอดเลือดในอนาคต ควรออกกำลังกายอย่างสม่ำเสมอ รับประทานผักผลไม้เป็นประจำ". $sug[$i] ." และตรวจสุขภาพประจำปี";
                    }else if ($cvdrisk['cvd_risk'] >= 10 && $cvdrisk['cvd_risk'] < 20){
                        echo "ควรออกกำลังกายอย่างสม่ำเสมอ รับประทานผักผลไม้เป็นประจำ". $sug[$i] ." และควรได้รับการตรวจร่างกายประจำปีอย่างสม่ำเสมอ";                        
                    }else if ($cvdrisk['cvd_risk'] >= 20 ){
                        echo "ควรเข้ารับคำปรึกษาจากแพทย์ ในเบื้องต้นควรออกกำลังกายอย่างสม่ำเสมอ รับประทานผักผลไม้เป็นประจำ". $sug[$i] ." และเข้ารับการตรวจสุขภาพประจำปีอย่างสม่ำเสมอ";
                    }else{
                        echo "สามารถป้องกันการเกิดโรคหลอดเลือดหัวใจในอนาคตได้ด้วยการออกกำลังกายอย่างสม่ำเสมอ";
                    }  
                    

                ?>
            </div>
        </div>-->
        
        
    </div>
</div>
<?php 
        if($color=='info'){
            $color='warning';
        } else {
            $color = 'info';
        }
   
    $i++;   
    } 
} ?>

