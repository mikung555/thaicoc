<?php

namespace backend\modules\ckd;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\modules\ckd\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
        
        $this->layout = '@backend/views/layouts/ckd';
    }
}
