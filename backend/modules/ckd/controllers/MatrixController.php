<?php

namespace backend\modules\ckd\controllers;

use yii\web\Controller;
use Yii;
use yii\helpers\ArrayHelper;

class MatrixController extends Controller
{
    public $activetab="matrix";

    public function actionIndex() {
        
       
        $y = (date('m')>9?date("Y")+1:date("Y"))+ 543;
        $year = range( $y, $y);
        $itemYear = [];
        foreach ($year as $value) {
            array_push($itemYear, ['id' => $value - 543, 'value' => $value]);
        }
        $itemYear = ArrayHelper::map($itemYear, 'id', 'value');
        
        $sitecode = Yii::$app->user->identity->userProfile->sitecode;
        $sqlCodeAll = " SELECT hcode, amphurcode, provincecode, province, amphur, `name`, zone_code FROM all_hospital_thai WHERE hcode='$sitecode'";
        $resCodeAll = \Yii::$app->db->createCommand($sqlCodeAll)->queryOne();
        $amphur_select = $resCodeAll['amphurcode'];
        $province_select = $resCodeAll['provincecode'];
        $zone_select = $resCodeAll['zone_code'];

        //\appxq\sdii\utils\VarDumper::dump($itemYear)    ;   
        return $this->render('index',[
            'itemYear'=>$itemYear,
            'amphur_select' => $amphur_select,
            'province_select' => $province_select,
            'zone_select' => $zone_select ,
           
            
        ]);

    }

    
} 
