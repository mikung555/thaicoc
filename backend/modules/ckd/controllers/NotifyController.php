<?php

namespace backend\modules\ckd\controllers;

use yii\web\Controller;
use yii\helpers\Html;
use backend\modules\ckdnet\classes\CkdnetFunc;

class NotifyController extends Controller {

    public $activetab = "notify";
    private $results = [
        'sbp' => 'SBP > 130', //
        'dbp' => 'DBP > 90', //
        'hb' => ' Hb  < 10 g/dl', //
        'c_acei_arbs' => 'ไม่ได้รับ ACEI /ARBs', //
        'egfr_sl' => 'Slope ของ eGFR ต่อระยะเวลา ≥ 4',
        'c_hba1c6' => 'HbA1C  < 6.5%', //
        'c_hba1c7' => 'HbA1C  > 7.5 %', //
        'c_ldl' => 'LDL cholesterol > 100 mg/dl', //
        'c_srp' => 'serum potassium > 5.5 mEq/L', //
        'c_srb' => 'serum bicarbonate < 22 mEq/L', //
        'q9' => 'ไม่ได้รับการประเมิน Urine Protien Strip  (รพ.ระดับ F-M)', //
        'c_upcraer' => 'ไม่ได้รับการประเมิน UPCR หรือ urine protein 24 hrs', //
        'c_upcr_aer_500' => 'UPCR >= 500 mg/g  หรือ Urine  protein 24 hrs >= 500 mg/day', //
        'c_spp_45' => 'Serum phosphate > 4.5 mg/L', //
        'c_srh' => 'Serum parathyroid hormone (PTH)  อยู่ในเกณฑ์ไม่ปกติ', //
        'q14' => 'ไม่ได้รับการเตรียม AVF ก่อนเริ่ม hemodialysis', //
        'q15' => 'ได้เข้าร่วม educational class ในหัวข้อต่างๆครบ',
        'q16' => 'แจ้งค่า eGFR และระยะของ CKD  ครั้งนี้ และภายใน 3 เดือนที่ผ่านมา',
        'c_egfr_60' => 'แจ้งค่า eGFR < 60 ควรพบอายุรแพทย์',
        'q18' => 'มีอัตราการลดลงของ GFR มากกว่าปีละ  5 ml/min/1.73 m2 ',
        'c_egfr_30' => 'eGFR < 30 ได้รับการให้คำปรึกษาเรื่อง RRT',
        'c_metfn_egfr_30' => 'eGFR < 30 ยังได้รับยา metformin',
        'c_acei_arbs_aer30' => 'ผู้ป่วยเบาหวานที่มี albuminuria > 30 mg/day และไม่ได้รับยา ACEI หรือ ARB',
        'c_acei_arbs_aer300' => 'ไม่ได้เป็นเบาหวานมี albuminuria > 300 mg/day และไม่ได้รับยา ACEI หรือ ARB',
        'x23' => 'ได้รับ ACEi/ARB  ระวังการเกิด AKI และ hyperkalemia ควรได้รับคำแนะนำการปฏิบัติตัว',
        'x24' => 'เคยได้รับยา ACEI หรือ ARB แต่ต้องหยุดยาเพราะมี adverse event',
        'x25' => 'มี ปริมาณโปรตีนในปัสสาวะ ≥ 1+',
        'x26' => 'ผู้ป่วยเบาหวานที่ไม่ได้ตรวจ urine albumin อย่างน้อย 2 ครั้งต่อปี',
        'x27' => 'ไม่ได้เป็น CKD และได้รับยา NSAIDs นานกว่า 2 สัปดาห์',
        'c_nsaids' => 'ผู้ป่วย CKD ที่ได้รับยา NSAIDs ',
        'x29' => 'เวลาสั่งจ่ายยาในโปรแกรม ถ้ามียาที่ต้องปรับตาม CrCl ถ้าสั่งผิดให้มีการเตือนทุกครั้ง หรือถ้าไม่มีการปรับตาม CrCl ให้เตือนทุกครั้ง',
        'x30' => 'ประวัติการเคยวินิจฉัยว่ามี acute kidney injury ล่าสุด วัน เดือน ปี เท่าไร ',
        'x31' => 'มีโรคหรือภาวะที่เสี่ยงต่อการเกิด CKD ได้รับการตรวจ serum creatinine และ urine protein หรือ albumin ปีละ 1 ครั้ง (เบาหวาน, ความดันโลหิตสูง, เก๊าท์, SLE, อายุมากกว่า 60 ปี, ได้รับยาที่ nephrotoxic, ติดเชื้อทางเดินปัสสาวะส่วนบน ≥ 3 ครั้งต่อปี, มีโรคหัวใจและหลอดเลือด, polycystic kidney disease, โรคไตตั้งแต่กำเนิด, มีประวัติโรคไตในครอบครัว)',
        'c_egfr_sl_02' => 'ตรวจ UA พบโปรตีนหรือหรือเม็ดเลือดแดง ในปัสสาวะพิจารณาส่งปรึกษาแพทย์',
        'c_egfr_sl_03' => 'Serum creatinine เพิ่มขึ้นมากกว่าหรือเท่ากับ 0.3 mg/dL: acute kidney injury ควรหาสาเหตุ',
        'c_egfr_30x' => 'GFR < 30 ml/min/1.73m2 พิจารณาส่งปรึกษาอายุรแพทย์โรคไต',
        'c_egfr_15' => 'GFR < 15 ml/min/1.73m2 ควรส่งประเมินเตรียมความพร้อมในการทำบำบัดทดแทนไต ',
        'c_k_nsaids' => 'ACEI/ARB และ Potassium >5 mEq/L'
    ];
    private $field = [
        'dbp' => [
            ',pt.dbp',
            ',pt.bp_date'
        ],
        'sbp' => [
            ',pt.sbp',
            ',pt.bp_date'
        ],
        'hb' => [
//            ',pt.c_hb',
            ',pt.hb',
            ',pt.hb_date'
        ],
        'c_acei_arbs' => [
            ',pt.c_acei_arbs',
            ',pt.acei',
            ',pt.arbs',
            ',pt.acei_date',
            ',pt.arbs_date'
        ],
        'egfr_sl' => [
            ',pt.c_egfr_sl_0',
            ',pt.egfr_sl',
            ',pt.egfr_date'
        ],
        'c_hba1c6' => [
            ',pt.hba1c',
            ',pt.hba1c_date'
        ],
        'c_hba1c7' => [
            ',pt.hba1c',
            ',pt.hba1c_date'
        ],
        'c_ldl' => [
            ',pt.ldl',
            ',pt.ldl_date'
        ],
        'c_srp' => [
            ',pt.c_srp',
            ',pt.srp',
            ',pt.srp_date'
        ],
        'c_srb' => [
            ',pt.c_srb',
            ',pt.srb',
            ',pt.srb_date'
        ],
        'q9' => [
            ",'' AS q9",
            ",'' AS q9x",
            ",'' AS q9xx"
        ],
        'c_upcraer' => [
            ',pt.c_upcraer',
            ',pt.upcr',
            ',pt.upcr_date',
            ',pt.aer',
            ',pt.aer_date'
        ],
        'c_upcr_aer_500' => [
            ',pt.c_upcr_aer_500',
            ',pt.upcr',
            ',pt.upcr_date',
            ',pt.aer',
            ',pt.aer_date'
        ],
        'c_spp_45' => [
            ',pt.c_spp_45',
            ',pt.spp',
            ',pt.spp_date'
        ],
        'c_srh' => [
            ",pt.c_srh",
            ",pt.srh",
            ",pt.srh_date"
        ],
        'q14' => [
            ",'' AS q14",
            ",'' AS q14x",
            ",'' AS q14xx"
        ],
        'q15' => [
            ",'' AS q15",
            ",'' AS q15x",
            ",'' AS q15xx"
        ],
        'q16' => [
            ",'' AS q16",
            ",'' AS q16x",
            ",'' AS q16xx"
        ],
        'c_egfr_60' => [
//            ',pt.c_egfr_60',
            ',pt.egfr',
            ',pt.egfr_date'
        ],
        'q18' => [
            ",'' AS q18",
            ",'' AS q18x",
            ",'' AS q18xx"
        ],
        'c_egfr_30' => [
            ',pt.c_egfr_30',
            ',pt.egfr',
            ',pt.egfr_date'
        ],
        'c_metfn_egfr_30' => [
            ',pt.c_metfn_egfr_30',
            ',pt.egfr',
            ',pt.egfr_date',
            ',pt.metfn',
            ',pt.metfn_date'
        ],
        'c_acei_arbs_aer30' => [
            ',pt.c_acei_arbs_aer30',
            ',pt.aer',
            ',pt.aer_date',
            ',pt.acei',
            ',pt.acei_date',
            ',pt.arbs',
            ',pt.arbs_date'
        ],
        'c_acei_arbs_aer300' => [
            ',pt.c_acei_arbs_aer300',
            ',pt.aer',
            ',pt.aer_date',
            ',pt.acei',
            ',pt.acei_date',
            ',pt.arbs',
            ',pt.arbs_date'
        ],
        'x23' => [
            ",'' AS x",
            ",'' AS xx",
            ",'' AS xxx"
        ],
        'x24' => [
            ",'' AS x24",
            ",'' AS x24x",
            ",'' AS x24xx"
        ],
        'x25' => [
            ",'' AS x",
            ",'' AS xx",
            ",'' AS xxx"
        ],
        'x26' => [
            ",'' AS x",
            ",'' AS xx",
            ",'' AS xxx"
        ],
        'x27' => [
            ",'' AS x",
            ",'' AS xx",
            ",'' AS xxx"
        ],
        'c_nsaids' => [
            ',pt.c_nsaids',
            ',pt.nsaids',
            ',pt.nsaids_date'
        ],
        'x29' => [
            ",'' AS x",
            ",'' AS xx",
            ",'' AS xxx"
        ],
        'x30' => [
            ",'' AS x",
            ",'' AS xx",
            ",'' AS xxx"
        ],
        'x31' => [
            ",'' AS x",
            ",'' AS xx",
            ",'' AS xxx"
        ],
        'c_egfr_sl_02' => [
            ',"" AS c_egfr_sl_02'
        ],
        'c_egfr_sl_03' => [
            ',"" AS c_egfr_sl_03'
        ],
        'c_egfr_30x' => [
            ',pt.c_egfr_30',
            ',pt.egfr_date'
        ],
        'c_egfr_15' => [
            ',pt.c_egfr_15',
            ',pt.egfr',
            ',pt.egfr_date'
        ],
        'c_k_nsaids' => [
            ',pt.c_k_nsaids',
            ',pt.srp',
            ',pt.srp_date',
            ',pt.nsaids',
            ',pt.nsaids_date',
            ',pt.arbs',
            ',pt.arbs_date',
            ',pt.acei',
            ',pt.acei_date'
        ]
    ];

    public function actionIndex() {

        return $this->render('index', [
                    'results' => $this->results
        ]);
    }

    public function actionNotify() {


        if (\Yii::$app->request->isAjax) {
            ini_set('memory_limit', '1024M');
            ini_set('max_execution_time', 30000);
            $gridColumns = [
                [
                    'attribute' => 'name',
                    'label' => 'ชื่อ',
                    'format' => 'raw',
                    'value' => function($model, $i = 1) {
                        return $model['name'] . Html::hiddenInput('page', $_GET['page'], ['class' => 'page']) . Html::hiddenInput('per-page', $_GET['per-page'], ['class' => 'per-page']);
                    },
                    'contentOptions' => ['style' => 'min-width:150px; text-align:center; '],
                    'headerOptions' => ['style' => 'text-align:center;'],
                ],
                [
                    'attribute' => 'lname',
                    'label' => 'นามสกุล',
                    'value' => function($model) {

                        return $model['lname'];
                    },
                    'contentOptions' => ['style' => 'min-width:150px; text-align:center; '],
                    'headerOptions' => ['style' => 'text-align:center;'],
                ],
                [
                    'attribute' => 'EMR',
                    'label' => 'EMR',
                    'format' => 'raw',
                    'value' => function($model) {

                        return Html::a('EMR', \yii\helpers\Url::to(['/ckd/emr/index', 'ptlink' => $model['ptlink']]), ['target' => '_blank', "data-pjax" => "0"]);
                    }
//                    'contentOptions' => ['style' => 'min-width:150px; text-align:center; '],
//                    'headerOptions' => ['style' => 'text-align:center;'],
                ]
            ];
            $whereParam = [':hospcode' => \Yii::$app->user->identity->userProfile->sitecode];
            if ($_GET['no'] != '' && $_GET['field'] == '') {
                $_GET['no'] == 'c_egfr_30x' ? $no = 'c_egfr_30' : $no = $_GET['no'];
//                $field .= ",pt." . $no . " AS " . $no;
//                \appxq\sdii\utils\VarDumper::dump($no);
                foreach ($this->field[$no] as $vField) {
                    if ($vField == 'c_hba1c6' || $vField == 'c_hba1c7') {
                        $vField = 'hba1c';
//                        $no = 'c_hba1c';
                    }
                    $field .= $vField;
                }
                if ($no == 'q9' || $no == 'q14' || $no == 'q15' || $no == 'q16' ||
                        $no == 'q18' || $no == 'x23' || $no == 'x24' || $no == 'x25' || $no == 'x26' ||
                        $no == 'x27' || $no == 'x28' || $no == 'x29' || $no == 'x30' || $no == 'x31' ||
                        $no == 'c_egfr_sl_02' || $no == 'c_egfr_sl_03') {
                    $andWhere .= " AND pt.q9 = 1";
                } else if ($no == 'egfr_sl') {
                    $andWhere .= " AND pt." . $no . " >= 4 ";
                } else if ($no == 'sbp') {
                    $andWhere .= " AND pt." . $no . " > 130 ";
                } else if ($no == 'dbp') {
                    $andWhere .= " AND pt." . $no . " > 90 ";
                } else if ($no == 'hb') {
                    $andWhere .= " AND pt." . $no . " < 10 ";
                } else if ($no == 'c_hba1c6') {
                    $andWhere .= " AND pt.hba1c < 6.5 ";
                } else if ($no == 'c_hba1c7') {
                    $andWhere .= " AND pt.hba1c > 7.5 ";
                } else if ($no == 'c_egfr_60' || $no == 'c_egfr_30' || $no == 'c_metfn_egfr_30' || $no == 'c_acei_arbs_aer30' || $no == 'c_acei_arbs_aer300' || $no == 'c_nsaids' || $no == 'c_egfr_15' || $no == 'c_k_nsaids') {
                    $andWhere .= " AND pt." . $no . " = 1 ";
                } else {

                    $andWhere .= " AND pt." . $no . " = 0";
                }
                $column = $this->getColumn($no, '2');
                if ($column != '') {
                    array_push($gridColumns, $column);
                }
            }
            $checkOr = 0;
            foreach ($_GET['field'] as $key => $value) {

                if ($value == 'c_egfr_30x') {
                    $value = 'c_egfr_30';
                }
//                if ($value == 'c_hba1c7' || $value == 'c_hba1c6') {
//                    $value = 'hba1c';
//                }

                if ($value == 'q9' || $value == 'q14' || $value == 'q15' || $value == 'q16' ||
                        $value == 'q18' || $value == 'x23' || $value == 'x24' || $value == 'x25' || $value == 'x26' ||
                        $value == 'x27' || $value == 'x28' || $value == 'x29' || $value == 'x30' || $value == 'x31' ||
                        $value == 'c_egfr_sl_02' || $value == 'c_egfr_sl_03') {
                    $fieldWhere = "q9";
                } else {
                    $fieldWhere = $value;
                }

                if ($_GET['status'][$key] == 2) {
                    if ($fieldWhere == 'egfr_sl') {
                        if ($_GET['and-or'][$key + 1] == 'OR' && $checkOr == 0) {
                            $andWhere .= " " . $_GET['and-or'][$key] . " (pt." . $fieldWhere . " >= 4";
                            $checkOr = 1;
                        } else {
                            $andWhere .= " " . $_GET['and-or'][$key] . " pt." . $fieldWhere . " >= 4";
                        }
                    } else if ($fieldWhere == 'sbp') {
                        if ($_GET['and-or'][$key + 1] == 'OR' && $checkOr == 0) {
                            $andWhere .= " " . $_GET['and-or'][$key] . " (pt." . $fieldWhere . " > 130 ";
                            $checkOr = 1;
                        } else {
                            $andWhere .= " " . $_GET['and-or'][$key] . " pt." . $fieldWhere . " > 130 ";
                        }
                    } else if ($fieldWhere == 'dbp') {
                        if ($_GET['and-or'][$key + 1] == 'OR' && $checkOr == 0) {
                            $andWhere .= " " . $_GET['and-or'][$key] . " (pt." . $fieldWhere . " > 90";
                            $checkOr = 1;
                        } else {
                            $andWhere .= " " . $_GET['and-or'][$key] . " pt." . $fieldWhere . " > 90";
                        }
                    } else if ($fieldWhere == 'hb') {
                        if ($_GET['and-or'][$key + 1] == 'OR' && $checkOr == 0) {
                            $andWhere .= " " . $_GET['and-or'][$key] . " (pt." . $fieldWhere . " < 10";
                            $checkOr = 1;
                        } else {
                            $andWhere .= " " . $_GET['and-or'][$key] . " pt." . $fieldWhere . " < 10";
                        }
                    } else if ($fieldWhere == 'c_hba1c6') {
                        if ($_GET['and-or'][$key + 1] == 'OR' && $checkOr == 0) {
                            $andWhere .= " " . $_GET['and-or'][$key] . " (pt.hba1c < 6.5";
                            $checkOr = 1;
                        } else {
                            $andWhere .= " " . $_GET['and-or'][$key] . " pt.hba1c  < 6.5";
                        }
                    } else if ($fieldWhere == 'c_hba1c7') {
                        if ($_GET['and-or'][$key + 1] == 'OR' && $checkOr == 0) {
                            $andWhere .= " " . $_GET['and-or'][$key] . " (pt.hba1c > 7.5";
                            $checkOr = 1;
                        } else {
                            $andWhere .= " " . $_GET['and-or'][$key] . " pt.hba1c > 7";
                        }
                    } else if ($fieldWhere == 'c_egfr_60' || $fieldWhere == 'c_egfr_30' || $fieldWhere == 'c_metfn_egfr_30' || $fieldWhere == 'c_acei_arbs_aer30' || $fieldWhere == 'c_acei_arbs_aer300' ||
                            $fieldWhere == 'c_nsaids' || $fieldWhere == 'c_egfr_15' || $fieldWhere == 'c_k_nsaids') {
                        if ($_GET['and-or'][$key + 1] == 'OR' && $checkOr == 0) {
                            $andWhere .= " " . $_GET['and-or'][$key] . " (pt." . $fieldWhere . " = 1";
                            $checkOr = 1;
                        } else {
                            $andWhere .= " " . $_GET['and-or'][$key] . " pt." . $fieldWhere . " = 1";
                        }
                    } else {
                        if ($_GET['and-or'][$key + 1] == 'OR' && $checkOr == 0) {
                            $andWhere .= " " . $_GET['and-or'][$key] . " (pt." . $fieldWhere . " = 0";
                            $checkOr = 1;
                        } else {
                            $andWhere .= " " . $_GET['and-or'][$key] . " pt." . $fieldWhere . " = 0";
                        }
                    }
                }
                if ($_GET['status'][$key] == 1) {
                    if ($fieldWhere == 'egfr_sl') {
                        if ($_GET['and-or'][$key + 1] == 'OR' && $checkOr == 0) {
                            $andWhere .= " " . $_GET['and-or'][$key] . " (pt." . $fieldWhere . " <= 4";
                            $checkOr = 1;
                        } else {
                            $andWhere .= " " . $_GET['and-or'][$key] . " pt." . $fieldWhere . " <= 4";
                        }
                    } else if ($fieldWhere == 'sbp') {
                        if ($_GET['and-or'][$key + 1] == 'OR' && $checkOr == 0) {
                            $andWhere .= " " . $_GET['and-or'][$key] . " (pt." . $fieldWhere . " < 130 ";
                            $checkOr = 1;
                        } else {
                            $andWhere .= " " . $_GET['and-or'][$key] . " pt." . $fieldWhere . " < 130 ";
                        }
                    } else if ($fieldWhere == 'dbp') {
                        if ($_GET['and-or'][$key + 1] == 'OR' && $checkOr == 0) {
                            $andWhere .= " " . $_GET['and-or'][$key] . " (pt." . $fieldWhere . " < 90";
                            $checkOr = 1;
                        } else {
                            $andWhere .= " " . $_GET['and-or'][$key] . " pt." . $fieldWhere . " < 90";
                        }
                    } else if ($fieldWhere == 'hb') {
                        if ($_GET['and-or'][$key + 1] == 'OR' && $checkOr == 0) {
                            $andWhere .= " " . $_GET['and-or'][$key] . " (pt." . $fieldWhere . " > 10";
                            $checkOr = 1;
                        } else {
                            $andWhere .= " " . $_GET['and-or'][$key] . " pt." . $fieldWhere . " > 10";
                        }
                    } else if ($fieldWhere == 'c_hba1c6') {
                        if ($_GET['and-or'][$key + 1] == 'OR' && $checkOr == 0) {
                            $andWhere .= " " . $_GET['and-or'][$key] . " (pt.hba1c > 6.5";
                            $checkOr = 1;
                        } else {
                            $andWhere .= " " . $_GET['and-or'][$key] . " pt.hba1c  > 6.5";
                        }
                    } else if ($fieldWhere == 'c_hba1c7') {
                        if ($_GET['and-or'][$key + 1] == 'OR' && $checkOr == 0) {
                            $andWhere .= " " . $_GET['and-or'][$key] . " (pt.hba1c < 7.5";
                            $checkOr = 1;
                        } else {
                            $andWhere .= " " . $_GET['and-or'][$key] . " pt.hba1c < 7";
                        }
                    } else if ($fieldWhere == 'c_egfr_60' || $fieldWhere == 'c_egfr_30' || $fieldWhere == 'c_metfn_egfr_30' || $fieldWhere == 'c_acei_arbs_aer30' || $fieldWhere == 'c_acei_arbs_aer300' ||
                            $fieldWhere == 'c_nsaids' || $fieldWhere == 'c_egfr_15' || $fieldWhere == 'c_k_nsaids') {
                        if ($_GET['and-or'][$key + 1] == 'OR' && $checkOr == 0) {
                            $andWhere .= " " . $_GET['and-or'][$key] . " (pt." . $fieldWhere . " = 0";
                            $checkOr = 1;
                        } else {
                            $andWhere .= " " . $_GET['and-or'][$key] . " pt." . $fieldWhere . " = 0";
                        }
                    } else {
                        if ($_GET['and-or'][$key + 1] == 'OR' && $checkOr == 0) {
                            $andWhere .= " " . $_GET['and-or'][$key] . " (pt." . $fieldWhere . " = 1";
                            $checkOr = 1;
                        } else {
                            $andWhere .= " " . $_GET['and-or'][$key] . " pt." . $fieldWhere . " = 1";
                        }
                    }
                }
                if ($_GET['status'][$key] == 3) {
                    if ($_GET['and-or'][$key + 1] == 'OR' && $checkOr == 0) {
                        $andWhere .= " " . $_GET['and-or'][$key] . " (pt." . $fieldWhere . " is null";
                        $checkOr = 1;
                    } else {
                        $andWhere .= " " . $_GET['and-or'][$key] . " pt." . $fieldWhere . " is null";
                    }
                }

                if (($_GET['and-or'][$key + 1] == 'AND' || $_GET['and-or'][$key + 1] == '') && $checkOr == 1) {
                    $andWhere .= " )";
                    $checkOr = 0;
                }


                foreach ($this->field[$value] as $vField) {
                    $field .= $vField;
                }
//               

                $column = $this->getColumn($value, $_GET['status'][$key]);
                if ($column != '') {
                    array_push($gridColumns, $column);
                }
            }

            $session = \Yii::$app->session;

            $key_ckd_report = $session['key_ckd_report'];
            $convert = $session['convert_ckd_char'];
            $save = $session['save_ckd_key'];

            if ($save == '') {
                unset($session['key_ckd_report']);
                unset($session['save_ckd_key']);
            }

            if ($key_ckd_report) {
                $pname = " decode(unhex(p.`Name`),sha2('$key_ckd_report',256)) AS name,decode(unhex(p.`lname`),sha2('$key_ckd_report',256)) AS lname,p.pid AS pid,p.ptlink AS ptlink";
                if ($convert == 1) {
                    $pname = " convert(decode(unhex(p.`Name`),sha2('$key_ckd_report',256)) using tis620) AS name,convert(decode(unhex(p.lname),sha2('$key_ckd_report',256)) using tis620) AS lname,p.pid AS pid,p.ptlink AS ptlink";
                } else {
                    unset($session['convert_ckd_char']);
                }
            } else {
                $pname = " p.`Name` AS name,p.Lname AS lname,p.pid AS pid,p.ptlink AS ptlink";
            }
//            if($mode == 'central'){
//                $sql = "SELECT " . $pname . $field .
//                        " FROM patient_profile_hospital_check AS pt" .
//                        " INNER JOIN f_person as p ON pt.ptlink = p.ptlink  " .
//                        " WHERE (p.HOSPCODE = :hospcode AND p.sitecode = :hospcode)" . $andWhere;
//            }else if($mode == 'hospital'){
            $sql = "SELECT " . $pname . $field .
                    " FROM patient_profile_hospital_check AS pt" .
                    " INNER JOIN f_person as p ON  pt.pid = p.PID AND pt.ptlink = p.ptlink  AND pt.hospcode = p.sitecode AND pt.hospcode = p.HOSPCODE" .
                    " WHERE p.TypeArea IN ('1','3') AND (pt.hospcode = :hospcode AND p.HOSPCODE = :hospcode AND p.sitecode = :hospcode)" . $andWhere;
//            }
//            echo $sql;            exit();
//            \appxq\sdii\utils\VarDumper::dump($gridColumns);
            $dataProvider = new \yii\data\ArrayDataProvider([
                'allModels' => CkdnetFunc::queryAll($sql, $whereParam),
                'pagination' => [
                    'pageSize' => 20,
                ],
            ]);

//            \appxq\sdii\utils\VarDumper::dump($dataProvider->getModels());

            return $this->renderAjax('_gridview', [
                        'dataProvider' => $dataProvider,
                        'gridColumns' => $gridColumns,
                        'no' => $_GET['no'],
                        'results' => $this->results
            ]);
        } else {
            return $this->redirect('index');
        }
    }

    public function actionShowNotify() {
        return $this->renderAjax('show-notify', [
                    'no' => $_GET['no'],
                    'results' => $this->results
        ]);
    }

    public function actionLoadForm() {
        return $this->renderAjax('_form', ['no' => $_GET['no'], 'numId' => $_GET['numId'], 'results' => $this->results]);
    }

    public function getColumn($value, $status) {
        if ($value == 'sbp') {
            return [
                'attribute' => $value,
                'label' => $this->results[$value],
                'format' => 'raw',
                'value' => function($model)use($value, $status) {
//                    print_r($model);
//                    if ($model['sbp'] != '') {
                    $text .= " SBP : " . $model['sbp'] . " | วันที่ " . $model['bp_date'];
                    if ($status == 1) {
                        return "<font color='green'><i class='fa fa-circle'> </i><br> " . $text . "</font>";
                    } else if ($status == 2) {
                        return "<font color='red'><i class='fa fa-circle'> </i> <br> " . $text . "</font>";
                    } else {
                        return '<b>N/A</b>';
                    }
//                    } 
                },
                'contentOptions' => ['style' => 'text-align:center;  '],
                'headerOptions' => ['style' => 'text-align:center;'],
            ];
        }
        if ($value == 'dbp') {
            return [
                'attribute' => $value,
                'label' => $this->results[$value],
                'format' => 'raw',
                'value' => function($model)use($value, $status) {
//                    print_r($model);
//                    if ($model['sbp'] != '') {
                    $text .= " DBP : " . $model['dbp'] . " | วันที่ " . $model['bp_date'];
                    if ($status == 1) {
                        return "<font color='green'><i class='fa fa-circle'> </i><br> " . $text . "</font>";
                    } else if ($status == 2) {
                        return "<font color='red'><i class='fa fa-circle'> </i> <br> " . $text . "</font>";
                    } else {
                        return '<b>N/A</b>';
                    }
//                    } 
                },
                'contentOptions' => ['style' => 'text-align:center;  '],
                'headerOptions' => ['style' => 'text-align:center;'],
            ];
        }
        if ($value == 'c_acei_arbs') {
            return [
                'attribute' => $value,
                'label' => $this->results[$value],
                'format' => 'raw',
                'value' => function($model)use($value, $status) {
                    if ($model['c_acei_arbs'] != '') {
                        $model['acei'] != '' ? $text .= " " . $model['acei'] . " | วันที่ " . $model['acei_date'] . '<hr>' : '';
                        $model['arbs'] != '' ? $text .= " " . $model['arbs'] . " | วันที่ " . $model['arbs_date'] : '';

                        if ($status == 1) {
                            return "<font color='green'><i class='fa fa-circle'> </i><br> " . $text . "</font>";
                        } else if ($status == 2) {
                            return "<font color='red'><i class='fa fa-circle'> </i><br> " . $text . "</font>";
                        } else {
                            return '<b>N/A</b>';
                        }
                    } else {
                        return '<b>N/A</b>';
                    }
                },
                'contentOptions' => ['style' => 'text-align:center;  '],
                'headerOptions' => ['style' => 'text-align:center;'],
            ];
        }
        if ($value == 'egfr_sl') {
            return [
                'attribute' => $value,
                'label' => $this->results[$value],
                'format' => 'raw',
                'value' => function($model)use($value, $status) {
//                    if ($model['egfr_sl'] != '') {
//                        $model['egfr_sl'] != '' ? 
//                    return number_format($model['egfr_sl'], 1);
                    $text .= " " . number_format($model['egfr_sl'], 1) . " | วันที่ " . $model['egfr_date'];
                    if ($status == 1) {
                        return "<font color='green'><i class='fa fa-circle' </i> <br>" . $text . "</font>";
                    } else if ($status == 2) {
                        return "<font color='red'><i class='fa fa-circle'> </i><br> " . $text . "</font>";
                    } else {
                        return '<b>N/A</b>';
                    }
//                    } else {
//                        return '<b>N/A</b>';
//                    }
                },
                'contentOptions' => ['style' => 'text-align:center;  '],
                'headerOptions' => ['style' => 'text-align:center;'],
            ];
        }
        if ($value == 'hb') {
            return [
                'attribute' => $value,
                'label' => $this->results[$value],
                'format' => 'raw',
                'value' => function($model)use($value, $status) {
//                    if ($model['c_hb'] != '') {
                    $model['hb'] != '' ? $text .= " " . $model['hb'] . " | วันที่ " . $model['hb_date'] : '';
                    if ($status == 1) {
                        return "<font color='green'><i class='fa fa-circle' </i> <br>" . $text . "</font>";
                    } else if ($status == 2) {
                        return "<font color='red'><i class='fa fa-circle'> </i><br> " . $text . "</font>";
                    } else {
                        return '<b>N/A</b>';
                    }
//                    } else {
//                        return '<b>N/A</b>';
//                    }
                },
                'contentOptions' => ['style' => 'text-align:center;  '],
                'headerOptions' => ['style' => 'text-align:center;'],
            ];
        }
        if ($value == 'c_hba1c6' || $value == 'c_hba1c7') {
            return [
                'attribute' => 'hba1c',
                'label' => $this->results[$value],
                'format' => 'raw',
                'value' => function($model)use($value, $status) {
//                    if ($model['c_hba1c'] != '') {
                    $model['hba1c'] != '' ? $text .= " " . $model['hba1c'] . " | วันที่ " . $model['hba1c_date'] : '';
                    if ($status == 1) {
                        return "<font color='green'><i class='fa fa-circle' </i><br> " . $text . "</font>";
                    } else if ($status == 2) {
                        return "<font color='red'><i class='fa fa-circle'> </i><br> " . $text . "</font>";
                    } else {
                        return '<b>N/A</b>';
                    }
//                    } else {
//                        return '<b>N/A</b>';
//                    }
                },
                'contentOptions' => ['style' => 'text-align:center;  '],
                'headerOptions' => ['style' => 'text-align:center;'],
            ];
        }

        if ($value == 'c_ldl') {
            return [
                'attribute' => $value,
                'label' => $this->results[$value],
                'format' => 'raw',
                'value' => function($model)use($value, $status) {
//                    if ($model['c_ldl'] != '') {
                    $model['ldl'] != '' ? $text .= " " . $model['ldl'] . " | วันที่ " . $model['ldl_date'] : '';
                    if ($status == 1) {
                        return "<font color='green'><i class='fa fa-circle' </i> <br>" . $text . "</font>";
                    } else if ($status == 2) {
                        return "<font color='red'><i class='fa fa-circle'> </i><br> " . $text . "</font>";
                    } else {
                        return '<b>N/A</b>';
                    }
//                    } else {
//                        return '<b>N/A</b>';
//                    }
                },
                'contentOptions' => ['style' => 'text-align:center;  '],
                'headerOptions' => ['style' => 'text-align:center;'],
            ];
        }

        if ($value == 'c_srp') {
            return [
                'attribute' => $value,
                'label' => $this->results[$value],
                'format' => 'raw',
                'value' => function($model)use($value, $status) {
                    if ($model['c_srp'] != '') {
                        $model['srp'] != '' ? $text .= " " . $model['srp'] . " | วันที่ " . $model['srp_date'] : '';
                        if ($status == 1) {
                            return "<font color='green'><i class='fa fa-circle' </i><br> " . $text . "</font>";
                        } else if ($status == 2) {
                            return "<font color='red'><i class='fa fa-circle'> </i> <br>" . $text . "</font>";
                        } else {
                            return '<b>N/A</b>';
                        }
                    } else {
                        return '<b>N/A</b>';
                    }
                },
                'contentOptions' => ['style' => 'text-align:center;  '],
                'headerOptions' => ['style' => 'text-align:center;'],
            ];
        }

        if ($value == 'c_srb') {
            return [
                'attribute' => $value,
                'label' => $this->results[$value],
                'format' => 'raw',
                'value' => function($model)use($value, $status) {
                    if ($model['c_srb'] != '') {
                        $model['srb'] != '' ? $text .= " " . $model['srb'] . " | วันที่ " . $model['srb_date'] : '';
                        if ($status == 1) {
                            return "<font color='green'><i class='fa fa-circle' </i> <br>" . $text . "</font>";
                        } else if ($status == 2) {
                            return "<font color='red'><i class='fa fa-circle'> </i><br> " . $text . "</font>";
                        } else {
                            return '<b>N/A</b>';
                        }
                    } else {
                        return '<b>N/A</b>';
                    }
                },
                'contentOptions' => ['style' => 'text-align:center;  '],
                'headerOptions' => ['style' => 'text-align:center;'],
            ];
        }

        if ($value == 'q9') {
            return [
                'attribute' => $value,
                'label' => $this->results[$value],
                'format' => 'raw',
                'value' => function($model)use($value, $status) {
                    if ($model['q9'] != '') {
                        $model['q9x'] != '' ? $text .= " " . $model['q9x'] . " | วันที่ " . $model['q9xx'] : '';
                        if ($status == 1) {
                            return "<font color='green'><i class='fa fa-circle' </i> <br>" . $text . "</font>";
                        } else if ($status == 2) {
                            return "<font color='red'><i class='fa fa-circle'> </i><br> " . $text . "</font>";
                        } else {
                            return '<b>N/A</b>';
                        }
                    } else {
                        return '<b>N/A</b>';
                    }
                },
                'contentOptions' => ['style' => 'text-align:center;  '],
                'headerOptions' => ['style' => 'text-align:center;'],
            ];
        }

        if ($value == 'c_upcraer') {
            return [
                'attribute' => $value,
                'label' => $this->results[$value],
                'format' => 'raw',
                'value' => function($model)use($value, $status) {
                    if ($model['c_upcraer'] != '') {
                        $model['upcr'] != '' ? $text .= " UPCR : " . $model['upcr'] . " | วันที่ " . $model['upcr_date'] . '<hr>' : '';
                        $model['aer'] != '' ? $text .= " Urine  protein 24 hrs : " . $model['aer'] . " | วันที่ " . $model['aer_date'] : '';
                        if ($status == 1) {
                            return "<font color='green'><i class='fa fa-circle' </i> <br> " . $text . "</font>";
                        } else if ($status == 2) {
                            return "<font color='red'><i class='fa fa-circle'> </i> <br> " . $text . "</font>";
                        } else {
                            return '<b>N/A</b>';
                        }
                    } else {
                        return '<b>N/A</b>';
                    }
                },
                'contentOptions' => ['style' => 'text-align:center;  '],
                'headerOptions' => ['style' => 'text-align:center;'],
            ];
        }

        if ($value == 'c_upcr_aer_500') {
            return [
                'attribute' => $value,
                'label' => $this->results[$value],
                'format' => 'raw',
                'value' => function($model)use($value, $status) {
                    if ($model['c_upcr_aer_500'] != '') {
                        $model['upcr'] != '' ? $text .= " " . $model['upcr'] . " | วันที่ " . $model['upcr_date'] . "<hr>" : '';
                        $model['aer'] != '' ? $text .= " " . $model['aer'] . " | วันที่ " . $model['aer_date'] . "<hr>" : '';
                        if ($status == 1) {
                            return "<font color='green'><i class='fa fa-circle' </i> <br>" . $text . "</font>";
                        } else if ($status == 2) {
                            return "<font color='red'><i class='fa fa-circle'> </i><br> " . $text . "</font>";
                        } else {
                            return '<b>N/A</b>';
                        }
                    } else {
                        return '<b>N/A</b>';
                    }
                },
                'contentOptions' => ['style' => 'text-align:center;  '],
                'headerOptions' => ['style' => 'text-align:center;'],
            ];
        }

        if ($value == 'c_spp_45') {
            return [
                'attribute' => $value,
                'label' => $this->results[$value],
                'format' => 'raw',
                'value' => function($model)use($value, $status) {
                    if ($model['c_spp_45'] != '') {
                        $model['spp'] != '' ? $text .= " " . $model['spp'] . " | วันที่ " . $model['spp_date'] . "<hr>" : '';
                        if ($status == 1) {
                            return "<font color='green'><i class='fa fa-circle' </i> <br>" . $text . "</font>";
                        } else if ($status == 2) {
                            return "<font color='red'><i class='fa fa-circle'> </i><br> " . $text . "</font>";
                        } else {
                            return '<b>N/A</b>';
                        }
                    } else {
                        return '<b>N/A</b>';
                    }
                },
                'contentOptions' => ['style' => 'text-align:center;  '],
                'headerOptions' => ['style' => 'text-align:center;'],
            ];
        }

        if ($value == 'c_srh') {
            return [
                'attribute' => $value,
                'label' => $this->results[$value],
                'format' => 'raw',
                'value' => function($model)use($value, $status) {
                    if ($model['c_srh'] != '') {
                        $model['srh'] != '' ? $text .= " " . $model['srh'] . " | วันที่ " . $model['srh_date'] . "<hr>" : '';
                        if ($status == 1) {
                            return "<font color='green'><i class='fa fa-circle' </i> <br>" . $text . "</font>";
                        } else if ($status == 2) {
                            return "<font color='red'><i class='fa fa-circle'> </i><br> " . $text . "</font>";
                        } else {
                            return '<b>N/A</b>';
                        }
                    } else {
                        return '<b>N/A</b>';
                    }
                },
                'contentOptions' => ['style' => 'text-align:center;  '],
                'headerOptions' => ['style' => 'text-align:center;'],
            ];
        }

        if ($value == 'q14') {
            return [
                'attribute' => $value,
                'label' => $this->results[$value],
                'format' => 'raw',
                'value' => function($model)use($value, $status) {
                    if ($model['q14'] != '') {
                        $model['q14x'] != '' ? $text .= " " . $model['q14x'] . " | วันที่ " . $model['q14xx'] . "<hr>" : '';
                        if ($status == 1) {
                            return "<font color='green'><i class='fa fa-circle' </i> <br>" . $text . "</font>";
                        } else if ($status == 2) {
                            return "<font color='red'><i class='fa fa-circle'> </i><br> " . $text . "</font>";
                        } else {
                            return '<b>N/A</b>';
                        }
                    } else {
                        return '<b>N/A</b>';
                    }
                },
                'contentOptions' => ['style' => 'text-align:center;  '],
                'headerOptions' => ['style' => 'text-align:center;'],
            ];
        }

        if ($value == 'q15') {
            return [
                'attribute' => $value,
                'label' => $this->results[$value],
                'format' => 'raw',
                'value' => function($model)use($value, $status) {
                    if ($model['q15'] != '') {
                        $model['q15x'] != '' ? $text .= " " . $model['q15x'] . " | วันที่ " . $model['q15xx'] . "<hr>" : '';
                        if ($status == 1) {
                            return "<font color='green'><i class='fa fa-circle' </i> <br>" . $text . "</font>";
                        } else if ($status == 2) {
                            return "<font color='red'><i class='fa fa-circle'> </i><br> " . $text . "</font>";
                        } else {
                            return '<b>N/A</b>';
                        }
                    } else {
                        return '<b>N/A</b>';
                    }
                },
                'contentOptions' => ['style' => 'text-align:center;  '],
                'headerOptions' => ['style' => 'text-align:center;'],
            ];
        }

        if ($value == 'q16') {
            return [
                'attribute' => $value,
                'label' => $this->results[$value],
                'format' => 'raw',
                'value' => function($model)use($value, $status) {
                    if ($model['q16'] != '') {
                        $model['q16x'] != '' ? $text .= " " . $model['q16x'] . " | วันที่ " . $model['q16xx'] . "<hr>" : '';
                        if ($status == 1) {
                            return "<font color='green'><i class='fa fa-circle' </i> <br>" . $text . "</font>";
                        } else if ($status == 2) {
                            return "<font color='red'><i class='fa fa-circle'> </i><br> " . $text . "</font>";
                        } else {
                            return '<b>N/A</b>';
                        }
                    } else {
                        return '<b>N/A</b>';
                    }
                },
                'contentOptions' => ['style' => 'text-align:center;  '],
                'headerOptions' => ['style' => 'text-align:center;'],
            ];
        }

        if ($value == 'c_egfr_60') {
            return [
                'attribute' => $value,
                'label' => $this->results[$value],
                'format' => 'raw',
                'value' => function($model)use($value, $status) {
//                    if ($model['c_egfr_60'] != '') {
                    $model['egfr'] != '' ? $text .= " " . $model['egfr'] . " | วันที่ " . $model['egfr_date'] . "<hr>" : '';
                    if ($status == 1) {
                        return "<font color='green'><i class='fa fa-circle' </i> <br>" . $text . "</font>";
                    } else if ($status == 2) {
                        return "<font color='red'><i class='fa fa-circle'> </i><br> " . $text . "</font>";
                    } else {
                        return '<b>N/A</b>';
                    }
//                    } else {
//                        return '<b>N/A</b>';
//                    }
                },
                'contentOptions' => ['style' => 'text-align:center;  '],
                'headerOptions' => ['style' => 'text-align:center;'],
            ];
        }

        if ($value == 'q18') {
            return [
                'attribute' => $value,
                'label' => $this->results[$value],
                'format' => 'raw',
                'value' => function($model)use($value, $status) {
                    if ($model['q18'] != '') {
                        $model['q18x'] != '' ? $text .= " " . $model['q18x'] . " | วันที่ " . $model['q18xx'] . "<hr>" : '';
                        if ($status == 1) {
                            return "<font color='green'><i class='fa fa-circle' </i> <br>" . $text . "</font>";
                        } else if ($status == 2) {
                            return "<font color='red'><i class='fa fa-circle'> </i><br> " . $text . "</font>";
                        } else {
                            return '<b>N/A</b>';
                        }
                    } else {
                        return '<b>N/A</b>';
                    }
                },
                'contentOptions' => ['style' => 'text-align:center;  '],
                'headerOptions' => ['style' => 'text-align:center;'],
            ];
        }

        if ($value == 'c_egfr_30') {
            return [
                'attribute' => $value,
                'label' => $this->results[$value],
                'format' => 'raw',
                'value' => function($model)use($value, $status) {
//                    if ($model['c_egfr_30'] != '') {
                    $model['egfr'] != '' ? $text .= " " . $model['egfr'] . " | วันที่ " . $model['egfr_date'] : '';
                    if ($status == 1) {
                        return "<font color='green'><i class='fa fa-circle' </i> <br>" . $text . "</font>";
                    } else if ($status == 2) {
                        return "<font color='red'><i class='fa fa-circle'> </i><br> " . $text . "</font>";
                    } else {
                        return '<b>N/A</b>';
                    }
//                    } else {
//                        return '<b>N/A</b>';
//                    }
                },
                'contentOptions' => ['style' => 'text-align:center;  '],
                'headerOptions' => ['style' => 'text-align:center;'],
            ];
        }

        if ($value == 'c_metfn_egfr_30') {
            return [
                'attribute' => $value,
                'label' => $this->results[$value],
                'format' => 'raw',
                'value' => function($model)use($value, $status) {
//                    if ($model['c_metfn_egfr_30'] != '') {
                    $model['metfn'] != '' ? $text .= " " . $model['metfn'] . " | วันที่ " . $model['metfn_date'] . "<hr>" : '';
                    $model['egfr'] != '' ? $text .= " egfr " . $model['egfr'] . " | วันที่ " . $model['egfr_date'] : '';
                    if ($status == 1) {
                        return "<font color='green'><i class='fa fa-circle' </i> <br>" . $text . "</font>";
                    } else if ($status == 2) {
                        return "<font color='red'><i class='fa fa-circle'> </i><br> " . $text . "</font>";
                    } else {
                        return '<b>N/A</b>';
                    }
//                    } else {
//                        return '<b>N/A</b>';
//                    }
                },
                'contentOptions' => ['style' => 'text-align:center;  '],
                'headerOptions' => ['style' => 'text-align:center;'],
            ];
        }

        if ($value == 'c_acei_arbs_aer30') {
            return [
                'attribute' => $value,
                'label' => $this->results[$value],
                'format' => 'raw',
                'value' => function($model)use($value, $status) {
                    if ($model['c_acei_arbs_aer30'] != '') {
                        $model['acei'] != '' ? $text .= " " . $model['acei'] . " | วันที่ " . $model['acei_date'] . "<hr>" : '';
                        $model['arbs'] != '' ? $text .= " " . $model['arbs'] . " | วันที่ " . $model['arbs_date'] . "<hr>" : '';
                        $model['aer'] != '' ? $text .= " " . $model['aer'] . " | วันที่ " . $model['aer_date'] : '';
                        if ($status == 1) {
                            return "<font color='green'><i class='fa fa-circle' </i> <br>" . $text . "</font>";
                        } else if ($status == 2) {
                            return "<font color='red'><i class='fa fa-circle'> </i><br> " . $text . "</font>";
                        } else {
                            return '<b>N/A</b>';
                        }
                    } else {
                        return '<b>N/A</b>';
                    }
                },
                'contentOptions' => ['style' => 'text-align:center;  '],
                'headerOptions' => ['style' => 'text-align:center;'],
            ];
        }

        if ($value == 'c_acei_arbs_aer300') {
            return [
                'attribute' => $value,
                'label' => $this->results[$value],
                'format' => 'raw',
                'value' => function($model)use($value, $status) {
                    if ($model['c_acei_arbs_aer300'] != '') {
                        $model['acei'] != '' ? $text .= " " . $model['acei'] . " | วันที่ " . $model['acei_date'] . "<hr>" : '';
                        $model['arbs'] != '' ? $text .= " " . $model['arbs'] . " | วันที่ " . $model['arbs_date'] . "<hr>" : '';
                        $model['aer'] != '' ? $text .= " " . $model['aer'] . " | วันที่ " . $model['aer_date'] : '';
                        if ($status == 1) {
                            return "<font color='green'><i class='fa fa-circle' </i> <br>" . $text . "</font>";
                        } else if ($status == 2) {
                            return "<font color='red'><i class='fa fa-circle'> </i><br> " . $text . "</font>";
                        } else {
                            return '<b>N/A</b>';
                        }
                    } else {
                        return '<b>N/A</b>';
                    }
                },
                'contentOptions' => ['style' => 'text-align:center;  '],
                'headerOptions' => ['style' => 'text-align:center;'],
            ];
        }

        if ($value == 'x23' || $value == 'x24' || $value == 'x25' || $value == 'x26' || $value == 'x27') {
            return [
                'attribute' => $value,
                'label' => $this->results[$value],
                'format' => 'raw',
                'value' => function($model)use($value, $status) {
                    if ($model['x'] != '') {
                        $model['xx'] != '' ? $text .= " " . $model['xx'] . " | วันที่ " . $model['xxx'] : '';
                        if ($status == 1) {
                            return "<font color='green'><i class='fa fa-circle' </i> <br>" . $text . "</font>";
                        } else if ($status == 2) {
                            return "<font color='red'><i class='fa fa-circle'> </i><br> " . $text . "</font>";
                        } else {
                            return '<b>N/A</b>';
                        }
                    } else {
                        return '<b>N/A</b>';
                    }
                },
                'contentOptions' => ['style' => 'text-align:center;  '],
                'headerOptions' => ['style' => 'text-align:center;'],
            ];
        }

        if ($value == 'c_nsaids') {
            return [
                'attribute' => $value,
                'label' => $this->results[$value],
                'format' => 'raw',
                'value' => function($model)use($value, $status) {
                    if ($model['c_nsaids'] != '') {
                        $model['nsaids'] != '' ? $text .= " " . $model['nsaids'] . " | วันที่ " . $model['nsaids_date'] : '';
                        if ($status == 1) {
                            return "<font color='green'><i class='fa fa-circle' </i> <br>" . $text . "</font>";
                        } else if ($status == 2) {
                            return "<font color='red'><i class='fa fa-circle'> </i><br> " . $text . "</font>";
                        } else {
                            return '<b>N/A</b>';
                        }
                    } else {
                        return '<b>N/A</b>';
                    }
                },
                'contentOptions' => ['style' => 'text-align:center;  '],
                'headerOptions' => ['style' => 'text-align:center;'],
            ];
        }


        if ($value == 'x29' || $value == 'x30' || $value == 'x31') {
            return [
                'attribute' => $value,
                'label' => $this->results[$value],
                'format' => 'raw',
                'value' => function($model)use($value, $status) {
                    if ($model['x'] != '') {
                        $model['xx'] != '' ? $text .= " " . $model['xx'] . " | วันที่ " . $model['xxx'] : '';
                        if ($status == 1) {
                            return "<font color='green'><i class='fa fa-circle' </i> <br>" . $text . "</font>";
                        } else if ($status == 2) {
                            return "<font color='red'><i class='fa fa-circle'> </i><br> " . $text . "</font>";
                        } else {
                            return '<b>N/A</b>';
                        }
                    } else {
                        return '<b>N/A</b>';
                    }
                },
                'contentOptions' => ['style' => 'text-align:center; max-width:150px; word-wrap: break-word; '],
                'headerOptions' => ['style' => 'text-align:center;max-width:150px; overflow: auto; word-wrap: break-word;'],
            ];
        }

        if ($value == 'c_egfr_sl_02' || $value == 'c_egfr_sl_03') {
            return [
                'attribute' => $value,
                'label' => $this->results[$value],
                'format' => 'raw',
                'value' => function($model)use($value, $status) {
                    if ($model['c_egfr_sl_02'] != '' || $model['c_egfr_sl_03'] != '') {
//                        $model['c_egfr_sl_02'] != '' ? $text .= " " . $model['c_egfr_sl_02'] . " | วันที่ " . $model['c_egfr_sl_02_date'] : '';
                        if ($status == 1) {
                            return "<font color='green'><i class='fa fa-circle' </i> <br>" . $text . "</font>";
                        } else if ($status == 2) {
                            return "<font color='red'><i class='fa fa-circle'> </i><br> " . $text . "</font>";
                        } else {
                            return '<b>N/A</b>';
                        }
                    } else {
                        return '<b>N/A</b>';
                    }
                },
                'contentOptions' => ['style' => 'text-align:center;  '],
                'headerOptions' => ['style' => 'text-align:center;'],
            ];
        }

        if ($value == 'c_egfr_30x') {
            return [
                'attribute' => $value,
                'label' => $this->results[$value],
                'format' => 'raw',
                'value' => function($model)use($value, $status) {
                    if ($model['c_egfr_30'] != '') {
                        $model['egfr'] != '' ? $text .= " " . $model['egfr'] . " | วันที่ " . $model['egfr_date'] : '';
                        if ($status == 1) {
                            return "<font color='green'><i class='fa fa-circle' </i> <br>" . $text . "</font>";
                        } else if ($status == 2) {
                            return "<font color='red'><i class='fa fa-circle'> </i><br> " . $text . "</font>";
                        } else {
                            return '<b>N/A</b>';
                        }
                    } else {
                        return '<b>N/A</b>';
                    }
                },
                'contentOptions' => ['style' => 'text-align:center;  '],
                'headerOptions' => ['style' => 'text-align:center;'],
            ];
        }

        if ($value == 'c_egfr_15') {
            return [
                'attribute' => $value,
                'label' => $this->results[$value],
                'format' => 'raw',
                'value' => function($model)use($value, $status) {
                    if ($model['c_egfr_15'] != '') {
                        $model['egfr'] != '' ? $text .= " " . $model['egfr'] . " | วันที่ " . $model['egfr_date'] : '';
                        if ($status == 1) {
                            return "<font color='green'><i class='fa fa-circle' </i> <br>" . $text . "</font>";
                        } else if ($status == 2) {
                            return "<font color='red'><i class='fa fa-circle'> </i><br> " . $text . "</font>";
                        } else {
                            return '<b>N/A</b>';
                        }
                    } else {
                        return '<b>N/A</b>';
                    }
                },
                'contentOptions' => ['style' => 'text-align:center;  '],
                'headerOptions' => ['style' => 'text-align:center;'],
            ];
        }

        if ($value == 'c_k_nsaids') {
            return [
                'attribute' => $value,
                'label' => $this->results[$value],
                'format' => 'raw',
                'value' => function($model)use($value, $status) {
                    if ($model['c_k_nsaids'] != '') {
                        $model['srp'] != '' ? $text .= " " . $model['srp'] . " | วันที่ " . $model['srp_date'] . '<hr>' : '';
                        $model['arbs'] != '' ? $text .= " " . $model['arbs'] . " | วันที่ " . $model['arbs_date'] . '<hr>' : '';
                        $model['acei'] != '' ? $text .= " " . $model['acei'] . " | วันที่ " . $model['acei_date'] . '<hr>' : '';
                        $model['nsaids'] != '' ? $text .= " " . $model['nsaids'] . " | วันที่ " . $model['nsaids_date'] : '';
                        if ($status == 1) {
                            return "<font color='green'><i class='fa fa-circle' </i> <br>" . $text . "</font>";
                        } else if ($status == 2) {
                            return "<font color='red'><i class='fa fa-circle'> </i><br> " . $text . "</font>";
                        } else {
                            return '<b>N/A</b>';
                        }
                    } else {
                        return '<b>N/A</b>';
                    }
                },
                'contentOptions' => ['style' => 'text-align:center;  '],
                'headerOptions' => ['style' => 'text-align:center;'],
            ];
        }
    }

//    public function getValue($value) {
//        $field = [
//            'c_bp_140_90' => 'BP ≤ 140/90 mmHg',
//            'c_acei_arbs' => 'ได้รับ ACEI /ARBs',
//            'egfr_sl' => 'Slope ของ eGFR ต่อระยะเวลา ≥ 4',
//            'c_hb' => 'Hb  ≥ 10 g/dl',
//            'c_hba1c' => 'HbA1C  < 7 %',
//            'c_ldl' => 'LDL cholesterol < 100 mg/dl',
//            'c_srp' => 'serum potassium < 5.5 mEq/L',
//            'c_srb' => 'serum bicarbonate > 22 mEq/L',
//            'q9' => 'ได้รับการประเมิน Urine Protien Strip  (รพ.ระดับ F-M)',
//            'c_upcraer' => 'ได้รับการประเมิน UPCR หรือ urine protein 24 hrs',
//            'c_upcr_aer_500' => 'UPCR < 500 mg/g  หรือ Urine  protein 24 hrs < 500 mg/day',
//            'c_spp_45' => 'Serum phosphate < 4.5 mg/L',
//            'c_srh' => 'Serum parathyroid hormone (PTH)  อยู่ในเกณฑ์ปกติ',
//            'q14' => 'ได้รับการเตรียม AVF ก่อนเริ่ม hemodialysis',
//            'q15' => 'ได้เข้าร่วม educational class ในหัวข้อต่างๆครบ',
//            'q16' => 'แจ้งค่า eGFR และระยะของ CKD  ครั้งนี้ และภายใน 3 เดือนที่ผ่านมา',
//            'c_egfr_60' => 'แจ้งค่า eGFR < 60 ควรพบอายุรแพทย์',
//            'q18' => 'มีอัตราการลดลงของ GFR มากกว่าปีละ  5 ml/min/1.73 m2 ',
//            'c_egfr_30' => 'eGFR < 30 ได้รับการให้คำปรึกษาเรื่อง RRT',
//            'c_metfn_egfr_30' => 'eGFR < 30 ยังได้รับยา metformin',
//            'c_acei_arbs_aer30' => 'ผู้ป่วยเบาหวานที่มี albuminuria > 30 mg/day และไม่ได้รับยา ACEI หรือ ARB',
//            'c_acei_arbs_aer300' => 'ไม่ได้เป็นเบาหวานมี albuminuria > 300 mg/day และไม่ได้รับยา ACEI หรือ ARB',
//            'x23' => 'ได้รับ ACEi/ARB  ระวังการเกิด AKI และ hyperkalemia ควรได้รับคำแนะนำการปฏิบัติตัว',
//            'x24' => 'เคยได้รับยา ACEI หรือ ARB แต่ต้องหยุดยาเพราะมี adverse event',
//            'x25' => 'มี ปริมาณโปรตีนในปัสสาวะ ≥ 1+',
//            'x26' => 'ผู้ป่วยเบาหวานที่ไม่ได้ตรวจ urine albumin อย่างน้อย 2 ครั้งต่อปี',
//            'x27' => 'ไม่ได้เป็น CKD และได้รับยา NSAIDs นานกว่า 2 สัปดาห์',
//            'c_nsaids' => 'ผู้ป่วย CKD ที่ได้รับยา NSAIDs ',
//            'x29' => 'เวลาสั่งจ่ายยาในโปรแกรม ถ้ามียาที่ต้องปรับตาม CrCl ถ้าสั่งผิดให้มีการเตือนทุกครั้ง หรือถ้าไม่มีการปรับตาม CrCl ให้เตือนทุกครั้ง',
//            'x30' => 'ประวัติการเคยวินิจฉัยว่ามี acute kidney injury ล่าสุด วัน เดือน ปี เท่าไร ',
//            'x31' => 'มีโรคหรือภาวะที่เสี่ยงต่อการเกิด CKD ได้รับการตรวจ serum creatinine และ urine protein หรือ albumin ปีละ 1 ครั้ง (เบาหวาน, ความดันโลหิตสูง, เก๊าท์, SLE, อายุมากกว่า 60 ปี, ได้รับยาที่ nephrotoxic, ติดเชื้อทางเดินปัสสาวะส่วนบน ≥ 3 ครั้งต่อปี, มีโรคหัวใจและหลอดเลือด, polycystic kidney disease, โรคไตตั้งแต่กำเนิด, มีประวัติโรคไตในครอบครัว)',
//            'c_egfr_sl_02' => 'ตรวจ UA พบโปรตีนหรือหรือเม็ดเลือดแดง ในปัสสาวะพิจารณาส่งปรึกษาแพทย์',
//            'c_egfr_sl_03' => 'Serum creatinine เพิ่มขึ้นมากกว่าหรือเท่ากับ 0.3 mg/dL: acute kidney injury ควรหาสาเหตุ',
//            'c_egfr_30x' => 'GFR < 30 ml/min/1.73m2 พิจารณาส่งปรึกษาอายุรแพทย์โรคไต',
//            'c_egfr_15' => 'GFR < 15 ml/min/1.73m2 ควรส่งประเมินเตรียมความพร้อมในการทำบำบัดทดแทนไต ',
//            'c_k_nsaids' => 'ACI หรือ Potassium >5'
//        ];
//        if (isset($value)) {
//            return $field[$value];
//        } else {
//            return $field;
//        }
//    }
//
//    public function getField($value) {
//        $field = [
//            'c_bp_140_90' => [
//                ',pt.c_bp_140_90',
//                ',pt.sbp',
//                ',pt.dbp',
//                ',pt.bp_date'
//            ],
//            'c_acei_arbs' => [
//                ',pt.c_acei_arbs',
//                ',pt.acei',
//                ',pt.arbs',
//                ',pt.acei_date',
//                ',pt.arbs_date'
//            ],
//            'egfr_sl' => [
//                ',pt.c_egfr_sl_0',
//                ',pt.egfr_sl',
//                ',pt.egfr_date'
//            ],
//            'c_hb' => [
//                ',pt.c_hb',
//                ',pt.hb',
//                ',pt.hb_date'
//            ],
//            'c_hba1c' => [
//                ',pt.c_hba1c',
//                ',pt.hba1c',
//                ',pt.hba1c_date'
//            ],
//            'c_ldl' => [
//                ',pt.c_ldl',
//                ',pt.ldl',
//                ',pt.ldl_date'
//            ],
//            'c_srp' => [
//                ',pt.c_srp',
//                ',pt.srp',
//                ',pt.srp_date'
//            ],
//            'c_srb' => [
//                ',pt.c_srb',
//                ',pt.srb',
//                ',pt.srb_date'
//            ],
//            'q9' => [
//                ",'' AS q9",
//                ",'' AS q9x",
//                ",'' AS q9xx"
//            ],
//            'c_upcraer' => [
//                ',pt.c_upcraer',
//                ',pt.upcr',
//                ',pt.upcr_date',
//                ',pt.aer',
//                ',pt.aer_date'
//            ],
//            'c_upcr_aer_500' => [
//                ',pt.c_upcr_aer_500',
//                ',pt.upcr',
//                ',pt.upcr_date',
//                ',pt.aer',
//                ',pt.aer_date'
//            ],
//            'c_spp_45' => [
//                ',pt.c_spp_45',
//                ',pt.spp',
//                ',pt.spp_date'
//            ],
//            'c_srh' => [
//                ",pt.c_srh",
//                ",pt.srh",
//                ",pt.srh_date"
//            ],
//            'q14' => [
//                ",'' AS q14",
//                ",'' AS q14x",
//                ",'' AS q14xx"
//            ],
//            'q15' => [
//                ",'' AS q15",
//                ",'' AS q15x",
//                ",'' AS q15xx"
//            ],
//            'q16' => [
//                ",'' AS q16",
//                ",'' AS q16x",
//                ",'' AS q16xx"
//            ],
//            'c_egfr_60' => [
//                ',pt.c_egfr_60',
//                ',pt.egfr',
//                ',pt.egfr_date'
//            ],
//            'q18' => [
//                ",'' AS q18",
//                ",'' AS q18x",
//                ",'' AS q18xx"
//            ],
//            'c_egfr_30' => [
//                ',pt.c_egfr_30',
//                ',pt.egfr',
//                ',pt.egfr_date'
//            ],
//            'c_metfn_egfr_30' => [
//                ',pt.c_metfn_egfr_30',
//                ',pt.egfr',
//                ',pt.egfr_date',
//                ',pt.metfn',
//                ',pt.metfn_date'
//            ],
//            'c_acei_arbs_aer30' => [
//                ',pt.c_acei_arbs_aer30',
//                ',pt.aer',
//                ',pt.aer_date',
//                ',pt.acei',
//                ',pt.acei_date',
//                ',pt.arbs',
//                ',pt.arbs_date'
//            ],
//            'c_acei_arbs_aer300' => [
//                ',pt.c_acei_arbs_aer300',
//                ',pt.aer',
//                ',pt.aer_date',
//                ',pt.acei',
//                ',pt.acei_date',
//                ',pt.arbs',
//                ',pt.arbs_date'
//            ],
//            'x23' => [
//                ",'' AS x",
//                ",'' AS xx",
//                ",'' AS xxx"
//            ],
//            'x24' => [
//                ",'' AS x24",
//                ",'' AS x24x",
//                ",'' AS x24xx"
//            ],
//            'x25' => [
//                ",'' AS x",
//                ",'' AS xx",
//                ",'' AS xxx"
//            ],
//            'x26' => [
//                ",'' AS x",
//                ",'' AS xx",
//                ",'' AS xxx"
//            ],
//            'x27' => [
//                ",'' AS x",
//                ",'' AS xx",
//                ",'' AS xxx"
//            ],
//            'c_nsaids' => [
//                ',pt.c_nsaids',
//                ',pt.nsaids',
//                ',pt.nsaids_date'
//            ],
//            'x29' => [
//                ",'' AS x",
//                ",'' AS xx",
//                ",'' AS xxx"
//            ],
//            'x30' => [
//                ",'' AS x",
//                ",'' AS xx",
//                ",'' AS xxx"
//            ],
//            'x31' => [
//                ",'' AS x",
//                ",'' AS xx",
//                ",'' AS xxx"
//            ],
//            'c_egfr_sl_02' => [
//                ',"" AS c_egfr_sl_02'
//            ],
//            'c_egfr_sl_03' => [
//                ',"" AS c_egfr_sl_03'
//            ],
//            'c_egfr_30x' => [
//                ',pt.c_egfr_30',
//                ',pt.egfr_date'
//            ],
//            'c_egfr_15' => [
//                ',pt.c_egfr_15',
//                ',pt.egfr_date'
//            ],
//            'c_k_nsaids' => [
//                ',pt.c_k_nsaids',
//                ',pt.srp',
//                ',pt.srp_date',
//                ',pt.nsaids',
//                ',pt.nsaids_date'
//            ]
//        ];
//
//        return $field[$value];
//    }
}
