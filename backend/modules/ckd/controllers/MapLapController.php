<?php

namespace backend\modules\ckd\controllers;

use yii\web\Controller;
use backend\modules\ckdnet\classes\CkdnetFunc;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\web\Response;
use Yii;
use backend\modules\ckd\models\LabMap;
use yii\base\Model;

class MapLapController extends Controller {

    public $activetab = "home";

    public function actionIndex() {
//         \appxq\sdii\utils\VarDumper::dump(Yii::$app->user->identity->userProfile);
//        if (Yii::$app->getRequest()->isAjax) {

        $model = new LabMap();
        $ckd = CkdnetFunc::getDb();


        if (Yii::$app->request->post()) {

            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

            $post = Yii::$app->request->post();
//                \appxq\sdii\utils\VarDumper::dump($post);
            $labmap = new LabMap();
//                if ($labmap->deleteAll(['HOSPCODE' => Yii::$app->user->identity->userProfile->sitecode])) {

            foreach ($post['LabMap']['items'] as $key => $value) {

                $labItem = "SELECT tdc_lab_name,graph_label,tdc_lab_unit FROM tdc_lab_items_site WHERE tdc_lab_code = :tdc_lab_code AND (hospcode = :hospcode1 OR hospcode = :hospcode2)";

                $labItem = $ckd->createCommand($labItem)->bindValues([
                            ':tdc_lab_code' => $value['tdc_lab_items_code'],
                            ':hospcode1' => Yii::$app->user->identity->userProfile->sitecode,
                            ':hospcode2' => '00000'])->queryOne();

                if (count($value['LABCODE']) != count($value['old_labcode'])) {
                    foreach ($value['old_labcode'] as $vOld) {
                        $d = $labmap->find()->where(['tdc_lab_items_code' => $value['tdc_lab_items_code'], 'LABCODE' => $vOld])->one();

                        if ($d) {
                            $d->delete();
                        }
                    }
                    
                }
//                if (count($value['LABCODE']) == 1) {
//
//                    if ($value['old_labcode'] == '') {
//                        $qLabmap = "REPLACE INTO lab_map (HOSPCODE,LABCODE,hos_lab_items_name,graph_label,hos_lab_unit,tdc_lab_items_code) "
//                                . "VALUES (:hospcode,:labcode,:hos_lab_item_name,:graph_label,:hos_lab_unit,:tdc_lab_item_code)";
//                    } else {
//                        $qLabmap = "UPDATE lab_map SET LABCODE = :labcode,hos_lab_items_name = :hos_lab_item_name,"
//                                . "graph_label = :graph_label,hos_lab_unit = :hos_lab_unit "
//                                . " WHERE HOSPCODE = :hospcode AND tdc_lab_items_code =:tdc_lab_item_code";
//                    }
//                }
//                if (count($value['LABCODE']) > 1) {

                    $qLabmap = "REPLACE INTO lab_map (HOSPCODE,LABCODE,hos_lab_items_name,graph_label,hos_lab_unit,tdc_lab_items_code) "
                            . "VALUES (:hospcode,:labcode,:hos_lab_item_name,:graph_label,:hos_lab_unit,:tdc_lab_item_code)";
//                }


                foreach ($value['LABCODE'] as $labcode) {

//                    $dataLabMap = $labmap->find()->where(['tdc_lab_items_code' => $value['tdc_lab_items_code'], 'LABCODE' => $labcode])->one();
//
//                    if (!$dataLabMap) {
                        try {
                            $ckd->createCommand($qLabmap)->bindValues([
                                ':hospcode' => Yii::$app->user->identity->userProfile->sitecode,
                                ':labcode' => $labcode,
                                ':hos_lab_item_name' => $labItem['tdc_lab_name'],
                                ':graph_label' => $labItem['graph_label'],
                                ':hos_lab_unit' => $labItem['tdc_lab_unit'],
                                ':tdc_lab_item_code' => $value['tdc_lab_items_code'],
//                                        ':old_labcode'=>$value['old_labcode'],
                            ])->execute();
                        } catch (yii\base\Exception $ex) {
                            
                        }
//                    }
                }
            }


            $result = [
                'status' => 'success',
                'message' => '<strong><i class="glyphicon glyphicon-ok-sign"></i> Success!</strong> ',
            ];

            return $result;
        } else {

            $sqlTdc = "SELECT * FROM tdc_lab_items_site WHERE (hospcode = :hospcode1 OR hospcode = :hospcode2) AND display_type IN ('1','2')";

            $dataTdc = $ckd->createCommand($sqlTdc)->bindValues([
                        ':hospcode1' => Yii::$app->user->identity->userProfile->sitecode,
                        ':hospcode2' => '00000'])->queryAll();


            $items = [];
            $i = 0;
            foreach ($dataTdc as $key => $value) {
                $sqlMap = "SELECT * FROM lab_map WHERE HOSPCODE = :hospcode AND tdc_lab_items_code = :tdc_lab_code";

                $dataMap = $ckd->createCommand($sqlMap)->bindValues([
                            ':hospcode' => Yii::$app->user->identity->userProfile->sitecode,
                            ':tdc_lab_code' => $value['tdc_lab_code']])->queryOne();

                $items[$value['tdc_lab_code']]['tdc_lab_items_code'] = $value['tdc_lab_code'];
                $items[$value['tdc_lab_code']]['HOSPCODE'] = Yii::$app->user->identity->userProfile->sitecode;
                $items[$value['tdc_lab_code']]['map_items'] = $value['tdc_lab_code'] . " " . $value['graph_label'] . " (" . $value['tdc_lab_unit'] . ")";
//                \yii\helpers\VarDumper::dump($dataMap);
//                echo "<br/>";
                if(!isset($items[$value['tdc_lab_code']]['LABCODE'])){
                    $items[$value['tdc_lab_code']]['LABCODE'] = [];
                }
                if (($dataMap['LABCODE'] == '' || $dataMap == false) && $value['43files_LABTEST'] != '') {

                    $sqlLap = "SELECT DISTINCT t.tdc_lab_code, t.tdc_lab_name, f.LABTEST, f.LABCODE, f.LABUNIT FROM lab_items_name f JOIN tdc_lab_items_site AS t ON f.LABTEST = t.43files_LABTEST
                                WHERE f.HOSPCODE = :hospcode AND f.LABTEST = :file43";

                    $dataLap = $ckd->createCommand($sqlLap)->bindValues([
                                ':hospcode' => Yii::$app->user->identity->userProfile->sitecode,
                                ':file43' => $value['43files_LABTEST']])->queryAll();
                    
//                    \appxq\sdii\utils\VarDumper::dump($dataLap,false);

                    if ($dataLap > 0 && ($value['43files_LABTEST'] != '' || $value['43files_LABTEST'] != null)) {

//                        if ($dataMap['tdc_lab_items_code'] == '') {
                            
                            foreach ($dataLap as $labV) {
                                $sql = "INSERT INTO lab_map (HOSPCODE,LABCODE,hos_lab_items_name,graph_label,hos_lab_unit,tdc_lab_items_code)"
                                        . " VALUES "
                                        . "(:hospcode,:labcode,:hos_lab_name,:graph_label,:hos_lab_unit,:tdc_lab_item_code)";
                                try {
                                    $ckd->createCommand($sql)->bindValues([
                                        ':hospcode' => Yii::$app->user->identity->userProfile->sitecode,
                                        ':labcode' => $labV['LABCODE'],
                                        ':hos_lab_name' => $value['tdc_lab_name'],
                                        ':graph_label' => $value['graph_label'],
                                        ':hos_lab_unit' => $labV['LABUNIT'],
                                        ':tdc_lab_item_code' => $value['tdc_lab_code']
                                    ])->execute();
                                    $items[$value['tdc_lab_code']]['old_labcode'] = $labV['LABCODE'];
//                                    $items[$i]['LABCODE'] = $labV['LABCODE'];
                                array_push($items[$value['tdc_lab_code']]['LABCODE'], $labV['LABCODE']);
                                } catch (\yii\db\Exception $ex) {
                                    
                                }
                            }

                            
//                        } else {
//                            $sql = "UPDATE lab_map SET LABCODE = :labcode,hos_lab_items_name = :hos_lab_item_name,"
//                                    . "graph_label = :graph_label,hos_lab_unit = :hos_lab_unit "
//                                    . " WHERE HOSPCODE = :hospcode AND tdc_lab_items_code =:tdc_lab_item_code";
//                            try {
//                                $ckd->createCommand($sql)->bindValues([
//                                    ':hospcode' => Yii::$app->user->identity->userProfile->sitecode,
//                                    ':labcode' => $dataLap['LABCODE'],
//                                    ':hos_lab_item_name' => $value['tdc_lab_name'],
//                                    ':graph_label' => $value['graph_label'],
//                                    ':hos_lab_unit' => $dataLap['LABUNIT'],
//                                    ':tdc_lab_item_code' => $dataMap['tdc_lab_items_code']
//                                ])->execute();
//                                $items[$i]['old_labcode'] = $dataLap['LABCODE'];
//                                $items[$i]['LABCODE'] = $dataLap['LABCODE'];
//                            } catch (\yii\db\Exception $ex) {
//                                
//                            }
//                        }
                    }

//                        else if ($dataMap == false) {
//                            $sql = "INSERT INTO lab_map (HOSPCODE,LABCODE,hos_lab_items_name,graph_label,hos_lab_unit,tdc_lab_items_code) "
//                                    . "VALUES "
//                                    . "(:hospcode,'',:tdc_lab_name,:graph_label,'',:tdc_lab_code)";
//
//                            CkdnetFunc::getDb()->createCommand($sql)->bindValues([
//                                ':hospcode' => Yii::$app->user->identity->userProfile->sitecode,
//                                ':tdc_lab_name' => $value['tdc_lab_name'],
//                                ':graph_label' => $value['graph_label'],
//                                ':tdc_lab_code' => $value['tdc_lab_code']
//                            ])->execute();
//                        }
                } else {

                    $items[$value['tdc_lab_code']]['old_labcode'] = $dataMap['LABCODE'];
//                    $items[$i]['LABCODE'] = $dataMap['LABCODE'];
                    array_push($items[$value['tdc_lab_code']]['LABCODE'], $dataMap['LABCODE']);

                    $dataMap2 = $ckd->createCommand($sqlMap)->bindValues([
                                ':hospcode' => Yii::$app->user->identity->userProfile->sitecode,
                                ':tdc_lab_code' => $value['tdc_lab_code']])->queryAll();

                    foreach ($dataMap2 as $v) {

                        $items[$value['tdc_lab_code']]['tdc_lab_items_code'] = $value['tdc_lab_code'];
                        $items[$value['tdc_lab_code']]['map_items'] = $value['tdc_lab_code'] . " " . $value['graph_label'] . " (" . $value['tdc_lab_unit'] . ")";
                        $items[$value['tdc_lab_code']]['old_labcode'] = $v['LABCODE'];
//                        $items[$i]['LABCODE'] = $v['LABCODE'];
                        array_push($items[$value['tdc_lab_code']]['LABCODE'], $v['LABCODE']);

                        $i++;
                    }
                }

                $i++;
            }

//                $qItems = "SELECT tdc_lab_items_code,hos_lab_unit,hos_lab_items_name,LABCODE,HOSPCODE, "
//                        . " CONCAT(IFNULL(tdc_lab_items_code,''),' ',IFNULL(hos_lab_items_name,''),"
//                        . " ' (',IFNULL(hos_lab_unit,''),')') AS map_items FROM lab_map  WHERE lab_map.HOSPCODE = :hospcode ORDER BY CAST(tdc_lab_items_code AS int) ASC ";
//                $model->items = CkdnetFunc::queryAll($qItems, [':hospcode' => Yii::$app->user->identity->userProfile->sitecode]);
            $model->items = $items;
//                \appxq\sdii\utils\VarDumper::dump($items);

            $sql = "SELECT DISTINCT LABCODE,CONCAT(IFNULL(LABCODE,' '),' ',IFNULL(LABNAME,' '),' (',IFNULL(LABUNIT,'-'),')') AS lab_items FROM lab_items_name WHERE HOSPCODE = :hospcode ORDER BY CAST(LABCODE AS int) ASC";
            $labItems = $ckd->createCommand($sql)->bindValues([':hospcode' => Yii::$app->user->identity->userProfile->sitecode])->queryAll();

//            $sql2 = "SELECT CONCAT(IFNULL(tdc_lab_code,''),' ',IFNULL(tdc_lab_name,''),' (',IFNULL(tdc_lab_unit,''),')') AS lab_items,
//                        tdc_lab_code, tdc_lab_name, tdc_lab_unit FROM tdc_lab_items";
//            $mapItems = CkdnetFunc::queryAll($sql2);

            $labItems = ArrayHelper::map($labItems, 'LABCODE', 'lab_items');
            // $mapItems = ArrayHelper::map($mapItems, 'tdc_lab_code', 'lab_items');
//                \appxq\sdii\utils\VarDumper::dump($model);

            return $this->renderAjax('index', ['model' => $model, 'labItems' => $labItems]);
        }
//        } else {
//            return \appxq\sdii\helpers\SDHtml::getMsgError() . Yii::t('app', 'Invalid request. Please do not repeat this request again.');
//        }
    }

    public function actionAddLab() {

        $model = new LabMap();
        $ckd = CkdnetFunc::getDb();
        if ($model->load(Yii::$app->request->post())) {
            $post = Yii::$app->request->post();
            // \appxq\sdii\utils\VarDumper::dump($post);

            foreach ($post['LabMap']['new_items'] as $value) {
                $qLabmap = "REPLACE INTO tdc_lab_items_site (hospcode,tdc_lab_name,graph_label,tdc_lab_unit,note,display_type)" .
                        " VALUES "
                        . "(:hospcode,:tdc_lab_item,:graph_label,:tdc_lab_unit,:note,'1')";

                if ($ckd->createCommand($qLabmap)->bindValues([
                            ':hospcode' => Yii::$app->user->identity->userProfile->sitecode,
                            ':tdc_lab_item' => $value['tdc_lab_name'],
                            ':graph_label' => $value['graph_label'],
                            ':tdc_lab_unit' => $value['tdc_lab_unit'],
                            ':note' => $value['note']
                        ])->execute()) {
                    Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                    $result = [
                        'status' => 'success',
                        'message' => '<strong><i class="glyphicon glyphicon-ok-sign"></i> Success!</strong> ' . Yii::t('app', ''),
                    ];
                } else {
                    Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                    $result = [
                        'status' => 'error',
                        'message' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> error!</strong> ' . Yii::t('app', ''),
                    ];
                }
            }

            return $result;
        }

        return $this->renderAjax('add-lab', ['model' => $model]);
    }

}
