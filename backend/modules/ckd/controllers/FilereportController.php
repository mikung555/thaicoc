<?php

namespace backend\modules\ckd\controllers;

use yii\web\Controller;
use yii\helpers\ArrayHelper;
use Yii;

class FilereportController extends Controller {

    public $activetab = "filereport";
    
    public function actionIndex() {
       $dataPost = \Yii::$app->request->post();

        //\appxq\sdii\utils\VarDumper::dump($dataPost);
//        
        // SELECT Zone 
        $y = (date('m')>9?date("Y")+1:date("Y"))+ 543;
        $year = range( $y, $y);
        //$year = range(date("Y") + 543, 2560);
        $itemYear = [];
        foreach ($year as $value) {
            array_push($itemYear, ['id' => $value - 543, 'value' => $value]);
        }
        $itemYear = ArrayHelper::map($itemYear, 'id', 'value');

        $itemZone = [
            ['id' => '01', 'value' => 'เขตสุขภาพที่ 1'],
            ['id' => '02', 'value' => 'เขตสุขภาพที่ 2'],
            ['id' => '03', 'value' => 'เขตสุขภาพที่ 3'],
            ['id' => '04', 'value' => 'เขตสุขภาพที่ 4'],
            ['id' => '05', 'value' => 'เขตสุขภาพที่ 5'],
            ['id' => '06', 'value' => 'เขตสุขภาพที่ 6'],
            ['id' => '07', 'value' => 'เขตสุขภาพที่ 7'],
            ['id' => '08', 'value' => 'เขตสุขภาพที่ 8'],
            ['id' => '09', 'value' => 'เขตสุขภาพที่ 9'],
            ['id' => '10', 'value' => 'เขตสุขภาพที่ 10'],
            ['id' => '11', 'value' => 'เขตสุขภาพที่ 11'],
            ['id' => '12', 'value' => 'เขตสุขภาพที่ 12'],
        ];
        $itemZone = ArrayHelper::map($itemZone, 'id', 'value');

        $sitecode = Yii::$app->user->identity->userProfile->sitecode;
        $sqlCodeAll = " SELECT hcode, amphurcode, provincecode, province, amphur, `name`, zone_code FROM all_hospital_thai WHERE hcode='$sitecode'";
        $resCodeAll = \Yii::$app->db->createCommand($sqlCodeAll)->queryOne();
        $zone_select = isset($dataPost['zone']) ? $dataPost['zone'] : $resCodeAll['zone_code'];
        
        $select_province = isset($dataPost['province']) ? $dataPost['province'] : $resCodeAll['provincecode'];
        $provincename = self::getNameProvince($select_province); 
        $select_hospital = self::getNameHospital($dataPost['hospital']);
        
        $dataYear = $y;//$dataPost['year']+543;
        //เช็คเงื่อนไข
        //======== Report No.1 ==========
//            if($dataPost['lab'] == 1){
                if ($dataPost['zone'] == '') { // ALL
//                    $linkpage_f0 = "https://cat16.thaicarecloud.org/ckdreport/f0/site/f0_" . $sitecode . ".js";
//                    $linkpage_f2 = "https://cat16.thaicarecloud.org/ckdreport/f2/site/f2_" . $sitecode . ".html";
//                    $linkpage_f3 = "https://cat16.thaicarecloud.org/ckdreport/f3/site/f3_" . $sitecode . ".png";

                    $linkpage_f0 = "https://report.cascap.in.th/ckdreport/f0/site/f0_" . $sitecode . ".js";
                    $linkpage_f2 = "https://report.cascap.in.th/ckdreport/f2/site/f2_" . $sitecode . ".html";
                    $linkpage_f3 = "https://report.cascap.in.th/ckdreport/f3/site/f3_" . $sitecode . ".png";
                    $hospital = $sitecode;
                    $select_hospital = self::getNameHospital($sitecode);
                    $text = "ปีงบประมาณ ".$year[0]." ( ".$select_hospital['hospital']." ) ";
                } else {
                    if ($dataPost['lab'] == 1 ){    
                        if ($dataPost['province'] == '') {
//                            $linkpage_f0 = "https://cat16.thaicarecloud.org/ckdreport/f0/zone/f0_" . $dataPost['zone'] . ".js";
                            $linkpage_f0 = "https://report.cascap.in.th/ckdreport/f0/zone/f0_" . $dataPost['zone'] . ".js";
                            
                            $text = "ปีงบประมาณ ".$dataYear." เขตสุขภาพที่ ".$dataPost['zone'];
                        } else {
                            
                            if ($dataPost['hospital'] == '') {
//                                $linkpage_f0 = "https://cat16.thaicarecloud.org/ckdreport/f0/province/f0_" . $dataPost['province'] . ".js";
//                                $linkpage_f2 = "https://cat16.thaicarecloud.org/ckdreport/f2/province/f2_" . $dataPost['province'] . ".html";
//                                $linkpage_f3 = "https://cat16.thaicarecloud.org/ckdreport/f3/province/f3_" . $dataPost['province'] . ".png";
                                $linkpage_f0 = "https://report.cascap.in.th/ckdreport/f0/province/f0_" . $dataPost['province'] . ".js";
                                $linkpage_f2 = "https://report.cascap.in.th/ckdreport/f2/province/f2_" . $dataPost['province'] . ".html";
                                $linkpage_f3 = "https://report.cascap.in.th/ckdreport/f3/province/f3_" . $dataPost['province'] . ".png";
                                
//                               
                                $text = "ปีงบประมาณ ".$dataYear." ( เขตสุขภาพที่ ".$dataPost['zone']. " จังหวัด ".$provincename['province']." ) ";
                            } else { //all hospital
//                                $linkpage_f0 = "https://cat16.thaicarecloud.org/ckdreport/f0/site/f0_" . $dataPost['hospital'] . ".js";
//                                $linkpage_f2 = "https://cat16.thaicarecloud.org/ckdreport/f2/site/f2_" . $dataPost['hospital'] . ".html";
//                                $linkpage_f3 = "https://cat16.thaicarecloud.org/ckdreport/f3/site/f3_" . $dataPost['hospital'] . ".png";
                                $linkpage_f0 = "https://report.cascap.in.th/ckdreport/f0/site/f0_" . $dataPost['hospital'] . ".js";
                                $linkpage_f2 = "https://report.cascap.in.th/ckdreport/f2/site/f2_" . $dataPost['hospital'] . ".html";
                                $linkpage_f3 = "https://report.cascap.in.th/ckdreport/f3/site/f3_" . $dataPost['hospital'] . ".png";
                                $hospital = $dataPost['hospital'];
                                
//                            
                                $text = "ปีงบประมาณ ".$dataYear." ( เขตสุขภาพที่ ".$dataPost['zone']."  ".$select_hospital['hospital'] . " จังหวัด".$provincename['province']." ) ";    

                            } //chk hospital
                        } //chk province
                    } // chk lab
                } //chk zone
//            } //chk Lap
                
        //\appxq\sdii\utils\VarDumper::dump($linkpage_f0);
        return $this->render('index', [
            'itemYear' => $itemYear,
            'itemZone' => $itemZone,
            'linkpage_f0' => $linkpage_f0,
            'linkpage_f2' => $linkpage_f2,
            'linkpage_f3' => $linkpage_f3,
            'zone' => $dataPost['zone'],
            'province' => $dataPost['province'],
//            'province_name' => $select_province['province'],
            'hospital' => $hospital,
            'hospital_name' => $select_hospital['hospital'],
            'text' => $text,
            'select_province' => $select_province,
            'zone_select' => $zone_select
        ]);
    }

//    public function actionDinamyLink(){
//        $linkpage1 = "http://61.19.254.16/ckdreport/f0/site/f0_10980.js"; 
//        return $this->renderAjax("_ckdreport_f0", ['link'=>$linkpage1]);
//    }

//    public function actionSearchReport() {
//        $dataPost = \Yii::$app->request->post();
////            \appxq\sdii\utils\VarDumper::dump($dataPost);
////            
////              $Js = "@web/js/angular.min.js";
//
//        if ($dataPost['lab'] == 1) {
//
//            if ($dataPost['zone'] == '') { // ALL
//                $sitecode = Yii::$app->user->identity->userProfile->sitecode;
//                //            $linkpage = "all/" . $nameLink . ".js"; //all zone
//                $linkpage = "http://61.19.254.16/ckdreport/f0/site/f0_" . $sitecode . ".js";
//            } else {
//
//                if ($dataPost['province'] == '') {
//                    $linkpage = "http://61.19.254.16/ckdreport/f0/zone/f0_" . $dataPost['zone'] . ".js";
//                    //                $linkpage = "zone/f0_" . $dataPost['zone'] . ".js";
//                } else {
//                    if ($dataPost['hospital'] == '') {
//                        $linkpage = "http://61.19.254.16/ckdreport/f0/province/f0_" . $dataPost['province'] . ".js";
//                        // $linkpage = "province/f0_" . $dataPost['province'] . ".js";
//                    } else { //all hospital
//                        $linkpage = "http://61.19.254.16/ckdreport/f0/site/f0_" . $dataPost['hospital'] . ".js";
//                        // $linkpage = "site/f0_" . $dataPost['hospital'] . ".js";
//                    }
//                }
//            }
//        }
//        return $this->renderAjax('_filereport_list', [
//            'linkpage' => $linkpage,
//            'Js' => $Js,
//            'year' => $dataPost['year'],
//            'lab' => $dataPost['lab'],
//            'zone' => $dataPost['zone'],
//            'province' => $dataPost['province'],
//            'hospital' => $dataPost['hospital'],
//        ]);
//    }

    public function actionGetProvince() {

        $out = [];
        if (Yii::$app->request->post('depdrop_parents')) {
            $post = end(Yii::$app->request->post('depdrop_parents'));
            if ($post != null) {
                $data = \Yii::$app->db->createCommand("SELECT province FROM db_config_province WHERE zonecode = :zonecode  ORDER BY province ASC")->bindValues([':zonecode' => $post])->queryAll();
                foreach ($data as $value) {
                    if ($p == '') {
                        $p = $value['province'];
                    } else {
                        $p .= "," . $value['province'];
                    }
                }
                if (isset($p)) {
                    $whereProvince = "WHERE PROVINCE_CODE IN ({$p})";
                }
                $province = \Yii::$app->db->createCommand("SELECT PROVINCE_CODE,PROVINCE_NAME FROM const_province " . $whereProvince)->queryAll();
                $selected = null;
                foreach ($province as $value) {
                    $out[] = ['id' => $value['PROVINCE_CODE'], 'name' => $value['PROVINCE_NAME']];
                }

                echo \yii\helpers\Json::encode(['output' => $out, 'selected' => $selected]);
                return;
            }
        }
        echo \yii\helpers\Json::encode(['output' => '', 'selected' => '']);
    }

    public function actionGetHospital() {
        $out = [];
        if (Yii::$app->request->post('depdrop_parents')) {
            $post = end(Yii::$app->request->post());

            if ($post['province'] != null) {
                $hospital = \Yii::$app->db->createCommand("SELECT hcode,name FROM all_hospital_thai WHERE provincecode = :provincecode ")->bindValues([':provincecode' => $post['province']])->queryAll();
              //  \appxq\sdii\utils\VarDumper::dump($hospital);
                $selected = null;
                if (!empty($hospital)) {
                    foreach ($hospital as $value) {
                        $out[] = ['id' => $value['hcode'], 'name' => $value['hcode'] . " - " . $value['name']];
                    }
                    echo \yii\helpers\Json::encode(['output' => $out, 'selected' => $selected]);
                    return;
                }
            }
        }
        echo \yii\helpers\Json::encode(['output' => '', 'selected' => '']);
    }

    
        
//    public function actionTemplate() {
//        return $this->renderAjax('template');
//    }
//    public function actionRenderReport() {
//        $Post = \Yii::$app->request->post();
//        \appxq\sdii\utils\VarDumper::dump($Post);
//        //เช็คเงื่อนไข
//        //======== Report No.1 ==========
//        if($Post['idpage'] == 0 ){
//            if ($dataPost['lab'] == 1) {
//
//                if ($dataPost['zone'] == '') { // ALL
//                    $sitecode = Yii::$app->user->identity->userProfile->sitecode;
//                    //            $linkpage = "all/" . $nameLink . ".js"; //all zone
//                    $linkpage = "http://61.19.254.16/ckdreport/f0/site/f0_" . $sitecode . ".js";
//                } else {
//
//                    if ($dataPost['province'] == '') {
//                        $linkpage = "http://61.19.254.16/ckdreport/f0/zone/f0_" . $dataPost['zone'] . ".js";
//                        //                $linkpage = "zone/f0_" . $dataPost['zone'] . ".js";
//                    } else {
//                        if ($dataPost['hospital'] == '') {
//                            $linkpage = "http://61.19.254.16/ckdreport/f0/province/f0_" . $dataPost['province'] . ".js";
//                            // $linkpage = "province/f0_" . $dataPost['province'] . ".js";
//                        } else { //all hospital
//                            $linkpage = "http://61.19.254.16/ckdreport/f0/site/f0_" . $dataPost['hospital'] . ".js";
//                            // $linkpage = "site/f0_" . $dataPost['hospital'] . ".js";
//                        }
//                    }
//                }
//            }
//        }
//
//
//
//        $linkjs = $dataPost['linkpage'];
//        $page_render = $dataPost['urlpage'];
//
//        \appxq\sdii\utils\VarDumper::dump($linkjs);
//        return $this->renderPartial($page_render, ['linkjs' => $linkjs]);
//    }
    
    public function getNameProvince($province_code){
        
        $province = \Yii::$app->db->createCommand("SELECT PROVINCE_NAME as 'province' FROM const_province WHERE PROVINCE_CODE = :province_code" )->bindValues([':province_code' => $province_code])->queryOne();
    
        return $province;
    }
    
    public function getNameHospital($hcode){
        
        $hospital = \Yii::$app->db->createCommand("SELECT `name` as 'hospital' FROM all_hospital_thai WHERE hcode = :hcode ")->bindValues([':hcode' => $hcode])->queryOne();
    
        return $hospital;
    }
    
    
}
