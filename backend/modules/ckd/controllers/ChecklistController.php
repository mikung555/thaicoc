<?php

namespace backend\modules\ckd\controllers;

use yii\web\Controller;
use backend\modules\ckdnet\classes\CkdnetFunc;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use Yii;
use backend\modules\ckd\models\LabMap;
use yii\base\Model;
use common\models\User;
use common\models\UserProfile;

class ChecklistController extends Controller {

    public $activetab = "Checklist";

    public function actionIndex() {
        $pid = isset($_GET['pid']) ? $_GET['pid'] : '';
        $ptlink = isset($_GET['ptlink']) ? $_GET['ptlink'] : '';

        return $this->render("index", ['data' => $data, 'pid' => $pid, 'ptlink' => $ptlink]);
    }

    public function actionRedirect($tab) {
        return $this->redirect('/ckd/' . $tab, 302);
    }

    public function actionShowlist() {
        $sitecode = Yii::$app->user->identity->userProfile->sitecode;
        $pid = isset($_GET['pid']) ? $_GET['pid'] : '';
        $ptlink = isset($_GET['ptlink']) ? $_GET['ptlink'] : '';
        $checkVal = isset($_GET['checkVal']) ? $_GET['checkVal'] : '';
        $cid = isset($_GET['cid']) ? $_GET['cid'] : '';
        $ptWhere = '';
        if ($ptlink != '') {
            $ptWhere = $ptlink;
        } else if ($cid != '') {
            $ptWhere = md5($cid);
        }

        $data = CkdnetFunc::queryOne("SELECT val FROM log_notify WHERE (sitecode = :sitecode AND ptlink = :ptlink)", [
                    ':sitecode' => $sitecode,
                    ':ptlink' => $ptWhere
        ]);

//        \appxq\sdii\utils\VarDumper::dump(md5($cid) ,0);
        if ($data == false) {
            CkdnetFunc::execute("REPLACE INTO log_notify (sitecode,ptlink,val) VALUES (:sitecode,:ptlink,:val)", [
                ':sitecode' => $sitecode,
                ':ptlink' => $ptWhere,
                ':val' => $checkVal
            ]);
        } else {
//            if ($checkVal == '' && $data['val'] == '') {
//                $checkVal = $data['val'];
//            } else 
            if ($checkVal == '') {
                $checkVal = $data['val'];
            }
//            else {
            CkdnetFunc::execute("REPLACE INTO log_notify (sitecode,ptlink,val) VALUES (:sitecode,:ptlink,:val)", [
                ':sitecode' => $sitecode,
                ':ptlink' => $ptWhere,
                ':val' => $checkVal
            ]);
//            }
        }
//            
//        if (empty($ptlink)){
//        $data = \backend\modules\ckd\classes\CkdCheck::getAllCheck($sitecode, $pid, $cid);
//        }
//        elseif (empty($cid)){
        $data = \backend\modules\ckd\classes\CkdCheck::getAllCheck($sitecode, $pid, $ptlink, $cid);
//        }


        $item = \backend\modules\ckd\classes\CkdCheck::getShowCheck();
//        \appxq\sdii\utils\VarDumper::dump($item) ;

        $isCheck = "fa fa-check";
        $nonCheck = "fa fa-close";
        $checkArray = array();
        $result = [];
        $arr1 = [];
        $arr0 = [];
        $arr9 = [];
//        \appxq\sdii\utils\VarDumper::dump($item);
        foreach ($item as $item) {
            $res = [];
            $txt = "";
            $fileds = explode(',', $item['result']);
            $txts = explode(',', $item['txt_result']);
            $edit = explode(',', $item['edit_code']);

            foreach ($fileds as $key => $val) {
                if (preg_match('/^[0-9]+(\.[0-9]+)?$/', $data[0][$val])) {
                    $data[0][$val] = number_format($data[0][$val], 2);
                } else {
                    $data[0][$val] = $data[0][$val];
                }
                if ($data[0][$val] == "" or $data[0][$val] == null) {
                    $txts[$key] = "";
                } else {
                    $txts[$key] = $txts[$key];
                }

                $res[] = $txts[$key] . " " . $data[0][$val] . " ";
                $res_edit[] = $edit[$val];
            }

            switch ($item['check']) {
                case "" :
                    echo eval($item['edit_code']);
                    if ($check == 1) {
                        array_push($arr1, ['items' => $item['item'], 'check' => $check, 'text' => JOIN('  ', $res)]);
                    }
                    if ($check == 0) {
                        array_push($arr0, ['items' => $item['item'], 'check' => $check, 'text' => JOIN('  ', $res)]);
                    }
                    if ($check == 9) {
                        array_push($arr9, ['items' => $item['item'], 'check' => $check, 'text' => JOIN('  ', $res)]);
                    }
                    array_push($result, ['items' => $item['item'], 'check' => $check, 'text' => JOIN('  ', $res)]);
//             appxq\sdii\utils\VarDumper::dump(JOIN('  ',$res));   
                    break;
                default :
                    if ($check == 1) {
                        array_push($arr1, ['items' => $item['item'], 'check' => $check, 'text' => JOIN('  ', $res)]);
                    }
                    if ($check == 0) {
                        array_push($arr0, ['items' => $item['item'], 'check' => $check, 'text' => JOIN('  ', $res)]);
                    }
                    if ($check == 9) {
                        array_push($arr9, ['items' => $item['item'], 'check' => $check, 'text' => JOIN('  ', $res)]);
                    }
                    array_push($result, ['items' => $item['item'], 'check' => $check, 'text' => JOIN('  ', $res)]);
                    break;
            }
//             \appxq\sdii\utils\VarDumper::dump($data[0]['sbp']) 
        }

//        uksort($result, function($a, $b) {
//            if ($a['check'] != 9 || $b['check'] != 9)
//                $retval = $a['check'] <=> $b['check'];
//            return $retval;
//        });
        if ($checkVal == '0') {
            $result = array_merge($arr0, $arr1, $arr9);
        }

//        $result = $this->order_array_num($result, 'check');
        return $this->renderAjax("showlist", ['result' => $result, 'checkVal' => $checkVal]);
    }

    public function order_array_num($array, $key, $order = "DESC") {
        $tmp = array();
        foreach ($array as $akey => $array2) {
            $tmp[$akey] = $array2[$key];
        }

        if ($order == "DESC") {
            arsort($tmp, SORT_NUMERIC);
        } else {
            asort($tmp, SORT_NUMERIC);
        }

        $tmp2 = array();
        foreach ($tmp as $key => $value) {
            $tmp2[$key] = $array[$key];
        }

        return $tmp2;
    }

    public function actionShowlistdev() {
        $sitecode = Yii::$app->user->identity->userProfile->sitecode;
        $pid = isset($_GET['pid']) ? $_GET['pid'] : '';
        $ptlink = isset($_GET['ptlink']) ? $_GET['ptlink'] : '';
        $data = \backend\modules\ckd\classes\CkdCheck::getAllCheck($sitecode, $pid, $ptlink);

        $isCheck = "fa fa-check";
        $nonCheck = "fa fa-close";
        $checkArray = array();


        return $this->renderAjax("showlistdev", ['data' => $data]
        );
    }

    public function actionSortjs() {
        return $this->renderPartial("sortjs");
    }

    public function actionCheck() {
        $isCheck = "fa fa-check";
        $nonCheck = "fa fa-close";
        return $nonCheck;
    }

}
