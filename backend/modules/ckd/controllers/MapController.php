<?php

namespace backend\modules\ckd\controllers;

use backend\modules\ckd\models\HelperGis;
use backend\modules\ckd\models\HospitalGis;
use backend\modules\ckd\models\HelperGisSearch;
use backend\modules\ckd\models\HelperGisSearch2;
use Yii;
class MapController extends \yii\web\Controller
{
    public $activetab="map";
    
    public function actionIndex()
    {
        $searchModel = new HelperGisSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $searchModel2 = new HelperGisSearch2();
        $dataProvider2 = $searchModel2->search(Yii::$app->request->queryParams);
        
        $patient = HelperGis::find()->all();
        
        $volunteer = HelperGis::find()->where(['<>','volunteer',''])->all();
        $hospital = HospitalGis::find()->all();
        
        $dataProvider->pagination->pageSize=10;
        $dataProvider2->pagination->pageSize=10;

        return $this->render('index',[
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,    
                    'searchModel2' => $searchModel2,
                    'dataProvider2' => $dataProvider2,                
                    'patient'=>$patient,
                    'volunteer'=>$volunteer,
                    'hospital'=>$hospital,
                ]);
    }

    public function actionView($id)
    {
	if (Yii::$app->getRequest()->isAjax) {
	    return $this->renderAjax('view', [
		'model' => $this->findModel($id),
	    ]);
	} else {
	    return $this->render('view', [
		'model' => $this->findModel($id),
	    ]);
	}
    }


    protected function findModel($id)
    {
        if (($model = HelperGis::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }    
}
