<?php

namespace backend\modules\ckd\controllers;

use yii;
use backend\modules\ckdnet\classes\CkdnetFunc;

class DoctorConfirmController extends \yii\web\Controller {

    public function actionIndex() {
        $model = new \backend\modules\ckd\models\FPerson();

        //print_r($_GET);exit(); 
        $sql = "SELECT * FROM patient_profile_hospital WHERE hospcode='" . $_GET['hpcode'] . "' AND pid='" . $_GET['pid'] . "'";
        $out = CkdnetFunc::queryOne($sql);
        $cf_doctor = $out["cf_doctor"];
        $cf_doctor_code = $out["cf_doctor_code"];

        if (empty($cf_doctor) && empty($cf_doctor_code)) {
            $outputs = "<b>ยังไม่ได้ยืนยัน</b> ";
        } else {
            if ($cf_doctor == null && $cf_doctor_code == '0') {//ยังไม่ได้ confirm
                $outputs = "<b>ยังไม่ได้ยืนยัน</b> ";
            } else if ($cf_doctor == 1 && $cf_doctor_code == null) {//NOT CKD
                $outputs = "<b>Not CKD</b> ";
            } else if ($cf_doctor == 1 && ($cf_doctor_code != null || $cf_doctor_code != '0')) {
                $label = substr($cf_doctor_code, 3);
                $outputs = "<b>N18" . $label . " : CKD Stage " . $label . "</b> ";
            }
        }

        return $this->renderAjax('index', [
              'model' => $model,
              'out' => $out,
              'outputs' => $outputs,
              'pid' => $_GET['pid'],
              'hpcode' => $_GET['hpcode']
        ]);
    }

//Index

    public function actionSave() {

        $pid = $_POST['pid'];
        $hpcode = $_POST['hpcode'];
        $item = $_POST['item'];
        $doctor_id = Yii::$app->user->identity->id;

        $cf_doctor_date = Date('Y-m-d H:i:s');
        $sets = "";

        if ($item == '0') {//Not CKD
            $sets = "ckd_final=1,ckd_final_code=0,ckd_final_date='" . $cf_doctor_date . "', cf_icd=1,cf_icd_code=null, cf_doctor=1, cf_doctor_code=null,
           cf_doctor_date='" . $cf_doctor_date . "' ";
        } else if ($item == 'null') {//ยังไม่ยืนยัน
            $sets = "ckd_final=null,ckd_final_code=null,ckd_final_date='" . $cf_doctor_date . "', cf_icd=null,cf_icd_code=null,  cf_doctor=null, cf_doctor_code=0,
           cf_doctor_date='" . $cf_doctor_date . "' ";
        } else {

            $items = 'N18' . $item;
            $sets = "ckd_final=1,ckd_final_code='" . $items . "', ckd_final_date='" . $cf_doctor_date . "', cf_icd=1,cf_icd_code='" . $items . "', cf_doctor=1, cf_doctor_code= '" . $items . "',
           cf_doctor_date='" . $cf_doctor_date . "' ";
        }

        $sql = "UPDATE patient_profile_hospital SET " . $sets . " WHERE  hospcode='" . $hpcode . "' AND pid = '" . $pid . "' ";
        $model = CkdnetFunc::getDb()->createCommand($sql);
        if ($model->execute()) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

            $result = [
              'status' => 'success',
              'action' => 'create',
              'message' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> Success!</strong> ' . Yii::t('app', ''),
            ];
            return $result;
        }
    }

//save

    public function actionShowckd() {

        $sql = "SELECT * FROM patient_profile_hospital WHERE hospcode='" . $_GET['hpcode'] . "' AND pid='" . $_GET['pid'] . "'";
        $out = CkdnetFunc::queryOne($sql);
        $c = $out['cf_doctor_code'];

        $cf_doctor = $out["cf_doctor"];
        $cf_doctor_code = $out["cf_doctor_code"];
        if (empty($cf_doctor) && empty($cf_doctor_code)) {
            $outputs = "<span>ยังไม่ได้ยืนยัน</span>";
        } else {
            if ($cf_doctor == null && $cf_doctor_code == '0') {//ยังไม่ได้ confirm
                $outputs = "<span>ยังไม่ได้ยืนยัน</span> ";
            } else if ($cf_doctor == 1 && $cf_doctor_code == null) {//NOT CKD
                $outputs = "<span>Not CKD</span> ";
            } else if ($cf_doctor == 1 && ($cf_doctor_code != null || $cf_doctor_code != '0')) {
                $label = substr($cf_doctor_code, 3);
                $show = "N18" . $label . " : CKD Stage " . $label;
                $outputs = "<span>" . $show . "</span> ";
            }
        }


        echo $outputs;
    }

//Showckd

    public function actionShowckd2() {

        $sql = "SELECT * FROM patient_profile_hospital WHERE hospcode='" . $_GET['hpcode'] . "' AND pid='" . $_GET['pid'] . "'";
        $out = CkdnetFunc::queryOne($sql);
        $c = $out['cf_doctor_code'];



        $cf_doctor = $out["cf_doctor"];
        $cf_doctor_code = $out["cf_doctor_code"];

        if (empty($cf_doctor) && empty($cf_doctor_code)) {
            $outputs = "<span >ยังไม่ได้ยืนยัน</span> ";
        } else {
            if ($cf_doctor == null && $cf_doctor_code == '0') {//ยังไม่ได้ confirm
                $outputs = "<span>ยังไม่ได้ยืนยัน</span> ";
            } else if ($cf_doctor == 1 && $cf_doctor_code == null) {//NOT CKD
                $outputs = "<span >Not CKD</span> ";
            } else if ($cf_doctor == 1 && ($cf_doctor_code != null || $cf_doctor_code != '0')) {
                $label = substr($cf_doctor_code, 3);
                $show = "N18" . $label . " : CKD Stage " . $label;
                $outputs = "<span>" . $show . "</span> ";
            }
        }




        echo $outputs;
    }

//Showckd

    public function actionShowckdcheck() {

        $sql = "SELECT * FROM patient_profile_hospital WHERE hospcode='" . $_GET['hpcode'] . "' AND pid='" . $_GET['pid'] . "'";
        $out = CkdnetFunc::queryOne($sql);
        $output = "";

        $cf_doctor = $out["cf_doctor"];
        $cf_doctor_code = $out["cf_doctor_code"];
        if (empty($cf_doctor) && empty($cf_doctor_code)) {
            $output = 'null';
        } else {
            if ($cf_doctor == null && $cf_doctor_code == '0') {//ยังไม่ได้ confirm
                $output = 'null';
            } else if ($cf_doctor == 1 && $cf_doctor_code == null) {//NOT CKD
                $output = '0';
            } else if ($cf_doctor == 1 && ($cf_doctor_code != null || $cf_doctor_code != '0')) {
                $output = substr($cf_doctor_code, 3);
            }
        }

        echo $output;
    }

//Showckd

    public $enableCsrfValidation = false;

    public function getCheckConfirm($check) {
        $btnShow = "";

        if (!empty($check)) {
            if ($check['fckd'] == 1) {
                if ($check['ckdtype'] == 1) {
                    $btnShow .= "<code>Not CKD</code> ";
                } else {
                    $btnShow .= "<code> CKD stage " . ($check['ckdtype'] - 1) . "</code> ";
                }
            } else {
                $btnShow .= "<code style='background:#00953a;color: white;'>ยังไม่ได้ยืนยัน CKD</code> ";
            } //ckd
            if ($check['dm'] == 1) {
                if ($check['dmtype'] == 1) {
                    $btnShow .= "<code>Not DM</code> ";
                } else if ($check['dmtype'] == 2) {
                    $btnShow .= "<code>GDM</code> ";
                } else if ($check['dmtype'] == 3) {
                    $btnShow .= "<code>Diabetes mellitus type 1</code> ";
                } else if ($check['dmtype'] == 4) {
                    $btnShow .= "<code>Diabetes mellitus type 2</code> ";
                }
            } else {
                $btnShow .= "<code style='background: #9E9E9E;color: white;'>ยังไม่ได้ยืนยัน DM</code> ";
            } //dm
            if ($check['ht'] == 1) {
                if ($check['dmtype'] == 1) {
                    $btnShow .= "<code>Not hypertension</code> ";
                }
                if ($check['dmtype'] == 2) {
                    $btnShow .= "<code>Hypertension</code> ";
                }
            } else {
                $btnShow .= "<code style='background: #d1d1f6;color: #333;'>ยังไม่ได้ยืนยัน HT</code> ";
            } //ht
        } else {
            $btnShow .= "<code style='background:#00953a;color: #fff;'>ยังไม่ได้ยืนยัน CKD</code> "
                . "<code style='background: #9E9E9E;color: #fff;'>ยังไม่ได้ยืนยัน DM</code> "
                . " <code style='background: #d1d1f6;color: #333;'>ยังไม่ได้ยืนยัน HT</code> ";
        }
        return $btnShow;
    }

    public function actionCheckConfirm() {

        $table = 'tbdata_1493269244023122200';
        $hospcode = \Yii::$app->request->post('hospcode');
        $pid = \Yii::$app->request->post('pid');
        $doctor_type3 = \Yii::$app->request->post('doctor_type2', "1");

        $check = \backend\modules\ckd\classes\ConfirmCheck::getCid($table, $hospcode, $pid)->queryOne();
        //return $check['id'];

        if (!empty($check)) {
            $dataset = [
              'doctor_type3' => $doctor_type3
            ];
            echo '' . \yii\helpers\Html::button('<i class="fa fa-user-md"></i> คลิกยืนยันสถานะ: ' . $this->getCheckConfirm($check), [
              'data-url' => \yii\helpers\Url::to(['/inv/inv-person/ezform-print',
                'ezf_id' => '1493269244023122200', //1493269244023122200
                'dataid' => $check['id'], //update id
                'comp_target' => 'skip',
                'dataset' => base64_encode(\yii\helpers\Json::encode($dataset)), //set value cid
              ]), //update
              'class' => 'btn btn-warning open-ezfrom', 'id' => 'btnConfirm']);
        } else {


            $dataset = [
              'cid' => '',
              'hospcode' => \Yii::$app->request->post('hospcode'),
              'pid' => \Yii::$app->session['emr_pid'],
              'doctor_type3' => $doctor_type3
            ];
            echo '' . \yii\helpers\Html::button('<i class="fa fa-user-md"></i> คลิกยืนยันสถานะ: ' . $this->getCheckConfirm($check), [
              'data-url' => \yii\helpers\Url::to(['/inv/inv-person/ezform-print',
                'ezf_id' => '1493269244023122200', //1493269244023122200
                //'dataid' => $check['id'], //update id
                'comp_target' => 'skip',
                'dataset' => base64_encode(\yii\helpers\Json::encode($dataset)), //set value cid
              ]), //update
              'class' => 'btn btn-warning open-ezfrom', 'id' => 'btnConfirm']);
        }
    }

//
//    public function getCheckConfirm2($check){
//        $btnShow="";
//        
//       
//        if(!empty($check)){
//            if($check['fckd'] == 1){
//                if($check['ckdtype'] == 1){
//                    $btnShow .= "<div>Not CKD</div> ";
//                }else{
//                    $btnShow .= "<div> CKD stage ".($check['ckdtype']-1)."</div> ";
//                    
//                }
//            }//ckd
//            if($check['dm'] == 1){
//                if($check['dmtype'] == 1){
//                    $btnShow .= "<div>Not DM</div> ";
//                }else if($check['dmtype'] == 2){
//                    $btnShow .= "<div>GDM</div> ";  
//                }else if($check['dmtype'] == 3){
//                    $btnShow .= "<div>Diabetes mellitus type 1</div> ";  
//                }else if($check['dmtype'] == 4){
//                    $btnShow .= "<div>Diabetes mellitus type 2</div> ";  
//                }
//            }//dm
//             if($check['ht'] == 1){
//                  if($check['dmtype'] == 1){
//                     $btnShow .= "<div>Not hypertension</div> "; 
//                  }
//                   if($check['dmtype'] == 2){
//                     $btnShow .= "<div>Hypertension</div> ";  
//                   }
//             }//ht
//        }else{
//            $btnShow .= "<div>ยังไม่ได้ยืนยัน</div> ";
//        } 
//        
//        
//        return $btnShow;
//    }
//    public function actionCheckConfirmAll(){
//        $table='tbdata_1493269244023122200_triger';
//        $hospcode = \Yii::$app->request->post('hospcode');
//        $pid = \Yii::$app->request->post('pid');
//        
//        $check = \backend\modules\ckd\classes\ConfirmCheck::getCid($table,$hospcode,$pid)->queryAll();
//        foreach($check as $value){
//            echo "<div>".$this->getCheckConfirm2($value)."</div>";
//        }
//    }//
    public function actionHistory() {
        return $this->renderAjax("_history", [
              "hospcode" => \Yii::$app->request->get('hospcode')
        ]);
    }

    public function actionCheckRole() {
        if (Yii::$app->user->can('doctorgen')) {
            return 1; // = doctorgen
        } else {
            return 2; //!= doctorgen
        }
    }

    public function actionSaveDoctorConfirm() {
        /*[fckd] => 1
          [ckdtype] => 3
          [dm] => 1
          [dmtype] => 3
          [ht] => 1
          [httype] => 2
          [hospcode] => 10980
          [pid] => 00000
          [doctor_type3] => 2
         */
        $post = \Yii::$app->request->post();
        $fckd = $post['data']["fckd"];
        $ckdtype = $post['data']["ckdtype"];
        $dm = $post['data']["dm"];
        $dmtype = $post['data']["dmtype"];
        $ht = $post['data']["ht"];
        $httype = $post['data']["httype"];
        $doctor_type3 = $post['data']["doctor_type3"];
        $ptlink = \Yii::$app->session['ptlinks'];
        $pid = $post["data"]["pid"];
        $hospcode = \Yii::$app->session['dynamic_connection']['sitecode'];
        
        if($ckdtype == '1'){ $ckdtype = '';
        }else if($ckdtype == '2'){ $ckdtype = 'N181';
        }else if($ckdtype == '3'){ $ckdtype = 'N182';
        }else if($ckdtype == '4'){ $ckdtype = 'N183';
        }else if($ckdtype == '5'){ $ckdtype = 'N184';
        }else if($ckdtype == '6'){ $ckdtype = 'N185';
        }
        
        $sql="UPDATE patient_profile_hospital SET cf_doctor=:cf_doctor,cf_doctor_code=:cf_doctor_code,cf_doctor_date=:cf_doctor_date
            WHERE hospcode = :hospcode AND pid = :pid AND ptlink = :ptlink
        ";
        
        $params=[
          ':hospcode'=>$hospcode,
          ':pid'=>$pid,
          ':ptlink'=>$ptlink,
          ':cf_doctor'=>$fckd,
          ':cf_doctor_code'=>$ckdtype,
          ':cf_doctor_date'=>Date('Y-m-d H:i:s')
        ];
        //2017-09-24 21:25:29
        if(CkdnetFunc::execute($sql,$params)){
            return yii\helpers\Json::encode([status=>"success", ptlink=>$ptlink]);
        }else{
            return yii\helpers\Json::encode([ptlink=>$ptlink]);
        }
        
    }

}
