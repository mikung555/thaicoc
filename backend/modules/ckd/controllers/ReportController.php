<?php

namespace backend\modules\ckd\controllers;

use backend\modules\ckdnet\classes\CkdnetFunc;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\Response;
use appxq\sdii\helpers\SDHtml;

class ReportController extends \yii\web\Controller {

    public $activetab = "report";

    public function beforeAction($action) {
        if (empty(Yii::$app->user->identity->id)) {
            return $this->layout = "@frontend/views/layouts/mainreport";
        }
        return parent::beforeAction($action);
    }

    public function actionIndex() {
        
        $sql = " SELECT SUM(kpi1A) as kpi1A, SUM(kpi1B) as kpi1B 
                    , SUM(kpi2A) as kpi2A , SUM(kpi2B) as kpi2B
                    , SUM(kpi3A) as kpi3A , SUM(kpi3B) as kpi3B
                    , SUM(kpi4A) as kpi4A , SUM(kpi4B) as kpi4B
                    , SUM(kpi5A) as kpi5A , SUM(kpi5B) as kpi5B
                    , SUM(kpi6A) as kpi6A , SUM(kpi6B) as kpi6B
                    , SUM(kpi7A) as kpi7A , SUM(kpi7B) as kpi7B
                    , SUM(kpi8A) as kpi8A , SUM(kpi8B) as kpi8B
                    , SUM(kpi9A) as kpi9A , SUM(kpi9B) as kpi9B
                    , SUM(kpi10A) as kpi10A , SUM(kpi10B) as kpi10B
                    , SUM(kpi11A) as kpi11A , SUM(kpi11B) as kpi11B
                    , SUM(kpi12A) as kpi12A , SUM(kpi12B) as kpi12B
                    , SUM(kpi13A) as kpi13A , SUM(kpi13B) as kpi13B
                    , SUM(kpi14A) as kpi14A , SUM(kpi14B) as kpi14B
                    , SUM(kpi15A) as kpi15A , SUM(kpi15B) as kpi15B 
                 FROM report_ckd_sum_all 
            ";
        
        $result = \Yii::$app->db->createCommand($sql)->queryOne();
        return $this->render('index',['result'=>$result]);
    }

    public function actionRedirect() {
        return $this->redirect('/ckd/report', 302);
    }

    public function actionReportState() {

        $y = (date('m')>9?date("Y")+1:date("Y"))+ 543;
        $year = range( $y, $y);
        //$year = range(date("Y") + 543, 2560);
        $itemYear = [];
        foreach ($year as $value) {
            array_push($itemYear, ['id' => $value - 543, 'value' => $value]);
        }
        $itemYear = ArrayHelper::map($itemYear, 'id', 'value');

        $itemZone = [
                ['id' => '01', 'value' => 'เขตสุขภาพที่ 1'],
                ['id' => '02', 'value' => 'เขตสุขภาพที่ 2'],
                ['id' => '03', 'value' => 'เขตสุขภาพที่ 3'],
                ['id' => '04', 'value' => 'เขตสุขภาพที่ 4'],
                ['id' => '05', 'value' => 'เขตสุขภาพที่ 5'],
                ['id' => '06', 'value' => 'เขตสุขภาพที่ 6'],
                ['id' => '07', 'value' => 'เขตสุขภาพที่ 7'],
                ['id' => '08', 'value' => 'เขตสุขภาพที่ 8'],
                ['id' => '09', 'value' => 'เขตสุขภาพที่ 9'],
                ['id' => '10', 'value' => 'เขตสุขภาพที่ 10'],
                ['id' => '11', 'value' => 'เขตสุขภาพที่ 11'],
                ['id' => '12', 'value' => 'เขตสุขภาพที่ 12'],
        ];
        $itemZone = ArrayHelper::map($itemZone, 'id', 'value');

        $sitecode = Yii::$app->user->identity->userProfile->sitecode;
        $sqlCodeAll = " SELECT hcode, amphurcode, provincecode, province, amphur, `name`, zone_code FROM all_hospital_thai WHERE hcode='$sitecode'";
        $resCodeAll = \Yii::$app->db->createCommand($sqlCodeAll)->queryOne();
        $amphur_select = $resCodeAll['amphurcode'];
        $province_select = $resCodeAll['provincecode'];
        $zone_select = $resCodeAll['zone_code'];


//        \appxq\sdii\utils\VarDumper::dump($zone_select);
        return $this->render('report-state', [
                    'itemYear' => $itemYear,
                    'itemZone' => $itemZone,
                    'amphur_select' => $amphur_select,
                    'province_select' => $province_select,
                    'zone_select' => $zone_select
        ]);

//        $hospital = \Yii::$app->db->createCommand("SELECT hcode,name FROM all_hospital_thai WHERE name LIKE 'โรงพยาบาล%'")->queryAll();
//        $hospital = ArrayHelper::map($hospital, 'hcode', 'name');
//        return $this->render('report-state', ['itemYear' => $itemYear, 'itemZone' => $itemZone, 'hospital' => $hospital]);
    }

    public function actionGetGraph() {

        $dataPost = Yii::$app->request->post();


        $dataGet = Yii::$app->request->get();

        if (!empty($dataPost)) {
            $dataPost['lavel'] = '4';
            $getData = $this->getData($dataPost);
        } else {
            if (!empty($dataGet)) {
                $getData = $this->getData($dataGet);
            }
        }

        if ($dataPost['page_number'] == '16' || $dataGet['page_number'] == '16') {
            $title = "ที่มารับบริการที่โรงพยาบาล";
        } else if ($dataPost['page_number'] == '17' || $dataGet['page_number'] == '17') {
            $title = "ในเขตรับผิดชอบ";
        }
        if (Yii::$app->request->isAjax) {

            foreach ($getData['categories'] as $key => $value) {
                if (stristr($value, "โรงพยาบาลส่งเสริมสุขภาพตำบล") == true) {
                    $getData['categories'][$key] = str_replace("โรงพยาบาลส่งเสริมสุขภาพตำบล", "รพ.สต.", $value);
                } else if (stristr($value, "โรงพยาบาล") == true) {
                    $getData['categories'][$key] = str_replace("โรงพยาบาล", "รพ.", $value);
                }
            }


//        $m = date_format(date_create($getData['dateCalculate']), "m");
//        $y = date_format(date_create($getData['dateCalculate']), "Y");
//        $d = date_format(date_create($getData['dateCalculate']), "d");
//        $dt = date_format(date_create($getData['dateCalculate']), "w");
//        $time = date_format(date_create($getData['dateCalculate']), "H:i:s");
//        $thaiDay = ["วันอาทิตย์", "วันจันทร์", "วันอังคาร", "วันพุธ", "วันพฤหัสบดี", "วันศุกร์", "วันเสาร์"];
//        $thaimonth = ["มกราคม", "กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม"];
//        $dateCalculate = "คำนวนล่าสุดเมื่อ " . $thaiDay[$dt] . " ที่ " . $d . " " . $thaimonth[$m - 1] . " " . ($y + 543) . " เวลา " . $time;
            $txtTitle = 'ของผู้ป่วยโรคไตเรื้อรังที่มา' . $title . ' จำแนกตาม Stage ' . $getData['title'];
            $graph_config = [
                'scripts' => [
                    'modules/exporting',
                    'highcharts-more',
                    'highcharts-3d',
                    'modules/drilldown',
                ],
                'options' => [
                    'chart' => ['renderTo' => 'showGraph', 'type' => 'column',],
                    'title' => ['text' => 'ร้อยละ' . $txtTitle],
                    'xAxis' => [
                        'categories' => $getData['categories'],
                        'crosshair' => true,
//                    'type' => 'category'
                    ],
                    'tooltip' => [
                        'headerFormat' => '<span style="font-size:10px">{point.key}</span><table>',
                        'pointFormat' => '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' .
                        '<td style="padding:0"><b>{point.y:.1f} %</b></td></tr>',
                        'footerFormat' => '</table>',
//                        'shared' => true,
//                        'useHTML' => true
                    ],
                    'plotOptions' => [
                        'series' => [
                            'pointPadding' => 0.2,
                            'borderWidth' => 0,
                            'dataLabels' => [
                                'enabled' => true,
                                'format' => '{point.y:.1f}'
                            ],
//                     
                        ]
                    ],
                    'yAxis' => [
                        'title' => ['text' => '']
                    ],
                    'series' => $getData['main'],
//                'drilldown' => [
//                    'series' => $getData['sub']
//                ],
                ],
            ];

            if ($getData['lavel'] != '6') {
                $dataProvider = new \yii\data\ArrayDataProvider([
                    'allModels' => $getData['arrayDataProvider'],
                    'sort' => [
                        'attributes' => ['zone'],
                    ],
                    'pagination' => [
                        'pageSize' => 50,
                    ],
                ]);
            }



            if ($dataGet['drilldown'] == '1') {

                if ($getData['lavel'] == '6') {

                    $dataStage = $getData['dataStage'];
                    foreach ($dataStage as $key => $value) {

                        $dataStage[$key] = new \yii\data\ArrayDataProvider([
                            'allModels' => $value,
                            'sort' => [
                                'attributes' => ['zone'],
                            ],
                            'pagination' => [
                                'pageSize' => 50,
                            ],
                        ]);
                    }
                    return $this->renderAjax('_drilldown-patient', [
                                'dataProvider' => $dataProvider,
                                'urlBackDrilldown' => $getData['urlBackDrilldown'],
                                'title' => $txtTitle,
                                'dataStage' => $dataStage,
                                'key_ckd_report' => $getData['key_ckd_report'],
                    ]);
                } else {

                    return $this->renderAjax('_drilldown', [
                                'dataProvider' => $dataProvider,
                                'graph_config' => $graph_config,
                                'agv' => $getData['agv'],
                                'titleColumn' => $getData['titleColumn'],
                                'urlBackDrilldown' => $getData['urlBackDrilldown'],
                                'title' => $txtTitle,
                    ]);
                }
            } else {
                return $this->renderAjax('_graph', [
                            'graph_config' => $graph_config,
                            'dataProvider' => $dataProvider,
                            'agv' => $getData['agv'],
//                    'dateCalculate' => $dateCalculate,
                            'titleColumn' => $getData['titleColumn'],
                            'title' => $txtTitle,
                ]);
            }
        } else {
            return $this->redirect(['report-state', 'page_number' => $dataPost['page_number'] ? $dataPost['page_number'] : $dataGet['page_number']]);
        }
    }

    public function getData($postData = null) {

        

        $title = '';

        $titleColumn = '';

        $page = isset($postData['page_number']) ? $postData['page_number'] : '';

        $calculate = isset($postData['calculate']) ? $postData['calculate'] : '';

        $year = isset($postData['year']) ? $postData['year'] : '';

        $zone = isset($postData['zone']) ? $postData['zone'] : '';

        $province = isset($postData['province']) ? $postData['province'] : '';

        $amphur = isset($postData['amphur']) ? $postData['amphur'] : '';

        $hospital = isset($postData['hospital']) ? $postData['hospital'] : '';

        $lavel = isset($postData['lavel']) ? $postData['lavel'] : '0';
//
//        $networkService = isset($postData['network-service']) ? $postData['network-service'] : '';
//
//        $service = isset($postData['service']) ? $postData['service'] : '';
//
//        $tumbon = isset($postData['tumbon']) ? $postData['tumbon'] : '';

        ini_set('memory_limit', '256M');
        ini_set('max_execution_time', 30000);


        if ($page == '16') {
            $sqlSelectSum = "SELECT hospcode,zonecode,provincecode,amphurcode,IFNULL(SUM(stage1),0) AS st1,"
                    . "IFNULL(SUM(stage2),0) AS st2,IFNULL(SUM(stage3),0) AS st3,IFNULL(SUM(stage4),0) AS st4,"
                    . "IFNULL(SUM(stage5),0) AS st5"
                    . " FROM "
                    . " report_ckd_sum_all WHERE 1";
        }

        if ($page == '17') {
            $sqlSelectSum = "SELECT hospcode,zonecode,provincecode,amphurcode,IFNULL(SUM(type_stage1),0) AS st1,"
                    . "IFNULL(SUM(type_stage2),0) AS st2,IFNULL(SUM(type_stage3),0) AS st3,IFNULL(SUM(type_stage4),0) AS st4,"
                    . "IFNULL(SUM(type_stage5),0) AS st5"
                    . " FROM "
                    . " report_ckd_sum_all WHERE 1";
        }

        if ($zone != '') {
            $title .= " เขตสุขภาพที่ " . intval($zone);
            $titleColumn = 'จังหวัด';
        }

        if ($province != '') {
            $p = \Yii::$app->db->createCommand("SELECT PROVINCE_ID,PROVINCE_CODE,PROVINCE_NAME FROM const_province WHERE PROVINCE_CODE = '" . $province . "'")->queryOne();

            $title .= " จังหวัด" . $p['PROVINCE_NAME'];
            $titleColumn = 'อำเภอ';
        }

        if ($amphur != '') {

            $a = \Yii::$app->db->createCommand("SELECT AMPHUR_ID,AMPHUR_CODE,AMPHUR_NAME FROM const_amphur WHERE AMPHUR_CODE = '" . $amphur . "'")->queryOne();
            $title .= " อำเภอ" . $a['AMPHUR_NAME'];
            $titleColumn = 'โรงพยาบาล';
        }


        if ($hospital != '') {
            $h = Yii::$app->db->createCommand("SELECT hcode,name FROM all_hospital_thai WHERE hcode = '" . $hospital . "'")->queryOne();
            $title .= " " . $h['name'];
            $titleColumn = 'โรงพยาบาล';
        }

        if ($year != '') {
            $year2 = $year - 1;
            $year += 543;
            $title .= ' ปีงบประมาณ ' . $year;
            $year -= 543;
        } else {
            $title .= ' ปีงบประมาณทั้งหมด ';
        }

        $dataReport = [];
        $subDataReport = [];
        $categories = [];
        $urlBackDrilldown = [];




        //เลือกทั้งหมด
        if ($zone == '' && $hospital == '') {

            $categories = ['เขตสุขภาพที่ 1', 'เขตสุขภาพที่ 2', 'เขตสุขภาพที่ 3', 'เขตสุขภาพที่ 4', 'เขตสุขภาพที่ 5', 'เขตสุขภาพที่ 6',
                'เขตสุขภาพที่ 7', 'เขตสุขภาพที่ 8', 'เขตสุขภาพที่ 9', 'เขตสุขภาพที่ 10', 'เขตสุขภาพที่ 11', 'เขตสุขภาพที่ 12'];

            $dataLoop = \Yii::$app->db->createCommand("SELECT zonecode FROM db_config_province WHERE zonecode != 13 GROUP BY zonecode ORDER BY zonecode ASC ")->queryAll();
            $c = 0;
            foreach ($dataLoop as $value) {

                $data = Yii::$app->db->createCommand($sqlSelectSum . " AND zonecode = :zonecode")->bindValues([':zonecode' => $value['zonecode']])->queryOne();

                $dataReport[$categories[$c]] = $this->pushDataReport(
                        $value['zonecode'], $province, $amphur, $hospital, $data['st1'], $data['st2'], $data['st3'], $data['st4'], $data['st5']
                );

                $c++;
            }

            $urlBackDrilldown = ['get-graph', 'drilldown' => '1', 'page_number' => $page];
            $lavel = '1';
            
        } else if ($zone != '') {
            //เลือกเขตพื้นที่
            if ($province == '') {

                $dataLoop = Yii::$app->db->createCommand("SELECT province FROM db_config_province WHERE zonecode = :zonecode")->bindValues([':zonecode' => $zone])->queryAll();

                foreach ($dataLoop as $value) {

                    $provinceData = Yii::$app->db->createCommand("SELECT PROVINCE_CODE,PROVINCE_NAME FROM const_province WHERE PROVINCE_CODE = :province_code")->bindValues([':province_code' => $value['province']])->queryOne();

                    $data = Yii::$app->db->createCommand($sqlSelectSum . " AND zonecode = :zone AND provincecode = :provincecode")
                                    ->bindValues([':zone' => $zone, ':provincecode' => $provinceData['PROVINCE_CODE']])->queryOne();

                    $dataReport[$provinceData['PROVINCE_NAME']] = $this->pushDataReport(
                            $zone, $value['province'], $amphur, $hospital, $data['st1'], $data['st2'], $data['st3'], $data['st4'], $data['st5']);


                    $categories[] = $provinceData['PROVINCE_NAME'];
                }
                $urlBackDrilldown = ['get-graph', 'drilldown' => '1', 'page_number' => $page];
                $lavel = '2';
            } else {
                //เลือกจังหวัด
                if ($amphur == '') {

                    $dataLoop = Yii::$app->db->createCommand("SELECT AMPHUR_CODE,AMPHUR_NAME FROM const_amphur WHERE PROVINCE_ID = :province_id")->bindValues([':province_id' => $p['PROVINCE_ID']])->queryAll();

                    foreach ($dataLoop as $value) {

                        $data = Yii::$app->db->createCommand($sqlSelectSum . " AND zonecode = :zone AND provincecode = :provincecode AND amphurcode = RIGHT(:amphurcode,2)")
                                        ->bindValues([':zone' => $zone, ':provincecode' => $province, ':amphurcode' => $value['AMPHUR_CODE']])->queryOne();



                        $dataReport[$value['AMPHUR_NAME']] = $this->pushDataReport(
                                $zone, $province, $value['AMPHUR_CODE'], $hospital, $data['st1'], $data['st2'], $data['st3'], $data['st4'], $data['st5']);


                        $categories[] = $value['AMPHUR_NAME'];

//                        $dataReport[$value['AMPHUR_NAME']]['drilldown'] = $data['amphurcode'];
                    }
                    $urlBackDrilldown = [
                        'get-graph',
                        'drilldown' => '1',
                        'page_number' => $page,
                        'zone' => $zone
                    ];
                    $lavel = '3';
                } else {
                    //เลือกอำเภอ
                    if ($hospital == '') {

                        $dataLoop = Yii::$app->db->createCommand("SELECT hcode,name FROM all_hospital_thai WHERE provincecode = :provincecode "
                                                . " AND amphurcode = RIGHT(:amphurcode,2) AND zone_code = :zone_code AND name LIKE :name")
                                        ->bindValues([':provincecode' => $province, ':amphurcode' => $amphur, ':zone_code' => $zone, ':name' => 'โรงพยาบาล%'])->queryAll();

                        foreach ($dataLoop as $value) {

                            $data = Yii::$app->db->createCommand($sqlSelectSum . " AND zonecode = :zone AND provincecode = :provincecode"
                                                    . " AND amphurcode = RIGHT(:amphurcode,2) AND hospcode = :hospcode")
                                            ->bindValues([':zone' => $zone, ':provincecode' => $province, ':amphurcode' => $amphur, ':hospcode' => $value['hcode']])->queryOne();



                            $dataReport[$value['name']] = $this->pushDataReport(
                                    $zone, $province, $amphur, $value['hcode'], $data['st1'], $data['st2'], $data['st3'], $data['st4'], $data['st5']);



                            $categories[] = $value['name'];

//                            $dataReport[$value['name']]['drilldown'] = $data['hospcode'];
                        }
                        $urlBackDrilldown = [
                            'get-graph',
                            'drilldown' => '1',
                            'page_number' => $page,
                            'zone' => $zone,
                            'province' => $province,
                        ];
                        $lavel = '4';
                    } else {
                        //เลือกโรงพยาบาล

                        if ($lavel == '4') {
                            $data = Yii::$app->db->createCommand($sqlSelectSum . " AND zonecode = :zone AND provincecode = :provincecode"
                                                    . " AND amphurcode = RIGHT(:amphurcode,2) AND hospcode = :hospcode")
                                            ->bindValues([':zone' => $zone, ':provincecode' => $province, ':amphurcode' => $amphur, ':hospcode' => $hospital])->queryOne();



                            $dataReport[$value['name']] = $this->pushDataReport(
                                    $zone, $province, $amphur, $hospital, $data['st1'], $data['st2'], $data['st3'], $data['st4'], $data['st5']);


                            $categories[] = $h['name'];

                            $urlBackDrilldown = [
                                'get-graph',
                                'drilldown' => '1',
                                'page_number' => $page,
                                'zone' => $zone,
                                'province' => $province,
                                'amphur' => $amphur,
                            ];
                            $lavel = '5';
                        } else {//drilldown hospital get patient
                            $session = Yii::$app->session;
                            if (Yii::$app->user->identity->userProfile->sitecode == $hospital) {

                                $key_ckd_report = $session['key_ckd_report'];
                                $convert = $session['convert_ckd_char'];
                                $save = $session['save_ckd_key'];

                                if ($save == '') {
                                    unset($session['key_ckd_report']);
                                    unset($session['save_ckd_key']);
                                }

                                if ($postData['stage'] == 'ALL') {
                                    $stage = " IN ('N181','N182','N183','N184','N185')";
                                } else {
                                    $stage = " = '" . $postData['stage'] . "'";
                                }

                                $selectSql = "SELECT p.PID AS pidd,p.CID AS cid,p.Name AS fname,p.Lname AS lname,r.stageing AS stage ";

                                $whereSql = "WHERE p.sitecode = :sitecode AND p.HOSPCODE = :hospcode AND r.stageing {$stage} "
                                        . " AND r.zonecode = :zone AND r.provincecode = :province AND r.amphurcode = RIGHT(:amphur,2) AND r.hospcode = :hospcode ORDER BY r.stageing ASC,CAST(r.PID AS int) ASC ";
                                if ($key_ckd_report != '') {

                                    $selectSql = "SELECT "
                                            . "p.PID AS pidd,"
                                            . "decode(unhex(p.CID),sha2('$key_ckd_report',256)) AS cid,"
                                            . "decode(unhex(p.Name),sha2('$key_ckd_report',256)) AS fname,"
                                            . "decode(unhex(p.Lname),sha2('$key_ckd_report',256)) AS lname,"
                                            . "r.stageing AS stage ";

//                                    $whereSql = "WHERE p.sitecode = :sitecode AND p.HOSPCODE = :hospcode AND r.stageing {$stage} "
//                                            . " AND r.zonecode = :zone AND r.provincecode = :province AND r.amphurcode = RIGHT(:amphur,2) AND r.hospcode = :hospcode ORDER BY r.stageing ASC,r.PID ASC ";
                                    if ($convert == 1) {
                                        $selectSql = "SELECT "
                                                . "p.PID AS pidd,"
                                                . "convert(decode(unhex(p.CID),sha2('$key_ckd_report',256)) using tis620) AS cid,"
                                                . "convert(decode(unhex(p.Name),sha2('$key_ckd_report',256)) using tis620) AS fname,"
                                                . "convert(decode(unhex(p.Lname),sha2('$key_ckd_report',256)) using tis620) AS lname,"
                                                . "r.stageing AS stage ";

//                                        $whereSql = "WHERE p.sitecode = :sitecode AND p.HOSPCODE = :hospcode AND r.stageing {$stage} "
//                                                . " AND r.zonecode = :zone AND r.provincecode = :province AND r.amphurcode = RIGHT(:amphur,2) AND r.hospcode = :hospcode ORDER BY r.stageing ASC,r.PID ASC ";
                                    } else {
                                        unset($session['convert_ckd_char']);
                                    }
                                }


                                $patientData = CkdnetFunc::queryAll($selectSql . " FROM f_person AS p JOIN report_ckd_idv AS r ON p.PID = r.pid "
                                                . $whereSql, [':sitecode' => $hospital, ':hospcode' => $hospital, ':zone' => $zone, ':province' => $province, ':amphur' => $amphur, ':hospcode' => $hospital]);

//                                \appxq\sdii\utils\VarDumper::dump($selectSql);


                                $dataStage = [
                                    'stage' => $postData['stage'],
                                    'N181' => [],
                                    'N182' => [],
                                    'N183' => [],
                                    'N184' => [],
                                    'N185' => [],
                                    'ALL' => []
                                ];
                                foreach ($patientData as $value) {

                                    if ($postData['stage'] == 'ALL') {
                                        array_push($dataStage['ALL'], [
                                            'pid' => $value['pidd'],
                                            'cid' => $value['cid'],
                                            'fname' => $value['fname'],
                                            'lname' => $value['lname'],
                                            'stage' => $value['stage'],
                                        ]);
                                    } else {
                                        array_push($dataStage[$value['stage']], [
                                            'pid' => $value['pidd'],
                                            'cid' => $value['cid'],
                                            'fname' => $value['fname'],
                                            'lname' => $value['lname'],
                                            'stage' => $value['stage'],
                                        ]);
                                    }
                                }
                            }

                            $urlBackDrilldown = [
                                'get-graph',
                                'drilldown' => '1',
                                'page_number' => $page,
                                'zone' => $zone,
                                'province' => $province,
                                'amphur' => $amphur,
                                'hospital' => $hospital,
                                'lavel' => '4',
                            ];
                            $lavel = '6';
//                            \appxq\sdii\utils\VarDumper::dump($dataStage);
                        }

//                        $dataReport[$value['name']]['drilldown'] = null;
                    }
                }
            }
        } else if ($zone == '' && $hospital != '') {
            $dataLoop = Yii::$app->db->createCommand("SELECT hcode,name FROM all_hospital_thai WHERE hcode = :hcode ")
                            ->bindValues([':hcode' => $hospital])->queryOne();


//            \appxq\sdii\utils\VarDumper::dump($dataLoop['name']);
            $data = Yii::$app->db->createCommand($sqlSelectSum . "  AND hospcode = :hospcode")
                            ->bindValues([':hospcode' => $hospital])->queryOne();



            $dataReport[$dataLoop['name']] = $this->pushDataReport(
                    $data['zonecode'], $data['provincecode'], $data['provincecode'] . $data['amphurcode'], $dataLoop['hcode'], $data['st1'], $data['st2'], $data['st3'], $data['st4'], $data['st5']);



            $categories[] = $dataLoop['name'];

//                            $dataReport[$value['name']]['drilldown'] = $data['hospcode'];

            $urlBackDrilldown = [
                'get-graph',
                'drilldown' => '1',
                'page_number' => $page,
                'zone' => $zone,
                'province' => $province,
            ];
            $lavel = '5';
        }



        $main = [
                ['name' => 'stage1', 'data' => []],
                ['name' => 'stage2', 'data' => []],
                ['name' => 'stage3', 'data' => []],
                ['name' => 'stage4', 'data' => []],
                ['name' => 'stage5', 'data' => []]
        ];

        $sub = [];



        $arrayDataProvider = [];
        $agv = [];
        $i = 0;
//        \appxq\sdii\utils\VarDumper::dump($dataReport);
        foreach ($dataReport as $zValue) {

            $p_stage1 = is_nan(($zValue['st1'] * 100) / $zValue['count']) ? 0.0 : round(($zValue['st1'] * 100) / $zValue['count'], 1);
            $p_stage2 = is_nan(($zValue['st2'] * 100) / $zValue['count']) ? 0.0 : round(($zValue['st2'] * 100) / $zValue['count'], 1);
            $p_stage3 = is_nan(($zValue['st3'] * 100) / $zValue['count']) ? 0.0 : round(($zValue['st3'] * 100) / $zValue['count'], 1);
            $p_stage4 = is_nan(($zValue['st4'] * 100) / $zValue['count']) ? 0.0 : round(($zValue['st4'] * 100) / $zValue['count'], 1);
            $p_stage5 = is_nan(($zValue['st5'] * 100) / $zValue['count']) ? 0.0 : round(($zValue['st5'] * 100) / $zValue['count'], 1);

            $agv['sumAllStage1'] += $zValue['st1'];
            $agv['sumAllStage2'] += $zValue['st2'];
            $agv['sumAllStage3'] += $zValue['st3'];
            $agv['sumAllStage4'] += $zValue['st4'];
            $agv['sumAllStage5'] += $zValue['st5'];
            $agv['sumAll'] += $zValue['count'];

//            array_push($main[0]['data'], ['name' => $categories[$i], 'y' => intval($p_stage1), 'drilldown' => $zValue['drilldown']]);
//            array_push($main[1]['data'], ['name' => $categories[$i], 'y' => intval($p_stage2), 'drilldown' => $zValue['drilldown']]);
//            array_push($main[2]['data'], ['name' => $categories[$i], 'y' => intval($p_stage3), 'drilldown' => $zValue['drilldown']]);
//            array_push($main[3]['data'], ['name' => $categories[$i], 'y' => intval($p_stage4), 'drilldown' => $zValue['drilldown']]);
//            array_push($main[4]['data'], ['name' => $categories[$i], 'y' => intval($p_stage5), 'drilldown' => $zValue['drilldown']]);

            array_push($main[0]['data'], $p_stage1);
            array_push($main[1]['data'], $p_stage2);
            array_push($main[2]['data'], $p_stage3);
            array_push($main[3]['data'], $p_stage4);
            array_push($main[4]['data'], $p_stage5);


            if (!$patientData) {
                array_push($arrayDataProvider, [
                    'zone' => $categories[$i],
                    'page_number' => $page,
                    'zonecode' => $zValue['zonecode'],
                    'provincecode' => $zValue['provincecode'],
                    'amphurcode' => $zValue['amphurcode'],
                    'hospcode' => $zValue['hospcode'],
                    'lavel' => $lavel,
                    'count' => $zValue['count'],
                    'stage1' => $zValue['st1'],
                    'p_stage1' => number_format($p_stage1, 1),
                    'stage2' => $zValue['st2'],
                    'p_stage2' => number_format($p_stage2, 1),
                    'stage3' => $zValue['st3'],
                    'p_stage3' => number_format($p_stage3, 1),
                    'stage4' => $zValue['st4'],
                    'p_stage4' => number_format($p_stage4, 1),
                    'stage5' => $zValue['st5'],
                    'p_stage5' => number_format($p_stage5, 1)
                ]);
            }



            $i++;
        }





        return [
            'arrayDataProvider' => $arrayDataProvider,
            'dataStage' => $dataStage,
            'main' => $main,
//            'sub' => $sub,
            'categories' => $categories,
            'agv' => $agv,
            'title' => $title,
            'dateCalculate' => $dateCalculate,
            'titleColumn' => $titleColumn,
            'urlBackDrilldown' => $urlBackDrilldown,
            'lavel' => $lavel,
            'conver_ckd_char' => $convert,
            'key_ckd_report' => $key_ckd_report,
        ];
    }

    public function pushDataReport($zonecode, $province, $amphur, $hospital, $st1, $st2, $st3, $st4, $st5) {
        $data_array = [];
        $data_array['zonecode'] = $zonecode;
        $data_array['provincecode'] = $province;
        $data_array['amphurcode'] = $amphur;
        $data_array['hospcode'] = $hospital;
        $data_array['st1'] = $st1;
        $data_array['st2'] = $st2;
        $data_array['st3'] = $st3;
        $data_array['st4'] = $st4;
        $data_array['st5'] = $st5;
        $data_array['count'] = $st1 + $st2 + $st3 + $st4 + $st5;
        return $data_array;
    }

    public function actionAddKey() {

        $post = Yii::$app->request->post();
        Yii::$app->response->format = Response::FORMAT_JSON;
        $session = Yii::$app->session;

        $session['key_ckd_report'] = $post['key_ckd'];
        $session['save_ckd_key'] = $post['save_key'];
        $session['convert_ckd_char'] = $post['convert'];

        return $result = [
            'status' => 'success',
            'message' => SDHtml::getMsgSuccess() . Yii::t('app', 'Data completed.'),
        ];

//        \appxq\sdii\utils\VarDumper::dump($session['key_ckd_report']);
    }

    public function actionGetHospital2($q = null, $zone = null, $province = null, $amphur = null) {
        $params = [];
        if ($zone != null) {
            $where = " AND zone_code=:zonecode ";
            $params = [':zonecode' => $zone];
            if ($province != null) {
                $where = " AND zone_code=:zonecode AND provincecode=:provincecode ";
                $params = [':zonecode' => $zone, ':provincecode' => $province];
                if ($amphur != null) {
                    $where = " AND zone_code=:zonecode AND provincecode=:provincecode AND amphurcode=:amphurcode ";
                    $params = [':zonecode' => $zone, ':provincecode' => $province, ':amphurcode' => $amphur];
                }
            }
        }
        $sqlSite = " SELECT `name`, hcode FROM all_hospital_thai WHERE CONCAT(`hcode`, ' ', `name`) LIKE '%" . $q . "%' AND hcode NOT RLIKE '^[a-zA-z]' $where LIMIT 0,100";
        $resSite = \Yii::$app->db->createCommand($sqlSite, $params)->queryAll();
        //\appxq\sdii\utils\VarDumper::dump($resSite);
        $out = [];
        $i = 0;
        foreach ($resSite as $value) {
            $out["results"][$i] = ['id' => $value['hcode'], 'text' => $value["hcode"] . " " . $value["name"]];
            $i++;
        }

        return json_encode($out);
    }

    public function actionGetProvince() {

        $out = [];
        if (Yii::$app->request->post('depdrop_parents')) {
            $post = end(Yii::$app->request->post('depdrop_parents'));
            if ($post != null) {
                $data = \Yii::$app->db->createCommand("SELECT province FROM db_config_province WHERE zonecode = :zonecode  ORDER BY province ASC")->bindValues([':zonecode' => $post])->queryAll();
                foreach ($data as $value) {
                    if ($p == '') {
                        $p = $value['province'];
                    } else {
                        $p .= "," . $value['province'];
                    }
                }
                if (isset($p)) {
                    $whereProvince = "WHERE PROVINCE_CODE IN ({$p})";
                }
                $province = \Yii::$app->db->createCommand("SELECT PROVINCE_CODE,PROVINCE_NAME FROM const_province " . $whereProvince)->queryAll();
                $selected = null;
                foreach ($province as $value) {
                    $out[] = ['id' => $value['PROVINCE_CODE'], 'name' => $value['PROVINCE_NAME']];
                }

                echo \yii\helpers\Json::encode(['output' => $out, 'selected' => $selected]);
                return;
            }
        }
        echo \yii\helpers\Json::encode(['output' => '', 'selected' => '']);
    }

    public function actionGetHospital() {
        $out = [];
        if (Yii::$app->request->post('depdrop_parents')) {
            $post = end(Yii::$app->request->post());
//
            if ($post['province'] != null && $post['amphur'] != null) {

//                $province = \Yii::$app->db->createCommand("SELECT PROVINCE_CODE FROM const_province WHERE PROVINCE_ID = '" . $post['province'] . "'")->queryOne();
//                $amphur = \Yii::$app->db->createCommand("SELECT AMPHUR_CODE FROM const_amphur WHERE PROVINCE_ID = '" . $post['province'] . "' AND AMPHUR_ID = '" . $post['amphur'] . "'")->queryOne();
                $hospital = \Yii::$app->db->createCommand("SELECT hcode,name FROM all_hospital_thai WHERE provincecode = :provincecode AND amphurcode = RIGHT(:amphurcode,2)")->bindValues([':provincecode' => $post['province'], ':amphurcode' => $post['amphur']])->queryAll();
//                \appxq\sdii\utils\VarDumper::dump($hospital);
                $selected = null;
                if (!empty($hospital)) {
                    foreach ($hospital as $value) {
                        $out[] = ['id' => $value['hcode'], 'name' => $value['hcode'] . " - " . $value['name']];
                    }
                    echo \yii\helpers\Json::encode(['output' => $out, 'selected' => $selected]);
                    return;
                }
            }
        }
        echo \yii\helpers\Json::encode(['output' => '', 'selected' => '']);
    }

    public function actionGetAmphur() {
        if (Yii::$app->request->post('depdrop_parents')) {
            $post = end(Yii::$app->request->post());

            $code = Yii::$app->request->get('code');
            //\appxq\sdii\utils\VarDumper::dump($code);
            $out = [];
            if ($post['province'] != null) {
                //\appxq\sdii\utils\VarDumper::dump($code);
                $amphur = \Yii::$app->db->createCommand("SELECT AMPHUR_CODE,AMPHUR_NAME FROM const_amphur WHERE LEFT(AMPHUR_CODE,2) = :provincecode")->bindValues([':provincecode' => $post['province']])->queryAll();
                $selected = $code;
                foreach ($amphur as $value) {
                    $out[] = ['id' => $value['AMPHUR_CODE'], 'name' => $value['AMPHUR_NAME']];
                }

                echo \yii\helpers\Json::encode(['output' => $out, 'selected' => $selected]);
                return;
            }
        }

        echo \yii\helpers\Json::encode(['output' => '', 'selected' => '']);
    }

//    public function actionGetTumbonService() {
//        if (isset($_POST['depdrop_parents'])) {
//            $post = end($_POST);
//
//            if ($post['tumbon-service'] == '0') {
//                if ($post['amphur'] != null) {
//
//                    $amphur = \Yii::$app->db->createCommand("SELECT AMPHUR_NAME FROM const_amphur WHERE AMPHUR_ID = '" . $post['amphur'] . "'")->queryOne();
////                                        \appxq\sdii\utils\VarDumper::dump($province);
//                    $hospital = \Yii::$app->db->createCommand("SELECT hcode,name FROM all_hospital_thai WHERE amphur = '" . $amphur['AMPHUR_NAME'] . "'")->queryAll();
//                    $selected = null;
//
//                    if (!empty($hospital)) {
//                        foreach ($hospital as $value) {
//                            $out[] = ['id' => $value['hcode'], 'name' => $value['hcode'] . " - " . $value['name']];
//                        }
//                        echo \yii\helpers\Json::encode(['output' => $out, 'selected' => $selected]);
//                        return;
//                    }
//                }
//            } else {
//                if (($post['amphur'] != null ) && ($post['province'] != null )) {
//
//                    $dataTumbon = \Yii::$app->db->createCommand("SELECT DISTRICT_ID,DISTRICT_NAME FROM const_district WHERE PROVINCE_ID = '" . $post['province'] . "' AND AMPHUR_ID = '" . $post['amphur'] . "'")->queryAll();
//                    $selected = null;
////                    \appxq\sdii\utils\VarDumper::dump($dataTumbon);
//                    if (!empty($dataTumbon)) {
//                        foreach ($dataTumbon as $value) {
//                            $out[] = ['id' => $value['DISTRICT_ID'], 'name' => $value['DISTRICT_NAME']];
//                        }
//                        echo \yii\helpers\Json::encode(['output' => $out, 'selected' => $selected]);
//                        return;
//                    }
//                }
//            }
//        }
//        echo \yii\helpers\Json::encode(['output' => '', 'selected' => '']);
//    }
//
//    public function actionGetNetwork() {
//        if (isset($_POST['depdrop_parents'])) {
//            $post = end($_POST);
//
//
//            if ($post['province'] != null && $post['province'] != 'Loading ...') {
//
//                $province = \Yii::$app->db->createCommand("SELECT PROVINCE_CODE FROM const_province WHERE PROVINCE_ID = {$post['province']}")->queryOne();
//                $network = \Yii::$app->db->createCommand("SELECT hcode,name FROM all_hospital_thai WHERE provincecode = '" . $province['PROVINCE_CODE'] . "' AND code11 = 'A' ")->queryAll();
//                $selected = null;
//
//                if (!empty($network)) {
//                    foreach ($network as $value) {
//                        $out[] = ['id' => $value['hcode'], 'name' => $value['hcode'] . " - " . $value['name']];
//                    }
//                    echo \yii\helpers\Json::encode(['output' => $out, 'selected' => $selected]);
//                    return;
//                }
//            }
//        }
//        echo \yii\helpers\Json::encode(['output' => '', 'selected' => '']);
//    }


    public function actionReportKpi() {
        $y = (date('m')>9?date("Y")+1:date("Y"))+ 543;
        $year = range( $y, $y);
        $itemYear = [];
        foreach ($year as $value) {
            array_push($itemYear, ['id' => $value - 543, 'value' => $value]);
        }
        $itemYear = ArrayHelper::map($itemYear, 'id', 'value');

        $sitecode = Yii::$app->user->identity->userProfile->sitecode;
        $sqlCodeAll = " SELECT hcode, amphurcode, provincecode, province, amphur, `name`, zone_code FROM all_hospital_thai WHERE hcode='$sitecode'";
        $resCodeAll = \Yii::$app->db->createCommand($sqlCodeAll)->queryOne();
        $amphur_select = $resCodeAll['amphurcode'];
        $province_select = $resCodeAll['provincecode'];
        $zone_select = $resCodeAll['zone_code'];


//        \appxq\sdii\utils\VarDumper::dump($itemYear);
        return $this->render('report-kpi', [
                    'itemYear' => $itemYear,
                    'amphur_select' => $amphur_select,
                    'province_select' => $province_select,
                    'zone_select' => $zone_select
        ]);
    }

    public function actionGraph() {
        $report_num = Yii::$app->request->post('report_num'); //\yii\helpers\Html::encode($_POST['report_num']);
        $province_select = Yii::$app->request->post('province'); //\yii\helpers\Html::encode($_POST['province']);
        $year_select = Yii::$app->request->post('year'); //\yii\helpers\Html::encode($_POST['year']);
        $zone_select = Yii::$app->request->post('zone-choice'); //\yii\helpers\Html::encode($_POST['zone-choice']);
        $amphur_select = Yii::$app->request->post('amphur'); //\yii\helpers\Html::encode($_POST['amphur']);
        $hospital_select = Yii::$app->request->post('hospital'); //\yii\helpers\Html::encode($_POST['hospital']);
        $patient_select = Yii::$app->request->post('patientamt');
        $viewRender = Yii::$app->request->post('view_render');
        $action = Yii::$app->request->post('action');

        $session = Yii::$app->session;

        $key_ckd_report = $session['key_ckd_report'];

        if (Yii::$app->request->get('report_num'))
            $report_num = Yii::$app->request->get('report_num');
        if (Yii::$app->request->get('province_select'))
            $province_select = Yii::$app->request->get('province_select');
        if (Yii::$app->request->get('zone_select'))
            $zone_select = Yii::$app->request->get('zone_select');
        if (Yii::$app->request->get('amphur_select')) {
            $amphur_select = Yii::$app->request->get('amphur_select');
            $amphur_select = $province_select . $amphur_select;
        }
        if (Yii::$app->request->get('hospital_select'))
            $hospital_select = Yii::$app->request->get('hospital_select');
        if (Yii::$app->request->get('sitecode_select'))
            $sitecode_select = Yii::$app->request->get('sitecode_select');
        if (Yii::$app->request->get('view_render'))
            $viewRender = Yii::$app->request->get('view_render');
        if (Yii::$app->request->get('textAB'))
            $textAB = Yii::$app->request->get('textAB');
        if (Yii::$app->request->get('kpiB'))
            $kpiBFilture = Yii::$app->request->get('kpiB');

        //echo "<script> console.log($zone_select +'zone') </script>";
        //echo "<script> alert($province_select) </script>";
        //\appxq\sdii\utils\VarDumper::dump($amphur_select);

        if ($action == "refresh") {
            $sitecode = Yii::$app->user->identity->userProfile->sitecode;
            $sqlCodeAll = " SELECT hcode, amphurcode, provincecode, province, amphur, `name`, zone_code FROM all_hospital_thai WHERE hcode='$sitecode'";
            $resCodeAll = \Yii::$app->db->createCommand($sqlCodeAll)->queryOne();
            $amphur_select = $resCodeAll['amphurcode'];
            $province_select = $resCodeAll['provincecode'];
            $zone_select = $resCodeAll['zone_code'];
        }

        if ($hospital_select != null) {
            $sqlCodeAll = " SELECT hcode, amphurcode, provincecode, province, amphur, `name`, zone_code FROM all_hospital_thai WHERE hcode='$hospital_select'";
            $resCodeAll = \Yii::$app->db->createCommand($sqlCodeAll)->queryOne();
            $amphur_select = $resCodeAll['amphurcode'];
            $province_select = $resCodeAll['provincecode'];
            $zone_select = $resCodeAll['zone_code'];
        }

        if (strlen($amphur_select) > 2) {
            $amphur_select = substr($amphur_select, 2);
        }

        $totalHosp = [];
        $totalHospital = "";
        $total = 0;
        $dataNumber = [];
        $dataBackward = [];
        $selectCode = [];
        $serieData = [];
        $sqlGroup = "";
        $labelForGraph = [];
        $sqlHosp = "SELECT hcode, amphurcode, provincecode, province, amphur, `name`, zone_code
                   FROM all_hospital_thai dc WHERE zone_code IS NOT NULL AND code2 <> 'demo' AND `name` NOT LIKE '%สำนักงาน%'  ";

        $sqlHospital = "SELECT hcode, amphurcode, provincecode, province, amphur, `name`, zone_code
                   FROM all_hospital_thai dc WHERE zone_code IS NOT NULL AND code2 <> 'demo' AND `name` NOT LIKE '%สำนักงาน%' ";

        $sqlLoc = "SELECT dc.server, dc.port, dc.db, dc.user,dc.passwd,dc.zonecode, dc.province
                   FROM db_config_province dc WHERE 1 ";

        if ($patient_select != null) {
            
        } else if ($hospital_select != '') {

            $sqlHosp .= " AND zone_code='$zone_select' AND provincecode='$province_select' AND amphurcode=$amphur_select AND hcode='$hospital_select' ";
            $sqlHospital .= " AND hcode='$hospital_select' AND amphurcode=$amphur_select AND provincecode='$province_select' AND zone_code='$zone_select'  ";
            $sqlLoc .= " AND zonecode='$zone_select' AND province='$province_select' ";
            $sqlGroup = "  ";
        } else if ($amphur_select != '') {
            $sqlHosp .= " AND zone_code='$zone_select' AND provincecode='$province_select' AND amphurcode=$amphur_select ";
            $sqlHospital .= " AND zone_code='$zone_select' AND provincecode='$province_select' AND amphurcode=$amphur_select    ";
            $sqlGroup = " GROUP BY hcode ";
        } else if ($province_select != '') {
            $sqlHosp .= " AND zone_code='$zone_select' AND provincecode='$province_select'   ";
            $sqlHospital .= " AND zone_code='$zone_select' AND provincecode='$province_select'    ";
            $sqlGroup = " GROUP BY amphurcode ";
        } else if ($zone_select != '') {
            $sqlHosp .= " AND zone_code='$zone_select'   ";
            $sqlHospital .= " AND zone_code='$zone_select'    ";
            $sqlGroup = " GROUP BY provincecode ";
        } else {
            $sqlHosp .= "  GROUP BY zone_code ";
            $sqlHospital .= "  GROUP BY hcode";
        }

        $sqlHosp .= $sqlGroup;
        $totalHosp = \Yii::$app->db->createCommand($sqlHosp)->queryAll();
        $totalHospital = \Yii::$app->db->createCommand($sqlHosp)->queryAll();
        //\appxq\sdii\utils\VarDumper::dump($amphur_select);
        $c = 0;
        foreach ($totalHospital as $value) {
            if ($totalHospital == '') {
                $totalHospital = "'" . $value['hcode'] . "'";
            } else {
                $totalHospital .= ",'" . $value['hcode'] . "'";
            }
        }
        foreach ($totalHosp AS $value) {

            //if (stristr($value['name'], "สำนักงาน") == false ) {
            if ((int) $value['zone_code'] < 13) {
                $selectCode[$c]['provincecode'] = '';
                $selectCode[$c]['zonecode'] = '';
                $selectCode[$c]['amphurcode'] = '';
                $selectCode[$c]['hospcode'] = '';
                $dataBackward[0]['provincecode'] = '';
                $dataBackward[0]['zonecode'] = '';
                $dataBackward[0]['amphurcode'] = '';
                $dataBackward[0]['hospcode'] = '';
                $name = "";
                if (stristr($value['name'], "โรงพยาบาลส่งเสริมสุขภาพตำบล") == true) {
                    $name = str_replace("โรงพยาบาลส่งเสริมสุขภาพตำบล", "รพ.สต.", $value['name']);
                } else if (stristr($value['name'], "โรงพยาบาล") == true) {
                    $name = str_replace("โรงพยาบาล", "รพ.", $value['name']);
                } else {
                    $name = $value['name'];
                }


                if ($zone_select != '') {
                    $labelForGraph['gauge'] = "เขตสุขภาพที่ " . $value['zone_code'];
                    $labelForGraph['graph'] = "จังหวัด";
                    $dataNumber[$c] = "จังหวัด" . $value['province'];
                    $dataBackward[0]['text'] = "เขตสุขภาพที่ " . $value['zone_code'];
                    $selectCode[$c]['provincecode'] = $value['provincecode'];
                    $selectCode[$c]['zonecode'] = $value['zone_code'];
                    $serieData[$c] = 0;
                } else {
                    $labelForGraph['gauge'] = "ประเทศ ";
                    $labelForGraph['graph'] = "เขตสุขภาพ ";
                    $dataNumber[$c] = "เขตสุขภาพที่ " . $value['zone_code'];
                    $selectCode[$c]['zonecode'] = $value['zone_code'];
                    $serieData[$c] = 0;
                }
                if ($province_select != '') {
                    $labelForGraph['gauge'] = "จังหวัด " . $value['province'];
                    $labelForGraph['graph'] = "อำเภอ ";
                    $dataNumber[$c] = "อำเภอ" . $value['amphur'];
                    $dataBackward[0]['text'] = "จังหวัด " . $value['province'];
                    $dataBackward[0]['zonecode'] = $value['zone_code'];
                    $selectCode[$c]['amphurcode'] = $value['amphurcode'];
                    $selectCode[$c]['provincecode'] = $value['provincecode'];
                    $serieData[$c] = 0;
                }
                if ($amphur_select != '') {
                    $labelForGraph['gauge'] = "อำเภอ " . $value['amphur'];
                    $labelForGraph['graph'] = "หน่วยงาน ";
                    $dataNumber[$c] = $name;

                    $dataBackward[0]['text'] = "อำเภอ " . $value['amphur'];
                    $dataBackward[0]['provincecode'] = $value['provincecode'];
                    $selectCode[$c]['hospcode'] = $value['hcode'];
                    $selectCode[$c]['amphurcode'] = $value['amphurcode'];
                    $serieData[$c] = 0;
                }
                if ($hospital_select != '') {
                    $labelForGraph['gauge'] = $value['name'];
                    $labelForGraph['graph'] = $value['name'];
                    $dataNumber[$c] = "" . $name;

                    $dataBackward[0]['text'] = "" . $value['name'];
                    $dataBackward[0]['amphurcode'] = $value['amphurcode'];
                    $selectCode[$c]['hospcode'] = $value['hcode'];
                    $serieData[$c] = 0;
                }


                $c++;
            }
            //}
        }

        $sqlLoc .= " ORDER BY zonecode";
        $sqlTable .= "";
        $province = "";
        $zone = "";
        $amphur = "";
        $dataQuery = [];
        $dataPatient = [];
        $fromValStart = 0;
        $toValStart = 0;
        $fromVal2End = 0;
        $toVal2End = 0;
        $sqlA = "kpi1A";
        $sqlB = "kpi1B";

        $sqlTable = " report_ckd_sum_all ";

        if ($report_num == 1) {
            $sql .= "SELECT hospcode, zonecode,amphurcode,provincecode,SUM(kpi1A) as 'amtA',sum(kpi1B) as 'amtB',sum(kpi1Q1) AS 'quarter1',sum(kpi1Q2) AS 'quarter2',sum(kpi1Q3) AS 'quarter3',sum(kpi1Q4) AS 'quarter4' FROM $sqlTable WHERE 1 ";
            //$dataQuery = Yii::$app->db->createCommand($sql)->queryALL();
            if ($hospital_select != '') {
                $sql .= " AND hospcode='$hospital_select' ";
            } else if ($amphur_select != '') {
                $sql .= " AND amphurcode='$amphur_select'";
            } else if ($province_select != '') {
                $sql .= " AND provincecode='$province_select' ";
            } else if ($zone_select != '') {
                $sql .= " AND zonecode= '$zone_select' ";
            }
            $sqlA = "rv.kpi1A";
            $sqlB = "rv.kpi1B";

            $fromValStart = 0;
            $toValStart = 90;
            $fromVal2End = $toValStart;
            $toVal2End = 100;
        } else if ($report_num == 2) {

            $sql .= "SELECT zonecode,hospcode,amphurcode,provincecode,SUM(kpi2A) as 'amtA',sum(kpi2B) as 'amtB',sum(kpi2Q1) AS 'quarter1',sum(kpi2Q2) AS 'quarter2',sum(kpi2Q3) AS 'quarter3',sum(kpi2Q4) AS 'quarter4' FROM $sqlTable WHERE 1 ";
            if ($hospital_select != '') {
                $sql .= " AND zonecode= $zone_select AND provincecode=$province_select AND amphurcode='$amphur_select' AND hospcode=$hospital_select ";
            } else if ($amphur_select != '') {
                $sql .= " AND zonecode= $zone_select AND provincecode=$province_select AND amphurcode='$amphur_select'";
            } else if ($province_select != '') {
                $sql .= " AND zonecode= $zone_select AND provincecode=$province_select ";
            } else if ($zone_select != '') {
                $sql .= " AND zonecode= $zone_select ";
            }

            $sqlA = "rv.kpi2A";
            $sqlB = "rv.kpi2B";
            $fromValStart = 0;
            $toValStart = 0;
            $fromVal2End = $toValStart;
            $toVal2End = 100;
        } else if ($report_num == 3) {
            $sql .= "SELECT zonecode,hospcode,amphurcode,provincecode,SUM(kpi3A) as 'amtA',sum(kpi3B) as 'amtB',sum(kpi3Q1) AS 'quarter1',sum(kpi3Q2) AS 'quarter2',sum(kpi3Q3) AS 'quarter3',sum(kpi3Q4) AS 'quarter4' FROM $sqlTable WHERE 1 ";
            if ($hospital_select != '') {
                $sql .= " AND zonecode= $zone_select AND provincecode=$province_select AND amphurcode='$amphur_select' AND hospcode=$hospital_select ";
            } else if ($amphur_select != '') {
                $sql .= " AND zonecode= $zone_select AND provincecode=$province_select AND amphurcode='$amphur_select'";
            } else if ($province_select != '') {
                $sql .= " AND zonecode= $zone_select AND provincecode=$province_select ";
            } else if ($zone_select != '') {
                $sql .= " AND zonecode= $zone_select ";
            }

            $sqlA = "rv.kpi3A";
            $sqlB = "rv.kpi3B";
            $fromValStart = 0;
            $toValStart = 80;
            $fromVal2End = $toValStart;
            $toVal2End = 100;
        } else if ($report_num == 4) {
            $sql .= "SELECT zonecode,hospcode,amphurcode,provincecode,SUM(kpi4A) as 'amtA',sum(kpi4B) as 'amtB',sum(kpi4Q1) AS 'quarter1',sum(kpi4Q2) AS 'quarter2',sum(kpi4Q3) AS 'quarter3',sum(kpi4Q4) AS 'quarter4' FROM $sqlTable WHERE 1 ";
            if ($hospital_select != '') {
                $sql .= " AND zonecode= $zone_select AND provincecode=$province_select AND amphurcode='$amphur_select' AND hospcode=$hospital_select ";
            } else if ($amphur_select != '') {
                $sql .= " AND zonecode= $zone_select AND provincecode=$province_select AND amphurcode='$amphur_select'";
            } else if ($province_select != '') {
                $sql .= " AND zonecode= $zone_select AND provincecode=$province_select ";
            } else if ($zone_select != '') {
                $sql .= " AND zonecode= $zone_select ";
            }

            $sqlA = "rv.kpi4A";
            $sqlB = "rv.kpi4B";
            $fromValStart = 0;
            $toValStart = 60;
            $fromVal2End = $toValStart;
            $toVal2End = 100;
        } else if ($report_num == 5) {
            $sql .= "SELECT zonecode,hospcode,amphurcode,provincecode,SUM(kpi5A) as 'amtA',sum(kpi5B) as 'amtB',sum(kpi5Q1) AS 'quarter1',sum(kpi5Q2) AS 'quarter2',sum(kpi5Q3) AS 'quarter3',sum(kpi5Q4) AS 'quarter4' FROM $sqlTable WHERE 1 ";
            if ($hospital_select != '') {
                $sql .= " AND zonecode= $zone_select AND provincecode=$province_select AND amphurcode='$amphur_select' AND hospcode=$hospital_select ";
            } else if ($amphur_select != '') {
                $sql .= " AND zonecode= $zone_select AND provincecode=$province_select AND amphurcode='$amphur_select'";
            } else if ($province_select != '') {
                $sql .= " AND zonecode= $zone_select AND provincecode=$province_select ";
            } else if ($zone_select != '') {
                $sql .= " AND zonecode= $zone_select ";
            }

            $sqlA = "rv.kpi5A";
            $sqlB = "rv.kpi5B";
            $fromValStart = 0;
            $toValStart = 65;
            $fromVal2End = $toValStart;
            $toVal2End = 100;
        } else if ($report_num == 6) {
            $sql .= "SELECT zonecode,hospcode,amphurcode,provincecode,SUM(kpi6A) as 'amtA',sum(kpi6B) as 'amtB',sum(kpi6Q1) AS 'quarter1',sum(kpi6Q2) AS 'quarter2',sum(kpi6Q3) AS 'quarter3',sum(kpi6Q4) AS 'quarter4' FROM $sqlTable WHERE 1 ";
            if ($hospital_select != '') {
                $sql .= " AND zonecode= $zone_select AND provincecode=$province_select AND amphurcode='$amphur_select' AND hospcode=$hospital_select ";
            } else if ($amphur_select != '') {
                $sql .= " AND zonecode= $zone_select AND provincecode=$province_select AND amphurcode='$amphur_select'";
            } else if ($province_select != '') {
                $sql .= " AND zonecode= $zone_select AND provincecode=$province_select ";
            } else if ($zone_select != '') {
                $sql .= " AND zonecode= $zone_select ";
            }

            $sqlA = "rv.kpi6A";
            $sqlB = "rv.kpi6B";
            $fromValStart = 0;
            $toValStart = 0;
            $fromVal2End = $toValStart;
            $toVal2End = 100;
        } else if ($report_num == 7) {
            $sql .= "SELECT zonecode,hospcode,amphurcode,provincecode,SUM(kpi7A) as 'amtA',sum(kpi7B) as 'amtB',sum(kpi7Q1) AS 'quarter1',sum(kpi7Q2) AS 'quarter2',sum(kpi7Q3) AS 'quarter3',sum(kpi7Q4) AS 'quarter4' FROM $sqlTable WHERE 1 ";
            if ($hospital_select != '') {
                $sql .= " AND zonecode= $zone_select AND provincecode=$province_select AND amphurcode='$amphur_select' AND hospcode=$hospital_select ";
            } else if ($amphur_select != '') {
                $sql .= " AND zonecode= $zone_select AND provincecode=$province_select AND amphurcode='$amphur_select'";
            } else if ($province_select != '') {
                $sql .= " AND zonecode= $zone_select AND provincecode=$province_select ";
            } else if ($zone_select != '') {
                $sql .= " AND zonecode= $zone_select ";
            }

            $sqlA = "rv.kpi7A";
            $sqlB = "rv.kpi7B";
            $fromValStart = 0;
            $toValStart = 40;
            $fromVal2End = $toValStart;
            $toVal2End = 100;
        } else if ($report_num == 8) {
            $sql .= "SELECT zonecode,hospcode,amphurcode,provincecode,SUM(kpi8A) as 'amtA',sum(kpi8B) as 'amtB',sum(kpi8Q1) AS 'quarter1',sum(kpi8Q2) AS 'quarter2',sum(kpi8Q3) AS 'quarter3',sum(kpi8Q4) AS 'quarter4' FROM $sqlTable WHERE 1 ";
            if ($hospital_select != '') {
                $sql .= " AND zonecode= $zone_select AND provincecode=$province_select AND amphurcode='$amphur_select' AND hospcode=$hospital_select ";
            } else if ($amphur_select != '') {
                $sql .= " AND zonecode= $zone_select AND provincecode=$province_select AND amphurcode='$amphur_select'";
            } else if ($province_select != '') {
                $sql .= " AND zonecode= $zone_select AND provincecode=$province_select ";
            } else if ($zone_select != '') {
                $sql .= " AND zonecode= $zone_select ";
            }

            $sqlA = "rv.kpi8A";
            $sqlB = "rv.kpi8B";
            $fromValStart = 0;
            $toValStart = 60;
            $fromVal2End = $toValStart;
            $toVal2End = 100;
        } else if ($report_num == 9) {
            $sql .= "SELECT zonecode,hospcode,amphurcode,provincecode,SUM(kpi9A) as 'amtA',sum(kpi9B) as 'amtB',sum(kpi9Q1) AS 'quarter1',sum(kpi9Q2) AS 'quarter2',sum(kpi9Q3) AS 'quarter3',sum(kpi9Q4) AS 'quarter4' FROM $sqlTable WHERE 1 ";
            if ($hospital_select != '') {
                $sql .= " AND zonecode= $zone_select AND provincecode=$province_select AND amphurcode='$amphur_select' AND hospcode=$hospital_select ";
            } else if ($amphur_select != '') {
                $sql .= " AND zonecode= $zone_select AND provincecode=$province_select AND amphurcode='$amphur_select'";
            } else if ($province_select != '') {
                $sql .= " AND zonecode= $zone_select AND provincecode=$province_select ";
            } else if ($zone_select != '') {
                $sql .= " AND zonecode= $zone_select ";
            }

            $sqlA = "rv.kpi9A";
            $sqlB = "rv.kpi9B";
            $fromValStart = 0;
            $toValStart = 80;
            $fromVal2End = $toValStart;
            $toVal2End = 100;
        } else if ($report_num == 10) {
            $sql .= "SELECT zonecode,hospcode,amphurcode,provincecode,SUM(kpi10A) as 'amtA',sum(kpi10B) as 'amtB',sum(kpi10Q1) AS 'quarter1',sum(kpi10Q2) AS 'quarter2',sum(kpi10Q3) AS 'quarter3',sum(kpi10Q4) AS 'quarter4' FROM $sqlTable WHERE 1 ";
            if ($hospital_select != '') {
                $sql .= " AND zonecode= $zone_select AND provincecode=$province_select AND amphurcode='$amphur_select' AND hospcode=$hospital_select ";
            } else if ($amphur_select != '') {
                $sql .= " AND zonecode= $zone_select AND provincecode=$province_select AND amphurcode='$amphur_select'";
            } else if ($province_select != '') {
                $sql .= " AND zonecode= $zone_select AND provincecode=$province_select ";
            } else if ($zone_select != '') {
                $sql .= " AND zonecode= $zone_select ";
            }
            $sqlA = "rv.kpi10A";
            $sqlB = "rv.kpi10B";
            $fromValStart = 0;
            $toValStart = 80;
            $fromVal2End = $toValStart;
            $toVal2End = 100;
        } else if ($report_num == 11) {
            $sql .= "SELECT zonecode,hospcode,amphurcode,provincecode,SUM(kpi11A) as 'amtA',sum(kpi11B) as 'amtB',sum(kpi11Q1) AS 'quarter1',sum(kpi11Q2) AS 'quarter2',sum(kpi11Q3) AS 'quarter3',sum(kpi11Q4) AS 'quarter4' FROM $sqlTable WHERE 1 ";
            if ($hospital_select != '') {
                $sql .= " AND zonecode= $zone_select AND provincecode=$province_select AND amphurcode='$amphur_select' AND hospcode=$hospital_select ";
            } else if ($amphur_select != '') {
                $sql .= " AND zonecode= $zone_select AND provincecode=$province_select AND amphurcode='$amphur_select'";
            } else if ($province_select != '') {
                $sql .= " AND zonecode= $zone_select AND provincecode=$province_select ";
            } else if ($zone_select != '') {
                $sql .= " AND zonecode= $zone_select ";
            }
            $sqlA = "rv.kpi11A";
            $sqlB = "rv.kpi11B";
            $fromValStart = 0;
            $toValStart = 80;
            $fromVal2End = $toValStart;
            $toVal2End = 100;
        } else if ($report_num == 12) {
            $sql .= "SELECT zonecode,hospcode,amphurcode,provincecode,SUM(kpi12A) as 'amtA',sum(kpi12B) as 'amtB',sum(kpi12Q1) AS 'quarter1',sum(kpi12Q2) AS 'quarter2',sum(kpi12Q3) AS 'quarter3',sum(kpi12Q4) AS 'quarter4' FROM $sqlTable WHERE 1 ";
            if ($hospital_select != '') {
                $sql .= " AND zonecode= $zone_select AND provincecode=$province_select AND amphurcode='$amphur_select' AND hospcode=$hospital_select ";
            } else if ($amphur_select != '') {
                $sql .= " AND zonecode= $zone_select AND provincecode=$province_select AND amphurcode='$amphur_select'";
            } else if ($province_select != '') {
                $sql .= " AND zonecode= $zone_select AND provincecode=$province_select ";
            } else if ($zone_select != '') {
                $sql .= " AND zonecode= $zone_select ";
            }

            $sqlA = "rv.kpi12A";
            $sqlB = "rv.kpi12B";
            $fromValStart = 0;
            $toValStart = 40;
            $fromVal2End = $toValStart;
            $toVal2End = 100;
        } else if ($report_num == 13) {
            $sql .= "SELECT zonecode,hospcode,amphurcode,provincecode,SUM(kpi13A) as 'amtA',sum(kpi13B) as 'amtB',sum(kpi13Q1) AS 'quarter1',sum(kpi13Q2) AS 'quarter2',sum(kpi13Q3) AS 'quarter3',sum(kpi13Q4) AS 'quarter4' FROM $sqlTable WHERE 1 ";
            if ($hospital_select != '') {
                $sql .= " AND zonecode= $zone_select AND provincecode=$province_select AND amphurcode='$amphur_select' AND hospcode=$hospital_select ";
            } else if ($amphur_select != '') {
                $sql .= " AND zonecode= $zone_select AND provincecode=$province_select AND amphurcode='$amphur_select'";
            } else if ($province_select != '') {
                $sql .= " AND zonecode= $zone_select AND provincecode=$province_select ";
            } else if ($zone_select != '') {
                $sql .= " AND zonecode= $zone_select ";
            }

            $sqlA = "rv.kpi13A";
            $sqlB = "rv.kpi13B";
            $fromValStart = 0;
            $toValStart = 40;
            $fromVal2End = $toValStart;
            $toVal2End = 100;
        } else if ($report_num == 14) {
            $sql .= "SELECT zonecode,hospcode,amphurcode,provincecode,SUM(kpi14A) as 'amtA',sum(kpi14B) as 'amtB',sum(kpi14Q1) AS 'quarter1',sum(kpi14Q2) AS 'quarter2',sum(kpi14Q3) AS 'quarter3',sum(kpi14Q4) AS 'quarter4' FROM $sqlTable WHERE 1 ";
            if ($hospital_select != '') {
                $sql .= " AND zonecode= $zone_select AND provincecode=$province_select AND amphurcode='$amphur_select' AND hospcode=$hospital_select ";
            } else if ($amphur_select != '') {
                $sql .= " AND zonecode= $zone_select AND provincecode=$province_select AND amphurcode='$amphur_select'";
            } else if ($province_select != '') {
                $sql .= " AND zonecode= $zone_select AND provincecode=$province_select ";
            } else if ($zone_select != '') {
                $sql .= " AND zonecode= $zone_select ";
            }

            $sqlA = "rv.kpi14A";
            $sqlB = "rv.kpi14B";
            $fromValStart = 0;
            $toValStart = 50;
            $fromVal2End = $toValStart;
            $toVal2End = 100;
        } else if ($report_num == 15) {
            $sql .= "SELECT zonecode,hospcode,amphurcode,provincecode,SUM(kpi15A) as 'amtA',sum(kpi15B) as 'amtB',sum(kpi15Q1) AS 'quarter1',sum(kpi15Q2) AS 'quarter2',sum(kpi15Q3) AS 'quarter3',sum(kpi15Q4) AS 'quarter4' FROM $sqlTable WHERE 1 ";
            if ($hospital_select != '') {
                $sql .= " AND zonecode= $zone_select AND provincecode=$province_select AND amphurcode='$amphur_select' AND hospcode=$hospital_select ";
            } else if ($amphur_select != '') {
                $sql .= " AND zonecode= $zone_select AND provincecode=$province_select AND amphurcode='$amphur_select'";
            } else if ($province_select != '') {
                $sql .= " AND zonecode= $zone_select AND provincecode=$province_select ";
            } else if ($zone_select != '') {
                $sql .= " AND zonecode= $zone_select ";
            }

            $sqlA = "rv.kpi15A";
            $sqlB = "rv.kpi15B";
            $fromValStart = 0;
            $toValStart = 50;
            $fromVal2End = $toValStart;
            $toVal2End = 100;
        }

        if ($sitecode_select != '') {
            if ($sitecode_select == Yii::$app->user->identity->userProfile->sitecode) {
                $dataPatientQ = self::queryDataPatient($sqlA, $sqlB, $sitecode_select, $kpiBFilture);
            }
            $dataPatient = new \yii\data\ArrayDataProvider([
                'allModels' => $dataPatientQ,
                'pagination' => [
                    'pageSize' => 100,
                ],
                'sort' => [
                    'attributes' => [
                        'pid',
                        'Pname',
                        'Name',
                        'Lname',
                        'KPI_B'
                    ],
                ],
            ]);
            //\appxq\sdii\utils\VarDumper::dump($dataPatient);
        } else {
            $dataQuery = self::queryDataForGraph($sql, $province_select, $zone_select, $hospital_select, $amphur_select, $selectCode, $sqlLoc);
        }

        $totalSumA = 0;
        $totalSumB = 0;
        $totalSum = 0;
        $sum = 0;
        //$dataFinal = array();
        $quarter1 = array();
        $quarter2 = array();
        $quarter3 = array();
        $quarter4 = array();
        $x = 0;

        $arr = 0;
        foreach ($dataQuery as $val) {
            $quasum1 = 0;
            $quasum2 = 0;
            $quasum3 = 0;
            $quasum4 = 0;

            if ($zone_select == '')
                $arr = (int) $val['zonecode'] - 1;

            if ($arr >= 0) {

                $totalSumA += (float) $val['amtA'];
                $totalSumB += (float) $val['amtB'];
                if ((int) $dataQuery[$arr]['amtA'] > 0) {
                    $sum = ((float) $dataQuery[$arr]['amtB'] / (float) $dataQuery[$arr]['amtA']) * 100;
                } else {
                    $sum = 0;
                }
                //\appxq\sdii\utils\VarDumper::dump($quarter1[$arr]['amtq1']); exit();
                if ((int) $val['amtB'] > 0) {
                    $quasum1 = ((float) $dataQuery[$arr]['amtq1'] / (float) $val['amtA']) * 100;
                }
                if ((int) $val['amtB'] > 0) {
                    $quasum2 = ((float) $dataQuery[$arr]['amtq2'] / (float) $val['amtA']) * 100;
                }
                if ((int) $val['amtB'] > 0) {
                    $quasum3 = ((float) $dataQuery[$arr]['amtq3'] / (float) $val['amtA']) * 100;
                }
                if ((int) $val['amtB'] > 0) {
                    $quasum4 = ((float) $dataQuery[$arr]['amtq4'] / (float) $val['amtA']) * 100;
                }
                if ($sum > 0)
                    $sum = number_format($sum, 2, '.', '');
                else
                    $sum = 0;

                $serieData[$arr] = $sum > 0 ? (float) number_format($sum, 1) : 0;


                $dataQuery[$arr]['amtsum'] = $sum;
                $dataQuery[$arr]['amtq1sum'] = $quasum1 > 0 ? number_format((float) $quasum1, 2, '.', '') : 0;
                $dataQuery[$arr]['amtq2sum'] = $quasum2 > 0 ? number_format((float) $quasum2, 2, '.', '') : 0;
                $dataQuery[$arr]['amtq3sum'] = $quasum3 > 0 ? number_format((float) $quasum3, 2, '.', '') : 0;
                $dataQuery[$arr]['amtq4sum'] = $quasum4 > 0 ? number_format((float) $quasum4, 2, '.', '') : 0;
                $arr++;
            }
            if ($totalSumA) {
                $totalSum = ($totalSumB / $totalSumA) * 100;
            } else {
                $totalSum = 0;
            }
        }

        $totalSum = 0 ? 0 : number_format($totalSum, 1);
        //\appxq\sdii\utils\VarDumper::dump($viewRender);
        return $this->renderAjax($viewRender, [
                    'totalSum' => $totalSum,
                    'dataNumber' => $dataNumber,
                    'serieData' => $serieData,
                    'dataQuery' => $dataQuery,
                    'dataPatient' => $dataPatient,
                    'selectCode' => $selectCode,
                    'dataBackward' => $dataBackward,
                    'fromValStart' => $fromValStart,
                    'toValStart' => $toValStart,
                    'fromVal2End' => $fromVal2End,
                    'toVal2End' => $toVal2End,
                    'report_num' => $report_num,
                    'year' => $year,
                    'data' => $data,
                    'zone_select' => $zone_select,
                    'kpiBFilture' => $kpiBFilture,
                    'key_ckd_report' => $key_ckd_report,
                    'labelForGraph' => $labelForGraph,
        ]);
    }

    public static function conDb($host, $port, $db, $user, $pass) {

        $connection = new \yii\db\Connection([
            'dsn' => "mysql:host=" . $host . "; port=" . $port . ";dbname=" . $db,
            'username' => $user,
            'password' => $pass,
            'charset' => 'utf8',
        ]);
        $connection->open();
        return $connection;
    }

    public function queryDataPatient($sqlA = null, $sqlB = null, $hospcode, $kpiBFilture) {
        ini_set('memory_limit', '256M');
        $session = Yii::$app->session;

        $key_ckd_report = $session['key_ckd_report'];
        $convert = $session['convert_ckd_char'];
        $save = $session['save_ckd_key'];

        if ($save == '') {
            unset($session['key_ckd_report']);
            unset($session['save_ckd_key']);
        }

        $selectSql = "SELECT rv.pid ,rv.ptlink,rv.hospcode , $sqlA as 'KPI_A', $sqlB as 'KPI_B', fp.cid, fp.Pname, fp.Name , fp.Lname ";
        if ($kpiBFilture != '')
            $whereSql = " WHERE rv.hospcode = :hospcode AND  $sqlB='1'";
        else
            $whereSql = " WHERE rv.hospcode = $hospcode AND $sqlA ='1' ";
        if ($key_ckd_report != '') {
            $selectSql = "SELECT rv.pid , "
                    . " $sqlA as 'KPI_A', "
                    . " $sqlB as 'KPI_B', "
                    . "decode(unhex(fp.CID),sha2('$key_ckd_report',256)) AS cid,"
                    . "decode(unhex(fp.Name),sha2('$key_ckd_report',256)) AS Name,"
                    . "decode(unhex(fp.Lname),sha2('$key_ckd_report',256)) AS Lname ";

            if ($convert == 1) {
                $selectSql = "SELECT rv.pid , "
                        . " $sqlA as 'KPI_A', "
                        . " $sqlB as 'KPI_B', "
                        . "convert(decode(unhex(fp.CID),sha2('$key_ckd_report',256)) using tis620) AS cid,"
                        . "convert(decode(unhex(fp.Name),sha2('$key_ckd_report',256)) using tis620) AS Name,"
                        . "convert(decode(unhex(fp.Lname),sha2('$key_ckd_report',256)) using tis620) AS Lname ";
            } else {
                unset($session['convert_ckd_char']);
            }
        }

        $sqlPatient = " $selectSql FROM f_person AS fp  "
                // . "  INNER JOIN report_ckd_idv AS rv ON fp.PID=rv.pid and rv.hospcode=fp.sitecode"
                . "  INNER JOIN report_ckd_idv AS rv ON fp.PID=rv.pid and rv.hospcode=fp.hospcode"
                . " $whereSql GROUP BY rv.pid ";
        $DataQuery = CkdnetFunc::queryAll($sqlPatient, [':hospcode' => $hospcode]);

        //\appxq\sdii\utils\VarDumper::dump($DataQuery);         exit();
        return $DataQuery;
    }

    public function queryDataForGraph($sql, $provinceSelect = null, $zoneSelect = null, $hospSelect = null, $amphurSelect = null, $selectCode = null) {

        $totalData = [];
        $total = 0;
        $sqlGroup = "";
        if ($hospSelect != '') {
            
        } else if ($amphurSelect != '') {
            $sqlGroup = " GROUP BY hospcode ";
        } else if ($provinceSelect != '') {
            $sqlGroup = " GROUP BY amphurcode ";
        } else if ($zoneSelect != '') {
            $sqlGroup = " GROUP BY provincecode ";
        } else {
            $sqlGroup = " GROUP BY zonecode ";
        }

        $sql .= $sqlGroup;
        //echo $sql;
        $data = Yii::$app->db->createCommand($sql)->queryALL();
        //$data = CkdnetFunc::queryAll($sql);
        //\appxq\sdii\utils\VarDumper::dump($selectCode);
        $checkCode = "";
        $checkKey = "";
        for ($i = 0; $i < count($selectCode); $i++) {
            $totalData[$i]['zonecode'] = 0;
            $totalData[$i]['province'] = 0;
            $totalData[$i]['amphurcode'] = 0;
            $totalData[$i]['hospcode'] = 0;
            $totalData[$i]['amtA'] = 0;
            $totalData[$i]['amtB'] = 0;
            $totalData[$i]['amtsum'] = 0;
            $totalData[$i]['amtq1'] = 0;
            $totalData[$i]['amtq1sum'] = 0;
            $totalData[$i]['amtq2'] = 0;
            $totalData[$i]['amtq2sum'] = 0;
            $totalData[$i]['amtq3'] = 0;
            $totalData[$i]['amtq3sum'] = 0;
            $totalData[$i]['amtq4'] = 0;
            $totalData[$i]['amtq4sum'] = 0;

            foreach ($data as $value) {
                if ($zoneSelect != '') {
                    $checkCode = $value['provincecode'];
                    $checkKey = 'provincecode';
                    if ($provinceSelect != '') {
                        $checkCode = $value['amphurcode'];
                        $checkKey = 'amphurcode';
                        if ($amphurSelect != '') {
                            $checkCode = $value['hospcode'];
                            $checkKey = 'hospcode';
                        }
                    }
                } else {
                    $checkCode = $value['zonecode'];
                    $checkKey = 'zonecode';
                }

                if ($selectCode[$i][$checkKey] == $checkCode) {
                    $totalData[$i]['zonecode'] = $value['zonecode'];
                    $totalData[$i]['province'] = $value['provincecode'];
                    $totalData[$i]['amphur'] = $value['amphurcode'];
                    $totalData[$i]['hospital'] = $value['hospcode'];
                    $totalData[$i]['amtA'] = $value['amtA'];
                    $totalData[$i]['amtB'] = $value['amtB'];
                    $totalData[$i]['amtq1'] = $value['quarter1'];
                    $totalData[$i]['amtq2'] = $value['quarter2'];
                    $totalData[$i]['amtq3'] = $value['quarter3'];
                    $totalData[$i]['amtq4'] = $value['quarter4'];
                }
            }
        }

        return $totalData;
    }

    public function actionDrilldownList() {
        return $this->renderAjax('drilldown-list');
    }

    public function getKpiAb($KpiAB) {
        if ($KpiAB == '1') {
            return "<center><i class='fa fa-check' aria-hidden='true' style='color:green;'></i></center>";
        } else if ($KpiAB == '0') {
            return "<center><i class='fa fa-times' aria-hidden='true' style='color:red;'></i></center>";
        }
    }

    public function getHospitalMatch($myhosp, $hospList) {
        $hosp = '';
        for ($i = 0; $i < count($hospList); $i++) {
            if ($myhosp == $hospList[$i]) {
                $hosp = $hospList[$i];
            }
        }

        return $hosp;
    }

    public function actionReport18() {
        
        $dataPost = Yii::$app->request->post();
        $y = (date('m')>9?date("Y")+1:date("Y"))+ 543;
        $year = range( $y, $y);
        $itemYear = [];
        foreach ($year as $value) {
            array_push($itemYear, ['id' => $value - 543, 'value' => $value]);
        }
        $itemYear = ArrayHelper::map($itemYear, 'id', 'value');
        
        $sitecode = Yii::$app->user->identity->userProfile->sitecode;
        $sqlCodeAll = " SELECT hcode, amphurcode, provincecode, province, amphur, `name`, zone_code FROM all_hospital_thai WHERE hcode='$sitecode'";
        $resCodeAll = \Yii::$app->db->createCommand($sqlCodeAll)->queryOne();
        $amphur_select = $resCodeAll['amphurcode'];
        $province_select = $resCodeAll['provincecode'];
        $zone_select = $resCodeAll['zone_code'];

                
        return $this->render('report18',[
            'itemYear'=>$itemYear,
            'amphur_select' => $amphur_select,
            'province_select' => $province_select,
            'zone_select' => $zone_select ,
            'dataPost' => $dataPost
            
        ]);
    }

    public function actionReportMatrix() {
        
        //$year_range = range(date("Y") + 543, 2560);

        $year_begin = Yii::$app->request->post('year_begin');
        $year_end = Yii::$app->request->post('year_end');
        
//        ini_set('memory_limit', '254M');
//        ini_set('max_execution_time', 30000);

        $diagsource = Yii::$app->request->post('diagsource');

        $itemDiag = [
                ['id' => '0', 'name' => '-----ทั้งหมด-----'],
                ['id' => '1', 'name' => 'แพทย์วินิจฉัย'],
                ['id' => '2', 'name' => 'ผล LAB'],
                ['id' => '3', 'name' => 'รหัสโรค ICD-10'],
        ];
        
        $sitecode = Yii::$app->user->identity->userProfile->sitecode;
        $sqlCode = " SELECT hcode, amphurcode, provincecode, province, amphur, `name`, zone_code FROM all_hospital_thai WHERE hcode='$sitecode'";
        $resCode = \Yii::$app->db->createCommand($sqlCode)->queryOne();
        
        $zone = Yii::$app->request->post('zone-choice');
        if($zone != ''){
            $province = Yii::$app->request->post('province',$resCode['provincecode']);
            if($province !=''){
                $amphur =  \Yii::$app->request->post("amphur",$province.$resCode['amphurcode']);
                $amphur = substr($amphur, 2);
                if($amphur !=''){
                    $hospital = Yii::$app->request->post('hospital');
                }
            }
        }
       
        
//        $amphur =  \Yii::$app->request->post("amphur",$province.$resCode['amphurcode']);
//        $amphur = substr($amphur, 2);
//        

        //\appxq\sdii\utils\VarDumper::dump($year_begin."/".$zone."/".$province."/".$amphur);

        $sql = "SELECT 
                SUM(N185N185) as `N185N185`,
                SUM(N185N184) as `N185N184`,
                SUM(N185N183) as `N185N183`,
                SUM(N185N182) as `N185N182`,
                SUM(N185N181) as `N185N181`,
                SUM(N184N185) as `N184N185`,
                SUM(N184N184) as `N184N184`,
                SUM(N184N183) as `N184N183`,
                SUM(N184N182) as `N184N182`,
                SUM(N184N181) as `N184N181`,
                SUM(N183N185) as `N183N185`,
                SUM(N183N184) as `N183N184`,
                SUM(N183N183) as `N183N183`,
                SUM(N183N182) as `N183N182`,
                SUM(N183N181) as `N183N181`,
                SUM(N182N185) as `N182N185`,
                SUM(N182N184) as `N182N184`,
                SUM(N182N183) as `N182N183`,
                SUM(N182N182) as `N182N182`,
                SUM(N182N181) as `N182N181`,
                SUM(N181N185) as `N181N185`,
                SUM(N181N184) as `N181N184`,
                SUM(N181N183) as `N181N183`,
                SUM(N181N182) as `N181N182`,
                SUM(N181N181) as `N181N181`
                FROM
                report_ckd_sum_all
                WHERE 1
                ";

        if ($zone != null) {
            $sql = $sql . " AND (zonecode = $zone)";
            $queryData = \Yii::$app->db->createCommand($sql)->queryAll();
        }
        if ($province != null) {
            $sql = $sql . " AND (provincecode = $province)";
            $queryData = \Yii::$app->db->createCommand($sql)->queryAll();
        }
        if ($amphur != null) {
            $sql = $sql . " AND (amphurcode = $amphur)";
            $queryData = \Yii::$app->db->createCommand($sql)->queryAll();
        }
        if ($hospital != null) {
            $sql = $sql . " AND (hospcode = $hospital)";
            $queryData = \Yii::$app->db->createCommand($sql)->queryAll();
        }

        $queryData = \Yii::$app->db->createCommand($sql)->queryAll();
        
        $sum_better = $queryData[0]['N182N181'] + $queryData[0]['N183N181'] + $queryData[0]['N184N181'] +
                $queryData[0]['N185N181'] + $queryData[0]['N183N182'] + $queryData[0]['N184N182'] +
                $queryData[0]['N185N182'] + $queryData[0]['N184N183'] + $queryData[0]['N185N183'] + $queryData[0]['N185N184'];
        $sum_stable = $queryData[0]['N181N181'] + $queryData[0]['N182N182'] + $queryData[0]['N183N183'] + $queryData[0]['N184N184'] + $queryData[0]['N185N185'];
        $sum_worst = $queryData[0]['N181N182'] + $queryData[0]['N181N183'] + $queryData[0]['N181N184'] +
                $queryData[0]['N181N185'] + $queryData[0]['N182N183'] + $queryData[0]['N182N184'] +
                $queryData[0]['N182N185'] + $queryData[0]['N183N184'] + $queryData[0]['N183N185'] + $queryData[0]['N184N185'];

        $queryCol = \Yii::$app->db->getTableSchema('report_ckd_sum_all')->getColumnNames(); //query columns : N181N181
        if ($hospital != null) {
            $sql_hname = "SELECT
                    hcode,`name` as hname
                    FROM
                    all_hospital_thai
                    WHERE
                    hcode = $hospital
                    ";
            $hname = \Yii::$app->db->createCommand($sql_hname)->queryAll();

//        \appxq\sdii\utils\VarDumper::dump($hname[0]['hname']);
        }
        return $this->renderAjax('_mat', [
                    'queryData' => $queryData,
                    'queryCol' => $queryCol,
                    'hname' => $hname,
                    'sum_better' => $sum_better,
                    'sum_stable' => $sum_stable,
                    'sum_worst' => $sum_worst,
                    'year_range' => $year_range,
                    'year_begin' => $year_begin,
                    'diagsource' => $diagsource,
                    'itemDiag' => $itemDiag,
                    'zone' => $zone,
                    'province' => $province,
                    'amphur' => $amphur,
                    'hospital' => $hospital
        ]);
    }

    public function actionDrilldownMat() {

        if (Yii::$app->request->isAjax) {

            $id = Yii::$app->request->get('id'); //stage n181181
            $zone = Yii::$app->request->get('zone');
            $province = Yii::$app->request->get('province');
            $amphur = Yii::$app->request->get('amphur');
            $hospital = Yii::$app->request->get('hospital');
            $hname = Yii::$app->request->get('hname');
            $drilldown = Yii::$app->request->get('drilldown');
            $stage = Yii::$app->request->get('stage');

            switch ($id) {
                case N181N181: $stage_desc = "ระยะที่ 1";
                    break;
                case N181N182: $stage_desc = "ระยะที่ 1 เปลี่ยนแปลงไปยังระยะที่ 2";
                    break;
                case N181N183: $stage_desc = "ระยะที่ 1 เปลี่ยนแปลงไปยังระยะที่ 3";
                    break;
                case N181N184: $stage_desc = "ระยะที่ 1 เปลี่ยนแปลงไปยังระยะที่ 4";
                    break;
                case N181N185: $stage_desc = "ระยะที่ 1 เปลี่ยนแปลงไปยังระยะที่ 5";
                    break;
                case N182N181: $stage_desc = "ระยะที่ 2 เปลี่ยนแปลงไปยังระยะที่ 1";
                    break;
                case N182N182: $stage_desc = "ระยะที่ 2";
                    break;
                case N182N183: $stage_desc = "ระยะที่ 2 เปลี่ยนแปลงไปยังระยะที่ 3";
                    break;
                case N182N184: $stage_desc = "ระยะที่ 2 เปลี่ยนแปลงไปยังระยะที่ 4";
                    break;
                case N182N185: $stage_desc = "ระยะที่ 2 เปลี่ยนแปลงไปยังระยะที่ 5";
                    break;
                case N183N181: $stage_desc = "ระยะที่ 3 เปลี่ยนแปลงไปยังระยะที่ 1";
                    break;
                case N183N182: $stage_desc = "ระยะที่ 3 เปลี่ยนแปลงไปยังระยะที่ 2";
                    break;
                case N183N183: $stage_desc = "ระยะที่ 3";
                    break;
                case N183N184: $stage_desc = "ระยะที่ 3 เปลี่ยนแปลงไปยังระยะที่ 4";
                    break;
                case N183N185: $stage_desc = "ระยะที่ 3 เปลี่ยนแปลงไปยังระยะที่ 5";
                    break;
                case N184N181: $stage_desc = "ระยะที่ 4 เปลี่ยนแปลงไปยังระยะที่ 1";
                    break;
                case N184N182: $stage_desc = "ระยะที่ 4 เปลี่ยนแปลงไปยังระยะที่ 2";
                    break;
                case N184N183: $stage_desc = "ระยะที่ 4 เปลี่ยนแปลงไปยังระยะที่ 3";
                    break;
                case N184N184: $stage_desc = "ระยะที่ 4";
                    break;
                case N184N185: $stage_desc = "ระยะที่ 4 เปลี่ยนแปลงไปยังระยะที่ 5";
                    break;
                case N185N181: $stage_desc = "ระยะที่ 5 เปลี่ยนแปลงไปยังระยะที่ 1";
                    break;
                case N185N182: $stage_desc = "ระยะที่ 5 เปลี่ยนแปลงไปยังระยะที่ 2";
                    break;
                case N185N183: $stage_desc = "ระยะที่ 5 เปลี่ยนแปลงไปยังระยะที่ 3";
                    break;
                case N185N184: $stage_desc = "ระยะที่ 5 เปลี่ยนแปลงไปยังระยะที่ 4";
                    break;
                case N185N185: $stage_desc = "5";
                    break;
            }

            $urlBackDrilldown = [];
// \appxq\sdii\utils\VarDumper::dump($_GET);
            if ($drilldown == NULL) {
                if ($id != '') {
                    $sqlWhere = ",sum($id) as amount";
                }
//               \appxq\sdii\utils\VarDumper::dump($driildown);
            } else {
                if ($stage == 'stage_d') {
                    $sqlWhere = ",sum(N182N181 + N183N181 + N184N181 + N185N181 + N183N182 + N184N182 + N185N182 + N184N183 + N185N183 + N185N184) as amount";
                    $stage_desc = "ที่มีการเปลี่ยนแปลงระยะของโรคในทางที่ดีขึ้น (ชะลอไตเสื่อม)";
                } else if ($stage == 'stage_m') {
                    $sqlWhere = ",sum(N181N181 + N182N182 + N183N183 + N184N184 + N185N185) as amount";
                    $stage_desc = "ที่มีการเปลี่ยนแปลงระยะของโรคคงที่ (ชะลอไตเสื่อม)";
                } else if ($stage == 'stage_b') {
                    $sqlWhere = ",sum(N181N182 + N181N183 + N181N184 + N181N185 + N182N183 + N182N184 + N182N185 + N183N184 + N183N185 + N184N185) as amount";
                    $stage_desc = "ที่มีการเปลี่ยนแปลงระยะของโรคในทางที่แย่ลง";
                }
            }
            if ($zone == NULL) {

                $sql = "SELECT IFNULL('" . $id . "',0) as id,'" . $stage . "' as stage,'" . $drilldown . "' as drilldown ,zonecode as zone $sqlWhere FROM report_ckd_sum_all"
                        . " WHERE zonecode !=13 GROUP BY zonecode";
                $query = \Yii::$app->db->createCommand($sql)->queryAll();
                $htxt = "จำนวนผู้ป่วยโรคไตเรื้อรัง " . $stage_desc . " แบ่งตามเขตสุขภาพ";
            } else {
                if ($province == NULL) {

                    $sql = "SELECT IFNULL('" . $id . "',0) as id,'" . $stage . "' as stage,'" . $drilldown . "' as drilldown,t1.zone_code,t1.provincecode,t1.province,IFNULL(t2.amount,0) as amount
                    FROM 
                    (SELECT zone_code,provincecode,province
                    FROM all_hospital_thai
                    WHERE zone_code = :zonecode
                    GROUP BY provincecode) t1
                    LEFT JOIN
                    (SELECT provincecode $sqlWhere FROM report_ckd_sum_all 
                    WHERE zonecode = :zonecode
                    GROUP BY provincecode) t2
                    ON t1.provincecode = t2.provincecode";
                    $query = \Yii::$app->db->createCommand($sql, [':zonecode' => $zone])->queryAll();
                    $htxt = "จำนวนผู้ป่วยโรคไตเรื้อรัง " . $stage_desc . " แบ่งตามจังหวัด";
                    $urlBackDrilldown = [
                        'drilldown-mat',
                        'id' => $id,
                        'drilldown' => $drilldown,
                        'stage' => $stage
                    ];
                } else {
                    if ($amphur == 0) {
//                         \appxq\sdii\utils\VarDumper::dump($sqlWhere);
                        $sql = "SELECT IFNULL('" . $id . "',0) as id,'" . $stage . "' as stage,'" . $drilldown . "' as drilldown,t1.zone_code,t1.provincecode,t1.amphurcode,t1.amphur,IFNULL(t2.amount,0) as amount
                                FROM 
                                (SELECT zone_code,provincecode,amphurcode,amphur
                                FROM all_hospital_thai
                                WHERE provincecode = :provincecode
                                GROUP BY amphurcode) t1
                                LEFT JOIN
                                (SELECT amphurcode $sqlWhere FROM report_ckd_sum_all 
                                WHERE provincecode = :provincecode
                                GROUP BY amphurcode) t2
                                ON t1.amphurcode = t2.amphurcode";
                        $query = \Yii::$app->db->createCommand($sql, [':provincecode' => $province])->queryAll();
                        $htxt = "จำนวนผู้ป่วยโรคไตเรื้อรัง " . $stage_desc . " แบ่งตามอำเภอ";
                        $urlBackDrilldown = [
                            'drilldown-mat',
                            'id' => $id,
                            'zone' => $zone,
                            'drilldown' => $drilldown,
                            'stage' => $stage
                        ];
                    } else {
                        if ($hospital == NULL) {
                            $sql = "SELECT IFNULL('" . $id . "',0) as id,'" . $stage . "' as stage,'" . $drilldown . "' as drilldown,t1.zone_code,t1.provincecode,t1.amphurcode,t1.hcode,t1.hname,IFNULL(t2.amount,0) as amount
                            FROM 
                            (SELECT zone_code,provincecode,amphurcode,hcode,`name` as hname
                            FROM all_hospital_thai
                            WHERE amphurcode = :amphurcode AND provincecode = :provincecode
                            GROUP BY hcode) t1
                            LEFT JOIN
                            (SELECT hospcode $sqlWhere FROM report_ckd_sum_all 
                            WHERE amphurcode = :amphurcode AND provincecode = :provincecode
                            GROUP BY hospcode) t2
                            ON t1.hcode = t2.hospcode";
                            $query = \Yii::$app->db->createCommand($sql, [':amphurcode' => $amphur, ':provincecode' => $province])->queryAll();
                            $htxt = "จำนวนผู้ป่วยโรคไตเรื้อรัง " . $stage_desc . " แบ่งตามโรงพยาบาล";
                            $urlBackDrilldown = [
                                'drilldown-mat',
                                'id' => $id,
                                'zone' => $zone,
                                'province' => $province,
                                'drilldown' => $drilldown,
                                'stage' => $stage
                            ];
                        } else {
                            if ($hospital == Yii::$app->session['dynamic_connection']['sitecode']) {
                                ini_set('memory_limit', '256M');
                                $session = Yii::$app->session;

                                $key_ckd_report = $session['key_ckd_report'];
                                $convert = $session['convert_ckd_char'];
                                $save = $session['save_ckd_key'];

                                if ($save == '') {
                                    unset($session['key_ckd_report']);
                                    unset($session['save_ckd_key']);
                                }

                                $selectSql = "SELECT SQL_NO_CACHE
                                        IFNULL('" . $id . "',0) as id,        
                                        report_ckd_idv.zonecode as zone_code,
                                        report_ckd_idv.provincecode as provincecode,
                                        report_ckd_idv.amphurcode as amphurcode,
                                        report_ckd_idv.hospcode as hcode,
                                        report_ckd_idv.pid as pid,
                                        f_person.PreName as prename,
                                        f_person.`Name` as fname,
                                        f_person.lName as lname ";

                                $whereSql = " WHERE f_person.sitecode = :hospcode AND f_person.HOSPCODE = :hospcode AND report_ckd_idv.hospcode = :hospcode";
//                                        AND CONCAT(report_ckd_idv.first_cfcode,report_ckd_idv.last_cfcode) = '" . $id . "'  ";
                                $Con = "CONCAT(report_ckd_idv.first_cfcode,report_ckd_idv.last_cfcode)";

                                if ($drilldown <> 1) {
                                    $conSql = " $Con = '" . $id . "' ";
                                } else {
                                    if ($stage == 'stage_d') {
                                        $conSql = "($Con = 'N182N181' OR $Con = 'N183N181' OR $Con = 'N184N181' OR $Con = 'N185N181' OR $Con =  'N183N182' "
                                                . "OR $Con = 'N184N182' OR $Con = 'N185N182' OR $Con = 'N184N183' OR $Con = 'N185N183' OR $Con = 'N185N184')";
                                        $htxt = "ตารางแสดงรายชื่อผู้ป่วยโรคไตเรื้อรังที่มีการเปลี่ยนแปลงระยะของโรคในทางที่ดีขึ้น (ชะลอไตเสื่อม)";
                                    } else if ($stage == 'stage_m') {
                                        $conSql = "($Con =  'N181N181' OR $Con =  'N182N182' OR $Con =  'N183N183' OR $Con =  'N184N184' OR $Con =  'N185N185')";
                                        $htxt = "ตารางแสดงรายชื่อผู้ป่วยโรคไตเรื้อรังที่มีการเปลี่ยนแปลงระยะของโรคคงที่ (ชะลอไตเสื่อม)";
                                    } else if ($stage == 'stage_b') {
                                        $conSql = "($Con = 'N181N182' OR $Con = 'N181N183' OR $Con = 'N181N184' OR $Con = 'N181N185' OR $Con =  'N182N183' "
                                                . "OR $Con = 'N182N184' OR $Con = 'N182N185' OR $Con = 'N183N184' OR $Con = 'N183N185' OR $Con = 'N184N185')";
                                        $htxt = "ตารางแสดงรายชื่อผู้ป่วยโรคไตเรื้อรังที่มีการเปลี่ยนแปลงระยะของโรคในทางที่แย่ลง";
                                    }
                                }

                                if ($key_ckd_report != '') {
                                    $selectSql = "SELECT SQL_NO_CACHE "
                                            . " '" . $id . "' as id, "
                                            . " report_ckd_idv.zonecode as zone_code, "
                                            . " report_ckd_idv.provincecode as provincecode, "
                                            . " report_ckd_idv.amphurcode as amphurcode, "
                                            . " report_ckd_idv.hospcode as hcode, "
                                            . "report_ckd_idv.pid AS pid,"
                                            . "decode(unhex(f_person.PreName),sha2('$key_ckd_report',256)) AS prename,"
                                            . "decode(unhex(f_person.`Name`),sha2('$key_ckd_report',256)) AS fname,"
                                            . "decode(unhex(f_person.LName),sha2('$key_ckd_report',256)) AS lname ";

                                    if ($convert == 1) {
                                        $selectSql = "SELECT SQL_NO_CACHE "
                                                . " '" . $id . "' as id, "
                                                . " report_ckd_idv.zonecode as zone_code, "
                                                . " report_ckd_idv.provincecode as provincecode, "
                                                . " report_ckd_idv.amphurcode as amphurcode, "
                                                . " report_ckd_idv.hospcode as hcode, "
                                                . " report_ckd_idv.pid AS pid,"
                                                . "convert(decode(unhex(f_person.PreName),sha2('$key_ckd_report',256)) using tis620) AS prename,"
                                                . "convert(decode(unhex(f_person.`Name`),sha2('$key_ckd_report',256)) using tis620) AS fname, "
                                                . "convert(decode(unhex(f_person.LName),sha2('$key_ckd_report',256)) using tis620) AS lname ";
                                    } else {
                                        unset($session['convert_ckd_char']);
                                    }
                                }

                                $sqlPatient = " $selectSql FROM report_ckd_idv JOIN f_person ON f_person.PID = report_ckd_idv.pid  $whereSql AND $conSql ";

                                $query = CkdnetFunc::queryAll($sqlPatient, [':hospcode' => $hospital]);
                            }
                            $htxt = "จำนวนผู้ป่วยโรคไตเรื้อรัง " . $stage_desc . " " . $hname;
                            $urlBackDrilldown = [
                                'drilldown-mat',
                                'id' => $id,
                                'zone' => $zone,
                                'province' => $province,
                                'amphur' => $amphur,
                                'drilldown' => $drilldown,
                                'stage' => $stage
                            ];
                        }
                    }
                }
            }

            $queryCol = \Yii::$app->db->getTableSchema('report_ckd_sum_all')->getColumnNames();

            $dataprovider = new \yii\data\ArrayDataProvider([
                'allModels' => $query,
                'pagination' => [
                    'pageSize' => 50,
                ]
            ]);

            return $this->renderAjax('_drilldown-matrix', [
                        'dataprovider' => $dataprovider,
                        'queryCol' => $queryCol,
                        'htxt' => $htxt,
                        'hname' => $hname,
                        'id' => $id,
                        'zone' => $zone,
                        'province' => $province,
                        'amphur' => $amphur,
                        'hospital' => $hospital,
                        'urlBackDrilldown' => $urlBackDrilldown,
                        'key_ckd_report' => $key_ckd_report]);
        } else {
            return $this->redirect(['report-18']);
        }
    }

}
