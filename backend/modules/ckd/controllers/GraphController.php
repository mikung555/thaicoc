<?php

namespace backend\modules\ckd\controllers;

use Yii;
use yii\web\Response;
use backend\modules\ckd\models\Graph;
use backend\modules\ckdnet\classes\CkdnetQuery;
use backend\modules\ckdnet\classes\CkdnetFunc;
use miloschuman\highcharts\Highcharts;

class GraphController extends \yii\web\Controller {

    public function actionGraphFull() {

        $hospcode = \Yii::$app->request->get("hospcode"); //$_GET["hospcode"];
        $ptlink = \Yii::$app->request->get("ptlink");
        $pid = \Yii::$app->request->get("pid");
        $graph = \Yii::$app->request->get("graph");
        $cid = \Yii::$app->request->get("cid");

        return $this->renderAjax("graphfull", [
                    'hospcode' => $hospcode,
                    'ptlink' => $ptlink,
                    'pid' => Yii::$app->session['emr_pid'],
                    'graph' => $graph,
                    'cid' => $cid,
        ]);
    }

    public function actionShowgraphload() {
        $hospcode = \Yii::$app->request->post("hospcode");
        $ptlink = \Yii::$app->request->post("ptlink");
        $pid = \Yii::$app->request->post("pid");
        $graph = \Yii::$app->request->post("graph");
        $cid = \Yii::$app->request->post("cid");
        //print_r($_POST);exit();

        return $this->renderAjax("/emr/_graph", [
                    'hospcode' => $hospcode,
                    'ptlink' => $ptlink,
                    'pid' => Yii::$app->session['emr_pid'],
                    'graph' => $graph,
                    'cid' => $cid,
        ]);
    }

    //put your code here
    public function actionIndex() {


        $model = new Graph();
        $post = \Yii::$app->request->post();

        $resort = Graph::find()
                ->where(["hospcode" => Yii::$app->session['dynamic_connection']['sitecode']])
                ->all();
        if (!empty($resort)) {
            $nomenu = ['1' => 'อยู่หน้าสุด'];
            $resort = yii\helpers\ArrayHelper::map($resort, 'forder', 'group');
            $resort2 = \yii\helpers\ArrayHelper::merge($nomenu, $resort);
            $x = [];
            foreach ($resort2 as $k => $v) {
                array_push($x, [
                    "forder" => $k,
                    "group" => $v
                ]);
            }
            $resort = \yii\helpers\ArrayHelper::map($x, 'forder', 'group');
        } else {
            $nomenu = [
                ["forder" => "1", "group" => "อยู่หน้าสุด"],
                ["forder" => "10", "group" => "อยู่หลัง CKD"],
                ["forder" => "20", "group" => "อยู่หลัง PD"],
                ["forder" => "30", "group" => "อยู่หลัง DM/HT"],
                ["forder" => "40", "group" => "อยู่หลัง All"]
            ];

            $resort = yii\helpers\ArrayHelper::map($nomenu, 'forder', 'group');
        }

        if ($model->load($post)) {

            $hospcode = Yii::$app->session['dynamic_connection']['sitecode'];

            $check = \backend\modules\ckd\classes\Ckdgraph::CheckLabGrapGroupSite($hospcode);

            if ($check == 0) {
                \backend\modules\ckd\classes\Ckdgraph::AddLabGraphGroupList($hospcode);
            }

            $forder = \backend\modules\ckd\classes\Ckdgraph::Showmaxforder($hospcode); //ดึงเอาค่ามากสุดของ forder มาบวกกัน
            $number = $post["Graph"]["numbers"]; //จะให้เรียงจากด้านหลังตัวไหน


            if (empty($number)) {//ถ้าไม่มีข้อมูล
                $forder += 10;
            } else {
                $forder = $number + 5;
            }


            $ckid = \backend\modules\ckd\models\LabGraphGroup::find()->where(['hospcode' => $hospcode])->orderBy(['id' => SORT_DESC])->one();
            if ($ckid->id >= 99) {
                $model->id = $ckid->id + 1;
            } else {
                $model->id = 99;
            }


            $model->hospcode = $hospcode;
            $model->group = \yii\helpers\Html::encode($post["Graph"]["group"]);
            $model->forder = $forder;
            if ($model->save()) {

                \Yii::$app->response->format = yii\web\Response::FORMAT_JSON;
                $result = [
                    'status' => 'success',
                    'action' => 'create',
                    'message' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> Success!</strong> ' . Yii::t('app', ''),
                ];
            } else {
                \Yii::$app->response->format = yii\web\Response::FORMAT_JSON;
                $result = [
                    'status' => 'errot',
                    'action' => 'create',
                    'message' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> error!</strong> ' . Yii::t('app', ''),
                ];
            }
            return $result;
        }


        return $this->renderAjax("graph", [
                    'model' => $model,
                    'resort' => $resort
        ]);
    }

//index

    public function actionForder() {
        //เรียงลำดับนะครับ 
        $sqli = "SELECT * from lab_graph_group_site ORDER BY forder ASC";
        $csort = \backend\modules\ckdnet\classes\CkdnetFunc::queryAll($sqli);
        $forder = 10;

        foreach ($csort as $v) {
            $sql = "UPDATE lab_graph_group_site SET forder=:forder WHERE id=:id ";
            \backend\modules\ckdnet\classes\CkdnetFunc::execute($sql, [
                ':forder' => $forder,
                ':id' => $v['id']
            ]);

            $forder += 10;
        }

        //
    }

    public function actionGraphUpdateMenu() {
        $model = Graph::find()
                        ->where(['id' => $_GET['id'], 'hospcode' => Yii::$app->session['dynamic_connection']['sitecode']])->one();
        $post = Yii::$app->request->post();

        $resort = Graph::find()->where(["hospcode" => Yii::$app->session['dynamic_connection']['sitecode']])->all();
        if (!empty($resort)) {
            $nomenu = ['1' => 'อยู่หน้าสุด'];
            $resort = yii\helpers\ArrayHelper::map($resort, 'forder', 'group');
            $resort2 = \yii\helpers\ArrayHelper::merge($nomenu, $resort);
            $x = [];
            foreach ($resort2 as $k => $v) {
                array_push($x, [
                    "forder" => $k,
                    "group" => $v
                ]);
            }
            $resort = \yii\helpers\ArrayHelper::map($x, 'forder', 'group');
        } else {
            $nomenu = [
                ["forder" => "1", "group" => "อยู่หน้าสุด"],
                ["forder" => "10", "group" => "CKD"],
                ["forder" => "20", "group" => "PD"],
                ["forder" => "30", "group" => "DM/HT"],
                ["forder" => "40", "group" => "All"]
            ];

            $resort = yii\helpers\ArrayHelper::map($nomenu, 'forder', 'group');
        }

        if ($model->load($post)) {

            $model->group = $post["Graph"]["group"];

            $forder = \backend\modules\ckd\classes\Ckdgraph::Showmaxforder($hospcode); //ดึงเอาค่ามากสุดของ forder มาบวกกัน
            $number = $post["Graph"]["numbers"]; //จะให้เรียงจากด้านหลังตัวไหน

            if (empty($number)) {//ถ้าไม่มีข้อมูล
                $forder += 10;
            } else {
                $forder = $number + 5;
            }
            $model->forder = $forder;


            if ($model->save()) {
                \Yii::$app->response->format = yii\web\Response::FORMAT_JSON;
                $result = [
                    'status' => 'success',
                    'action' => 'create',
                    'message' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> Success!</strong> ' . Yii::t('app', ''),
                ];
            } else {
                \Yii::$app->response->format = yii\web\Response::FORMAT_JSON;
                $result = [
                    'status' => 'errot',
                    'action' => 'create',
                    'message' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> error!</strong> ' . Yii::t('app', ''),
                ];
            }
            return $result;
        }


        return $this->renderAjax("graph", [
                    'model' => $model,
                    'resort' => $resort
        ]);
    }

//แก้ไขเมนูกราฟ

    public function actionAdd() {

        $model = new \backend\modules\ckd\models\Labgraplists();

        $group_id = \Yii::$app->request->get('graph');

        $id = \Yii::$app->request->get('id');
        \Yii::$app->session['cid_'] = $id;
        //\appxq\sdii\utils\VarDumper::dump($id);


        $cstatus = \Yii::$app->request->get("status");

        \Yii::$app->session['group_id'] = $group_id;
        $post = \Yii::$app->request->post();
        $status = 1; //สร้างใหม่
        if (!empty($id)) {
            $status = 2;
        }

        //\appxq\sdii\utils\VarDumper::dump($model);

        $sql = "SELECT * FROM lab_map WHERE HOSPCODE=:hospcode";
        $params = [
            ':hospcode' => Yii::$app->session['dynamic_connection']['sitecode']
        ];
        $labmap = \backend\modules\ckdnet\classes\CkdnetFunc::queryAll($sql, $params);
        $labmap = \yii\helpers\ArrayHelper::map($labmap, 'tdc_lab_items_code', 'graph_label');
        return $this->renderAjax('add', [
                    'graph' => $group_id, //graph
                    'model' => $model,
                    'labmap' => $labmap,
                    'id' => $id,
                    'status' => $status,
                    'cstatus' => $cstatus
        ]);
    }

//add

    public function actionGetForm() {
        $model = new \backend\modules\ckd\models\Labgraplists();
        $post = \Yii::$app->request->post();
        $select_graph = \Yii::$app->request->get('select_graph');
        $id = \Yii::$app->request->get('id');

        //\appxq\sdii\utils\VarDumper::dump($id);
        $cstatus = \Yii::$app->request->get('cstatus');
        $group_id = \Yii::$app->request->get('graph');

        //echo $group_id;exit();
        $tdc_l = [];

        if ($select_graph != '0000') {//
            $model = \backend\modules\ckd\models\Labgraplists::find()->where(['id' => $select_graph])->one();



            $defaults = [['id' => '0000', 'graph_name' => 'สร้างเอง']];
            $check_type = "SELECT  GROUP_CONCAT(tdc_lab_code) as labcode, graph_label as labname , tdc_lab_code as labid,tdc_lab_unit as labunit , display_type 
                FROM tdc_lab_items_site 
                where (hospcode = '10980' OR hospcode = '00000') AND (tdc_lab_code<>'' and tdc_lab_code is not null) AND display_type=3
                GROUP BY tdc_lab_code
                ORDER BY graph_label";
            $query = CkdnetFunc::queryAll($check_type);
            foreach ($query as $q) {
                if ($q["labname"] == $model->graph_name) {
                    $model->id = $q["labcode"];
                    //\appxq\sdii\utils\VarDumper::dump($model);
                }
            }
            $tdc_lab_item_code = explode(",", $model->tdc_lab_item_code);
            $model->tdc_lab_item_code = $tdc_lab_item_code;
        }

        //Ajax Validation Start
        $request = Yii::$app->getRequest();
        if ($request->isPost && $request->post('ajax') !== null) {
            $model->load(Yii::$app->request->post());
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON; //กำหนดให้ข้อมูล response ในรูปแบบ JSON
            $result = \yii\bootstrap\ActiveForm::validate($model); // validate ได้หลาย model
            return $result;
        }

        //Ajax Validation End

        if (!empty($post['Labgraplists'])) {
            //\appxq\sdii\utils\VarDumper::dump($_POST['Labgraplists']); 
            $csid = $post['Labgraplists']['cid']; //รหัส 
            $cstatus_c = $post['Labgraplists']['cstatus'];

            $cstatus = "create";

            if (!empty($csid)) {//update
                $cstatus = "update";
                $model = \backend\modules\ckd\models\Labgraplists::findOne($csid);
            } else {
                $mid = 999;
                $model = new \backend\modules\ckd\models\Labgraplists();

                $maxid = \backend\modules\ckd\models\Labgraplists::find()->orderBy(['id' => SORT_DESC])->one();
                if (empty($maxid)) {
                    $mid = 999;
                    $csid = $mid;
                } else {
                    $mid = $maxid->id + 1;
                    $csid = $mid;
                }
            }//create
            // echo $csid;exit();



            $model->group_id = $group_id;
            $model->graph_name = $post['Labgraplists']['graph_name'];
            $model->cols = $post['Labgraplists']['cols'];
            $model->hospcode = Yii::$app->session['dynamic_connection']['sitecode'];


            Yii::$app->session['cols'] = $model->cols;

            $model->id = $csid;

            $tdc_lab_item_code = "";
            foreach ($post['Labgraplists']["tdc_lab_item_code"] as $key => $value) {
                $tdc_lab_item_code .= "," . $value;
            }
            $length = strlen($tdc_lab_item_code);
            $substrs = substr($tdc_lab_item_code, 1, $length);

            $tdc_lab_item_code = $substrs;
            $model->tdc_lab_item_code = $tdc_lab_item_code;

            $_width = $post['Labgraplists']['cols'];
            if ($_width == '6') {
                $_width = '435';
            } else {
                $_width = '899';
            }
            if ($model->save()) {

                $maxid2 = \backend\modules\ckd\models\Labgraplists::find()->orderBy(['id' => SORT_DESC])->one(); //create
                \Yii::$app->response->format = yii\web\Response::FORMAT_JSON;
                $result = [
                    'status' => 'success',
                    'action' => 'create',
                    'message' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> Success!</strong> ' . Yii::t('app', ''),
                    'id' => $id, //update , delete
                    'cstatus' => $cstatus,
                    'maxid' => $maxid2->id,
                    '_width' => $_width
                ];
            } else {
                \Yii::$app->response->format = yii\web\Response::FORMAT_JSON;
                $result = [
                    'status' => 'errot',
                    'action' => 'create',
                    'message' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> error!</strong> ' . Yii::t('app', ''),
                ];
            }

            return $result;
        }


        $sql = "SELECT * FROM lab_map WHERE HOSPCODE=:hospcode AND LABCODE != '' ";
        $params = [
            ':hospcode' => Yii::$app->session['dynamic_connection']['sitecode']
        ];
        $labmap = \backend\modules\ckdnet\classes\CkdnetFunc::queryAll($sql, $params);
        //$labmap = \yii\helpers\ArrayHelper::map($labmap, 'tdc_lab_items_code', 'graph_label');


        $sql2 = "SELECT tdc_lab_code as tdc_lab_items_code ,graph_label, tdc_lab_unit,display_type FROM tdc_lab_items_site WHERE (HOSPCODE=:hospcode OR HOSPCODE='00000') AND display_type =3 ";
        $params2 = [
            ':hospcode' => Yii::$app->session['dynamic_connection']['sitecode']
        ];
        $labmap2 = \backend\modules\ckdnet\classes\CkdnetFunc::queryAll($sql2, $params2);

        $labmap = \yii\helpers\ArrayHelper::merge($labmap, $labmap2);
        $labmap = \yii\helpers\ArrayHelper::map($labmap, 'tdc_lab_items_code', 'graph_label');

        //\appxq\sdii\utils\VarDumper::dump($labmap);
        return $this->renderAjax('add2', [
                    'model' => $model,
                    'labmap' => $labmap,
                    'id' => $id,
                    'cstatus' => $cstatus,
                    'group_id' => $group_id,
                    'tdc_l' => $tdc_l
        ]);
    }

    public function actionUpdate() {

        $group_id = \Yii::$app->session['graph']; //group_id
        $id = \Yii::$app->request->get('id'); //id
        $model = \backend\modules\ckd\models\Labgraplists::find()->where(['id' => $id])->one();
        //echo $group_id;exit();


        $post = \Yii::$app->request->post();

        //Ajax Validation Start
        $request = Yii::$app->getRequest();
        if ($request->isPost && $request->post('ajax') !== null) {
            $model->load(Yii::$app->request->post());
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON; //กำหนดให้ข้อมูล response ในรูปแบบ JSON
            $result = \yii\bootstrap\ActiveForm::validate($model); // validate ได้หลาย model
            return $result;
        }
        //Ajax Validation End


        if (!empty($post)) {
            \backend\modules\ckd\models\Labgraplists::find()->where(['id' => $id])->one()->delete();
            $model = new \backend\modules\ckd\models\Labgraplists();
            $tdc_lab_item_code = "";
            foreach ($post['Labgraplists']["tdc_lab_item_code"] as $key => $value) {
                $tdc_lab_item_code .= "," . $value;
            }
            $length = strlen($tdc_lab_item_code);
            $substrs = substr($tdc_lab_item_code, 1, $length);

            $tdc_lab_item_code = $substrs;

            $model->group_id = $group_id;
            $model->graph_name = $post['Labgraplists']['graph_name'];
            $model->tdc_lab_item_code = $tdc_lab_item_code;



            if ($model->save()) {

                \Yii::$app->response->format = yii\web\Response::FORMAT_JSON;
                $result = [
                    'status' => 'success',
                    'action' => 'create',
                    'message' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> Success!</strong> ' . Yii::t('app', ''),
                ];
            } else {
                \Yii::$app->response->format = yii\web\Response::FORMAT_JSON;
                $result = [
                    'status' => 'errot',
                    'action' => 'create',
                    'message' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> error!</strong> ' . Yii::t('app', ''),
                ];
            }

            return $result;
        } else {
            $sql = "SELECT * FROM lab_map WHERE HOSPCODE=:hospcode";
            $params = [
                ':hospcode' => Yii::$app->session['dynamic_connection']['sitecode']
            ];
            $labmap = \backend\modules\ckdnet\classes\CkdnetFunc::queryAll($sql, $params);
            $labmap = \yii\helpers\ArrayHelper::map($labmap, 'tdc_lab_items_code', 'graph_label');

            $tdc_lab_item_code_arr = explode(",", $model->tdc_lab_item_code);
            // print_r($tdc_lab_item_code_arr); exit();
            $model->tdc_lab_item_code = $tdc_lab_item_code_arr;

            return $this->renderAjax('add', [
                        'graph' => $graph,
                        'model' => $model,
                        'labmap' => $labmap
            ]);
        }//endif
    }

//Edit

    public function actionDelete() {
        $hospcode = $_POST['hospcode'];
        $graph = $_POST['graph'];
        $graph = Graph::find()->where(['hospcode' => $hospcode, 'id' => $graph])->all();
        $status = 0;

        if (!empty($graph)) {
            foreach ($graph as $g) {
                $sql = "DELETE FROM lab_graph_list WHERE group_id=:group_id";
                \backend\modules\ckdnet\classes\CkdnetFunc::execute($sql, [
                    ':group_id' => $g->id
                ]);
                $g->delete();
                $status = 1;
            }
        }


        if ($status == 1) {
            \Yii::$app->response->format = yii\web\Response::FORMAT_JSON;
            $result = [
                'status' => 'success',
                'action' => 'create',
                'message' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> success</strong> ' . Yii::t('app', ''),
            ];
        } else {
            \Yii::$app->response->format = yii\web\Response::FORMAT_JSON;
            $result = [
                'status' => 'errot',
                'action' => 'create',
                'message' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> error!</strong> ' . Yii::t('app', ''),
            ];
        }

        return $result;
    }

//delete graph

    public function actionDeleteGraph() {
        $id = \Yii::$app->request->post('id');

        if (!empty(\Yii::$app->request->post())) {
            $model = \backend\modules\ckd\models\Labgraplists::find()->where(['id' => $id])->one();
            $status = 0;
            if ($model->delete()) {
                $status = 1;
            }
            //$status=1;

            if ($status == 1) {
                \Yii::$app->response->format = yii\web\Response::FORMAT_JSON;
                $result = [
                    'status' => 'success',
                    'action' => 'create',
                    'message' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> success</strong> ' . Yii::t('app', ''),
                ];
            } else {
                \Yii::$app->response->format = yii\web\Response::FORMAT_JSON;
                $result = [
                    'status' => 'errot',
                    'action' => 'create',
                    'message' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> error!</strong> ' . Yii::t('app', ''),
                ];
            }//end if 
            return $result;
        }//end if
    }

//

    public function actionCheckForm() {
        $id = \Yii::$app->request->get('id');
        echo $id;
    }

    public function actionPopupGraph() {


        $id = \Yii::$app->request->get("id");
        $hospcode = \Yii::$app->session['dynamic_connection']['sitecode'];
        $ptlink = \Yii::$app->request->get("ptlink");

        $pid = isset($_GET['pid']) ? $_GET['pid'] : null; //\Yii::$app->request->get("pid");//เลขระบุตัวตน
        $graph = \Yii::$app->request->get("graph");
        $cid = \Yii::$app->request->get("cid");
        $renderid = isset($_GET['renderid']) ? $_GET['renderid'] : $id;

        $widths = isset($_GET['widths']) ? $_GET['widths'] : 435;
//        \appxq\sdii\utils\VarDumper::dump(\Yii::$app->request->get("ptlink"));
//        if($widths != 435){
//            $widths;
//        }
        //\appxq\sdii\utils\VarDumper::dump(Yii::$app->session['emr_pid']);
        //$series = CkdnetFunc::getLab($ptlink, $hospcode, Yii::$app->session['emr_pid']);

        $series = [];
        $highcharts_config = [
            'setupOptions' => [
                'lang' => [
                    'shortMonths' => ['ม.ค.', 'ก.พ.', 'มี.ค.', 'เม.ย.', 'พ.ค.', 'มิ.ย.', 'ก.ค.', 'ส.ค.', 'ก.ย.', 'ต.ค.', 'พ.ย.', 'ธ.ค.'],
                    'shortWeekdays' => ['อา.', 'จ.', 'อ.', 'พ.', 'พฤ.', 'ศ.', 'ส.'],
                    'weekdays' => ['อาทิตย์', 'จันทร์', 'อังคาร', 'พุธ', 'พฤหัสบดี', 'ศุกร์', 'เสาร์'],
                ],
            ],
            'options' => [
                'chart' => [
                    'width' => $widths,
                    'renderTo' => $renderid
                ],
                'title' => ['text' => 'กราฟ'],
                'xAxis' => [
                    'type' => 'datetime',
                    'dateTimeLabelFormats' => [
                        'day' => "%e %b %Y",
                        'month' => '%e %b %Y',
                        'year' => '%e %b %Y'
                    ],
                    'title' => [
                        'text' => 'Visit Date'
                    ],
                    'labels' => [
                        'rotation' => -45,
                    ],
                ],
                'yAxis' => [
                    'title' => ['text' => 'Result'],
                ],
                'tooltip' => [
                    'shared' => true,
                    'useHTML' => true,
                    'headerFormat' => '<small><b>{point.x:%A, %e %b %Y}</b></small><table><tr><td>',
//                            'pointFormat' => '<tr><td style="color: {series.color}">{series.name}: </td><td style="text-align: right"><b>{point.y:.2f} </b></td></tr>',
                    'footerFormat' => '</td></tr></table>',
                ],
                'plotOptions' => [
                    'line' => [
                        'dataLabels' => [
                            'enabled' => false,
                            'color' => 'gray',
                            'y' => -5,
                        ],
                    ]
                ],
                'series' => []
            ]
        ]; //$highcharts_config
//        \appxq\sdii\utils\VarDumper::dump($ptlink);
        $series = \backend\modules\ckdnet\classes\CkdnetFunc::getLab($ptlink, $hospcode, $pid);

        if (!empty($series)) {
            $list_g = CkdnetQuery::getListGraphOne($graph, $id);
            foreach ($list_g as $key => $value) {
                $findStr = ",{$value['tdc_lab_item_code']},";
                $highcharts_config['options']['title']['text'] = $value['graph_name'];
                $highcharts_config['options']['series'] = $series;
                $highcharts_config['options']['chart']['events']['load'] = new \yii\web\JsExpression("
                function(event) {
                    this.series.forEach(function(d,i){
                        var find_id = ','+d.options.id+',';
                        var find_in_set = '$findStr';
                        
                        if(!find_in_set.includes(find_id)){
                           d.hide();
                        }
                    });
                }
                    ");
            }//foreach
        }//if


        $jsOptions = \yii\helpers\Json::encode($highcharts_config['options']);
        $setupOptions = \yii\helpers\Json::encode($highcharts_config['setupOptions']);

        $data = \backend\modules\ckdnet\classes\CkdnetFunc::queryAll("SELECT lm.`LABCODE`,ls.`display_type` "
                        . "FROM lab_map lm JOIN tdc_lab_items_site ls ON lm.tdc_lab_items_code = ls.tdc_lab_code "
                        . "WHERE lm.HOSPCODE = :hospcode AND lm.tdc_lab_items_code = :tdc_lab_items_code "
                        . "AND ls.display_type = '2'", [
                    ':hospcode' => \Yii::$app->session['dynamic_connection']['sitecode'],
                    ':tdc_lab_items_code' => $list_g[0]['tdc_lab_item_code']
        ]);
//            \appxq\sdii\utils\VarDumper::dump($data,0);
        $arrLab = "";
        foreach ($data as $value) {
            $arrLab .= $value['LABCODE'] . ", ";
        }
        if ($arrLab != "") {
            $arrLab = substr($arrLab, 0, strlen($arrLab) - 2);
//        \appxq\sdii\utils\VarDumper::dump($arrLab,0);
//        if ($data['display_type'] == '2') {

            $dataLabFu = \backend\modules\ckdnet\classes\CkdnetFunc::queryAll("SELECT DATE_SERV,LABRESULT FROM f_labfu "
                            . " WHERE (SITECODE = :sitecode OR HOSPCODE = :hospcode) AND LABCODE IN ({$arrLab}) AND (PID = :pid"
                            . " OR ptlink = :ptlink)", [
                        ':sitecode' => \Yii::$app->session['dynamic_connection']['sitecode'],
                        ':hospcode' => \Yii::$app->session['dynamic_connection']['sitecode'],
//                        ':labcode' => $data['LABCODE'],
                        ':pid' => $pid,
                        ':ptlink' => $ptlink
            ]);
            $dataProvider = new \yii\data\ArrayDataProvider([
                'allModels' => $dataLabFu
            ]);
        }


        return $this->renderAjax('_itemg', [
                    'jsOptions' => $jsOptions,
                    'setupOptions' => $setupOptions,
                    'id' => $id,
                    'dataProvider' => $dataProvider,
                    'list_g' => $list_g
        ]);
    }

//actionPopupGraph

    public function actionPopupGraphOne() {
        $id = \Yii::$app->request->get("id");
        $hospcode = \Yii::$app->request->get("hospcode");
        $ptlink = \Yii::$app->request->get("ptlink");
        $pid = \Yii::$app->request->get("pid");
        $graph = \Yii::$app->request->get("graph");
        $cid = \Yii::$app->request->get("cid");



        return $this->renderAjax('popupgraph', [
                    'hospcode' => $hospcode,
                    'ptlink' => $ptlink,
                    'pid' => Yii::$app->session['emr_pid'],
                    'graph' => $graph,
                    'cid' => $cid,
                    'id_graph' => $id
        ]);
    }

    public function actionCountgraph() {
        $get = \Yii::$app->request->get();
        $b = $get['b'];
        $e = $get['e'];
        $graph = $get["graph"];

        if ($graph == '99') {
            $group_id = "AND group_id != 000";
        } else {
            $group_id = " AND group_id = $graph";
        }

        $hospcode = \Yii::$app->session['dynamic_connection']['sitecode'];
        $sql = "SELECT * FROM lab_graph_list_site WHERE hospcode =:hospcode $group_id  ORDER BY forder ASC Limit :b,:e";
        $query = CkdnetFunc::queryAll($sql, [':hospcode' => $hospcode, ':b' => (int) $b, ':e' => (int) $e]);

        return \yii\helpers\Json::encode($query);
    }

    public function actionLoadgraph() {
        $get = \Yii::$app->request->get();
        $hospcodes = $get["hospcode"];
        $ptlink = $get["ptlink"];
        $pid = $get["pid"];
        $graph = $get["graph"];
        $cid = $get["cid"];


        if ($graph == '99') {
            $group_id = " AND group_id != 000";
        } else {
            $group_id = " AND group_id = $graph";
        }

        $begin = 0;
        $end = 10;
        $b = $get['b']; //Yii::$app->session['begin'] = $begin+4;  //=4  ,8,12,16
        $e = $get['e']; //Yii::$app->session['end'] = $end+4;     //=8   ,12,16,20 

        $hospcode = Yii::$app->session['dynamic_connection']['sitecode'];
        $sql = "SELECT * FROM lab_graph_list_site WHERE hospcode =:hospcode $group_id ORDER BY forder ASC Limit :b,:e";
        $query = CkdnetFunc::queryAll($sql, [':hospcode' => $hospcode, ':b' => (int) $b, ':e' => (int) $e]);

        return \yii\helpers\Json::encode($query);

//        $list_g = \backend\modules\ckdnet\classes\CkdnutQuery::getListGraph($graph,$number);
//        return $this->renderAjax("loadgraph",[
//            'list_g'=>$list_g
//        ]);
    }

    public function actionLoad_() {
        $get = Yii::$app->request->get();
        $hospcode = $get["hospcode"];
        $ptlink = $get["ptlink"];
        $pid = $get["pid"];
        $graph = $get["graph"];
        $cid = $get["cid"];
        $number = $get['number'];

        $list_g = \backend\modules\ckdnet\classes\CkdnutQuery::getListGraph($graph, $number);
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;


        return $list_g;
    }

//Load_

    public function actionMaxId() {
        $id = $_POST['id'];

        $model = \backend\modules\ckd\models\Labgraplists::find()->where(['id' => $id, 'hospcode' => Yii::$app->session['dynamic_connection']['sitecode']])->one();
        $arr = [];
        foreach ($model as $m) {
            $arr[] = $m;
        }
        echo json_encode($arr);
    }

    //
    public function actionNut() {
        $hospcode = '10980';
        $ptlink = 'e71d11c01aba43642ca9fa5537967d77';
        $sql = "SELECT  GROUP_CONCAT(LABCODE) as labcode, graph_label as labname , tdc_lab_items_code as labid,hos_lab_unit as labunit
                FROM lab_map 
                where hospcode = :hospcode AND (labcode<>'' and labcode is not null)
                GROUP BY tdc_lab_items_code
                ORDER BY hos_lab_items_name
		";
        //sbp,dbp,rr,pr,btemp,
        $sql = "SELECT SQL_NO_CACHE
f_service.dbp,
f_service.sbp,
f_service.date_serv
FROM
f_service
WHERE ((HOSPCODE='10980' AND SITECODE='10980' AND pid='100'  
AND (ptlink <>'' and ptlink is not null) ) OR ptlink='e71d11c01aba43642ca9fa5537967d77')
AND (dbp <>'' and dbp is not null) AND (sbp <>'' and sbp is not null) 
ORDER BY date_serv ASC
        ";
        $data = CkdnetFunc::queryAll($sql);

        // \appxq\sdii\utils\VarDumper::dump($data); ///
    }

    //
    public function actionNuts() {
        $arr = ['dbp', 'sbp', 'rr', 'BW', 'height'];

        $data_lab = $data_lab = $this->getserviceCode("e71d11c01aba43642ca9fa5537967d77", '10980', '100', 'pulse');


        //\appxq\sdii\utils\VarDumper::dump($data_lab);
//       $series = [];
//        if ($data_lab) {
//            foreach ($data_lab as $key => $value) {
//                
//                $data = CkdnetQuery::getserviceCode($ptlink, $hospcode, $pid, $value['labname']);//labcode
//                $dataLab = CkdnetFunc::getLabItemHighcharts($data);
//                if ($dataLab) {
//                    $series[] = ['name' => $value['labname'], 'tooltip' => ['valueSuffix' => ' ' . $value['labunit']], 'id' => $value['labid'], 'data' => new \yii\web\JsExpression($dataLab)];
//                } else {
//                    $series[] = ['name' => $value['labname'], 'tooltip' => ['valueSuffix' => ' ' . $value['labunit']], 'id' => $value['labid'], 'data' => []];
//                }
//            }
//        }
    }

    public function getserviceCode($ptlink, $hospcode, $pid, $gafcode = "") {
        //$gafcode="sbp";
        $sql = "select tdc_lab_items.tdc_lab_unit from tdc_lab_items where tdc_lab_items.graph_label = '" . $gafcode . "'";
        $query = \backend\modules\ckdnet\classes\CkdnetFunc::queryOne($sql);

        $tdc_lab_unit = $query['tdc_lab_unit'];
        // \appxq\sdii\utils\VarDumper::dump($tdc_lab_unit);

        $sql = "SELECT f_service.$gafcode as result, f_service.DATE_SERV as date_serv,'$tdc_lab_unit' as LABUNIT 
                from f_service 
                where ((ptlink=:ptlink AND ptlink<>'' AND ptlink is not null) 
                         OR (sitecode = :sitecode AND hospcode = :hospcode AND pid=:pid)) and ( f_service.$gafcode <> '' and f_service.$gafcode is not null)
                ORDER BY date_serv asc
		";

        return CkdnetFunc::queryAll($sql, [
                    ':ptlink' => $ptlink,
                    ':sitecode' => $hospcode,
                    ':hospcode' => $hospcode,
                    ':pid' => $pid]);
    }

    public function getForderMax() {
        $hospcode = \Yii::$app->session['dynamic_connection']['sitecode'];
        return \backend\modules\ckd\models\Labgraplists::find()->where(['hospcode' => $hospcode])->orderBy(['forder' => SORT_DESC])->one();
    }

    public function getForders($id) {
        $hospcode = Yii::$app->session['dynamic_connection']['sitecode'];
        $data = \backend\modules\ckd\models\Labgraplists::find()->where(['hospcode' => $hospcode, 'id' => $id])->orderBy(['forder' => SORT_DESC])->one();

        if (empty($data->forder)) {
            $data->forder = 0;
        }

        return $data->forder;
    }

    public function actionShorter() {
        $id = \Yii::$app->request->get('id'); //รหัสกราฟที่จะเรียงลำดับ
//        echo $id; exit();
        $maxid = $this->getForderMax()->id; //ค่ามากสุดของ lab_graph_list_site

        if (empty($group_id)) {
            $group_id = 1;
        }
        $group_id = \Yii::$app->request->get('group_id');
        \Yii::$app->session['group_id2'] = $group_id;
//        echo $group_id; exit();
        //$model = new \backend\modules\ckd\models\LabGrapList();

        if (!empty(\Yii::$app->request->post())) {
            $id_forder = \Yii::$app->request->post('forder'); //id ตำแหน่งที่จะให้ไปอยู่นะ 
            $ids = \Yii::$app->request->post('id'); //รหัสที่ต้องการ เรียงลำดับ

            $forder = $this->getForders($id_forder) + 5; //ตำแหน่งที่จะย้ายไปอยู่

            $sql_update = "UPDATE lab_graph_list_site SET forder=:fordre WHERE id=:id AND hospcode=:hospcode";
            $params_update = [
                ":fordre" => $forder,
                ":id" => $ids,
                ":hospcode" => \Yii::$app->session['dynamic_connection']['sitecode']
            ];
            $update = CkdnetFunc::execute($sql_update, $params_update);
//            
//             
//            $model = \backend\modules\ckd\models\Labgraplists::find()
//                ->where(['hospcode'=>\Yii::$app->session['dynamic_connection']['sitecode']])
//                ->orderBy(['forder' => SORT_ASC])->all();
            $this->getSorter();

            \Yii::$app->response->format = yii\web\Response::FORMAT_JSON;
            $result = [
                'status' => 'success',
                'action' => 'create',
                'message' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> success</strong> ' . Yii::t('app', ''),
            ];

            return $result;
            exit();
        }
        //$model = \backend\modules\ckd\models\LabGrapList::find()->where(['id'=>$id])->one();
        return $this->renderAjax("sorter", [
                    //'model'=>$model,
                    'id' => $id,
                    'group_id' => $group_id
        ]);
    }

    public function actionTest() {
        $this->getSorter();
    }

    public function getSorter() {

        $model = \backend\modules\ckd\models\Labgraplists::find()
                        ->where(['hospcode' => \Yii::$app->session['dynamic_connection']['sitecode']])
                        ->orderBy(['forder' => SORT_ASC])->all();

        $forder = 10;
        $status = 0;

        foreach ($model as $v) {
            $sql = "UPDATE lab_graph_list_site SET forder=:forder WHERE id=:id AND hospcode=:hospcode";
            $query = CkdnetFunc::execute($sql, [
                        ':forder' => $forder,
                        ':id' => $v->id,
                        ':hospcode' => \Yii::$app->session['dynamic_connection']['sitecode']
            ]);

            if ($query) {
                $status = 1;
            }
            $forder += 10;
        }
        return $status;
    }

    //

    public $activetab = "emr";

    public function actionHdAjax() {
        $ptlink = $_GET['ptlink'];
        $cid = $_GET['cid'];

        $hospcode = Yii::$app->user->identity->userProfile->sitecode;
        $ptlink = $ptlink == "" ? md5($cid) : $ptlink;
        if ($ptlink != '') {
            return $this->renderAjax('showhdajax', [
                        'ptlink' => $ptlink,
                        'cid' => $cid,
            ]);
        } else {
            return $this->renderAjax('patientnotfound');
        }
    }

    public function actionHdAjaxPtid() {
        $ptlink = $_GET['ptlink'];
        $cid = $_GET['cid'];
        $ptid = $_GET['ptid'];

        $hospcode = Yii::$app->user->identity->userProfile->sitecode;
        $ptlink = $ptlink == "" ? md5($cid) : $ptlink;
        if ($ptlink != '') {
            return $this->renderAjax('showhdajax', [
                        'ptlink' => $ptlink,
                        'cid' => $cid,
                        'ptid' => $ptid,
            ]);
        } else {
            return $this->renderAjax('patientnotfound');
        }
    }

    public function actionShowlab() {

        $group_id = isset($_GET['group_id']) ? $_GET['group_id'] : '';
        \Yii::$app->session['graph'] = $group_id;
        //\appxq\sdii\utils\VarDumper::dump($group_id);

        $cid = isset($_GET['cid']) ? $_GET['cid'] : '';
        $ptlink = isset($_GET['ptlink']) ? $_GET['ptlink'] : '';
        $tab = isset(\Yii::$app->session['emr_tab']) ? \Yii::$app->session['emr_tab'] : 'ckd';
        \Yii::$app->session['emr_tab'] = $tab;

        if ($cid != '' && is_numeric($cid) && \backend\modules\inv\classes\InvFunc::checkCid($cid)) {
            $ptlink = md5($cid);
        } else {
            $cid = '';
        }

        $hospcode = Yii::$app->user->identity->userProfile->sitecode;

        $returnArr = [
            'ptlink' => $ptlink,
            'cid' => $cid,
            'hospcode' => $hospcode,
            'tab' => $tab,
        ];

        if ($ptlink != '') {
            $patient = CkdnetQuery::getPatientUnhexAll($ptlink, $hospcode);

            Yii::$app->session['emr_pid'] = $patient['PID'];

            $returnArr = \yii\helpers\ArrayHelper::merge($returnArr, [
                        //'ptdata'=>$ptdata,
                        'patient' => $patient,
            ]);
        }

        return $this->renderAjax('showlab', [
                    'returnArr' => $returnArr,
                    'group_id' => $group_id
        ]);
    }

    /*     * ********************* ทำหน้าที่บันทึกข้อมูลเมนูกราฟเป็น เทมเพลต ************************** */

    public function actionSaveLabGraphGroupSite() {
        try {
            $lab_graph_group_site = CkdnetFunc::execute("
                set @i=0;
                insert ignore into lab_graph_group_site 
                SELECT
                lab_graph_group.id,
                '" . Yii::$app->session['dynamic_connection']['sitecode'] . "' hospcode,
                lab_graph_group.`group`,
                (@i:=ifnull(@i,0)+1)*10 forder
                FROM lab_graph_group   
            ");
        } catch (\yii\db\Exception $e) {
            echo $e->getMessage;
        }
    }

    public function actionSaveLabGraphListSiteTemplate() {

        //\appxq\sdii\utils\VarDumper::dump($_GET['data']);
        //$data = \Yii::$app->request->get("data");  
        $sinsert = "INSERT INTO lab_graph_list_site(hospcode,group_id,graph_name,tdc_lab_item_code,cols,forder)
      VALUES(:hospcode,:group_id,:graph_name,:tdc_lab_item_code,:cols,:forder)";
        $params = [
            ':hospcode' => \Yii::$app->session['dynamic_connection']['sitecode'],
            ':group_id' => $_POST['group_id'],
            ':graph_name' => $_POST['graph_name'],
            ':tdc_lab_item_code' => $_POST['tdc_lab_item_code'],
            ':cols' => $_POST['cols'],
            ':forder' => 1//$forders
        ];
        $query_insert = CkdnetFunc::execute($sinsert, $params);
    }

    public function actionSaveLabGraphListSite() {
        try {
            $sql = "SELECT * FROM `lab_graph_list_site` WHERE `hospcode`=:hospcode";
            $count_lab = CkdnetFunc::queryAll($sql, [
                        ':hospcode' => \Yii::$app->session['dynamic_connection']['sitecode']]);
            //\appxq\sdii\utils\VarDumper::dump($query2);
            if (empty($count_lab)) {
                $sql2 = "SELECT * FROM lab_graph_list";
                $query2 = CkdnetFunc::queryAll($sql2);
                //return \yii\helpers\Json::encode($query2);
                $forders = 10;
                foreach ($query2 as $q) {
                    //appxq\sdii\utils\VarDumper::dump($q);
                    $sinsert = "INSERT INTO lab_graph_list_site(hospcode,group_id,graph_name,tdc_lab_item_code,cols,forder)
                    VALUES(:hospcode,:group_id,:graph_name,:tdc_lab_item_code,:cols,:forder)";
                    //values('" .  . "','" .  . "','" .  . "','" .  . "','" .  . "','" .  . "')";
                    $params = [
                        ':hospcode' => \Yii::$app->session['dynamic_connection']['sitecode'],
                        ':group_id' => $q['group_id'],
                        ':graph_name' => $q['graph_name'],
                        ':tdc_lab_item_code' => $q['tdc_lab_item_code'],
                        ':cols' => $q['cols'],
                        ':forder' => $forders
                    ];
                    $query_insert = CkdnetFunc::execute($sinsert, $params);
                    $forders += 10;
                    if ($query_insert) {
                        echo 'success';
                    } else {
                        echo 'error';
                    }
                }
            }
        } catch (\yii\db\Exception $e) {
            echo $e->getMessage;
        }
    }

    //reset graph
    public function actionClearGraph() {
        $hospcode = \Yii::$app->request->get("hospcode", "");
        //echo $hospcode;
        $sql = "DELETE FROM lab_graph_list_site WHERE hospcode=:hospcode";
        $params = [":hospcode" => $hospcode];
        $delete = CkdnetFunc::execute($sql, $params);
        if ($delete) {
            echo 1;
        } else {
            echo 0;
        }
    }

    public function actionSetSession() {
        $name = \Yii::$app->request->get("names", "");
        $values = \Yii::$app->request->get("values", "");

        $session = \backend\modules\ckd\classes\CkdCheck::getSession($name, $values);
        return $session;
    }

}
