<?php

namespace backend\modules\ckd\controllers;

use Yii;
use yii\web\Controller;

class DefaultController extends Controller
{
    public $activetab="home";
    
    public function actionIndex()
    {
        $parampost = \Yii::$app->request->post();
        $sitecode = \Yii::$app->user->identity->userProfile->sitecode;
        $QueryTdc = \backend\modules\ckdnet\classes\CkdnetQuery::getTdcSync($sitecode);
        
        if($parampost['listType'] !=''){
            foreach ($parampost['listType'] as $val){
                if ($type == NULL) { $type =  $val; } else { $type .= ",".$val; }
            }
        }else{
           $type = isset(Yii::$app->session['type_ckd']) ?Yii::$app->session['type_ckd'] : "1,3";
        }
        
        if(empty(Yii::$app->session['type_ckd'])){
            Yii::$app->session['type_ckd'] = $type;
        }
        
        if(!empty(Yii::$app->session['type_ckd'])){
            if($type == ' ' ){
                Yii::$app->session['type_ckd'] = Yii::$app->session['type_ckd'];
            }else if($type != Yii::$app->session['type_ckd'] ){
                Yii::$app->session['type_ckd'] = $type;                   
            }
        }

        if($_GET['btnreset'] == 1){
            $_COOKIE['save_typeckd'] = false;
            setcookie('save_typeckd', null, time()-1, "/", Yii::$app->keyStorage->get('frontend.domain'), false);
            setcookie('save_modeckd', null, time()-1, "/", Yii::$app->keyStorage->get('frontend.domain'), false);
            Yii::$app->session['save_typeckd'] = 0;
            Yii::$app->session['type_ckd'] = "1,3";
            Yii::$app->session['mode_ckd'] = "central";
        }else{
             
            if(isset($_COOKIE['save_typeckd']) && $_COOKIE['save_typeckd']==1){
                if (isset($_COOKIE['save_typeckd'])) {
                    Yii::$app->session['save_typeckd'] = $_COOKIE['save_typeckd'];
                    if(Yii::$app->session['save_typeckd'] ==1){
                        $_COOKIE['type_ckd'] = Yii::$app->session['type_ckd'];
                        Yii::$app->session['type_ckd'] = $_COOKIE['type_ckd'];
                    }
                }
            }else{
                Yii::$app->session['save_typeckd'] = 0;
                setcookie('save_typeckd', null, time()-1, "/", Yii::$app->keyStorage->get('frontend.domain'), false);
            }

            if(isset($_POST['save_typeckd']) && $_POST['save_typeckd']==1){ 
                setcookie("save_typeckd", $_POST['save_typeckd'], time()+3600*24*30, "/", Yii::$app->keyStorage->get('frontend.domain'), false);
            }
        }

        $idcen = isset($_GET['idcen'])? $_GET['idcen']: Yii::$app->session['mode_ckd'];
        if(empty(Yii::$app->session['mode_ckd'])){
            Yii::$app->session['mode_ckd'] = $idcen;
        }
        
        if(!empty(Yii::$app->session['mode_ckd'])){
            if($idcen == ' ' ){
                Yii::$app->session['mode_ckd'] = Yii::$app->session['mode_ckd'];
            }else if($idcen != Yii::$app->session['mode_ckd'] ){
                Yii::$app->session['mode_ckd'] = $idcen; 
                
            }
        }
//        $_COOKIE['save_modeckd'] = Yii::$app->session['mode_ckd'];
        if(isset($_COOKIE['save_modeckd']) ? $_COOKIE['save_modeckd'] : Yii::$app->session['mode_ckd']){
            setcookie("save_modeckd", Yii::$app->session['mode_ckd'], time()+3600*24*30, "/", Yii::$app->keyStorage->get('frontend.domain'), false);
        }
    
        
        return $this->render('index',[
            'idcen'=>$idcen,
            'tdc' => $QueryTdc,
            'type'=>$type,
            'reset' =>$_GET['btnreset']
        ]);
    }
    
    public function actionRedirect($tab) {
        return $this->redirect('/ckd/'.$tab,302);
    }
    
    public function actionUpdateReport() 
    {
        $model = \common\models\KeyStorageItem::findOne(['key'=>'ckd.report_desc1']);
        
        if ($model->load(\Yii::$app->request->post())) {
            if ($model->save()) {
                return $this->render('index');
            }            
        }else{
            
            return $this->render('editreport',[
                'model' => $model
            ]);
        
        }
    }
}
