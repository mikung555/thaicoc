<?php

namespace backend\modules\ckd\controllers;
use app\models\FPerson;
use \app\models\FLabfu;
use Yii;
use yii\helpers\Json;
use backend\modules\ckdnet\classes\CkdnetQuery;
use backend\modules\ckdnet\classes\CkdnetFunc;

class EmrController extends \yii\web\Controller
{
    public $activetab="emr";
    
    public function actionIndex()
    {   
        $cid = isset($_GET['cid'])?$_GET['cid']:'';
        $ptlink = isset($_GET['ptlink'])?$_GET['ptlink']:'';
        $hospcode = isset($_GET['hospcode'])?$_GET['hospcode']:'';
        $pid = isset($_GET['pid'])?$_GET['pid']:'';
        $tab_idemr = isset($_GET['idemr'])?$_GET['idemr']:'ckd';
        $hospcode_cup = isset($_GET['hospcode_cup'])?$_GET['hospcode_cup']:'';
        
       // if($ptlink=='0106370bd05c8f1820fa01015f78fe88')$ptlink='3c5bdd0f78989455a4ec5fc7cc4f54a9';
       
        $returnArr = self::getPatientckd($cid,$pid,$ptlink,$hospcode);
        if($tab_idemr == 'ckd'){
            Yii::$app->session['emr_tab'] = $tab_idemr;
            $tab = Yii::$app->session['emr_tab'];
        }else{
            Yii::$app->session['emr_tab'] = $tab_idemr;
            $tab = Yii::$app->session['emr_tab'];
        }
        $returnArr = \yii\helpers\ArrayHelper::merge($returnArr, ['tab_idemr'=>$tab_idemr,'tab' => $tab ]);
        //\appxq\sdii\utils\VarDumper::dump($tab."::".Yii::$app->session['emr_tab']."//".$tab_idemr);
        return $this->render('index', $returnArr);
    }
    
    public function actionIndex2()
    {   
        $cid = isset($_GET['cid'])?$_GET['cid']:'';
        $ptlink = isset($_GET['ptlink'])?$_GET['ptlink']:'';
        $hospcode = isset($_GET['hospcode'])?$_GET['hospcode']:'';
        $pid = isset($_GET['pid'])?$_GET['pid']:'';
        $tab_idemr = isset($_GET['idemr'])?$_GET['idemr']:'ckd';
        $hospcode_cup = isset($_GET['hospcode_cup'])?$_GET['hospcode_cup']:'';
        
       // if($ptlink=='0106370bd05c8f1820fa01015f78fe88')$ptlink='3c5bdd0f78989455a4ec5fc7cc4f54a9';
       
        $returnArr = self::getPatientckd($cid,$pid,$ptlink,$hospcode);
        if($tab_idemr == 'ckd'){
            Yii::$app->session['emr_tab'] = $tab_idemr;
            $tab = Yii::$app->session['emr_tab'];
        }else{
            Yii::$app->session['emr_tab'] = $tab_idemr;
            $tab = Yii::$app->session['emr_tab'];
        }
        $this->layout="emr";
        $returnArr = \yii\helpers\ArrayHelper::merge($returnArr, ['tab_idemr'=>$tab_idemr,'tab' => $tab ]);
        //\appxq\sdii\utils\VarDumper::dump($tab."::".Yii::$app->session['emr_tab']."//".$tab_idemr);
        return $this->render('index', $returnArr);
    }
    
    public function getPatientckd($cid,$pid,$ptlink,$hospcode)
    {
        //if($tab_idemr != ''){
//            $tab = isset(Yii::$app->session['emr_tab'])?Yii::$app->session['emr_tab']:'ckd';
//            Yii::$app->session['emr_tab'] = $tab;
        

            if ($cid != ''&& is_numeric($cid) && \backend\modules\inv\classes\InvFunc::checkCid($cid)) {
                $ptlink = md5($cid);
            } else {
                $cid = '';
            }
            if($cid =='' && $ptlink=='' && $pid !='' &&  $hospcode != '' ){

                $ptlink = \backend\modules\ckd\classes\CkdPatient::getPtlink($pid, $hospcode)['ptlink'];
              
            }
            $hospcode = Yii::$app->user->identity->userProfile->sitecode;
            $sqlHospital = "SELECT `name` FROM all_hospital_thai WHERE hcode=$hospcode ";
            $nameHospital = Yii::$app->db->createCommand($sqlHospital)->queryScalar(); 
            
            ///Data Tab CCA 
            $sqlCca = "SELECT symptoms,symptoms_date,an,cca_code,cca_code_date From cca_diagram WHERE ptlink = :ptlink AND hospcode = :sitecode ";
            $querycca = \backend\modules\ccanet\classes\CcanetFunc::queryOne($sqlCca,[':ptlink'=> $ptlink,':sitecode'=> $hospcode]);
        

            $returnArr = [
                'ptlink' => $ptlink,
                'cid' => $cid,
                'hospcode' => $hospcode,
//                'tab' => $tab,
                'nameHospital' => $nameHospital,
                'querycca'=>$querycca
                   
            ];

            if ($ptlink != '') {
                $patient = CkdnetQuery::getPatientUnhexAll($ptlink, $hospcode);

                Yii::$app->session['emr_pid'] = $patient['PID'];

                $returnArr = \yii\helpers\ArrayHelper::merge($returnArr, [
                    //'ptdata'=>$ptdata,
                    'patient'=>$patient,

                ]);
            }
             //\appxq\sdii\utils\VarDumper::dump($returnArr."::".Yii::$app->session['emr_tab']." tab:: ".$tab." tabid::".$tab_idemr);
            return $returnArr;
    }
    
    public function actionCkd($ptlink,$hospcode)
    {
        $cid = isset($_GET['cid'])?$_GET['cid']:'';
        $tab = isset($_GET['tab'])?$_GET['tab']:'ckd';
        Yii::$app->session['emr_tab'] = $tab;
        
//        $tab_idemr = isset($_GET['tab_idemr'])?$_GET['tab_idemr']:$tab_idemr;
//        Yii::$app->session['emr_tab'] = $tab_idemr;
        
        //$patient = CkdnetQuery::getPatientUnhex($ptlink, $hospcode);
        $patient = CkdnetQuery::getPatientUnhexAll($ptlink, $hospcode);
        
        $sqlHospital = "SELECT `name` FROM all_hospital_thai WHERE hcode=$hospcode ";
        $nameHospital = Yii::$app->db->createCommand($sqlHospital)->queryScalar(); 
        
        return Json::encode($this->renderAjax('ckd',[
            'hospcode'=> $hospcode,
            'ptlink'=> $ptlink,
            'patient'=> $patient,
            'cid' => $cid,
            'tab_idemr' =>$tab_idemr,
            'nameHospital' => $nameHospital
            ]));
    }
    public function actionPhr($ptlink,$hospcode)
    {
        $tab = isset($_GET['tab'])?$_GET['tab']:'ckd';
        Yii::$app->session['emr_tab'] = $tab;
        
        $patient = CkdnetQuery::getPatient($ptlink, $hospcode);
        $cidhex = isset($patient['cid'])?$patient['cid']:'';
        
        return Json::encode($this->renderAjax('phr',[
            'hospcode'      => $hospcode,
            'ptlink'       =>$ptlink,
            'patient'       =>  $patient,
            'cidhex'    =>  $cidhex,
            ]));
    }   
    
    public function actionDiagAll() {
        $ptlink = isset($_GET['ptlink'])?$_GET['ptlink']:'';
        $hospcode = isset($_GET['hospcode'])?$_GET['hospcode']:'';
        
	if (Yii::$app->getRequest()->isAjax) {
            Yii::$app->session['emr_limit'] = 0;
            $data = CkdnetQuery::getServiceLimit($ptlink, $hospcode, Yii::$app->session['emr_limit']);
            Yii::$app->session['emr_limit'] = Yii::$app->session['emr_limit']+10;
            
            $drugallergy = CkdnetQuery::getDrugallergy($ptlink, $hospcode);
            Yii::$app->session['emr_drugallergy'] = $drugallergy;
            $sqlHospital = "SELECT `name` FROM all_hospital_thai WHERE hcode=$hospcode ";
            $nameHospital = Yii::$app->db->createCommand($sqlHospital)->queryScalar(); 
            return $this->renderAjax('_diag_all', [
                'data' => $data,
                'hospcode'      => $hospcode,
                'ptlink'       =>$ptlink,
                'drugallergy'  =>  $drugallergy,
                'nameHospital' => $nameHospital
            ]);
	} else {
            
            return \appxq\sdii\helpers\SDHtml::getMsgError() . Yii::t('app', 'Invalid request. Please do not repeat this request again.');
	}
    }
    
    public function actionDiagItem() {
        $ptlink = isset($_GET['ptlink'])?$_GET['ptlink']:'';
        $hospcode = isset($_GET['hospcode'])?$_GET['hospcode']:'';
        
	if (Yii::$app->getRequest()->isAjax) {
            $data = CkdnetQuery::getServiceLimit($ptlink, $hospcode, Yii::$app->session['emr_limit']);
            Yii::$app->session['emr_limit'] = Yii::$app->session['emr_limit']+10;
            $sqlHospital = "SELECT `name` FROM all_hospital_thai WHERE hcode=$hospcode ";
            $nameHospital = Yii::$app->db->createCommand($sqlHospital)->queryScalar(); 
    
            return $this->renderAjax('_diag_item', [
                'data' => $data,
                'hospcode'      => $hospcode,
                'ptlink'       =>$ptlink,
                'drugallergy'  =>  Yii::$app->session['emr_drugallergy'],
                'nameHospital' => $nameHospital
            ]);
	} else {
            
            return \appxq\sdii\helpers\SDHtml::getMsgError() . Yii::t('app', 'Invalid request. Please do not repeat this request again.');
	}
    }
    
    public function actionDrugAll() {
        $ptlink = isset($_GET['ptlink'])?$_GET['ptlink']:'';
        $hospcode = isset($_GET['hospcode'])?$_GET['hospcode']:'';
        
	if (Yii::$app->getRequest()->isAjax) {
            $data_drug_opd = CkdnetQuery::getDrugOpdAll($ptlink, $hospcode);
            $data_drug_ipd = CkdnetQuery::getDrugIpdAll($ptlink, $hospcode);
            $drug = \yii\helpers\ArrayHelper::merge($data_drug_opd, $data_drug_ipd);//เอาข้อมูลIPDกับOPDมารวมกัน
            $data = \appxq\sdii\utils\SortArray::sortingArrayDESC($drug, 'DATE_SERV');
//            \appxq\sdii\utils\VarDumper::dump($data);
            $dateData = [];
            $date = '';
            foreach ($data as  $value) {
                if($date != $value['DATE_SERV']){
                    $date = $value['DATE_SERV'];
                    $dateData[$value['DATE_SERV']] = [];
                    $dateData[$value['DATE_SERV']]['type'] = $value['AN'];
                    $dateData[$value['DATE_SERV']]['hosp'] = $value['HOSPCODE'];
                }
                array_push($dateData[$value['DATE_SERV']], $value['DNAME'].'  <code>'.$value['AMOUNT'].' '.$value['UNIT_PACKING'].'</code> ');
            }
//            \appxq\sdii\utils\VarDumper::dump($dateData);
            $sqlHospital = "SELECT `name` FROM all_hospital_thai WHERE hcode=$hospcode ";
            $nameHospital = Yii::$app->db->createCommand($sqlHospital)->queryScalar(); 
            return $this->renderAjax('_drug_all', [
                'dateData'=>$dateData,
                'nameHospital' =>$nameHospital,
                'hospcode'      => $hospcode,
                'ptlink'       =>$ptlink,
                
            ]);
	} else {
            
            return \appxq\sdii\helpers\SDHtml::getMsgError() . Yii::t('app', 'Invalid request. Please do not repeat this request again.');
	}
    }

    public function actionCvdRisk() { 
        $ptlink = isset($_GET['ptlink'])?$_GET['ptlink']:''; 
        $hospcode = isset($_GET['hospcode'])?$_GET['hospcode']:''; 
         
                
        if (Yii::$app->getRequest()->isAjax) { 
            
            
            //$data = CkdnetQuery::getServiceLimit($ptlink, $hospcode, Yii::$app->session['emr_limit']); 
            //  $cvdall = CkdnetQuery::getCvdall('e71d11c01aba43642ca9fa5537967d77', '10980'); 
            Yii::$app->session['cvd_limit'] = 0; 
            $cvdall = CkdnetQuery::getCvdall($ptlink, $hospcode, Yii::$app->session['cvd_limit']); 
            Yii::$app->session['cvd_limit'] = Yii::$app->session['cvd_limit']+10; 
            //  Yii::$app->session['emr_cvdall'] = $cvdall; 
            $sqlHospital = "SELECT `name` FROM all_hospital_thai WHERE hcode=$hospcode ";
            $nameHospital = Yii::$app->db->createCommand($sqlHospital)->queryScalar();   
            
                   return $this->renderAjax('_cvd_all', [ 
                       'data' => $data, 
                       'hospcode' => $hospcode, 
                       'ptlink' =>$ptlink, 
                       'cvdall' =>  $cvdall, 
                       'nameHospital' =>$nameHospital
                    ]); 
        } else { 
            return \appxq\sdii\helpers\SDHtml::getMsgError() . Yii::t('app', 'Invalid request. Please do not repeat this request again.'); 

        } 
   
    }
    
    public function actionCvdItem(){
        
        $ptlink = isset($_GET['ptlink'])?$_GET['ptlink']:''; 
        $hospcode = isset($_GET['hospcode'])?$_GET['hospcode']:''; 
         
                
        if (Yii::$app->getRequest()->isAjax) { 
            
            $cvdall = CkdnetQuery::getCvdall($ptlink, $hospcode, Yii::$app->session['cvd_limit']); 
            Yii::$app->session['cvd_limit'] = Yii::$app->session['cvd_limit']+10; 
            //  Yii::$app->session['emr_cvdall'] = $cvdall; 
            $sqlHospital = "SELECT `name` FROM all_hospital_thai WHERE hcode=$hospcode ";
            $nameHospital = Yii::$app->db->createCommand($sqlHospital)->queryScalar();    
                    return $this->renderAjax('_cvd_item', [ 
                       'data' => $data, 
                       'hospcode' => $hospcode, 
                       'ptlink' =>$ptlink, 
                       'cvdall' =>  $cvdall, 
                       'nameHospital' =>$nameHospital
                    ]); 
        } else { 
            return \appxq\sdii\helpers\SDHtml::getMsgError() . Yii::t('app', 'Invalid request. Please do not repeat this request again.'); 

        } 
   
    }
    
    public function actionCvaAll() { 
        $ptlink = isset($_GET['ptlink'])?$_GET['ptlink']:''; 
        $hospcode = isset($_GET['hospcode'])?$_GET['hospcode']:''; 
         
                
        if (Yii::$app->getRequest()->isAjax) { 
            
            
            //$data = CkdnetQuery::getServiceLimit($ptlink, $hospcode, Yii::$app->session['emr_limit']); 
            //  $cvdall = CkdnetQuery::getCvdall('e71d11c01aba43642ca9fa5537967d77', '10980'); 
            Yii::$app->session['cva_limit'] = 0; 
            $cva = CkdnetQuery::getCvaall($ptlink, $hospcode, Yii::$app->session['cva_limit']);
            $cvaall = \appxq\sdii\utils\SortArray::sortingArrayDESC($cva,'Date_Serv');
            Yii::$app->session['cva_limit'] = Yii::$app->session['cva_limit']+10; 
            $sqlHospital = "SELECT `name` FROM all_hospital_thai WHERE hcode=$hospcode ";
            $nameHospital = Yii::$app->db->createCommand($sqlHospital)->queryScalar(); 
            
                   return $this->renderAjax('_cva_all', [ 
                       'hospcode' => $hospcode, 
                       'ptlink' =>$ptlink, 
                       'cvaall' =>  $cvaall, 
                       'nameHospital' =>$nameHospital
                    ]); 
        } else { 
            return \appxq\sdii\helpers\SDHtml::getMsgError() . Yii::t('app', 'Invalid request. Please do not repeat this request again.'); 

        } 
   
    }
    public function actionCvaItem(){
        
        $ptlink = isset($_GET['ptlink'])?$_GET['ptlink']:''; 
        $hospcode = isset($_GET['hospcode'])?$_GET['hospcode']:''; 
         
                
        if (Yii::$app->getRequest()->isAjax) { 
            
            $cva = CkdnetQuery::getCvaall($ptlink, $hospcode, Yii::$app->session['cva_limit']); 
            $cvaall = \appxq\sdii\utils\SortArray::sortingArrayDESC($cva,'Date_Serv');
            Yii::$app->session['cva_limit'] = Yii::$app->session['cva_limit']+10; 
            $sqlHospital = "SELECT `name` FROM all_hospital_thai WHERE hcode=$hospcode ";
            $nameHospital = Yii::$app->db->createCommand($sqlHospital)->queryScalar(); 
              
                    return $this->renderAjax('_cva_item', [ 
                       'hospcode' => $hospcode, 
                       'ptlink' =>$ptlink, 
                       'cvaall' =>  $cvaall, 
                       'nameHospital' =>$nameHospital
                    ]); 
                  // \appxq\sdii\utils\VarDumper::dump($cvdall); 
        } else { 
            return \appxq\sdii\helpers\SDHtml::getMsgError() . Yii::t('app', 'Invalid request. Please do not repeat this request again.'); 

        } 
   
    }
    
    public function actionMiAll() { 
        $ptlink = isset($_GET['ptlink'])?$_GET['ptlink']:''; 
        $hospcode = isset($_GET['hospcode'])?$_GET['hospcode']:''; 
         
                
        if (Yii::$app->getRequest()->isAjax) { 

            //$data = CkdnetQuery::getServiceLimit($ptlink, $hospcode, Yii::$app->session['emr_limit']); 
            //  $cvdall = CkdnetQuery::getCvdall('e71d11c01aba43642ca9fa5537967d77', '10980'); 
            Yii::$app->session['mi_limit'] = 0; 
            $mi = CkdnetQuery::getMiall($ptlink, $hospcode, Yii::$app->session['mi_limit']); 
            $miall = \appxq\sdii\utils\SortArray::sortingArrayDESC($mi,'Date_Serv');
            Yii::$app->session['mi_limit'] = Yii::$app->session['mi_limit']+10; 
             $sqlHospital = "SELECT `name` FROM all_hospital_thai WHERE hcode=$hospcode ";
            $nameHospital = Yii::$app->db->createCommand($sqlHospital)->queryScalar();  
                   return $this->renderAjax('_mi_all', [ 
                       'hospcode' => $hospcode, 
                       'ptlink' =>$ptlink, 
                       'miall' =>  $miall, 
                       'nameHospital' =>$nameHospital
                    ]); 
        } else { 
            
            return \appxq\sdii\helpers\SDHtml::getMsgError() . Yii::t('app', 'Invalid request. Please do not repeat this request again.'); 

        } 
   
    }
    
    public function actionMiItem(){
        
        $ptlink = isset($_GET['ptlink'])?$_GET['ptlink']:''; 
        $hospcode = isset($_GET['hospcode'])?$_GET['hospcode']:''; 
         
                
        if (Yii::$app->getRequest()->isAjax) { 
            
            $mi = CkdnetQuery::getMiall($ptlink, $hospcode, Yii::$app->session['mi_limit']); 
            $miall = \appxq\sdii\utils\SortArray::sortingArrayDESC($mi,'Date_Serv');
            Yii::$app->session['mi_limit'] = Yii::$app->session['mi_limit']+10; 
            $sqlHospital = "SELECT `name` FROM all_hospital_thai WHERE hcode=$hospcode ";
            $nameHospital = Yii::$app->db->createCommand($sqlHospital)->queryScalar();        
                    return $this->renderAjax('_mi_item', [ 
                       'data' => $data, 
                       'hospcode' => $hospcode, 
                       'ptlink' =>$ptlink, 
                       'miall' =>  $miall, 
                       'nameHospital' =>$nameHospital
                    ]);
        } else { 
            return \appxq\sdii\helpers\SDHtml::getMsgError() . Yii::t('app', 'Invalid request. Please do not repeat this request again.'); 

        } 
   
    }
    
    public function actionIndex1(){
        
        $cid = isset($_GET['cid'])?$_GET['cid']:'';
        $ptlink = isset($_GET['ptlink'])?$_GET['ptlink']:'';
        $hospcode = isset($_GET['hospcode'])?$_GET['hospcode']:'';
        $pid = isset($_GET['pid'])?$_GET['pid']:'';
        $tab_idemr = isset($_GET['idemr'])?$_GET['idemr']:'ckd';
        $querycca = isset($_GET['querycca'])?$_GET['querycca']:'';
       // if($ptlink=='0106370bd05c8f1820fa01015f78fe88')$ptlink='3c5bdd0f78989455a4ec5fc7cc4f54a9';
        if($tab_idemr == 'ckd'){
            Yii::$app->session['emr_tab'] = $tab_idemr;
            $tab = Yii::$app->session['emr_tab'];
        }else{
            Yii::$app->session['emr_tab'] = $tab_idemr;
            $tab = Yii::$app->session['emr_tab'];
        }
        //\appxq\sdii\utils\VarDumper::dump($querycca);
        $returnArr = self::getPatientckd($cid,$pid,$ptlink,$hospcode);
        $returnArr = \yii\helpers\ArrayHelper::merge($returnArr, ['tab_idemr'=>$tab_idemr,'tab' => $tab ]);
        //\appxq\sdii\utils\VarDumper::dump($returnArr);
        return $this->renderAjax('index1',$returnArr);
    }
    
    public function actionCkdvisit(){
        
       
        $hospcode = isset($_POST['hospcode'])?$_POST['hospcode']:'';
        $hospcode_cup = isset($_POST['hospcode_cup'])?$_POST['hospcode_cup']:'';
        $patient = isset($_POST['patient'])?$_POST['patient']:'';
        $ptlink = isset($_POST['ptlink'])?$_POST['ptlink']:'';
        $cid = isset($_POST['cid'])?$_POST['cid']:'';
        $nameHospital = isset($_POST['nameHospital'])?$_POST['nameHospital']:'';
        $one_serv = isset($_POST['one_serv'])?$_POST['one_serv']:'';
        $one_serv_data = isset($_POST['one_serv_data'])?$_POST['one_serv_data']:'';
        $tab_idemr = isset($_POST['tab_idemr'])?$_POST['tab_idemr']:'';
        $tab = isset($_POST['tab'])?$_GET['tab']:'';
        $querycca = isset($_POST['querycca'])?$_POST['querycca']:'';
        
       
        if($hospcode_cup != ''){
            $check_cup = CkdnetQuery::getHospitalCup($hospcode, $hospcode_cup);
        }
          
        $returnArr = self::getPatientckd($cid,$pid,$ptlink,$hospcode);
        $returnArr = array_merge($returnArr,[
            'tab' => $tab,
            'one_serv'=>$one_serv,
            'one_serv_data'=>$one_serv_data,
            'tab_idemr'=>$tab_idemr,
        ]);
        //\appxq\sdii\utils\VarDumper::dump($returnArr);
//        $returnArr = [
//            'ptlink' => $ptlink,
//            'cid' => $cid,
//            'hospcode' => $hospcode,
//            'tab' => $tab,
//            'patient'=>$patient,
//            'nameHospital' =>$nameHospital,
//            'one_serv'=>$one_serv,
//            'one_serv_data'=>$one_serv_data,
//            'tab_idemr'=>$tab_idemr,
//            'querycca'=>$querycca
//        ];
        
        if($check_cup['check_cup'] == '0'){
            return $this->renderAjax('_alert_cup',$returnArr);
        }else{
            return $this->renderAjax('ckd',$returnArr);
        }

    }
    
    
    
}
