<?php

namespace backend\modules\ckd\classes;

use backend\modules\ckdnet\classes\CkdnetFunc;

class Ckdgraph {

    public static function ShowLabGraphGroupSite($hospcode) {
        $sql = "SELECT * FROM lab_graph_group_site";
        return CkdnetFunc::queryAll($sql);
    }

//แสดงข้อมูล เมื่อมี hpcode 

    public static function AddLabGraphGroupList($hospcode) {
        $query = \backend\modules\ckdnet\classes\CkdnetQuery::getGraph();
        $forder = 10;
        foreach ($query as $key => $value) {
            $sql = "INSERT INTO lab_graph_group_site VALUES(null, '" . $hospcode . "' ,'" . $value['group'] . "', '" . $forder . "')";
            CkdnetFunc::execute($sql);
            $forder += 10;
        }
    }

//เพิ่มข้อมูลเมื่อไม่มีกราฟเริ่มต้น table lab_graph_group_site

    public static function Showmaxforder($hospcod) {
        $sql = "SELECT * FROM lab_graph_group_site";
        $result = \backend\modules\ckd\models\Graph::find()->where(['hospcode' => $hospcod])->orderBy(['forder' => SORT_DESC])->one();
        return $result->forder;
    }

//เพิ่มข้อมูลมีพารามิเตอร์ด้วยนะ table lab_graph_group_site

    public static function AddLab_Graph_List_Site($hospcode) {
        $sql = "SELECT * FROM lab_graph_list";
        $data = CkdnetFunc::queryAll($sql);

        foreach ($data as $key => $value) {
            $sql = "INSERT INTO lab_graph_list_site VALUES(null, '" . $hospcode . "' ,'" . $value['group_id'] . "', '" . $value['graph_name'] . "','" . $value['tdc_lab_item_code'] . "')";
            CkdnetFunc::execute($sql);
        }
    }

//เพิ่อมข้อมูลเมื่อไม่มีการใน table lab_graph_list_site

    public static function CheckLabGrapGroupSite($hospcode) {
        $sql = "SELECT * FROM lab_graph_group_site";
        $c = CkdnetFunc::queryAll($sql);
        $status = 0;
        if (!empty($c)) {
            $status = 1;
        } else {
            $status = 0;
        }

        return $status;
    }

//ตรวจสอบว่ามี ข้อมูลหรือเปล่า
    
    
}
