<?php

 

namespace backend\modules\ckd\classes;

use Yii;
use kartik\base\Widget;
use yii\helpers\Json;
use yii\web\View;
 
 
class NutDialog extends \kartik\dialog\Dialog
{
   public function initOptions()
    {
        $ok = Yii::t('kvdialog', 'Ok');
        $cancel = Yii::t('kvdialog', 'Cancel');
        $info = Yii::t('kvdialog', 'Information');
        $okLabel = '<span class="' . self::ICON_OK . '"></span> ' . $ok;
        $cancelLabel = '<span class="' . self::ICON_CANCEL . '"></span> ' . $cancel;
        $promptDialog = $otherDialog = [
            'draggable' => false,
            'title' => $info,
            'buttons' => [
                ['label' => $cancel, 'icon' => self::ICON_CANCEL],
                ['label' => $ok, 'icon' => self::ICON_OK, 'class' => 'btn-primary'],
            ]
        ];
        $otherDialog['draggable'] = true;
        $promptDialog['closable'] = false;
        $this->dialogDefaults = array_replace_recursive([
            self::DIALOG_ALERT => [
                'type' => self::TYPE_INFO,
                'title' => $info,
                'buttonLabel' => $okLabel
            ],
            self::DIALOG_CONFIRM => [
                'type' => self::TYPE_PRIMARY,
                'title' => Yii::t('kvdialog', 'Confirmation'),
                'btnOKClass' => 'btn-primary',
                'btnOKLabel' => $okLabel,
                'btnCancelLabel' => $cancelLabel
            ],
            self::DIALOG_PROMPT => $promptDialog,
            self::DIALOG_OTHER => $otherDialog
        ], $this->dialogDefaults);
    }
}
