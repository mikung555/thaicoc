<?php
namespace backend\modules\ckd\classes;
use yii\web\Response;

use Yii;
 
class ConfirmCheck {
    
    public static function getCid($table,$hospcode,$pid){

        $sql="SELECT * FROM ".$table." WHERE hospcode=:hospcode AND pid = :pid order by id DESC";
        $params=[
          ':hospcode' => $hospcode,
          ':pid' => $pid
        ];
        $query = \Yii::$app->db->createCommand($sql,$params);
        
        return $query;
    }//
    
}
