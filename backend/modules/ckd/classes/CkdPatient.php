<?php
namespace backend\modules\ckd\classes;
use backend\modules\ckdnet\classes\CkdnetFunc;
use yii\web\Response;

use Yii;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace backend\modules\ckd\classes;

/**
 * Description of CkdPatient
 *
 * @author engiball
 */
class CkdPatient {
    //put your code here
       public static function getPtlink($pid, $hospcode) {
	$sql = "SELECT ptlink from f_person where hospcode = :hospcode AND pid=:pid AND sitecode = :hospcode
		";
	return \backend\modules\ckdnet\classes\CkdnetFunc::queryOne ($sql, [':hospcode'=>$hospcode, ':pid'=>$pid]);
    }
}
