<?php

 

namespace backend\modules\ckd\models;
use yii;
 
class Graph extends \yii\db\ActiveRecord{
    //public  $group;
    
    //public $group_id, $graph_name, $tdc_lab_item_code; 
    public $numbers; 
    public static function tableName(){
        return "lab_graph_group_site";
    }
     public static function getDb() {
        return \backend\modules\ckdnet\classes\CkdnetFunc::getDb();
    }
    public function rules() {
        return [
          [['group'],'required','message'=>'กรุณาระบุ']  
        ];
    }
    public function attributeLabels() {
        return[
            'group'=>'เมนูกราฟ'
        ];
    }
}
