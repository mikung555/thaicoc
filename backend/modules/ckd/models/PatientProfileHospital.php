<?php

namespace backend\modules\ckd\models;

use Yii;

/**
 * This is the model class for table "patient_profile_hospital".
 *
 * @property string $hospcode
 * @property string $pid
 * @property string $ptlink
 * @property integer $death
 * @property string $death_date
 * @property string $regdate
 * @property integer $dm
 * @property string $dm_date
 * @property integer $ht
 * @property string $ht_date
 * @property integer $target
 * @property integer $screened
 * @property string $screened_date
 * @property string $screened_year
 * @property integer $cf_lab
 * @property string $cf_lab_date
 * @property integer $cf_doctor
 * @property string $cf_doctor_code
 * @property string $cf_doctor_date
 * @property integer $cf_icd
 * @property string $cf_icd_code
 * @property string $cf_icd_date
 * @property string $egfr
 * @property string $egfr_date
 * @property string $cvd_risk
 * @property string $cvd_risk_date
 * @property integer $reported_ckd
 * @property string $reported_ckd_date
 * @property integer $ckd_final
 * @property string $ckd_final_code
 * @property string $ckd_final_date
 */
class PatientProfileHospital extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'patient_profile_hospital';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['hospcode', 'pid', 'cf_doctor_code', 'cf_icd_code', 'ckd_final_code'], 'required'],
            [['death', 'dm', 'ht', 'target', 'screened', 'cf_lab', 'cf_doctor', 'cf_icd', 'reported_ckd', 'ckd_final'], 'integer'],
            [['death_date', 'regdate', 'dm_date', 'ht_date', 'screened_date', 'cf_lab_date', 'cf_doctor_date', 'cf_icd_date', 'egfr_date', 'cvd_risk_date', 'reported_ckd_date', 'ckd_final_date'], 'safe'],
            [['hospcode', 'egfr', 'cvd_risk'], 'string', 'max' => 10],
            [['pid'], 'string', 'max' => 20],
            [['ptlink'], 'string', 'max' => 150],
            [['screened_year'], 'string', 'max' => 250],
            [['cf_doctor_code', 'cf_icd_code', 'ckd_final_code'], 'string', 'max' => 5]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'hospcode' => 'Hospcode',
            'pid' => 'Pid',
            'ptlink' => 'Ptlink',
            'death' => 'Death',
            'death_date' => 'Death Date',
            'regdate' => 'Regdate',
            'dm' => 'Dm',
            'dm_date' => 'Dm Date',
            'ht' => 'Ht',
            'ht_date' => 'Ht Date',
            'target' => 'Target',
            'screened' => 'Screened',
            'screened_date' => 'Screened Date',
            'screened_year' => 'Screened Year',
            'cf_lab' => 'Cf Lab',
            'cf_lab_date' => 'Cf Lab Date',
            'cf_doctor' => 'Cf Doctor',
            'cf_doctor_code' => 'Cf Doctor Code',
            'cf_doctor_date' => 'Cf Doctor Date',
            'cf_icd' => 'Cf Icd',
            'cf_icd_code' => 'Cf Icd Code',
            'cf_icd_date' => 'Cf Icd Date',
            'egfr' => 'Egfr',
            'egfr_date' => 'Egfr Date',
            'cvd_risk' => 'Cvd Risk',
            'cvd_risk_date' => 'Cvd Risk Date',
            'reported_ckd' => 'Reported Ckd',
            'reported_ckd_date' => 'Reported Ckd Date',
            'ckd_final' => 'Ckd Final',
            'ckd_final_code' => 'Ckd Final Code',
            'ckd_final_date' => 'Ckd Final Date',
        ];
    }
}
