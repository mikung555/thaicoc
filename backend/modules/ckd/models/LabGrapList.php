<?php

use yii;
namespace backend\modules\ckd\models;
 

class LabGrapList extends \yii\db\ActiveRecord{
 
    public static function tableName(){
        return "lab_graph_list";
    }
     public static function getDb() {
       return \backend\modules\ckdnet\classes\CkdnetFunc::getDb();         
    }
    public function rules() {
        //group_id
        return [
          [['graph_name','tdc_lab_item_code'],'required','message'=>'กรุณาระบุ']  
        ];
    }
    public function attributeLabels() {
        return[
            'graph_name'=>'ชื่อกราฟ',
            'tdc_lab_item_code'=>'ข้อมูลกราฟ'
        ];
    }
}
