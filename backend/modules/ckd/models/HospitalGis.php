<?php

namespace backend\modules\ckd\models;

use Yii;

/**
 * This is the model class for table "hospital_gis".
 *
 * @property string $hcode
 * @property string $name
 * @property string $lat
 * @property string $lng
 */
class HospitalGis extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'hospital_gis';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['hcode'], 'required'],
            [['hcode'], 'string', 'max' => 10],
            [['name', 'lat', 'lng'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'hcode' => 'Hcode',
            'name' => 'Name',
            'lat' => 'Lat',
            'lng' => 'Lng',
        ];
    }
}
