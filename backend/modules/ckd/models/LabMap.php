<?php

namespace backend\modules\ckd\models;

use Yii;

class LabMap extends \yii\db\ActiveRecord {

    public $items;
    public $map_items;
    public $new_items;

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'lab_map';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb() {
        return \backend\modules\ckdnet\classes\CkdnetFunc::getDb();
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
                [['HOSPCODE', 'LABCODE', 'hos_lab_items_name', 'graph_label', 'hos_lab_unit', 'tdc_lab_items_code','map_items','new_items'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'HOSPCODE' => 'HOSPCODE',
            'LABCODE' => 'LABCODE',
            'hos_lab_items_name' => 'hos_lab_items_name',
            'graph_label' => 'graph_label',
            'hos_lab_unit' => 'hos_lab_unit',
            'tdc_lab_items_code' => 'tdc_lab_items_code',
            'items' => 'Items',
            'map_items'
        ];
    }

}
