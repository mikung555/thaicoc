<?php

namespace backend\modules\ckd\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\ckd\models\HelperGis;

/**
 * HelperGisSearch represents the model behind the search form about `backend\modules\ckd\models\HelperGis`.
 */
class HelperGisSearch extends HelperGis
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'hn'], 'integer'],
            [['title', 'patient_name', 'patient_id', 'volunteer', 'volunteer_id', 'address', 'tel'], 'safe'],
            [['lat', 'lng'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = HelperGis::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'hn' => $this->hn,
            'lat' => $this->lat,
            'lng' => $this->lng,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'patient_name', $this->patient_name])
            ->andFilterWhere(['like', 'patient_id', $this->patient_id])
            ->andFilterWhere(['like', 'volunteer', $this->volunteer])
            ->andFilterWhere(['like', 'volunteer_id', $this->volunteer_id])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'tel', $this->tel]);

        return $dataProvider;
    }
}
