<?php

namespace backend\modules\ckd\models;

use Yii;

/**
 * This is the model class for table "helper_gis".
 *
 * @property string $title
 * @property string $patient_name
 * @property integer $hn
 * @property string $patient_id
 * @property string $volunteer
 * @property string $volunteer_id
 * @property string $address
 * @property double $lat
 * @property double $lng
 * @property string $tel
 */
class HelperGis extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'helper_gis';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'patient_name', 'hn', 'patient_id', 'volunteer', 'volunteer_id', 'address', 'lat', 'lng', 'tel'], 'required'],
            [['hn'], 'integer'],
            [['lat', 'lng'], 'number'],
            [['title', 'patient_name', 'patient_id', 'volunteer', 'volunteer_id', 'address', 'tel'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'title' => 'คำนำหน้า',
            'patient_name' => 'ผู้ป่วย',
            'hn' => 'HN',
            'patient_id' => 'เลขบัตร ปชช ผู้ป่วย',
            'volunteer' => 'อาสาสมัคร',
            'volunteer_id' => 'เลขบัตร ปชช อาสาสมัคร',
            'address' => 'ที่อยู่',
            'lat' => 'ละติจูด',
            'lng' => 'ลองจิจูด',
            'tel' => 'เบอร์โทร',
        ];
    }
}
