<?php

 

namespace backend\modules\ckd\models;
use Yii;
 
class  Labgraplists extends \yii\db\ActiveRecord{
    public $select_graph;
    public $widths,$cstatus,$cid;
    public static function tableName(){
        return "lab_graph_list_site";
    }
     public static function getDb() {
       return \backend\modules\ckdnet\classes\CkdnetFunc::getDb();         
    }
    public function rules() {
        //group_id
        return [
          //[['select_graph'],'required','message'=>'กรุณาเลือกกราฟ'],
          [['graph_name'],'required','message'=>'กรุณาระบุ'],  
          [['tdc_lab_item_code'],'required','message'=>'กรุณาเลือกข้อมูลกราฟ']
        ];
    }
    public function attributeLabels() {
        return[
            'graph_name'=>'ชื่อกราฟ',
            'tdc_lab_item_code'=>'ข้อมูลกราฟ',
            'select_graph'=>'เลือกกราฟ'
        ];
    }
    public function getGraphWidth(){
        return[
          '6'=>'ครึ่งความกว้าง',
          '12'=>'เต็มความกว้าง'
        ];
    }
}
