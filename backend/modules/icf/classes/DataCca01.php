<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace backend\modules\icf\classes;
use Yii;

/**
 * Description of DataCca01
 *
 * @author chaiwat
 */
class DataCca01 {
    //put your code here
    public static function getListByPtid($ptid){
        $sql    = 'select id,ptid,target,rstat,hsitecode,hptcode,sitecode,ptcode,f1vdcomp from tb_data_2 where ptid=:ptid ';
        $dset   = Yii::$app->db->createCommand($sql,[':ptid'=>$ptid])->queryAll();
        return $dset;
    }
    
    public static function getCheckCCA01($ptid){
        $list = self::getListByPtid($ptid);
        $out[f1vdcomp]='';
        $out[rstat]='';
        if( count($list)>0 ){
            foreach( $list as $kvalue ){
                if($kvalue['f1vdcomp']>0 && $kvalue['rstat']=='2'){
                    $out[f1vdcomp]=$kvalue['f1vdcomp'];
                    $out[rstat]=$kvalue['rstat'];
                }
            if($kvalue['f1vdcomp']>0 && $kvalue['rstat']=='1' && (strlen($out[rstat])==0 || $out[rstat]=='0') ){
                    $out[f1vdcomp]=$kvalue['f1vdcomp'];
                    $out[rstat]=$kvalue['rstat'];
                }
            }
        }
        return $out;
    }
    
}
