<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace backend\modules\icf\classes;

use Yii;
use backend\modules\icf\controllers\CheckIcfController;

/**
 * Description of DataRegister
 *
 * @author chaiwat
 */
class DataRegister {
    //put your code here
    public static function getListByPtid($ptid){
        $sql    = 'select id,ptid,target,rstat,hsitecode,hptcode,sitecode,ptcode,title,name,surname,cid,hncode from tb_data_1 where ptid=:ptid ';
        $sql.=  'order by create_date ';
        $dset   = Yii::$app->db->createCommand($sql,[':ptid'=>$ptid])->queryAll();
        return $dset;
    }
    
    public static function getHospitalRegistered($reg){
        if( count($reg)>0 ){
            $sql = '';
            foreach($reg as $kvalue){
                if( strlen($kvalue['hsitecode'])>0 ){
                    if( strlen($sql)==0 ){
                        $sql = 'hcode="'.$kvalue['hsitecode'].'" ';
                    }else{
                        $sql.= 'or hcode="'.$kvalue['hsitecode'].'" ';
                    }
                }
            }
            if( strlen($sql)>0 ){
                $sql ='select hcode,name,zone_code,province,amphur from all_hospital_thai where ('.$sql.') ';
                $dset   = Yii::$app->db->createCommand($sql)->queryAll();
                if( count($dset)>0 ){
                    foreach( $dset as $kvalue ){
                        $out[$kvalue['hcode']]=$kvalue;
                    }
                }
        
            }
        }
        return $out;
    }
    
    public static function getCheckICF($ptid){
        $out[icf1]='0';
        $out[icf0]='0';
        $out[icfn]='1';
        $out[icft]=0;
        $out[icfptid]=$ptid;
        $out[txt]='<span class="glyphicon glyphicon-exclamation-sign" style="color: red"></span>';
        if(strlen($ptid)>0){
            // Check on verify from team check
            $sql = 'select id,hsitecode,confirm,error,rstat,icf_upload1 ';
            $sql.= ',case 1 when dlastcheck is null then "0" ';
            $sql.= 'when dlastcheck>update_date then "3" ';
            $sql.= 'when dlastcheck<update_date then "2" ';
            $sql.= 'else "0" end as confirmcheckdate ';
            $sql.= 'from tb_data_1 where ptid=:ptid ';
            $out[icfsql] = Yii::$app->db->createCommand($sql,['ptid'=>$ptid])->rawSql;
            $icf   = Yii::$app->db->createCommand($sql,['ptid'=>$ptid])->queryAll();
            if( count($icf)>0 ){
                foreach( $icf as $kvalue ){
                    if($kvalue['confirm']>0){
                        $out[icf1]='1';
                        $out[icfn]='0';
                        $out[txt]='<span class="glyphicon glyphicon-ok" style="color: green"></span>';
                        $data_original_title='ตรวจผ่านแล้ว';
                        $error = '';
                    }
                    if($kvalue['confirm']=='0' && $kvalue['rstat']=='1'){
                        $out[icf0]='1';
                        $out[icfn]='0';
                        $out[txt]='<span class="glyphicon glyphicon-edit" style="color: oragne"></span>';
                        $data_original_title='แก้ไขมาแล้ว รอการตรวจสอบอีกครั้ง';
                        $error = '';
                        $out[icf1]='6';
                    }else if($kvalue['confirm']=='0'){
                        $out[icf0]='1';
                        $out[icfn]='0';
                        $out[txt]='<span class="glyphicon glyphicon-remove" style="color: red"></span>';
                        $data_original_title='ตรวจใบยินยอมแล้ว แต่ไม่ผ่าน !ผลจากการตรวจ:';
                        $error = '';
                    }
                    if( strlen($kvalue[icf_upload1])==0 && $out[icf1]!='1' ){
                        $out[txt] = '<span class="glyphicon glyphicon-exclamation-sign" style="color: red"></span>';
                        $data_original_title = 'ยังไม่อัพโหลดใบยินยอม หรือฟอร์มมี Error กรุณา Upload ใบยินยอมฯ   ';
                        $error = 'ยังไม่อัพโหลดใบยินยอม หรือฟอร์มมี Error กรุณา Upload ใบยินยอมฯ   ';
                        $out[icf1]='4';
                    }else if(strlen($kvalue[error])>0 && $out[icf1]!='1' && $out[icf1]!='1' ){
                        $error = strip_tags($kvalue[error]);
                        $out[icf1]='4';
                    }else if( $kvalue[confirm]=='0' && $kvalue[confirmcheckdate]=='2' && $out[icf1]!='1' ){
                        // 6: แก้ไข มาแล้ว หลังจากตรวจไปแล้ว
                        $out[icf1]='6';
                    }else if( (strlen($kvalue[confirm])==0 && $kvalue[confirmcheckdate]=='0') && $out[icf1]!='1' ){
                        // 2:/ ยังไม่ตรวจ Submit มาแล้ว (ครั้งที่ 1)
                        $out[icf1]='2';    
                    }else if( $kvalue[rstat]=='2' && $kvalue[confirmcheckdate]=='2' && $out[icf1]!='1' ){
                        // 2:/ ยังไม่ตรวจ Submit มาแล้ว (ครั้งที่ 2 ขึ้นไป)
                        $out[icf1]='2';
                    }else if( $kvalue[confirmcheckdate]=='3' && $out[icf1]!='1' ){
                        // 3: ตรวจแล้วไม่ผ่าน
                        $out[icf1]='3';
                    }else if( $kvalue[rstat]=='1' && $out[icf1]!='1'){
                        // <a href="/inputdata/redirect-page?dataid=1492498860016797600&amp;ezf_id=1437377239070461301&amp;rurl=L2ljZi90b3RhbA%3D%3D" data-toggle="tooltip" data-original-title="สถานะ save draft ยังไม่ submit" data-pjax="0" target="_blank"><span class="glyphicon glyphicon-edit" style="color: oragne"></span></a>
                        $out[txt]='<span class="glyphicon glyphicon-edit" style="color: oragne"></span>';
                        $data_original_title='สถานะ save draft ยังไม่ submit';
                        $out[icf1]='5';
                    }
                    if(strlen($error)==0){
                        $error = strip_tags($kvalue[error]);
                    }
                    if($kvalue[hsitecode]==$sitecode){
                        $dataid=$kvalue[id];
                    }
                }
            }
            /*
                * 0:/ ยังไม่ตรวจ
                * 1: ตรวจแล้วผ่าน
                * 2:/ ยังไม่ตรวจ Submit มาแล้ว
                * 3:/ ตรวจแล้วไม่ผ่าน
                * 4: มี Error หรือ ยังไม่ Upload (*)
                * 5: ยังไม่ Submit ข้อมูลสมบูรณ์แล้ว
                * 6: แก้ไข มาแล้ว หลังจากตรวจไปแล้ว
             */
            if($out[icf1]=='1'){
                // 1: ตรวจแล้วผ่าน
            }else if( $out[icf1]=='5' ){
                // 5: ยังไม่ Submit ข้อมูลสมบูรณ์แล้ว
            }else{
                //
            }
            $out[icft] = CheckIcfController::checkIcfTimeChecking($ptid);
            
        }
        if( strlen($error)>0 ){
            $data_original_title = $error;
        }
        if( $out[icft]==2 ){
            $time1error = 'ตรวจใบยินยอมแล้ว แต่ไม่ผ่าน !ผลจากการตรวจ:';
            $out[txtlink]='<a href="/inputdata/redirect-page?dataid='.$dataid.'&ezf_id=1437377239070461301" data-toggle="tooltip" data-original-title="'.$time1error.'" data-pjax="0" target="_blank">';
            $out[txtlink]=$out[txtlink].'<span class="glyphicon glyphicon-remove" style="color: red"></span>'.'</a>';
            $out[txtlink]=$out[txtlink].'<a href="/inputdata/redirect-page?dataid='.$dataid.'&ezf_id=1437377239070461301" data-toggle="tooltip" data-original-title="'.$data_original_title.'" data-pjax="0" target="_blank">';
            $out[txtlink]=$out[txtlink].$out[txt].'</a>';
        }else{
            $out[txtlink]='<a href="/inputdata/redirect-page?dataid='.$dataid.'&ezf_id=1437377239070461301" data-toggle="tooltip" data-original-title="'.$data_original_title.'" data-pjax="0" target="_blank">';
            $out[txtlink]=$out[txtlink].$out[txt].'</a>';
        }
        
        /*
         * $out[icf1]='1'  => ตรวจสอบแล้วผ่าน
         * $out[icf0]='1'  => เคยรวจสอบแล้ว และไม่ผ่าน
         * $out[icfn]='1'  => ยังไม่เคยตรวจสอบเลย
         */
        return $out;
    }
    
}
