<?php
namespace backend\modules\icf\classes;

use Yii;
use yii\helpers\Html;

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class KeySummary {
    
    public static function menuICFAll() {
	//$txtMenu[];
       
        $txtMenu[0]=  Html::a('Register', ['/icf/reg'], ['class'=>'btn btn-info', 'role'=>'button']);
	return $txtMenu;
    }
    
    public static function getDatefromYearPeriod($year){
        if( strlen($year)== 0 ){
            $year='59';
        }
        if($year=='57'){
            $startDate='2013-10-01';
            $endDate='2014-09-30';
        }else if($year=='58'){
            $startDate='2014-10-01';
            $endDate='2015-09-30';
        }else if($year=='59'){
            // ปีงบ 59
            $startDate='2015-10-01';
            $endDate='2016-09-30';
        }else if($year=='60'){
            // ปีงบ 60
            $startDate='2016-10-01';
            $endDate='2017-09-30';
        }else{
            $startDate='2015-10-01';
            $endDate='2016-09-30';
        }
        
        $out['startDate']=$startDate;
        $out['endDate']=$endDate;
        return $out;
    }
    
    public static function groupUserOnEdit($ezf_id){
        if(strlen($ezf_id)>0){
            // ดึงชื่อ table จาก ezf_id
            $sql    = 'select * from ezform where ezf_id=:ezf_id ';
            $dset   = Yii::$app->db->createCommand($sql,[':ezf_id'=>$ezf_id])->queryOne();
            $table  = $dset['ezf_table'];
            if( 0 ){
                echo "<pre align='left'>";
                print_r($dset);
                echo "</pre>";
                $out    = $dset;
            }
        }
        
        if(strlen($table)>0 ){
            $userId = Yii::$app->user->identity->id;
            $tmptb  =   'stat_key_'.$userId.str_replace('.','',microtime(true));
            
            $sql    = 'DROP TABLE IF EXISTS '.$tmptb;
            $dset   = Yii::$app->db->createCommand($sql)->query();
            
            $sql    =   'CREATE TEMPORARY TABLE '.$tmptb;
            $sql    .=  ' select user_update,rstat,count(*) as recs ';
            $sql    .=  ',max(update_date) as max_update, min(update_date) as min_update ';
            $sql    .=  'from '.$table.' ';
            $sql    .=  'where rstat <> "3" ';
            $sql    .=  'group by user_update ';
            $sql    .=  'order by rstat, user_update ';
            $dset   = Yii::$app->db->createCommand($sql)->query();
            
            $sql    =   'select u.firstname, u.lastname, k.* from '.$tmptb.' k ';
            $sql    .=  'left join user_profile u on u.user_id=k.user_update ';
            $sql    .=  'where u.user_id is not null ';
            $dset   = Yii::$app->db->createCommand($sql)->queryAll();
            
            if( 0 ){
                echo "<pre align='left'>";
                print_r($dset);
                echo "</pre>";
                
            }
            $out    = $dset;
        }
        return $out;
    }
    
    public static function tbdataFromEzfID($ezf_id){
        if(strlen($ezf_id)>0){
            // ดึงชื่อ table จาก ezf_id
            $sql    = 'select * from ezform where ezf_id=:ezf_id ';
            $dset   = Yii::$app->db->createCommand($sql,[':ezf_id'=>$ezf_id])->queryOne();
        }
        return $dset;
    }
    
    public static function tbdataRecordByUserUpdate($ezf_id, $userid){
        if(strlen($ezf_id)>0){
            $ezf = self::tbdataFromEzfID($ezf_id);
            // ดึงชื่อ table จาก ezf_id
            $sql = 'select * from '.$ezf['ezf_table'].' where user_update=:userid ';
            $sql.= 'and rstat<>"3" ';
            $dset   = Yii::$app->db->createCommand($sql,[':userid'=>$userid])->queryAll();
        }
        return $dset;
    }
}
