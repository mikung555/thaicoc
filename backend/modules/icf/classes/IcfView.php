<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//use yii\jui\DatePicker;
namespace backend\modules\icf\classes;
use Yii;

/**
 * Description of IcfView
 *
 * @author siriwat
 */
class IcfView {
    //put your code here
    public function getFileType($str){
        $strarray = explode('.', $str);
        return $strarray[sizeof($strarray) - 1 ];
    }
    private function getFilename($str){
        $strarray = explode('/', $str);
        return $strarray[sizeof($strarray) - 1 ];
    }
    public function setSiteId($patient){
        $t = '';
        foreach ($patient as $p){
            $t .=  '<div class="subview-label col-sm-6"> Site ID:  </div>
                    <div class="subview-data  col-sm-6"> <b>'.$p['hsitecode']. '</b></div>
                    <div class="subview-label col-sm-6"> Participant ID: </div>
                    <div class="subview-data  col-sm-6"> <b>'.$p['hptcode'].'</b></div>' ; 
        }
        return $t;
    }
  
    public function setFileShow($img, $id, $radio){
        $t = '';
        if( $img != '' && $img != null){
            $filetype = $this->getFileType($img);
            $t .= '<div class="dsubview-img-list col-sm-8">';
                // in case has only pdf and img file type

            if($filetype == 'pdf' || $filetype == 'PDF'){ // show object 
                $t .= '<object width="800" height="1000" data="'.$this->setFullFileUrl($img).'"></object>' ; 
            }else{
                $t .= '<img class="dsubview-img-list-img image-rotate-0" id="dsubview-img-'.$id.'" src="'.$this->setFullFileUrl($img).'"  data-zoom-image="'.$this->setFullFileUrl($img).'"> <br /> ';       
            }
            $t .= '</div><div class="dsubview-img-list-chkbox col-sm-4" ><input type="radio" name="selectimg" value="'.$id.'" '.$radio.' /> เลือก file นี้เป็น file ที่ใช้</div>';
        }
        return $t;
    }
    public function setSelectOption($array, $val){
        $t = '';
        foreach ( $array as $list){
            $t .= $list['val']== $val ? '<option value="'.$list['val'].'" selected="selected" >'.$list['text'].'</option>' : '<option value="'.$list['val'].'">'.$list['text'].'</option>';
        }
        return $t;
    }
    public function setNotAccept($data){
        $d = iconv("ISO-8859-11", "UTF-8", $data);
        //iconv('ISO-8859-11', 'utf-8', $accepthis['comment'])
        //$d = mb_convert_encoding($data['comment'], 'UTF-8', 'ISO-8859-1');
        $dataarray = explode(',', $d);
        $t = '';
        foreach($dataarray as $notaccept){
            $t .= '<div class="subview-data col-sm-12">'.$notaccept.' </div>';
        }
        return $t; 
    }
    public function setCheckingList($dataarray, $status){
        $t = '';
        $i = 1 ;
        if($status == 3){
            foreach($dataarray as $checking){
                $t .= '<span>'. $i.'. ';
                $t .= iconv("ISO-8859-11", "UTF-8", $checking['comment']). ' <b><a id="delete-answer-'.$checking['qid'].'" class="red14n delete-answer"> ยกเลิก </a></b> </span><br />';
                $i++;     
            }
        }
        return $t;
    }
    public function setQueryList($dataarray, $cnumber, $text, $passid, $statuscheck){
        // check not pass id if statuscheck == 3
        // if statuscheck == 1 disable passcheck
        // query submit data list
        $t = '<div class="dsubview-chkbox col-sm-12"> '
                . '<h3><small>'.$text.'</h3></small> ';
        $querylist = $q = Yii::$app->dbcascap->createCommand("select * from query_message_error where msg_use  = 1 ")->queryAll();
        
        foreach ($dataarray as $list){
            $key = array_search($list, array_column($querylist, 'id'));
            $t .= '<div class="subview-data col-sm-12">';
            $dis = $statuscheck == 1 ? 'disabled="disabled"' : '' ;
            if(in_array($list, $passid)){
                $t .= '<input type="checkbox" class="chpassans" id="chpassid['.$list.']" name="chpassid['.$list.']" value="1" checked="checked" '.$dis.'> ';
            }else{
                $t .= '<input type="checkbox" class="chpassans" id="chpassid['.$list.']" name="chpassid['.$list.']" value="1" '.$dis.'> ';
            }
            $t .= $cnumber .'. <span id="span-chpassid-'.$list.'">'. iconv('ISO-8859-11', 'utf-8',  $querylist[$key]['name']).'</span>';
            $t  .= '</div>';
            $cnumber++;
        }
        $t .= '</div>';
        return [$t, $cnumber];            
    }
    public function setFullFileUrl($url){
        return 'https://cloudbackend.cascap.in.th/fileinput/'.$url;
    }
    public function setThDate($date){
        // get database date : yy-mm-dd;
        $d = explode('-',$date);
        return $d[2].'/'.$d[1].'/'.((int)($d[0] + 543));
    }
}
