<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace backend\modules\icf\classes;
use Yii;

/**
 * Description of DataCca02
 *
 * @author chaiwat
 */
class DataCca02 {
    //put your code here
    public static function getListByPtid($ptid){
        $sql = 'select id,ptid,target,rstat,hsitecode,hptcode,sitecode,ptcode from tb_data_3 ';
        $sql.= 'where ptid=:ptid ';
        $sql.= 'and rstat in (1,2) ';
        $sql.= 'order by f2v1 desc ';
        $dset   = Yii::$app->db->createCommand($sql,[':ptid'=>$ptid])->queryAll();
        return $dset;
    }
}
