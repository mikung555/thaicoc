<?php
namespace backend\modules\icf\classes;

use Yii;
use yii\helpers\Html;

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class ICFFunc {
    
    public static function menuICFAll() {
	//$txtMenu[];
       
        $txtMenu[0]=  Html::a('Register', ['/icf/reg'], ['class'=>'btn btn-info', 'role'=>'button']);
	return $txtMenu;
    }
    
    public static function getDatefromYearPeriod($year){
        if( strlen($year)== 0 ){
            $year='59';
        }
        if($year=='57'){
            $startDate='2013-10-01';
            $endDate='2014-09-30';
        }else if($year=='58'){
            $startDate='2014-10-01';
            $endDate='2015-09-30';
        }else if($year=='59'){
            // ปีงบ 59
            $startDate='2015-10-01';
            $endDate='2016-09-30';
        }else if($year=='60'){
            // ปีงบ 60
            $startDate='2016-10-01';
            $endDate='2017-09-30';
        }else{
            $startDate='2015-10-01';
            $endDate='2016-09-30';
        }
        
        $out['startDate']=$startDate;
        $out['endDate']=$endDate;
        return $out;
    }
    
    public static function getICFStatus($icf_status,$icf_status_wait2=0){
        if($icf_status_wait2==1){
            $icf_status=31;
        }
        switch ($icf_status){
            case 4 : 
                $out = "<span class=\"label label-success label-as-badge\" style=\"font-size: 20px\">ใบยินยอมได้รับการตรวจสอบผ่านแล้ว ";
                $out .= "<span class=\"label label-primary label-as-badge\">(เมื่อลงข้อมูลสมบูรณ์และ Submit ข้อมูลนี้ จะได้รับค่าตอบแทน)</span></span>";
                break;
            case 3 : 
                $out = "<span class=\"label label-warning label-as-badge\" style=\"font-size: 20px\">ใบยินยอมตรวจแล้ว ไม่ผ่าน ";
                $out .= "<span class=\"label label-danger label-as-badge\">(ไม่สามารถเบิกค่าตอบแทนได้)</span> กรุณาแก้ไขตามการแจ้งเตือน</span>";
                break;
            case 31 : 
                $out = "<span class=\"label label-warning label-as-badge\" style=\"font-size: 20px\">ใบยินยอมเคยตรวจแล้ว ไม่ผ่าน ";
                $out .= "<span class=\"label label-danger label-as-badge\">(ไม่สามารถเบิกค่าตอบแทนได้)</span> รอการตรวจสอบอีกรอบจากส่วนส่วนกลาง</span>";
                break;
            case 2 : 
                $out = "<span class=\"label label-warning label-as-badge\" style=\"font-size: 20px\">ใบยินยอมอยู่ในขั้นตอนการตรวจสอบจาก CASCAP ";
                $out .= "<span class=\"label label-danger label-as-badge\">(ไม่สามารถเบิกค่าตอบแทนได้)</span></span>";
                break;
            case 1 : 
                $out = "<span class=\"label label-warning label-as-badge\" style=\"font-size: 20px\">ใบยินยอมยังไม่ Submit เพื่อส่งตรวจใบยินยอม ";
                $out .= "<span class=\"label label-danger label-as-badge\">(ไม่สามารถเบิกค่าตอบแทนได้)</span></span>";
                break;
            case 0 : 
                $out = "<span class=\"label label-warning label-as-badge\" style=\"font-size: 20px\">ยังไม่ Upload ใบยินยอม ";
                $out .= "<span class=\"label label-danger label-as-badge\">(ไม่สามารถเบิกค่าตอบแทนได้)</span></span>";
                break;
        }
        
        return $out;
    }
    
    
    public static function callICFStatus($ez_fromId,$dataId){
        $sql = "CALL patientStatusProcess ({$dataId},'" . $ez_fromId . "')";
        $icfStatus = Yii::$app->db->createCommand($sql)->queryOne();
        return $icfStatus;
        // call $txtICFStatus = \backend\modules\icf\classes\ICFFunc::getICFStatus($sqlData['icf_status'], $sqlData['icf_status_wait2']);
    }
}
