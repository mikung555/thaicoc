<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace backend\modules\icf\classes;

use Yii;

/**
 * Description of DataResaveDaftLogStat
 *
 * @author jokeclancool
 */
class DataResaveDaftLogStat {

    public static function onResaving($dataid, $ezf_id, $divresponse) {

        //\appxq\sdii\utils\VarDumper::dump($dataid) ;
        $create_date = date("Y-m-d H:i:s");
        // Insert data to log rstat ----------------------
        $user_id = Yii::$app->user->identity->userProfile->user_id;
        $username = Yii::$app->user->identity->username;
        $firstname = Yii::$app->user->identity->userProfile->firstname;
        $lastname = Yii::$app->user->identity->userProfile->lastname;
        $fullname = $firstname . " " . $lastname;

        $sqlEzform = "SELECT ezf_table FROM ezform WHERE ezf_id='$ezf_id'";
        $resultEzf = Yii::$app->db->createCommand($sqlEzform)->queryAll();
        if ($resultEzf != null) {
            $table = $resultEzf[0]['ezf_table'];

            // Select data from tb_data...
            $sqltb = " SELECT  id,ptid , target, xsourcex FROM $table WHERE id = '$dataid'";
            $resulttb = \Yii::$app->db->createCommand($sqltb)->queryAll();
            $ptid = $resulttb[0]['ptid'];
            $id = $resulttb[0]['id'];
            $target = $resulttb[0]['target'];
            $xsourcex = $resulttb[0]['xsourcex'];


            if ($resulttb != null) {

                $old = '2';
                $change = '1';

                $sqlInsert = "INSERT INTO log_rstat(data_id, dadd,field_label, rstat_org, rstat_new, `table`, ptid, user_id, username, fullname)"
                        . " VALUES('$dataid','$create_date','rstat','$old' ,'$change','$table','$ptid','$user_id','$username','$fullname')";
                $resIns = \Yii::$app->db->createCommand($sqlInsert)->execute();

                $sqlInsert = "INSERT INTO ezform_data_log(dataid, ptid,rstat, ezf_id, ezf_field_name, `old`, `change`, xsourcex, user_create, create_date)"
                        . " VALUES('$dataid','$ptid','1' ,'$ezf_id','rstat','$old','$change','$xsourcex','$user_id',NOW())";
                $resIns = \Yii::$app->db->createCommand($sqlInsert)->execute();

                $sqlUpdate = " UPDATE $table SET rstat=1 WHERE id = '$dataid' ";
                \Yii::$app->db->createCommand($sqlUpdate)->execute();

                $sqlUpdateTarget = "UPDATE ezform_target SET update_date=NOW(), rstat=1 WHERE data_id=:dataid ";
                $resUpdateTarget = \Yii::$app->db->createCommand($sqlUpdateTarget, [':dataid' => $dataid])->execute();
                
                //$sqlUpdateReply = "UPDATE ezform_reply SET update_date=NOW(), rstat=1 WHERE data_id=:dataid ";
                //$resUpdateReply = \Yii::$app->db->createCommand($sqlUpdateTarget, [':dataid' => $dataid])->execute();
            }
        }
    }

    public static function loadHistory($dataid, $dataid2=null, $ezf_id, $divresponse) {
        $sqllog = " SELECT dadd,field_label, rstat_org, rstat_new, username, fullname "
                . " FROM log_rstat WHERE (data_id ='$dataid' or data_id ='$dataid2') or ptid='$dataid' order by dadd DESC ";
        $resultlog = \Yii::$app->db->createCommand($sqllog)->queryAll();

        foreach ($resultlog as $val) {
            $dadd = $val['dadd'];
            $rstat_org = $val['rstat_org'];
            $rstat_new = $val['rstat_new'];
            $fullname = $val['fullname'];
            $field_label = $val['field_label'];
        }
        //if($resultlog)
        $return = null;
        if ($resultlog != null) {
            $return = [
                'resultlog' => $resultlog,
                'dataid' => $dataid,
                'dataid2' => $dataid2,
                'ezf_id' => $ezf_id,
                'divresponse' => $divresponse,
                'rstat_org' => $rstat_org,
                'rstat_new' => $rstat_new,
                'fullname' => $fullname,
                'field_label' => $field_label,
                'dadd' => $dadd
            ];
        }

        return $return;
    }

}
