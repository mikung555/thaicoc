<?php
/* @var $this yii\web\View */
if( 0 ){
    echo "<pre align='left'>";
    //print_r($dataCount);
    print_r($_webget);
    print_r($_webserver);
    echo "</pre>";
}
?>
<style>
    .tdtext-number-show{
        text-align: right;
        font-size: 24px;
        font-weight: bold;
        color: blue;
    }
    .tdtext-header-show{
        text-align: left;
        font-size: 24px;
        font-weight: bold;
        color: black;
    }
    .tdtext-number-show-black{
        text-align: right;
        color: black;
    }
    .tdtext-number-show-red{
        text-align: right;
        color: red;
    }
    .tdtext-number-show-blue{
        text-align: right;
        color: blue;
    }
    .table {
        border-radius: 5px;
        width: 70%;
        float: none;
        font-family: "Angsana New",Arial,serif;
        font-size: 20px;
    }
    a.link{
        text-decoration: none;
    }
    a.hover{
        text-decoration: underline;
    }
</style>
<h1>ตรวจสอบ ความถูกต้องของการนำเข้าข้อมูล CCA-01</h1>
<div class="container" >
    <?php
        echo $this->render('/reg/_menu', [
                '_webserver' => $_webserver,
                '_webget' => $_webget,
            ]);
        // add url
        if(strlen($_webget['pryear'])>0){
            $optionLinkAdd="&pryear=".$_webget['pryear'];
        }
    ?>
    <br />
    <br />
    <div class="header inline">
        <label class="text-bold">สามารถเข้าดูข้อมูลราย Records โดยการคลิ๊กที่ตัวเลข ของแต่ละเงื่อนไขได้</label>
    </div>
    <table class="table table-striped">
        <thead class="table table-inverse">
            <tr>
                <th>#</th>
                <th class="tdtext-header-show">ผลการตรวจสอบ</th>
                <th class="tdtext-number-show">จำนวนที่ตรวจ</th>
            </tr>
        </thead>
        <tbody>
            <?php
                $irow=1;
                if(count($_filter['condition'])>0){
                    foreach( $_filter['condition'] as $k => $v ){
            ?>
            <tr>
                <th scope="row"><?php echo $irow; ?></th>
                <td><?php echo $_filter['condition'][$k]['msg']; ?></td>
                <td class="tdtext-number-show-black">
                    <a href="<?php echo $_filter['condition'][$k]['url'].$optionLinkAdd; ?>" class="tdtext-number-show-black">
                    <?php
                        echo number_format($dataCount['dataGroup'][$_filter['condition'][$k]['sqlname']],0,'.',',');
                    ?>
                    </a>
                </td>
            </tr>
            <?php
                        $irow++;
                    }
                }
            ?>
        </tbody>
    </table>
</div>
