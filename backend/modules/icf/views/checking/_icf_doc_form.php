<?php
use backend\modules\icf\classes\IcfView;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$icfview = new IcfView();
$ele = 'vid='.$_GET['vid'].'&dataid='.$_GET['dataid'].'&xsourcex='.$_GET['xsourcex'].'&user_key='.$_GET['user_key'];

$radio1 = NULL;
$radio2 = NULL;
if($accepthis['value_old'] == 1){
    // select upload1
    $radio1 = 'checked="checked"';
}else if($accepthis['value_old'] == 2){
    // select upload2
    $radio2 = 'checked="checked"';
}else if($accepthis['value_old'] == 3){
    $radio3 = 'checked="checked"';
}
$datespl = explode('/', $patient[0]['sys_dateoficf']); // dd/mm/yy

?>

<div class="panel panel-primary">
    <div class="panel-heading icf-heading">ข้อมูลเอกสาร</div>
    
    <div class="panel-body">
        <div class="col-sm-4 col-sm-offset-8"> <?=Html::button('submit a query', ['data-url'=>Url::to(['/icf/checking/submit-load?'.$ele]), 
            'class' => 'btn btn-primary', 'id'=>'call-submit-query-btn'])?></div>
        <?php $form = ActiveForm::begin(['id'=>'icf-image-submit', 'action'=>'image-submit']); ?>
        <h3><small>tools (ICF+CID)</small></h3>
        <div class="dsubview-img col-sm-12">
            
            <?php
             $t = '';
             $submitquerybtn = 
             $form = ' 
                <div class="select-date-form">
                 
                 <div class="form-group">
                    <label class="col-sm-6"> วันที่ขอคำยินยอม </label>
                    <div class="col-sm-6" >  
                        '. \common\lib\damasac\widgets\DMSDateWidget::widget([
                        //'model'=>$model,
                        'id'=>'dateoficf',
                        'value' => $patient[0]['sys_dateoficf'],
                        'name' =>'dateoficf',
                    ]) .'</div>
                </div>
                <div class="form-group">
                    <label class="col-sm-6">ตราประทับจาก EC:</label>
                    <div class="col-sm-6">
                        <select name="selectoficf" id="selectoficf" class="form-control">
                            <option value=""> เลือกวันที่ประทับตราจาก EC </option>
                            '. $icfview->setSelectOption($datelist, $patient[0]['sys_ecoficf']) . '</select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-6 col-sm-offset-6">
                        <input type="hidden" name="q_data_id" value="'.$_GET['dataid'].'">
                        <input type="hidden" name="q_target_id" value="'.$_GET['vid'].'">
                        <input type="hidden" name="q_userkey" value="'.$_GET['user_key'].'">
                        <input type="hidden" name="q_xsourcex" value="'.$_GET['xsourcex'].'">
                        <input type="submit" name="btnSaveICF" class="btn btn-primary" id="btnSaveICF" value="บันทึก" />
                    </div>
                </div></div>';
             
                $filetype = $icfview->getFileType($patient[0]['icf_upload1']);
                $t .= '<div class="dsubview-img-list col-sm-8">';
                if($filetype == 'pdf' || $filetype == 'PDF'){ // show object 
                    $t .= '<object width="800" height="1000" data="'.$icfview->setFullFileUrl($patient[0]['icf_upload1']).'"></object>' ; 
                }else{
                    $t .= '<img class="dsubview-img-list-img image-rotate-0" id="dsubview-img-'.$imglist[0].'" src="'.$icfview->setFullFileUrl($patient[0]['icf_upload1']).'"  data-zoom-image="'.$icfview->setFullFileUrl($patient[0]['icf_upload1']).'" > <br /> ';       
                }
                $t .= '</div><div class="dsubview-img-list-chkbox col-sm-4" ><input type="radio" name="selectimg" value="'.$imglist[0].'" '.$radio1.' /> เลือก file นี้เป็น file ที่ใช้';
                $t .= $form;
                $t .= '</div>';
                
            echo $t;
            ?>
        </div>
        <h3><small>ICF+บัตรประจำตัวประชาชน</small></h3>
        <div class="dsubview-img col-sm-12">
            <?= $icfview->setFileShow($patient[0]['icf_upload2'], $imglist[1], $radio2 ) ?>
        </div>
        <h3><small>เอกสารอื่นๆ ที่ต้องการเก็บ</small></h3>
        <div class="dsubview-img col-sm-12">
            <?= $icfview->setFileShow($patient[0]['icf_upload3'], $imglist[2], $radio3 ) ?>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>


        