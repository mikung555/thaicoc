<?php
use yii\widgets\ActiveForm;
use yii\jui\DatePicker;
use backend\modules\icf\classes\IcfView;
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$icfview = new IcfView();
?>

   
<div class="container-fluid">
    
    <?php echo $this->render('_icf_patient_view', ['cca'=>$cca, 'patient'=>$patient, 'ezyformid' =>$ezformid]); ?>
    <?php echo $this->render('_icf_doc_form', ['imglist'=>$imglist, 'date'=>$date, 'datelist' =>$datelist, 'patient'=>$patient, 
        'accepthis'=> $accepthis, 'checkinghis' =>$checkinghis,]); ?>
    
    <?php echo $this->render('_icf_patient_view', ['cca'=>$cca, 'patient'=>$patient, 'update'=>$update, 'site'=>$site]); ?>
    <?php echo $this->render('_icf_officer_bio', ['officer'=>$officer]); ?>
     <!-- etc--  -->
    <?php echo $this->render('_icf_history', ['accepthis'=> $accepthis, 'checkinghis' =>$checkinghis]); ?>
     
     <!-- submit query -->
     <button class="btn btn-default" id="test-button"> click </button>
     
     <?php
        yii\bootstrap\Modal::begin([
            'headerOptions' => ['id' => 'modalHeader'],
            'header' =>'<h4 class="modal-title" id="itemModalLabel">Submit a query</h4>',
            'id' => 'modal',
            'size' => 'modal-lg',
            //keeps from closing modal with esc key or by clicking out of the modal.
            // user must click cancel or X to close
            'clientOptions' => ['backdrop' => 'static', 'keyboard' => FALSE]
        ]);
        echo "<div id='modalContent'></div>";
        yii\bootstrap\Modal::end();
    ?>
     
     
</div>

<?php
    $this->registerJs(
        " // set image rorate angle
        var angel = 0 ; // start at 0 degree
            
        $('.dsubview-img-list-img').on('click', function() {
            // get id 
            var id = $(this).attr('id');
            // get image rotation class
            var classList = $('#'+id).attr('class').split(/\s+/);
            var rorateclass = '';
            var substr = 'image-rotate';
            for(var i = 0; i < classList.length ;i++){
                if(classList[i].indexOf(substr) !== -1){ //check has class in list
                    rorateclass = classList[i];
                }
            }
            var rclass = rorateclass.split('-') ;
            var angle = parseInt(rclass[rclass.length - 1] );
            
            var nextangle = angle == 270 ? 0 : angle+90;
            $('#'+id).removeClass(rorateclass);
            $('#'+id).addClass('image-rotate-' + nextangle.toString());
            
            // remove zoom element 
            //$.removeData($('img'), 'elevateZoom');
            //$('.zoomContainer').remove();
        
            //$('.dsubview-img-list-img').elevateZoom();
            
        });
        
        // call function again
        $('.dsubview-img-list-img').elevateZoom({scrollZoom : true});
        
        $('#call-submit-query-btn').on('click', function() {
            modalEzfrom($(this).attr('data-url'));
           // alert(33);
        });
        /*
        $(document).on('click', '.chpassans', function(){
            // get array number
            var array = [];
            $('.chpassans').each(function(){
                if(this.checked){
                    var id = $(this).attr('id');
                    var idtext = id.toString();
                    var idtextspl = idtext.split('[');
                    var idtextlast = idtextspl[idtextspl.length -1].slice(0, -1); 
                   
                    array.push($('#span-chpassid-'+idtextlast).html());
                }
            });
            var text = array.join(',');
            $('#submitQueryComment').val(text);
        });
        */
        function modalEzfrom(url) {
            $('#modal .fade-modal').show();
            $('#modal-print .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
            $('#modal-print').modal('show');
            
            $.ajax({
                method: 'get',
                url: url,
                dataType: 'HTML',
                success: function(result, textStatus) {
                    $('#modal #modalContent').html(result);
                    $('#modal').modal('show');
                    return false;
                }
            });
        }
        $(document).on('submit', '#icf-submit-query', function(e){
            $.ajax({
                url: $(this).attr('action'),
                type:'POST',
                dataType:'JSON',
                data: $(this).serialize(),
                success:function(data){
                       if(data ){
                            alert(data.success);
                        }
                    }
                });
            e.preventDefault();
        });
        
        $(document).on('submit', '#icf-image-submit', function(e){
            $.ajax({
                url: $(this).attr('action'),
                type:'POST',
                dataType:'JSON',
                data: $(this).serialize(),
                success:function(data){
                       if(data ){
                            alert(data.success);
                        }
                    }
                });
            e.preventDefault();
        });
        $(document).on('click', '.chpass', function(){
            if($(this).val() == 1){
                $('.chpassans').attr('disabled', true);
            }else{
                $('.chpassans').removeAttr('disabled');
            }
        });
        ///
        $('.del-all-btn').click(function(){
            var data = $(this).attr('id');
            var d = data.split('-');
            var xsourcex = d[d.length -1];
            var dataid = d[d.length -2 ];
            if(confirm('ยืนยันการยกเลิกคำตอบทั้งหมด')){
                $.ajax({
                    url: 'delete-all',
                    type:'POST',
                    dataType:'JSON',
                    data: {dataid : dataid, xsourcex : xsourcex},
                    success:function(data){
                        if(data ){
                            alert(data.success);
                            $('#confirm-answer-list').html('');
                        }
                    }
                });
            }
        });
        $('#test-button').click(function(){
            $.ajax({
                    url: 'test-click',
                    type:'POST',
                    dataType:'HTML',
                    success:function(result, textStatus){
                       // $('#modal-print .modal-content').html(result);
                       //  return false;
                       console.log(result)
                    }
                });
        });
        $('.delete-answer').click(function(){
            var id = $(this).attr('id');
            var id2 = id.split('-');
            var qid = id2[id2.length-1];
            if(confirm('ยืนยันการยกเลิกคำตอบนี้')){
                $.ajax({
                    url: 'delete-answer',
                    type:'POST',
                    dataType:'JSON',
                    data: {qid : qid},
                    success:function(data){
                        if(data ){
                            alert(data.success);
                            $('#'+id).parent().parent().remove();
                        }
                    }
                });
            }
        });
        
    "
    );
    $this->registerJsFile(
    'elevatezoom-master/jquery.elevateZoom-3.0.8.min.js',
    ['depends' => [\yii\web\JqueryAsset::className()]]
    );
    
?>
<style>
    
    .subview-data a:hover, .subview-link a:hover{
        color : #dd0000;
    }
    .subview-data-ccastatus{
        color : #0000ff;
    }
    .subview-label{
        text-align : right;
    }
    .righticf{
        color : #0000ff;
    }
    .wrongicf{
        color : #ff0000;
    }
    .subview-link{
        
    }
    .icf-heading{
        font-size : 25px;
    }
    /* rotate image class*/
    .image-rotate-0 {
    transform-origin: top left; /* IE 10+, Firefox, etc. */
    -webkit-transform-origin: top left; /* Chrome */
    -ms-transform-origin: top left; /* IE 9 */
}
    .image-rotate-90 {
        transform: rotate(90deg) translateY(0%);
        -webkit-transform: rotate(90deg) translateY(0%);
        -ms-transform: rotate(90deg) translateY(0%);
    }
    .image-rotate-180 {
        transform: rotate(180deg) translate(0%,0%);
        -webkit-transform: rotate(180deg) translate(0%,0%);
        -ms-transform: rotate(180deg) translateX(0%,0%);
    }
    .image-rotate-270 {
        transform: rotate(270deg) translateX(0%);
        -webkit-transform: rotate(270deg) translateX(0%);
        -ms-transform: rotate(270deg) translateX(0%);
    }
    .dsubview-img-list-img{
        cursor : pointer;
    }
    .dsubview-img-list img{
        max-width : 800px;
        max-height : 100%;
    }
    .select-date-form{
        margin-top : 400px;
    }
    .red14n{
        color : red;
        cursor : pointer;
    }
</style>

