<?php
use backend\modules\icf\classes\IcfView;
use yii\widgets\ActiveForm;

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$icflist = ['71','72','73','74','75','112','111','114','76','115','116','117','121'];
$cidlist = ['78','113','79'];
$siteidlist = ['80','81','82','83'];
$addresslist = ['84','85'];
$agelist = ['87', '88'];
$descriptionlist = ['89','90'];
$acceptlist = ['91', '92'];
$signlist = ['93', '94','95','96','97', '98', '110', '100'];
$acceptdatelist = ['101', '102'];
$birthdatelist =['103', '104', '105'];
$editlist = ['107','118', '119'];
$ccalist = ['106', '120'];
$cnumber = 1 ;
$icfView = new IcfView();
/// set test sitecode and ptcode 
$sitecode = $_GET['xsourcex'];

$ptcode = Yii::$app->db->createCommand("select ptcode  from tb_data_1 where ptid = :ptid and xsourcex = :xsourcex", [':ptid'=>$_GET['dataid'], ':xsourcex'=>$_GET['xsourcex']])->queryScalar(); 

$accepthis =Yii::$app->dbcascap->createCommand("select *  from query_confirmq where ptid = :ptid", [':ptid'=>$_GET['dataid']])->queryOne(); 
$checkinghis = Yii::$app->dbcascap->createCommand("select *  from query_confirmq where qid_key = :qid", [':qid'=>$accepthis['qid'] ])->queryAll(); 

$comment = Yii::$app->dbcascap->createCommand("select *  from query_confirmq where qid_key = :qid and value = 99 ", [':qid'=>$accepthis['qid'] ])->queryOne(); 
$d = [];
  foreach($checkinghis as $c){
      $d[] = $c['value'];
  }
?>
<?php $form = ActiveForm::begin(['id'=>'icf-submit-query', 'action'=>'submit-query']); ?>
    
<div class="panel">
    <!-- <div class="panel-heading icf-heading">Submit a query</div>  -->
    <div class="panel-body">
        <div class="dsubview-radio col-sm-12">
            
                <?php if($accepthis['value_status'] == 1 ){ ?>
                <div class="subview-data col-sm-12"> 
                    <input type="radio" id="chpass01" class="chpass" name="chpass" value="1" checked="checked">ตรวจสอบแล้วผ่านข้อกำหนด
                </div>   
                <div class="subview-data col-sm-12">
                    <input type="radio" id="chpass03" class="chpass" name="chpass" value="3">ตรวจสอบแล้วไม่ผ่านข้อกำหนด
                </div>
                <?php  }else{ ?>
                <div class="subview-data col-sm-12">
                    <input type="radio" id="chpass01" class="chpass" name="chpass" value="1"> ตรวจสอบแล้วผ่านข้อกำหนด
                </div>
          
                <div class="subview-data col-sm-12">
                    <input type="radio" id="chpass03" class="chpass" name="chpass" value="3" checked="checked">ตรวจสอบแล้วไม่ผ่านข้อกำหนด
                </div>
                <?php   } ?>
        </div>
        <!----------- list ----------->
        <?php    
        $q = $icfView->setQueryList($icflist, $cnumber, 'ใบยินยอม', $d, $accepthis['value_status']); // set text and set counting number
        echo $q[0];
        $cnumber = $q[1];
        
        $q = $icfView->setQueryList($cidlist, $cnumber, 'บัตรประชาชน', $d, $accepthis['value_status']); // set text and set counting number
        echo $q[0];
        $cnumber = $q[1];
        
        $q = $icfView->setQueryList($siteidlist, $cnumber, 'Site ID&Participant ID', $d, $accepthis['value_status']); // set text and set counting number
        echo $q[0];
        $cnumber = $q[1];
        
        $q = $icfView->setQueryList($addresslist, $cnumber, 'ที่อยู่', $d, $accepthis['value_status']); // set text and set counting number
        echo $q[0];
        $cnumber = $q[1];
        
        $q = $icfView->setQueryList($agelist, $cnumber, 'อายุ', $d, $accepthis['value_status']); // set text and set counting number
        echo $q[0];
        $cnumber = $q[1];
        
        $q = $icfView->setQueryList($descriptionlist, $cnumber, 'ได้รับฟังคำอธิบายจาก', $d, $accepthis['value_status']); // set text and set counting number
        echo $q[0];
        $cnumber = $q[1];
        
        $q = $icfView->setQueryList($acceptlist, $cnumber, 'ยินดี หรือ ไม่ยินดี ', $d, $accepthis['value_status']); // set text and set counting number
        echo $q[0];
        $cnumber = $q[1];
        
        $q = $icfView->setQueryList($signlist, $cnumber, 'ลายเซ็น', $d, $accepthis['value_status']); // set text and set counting number
        echo $q[0];
        $cnumber = $q[1];
        
        $q = $icfView->setQueryList($acceptdatelist, $cnumber, 'วันที่ขอคำยินยอม', $d, $accepthis['value_status']); // set text and set counting number
        echo $q[0];
        $cnumber = $q[1];
        
        $q = $icfView->setQueryList($birthdatelist, $cnumber, 'วันเดือนปีเกิด', $d, $accepthis['value_status']); // set text and set counting number
        echo $q[0];
        $cnumber = $q[1];
        
        $q = $icfView->setQueryList($editlist, $cnumber, 'การแก้ไขคำผิด', $d, $accepthis['value_status']); // set text and set counting number
        echo $q[0];
        $cnumber = $q[1];
        
        $q = $icfView->setQueryList($ccalist, $cnumber, 'CCA-01', $d, $accepthis['value_status']); // set text and set counting number
        echo $q[0];
        $cnumber = $q[1];
        ?>
        
        <div class="col-sm-12">
            <div class="col-sm-12"> หมายเหตุ </div>
            <div class="col-sm-12"> <textarea cols="60" rows="5" id="submitQueryComment" name="comment"><?= iconv('ISO-8859-11', 'utf-8', $comment['comment']) ?></textarea> </div>
            <input type="hidden" name="crf_sitecode" value="<?= $sitecode ?>">
            <input type="hidden" name="crf_ptcode" value="<?= $ptcode ?>">
            
            <input type="hidden" name="q_data_id" value="<?= $_GET['dataid'] ?>">
            <input type="hidden" name="q_target_id" value="<?= $_GET['vid'] ?>">
            <input type="hidden" name="q_userkey" value="<?= $_GET['user_key'] ?>">
            <input type="hidden" name="q_xsourcex" value="<?= $_GET['xsourcex'] ?>">
            <div class="col-sm-12"> <input type="submit" id="btnSubmitAQuery" name="btnSubmitAQuery" class="btn btn-primary" value=" Submit a query "> </div>
        </div>
        
        
    </div>
</div>

    <?php ActiveForm::end(); ?>