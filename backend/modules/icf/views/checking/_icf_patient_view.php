<?php
use backend\modules\icf\classes\IcfView;
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */   
        // show view
$icfview = new IcfView();

$sitepatient  = [];
foreach($patient as $p){
    if($_GET['xsourcex'] == $p['xsourcex']){
        $sitepatient = $p;
    }
}
$rstatus = '';
switch ($cca['rstat']){
    case 2 : $rstatus = 'Complete';
    break;
    default : $rstatus = 'Not Complete';
    break;
}
?>
<div class="panel panel-primary">
    <div class="panel-heading icf-heading"> ข้อมูลจาก Cloud </div>
    <div class="panel-body">
        <div class="col-sm-8">
            <dl class="dl-horizontal">
                <dt>วันที่เก็บข้อมูล CCA-01:</dt>
                <dd>
                    <b><?= $icfview->setThDate($cca['f1vdcomp']) ?><span class="subview-data-ccastatus"> CCA-01:<?= $rstatus ?> </span> 
                    <a href="https://cloudbackend.cascap.in.th/inputdata/redirect-page/?ezf_id=<?= $ezyformid['ccaezfid']?>&amp;dataid=<?= $_GET['dataid'] ?>" target="_blank"> [เข้าสู่ CCA-01] </a></b>
                </dd> 
                <dt>วันเดือนปีเกิด CCA-01:</dt>
                <dd>
                    <b><?= $icfview->setThDate($cca['f1v2']) ?><a href="https://cloudbackend.cascap.in.th/inputdata/redirect-page/?ezf_id=<?= $ezyformid['regdataid'] ?>&amp;dataid=<?= $_GET['dataid'] ?>" target="_blank">[เข้าสู่ Register]</a> </b>
                </dd>
                <dt>ชื่อ:</dt>
                <dd><b><?= $sitepatient['title'] .' '. $sitepatient['name'].' '.$sitepatient['surname'] ?></b> </dd>
                <dt>เลขที่บัตรประชาชน:</dt> 
                <dd><b><?= $sitepatient['cid'] ?></b></dd>
                <dt>อายุ:</dt>
                <dd><b><?= $sitepatient['age'] ?></b> </dd>
                <dt>วัน/เดือน/ปี เกิด:</dt>
                <dd><b><?= $sitepatient['v2'] ?></b></dd>
                <dt>  ที่อยู่: </dt>
                <dd><b>
                    บ้านเลขที่: <?= $sitepatient['add1n1'].' หมู่บ้าน: '.$sitepatient['add1n2'].' หมู่ที่: '. $sitepatient['add1n5'].'  
                                ตำบล: '.$sitepatient['distinct_name'].' อำเภอ: '. $sitepatient['amphur_name'].' จังหวัด: '. $sitepatient['province_name'] ?>
                    </b>
                </dd>
                <dt>วันที่แก้ไขล่าสุด:</dt>
                <dd><?= $sitepatient['update_date'] ?></dd>
                <dt>วันที่ลงข้อมูลครั้งแรก:</dt>
                <dd><?= $sitepatient['create_date'] ?></dd>
                <dt>ตรวจล่าสุด:</dt>
                <dd><?= $sitepatient['dlastcheck'] ?> </dd>
            </dl>
        </div>
        <div class="col-sm-4">
            <div class="col-sm-12"> <?= $icfview->setSiteId($patient)?></div>
        </div>
        
    </div>
</div>
    
        
 