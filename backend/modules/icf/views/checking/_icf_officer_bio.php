<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$img =  $officer['cardimg'] ? '<img class="dsubview-img" src="'.$officer['cardimg'].'">' : '' ;
?>
<div class="panel panel-primary">
    <div class="panel-heading icf-heading">ข้อมูลผู้ลงข้อมูล</div>
    <div class="panel-body">
        <dl class="dl-horizontal">
            <dt> ชื่อ: </dt>
            <dd><?= $officer['firstname'].' '. $officer['middlename'].' ' .$officer['lastname'] ?></dd>
            <dt> เบอร์โทรติดต่อ: </dt>
            <dd><?= $officer['telephone'] ?></dd>
            <dt> e-mail: </dt>
            <dd><?= $officer['email'] ?></dd>
            <dt> โรงพยาบาล: </dt>
            <dd><?= $officer['hospname'] ?></dd>
            <dt>ภาพบัตรประจำตัวประชาชน </dt>
            <dd><?= $officer['cid'] ?> </dd>
        </dl>
    </div>
</div>
    
        
