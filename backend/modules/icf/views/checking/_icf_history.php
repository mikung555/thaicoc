<?php
use backend\modules\icf\classes\IcfView;
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$checkingHisText = $accepthis['value_status'] == 1 ? '<b>ผ่านการตรวจสอบแล้ว <span class="righticf">ICF ถูกต้อง </span></b> ' :  '<b>ตรวจสอบแล้วไม่ผ่าน: <span class="wrongicf">ICF ยังไม่ถูกต้อง </span> ตามรายละเอียดการแจ้งด้านล่างนี้ </b><br /> ';

$icfView = new IcfView();
?>
<div class="panel panel-primary">
    <div class="panel-heading icf-heading">ประวัติที่เคยตรวจแล้วไม่ผ่าน</div>
    <div class="panel-body">
        <?= $icfView->setNotAccept($accepthis['comment']) ?>
    </div>
</div>
<div class="panel panel-primary">
    <div class="panel-heading icf-heading">จำนวนครั้งที่ตรวจ</div>
    <div class="panel-body">
        <dl class="dl-horizontal">
            <dt>จำนวนครั้งที่ตรวจ:</dt>
            <dd> 1 ครั้ง </dd> <!---------- not has code ----->
            <dt>สถานะของข้อมูล:</dt>
            <dd ><?= $checkingHisText.'<span id="confirm-answer-list">'.$icfView->setCheckingList($checkinghis, $accepthis['value_status']).'</span>' ?></dd> 
            <dt></dt>
            <dd><a id="del-all-btn-<?= $_GET['dataid'] ?>-<?= $_GET['xsourcex'] ?>" class="btn btn-danger del-all-btn">ยกเลิกทั้งหมด</a></dd>
        </dl>
    </div>
</div>