<?php
/* @var $this yii\web\View */
?>
<style>
    .tdtext-number-show{
        text-align: right;
        font-size: 24px;
        font-weight: bold;
        color: blue;
    }
    .tdtext-header-show{
        text-align: left;
        font-size: 24px;
        font-weight: bold;
        color: black;
    }
    .tdtext-number-show-black{
        text-align: right;
        color: black;
    }
    .tdtext-number-show-red{
        text-align: right;
        color: red;
    }
    .tdtext-number-show-blue{
        text-align: right;
        color: blue;
    }
    .table {
        border-radius: 5px;
        width: 70%;
        float: none;
        font-family: "Angsana New",Arial,serif;
        font-size: 20px;
    }
    a.link{
        text-decoration: none;
    }
    a.hover{
        text-decoration: underline;
    }
</style>
<h1>ตรวจสอบ ความถูกต้องของการนำเข้าข้อมูล CCA-02</h1>
<div class="container" >
    <?php
        echo $this->render('/reg/_menu', [
                '_webserver' => $_webserver,
            ]);
    ?>
    <br />
</div>
