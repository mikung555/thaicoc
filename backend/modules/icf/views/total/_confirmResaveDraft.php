<?php
use yii\helpers\Html;
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<br />
<div class="col-md-offset-2 col-md-4 bg-danger">
    <br />
    <div class="row">
        <div class="col-md-offset-2">
            คุณต้องการ Resave draft หรือไม่
        </div>
    </div>
    <div class="row">
        <div class="col-md-offset-4">
            <?php

            echo Html::button('Yes',[
                    'id'        => 'btnConfirmYes',
                    'dataid'    => $dataid,
                    'dataid2'    => $dataid2,
                    'ezf_id'    => $ezf_id,
                    'divresponse' => $divresponse,
                    'class'     => "btn btn-primary btn-sm",
                    'onclick' => "onResave(this)"
                ]);
            echo "&nbsp;";
            echo Html::button('No',[
                    'id'        => 'btnConfirmNo',
                    'dataid'    => $dataid,
                    'ezf_id'    => $ezf_id,
                    'class'     => "btn btn-danger btn-sm",
                ]);
            ?>
        </div>
        <div id="loading-save" class="col-md-offset-4" hidden="hidden">
            
            <i class="fa fa-spinner fa-pulse"></i> Resaving...
            
        </div>
        </div>
    </div>
    <br />
</div>