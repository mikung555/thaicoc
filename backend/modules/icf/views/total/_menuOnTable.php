<?php

use backend\modules\cpay\classes\HospitalFunc;
use backend\modules\cpay\classes\UserCanView;
use backend\modules\teleradio\classes\QueryUserProfile;

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$js = <<< JS
$("#btn-popupreg").on("click", function() {
    modalShowPopup($(this).attr('data-url'));
});
//$("#btn-pop").on("click", function() {
$(document).on("click", "*[id^=btn-pop]", function() {
    modalShowPopup($(this).attr('data-url'));
});
function modalShowPopup(url) {
    $('#modal-popupreg .modal-content').html('<div class="sdloader "><i class="sdloader-icon"></i></div>');
    $('#modal-popupreg').modal('show')
    .find('.modal-content')
    .load(url);
}
        
$(document).on("click", "*[id^=openUser]", function() {
    var url             = $(this).attr('data-url');
    var period          = $(this).attr('period');
    var paytype         = $(this).attr('paytype');
    var username        = $(this).attr('username');
    var hsitecode       = $(this).attr('hsitecode');
    url = url+'?paytype='+paytype;
    url = url+'&period='+period;
    url = url+'&username='+username;
    url = url+'&hsitecode='+hsitecode;
    //alert(url);
    modalShowPopupPay(url);
});
function modalShowPopupPay(url) {
    $('#modal-popupreg .modal-dialog').attr('style','width:90%');
    $('#modal-popupreg .modal-content').html('<div class="sdloader "><i class="sdloader-icon"></i></div>');
    $('#modal-popupreg').modal('show')
    .find('.modal-content')
    .load(url);
}
JS;
// register your javascript
$this->registerJs($js, \yii\web\View::POS_END);

$request    = Yii::$app->request;

$userid     = Yii::$app->user->identity->id;
$username     = Yii::$app->user->identity->username;
$usersitecode=Yii::$app->user->identity->userProfile->sitecode;
$myuserprofile['hospital'] = HospitalFunc::userOnHospital($usersitecode);
$uprofile = QueryUserProfile::get($userid);
$vuser = $myuserprofile['hospital'][0];

$keyin = 'keyin';
$period = '20170301';
if( 0 ){
    // เปลี่ยน user ที่เราต้องการ
    // คนนำเข้าข้อมูล
    $vuser['hcode'] = '13780';
    $username = '3520101129050';
    $keyin = 'keyin';
    // แพทย์
//    $vuser['hcode'] = '10928';
//    $username = '6';
//    $keyin = 'service';
    
}

$checkKeyin = UserCanView::checkKeyin($period,$username,$keyin);
?>
<div class="container-fluid">
    <div class="row">
        <div>
            <button type='button' class='btn btn-warning' id='popupsymbol' data-toggle='modal' data-target='#myModal'>อ่านคำอธิบายสัญลักษณ์</button>

            <button type='button' class='btn btn-warning' style="width: 180px" id='btn-popupreg' data-url="/icf/reg/index-modal" > Reg + ICF </button>
            <?php
            if( $checkKeyin ){
                echo \yii\helpers\Html::a(' ค่าตอบแทน รอบแรกปี 60 '
                    ,NULL 
                    ,[
                        'id'            => 'openUserReturn',
                        'data-url'      => '/cpay/y60/show-detail',
                        'period'        => $period, //$request->get('period'),
                        'zone_code'     => $vuser['zone_code'],
                        'provincecode'  => $vuser['provincecode'],
                        'amphurcode'    => $vuser['amphurcode'],
                        'tamboncode'    => $vuser['tamboncode'],
                        'hsitecode'     => $vuser['hcode'],
                        'username'      => $username, //$vuser['username'],
                        'paytype'       => $keyin, //$vuser['paytype'],
                        'userid'        => '',
                        'fullname'      => $uprofile['firstname'].' '.$uprofile['lastname'],
                        'class'         => 'btn btn-primary'
                    ]);
            }else if( $myuserprofile['hospital'][0][code4]=='1' ){
                echo \yii\helpers\Html::a(' ค่าตอบแทน รอบแรกปี 60 (สสจ) '
                    , \yii\helpers\Url::to(['/cpay/y60'])
                    ,[
                        'id'            => 'btnProvinceOpen',
                        'target'        => '_blank',
                        'class'         => 'btn btn-primary'
                    ]);
            }
            ?>
        </div>
    </div>
</div>
<p></p>
