<?php

use yii\helpers\Html;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

if (0) {
    echo "<pre align='left'>";
    print_r($icf);
    echo "</pre>";
}

$requestResaveDraftRegisEnable = False;
$requestResaveDraftCCA01Enable = False;
//\appxq\sdii\utils\VarDumper::dump($reg);
?>

<br />
<div class="row">
    <div class="col-sm-offset-1">
        <?php
        echo Html::a("<button class='btn btn-success'>ประวัติทั่วไป</button>", [
            '/inputdata/redirect-page',
            'dataid' => $ptid, 'ezf_id' => $ezf_id, 'rurl' => base64_encode(Yii::$app->request->url)
                ], [
            'data-pjax' => '0',
            'target' => '_blank'
        ]);
        ?>
    </div>
</div>

<div class="row">
    <div class="col-sm-offset-2">ชื่อ: <?php echo $reg[0]['name'] . " " . $reg[0]['surname']; ?></div>
</div>

<div class="row">
    <div class="col-sm-offset-1">ประวัติหน่วยบริการที่บันทึกข้อมูล (ใบทำบัตร)</div>
</div>
<?php
if (count($reg) > 0) {
    $i = 1;
    $dataid_cca = $cca01[0]['id'];
    foreach ($reg as $kvalue) {
        if ($kvalue['rstat'] == '2') {
            $requestResaveDraftRegisEnable = True;
        }
        if ($kvalue['hsitecode'] == $userSiteCode) {
            $dataid_regis = $kvalue['id'];
            $ezf_id = '1437377239070461301';
        }
        ?>
        <div class="row">
            <div class="col-sm-offset-2">ครั้งที่: <?php echo $i . " ที่: " . $kvalue['hsitecode'] . " " . $hosp[$kvalue['hsitecode']]['name']; ?></div>
        </div>
        <div class="row">
            <div class="col-sm-offset-2">&nbsp;&nbsp;สถานะข้อมูล: <?php
                if ($kvalue['rstat'] == '0') {
                    echo Html::button('Add new record', [
                        'class' => "btn btn-info btn-sm",
                    ]);
                } else if ($kvalue['rstat'] == '1') {
                    echo Html::button('Save Draft', [
                        'class' => "btn btn-info btn-sm",
                    ]);
                } else if ($kvalue['rstat'] == '2') {
                    echo Html::button('Submitted', [
                        'class' => "btn btn-primary btn-sm",
                    ]);
                }
                ?></div>
        </div>
        <?php
        $i++;
    }
}

if ($icf['icf_status'] == '4') {
    // ตรวจผ่านแล้ว ไม่ต้องทำอะไร
    $requestResaveDraftRegisEnable = False;
}
if ($requestResaveDraftRegisEnable && strlen($dataid_regis) > 0) {
    // สามารถขอยื่นสิทธิ์ การ Resave draft ได้
    ?>
    <div class="row">
        <div class="col-sm-offset-1">ต้องการ Re-Save Draft</div>
    </div>
    <div class="row">
        <div class="col-sm-offset-2">
            <?php
            echo Html::button('ขอ Re-Save Draft ใบทำบัตร', [
                'id' => 'btnResaveDraftReg',
                'dataid' => $dataid_regis,
                'dataid2' => $dataid_cca,
                'ezf_id' => $ezf_id,
                'divresponse' => 'confirmResaveDraftReg',
                'class' => "btn btn-warning btn-sm",
            ]);
            ?>

        </div>
    </div>
    <div class="row" id="confirmResaveDraftReg">

    </div>
    <?php
}


// STATUS for CCA-01
if ($cca01[0]['rstat'] == '2') {
    $requestResaveDraftCCA01Enable = True;
    if ($cca01[0]['hsitecode'] == $userSiteCode) {
        $dataid_cca = $cca01[0]['id'];
        $ezf_id = '1437377239070461302';
    }
}
if ($icf['icf_status'] == '4') {
    // ตรวจผ่านแล้ว ไม่ต้องทำอะไร (ต้องขอส่วนกลางเท่านั้น)
    $requestResaveDraftCCA01Enable = False;
}
?>

<div class="row">
    <div class="col-sm-offset-1">
        <?php
        echo Html::a("<button class='btn btn-success'>CCA-01</button>", [
            '/inputdata/redirect-page',
            'dataid' => $dataid_cca,
            'dataid2' => $dataid_regis,
            'ezf_id' => $ezf_id,
            'rurl' => base64_encode(Yii::$app->request->url)
                ], [
            'data-pjax' => '0',
            'target' => '_blank'
        ]);
        ?>
    </div>
</div>

<div class="row">
    <div class="col-sm-offset-2">โดย: <?php echo $cca01[0]['hsitecode'] . " " . $hosp[$cca01[0]['hsitecode']]['name']; ?></div>
</div>
<div class="row">
    <div class="col-sm-offset-2">วันที่ลงข้อมูล: <?php echo $cca01[0]['f1vdcomp']; ?></div>
</div>
<div class="row">
    <div class="col-sm-offset-2">&nbsp;&nbsp;สถานะข้อมูล: <?php
        if ($cca01[0]['rstat'] == '0') {
            echo Html::button('Add new record', [
                'class' => "btn btn-info btn-sm",
            ]);
        } else if ($cca01[0]['rstat'] == '1') {
            echo Html::button('Save Draft', [
                'class' => "btn btn-info btn-sm",
            ]);
        } else if ($cca01[0]['rstat'] == '2') {
            echo Html::button('Submitted', [
                'class' => "btn btn-primary btn-sm",
            ]);
        }
        ?></div>
</div>

<div class="row">
    <div class="col-sm-offset-2">สถานะการตรวจ: <label class="bg-danger"><?php
            echo strip_tags($txtICFStatus);
            ?></label></div>
</div>
<?php
if (True) {
    if ($requestResaveDraftCCA01Enable) {
        // สามารถขอยื่นสิทธิ์ การ Resave draft ได้
        ?>
        <div class="row">
            <div class="col-sm-offset-1">ต้องการ Re-Save Draft</div>
        </div>
        <div class="row">
            <div class="col-sm-offset-2">
                <?php
                echo Html::button('ขอ Re-Save Draft CCA-01', [
                    'id' => 'btnResaveDraftCCA01',
                    'dataid' => $dataid_cca,
                    'dataid2' => $dataid_regis,
                    'ezf_id' => $ezf_id,
                    'divresponse' => 'confirmResaveDraftCca01',
                    'class' => "btn btn-warning btn-sm",
                ]);
                ?>

            </div>
        </div>
        <div class="row" id="confirmResaveDraftCca01">

        </div>
        <?php
    }
}
?>

<div class="row" id="history-draft">
    <div class="col-md-offset-2 col-md-4 bg-danger">
        <i class="fa fa-spinner fa-pulse"></i> History Loading...
    </div>
</div>
<br/>
<?php
$jsAdd = <<< JS
    $(function(){
        $('#loading-save').hide();
        var url     = '/icf/total/history-draft';
        var div     = 'confirmResaveDraftCca01';
        var dataid_regis  = '$dataid_regis';
        var dataid_cca  = '$dataid_cca';
        var ezf_id  = '$ezf_id';
        var div_his = $('#history-draft');
        var dataget={
                dataid:dataid_regis,
                dataid2:dataid_cca,
                ezf_id:ezf_id,
                div:div
            };
        
        $.get(url,dataget).done(function( data ) {
            //console.log(data);
            $("#"+div).empty();
            div_his.empty();
            div_his.html(data);
            
        });
    });
        
    
    $("*[id^=btnResaveDraft]").click(function() {
        $(this).attr("disabled",true);
        var url     = '/icf/total/confirm-re-save-draft-waiting';
        var div     = $(this).attr("divresponse");
        
        var dataid  = $(this).attr("dataid");
        var dataid2  = $(this).attr("dataid2");
        var ezf_id  = $(this).attr("ezf_id");
        //console.log(dataid+' '+ ezf_id);
        var dataget={
                dataid:dataid,
                dataid2:dataid2,
                ezf_id:ezf_id,
                div:div
            };
   
        $.get(url,dataget).done(function( data ) {
            //console.log(data);
            $("#"+div).empty();
            $("#"+div).html(data);
            //console.log(data);
        });
    });
    
     function onResave(btn) {
        var load = $('#loading-save');
        load.show();
        load.html('<i class="fa fa-spinner fa-pulse"></i> Resaving...')
        var url     = '/icf/total/confirm-re-save-draft';
        var div     = $(btn).attr("divresponse");
        var div_his = $('#history-draft');
        var dataid  = $(btn).attr("dataid");
        var dataid2  = $(btn).attr("dataid2");
        var ezf_id  = $(btn).attr("ezf_id");
        //console.log(dataid+'  '+dataid2);
        var dataget={
                dataid:dataid,
                dataid2:dataid2,
                ezf_id:ezf_id,
                div:div
            };
        $.get(url,dataget).done(function( data ) {
            //console.log(data);
            $("#"+div).empty();
            div_his.empty();
            div_his.html(data);
        });

    }
JS;
$this->registerJs($jsAdd);
?>