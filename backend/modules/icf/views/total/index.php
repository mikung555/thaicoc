<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
//use kartik\widgets\Select2;
use bootstrap\helpers;

//use kartik\widgets\DepDrop;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\icf\models\TotalSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->registerCss('
.tooltip-inner {
    max-width: 1000px;
    padding: 3px 8px;
    color: #fff;
    background-color: #000;
    border-radius: 4px;
    text-align: left;
    font-size: medium;
    white-space: pre;
}
');

$this->title = 'กำกับงาน CASCAP';
$this->params['breadcrumbs'][] = $this->title;


// Modal reg
echo \common\lib\sdii\widgets\SDModalForm::widget([
    'id' => 'modal-popupreg',
    'size' => 'modal-lg',
    'tabindexEnable' => false
]);
?>
<?php
Pjax::begin();
?>
<div class="row">
    <div class="col-sm-12">
        <?php echo $this->render('_search', ['model' => $searchModel]); ?>
    </div>
</div>

<?php
echo "<div class=\"row\">";
echo "<div class=\"col-sm-8\" >";
echo Html::label('<a href="#trip" data-rel="popup" class="ui-btn ui-btn-inline ui-corner-all">Register : </a> ') . ' ';
echo Html::a(' [' . "<span class='glyphicon glyphicon-exclamation-sign' style='color: red'></span>" . ' Error ' . $errorRegisterCount . ' ]', ['/icf/total'], [
    'data' => [
        'method' => 'get',
        'params' => [
            'action' => 'register_error'
        ]
    ],
    'style' => ['color' => 'red']
        ]
) . ' ';
echo Html::a('[' . "<span class='glyphicon glyphicon-time' style='color: orange'></span>" . ' รอตรวจ ' . $waitingRegisterCount . ' ]', ['/icf/total'], [
    'data' => [
        'method' => 'get',
        'params' => [
            'action' => 'waiting'
        ]
    ],
    'style' => ['color' => 'orange']
        ]
) . ' ';
echo Html::a('[' . "<span class='glyphicon glyphicon-ok' style='color: green'></span>" . ' ผ่าน ' . $successRegisterCount . ' ] ', ['/icf/total'], [
    'data' => [
        'method' => 'get',
        'params' => [
            'action' => 'complete'
        ]
    ],
    'style' => ['color' => 'green']
        ]
) . ' ';
echo Html::a('[' . "<span class='glyphicon glyphicon-remove' style='color: red'></span>" . ' ตรวจแล้วไม่ผ่าน ' . $notSuccessConfirmRegisterCount . ' ] ', ['/icf/total'], [
    'data' => [
        'method' => 'get',
        'params' => [
            'action' => 'not_complete'
        ]
    ],
    'style' => ['color' => 'red']
        ]
) . ' ';
echo Html::a('All', [
    '/icf/total',
        ], ['class' => 'btn btn-info btn-sm']) . '';
echo "&nbsp";
echo Html::a('Reload', [
    '/icf/total/?reload=1',
        ], ['class' => 'btn btn-info btn-sm']) . '';
//total number
//echo "<br>";
//echo "&nbsp;";
// Menu

echo "</div>";


echo "<div class=\"col-sm-4\" align=\"right\" >";
echo "<span>ข้อมูลทั้งหมด: </span>";
if ($pageMaxpage > 1) {
    for ($iIndex = 0; $iIndex < $pageMaxpage; $iIndex++) {
        $pageEachpage["page"][$iIndex] = "หน้า " . ($iIndex + 1);
    }
}
if ($pageMaxpage > 1) {
    ?>
    <script type="text/javascript" >
        function showMsg(obj)
        {
            window.open(obj.options[obj.selectedIndex].value, '_self');
        }
    </script>

    <select id="someCountries" onchange="showMsg(this)" class="btn-info">
        <option label="เลือกหน้าที่ต้องการ" value="" selected >เลือกหน้าที่ต้องการ</option>
    <?php
    for ($iIndex = 0; $iIndex < $pageMaxpage; $iIndex++) {
        $pageEachpage["page"][$iIndex] = "หน้า " . ($iIndex + 1);
        ?>
            <option label="<?php echo $pageEachpage["page"][$iIndex]; ?>" value="/icf/total?page=<?php echo ($iIndex + 1); ?>&per-page=<?php echo $getPerpage; ?>" <?php if (($iIndex + 1) == $getCurrentpage) {
            echo "selected";
        } ?> >หน้า <?php echo ($iIndex + 1); ?></option>
            <?php
        }
        ?>
    </select>
        <?php
    }

//echo "</div>";
//echo "<div class=\"col-sm-2\" >";
    if ($_GET["page"] == $pageMaxpage) {
        echo Html::a('หน้าแรก', [
            '/icf/total',
                ], ['class' => 'btn btn-info btn-sm']) . '';
    } else {
        echo Html::a('หน้าสุดท้าย', [
            '/icf/total',
            'page' => $pageMaxpage,
            'per-page' => $getPerpage,
                /* 'datadet' => $_GET["page"]."->".$_GET["per-page"].'allrows->'.$modelAllrow.'maxpage->'.$pageMaxpage, */
                ], ['class' => 'btn btn-info btn-sm']) . '';
    }
    echo "&nbsp;&nbsp;";
    if (1) {
        echo "<a href='http://tools.cascap.in.th/v4cascap/testexcel/PHPExcel_1/Examples/01worklistxlssitelist.php?hcode=" . $sitecode . "' class='btn btn-info btn-sm active' role='button' data-toggle='tooltip' data-original-title='Export รายชื่อทั้งหมด เป็น Excel' target='_self'>Export Excel</a>";
    }
    echo "<br><br>";
    echo "</div>";
    echo "</div>";

    echo $this->render('_menuOnTable');
    ?>
<div class="total-index">
<?php
if ($status == 0) {
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'id'=>'gridID',
        'columns' => [
                ['class' => 'yii\grid\SerialColumn',
                'contentOptions' => ['style' => 'width: 50px;', 'class' => 'text-center'],
                'headerOptions' => ['class' => 'text-center'],
            ],
                [
                'attribute' => 'fullcode',
                'format' => 'text',
                'label' => 'HOSPCODE+PID',
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center'],
            ],
                [
                'attribute' => 'hn',
                'format' => 'text',
                'label' => 'HN',
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center'],
            ],
                [
                'attribute' => 'cid',
                'format' => 'text',
                'label' => 'เลขบัตรประชาชน',
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center'],
            ],
                [
                'attribute' => 'fullname',
                'format' => 'raw',
                'label' => 'ชื่อ - สกุล',
                'headerOptions' => ['class' => 'text-left'],
                'contentOptions' => ['class' => 'text-left'],
                'value' => function($model) {
                    $dataurl = '/icf/total/pop-up-emr';
                    $dataurl .= '?id=' . $model['register_ptid'];
                    $dataurl .= '&fullcode=' . $model['fullcode'];
                    $textOfName = $model['title'] . $model['fullname'];
                    if (Yii::$app->keyStorage->get('frontend.domain') == 'yii2-starter-kit.dev' || Yii::$app->keyStorage->get('frontend.domain') == 'dpmcloud.dev') {
                        //if( TRUE ){
                        if ($model['register_status'] == '1') {
                            
                        } else if ($model['register_status'] == '2' || $model['cca01_status'] == '2') {
                            $textOfName .= Html::button(" <span class='glyphicon glyphicon-wrench' style='color:blue'></span>", [
                                        'data-toggle' => 'tooltip', 'data-original-title' => 'ยื่นคำขอเพื่อ Re-save draft',
                                        'data-pjax' => '0',
                                        'id' => 'btn-pop' . $model['fullcode'],
                                        'data-url' => $dataurl,
                                        'class' => 'btn btn-link btn-sm',
                            ]);
                        }
                    }
                    return $textOfName;
                }
            ],
                [
                'format' => 'raw',
                'label' => 'Reg + ICF',
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'value' => function($model) {
                    //$check2times=backend\modules\icf\controllers\CheckIcfController::checkIcf2Times($model['register_ptid']);
                    $check2times = backend\modules\icf\controllers\CheckIcfController::checkIcfTimeChecking($model['register_ptid']);
                    $check2timesx = ''; //$check2times;
                    if ($check2times == 2) {
                        $resultt2 = ""; //'<a href="/inputdata/redirect-page?dataid=' . $model[register_ptid] . '&amp;ezf_id=1437377239070461301&amp;rurl=' . base64_encode(Yii::$app->request->url) . '" data-toggle="tooltip" data-original-title="ตรวจใบยินยอมแล้ว แต่ไม่ผ่าน !ผลจากการตรวจ:' . $strError . '" data-pjax="0" target="_blank"><span class="glyphicon glyphicon-remove" style="color: red"></span></a>';
                    } else {
                        $resultt2 = "";
                    }
                    //if ($check2times == 1 || $check2times == 2) {
                    if ($check2times == 2) {
                        $result1 = '<a href="/inputdata/redirect-page?dataid=' . $model[register_ptid] . '&amp;ezf_id=1437377239070461301&amp;rurl=' . base64_encode(Yii::$app->request->url) . '" data-toggle="tooltip" data-original-title="ตรวจใบยินยอมแล้ว แต่ไม่ผ่าน !ผลจากการตรวจ:' . $strError . '" data-pjax="0" target="_blank"><span class="glyphicon glyphicon-remove" style="color: red"></span></a>';
                    } else {
                        $result1 = "";
                    }
                    //$result1 .= $model['register_status'];


                    if ($model['register_status'] == '1') {

                        $result2 = Html::a("<span class='glyphicon glyphicon-ok' style='color: green'></span>", [
                                    '/inputdata/redirect-page',
                                    'dataid' => $model['register_ptid'], 'ezf_id' => '1437377239070461301', 'rurl' => base64_encode(Yii::$app->request->url)
                                        ], [
                                    'data-toggle' => 'tooltip', 'data-original-title' => 'ตรวจผ่านแล้ว',
                                    'data-pjax' => '0',
                                    'target' => '_blank'
                        ]);

                        $data_result = $check2timesx . $resultt2 . $result1 . $result2;
                        return $data_result;
                    } elseif ($model['register_status'] == '2') {
                        $result2 = Html::a("<span class='glyphicon glyphicon-time' style='color: orange'></span>", [
                                    '/inputdata/redirect-page',
                                    'dataid' => $model['register_ptid'], 'ezf_id' => '1437377239070461301', 'rurl' => base64_encode(Yii::$app->request->url)
                                        ], [
                                    /* 'data-toggle' =>'tooltip', 'data-original-title' =>'อัพโหลดใบยินยอมแล้ว รอตรวจใบยินยอมจากเจ้าหน้าที่', */
                                    'data-toggle' => 'tooltip', 'data-original-title' => 'การลงทะเบียนเสร็จสมบูรณ์แล้ว อย่างไรก็ตาม เพื่อเป็นไปตามมาตรการประกันคุณภาพข้อมูล เจ้าหน้าที่จะตรวจสอบความถูกต้องของใบยินยอมก่อนที่จะนับเป็นผลงานเพื่อจ่ายค่าตอบแทนผู้บันทึกข้อมูล (ถ้ามี)',
                                    'data-pjax' => '0',
                                    'target' => '_blank'
                        ]);
                        $data_result = $check2timesx . $resultt2 . $result1 . $result2;
                        return $data_result;
                    } elseif ($model['register_status'] == '6') {
                        if ($check2times == 2) {
                            $message_title = 'ได้แก้ไข จากการแจ้งผลการตรวจอีกครั้ง ไม่มีกำหนดเวลาในการแจ้งผลการตรวจในครั้งนี้ (กรณีการตรวจครั้งที่ 3 เป็นต้นไป จะไม่มีกำหนดเวลาในการตรวจแน่นอน)';
                        } else {
                            $message_title = 'ได้แก้ไข จากการแจ้งผลการตรวจอีกครั้ง การลงทะเบียนเสร็จสมบูรณ์แล้ว อย่างไรก็ตาม เพื่อเป็นไปตามมาตรการประกันคุณภาพข้อมูล เจ้าหน้าที่จะตรวจสอบความถูกต้องของใบยินยอมก่อนที่จะนับเป็นผลงานเพื่อจ่ายค่าตอบแทนผู้บันทึกข้อมูล (ถ้ามี)';
                        }
                        $result2 = Html::a("<span class='glyphicon glyphicon-time' style='color: orange'></span>", [
                                    '/inputdata/redirect-page',
                                    'dataid' => $model['register_ptid'], 'ezf_id' => '1437377239070461301', 'rurl' => base64_encode(Yii::$app->request->url)
                                        ], [
                                    /* 'data-toggle' =>'tooltip', 'data-original-title' =>'อัพโหลดใบยินยอมแล้ว รอตรวจใบยินยอมจากเจ้าหน้าที่', */
                                    'data-toggle' => 'tooltip', 'data-original-title' => $message_title,
                                    'data-pjax' => '0',
                                    'target' => '_blank'
                        ]);
                        $data_result = $check2timesx . $resultt2 . $result1 . $result2;
                        return $data_result;
                    } elseif ($model['register_status'] == '3') {
                        $strError = str_replace("<br />", " ", $model['register_error']);
                        $result2 = Html::a("<span class='glyphicon glyphicon-remove' style='color: red'></span>", [
                                    '/inputdata/redirect-page',
                                    'dataid' => $model['register_ptid'], 'ezf_id' => '1437377239070461301', 'rurl' => base64_encode(Yii::$app->request->url)
                                        ], [
                                    'data-toggle' => 'tooltip', 'data-original-title' => 'ตรวจใบยินยอมแล้ว แต่ไม่ผ่าน !ผลจากการตรวจ: ' . $strError /* Show error จาก tools */,
                                    'data-pjax' => '0',
                                    'target' => '_blank'
                        ]);
                        $data_result = $check2timesx . $result1 . $result2;
                        return $data_result;
                    } elseif ($model['register_status'] == '4') {
                        $strError = str_replace("<br />", " ", $model['register_error']);
                        $result2 = Html::a("<span class='glyphicon glyphicon-exclamation-sign'  style='color: red'></span>", [
                                    '/inputdata/redirect-page',
                                    'dataid' => $model['register_ptid'], 'ezf_id' => '1437377239070461301', 'rurl' => base64_encode(Yii::$app->request->url)
                                        ], [
                                    'data-toggle' => 'tooltip', 'data-original-title' => 'ยังไม่อัพโหลดใบยินยอม หรือฟอร์มมี Error ' . str_replace("<br>", " ", $strError),
                                    'data-pjax' => '0',
                                    'target' => '_blank'
                        ]);
                        $data_result = $check2timesx . $resultt2 . $result1 . $result2;
                        return $data_result;
                    } else {
                        $strError = str_replace("<br />", " ", $model['register_error']);
                        $result2 = Html::a("<span class='glyphicon glyphicon-edit' style='color: oragne'></span>", [
                                    '/inputdata/redirect-page',
                                    'dataid' => $model['register_ptid'], 'ezf_id' => '1437377239070461301', 'rurl' => base64_encode(Yii::$app->request->url)
                                        ], [
                                    'data-toggle' => 'tooltip', 'data-original-title' => 'สถานะ save draft ยังไม่ submit',
                                    'data-pjax' => '0',
                                    'target' => '_blank'
                        ]);
                        $data_result = $check2timesx . $resultt2 . $result1 . $result2;
                        return $data_result;
                    }
                }
            ],
                [
                'format' => 'raw',
                'label' => 'CCA-01',
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'value' => function($model) {
                    if ($model['cca01_id'] == '') {
                        return Html::a("<span class='glyphicon glyphicon-plus' style='color:pink'></span>", [
                                    '/inputdata/step4',
                                    'comp_id_target' => '1437725343023632100',
                                    'ezf_id' => '1437377239070461302',
                                    'target' => base64_encode($model['register_ptid'])
                                        ], [
                                    'data-toggle' => 'tooltip', 'data-original-title' => 'เพิ่มฟอร์ม',
                                    'data-pjax' => '0',
                                    'target' => '_blank'
                        ]);
                    } else {
                        if ($model['cca01_error'] == '' && $model['cca01_status'] == '2') {
                            return Html::a("<span class='glyphicon glyphicon-ok' style='color: green'></span>", [
                                        '/inputdata/redirect-page',
                                        'dataid' => $model['cca01_id'],
                                        'ezf_id' => '1437377239070461302',
                                        'rurl' => base64_encode(Yii::$app->request->url)
                                            ], [
                                        'data-toggle' => 'tooltip', 'data-original-title' => 'ผ่าน',
                                        'data-pjax' => '0',
                                        'target' => '_blank'
                            ]);
                        } else if ($model['cca01_error'] == '' && $model['cca01_status'] == '1') {
                            return Html::a("<span class='glyphicon glyphicon-edit' style='color:blue'></span>", [
                                        '/inputdata/redirect-page',
                                        'dataid' => $model['cca01_id'],
                                        'ezf_id' => '1437377239070461302',
                                        'rurl' => base64_encode(Yii::$app->request->url)
                                            ], [
                                        'data-toggle' => 'tooltip', 'data-original-title' => 'สถานะ save draft ยังไม่ submit',
                                        'data-pjax' => '0',
                                        'target' => '_blank'
                            ]);
                            //} elseif (strlen($model['cca01_error']) >0) {
                        } else {
                            $strError = str_replace("<br />", " ", $model['cca01_error']);
                            return Html::a("<span class='glyphicon glyphicon-remove' style='color: red'></span>", [
                                        '/inputdata/redirect-page',
                                        'dataid' => $model['cca01_id'],
                                        'ezf_id' => '1437377239070461302',
                                        'rurl' => base64_encode(Yii::$app->request->url)
                                            ], [
                                        'data-toggle' => 'tooltip', 'data-original-title' => 'มี Error ' . str_replace("<br>", " ", $strError),
                                        'data-pjax' => '0',
                                        'target' => '_blank'
                            ]);
                        }
                    }
                }
            ],
                [
                'format' => 'raw',
                'label' => 'CCA-02',
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'value' => function($model) {
                    if ($model['cca02_id'] == '') {
                        return Html::a("<span class='glyphicon glyphicon-plus' style='color:pink'></span>", [
                                    '/inputdata/step4',
                                    'comp_id_target' => '1437725343023632100',
                                    'ezf_id' => '1437619524091524800',
                                    'target' => base64_encode($model['register_ptid'])
                                        ], [
                                    'data-toggle' => 'tooltip', 'data-original-title' => 'เพิ่มฟอร์ม',
                                    'data-pjax' => '0',
                                    'target' => '_blank'
                        ]);
                    } else {
                        if ($model['cca02_error'] == '') {
                            return Html::a("<span class='glyphicon glyphicon-ok' style='color: green'></span>", [
                                        '/inputdata/redirect-page',
                                        'dataid' => $model['cca02_id'],
                                        'ezf_id' => '1437619524091524800',
                                        'rurl' => base64_encode(Yii::$app->request->url)
                                            ], [
                                        'data-toggle' => 'tooltip', 'data-original-title' => 'ผ่าน',
                                        'data-pjax' => '0',
                                        'target' => '_blank'
                                    ]) . ' (' . $model['cca02_new'] . '/' . $model['cca02_draft'] . '/' . $model['cca02_submitted'] . ')';
                        } elseif (strlen($model['cca02_error']) > 0) {
                            $strError = str_replace("<br />", " ", $model['cca02_error']);
                            return Html::a("<span class='glyphicon glyphicon-remove' style='color: red'></span>", [
                                        '/inputdata/redirect-page',
                                        'dataid' => $model['cca02_id'],
                                        'ezf_id' => '1437619524091524800',
                                        'rurl' => base64_encode(Yii::$app->request->url),
                                            ], [
                                        'data-toggle' => 'tooltip', 'data-original-title' => 'มี Error ' . str_replace("<br>", " ", $strError),
                                        'data-pjax' => '0',
                                        'target' => '_blank'
                                    ]) . ' (' . $model['cca02_new'] . '/' . $model['cca02_draft'] . '/' . $model['cca02_submitted'] . ')';
                        }
                    }
                }
            ],
                [
                'format' => 'raw',
                'label' => 'CCA-02.1',
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'value' => function($model) {
                    if ($model['cca02_1_id'] == '') {
                        return Html::a("<span class='glyphicon glyphicon-plus' style='color:pink'></span>", [
                                    '/inputdata/step4',
                                    'comp_id_target' => '1437725343023632100',
                                    'ezf_id' => '1454041742064651700',
                                    'target' => base64_encode($model['register_ptid'])
                                        ], [
                                    'data-toggle' => 'tooltip', 'data-original-title' => 'เพิ่มฟอร์ม',
                                    'data-pjax' => '0',
                                    'target' => '_blank'
                        ]);
                    } else {
                        if ($model['cca02_1_error'] == '') {
                            return Html::a("<span class='glyphicon glyphicon-ok' style='color: green'></span>", [
                                        '/inputdata/redirect-page',
                                        'dataid' => $model['cca02_1_id'], 'ezf_id' => '1454041742064651700', 'rurl' => base64_encode(Yii::$app->request->url)
                                            ], [
                                        'data-toggle' => 'tooltip', 'data-original-title' => 'ผ่าน',
                                        'data-pjax' => '0',
                                        'target' => '_blank'
                                    ]) . ' (' . $model['cca02_1_new'] . '/' . $model['cca02_1_draft'] . '/' . $model['cca02_1_submitted'] . ')';
                        } elseif (strlen($model['cca02_1_error']) > 0) {
                            $strError = str_replace("<br />", " ", $model['cca02_1_error']);
                            return Html::a("<span class='glyphicon glyphicon-remove' style='color: red'></span>", [
                                        '/inputdata/redirect-page',
                                        'dataid' => $model['cca02_1_id'], 'ezf_id' => '1454041742064651700', 'rurl' => base64_encode(Yii::$app->request->url)
                                            ], [
                                        'data-toggle' => 'tooltip', 'data-original-title' => 'มี Error ' . str_replace("<br>", " ", $strError),
                                        'data-pjax' => '0',
                                        'target' => '_blank'
                                    ]) . ' (' . $model['cca02_1_new'] . '/' . $model['cca02_1_draft'] . '/' . $model['cca02_1_submitted'] . ')';
                        }
                    }
                }
            ],
                [
                'format' => 'raw',
                'label' => 'CCA-03',
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'value' => function($model) {
                    if ($model['cca03_id'] == '') {
                        return Html::a("<span class='glyphicon glyphicon-plus' style='color:pink'></span>", [
                                    '/inputdata/step4',
                                    'comp_id_target' => '1437725343023632100',
                                    'ezf_id' => '1451381257025574200',
                                    'target' => base64_encode($model['register_ptid'])
                                        ], [
                                    'data-toggle' => 'tooltip', 'data-original-title' => 'เพิ่มฟอร์ม',
                                    'data-pjax' => '0',
                                    'target' => '_blank'
                        ]);
                    } else {
                        if ($model['cca03_error'] == '') {
                            return Html::a("<span class='glyphicon glyphicon-ok' style='color: green'></span>", [
                                        '/inputdata/redirect-page',
                                        'dataid' => $model['cca03_id'], 'ezf_id' => '1451381257025574200', 'rurl' => base64_encode(Yii::$app->request->url)
                                            ], [
                                        'data-toggle' => 'tooltip', 'data-original-title' => 'ผ่าน',
                                        'data-pjax' => '0',
                                        'target' => '_blank'
                                    ]) . ' (' . $model['cca03_new'] . '/' . $model['cca03_draft'] . '/' . $model['cca03_submitted'] . ')';
                        } elseif (strlen($model['cca03_error']) > 0) {
                            $strError = str_replace("<br />", " ", $model['cca03_error']);
                            return Html::a("<span class='glyphicon glyphicon-remove' style='color: red'></span>", [
                                        '/inputdata/redirect-page',
                                        'dataid' => $model['cca03_id'], 'ezf_id' => '1451381257025574200', 'rurl' => base64_encode(Yii::$app->request->url)
                                            ], [
                                        'data-toggle' => 'tooltip', 'data-original-title' => 'มี Error ' . str_replace("<br>", " ", $strError),
                                        'data-pjax' => '0',
                                        'target' => '_blank'
                                    ]) . ' (' . $model['cca03_new'] . '/' . $model['cca03_draft'] . '/' . $model['cca03_submitted'] . ')';
                        }
                    }
                }
            ],
                [
                'format' => 'raw',
                'label' => 'CCA-04',
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'value' => function($model) {
                    if ($model['cca04_id'] == '') {
                        return Html::a("<span class='glyphicon glyphicon-plus' style='color:pink'></span>", [
                                    '/inputdata/step4',
                                    'comp_id_target' => '1437725343023632100',
                                    'ezf_id' => '1452061550097822200',
                                    'target' => base64_encode($model['register_ptid'])
                                        ], [
                                    'data-toggle' => 'tooltip', 'data-original-title' => 'เพิ่มฟอร์ม',
                                    'data-pjax' => '0',
                                    'target' => '_blank'
                        ]);
                    } else {
                        if ($model['cca04_error'] == '') {
                            return Html::a("<span class='glyphicon glyphicon-ok' style='color: green'></span>", [
                                        '/inputdata/redirect-page',
                                        'dataid' => $model['cca04_id'], 'ezf_id' => '1452061550097822200', 'rurl' => base64_encode(Yii::$app->request->url)
                                            ], [
                                        'data-toggle' => 'tooltip', 'data-original-title' => 'ผ่าน',
                                        'data-pjax' => '0',
                                        'target' => '_blank'
                                    ]) . ' (' . $model['cca04_new'] . '/' . $model['cca04_draft'] . '/' . $model['cca04_submitted'] . ')';
                        } elseif (strlen($model['cca04_error']) > 0) {
                            $strError = str_replace("<br />", " ", $model['cca04_error']);
                            return Html::a("<span class='glyphicon glyphicon-remove' style='color: red'></span>", [
                                        '/inputdata/redirect-page',
                                        'dataid' => $model['cca04_id'], 'ezf_id' => '1452061550097822200', 'rurl' => base64_encode(Yii::$app->request->url)
                                            ], [
                                        'data-toggle' => 'tooltip', 'data-original-title' => 'มี Error ' . str_replace("<br>", " ", $strError),
                                        'data-pjax' => '0',
                                        'target' => '_blank'
                                    ]) . ' (' . $model['cca04_new'] . '/' . $model['cca04_draft'] . '/' . $model['cca04_submitted'] . ')';
                        }
                    }
                }
            ],
                [
                'format' => 'raw',
                'label' => 'CCA-05',
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'value' => function($model) {
                    if ($model['cca05_id'] == '') {
                        return Html::a("<span class='glyphicon glyphicon-plus' style='color:pink'></span>", [
                                    '/inputdata/step4',
                                    'comp_id_target' => '1437725343023632100',
                                    'ezf_id' => '1440515302053265900',
                                    'target' => base64_encode($model['register_ptid'])
                                        ], [
                                    'data-toggle' => 'tooltip', 'data-original-title' => 'เพิ่มฟอร์ม',
                                    'data-pjax' => '0',
                                    'target' => '_blank'
                        ]);
                    } else {
                        if ($model['cca05_error'] == '') {
                            return Html::a("<span class='glyphicon glyphicon-ok' style='color: green'></span>", [
                                        '/inputdata/redirect-page',
                                        'dataid' => $model['cca05_id'], 'ezf_id' => '1440515302053265900', 'rurl' => base64_encode(Yii::$app->request->url)
                                            ], [
                                        'data-toggle' => 'tooltip', 'data-original-title' => 'ผ่าน',
                                        'data-pjax' => '0',
                                        'target' => '_blank'
                                    ]) . ' (' . $model['cca05_new'] . '/' . $model['cca05_draft'] . '/' . $model['cca05_submitted'] . ')';
                        } elseif (strlen($model['cca05_error']) > 0) {
                            $strError = str_replace("<br />", " ", $model['cca05_error']);
                            return Html::a("<span class='glyphicon glyphicon-remove' style='color: red'></span>", [
                                        '/inputdata/redirect-page',
                                        'dataid' => $model['cca05_id'], 'ezf_id' => '1440515302053265900', 'rurl' => base64_encode(Yii::$app->request->url)
                                            ], [
                                        'data-toggle' => 'tooltip', 'data-original-title' => 'มี Error ' . str_replace("<br>", " ", $strError),
                                        'data-pjax' => '0',
                                        'target' => '_blank'
                                    ]) . ' (' . $model['cca05_new'] . '/' . $model['cca05_draft'] . '/' . $model['cca05_submitted'] . ')';
                        }
                    }
                }
            ],
        ],
        'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => ''],
        'resizableColumns' => true,
        'responsive' => true,
        'hover' => true,
        'pjax' => true,
        'toolbar' => [
            '{toggleData}',
        ],
        'export' => [
            'fontAwesome' => true
        ],
        'containerOptions' => ['style' => 'overflow: auto'],
        'headerRowOptions' => ['class' => 'kartik-sheet-style'],
        'filterRowOptions' => ['class' => 'kartik-sheet-style'],
        'exportConfig' => $exportConfig,
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => 'ฟอร์มทั้งหมด',
        ],
        'layout' => '{summary}{items}{pager}',
    ]);
} elseif ($status == 1) {
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
                ['class' => 'yii\grid\SerialColumn',
                'contentOptions' => ['style' => 'width: 50px;', 'class' => 'text-center'],
                'headerOptions' => ['class' => 'text-center'],
            ],
                [
                'attribute' => 'fullcode',
                'format' => 'text',
                'label' => 'HOSPCODE+PID',
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center'],
            ],
                [
                'attribute' => 'cid',
                'format' => 'text',
                'label' => 'เลขบัตรประชาชน',
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center'],
            ],
                [
                'attribute' => 'fullname',
                'format' => 'text',
                'label' => 'ชื่อ - สกุล',
                'headerOptions' => ['class' => 'text-left'],
                'contentOptions' => ['class' => 'text-left'],
                'value' => function($model) {
                    return $model['title'] . $model['fullname'];
                }
            ],
                [
                'format' => 'raw',
                'label' => 'Reg + ICF',
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'value' => function($model) {
                    if ($model['register_status'] == '1') {
                        return Html::a("<span class='glyphicon glyphicon-ok' style='color: green'></span>", [
                                    '/inputdata/redirect-page',
                                    'dataid' => $model['register_ptid'], 'ezf_id' => '1437377239070461301', 'rurl' => base64_encode(Yii::$app->request->url)
                                        ], [
                                    'data-toggle' => 'tooltip', 'data-original-title' => 'ตรวจผ่านแล้ว',
                                    'data-pjax' => '0',
                                    'target' => '_blank'
                        ]);
                    } elseif ($model['register_status'] == '2') {
                        return Html::a("<span class='glyphicon glyphicon-time' style='color: orange'></span>", [
                                    '/inputdata/redirect-page',
                                    'dataid' => $model['register_ptid'], 'ezf_id' => '1437377239070461301', 'rurl' => base64_encode(Yii::$app->request->url)
                                        ], [
                                    'data-toggle' => 'tooltip', 'data-original-title' => 'อัพโหลดใบยินยอมแล้ว รอตรวจใบยินยอมจากเจ้าหน้าที่',
                                    'data-pjax' => '0',
                                    'target' => '_blank'
                        ]);
                    } elseif ($model['register_status'] == '3') {
                        $strError = str_replace("<br />", " ", $model['register_error']);
                        return Html::a("<span class='glyphicon glyphicon-remove' style='color: red'></span>", [
                                    '/inputdata/redirect-page',
                                    'dataid' => $model['register_ptid'], 'ezf_id' => '1437377239070461301', 'rurl' => base64_encode(Yii::$app->request->url)
                                        ], [
                                    'data-toggle' => 'tooltip', 'data-original-title' => 'ตรวจใบยินยอมแล้ว แต่ไม่ผ่าน !ผลจากการตรวจ: ' . $strError /* Show error จาก tools */,
                                    'data-pjax' => '0',
                                    'target' => '_blank'
                        ]);
                    } else {
                        $strError = str_replace("<br />", " ", $model['register_error']);
                        return Html::a("<span class='glyphicon glyphicon-remove' style='color: red'></span>", [
                                    '/inputdata/redirect-page',
                                    'dataid' => $model['register_ptid'], 'ezf_id' => '1437377239070461301', 'rurl' => base64_encode(Yii::$app->request->url)
                                        ], [
                                    'data-toggle' => 'tooltip', 'data-original-title' => 'ยังไม่อัพโหลดใบยินยอม หรือฟอร์มมี Error ' . str_replace("<br>", " ", $strError),
                                    'data-pjax' => '0',
                                    'target' => '_blank'
                        ]);
                    }
                }
            ],
                [
                'format' => 'raw',
                'label' => 'CCA-01',
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center'],
            ],
                [
                'format' => 'raw',
                'label' => 'CCA-02',
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center'],
            ],
                [
                'format' => 'raw',
                'label' => 'CCA-02.1',
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center'],
            ],
                [
                'format' => 'raw',
                'label' => 'CCA-03',
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center'],
            ],
                [
                'format' => 'raw',
                'label' => 'CCA-04',
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center'],
            ],
                [
                'format' => 'raw',
                'label' => 'CCA-05',
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center'],
            ],
        ],
        'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => ''],
        'resizableColumns' => true,
        'responsive' => true,
        'hover' => true,
        'pjax' => true,
        'toolbar' => [
            '{toggleData}',
        ],
        'export' => [
            'fontAwesome' => true
        ],
        'containerOptions' => ['style' => 'overflow: auto'],
        'headerRowOptions' => ['class' => 'kartik-sheet-style'],
        'filterRowOptions' => ['class' => 'kartik-sheet-style'],
        'exportConfig' => $exportConfig,
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => 'ฟอร์มทั้งหมด',
        ],
        'layout' => '{summary}{items}{pager}',
    ]);
}
?>

    <?php Pjax::end(); ?>

</div>
<script>
    function popupsymbol() {
        alert("");
    }
    
</script>

<div data-role="popup" id="popup_symbol" class="ui-content">
    <p>  <b>สัญลักษณ์ รูปที่แสดงในตาราง ความหมายของสัญลักษณ์ </b><br>
        <span class="glyphicon glyphicon-exclamation-sign" style="color:red"></span><span style="margin-left:40px"> ข้อมูลไม่สมบูรณ์ </span>
        <br><span class="glyphicon glyphicon-edit" style="color:blue"></span><span style="margin-left:40px">  save draft </span>
        <br><span class="glyphicon glyphicon-time" style="color: orange"></span><span style="margin-left:40px">  รอตรวจ - ครั้งที่ 1 </span>
        <br>แจ้งผลการตรวจครั้งที่ 1
        <br><span class="glyphicon glyphicon-ok" style="color: green"></span><span style="margin-left:40px">ผ่าน</span>
        <br><span class="glyphicon glyphicon-remove" style="color: red" ></span><span style="margin-left:40px">ไม่ผ่าน หลังจากแก้ไขแล้ว ขอให้กด submit </span>
        <br>แจ้งผลการตรวจครั้งที่ 2
        <br><span class="glyphicon glyphicon-remove" style="color: red" ></span><span class="glyphicon glyphicon-time" style="color: orange"></span><span style="margin-left:30px">รอตรวจ</span>
        <br><span class="glyphicon glyphicon-remove" style="color: red" ></span><span class="glyphicon glyphicon-ok" style="color: green"></span><span style="margin-left:30px">ผ่าน</span>
        <br><span class="glyphicon glyphicon-remove" style="color: red" ></span><span class="glyphicon glyphicon-remove" style="color: red" ></span><span style="margin-left:30px">ไม่ผ่าน หลังจากแก้ไขแล้ว ขอให้กด submit</span>
        <br>แจ้งผลการตรวจครั้งที่ 3
        <br><span class="glyphicon glyphicon-remove" style="color: red" ></span><span class="glyphicon glyphicon-remove" style="color: red" ></span><span class="glyphicon glyphicon-time" style="color: orange"></span><span style="margin-left:15px">  ไม่มีกำหนดเวลาในการแจ้งผลการตรวจในครั้งนี้</span>
    </p>
</div>
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document" style="width:1000px">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"></h4>
            </div>
            <div class="modal-body">
                <div data-role="popup" id="popup_symbol" class="ui-content">
                    <p>  <b>สัญลักษณ์ รูปที่แสดงในตาราง ความหมายของสัญลักษณ์ </b><br>
                        <span class="glyphicon glyphicon-exclamation-sign" style="color:red"></span><span style="margin-left:40px"> ข้อมูลไม่สมบูรณ์ </span>
                        <br><span class="glyphicon glyphicon-edit" style="color:blue"></span><span style="margin-left:40px">  save draft </span>
                        <br><span class="glyphicon glyphicon-time" style="color: orange"></span><span style="margin-left:40px">  รอตรวจ - ครั้งที่ 1 </span>
                        <br>แจ้งผลการตรวจครั้งที่ 1
                        <br><span class="glyphicon glyphicon-ok" style="color: green"></span><span style="margin-left:40px">ผ่าน</span>
                        <br><span class="glyphicon glyphicon-remove" style="color: red" ></span><span style="margin-left:40px">ไม่ผ่าน หลังจากแก้ไขแล้ว กด submit</span>
                        <br>แจ้งผลการตรวจครั้งที่ 2
                        <br><span class="glyphicon glyphicon-remove" style="color: red" ></span><span class="glyphicon glyphicon-time" style="color: orange"></span><span style="margin-left:30px">รอตรวจ</span>
                        <br><span class="glyphicon glyphicon-remove" style="color: red" ></span><span class="glyphicon glyphicon-ok" style="color: green"></span><span style="margin-left:30px">ผ่าน</span>
                        <br><span class="glyphicon glyphicon-remove" style="color: red" ></span><span class="glyphicon glyphicon-remove" style="color: red" ></span><span style="margin-left:30px">ไม่ผ่าน หลังจากแก้ไขแล้ว ขอให้กด submit</span>
                        <br>แจ้งผลการตรวจครั้งที่ 3
                        <br><span class="glyphicon glyphicon-remove" style="color: red" ></span><span class="glyphicon glyphicon-remove" style="color: red" ></span><span class="glyphicon glyphicon-time" style="color: orange"></span><span style="margin-left:15px">  ไม่มีกำหนดเวลาในการแจ้งผลการตรวจในครั้งนี้</span>
                    </p>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<div id="currentdataid" dataid=""></div>
<!-- Modal -->
<?php
$this->registerJs("
    $('#gridID table tbody tr').mouseover(function()
    {
        var id = $(this).attr('data-key');
        // get
//        $.getJSON( '/icf/total/get-data-update?ptid=1', function( json ) {
//            console.log( 'JSON Data: ' + json);
//            console.log( 'JSON Data: ' + json.reg );
//            console.log( 'JSON Data: ' + json.cca01 );
//        });
        $(this).attr('id','datarecord'+id);
        if( $(this).attr('updated')=='1' ){
        
        }else{
            if( $('#currentdataid').attr('dataid')!= id ){
                $('#currentdataid').attr('dataid',id);
                $.ajax({ 
                    type: 'GET', 
                    url: '/icf/total/get-data-update', 
                    data: { id: id }, 
                    success: function (data) { 
                        console.log( 'JSON Data: ' + data);
                        console.log( 'JSON Data: ' + data.reg.icfptid );
                        console.log( 'JSON Data (SQL): ' + data.reg.icfsql );
                        console.log( 'JSON Data (ผลตรวจ): ' + data.reg.icf1 );
                        console.log( 'JSON Data (เวลาตรวจ): ' + data.reg.icft );
                        console.log( 'JSON Data: ' + data.cca01 );
                        console.log( 'JSON Data (CCA-01): ' + data.cca01.f1vdcomp );
                        console.log( 'JSON Data (rstat): ' + data.cca01.rstat );
                        console.log($('#datarecord'+id).attr('data-key'));
                        console.log(data.reg.txtlink);
                        $('#datarecord'+id).find('td:eq(5)').html(data.reg.txtlink);
                        //$(this).attr('updated','1');
                    }
                });
            }
        }
        // ICF
        // $(this).find('td:eq(5)').html(data.reg.txt);
        //$(this).attr('updated','1');
    });
");
?>