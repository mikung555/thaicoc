<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use yii\helpers\Url;
//$this->registerCssFile('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css');
$this->registerCssFile('//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css');
$this->registerCssFile('https://cdn.datatables.net/1.10.15/css/dataTables.bootstrap.min.css');
    
$request = Yii::$app->request;
?>
<div class="table-responsive">
    <div class="tab-content">
        <div class="tab-pane fade in active">
            <table  id="example" class="table table-striped table-bordered" cellspacing="0" width="90%" style="alignment-adjust: central;" >
                <thead>
                    <tr>
                        <th width="auto">
                            ลำดับที่
                        </th>
                        <th width="auto">
                            ชื่อสกุล
                        </th>
                        <th width="auto">จำนวนที่คีย์ได้</th>
                        <th width="auto">แก้ไขล่าสุดเมื่อ</th>
                    </tr>
                </thead>
                <?php
                    if( count($data)>0 ){
                ?>
                <tbody>
                    <?php
                        $irow = 1;
                        foreach($data as $v){
                    ?>
                    <tr>
                        <td style="text-align: right;">
                            <?php echo $irow; ?>
                        </td>
                        <td style="text-align: left;">
                            <?php 
                           
                            echo $v['firstname']; 
                            echo " ";
                            echo $v['lastname'];
                            
                            
                            ?>
                        </td>
                        <td style="text-align: right;">
                            <?php
                            //echo $v['recs'];
                            echo yii\helpers\Html::a($v['recs'],
                                    '#listrecord',
                                    [
                                        'id' => 'linkUserUpdate',
                                        'data-url'=> Url::to(['/icf/ksum/driw-down']),
                                        'ezf-id' => $request->get('id'),
                                        'user_update' => $v['user_update'],
                                        'divresponse' => 'tablerecord',
                                    ]);
                            ?>
                        </td>
                        <td style="text-align: right;">
                            <?php
                            echo $v['max_update'];
                            ?>
                        </td>
                    </tr>
                    <?php
                            $irow ++;
                        }
                    ?>
                </tbody>
                <?php
                    }
                ?>
            </table>
        </div>
        <a name="listrecord"></a>
        <div id="tablerecord">
            
        </div>
    </div>
</div>
<?php
$jsAdd = <<< JS
    $("*[id^=linkUserUpdate]").click(function() {
        $(this).attr("disabled",true);
        var url         = $(this).attr("data-url");
        var div         = $(this).attr("divresponse");
        var id          = $(this).attr("ezf-id");
        var user_update = $(this).attr("user_update");
        var ezf_id      = $(this).attr("ezf-id");
        console.log(id+' '+ ezf_id);
        var dataget={
                id:id,
                ezf_id:ezf_id,
                user_update:user_update,
            };
   
        $.get(url,dataget).done(function( data ) {
            //console.log(data);
            $("#"+div).empty();
            $("#"+div).html(data);
            console.log(data);
        });
    });
JS;
$this->registerJs($jsAdd);
