<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;
use appxq\sdii\widgets\ModalForm;

if( 0 ){
    echo "<pre align='left'>";
    print_r($data);
    echo "</pre>";
}


$request = Yii::$app->request;


//Pjax::begin(['id'=>'inv-person-grid-pjax', 'timeout' => 1000*60]);

?>
<br />
<br />
<br />
<div class="panel panel-default">
    <div class="panel-heading"><h2><b>แสดงรายการข้อมูลที่คีย์เข้ามา</b></h2></div>
    <div class="panel-body">
        <p>สมารถคลิ๊กดูรายละเอียด และคลิ๊กดูข้อมูลที่บันทึกลงใน from ได้</p>
    </div>

    <div class="table-responsive">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>#</th>
                    <th>วันที่แก้ไขล่าสุด</th>
                    <th>rstat</th>
                    <th>HN</th>
                    <th>AN</th>
                    <th>ชื่อ-สกุล</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $irow = 1;
                if( count($data)>0 ){
                    foreach($data as $v){
                ?>
                <tr>
                    <td><?php echo $irow; ?></td>
                    <td>
                        <?php 
                        //echo $v['update_date']; 
                        // '/inputdata/redirect-page',
                        // https://cloudbackend.cascap.in.th/inputdata/step4?comp_id_target=1437725343023632100&ezf_id=1452061550097822200&target=MTM5MTM1Mzg0NDc4OQ%3D%3D&dataid=1421234955865
                        //
                        // popup
                        // <a class="btn-lg open-ezfrom" title="" data-url="/inv/inv-person/ezform-print?ezf_id=1494228552051848100&amp;dataid=1493016772000000004&amp;target=MTQ5MzAxNjc3MjAwMDAwMDAwNA%3D%3D&amp;comp_id_target=1494228847094635900" data-toggle="tooltip" style="cursor: pointer;" data-original-title="แสดงข้อมูล"><i class="glyphicon glyphicon-time"></i></a>
                        // data-url="/inv/inv-person/ezform-print?ezf_id=1494228552051848100&amp;dataid=1493016772000000004&amp;target=MTQ5MzAxNjc3MjAwMDAwMDAwNA%3D%3D&amp;comp_id_target=1494228847094635900"
                        // data-url="/inv/inv-person/ezform-print?ezf_id=1490927197086044600&amp;target=MTQ5MzAxNjc3MjAwMDAwMDAwNQ%3D%3D&amp;dataid=1493016772100000005&amp;comp_id_target=1494228847094635900&amp;readonly=0"
                        // <a class="open-ezfrom open-form btn btn-primary" data-url="/inv/inv-person/ezform-print?ezf_id=1490927197086044600&amp;target=MTQ5MzAxNjc3MjAwMDAwMDAwNQ%3D%3D&amp;dataid=1493016772100000005&amp;comp_id_target=1494228847094635900&amp;readonly=0" data-pjax="1" style="cursor: pointer;" data-toggle="tooltip" data-original-title="แก้ไขข้อมูลนี้"><i class="fa fa-edit"></i> ดู / แก้ไข</a>
                        
                        
                        echo Html::a($v['update_date'],
                                    Url::to([
                                        '/inputdata/step4',
                                        'comp_id_target' => '1492675681064329000',
                                        'ezf_id' => $request->get('ezf_id'),
                                        'target' => base64_encode($v['target']),
                                        'dataid' => $v['id'],
                                        ]),
                                [
                                    'id' => 'linkdata',
                                    'target' => '_blank',
                                ]);
                        /*
                        echo Html::a('<i class="glyphicon glyphicon-plus"></i> จาก '.$v['id'],
                                NULL, 
                                [
                                    'data-url'=>Url::to([
                                        '/inv/inv-person/ezform-print',
                                        'ezf_id' => $request->get('ezf_id'),
                                        'comp_id_target' => '1492675681064329000',
                                        'end' => $inv_main['ezf_id']==$inv_main['main_ezf_id']?1:0,
                                        ]), 
                                    'class' => 'btn btn-success btn-sm open-ezfrom', 
                                    'style'=>'margin-bottom: 6px; cursor: pointer;'
                                    ]);
                        */
                        ?>
                    </td>
                    <td><?php echo $v['rstat']; ?></td>
                    <td><?php echo $v['hn']; ?></td>
                    <td><?php echo $v['an']; ?></td>
                    <td><?php echo $v['fname'].' '.$v['lname']; ?></td>
                </tr>
                <?php
                        $irow++;
                    }
                }
                ?>
            </tbody>
        </table>
    </div>
</div>
<?php //Pjax::end(); ?>
<?=  ModalForm::widget([
    'id' => 'modal-print',
    'size'=>'modal-lg',
    'tabindexEnable'=>false,
    'clientOptions'=>['backdrop'=>'static'],
    'options'=>['style'=>'overflow-y:scroll;']
]);
                
                
$this->registerJs("

$('#modal-print').on('hidden.bs.modal', function (e) {
    if($('#preload').val()>0){
        $('#preload').val(0);
        $.pjax.reload({container:'#inv-person-grid-pjax'});
    }
});

$('#inv-person-grid-pjax').on('click', '.open-ezfrom', function() {
    modalEzfrom($(this).attr('data-url'));
    
});

function modalEzfrom(url) {
    $('#modal-print .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
    $('#modal-print').modal('show');
    $.ajax({
	method: 'POST',
	url: url,
	dataType: 'HTML',
	success: function(result, textStatus) {
	    $('#modal-print .modal-content').html(result);
	    return false;
	}
    });
}

");
?>
