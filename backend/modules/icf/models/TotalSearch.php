<?php

namespace backend\modules\icf\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\icf\models\Total;

/**
 * TotalSearch represents the model behind the search form about `backend\modules\icf\models\Total`.
 */
class TotalSearch extends Total
{
    /**
     * @inheritdoc
     */
    public  $globalSearch;
    public function rules()
    {
        return [
            [['register_ptid', 'register_status', 'cca01_id', 'cca01_status', 'cca02_id', 'cca02_status', 'cca02_new', 'cca02_draft', 'cca02_submitted', 'cca02_1_id', 'cca02_1_status', 'cca02_1_new', 'cca02_1_draft', 'cca02_1_submitted', 'cca03_id', 'cca03_status', 'cca03_new', 'cca03_draft', 'cca03_submitted', 'cca04_id', 'cca04_status', 'cca04_new', 'cca04_draft', 'cca04_submitted', 'cca05_id', 'cca05_status', 'cca05_new', 'cca05_draft', 'cca05_submitted'], 'integer'],
            [['fullcode', 'title','globalSearch','fullname', 'register_error', 'cca01_error', 'cca02_error', 'cca02_1_error', 'cca03_error', 'cca04_error', 'cca05_error'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Total::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pagesize' => 100,
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

//        $query->andFilterWhere([
//            'register_ptid' => $this->register_ptid,
//            'register_status' => $this->register_status,
//            'cca01_id' => $this->cca01_id,
//            'cca01_status' => $this->cca01_status,
//            'cca02_id' => $this->cca02_id,
//            'cca02_status' => $this->cca02_status,
//            'cca02_new' => $this->cca02_new,
//            'cca02_draft' => $this->cca02_draft,
//            'cca02_submitted' => $this->cca02_submitted,
//            'cca02_1_id' => $this->cca02_1_id,
//            'cca02_1_status' => $this->cca02_1_status,
//            'cca02_1_new' => $this->cca02_1_new,
//            'cca02_1_draft' => $this->cca02_1_draft,
//            'cca02_1_submitted' => $this->cca02_1_submitted,
//            'cca03_id' => $this->cca03_id,
//            'cca03_status' => $this->cca03_status,
//            'cca03_new' => $this->cca03_new,
//            'cca03_draft' => $this->cca03_draft,
//            'cca03_submitted' => $this->cca03_submitted,
//            'cca04_id' => $this->cca04_id,
//            'cca04_status' => $this->cca04_status,
//            'cca04_new' => $this->cca04_new,
//            'cca04_draft' => $this->cca04_draft,
//            'cca04_submitted' => $this->cca04_submitted,
//            'cca05_id' => $this->cca05_id,
//            'cca05_status' => $this->cca05_status,
//            'cca05_new' => $this->cca05_new,
//            'cca05_draft' => $this->cca05_draft,
//            'cca05_submitted' => $this->cca05_submitted,
//        ]);

        $query->orFilterWhere(['like', 'fullcode', $this->globalSearch])
            //->orFilterWhere(['like', 'title', $this->globalSearch])
            ->orFilterWhere(['=', 'cid', $this->globalSearch])
            ->orFilterWhere(['like', 'hn', $this->globalSearch])
            ->orFilterWhere(['like', 'fullname', $this->globalSearch]);
//            ->orFilterWhere(['like', 'register_error', $this->globalSearch])
//            ->orFilterWhere(['like', 'cca01_error', $this->globalSearch])
//            ->orFilterWhere(['like', 'cca02_error', $this->globalSearch])
//            ->orFilterWhere(['like', 'cca02_1_error', $this->globalSearch])
//            ->orFilterWhere(['like', 'cca03_error', $this->globalSearch])
//            ->orFilterWhere(['like', 'cca04_error', $this->globalSearch])
//            ->orFilterWhere(['like', 'cca05_error', $this->globalSearch]);

        return $dataProvider;
    }
}
