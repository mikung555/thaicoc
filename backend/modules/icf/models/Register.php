<?php

namespace backend\modules\icf\models;

use Yii;

/**
 * This is the model class for table "tb_data_1".
 *
 * @property string $id
 * @property integer $usmobile
 * @property string $sitecode
 * @property string $ptcode
 * @property string $ptcodefull
 * @property string $ptid_link
 * @property string $ptid_key
 * @property string $reccheck
 * @property string $cid
 * @property integer $cid_check
 * @property string $title
 * @property string $name
 * @property string $surname
 * @property string $uidadd
 * @property string $dadd
 * @property string $uidedit
 * @property string $dedit
 * @property string $regdate
 * @property string $regdatedb
 * @property string $v2
 * @property string $bdatedb
 * @property string $age
 * @property string $v3
 * @property string $mobile
 * @property string $homephone
 * @property string $telcontact1
 * @property string $telcontact2
 * @property string $telcontact3
 * @property string $add1n1
 * @property string $add1n2
 * @property string $add1n3
 * @property string $add1n4
 * @property string $add1n5
 * @property string $add1n6code
 * @property string $add1n6
 * @property string $add1n7code
 * @property string $add1n7
 * @property string $add1n8code
 * @property string $add1n8
 * @property string $add1n9
 * @property string $hospitalcurrent
 * @property string $hn
 * @property string $selfenroll
 * @property string $selfenrolldate
 * @property string $vconsent
 * @property string $vconsentdate
 * @property string $vconsentdatedb
 * @property string $sys_assigncheck
 * @property string $dlastcheck
 * @property string $confirmdate
 * @property string $confirm
 * @property string $venroll
 * @property string $venrolldate
 * @property string $venrolldatedb
 * @property string $pay
 * @property string $paytime
 * @property string $recieve
 * @property string $recievedate
 * @property string $bankdate
 * @property string $sys_dateoficf
 * @property string $sys_dateoficfdb
 * @property string $sys_ecoficf
 * @property string $sys_ecoficfdb
 * @property string $rstat
 * @property string $addr
 * @property string $lat
 * @property string $lng
 * @property string $geocode
 * @property integer $pidcheck
 * @property integer $agecheck
 * @property string $sitezone
 * @property string $siteprov
 * @property string $siteamp
 * @property string $sitetmb
 * @property string $target
 * @property string $xsourcex
 * @property string $user_create
 * @property string $create_date
 * @property string $user_update
 * @property string $update_date
 * @property string $value1
 * @property string $value2
 * @property string $value3
 * @property string $edattype
 * @property string $icf_upload1
 * @property string $icf_upload2
 * @property string $icf_upload3
 * @property string $error
 * @property string $hncode
 * @property string $hsitecode
 * @property string $hptcode
 */
class Register extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tb_data_1';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'addr', 'geocode', 'xsourcex', 'value1', 'value2', 'value3', 'edattype'], 'required'],
            [['id', 'usmobile', 'ptid_link', 'ptid_key', 'cid_check', 'uidadd', 'uidedit', 'sys_assigncheck', 'confirm', 'pay', 'recieve', 'pidcheck', 'agecheck', 'user_create', 'user_update'], 'integer'],
            [['dadd', 'dedit', 'regdatedb', 'v2', 'selfenrolldate', 'vconsentdatedb', 'dlastcheck', 'confirmdate', 'venrolldatedb', 'paytime', 'recievedate', 'bankdate', 'sys_dateoficfdb', 'sys_ecoficfdb', 'create_date', 'update_date'], 'safe'],
            [['geocode', 'error'], 'string'],
            [['sitecode', 'ptcode', 'ptcodefull', 'regdate', 'age', 'vconsentdate', 'venrolldate', 'rstat', 'hsitecode', 'hptcode'], 'string', 'max' => 10],
            [['reccheck', 'v3', 'selfenroll', 'vconsent', 'venroll', 'sitezone', 'siteprov'], 'string', 'max' => 2],
            [['cid', 'bdatedb', 'sys_dateoficf', 'sys_ecoficf', 'xsourcex', 'value1', 'value2', 'value3'], 'string', 'max' => 20],
            [['title', 'add1n1', 'add1n2', 'add1n3', 'add1n4', 'add1n5', 'add1n9'], 'string', 'max' => 50],
            [['name', 'surname', 'mobile', 'homephone', 'telcontact1', 'telcontact2', 'telcontact3', 'add1n6code', 'add1n6', 'add1n7code', 'add1n7', 'add1n8code', 'add1n8', 'hospitalcurrent', 'hn', 'addr', 'target'], 'string', 'max' => 100],
            [['lat', 'lng'], 'string', 'max' => 30],
            [['siteamp'], 'string', 'max' => 4],
            [['sitetmb'], 'string', 'max' => 6],
            [['edattype'], 'string', 'max' => 5],
            [['icf_upload1', 'icf_upload2', 'icf_upload3', 'hncode'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'usmobile' => 'Usmobile',
            'sitecode' => 'Sitecode',
            'ptcode' => 'Ptcode',
            'ptcodefull' => 'Ptcodefull',
            'ptid_link' => 'Ptid Link',
            'ptid_key' => 'Ptid Key',
            'reccheck' => 'Reccheck',
            'cid' => 'Cid',
            'cid_check' => 'Cid Check',
            'title' => 'Title',
            'name' => 'Name',
            'surname' => 'Surname',
            'uidadd' => 'Uidadd',
            'dadd' => 'Dadd',
            'uidedit' => 'Uidedit',
            'dedit' => 'Dedit',
            'regdate' => 'Regdate',
            'regdatedb' => 'Regdatedb',
            'v2' => 'V2',
            'bdatedb' => 'Bdatedb',
            'age' => 'Age',
            'v3' => 'V3',
            'mobile' => 'Mobile',
            'homephone' => 'Homephone',
            'telcontact1' => 'Telcontact1',
            'telcontact2' => 'Telcontact2',
            'telcontact3' => 'Telcontact3',
            'add1n1' => 'Add1n1',
            'add1n2' => 'Add1n2',
            'add1n3' => 'Add1n3',
            'add1n4' => 'Add1n4',
            'add1n5' => 'Add1n5',
            'add1n6code' => 'Add1n6code',
            'add1n6' => 'Add1n6',
            'add1n7code' => 'Add1n7code',
            'add1n7' => 'Add1n7',
            'add1n8code' => 'Add1n8code',
            'add1n8' => 'Add1n8',
            'add1n9' => 'Add1n9',
            'hospitalcurrent' => 'Hospitalcurrent',
            'hn' => 'Hn',
            'selfenroll' => 'Selfenroll',
            'selfenrolldate' => 'Selfenrolldate',
            'vconsent' => 'Vconsent',
            'vconsentdate' => 'Vconsentdate',
            'vconsentdatedb' => 'Vconsentdatedb',
            'sys_assigncheck' => 'Sys Assigncheck',
            'dlastcheck' => 'Dlastcheck',
            'confirmdate' => 'Confirmdate',
            'confirm' => 'Confirm',
            'venroll' => 'Venroll',
            'venrolldate' => 'Venrolldate',
            'venrolldatedb' => 'Venrolldatedb',
            'pay' => 'Pay',
            'paytime' => 'Paytime',
            'recieve' => 'Recieve',
            'recievedate' => 'Recievedate',
            'bankdate' => 'Bankdate',
            'sys_dateoficf' => 'Sys Dateoficf',
            'sys_dateoficfdb' => 'Sys Dateoficfdb',
            'sys_ecoficf' => 'Sys Ecoficf',
            'sys_ecoficfdb' => 'Sys Ecoficfdb',
            'rstat' => 'Rstat',
            'addr' => 'Addr',
            'lat' => 'Lat',
            'lng' => 'Lng',
            'geocode' => 'Geocode',
            'pidcheck' => 'Pidcheck',
            'agecheck' => 'Agecheck',
            'sitezone' => 'Sitezone',
            'siteprov' => 'Siteprov',
            'siteamp' => 'Siteamp',
            'sitetmb' => 'Sitetmb',
            'target' => 'Target',
            'xsourcex' => 'Xsourcex',
            'user_create' => 'User Create',
            'create_date' => 'Create Date',
            'user_update' => 'User Update',
            'update_date' => 'Update Date',
            'value1' => 'Value1',
            'value2' => 'Value2',
            'value3' => 'Value3',
            'edattype' => 'Edattype',
            'icf_upload1' => 'Icf Upload1',
            'icf_upload2' => 'Icf Upload2',
            'icf_upload3' => 'Icf Upload3',
            'error' => 'Error',
            'hncode' => 'Hncode',
            'hsitecode' => 'Hsitecode',
            'hptcode' => 'Hptcode',
        ];
    }
}
