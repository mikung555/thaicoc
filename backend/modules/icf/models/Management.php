<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 3/3/2016
 * Time: 5:27 PM
 */

namespace backend\modules\icf\models;
use yii\base\Model;

class Management extends Model
{
    public $register_id;
    public $ezf_id_register='1437377239070461301';
    public $fullcode;
    public $fullname;
    public $status;
    public $cca01;
    public $cca02;
    public $cca01_status;
    public $cca02_status;
    public function attributeLabels()
    {
        return[
            'fullcode' =>'Full Code',
            'fullname' =>'Full Name',
            'status' => 'Status',
            'register_id' => 'Register ID',
            'ezf_id_register' => 'Ezf_ID',
            'cca01_id' => 'CCA-01 ID',
            'cca02_id' => 'CCA-02 ID',
            'cca01_status' => 'CCA-01 Status',
            'cca02_status' => 'CCA-02',
        ];

    }
    public function rules()
    {
       return[
           [['fullcode','fullname','register_id','ezf_id_register'],'required'],
           [['fullcode','fullname'],'safe'],
           [['status','cca01_id','cca02_id','cca01_status','cca02_status'],'string']

       ];

    }
}