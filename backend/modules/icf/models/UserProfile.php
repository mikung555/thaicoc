<?php

namespace backend\modules\icf\models;

use common\models\User;
use Yii;

/**
 * This is the model class for table "user_profile".
 *
 * @property string $user_id
 * @property string $firstname
 * @property string $middlename
 * @property string $lastname
 * @property string $avatar_path
 * @property string $avatar_base_url
 * @property string $locale
 * @property integer $nation
 * @property string $cid
 * @property integer $gender
 * @property string $sitecode
 * @property string $email
 * @property string $telephone
 * @property string $title
 * @property integer $status
 * @property integer $status_personal
 * @property integer $status_manager
 * @property string $status_other
 * @property string $department
 * @property string $department_nation_text
 * @property string $department_area
 * @property string $department_area_text
 * @property string $department_province
 * @property string $department_province_text
 * @property string $department_amphur
 * @property string $department_amphur_text
 * @property string $department_group
 * @property integer $activate
 * @property string $address_province
 * @property string $address_amphur
 * @property string $address_tambon
 * @property string $address_text
 * @property string $citizenid_file
 * @property string $secret_file
 * @property string $item_name
 *
 * @property User $user
 */
class UserProfile extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_profile';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['locale'], 'required'],
            [['nation', 'gender', 'status', 'status_personal', 'status_manager', 'activate'], 'integer'],
            [['citizenid_file', 'secret_file'], 'string'],
            [['firstname', 'middlename', 'lastname', 'avatar_path', 'avatar_base_url', 'email', 'telephone', 'status_other', 'department_nation_text', 'department_area_text', 'department_amphur_text', 'address_text'], 'string', 'max' => 255],
            [['locale'], 'string', 'max' => 32],
            [['cid', 'sitecode', 'title', 'department', 'department_area', 'department_province', 'department_province_text', 'department_amphur', 'department_group', 'item_name'], 'string', 'max' => 100],
            [['address_province', 'address_amphur', 'address_tambon'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_id' => 'User ID',
            'firstname' => 'Firstname',
            'middlename' => 'Middlename',
            'lastname' => 'Lastname',
            'avatar_path' => 'Avatar Path',
            'avatar_base_url' => 'Avatar Base Url',
            'locale' => 'Locale',
            'nation' => 'Nation',
            'cid' => 'Cid',
            'gender' => 'Gender',
            'sitecode' => 'Sitecode',
            'email' => 'Email',
            'telephone' => 'Telephone',
            'title' => 'Title',
            'status' => 'Status',
            'status_personal' => 'Status Personal',
            'status_manager' => 'Status Manager',
            'status_other' => 'Status Other',
            'department' => 'Department',
            'department_nation_text' => 'Department Nation Text',
            'department_area' => 'Department Area',
            'department_area_text' => 'Department Area Text',
            'department_province' => 'Department Province',
            'department_province_text' => 'Department Province Text',
            'department_amphur' => 'Department Amphur',
            'department_amphur_text' => 'Department Amphur Text',
            'department_group' => 'Department Group',
            'activate' => 'Activate',
            'address_province' => 'Address Province',
            'address_amphur' => 'Address Amphur',
            'address_tambon' => 'Address Tambon',
            'address_text' => 'Address Text',
            'citizenid_file' => 'Citizenid File',
            'secret_file' => 'Secret File',
            'item_name' => 'Item Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
