<?php

namespace backend\modules\icf\controllers;

use Yii;
use backend\modules\icf\models\Total;
use yii\data\ActiveDataProvider;
use yii\helpers\VarDumper;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\modules\icf\models\UserProfile;
use \yii\web\Controller;
use backend\modules\icf\models\TotalSearch;
use backend\modules\icf\classes\DataRegister;
use backend\modules\icf\classes\DataCca01;
use backend\modules\icf\classes\DataCca02;
use backend\modules\icf\classes\ICFFunc;
use backend\modules\icf\classes\DataResaveDaftLogStat;
use yii\web\Response;
use yii\helpers\Json;

/**
 * TotalController implements the CRUD actions for Total model.
 */
class TotalController extends Controller {

    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Total models.
     * @return mixed
     */
    public function checkTempTableUpdate($hsitecode) {
        $sql = 'select if(if(max(create_date)>max(update_date),max(create_date),max(update_date))>max(dlastcheck)';
        $sql .= ',if(max(create_date)>max(update_date),max(create_date),max(update_date))';
        $sql .= ',max(dlastcheck))  as updatedate ';
        $sql .= 'from tb_data_1 where hsitecode="' . addslashes($hsitecode) . '"; ';
        //echo "<br />";
        //echo "<br />";
        //echo "<br />";
        //echo $sql;
        $result = Yii::$app->db->createCommand($sql)->queryOne();
        if ($result) {
            $out = $result;
        }

        $lastEdit = $out['updatedate'];
        $sql = 'select max(update_date) as updatedate ';
        $sql .= 'from cascap_log.`view_cascapjobcontrol_v201606` ';
        $sql .= 'where hsitecode="' . addslashes($hsitecode) . '"; ';
        $result = Yii::$app->db->createCommand($sql)->queryOne();
        if ($result) {
            $out = $result;
        }
        $lastView = $out['updatedate'];

        //$start_date = $lastView;
        //$end_date = $lastEdit;
        $timeLastView = new \DateTime($lastView);
        $timeLastEdit = new \DateTime($lastEdit);
        // Return from function
        if (strlen($lastView) == 0 || strlen($lastEdit) == 0) {
            return false;
        } else if ($timeLastView < $timeLastEdit) {
            // do something
            // มีแก้ไขเข้ามาใหม่ 
            return false;
        } else {
            // do something else
            // ข้อมูลไม่มีแก้ไขมาใหม่
            return true;
        }
    }
    
    public function actionGetDataUpdate() {
        Yii::$app->response->format = Response::FORMAT_JSON;
        
        $data[reg] = DataRegister::getCheckICF(Yii::$app->request->get('id'));
        $data[cca01] = DataCca01::getCheckCCA01(Yii::$app->request->get('id'));
        return $data;
    }

    public function actionIndex() {
        $userId = \Yii::$app->user->identity->id;
        $sitecode = \Yii::$app->user->identity->userProfile->sitecode;
        $userProfile = UserProfile::find()
                ->where(['user_id' => $userId])
                ->one();

        if ($_GET['reload'] == '1') {
            $sql = 'delete from cascap_log.view_cascapjobcontrol_v201606 where hsitecode like "' . $sitecode . '" ';
            \Yii::$app->db->createCommand($sql)->execute();
        }
        $calltemview = self::checkTempTableUpdate($sitecode);
        $tempTableName = "icf_" . str_replace('.', '', microtime(true));
        if ($calltemview) {
            //echo '<br />True;';
            $addfield = '`hsitecode` varchar(20) COLLATE utf8_unicode_ci NOT NULL,';
            $addfield .= '`update_date` datetime DEFAULT NULL,';
        } else {
            //echo '<br />False;';
            $addfield = '';
        }
        $sqlCreateTempTable = "CREATE TEMPORARY TABLE " . $tempTableName . "( " . $addfield . "
                                register_ptid BIGINT(21),
                                fullcode VARCHAR(20),
                                cid VARCHAR(20),
                                title VARCHAR(50),
                                fullname VARCHAR(255),
                                register_status TINYINT(2),
                                register_error MEDIUMTEXT,
                                cca01_id BIGINT(21),
                                cca01_status TINYINT(2),
                                cca01_error MEDIUMTEXT,
                                cca02_id BIGINT(21),
                                cca02_status TINYINT(2),
                                cca02_new INT(3),
                                cca02_draft INT(3),
                                cca02_submitted INT(3),
                                cca02_error MEDIUMTEXT,
                                cca02_1_id BIGINT(21),
                                cca02_1_status TINYINT(2),
                                cca02_1_new INT(3),
                                cca02_1_draft INT(3),
                                cca02_1_submitted INT(3),
                                cca02_1_error MEDIUMTEXT,
                                cca03_id BIGINT(21),
                                cca03_status TINYINT(2),
                                cca03_new INT(3),
                                cca03_draft INT(3),
                                cca03_submitted INT(3),
                                cca03_error MEDIUMTEXT,
                                cca04_id BIGINT(21),
                                cca04_status TINYINT(2),
                                cca04_new INT(3),
                                cca04_draft INT(3),
                                cca04_submitted INT(3),
                                cca04_error MEDIUMTEXT,
                                cca05_id BIGINT(21),
                                cca05_status TINYINT(2),
                                cca05_new INT(3),
                                cca05_draft INT(3),
                                cca05_submitted INT(3),
                                cca05_error MEDIUMTEXT,
                                hn VARCHAR(50),
                                PRIMARY KEY (register_ptid),
                                INDEX (register_ptid)
                            )CHARACTER SET utf8 COLLATE utf8_unicode_ci;";
        \Yii::$app->db->createCommand($sqlCreateTempTable)->execute();
        if ($calltemview) {
            // กรณี แบบอัตโนมัต //
            $sql = 'replace into ' . $tempTableName . ' ';
            $sql .= 'select * from cascap_log.`view_cascapjobcontrol_v201606` ';
            $sql .= 'where hsitecode="' . $sitecode . '" ';
            $sql .= 'order by fullcode ';
            \Yii::$app->db->createCommand($sql)->execute();
        } else {
            /*
             * register_status
             * 1: ตรวจผ่าน
             * 2: Submit แล้ว และเป็นการตรวจครั้งแรก
             * 3: ตรวจแล้วไม่ผ่าน *** กรณีตรวจแล้วไม่ผ่าน จะมีที่ไม่มีการ Up บัตรด้วย
             * 4: มี Error หรือ ยังไม่ Upload ใบยินยอม
             * 5: เอกสารครบ แต่ยังไม่ Submit
             * 6: ตรวจครั้งแรกไม่ผ่าน ได้แก้ไขมาเพื่อส่งตรวจครั้งที่ 2 แล้ว
             * 
             * มีการตรวจมากกว่า 1 ครั้ง
             *  ตรวจครั้งที่ 1
             *  ตรวจครั้งที่ 2
             *  ตรวจครั้งที่ x
             *  กรณีครั้งสุดท้ายที่ไม่มีการนับ หรือ ครั้งเดียวกับ x
             * เปลี่ยนสถานะใหม่
             */
            $sql = "replace into " . $tempTableName . " select
                    ptid as register_ptid,
                    concat(hsitecode,hptcode) as fullcode,
                    cid,
                    title,
                    concat(name,' ',surname) as fullname,
                    case 1
                    when (confirm>0) then '1'
                    when ((confirm is null) AND (length(icf_upload1)>0 ) AND (rstat=2) ) then '2'
                    when ((confirm=0) AND (dlastcheck is null ) AND (rstat=2) ) then '2'
                    when ((confirm=0) AND (length(icf_upload1)>0) AND dlastcheck>update_date) then '3'
                    when((length(error)>0)OR(icf_upload1 is null)OR(length(icf_upload1)=0)) then '4'
                    when((length(error)<0) AND (length(icf_upload1)>0 ) AND (rstat=1))   then '5'
                    when (rstat='2' && confirm='0' AND dlastcheck<update_date) then '6'
                    end as register_status,
                    error AS register_error,
                    NULL AS cca01_id,
                    NULL AS cca01_status,
                    NULL AS cca01_error,
                    NULL AS cca02_id,
                    NULL AS cca02_status,
                    NULL AS cca02_new,
                    NULL AS cca02_draft,
                    NULL AS cca02_submitted,
                    NULL AS cca02_error,
                    NULL AS cca02_1_id,
                    NULL AS cca02_1_status,
                    NULL AS cca02_1_new,
                    NULL AS cca02_1_draft,
                    NULL AS cca02_1_submitted,
                    NULL AS cca02_1_error,
                    NULL AS cca03_id,
                    NULL AS cca03_status,
                    NULL AS cca03_new,
                    NULL AS cca03_draft,
                    NULL AS cca03_submitted,
                    NULL AS cca03_error,
                    NULL AS cca04_id,
                    NULL AS cca04_status,
                    NULL AS cca04_new,
                    NULL AS cca04_draft,
                    NULL AS cca04_submitted,
                    NULL AS cca04_error,
                    NULL AS cca05_id,
                    NULL AS cca05_status,
                    NULL AS cca05_new,
                    NULL AS cca05_draft,
                    NULL AS cca05_submitted,
                    NULL AS cca05_error,
                    hncode as hn
                    from tb_data_1
                    where hsitecode=:sitecode
                    AND rstat <>'3'
                    ORDER BY fullcode";
            \Yii::$app->db->createCommand($sql, [':sitecode' => $userProfile->sitecode])->execute();
            $sqlCCA01 = "UPDATE " . $tempTableName . "
                            INNER JOIN tb_data_2 ON " . $tempTableName . ".register_ptid = tb_data_2.ptid
                            SET cca01_id = tb_data_2.id,
                                cca01_status =
                                CASE 1
                                    WHEN tb_data_2.id IS NULL THEN 0
                                    WHEN tb_data_2.rstat='2' THEN 2
                                    WHEN LENGTH(tb_data_2.error)>0 or tb_data_2.rstat='0' or tb_data_2.rstat='1' THEN 1
                                ELSE  2
                            END,
                                cca01_error = tb_data_2.error WHERE tb_data_2.rstat<>3";
            \Yii::$app->db->createCommand($sqlCCA01)->execute();
            $sqlCCA02 = "UPDATE " . $tempTableName . "
                            INNER JOIN
                            (
                                SELECT
                                    *,
                                    (SELECT error FROM tb_data_3 WHERE id = maxid) AS maxerror
                                FROM
                                (
                                    SELECT
                                        ptid,
                                        MAX(id) AS maxid,
                                        SUM(rstat = 0) AS new,
                                        SUM(rstat = 1) AS draft,
                                        SUM(rstat = 2) AS submit

                                    FROM
                                        tb_data_3
                                    WHERE
                                        rstat<>'3'
                                    GROUP BY
                                        ptid
                                ) AS t
                            ) AS t ON " . $tempTableName . ".register_ptid = t.ptid
                            SET
                             cca02_status = IF(LENGTH(t.maxerror)>0, 1,0),
                             cca02_id = t.maxid,
                             cca02_new = t.new,
                             cca02_draft = t.draft,
                             cca02_submitted = t.submit,
                             cca02_error = t.maxerror
                            ";
            \Yii::$app->db->createCommand($sqlCCA02)->execute();
            $sqlCCA02_1 = "UPDATE " . $tempTableName . "
                            INNER JOIN
                            (
                                SELECT
                                    *,
                                    (SELECT error FROM tb_data_4 WHERE id = maxid) AS maxerror
                                FROM
                                (
                                    SELECT
                                        ptid,
                                        MAX(id) AS maxid,
                                        SUM(rstat = 0) AS new,
                                        SUM(rstat = 1) AS draft,
                                        SUM(rstat = 2) AS submit

                                    FROM
                                        tb_data_4
                                    WHERE
                                        rstat<>'3'
                                    GROUP BY
                                        ptid
                                ) AS t
                            ) AS t ON " . $tempTableName . ".register_ptid = t.ptid
                            SET
                             cca02_1_status = IF(LENGTH(t.maxerror)>0, 1,0),
                             cca02_1_id = t.maxid,
                             cca02_1_new = t.new,
                             cca02_1_draft = t.draft,
                             cca02_1_submitted = t.submit,
                             cca02_1_error = t.maxerror";
            \Yii::$app->db->createCommand($sqlCCA02_1)->execute();
            $sqlCCA03 = "UPDATE " . $tempTableName . "
                        INNER JOIN
                        (
                            SELECT
                                *,
                                (SELECT error FROM tb_data_7 WHERE id = maxid) AS maxerror
                            FROM
                            (
                                SELECT
                                    ptid,
                                    MAX(id) AS maxid,
                                    SUM(rstat = 0) AS new,
                                    SUM(rstat = 1) AS draft,
                                    SUM(rstat = 2) AS submit

                                FROM
                                    tb_data_7
                                WHERE
                                    rstat<>'3'
                                GROUP BY
                                    ptid
                            ) AS t
                        ) AS t ON " . $tempTableName . ".register_ptid = t.ptid
                        SET
                         cca03_status = IF(LENGTH(t.maxerror)>0, 1,0),
                         cca03_id = t.maxid,
                         cca03_new = t.new,
                         cca03_draft = t.draft,
                         cca03_submitted = t.submit,
                         cca03_error = t.maxerror";
            \Yii::$app->db->createCommand($sqlCCA03)->execute();
            $sqlCCA04 = "UPDATE " . $tempTableName . "
                            INNER JOIN
                            (
                                SELECT
                                    *,
                                    (SELECT error FROM tb_data_8 WHERE id = maxid) AS maxerror
                                FROM
                                (
                                    SELECT
                                        ptid,
                                        MAX(id) AS maxid,
                                        SUM(rstat = 0) AS new,
                                        SUM(rstat = 1) AS draft,
                                        SUM(rstat = 2) AS submit

                                    FROM
                                        tb_data_8
                                    WHERE
                                        rstat<>'3'
                                    GROUP BY
                                        ptid
                                ) AS t
                            ) AS t ON " . $tempTableName . ".register_ptid = t.ptid
                            SET
                             cca04_status = IF(LENGTH(t.maxerror)>0, 1,0),
                             cca04_id = t.maxid,
                             cca04_new = t.new,
                             cca04_draft = t.draft,
                             cca04_submitted = t.submit,
                             cca04_error = t.maxerror";
            \Yii::$app->db->createCommand($sqlCCA04)->execute();
            $sqlCCA05 = "UPDATE " . $tempTableName . "
                            INNER JOIN
                            (
                                SELECT
                                    *,
                                    (SELECT error FROM tb_data_11 WHERE id = maxid) AS maxerror
                                FROM
                                (
                                    SELECT
                                        ptid,
                                        MAX(id) AS maxid,
                                        SUM(rstat = 0) AS new,
                                        SUM(rstat = 1) AS draft,
                                        SUM(rstat = 2) AS submit

                                    FROM
                                        tb_data_11
                                    WHERE
                                        rstat<>'3'
                                    GROUP BY
                                        ptid
                                ) AS t
                            ) AS t ON " . $tempTableName . ".register_ptid = t.ptid
                            SET
                             cca05_status = IF(LENGTH(t.maxerror)>0, 1,0),
                             cca05_id = t.maxid,
                             cca05_new = t.new,
                             cca05_draft = t.draft,
                             cca05_submitted = t.submit,
                             cca05_error = t.maxerror";
            \Yii::$app->db->createCommand($sqlCCA05)->execute();

            $sqlQuery = "UPDATE " . $tempTableName . "
                            INNER JOIN
                            cascap_log.query_confirmq as q ON " . $tempTableName . ".register_ptid = q.ptid
                            SET
                             register_error = concat(ifnull('',register_error),if(q.comment is null,'',q.comment)) ";
            \Yii::$app->db->createCommand($sqlQuery)->execute();

            // save to log table
            //$current_time = Yii::$app->formatter->asDate($time, 'yyyyMMddkkmmss');
            // ".$sitecode."_".$tempTableName."
            // clear old data
            $sqlLogView = "delete from cascap_log.`view_cascapjobcontrol_v201606` where hsitecode='" . $sitecode . "' ";
            \Yii::$app->db->createCommand($sqlLogView)->execute();
            // insert new data
            $sqlLogView = "replace into cascap_log.`view_cascapjobcontrol_v201606` select '" . $sitecode . "',NOW()," . $tempTableName . ".* from " . $tempTableName . " ";
            \Yii::$app->db->createCommand($sqlLogView)->execute();
        }

        $searchModel = new TotalSearch();
        $searchModel->setTableName($tempTableName);

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $errorRegisterCount = 0;
        $waitingRegisterCount = 0;
        $successRegisterCount = 0;
        $notSuccessConfirmRegisterCount = 0;
        $model = Total::find()->all();
        $countAllRecord = count($model);
        $allrow = 0;
        /*
         * 1: ตรวจแล้วผ่าน
         * 2: ยังไม่ตรวจ Submit มาแล้ว
         * 3: ตรวจแล้วไม่ผ่าน
         * 4: มี Error หรือ ยังไม่ Upload (*)
         * 5: ยังไม่ Submit ข้อมูลสมบูรณ์แล้ว
         * 6: แก้ไข มาแล้ว หลังจากตรวจไปแล้ว
         * 
         * 
         * when (confirm>0) then '1'
          when ((confirm is null) AND (length(icf_upload1)>0 ) AND (rstat=2)) then '2'
          when ((confirm=0) AND (length(icf_upload1)>0) AND dlastcheck>update_date) then '3'
          when((length(error)>0)OR(icf_upload1 is null)OR(length(icf_upload1)=0)) then '4'
          when((length(error)<0) AND (length(icf_upload1)>0 ) AND (rstat=1))   then '5'
          when (rstat='2' && confirm='0' AND dlastcheck<update_date) then '6'
         */
        foreach ($model as $row) {
            if ($row['register_status'] == '1') {
                $successRegisterCount++;
            } elseif ($row['register_status'] == '2') {
                $waitingRegisterCount++;
            } elseif ($row['register_status'] == '3') {
                $notSuccessConfirmRegisterCount++;
            } elseif ($row['register_status'] == '4') {
                $errorRegisterCount++;
            } elseif ($row['register_status'] == '6') {
                $waitingRegisterCount++;
            } else {
                $otherRegisterCount++;
            }
            $allrow++;
        }
        $action = \Yii::$app->request->get('action');
        $status = 0;
        if ($action == 'register_error') {
            $dataProvider = new ActiveDataProvider([
                'query' => Total::find()->where(['register_status' => '4']),
                'pagination' => [
                    'pageSize' => 100,
                ]
            ]);
            $status = 1;
        } elseif ($action == 'waiting') {
            $total = Total::find()->where('register_status=:rs OR register_status=:ss', [':rs' => 2, ':ss' => 6]);
            $dataProvider = new ActiveDataProvider([
                'query' => $total, //Total::find()->orWhere (['register_status'=>'2','register_status'=>'6']),
                'pagination' => [
                    'pageSize' => 100,
                ]
            ]);
            $status = 1;
        } elseif ($action == 'complete') {
            $dataProvider = new ActiveDataProvider([
                'query' => Total::find()->where(['register_status' => '1']),
                'pagination' => [
                    'pageSize' => 100,
                ]
            ]);
            $status = 1;
        } elseif ($action == 'not_complete') {
            $dataProvider = new ActiveDataProvider([
                'query' => Total::find()->where(['register_status' => '3']),
                'pagination' => [
                    'pageSize' => 100,
                ]
            ]);
            $status = 1;
        }

        $getLastpage = \Yii::$app->request->get('page'); //$_GET["page"]+1;
        $getPerpage = \Yii::$app->request->get('per-page'); //$_GET["per-page"];
        if ($getPerpage < 1) {
            $getPerpage = 100;
        }
        $pageMaxpage = ceil($countAllRecord / $getPerpage);
        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'errorRegisterCount' => $errorRegisterCount,
                    'waitingRegisterCount' => $waitingRegisterCount,
                    'successRegisterCount' => $successRegisterCount,
                    'notSuccessConfirmRegisterCount' => $notSuccessConfirmRegisterCount,
                    'sitecode' => $sitecode,
                    'getLastpage' => $getLastpage,
                    'getCurrentpage' => $getLastpage,
                    'getPerpage' => $getPerpage,
                    'modelAllrow' => $countAllRecord,
                    'pageMaxpage' => $pageMaxpage,
                    'status' => $status
        ]);
    }

    public function actionPopUpEmr() {
        $request = Yii::$app->request;
        $userSiteCode = Yii::$app->user->identity->userProfile->sitecode;
        $icf = ICFFunc::callICFStatus('ptid', $request->get('id'));
        $txtICFStatus = ICFFunc::getICFStatus($icf['icf_status'], $icf['icf_status_wait2']);
        $reg = DataRegister::getListByPtid($request->get('id'));
        $hosp = DataRegister::getHospitalRegistered($reg);
        $cca01 = DataCca01::getListByPtid($request->get('id'));
        $cca02 = DataCca02::getListByPtid($request->get('id'));
        return $this->renderAjax('_popupPatientEMR', [
                    'userSiteCode' => $userSiteCode,
                    'hosp' => $hosp,
                    'reg' => $reg,
                    'icf' => $icf,
                    'txtICFStatus' => $txtICFStatus,
                    'cca01' => $cca01,
                    'cca02' => $cca02,
        ]);
    }
    
    public function actionHistoryDraft() {
        $request = Yii::$app->request;
        $dataid = $request->get('dataid');
        $dataid2 = $request->get('dataid2');
        $ezf_id = $request->get('ezf_id');
        $divresponse = $request->get('div');

        $data = DataResaveDaftLogStat::loadHistory($dataid,$dataid2,$ezf_id,$divresponse);
        return $this->renderAjax('_historyResaveDraft',$data);
    }
    

    public function actionConfirmReSaveDraftWaiting() {
        $request = Yii::$app->request;
        $dataid = $request->get('dataid');
        $dataid2 = $request->get('dataid2');
        $ezf_id = $request->get('ezf_id');
        $divresponse = $request->get('div');
        $pageRender = "_confirmReSaveDraft";
        $sqllog = " SELECT dadd,field_label, rstat_org, rstat_new, username, fullname "
                . " FROM log_rstat WHERE data_id='$dataid' order by dadd DESC LIMIT 1";
        $resultlog = \Yii::$app->db->createCommand($sqllog)->queryAll();
        if (count($resultlog) > 0) {
            $pageRender = '_historyResaveDraft';
        }

        foreach ($resultlog as $val) {
            $dadd = $val['dadd'];
            $rstat_org = $val['rstat_org'];
            $rstat_new = $val['rstat_new'];
            $fullname = $val['fullname'];
            $field_label = $val['field_label'];
        }
        //if($resultlog)
        return $this->renderAjax($pageRender, [
                    'resultlog'=>$resultlog,
                    'dataid' => $dataid,
                    'dataid2' => $dataid2,
                    'ezf_id' => $ezf_id,
                    'divresponse' => $divresponse,
                    'rstat_org' => $rstat_org,
                    'rstat_new' => $rstat_new,
                    'fullname' => $fullname,
                    'field_label' => $field_label,
                    'dadd' => $dadd
        ]);
    
    }

    public function actionConfirmReSaveDraft(){
        $request = Yii::$app->request;
        $dataid = $request->get('dataid');
        $dataid2 = $request->get('dataid2');
        $ezf_id = $request->get('ezf_id');
        $divresponse = $request->get('div');
        
        DataResaveDaftLogStat::onResaving($dataid,$ezf_id,$divresponse);
        $data = DataResaveDaftLogStat::loadHistory($dataid,$dataid2,$ezf_id,$divresponse);

        return $this->renderAjax('_historyResaveDraft',$data);
    }

    /**
     * Displays a single Total model.
     * @param string $id
     * @return mixed
     */
//    public function actionView($id)
//    {
//        return $this->render('view', [
//            'model' => $this->findModel($id),
//        ]);
//    }
//
//    /**
//     * Creates a new Total model.
//     * If creation is successful, the browser will be redirected to the 'view' page.
//     * @return mixed
//     */
//    public function actionCreate()
//    {
//        $model = new Total();
//
//        if ($model->load(Yii::$app->request->post()) && $model->save()) {
//            return $this->redirect(['view', 'id' => $model->register_ptid]);
//        } else {
//            return $this->render('create', [
//                'model' => $model,
//            ]);
//        }
//    }
//
//    /**
//     * Updates an existing Total model.
//     * If update is successful, the browser will be redirected to the 'view' page.
//     * @param string $id
//     * @return mixed
//     */
//    public function actionUpdate($id)
//    {
//        $model = $this->findModel($id);
//
//        if ($model->load(Yii::$app->request->post()) && $model->save()) {
//            return $this->redirect(['view', 'id' => $model->register_ptid]);
//        } else {
//            return $this->render('update', [
//                'model' => $model,
//            ]);
//        }
//    }
//
//    /**
//     * Deletes an existing Total model.
//     * If deletion is successful, the browser will be redirected to the 'index' page.
//     * @param string $id
//     * @return mixed
//     */
//    public function actionDelete($id)
//    {
//        $this->findModel($id)->delete();
//
//        return $this->redirect(['index']);
//    }
//
//    /**
//     * Finds the Total model based on its primary key value.
//     * If the model is not found, a 404 HTTP exception will be thrown.
//     * @param string $id
//     * @return Total the loaded model
//     * @throws NotFoundHttpException if the model cannot be found
//     */
    protected function findModel($id) {
        if (($model = Total::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    

}
