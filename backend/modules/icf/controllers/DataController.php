<?php

namespace backend\modules\icf\controllers;

class DataController extends \yii\web\Controller
{
    public function actionIndex()
    {
        return $this->render('index');
    }
    
    public function actionTable()
    {
        return $this->render('_table');
    }

}
