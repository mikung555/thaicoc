<?php

namespace backend\modules\icf\controllers;

class Cca02Controller extends \yii\web\Controller
{
    public function actionIndex()
    {
        $_webserver = $_SERVER;
        return $this->render('index', [
            '_webserver' => $_webserver,
        ]);
    }

}
