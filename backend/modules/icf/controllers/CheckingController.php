<?php

namespace backend\modules\icf\controllers;
use Yii;

class CheckingController extends \yii\web\Controller
{
    public function actionIndex()
    {
        return $this->render('index');
    }
    
    public function actionIcf()
    {
        // test patient id
        $dataid = $_GET['dataid'];
        $cca = Yii::$app->db->createCommand("select f1v2, f1vdcomp, rstat from tb_data_2 where ptid = :ptid
                    ", [':ptid'=>$_GET['dataid'] ])->queryOne(); // f1v2 วันเกิด , f1vdcomp วันตรวจม rstat สถานะ
        
        $ezyformid = ['ccaezfid'=>'1437377239070461302','regezfid'=>'1437377239070461301'];
        // get data from tb_data_1
        
        $patient = Yii::$app->db->createCommand("select ptb.*, cd.DISTRICT_NAME as district_name, ca.AMPHUR_NAME as amphur_name , cp.PROVINCE_NAME as province_name  from tb_data_1 ptb 
        inner join const_district cd on cd.DISTRICT_CODE = ptb.add1n6code
        inner join const_amphur ca on ca.AMPHUR_CODE = ptb.add1n7code
        inner join const_province cp on cp.PROVINCE_CODE = ptb.add1n8code
        where ptb.ptid= :ptid ", [':ptid'=>$_GET['dataid'] ])->queryAll();
       
        $imglist =['1', '2', '3']; // set input file upload column 1, 2 and 3 
        $datelist=[['val'=>'31/12/2600', 'text'=>'!ไม่มีตราประทับจาก EC'], ['val'=>'09/02/2556', 'text'=>'9 ก.พ. 2556'],
                    ['val'=>'20/12/2556', 'text'=>'20 ธ.ค. 2556 '],['val'=>'20/05/2557', 'text'=>'20 พ.ค. 2557'],['val'=>'29/03/2560', 'text'=>'29 มี.ค. 2560']];
        
        $officer =Yii::$app->db->createCommand("select u.firstname, u.middlename, u.lastname, u.telephone, u.email, u.citizenid_file, u.cid, ah.name as hospname 
                                    from user_profile u inner join 
                                    all_hospital_thai ah on ah.hcode = u.sitecode
                                    where user_id = :userid", [':userid'=>$patient[0]['user_create'] ])->queryOne(); 
                
        $accepthis =Yii::$app->dbcascap->createCommand("select *  from query_confirmq where ptid = :ptid", [':ptid'=>$dataid])->queryOne(); 
        $checkinghis = Yii::$app->dbcascap->createCommand("select *  from query_confirmq where qid_key = :qid", [':qid'=>$accepthis['qid'] ])->queryAll(); 
        
        return $this->render('icf', ['cca'=>$cca, 'patient'=>$patient, 'imglist' =>$imglist, 'officer' => $officer, 'accepthis'=> $accepthis, 
            'checkinghis' =>$checkinghis, 'datelist'=>$datelist,' ezyformid' =>$ezyformid]);
    }
    public function actionSubmitLoad(){
        
        return $this->renderAjax('_icf_submit_query', ['dataid'=>$dataid]);
    }
    public function actionSubmitQuery(){
        $chpass = $_POST['chpass'];
        $chpassid = $_POST['chpassid'];
        $arraykey = array_keys($chpassid);
        $comment = $_POST['comment'];
        
        $q_data_id = $_POST['q_data_id'];
        $q_target_id = $_POST['q_target_id'];
        $q_userkey = $_POST['q_userkey'];
        $q_xsourcex = $_POST['q_xsourcex'];
        $sitecode = $_POST['crf_sitecode'];
        $ptcode = $_POST['crf_ptcode'];
       
        $qid = $this->setTimeCode() ; // 13 digit // microtime
        $qtype_main = 'q';
        $qtype_ans = 'a';
        $var = 'icf';
        $value_main = '3';
        $encodeutf8 = '1';
        
        $userid = \Yii::$app->user->identity->id;
        
        // insert test value 
        // get array index to new array
        
        $remote_address = $_SERVER['REMOTE_ADDR'];
        $http_x_real_ip = $_SERVER['HTTP_X_REAL_IP'];
        $http_forwarded_for = $_SERVER['HTTP_X_FORWARDED_FOR'];
        $http_host = $_SERVER['HTTP_HOST'];
        $server_addr = $_SERVER['SERVER_ADDR'];
        $content_length = $_SERVER['CONTENT_LENGTH'];
        $http_accept = $_SERVER['HTTP_ACCEPT'];
        $http_user_agent = $_SERVER['HTTP_USER_AGENT'];
        // select data from data table database
        
        $icf_old_qid = Yii::$app->dbcascap->createCommand("select qid from query_confirmq where qtype = 'q' and sitecode= :sitecode and ptcode = :ptcode and ptid = :ptid and var = 'icf' and value = '3'", 
                [':sitecode'=>$sitecode, ':ptcode'=>$ptcode, ':ptid'=>$q_target_id ])->queryScalar();
        // replace data from chpass
        // encode to latin-1
        //$commentlatin = iconv('utf-8', 'ISO-8859-11', $comment);
        $answerlist = [];
        foreach($arraykey as $a){
            $answerlist[] = $a;
        }
        $q = "select * from query_message_error where id in (".implode(',', $answerlist).")";
        $answerq = Yii::$app->dbcascap->createCommand($q)->queryAll(); 
        
        $answer = [];
        foreach ($answerq as $b){
            $answer[] = $b['name'];
        }
        $ansname = implode(', ', $answer);
        
        if($icf_old_qid != NULL){
            // update on database
            Yii::$app->dbcascap->createCommand("update query_confirmq set value_status = :value_status, comment = :comment, uupdate = :uupdate, dupdate = NOW(),
                REMOTE_ADDR = :remote_addr, HTTP_X_REAL_IP = :real_ip, HTTP_X_FORWARDED_FOR = :forward_for, HTTP_HOST = :http_host,
                SERVER_ADDR =:server_addr, CONTENT_LENGTH =:content_length, HTTP_ACCEPT =:http_accept, HTTP_USER_AGENT =:http_user_agent
                where qid =:qid", [':value_status'=>$chpass, ':comment'=>$ansname,':uupdate'=>$userid, ':remote_addr' =>$remote_address, 
                ':real_ip'=>$http_x_real_ip, ':forward_for'=>$http_forwarded_for,':http_host'=>$http_host, ':server_addr'=>$server_addr, ':content_length'=>$content_length, 
                ':http_accept'=>$http_accept, ':http_user_agent'=>$http_user_agent,':qid'=>$icf_old_qid])->execute();
        }else{
            // insert
            Yii::$app->dbcascap->createCommand("insert into query_confirmq VALUES  (:qid, NULL, :qtype, :sitecode, :ptcode, :ptid, :id, :proj_id, :field_id, :box_id,
                :var, :value,:value_status,:comment,NULL, :uadd,:uupdate, NOW(), NOW(), :remote_addr, :real_ip, :forward_for, :http_host, :server_addr, 
                :content_length, :http_accept, :http_user_agent)", [':qid'=>$qid, ':qtype'=>$qtype_main, ':sitecode'=>$sitecode, ':ptcode'=>$ptcode,
                ':ptid'=>$q_target_id,':id'=>0, ':proj_id'=>0, ':field_id'=>0,':box_id'=>0,':var'=>$var, ':value'=>$value_main, ':value_status'=>$chpass,
                ':comment'=>$ansname,':uadd'=>$userid, ':uupdate'=>$userid, ':remote_addr' =>$remote_address, ':real_ip'=>$http_x_real_ip, ':forward_for'=>$http_forwarded_for,
                ':http_host'=>$http_host, ':server_addr'=>$server_addr, ':content_length'=>$content_length, ':http_accept'=>$http_accept, ':http_user_agent'=>$http_user_agent])->execute();
        }
        // log table
        // check data in log table
        $icf_log = Yii::$app->dblog->createCommand("select qid from query_confirmq where qtype = 'q' and sitecode= :sitecode and ptcode = :ptcode and ptid = :ptid and var = 'icf' and value = '3'", 
                [':sitecode'=>$sitecode, ':ptcode'=>$ptcode, ':ptid'=>$q_target_id ])->queryScalar();
        if($icf_log == NULL){
            $anscomment = iconv('ISO-8859-11', 'utf-8', $ansname);
            Yii::$app->dblog->createCommand("insert into query_confirmq VALUES  (:qid, NULL, :qtype, :sitecode, :ptcode, :ptid, :id, :proj_id, :field_id, :box_id,
                :var, :value,:value_status,:comment,NULL, :uadd,:uupdate, NOW(), NOW(), :remote_addr, :real_ip, :forward_for, :http_host, :server_addr, 
                :content_length, :http_accept, :http_user_agent,:encodeutf8)", [':qid'=>$qid, ':qtype'=>$qtype_main, ':sitecode'=>$sitecode, ':ptcode'=>$ptcode,
                ':ptid'=>$q_target_id,':id'=>0, ':proj_id'=>0, ':field_id'=>0,':box_id'=>0,':var'=>$var, ':value'=>$value_main, ':value_status'=>$chpass,
                ':comment'=>$anscomment,':uadd'=>$userid, ':uupdate'=>$userid, ':remote_addr' =>$remote_address, ':real_ip'=>$http_x_real_ip, ':forward_for'=>$http_forwarded_for,
                ':http_host'=>$http_host, ':server_addr'=>$server_addr, ':content_length'=>$content_length, ':http_accept'=>$http_accept, ':http_user_agent'=>$http_user_agent, 
                ':encodeutf8'=>$encodeutf8])->execute();
        }
        
        $icf_qid = Yii::$app->dbcascap->createCommand("select qid from query_confirmq where qtype = 'q' and sitecode= :sitecode and ptcode = :ptcode and ptid = :ptid and var = 'icf' and value = '3'", 
                [':sitecode'=>$sitecode, ':ptcode'=>$ptcode, ':ptid'=>$q_target_id ])->queryScalar();
        if($chpass == 3){
            
            foreach ($arraykey as $v) {
                // check has data ?
                $icf_ans_old = Yii::$app->dbcascap->createCommand("select count(*)  from query_confirmq where qid_key = :qid and value = :value", [':qid'=>$icf_qid, ':value'=>$v])->queryScalar();
                // get qid timestamp
                $time = $this->setTimeCode();
                if($icf_ans_old == 0){
                    // get comemnt
                    $ans_comment = Yii::$app->dbcascap->createCommand("select name from query_message_error where id = :id", [':id'=>$v])->queryScalar();
                    // insert new
                    Yii::$app->dbcascap->createCommand("insert into query_confirmq VALUES  (:qid, :qid_key, :qtype, :sitecode, :ptcode, :ptid, :id, :proj_id, :field_id, :box_id,
                        :var, :value,:value_status,:comment,NULL, :uadd,:uupdate, NOW(), NOW(), :remote_addr, :real_ip, :forward_for, :http_host, :server_addr, 
                        :content_length, :http_accept, :http_user_agent)", [':qid'=>$time, ':qid_key'=>$icf_qid, ':qtype'=>$qtype_ans, ':sitecode'=>$sitecode, ':ptcode'=>$ptcode,
                        ':ptid'=>$q_target_id,':id'=>0, ':proj_id'=>0, ':field_id'=>0,':box_id'=>0,':var'=>$var, ':value'=>$v, ':value_status'=>$v,
                        ':comment'=>$ans_comment,':uadd'=>$userid, ':uupdate'=>$userid, ':remote_addr' =>$remote_address, ':real_ip'=>$http_x_real_ip, ':forward_for'=>$http_forwarded_for,
                        ':http_host'=>$http_host, ':server_addr'=>$server_addr, ':content_length'=>$content_length, ':http_accept'=>$http_accept, ':http_user_agent'=>$http_user_agent
                        ])->execute();
                   
                }
                // insert log
                // check data in log table
                $icf_ans_log = Yii::$app->dblog->createCommand("select count(*)  from query_confirmq where qid_key = :qid and value = :value", [':qid'=>$icf_qid, ':value'=>$v])->queryScalar();
                if($icf_ans_log == 0){
                    // get comemnt
                    $ans_comment = Yii::$app->dbcascap->createCommand("select name from query_message_error where id = :id", [':id'=>$v])->queryScalar();
                    // convert latin-1 to utf-8
                    $c_utf = iconv('ISO-8859-11', 'utf-8', $ans_comment);
                    // insert new
                    Yii::$app->dblog->createCommand("insert into query_confirmq VALUES  (:qid, :qid_key, :qtype, :sitecode, :ptcode, :ptid, :id, :proj_id, :field_id, :box_id,
                        :var, :value,:value_status,:comment,NULL, :uadd,:uupdate, NOW(), NOW(), :remote_addr, :real_ip, :forward_for, :http_host, :server_addr, 
                        :content_length, :http_accept, :http_user_agent,:encodeutf8)", [':qid'=>$time, ':qid_key'=>$icf_qid, ':qtype'=>$qtype_ans, ':sitecode'=>$sitecode, ':ptcode'=>$ptcode,
                        ':ptid'=>$q_target_id,':id'=>0, ':proj_id'=>0, ':field_id'=>0,':box_id'=>0,':var'=>$var, ':value'=>$v, ':value_status'=>$v,
                        ':comment'=>$c_utf,':uadd'=>$userid, ':uupdate'=>$userid, ':remote_addr' =>$remote_address, ':real_ip'=>$http_x_real_ip, ':forward_for'=>$http_forwarded_for,
                        ':http_host'=>$http_host, ':server_addr'=>$server_addr, ':content_length'=>$content_length, ':http_accept'=>$http_accept, ':http_user_agent'=>$http_user_agent, 
                        ':encodeutf8'=>$encodeutf8])->execute();
                }
            }
        }
        // add comment as 99;
        if(strlen(trim($comment))!= 0 ){
            $icf_ans = Yii::$app->dbcascap->createCommand("select count(*)  from query_confirmq where qid_key = :qid and value = :value", [':qid'=>$icf_qid, ':value'=>99])->queryScalar();
            //m convert to latin
            $c_latin = iconv('utf-8', 'ISO-8859-11', $comment);
            if($icf_ans == 0){ 
                Yii::$app->dbcascap->createCommand("insert into query_confirmq VALUES  (:qid, :qid_key, :qtype, :sitecode, :ptcode, :ptid, :id, :proj_id, :field_id, :box_id,
                        :var, :value,:value_status,:comment,NULL, :uadd,:uupdate, NOW(), NOW(), :remote_addr, :real_ip, :forward_for, :http_host, :server_addr, 
                        :content_length, :http_accept, :http_user_agent)", [':qid'=>$time, ':qid_key'=>$icf_qid, ':qtype'=>$qtype_ans, ':sitecode'=>$sitecode, ':ptcode'=>$ptcode,
                        ':ptid'=>$q_target_id,':id'=>0, ':proj_id'=>0, ':field_id'=>0,':box_id'=>0,':var'=>$var, ':value'=>99, ':value_status'=>99,
                        ':comment'=>$c_latin,':uadd'=>$userid, ':uupdate'=>$userid, ':remote_addr' =>$remote_address, ':real_ip'=>$http_x_real_ip, ':forward_for'=>$http_forwarded_for,
                        ':http_host'=>$http_host, ':server_addr'=>$server_addr, ':content_length'=>$content_length, ':http_accept'=>$http_accept, ':http_user_agent'=>$http_user_agent
                        ])->execute();
                
            }else{
                Yii::$app->dbcascap->createCommand("update query_confirmq set comment = :comment, uupdate = :uupdate, dupdate = NOW(),
                REMOTE_ADDR = :remote_addr, HTTP_X_REAL_IP = :real_ip, HTTP_X_FORWARDED_FOR = :forward_for, HTTP_HOST = :http_host,
                SERVER_ADDR =:server_addr, CONTENT_LENGTH =:content_length, HTTP_ACCEPT =:http_accept, HTTP_USER_AGENT =:http_user_agent
                where qid_key = :qid and value = :value", [':comment'=>$c_latin,':uupdate'=>$userid, ':remote_addr' =>$remote_address, 
                ':real_ip'=>$http_x_real_ip, ':forward_for'=>$http_forwarded_for,':http_host'=>$http_host, ':server_addr'=>$server_addr, ':content_length'=>$content_length, 
                ':http_accept'=>$http_accept, ':http_user_agent'=>$http_user_agent,':qid_key'=>$icf_qid, ':value'=>99])->execute();
            }
            // check data 99 in log
            $icf_ans_log = Yii::$app->dblog->createCommand("select count(*)  from query_confirmq where qid_key = :qid and value = :value", [':qid'=>$icf_qid, ':value'=>99])->queryScalar();
                if($icf_ans_log == 0){
                    // convert latin-1 to utf-8
                    // insert new
                    Yii::$app->dblog->createCommand("insert into query_confirmq VALUES  (:qid, :qid_key, :qtype, :sitecode, :ptcode, :ptid, :id, :proj_id, :field_id, :box_id,
                        :var, :value,:value_status,:comment,NULL, :uadd,:uupdate, NOW(), NOW(), :remote_addr, :real_ip, :forward_for, :http_host, :server_addr, 
                        :content_length, :http_accept, :http_user_agent,:encodeutf8)", [':qid'=>$time, ':qid_key'=>$icf_qid, ':qtype'=>$qtype_ans, ':sitecode'=>$sitecode, ':ptcode'=>$ptcode,
                        ':ptid'=>$q_target_id,':id'=>0, ':proj_id'=>0, ':field_id'=>0,':box_id'=>0,':var'=>$var, ':value'=>99, ':value_status'=>99,
                        ':comment'=>$comment,':uadd'=>$userid, ':uupdate'=>$userid, ':remote_addr' =>$remote_address, ':real_ip'=>$http_x_real_ip, ':forward_for'=>$http_forwarded_for,
                        ':http_host'=>$http_host, ':server_addr'=>$server_addr, ':content_length'=>$content_length, ':http_accept'=>$http_accept, ':http_user_agent'=>$http_user_agent, 
                        ':encodeutf8'=>$encodeutf8])->execute();
                }
        }
        return json_encode(['success'=>'submit a query insert success ! ']);
    }
    public function actionImageSubmit(){
        $selectimg = $_POST['selectimg']; // radio check
        //$imglist =['945177', '945178']; //  77 = icf upload 1 78 == icf upload2
        $dateoficf = $_POST['dateoficf'];
        $selectoficf = $_POST['selectoficf'];
        
        $q_data_id = $_POST['q_data_id'];
        $q_target_id = $_POST['q_target_id'];
        $q_userkey = $_POST['q_userkey'];
        $q_xsourcex = $_POST['q_xsourcex'];
        
        $remote_address = $_SERVER['REMOTE_ADDR'];
        $http_x_real_ip = $_SERVER['HTTP_X_REAL_IP'];
        $http_forwarded_for = $_SERVER['HTTP_X_FORWARDED_FOR'];
        $http_host = $_SERVER['HTTP_HOST'];
        $server_addr = $_SERVER['SERVER_ADDR'];
        $content_length = $_SERVER['CONTENT_LENGTH'];
        $http_accept = $_SERVER['HTTP_ACCEPT'];
        $http_user_agent = $_SERVER['HTTP_USER_AGENT'];
        
        $userid = \Yii::$app->user->identity->id;
        
        // get image data from tb_data_1
        
        //$icf_upload = Yii::$app->db->createCommand("select icf_upload1, icf_upload2  from tb_data_1 where ptid = :ptid and xsourcex = :xsourcex", 
        //        [':ptid'=>$q_data_id, ':xsourcex'=>$q_xsourcex])->queryOne();
        //$value_old = $selectimg == 945177 ? $icf_upload['icf_upload1'] : $icf_upload['icf_upload2']; 
        // check qid
        
        $icf_qid = Yii::$app->dbcascap->createCommand("select qid from query_confirmq where qtype = 'q' and sitecode= :sitecode and ptid = :ptid and var = 'icf' and value = '3'", 
                [':sitecode'=>$q_xsourcex, ':ptid'=>$q_target_id ])->queryScalar();   
        if($icf_qid){
            // update to query_confirm
            
            Yii::$app->dbcascap->createCommand("update query_confirmq set value_old = :value_old, uupdate = :uupdate, dupdate = NOW(),
                REMOTE_ADDR = :remote_addr, HTTP_X_REAL_IP = :real_ip, HTTP_X_FORWARDED_FOR = :forward_for, HTTP_HOST = :http_host,
                SERVER_ADDR =:server_addr, CONTENT_LENGTH =:content_length, HTTP_ACCEPT =:http_accept, HTTP_USER_AGENT =:http_user_agent
                where qid =:qid", [':value_old'=>$selectimg,':uupdate'=>$userid, ':remote_addr' =>$remote_address, 
                ':real_ip'=>$http_x_real_ip, ':forward_for'=>$http_forwarded_for,':http_host'=>$http_host, ':server_addr'=>$server_addr, ':content_length'=>$content_length, 
                ':http_accept'=>$http_accept, ':http_user_agent'=>$http_user_agent,':qid'=>$icf_qid])->execute();
            
        }
       // set date be => ce 
        $dateoficfdb = $this->setDateFormat($dateoficf);
        $selectoficfdb = $this->setDateFormat($selectoficf);
        // update time if not equal tb_data_1
        $patientdateicf =Yii::$app->db->createCommand("select sys_dateoficf, sys_ecoficf from tb_data_1
                                    where ptid = :ptid and xsourcex = :xsourcex", [':ptid'=>$q_data_id, ':xsourcex' =>$q_xsourcex ])->queryOne(); 
        $checkicftime = null;
        if($patientdateicf['sys_dateoficf']!= $dateoficf && $patientdateicf['sys_dateoficf'] != $selectoficf) {
            // update icf time data
            $checkicftime = 'different';
            Yii::$app->db->createCommand("update tb_data_1 set sys_dateoficf = :dateoficf, sys_dateoficfdb = :dateoficfdb, sys_ecoficf = :ecoficf, sys_ecoficfdb = :ecoficfdb, 
                    user_update = :user_update, update_date = NOW()
                    where ptid = :ptid and xsourcex = :xsourcex ", [':dateoficf'=> $dateoficf, ':dateoficfdb'=> $dateoficfdb, ':ecoficf'=> $selectoficf, ':ecoficfdb'=> $selectoficfdb,  
                       ':user_update' => $userid, ':ptid'=>$q_data_id, ':xsourcex' =>$q_xsourcex ])->execute(); 
            
        }else{
            $checkicftime = 'same';
        }
        
        return json_encode(['success'=>'update success !']);
    }
    public function actionDeleteAnswer(){
        $qid = $_POST['qid'];
        Yii::$app->dbcascap->createCommand("delete from query_confirmq where qid = :qid ",
                [':qid'=>$qid])->execute(); // dont delete old data
        return json_encode(['success'=>'ลบข้อมูลสำเร็จ']);
    }
    public function actionDeleteAll(){
        $dataid = $_POST['dataid'];
        $xsourcex = $_POST['xsourcex'];
        // get main data row
        $qid = Yii::$app->dbcascap->createCommand("select qid from query_confirmq where ptid = :ptid and sitecode = :sitecode", 
                [':ptid'=>$dataid, ':sitecode'=>$xsourcex])->queryScalar();   
        // delete all create new data
        Yii::$app->dbcascap->createCommand("delete from query_confirmq where qid_key = :qid and qid <> 1464924018863",
                [':qid'=>$qid])->execute(); // dont delete old data
        return json_encode(['success'=>'ลบข้อมูลทั้งหมดสำเร็จ']);
    }
    public function actionChecking()
    {
        return $this->render('checking');
    }
    private function setTimeCode(){
        $t = microtime();
        $time = explode(' ', $t);
        $millisec = (int)((float)$time[0] * 1000) ;
        if(strlen($millisec) == 1 ){
            $millisec =  '00'.$millisec;
        }else if(strlen($millisec) == 2 ){
            $millisec =  '0'.$millisec;
        }
        return $time[1].(string)$millisec;
    }
    private function setDateFormat($date){
        $datearr = explode('/', $date); // order == dd/mm/yy
        return (string)((integer)$datearr[2] - 543) .'-'.$datearr[1].'-'.$datearr[0];
    }
    public function actionTestClick(){
        $array = ['t1'=>'test 1', 't2'=>'test 2'];
        return 'kfhgkfjghkjgkjfhjk';
        
    }
}
