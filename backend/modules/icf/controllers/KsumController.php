<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace backend\modules\icf\controllers;

use yii\web\Controller;
use Yii;

// class
use \backend\modules\icf\classes\KeySummary;

/**
 * THE CONTROLLER ACTION
 */
class KsumController extends Controller
{
    public function actionIndex()
    {
        
        $page=1;
        return $this->render('index', [
            'page' => $page,
        ]);
    }
    
    public function actionRealView(){
        $dataProvider   = KeySummary::groupUserOnEdit(Yii::$app->request->get('id'));
        return $this->renderAjax('index',[
            'data'  => $dataProvider,
        ]);
    }
    
    public function actionDriwDown(){
        $request = Yii::$app->request;
        $dataProvider   = KeySummary::tbdataRecordByUserUpdate($request->get('id'),$request->get('user_update'));
        return $this->renderAjax('_showrecord',[
            'data'  => $dataProvider,
        ]);
    }
    

}

