<?php

namespace backend\modules\icf\controllers;

use Yii;
use yii\data\ActiveDataProvider;
use backend\modules\icf\classes\ICFFunc;

class Cca01Controller extends \yii\web\Controller
{
    public function actionIndex()
    {
        $_webget=$_GET;
        $_webserver = $_SERVER;
        $dateCheck = ICFFunc::getDatefromYearPeriod($_GET['pryear']); 
        $userDet['userId'] = \Yii::$app->user->identity->id;
        $userDet['sitecode'] = \Yii::$app->user->identity->userProfile->sitecode;
        $_filter['condition'] = self::getListOfGroupRecord();
        $dataCount['dataGroup']=  self::dataGetDataGroupBySite($userDet['sitecode'],$dateCheck['startDate'],$dateCheck['endDate']);
        
        
        return $this->render('index', [
            '_webget' => $_webget,
            '_webserver' => $_webserver,
            'userDet' => $userDet,
            '_filter' => $_filter,
            'dataCount' => $dataCount,
        ]);
    }
    
    
    public function getListOfGroupRecord(){
        $irow=0;
        $out[$irow]['msg']='จำนวนที่ตรวจแล้วผ่าน และได้รับค่าตอบแทนแล้ว';
        $out[$irow]['filter']="lock";
        $out[$irow]['url']="/icf/cca01/list?filter=lock";
        $out[$irow]['sqlname']="record_icf_lock";
        $out[$irow]['sqlcond']="cca01.rstat='4'";
        $irow++;
        $out[$irow]['msg']='จำนวนที่ตรวจแล้วผ่าน (+ICF)';
        $out[$irow]['filter']="completed";
        $out[$irow]['url']="/icf/cca01/list?filter=completed";
        $out[$irow]['sqlname']="record_icf_completed";
        $out[$irow]['sqlcond']="cca01.ICF='1' and length(cca01.error)=0";
        $irow++;
        $out[$irow]['msg']='จำนวนที่คีย์สมบูรณ์แล้ว แต่ ICF ยังไม่ถูกต้อง';
        $out[$irow]['filter']="icfnotcomp";
        $out[$irow]['url']="/icf/cca01/list?filter=icfnotcomp";
        $out[$irow]['sqlname']="record_icfnotc";
        $out[$irow]['sqlcond']="(cca01.ICF='0' or cca01.ICF is null) and length(cca01.error)=0";
        $irow++;
        $out[$irow]['msg']='จำนวนที่ผ่านเงื่อนไข (ไม่มี Error) และ Submit แล้ว รอการตรวจ';
        $out[$irow]['filter']="wchecking";
        $out[$irow]['url']="/icf/cca01/list?filter=wchecking";
        $out[$irow]['sqlname']="record_submited_waitingchecking";
        $out[$irow]['sqlcond']="(cca01.ICF='0' or cca01.ICF is null) and length(cca01.error)=0 and cca01.rstat='2'";
        $irow++;
        $out[$irow]['msg']='จำนวนที่ผ่านเงื่อนไข (ไม่มี Error) ยังไม่ได้ Submit';
        $out[$irow]['filter']="wsubmit";
        $out[$irow]['url']="/icf/cca01/list?filter=wsubmit";
        $out[$irow]['sqlname']="record_compedted_waitingsubmit";
        $out[$irow]['sqlcond']="(cca01.ICF='0' or cca01.ICF is null) and length(cca01.error)=0 and cca01.rstat='1'";
        $irow++;
        $out[$irow]['msg']='จำนวนที่ข้อมูลยังไม่สมบูรณ์ (กรุณาเข้าไปแก้ไขเพื่อให้ข้อมูลสมบูรณ์)';
        $out[$irow]['filter']="error";
        $out[$irow]['url']="/icf/cca01/list?filter=error";
        $out[$irow]['sqlname']="record_notcompedte";
        $out[$irow]['sqlcond']="length(cca01.error)>0";
        $irow++;
        $out[$irow]['msg']='จำนวนที่ข้อมูลที่เพิ่มใหม่';
        $out[$irow]['filter']="new";
        $out[$irow]['url']="/icf/cca01/list?filter=new";
        $out[$irow]['sqlname']="record_new";
        $out[$irow]['sqlcond']="cca01.rstat='0'";
        return $out;
    }
    
    public function dataGetDataGroupBySite($sitecode,$startDate,$endDate){
        $lscond=Self::getListOfGroupRecord();
        //$userid=1;
        $sql = "select count(distinct id) as allid ";
        if( count($lscond)>0 ){
            foreach($lscond as $k => $v){
                $sql.=",";
                $sql.="count(if(".$lscond[$k]['sqlcond'].",ptid,null)) as ".$lscond[$k]['sqlname']." ";
            }
        }
        $sql.= "from tb_data_2 as cca01 where hsitecode=\"".addslashes($sitecode)."\" ";
        $sql.= "and cca01.f1vdcomp between \"".$startDate."\" and \"".$endDate."\" ";
        $sql.= "and rstat<>'3' ";
        //echo "<br /><br /><br /><br /><br />";
        //echo $sql;
        $result =  Yii::$app->db->createCommand($sql)->queryAll();
        if($result){
            $out=$result[0];
        }
        return $out;   
    }
    
    
    
    
    
    // List data
    public function actionList()
    {
        
        $_webget=$_GET;
        $_webserver = $_SERVER;
        //$_webget['limitperpage'] = self::getLimitPage();
        if(strlen($_webget['page']) == 0){
            //$_webget['page'] =  self::getDefaultPage();
        }
        $userDet['userId'] = \Yii::$app->user->identity->id;
        $userDet['sitecode'] = \Yii::$app->user->identity->userProfile->sitecode;
        $_filter['condition'] = self::getListOfGroupRecord();
        $listRecord['row'] = self::getListDataGroupBySite($userDet['sitecode'],$_webget['filter']);
        $dateCheck = ICFFunc::getDatefromYearPeriod($_GET['pryear']);
        $dataCount['dataGroup']=  self::dataGetDataGroupBySite($userDet['sitecode'],$dateCheck['startDate'],$dateCheck['endDate']);
        
        
        //return $this->render('list');
        
        return $this->render('list', [
            '_webget' => $_webget,
            '_webserver' => $_webserver,
            'userDet' => $userDet,
            '_filter' => $_filter,
            'listRecord' => $listRecord,
            'dataCount' => $dataCount,
        ]);
    }
    
        
    public function getListDataGroupBySite($sitecode,$filter){
        $lscond=Self::getListOfGroupRecord();
        if( 0 ){
            echo "<br /><br /><br />";
            echo "filter: ".$filter;
            echo "<pre align='left'>";
            print_r($lscond);
            echo "</pre>";
            echo "<br />";
        }
        // update ICF
        $sql = "update tb_data_2 as cca01 ";
        $sql.= "left join tb_data_1 as reg on reg.ptid=cca01.ptid ";
        $sql.= "set cca01.ICF='0' ";
        $sql.= "where reg.confirm='0' and cca01.hsitecode=\"".addslashes($sitecode)."\" ";
        $result =  Yii::$app->db->createCommand($sql)->execute();
        
        $sql = "update tb_data_2 as cca01 ";
        $sql.= "left join tb_data_1 as reg on reg.ptid=cca01.ptid ";
        $sql.= "set cca01.ICF='1' ";
        $sql.= "where reg.confirm>0 and cca01.hsitecode=\"".addslashes($sitecode)."\" ";
        $result =  Yii::$app->db->createCommand($sql)->execute();
        
        //$query = new \yii\db\Query();
        $sql = "select distinct cca01.id,cca01.ptid,cca01.ICF,reg.confirm,cca01.hsitecode,cca01.hptcode ";
        $sql.= ",reg.title,reg.name,reg.surname,cca01.error ";
        $sql.= ",reg.`dlastcheck`,cca01.`update_date` ";
        $sql.= ",cca01.rstat ";
        $sql.= ",if(Not(cca01.rstat in ('0','3')) and (length(cca01.`error`)=0 and cca01.`error` is not null),'<span class=tdtext-text-show-blue>สมบูรณ์</span>' ";
        $sql.= ",if(cca01.`error` is null,'<span class=tdtext-text-show-black>ยังไม่ตรวจสอบ</span>' ";
        $sql.= ",'<span class=tdtext-text-show-red>ไม่สมบูรณ์</span>')) as 'cca01_error' ";
        $sql.= "from tb_data_2 as cca01 ";
        $sql.="inner join tb_data_1 as reg on cca01.ptid=reg.ptid and cca01.hsitecode=reg.hsitecode ";
        $sql.= "where cca01.hsitecode=\"".addslashes($sitecode)."\" ";
        $sql.= "and cca01.rstat<>'3' ";
        if( count($lscond)>0 ){
            foreach($lscond as $k => $v){
                if( $lscond[$k]['filter']==$filter ){
                    $sql.="and ";
                    $sql.="(".$lscond[$k]['sqlcond'].") ";
                }
            }
        }
        $sql.= "order by cca01.hsitecode,cca01.hptcode ";
        $sql.= "limit 4000 ";
        
        
        
        $result =  Yii::$app->db->createCommand($sql)->queryAll();
        if($result){
            $out=$result;
        }
        if(count($out)>0){
            foreach ($out as $k => $v){
                if($out[$k]['confirm']>0){
                    //
                }else{
                    //$errTxt=  self::getICFCheckErrorFromTools($out[$k]['ptid']);
                    //$out[$k]['txtError']=$errTxt['txtError'];
                }
            }
        }
        return $out;
    }
    

}
