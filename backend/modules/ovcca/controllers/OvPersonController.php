<?php

namespace backend\modules\ovcca\controllers;

use Yii;
use backend\modules\ovcca\models\OvPerson;
use backend\modules\ovcca\models\OvPersonSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\Response;
use appxq\sdii\helpers\SDHtml;
use common\lib\tcpdf\SDPDF;
use backend\modules\ovcca\classes\OvccaQuery;
use backend\modules\ovcca\models\OvFilter;
use common\lib\sdii\components\utils\SDdate;
use yii\helpers\ArrayHelper;
use backend\modules\ovcca\models\OvFilterSub;
use backend\modules\ovcca\models\OvSubList;
use backend\modules\ovcca\models\TbData1Search;
use backend\modules\ovcca\models\TbData1;
use backend\modules\ovcca\classes\OvccaFunc;

/**
 * OvPersonController implements the CRUD actions for OvPerson model.
 */
class OvPersonController extends Controller
{
    public function behaviors()
    {
        return [
	    'access' => [
		'class' => AccessControl::className(),
		'rules' => [
		    [
			'allow' => true,
			'actions' => ['index', 'view'], 
			'roles' => ['?', '@'],
		    ],
		    [
			'allow' => true,
			'actions' => ['view', 'create', 'update', 'delete', 'deletes', 'import-all', 'import-views', 'ovfilter'], 
			'roles' => ['@'],
		    ],
		],
	    ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public function beforeAction($action) {
	if (parent::beforeAction($action)) {
	    if (in_array($action->id, array('create', 'update'))) {
		
	    }
	    return true;
	} else {
	    return false;
	}
    }
    
    public function actionOvfilter()
    {
	if (Yii::$app->getRequest()->isAjax) {
	    $model = new OvFilter();
	    $num = OvFilter::find()->where('sitecode=:sitecode', [':sitecode'=>Yii::$app->user->identity->userProfile->sitecode])->count()+1;
	    $model->name = 'รอบที่ '.$num;
	    $model->sitecode = Yii::$app->user->identity->userProfile->sitecode;
	    
	    if ($model->load(Yii::$app->request->post())) {
		Yii::$app->response->format = Response::FORMAT_JSON;
		$model->start = SDdate::phpThDate2mysqlDate($model->start, '-');
		$model->end = SDdate::phpThDate2mysqlDate($model->end, '-');
		
		if ($model->save()) {
		    $result = [
			'status' => 'success',
			'action' => 'create',
			'message' => SDHtml::getMsgSuccess() . Yii::t('app', 'Data completed.'),
			'data' => $model,
		    ];
		    return $result;
		} else {
		    $result = [
			'status' => 'error',
			'message' => SDHtml::getMsgError() . Yii::t('app', 'Can not create the data.'),
			'data' => $model,
		    ];
		    return $result;
		}
	    } else {
		return $this->renderAjax('ovfilter', [
		    'model' => $model,
		]);
	    }
	} else {
	    throw new NotFoundHttpException('Invalid request. Please do not repeat this request again.');
	}
    }
    
    public function actionOvfilterSub()
    {
	if (Yii::$app->getRequest()->isAjax) {
	    $model = new OvFilterSub();
	    $sitecode = Yii::$app->user->identity->userProfile->sitecode;
	    $model->sitecode = $sitecode;
	    $model->urine_status = 1;
	    
	    if ($model->load(Yii::$app->request->post())) {
		Yii::$app->response->format = Response::FORMAT_JSON;
		$model->sitecode = $sitecode;
		$model->created_by = Yii::$app->user->id;
		
		if ($model->save()) {
		    $result = [
			'status' => 'success',
			'action' => 'create',
			'message' => SDHtml::getMsgSuccess() . Yii::t('app', 'Data completed.'),
			'data' => $model,
		    ];
		    return $result;
		} else {
		    $result = [
			'status' => 'error',
			'message' => SDHtml::getMsgError() . Yii::t('app', 'Can not create the data.'),
			'data' => $model,
		    ];
		    return $result;
		}
	    } else {
		return $this->renderAjax('_form_fileter_sub', [
		    'model' => $model,
		]);
	    }
	} else {
	    throw new NotFoundHttpException('Invalid request. Please do not repeat this request again.');
	}
    }
    /**
     * Lists all OvPerson models.
     * @return mixed
     */
    public function actionIndex()
    {
//        \appxq\sdii\utils\VarDumper::dump(Yii::$app->request->queryParams);
	$sitecode = Yii::$app->user->identity->userProfile->sitecode;
        
	$ovCount = \backend\modules\ovcca\models\OvPerson::find()->where('ov_person.hospcode = :sitecode', [':sitecode'=>$sitecode])->count();
	
	$dataOvFilterSub = OvccaQuery::getFilterSub($sitecode);
	
	$ovfilter_sub = isset($_GET['ovfilter_sub'])?$_GET['ovfilter_sub']:0;
	
	$searchModel = new \backend\modules\ovcca\models\TbData1Search();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $ovfilter_sub);
	$dataProvider->pagination->pageSize = 100;
//        \appxq\sdii\utils\VarDumper::dump($dataProvider);
	
	$totalAll = TbData1::find()->where('rstat<>3 AND hsitecode = :sitecode', [':sitecode'=>$sitecode])->count();
	$totalBot = TbData1::find()->where('rstat<>3 AND tccbot=1 AND hsitecode = :sitecode', [':sitecode'=>$sitecode])->count();
	$totalTb = TbData1::find()->where('rstat<>3 AND tccbot=0 AND hsitecode = :sitecode', [':sitecode'=>$sitecode])->count();
	
	$total = 0;
	$total_label = 'ทั้งหมดในคลังจาก TDC';
	$modelSum = \backend\modules\ovcca\models\OvSum::find()->where('sitecode=:sitecode', [':sitecode'=>$sitecode])->one();
	if($modelSum){
	    $total = $modelSum->total;
	    if($modelSum->type==2){
		$total_label = 'ทั้งหมดในคลังจากใบทำบัตร';
	    }
	}
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
	    'ovCount' => $ovCount,
	    'total' => $total,
	    'total_label' => $total_label,
	    'modelSum' => $modelSum,
	    'dataOvFilterSub' => $dataOvFilterSub,
	    'ovfilter_sub'=>$ovfilter_sub,
	    'sitecode' =>$sitecode,
	    'totalAll'=>$totalAll,
	    'totalBot'=>$totalBot,
	    'totalTb'=>$totalTb,
        ]);
    }
    
    public function actionIndexMini()
    {
	$sitecode = Yii::$app->user->identity->userProfile->sitecode;
        
	$ovCount = \backend\modules\ovcca\models\OvPerson::find()->where('ov_person.hospcode = :sitecode', [':sitecode'=>$sitecode])->count();
	
	$dataOvFilterSub = OvccaQuery::getFilterSub($sitecode);
	
	$ovfilter_sub = isset($_GET['ovfilter_sub'])?$_GET['ovfilter_sub']:0;
	
	$searchModel = new \backend\modules\ovcca\models\TbData1Search();
        $dataProvider = $searchModel->searchMini(Yii::$app->request->queryParams, $ovfilter_sub);
	$dataProvider->pagination->pageSize = 100;
	
	$totalAll = TbData1::find()->where('rstat<>3 AND hsitecode = :sitecode', [':sitecode'=>$sitecode])->count();
	$totalBot = TbData1::find()->where('rstat<>3 AND tccbot=1 AND hsitecode = :sitecode', [':sitecode'=>$sitecode])->count();
	$totalTb = TbData1::find()->where('rstat<>3 AND tccbot=0 AND hsitecode = :sitecode', [':sitecode'=>$sitecode])->count();
	
	$total = 0;
	$total_label = 'ทั้งหมดในคลังจาก TDC';
	$modelSum = \backend\modules\ovcca\models\OvSum::find()->where('sitecode=:sitecode', [':sitecode'=>$sitecode])->one();
	if($modelSum){
	    $total = $modelSum->total;
	    if($modelSum->type==2){
		$total_label = 'ทั้งหมดในคลังจากใบทำบัตร';
	    }
	}
        return $this->render('index-mini', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
	    'ovCount' => $ovCount,
	    'total' => $total,
	    'total_label' => $total_label,
	    'modelSum' => $modelSum,
	    'dataOvFilterSub' => $dataOvFilterSub,
	    'ovfilter_sub'=>$ovfilter_sub,
	    'sitecode' =>$sitecode,
	    'totalAll'=>$totalAll,
	    'totalBot'=>$totalBot,
	    'totalTb'=>$totalTb,
        ]);
    }

    public function actionIndexreg()
    {
	$sitecode = Yii::$app->user->identity->userProfile->sitecode;
        
	if (isset($_POST['findbot'])) {
	    $cid = $_POST['findbot']['cid'];
	    $key = $_POST['findbot']['key'];
	    $convert = isset($_POST['findbot']['convert'])?$_POST['findbot']['convert']:0;
	    
	    $checkCid = OvccaFunc::check_citizen($cid);
	    if($checkCid){
		
		$tccbot = OvccaQuery::getPersonOneByCid($sitecode, $cid, $key, $convert);
		//ดึงข้อมูลจาก TDC
		//เช็คความถูกต้องของข้อมูล
		//นำเข้าข้อมูล
		
		if($tccbot){
		    if(OvccaFunc::check_citizen($tccbot['cid'])){
			$checkthaiword = trim(OvccaFunc::checkthai($tccbot['fname']));
			if ($checkthaiword  != '') {
			    $r = OvccaFunc::findImportPersonOne($sitecode, $cid, $tccbot);
			    if($r){
				Yii::$app->session->setFlash('alert', [
				    'options' => ['class' => 'alert-success'],
				    'body' => "นำเข้าข้อมูลแล้ว <code>เลขบัตรประชาชน: {$tccbot['cid']} ชื่อ: {$tccbot['pname']}{$tccbot['fname']} {$tccbot['lname']}</code>"	
				]);
			    } else {
				Yii::$app->session->setFlash('alert', [
				    'options' => ['class' => 'alert-danger'],
				    'body' => "ไม่สามารถนำเข้าข้อมูลเข้าสู่ระบบ <code>เลขบัตรประชาชน: {$tccbot['cid']} ชื่อ: {$tccbot['pname']}{$tccbot['fname']} {$tccbot['lname']}</code>"	
				]);
			    }
			    
			} else {
			    Yii::$app->session->setFlash('alert', [
				'options' => ['class' => 'alert-danger'],
				'body' => "กรณีชื่อ-สกุล อ่านไม่ออกให้เข้ารหัสแบบ tis620 <code>เลขบัตรประชาชน: {$tccbot['cid']} ชื่อ: {$tccbot['pname']}{$tccbot['fname']} {$tccbot['lname']}</code>"
			    ]);
			}
		    } else {
			Yii::$app->session->setFlash('alert', [
			    'options' => ['class' => 'alert-danger'],
			    'body' => "Convert ข้อมูลไม่ถูกต้องกรุณาใส่ Key ใหม่เพื่อถอดรหัสให้ถูกต้อง <code>เลขบัตรประชาชน: {$tccbot['cid']} ชื่อ: {$tccbot['pname']}{$tccbot['fname']} {$tccbot['lname']}</code>"
			]);
		    }
		    
		} else {
		    Yii::$app->session->setFlash('alert', [
			'options' => ['class' => 'alert-danger'],
			'body' => 'ไม่พบข้อมูลใน TDC'
		    ]);
		}
		
	    } else {
		Yii::$app->session->setFlash('alert', [
		    'options' => ['class' => 'alert-danger'],
		    'body' => 'เลขบัตรประชาชนไม่ถูกต้อง'
		]);
	    }
	    
	    
	}
	
	$searchModel = new OvPersonSearch();
        $dataProvider = $searchModel->searchOrg(Yii::$app->request->queryParams);
	$dataProvider->pagination->pageSize = 100;
	
	$totalAll = TbData1::find()->where('rstat<>3 AND hsitecode = :sitecode', [':sitecode'=>$sitecode])->count();
	$totalTb = OvPerson::find()->where('status_import=0 AND hospcode = :sitecode', [':sitecode'=>$sitecode])->count();
	$ovCount = OvPerson::find()->where('ov_person.hospcode = :sitecode', [':sitecode'=>$sitecode])->count();
	$ovJoinCount = OvPerson::find()->where('status_import=1 AND  ov_person.hospcode = :sitecode', [':sitecode'=>$sitecode])->count();
	
	$total = 0;
	$total_label = 'จำนวนทั้งหมดในคลังจาก TDC';
	$modelSum = \backend\modules\ovcca\models\OvSum::find()->where('sitecode=:sitecode', [':sitecode'=>$sitecode])->one();
	if($modelSum){
	    if($modelSum->type==1){
		$total = $modelSum->total;
	    }
	}
	
        return $this->render('indexreg', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
	    'ovCount' => $ovCount,
	    'ovJoinCount' => $ovJoinCount,
	    'sitecode' =>$sitecode,
	    'total' => $total,
	    'total_label' => $total_label,  
	    'totalAll'=>$totalAll,
	    
	    'totalTb'=>$totalTb,
	    'cid'=>$cid,
	    'key'=>$key,
	    'convert'=>$convert
        ]);
    }
    
    /**
     * Displays a single OvPerson model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
	if (Yii::$app->getRequest()->isAjax) {
	    return $this->renderAjax('view', [
		'model' => $this->findModel($id),
	    ]);
	} else {
	    return $this->render('view', [
		'model' => $this->findModel($id),
	    ]);
	}
    }

    /**
     * Creates a new OvPerson model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
	if (Yii::$app->getRequest()->isAjax) {
	    $model = new OvPerson();

	    if ($model->load(Yii::$app->request->post())) {
		Yii::$app->response->format = Response::FORMAT_JSON;
		if ($model->save()) {
		    $result = [
			'status' => 'success',
			'action' => 'create',
			'message' => SDHtml::getMsgSuccess() . Yii::t('app', 'Data completed.'),
			'data' => $model,
		    ];
		    return $result;
		} else {
		    $result = [
			'status' => 'error',
			'message' => SDHtml::getMsgError() . Yii::t('app', 'Can not create the data.'),
			'data' => $model,
		    ];
		    return $result;
		}
	    } else {
		return $this->renderAjax('create', [
		    'model' => $model,
		]);
	    }
	} else {
	    throw new NotFoundHttpException('Invalid request. Please do not repeat this request again.');
	}
    }

    /**
     * Updates an existing OvPerson model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
	if (Yii::$app->getRequest()->isAjax) {
	    $model = $this->findModel($id);

	    if ($model->load(Yii::$app->request->post())) {
		Yii::$app->response->format = Response::FORMAT_JSON;
		if ($model->save()) {
		    $result = [
			'status' => 'success',
			'action' => 'update',
			'message' => SDHtml::getMsgSuccess() . Yii::t('app', 'Data completed.'),
			'data' => $model,
		    ];
		    return $result;
		} else {
		    $result = [
			'status' => 'error',
			'message' => SDHtml::getMsgError() . Yii::t('app', 'Can not update the data.'),
			'data' => $model,
		    ];
		    return $result;
		}
	    } else {
		return $this->renderAjax('update', [
		    'model' => $model,
		]);
	    }
	} else {
	    throw new NotFoundHttpException('Invalid request. Please do not repeat this request again.');
	}
    }

    /**
     * Deletes an existing OvPerson model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionStatus($id)
    {
	if (Yii::$app->getRequest()->isAjax) {
	    Yii::$app->response->format = Response::FORMAT_JSON;
	    $model = $this->findModel($id);
	    if($model->ov01_status){
		$model->ov01_status = 0;
	    }else{
		$model->ov01_status = 1;
	    }
	    if ($model->save()) {
		$result = [
		    'status' => 'success',
		    'action' => 'update',
		    'message' => SDHtml::getMsgSuccess() . Yii::t('app', 'Update completed.'),
		    'data' => $id,
		];
		return $result;
	    } 
	} else {
	    $result = [
		'status' => 'error',
		'message' => SDHtml::getMsgError() . Yii::t('app', 'Invalid request. Please do not repeat this request again.'),
		'data' => $id,
	    ];
	    return $result;
	}
    }
    
    public function actionStatust()
    {
	if (Yii::$app->getRequest()->isAjax) {
	    Yii::$app->response->format = Response::FORMAT_JSON;
	    $sitecode = Yii::$app->user->identity->userProfile->sitecode;
	    $model = Yii::$app->db->createCommand()->update('ov_person', ['ov01_status'=>1], 'hospcode=:sitecode', [':sitecode'=>$sitecode])->execute();
	    
	    if ($model) {
		$result = [
		    'status' => 'success',
		    'action' => 'update',
		    'message' => SDHtml::getMsgSuccess() . Yii::t('app', 'Update completed.'),
		    'data' => $id,
		];
		return $result;
	    } else {
		
	    }
	} else {
	    $result = [
		'status' => 'error',
		'message' => SDHtml::getMsgError() . Yii::t('app', 'Invalid request. Please do not repeat this request again.'),
		'data' => $id,
	    ];
	    return $result;
	}
    }
    
    public function actionStatusf()
    {
	if (Yii::$app->getRequest()->isAjax) {
	    Yii::$app->response->format = Response::FORMAT_JSON;
	    $sitecode = Yii::$app->user->identity->userProfile->sitecode;
	    $model = Yii::$app->db->createCommand()->update('ov_person', ['ov01_status'=>0], 'hospcode=:sitecode', [':sitecode'=>$sitecode])->execute();
	    
	    if ($model) {
		$result = [
		    'status' => 'success',
		    'action' => 'update',
		    'message' => SDHtml::getMsgSuccess() . Yii::t('app', 'Update completed.'),
		    'data' => $id,
		];
		return $result;
	    } else {
		$result = [
		    'status' => 'error',
		    'message' => SDHtml::getMsgError() . Yii::t('app', 'Can not update the data.'),
		    'data' => $id,
		];
		return $result;
	    }
	} else {
	    throw new NotFoundHttpException('Invalid request. Please do not repeat this request again.');
	}
    }
    
    public function actionDelete($id)
    {
	if (Yii::$app->getRequest()->isAjax) {
	    Yii::$app->response->format = Response::FORMAT_JSON;
	    if ($this->findModel($id)->delete()) {
		$result = [
		    'status' => 'success',
		    'action' => 'update',
		    'message' => SDHtml::getMsgSuccess() . Yii::t('app', 'Deleted completed.'),
		    'data' => $id,
		];
		return $result;
	    } else {
		$result = [
		    'status' => 'error',
		    'message' => SDHtml::getMsgError() . Yii::t('app', 'Can not delete the data.'),
		    'data' => $id,
		];
		return $result;
	    }
	} else {
	    throw new NotFoundHttpException('Invalid request. Please do not repeat this request again.');
	}
    }

    public function actionSavelist() {
	if (Yii::$app->getRequest()->isAjax) {
	    Yii::$app->response->format = Response::FORMAT_JSON;

	    if(isset($_POST['selection'])){
		foreach ($_POST['selection'] as $key => $value) {
		    
		    $ceck = OvSubList::find()->where('sub_id=:sub_id AND person_id=:id', [':sub_id'=>$_POST['sub'], ':id'=>$value])->count();
		    if($ceck==0){
			$model = new OvSubList();
			$model->sub_id = $_POST['sub'];
			$model->filter_id = $_POST['id'];
			$model->person_id = $value;
			$model->save();
		    }
		}
	    }
	    
	    $result = [
		'status' => 'success',
		'message' => SDHtml::getMsgSuccess() . Yii::t('app', 'Add list completed.'),
	    ];
	    return $result;
	}
    }
    public function actionAddSelectlist() {
	if (Yii::$app->getRequest()->isAjax) {
	    Yii::$app->response->format = Response::FORMAT_JSON;
	    $ovfilter_sub = isset($_GET['ovfilter_sub'])?$_GET['ovfilter_sub']:0;
	    
	    $sitecode = Yii::$app->user->identity->userProfile->sitecode;
	    if (isset($_POST['selection'])) {
		$dataOvFilterSub = OvFilterSub::find()->where('sub_id<>:sub_id AND sitecode=:sitecode', [':sitecode'=>$sitecode, ':sub_id'=>$ovfilter_sub])->all();
		
		$return = $this->renderAjax('_sub_selectlist', [
		    'model' => $dataOvFilterSub,
		    'selection'=>$_POST['selection'],
		]);
		
		$result = [
		    'status' => 'success',
		    'message' => SDHtml::getMsgError() . Yii::t('app', 'Load completed.'),
		    'html' => $return,
		];
		return $result;
		
	    } else {
		$result = [
		    'status' => 'error',
		    'message' => SDHtml::getMsgError() . Yii::t('app', 'Can not add the data.'),
		    'data' => $id,
		];
		return $result;
	    }
	} else {
	    throw new NotFoundHttpException('Invalid request. Please do not repeat this request again.');
	}
    }
    
    public function actionAddlist() {
	if (Yii::$app->getRequest()->isAjax) {
	    ini_set('max_execution_time', 0);
	    set_time_limit(0);
	    ini_set('memory_limit','256M');
	    
	    $sitecode = Yii::$app->user->identity->userProfile->sitecode;
	    Yii::$app->response->format = Response::FORMAT_JSON;
	    $ovfilter_sub = isset($_GET['ovfilter_sub'])?$_GET['ovfilter_sub']:0;
	    
	    $data = \backend\modules\ovcca\models\TbData1::find()
		    ->select(['id', "CONCAT(`hsitecode`, hptcode, ' ', `title`, `name`, ' ', `surname`) AS `name`"])
		    ->where('rstat<>3 AND hsitecode=:hsitecode', [':hsitecode'=>$sitecode]);
	    
	    $model = new \backend\modules\ovcca\models\ListForm();
	    $selecList = OvccaQuery::getSeclectList($ovfilter_sub);
	    
	    $model->list_person = $selecList?\yii\helpers\Json::encode($selecList):'[]';
	    
	    
	    if ($model->load(Yii::$app->request->post())) {
		Yii::$app->response->format = Response::FORMAT_JSON;
		if ($model->validate()) {
		    $saveList = \yii\helpers\Json::decode($model->list_person);
		    Yii::$app->db->createCommand()
			    ->delete('ov_sub_list', 'ov_sub_list.sub_id=:sub', [':sub'=>$ovfilter_sub])
			    ->execute();
		    if(is_array($saveList)){
			$return = [];
			
			foreach($saveList as $val) {
			    $return[$val] = $val;
			}
			
			$return = array_keys($return);
			foreach ($return as $value) {
			    $ov_sub_list = new OvSubList();
			    $ov_sub_list->sub_id = $ovfilter_sub;
			    $ov_sub_list->person_id = $value;
			    $ov_sub_list->filter_id = 0;
			    $ov_sub_list->save();
			    
			}
		    }
		    $result = [
			'status' => 'success',
			'action' => 'create',
			'message' => SDHtml::getMsgSuccess() . Yii::t('app', 'Data completed.'),
			'html' => $return,
		    ];
		    return $result;
		} else {
		    $result = [
			'status' => 'error',
			'message' => SDHtml::getMsgError() . Yii::t('app', 'Can not create the data.'),
		    ];
		    return $result;
		}
	    } else {
		$return = $this->renderAjax('_sublist', [
		    'model' => $model,
		    'data' => $data,
		    'ovfilter_sub' => $ovfilter_sub,
		]);
		$result = [
		    'status' => 'success',
		    'message' => SDHtml::getMsgError() . Yii::t('app', 'Load completed.'),
		    'html' => $return,
		];
		return $result;
	    }
	    
	    

	    
	} else {
	    throw new NotFoundHttpException('Invalid request. Please do not repeat this request again.');
	}
    }
    
    public function actionDeletesAll() {
	if (Yii::$app->getRequest()->isAjax) {
	    Yii::$app->response->format = Response::FORMAT_JSON;
	    $sitecode = Yii::$app->user->identity->userProfile->sitecode;
	    
	    $data = Yii::$app->db->createCommand()->delete('ov_person', 'hospcode=:hospcode', [':hospcode'=>$sitecode])->execute();
	    if($data){
		$result = [
		    'status' => 'success',
		    'action' => 'deletes',
		    'message' => SDHtml::getMsgSuccess() . Yii::t('app', 'Deleted completed.'),
		    'data' => $data,
		];
	    } else {
		$result = [
		    'status' => 'error',
		    'message' => SDHtml::getMsgError() . Yii::t('app', 'Can not delete the data.'),
		    'data' => $data,
		];
		return $result;
	    }
	    
	    return $result;
	} else {
	    throw new NotFoundHttpException('Invalid request. Please do not repeat this request again.');
	}
    }
    
    public function actionDeletes() {
	if (Yii::$app->getRequest()->isAjax) {
	    Yii::$app->response->format = Response::FORMAT_JSON;
	    if (isset($_POST['selection'])) {
		foreach ($_POST['selection'] as $id) {
		    $this->findModel($id)->delete();
		}
		$result = [
		    'status' => 'success',
		    'action' => 'deletes',
		    'message' => SDHtml::getMsgSuccess() . Yii::t('app', 'Deleted completed.'),
		    'data' => $_POST['selection'],
		];
		return $result;
	    } else {
		$result = [
		    'status' => 'error',
		    'message' => SDHtml::getMsgError() . Yii::t('app', 'Can not delete the data.'),
		    'data' => $id,
		];
		return $result;
	    }
	} else {
	    throw new NotFoundHttpException('Invalid request. Please do not repeat this request again.');
	}
    }
    
    public function actionImportViews() {
	if (Yii::$app->getRequest()->isAjax) {
	    try {
		
		
		$sitecode = Yii::$app->user->identity->userProfile->sitecode;
		
		$select = isset($_GET['select'])?$_GET['select']:0;
		$selectItem = isset($_POST['selection'])?$_POST['selection']:0;
		$dataPerson=0;
		if($select==1){
		    $strWhere = '';
		    if ($select){
			$strItem = join(',', $selectItem);
			$strWhere = " AND id IN ($strItem)";
		    } 

		    $dataPerson = OvPerson::find()->where("status_import=1 AND hospcode=:sitecode $strWhere", [':sitecode'=>$sitecode])->count();

		    
		} else {
		    
		    $dataPerson = OvPerson::find()->where('status_import=1 AND hospcode=:sitecode', [':sitecode'=>$sitecode])->count();
		}
		
		$import = \backend\modules\ovcca\classes\OvccaFunc::findViewsPerson($sitecode, $select, $selectItem);
		return $this->renderAjax('_import_view', [
		    'sum' => $import['sum']+$dataPerson,
		    'sumT' => $import['sumT'],
		    'sumF' => $import['sumF'],
		    'sumN' => $import['sumN']+$dataPerson,
		    'select' => $select,
		    'selectItem' => $selectItem,
		]);
		
	    } catch (\yii\db\Exception $e) {
		Yii::$app->response->format = Response::FORMAT_JSON;

		$result = [
		    'status' => 'error',
		    'message' => SDHtml::getMsgError() . '<strong>Error: ' . $e->getCode() . ' </strong> ' . $e->getMessage(),
		    'data' => $id,
		];
		return $result;
	    }
	} else {
	    throw new NotFoundHttpException('Invalid request. Please do not repeat this request again.');
	}
    }
    
    public function actionImportAll() {
	if (Yii::$app->getRequest()->isAjax) {
	    try {
		$sitecode = Yii::$app->user->identity->userProfile->sitecode;
		
		$select = isset($_GET['select'])?$_GET['select']:0;
		$selectItem = isset($_GET['selectItem'])?$_GET['selectItem']:0;
		
		$dataPerson=0;
		if($select==1){
		    $strWhere = '';
		    if ($select){
			$strItem = join(',', $selectItem);
			$strWhere = " AND id IN ($strItem)";
		    } 

		    $dataPerson = OvPerson::find()->where("status_import=1 AND hospcode=:sitecode $strWhere", [':sitecode'=>$sitecode])->count();

		    
		} else {
		    
		    $dataPerson = OvPerson::find()->where('status_import=1 AND hospcode=:sitecode', [':sitecode'=>$sitecode])->count();
		}
		
		Yii::$app->db->createCommand('DELETE FROM `tmp_person` WHERE hospcode=:sitecode', [':sitecode'=>$sitecode])->execute();
		$import = \backend\modules\ovcca\classes\OvccaFunc::findImportPerson($sitecode, $select, $selectItem);
		return $this->renderAjax('_import', [
		    'sum' => $import['sum']+$dataPerson,
		    //'sum' => $import['sum'],
		    'sumT' => $import['sumT'],
		    'sumF' => $import['sumF'],
		    'sumN' => $import['sumN']+$dataPerson,
		    //'sumN' => $import['sumN'],
		]);
		
	    } catch (\yii\db\Exception $e) {
		Yii::$app->response->format = Response::FORMAT_JSON;

		$result = [
		    'status' => 'error',
		    'message' => SDHtml::getMsgError() . '<strong>Error: ' . $e->getCode() . ' </strong> ' . $e->getMessage(),
		    'data' => $id,
		];
		return $result;
	    }
	} else {
	    throw new NotFoundHttpException('Invalid request. Please do not repeat this request again.');
	}
    }
    
    public function actionImportUpdate() {
	if (Yii::$app->getRequest()->isAjax) {
	    try {
		$sitecode = Yii::$app->user->identity->userProfile->sitecode;
		
		$select = isset($_GET['select'])?$_GET['select']:0;
		$selectItem = isset($_GET['selectItem'])?$_GET['selectItem']:0;
		
		$import = \backend\modules\ovcca\classes\OvccaFunc::findImportPersonUpdate($sitecode, $select, $selectItem);
		//$dataPerson = OvPerson::find()->where('status_import=1 AND hospcode=:sitecode', [':sitecode'=>$sitecode])->count();
		return $this->renderAjax('_import', [
		    //'sum' => $import['sum']+$dataPerson,
		    'sum' => $import['sum'],
		    'sumT' => $import['sumT'],
		    'sumF' => $import['sumF'],
		    //'sumN' => $import['sumN']+$dataPerson,
		    'sumN' => $import['sumN'],
		]);
		
	    } catch (\yii\db\Exception $e) {
		Yii::$app->response->format = Response::FORMAT_JSON;

		$result = [
		    'status' => 'error',
		    'message' => SDHtml::getMsgError() . '<strong>Error: ' . $e->getCode() . ' </strong> ' . $e->getMessage(),
		    'data' => $id,
		];
		return $result;
	    }
	} else {
	    throw new NotFoundHttpException('Invalid request. Please do not repeat this request again.');
	}
    }

    public function actionReportOvcca() {
	if (Yii::$app->getRequest()->isAjax) {
	    Yii::$app->response->format = Response::FORMAT_JSON;
	    $fid = isset($_GET['fid'])?$_GET['fid']:0;
	    $sid = isset($_GET['sid'])?$_GET['sid']:0;
	    
	    
	    $selection = null;
	    if (isset($_POST['selection'])) {
		$selection = implode(',', $_POST['selection']);
		Yii::$app->session['selection'] = $selection;
	    } else {
		unset(Yii::$app->session['selection']);
	    }
	    
	    $url = \yii\helpers\Url::to(['report', 'ovfilter'=>$fid, 'ovfilter_sub'=>$sid]);
	    
	    $html = \backend\modules\ovcca\classes\OvccaFunc::getIframe($url);
	    $result = [
		'status' => 'success',
		'action' => 'report',
		'message' => SDHtml::getMsgSuccess() . Yii::t('app', 'Load completed.'),
		'html' => $html,
	    ];
	    return $result;
	} else {
	    throw new NotFoundHttpException('Invalid request. Please do not repeat this request again.');
	}
    }
    
    public function actionReportDetail() {
	if (Yii::$app->getRequest()->isAjax) {
	    Yii::$app->response->format = Response::FORMAT_JSON;
	    $ovfilter = isset($_GET['fid'])?$_GET['fid']:0;
	    $ovfilter_sub = isset($_GET['sid'])?$_GET['sid']:0;
	    
	    $data;
	    $sitecode = Yii::$app->user->identity->userProfile->sitecode;
	    if($ovfilter_sub>0){
		$data = OvccaQuery::getOVDataSetect($sitecode, $ovfilter, $ovfilter_sub);
	    } else {
		$data = OvccaQuery::getOVData($sitecode);
	    }
	    
	    $i=0;
	    $sumT =0;
	    $sumTY =0;
	    $sumTN =0;
	    $sumF =0;
	    $sumFY =0;
	    $sumFN =0;
	    //getOVDataSetect
	    foreach ($data as $key => $value) {
		if($value['ptcode']!='' && $value['ptid_key']!=''){
		    $dataCCA01 = OvccaQuery::getCCA01($value['ptid_key'], Yii::$app->session['ovstart'], Yii::$app->session['ovend']);
		    if($dataCCA01){
			$value['cca01'] = 1;
		    } else{
			$value['cca01'] = 0;
		    }

		    $dataOv01 = OvccaQuery::getOv01($value['ptid_key'], 'tbdata_21', Yii::$app->session['ovstart'], Yii::$app->session['ovend']);
		    if($dataOv01){
			$r = 0;
			if($dataOv01['results']=='1'){
			    $r = 1;
			} elseif($dataOv01['results']=='0'){
			    $r = 1;
			}

			$value['ov01k'] = $r;
		    }else{
			$value['ov01k'] = 0;
		    }

		    $dataOv01 = OvccaQuery::getOv01($value['ptid_key'], 'tbdata_25', Yii::$app->session['ovstart'], Yii::$app->session['ovend']);
		    if($dataOv01){
			$value['ov02'] = 1;
		    }else{
			$value['ov02'] = 0;
		    }

		}
		if($value['ov01_status']==1){
		    $sumT++;
		    if($value['cca01']==1 && $value['ov01k']==1 && $value['ov02']==1){
			$sumTY++;
		    } else{
			$sumTN++;
		    }
		} else{
		    $sumF++;
		    if($value['cca01']==1 && $value['ov02']==1){
			$sumFY++;
		    } else{
			$sumFN++;
		    }
		}
		
		
		
		$i++;
	    }
	
	    $html = $this->renderAjax('_detail_view', [
		'sum' => $i,
		'sumT' => $sumT,
		'sumF' => $sumF,
		'sumTY' => $sumTY,
		'sumFY' => $sumFY,
		'sumTN' => $sumTN,
		'sumFN' => $sumFN,
	    ]);
	    
	    $result = [
		'status' => 'success',
		'action' => 'report',
		'message' => SDHtml::getMsgSuccess() . Yii::t('app', 'Load completed.'),
		'html' => $html,
	    ];
	    return $result;
	} else {
	    throw new NotFoundHttpException('Invalid request. Please do not repeat this request again.');
	}
    }
    
    public function actionReportSticker() {
	if (Yii::$app->getRequest()->isAjax) {
	    Yii::$app->response->format = Response::FORMAT_JSON;
	    $fid = isset($_GET['fid'])?$_GET['fid']:0;
	    $sid = isset($_GET['sid'])?$_GET['sid']:0;
	    $sizemini = isset($_GET['sizemini'])?$_GET['sizemini']:0;
	    
	    $selection = null;
	    if (isset($_POST['selection'])) {
		$selection = implode(',', $_POST['selection']);
		Yii::$app->session['selection'] = $selection;
	    } else {
		unset(Yii::$app->session['selection']);
	    }
	    
	    $page = 'report-label';
	    
	    if($sizemini==1){
		$page = 'report-labelmini';
	    } 
	    
	    $url = \yii\helpers\Url::to([$page, 'ovfilter'=>$fid, 'ovfilter_sub'=>$sid]);
	    
	    $html = \backend\modules\ovcca\classes\OvccaFunc::getIframe($url);
	    $result = [
		'status' => 'success',
		'action' => 'report',
		'message' => SDHtml::getMsgSuccess() . Yii::t('app', 'Load completed.'),
		'html' => $html,
	    ];
	    return $result;
	} else {
	    throw new NotFoundHttpException('Invalid request. Please do not repeat this request again.');
	}
    }
    
    public function actionReportExel() {
	if (Yii::$app->getRequest()->isAjax) {
	    Yii::$app->response->format = Response::FORMAT_JSON;
	    $fid = isset($_GET['fid'])?$_GET['fid']:0;
	    $sid = isset($_GET['sid'])?$_GET['sid']:0;
	    
	    $selection = null;
	    $post = null;
	    if (isset($_POST['selection'])) {
		$selection = implode(',', $_POST['selection']);
		Yii::$app->session['selection'] = $selection;
	    } else {
		unset(Yii::$app->session['selection']);
	    }
	    

	    $url = \yii\helpers\Url::to(['report-export', 'ovfilter'=>$fid, 'ovfilter_sub'=>$sid]);
	    
	    $html = \backend\modules\ovcca\classes\OvccaFunc::getIframe($url);
	    
	    $result = [
		'status' => 'success',
		'action' => 'report',
		'message' => SDHtml::getMsgSuccess() . Yii::t('app', 'Load completed.'),
		'html' => $html,
	    ];
	    return $result;
	} else {
	    throw new NotFoundHttpException('Invalid request. Please do not repeat this request again.');
	}
    }
    
    public function actionReport($ovfilter, $ovfilter_sub, $selection=null)
    {
	if(isset(Yii::$app->session['selection'])){
	    $selection = Yii::$app->session['selection'];
	}
	ini_set('max_execution_time', 0);
	set_time_limit(0);
	ini_set('memory_limit','512M');        
	// create new PDF document
	//PDF_UNIT = pt , mm , cm , in 
	//PDF_PAGE_ORIENTATION = P , LANDSCAPE = L
	//PDF_PAGE_FORMAT 4A0,2A0,A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,B0,B1,B2,B3,B4,B5,B6,B7,B8,B9,B10,C0,C1,C2,C3,C4,C5,C6,C7,C8,C9,C10,RA0,RA1,RA2,RA3,RA4,SRA0,SRA1,SRA2,SRA3,SRA4,LETTER,LEGAL,EXECUTIVE,FOLIO
	$orient = 'L';

	$pdf = new SDPDF($orient, PDF_UNIT, 'A4', true, 'UTF-8', false);
	//spl_autoload_register(array('YiiBase', 'autoload'));

	// set document information
	$pdf->SetCreator('AppXQ');
	$pdf->SetAuthor('iencoded@gmail.com');
	$pdf->SetTitle('Report');
	$pdf->SetSubject('Original');
	$pdf->SetKeywords('AppXQ, SDII, PDF, report, medical, clinic');

	// remove default header/footer
	$pdf->setPrintHeader(TRUE);
	$pdf->setPrintFooter(TRUE);

	// set margins
	$pdf->SetMargins(10, 17, 10);
	$pdf->SetHeaderMargin(10);
	$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

	// set auto page breaks
	$pdf->SetAutoPageBreak(TRUE, 15);

	// set image scale factor
	$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

	// set font
	$pdf->SetFont($pdf->fontName, '', 10);

	/* ------------------------------------------------------------------------------------------------------------------ */
	$header = [
	    [
		'name'=>'orderID',
		'txt'=>'ลำดับ',
		'w'=>13,
		'align'=>'C'
	    ],
	    [
		'name'=>'hsitecode',
		'txt'=>'HCODE',
		'w'=>12,
		'align'=>'C'
	    ],
	    [
		'name'=>'hptcode',
		'txt'=>'PID',
		'w'=>12,
		'align'=>'C'
	    ],
	    [
		'name'=>'cid',
		'txt'=>'บัตรประชาชน',
		'w'=>25,
		'align'=>'C'
	    ],
	    [
		'name'=>'fullname',
		'txt'=>'ชื่อ',
		'w'=>40,
		'align'=>'L'
	    ],
	    [
		'name'=>'age',
		'txt'=>'อายุ',
		'w'=>10,
		'align'=>'C'
	    ],
	    [
		'name'=>'add1n1',
		'txt'=>'บ้านเลขที่',
		'w'=>15,
		'align'=>'C'
	    ],
	    [
		'name'=>'add1n5',
		'txt'=>'หมู่',
		'w'=>10,
		'align'=>'C'
	    ],
	    [
		'name'=>'notset',
		'txt'=>'อยู่บ้าน',
		'w'=>13,
		'align'=>'C'
	    ],
	    [
		'name'=>'notset',
		'txt'=>'ให้คำยินยอม',
		'w'=>18,
		'align'=>'C'
	    ],
	    [
		'name'=>'cca01',
		'txt'=>'CCA-01',
		'w'=>14,
		'align'=>'C'
	    ],
	    [
		'name'=>'ov01k',
		'txt'=>'OV-01K',
		'w'=>14,
		'align'=>'C'
	    ],
	    [
		'name'=>'ov01p',
		'txt'=>'OV-01P',
		'w'=>14,
		'align'=>'C'
	    ],
	    [
		'name'=>'ov01f',
		'txt'=>'OV-01F',
		'w'=>14,
		'align'=>'C'
	    ],
	    [
		'name'=>'ov01u',
		'txt'=>'OV-01U',
		'w'=>14,
		'align'=>'C'
	    ],
	    [
		'name'=>'ov02',
		'txt'=>'OV-02',
		'w'=>13,
		'align'=>'C'
	    ],
	    [
		'name'=>'ov03',
		'txt'=>'OV-03',
		'w'=>13,
		'align'=>'C'
	    ],
	    [
		'name'=>'cca02',
		'txt'=>'CCA-02',
		'w'=>0,
		'align'=>'C'
	    ],
//	    [
//		'name'=>'notset',
//		'txt'=>'หมายเหตุ',
//		'w'=>0,
//		'align'=>'C'
//	    ],
	];
	$sitecode = Yii::$app->user->identity->userProfile->sitecode;
	
	
	$pdf->contentHeader=[
	    'title'=>'รายงานติดตามกำกับงาน',
	    
	];
	$pdf->tableHeader=$header;
	$pdf->lineFooter=true;

	// add a page
	$pdf->AddPage();
	
	$data = OvccaQuery::getTbData($sitecode, $ovfilter_sub, $selection);
	
	//getOVDataSetect
	foreach ($data as $key => $value) {
	   
	    if ($value['id']!=null) {
		
		if ($value['cca01_id']!=null) {
		    $arrStr = explode(',', $value['cca01_id']);
		    $rowReturn = '';
		    $comma = '';
		    foreach ($arrStr as $valueID) {
			$rowReturn .= $comma.'/';
			$comma = ', ';
		    }
		    $data[$key]['cca01'] = $rowReturn;
		}
		
		if ($value['ov01k_id']!=null) {
		    //exit();
		    $arrStr = explode(',', $value['ov01k_id']);
		    $arrResult = explode(',', $value['ov01k_result']);
		    $rowReturn = '';
		    $comma = '';
		    foreach ($arrStr as $i=>$valueID) {
			if($arrResult[$i]=='1'){
			    $rowReturn .= $comma.'+';
			} elseif($arrResult[$i]=='0'){
			    $rowReturn .= $comma.'-';
			}else {
			    $rowReturn .= $comma.'';
			}
			$comma = ', ';
		    }
		    $data[$key]['ov01k'] = $rowReturn;
		}
		
		if ($value['ov01p_id']!=null) {
		    $arrStr = explode(',', $value['ov01p_id']);
		    $arrResult = explode(',', $value['ov01p_result']);
		    $rowReturn = '';
		    $comma = '';
		    foreach ($arrStr as $i=>$valueID) {
			if($arrResult[$i]=='1'){
			    $rowReturn .= $comma.'+';
			} elseif($arrResult[$i]=='0'){
			    $rowReturn .= $comma.'-';
			}else {
			    $rowReturn .= $comma.'';
			}
			$comma = ', ';
		    }
		    $data[$key]['ov01p'] = $rowReturn;
		}
		
		if ($value['ov01f_id']!=null) {
		    $arrStr = explode(',', $value['ov01f_id']);
		    $arrResult = explode(',', $value['ov01f_result']);
		    $rowReturn = '';
		    $comma = '';
		    foreach ($arrStr as $i=>$valueID) {
			if($arrResult[$i]=='1'){
			    $rowReturn .= $comma.'+';
			} elseif($arrResult[$i]=='0'){
			    $rowReturn .= $comma.'-';
			}else {
			    $rowReturn .= $comma.'';
			}
			$comma = ', ';
		    }
		    $data[$key]['ov01f'] = $rowReturn;
		}
		
		if ($value['ov01u_id']!=null) {
		    $arrStr = explode(',', $value['ov01u_id']);
		    $arrResult = explode(',', $value['ov01u_result']);
		    $rowReturn = '';
		    $comma = '';
		    foreach ($arrStr as $i=>$valueID) {
			if($arrResult[$i]=='1'){
			    $rowReturn .= $comma.'+';
			} elseif($arrResult[$i]=='0'){
			    $rowReturn .= $comma.'-';
			}else {
			    $rowReturn .= $comma.'';
			}
			$comma = ', ';
		    }
		    $data[$key]['ov01u'] = $rowReturn;
		}
		
		if ($value['ov02_id']!=null) {
		    $arrStr = explode(',', $value['ov02_id']);
		    $rowReturn = '';
		    $comma = '';
		    foreach ($arrStr as $valueID) {
			$rowReturn .= $comma.'/';
			$comma = ', ';
		    }
		    $data[$key]['ov02'] = $rowReturn;
		}
		
		if ($value['ov03_id']!=null) {
		    $arrStr = explode(',', $value['ov03_id']);
		    $rowReturn = '';
		    $comma = '';
		    foreach ($arrStr as $valueID) {
			$rowReturn .= $comma.'/';
			$comma = ', ';
		    }
		    $data[$key]['ov03'] = $rowReturn;
		}
		
		if ($value['cca02_id']!=null) {
		    $arrStr = explode(',', $value['cca02_id']);
		    $rowReturn = '';
		    $comma = '';
		    foreach ($arrStr as $valueID) {
			$rowReturn .= $comma.'/';
			$comma = ', ';
		    }
		    $data[$key]['cca02'] = $rowReturn;
		}
		
//		if ($value['v2']!=null) {
//		    $age = SDdate::getAge($value['v2']);
//		    $data[$key]['age'] = $age;
//		}
		
		if ($value['name']!=null) {
		    
		    $data[$key]['fullname'] = $value['title'].$value['name'].' '.$value['surname'];
		}
		
	    }
	}
	
	$pdf->createTableData($header, $data, false);

	$pdf->Ln();
	$pdf->Cell(0, 7, 'หมายเหตุ***', 0, 1);
	$pdf->Cell(0, 7, 'CCA-01 = ข้อมูลปัจจัยเสี่ยง', 0, 1);
	$pdf->Cell(0, 7, 'OV-01k = ตรวจอุจจาระวิธี Krato-Katz', 0, 1);
	$pdf->Cell(0, 7, 'OV-01P = ตรวจอุจจาระวิธี Parasep', 0, 1);
	$pdf->Cell(0, 7, 'OV-01F = ตรวจอุจจาระวิธี FECT', 0, 1);
	$pdf->Cell(0, 7, 'OV-01U = ตรวจปัสสาวะ', 0, 1);
	$pdf->Cell(0, 7, 'OV-02 = สำรวจพฤติกรรม', 0, 1);
	$pdf->Cell(0, 7, 'OV-03 = ให้การรักษาพยาธิใบไม้ตับ', 0, 1);
	
	
	$pdf->Output('report.pdf', 'I');
	Yii::$app->end();
    }
    
    public function actionReportLabel($ovfilter, $ovfilter_sub, $selection=null)
    {
	ini_set('max_execution_time', 0);
	set_time_limit(0);
	ini_set('memory_limit','512M'); 
	if(isset(Yii::$app->session['selection'])){
	    $selection = Yii::$app->session['selection'];
	}
	// create new PDF document
	//PDF_UNIT = pt , mm , cm , in 
	//PDF_PAGE_ORIENTATION = P , LANDSCAPE = L
	//PDF_PAGE_FORMAT 4A0,2A0,A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,B0,B1,B2,B3,B4,B5,B6,B7,B8,B9,B10,C0,C1,C2,C3,C4,C5,C6,C7,C8,C9,C10,RA0,RA1,RA2,RA3,RA4,SRA0,SRA1,SRA2,SRA3,SRA4,LETTER,LEGAL,EXECUTIVE,FOLIO
	$orient = 'L';

	$pdf = new SDPDF($orient, PDF_UNIT, 'A4', true, 'UTF-8', false);
	//spl_autoload_register(array('YiiBase', 'autoload'));

	// set document information
	$pdf->SetCreator('AppXQ');
	$pdf->SetAuthor('iencoded@gmail.com');
	$pdf->SetTitle('Report');
	$pdf->SetSubject('Original');
	$pdf->SetKeywords('AppXQ, SDII, PDF, report, medical, clinic');

	// remove default header/footer
	$pdf->setPrintHeader(false);
	$pdf->setPrintFooter(false);

	// set margins
	$pdf->SetMargins(10, 10, 10);
	$pdf->SetHeaderMargin(10);
	$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

	// set auto page breaks
	$pdf->SetAutoPageBreak(TRUE, 25);

	// set image scale factor
	$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

	// set font
	$pdf->SetFont($pdf->fontName, '', $pdf->fontSize);

	/* ------------------------------------------------------------------------------------------------------------------ */
	$pdf->fontSize=14;
	$sitecode = Yii::$app->user->identity->userProfile->sitecode;
	$style = array(
			'position' => '',
			'align' => 'C',
			'stretch' => false,
			'fitwidth' => true,
			'cellfitalign' => '',
			'border' => false,
			'fgcolor' => array(0,0,0),
			'bgcolor' => false, //array(255,255,255),
			'text' => true,
			'font' => 'helvetica',
			'fontsize' => 10,
			'stretchtext' => 0
		);
	$data = OvccaQuery::getTbSkData($sitecode, $ovfilter_sub, $selection);
	
	$pdf->AddPage();
	//getOVDataSetect
	$pdf->SetFont($pdf->fontName, '', $pdf->fontSize+4);

	$rowX = 155;
	$rowY = 10;
	$rowY1 = $rowY;
	$addPage = 1;
	foreach ($data as $key => $value) {
		$age = $value['age'];
		$txtFull = '&nbsp;HOSPCODE '.$value['hsitecode'] . '   PID '.$value['hptcode'].'<br> ชื่อ-สกุล '.$value['title'].$value['name'].' '.$value['surname']."<br> อายุ {$age} ปี วันที่เก็บ ......./......./.......";

		$pdf->SetFont($pdf->fontName, '', $pdf->fontSize+5);
		$pdf->MultiCell(70, 28, $txtFull, 1, 'J', FALSE, 0, 15, $rowY1, true, 0, true, true, 0, 'T', false);
		$pdf->MultiCell(70, 28, $txtFull, 1, 'J', FALSE, 1, 85, $rowY1, true, 0, true, true, 0, 'T', false);

		$pdf->SetFont($pdf->fontName, '', $pdf->fontSize-4);
		$pdf->MultiCell(40, 14, $txtFull, 1, 'J', FALSE, 0, $rowX, $rowY, true, 0, true, true, 0, 'T', false);
		$pdf->MultiCell(40, 14, $txtFull, 1, 'J', FALSE, 0, $rowX+40, $rowY, true, 0, true, true, 0, 'T', false);
		$pdf->MultiCell(40, 14, $txtFull, 1, 'J', FALSE, 1, $rowX+40+40, $rowY, true, 0, true, true, 0, 'T', false);
		$rowY+=14;

		$pdf->MultiCell(40, 14, $txtFull, 1, 'J', FALSE, 0, $rowX, $rowY, true, 0, true, true, 0, 'T', false);
		$pdf->MultiCell(40, 14, $txtFull, 1, 'J', FALSE, 0, $rowX+40, $rowY, true, 0, true, true, 0, 'T', false);
		$pdf->MultiCell(40, 14, $txtFull, 1, 'J', FALSE, 1, $rowX+40+40, $rowY, true, 0, true, true, 0, 'T', false);

		if($addPage==6){
		    $pdf->AddPage();
		    //getOVDataSetect
		    $pdf->SetFont($pdf->fontName, '', $pdf->fontSize+4);
		    
		    $rowX = 155;
		    $rowY = 10;
		    $rowY1 = $rowY;
		    $addPage = 1;
		    
		    continue;
		}
		
		$rowY+=14;
		$rowY1+=28;
		$addPage++;
	}
	
	$pdf->Output('report.pdf', 'I');
	Yii::$app->end();
    }
    
    public function actionReportLabelmini($ovfilter, $ovfilter_sub, $selection=null)
    {
	ini_set('max_execution_time', 0);
	set_time_limit(0);
	ini_set('memory_limit','512M'); 
	if(isset(Yii::$app->session['selection'])){
	    $selection = Yii::$app->session['selection'];
	}
	// create new PDF document
	//PDF_UNIT = pt , mm , cm , in 
	//PDF_PAGE_ORIENTATION = P , LANDSCAPE = L
	//PDF_PAGE_FORMAT 4A0,2A0,A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,B0,B1,B2,B3,B4,B5,B6,B7,B8,B9,B10,C0,C1,C2,C3,C4,C5,C6,C7,C8,C9,C10,RA0,RA1,RA2,RA3,RA4,SRA0,SRA1,SRA2,SRA3,SRA4,LETTER,LEGAL,EXECUTIVE,FOLIO
	$orient = 'L';

	$pdf = new SDPDF($orient, PDF_UNIT, 'A4', true, 'UTF-8', false);
	//spl_autoload_register(array('YiiBase', 'autoload'));

	// set document information
	$pdf->SetCreator('AppXQ');
	$pdf->SetAuthor('iencoded@gmail.com');
	$pdf->SetTitle('Report');
	$pdf->SetSubject('Original');
	$pdf->SetKeywords('AppXQ, SDII, PDF, report, medical, clinic');

	// remove default header/footer
	$pdf->setPrintHeader(false);
	$pdf->setPrintFooter(false);

	// set margins
	$pdf->SetMargins(10, 10, 10);
	$pdf->SetHeaderMargin(10);
	$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

	// set auto page breaks
	$pdf->SetAutoPageBreak(TRUE, 25);

	// set image scale factor
	$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

	// set font
	$pdf->SetFont($pdf->fontName, '', $pdf->fontSize);

	/* ------------------------------------------------------------------------------------------------------------------ */
	$pdf->fontSize=14;
	$sitecode = Yii::$app->user->identity->userProfile->sitecode;
	$style = array(
			'position' => '',
			'align' => 'C',
			'stretch' => false,
			'fitwidth' => true,
			'cellfitalign' => '',
			'border' => false,
			'fgcolor' => array(0,0,0),
			'bgcolor' => false, //array(255,255,255),
			'text' => true,
			'font' => 'helvetica',
			'fontsize' => 10,
			'stretchtext' => 0
		);
	$data = OvccaQuery::getTbSkData($sitecode, $ovfilter_sub, $selection);
	
	$pdf->AddPage();
	//getOVDataSetect
	$pdf->SetFont($pdf->fontName, '', $pdf->fontSize+4);

	$rowX = 85;
	$rowY = 10;
	$rowY1 = $rowY;
	$addPage = 0;
	$col = 0;
	foreach ($data as $key => $value) {
		$age = $value['age'];
		$txtFull = '&nbsp;HOSPCODE '.$value['hsitecode'] . '   PID '.$value['hptcode'].'<br> ชื่อ-สกุล '.$value['title'].$value['name'].' '.$value['surname']."<br> อายุ {$age} ปี วันที่เก็บ ......./......./.......";
	    if($col==0){
		$pdf->SetFont($pdf->fontName, '', $pdf->fontSize+5);
		$pdf->MultiCell(70, 28, $txtFull, 1, 'J', FALSE, 0, 15, $rowY1, true, 0, true, true, 0, 'T', false);
		$pdf->SetFont($pdf->fontName, '', $pdf->fontSize-4);
		$pdf->MultiCell(40, 14, $txtFull, 1, 'J', FALSE, 0, $rowX, $rowY, true, 0, true, true, 0, 'T', false);
		$rowY+=14;
		$pdf->MultiCell(40, 14, $txtFull, 1, 'J', FALSE, 0, $rowX, $rowY, true, 0, true, true, 0, 'T', false);
		$rowY-=14;
		$col=110;
	    } else {
		$pdf->SetFont($pdf->fontName, '', $pdf->fontSize+5);
		$pdf->MultiCell(70, 28, $txtFull, 1, 'J', FALSE, 0, 15+$col, $rowY1, true, 0, true, true, 0, 'T', false);
		$pdf->SetFont($pdf->fontName, '', $pdf->fontSize-4);
		$pdf->MultiCell(40, 14, $txtFull, 1, 'J', FALSE, 0, $rowX+$col, $rowY, true, 0, true, true, 0, 'T', false);
		$rowY+=14;
		$pdf->MultiCell(40, 14, $txtFull, 1, 'J', FALSE, 0, $rowX+$col, $rowY, true, 0, true, true, 0, 'T', false);	
		
		$col=0;
		$rowY+=14;
		$rowY1+=28;
		$addPage++;
	    }	
		
		if($addPage==6){
		    $pdf->AddPage();
		    //getOVDataSetect
		    $pdf->SetFont($pdf->fontName, '', $pdf->fontSize+4);
		    
		    $rowX = 85;
		    $rowY = 10;
		    $rowY1 = $rowY;
		    $addPage = 1;
		    $col=0;
		    
		    continue;
		}
	}
	
	$pdf->Output('report.pdf', 'I');
	Yii::$app->end();
    }
    
    public function actionReportExport($ovfilter, $ovfilter_sub, $selection=null)//
    {
	ini_set('max_execution_time', 0);
	set_time_limit(0);
	ini_set('memory_limit','512M'); 
	
	if(isset(Yii::$app->session['selection'])){
	    $selection = Yii::$app->session['selection'];
	}
	
	$objPHPExcel = new \common\lib\phpexcel\SDExcelView();
	
	// Set document properties
	$objPHPExcel->getProperties()->setCreator("Damasac")
				    ->setLastModifiedBy("Damasac")
				    ->setTitle("Export Document")
				    ->setSubject("Export Document")
				    ->setDescription("Export ovcca")
				    ->setKeywords("ovcca")
				    ->setCategory("ovcca");
	// Add some data
	$sitecode = Yii::$app->user->identity->userProfile->sitecode;
	$data = OvccaQuery::getTbData($sitecode, $ovfilter_sub, $selection);
	
	$dataColumn = [
		'hsitecode'=>'HOSPCODE', 
		'hptcode'=>'PID', 
		//'cid'=>'บัตรประชาชน', 
		'fullname'=>'ชื่อ-สกุล', 
		'age'=>'อายุ', 
		'add1n1'=>'บ้านเลขที่', 
		'add1n5'=>'หมู่', 
		'notset'=>'อยู่บ้าน', 
		'notset'=>'ให้คำยินยอม', 
		'cca01'=>'CCA-01', 
		'ov01k'=>'OV-01K', 
		'ov01p'=>'OV-01P', 
		'ov01f'=>'OV-01F', 
		'ov01u'=>'OV-01U', 
		'ov02'=>'OV-02', 
		'ov03'=>'OV-03', 
		'cca02'=>'CCA-02', 
		'commtnotset'=>'หมายเหตุ'
	    ];
	$column = 'A';
	$row=1;
	foreach ($dataColumn as $key => $value) {
	    $objPHPExcel->setActiveSheetIndex(0)->setCellValue($column . $row, $value);
	    $column++;
	}
//	
//	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1', 'HOSPCODE')
//		    ->setCellValue('B1', 'PID')
//		    ->setCellValue('C1', 'บัตรประชาชน')
//		    ->setCellValue('C1', 'ชื่อ')
//		    ->setCellValue('D1', 'อายุ')
//		    ->setCellValue('E1', 'บ้านเลขที่')
//		    ->setCellValue('F1', 'หมู่')
//		    ->setCellValue('G1', 'อยู่บ้าน')
//		    ->setCellValue('H1', 'ให้คำยินยอม')
//		    ->setCellValue('I1', 'CCA-01')
//		    ->setCellValue('J1', 'OV-01K')
//		    ->setCellValue('K1', 'OV-01P')
//		    ->setCellValue('L1', 'OV-01F')
//		    ->setCellValue('M1', 'OV-01U')
//		    ->setCellValue('N1', 'OV-02')
//		    ->setCellValue('O1', 'OV-03')
//		    ->setCellValue('P1', 'CCA-02')
//		    ->setCellValue('Q1', 'หมายเหตุ');
//	    
	$row=2;
	//getOVDataSetect
	foreach ($data as $key => $value) {
	    
	    if ($value['id']!=null) {
		
		if ($value['cca01_id']!=null) {
		    $arrStr = explode(',', $value['cca01_id']);
		    $rowReturn = '';
		    $comma = '';
		    foreach ($arrStr as $valueID) {
			$rowReturn .= $comma.'/';
			$comma = ', ';
		    }
		    $value['cca01'] = $rowReturn;
		}
		
		if ($value['ov01k_id']!=null) {
		    //exit();
		    $arrStr = explode(',', $value['ov01k_id']);
		    $arrResult = explode(',', $value['ov01k_result']);
		    $rowReturn = '';
		    $comma = '';
		    foreach ($arrStr as $i=>$valueID) {
			if($arrResult[$i]=='1'){
			    $rowReturn .= $comma.'+';
			} elseif($arrResult[$i]=='0'){
			    $rowReturn .= $comma.'-';
			}else {
			    $rowReturn .= $comma.'';
			}
			$comma = ', ';
		    }
		    $value['ov01k'] = $rowReturn;
		}
		
		if ($value['ov01p_id']!=null) {
		    $arrStr = explode(',', $value['ov01p_id']);
		    $arrResult = explode(',', $value['ov01p_result']);
		    $rowReturn = '';
		    $comma = '';
		    foreach ($arrStr as $i=>$valueID) {
			if($arrResult[$i]=='1'){
			    $rowReturn .= $comma.'+';
			} elseif($arrResult[$i]=='0'){
			    $rowReturn .= $comma.'-';
			}else {
			    $rowReturn .= $comma.'';
			}
			$comma = ', ';
		    }
		    $value['ov01p'] = $rowReturn;
		}
		
		if ($value['ov01f_id']!=null) {
		    $arrStr = explode(',', $value['ov01f_id']);
		    $arrResult = explode(',', $value['ov01f_result']);
		    $rowReturn = '';
		    $comma = '';
		    foreach ($arrStr as $i=>$valueID) {
			if($arrResult[$i]=='1'){
			    $rowReturn .= $comma.'+';
			} elseif($arrResult[$i]=='0'){
			    $rowReturn .= $comma.'-';
			}else {
			    $rowReturn .= $comma.'';
			}
			$comma = ', ';
		    }
		    $value['ov01f'] = $rowReturn;
		}
		
		if ($value['ov01u_id']!=null) {
		    $arrStr = explode(',', $value['ov01u_id']);
		    $arrResult = explode(',', $value['ov01u_result']);
		    $rowReturn = '';
		    $comma = '';
		    foreach ($arrStr as $i=>$valueID) {
			if($arrResult[$i]=='1'){
			    $rowReturn .= $comma.'+';
			} elseif($arrResult[$i]=='0'){
			    $rowReturn .= $comma.'-';
			}else {
			    $rowReturn .= $comma.'';
			}
			$comma = ', ';
		    }
		    $value['ov01u'] = $rowReturn;
		}
		
		if ($value['ov02_id']!=null) {
		    $arrStr = explode(',', $value['ov02_id']);
		    $rowReturn = '';
		    $comma = '';
		    foreach ($arrStr as $valueID) {
			$rowReturn .= $comma.'/';
			$comma = ', ';
		    }
		    $value['ov02'] = $rowReturn;
		}
		
		if ($value['ov03_id']!=null) {
		    $arrStr = explode(',', $value['ov03_id']);
		    $rowReturn = '';
		    $comma = '';
		    foreach ($arrStr as $valueID) {
			$rowReturn .= $comma.'/';
			$comma = ', ';
		    }
		    $value['ov03'] = $rowReturn;
		}
		
		if ($value['cca02_id']!=null) {
		    $arrStr = explode(',', $value['cca02_id']);
		    $rowReturn = '';
		    $comma = '';
		    foreach ($arrStr as $valueID) {
			$rowReturn .= $comma.'/';
			$comma = ', ';
		    }
		    $value['cca02'] = $rowReturn;
		}
		
//		$age = SDdate::getAge($value['v2']);
//		$value['age'] = $age;
		
		$value['fullname'] = $value['title'].$value['name'].' '.$value['surname'];
	    }
	    
	    $column = 'A';
	    foreach ($dataColumn as $keyCol => $valueCol) {
		
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue($column . $row, $value[$keyCol]);
		$column++;
	    }
//	    
//	    $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$ic, $value['hsitecode'])
//		    ->setCellValue('B'.$ic, $value['hptcode'])
//		    ->setCellValue('C'.$ic, $value['fullname'])
//		    ->setCellValue('D'.$ic, $value['age'])
//		    ->setCellValue('E'.$ic, $value['add1n1'])
//		    ->setCellValue('F'.$ic, $value['add1n5'])
//		    ->setCellValue('G'.$ic, $value['notset'])
//		    ->setCellValue('H'.$ic, $value['notset'])
//		    ->setCellValue('I'.$ic, $value['cca01'])
//		    ->setCellValue('J'.$ic, $value['ov01k'])
//		    ->setCellValue('K'.$ic, $value['ov01p'])
//		    ->setCellValue('L'.$ic, $value['ov01f'])
//		    ->setCellValue('M'.$ic, $value['ov01u'])
//		    ->setCellValue('N'.$ic, $value['ov02'])
//		    ->setCellValue('O'.$ic, $value['ov03'])
//		    ->setCellValue('P'.$ic, $value['cca02'])
//		    ->setCellValue('Q'.$ic, $value['notset']);
//	    
	    $row++;
	}
	
	// Rename worksheet
	$objPHPExcel->getActiveSheet()->setTitle('OV-CCA');
	// Set active sheet index to the first sheet, so Excel opens this as the first sheet
	$objPHPExcel->setActiveSheetIndex(0);
	
	// Redirect output to a client’s web browser (Excel5)
	header('Content-Type: application/vnd.ms-excel');
	header('Content-Disposition: attachment;filename="report_ovcca.xls"');
	header('Cache-Control: max-age=0');
	// If you're serving to IE 9, then the following may be needed
	header('Cache-Control: max-age=1');
	// If you're serving to IE over SSL, then the following may be needed
	header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
	header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
	header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
	header ('Pragma: public'); // HTTP/1.0
	$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
	$objWriter->save('php://output');
	exit;
    }
    /**
     * Finds the OvPerson model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return OvPerson the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = OvPerson::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
