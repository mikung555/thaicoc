<?php
use kartik\form\ActiveForm;
use yii\helpers\Html;
use appxq\sdii\helpers\SDHtml;
use common\lib\sdii\components\helpers\SDNoty;

?>

<?php $form = ActiveForm::begin([
	'id'=>'sublist-form',
    ]); ?>

<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title" id="itemModalLabel">เลือกบุคคลเพื่อเพิ่มลงในใบกำกับงาน</h4>
</div>

<div class="modal-body">
    <?php
    if($ovfilter_sub!=0){
	$ovsub = backend\modules\ovcca\models\OvFilterSub::findOne(['sub_id'=>$ovfilter_sub]);
    }
    ?>
    
    <?= maksyutin\duallistbox\Widget::widget([
    'model' => $model,
    'attribute' => 'list_person',
    'title' => 'ใบกำกับงาน',
    'data' => $data,
    'data_id'=> 'id',
    'data_value'=> 'name',
	'lngOptions'=>[
	    'available' => 'Source:ทั้งหมด',
	    'selected' => (isset($ovsub))?'Target:'.$ovsub['sub_name']:'ใบกำกับงานย่อย',
	    'showing' => 'จำนวน',
	    'search_placeholder'=>'ค้นหา'
	]
  ]);?>
    
    
</div>
<div class="modal-body">
    <hr>
    <h4>คู่มือ</h4>
    
    <div class="row">
	<div class="col-md-6">
	    <p><button type="button" class="btn btn-default">
		<span class="glyphicon glyphicon-chevron-right"></span> 
		<span style="margin-left: -10px;" class="glyphicon glyphicon-chevron-right"></span>
	    </button> = เลือกทั้งหมด </p>
	    <p><button type="button" class="btn btn-default">
		<span class="glyphicon glyphicon-chevron-right"></span>
	    </button> = เลือกที่เลือก </p>
	    
	    <p><code>Ctrl+Clik</code> = เลือกที่ละรายการ</p>
	    <p><code>Ctrl+A</code> = เลือกทั้งหมด</p>
	    <p><code>Shift+Clik</code> = เลือกเป็นช่วง</p>
	</div>
	<div class="col-md-6">
	    <p><button type="button" class="btn btn-default">
		<span class="glyphicon glyphicon-chevron-left"></span>
	    </button> = คืนที่เลือก </p>
	    <p><button type="button" class="btn btn-default">
		<span class="glyphicon glyphicon-chevron-left"></span>
		<span style="margin-left: -10px;" class="glyphicon glyphicon-chevron-left"></span>
	    </button> = คืนทั้งหมด </p>
	    
	    
	</div>
    </div>
    
    
    
    
    
</div>
<div class="modal-footer">
    <?= Html::submitButton('บันทึก', ['class' => 'btn btn-primary']) ?>
    
</div>

<?php ActiveForm::end(); ?>

<?php  $this->registerJs("

$('form#sublist-form').on('beforeSubmit', function(e) {
    var \$form = $(this);
    $.post(
	\$form.attr('action'), //serialize Yii2 form
	\$form.serialize()
    ).done(function(result) {
	if(result.status == 'success') {
	    ". SDNoty::show('result.message', 'result.status') ."
	    if(result.action == 'create') {
		$(document).find('#modal-ovlist').modal('hide');
		$.pjax.reload({container:'#ov-person-grid-pjax'});
	    }
	} else {
	    ". SDNoty::show('result.message', 'result.status') ."
	} 
    }).fail(function() {
	". SDNoty::show("'" . SDHtml::getMsgError() . "Server Error'", '"error"') ."
	console.log('server error');
    });
    return false;
});

");?>