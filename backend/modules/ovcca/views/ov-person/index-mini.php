<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\helpers\Url;
use appxq\sdii\widgets\GridView;
use appxq\sdii\widgets\ModalForm;
use appxq\sdii\helpers\SDNoty;
use appxq\sdii\helpers\SDHtml;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\widgets\DatePicker;
use kartik\field\FieldRange;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\ovcca\models\OvPersonSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'จัดการใบกำกับงาน (Ver.2.5)');
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="ov-person-index-mini">
   
    <?php  Pjax::begin(['id'=>'ov-person-grid-pjax', 'timeout' => 10000]);?>
   
    <div class="row" >
	<div class="col-md-12 " style="margin-bottom: 15px;">
	    <div style="border: 1px solid #ddd; padding: 5px 15px;border-radius: 4px;">
	    <div class="box-title"> 
		<div class="box-arrow" style="border-top-color: #1b95e0"></div> 
		<div class="box-inner" style="background-color: #1b95e0"> 1 </div> 
	    </div>
		<div style="display: inline-block;">
		    <span style="font-size: 16px;margin-bottom: 5px;">จากประชากรทั้งหมด <code><?=number_format($total)?></code> ราย ลงทะเบียนใน Isan Cohort (มี PID) แล้ว <code><?=number_format($totalAll)?></code> ราย </span>
			<br>
			ลงทะเบียนเพิ่ม 
		    <?= Html::a('<i class="glyphicon glyphicon-plus"></i> จาก TDC (เดิมมี <code>'.number_format($totalBot).'</code> ราย)', ['/ovcca/ov-person/indexreg'], ['class' => 'btn btn-primary btn-sm', 'style'=>'margin-bottom: 6px;']) ?> 
		    <?=Html::a('<i class="glyphicon glyphicon-plus"></i> จากใบทำบัตร (เดิมมี <code>'.number_format($totalTb).'</code> ราย)', Url::to(['/inputdata/step2',
				    'ezf_id'=>'1437377239070461301',
				    'target'=>'',
				    'comp_id_target'=>'1437725343023632100',
				    ]), ['class' => 'btn btn-success btn-sm', 'style'=>'margin-bottom: 6px;']);?>
		</div>
	    
	    <div class="pull-right">
		<a class="btn btn-danger" href="https://cloudstorage.cascap.in.th/VDO/ControlChart01/ControlChart01.html" target="_blank"><i class="fa fa-youtube-play"></i> VDO สอนการใช้งาน !!</a>
	    </div>
	    </div>
	</div>
    </div>
    <div class="row">
	<div class="col-md-12 " style="margin-bottom: 15px;">
	    <div style="border: 1px solid #ddd; padding: 5px 15px;border-radius: 4px;">
	    <div class="box-title"> 
		<div class="box-arrow" style="border-top-color: #F1C40F"></div> 
		<div class="box-inner" style="background-color: #F1C40F"> 2 </div> 
	    </div>
		
			<?php $form = ActiveForm::begin([
		'id' => 'jump_menu',
		'action' => ['index-mini'],
		'method' => 'get',
		'layout' => 'inline',
		'options' => ['style'=>'display: inline-block;']	    
	    ]); 
    
	    ?>
			<div style="margin-bottom: -10px;">
			    <span style="font-size: 16px;">กลุ่มที่ลงทะเบียนใน Isan Cohort ซึ่งมีจำนวน <code><?=number_format($totalAll)?></code> ราย </span>
			    <br>
		<?php
		$datasub = ArrayHelper::map($dataOvFilterSub, 'sub_id', 'sub_name');
		$datasubstatus = ArrayHelper::map($dataOvFilterSub, 'sub_id', 'urine_status');
		$ovshow = ($ovfilter_sub!=0)?ArrayHelper::getValue($datasubstatus, $ovfilter_sub):1;
		?>	
			    จัดการข้อมูล 
		
		<?=Html::button('<span class="glyphicon glyphicon-plus"></span> สร้างใบกำกับงาน', ['data-url'=>Url::to(['/ovcca/ov-person/ovfilter-sub']), 'class' => 'btn btn-success btn-sm', 'id'=>'addbtn-filter-sub'])?>
		<?= Html::dropDownList('ovfilter_sub', $ovfilter_sub, $datasub, ['class'=>'form-control input-sm', 'prompt'=>'แสดงทั้งหมด ('.  number_format($totalAll).')', 'onChange'=>'$("#jump_menu").submit()'])?>
		<?= Html::a('<span class="glyphicon glyphicon-pencil"></span> จัดการใบกำกับงาน', ['/ovcca/ov-filter-sub/index'], ['class' => 'btn btn-primary btn-sm']) ?>
		    <?php
		if($ovfilter_sub>0){
		    echo Html::button('<span class="glyphicon glyphicon-transfer"></span> จัดการกลุ่มเป้าหมายในใบกำกับงาน', ['data-url'=>Url::to(['ov-person/addlist', 'ovfilter_sub'=>$ovfilter_sub]), 'class' => 'btn btn-sm btn-warning addlistbtn']).' '; 
		    
		    
		}
		echo Html::button('<span class="glyphicon glyphicon-print"></span> พิมพ์รายงาน', ['id'=>'reportOvcca', 'data-url'=>Url::to(['ov-person/report-ovcca', 'fid'=>$ovfilter, 'sid'=>$ovfilter_sub]), 'class' => 'btn  btn-default btn-sm']).' ';
		echo Html::button('<span class="glyphicon glyphicon-tags"></span> พิมพ์สติ๊กเกอร์แบบ 2+6', ['data-url'=>Url::to(['ov-person/report-sticker', 'fid'=>$ovfilter, 'sid'=>$ovfilter_sub]), 'class' => 'btn  btn-default reportSticker btn-sm']).' ';
		echo Html::button('<span class="glyphicon glyphicon-tags"></span> พิมพ์สติ๊กเกอร์แบบ 1+2', ['data-url'=>Url::to(['ov-person/report-sticker', 'sizemini'=>1, 'fid'=>$ovfilter, 'sid'=>$ovfilter_sub]), 'class' => 'btn  btn-default reportSticker btn-sm']).' ';
		echo Html::button('<span class="glyphicon glyphicon-export"></span> export', ['id'=>'reportExel', 'data-url'=>Url::to(['ov-person/report-exel', 'fid'=>$ovfilter, 'sid'=>$ovfilter_sub]), 'class' => 'btn  btn-default btn-sm']).' ';
		?>
		
	    <?php ActiveForm::end(); ?>
		    </div>
		</div>    
	</div>
    </div>
    
<!--    <div class="row">
	<div class="col-md-12 " style="margin-bottom: 15px;">
	    <div class="box-title"> 
		<div class="box-arrow" style="border-top-color: #31b20e"></div> 
		<div class="box-inner" style="background-color: #31b20e"> 3 </div> 
	    </div>
	    
	    
	</div>
    </div>
    <hr>-->

    <ul class="nav nav-tabs">
	<li role="presentation" ><a href="<?=  Url::to(['/ovcca/ov-person/index'])?>">หน้าหลัก</a></li>
	<li role="presentation" class="active"><a href="<?=  Url::to(['/ovcca/ov-person/index-mini'])?>">บันทึกผลตรวจ</a></li>
    </ul>

    <?php  Pjax::end();?>

</div>

<?=  ModalForm::widget([
    'id' => 'modal-ov-person',
    'size'=>'modal-lg',
]);
?>

<?=  ModalForm::widget([
    'id' => 'modal-ovlist',
    'size'=>'modal-lg',
]);
?>

<?php  $this->registerJs("
    
$('#divToScroll').attachDragger();

$('#ov-person-grid-pjax').on('click', '#modal-addbtn-ov-person', function() {
    modalOvPerson($(this).attr('data-url'));
});

$('#ov-person-grid-pjax').on('click', '#modal-delbtn-ov-person', function() {
    selectionOvPersonGrid($(this).attr('data-url'));
});

$('#ov-person-grid-pjax').on('click', '#modal-delallbtn-ov-person', function() {
    selectionOvPersonGrid($(this).attr('data-url'));
});

$('#ov-person-grid-pjax').on('click', '#modal-importbtn-ov-person', function() {
    selectionOvPersonGrid($(this).attr('data-url'));
});

$('#ov-person-grid-pjax').on('click', '.select-on-check-all', function() {
    window.setTimeout(function() {
	var key = $('#ov-person-grid').yiiGridView('getSelectedRows');
	disabledOvPersonBtn(key.length);
    },100);
});

$('#ov-person-grid-pjax').on('click', '.selectionOvPersonIds', function() {
    var key = $('input:checked[class=\"'+$(this).attr('class')+'\"]');
    disabledOvPersonBtn(key.length);
});

$('#ov-person-grid-pjax').on('dblclick', 'tbody tr', function() {
    var id = $(this).attr('data-key');
    //modalOvPerson('".Url::to(['ov-person/update', 'id'=>''])."'+id);
});	

$('#ov-person-grid-pjax').on('click', 'tbody tr td a', function() {
    var url = $(this).attr('href');
    var action = $(this).attr('data-action');

    if(action === 'update' || action === 'view') {
	modalOvPerson(url);
	return false;
    } else if(action === 'delete') {
	yii.confirm('".Yii::t('app', 'Are you sure you want to delete this item?')."', function() {
	    $.post(
		url
	    ).done(function(result) {
		if(result.status == 'success') {
		    ". SDNoty::show('result.message', 'result.status') ."
		    $.pjax.reload({container:'#ov-person-grid-pjax'});
		} else {
		    ". SDNoty::show('result.message', 'result.status') ."
		}
	    }).fail(function() {
		". SDNoty::show("'" . SDHtml::getMsgError() . "Server Error'", '"error"') ."
		console.log('server error');
	    });
	});
	return false;
    } else if(action === 'ov01') {
	return false;
    } else if(action === 'ov01-status') {
	$.post(
	    url
	).done(function(result) {
	    if(result.status == 'success') {
		". SDNoty::show('result.message', 'result.status') ."
		$.pjax.reload({container:'#ov-person-grid-pjax'});
	    } else {
		". SDNoty::show('result.message', 'result.status') ."
	    }
	}).fail(function() {
	    ". SDNoty::show("'" . SDHtml::getMsgError() . "Server Error'", '"error"') ."
	    console.log('server error');
	});
	return false;
    }
   
});

//$('#ov-person-grid-pjax').on('click', '#select_all', function() {
//    if($(this).prop('checked')){
//	$('#modal-importallbtn-ov-person').attr('disabled', false);
//	$('#select_show').html($(this).attr('data-count'));
//	$('.cart-num').html($(this).attr('data-count'));
//    } else {
//	$('#modal-importallbtn-ov-person').attr('disabled', true);
//	$('#select_show').html(0);
//	$('.cart-num').html(0);
//    }
//});

$('#ov-person-grid-pjax').on('click', '#modal-importallbtn-ov-person', function() {
    var url = $(this).attr('data-url');
    modalOvPerson(url);
});

function disabledOvPersonBtn(num) {
    if(num>0) {
	$('#modal-stickerbtn-ov-person').attr('disabled', false);
	$('#modal-addlistbtn-ov-person').attr('disabled', false);
    } else {
	$('#modal-stickerbtn-ov-person').attr('disabled', true);
	$('#modal-addlistbtn-ov-person').attr('disabled', true);
    }
    $('.cart-num').html(num);
}

$('#reportOvcca, #reportOvcca1').on('click', function() {
    var url = $(this).attr('data-url');
    selectionOvPersonReport(url);
});

$('.reportSticker').on('click', function() {
    var url = $(this).attr('data-url');
    selectionOvPersonReport(url);
});

$('#reportDetail, #reportDetail1').on('click', function() {
    var url = $(this).attr('data-url');
    selectionOvPersonReport(url);
});

$('#reportExel, #reportExel1').on('click', function() {
    var url = $(this).attr('data-url');
    selectionOvPersonExel(url);
});

function selectionOvPersonExel(url) {
    $('#modal-ov-person .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
    $('#modal-ov-person').modal('show');
    $.ajax({
	method: 'POST',
	url: url,
	data: $('.selectionOvPersonIds:checked[name=\"selection[]\"]').serialize(),
	dataType: 'JSON',
	success: function(result, textStatus) {
	    if(result.status == 'success') {
		". SDNoty::show('result.message', 'result.status') ."
		$('#modal-ov-person .modal-content').html(result.html);
		
		$('#modal-ov-person').modal('hide');
	    } else {
		". SDNoty::show('result.message', 'result.status') ."
	    }
	}
    });
}

function selectionOvPersonReport(url) {
    $('#modal-ov-person .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
    $('#modal-ov-person').modal('show');
    $.ajax({
	method: 'POST',
	url: url,
	data: $('.selectionOvPersonIds:checked[name=\"selection[]\"]').serialize(),
	dataType: 'JSON',
	success: function(result, textStatus) {
	    if(result.status == 'success') {
		". SDNoty::show('result.message', 'result.status') ."
		    $('#modal-ov-person .modal-content').html(result.html);
	    } else {
		". SDNoty::show('result.message', 'result.status') ."
	    }
	}
    });
}

function selectionOvPersonGrid(url) {
    yii.confirm('".Yii::t('app', 'Are you sure you want to delete these items?')."', function() {
	$.ajax({
	    method: 'POST',
	    url: url,
	    data: $('.selectionOvPersonIds:checked[name=\"selection[]\"]').serialize(),
	    dataType: 'JSON',
	    success: function(result, textStatus) {
		if(result.status == 'success') {
		    ". SDNoty::show('result.message', 'result.status') ."
		    $.pjax.reload({container:'#ov-person-grid-pjax'});
		} else {
		    ". SDNoty::show('result.message', 'result.status') ."
		}
	    }
	});
    });
}

function statusOvPersonGrid(url) {
    yii.confirm('".Yii::t('app', 'Are you sure you want to update status all items?')."', function() {
	$.ajax({
	    method: 'POST',
	    url: url,
	    dataType: 'JSON',
	    success: function(result, textStatus) {
		if(result.status == 'success') {
		    ". SDNoty::show('result.message', 'result.status') ."
		    $.pjax.reload({container:'#ov-person-grid-pjax'});
		} else {
		    ". SDNoty::show('result.message', 'result.status') ."
		}
	    }
	});
    });
}

$('#ov-person-grid-pjax').on('click', '#modal-statust-ov-person', function() {
    statusOvPersonGrid($(this).attr('data-url'));
});

$('#ov-person-grid-pjax').on('click', '#modal-statusf-ov-person', function() {
    statusOvPersonGrid($(this).attr('data-url'));
});

$('#ov-person-grid-pjax').on('click', '#addbtn-filter', function() {
    var url = $(this).attr('data-url');
    modalOvPerson(url);
});

$('#ov-person-grid-pjax').on('click', '#addbtn-filter-sub', function() {
    var url = $(this).attr('data-url');
    modalOvPerson(url);
});

$('#ov-person-grid-pjax').on('click', '.addlistbtn', function() {
    modalOvList($(this).attr('data-url'));
});

function modalOvList(url) {
    $('#modal-ovlist .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
    $('#modal-ovlist').modal('show');
    $.ajax({
	method: 'POST',
	url: url,
	data: $('.selectionOvPersonIds:checked[name=\"selection[]\"]').serialize(),
	dataType: 'JSON',
	success: function(result, textStatus) {
	    $('#modal-ovlist .modal-content').html(result.html);
	}
    });
}

function modalOvPerson(url) {
    $('#modal-ov-person .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
    $('#modal-ov-person').modal('show')
    .find('.modal-content')
    .load(url);
}

$('#ov-person-grid-pjax').on('click', '.btn-action-ezf', function() {
    var target = $(this).attr('data-target');
    var url = $(this).attr('href');
    var ezf_id = $(this).attr('data-ezf_id');
    var comp_id = $(this).attr('data-comp_id');
    
    $.post( url, {ezf_id: ezf_id, target: $.trim(target), comp_id_target: comp_id, rurl: '". base64_encode(Yii::$app->request->url)."'}, function(result){
	$(location).attr('href',result);
    });
    
});


");?>