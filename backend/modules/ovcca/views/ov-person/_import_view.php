<?php 
use yii\helpers\Html;
?>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title" id="itemModalLabel">ลงทะเบียนใน Isan Cohort</h4>
</div>

<div class="modal-body">
    <table class="table">
	<tr>
	    <td><h2>เลือกเพื่อลงทะเบียนในครั้งนี้</h2></td>
	    <td><h2><?=$sum-$sumF?> ราย</h2></td>
	</tr>
	<tr>
	    <td><h2>สามารถลงทะเบียนได้ในครั้งนี้</h2></td>
	    <td><h2><?=$sumT+$sumN?> ราย</h2></td>
	</tr>
	<tr>
	    <td><h2>ไม่สามารถลงทะเบียนไม่ได้ในครั้งนี้</h2></td>
	    <td><h2><?=$sumF?> ราย</h2></td>
	</tr>
	
    </table>
    <?php if($sumF>0): ?>
    <code>*หมายเหตุ : ลงทะเบียนไม่ได้ อาจเนื่องมาจาก เลขบัตรไม่ถูกต้อง, ไม่ได้ถอดรหัสข้อมูล, ข้อมูลไม่ครบถ้วน</code>
    <?php endif;?>
</div>
<div class="modal-footer">
    <?=Html::button('ลงทะเบียนเพื่อสร้าง PID', ['data-url'=>  yii\helpers\Url::to(['ov-person/import-all', 'select'=>$select, 'selectItem'=>$selectItem]), 'class' => 'btn btn-primary', 'id'=>'importallbtn'])?>
    <?=Html::button('อัพเดทข้อมูลใบทำบัตร', ['data-url'=>  yii\helpers\Url::to(['ov-person/import-update', 'select'=>$select, 'selectItem'=>$selectItem]), 'class' => 'btn btn-primary', 'id'=>'importupdatebtn', 'data-toggle'=>'tooltip', 'title'=>'ใช้เมื่อมีการถอดรหัสข้อมูลภายหลังจากได้ลงทะเบียนไปแล้ว'])?>
    <button type="button" class="btn btn-default" data-dismiss="modal">ปิด</button>
</div>

<?php  $this->registerJs("
    
$('#importallbtn').on('click', function() {
    var url = $(this).attr('data-url');
    modalOvPerson(url);
});

$('#importupdatebtn').on('click', function() {
    var url = $(this).attr('data-url');
    modalOvPerson(url);
});

function modalOvPerson(url) {
    $('#modal-ov-person .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
    $('#modal-ov-person').modal('show')
    .find('.modal-content')
    .load(url);
}

");?>