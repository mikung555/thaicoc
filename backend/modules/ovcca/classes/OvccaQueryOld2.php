<?php
namespace backend\modules\ovcca\classes;

use Yii;
use yii\data\SqlDataProvider;
use backend\modules\ckdnet\classes\CkdnetFunc;
/**
 * OvccaQuery class file UTF-8
 * @author SDII <iencoded@gmail.com>
 * @copyright Copyright &copy; 2015 AppXQ
 * @license http://www.appxq.com/license/
 * @version 1.0.0 Date: 9 ก.พ. 2559 12:38:14
 * @link http://www.appxq.com/
 * @example 
 */
class OvccaQueryOld2 {
    public static function getPersonAllDecodeIn($key, $convert, $find, $hospcode='05147') {
	$sqlsp = "DROP PROCEDURE IF EXISTS `get_person_decode_in`;";
	CkdnetFunc::execute($sqlsp);
	$sqlsp = "	
		CREATE DEFINER=`webservice`@`%` PROCEDURE `get_person_decode_in`(IN sitecode varchar(5), IN pkey varchar(100), IN pconvert tinyint(1), IN find_link text)
		BEGIN

		SET @s = CONCAT('DROP TABLE IF EXISTS f_person_', sitecode);
		PREPARE stmt FROM @s;
		EXECUTE stmt;
		DEALLOCATE PREPARE stmt;

		SET @s = CONCAT('CREATE TEMPORARY TABLE f_person_', sitecode, ' ( INDEX(HOSPCODE, PID) ) AS (
		SELECT *
		FROM f_person
		WHERE HOSPCODE=\'', sitecode, '\' AND f_person.ptlink IN (', find_link, ')
		)');
		PREPARE stmt FROM @s;
		EXECUTE stmt;
		DEALLOCATE PREPARE stmt;

		SET @s = CONCAT('DROP TABLE IF EXISTS f_address_', sitecode);
		PREPARE stmt FROM @s;
		EXECUTE stmt;
		DEALLOCATE PREPARE stmt;

		SET @s = CONCAT('CREATE TEMPORARY TABLE f_address_', sitecode, ' ( INDEX(HOSPCODE, PID) ) AS (
		SELECT *
		FROM f_address
		WHERE HOSPCODE=\'', sitecode,'\')');
		PREPARE stmt FROM @s;
		EXECUTE stmt;
		DEALLOCATE PREPARE stmt;

		SET @s = CONCAT('DROP TABLE IF EXISTS f_death_', sitecode);
		PREPARE stmt FROM @s;
		EXECUTE stmt;
		DEALLOCATE PREPARE stmt;

		SET @s = CONCAT('CREATE TEMPORARY TABLE f_death_', sitecode, ' ( INDEX(HOSPCODE, PID) ) AS (
		SELECT HOSPCODE, PID, DDeath
		FROM f_death
		WHERE HOSPCODE=\'', sitecode, '\')');
		PREPARE stmt FROM @s;
		EXECUTE stmt;
		DEALLOCATE PREPARE stmt;


		SET @ssql = CONCAT('decode(unhex(a.HouseNo),sha2(', pkey, ',256)) AS address,
				    decode(unhex(a.House_id),sha2(', pkey, ',256)) AS house_id,
				    decode(unhex(p.CID),sha2(', pkey, ',256)) AS cid,
				    decode(unhex(p.HN),sha2(', pkey, ',256)) AS hn,
				    decode(unhex(p.Pname),sha2(', pkey, ',256)) AS pname,
				    decode(unhex(p.`Name`),sha2(', pkey, ',256)) AS fname,
				    decode(unhex(p.Lname),sha2(', pkey, ',256)) AS lname,');

		IF pconvert=1 THEN
			SET @ssql = CONCAT('convert(decode(unhex(a.HouseNo),sha2(', pkey, ',256)) using tis620) AS address,
				    convert(decode(unhex(a.House_id),sha2(', pkey, ',256)) using tis620) AS house_id,
				    convert(decode(unhex(p.CID),sha2(', pkey, ',256)) using tis620) AS cid,
				    convert(decode(unhex(p.HN),sha2(', pkey, ',256)) using tis620) AS hn,
				    convert(decode(unhex(p.Pname),sha2(', pkey, ',256)) using tis620) AS pname,
				    convert(decode(unhex(p.`Name`),sha2(', pkey, ',256)) using tis620) AS fname,
				    convert(decode(unhex(p.Lname),sha2(', pkey, ',256)) using tis620) AS lname,');
		END IF;
		
		IF pkey='' THEN
		    SET @ssql = CONCAT('a.HouseNo AS address,
				a.House_id AS house_id,
				p.CID AS cid,
				p.HN AS hn,
				p.Pname AS pname,
				p.`Name` AS fname,
				p.Lname AS lname,');
		END IF;
		
		SET @s = CONCAT('SELECT a.Changwat AS province, 
				    a.Ampur AS amphur, 
				    a.Tambon AS tambon, 
				    p.HOSPCODE AS hospcode, 
				    p.PID AS person_id, 
				    ', @ssql, '
				    p.sex AS sex, 
				    p.Nation AS nationality, 
				    p.Education AS education, 
				    p.TypeArea AS type_area, 
				    p.Religion AS religion, 
				    p.Birth AS birthdate, 
				    floor(DATEDIFF(CURRENT_DATE, STR_TO_DATE(p.Birth, \'%Y-%m-%d\'))/365) AS age,
				    a.Village AS village_id, 
				    CONCAT(a.Changwat,a.Ampur,a.Tambon,a.Village) AS village_code,
				    a.villaname AS village_name, 
				    p.sitecode AS sitecode,
				    NULL AS khet,
				    NULL AS hospname,
				    NULL AS pttype, 
				    NULL AS pttype_begin_date, 
				    NULL AS pttype_expire_date, 
				    NULL AS pttype_hospmain, 
				    NULL AS pttype_hospsub, 
				    p.Mstatus AS marrystatus, 
				    IF(ISNULL(d.DDeath),\'N\',\'Y\') AS death, 
				    p.ptlink AS cidlink,
				    d.DDeath AS death_date
			    FROM f_person_', sitecode, ' p INNER JOIN f_address_', sitecode, ' a ON p.HOSPCODE = a.HOSPCODE AND p.PID = a.PID
				    LEFT JOIN f_death_', sitecode, ' d ON p.HOSPCODE = d.HOSPCODE AND p.PID = d.PID
			    WHERE p.hospcode=\'', sitecode, '\'');
		PREPARE stmt FROM @s;
		EXECUTE stmt;
		DEALLOCATE PREPARE stmt;

		END;
		
	    ";
	CkdnetFunc::execute($sqlsp);
	
	$sql = "CALL get_person_decode_in(:hospcode, :key, :convert, :find)";
	
	return CkdnetFunc::queryAll($sql, [':key'=>$key, ':hospcode'=>$hospcode, ':find'=>$find]);
	
    }
    
    public static function getPersonAllDecode($min, $max, $type, $village, $key, $hospcode='05147') {
	$sqlsp = "DROP PROCEDURE IF EXISTS `get_person_decode`;";
	CkdnetFunc::execute($sqlsp);
	$sqlsp = "	
		CREATE DEFINER=`webservice`@`%` PROCEDURE `get_person_decode`(IN sitecode varchar(5), IN min int(3), IN max int(3), IN type_area varchar(50), IN pkey varchar(100), IN pconvert tinyint(1), IN vcode varchar(10))
		BEGIN

		SET @s = CONCAT('DROP TABLE IF EXISTS f_person_', sitecode);
		PREPARE stmt FROM @s;
		EXECUTE stmt;
		DEALLOCATE PREPARE stmt;

		SET @s = CONCAT('CREATE TEMPORARY TABLE f_person_', sitecode, ' ( INDEX(HOSPCODE, PID) ) AS (
		SELECT *
		FROM f_person
		WHERE HOSPCODE=\'', sitecode, '\' AND floor(DATEDIFF(CURRENT_DATE, STR_TO_DATE(Birth, \'%Y-%m-%d\'))/365) BETWEEN ', min, ' AND ', max, ' AND TypeArea IN(', type_area, ')
		)');
		PREPARE stmt FROM @s;
		EXECUTE stmt;
		DEALLOCATE PREPARE stmt;

		SET @s = CONCAT('DROP TABLE IF EXISTS f_address_', sitecode);
		PREPARE stmt FROM @s;
		EXECUTE stmt;
		DEALLOCATE PREPARE stmt;

		SET @s = CONCAT('CREATE TEMPORARY TABLE f_address_', sitecode, ' ( INDEX(HOSPCODE, PID) ) AS (
		SELECT *
		FROM f_address
		WHERE HOSPCODE=\'', sitecode,'\')');
		PREPARE stmt FROM @s;
		EXECUTE stmt;
		DEALLOCATE PREPARE stmt;

		SET @s = CONCAT('DROP TABLE IF EXISTS f_death_', sitecode);
		PREPARE stmt FROM @s;
		EXECUTE stmt;
		DEALLOCATE PREPARE stmt;

		SET @s = CONCAT('CREATE TEMPORARY TABLE f_death_', sitecode, ' ( INDEX(HOSPCODE, PID) ) AS (
		SELECT HOSPCODE, PID, DDeath
		FROM f_death
		WHERE HOSPCODE=\'', sitecode, '\')');
		PREPARE stmt FROM @s;
		EXECUTE stmt;
		DEALLOCATE PREPARE stmt;


		SET @ssql = CONCAT('decode(unhex(a.HouseNo),sha2(', pkey, ',256)) AS address,
				    decode(unhex(a.House_id),sha2(', pkey, ',256)) AS house_id,
				    decode(unhex(p.CID),sha2(', pkey, ',256)) AS cid,
				    decode(unhex(p.HN),sha2(', pkey, ',256)) AS hn,
				    decode(unhex(p.Pname),sha2(', pkey, ',256)) AS pname,
				    decode(unhex(p.`Name`),sha2(', pkey, ',256)) AS fname,
				    decode(unhex(p.Lname),sha2(', pkey, ',256)) AS lname,');

		IF pconvert=1 THEN
			SET @ssql = CONCAT('convert(decode(unhex(a.HouseNo),sha2(', pkey, ',256)) using tis620) AS address,
				    convert(decode(unhex(a.House_id),sha2(', pkey, ',256)) using tis620) AS house_id,
				    convert(decode(unhex(p.CID),sha2(', pkey, ',256)) using tis620) AS cid,
				    convert(decode(unhex(p.HN),sha2(', pkey, ',256)) using tis620) AS hn,
				    convert(decode(unhex(p.Pname),sha2(', pkey, ',256)) using tis620) AS pname,
				    convert(decode(unhex(p.`Name`),sha2(', pkey, ',256)) using tis620) AS fname,
				    convert(decode(unhex(p.Lname),sha2(', pkey, ',256)) using tis620) AS lname,');
		END IF;

		SET @s = CONCAT('SELECT a.Changwat AS province, 
				    a.Ampur AS amphur, 
				    a.Tambon AS tambon, 
				    p.HOSPCODE AS hospcode, 
				    p.PID AS person_id, 
				    ', @ssql, '
				    p.sex AS sex, 
				    p.Nation AS nationality, 
				    p.Education AS education, 
				    p.TypeArea AS type_area, 
				    p.Religion AS religion, 
				    p.Birth AS birthdate, 
				    floor(DATEDIFF(CURRENT_DATE, STR_TO_DATE(p.Birth, \'%Y-%m-%d\'))/365) AS age,
				    a.Village AS village_id, 
				    CONCAT(a.Changwat,a.Ampur,a.Tambon,a.Village) AS village_code,
				    a.villaname AS village_name, 
				    p.sitecode AS sitecode,
				    NULL AS khet,
				    NULL AS hospname,
				    NULL AS pttype, 
				    NULL AS pttype_begin_date, 
				    NULL AS pttype_expire_date, 
				    NULL AS pttype_hospmain, 
				    NULL AS pttype_hospsub, 
				    p.Mstatus AS marrystatus, 
				    IF(ISNULL(d.DDeath),\'N\',\'Y\') AS death, 
				    p.ptlink AS cidlink,
				    d.DDeath AS death_date
			    FROM f_person_', sitecode, ' p INNER JOIN f_address_', sitecode, ' a ON p.HOSPCODE = a.HOSPCODE AND p.PID = a.PID
					LEFT JOIN f_death_', sitecode, ' d ON p.HOSPCODE = d.HOSPCODE AND p.PID = d.PID
				WHERE p.hospcode=\'', sitecode, '\' AND CONCAT(a.Changwat,a.Ampur,a.Tambon,a.Village) = \'', vcode, '\' AND d.PID IS NULL');
		PREPARE stmt FROM @s;
		EXECUTE stmt;
		DEALLOCATE PREPARE stmt;

		END;
		
	    ";
	CkdnetFunc::execute($sqlsp);
	
	$convert = 0;
	if (isset(Yii::$app->session['convert']) && Yii::$app->session['convert']==1) {
	    $convert = 1;
	}
	
	$sql = "CALL get_person_decode(:hospcode, :min, :max, :type_area, :key, :convert, :village_code)";
	
	return CkdnetFunc::queryAll($sql, [':type_area'=>$type, ':key'=>$key, ':convert'=>$convert, ':min'=>$min, ':max'=>$max, ':village_code'=>$village, ':hospcode'=>$hospcode]);
	
	
    }
    
    public static function getPersonAll($min, $max, $type, $village, $hospcode='05147') {
	$sqlsp = "DROP PROCEDURE IF EXISTS `get_person`;";
	CkdnetFunc::execute($sqlsp);
	$sqlsp = "	
		CREATE DEFINER=`webservice`@`%` PROCEDURE `get_person`(IN sitecode varchar(5), IN min int(3), IN max int(3), IN type_area varchar(50), IN vcode varchar(10))
		BEGIN

		SET @s = CONCAT('DROP TABLE IF EXISTS f_person_', sitecode);
		PREPARE stmt FROM @s;
		EXECUTE stmt;
		DEALLOCATE PREPARE stmt;

		SET @s = CONCAT('CREATE TEMPORARY TABLE f_person_', sitecode, ' ( INDEX(HOSPCODE, PID) ) AS (
		SELECT *
		FROM f_person
		WHERE HOSPCODE=\'', sitecode, '\' AND floor(DATEDIFF(CURRENT_DATE, STR_TO_DATE(Birth, \'%Y-%m-%d\'))/365) BETWEEN ', min, ' AND ', max, ' AND TypeArea IN(', type_area, ')
		)');
		PREPARE stmt FROM @s;
		EXECUTE stmt;
		DEALLOCATE PREPARE stmt;

		SET @s = CONCAT('DROP TABLE IF EXISTS f_address_', sitecode);
		PREPARE stmt FROM @s;
		EXECUTE stmt;
		DEALLOCATE PREPARE stmt;

		SET @s = CONCAT('CREATE TEMPORARY TABLE f_address_', sitecode, ' ( INDEX(HOSPCODE, PID) ) AS (
		SELECT *
		FROM f_address
		WHERE HOSPCODE=\'', sitecode,'\')');
		PREPARE stmt FROM @s;
		EXECUTE stmt;
		DEALLOCATE PREPARE stmt;

		SET @s = CONCAT('DROP TABLE IF EXISTS f_death_', sitecode);
		PREPARE stmt FROM @s;
		EXECUTE stmt;
		DEALLOCATE PREPARE stmt;

		SET @s = CONCAT('CREATE TEMPORARY TABLE f_death_', sitecode, ' ( INDEX(HOSPCODE, PID) ) AS (
		SELECT HOSPCODE, PID, DDeath
		FROM f_death
		WHERE HOSPCODE=\'', sitecode, '\')');
		PREPARE stmt FROM @s;
		EXECUTE stmt;
		DEALLOCATE PREPARE stmt;

		SET @s = CONCAT('SELECT a.Changwat AS province, 
				    a.Ampur AS amphur, 
				    a.Tambon AS tambon, 
				    p.HOSPCODE AS hospcode, 
				    p.PID AS person_id, 
				    a.HouseNo AS address,
				    a.House_id AS house_id,
				    p.CID AS cid,
				    p.HN AS hn,
				    p.Pname AS pname,
				    p.`Name` AS fname,
				    p.Lname AS lname,
				    p.sex AS sex, 
				    p.Nation AS nationality, 
				    p.Education AS education, 
				    p.TypeArea AS type_area, 
				    p.Religion AS religion, 
				    p.Birth AS birthdate, 
				    floor(DATEDIFF(CURRENT_DATE, STR_TO_DATE(p.Birth, \'%Y-%m-%d\'))/365) AS age,
				    a.Village AS village_id, 
				    CONCAT(a.Changwat,a.Ampur,a.Tambon,a.Village) AS village_code,
				    a.villaname AS village_name, 
				    p.sitecode AS sitecode,
				    NULL AS khet,
				    NULL AS hospname,
				    NULL AS pttype, 
				    NULL AS pttype_begin_date, 
				    NULL AS pttype_expire_date, 
				    NULL AS pttype_hospmain, 
				    NULL AS pttype_hospsub, 
				    p.Mstatus AS marrystatus, 
				    IF(ISNULL(d.DDeath),\'N\',\'Y\') AS death, 
				    p.ptlink AS cidlink,
				    d.DDeath AS death_date
			    FROM f_person_', sitecode, ' p INNER JOIN f_address_', sitecode, ' a ON p.HOSPCODE = a.HOSPCODE AND p.PID = a.PID
					LEFT JOIN f_death_', sitecode, ' d ON p.HOSPCODE = d.HOSPCODE AND p.PID = d.PID
				WHERE p.hospcode=\'', sitecode, '\' AND CONCAT(a.Changwat,a.Ampur,a.Tambon,a.Village) = \'', vcode, '\' AND d.PID IS NULL');
		PREPARE stmt FROM @s;
		EXECUTE stmt;
		DEALLOCATE PREPARE stmt;

		END;
		
	    ";
	CkdnetFunc::execute($sqlsp);
	
	$sql = "CALL get_person(:hospcode, :min, :max, :type_area, :village_code)";
	
	return CkdnetFunc::queryAll($sql, [':type_area'=>$type, ':min'=>$min, ':max'=>$max, ':village_code'=>$village, ':hospcode'=>$hospcode]);
	
    }
    
    public static function getVillagelist($min, $max, $type, $hospcode='05147') {
	
	$sqlsp = "DROP PROCEDURE IF EXISTS `village_list`;";
	CkdnetFunc::execute($sqlsp);
	$sqlsp = "
		CREATE DEFINER=`webservice`@`%` PROCEDURE `village_list`(IN sitecode varchar(5), IN min int(3), IN max int(3), IN type_area varchar(50))
		BEGIN

		SET @s = CONCAT('DROP TABLE IF EXISTS f_person_', sitecode);
		PREPARE stmt FROM @s;
		EXECUTE stmt;
		DEALLOCATE PREPARE stmt;

		SET @s = CONCAT('CREATE TEMPORARY TABLE f_person_', sitecode, ' ( INDEX(HOSPCODE, PID) ) AS (
		SELECT *
		FROM f_person
		WHERE HOSPCODE=\'', sitecode, '\' AND floor(DATEDIFF(CURRENT_DATE, STR_TO_DATE(Birth, \'%Y-%m-%d\'))/365) BETWEEN ', min, ' AND ', max, ' AND TypeArea IN(', type_area, ')
		)');
		PREPARE stmt FROM @s;
		EXECUTE stmt;
		DEALLOCATE PREPARE stmt;

		SET @s = CONCAT('DROP TABLE IF EXISTS f_address_', sitecode);
		PREPARE stmt FROM @s;
		EXECUTE stmt;
		DEALLOCATE PREPARE stmt;

		SET @s = CONCAT('CREATE TEMPORARY TABLE f_address_', sitecode, ' ( INDEX(HOSPCODE, PID) ) AS (
		SELECT *
		FROM f_address
		WHERE HOSPCODE=\'', sitecode,'\')');
		PREPARE stmt FROM @s;
		EXECUTE stmt;
		DEALLOCATE PREPARE stmt;

		SET @s = CONCAT('DROP TABLE IF EXISTS f_death_', sitecode);
		PREPARE stmt FROM @s;
		EXECUTE stmt;
		DEALLOCATE PREPARE stmt;

		SET @s = CONCAT('CREATE TEMPORARY TABLE f_death_', sitecode, ' ( INDEX(HOSPCODE, PID) ) AS (
		SELECT HOSPCODE, PID, DDeath
		FROM f_death
		WHERE HOSPCODE=\'', sitecode, '\')');
		PREPARE stmt FROM @s;
		EXECUTE stmt;
		DEALLOCATE PREPARE stmt;

		SET @s = CONCAT('SELECT p.HOSPCODE AS hospcode, 
		p.TypeArea AS type_area, 
		p.Birth AS birthdate, 
		CONCAT(a.Changwat,a.Ampur,a.Tambon,a.Village) AS village_code,
		a.villaname AS village_name, 
		CONCAT(a.Changwat,a.Ampur,a.Tambon,a.Village, \' \', a.villaname, \' (\', count(*), \')\') AS text,
		COUNT(*) as num
		FROM f_person_', sitecode, ' p INNER JOIN f_address_', sitecode, ' a ON p.HOSPCODE = a.HOSPCODE AND p.PID = a.PID
		LEFT JOIN f_death_', sitecode, ' d ON p.HOSPCODE = d.HOSPCODE AND p.PID = d.PID
		WHERE p.hospcode=\'', sitecode, '\' AND d.pid IS NULL
		GROUP BY a.Changwat,a.Ampur,a.Tambon,a.Village');
		PREPARE stmt FROM @s;
		EXECUTE stmt;
		DEALLOCATE PREPARE stmt;

		END;
		
	    ";
	CkdnetFunc::execute($sqlsp);
	
	$sql = "CALL village_list(:hospcode, :min, :max, :type_area)";
	
	return CkdnetFunc::queryAll($sql, [':type_area'=>$type, ':min'=>$min, ':max'=>$max, ':hospcode'=>$hospcode]);
    }
    
    public static function getSumPerson($sitecode) {
	$sql = "SELECT count(*) AS num FROM f_person WHERE HOSPCODE=:hospcode";
	
	return CkdnetFunc::queryScalar($sql, [':hospcode'=>$sitecode]);
    }
    
    public static function getPersonOneByCid($sitecode, $cid, $key, $convert) {
	$sqlsp = "DROP PROCEDURE IF EXISTS `get_person_decode_one`;";
	CkdnetFunc::execute($sqlsp);
	$sqlsp = "	
		CREATE DEFINER=`webservice`@`%` PROCEDURE `get_person_decode_one`(IN sitecode varchar(5), IN pkey varchar(100), IN pconvert tinyint(1), IN pcid varchar(100))
		BEGIN

		SET @s = CONCAT('DROP TABLE IF EXISTS f_person_', sitecode);
		PREPARE stmt FROM @s;
		EXECUTE stmt;
		DEALLOCATE PREPARE stmt;

		SET @s = CONCAT('CREATE TEMPORARY TABLE f_person_', sitecode, ' ( INDEX(HOSPCODE, PID) ) AS (
		SELECT *
		FROM f_person
		WHERE HOSPCODE=\'', sitecode, '\' AND ptlink like md5(\'', pcid, '\')
		)');
		PREPARE stmt FROM @s;
		EXECUTE stmt;
		DEALLOCATE PREPARE stmt;

		SET @s = CONCAT('DROP TABLE IF EXISTS f_address_', sitecode);
		PREPARE stmt FROM @s;
		EXECUTE stmt;
		DEALLOCATE PREPARE stmt;

		SET @s = CONCAT('CREATE TEMPORARY TABLE f_address_', sitecode, ' ( INDEX(HOSPCODE, PID) ) AS (
		SELECT *
		FROM f_address
		WHERE HOSPCODE=\'', sitecode,'\')');
		PREPARE stmt FROM @s;
		EXECUTE stmt;
		DEALLOCATE PREPARE stmt;

		SET @s = CONCAT('DROP TABLE IF EXISTS f_death_', sitecode);
		PREPARE stmt FROM @s;
		EXECUTE stmt;
		DEALLOCATE PREPARE stmt;

		SET @s = CONCAT('CREATE TEMPORARY TABLE f_death_', sitecode, ' ( INDEX(HOSPCODE, PID) ) AS (
		SELECT HOSPCODE, PID, DDeath
		FROM f_death
		WHERE HOSPCODE=\'', sitecode, '\')');
		PREPARE stmt FROM @s;
		EXECUTE stmt;
		DEALLOCATE PREPARE stmt;


		SET @ssql = CONCAT('decode(unhex(a.HouseNo),sha2(', pkey, ',256)) AS address,
				    decode(unhex(a.House_id),sha2(', pkey, ',256)) AS house_id,
				    decode(unhex(p.CID),sha2(', pkey, ',256)) AS cid,
				    decode(unhex(p.HN),sha2(', pkey, ',256)) AS hn,
				    decode(unhex(p.Pname),sha2(', pkey, ',256)) AS pname,
				    decode(unhex(p.`Name`),sha2(', pkey, ',256)) AS fname,
				    decode(unhex(p.Lname),sha2(', pkey, ',256)) AS lname,');

		IF pconvert=1 THEN
			SET @ssql = CONCAT('convert(decode(unhex(a.HouseNo),sha2(', pkey, ',256)) using tis620) AS address,
				    convert(decode(unhex(a.House_id),sha2(', pkey, ',256)) using tis620) AS house_id,
				    convert(decode(unhex(p.CID),sha2(', pkey, ',256)) using tis620) AS cid,
				    convert(decode(unhex(p.HN),sha2(', pkey, ',256)) using tis620) AS hn,
				    convert(decode(unhex(p.Pname),sha2(', pkey, ',256)) using tis620) AS pname,
				    convert(decode(unhex(p.`Name`),sha2(', pkey, ',256)) using tis620) AS fname,
				    convert(decode(unhex(p.Lname),sha2(', pkey, ',256)) using tis620) AS lname,');
		END IF;

		SET @s = CONCAT('SELECT a.Changwat AS province, 
				    a.Ampur AS amphur, 
				    a.Tambon AS tambon, 
				    p.HOSPCODE AS hospcode, 
				    p.PID AS person_id, 
				    ', @ssql, '
				    p.sex AS sex, 
				    p.Nation AS nationality, 
				    p.Education AS education, 
				    p.TypeArea AS type_area, 
				    p.Religion AS religion, 
				    p.Birth AS birthdate, 
				    floor(DATEDIFF(CURRENT_DATE, STR_TO_DATE(p.Birth, \'%Y-%m-%d\'))/365) AS age,
				    a.Village AS village_id, 
				    CONCAT(a.Changwat,a.Ampur,a.Tambon,a.Village) AS village_code,
				    a.villaname AS village_name, 
				    p.sitecode AS sitecode,
				    NULL AS khet,
				    NULL AS hospname,
				    NULL AS pttype, 
				    NULL AS pttype_begin_date, 
				    NULL AS pttype_expire_date, 
				    NULL AS pttype_hospmain, 
				    NULL AS pttype_hospsub, 
				    p.Mstatus AS marrystatus, 
				    IF(ISNULL(d.DDeath),\'N\',\'Y\') AS death, 
				    p.ptlink AS cidlink,
				    d.DDeath AS death_date
			    FROM f_person_', sitecode, ' p INNER JOIN f_address_', sitecode, ' a ON p.HOSPCODE = a.HOSPCODE AND p.PID = a.PID
					LEFT JOIN f_death_', sitecode, ' d ON p.HOSPCODE = d.HOSPCODE AND p.PID = d.PID
				WHERE p.hospcode=\'', sitecode, '\' AND d.PID IS NULL');
		PREPARE stmt FROM @s;
		EXECUTE stmt;
		DEALLOCATE PREPARE stmt;

		END;
		
	    ";
	CkdnetFunc::execute($sqlsp);
	
	$sql = "CALL get_person_decode_one(:hospcode, :key, :convert, :pcid)";
	
	return CkdnetFunc::queryOne($sql, [':key'=>$key, ':convert'=>$convert, ':pcid'=>$cid, ':hospcode'=>$sitecode]);
    }
    
    // End db bot
    
    public static function getPersonAll2($min, $max, $village, $hospcode='05147') {
	$sql = "SELECT tb_data_1.*
	    FROM tb_data_1
	    WHERE floor(DATEDIFF(CURRENT_DATE, STR_TO_DATE(v2, '%Y-%m-%d'))/365) BETWEEN :min AND :max AND hsitecode = :hospcode AND add1n6code = :village_code AND add1n5 REGEXP '[0-9]+'
	    ";
	
	return Yii::$app->db->createCommand($sql, [':min'=>$min, ':max'=>$max, ':village_code'=>$village, ':hospcode'=>$hospcode])->queryAll();
    }
    
    public static function getPersonAll3($min, $max, $village, $hospcode='05147') {
	$sql = "SELECT tb_data_1.*
	    FROM tb_data_1
	    WHERE floor(DATEDIFF(CURRENT_DATE, STR_TO_DATE(v2, '%Y-%m-%d'))/365) BETWEEN :min AND :max AND hsitecode = :hospcode AND CONCAT(add1n6code, add1n5) = :village_code AND add1n5 REGEXP '[0-9]+'
	    ";
	
	return Yii::$app->db->createCommand($sql, [':min'=>$min, ':max'=>$max, ':village_code'=>$village, ':hospcode'=>$hospcode])->queryAll();
    }
    
    
    
    public static function getVillagelist2($min, $max, $hospcode='05147') {
	$sql = "SELECT add1n6code AS add1n2, 
			CONCAT('ต.', const_district.DISTRICT_NAME, ' (', count(*), ')') AS text,
			count(*) AS num
		FROM tb_data_1 INNER JOIN const_district ON tb_data_1.add1n6code = const_district.DISTRICT_CODE
		WHERE floor(DATEDIFF(CURRENT_DATE, STR_TO_DATE(v2, '%Y-%m-%d'))/365) BETWEEN :min AND :max AND hsitecode=:hospcode AND add1n5 REGEXP '[0-9]+'
		GROUP BY add1n6code
		";//HAVING count(*) > 5;
	
	return Yii::$app->db->createCommand($sql, [':min'=>$min, ':max'=>$max, ':hospcode'=>$hospcode])->queryAll();
    }
    
    public static function getVillagelist3($min, $max, $hospcode='05147') {
	$sql = "SELECT CONCAT(add1n6code, add1n5) AS add1n2, 
			CONCAT('ต.', const_district.DISTRICT_NAME, 'บ.', add1n2, ' หมู่ ', add1n5, ' (', count(*), ')') AS text,
			count(*) AS num
		FROM tb_data_1 INNER JOIN const_district ON tb_data_1.add1n6code = const_district.DISTRICT_CODE
		WHERE floor(DATEDIFF(CURRENT_DATE, STR_TO_DATE(v2, '%Y-%m-%d'))/365) BETWEEN :min AND :max AND hsitecode=:hospcode AND add1n5 REGEXP '[0-9]+'
		GROUP BY add1n6code, add1n5
		";//HAVING count(*) > 5;
	
	return Yii::$app->db->createCommand($sql, [':min'=>$min, ':max'=>$max, ':hospcode'=>$hospcode])->queryAll();
    }
    public static function getRegisterId($sitecode) {
	$sql = "SELECT concat('\'',md5(cid),'\'') as cid FROM tb_data_1 WHERE rstat<>3 AND tb_data_1.hsitecode = :sitecode";
	return Yii::$app->db->createCommand($sql, [':sitecode'=>$sitecode])->queryAll();
    }
    public static function getRegisterCid($sitecode) {
	$sql = "SELECT cid FROM tb_data_1 WHERE rstat<>3 AND tb_data_1.hsitecode = :sitecode";
	return Yii::$app->db->createCommand($sql, [':sitecode'=>$sitecode])->queryAll();
    }
    public static function checkCid($cid, $sitecode) {
	$sql = "SELECT * FROM tb_data_1 WHERE rstat<>3 AND cid = :cid AND tb_data_1.hsitecode = :sitecode";
	return Yii::$app->db->createCommand($sql, [':cid'=>$cid, ':sitecode'=>$sitecode])->queryOne();
    }
    
    public static function checkCidupdate($cid, $sitecode) {
	$sql = "SELECT * FROM tb_data_1 WHERE rstat=1 AND cid = :cid AND tb_data_1.hsitecode = :sitecode";
	return Yii::$app->db->createCommand($sql, [':cid'=>$cid, ':sitecode'=>$sitecode])->queryOne();
    }
    
    public static function checkCidAll($cid) {
	$sql = "SELECT * FROM tb_data_1 WHERE rstat<>3 AND cid = :cid";
	return Yii::$app->db->createCommand($sql, [':cid'=>$cid])->queryOne();
    }
    
    public static function getCCA01($target, $start, $end) {
	$sql = "SELECT * FROM tb_data_2 WHERE rstat<>3 AND target = :target AND f1vdcomp BETWEEN :start AND :end ORDER BY f1vdcomp DESC limit 1";
	return Yii::$app->db->createCommand($sql, [':target'=>$target, ':start'=>$start, ':end'=>$end])->queryOne();
    }
    
    public static function getOv01($target, $table, $start, $end) {
	$sql = "SELECT * FROM $table WHERE rstat<>3 AND target = :target AND vdate BETWEEN :start AND :end ORDER BY vdate DESC limit 1";
	return Yii::$app->db->createCommand($sql, [':target'=>$target, ':start'=>$start, ':end'=>$end])->queryOne();
    }
    
    public static function getOv02($target, $table, $start, $end) {
	$sql = "SELECT * FROM $table WHERE rstat<>3 AND target = :target AND vdate BETWEEN :start AND :end ORDER BY vdate DESC limit 1";
	return Yii::$app->db->createCommand($sql, [':target'=>$target, ':start'=>$start, ':end'=>$end])->queryOne();
    }
    
    public static function getOv03($target, $table, $start, $end) {
	$sql = "SELECT * FROM $table WHERE rstat<>3 AND target = :target AND vdate BETWEEN :start AND :end ORDER BY vdate DESC limit 1";
	return Yii::$app->db->createCommand($sql, [':target'=>$target, ':start'=>$start, ':end'=>$end])->queryOne();
    }
    
    public static function getCca02($target, $table, $start, $end) {
	$sql = "SELECT * FROM $table WHERE rstat<>3 AND target = :target AND fsupdate BETWEEN :start AND :end ORDER BY fsupdate DESC limit 1";
	return Yii::$app->db->createCommand($sql, [':target'=>$target, ':start'=>$start, ':end'=>$end])->queryOne();
    }
    
    public static function getRegister($target, $table) {
	$sql = "SELECT * FROM $table WHERE rstat<>3 AND target = :target limit 1";
	return Yii::$app->db->createCommand($sql, [':target'=>$target])->queryOne();
    }
    
    public static function getOVData($sitecode, $select=null, $order='village_code, address, fname, lname') {
	
	$str = '';
	if(isset($select)){
	    $str = ' AND ov_person.id IN ('.$select.')';
	}
	
	\backend\modules\ovcca\classes\OvccaQuery::dpOvTemporaryAll($sitecode, $select);
	
	$sql = "SELECT ov_person.id, 
		    ov_person.khet, 
		    ov_person.province, 
		    ov_person.amphur, 
		    ov_person.tambon, 
		    ov_person.hospcode, 
		    ov_person.hospname, 
		    ov_person.person_id, 
		    ov_person.house_id, 
		    ov_person.address, 
		    ov_person.cid, 
		    ov_person.hn, 
		    floor(DATEDIFF(CURRENT_DATE, STR_TO_DATE(ov_person.birthdate, '%Y-%m-%d'))/365) AS age,
		    CONCAT(ov_person.pname, ov_person.fname, ' ', ov_person.lname) AS fullname,
		    ov_person.pname, 
		    ov_person.fname, 
		    ov_person.lname, 
		    ov_person.sex, 
		    ov_person.nationality, 
		    ov_person.education, 
		    ov_person.type_area, 
		    ov_person.religion, 
		    ov_person.birthdate, 
		    ov_person.village_id, 
		    ov_person.village_code, 
		    ov_person.village_name, 
		    ov_person.pttype, 
		    ov_person.pttype_begin_date, 
		    ov_person.pttype_expire_date, 
		    ov_person.pttype_hospmain, 
		    ov_person.pttype_hospsub, 
		    ov_person.marrystatus, 
		    ov_person.death, 
		    ov_person.death_date, 
		    ov_person.moo, 
		    ov_person.ov01_status, 
		    ov_person.status_import, 
		    ov_person.ptcode, 
		    ov_person.ptid_key, 
		    ov_person.ptid,
		    tmpov.*
	    FROM ov_person INNER JOIN tb_data_1 ON ov_person.cid = tb_data_1.cid AND ov_person.hospcode = tb_data_1.hsitecode
	    LEFT JOIN tmpov ON tmpov.tmp_id=ov_person.id
	    WHERE ov_person.status_import = 1 and ov_person.hospcode = :sitecode $str
	    group by ov_person.id
	    order by $order
		";
	return Yii::$app->db->createCommand($sql, [':sitecode'=>$sitecode])->queryAll();
    }
    
    public static function getOVDataSetect($sitecode, $fid, $sid, $select=null, $order='village_code, address, fname, lname') {
	
	$str = '';
	if(isset($select)){
	    $str = ' AND ov_person.id IN ('.$select.')';
	}
	
	\backend\modules\ovcca\classes\OvccaQuery::dpOvTemporaryAll($sitecode, $select, $ovfilter_sub);
	
	$sql = "SELECT ov_person.id, 
		    ov_person.khet, 
		    ov_person.province, 
		    ov_person.amphur, 
		    ov_person.tambon, 
		    ov_person.hospcode, 
		    ov_person.hospname, 
		    ov_person.person_id, 
		    ov_person.house_id, 
		    ov_person.address, 
		    ov_person.cid, 
		    ov_person.hn, 
		    floor(DATEDIFF(CURRENT_DATE, STR_TO_DATE(ov_person.birthdate, '%Y-%m-%d'))/365) AS age,
		    CONCAT(ov_person.pname, ov_person.fname, ' ', ov_person.lname) AS fullname,
		    ov_person.pname, 
		    ov_person.fname, 
		    ov_person.lname, 
		    ov_person.sex, 
		    ov_person.nationality, 
		    ov_person.education, 
		    ov_person.type_area, 
		    ov_person.religion, 
		    ov_person.birthdate, 
		    ov_person.village_id, 
		    ov_person.village_code, 
		    ov_person.village_name, 
		    ov_person.pttype, 
		    ov_person.pttype_begin_date, 
		    ov_person.pttype_expire_date, 
		    ov_person.pttype_hospmain, 
		    ov_person.pttype_hospsub, 
		    ov_person.marrystatus, 
		    ov_person.death, 
		    ov_person.death_date, 
		    ov_person.moo, 
		    ov_person.ov01_status, 
		    ov_person.status_import, 
		    ov_person.ptcode, 
		    ov_person.ptid_key, 
		    ov_person.ptid,
		    tmpov.*
	    FROM ov_person INNER JOIN ov_sub_list ON ov_sub_list.person_id = ov_person.id
	    INNER JOIN tb_data_1 ON ov_person.cid = tb_data_1.cid AND ov_person.hospcode = tb_data_1.hsitecode
	    LEFT JOIN tmpov ON tmpov.tmp_id=ov_person.id
	    WHERE ov_person.status_import = 1 and ov_person.hospcode = :sitecode AND ov_sub_list.filter_id=:fid AND ov_sub_list.sub_id=:sid $str
	    group by ov_person.id
	    order by $order
		";
	return Yii::$app->db->createCommand($sql, [':sitecode'=>$sitecode, ':fid'=>$fid, ':sid'=>$sid])->queryAll();
    }
    
    public static function checkConvert($sitecode) {
	$sql = "SELECT COUNT(*) AS num
		FROM tmp_person
		WHERE tmp_person.hospcode = :sitecode AND tmp_person.pname IN ('นาย', 'นาง', 'น.ส.')";
	return Yii::$app->db->createCommand($sql, [':sitecode'=>$sitecode])->queryScalar();
    }
    
    public static function dpOvTemporary($sitecode, $page=null, $perPage=100, $sort='village_code, address, fname, lname', $ovfilter_sub=null) {
	
	$group = 'ov_person.hospcode, ov_person.cid';
	$pageStart = 0;
	if(isset($page)){
	    $count = Yii::$app->db->createCommand("
		SELECT COUNT(*) FROM ov_person WHERE hospcode = :sitecode
	    ", [':sitecode' => $sitecode])->queryScalar();
	    
	    $pageStart = ( $perPage * $page ) - $perPage;
	    
	} 
	
	$limit = "LIMIT $pageStart, $perPage";
	
	if($sort!='village_code, address, fname, lname'){
	    
	    if(substr($sort, 0,1)=='-'){
		$sort = str_replace('-', '', $sort);
		$sort = 'ov_person.'.$sort.' DESC';
	    }else {
		$sort = 'ov_person.'.$sort;
	    }
	}
	$ov_sub_list = '';
	if($ovfilter_sub!=null){
	    $ov_sub_list = "INNER JOIN ov_sub_list ON ov_sub_list.person_id = ov_person.id AND ov_sub_list.sub_id = $ovfilter_sub";
	    $group = 'ov_person.id';
	}
	
	Yii::$app->db->createCommand("DROP TABLE IF EXISTS tmpov;")->execute();
	//			ov_person.khet, 
//			ov_person.province, 
//			ov_person.amphur, 
//			ov_person.tambon, 
//			ov_person.hospcode, 
//			ov_person.hospname, 
//			ov_person.person_id, 
//			ov_person.house_id, 
//			ov_person.address, 
//			ov_person.cid, 
//			ov_person.hn, 
//			ov_person.pname, 
//			ov_person.fname, 
//			ov_person.lname, 
//			ov_person.sex, 
//			ov_person.nationality, 
//			ov_person.education, 
//			ov_person.type_area, 
//			ov_person.religion, 
//			ov_person.birthdate, 
//			ov_person.village_id, 
//			ov_person.village_code, 
//			ov_person.village_name, 
//			ov_person.pttype, 
//			ov_person.pttype_begin_date, 
//			ov_person.pttype_expire_date, 
//			ov_person.pttype_hospmain, 
//			ov_person.pttype_hospsub, 
//			ov_person.marrystatus, 
//			ov_person.death, 
//			ov_person.death_date, 
//			ov_person.moo, 
//			ov_person.ov01_status, 
//			ov_person.status_import, 
//			ov_person.ptcode, 
//			ov_person.ptcodefull, 
//			ov_person.ptid_key, 
//			ov_person.ptid,
	$sql = "CREATE TEMPORARY TABLE tmpov
		SELECT ov_person.id AS tmp_id, 
			tb_data_1.id AS register_id
		FROM ov_person INNER JOIN tb_data_1 ON ov_person.ptid = tb_data_1.ptid
		$ov_sub_list
		WHERE hospcode = :sitecode
		GROUP BY $group
		ORDER BY $sort
		$limit 
		;";
	Yii::$app->db->createCommand($sql, [':sitecode'=>$sitecode])->execute();
	
	$sql = "ALTER TABLE tmpov ADD `cca01_id` LONGTEXT NULL DEFAULT NULL, 
		ADD `cca01_result` LONGTEXT NULL DEFAULT NULL,
		ADD `ov01k_id` LONGTEXT NULL DEFAULT NULL,
		ADD `ov01k_result` LONGTEXT NULL DEFAULT NULL,
		ADD `ov01p_id` LONGTEXT NULL DEFAULT NULL,
		ADD `ov01p_result` LONGTEXT NULL DEFAULT NULL,
		ADD `ov01f_id` LONGTEXT NULL DEFAULT NULL,
		ADD `ov01f_result` LONGTEXT NULL DEFAULT NULL,
		ADD `ov01u_id` LONGTEXT NULL DEFAULT NULL,
		ADD `ov01u_result` LONGTEXT NULL DEFAULT NULL,
		ADD `ov02_id` LONGTEXT NULL DEFAULT NULL,
		ADD `ov02_result` LONGTEXT NULL DEFAULT NULL,
		ADD `ov03_id` LONGTEXT NULL DEFAULT NULL,
		ADD `ov03_result` LONGTEXT NULL DEFAULT NULL,
		ADD `cca02_id` LONGTEXT NULL DEFAULT NULL,
		ADD `cca02_result` LONGTEXT NULL DEFAULT NULL;";
	Yii::$app->db->createCommand($sql)->execute();
	
	$group = 'tb.hsitecode, tb.hptcode';
	
	if($ovfilter_sub!=null){
	    $group = 'ov_person.id';
	}
	//cca01
	$sql = "UPDATE tmpov 
		INNER JOIN (
		    SELECT GROUP_CONCAT(tb.id) AS tb_id,
			    GROUP_CONCAT(tb.id) AS tb_result,
			    ov_person.id
		    FROM ov_person INNER JOIN tb_data_2 AS tb ON ov_person.ptid = tb.ptid
		    $ov_sub_list
		    WHERE hospcode = :sitecode
		    GROUP BY $group
		    ORDER BY $sort
		    $limit 
		) AS cca01 ON tmpov.tmp_id=cca01.id 
		SET tmpov.cca01_id=cca01.tb_id,
		    tmpov.cca01_result=cca01.tb_result;";
	Yii::$app->db->createCommand($sql, [':sitecode'=>$sitecode])->execute();
	
	//ov01k
	$sql = "UPDATE tmpov 
		INNER JOIN (
		    SELECT GROUP_CONCAT(tb.id) AS tb_id,
			    GROUP_CONCAT(tb.results) AS tb_result,
			    ov_person.id
		    FROM ov_person INNER JOIN tbdata_21 AS tb ON ov_person.ptid = tb.ptid
		    $ov_sub_list
		    WHERE hospcode = :sitecode
		    GROUP BY $group
		    ORDER BY $sort
		    $limit 
		) AS ov01k ON tmpov.tmp_id=ov01k.id 
		SET tmpov.ov01k_id=ov01k.tb_id,
		    tmpov.ov01k_result=ov01k.tb_result;";
	Yii::$app->db->createCommand($sql, [':sitecode'=>$sitecode])->execute();
	
	//ov01p
	$sql = "UPDATE tmpov 
		INNER JOIN (
		    SELECT GROUP_CONCAT(tb.id) AS tb_id,
			    GROUP_CONCAT(tb.results) AS tb_result,
			    ov_person.id
		    FROM ov_person INNER JOIN tbdata_22 AS tb ON ov_person.ptid = tb.ptid
		    $ov_sub_list
		    WHERE hospcode = :sitecode
		    GROUP BY $group
		    ORDER BY $sort
		    $limit 
		) AS ov01p ON tmpov.tmp_id=ov01p.id 
		SET tmpov.ov01p_id=ov01p.tb_id,
		    tmpov.ov01p_result=ov01p.tb_result;";
	Yii::$app->db->createCommand($sql, [':sitecode'=>$sitecode])->execute();
	
	//ov01f
	$sql = "UPDATE tmpov 
		INNER JOIN (
		    SELECT GROUP_CONCAT(tb.id) AS tb_id,
			    GROUP_CONCAT(tb.results) AS tb_result,
			    ov_person.id
		    FROM ov_person INNER JOIN tbdata_23 AS tb ON ov_person.ptid = tb.ptid
		    $ov_sub_list
		    WHERE hospcode = :sitecode
		    GROUP BY $group
		    ORDER BY $sort
		    $limit 
		) AS ov01f ON tmpov.tmp_id=ov01f.id 
		SET tmpov.ov01f_id=ov01f.tb_id,
		    tmpov.ov01f_result=ov01f.tb_result;";
	Yii::$app->db->createCommand($sql, [':sitecode'=>$sitecode])->execute();
	
	//ov01u
	$sql = "UPDATE tmpov 
		INNER JOIN (
		    SELECT GROUP_CONCAT(tb.id) AS tb_id,
			    GROUP_CONCAT(tb.results) AS tb_result,
			    ov_person.id
		    FROM ov_person INNER JOIN tbdata_24 AS tb ON ov_person.ptid = tb.ptid
		    $ov_sub_list
		    WHERE hospcode = :sitecode
		    GROUP BY $group
		    ORDER BY $sort
		    $limit 
		) AS ov01u ON tmpov.tmp_id=ov01u.id 
		SET tmpov.ov01u_id=ov01u.tb_id,
		    tmpov.ov01u_result=ov01u.tb_result;";
	Yii::$app->db->createCommand($sql, [':sitecode'=>$sitecode])->execute();
	
	//ov02
	$sql = "UPDATE tmpov 
		INNER JOIN (
		    SELECT GROUP_CONCAT(tb.id) AS tb_id,
			    GROUP_CONCAT(tb.id) AS tb_result,
			    ov_person.id
		    FROM ov_person INNER JOIN tbdata_25 AS tb ON ov_person.ptid = tb.ptid
		    $ov_sub_list
		    WHERE hospcode = :sitecode
		    GROUP BY $group
		    ORDER BY $sort
		    $limit 
		) AS ov02 ON tmpov.tmp_id=ov02.id 
		SET tmpov.ov02_id=ov02.tb_id,
		    tmpov.ov02_result=ov02.tb_result;";
	Yii::$app->db->createCommand($sql, [':sitecode'=>$sitecode])->execute();
	
	//ov03
	$sql = "UPDATE tmpov 
		INNER JOIN (
		    SELECT GROUP_CONCAT(tb.id) AS tb_id,
			    GROUP_CONCAT(tb.id) AS tb_result,
			    ov_person.id
		    FROM ov_person INNER JOIN tbdata_26 AS tb ON ov_person.ptid = tb.ptid
		    $ov_sub_list
		    WHERE hospcode = :sitecode
		    GROUP BY $group
		    ORDER BY $sort
		    $limit 
		) AS ov03 ON tmpov.tmp_id=ov03.id 
		SET tmpov.ov03_id=ov03.tb_id,
		    tmpov.ov03_result=ov03.tb_result;";
	Yii::$app->db->createCommand($sql, [':sitecode'=>$sitecode])->execute();
	
	//cca02
	$sql = "UPDATE tmpov 
		INNER JOIN (
		    SELECT GROUP_CONCAT(tb.id) AS tb_id,
			    GROUP_CONCAT(tb.id) AS tb_result,
			    ov_person.id
		    FROM ov_person INNER JOIN tb_data_3 AS tb ON ov_person.ptid = tb.ptid
		    $ov_sub_list
		    WHERE hospcode = :sitecode
		    GROUP BY $group
		    ORDER BY $sort
		    $limit 
		) AS cca02 ON tmpov.tmp_id=cca02.id 
		SET tmpov.cca02_id=cca02.tb_id,
		    tmpov.cca02_result=cca02.tb_result;";
	Yii::$app->db->createCommand($sql, [':sitecode'=>$sitecode])->execute();
	
	
	return true;
    }
    
    public static function dpOvTemporaryAll($sitecode, $select, $ovfilter_sub=null) {
	$group = 'ov_person.hospcode, ov_person.cid';
	
	$str = '';
	if(isset($select)){
	    $str = ' AND ov_person.id IN ('.$select.')';
	}
	
	$ov_sub_list = '';
	if($ovfilter_sub!=null){
	    $ov_sub_list = "INNER JOIN ov_sub_list ON ov_sub_list.person_id = ov_person.id AND ov_sub_list.sub_id = $ovfilter_sub";
	    $group = 'ov_person.id';
	}
	
	Yii::$app->db->createCommand("DROP TABLE IF EXISTS tmpov;")->execute();
	
	$sql = "CREATE TEMPORARY TABLE tmpov
		SELECT ov_person.id AS tmp_id, 
			tb_data_1.id AS register_id
		FROM ov_person INNER JOIN tb_data_1 ON ov_person.ptid = tb_data_1.ptid
		WHERE hospcode = :sitecode $str
		GROUP BY $group
		;";
	Yii::$app->db->createCommand($sql, [':sitecode'=>$sitecode])->execute();
	
	$sql = "ALTER TABLE tmpov ADD `cca01_id` LONGTEXT NULL DEFAULT NULL, 
		ADD `cca01_result` LONGTEXT NULL DEFAULT NULL,
		ADD `ov01k_id` LONGTEXT NULL DEFAULT NULL,
		ADD `ov01k_result` LONGTEXT NULL DEFAULT NULL,
		ADD `ov01p_id` LONGTEXT NULL DEFAULT NULL,
		ADD `ov01p_result` LONGTEXT NULL DEFAULT NULL,
		ADD `ov01f_id` LONGTEXT NULL DEFAULT NULL,
		ADD `ov01f_result` LONGTEXT NULL DEFAULT NULL,
		ADD `ov01u_id` LONGTEXT NULL DEFAULT NULL,
		ADD `ov01u_result` LONGTEXT NULL DEFAULT NULL,
		ADD `ov02_id` LONGTEXT NULL DEFAULT NULL,
		ADD `ov02_result` LONGTEXT NULL DEFAULT NULL,
		ADD `ov03_id` LONGTEXT NULL DEFAULT NULL,
		ADD `ov03_result` LONGTEXT NULL DEFAULT NULL,
		ADD `cca02_id` LONGTEXT NULL DEFAULT NULL,
		ADD `cca02_result` LONGTEXT NULL DEFAULT NULL;";
	Yii::$app->db->createCommand($sql)->execute();
	
	$group = 'tb.hsitecode, tb.hptcode';
	
	if($ovfilter_sub!=null){
	    $group = 'ov_person.id';
	}
	
	//cca01
	$sql = "UPDATE tmpov 
		INNER JOIN (
		    SELECT GROUP_CONCAT(tb.id) AS tb_id,
			    GROUP_CONCAT(tb.id) AS tb_result,
			    ov_person.id
		    FROM ov_person INNER JOIN tb_data_2 AS tb ON ov_person.ptid = tb.ptid
		    $ov_sub_list
		    WHERE status_import = 1 AND hospcode = :sitecode $str
		    GROUP BY $group
		) AS cca01 ON tmpov.tmp_id=cca01.id 
		SET tmpov.cca01_id=cca01.tb_id,
		    tmpov.cca01_result=cca01.tb_result;";
	Yii::$app->db->createCommand($sql, [':sitecode'=>$sitecode])->execute();
	
	//ov01k
	$sql = "UPDATE tmpov 
		INNER JOIN (
		    SELECT GROUP_CONCAT(tb.id) AS tb_id,
			    GROUP_CONCAT(tb.results) AS tb_result,
			    ov_person.id
		    FROM ov_person INNER JOIN tbdata_21 AS tb ON ov_person.ptid = tb.ptid
		    $ov_sub_list
		    WHERE status_import = 1 AND hospcode = :sitecode $str
		    GROUP BY $group
		) AS ov01k ON tmpov.tmp_id=ov01k.id 
		SET tmpov.ov01k_id=ov01k.tb_id,
		    tmpov.ov01k_result=ov01k.tb_result;";
	Yii::$app->db->createCommand($sql, [':sitecode'=>$sitecode])->execute();
	
	//ov01p
	$sql = "UPDATE tmpov 
		INNER JOIN (
		    SELECT GROUP_CONCAT(tb.id) AS tb_id,
			    GROUP_CONCAT(tb.results) AS tb_result,
			    ov_person.id
		    FROM ov_person INNER JOIN tbdata_22 AS tb ON ov_person.ptid = tb.ptid
		    $ov_sub_list
		    WHERE status_import = 1 AND hospcode = :sitecode $str
		    GROUP BY $group
		) AS ov01p ON tmpov.tmp_id=ov01p.id 
		SET tmpov.ov01p_id=ov01p.tb_id,
		    tmpov.ov01p_result=ov01p.tb_result;";
	Yii::$app->db->createCommand($sql, [':sitecode'=>$sitecode])->execute();
	
	//ov01f
	$sql = "UPDATE tmpov 
		INNER JOIN (
		    SELECT GROUP_CONCAT(tb.id) AS tb_id,
			    GROUP_CONCAT(tb.results) AS tb_result,
			    ov_person.id
		    FROM ov_person INNER JOIN tbdata_23 AS tb ON ov_person.ptid = tb.ptid
		    $ov_sub_list
		    WHERE status_import = 1 AND hospcode = :sitecode $str
		    GROUP BY $group
		) AS ov01f ON tmpov.tmp_id=ov01f.id 
		SET tmpov.ov01f_id=ov01f.tb_id,
		    tmpov.ov01f_result=ov01f.tb_result;";
	Yii::$app->db->createCommand($sql, [':sitecode'=>$sitecode])->execute();
	
	//ov01u
	$sql = "UPDATE tmpov 
		INNER JOIN (
		    SELECT GROUP_CONCAT(tb.id) AS tb_id,
			    GROUP_CONCAT(tb.results) AS tb_result,
			    ov_person.id
		    FROM ov_person INNER JOIN tbdata_24 AS tb ON ov_person.ptid = tb.ptid
		    $ov_sub_list
		    WHERE status_import = 1 AND hospcode = :sitecode $str
		    GROUP BY $group
		) AS ov01u ON tmpov.tmp_id=ov01u.id 
		SET tmpov.ov01u_id=ov01u.tb_id,
		    tmpov.ov01u_result=ov01u.tb_result;";
	Yii::$app->db->createCommand($sql, [':sitecode'=>$sitecode])->execute();
	
	//ov02
	$sql = "UPDATE tmpov 
		INNER JOIN (
		    SELECT GROUP_CONCAT(tb.id) AS tb_id,
			    GROUP_CONCAT(tb.id) AS tb_result,
			    ov_person.id
		    FROM ov_person INNER JOIN tbdata_25 AS tb ON ov_person.ptid = tb.ptid
		    $ov_sub_list
		    WHERE status_import = 1 AND hospcode = :sitecode $str
		    GROUP BY $group
		) AS ov02 ON tmpov.tmp_id=ov02.id 
		SET tmpov.ov02_id=ov02.tb_id,
		    tmpov.ov02_result=ov02.tb_result;";
	Yii::$app->db->createCommand($sql, [':sitecode'=>$sitecode])->execute();
	
	//ov03
	$sql = "UPDATE tmpov 
		INNER JOIN (
		    SELECT GROUP_CONCAT(tb.id) AS tb_id,
			    GROUP_CONCAT(tb.id) AS tb_result,
			    ov_person.id
		    FROM ov_person INNER JOIN tbdata_26 AS tb ON ov_person.ptid = tb.ptid
		    $ov_sub_list
		    WHERE status_import = 1 AND hospcode = :sitecode $str
		    GROUP BY $group
		) AS ov03 ON tmpov.tmp_id=ov03.id 
		SET tmpov.ov03_id=ov03.tb_id,
		    tmpov.ov03_result=ov03.tb_result;";
	Yii::$app->db->createCommand($sql, [':sitecode'=>$sitecode])->execute();
	
	//cca02
	$sql = "UPDATE tmpov 
		INNER JOIN (
		    SELECT GROUP_CONCAT(tb.id) AS tb_id,
			    GROUP_CONCAT(tb.id) AS tb_result,
			    ov_person.id
		    FROM ov_person INNER JOIN tb_data_3 AS tb ON ov_person.ptid = tb.ptid
		    $ov_sub_list
		    WHERE status_import = 1 AND hospcode = :sitecode $str
		    GROUP BY $group
		) AS cca02 ON tmpov.tmp_id=cca02.id 
		SET tmpov.cca02_id=cca02.tb_id,
		    tmpov.cca02_result=cca02.tb_result;";
	Yii::$app->db->createCommand($sql, [':sitecode'=>$sitecode])->execute();
	
	
	return true;
    }
    
    public static function getFilterSub($sitecode) {
	$sql = "SELECT ov_filter_sub.sub_id, 
			ov_filter_sub.urine_status, 
			ov_filter_sub.filter_id, 
			ov_filter_sub.sitecode, 
			ov_filter_sub.created_by,
			CONCAT(ov_filter_sub.sub_name, ' (', (SELECT COUNT(*) AS num FROM ov_sub_list INNER JOIN tb_data_1  ON tb_data_1.id = ov_sub_list.person_id WHERE ov_sub_list.sub_id = ov_filter_sub.sub_id),')') AS sub_name
		FROM ov_filter_sub
		WHERE ov_filter_sub.sitecode = :sitecode ";
	return Yii::$app->db->createCommand($sql, [':sitecode'=>$sitecode])->queryAll();
    }
    
    public static function getRegisterList() {
	$sql = "SELECT ptid, CONCAT(title, name, ' ', surname) as fullname FROM tb_data_1 WHERE rstat<>3";
	return Yii::$app->db->createCommand($sql)->queryall();
    }
    
    public static function getSeclectList($sub) {
	$sql = "SELECT ov_sub_list.person_id
		FROM ov_filter_sub INNER JOIN ov_sub_list ON ov_filter_sub.sub_id = ov_sub_list.sub_id
		where ov_sub_list.sub_id=:sub";
	return Yii::$app->db->createCommand($sql, [':sub'=>$sub])->queryColumn();
    }
    
    public static function getTbData($sitecode, $ovfilter_sub, $select=null) {
	
	$str = '';
	if(isset($select)){
	    $str = ' AND tb_data_1.id IN ('.$select.')';
	}
	
	$fromSub = '';
	$selectSub = '';
	if($ovfilter_sub>0){
	    $fromSub = "inner join ov_sub_list on ov_sub_list.person_id = tb_data_1.id";
	    $selectSub = " AND ov_sub_list.sub_id = $ovfilter_sub";
	}
	//floor(DATEDIFF(CURRENT_DATE, STR_TO_DATE(tb_data_1.v2, '%Y-%m-%d'))/365) AS age,
	//CONCAT(tb_data_1.title, tb_data_1.name, ' ', tb_data_1.surname) AS fullname,
	$sql = "SELECT tb_data_1.id, 
		    tb_data_1.cid, 
		    tb_data_1.hn, 
		    tb_data_1.v2,
		    tb_data_1.title,
		    tb_data_1.name,
		    tb_data_1.surname,
		    tb_data_1.add1n5, 
		    tb_data_1.hsitecode, 
		    tb_data_1.hptcode, 
		    tb_data_1.add1n1, 
		    tb_data_1.add1n5, 
		    tb_data_1.add1n6code, 
		    tb_data_1.add1n7code, 
		    tb_data_1.add1n8code, 
		    tb_data_1.age,
		    tb_data_1.ptid,
		    (SELECT group_concat(ov01k.id) FROM tbdata_21 ov01k WHERE ov01k.ptid = tb_data_1.ptid AND ov01k.rstat<>3 GROUP BY ov01k.ptid) AS ov01k_id,
		    (SELECT group_concat(ov01k.results) FROM tbdata_21 ov01k WHERE ov01k.ptid = tb_data_1.ptid AND ov01k.rstat<>3 GROUP BY ov01k.ptid) AS ov01k_result,
		    (SELECT group_concat(ov01p.id) FROM tbdata_22 ov01p WHERE ov01p.ptid = tb_data_1.ptid AND ov01p.rstat<>3 GROUP BY ov01p.ptid) AS ov01p_id,
		    (SELECT group_concat(ov01p.results) FROM tbdata_22 ov01p WHERE ov01p.ptid = tb_data_1.ptid AND ov01p.rstat<>3 GROUP BY ov01p.ptid) AS ov01p_result,
		    (SELECT group_concat(ov01f.id) FROM tbdata_23 ov01f WHERE ov01f.ptid = tb_data_1.ptid AND ov01f.rstat<>3 GROUP BY ov01f.ptid) AS ov01f_id,
		    (SELECT group_concat(ov01f.results) FROM tbdata_23 ov01f WHERE ov01f.ptid = tb_data_1.ptid AND ov01f.rstat<>3 GROUP BY ov01f.ptid) AS ov01f_result,
		    (SELECT group_concat(ov01u.id) FROM tbdata_24 ov01u WHERE ov01u.ptid = tb_data_1.ptid AND ov01u.rstat<>3 GROUP BY ov01u.ptid) AS ov01u_id,
		    (SELECT group_concat(ov01u.results) FROM tbdata_24 ov01u WHERE ov01u.ptid = tb_data_1.ptid AND ov01u.rstat<>3 GROUP BY ov01u.ptid) AS ov01u_result,
		    (SELECT group_concat(ov02.id) FROM tbdata_25 ov02 WHERE ov02.ptid = tb_data_1.ptid AND ov02.rstat<>3 GROUP BY ov02.ptid) AS ov02_id,
		    (SELECT group_concat(ov03.id) FROM tbdata_26 ov03 WHERE ov03.ptid = tb_data_1.ptid AND ov03.rstat<>3 GROUP BY ov03.ptid) AS ov03_id,
		    (SELECT group_concat(cca02.id) FROM tb_data_3 cca02 WHERE cca02.ptid = tb_data_1.ptid AND cca02.rstat<>3 GROUP BY cca02.ptid) AS cca02_id,
		    (SELECT group_concat(cca01.id) FROM tb_data_2 cca01 WHERE cca01.ptid = tb_data_1.ptid AND cca01.rstat<>3 GROUP BY cca01.ptid) AS cca01_id
	    FROM tb_data_1 $fromSub
	    WHERE tb_data_1.rstat<>3 AND tb_data_1.hsitecode = :sitecode $str $selectSub
	    order by add1n8code, add1n7code, add1n6code, add1n5, add1n1, name, surname
		";
	
	return Yii::$app->db->createCommand($sql, [':sitecode'=>$sitecode])->queryAll();
    }
    
    public static function getTbSkData($sitecode, $ovfilter_sub, $select=null) {
	
	$str = '';
	if(isset($select)){
	    $str = ' AND tb_data_1.id IN ('.$select.')';
	}
	
	$fromSub = '';
	$selectSub = '';
	if($ovfilter_sub>0){
	    $fromSub = "inner join ov_sub_list on ov_sub_list.person_id = tb_data_1.id";
	    $selectSub = " AND ov_sub_list.sub_id = $ovfilter_sub";
	}
	
	$sql = "SELECT tb_data_1.id, 
		    tb_data_1.cid, 
		    tb_data_1.hn, 
		    tb_data_1.v2,
		    tb_data_1.title,
		    tb_data_1.name,
		    tb_data_1.surname,
		    tb_data_1.add1n5, 
		    tb_data_1.hsitecode, 
		    tb_data_1.hptcode, 
		    tb_data_1.add1n1, 
		    tb_data_1.add1n5, 
		    tb_data_1.add1n6code, 
		    tb_data_1.add1n7code, 
		    tb_data_1.add1n8code, 
		    tb_data_1.age,
		    tb_data_1.ptid
	    FROM tb_data_1 $fromSub
	    WHERE tb_data_1.rstat<>3 AND tb_data_1.hsitecode = :sitecode $str $selectSub
	    order by hptcode
		";
	return Yii::$app->db->createCommand($sql, [':sitecode'=>$sitecode])->queryAll();
    }
    
    
}
