<?php

namespace backend\modules\ovcca\classes;

use Yii;
use backend\modules\ovcca\models\OvPerson;
use backend\modules\ovcca\classes\OvccaQuery;
use common\lib\codeerror\helpers\GenMillisecTime;
use backend\models\EzformTarget;
use yii\bootstrap\Html;
use yii\db\Expression;

/**
 * OvccaFunc class file UTF-8
 * @author SDII <iencoded@gmail.com>
 * @copyright Copyright &copy; 2015 AppXQ
 * @license http://www.appxq.com/license/
 * @version 1.0.0 Date: 9 ก.พ. 2559 12:38:14
 * @link http://www.appxq.com/
 * @example 
 */
class OvccaFunc {

    public static function checkthai($word) { 
	return preg_replace('/[^ก-๙]/ u','',$word); 	
    } 
    
    public static function check_citizen($personID) { 
//	if (strlen($pid) != 13) return false;
//        for ($i = 0, $sum = 0; $i < 12; $i++)
//            $sum += (int)($pid{$i}) * (13 - $i);
//        if ((11 - ($sum % 11)) % 10 == (int)($pid{12}))
//            return true;
//	
//        return false;
	if (strlen($personID) != 13) {
	    return false;
	}

	$rev = strrev($personID); // reverse string ขั้นที่ 0 เตรียมตัว
	$total = 0;
	for($i=1;$i<13;$i++) // ขั้นตอนที่ 1 - เอาเลข 12 หลักมา เขียนแยกหลักกันก่อน
	{
		$mul = $i +1;
		$count = $rev[$i]*$mul; // ขั้นตอนที่ 2 - เอาเลข 12 หลักนั้นมา คูณเข้ากับเลขประจำหลักของมัน
		$total = $total + $count; // ขั้นตอนที่ 3 - เอาผลคูณทั้ง 12 ตัวมา บวกกันทั้งหมด
	}
	$mod = $total % 11; //ขั้นตอนที่ 4 - เอาเลขที่ได้จากขั้นตอนที่ 3 มา mod 11 (หารเอาเศษ)
	$sub = 11 - $mod; //ขั้นตอนที่ 5 - เอา 11 ตั้ง ลบออกด้วย เลขที่ได้จากขั้นตอนที่ 4
	$check_digit = $sub % 10; //ถ้าเกิด ลบแล้วได้ออกมาเป็นเลข 2 หลัก ให้เอาเลขในหลักหน่วยมาเป็น Check Digit
	if($rev[0] == $check_digit)  // ตรวจสอบ ค่าที่ได้ กับ เลขตัวสุดท้ายของ บัตรประจำตัวประชาชน
		return true; /// ถ้า ตรงกัน แสดงว่าถูก
	else
		return false; // ไม่ตรงกันแสดงว่าผิด 
    }
    
    public static function findImportPersonUpdate($sitecode, $select=0, $selectItem=null) {
	ini_set('max_execution_time', 0);
	set_time_limit(0);
	ini_set('memory_limit','256M');
	
	$strWhere = '';
	if ($select){
	    $strItem = join(',', $selectItem);
	    $strWhere = " AND id IN ($strItem)";
	} 
	
	
		
	$dataPerson = OvPerson::find()->where("hospcode=:sitecode $strWhere", [':sitecode'=>$sitecode])->all();//status_import=0 AND 
	
	$import['sum'] = 0;
	$import['sumT'] = 0;
	$import['sumN'] = 0;
	$import['sumF'] = 0;
	
	if($dataPerson){
	    foreach ($dataPerson as $key => $value) {
		
		$import['sum']++;
		
		//check cid
		$checkCid = OvccaFunc::check_citizen($value['cid']);
		if(!$checkCid){
		    $import['sumF']++;
		    continue;
		}
		
		$checkthaiword = trim(OvccaFunc::checkthai($value['fname']));
		if ($checkthaiword  == '') {
		    $import['sumF']++;
		    continue;
		}
		
		$dataCid = OvccaQuery::checkCidupdate($value['cid'], $value['hospcode']);
		if($dataCid){
		    $r = Yii::$app->db->createCommand()->update('tb_data_1', [
			'title' => $value['pname'],
			'name' => $value['fname'],
			'surname' => $value['lname'],
			
			'add1n1' => $value['address'],
			//'tccbot'=>1,
			'add1n2' => $value['village_name'],
			'add1n5'=>substr($value['village_code'], -2),
			'add1n8code'=>substr($value['village_code'], 0, 2),
			'add1n7code'=>substr($value['village_code'], 0, 4),
			'add1n6code'=>substr($value['village_code'], 0, 6),
			'hn' => $value['hn'],
			'hncode' => $value['hn'],
		    ], 'rstat=1 AND cid = :cid AND hsitecode = :sitecode', [':cid'=>$value['cid'], ':sitecode'=>$value['hospcode']])->execute();
		    //ถ้ามีให้อัพเดทข้อมูลสำคัญ
		    $model = OvPerson::findOne($value['id']);
		    $model->ptcode = $dataCid['hptcode'];
		    $model->ptcodefull = $dataCid['hsitecode'].$dataCid['hptcode'];
		    $model->ptid_key = $dataCid['ptid'];
		    $model->ptid = $dataCid['id'];
		    $model->status_import = 1;
		    if($model->save()){
			$import['sumN']++;
		    } else {
			$import['sumF']++;
		    }
		} else {
		    $import['sumF']++;
		}
	    }
	}
	
	return $import;
    }
    public static function findImportPersonOneGetId($sitecode, $cid, $value) {
	$import = false;
	
	$r = Yii::$app->db->createCommand()->update('tb_data_1', [
	    'title' => $value['pname'],
	    'name' => $value['fname'],
	    'surname' => $value['lname'],
	    //'cid' => $value['cid'],
	    'add1n1' => $value['address'],
//	    'tccbot'=>1,
//	    'add1n2' => $value['village_name'],
//	    'add1n5'=>substr($value['village_code'], -2),
//	    'add1n8code'=>substr($value['village_code'], 0, 2),
//	    'add1n7code'=>substr($value['village_code'], 0, 4),
//	    'add1n6code'=>substr($value['village_code'], 0, 6),
	    'hn' => $value['hn'],
//	    'hncode' => $value['hn'],
	], 'cid = :cid AND hsitecode = :sitecode', [':cid'=>$value['cid'], ':sitecode'=>$value['hospcode']])->execute();
	
	if($r>0){
	    $import = true;
	}
	return $import;
    }
    
    public static function findImportPersonOne($sitecode, $cid, $data) {
	ini_set('max_execution_time', 0);
	set_time_limit(0);
	ini_set('memory_limit','256M');
	
	$model = OvPerson::find()->where('hospcode=:hospcode AND person_id=:person_id', [':hospcode'=>$sitecode, ':person_id'=>$data['person_id']])->one();
	if($model){
	    $model->status_import = 0;
	} else {
	    $model = new OvPerson();
	    $model->attributes = $data;
	}
	$model->moo = substr($data['village_code'], -2);
	
	$import = FALSE;
	if($model->save()){
	    $import = true;
	} 
	return $import;
    }
    
    public static function findImportPerson($sitecode, $select=0, $selectItem=null) {
	ini_set('max_execution_time', 0);
	set_time_limit(0);
	ini_set('memory_limit','256M');

	$strWhere = '';
	if ($select){
	    $strItem = join(',', $selectItem);
	    $strWhere = " AND id IN ($strItem)";
	} 
	
	$dataPerson = OvPerson::find()->where("status_import=0 AND hospcode=:sitecode $strWhere", [':sitecode'=>$sitecode])->orderBy('village_code, address, fname, lname')->all();//status_import=0 AND 
	
	$import['sum'] = 0;
	$import['sumT'] = 0;
	$import['sumN'] = 0;
	$import['sumF'] = 0;
	
	if($dataPerson){
	    foreach ($dataPerson as $key => $value) {
		
		$import['sum']++;
		
		//check cid
		$checkCid = OvccaFunc::check_citizen($value['cid']);
		if(!$checkCid){
		    $import['sumF']++;
		    continue;
		}
		
		$checkthaiword = trim(OvccaFunc::checkthai($value['fname']));
		if ($checkthaiword  == '') {
		    $import['sumF']++;
		    continue;
		}
		
		$dataCid = OvccaQuery::checkCid($value['cid'], $value['hospcode']);
		if($dataCid){
		    
		    //ถ้ามีให้อัพเดทข้อมูลสำคัญ
		    $model = OvPerson::findOne($value['id']);
		    $model->ptcode = $dataCid['hptcode'];
		    $model->ptcodefull = $dataCid['hsitecode'].$dataCid['hptcode'];
		    $model->ptid_key = $dataCid['ptid'];
		    $model->ptid = $dataCid['id'];
		    $model->status_import = 1;
		    if($model->save()){
			$import['sumN']++;
		    } else {
			$import['sumF']++;
		    }
		}  else {
		    $dataCidAll = OvccaQuery::checkCidAll($value['cid']);
		    if($dataCidAll){
			//โอนข้อมูล 
			
			/* ห้ามเปลี่ยน
			 * sitecode ที่แรก
			 * ptcode ที่แรก
			 * ptcodefull ที่แรก
			 * ptid_key ที่แรก
			 * 
			 * ค่าที่เปลี่ยน
			 * hsitecode  sitecode ที่บันทึก ณ ปัจจุบัน
			 * hptcode pid ที่บันทึก ณ ปัจจุบัน
			 * id ที่บันทึก ณ ปัจจุบัน
			 * xsourcex ที่บันทึก ณ ปัจจุบัน
			 * user_create ที่บันทึก ณ ปัจจุบัน
			 * create_date ที่บันทึก ณ ปัจจุบัน
			 * rstat = 1
			 * target = ptid_key ที่แรก
			 */
			$id = GenMillisecTime::getMillisecTime();
			$hpcode = $value['hospcode'];
			$user_create = Yii::$app->user->id;
			$create_date = new Expression('NOW()');
			$pid = Yii::$app->db->createCommand("SELECT (hptcode) AS pidcode FROM `tb_data_1` WHERE hsitecode = '$hpcode' ORDER BY id DESC LIMIT 0,1;")->queryOne();
			$pid = str_pad($pid['pidcode']+1, 5, "0", STR_PAD_LEFT);
			$cid = $value['cid'];
			$cid = preg_replace("/[^0-9]/", "", $cid);
			$modelTb1 = Yii::$app->db->createCommand("SELECT * FROM `tb_data_1` WHERE  `cid` LIKE '" . $cid . "' AND id=ptid AND rstat <> 3 AND rstat IS NOT NULL;")->queryOne();
			
			$ptid_key = '';
			$ptcode = '';
			$hptcode = '';
			$hptcodefull = '';
			if($modelTb1){
			    $ptid_key = $modelTb1['ptid'];
			    $ptcode = $modelTb1['ptcode'];
			    $ptcodefull = $modelTb1['ptcodefull'];
			    
			    $modelTb1['id'] = $id;
			    $modelTb1['xsourcex'] = $hpcode;
			    $modelTb1['user_create'] = $user_create;
			    $modelTb1['create_date'] = $create_date;
			    $modelTb1['hsitecode'] = $hpcode;
			    $modelTb1['hptcode'] = $pid;
			    $modelTb1['rstat'] = 1;
			    $modelTb1['target'] = $id;
			    
			    $modelTb1['tccbot'] = 1;
			    $modelTb1['hn'] = $value['hn'];
			    $modelTb1['hncode'] = $value['hn'];
			    
			    $hptcode = $modelTb1['hptcode'];
			    $hptcodefull = $modelTb1['hsitecode'].$modelTb1['hptcode'];
			}
			
			$r = Yii::$app->db->createCommand()->insert('tb_data_1',  $modelTb1)->execute();
			
			if($r){
			    $import['sumT']++;
			    //update. ใบกำกับงาน
			    $model = OvPerson::findOne($value['id']);
			    $model->ptcode = $hptcode;
			    $model->ptcodefull = $hptcodefull;
			    $model->ptid_key = $ptid_key;
			    $model->ptid = $id;
			    $model->status_import = 1;
			    $model->save();
			    //save EMR
			    $modelTarget = new EzformTarget();
			    $modelTarget->ezf_id = '1437377239070461301';
			    $modelTarget->data_id = $id;
			    $modelTarget->target_id = $ptid_key;
			    $modelTarget->user_create = Yii::$app->user->id;
			    $modelTarget->create_date = new Expression('NOW()');
			    $modelTarget->user_update = Yii::$app->user->id;
			    $modelTarget->update_date = new Expression('NOW()');
			    $modelTarget->rstat = 1;
			    $modelTarget->xsourcex = $hpcode;
			    $modelTarget->save();
			} else {
			    $import['sumF']++;
			}
			
		    } else {
			//นำเข้าข้อมูลใหม่
			$id = GenMillisecTime::getMillisecTime();
			$hpcode = $value['hospcode'];
			$user_create = Yii::$app->user->id;
			$create_date = date('Y-m-d H:i:s');
			$pid = Yii::$app->db->createCommand("SELECT hptcode AS pidcode FROM `tb_data_1` WHERE hsitecode = '$hpcode' ORDER BY id DESC LIMIT 0,1;")->queryOne();
			$pid = str_pad($pid['pidcode']+1, 5, "0", STR_PAD_LEFT);
			//$pid = $pid['pidcode'];
			$cid = $value['cid'];
			$ptcodefull = $hpcode.$pid;

			$r = Yii::$app->db->createCommand()->insert('tb_data_1',  [
			    'id' => $id,
			    'ptid' => $id,
			    'xsourcex' => $hpcode,
			    'target' => $id,
			    'user_create' => $user_create,
			    'create_date' => $create_date,
			    'sitecode' => $hpcode,
			    'ptcode' => $pid,
			    'ptcodefull' => $ptcodefull,
			    'hsitecode' => $hpcode,
			    'hptcode' => $pid,
			    'cid' => $cid,
			    'rstat' => 1,
			    //new
			    'tccbot'=>1,
			    'title' => $value['pname'],
			    'name' => $value['fname'],
			    'surname' => $value['lname'],
			    'v2' => $value['birthdate'],
			    'v3' => $value['sex'],
			    'age' => OvccaFunc::getAge($value['birthdate']),
			    'add1n2' => $value['village_name'],
			    'add1n1' => $value['address'],
			    'add1n5'=>substr($value['village_code'], -2),
			    
			    'add1n8code'=>substr($value['village_code'], 0, 2),
			    'add1n7code'=>substr($value['village_code'], 0, 4),
			    'add1n6code'=>substr($value['village_code'], 0, 6),
			    
			    'hn' => $value['hn'],
			    'hncode' => $value['hn'],
			])->execute();
			
			if($r){
			    $import['sumT']++;
			    //update. ใบกำกับงาน
			    $model = OvPerson::findOne($value['id']);
			    $model->ptcode = $pid;
			    $model->ptcodefull = $ptcodefull;
			    $model->ptid_key = $id;
			    $model->ptid = $id;
			    $model->status_import = 1;
			    $model->save();
			    //save EMR
			    $modelTarget = new EzformTarget();
			    $modelTarget->ezf_id = '1437377239070461301';
			    $modelTarget->data_id = $id;
			    $modelTarget->target_id = $id;
			    $modelTarget->user_create = Yii::$app->user->id;
			    $modelTarget->create_date = new Expression('NOW()');
			    $modelTarget->user_update = Yii::$app->user->id;
			    $modelTarget->update_date = new Expression('NOW()');
			    $modelTarget->rstat = 1;
			    $modelTarget->xsourcex = $hpcode;
			    $modelTarget->save();
			    
			} else{
			    $import['sumF']++;
			}
		    }
		}
	    }
	}
	
	return $import;
    }

    public static function findViewsPerson($sitecode, $select=0, $selectItem=null) {
	$strWhere = '';
	if ($select){
	    $strItem = join(',', $selectItem);
	    $strWhere = " AND id IN ($strItem)";
	} 
	
	$dataPerson = OvPerson::find()->where("status_import=0 AND hospcode=:sitecode $strWhere", [':sitecode'=>$sitecode])->all();
	
	$import['sum'] = 0;
	$import['sumT'] = 0;
	$import['sumN'] = 0;
	$import['sumF'] = 0;
	if($dataPerson){
	    foreach ($dataPerson as $key => $value) {
		//check cid
		$checkCid = OvccaFunc::check_citizen($value['cid']);
		if(!$checkCid){
		    $import['sumF']++;
		    continue;
		}
		
		$checkthaiword = trim(OvccaFunc::checkthai($value['fname']));
		if ($checkthaiword  == '') {
		    $import['sumF']++;
		    continue;
		}
		
		$import['sum']++;
		$dataCid = OvccaQuery::checkCid($value['cid'], $value['hospcode']);
		if($dataCid){
		    //ถ้ามีให้อัพเดทข้อมูลสำคัญ
		    $model = OvPerson::findOne($value['id']);
		    $model->ptcode = $dataCid['hptcode'];
		    $model->ptid_key = $dataCid['ptid'];
		    $model->ptid = $dataCid['id'];
		    $model->status_import = 1;
		    if($model->validate()){
			$import['sumN']++;
		    } else {
			$import['sumF']++;
		    }
		}  else {
		    $dataCidAll = OvccaQuery::checkCidAll($value['cid']);
		    if($dataCidAll){
			//โอนข้อมูล 
			
			$cid = $value['cid'];
			$cid = preg_replace("/[^0-9]/", "", $cid);
			$modelTb1 = Yii::$app->db->createCommand("SELECT * FROM `tb_data_1` WHERE  `cid` LIKE '" . $cid . "' AND id=ptid AND rstat <> 3 AND rstat IS NOT NULL;")->queryOne();
			
			$ptid_key = '';
			$ptcode = '';
			
			if($modelTb1){
			    $ptid_key = $modelTb1['ptid'];
			    $ptcode = $modelTb1['ptcode'];
			    
			    $import['sumT']++;
			}else {
			    $import['sumF']++;
			}
			
		    } else {
			//นำเข้าข้อมูลใหม่
			$import['sumT']++;
		    }
		}
	    }
	}
	
	return $import;
    }
    
    public static function decodeField($field, $text) {
	
	return $text;
    }

    public static function getAge($date) {
	//calculate years of age (input string: YYYY-MM-DD)
	$age = new \DateTime($date);
	$now = new \DateTime();
	$interval = $now->diff($age);

	return $interval->y;
    }

    public static function getIframe($url) {
	    $protocol = ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
	    $html = '<iframe src="'.$protocol.getenv('HTTP_HOST').$url.'" width="100%" height="640px" />';
	    return $html;
    }
    
    public static function exists($uri)
    {
	$ch = curl_init($uri);
	curl_setopt($ch, CURLOPT_NOBODY, true);
	curl_exec($ch);
	$mimeType = curl_getinfo($ch, CURLINFO_CONTENT_TYPE);
	curl_close($ch);
	
	return $mimeType;
    }

    public static function getIframeFix($url) {
	    $protocol = ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
	    $furl = $protocol.getenv('HTTP_HOST').$url;
	    $html = '<iframe src="'.$furl.'" width="100%" height="640px" />';
		$html .= Html::a('<li class="fa fa-file-image-o"></li> ดูไฟล์ขนาดเต็ม', $furl, ['class'=>'btn btn-primary btn-sm', 'target' => '_blank', 'style' => 'margin : 10px;']);
		$mimeType = self::exists($furl);
	    
	    if ($mimeType != 'application/pdf') {
		$img = \yii\helpers\Url::to(['/site/view-img', 'img'=>$furl]);
		$html = '<iframe src="'.$img.'" width="100%" height="640px" />';
		$html .= Html::a('<li class="fa fa-file-image-o"></li> ดูไฟล์ขนาดเต็ม', $img, ['class'=>'btn btn-primary btn-sm', 'target' => '_blank', 'style' => 'margin : 10px;']);
	    }
	    
	    return $html;
    }
    
    public static function getIframeFUrl($url) {
	
	$html = '<iframe src="'.$url.'" width="100%" height="640px" />';
	
	$mimeType = self::exists($url);
	
	if ($mimeType != 'application/pdf') {
	    $img = \yii\helpers\Url::to(['/site/view-img', 'img'=>$url]);
	    $html = '<iframe src="'.$img.'" width="100%" height="640px" />';
	}
	    
	    return $html;
    }
    
}
