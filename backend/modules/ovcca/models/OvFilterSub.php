<?php

namespace backend\modules\ovcca\models;

use Yii;

/**
 * This is the model class for table "ov_filter_sub".
 *
 * @property integer $sub_id
 * @property string $sub_name
 * @property integer $filter_id
 * @property integer $sitecode
 * @property integer $created_by
 * @property integer $urine_status
 */
class OvFilterSub extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ov_filter_sub';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sub_name', 'sitecode'], 'required'],
            [['filter_id', 'created_by', 'urine_status'], 'integer'],
            [['sub_name', 'sitecode'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'sub_id' => Yii::t('app', 'ID'),
            'sub_name' => Yii::t('app', 'ใบกำกับงาน'),
            'filter_id' => Yii::t('app', 'รอบการให้บริการ'),
            'sitecode' => Yii::t('app', 'sitecode'),
	    'urine_status' => Yii::t('app', 'ตรวจ Fecal/Urine'),
        ];
    }
}
