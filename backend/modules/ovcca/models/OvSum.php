<?php

namespace backend\modules\ovcca\models;

use Yii;

/**
 * This is the model class for table "ov_sum".
 *
 * @property integer $id
 * @property string $sitecode
 * @property integer $total
 * @property integer $type
 */
class OvSum extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ov_sum';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sitecode', 'total', 'type'], 'required'],
            [['total', 'type'], 'integer'],
            [['sitecode'], 'string', 'max' => 5]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'sitecode' => Yii::t('app', 'Sitecode'),
            'total' => Yii::t('app', 'Total'),
            'type' => Yii::t('app', 'Type'),
        ];
    }
}
