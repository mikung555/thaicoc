<?php

namespace backend\modules\ovcca\models;

use Yii;

/**
 * This is the model class for table "tmp_person".
 *
 * @property integer $id
 * @property string $khet
 * @property string $province
 * @property string $amphur
 * @property string $tambon
 * @property string $hospcode
 * @property string $hospname
 * @property integer $person_id
 * @property string $house_id
 * @property string $address
 * @property string $cid
 * @property string $hn
 * @property string $pname
 * @property string $fname
 * @property string $lname
 * @property string $sex
 * @property string $nationality
 * @property string $education
 * @property string $type_area
 * @property string $religion
 * @property string $birthdate
 * @property string $village_id
 * @property string $village_code
 * @property string $village_name
 * @property string $pttype
 * @property string $pttype_begin_date
 * @property string $pttype_expire_date
 * @property string $pttype_hospmain
 * @property string $pttype_hospsub
 * @property string $marrystatus
 * @property string $death
 * @property string $death_date
 * @property string $ptcode
 * @property string $ptid_key
 * @property string $ptid
 * @property string $moo
 */
class TmpPerson extends \yii\db\ActiveRecord
{
    public $register_id;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tmp_person';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['hospcode', 'person_id', 'cid'], 'required'],
            //[[], 'integer'],
            [['village_name', 'village_code', 'address', 'house_id', 'khet', 'ptcode', 'moo', 'pttype_hospsub', 'pttype_hospmain', 'pttype', 'person_id','ptid_key', 'ptid', 'birthdate', 'pttype_begin_date', 'pttype_expire_date', 'death_date'], 'safe'],
            [[ 'province', 'education','religion', 'village_id'], 'string', 'max' => 4],
            [['amphur'], 'string', 'max' => 4],
            [['tambon'], 'string', 'max' => 6],
            [['hospcode'], 'string', 'max' => 10],
            //[['hospname'], 'string', 'max' => 150],
            
            [['cid', 'cidlink', 'hn', 'pname'], 'string', 'max' => 100],
            [['fname', 'lname'], 'string', 'max' => 200],
            [['sex',  'type_area', 'marrystatus', 'death'], 'string', 'max' => 1],
            [['nationality'], 'string', 'max' => 3],
            [['hospcode', 'person_id'], 'unique', 'targetAttribute' => ['hospcode', 'person_id'], 'message' => 'The combination of Hospcode and Person ID has already been taken.']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'khet' => Yii::t('app', 'Khet'),
            'province' => Yii::t('app', 'Province'),
            'amphur' => Yii::t('app', 'Amphur'),
            'tambon' => Yii::t('app', 'Tambon'),
            'hospcode' => Yii::t('app', 'Hospcode'),
            'hospname' => Yii::t('app', 'Hospname'),
            'person_id' => Yii::t('app', 'Person ID'),
            'house_id' => Yii::t('app', 'House ID'),
            'address' => Yii::t('app', 'Address'),
            'cid' => Yii::t('app', 'Cid'),
            'hn' => Yii::t('app', 'Hn'),
            'pname' => Yii::t('app', 'Pname'),
            'fname' => Yii::t('app', 'Fname'),
            'lname' => Yii::t('app', 'Lname'),
            'sex' => Yii::t('app', 'Sex'),
            'nationality' => Yii::t('app', 'Nationality'),
            'education' => Yii::t('app', 'Education'),
            'type_area' => Yii::t('app', 'Type Area'),
            'religion' => Yii::t('app', 'Religion'),
            'birthdate' => Yii::t('app', 'Birthdate'),
            'village_id' => Yii::t('app', 'Village ID'),
            'village_code' => Yii::t('app', 'Village Code'),
            'village_name' => Yii::t('app', 'Village Name'),
            'pttype' => Yii::t('app', 'Pttype'),
            'pttype_begin_date' => Yii::t('app', 'Pttype Begin Date'),
            'pttype_expire_date' => Yii::t('app', 'Pttype Expire Date'),
            'pttype_hospmain' => Yii::t('app', 'Pttype Hospmain'),
            'pttype_hospsub' => Yii::t('app', 'Pttype Hospsub'),
            'marrystatus' => Yii::t('app', 'Marrystatus'),
            'death' => Yii::t('app', 'Death'),
            'death_date' => Yii::t('app', 'Death Date'),
	    'ptcode' => Yii::t('app', 'PTcode'),
	    'ptid_key' => Yii::t('app', 'ptid_key'),
	    'ptid' => Yii::t('app', 'ptid'),
	    'moo' => Yii::t('app', 'หมู่ที่'),
	    'cidlink' => Yii::t('app', 'cidlink'),
        ];
    }
}
