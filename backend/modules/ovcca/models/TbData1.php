<?php

namespace backend\modules\ovcca\models;

use Yii;

/**
 * This is the model class for table "tb_data_1".
 *
 * @property integer $id
 * @property integer $usmobile
 * @property string $sitecode
 * @property string $ptcode
 * @property string $ptcodefull
 * @property integer $ptid_link
 * @property integer $ptid
 * @property string $reccheck
 * @property string $cid
 * @property integer $cid_check
 * @property string $title
 * @property string $name
 * @property string $surname
 * @property integer $uidadd
 * @property string $dadd
 * @property integer $uidedit
 * @property string $dedit
 * @property string $v2
 * @property string $age
 * @property string $v3
 * @property string $mobile
 * @property string $homephone
 * @property string $telcontact1
 * @property string $telcontact2
 * @property string $telcontact3
 * @property string $add1n1
 * @property string $add1n2
 * @property string $add1n3
 * @property string $add1n4
 * @property string $add1n5
 * @property string $add1n6code
 * @property string $add1n7code
 * @property string $add1n8code
 * @property string $add1n9
 * @property string $hospitalcurrent
 * @property string $hn
 * @property string $selfenroll
 * @property string $selfenrolldate
 * @property string $vconsent
 * @property string $vconsentdate
 * @property string $vconsentdatedb
 * @property integer $sys_assigncheck
 * @property string $dlastcheck
 * @property string $confirmdate
 * @property integer $confirm
 * @property string $venroll
 * @property string $venrolldate
 * @property string $venrolldatedb
 * @property integer $pay
 * @property string $paytime
 * @property integer $recieve
 * @property string $recievedate
 * @property string $bankdate
 * @property string $sys_dateoficf
 * @property string $sys_dateoficfdb
 * @property string $sys_ecoficf
 * @property string $sys_ecoficfdb
 * @property string $rstat
 * @property string $addr
 * @property string $lat
 * @property string $lng
 * @property string $geocode
 * @property integer $pidcheck
 * @property integer $agecheck
 * @property string $sitezone
 * @property string $siteprov
 * @property string $siteamp
 * @property string $sitetmb
 * @property string $target
 * @property string $xsourcex
 * @property integer $user_create
 * @property string $create_date
 * @property integer $user_update
 * @property string $update_date
 * @property string $value1
 * @property string $value2
 * @property string $value3
 * @property string $edattype
 * @property string $icf_upload1
 * @property string $icf_upload2
 * @property string $icf_upload3
 * @property string $error
 * @property string $hncode
 * @property string $hsitecode
 * @property string $hptcode
 * @property string $cohorttype_chartreview
 */
class TbData1 extends \yii\db\ActiveRecord
{
    public $tmp_id;
    public $register_id;
    public $cca01_id;
    public $cca01_result;
    public $ov01k_id;
    public $ov01k_result;
    public $ov01k_date;
    public $ov01p_id;
    public $ov01p_result;
    public $ov01p_date;
    public $ov01f_id;
    public $ov01f_result;
    public $ov01f_date;
    public $ov01u_id;
    public $ov01u_result;
    public $ov01u_date;
    public $ov02_id;
    public $ov02_result;
    public $ov03_id;
    public $ov03_result;
    public $cca02_id;
    public $cca02_result;
    public $ov01_status;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tb_data_1';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'addr', 'geocode', 'xsourcex', 'value1', 'value2', 'value3', 'edattype'], 'required'],
            [['id', 'usmobile', 'ptid_link', 'ptid', 'cid_check', 'uidadd', 'uidedit', 'sys_assigncheck', 'confirm', 'pay', 'recieve', 'pidcheck', 'agecheck', 'user_create', 'user_update'], 'integer'],
            [['dadd', 'dedit', 'v2', 'selfenrolldate', 'vconsentdatedb', 'dlastcheck', 'confirmdate', 'venrolldatedb', 'paytime', 'recievedate', 'bankdate', 'sys_dateoficfdb', 'sys_ecoficfdb', 'create_date', 'update_date'], 'safe'],
            [['geocode', 'error'], 'string'],
            [['sitecode', 'ptcode', 'ptcodefull', 'age', 'vconsentdate', 'venrolldate', 'rstat', 'hsitecode', 'hptcode', 'cohorttype_chartreview'], 'string', 'max' => 10],
            [['reccheck', 'v3', 'selfenroll', 'vconsent', 'venroll', 'sitezone', 'siteprov'], 'string', 'max' => 2],
            [['cid', 'sys_dateoficf', 'sys_ecoficf', 'xsourcex', 'value1', 'value2', 'value3'], 'string', 'max' => 20],
            [['title', 'add1n1', 'add1n2', 'add1n3', 'add1n4', 'add1n5', 'add1n9', 'hncode'], 'string', 'max' => 50],
            [['name', 'surname', 'mobile', 'homephone', 'telcontact1', 'telcontact2', 'telcontact3', 'hospitalcurrent', 'hn', 'addr', 'target'], 'string', 'max' => 100],
            [['add1n6code', 'add1n7code', 'add1n8code', 'lat', 'lng'], 'string', 'max' => 30],
            [['siteamp'], 'string', 'max' => 4],
            [['sitetmb'], 'string', 'max' => 6],
            [['edattype'], 'string', 'max' => 5],
            [['icf_upload1', 'icf_upload2', 'icf_upload3'], 'string', 'max' => 150]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ptid'),
            'usmobile' => Yii::t('app', 'Usmobile'),
            'sitecode' => Yii::t('app', 'Sitecode'),
            'ptcode' => Yii::t('app', 'Ptcode'),
            'ptcodefull' => Yii::t('app', 'Ptcodefull'),
            'ptid_link' => Yii::t('app', 'Ptid Link'),
            'ptid' => Yii::t('app', 'Ptid'),
            'reccheck' => Yii::t('app', 'Reccheck'),
            'cid' => Yii::t('app', 'CID 13 digit'),
            'cid_check' => Yii::t('app', 'Cid Check'),
            'title' => Yii::t('app', 'Title'),
            'name' => Yii::t('app', 'Name'),
            'surname' => Yii::t('app', 'Surname'),
            'uidadd' => Yii::t('app', 'Uidadd'),
            'dadd' => Yii::t('app', 'Dadd'),
            'uidedit' => Yii::t('app', 'Uidedit'),
            'dedit' => Yii::t('app', 'Dedit'),
            'v2' => Yii::t('app', 'V2'),
            'age' => Yii::t('app', 'Age'),
            'v3' => Yii::t('app', 'sex'),
            'mobile' => Yii::t('app', 'Mobile'),
            'homephone' => Yii::t('app', 'Homephone'),
            'telcontact1' => Yii::t('app', 'Telcontact1'),
            'telcontact2' => Yii::t('app', 'Telcontact2'),
            'telcontact3' => Yii::t('app', 'Primary care'),
            'add1n1' => Yii::t('app', 'Add1n1'),
            'add1n2' => Yii::t('app', 'Add1n2'),
            'add1n3' => Yii::t('app', 'Add1n3'),
            'add1n4' => Yii::t('app', 'Add1n4'),
            'add1n5' => Yii::t('app', 'Add1n5'),
            'add1n6code' => Yii::t('app', 'Add1n6code'),
            'add1n7code' => Yii::t('app', 'Add1n7code'),
            'add1n8code' => Yii::t('app', 'Add1n8code'),
            'add1n9' => Yii::t('app', 'Add1n9'),
            'hospitalcurrent' => Yii::t('app', 'Hospitalcurrent'),
            'hn' => Yii::t('app', 'Hn'),
            'selfenroll' => Yii::t('app', 'Selfenroll'),
            'selfenrolldate' => Yii::t('app', 'Selfenrolldate'),
            'vconsent' => Yii::t('app', 'Vconsent'),
            'vconsentdate' => Yii::t('app', 'Vconsentdate'),
            'vconsentdatedb' => Yii::t('app', 'Vconsentdatedb'),
            'sys_assigncheck' => Yii::t('app', 'Sys Assigncheck'),
            'dlastcheck' => Yii::t('app', 'Dlastcheck'),
            'confirmdate' => Yii::t('app', 'Confirmdate'),
            'confirm' => Yii::t('app', 'Confirm'),
            'venroll' => Yii::t('app', 'Venroll'),
            'venrolldate' => Yii::t('app', 'Venrolldate'),
            'venrolldatedb' => Yii::t('app', 'Venrolldatedb'),
            'pay' => Yii::t('app', 'Pay'),
            'paytime' => Yii::t('app', 'Paytime'),
            'recieve' => Yii::t('app', 'Recieve'),
            'recievedate' => Yii::t('app', 'Recievedate'),
            'bankdate' => Yii::t('app', 'Bankdate'),
            'sys_dateoficf' => Yii::t('app', '�ѹ����红�����'),
            'sys_dateoficfdb' => Yii::t('app', 'Sys Dateoficfdb'),
            'sys_ecoficf' => Yii::t('app', 'Sys Ecoficf'),
            'sys_ecoficfdb' => Yii::t('app', 'Sys Ecoficfdb'),
            'rstat' => Yii::t('app', 'Rstat'),
            'addr' => Yii::t('app', 'Addr'),
            'lat' => Yii::t('app', 'Lat'),
            'lng' => Yii::t('app', 'Lng'),
            'geocode' => Yii::t('app', 'Geocode'),
            'pidcheck' => Yii::t('app', 'Pidcheck'),
            'agecheck' => Yii::t('app', 'Agecheck'),
            'sitezone' => Yii::t('app', 'Sitezone'),
            'siteprov' => Yii::t('app', 'Siteprov'),
            'siteamp' => Yii::t('app', 'Siteamp'),
            'sitetmb' => Yii::t('app', 'Sitetmb'),
            'target' => Yii::t('app', 'Target'),
            'xsourcex' => Yii::t('app', 'Xsourcex'),
            'user_create' => Yii::t('app', 'User Create'),
            'create_date' => Yii::t('app', 'Create Date'),
            'user_update' => Yii::t('app', 'User Update'),
            'update_date' => Yii::t('app', 'Update Date'),
            'value1' => Yii::t('app', 'Value1'),
            'value2' => Yii::t('app', 'Value2'),
            'value3' => Yii::t('app', 'Value3'),
            'edattype' => Yii::t('app', 'Edattype'),
            'icf_upload1' => Yii::t('app', 'Icf Upload1'),
            'icf_upload2' => Yii::t('app', 'Icf Upload2'),
            'icf_upload3' => Yii::t('app', 'Icf Upload3'),
            'error' => Yii::t('app', 'Error'),
            'hncode' => Yii::t('app', 'Hncode'),
            'hsitecode' => Yii::t('app', 'Hsitecode'),
            'hptcode' => Yii::t('app', 'Hptcode'),
            'cohorttype_chartreview' => Yii::t('app', 'Cohorttype Chartreview'),
        ];
    }
}
