<?php

namespace backend\modules\ovcca\models;

use Yii;

/**
 * This is the model class for table "ov_sub_list".
 *
 * @property integer $list_id
 * @property integer $sub_id
 * @property integer $filter_id
 * @property integer $person_id
 */
class OvSubList extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ov_sub_list';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sub_id', 'filter_id', 'person_id'], 'required'],
            [['sub_id', 'filter_id', 'person_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'list_id' => Yii::t('app', 'List ID'),
            'sub_id' => Yii::t('app', 'Sub ID'),
            'filter_id' => Yii::t('app', 'Filter ID'),
            'person_id' => Yii::t('app', 'Person ID'),
        ];
    }
}
