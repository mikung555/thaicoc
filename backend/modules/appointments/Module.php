<?php
/*
 * Module Follow up.
 * Developed by Mark.
 * Date : 2016-04-01
*/

namespace backend\modules\appointments;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\modules\appointments\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
