<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace backend\modules\appointments\classes;

use Yii;

/**
 * Description of Address
 *
 * @author chaiwat
 */
class Address {
    //put your code here
    protected $tambon;
    protected $amphur;
    protected $province;
    
    public static function accessObjectArray(){
        return 0;
    }
    
    public static function getAllTDepartmentFromHCODE($hcode){
        if( strlen($hcode)>0 ){
            $sql = "select * ";
            $sql.= "from all_hospital_thai where hcode=:hcode limit 1 ";
            $out = Yii::$app->db->createCommand($sql, [':hcode'=>$hcode])->queryOne();
        }
        return $out;
    }
    
    public static function getAllTambonFromHCODE($hcode){
        if( strlen($hcode)>0 ){
            $depart = self::getAllTDepartmentFromHCODE($hcode);
            if(strlen($depart['provincecode'])>0){
                $out['dep']=$depart;
                $out['tambon']= self::getAllTambonFromProvince($depart['provincecode']);
            }
        }
        return $out;
    }
    
    public static function getAllTambonFromProvince($provincecode){
        if( strlen($provincecode)>0 ){
            $sql = "select distinct concat(provincecode,amphurcode,tamboncode) as add1n6code, tambon, amphur, province ";
            $sql.= "from all_hospital_thai where provincecode=:provincecode ";
            $set = Yii::$app->db->createCommand($sql, [':provincecode'=>$provincecode])->queryAll();
            if(count($set)>0){
                foreach($set as $value){
                    $out[$value['add1n6code']]=$value;
                }
            }
        }
        return $out;
    }
    //other member functions
}
