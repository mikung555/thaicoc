<?php
/*
 * Module Follow up.
 * Developed by Mark.
 * Date : 2016-04-01
*/

namespace backend\modules\appointments\controllers;


use Yii;
use yii\helpers\VarDumper;
use yii\web\Controller;
use DateTime;
use backend\modules\appointments\classes\Address;

class DefaultController extends Controller
{
    public function actionIndex()
    {
        $siteCode = Yii::$app->user->identity->userProfile->sitecode;
        $qrySiteDetail = $this->getSiteDetail($siteCode);
        $strReportAppointment = $this->getReportAppointment($siteCode,2,2);

        return $this->render('index', [
            'mysitecode'=>$siteCode,
            'qrySiteDetails'=>$qrySiteDetail,
            'strReportAppointment'=>$strReportAppointment,
        ]);
    }

    public function actionGetReport(){
        $sitecode = isset($_GET['sitecode'])?$_GET['sitecode']:"";
        $numOfMonthBefore = isset($_GET['numOfMonthBefore'])?$_GET['numOfMonthBefore']:"2";
        $numOfMonthAfter = isset($_GET['numOfMonthAfter'])?$_GET['numOfMonthAfter']:"2";
        echo $this->getReportAppointment($sitecode,$numOfMonthBefore, $numOfMonthAfter);
    }

    public function actionFilterAppointment(){
        $sitecode = isset($_GET['sitecode'])?$_GET['sitecode']:"";
        $filterAppointmentMonth = isset($_GET['selected'])?$_GET['selected']:"";
        $startDate = isset($_GET['startDate'])?$_GET['startDate']:"";
        $endDate = isset($_GET['endDate'])?$_GET['endDate']:"";
        $filterAppointmenthMonthArray = explode(',',$filterAppointmentMonth);
        $sizeFilterAppMonth = sizeof($filterAppointmenthMonthArray);

        $strSqlFilterSuspected = '';
        if(in_array(3,$filterAppointmenthMonthArray)){
            if($sizeFilterAppMonth>1){
                $strSqlFilterSuspected .= "OR ";
            }
            $strSqlFilterSuspected .= "`tb_data_3`.`f2v6a3b1` = 1 ";
            array_pop($filterAppointmenthMonthArray);
            $sizeFilterAppMonth-=1;
        }

        $srtsqlFilter = '';
        if($sizeFilterAppMonth>1){
            $srtsqlFilter .= "AND ( ".
                "(`tb_data_3`.`f2v6` = ".$filterAppointmenthMonthArray[0]." ".
                "OR `tb_data_3`.`f2v6` = ".$filterAppointmenthMonthArray[1].") ".
                "$strSqlFilterSuspected )";
        }else if($sizeFilterAppMonth==1){
            $srtsqlFilter .= "AND ((`tb_data_3`.`f2v6` = '".$filterAppointmenthMonthArray[0]."') $strSqlFilterSuspected) ";
        }else{
            $srtsqlFilter .= "AND ( $strSqlFilterSuspected) ";
        }

        if( $startDate!='' && $endDate!='' ){
            $srtsqlFilter .= "AND `tb_data_3`.`f2v1` BETWEEN '".(str_replace(" ","",$startDate))."' AND '".(str_replace(" ","",$endDate))."' ";
        }

        $strSqlGetReportAppointment = "SELECT ".
                "`tb_data_1`.`ptcodefull`, ".
                "`tb_data_1`.`sitecode`, ".
                "`tb_data_1`.`ptcode`, ".
                "`tb_data_1`.`name`, ".
                "`tb_data_1`.`surname`, ".
                "`tb_data_1`.`add1n1`, ".
                "`tb_data_1`.`add1n5`, ".
                "`tb_data_1`.`add1n6code`, ".
                "`tb_data_3`.`id`, ".
                "`tb_data_3`.`ptid`, ".
                "`tb_data_3`.`target`, ".
                "`tb_data_3`.`rstat`, ".
                "`tb_data_3`.`hsitecode`, ".
                "`tb_data_3`.`f2v1`, ".
                "`tb_data_3`.`f2v6`, ".
                "`tb_data_3`.`f2v6a3b1`, ".
                "`tb_data_3`.`f2doctorcode`, ".
                "CASE WHEN `tb_data_3`.`f2v6` = '1' THEN 12 ".
                    "WHEN `tb_data_3`.`f2v6` = '2' THEN 6 ".
                    "ELSE 0 ".
                "END AS `monthAppointment`, ".
                "DATE_ADD(`tb_data_3`.`f2v1`,INTERVAL ( ".
                    "CASE WHEN `tb_data_3`.`f2v6` = '1' THEN 12 ".
                        "WHEN `tb_data_3`.`f2v6` = '2' THEN 6 ".
                        "ELSE 0 ".
                    "END ".
                ") MONTH) as `dateAppointment`, ".
                "TIMESTAMPDIFF(MONTH, ".
                    "DATE_ADD( `tb_data_3`.`f2v1`, INTERVAL ( ".
                        "CASE WHEN `tb_data_3`.`f2v6` = '1' THEN 12 ".
                            "WHEN `tb_data_3`.`f2v6` = '2' THEN 6 ".
                            "ELSE 0 ".
                        "END ".
                    ") MONTH), ".
                    "NOW() ".
                ") as `monthLate` ".
            "FROM `tb_data_3`, `tb_data_1` ".
            "WHERE `tb_data_3`.`hsitecode` = '".$sitecode."' ".
            "AND `tb_data_3`.`f2v6` IS NOT NULL ".
            $srtsqlFilter.
            "AND `tb_data_3`.`rstat` NOT LIKE '%0%' ".
            "AND `tb_data_3`.`rstat` NOT LIKE '%3%' ".
            "AND `tb_data_1`.`id` = `tb_data_3`.`target` ".
            "ORDER BY `dateAppointment` ASC";

        $qryGetReportAppointment = Yii::$app->db->createCommand($strSqlGetReportAppointment)->queryAll();

        $i=1;
        $str = '';
//        $str .= '<tr><td colspan="7">'.$strSqlGetReportAppointment.'</td></tr>';
        $address = new Address();
        $tambon = $address->getAllTambonFromHCODE($sitecode);
        // dispaly record
        foreach ($qryGetReportAppointment as $key => $value){
            $dateVisit = new DateTime($value['f2v1']);
            $dateAppointment = new DateTime($value['dateAppointment']);
            $strAddr = '';
            if( strlen($value['add1n5'])>0 ){
                $strAddr = 'หมู่ที่ '.$value['add1n5'];
            }
            if( strlen($tambon[$value['add1n6code']])>0 ){
                $strAddr.= ' ต.'.$tambon[$value['add1n6code']];
            }
            /*
                '<td>'.$tambon['tambon'][$value['add1n6code']]['tambon'].'</td>'.
                '<td>'.$tambon['tambon'][$value['add1n6code']]['amphur'].'</td>'.
                '<td>'.$tambon['tambon'][$value['add1n6code']]['province'].'</td>'.
             * 
             */
            $str .= '<tr>'.
                '<td>'.$i.'</td>'.
                '<td>'.$value['sitecode'].' '.$value['ptcode'].'</td>'.
                '<td>'.$value['add1n5'].'</td>'.
                '<td>'.$value['name'].' '.$value['surname'].'</td>'.
                '<td>'.date_format($dateVisit,"d/m/Y").'</td>'.
                '<td>'.$value['monthAppointment'].' เดือน '.( $value['f2v6a3b1']=='1'?'(Suspected CCA)':'' ).'</td>'.
                '<td>'.( ($value['monthLate']>0)?$value['monthLate']:"0" ).' เดือน</td>'.
                '<td>'.date_format($dateAppointment,"d/m/Y").'</td>'.
                '</tr>';
            $i+=1;
        }
        echo $str;
    }

    private function getSiteDetail($siteCode){
        $sqlSite = "SELECT `hcode`,`name` FROM all_hospital_thai WHERE hcode='".($siteCode)."' ";
        $qrySiteDetail = Yii::$app->db->createCommand($sqlSite)->queryOne();
        return $qrySiteDetail;
    }

    private function getReportAppointment($sitecode,$numOfMonthBefore = 2, $numOfMonthAfter = 2){
        $strSqlGetReportAppointment = "SELECT ".
                "`tb_data_1`.`ptcodefull`, ".
                "`tb_data_1`.`sitecode`, ".
                "`tb_data_1`.`ptcode`, ".
                "`tb_data_1`.`name`, ".
                "`tb_data_1`.`surname`, ".
                "`tb_data_1`.`add1n1`, ".
                "`tb_data_1`.`add1n5`, ".
                "`tb_data_1`.`add1n6code`, ".
                "`tb_data_3`.`id`, ".
                "`tb_data_3`.`ptid`, ".
                "`tb_data_3`.`target`, ".
                "`tb_data_3`.`rstat`, ".
                "`tb_data_3`.`hsitecode`, ".
                "`tb_data_3`.`f2v1`, ".
                "`tb_data_3`.`f2v6`, ".
                "`tb_data_3`.`f2v6a3b1`, ".
                "`tb_data_3`.`f2doctorcode`, ".
                "DATE_SUB(NOW(),INTERVAL ".$numOfMonthBefore." MONTH) AS `DateBefore`, ".
                "DATE_ADD(NOW(),INTERVAL ".$numOfMonthAfter." MONTH) As `DateAfter`, ".
                "CASE WHEN `tb_data_3`.`f2v6` = '1' THEN 12 ".
                    "WHEN `tb_data_3`.`f2v6` = '2' THEN 6 ".
                    "ELSE 0 ".
                "END AS `monthAppointment`, ".
                "DATE_ADD(`tb_data_3`.`f2v1`,INTERVAL ( ".
                "CASE WHEN `tb_data_3`.`f2v6` = '1' THEN 12 ".
                    "WHEN `tb_data_3`.`f2v6` = '2' THEN 6 ".
                    "ELSE 0 ".
                    "END ".
                ") MONTH) as `dateAppointment`, ".
                "TIMESTAMPDIFF(MONTH, ".
                    "DATE_ADD( `tb_data_3`.`f2v1`, INTERVAL ( ".
                        "CASE WHEN `tb_data_3`.`f2v6` = '1' THEN 12 ".
                            "WHEN `tb_data_3`.`f2v6` = '2' THEN 6 ".
                            "ELSE 0 ".
                        "END ".
                    ") MONTH), ".
                    "NOW() ".
                ") as `monthLate` ".
            "FROM `tb_data_3`, `tb_data_1` ".
            "WHERE `tb_data_3`.`hsitecode` = '".$sitecode."' ".
            "AND `tb_data_3`.`f2v6` IS NOT NULL ".
            "AND (`tb_data_3`.`f2v6` != '' OR `tb_data_3`.`f2v6a3b1` = 1) ".
            "AND DATE_ADD(`tb_data_3`.`f2v1`,INTERVAL ( ".
	            "CASE WHEN `tb_data_3`.`f2v6` = '1' THEN 12 ".
		            "WHEN `tb_data_3`.`f2v6` = '2' THEN 6 ".
                    "ELSE 0 ".
            	"END ".
            ") MONTH) BETWEEN ".
                "DATE_SUB(NOW(),INTERVAL ".$numOfMonthBefore." MONTH) ".
                "AND ".
                "DATE_ADD(NOW(),INTERVAL ".$numOfMonthAfter." MONTH) ".
            "AND `tb_data_3`.`rstat` NOT LIKE '%0%' ".
            "AND `tb_data_3`.`rstat` NOT LIKE '%3%' ".
            "AND `tb_data_1`.`id` = `tb_data_3`.`target` ".
            "ORDER BY `dateAppointment` ASC";
        $qryGetReportAppointment = Yii::$app->db->createCommand($strSqlGetReportAppointment)->queryAll();

        $i=1;
        $str = '';
//        $str .= '<tr><td colspan="7">'.$strSqlGetReportAppointment.'</td></tr>';
        $address = new Address();
        $tambon = $address->getAllTambonFromHCODE($sitecode);
        // dispaly record
        foreach ($qryGetReportAppointment as $key => $value){
            $dateVisit = new DateTime($value['f2v1']);
            $dateAppointment = new DateTime($value['dateAppointment']);
            /*
                '<td>'.$tambon['tambon'][$value['add1n6code']]['tambon'].'</td>'.
                '<td>'.$tambon['tambon'][$value['add1n6code']]['amphur'].'</td>'.
                '<td>'.$tambon['tambon'][$value['add1n6code']]['province'].'</td>'.
             * 
             */
            $str .= '<tr>'.
                '<td>'.$i.'</td>'.
                '<td>'.$value['sitecode'].' '.$value['ptcode'].'</td>'.
                '<td>'.$value['add1n5'].'</td>'.
                '<td>'.$value['name'].' '.$value['surname'].'</td>'.
                '<td>'.date_format($dateVisit,"d/m/Y").'</td>'.
                '<td>'.$value['monthAppointment'].' เดือน '.( $value['f2v6a3b1']=='1'?'(Suspected CCA)':'' ).'</td>'.
                '<td>'.( ($value['monthLate']>0)?$value['monthLate']:"0" ).' เดือน</td>'.
                '<td>'.date_format($dateAppointment,"d/m/Y").'</td>'.
                '</tr>';
            $i+=1;
        }
        
        return $str;
    }
}
