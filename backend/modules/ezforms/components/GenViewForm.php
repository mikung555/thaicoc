<?php

namespace backend\modules\ezforms\components;

use Yii;
use common\lib\codeerror\helpers\GenMillisecTime;
use backend\modules\ezforms\models\EzformFields;
use kartik\widgets\DateTimePicker;
use kartik\widgets\Select2;
use kartik\widgets\FileInput;
use yii\widgets\MaskedInput;
use yii\web\JsExpression;
use yii\helpers\Url;
use backend\modules\component\models\EzformComponent;
use backend\modules\ezforms\models\Ezform;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;


class GenViewForm
{
    public static function getTypeEform($modelfield,$modelform,$dataid, $target){
        if($modelfield->ezf_field_type == 18){
            //เก็บ temp ไว้
            $ezf_field_order = $modelfield->ezf_field_order;
            $ezf_field_id = $modelfield->ezf_field_id;
            //
            $modelfield = EzformFields::find()->where(['ezf_field_id' => $modelfield->ezf_field_ref_field])->one();
            $modelfield->ezf_field_order = $ezf_field_order;
            $modelfield->ezf_field_ref_field = $ezf_field_id; //เก็บ id field ต้นทางไปด้วย
        }
        if($modelfield->ezf_field_type==1){
           $modelform = Ezform::find()->where(['ezf_id' => $modelfield->ezf_id])->one();
           if($modelfield->ezf_field_ref_field){
                $sql = "SELECT id FROM `".$modelform->ezf_table."` WHERE target = '$target' AND rstat = '2' ORDER BY create_date DESC LIMIT 1;";
                $ezf_tb = Yii::$app->db->createCommand($sql)->queryOne();
                $dataid = $ezf_tb['id'];
                $modelfield->ezf_field_default = 'disabled';
           }
           return self::typeText($modelfield,$modelform,$dataid);
        }
        if($modelfield->ezf_field_type==2){
           $modelform = Ezform::find()->where(['ezf_id' => $modelfield->ezf_id])->one();
           return self::typeHeading($modelfield,$modelform,$dataid);
        }
        if($modelfield->ezf_field_type==3){
           $modelform = Ezform::find()->where(['ezf_id' => $modelfield->ezf_id])->one();
           if($modelfield->ezf_field_ref_field){
                $sql = "SELECT id FROM `".$modelform->ezf_table."` WHERE target = '$target' AND rstat = '2' ORDER BY create_date DESC LIMIT 1;";
                $ezf_tb = Yii::$app->db->createCommand($sql)->queryOne();
                $dataid = $ezf_tb['id'];
                $modelfield->ezf_field_default = 'disabled';
           }
           return self::typeTextArea($modelfield,$modelform,$dataid);
        }
         if($modelfield->ezf_field_type==4){
           $modelform = Ezform::find()->where(['ezf_id' => $modelfield->ezf_id])->one();
           if($modelfield->ezf_field_ref_field){
                $sql = "SELECT id FROM `".$modelform->ezf_table."` WHERE target = '$target' AND rstat = '2' ORDER BY create_date DESC LIMIT 1;";
                $ezf_tb = Yii::$app->db->createCommand($sql)->queryOne();
                $dataid = $ezf_tb['id'];
                $modelfield->ezf_field_default = 'disabled';
           }
           return self::typeRadioOnload($modelfield,$modelform,$dataid);
        }
        if($modelfield->ezf_field_type==5){
           $modelform = Ezform::find()->where(['ezf_id' => $modelfield->ezf_id])->one();
           if($modelfield->ezf_field_ref_field){
                $sql = "SELECT id FROM `".$modelform->ezf_table."` WHERE target = '$target' AND rstat = '2' ORDER BY create_date DESC LIMIT 1;";
                $ezf_tb = Yii::$app->db->createCommand($sql)->queryOne();
                $dataid = $ezf_tb['id'];
                $modelfield->ezf_field_default = 'disabled';
           }
           return self::typeCheckboxOnload($modelfield,$modelform,$dataid);
        }
        if($modelfield->ezf_field_type==6){
           $modelform = Ezform::find()->where(['ezf_id' => $modelfield->ezf_id])->one();
           if($modelfield->ezf_field_ref_field){
                $sql = "SELECT id FROM `".$modelform->ezf_table."` WHERE target = '$target' AND rstat = '2' ORDER BY create_date DESC LIMIT 1;";
                $ezf_tb = Yii::$app->db->createCommand($sql)->queryOne();
                $dataid = $ezf_tb['id'];
                $modelfield->ezf_field_default = 'disabled';
           }
           return self::typeSelectOnload($modelfield,$modelform,$dataid);
        }
        if($modelfield->ezf_field_type==7){
           $modelform = Ezform::find()->where(['ezf_id' => $modelfield->ezf_id])->one();
           if($modelfield->ezf_field_ref_field){
                $sql = "SELECT id FROM `".$modelform->ezf_table."` WHERE target = '$target' AND rstat = '2' ORDER BY create_date DESC LIMIT 1;";
                $ezf_tb = Yii::$app->db->createCommand($sql)->queryOne();
                $dataid = $ezf_tb['id'];
                $modelfield->ezf_field_default = 'disabled';
           }
           return self::typeDate($modelfield,$modelform,$dataid);
        }
        if($modelfield->ezf_field_type==8){
           $modelform = Ezform::find()->where(['ezf_id' => $modelfield->ezf_id])->one();
           if($modelfield->ezf_field_ref_field){
                $sql = "SELECT id FROM `".$modelform->ezf_table."` WHERE target = '$target' AND rstat = '2' ORDER BY create_date DESC LIMIT 1;";
                $ezf_tb = Yii::$app->db->createCommand($sql)->queryOne();
                $dataid = $ezf_tb['id'];
                $modelfield->ezf_field_default = 'disabled';
           }
           return self::typeTime($modelfield,$modelform,$dataid);
        }
        if($modelfield->ezf_field_type==9){
           $modelform = Ezform::find()->where(['ezf_id' => $modelfield->ezf_id])->one();
           if($modelfield->ezf_field_ref_field){
                $sql = "SELECT id FROM `".$modelform->ezf_table."` WHERE target = '$target' AND rstat = '2' ORDER BY create_date DESC LIMIT 1;";
                $ezf_tb = Yii::$app->db->createCommand($sql)->queryOne();
                $dataid = $ezf_tb['id'];
                $modelfield->ezf_field_default = 'disabled';
           }
           return self::typeDateTime($modelfield,$modelform,$dataid);
        }
        if ($modelfield->ezf_field_type == 10) {
           $modelform = Ezform::find()->where(['ezf_id' => $modelfield->ezf_id])->one();
           if($modelfield->ezf_field_ref_field){
                $sql = "SELECT id FROM `".$modelform->ezf_table."` WHERE target = '$target' AND rstat = '2' ORDER BY create_date DESC LIMIT 1;";
                $ezf_tb = Yii::$app->db->createCommand($sql)->queryOne();
                $dataid = $ezf_tb['id'];
                $modelfield->ezf_field_default = 'disabled';
           }
           return self::typeComponent($modelfield,$modelform,$dataid);
        }
        if ($modelfield->ezf_field_type == 11) {
           $modelform = Ezform::find()->where(['ezf_id' => $modelfield->ezf_id])->one();
           if($modelfield->ezf_field_ref_field){
                $sql = "SELECT id FROM `".$modelform->ezf_table."` WHERE target = '$target' AND rstat = '2' ORDER BY create_date DESC LIMIT 1;";
                $ezf_tb = Yii::$app->db->createCommand($sql)->queryOne();
                $dataid = $ezf_tb['id'];
                $modelfield->ezf_field_default = 'disabled';
           }
           return self::typeSnomed($modelfield,$modelform,$dataid);
        }
        if ($modelfield->ezf_field_type == 12) {
           $modelform = Ezform::find()->where(['ezf_id' => $modelfield->ezf_id])->one();
           if($modelfield->ezf_field_ref_field){
                $sql = "SELECT id FROM `".$modelform->ezf_table."` WHERE target = '$target' AND rstat = '2' ORDER BY create_date DESC LIMIT 1;";
                $ezf_tb = Yii::$app->db->createCommand($sql)->queryOne();
                $dataid = $ezf_tb['id'];
                $modelfield->ezf_field_default = 'disabled';
           }
           return self::typePersonal($modelfield,$modelform,$dataid);
        }
        if ($modelfield->ezf_field_type == 13) {
           $modelform = Ezform::find()->where(['ezf_id' => $modelfield->ezf_id])->one();
           if($modelfield->ezf_field_ref_field){
                $sql = "SELECT id FROM `".$modelform->ezf_table."` WHERE target = '$target' AND rstat = '2' ORDER BY create_date DESC LIMIT 1;";
                $ezf_tb = Yii::$app->db->createCommand($sql)->queryOne();
                $dataid = $ezf_tb['id'];
                $modelfield->ezf_field_default = 'disabled';
           }
           return self::typeProvince($modelfield,$modelform,$dataid);
        }
        if ($modelfield->ezf_field_type == 14) {
           $modelform = Ezform::find()->where(['ezf_id' => $modelfield->ezf_id])->one();
           if($modelfield->ezf_field_ref_field){
                $sql = "SELECT id FROM `".$modelform->ezf_table."` WHERE target = '$target' AND rstat = '2' ORDER BY create_date DESC LIMIT 1;";
                $ezf_tb = Yii::$app->db->createCommand($sql)->queryOne();
                $dataid = $ezf_tb['id'];
                $modelfield->ezf_field_default = 'disabled';
           }
           return self::typeFileinput($modelfield,$modelform,$dataid);
        }
        if ($modelfield->ezf_field_type == 15) {
           $modelform = Ezform::find()->where(['ezf_id' => $modelfield->ezf_id])->one();
           if($modelfield->ezf_field_ref_field){
                $sql = "SELECT id FROM `".$modelform->ezf_table."` WHERE target = '$target' AND rstat = '2' ORDER BY create_date DESC LIMIT 1;";
                $ezf_tb = Yii::$app->db->createCommand($sql)->queryOne();
                $dataid = $ezf_tb['id'];
                $modelfield->ezf_field_default = 'disabled';
           }
           return self::typeQuestion($modelfield,$modelform,$dataid);
        }
        if ($modelfield->ezf_field_type == 16) {
           $modelform = Ezform::find()->where(['ezf_id' => $modelfield->ezf_id])->one();
           if($modelfield->ezf_field_ref_field){
                $sql = "SELECT id FROM `".$modelform->ezf_table."` WHERE target = '$target' AND rstat = '2' ORDER BY create_date DESC LIMIT 1;";
                $ezf_tb = Yii::$app->db->createCommand($sql)->queryOne();
                $dataid = $ezf_tb['id'];
                $modelfield->ezf_field_default = 'disabled';
           }
           return self::typeScheckbox($modelfield,$modelform,$dataid);
        }
        if($modelfield->ezf_field_type==17){
           $modelform = Ezform::find()->where(['ezf_id' => $modelfield->ezf_id])->one();
           if($modelfield->ezf_field_ref_field){
                $sql = "SELECT id FROM `".$modelform->ezf_table."` WHERE target = '$target' AND rstat = '2' ORDER BY create_date DESC LIMIT 1;";
                $ezf_tb = Yii::$app->db->createCommand($sql)->queryOne();
                $dataid = $ezf_tb['id'];
                $modelfield->ezf_field_default = 'disabled';
           }
           return self::typeHospital($modelfield,$modelform,$dataid);
        }
        if($modelfield->ezf_field_type==27){
           $modelform = Ezform::find()->where(['ezf_id' => $modelfield->ezf_id])->one();
           if($modelfield->ezf_field_ref_field){
                $sql = "SELECT id FROM `".$modelform->ezf_table."` WHERE target = '$target' AND rstat = '2' ORDER BY create_date DESC LIMIT 1;";
                $ezf_tb = Yii::$app->db->createCommand($sql)->queryOne();
                $dataid = $ezf_tb['id'];
                $modelfield->ezf_field_default = 'disabled';
           }
           return self::typeIcd9($modelfield,$modelform,$dataid);
        }

        if($modelfield->ezf_field_type==28){
           $modelform = Ezform::find()->where(['ezf_id' => $modelfield->ezf_id])->one();
           if($modelfield->ezf_field_ref_field){
                $sql = "SELECT id FROM `".$modelform->ezf_table."` WHERE target = '$target' AND rstat = '2' ORDER BY create_date DESC LIMIT 1;";
                $ezf_tb = Yii::$app->db->createCommand($sql)->queryOne();
                $dataid = $ezf_tb['id'];
                $modelfield->ezf_field_default = 'disabled';
           }
           return self::typeIcd10($modelfield,$modelform,$dataid);
        }




    }
    public static function typeText($modelfield,$modelform,$dataid){
        if($modelfield->ezf_field_label!=""){
            $label = "<label>$modelfield->ezf_field_label</label>";
        }else{
            $label = "";
        }

        $sql = "SELECT ".$modelfield->ezf_field_name." FROM `".$modelform->ezf_table."` WHERE `id`='".$dataid."' ";
        $valueField = \Yii::$app->db->createCommand($sql)->queryOne();
        $getValueField = $valueField[$modelfield->ezf_field_name];

        return
         "<div class='col-md-".$modelfield->ezf_field_lenght."' id='formshow'>"
            .$label
        . "<input $modelfield->ezf_field_default type='text' class='form-control' id='$modelfield->ezf_field_name' name='$modelfield->ezf_field_name' value='$getValueField'></div>";
    }
    public static function typeHeading($modelfield,$modelform,$dataid){
        return "<div class='col-md-12'><h3>$modelfield->ezf_field_label<h3><hr></div>";
    }
    public static function typeTextArea($modelfield,$modelform,$dataid){
        if($modelfield->ezf_field_label!=""){
            $label = "<label>$modelfield->ezf_field_label</label>";
        }else{
            $label = "";
        }

        $sql = "SELECT ".$modelfield->ezf_field_name." FROM `".$modelform->ezf_table."` WHERE `id`='".$dataid."' ";
        $valueField = \Yii::$app->db->createCommand($sql)->queryOne();
        $getValueField = $valueField[$modelfield->ezf_field_name];
       return
         "<div class='col-md-".$modelfield->ezf_field_lenght."' id='formshow'>"
            .$label
        . "<textarea $modelfield->ezf_field_default class='form-control' rows='$modelfield->ezf_field_rows' name='$modelfield->ezf_field_name' id='$modelfield->ezf_field_name'>$getValueField</textarea></div>";
    }
    public static function typeRadioOnload($modelfield,$modelform,$dataid){
        if($modelfield->ezf_field_label!=""){
            $label = "<label>$modelfield->ezf_field_label</label>";
        }else{
            $label = "";
        }

        $sql = "SELECT ".$modelfield->ezf_field_name." FROM `".$modelform->ezf_table."` WHERE `id`='".$dataid."' ";
        $valueField = \Yii::$app->db->createCommand($sql)->queryOne();
        $getValueField = $valueField[$modelfield->ezf_field_name];

        $sql = "SELECT ezf_field_label FROM `ezform_fields` WHERE `ezf_field_id`='".$modelfield->ezf_field_id."';";
        $dataField= \Yii::$app->db->createCommand($sql)->queryAll();

        //$formradio = "<div class='col-md-".$modelfield->ezf_field_lenght."' id='formshow'>";
        $formradio = "<div class='col-md-$modelfield->ezf_field_lenght col-md-offset-$modelfield->ezf_margin_col'>".$label;

        //$sql = " SELECT count(*) as total  FROM `ezform_choice` WHERE ezf_field_id='".($modelfield->ezf_field_id)."' ORDER BY ezf_choice_id";
        //$dataField = \Yii::$app->db->createCommand($sql)->queryAll();

        /*
        $strlenght = $dataField[0]['total'];
        $sql = " SELECT ezf_choicevalue, ezf_choicelabel,ezf_choice_col FROM `ezform_choice` WHERE ezf_field_id='".($modelfield->ezf_field_id)."' ORDER BY ezf_choice_id";
        $dataField = \Yii::$app->db->createCommand($sql)->queryAll();
        $formradio .= "<div class='row'>";
        for($i=0;$i<$strlenght;$i++){
            $sql = "SELECT ".$dataField[$i]['ezf_choicevalue']." FROM `".$modelform->ezf_table."` WHERE `id`='".$dataid."' ";
            $valueField = \Yii::$app->db->createCommand($sql)->queryOne();
            $getValueField = $valueField[$dataField[$i]['ezf_choicevalue']];
            if($dataField[$i]['ezf_choicevalue'] == $getValueField){
                $check = "checked";
            }else{
                $check = "";
            }
            $formradio .= "<div class=col-sm-".$dataField[$i]['ezf_choice_col']."><div class='radio'><input type='radio' name='".$modelfield->ezf_field_name."' id='".$modelfield->ezf_field_name."_".$i."' value='".$dataField[$i]['ezf_choicevalue']."' $check><label for='".$modelfield->ezf_field_name."_".$i."' >".$dataField[$i]['ezf_choicelabel']."</label></div></div>";
        }
        $formradio .= "</radio>";
        return $formradio."</div></div>";

        }*/
        $sql = "SELECT ezf_choice_id, ezf_choicevalue, ezf_choicelabel, ezf_choiceetc FROM `ezform_choice` WHERE ezf_field_id='" . ($modelfield->ezf_field_id) . "' AND ezf_choiceetc IS NULL ORDER BY ezf_choice_id";
        $dataField = \Yii::$app->db->createCommand($sql)->queryAll();
        $strlenght = \Yii::$app->db->createCommand($sql)->query()->count();

        $sql = "SELECT ".$modelfield->ezf_field_name." FROM `".$modelform->ezf_table."` WHERE `id`='".$dataid."' ";
        $valueField = \Yii::$app->db->createCommand($sql)->queryOne();
        $getValueField = $valueField[$modelfield->ezf_field_name];

        //$formradio .= "<div class=''>";
        for ($i = 0; $i < $strlenght; $i++) {

            if($dataField[$i]['ezf_choicevalue'] == $getValueField){
                $check = "checked";
            }else{
                $check = "";
            }

            $formradio .= "<div class='radio'><input $modelfield->ezf_field_default type='radio' class='form-control' name='" . $modelfield->ezf_field_name . "' value='" . $dataField[$i]['ezf_choicevalue'] . "' $check><label for='".$dataField[$i]['ezf_choice_id']."'>" . $dataField[$i]['ezf_choicelabel']."";
            $sql = " SELECT ezf_choicevalue, ezf_choicelabel, ezf_choiceetc FROM `ezform_choice` WHERE ezf_choiceetc ='".$dataField[$i]['ezf_choice_id']."' ORDER BY ezf_choice_id";
            $formradio .= "</label></div>";
            if(\Yii::$app->db->createCommand($sql)->query()->count()>0){
              $dataOther = \Yii::$app->db->createCommand($sql)->queryOne();
              $sql = "SELECT ".$dataOther['ezf_choicevalue']." FROM `".$modelform->ezf_table."` WHERE `id`='".$dataid."' ";
              $valueField = \Yii::$app->db->createCommand($sql)->queryOne();
              $formradio .= "<div style='margin-left:45px;'><input type='text' placeholder='".$dataOther['ezf_choicelabel']."' class='form-control' id='".$dataOther['ezf_choicevalue']."' name='".$dataOther['ezf_choicevalue']."' value='".$valueField[$dataOther['ezf_choicevalue']]."'></div>";
            }
            //$formradio .= "</div>";
        }
        $formradio .= "</div>";
        return $formradio;
    }
     public static function typeCheckboxOnload($modelfield,$modelform,$dataid){

//        $check = "";
        if($modelfield->ezf_field_label!=""){
            $label = "<label>$modelfield->ezf_field_label</label>";
        }else{
            $label = "";
        }
        $html = "<div class='col-md-".$modelfield->ezf_field_lenght."' id='formshow'>";
        $html .= $label;
        $sql = "SELECT ezf_choice_id, ezf_choicevalue, ezf_choicelabel, ezf_choiceetc FROM `ezform_choice` WHERE ezf_field_id='" . ($modelfield->ezf_field_id) . "' AND ezf_choiceetc IS NULL ORDER BY ezf_choice_id";
        $dataField = \Yii::$app->db->createCommand($sql)->queryAll();


        foreach($dataField as $valuefield){
            $sql = "SELECT ".$valuefield["ezf_choicevalue"]." FROM `".$modelform->ezf_table."` WHERE `id`='".$dataid."' ";
            $valueField = \Yii::$app->db->createCommand($sql)->queryOne();
            $getValueField = $valueField[$valuefield["ezf_choicevalue"]];
            if($valuefield["ezf_choicelabel"] == $getValueField){
                $check = "checked";
            }else{
                $check = "";
            }
            $html .= "<div class='checkbox'>";
            $html .= "<input $modelfield->ezf_field_default type='checkbox' name='".$valuefield["ezf_choicevalue"]."' id='".$valuefield["ezf_choicevalue"]."' value='".$valuefield["ezf_choicelabel"]."' $check><label for='".$valuefield["ezf_choicevalue"]."' >".$valuefield["ezf_choicelabel"]."</label>";
            $sql = " SELECT ezf_choicevalue, ezf_choicelabel, ezf_choiceetc FROM `ezform_choice` WHERE ezf_choiceetc ='".$valuefield['ezf_choice_id']."' ORDER BY ezf_choice_id";
            if(\Yii::$app->db->createCommand($sql)->query()->count()>0){
              $dataOther = \Yii::$app->db->createCommand($sql)->queryOne();
              $sql = "SELECT ".$dataOther['ezf_choicevalue']." FROM `".$modelform->ezf_table."` WHERE `id`='".$dataid."' ";
              $valueField = \Yii::$app->db->createCommand($sql)->queryOne();
              $html .= "<div style='margin-left:45px;'><input type='text' placeholder='".$dataOther['ezf_choicelabel']."' class='form-control' id='".$dataOther['ezf_choicevalue']."' name='".$dataOther['ezf_choicevalue']."' value='".$valueField[$dataOther['ezf_choicevalue']]."'></div>";
            }
            $html .= "<br></div>";
        }
        $sql2 = "SELECT * FROM `ezform_choice` WHERE `ezf_field_id`='".$modelfield->ezf_field_id."' AND `ezf_choiceetc`='1' ";
        $data2 = \Yii::$app->db->createCommand($sql2)->queryOne();
        if($data2!=""){
            $sql3 = "SELECT ".$data2["ezf_choicevalue"]." FROM `".$modelform->ezf_table."` WHERE `id`='".$dataid."' ";
            $data3 = \Yii::$app->db->createCommand($sql3)->queryOne();
            $valueEtc = $data3[$data2["ezf_choicevalue"]];
            if($valueEtc!=""){
                $checkEtc = "checked";
            }else{
                $checkEtc = "";
            }
        }

        if(($data2)!=""){
                $html .= "<div class='checkbox'>";
		$html .= "<input type='checkbox' name='".$data2["ezf_choicevalue"]."' id='".$data2["ezf_choicevalue"]."' value='".$modelfield->ezf_field_name."' $checkEtc><label for='".$data2["ezf_choicevalue"]."'>".$data2["ezf_choicelabel"]."</label>";
		$html .= "<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type='text' class='form-control' name='".$data2["ezf_choicevalue"]."' id='".$data2["ezf_choicevalue"]."'style='width:250px;' value='".$valueEtc."'/></div>";
        }

        return $html."</div>";
    }
    public static function typeSelectOnload($modelfield,$modelform,$dataid){
        if($modelfield->ezf_field_label!=""){
            $label = "<label>$modelfield->ezf_field_label</label>";
        }else{
            $label = "";
        }

        $sql = "SELECT ".$modelfield->ezf_field_name." FROM `".$modelform->ezf_table."` WHERE `id`='".$dataid."' ";
        $valueField = \Yii::$app->db->createCommand($sql)->queryOne();
        $getValueField = $valueField[$modelfield->ezf_field_name];
        $sql = "SELECT * FROM `ezform_fields` WHERE `ezf_field_id`='".$modelfield->ezf_field_id."';";
        $dataField= \Yii::$app->db->createCommand($sql)->queryAll();
        $formselect = "<div class='col-md-".$modelfield->ezf_field_lenght."' id='formshow'>";
        $formselect .= $label."</label>";
        $formselect .= "<select $modelfield->ezf_field_default class='form-control' id='".($dataField[0]['ezf_field_name'])."' name='".($dataField[0]['ezf_field_name'])."' style='width:auto;'>";

        $sql = " SELECT count(*) as total  FROM `ezform_choice` WHERE ezf_field_id='".($modelfield->ezf_field_id)."' ORDER BY ezf_choice_id";
        $dataField = \Yii::$app->db->createCommand($sql)->queryAll();
        $strlenght = $dataField[0]['total'];

        $sql = " SELECT ezf_choicevalue, ezf_choicelabel FROM `ezform_choice` WHERE ezf_field_id='".($modelfield->ezf_field_id)."' ORDER BY ezf_choice_id";
        $dataField = \Yii::$app->db->createCommand($sql)->queryAll();
        for($i=0;$i<$strlenght;$i++){
            if($dataField[$i]['ezf_choicevalue']==$getValueField){
                $check = "selected";
            }else{
                $check = "";
            }
            $formselect .= "<option value='".$dataField[$i]['ezf_choicevalue']."' $check>".$dataField[$i]['ezf_choicelabel']."</option>";
        }
        $formselect .= "</select>";
        return $formselect."</div>";
    }
    public static function typeDate($modelfield,$modelform,$dataid){
        if($modelfield->ezf_field_label!=""){
            $label = "<label>$modelfield->ezf_field_label</label>";
        }else{
            $label = "";
        }

        $sql = "SELECT ".$modelfield->ezf_field_name." FROM `".$modelform->ezf_table."` WHERE `id`='".$dataid."' ";
        $valueField = \Yii::$app->db->createCommand($sql)->queryOne();
        $getValueField = $valueField[$modelfield->ezf_field_name];
        if(isset($getValueField)){

        }else{
            $getValueField = "__/__/____";
        }
        return "<div class='col-md-".$modelfield->ezf_field_lenght."' id='formshow'>".$label
                . '<input type="text" '
                .$modelfield->ezf_field_default. ' id="fieldDateTime" '
                . 'class="form-control" '
                . 'data-provide="datepicker" data-date-language="th-th"'
                . 'name='.$modelfield->ezf_field_name.' '
                . 'value='.$getValueField.' '
                . 'data-inputmask="&quot;mask&quot;: &quot;99/99/9999&quot;" '
                . 'data-mask></div>';


    }
    public static function typeTime($modelfield,$modelform,$dataid){
        if($modelfield->ezf_field_label!=""){
            $label = "<label>$modelfield->ezf_field_label</label>";
        }else{
            $label = "";
        }

        $sql = "SELECT ".$modelfield->ezf_field_name." FROM `".$modelform->ezf_table."` WHERE `id`='".$dataid."' ";
        $valueField = \Yii::$app->db->createCommand($sql)->queryOne();
        $getValueField = $valueField[$modelfield->ezf_field_name];
        if(isset($getValueField)){

        }else{
            $getValueField = "__:__";
        }
        return "<div class='col-md-".$modelfield->ezf_field_lenght."' id='formshow'>".$label
                . '<input type="text" '
                .$modelfield->ezf_field_default. ' class="form-control" '
                . 'id="fieldTime"'
                . 'name='.$modelfield->ezf_field_name.' '
                . 'value='.$getValueField.' '
                . 'data-inputmask="&quot;mask&quot;: &quot;99:99&quot;" '
                . 'data-mask></div>';
    }
    public static function typeDateTime($modelfield,$modelform,$dataid){
        if($modelfield->ezf_field_label!=""){
            $label = "<label>$modelfield->ezf_field_label</label>";
        }else{
            $label = "";
        }

        $sql = "SELECT ".$modelfield->ezf_field_name." FROM `".$modelform->ezf_table."` WHERE `id`='".$dataid."' ";
        $valueField = \Yii::$app->db->createCommand($sql)->queryOne();
        $getValueField = $valueField[$modelfield->ezf_field_name];
        if(isset($getValueField)){

        }else{
            $getValueField = "__/__/____ __:__";
        }
        return "<div class='col-md-".$modelfield->ezf_field_lenght."' id='formshow'>".$label
                . "<input type='text' "
                .$modelfield->ezf_field_default. " class='form-control' "
                . "name='".$modelfield->ezf_field_name."' "
                . "value='".$getValueField."' "
                . "data-inputmask='&quot;mask&quot;: &quot;99/99/9999 99:99&quot;' "
                . "data-mask></div>";
    }
    public static function typePersonal($modelfield,$modelform,$dataid){
        if($modelfield->ezf_field_label!=""){
            $label = "<label>$modelfield->ezf_field_label</label>";
        }else{
            $label = "";
        }

        $sql = "SELECT ".$modelfield->ezf_field_name." FROM `".$modelform->ezf_table."` WHERE `id`='".$dataid."' ";
        $valueField = \Yii::$app->db->createCommand($sql)->queryOne();
        $getValueField = $valueField[$modelfield->ezf_field_name];
        if(isset($getValueField)){

        }else{
            $getValueField = "_-____-_____-__-_";
        }
        return "<div class='col-md-".$modelfield->ezf_field_lenght."' id='formshow'>".$label

                . '<input type="text" '
                .$modelfield->ezf_field_default. ' class="form-control" '
                . 'name='.$modelfield->ezf_field_name.' '
                . 'value='.$getValueField.' '
                . 'data-inputmask="&quot;mask&quot;: &quot;9-9999-99999-99-9&quot;" '
                . 'data-mask></div>';
    }
    public static function typeSnomed($modelfield,$modelform,$dataid){

        $sql = "SELECT ".$modelfield->ezf_field_name." FROM `".$modelform->ezf_table."` WHERE `id`='".$dataid."' ";
        $valueField = \Yii::$app->db->createCommand($sql)->queryOne();
        $getValueField = $valueField[$modelfield->ezf_field_name];
        $url = \yii\helpers\Url::to(['/ezforms/snomed/searchsnomed']);
        if($modelfield->ezf_field_label!=""){
            $label = "<label>$modelfield->ezf_field_label</label>";
        }else{
            $label = "";
        }
        return "<div class='col-md-".$modelfield->ezf_field_lenght."' id='formshow'>".$label.Select2::widget([
            'initValueText'=>$getValueField,
            'options' => ['placeholder' => 'Search for a snomed ...', ],
            'name'=>$modelfield->ezf_field_name,
            'pluginOptions' => [
                'allowClear' => true,
                'minimumInputLength' => 2,
                'ajax' => [
                    'url' =>$url,
                    'dataType' => 'json',
                    'data' => new JsExpression('function(params) { return {q:params.term}; }')
                ],
                'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                'templateResult' => new JsExpression('function(city) { return city.text; }'),
                'templateSelection' => new JsExpression('function (city) { return city.text; }'),
            ],
        ])."</div>";
    }
    public static function typeHospital($modelfield,$modelform,$dataid){
        return Yii::$app->runAction('ezforms/hospital/gen/',['modelfield'=>$modelfield,'modelform'=>$modelform,'dataid'=>$dataid]);
    }
    public static function typeIcd9($modelfield,$modelform,$dataid){
        return Yii::$app->runAction('ezforms/icd9/gen/',['modelfield'=>$modelfield,'modelform'=>$modelform,'dataid'=>$dataid]);
    }
    public static function typeIcd10($modelfield,$modelform,$dataid){
        return Yii::$app->runAction('ezforms/icd10/gen/',['modelfield'=>$modelfield,'modelform'=>$modelform,'dataid'=>$dataid]);
    }
    public static function typeProvince($modelfield,$modelform,$dataid){
       return Yii::$app->runAction('ezforms/genprovince/gen/',['modelfield'=>$modelfield,'modelform'=>$modelform,'dataid'=>$dataid]);
    }
    public static function typeFileinput($modelfield,$modelform,$dataid){

        $sql = "SELECT " . $modelfield->ezf_field_name . " FROM `" . $modelform->ezf_table . "` WHERE `id`='" . $dataid . "' ";
        $valueField = \Yii::$app->db->createCommand($sql)->queryOne();
        $getValueField = $valueField[$modelfield->ezf_field_name];
        $path = Yii::$app->homeUrl."/uploads/imgform/".$modelfield->ezf_id."/".$dataid."_".$getValueField;
        if($getValueField!=""){
            echo "<div class='col-md-$modelfield->ezf_field_lenght'><label>" . $modelfield->ezf_field_label . "</label>";
            echo FileInput::widget([
                'name' => $modelfield->ezf_field_name,
                'options' => ['accept' => 'image/*'],
                'pluginOptions' => [
                    'showUpload' => false,
                    'previewFileType' => 'image',
                    'initialPreview' => [
                        Html::img($path, ['class' => 'file-preview-image','style'=>'cursor:pointer']),
                    ],
                    'overwriteInitial'=>true,
                ],
                'options' => ['multiple' => false,'accept' => 'image/*']
            ])."</div>";
        }else {
            echo "<div class='col-md-$modelfield->ezf_field_lenght'><label>" . $modelfield->ezf_field_label . "</label>";
            echo FileInput::widget([
                'name' => $modelfield->ezf_field_name,
                'pluginOptions' => [
                    'showRemove' => false,
                    'showUpload' => false,
                ],
                'options' => ['multiple' => false,'accept' => 'image/*']
            ])."</div>";
        }

    }
    public static function typeComponent($modelfield,$modelform,$dataid)
    {
        //\yii\helpers\VarDumper::dump($modelfield, 10, true);
        $ezformComponents = EzformComponent::find()
                                ->where(['comp_id' => $modelfield->ezf_component])
                                ->one();

        /**  Get field key name. */
        $field_key = EzformFields::find()
                            ->where(['ezf_field_id' => $ezformComponents->field_id_key])
                            ->one();

        /** Get field desc name. */
        if (!empty($ezformComponents->field_id_desc)) {
            $field_desc_array = explode(',', $ezformComponents->field_id_desc);
        }

        $field_desc_list = '';

        foreach ($field_desc_array as $value) {

            if (!empty($value)) {
                $field_desc = EzformFields::find()
                                ->where(['ezf_field_id' => $value])
                                ->one();

                $field_desc_list .= 'COALESCE('.$field_desc->ezf_field_name.", '-'), ' ',";
            }

        }

        $field_desc_list = substr($field_desc_list, 0 ,strlen($field_desc_list)-6);

        /** Get table name. */
        $ezform = Ezform::find()
                    ->where(['ezf_id' => $ezformComponents->ezf_id])
                    ->one();

        $dynamic_datas = ArrayHelper::map(Yii::$app->db->createCommand('SELECT *, CONCAT('.$field_desc_list.') AS field_desc FROM '.$ezform->ezf_table)
                            ->queryAll(), $field_key->ezf_field_name, 'field_desc');


        //\yii\helpers\VarDumper::dump($dynamic_datas, 10, true);

        /*$select = '<select class="form-control js-sin">';

        foreach ($dynamic_datas as $key => $dynamic_data) {
            $select .= "<option value='".$key."'>".$dynamic_data."</option>";
        }

        $select .= '</select>';*/

        /*return "<label>".$modelfield->ezf_field_label."</label>"
                .$select;*/

        $sql = "SELECT " . $modelfield->ezf_field_name . " FROM `" . $modelform->ezf_table . "` WHERE `id`='" . $dataid . "' ";
        $valueField = \Yii::$app->db->createCommand($sql)->queryOne();
        $getValueField = $valueField[$modelfield->ezf_field_name];
        $arValue = explode(',',$getValueField);

        if (1 == $ezformComponents->comp_select) {
            return "<div class='col-md-$modelfield->ezf_field_lenght'><label>$modelfield->ezf_field_label</label>".Select2::widget([

                'options' => ['placeholder' => 'ค้นหา...', 'multiple' => true],
                'data' => $dynamic_datas,
                'name' => $modelfield->ezf_field_name,
                'value' => $arValue,
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ])."</div>";
        } else {
            return "<div class='col-md-$modelfield->ezf_field_lenght'><label>$modelfield->ezf_field_label</label>".Select2::widget([
                'options' => ['placeholder' => 'ค้นหา...', 'multiple' => true],
                'data' => $dynamic_datas,
                'value' => $arValue,
                'name' => $modelfield->ezf_field_name,
                'pluginOptions' => [
                    'allowClear' => true,
                    'tags' => true
                ],
            ])."</div>";
        }
    }
    public static function typeQuestion($modelfield,$modelform,$dataid){
        return "<div class='col-md-12'><h4>$modelfield->ezf_field_label</h4><hr></div>";
    }
    public static function typeScheckbox($modelfield,$modelform,$dataid){
        $sql = "SELECT * FROM `".$modelform->ezf_table."` WHERE `id`='".$dataid."' ";
        $valueField = \Yii::$app->db->createCommand($sql)->queryOne();
        $valScheckbox = $valueField[$modelfield->ezf_field_name];
        if($valScheckbox==1){
            $check = "checked";
        }else{
            $check = "";
        }

        return "<div class='col-md-".$modelfield->ezf_field_lenght." col-md-offset-".$modelfield->ezf_margin_col."' id='formshow'>"
        ."<div class='checkbox checkbox-default'>"
        . "<input $modelfield->ezf_field_default type='checkbox' id='$modelfield->ezf_field_name' name='$modelfield->ezf_field_name' value='1' $check>"
        . "<label for='$modelfield->ezf_field_name'>$modelfield->ezf_field_label</label>"
        . "</div></div>";

    }
}
