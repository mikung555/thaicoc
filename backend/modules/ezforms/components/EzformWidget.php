<?php
namespace backend\modules\ezforms\components;

use backend\models\Ezform;
use backend\modules\ezforms\models\EzformChoice;
use backend\modules\ezforms\models\EzformFields;
use common\lib\damasac\widgets\DMSCheckboxWidget;
use common\lib\damasac\widgets\DMSDatetimeWidget;
use common\lib\damasac\widgets\DMSDateWidget;
use common\lib\damasac\widgets\DMSTimeWidget;
use Faker\Provider\DateTime;
use Yii;
use yii\bootstrap\BaseHtml;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use kartik\widgets\Select2;
use kartik\depdrop\DepDrop;
use kartik\widgets\DateTimePicker;
use kartik\widgets\FileInput;
use yii\helpers\VarDumper;
use yii\web\JsExpression;
use kartik\widgets\DatePicker;
use yii\widgets\MaskedInput;
use kartik\widgets\TimePicker;
use common\lib\sdii\components\helpers\SDNoty;

class EzformWidget extends Html {
    public static function activeSelectInput($model, $attribute, $field, $option){
        $items = EzformQuery::getChoiceId($field->ezf_field_id);
        $html = Html::activeLabel($model, $attribute);
        $html .= $field["ezf_field_help"];
        $html .= Html::activeDropDownList($model, $attribute, ArrayHelper::map($items, 'ezf_choicevalue', 'ezf_choicelabel'), $option);
        $html .= Html::error($model, $attribute);
        return $html;
    }

    public static function selectInputValidate($form, $model, $attribute, $field, $options){
        $items = EzformQuery::getChoiceId($field->ezf_field_id);
        $annotatedVal ='';
        if((string)$form->attributes['rstat']=='annotated'){
            foreach ($items as $val){
                $annotatedVal .= $val['ezf_choicevalue'].' = '.$val['ezf_choicelabel'].'<br>';
            }
            $annotatedVal =  '<div class="well well-sm">'.$annotatedVal.'</div>';
        }
        return $form->field($model, $attribute)->dropDownList(ArrayHelper::map($items, 'ezf_choicevalue', 'ezf_choicelabel'), $options)->label($field['ezf_field_label'].$field["ezf_field_help"]).$annotatedVal;

    }

    public static function headingValidate($form, $model, $attribute, $field, $option){
        return self::activeHeading($model, $attribute, $field, $option);
    }

    public static function questionValidate($form, $model, $attribute, $field, $option){
        return self::activeQuestion($model, $attribute, $field, $option);
    }

    public static function activeHeading($model, $attribute, $field, $option){
        return Html::tag('h3', $option['labelOptions']['label'], $option['labelOptions'])."<hr>".$field["ezf_field_help"];
    }

    public static function activeQuestion($model, $attribute, $field, $option){
        return Html::label($option['labelOptions']['label'].$field["ezf_field_help"], null);
    }

    public static function activeSCheckbox($model, $attribute, $field, $option)
    {
        $option['class'] = '';
        return $field["ezf_field_help"].static::activeCheckbox($model, $attribute, $option);
    }

    public static function checkboxValidate($form, $model, $attribute, $field, $option)
    {
        $annotatedVal =false;
        if((string)$form->attributes['rstat']=='annotated'){
            $annotatedVal = '<div style="margin-top: -20px; margin-left: 30px;"><sub style="color:green;">0 = No (ปล่อยว่าง); 1 = Yes (เลือก)</sub></div>';
        }
        $option['class'] = '';
        return $field["ezf_field_help"].$form->field($model, $attribute)->checkbox($option).$annotatedVal;
    }

    public static function checkboxListValidate($form, $model, $attribute, $field, $option)
    {
        $checkbox = \backend\modules\ezforms\models\EzformFields::find()
            ->where(['ezf_field_sub_id'=>$field->ezf_field_id])
            ->andWhere('ezf_field_label <> "" ')
            ->all();

        $html = Html::activeLabel($model, $attribute);
        $html .= $field["ezf_field_help"];
        if($field['ezf_field_default']==1){
            foreach($checkbox as $itemCheckbox){
                $annotatedVal=false; $ann_sub_textvalue=false; $annFieldName=false;
                if((string)$form->attributes['rstat']=='annotated'){
                    $annotatedVal = '<div style="margin-top: -20px; margin-left: 30px;"><sub style="color:green;">0 = No (ปล่อยว่าง); 1 = Yes (เลือก)</sub></div>';
                    $ann_sub_textvalue = '<code>'.$itemCheckbox["ezf_field_sub_textvalue"].'</code><br>';
                    $annFieldName = '<br><code>'.$itemCheckbox['ezf_field_name'].'</code>';
                }
                $html .= $annFieldName.$form->field($model,$itemCheckbox["ezf_field_name"])->checkbox(['class'=>'']).$annotatedVal;

                if($itemCheckbox["ezf_field_sub_textvalue"]!==""){
                    $html .= $form->field($model,$itemCheckbox["ezf_field_sub_textvalue"])->textInput(['placeholder'=>'ระบุ ...'])->label($ann_sub_textvalue);
                }
            }
        }
        else{
            $html .= '<div class="row">';
            foreach($checkbox as $itemCheckbox){
                $annotatedVal =false; $ann_sub_textvalue =false; $annFieldName=false;
                if((string)$form->attributes['rstat']=='annotated'){
                    $annotatedVal = '<div style="margin-top: -20px; margin-left: 30px;"><sub style="color:green;">0 = No (ปล่อยว่าง); 1 = Yes (เลือก)</sub></div>';
                    $ann_sub_textvalue = '<code>'.$itemCheckbox["ezf_field_sub_textvalue"].'</code><br>';
                    $annFieldName = '<br><code>'.$itemCheckbox['ezf_field_name'].'</code>';
                }

                $html .= '<div class="col-lg-2">';
                $html .= $annFieldName.$form->field($model,$itemCheckbox["ezf_field_name"])->checkbox(['class'=>'']).$annotatedVal;

                if($itemCheckbox["ezf_field_sub_textvalue"]!==""){
                    $html .= $form->field($model,$itemCheckbox["ezf_field_sub_textvalue"])->textInput(['placeholder'=>'ระบุ ...'])->label($ann_sub_textvalue);
                }
                $html .= '</div>';
            }
            $html .= '</div>';
        }
        return $html;
    }

    public static function radioListValidate($form, $model, $attribute, $field, $option)
    {
        $items = EzformQuery::getChoiceId($field->ezf_field_id);
        $option['class'] = '';
        $option['field_other'] = $field->ezf_field_id;


        $html = Html::activeLabel($model, $attribute);
        $html .= $field["ezf_field_help"];
        $html .= Html::error($model, $attribute);
        //\yii\helpers\VarDumper::dump($items, 10, TRUE);

        if($field['ezf_field_default']==1){
            foreach($items as $key=>$value){
                $html .= "<div id='sddynamicmodel-".$attribute."-".$value['ezf_choicevalue']."'>";
                $annotatedVal =false;
                if((string)$form->attributes['rstat']=='annotated'){
                    $annotatedVal = '<sub style="color:green;">'.$value['ezf_choicevalue'].'</sub> ';
                }
                //
                //$html .= $form->field($model, $attribute)->radio(['value'=>$value['ezf_choicevalue'], 'id' => 'radio_'.$attribute.$key])->label($value['ezf_choicelabel']);
                $html .= Html::activeRadio($model, $attribute, ['onclick'=>'$("#'.$attribute.$key.'_text").fadeIn()', 'label'=>$annotatedVal.$value['ezf_choicelabel'], 'value'=>$value['ezf_choicevalue'], 'uncheck' => null]);
                $html .= "<br>";
                //echo $value['ezf_choice_id'], ', ';
                $etc = EzformFields::find()->select('ezf_field_name')->where('ezf_field_ref = :ezf_field_ref', [':ezf_field_ref' => $value['ezf_choice_id']]);
                //\yii\helpers\VarDumper::dump($etc->one(), 10, TRUE);
                if($etc->count()){
                    $etc = $etc->one();
                    $etc_field_name = $etc->ezf_field_name;
                    //
                    $ann_sub_textvalue ='';
                    if((string)$form->attributes['rstat']=='annotated'){
                        $ann_sub_textvalue = '<code>'.$etc_field_name.'</code><br>';
                    }
                    $html .= $ann_sub_textvalue.Html::activeTextInput($model, $etc_field_name, ['id'=>$attribute.$key.'_text','class'=>'form-control', 'style'=>'width:100%; display:'.(($model->$etc_field_name ==''  && $model->$attribute !=$value['ezf_choicevalue'] && $ann_sub_textvalue=='') ? 'none' : '').';', 'placeholder'=> 'ระบุ...']);
                }
                $html .= '</div>';
            }
        }else{
            $html .= '<div class="row">';
            foreach($items as $key=>$value){
                $html .= "<div id='sddynamicmodel-".$attribute."-".$value['ezf_choicevalue']."'>";
                $annotatedVal =false;
                if((string)$form->attributes['rstat']=='annotated'){
                    $annotatedVal = '<sub style="color:green;">'.$value['ezf_choicevalue'].'</sub>';
                }
                //
                $html .= '<div class="col-lg-2">';
                $html .= Html::activeRadio($model, $attribute, ['onclick'=>'$("#'.$attribute.$key.'_text").fadeIn()', 'label'=>$annotatedVal.$value['ezf_choicelabel'], 'value'=>$value['ezf_choicevalue'], 'uncheck' => null]);
                $html .= "<br>";
                $etc = EzformFields::find()->select('ezf_field_name')->where('ezf_field_ref = :ezf_field_ref', [':ezf_field_ref' => $value['ezf_choice_id']]);
                if($etc->count()){
                    $etc = $etc->one();
                    $etc_field_name = $etc->ezf_field_name;
                    //
                    $ann_sub_textvalue ='';
                    if((string)$form->attributes['rstat']=='annotated'){
                        $ann_sub_textvalue = '<code>'.$etc_field_name.'</code><br>';
                    }
                    $html .= $ann_sub_textvalue.Html::activeTextInput($model, $etc_field_name, ['id'=>$attribute.$key.'_text','class'=>'form-control', 'style'=>'width:100%; display:'.(($model->$etc_field_name ==''  && $model->$attribute !=$value['ezf_choicevalue'] && $ann_sub_textvalue=='') ? 'none' : '').';', 'placeholder'=> 'ระบุ...']);
                }
                $html .= '</div>';
                $html .= '</div>';
            }
            $html .= '</div>';
        }

        return $html;
    }

    public static function activeRadioOnload($model, $attribute, $field, $option)
    {
        $option['class'] = '';
        $option['field_other'] = $field->ezf_field_id;
        $items = EzformQuery::getChoiceId($field->ezf_field_id);

        $html = Html::activeLabel($model, $attribute);
        $html .= $field["ezf_field_help"];
        $html .= Html::error($model, $attribute);
        if($field['ezf_field_default']==1){
            foreach($items as $key=>$value){
                $html .= Html::activeRadio($model, $attribute, ['label'=>$value['ezf_choicelabel'], 'value'=>$value['ezf_choicevalue']]);
                $html .= '<br>';
                $etc = EzformFields::find()->select('ezf_field_name')->where('ezf_field_ref = :ezf_field_ref', [':ezf_field_ref' => $value['ezf_choice_id']]);
                if($etc->count()){
                    $etc = $etc->one();
                    $html .= BaseHtml::textInput($etc->ezf_field_name, null, ['class'=>'form-control', 'style'=>'width:100%;', 'placeholder'=> 'ระบุ...']);
                }
            }
        }
        else{
            $html .= '<div class="row">';
            foreach($items as $key=>$value){
                $html .= '<div class="col-lg-2">';
                $html .= Html::activeRadio($model, $attribute, ['label'=>$value['ezf_choicelabel'], 'value'=>$value['ezf_choicevalue']]);
                $etc = EzformFields::find()->select('ezf_field_name')->where('ezf_field_ref = :ezf_field_ref', [':ezf_field_ref' => $value['ezf_choice_id']]);
                if($etc->count()){
                    $etc = $etc->one();
                    $html .= BaseHtml::textInput($etc->ezf_field_name, null, ['class'=>'form-control', 'style'=>'width:100%;', 'placeholder'=> 'ระบุ...']);
                }
                $html .= '</div>';
            }
            $html .= '</div>';
        }

        return $html;
    }
    public static function activeCheckboxHead($model, $attribute, $field, $option){

        $checkbox = \backend\modules\ezforms\models\EzformFields::find()
            ->where(['ezf_field_sub_id'=>$field->ezf_field_id])
            ->andWhere('ezf_field_label <> "" ')
            ->all();

        $html = Html::activeLabel($model, $attribute);
        $html .= $field["ezf_field_help"];
        $option['class'] = '';
        if($field['ezf_field_default']==1){
            foreach($checkbox as $key=>$value){
                $html .= '<div class="checkbox"><label class="checkbox-inline"><input type="checkbox" >'.$value["ezf_field_label"].'</label></div>';
                if($value["ezf_field_sub_textvalue"]!==""){
                    $html .= "";
                    $html .= Html::activeTextInput($model,$value["ezf_field_sub_textvalue"],['class'=>'form-control','placholder'=>'ระบุ ...']);
                }
            }
        }
        else{
            $html .= '<div clas="row">';
            foreach($checkbox as $key=>$value){
                $html .= '<div class="col-lg-2">';
                $html .= '<div class="checkbox"><label class="checkbox-inline"><input type="checkbox" >'.$value["ezf_field_label"].'</label></div>';
                if($value["ezf_field_sub_textvalue"]!==""){
                    $html .= "";
                    $html .= Html::activeTextInput($model,$value["ezf_field_sub_textvalue"],['class'=>'form-control','placholder'=>'ระบุ ...']);
                }
                $html .= '</div>';
            }
            $html .= '</div>';
        }
        $html .= Html::error($model, $attribute);
        return $html;
    }
    public static function activeCheckboxOnload($model, $attribute, $field, $option)
    {
        $items = \backend\modules\ezforms\models\EzformFields::find()->where(['ezf_field_sub_id'=>$field->ezf_field_id])->andWhere('ezf_field_sub_other is NULL')->all();

        $option['class'] = '';
        $option['field_other'] = $field->ezf_field_id;
        $html = Html::activeLabel($model, $attribute);
        $html .= Html::error($model, $attribute);

        return $html;
    }



    public static function activeComponentInput($model,$attribute,$field,$option){

        $ezformComponents = EzformQuery::getFieldComponent($field->ezf_component);
        //VarDumper::dump($ezformComponents, 10, true);

        $field_key = EzformFields::find()->where(['ezf_field_id' => $ezformComponents->field_id_key])->one();
        //VarDumper::dump($ezformComponents, 10, true);

        if ($ezformComponents->field_id_desc) {
            $field_desc_array = explode(',', $ezformComponents->field_id_desc);
        }

        $ezform = EzformQuery::getForm($ezformComponents->ezf_id);

        $sql =" WHERE 1"; $field_desc_list = '';
        if ($ezformComponents->comp_id == 100000 OR $ezformComponents->comp_id == 100001) {
            $sql .=  " AND sitecode = '" . Yii::$app->user->identity->userProfile->sitecode . "'";
            if(!$model->$attribute && ($form->attributes['rstat'] ==0 || $form->attributes['rstat'] ==1))
                $model->$attribute = Yii::$app->user->id;
            else
                $sql .=  " OR user_id = '" . $model->$attribute . "'";

            //
            if(Yii::$app->keyStorage->get('frontend.domain')=='cascap.in.th'){
                $field_desc_list = "COALESCE(SUBSTR(cid,-4), '-'), ' ',";
            }
        }

        //if find by target
        if($ezformComponents->find_target AND $ezformComponents->special){
            //$sql .= " LIMIT 0,1";
        }else if($ezformComponents->find_target){
            //$sql .= " LIMIT 0,1";
        }

        if(count($field_desc_array)){
            foreach ($field_desc_array as $value) {
                if (!empty($value)) {
                    $field_desc = EzformFields::find()->where(['ezf_field_id' => $value])->one();
                    $field_desc_list .= 'COALESCE('.$field_desc->ezf_field_name.", '-'), ' ',";
                }
            }
        }else{
            $field_desc_list .= "COALESCE('ยังไม่ได้ระบุ Component Description', '-'), ' ',";
        }

        $field_desc_list = substr($field_desc_list, 0 ,-6);
        //VarDumper::dump($field_desc_list, 10, true);

        if ($ezformComponents->comp_id == 100000 OR $ezformComponents->comp_id == 100001) {
            $sql =  " WHERE sitecode = '" . Yii::$app->user->identity->userProfile->sitecode . "'";
        }

        $ezform = EzformQuery::getForm($ezformComponents->ezf_id);

        $dynamic_datas = ArrayHelper::map(Yii::$app->db->createCommand('SELECT '.$field_key->ezf_field_name.' as id, CONCAT('.$field_desc_list.') AS field_desc FROM '.($ezform->ezf_table).$sql ." LIMIT 0,1")
            ->queryAll(), $field_key->ezf_field_name, 'field_desc');

        if($field->ezf_field_ref_field)
            $field->ezf_field_id = $field->ezf_field_ref_field;

        $html = Html::activeLabel($model, $attribute);
        $html .= $field["ezf_field_help"];
        $html .= Select2::widget([
            'options' => ['placeholder' => 'เลือกรายการ...', 'multiple' => ($ezformComponents->comp_select==2 ? true : false)],
            'data' => $dynamic_datas,
            'model' =>$model,
            'attribute'=>$attribute,
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]);
        $html .= Html::error($model, $attribute);
        return $html;
    }
    public static function activeSnomedInput($model,$attribute,$field,$option){
        $url = \yii\helpers\Url::to(['/ezforms/snomed/searchsnomed']);
        $html = Html::activeLabel($model, $attribute);
        $html .= $field["ezf_field_help"];
        $html .= Select2::widget([ // set the initial display text
            'model' =>$model,
            'attribute'=>$attribute,
            'options' => ['placeholder' => 'Search for a snomed ...'],
            'pluginOptions' => [
                'allowClear' => true,
                'minimumInputLength' => 3,
                'ajax' => [
                    'url' => $url,
                    'dataType' => 'json',
                    'data' => new JsExpression('function(params) { return {q:params.term}; }')
                ],
                'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                'templateResult' => new JsExpression('function(city) { return city.text; }'),
                'templateSelection' => new JsExpression('function (city) { return city.text; }'),
            ],
        ]);
        $html .= Html::error($model, $attribute);
        return $html;
    }
    public static function activeFileUploadInput($model,$attribute,$field,$option){
        $html = Html::activeLabel($model,$attribute);
        $html .= $field["ezf_field_help"];
        $path = Yii::$app->homeUrl."/uploads/imgform/";
        $html .= FileInput::widget([
            'model' =>$model,
            'attribute'=>$attribute,
            'pluginOptions' => [
                'showRemove' => false,
                'showUpload' => false,
                'allowedFileExtensions'=>['pdf','png','jpg','jpeg'],
            ],
            'options' => ['multiple' => false]//,'accept' => 'image/*'
        ]);
        $html .= Html::error($model, $attribute);
        return $html;
    }

    public static function activeDrawingInput($model,$attribute,$field,$option){
        $html = '<img src="'.Url::to(['@web/img/editor.png']).'">';
        return $html;
    }

    public static function activeDatetimeInput($model,$attribute,$field,$option){
        $html = Html::activeLabel($model,$attribute);
        $html .= $field["ezf_field_help"];
        $html .= DMSDatetimeWidget::widget([
            'model'=>$model,
            'attribute'=>$attribute
        ]);
        return $html;
    }
    public static function activeProvince($model, $attribute, $field, $option){
        $dataProvince = \backend\modules\ezforms\models\EzformFields::find()->where(['ezf_field_sub_id'=>$field->ezf_field_id])
            ->andWhere(['ezf_field_label'=>1])
            ->One();
        $dataAmphur = \backend\modules\ezforms\models\EzformFields::find()->where(['ezf_field_sub_id'=>$field->ezf_field_id])
            ->andWhere(['ezf_field_label'=>2])
            ->One();
        $dataTumbon = \backend\modules\ezforms\models\EzformFields::find()->where(['ezf_field_sub_id'=>$field->ezf_field_id])
            ->andWhere(['ezf_field_label'=>3])
            ->One();

        $itemsProvince = EzformQuery::getProvince();
        $html = "<select class='form-control' style='width:300px;'><option>จังหวัด</option></select>";
        $html .= $field["ezf_field_help"];
        $html .= "<select class='form-control' style='width:300px;position: absolute;left: 400px;bottom: 22px;'><option>อำเภอ</option></select>";
        $html .= "<select class='form-control' style='width:300px;position: absolute;left: 800px;bottom: 24px;'><option>ตำบล</option></select>";
//        $html = "<div class='row'>";
//        $html = "<div class='col-md-4'>";
//        $html =  Select2::widget([
//                'options' => ['placeholder' => '�ѧ��Ѵ','id'=>'province','style'=>'width:250px;'],
//                'data' => ArrayHelper::map($itemsProvince,'PROVINCE_CODE','PROVINCE_NAME'),
//                'model' =>$model,
//                'attribute'=>$dataProvince["ezf_field_name"],
//                'pluginOptions' => [
//                    'allowClear' => true
//                ],
//            ]);
////        $html .= "</div>";
//        $html .= Select2::widget([
//            'options' => ['placeholder' => '�ѧ��Ѵ','id'=>'amphur','style'=>'width:250px;'],
//            'model' =>$model,
//            'attribute'=>$dataAmphur["ezf_field_name"],
//            'pluginOptions' => [
//                'allowClear' => true
//            ],
//        ]);
//        $html .= "</div>";
//        if(isset($dataTumbon)){
//            $html .= "<div class='col-md-4'>";
//            $html .= Select2::widget([
//                'options' => ['placeholder' => '�ѧ��Ѵ','id'=>'tumbon','style'=>'width:250px;'],
//                'model' =>$model,
//                'attribute'=>$dataTumbon["ezf_field_name"],
//                'pluginOptions' => [
//                    'allowClear' => true
//                ],
//            ]);
//            $html .= "</div>";
//        }
//        $html = "</div>";
        return $html;
    }
    public static function activeProvinceSave($form,$model, $attribute, $field, $option){
        $dataProvince = EzformFields::find()->where(['ezf_field_sub_id'=>$field->ezf_field_id])
            ->andWhere(['ezf_field_label'=>1])
            ->One();
        $dataAmphur = EzformFields::find()->where(['ezf_field_sub_id'=>$field->ezf_field_id])
            ->andWhere(['ezf_field_label'=>2])
            ->One();
        $dataTumbon = EzformFields::find()->where(['ezf_field_sub_id'=>$field->ezf_field_id])
            ->andWhere(['ezf_field_label'=>3])
            ->One();

        $model_form = Ezform::find()->where(['ezf_id'=>$field->ezf_id])->One();

        $itemsProvince = EzformQuery::getProvince();

        if(isset($_GET["dataid"])){
            $sql = "SELECT * FROM ".$model_form->ezf_table." WHERE `id` = '".$_GET["dataid"]."' ";
            $data = Yii::$app->db->createCommand($sql)->queryOne();
            $sqlAmphurEdit = "SELECT AMPHUR_NAME FROM `const_amphur` WHERE AMPHUR_CODE='".$data[$dataAmphur["ezf_field_name"]]."'";
//            echo $sqlAmphurEdit;
            $dataAmphurEdit = Yii::$app->db->createCommand($sqlAmphurEdit)->queryOne();
            $sqlTumbonEdit = "SELECT DISTRICT_NAME FROM `const_district` WHERE DISTRICT_CODE='".$data[$dataTumbon["ezf_field_name"]]."'";
//            echo $sqlTumbonEdit;
            $dataTumbonEdit = Yii::$app->db->createCommand($sqlTumbonEdit)->queryOne();
            $placeholderAmphur = $dataAmphurEdit["AMPHUR_NAME"];
            $placeholderTumbon = $dataTumbonEdit["DISTRICT_NAME"];


        }else{
            $placeholderAmphur = 'อำเภอ';
            $placeholderTumbon = 'ตำบล';
        }

	    //VarDumper::dump($model, 10, TRUE);
        //exit;
        //$model->ezf_field_name = $dataProvince["ezf_field_name"];
        //exit;

        $html = "<div class='col-md-4'>";
        $html .= '<div>';

        $html .= $field["ezf_field_help"];
        $html .=  Select2::widget([
            'options' => ['placeholder' => 'จังหวัด','id'=>$dataProvince["ezf_field_name"]],
            'data' => ArrayHelper::map($itemsProvince,'PROVINCE_CODE','PROVINCE_NAME'),
            'model' =>$model,
            'attribute'=>$dataProvince["ezf_field_name"],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]);
        $html .= '</div>';
        $html .= "</div>";
        //exit;
        $html .= "<div class='col-md-4'>";
        $html .= '<div>';

        $html .= $form->field($model,$dataAmphur["ezf_field_name"])->widget(DepDrop::classname(),[
            'type'=>  DepDrop::TYPE_SELECT2,
            'options'=>['id'=>$dataAmphur["ezf_field_name"]],
            'model'=>$model,
            'attribute'=>$dataAmphur["ezf_field_name"],
            'pluginOptions'=>[
                'depends'=>[$dataProvince["ezf_field_name"]],
                'placeholder'=>$placeholderAmphur,
                'url'=>Url::to(['/ezforms/province/genamphur'])
            ]
        ])->label(false);

        $html .= "</div>";
        $html .= "</div>";

        if(isset($dataTumbon)){
            $html .= "<div class='col-md-3'>";
            $html .= '<div>';

            $html .= $form->field($model,$dataTumbon["ezf_field_name"])->widget(DepDrop::classname(),[
                'type'=>  DepDrop::TYPE_SELECT2,
                'model'=>$model,
                'attribute'=>$dataAmphur["ezf_field_name"],
                'pluginOptions'=>[
                    'depends'=>[$dataAmphur["ezf_field_name"]],
                    'placeholder'=>$placeholderTumbon,
                    'url'=>Url::to(['/ezforms/province/gentumbon'])
                ]
            ])->label(false);
            $html .= "</div>";
            $html .= "</div>";
        }
        return $html;
    }
    public static function activeHospital($model, $attribute, $field, $option){
        $url = \yii\helpers\Url::to(['/ezforms/hospital/query']);
        $html = Html::activeLabel($model, $attribute);
        $html .= $field["ezf_field_help"];
        $html .= Select2::widget([ // set the initial display text
            'model' =>$model,
            'attribute'=>$attribute,
            'options' => ['placeholder' => 'Search for hospital ...'],
            'pluginOptions' => [
                'allowClear' => true,
                'minimumInputLength' => 3,
                'ajax' => [
                    'url' => $url,
                    'dataType' => 'json',
                    'data' => new JsExpression('function(params) { return {q:params.term}; }')
                ],
                'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                'templateResult' => new JsExpression('function(city) { return city.text; }'),
                'templateSelection' => new JsExpression('function (city) { return city.text; }'),
            ],
        ]);
        $html .= Html::error($model, $attribute);
        return $html;
    }
    public static function activeHospitalSave($form,$model, $attribute, $field, $option, $text=null){

        if($text) {
            $text = $text['ezf_field_label'].$text["ezf_field_help"];
        }else{
            $text = $field['ezf_field_label'].$field["ezf_field_help"];
        }

        $url = \yii\helpers\Url::to(['/ezforms/hospital/searchhospital']);
        $sql = "SELECT hcode, `name` FROM all_hospital_thai WHERE `hcode`='".$model[$attribute]."'";
        $data = Yii::$app->db->createCommand($sql)->queryOne();
        //$arrayMap = ArrayHelper::map($data,$data['code'],$data['name']);
        $html = $form->field($model, $attribute)->widget(Select2::classname(),[ // set the initial display text
            'model' =>$model,
            'attribute'=>$attribute,
            'data'=>array($data["hcode"]=>$data["hcode"]." : " . $data["name"]),
            'initValueText'=>array($data["hcode"]=> $data["hcode"]." : " . $data["name"]),
            'options' => ['placeholder' => 'Search for hospital ...'],
            'pluginOptions' => [
                'allowClear' => true,
                'minimumInputLength' => 3,
                'ajax' => [
                    'url' => $url,
                    'dataType' => 'json',
                    'data' => new JsExpression('function(params) { return {q:params.term}; }')
                ],
                'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                'templateResult' => new JsExpression('function(city) { return city.text; }'),
                'templateSelection' => new JsExpression('function (city) { return city.text; }'),
            ],
        ])->label($text);
//         "<<< SCRIPT
//            function format(state) {
//            if (!state.id) return state.text; // optgroup
//            src = '$url' + state.id.toLowerCase() + '.png'
//            return '<img class="flag" src="' + src + '"/>' + state.text;
//            }
//        SCRIPT;";
        return $html;
    }
    public static function activeLocalAdministration($model, $attribute, $field, $option){
        $url = \yii\helpers\Url::to(['/ezforms/hospital/query-local']);
        $html = Html::activeLabel($model, $attribute);
        $html .= $field["ezf_field_help"];
        $html .= Select2::widget([ // set the initial display text
            'model' =>$model,
            'attribute'=>$attribute,
            'options' => ['placeholder' => 'Search for local administration ...'],
            'pluginOptions' => [
                'allowClear' => true,
                'minimumInputLength' => 3,
                'ajax' => [
                    'url' => $url,
                    'dataType' => 'json',
                    'data' => new JsExpression('function(params) { return {q:params.term}; }')
                ],
                'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                'templateResult' => new JsExpression('function(city) { return city.text; }'),
                'templateSelection' => new JsExpression('function (city) { return city.text; }'),
            ],
        ]);
        $html .= Html::error($model, $attribute);
        return $html;
    }
    public static function activeLocalAdministrationSave($form,$model, $attribute, $field, $option, $text=null){

        if($text) {
            $text = $text['ezf_field_label'].$text["ezf_field_help"];
        }else{
            $text = $field['ezf_field_label'].$field["ezf_field_help"];
        }

        $url = \yii\helpers\Url::to(['/ezforms/hospital/searchlocal']);
        $sql = "SELECT orgcode, `name`, amphur, province FROM all_hos_org WHERE `orgcode`='".$model[$attribute]."'";
        $data = Yii::$app->db->createCommand($sql)->queryOne();
        //$arrayMap = ArrayHelper::map($data,$data['code'],$data['name']);
        $html = $form->field($model, $attribute)->widget(Select2::classname(),[ // set the initial display text
            'model' =>$model,
            'attribute'=>$attribute,
            'data'=>array($data["orgcode"]=>$data["orgcode"]." : " . $data["name"]. " อ.{$data["amphur"]} จ.{$data["province"]} "),
            'initValueText'=>array($data["orgcode"]=> $data["orgcode"]." : " . $data["name"]. " อ.{$data["amphur"]} จ.{$data["province"]} "),
            'options' => ['placeholder' => 'Search for local administration ...'],
            'pluginOptions' => [
                'allowClear' => true,
                'minimumInputLength' => 3,
                'ajax' => [
                    'url' => $url,
                    'dataType' => 'json',
                    'data' => new JsExpression('function(params) { return {q:params.term}; }')
                ],
                'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                'templateResult' => new JsExpression('function(city) { return city.text; }'),
                'templateSelection' => new JsExpression('function (city) { return city.text; }'),
            ],
        ])->label($text);
//         "<<< SCRIPT
//            function format(state) {
//            if (!state.id) return state.text; // optgroup
//            src = '$url' + state.id.toLowerCase() + '.png'
//            return '<img class="flag" src="' + src + '"/>' + state.text;
//            }
//        SCRIPT;";
        return $html;
    }
//icd9
public static function activeIcd9($model, $attribute, $field, $option){
    $url = \yii\helpers\Url::to(['/ezforms/icd9/query']);
    $html = Html::activeLabel($model, $attribute);
    $html .= $field["ezf_field_help"];
    $html .= Select2::widget([ // set the initial display text
        'model' =>$model,
        'attribute'=>$attribute,
        'options' => ['placeholder' => 'Search for ICD9...'],
        'pluginOptions' => [
            'allowClear' => true,
            'minimumInputLength' => 1,
            'ajax' => [
                'url' => $url,
                'dataType' => 'json',
                'data' => new JsExpression('function(params) { return {q:params.term}; }')
            ],
            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
            'templateResult' => new JsExpression('function(city) { return city.text; }'),
            'templateSelection' => new JsExpression('function (city) { return city.text; }'),
        ],
    ]);
    $html .= Html::error($model, $attribute);
    return $html;
}
public static function activeIcd9Save($form,$model, $attribute, $field, $option, $text=null){

    if($text) {
        $text = $text['ezf_field_label'].$text["ezf_field_help"];
    }else{
        $text = $field['ezf_field_label'].$field["ezf_field_help"];
    }

    $url = \yii\helpers\Url::to(['/ezforms/icd9/searchicd9']);
    $sql = "SELECT * FROM icd9 WHERE `code`='".$model[$attribute]."'";
    $data = Yii::$app->db->createCommand($sql)->queryOne();
    //$arrayMap = ArrayHelper::map($data,$data['code'],$data['name']);
    $html = $form->field($model, $attribute)->widget(Select2::classname(),[ // set the initial display text
        'model' =>$model,
        'attribute'=>$attribute,
        'data'=>array($data["code"]=>$data["code"]." : " . $data["name"]),
        'initValueText'=>array($data["code"]=> $data["code"]." : " . $data["name"]),
        'options' => ['placeholder' => 'Search for ICD9...'],
        'pluginOptions' => [
            'allowClear' => true,
            'minimumInputLength' => 1,
            'ajax' => [
                'url' => $url,
                'dataType' => 'json',
                'data' => new JsExpression('function(params) { return {q:params.term}; }')
            ],
            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
            'templateResult' => new JsExpression('function(city) { return city.text; }'),
            'templateSelection' => new JsExpression('function (city) { return city.text; }'),
        ],
    ])->label($text);
//         "<<< SCRIPT
//            function format(state) {
//            if (!state.id) return state.text; // optgroup
//            src = '$url' + state.id.toLowerCase() + '.png'
//            return '<img class="flag" src="' + src + '"/>' + state.text;
//            }
//        SCRIPT;";
    return $html;
}
//endicd9

public static function activeIcd10($model, $attribute, $field, $option){
    $url = \yii\helpers\Url::to(['/ezforms/icd10/query']);
    $html = Html::activeLabel($model, $attribute);
    $html .= $field["ezf_field_help"];
    $html .= Select2::widget([ // set the initial display text
        'model' =>$model,
        'attribute'=>$attribute,
        'options' => ['placeholder' => 'Search for ICD10 ...'],
        'pluginOptions' => [
            'allowClear' => true,
            'minimumInputLength' => 1,
            'ajax' => [
                'url' => $url,
                'dataType' => 'json',
                'data' => new JsExpression('function(params) { return {q:params.term}; }')
            ],
            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
            'templateResult' => new JsExpression('function(city) { return city.text; }'),
            'templateSelection' => new JsExpression('function (city) { return city.text; }'),
        ],
    ]);
    $html .= Html::error($model, $attribute);
    return $html;
}
public static function activeIcd10Save($form,$model, $attribute, $field, $option, $text=null){

    if($text) {
        $text = $text['ezf_field_label'].$text["ezf_field_help"];
    }else{
        $text = $field['ezf_field_label'].$field["ezf_field_help"];
    }

    $url = \yii\helpers\Url::to(['/ezforms/icd10/searchicd10']);
    $sql = "SELECT * FROM icd10 WHERE `code`='".$model[$attribute]."'";
    $data = Yii::$app->db->createCommand($sql)->queryOne();
    //$arrayMap = ArrayHelper::map($data,$data['code'],$data['name']);
    $html = $form->field($model, $attribute)->widget(Select2::classname(),[ // set the initial display text
        'model' =>$model,
        'attribute'=>$attribute,
        'data'=>array($data["code"]=>$data["code"]." : " . $data["name"]),
        'initValueText'=>array($data["code"]=> $data["code"]." : " . $data["name"]),
        'options' => ['placeholder' => 'Search for ICD10...'],
        'pluginOptions' => [
            'allowClear' => true,
            'minimumInputLength' => 1,
            'ajax' => [
                'url' => $url,
                'dataType' => 'json',
                'data' => new JsExpression('function(params) { return {q:params.term}; }')
            ],
            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
            'templateResult' => new JsExpression('function(city) { return city.text; }'),
            'templateSelection' => new JsExpression('function (city) { return city.text; }'),
        ],
    ])->label($text);
//         "<<< SCRIPT
//            function format(state) {
//            if (!state.id) return state.text; // optgroup
//            src = '$url' + state.id.toLowerCase() + '.png'
//            return '<img class="flag" src="' + src + '"/>' + state.text;
//            }
//        SCRIPT;";
    return $html;
}
//endicd10





    public static function componentValidate($form,$model,$attribute,$field,$options, $text=null){
        if($field->ezf_component) {
            $ezformComponents = EzformQuery::getFieldComponent($field->ezf_component);
        }else{
            $ezformComponents = EzformQuery::getFieldComponent($field['ezf_component']);
        }
        $field_key = EzformFields::find()->where(['ezf_field_id' => $ezformComponents->field_id_key])->one();
        //VarDumper::dump($field_key, 10, true);

        if (!empty($ezformComponents->field_id_desc)) {
            $field_desc_array = explode(',', $ezformComponents->field_id_desc);
        }
        //

        $ezform = EzformQuery::getForm($ezformComponents->ezf_id);

        $sql =" WHERE 1"; $field_desc_list = '';
        if ($ezformComponents->comp_id == 100000 || $ezformComponents->comp_id == 100001) {
            //
            if(Yii::$app->keyStorage->get('frontend.domain')=='cascap.in.th'){
                $field_desc_list = "COALESCE(SUBSTR(cid,-4), '-'), ' ',";
            }
        }

        //if find by target
        if($ezformComponents->find_target && $ezformComponents->special){
            $sql .= " AND ptid = '".base64_decode($_GET['target'])."' AND rstat <> 3";
        }else if($ezformComponents->find_target){
            $sql .= " AND target = '".base64_decode($_GET['target'])."' AND rstat <> 3";
        }

        //if find by site
        if($ezformComponents->find_bysite){
            $sql .= " AND rstat <> 3 AND xsourcex = '".Yii::$app->user->identity->userProfile->sitecode."'";
        }

        //
        if(count($field_desc_array)){
            foreach ($field_desc_array as $value) {
                if (!empty($value)) {
                    $field_desc = EzformFields::find()->where(['ezf_field_id' => $value])->one();
                    $field_desc_list .= 'COALESCE('.$field_desc->ezf_field_name.", '-'), ' ',";
                }
            }
        }else{
            $field_desc_list .= "COALESCE('ยังไม่ได้ระบุ Component Description', '-'), ' ',";
        }

        $field_desc_list = substr($field_desc_list, 0 ,-6);

        //VarDumper::dump(Yii::$app->db->createCommand('SELECT *, CONCAT('.$field_desc_list.') AS field_desc FROM '.$ezform->ezf_table)->queryAll(), 10, true);

        $sql = 'SELECT '.($field_key->ezf_field_name).' as id, CONCAT('.$field_desc_list.') AS text FROM '.($ezform->ezf_table).$sql;
        //$dynamic_datas = ArrayHelper::map(Yii::$app->db->createCommand($sql)->queryAll(), 'id', 'field_desc');
        //เตรียม SQL

        if($model->$attribute || ($ezformComponents->comp_id == 100000 || $ezformComponents->comp_id == 100001)) {
            //initial value
            $sqlInti = '';
            $sqlInti .= 'SELECT '.($field_key->ezf_field_name).' as id, CONCAT('.$field_desc_list.') AS text FROM '.($ezform->ezf_table).' WHERE 1 ';;

            //แบบตัวเลือกเดียว
            if ($ezformComponents->comp_select != 2) {

                if ($ezformComponents->comp_id == 100000 || $ezformComponents->comp_id == 100001) {
                    //VarDumper::dump($sqlInti,10,true); exit;
                    if (!$model->$attribute && $form->attributes['rstat'] == 0) {
                        $sqlInti .= " AND user_id = '" . (Yii::$app->user->id) . "'";
                        $model->$attribute = Yii::$app->user->id;
                    }else{
                        $sqlInti .= " AND user_id = '" . ($model->$attribute) . "'";
                    }

                } else {
                    $sqlInti .= " AND " . ($field_key->ezf_field_name) . " = '" . ($model->$attribute) . "'";
                }

                $initValueSingle = Yii::$app->db->createCommand($sqlInti)->queryOne();
                $initValueSingle = $initValueSingle['text'];
                //VarDumper::dump($sqlInti,10,true); exit;

            } //แบบหลายตัวเลือก
            else if ($ezformComponents->comp_select == 2) {

                if ($ezformComponents->comp_id == 100000 || $ezformComponents->comp_id == 100001) {
                    //VarDumper::dump($form->attributes['rstat'],10,true); exit;
                    if (!$model->$attribute && $form->attributes['rstat'] == 0) {
                        $sqlInti .= " AND user_id = '" . Yii::$app->user->id . "'";
                    } else
                        $sqlInti .= " AND user_id in(" . ($model->$attribute) . ")";

                } else {
                    $sqlInti .= " AND " . ($field_key->ezf_field_name) . " in(" . ($model->$attribute) . ")";
                }
                $result = Yii::$app->db->createCommand($sqlInti);
                $initValueTag = ArrayHelper::map($result->queryAll(), 'id', 'text');
                $initValueTagInit = $result->queryColumn();
            }
        }

        //display
        if($text) {
            $text = $text['ezf_field_label'].$text["ezf_field_help"];
        }else{
            $text = $field['ezf_field_label'].$field["ezf_field_help"];
        }
        //link add doctor
        if($ezformComponents->comp_id=='1446783430006597800'){
            $text .= Html::a(' | <span class="text text-danger">หาไม่พบ คลิกที่นี่เพื่อเพิ่มรายชื่อแพทย์</span>', '/adddoctor', ['target'=>'_blank']);
        }

        //เตรียมส่งให้ sql query string
        $sql .= " AND CONCAT(".$field_desc_list.") ";

        //single tag
        if($ezformComponents->comp_select != 2) {
            $html = $form->field($model, $attribute)->widget(Select2::classname(), [
                'initValueText' => $initValueSingle,
                'options' => ['placeholder' => 'เลือกรายการ ...',],
                'pluginOptions' => [
                    'allowClear' => true,
                    //'maximumInputLength' => 3,
                    'ajax' => [
                        'url' => Url::to('/ezforms/ezform/find-component'),
                        'method' => 'POST',
                        'dataType' => 'json',
                        'delay' => 250,
                        'data' => new JsExpression('function(params) { return {q:params.term, q_str:\''.base64_encode($sql).'\'};}'),
                        'cache' => true
                    ],
                ],
                'pluginEvents' => [
                    'change' => "function() { console.log('change = '+$(this).val()); }",
                    //"select2:select" => "function() { console.log('select = '+$(this).val()); }",
                ]
            ]);
        }else if($ezformComponents->comp_select == 2){
            // Tagging support Multiple
            $html = Html::label($text);
            $html .= Select2::widget([
                'name' => 'SDDynamicModel['.($attribute).'][]',
                'value' => $initValueTagInit, // initial value
                'data' => $initValueTag,
                'options' => ['placeholder' => 'เลือกรายการ ...', 'multiple' => true],
                'pluginOptions' => [
                    'tags' => false,
                    //'maximumInputLength' => 3,
                    'ajax' => [
                        'url' => Url::to('/ezforms/ezform/find-component'),
                        'method' => 'POST',
                        'dataType' => 'json',
                        'delay' => 250,
                        'data' => new JsExpression('function(params) { return {q:params.term, q_str:\''.base64_encode($sql).'\'}; }'),
                        'cache' => true
                    ],
                ],
            ]);
        }

        return $html;
    }
    public static function snomedValidate($form,$model,$attribute,$field,$options, $text=null){
        if($text) {
            $text = $text['ezf_field_label'].$text["ezf_field_help"];
        }else{
            $text = $field['ezf_field_label'].$field["ezf_field_help"];
        }

        $url = \yii\helpers\Url::to(['/ezforms/snomed/searchsnomed']);

        $html = $form->field($model, $attribute)->widget(Select2::classname(),[ // set the initial display text
            'model' =>$model,
            'attribute'=>$attribute,
            'options' => ['placeholder' => 'Search for a snomed ...'],
            'pluginOptions' => [
                'allowClear' => true,
                'minimumInputLength' => 3,
                'ajax' => [
                    'url' => $url,
                    'dataType' => 'json',
                    'data' => new JsExpression('function(params) { return {q:params.term}; }')
                ],
                'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                'templateResult' => new JsExpression('function(city) { return city.text; }'),
                'templateSelection' => new JsExpression('function (city) { return city.text; }'),
            ],
        ])->label($text);
        return $html;
    }
    public static function fileinputValidate($form,$model,$attribute,$field,$options, $text=null){
        if($text) {
            $text = $text['ezf_field_label'].$text["ezf_field_help"];
        }else{
            $text = $field['ezf_field_label'].$field["ezf_field_help"];
        }

        $path = Yii::$app->homeUrl."/uploads/imgform/";

        $model_form = Ezform::find()->where(['ezf_id'=>$field->ezf_id])->One();

        $sql = "SELECT * FROM ".$model_form->ezf_table." WHERE id='".$model->id."' ";

        $dataFileInput = Yii::$app->db->createCommand($sql)->queryOne();


        $fileImg = $dataFileInput[$attribute];
        $imgPath = Yii::getAlias('@web/fileinput').'/';//Yii::getAlias('@backendUrl') . '/fileinput/';
        if(isset($fileImg) && !empty($fileImg)){
	    $active = false;
	    $items = [];
        //file_active = 0 new upload
        //file_active = 1 file confirm
        //file_active = 9 file waiting for approve
        //file_active = -9 file disable (query tools)
        //file_active = -10 file disable (input data)
	    $fileItemActive = \backend\modules\ezforms\models\FileUpload::find()->where('tbid=:tbid and ezf_field_id=:ezf_field_id and ezf_id=:ezf_id and mode = :mode AND file_active =1', [':tbid'=>$model->id, ':ezf_id'=>$field->ezf_id, ':ezf_field_id'=>$field->ezf_field_id, ':mode' => ($options['query_tools_input'] ? $options['query_tools_input'] : '1')])->orderBy('created_at desc')->one();
	    if($fileItemActive){
		$items[] = $fileItemActive['file_name'];
		$active = true;
	    } else {
		$fileItem = \backend\modules\ezforms\models\FileUpload::find()->where('tbid=:tbid and ezf_field_id=:ezf_field_id and ezf_id=:ezf_id and mode = :mode', [':tbid'=>$model->id, ':ezf_id'=>$field->ezf_id, ':ezf_field_id'=>$field->ezf_field_id, ':mode' => ($options['query_tools_input'] ? $options['query_tools_input'] : '1')])->orderBy('created_at desc')->one();
		if($fileItem){
		    $items[] = $fileItem['file_name'];
		}
	    }

            $initialPreview = [];
            foreach ($items as $item) {
		$img = $imgPath.$item;
		$img_old = $img;
		$ext = strtolower(pathinfo($item, PATHINFO_EXTENSION));
		if($ext=='pdf'){
		    $img = Yii::getAlias('@storageUrl').'/source/pdf_icon.png';
		}
                $initialPreview[] = Html::img($img, ['data-filename'=>$img_old,'class'=>'file-preview-image','alt'=>$field->ezf_field_label,'title'=>$field->ezf_field_label]);
            }
            $html='';
            if($options['query_tools_render']!=1) {
                $html .= $form->field($model, $attribute . '[]')->widget(FileInput::classname(), [
                    //'disabled' => $options['query_tools_input'] ? false : $active,
                    'pluginOptions' => [
                        'previewFileType' => 'image',
                        'initialPreview' => $initialPreview,
                        'overwriteInitial' => true,
                        'showRemove' => false,
                        'showUpload' => false,
                        'allowedFileExtensions' => ['pdf', 'png', 'jpg', 'jpeg'],
			'maxFileSize' => 5000,
                    ],
                    'options' => ['multiple' => true]//,'accept' => 'image/*'
                ])->label($text);
            }
	    $getSite = isset($dataFileInput['hsitecode']);
	    $sitecode = Yii::$app->user->identity->userProfile->sitecode;
	    if(!$getSite || ($getSite && $getSite == $sitecode)){
		$fileModel = new \backend\modules\ezforms\models\FileUploadSearch();
		$fileModel->ezf_id = $field->ezf_id;
		$fileModel->ezf_field_id = $field->ezf_field_id;
		$fileModel->tbid = $model->id;
        $fileModel->mode = $options['query_tools_input'] ? $options['query_tools_input'] : '1';
		$dataProvider = $fileModel->search();

		$view = Yii::$app->getView();
		$html .= $view->render('//inputdata/_files_upload', ['dataProvider'=>$dataProvider, 'field'=>$field]);

	    }

	   // $html .= ;
        }else {
            $html = '';
            if ($options['query_tools_render']!=1) {
                $html .= $form->field($model, $attribute . '[]')->widget(FileInput::classname(), [
                    'pluginOptions' => [
                        'allowedFileExtensions' => ['pdf', 'png', 'jpg', 'jpeg'],
                        'showRemove' => false,
                        'showUpload' => false,
                    ],
                    'options' => ['multiple' => true]
                ])->label($text);
            }
        }
//        $html .= Html::img($imgPath);
        return $html;
    }
    public static function datetimeValidate($form,$model,$attribute,$field,$options){
        $html = $form->field($model,$attribute)->widget(DMSDatetimeWidget::classname(),[
//                    'mask'=>'9999-99-99 99:99'
        ])->label($field['ezf_field_label'].$field["ezf_field_help"]);
        return $html;
    }
    public static function activeDateInput($model,$attribute,$field,$option){
        if($field["ezf_field_label"] == ""){
            $html = DMSDateWidget::widget([
                'model'=>$model,
                'attribute'=>$attribute
            ]);
            $html .= Html::error($model, $attribute);
        }else{
            $html = Html::activeLabel($model,$attribute);
            $html .= $field["ezf_field_help"];
            $html .= DMSDateWidget::widget([
                'model'=>$model,
                'attribute'=>$attribute
            ]);
            $html .= Html::error($model, $attribute);
        }
        return $html;

    }
    public static function activeDateInputSave($form, $model, $attribute, $field, $option){
        $date = $model->$attribute;
        //VarDumper::dump($form, 10, true);
        //VarDumper::dump($model, 10, true);
        //VarDumper::dump($attribute, 10, true);
        //VarDumper::dump($field, 10, true);
        //exit;

        if($date+0) {
            $explodeDate = explode('-', $date);
            $formateDate = $explodeDate[2] . "/" . $explodeDate[1] . "/" . ($explodeDate[0] + 543);
            $model->$attribute = $formateDate;
        }else{
            $model->$attribute = null;
        }
        if($field["ezf_field_label"] == ""){
            $html = $form->field($model, $attribute)->widget(DMSDateWidget::className(), [
            ])->label(false);
        }else{
            $html = $form->field($model, $attribute)->widget(DMSDateWidget::className(), [
            ])->label($field['ezf_field_label'].$field["ezf_field_help"]);
        }
        return $html;
    }
    public static function activeTimeInput($model,$attribute,$field,$option){
        $html = Html::activeLabel($model, $attribute);
        $html .= $field["ezf_field_help"];
        $html .= DMSTimeWidget::widget([
            'model'=>$model,
            'attribute'=>$attribute
        ]);
        $html .= Html::error($model, $attribute);
        return $html;

    }
    public static function activeTimeInputSave($form,$model,$attribute,$field,$option){
        $html = $form->field($model, $attribute)->widget(DMSTimeWidget::classname(), [])->label($field['ezf_field_label'].$field["ezf_field_help"]);
        $html .= Html::error($model, $attribute);
        return $html;
    }
    public static function activeText($model,$attribute,$field,$option)
    {
        if ($field["ezf_field_label"] == "") {
            $html = Html::activeInput('text', $model, $attribute, [
                'label' => '',
                'class' => 'form-control'
            ]);
        }else{
            $html = Html::activeLabel($model,$attribute, $option['labelOptions']);
            $html .= $field["ezf_field_help"];
            $html .= Html::activeTextInput($model, $attribute, [
                'class' => 'form-control'
            ]);
        }
        return $html;
    }
    public static function textInputValidate($form, $model, $attribute, $field, $option){
        if($field["ezf_field_label"]==""){
            $html = $form->field($model,$attribute, ['labelOptions'=>$option['labelOptions']])->textInput($option)->label(false);
        }
        else{
            $html = $field["ezf_field_help"];
            $html .= $form->field($model,$attribute, ['labelOptions'=>$option['labelOptions']])->textInput($option);
        }
        return $html;
    }
    public static function activeParagraph($model,$attribute,$field,$option)
    {
        /*
        if ($field["ezf_field_label"] == "") {
            $html = Html::activeTextarea($model, $attribute, [
                'label' => '',
                'rows' => $field->ezf_field_rows,
                'class' => 'form-control'
            ]);
        }else{
            $html = Html::activeLabel($model,$attribute);
            $html .= $field["ezf_field_help"];
            $html .= Html::activeTextarea($model, $attribute, [
                'rows' =>  $field->ezf_field_rows,
                'class' => 'form-control'
            ]);
        }
        */
        if($field['ezf_field_text_html']==1) {
            $html = Html::activeLabel($model, $attribute);
            $html .= $field["ezf_field_help"];
            $html .= \dosamigos\tinymce\TinyMce::widget([
                'name' => $attribute,
                'options' => [
                    'rows' =>  $field->ezf_field_rows,
                    'class' => 'form-control'
                ],
                'language' => 'th_TH',
                'clientOptions' => [
                    'fontsize_formats' => '8pt 9pt 10pt 11pt 12pt 26pt 36pt',
                    'plugins' => [
                        "advlist autolink lists link image charmap print preview hr anchor pagebreak",
                        "searchreplace wordcount visualblocks visualchars code fullscreen",
                        "insertdatetime media nonbreaking save table contextmenu directionality",
                        "emoticons template paste textcolor colorpicker textpattern",
                    ],
                    'toolbar' => "undo redo | styleselect fontsizeselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media | forecolor backcolor emoticons",
                    'content_css' => Yii::getAlias('@backendUrl') . '/css/bootstrap.min.css',
                    'image_advtab' => true,
                    'filemanager_crossdomain' => true,
                    'external_filemanager_path' => Yii::getAlias('@storageUrl') . '/filemanager/',
                    'filemanager_title' => 'Responsive Filemanager',
                    'external_plugins' => array('filemanager' => Yii::getAlias('@storageUrl') . '/filemanager/plugin.min.js')
                ]
            ]);
        }else{
            $html = Html::activeLabel($model,$attribute);
            $html .= $field["ezf_field_help"];
            $html .= Html::activeTextarea($model, $attribute, [
                'rows' =>  $field->ezf_field_rows,
                'class' => 'form-control'
            ]);
        }
        return $html;
    }
    public static function paragraphValidate($form, $model, $attribute, $field, $option){
        /*
        if($field["ezf_field_label"]==""){
            //$html = $form->field($model,$attribute)->textarea($option)->label(false);
        }
        else{
            //$html = $form->field($model,$attribute)->textarea($option)->label($field['ezf_field_label'].$field["ezf_field_help"]);
        }
        */
        if($field['ezf_field_text_html']==1) {
//            $views = new \yii\web\View();
//            $views->registerJs("CKEDITOR.plugins.addExternal('uploadimage', '/ckeditor/uploadimage/plugin.js', '');");
//            
//            $html = $form->field($model, $attribute)->widget(\dosamigos\ckeditor\CKEditor::className(), [ 
//                'preset' => 'basic',
//                'clientOptions' => [
//                    'filebrowserBrowseUrl' => Yii::getAlias('@storageUrl') . '/filemanager/dialog.php?type=2&editor=ckeditor&fldr=',
//                    'filebrowserUploadUrl' => Yii::getAlias('@storageUrl') . '/filemanager/dialog.php?type=2&editor=ckeditor&fldr=',
//                    'filebrowserImageBrowseUrl' => Yii::getAlias('@storageUrl') . '/filemanager/dialog.php?type=1&editor=ckeditor&fldr=',
//                    'filebrowserImageUploadUrl' => Yii::getAlias('@storageUrl') . '/filemanager/dialog.php?type=1&editor=ckeditor&fldr=',
//                    'filebrowserWindowWidth' => 800,
//                    'filebrowserWindowHeight' => 600,
//                   
//                ]
//                ]);
            
            
            $html = $form->field($model, $attribute)->widget(\vova07\imperavi\Widget::className(), [
                    'options' => $option,
		    'settings' => [
			    'lang' => 'th',
			    'minHeight' => 50,
			    'imageManagerJson' => Url::to(['/ezforms/ezform/images-get']),
                            'fileManagerJson' => Url::to(['/ezforms/ezform/files-get']),
			    'imageUpload' => Url::to(['/ezforms/ezform/image-upload']),
			    'fileUpload' => Url::to(['/ezforms/ezform/file-upload']),
			    'plugins' => [
                                'fontcolor',
                                'fontfamily',
                                'fontsize',
				'textdirection',
                                'textexpander',
                                'counter',
                                'table',
                                'definedlinks',
                                'video',
				'imagemanager',
                                'filemanager',
                                'limiter',
                                'fullscreen',
			    ]
			],
//                        'plugins' => [
//                            'my-custom-plugin' => 'app\assets\MyPluginBundle'
//                        ]
		])->label($field['ezf_field_label'].$field["ezf_field_help"]);
            
//            $html = $form->field($model, $attribute)->widget(\dosamigos\tinymce\TinyMce::className(), [
//                'options' => $option,
//                'language' => 'th_TH',
//                'clientOptions' => [
//                    'fontsize_formats' => '8pt 9pt 10pt 11pt 12pt 26pt 36pt',
//                    'plugins' => [
//                        "advlist autolink lists link image charmap print preview hr anchor pagebreak",
//                        "searchreplace wordcount visualblocks visualchars code fullscreen",
//                        "insertdatetime media nonbreaking save table contextmenu directionality",
//                        "emoticons template paste textcolor colorpicker textpattern",
//                    ],
//                    'toolbar' => "undo redo | styleselect fontsizeselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media | forecolor backcolor emoticons",
//                    'content_css' => Yii::getAlias('@backendUrl') . '/css/bootstrap.min.css',
//                    'image_advtab' => true,
//                    'filemanager_crossdomain' => true,
//                    'external_filemanager_path' => Yii::getAlias('@storageUrl') . '/filemanager/',
//                    'filemanager_title' => 'Responsive Filemanager',
//                    'external_plugins' => array('filemanager' => Yii::getAlias('@storageUrl') . '/filemanager/plugin.min.js')
//                ]
//            ])->label($field['ezf_field_label'].$field["ezf_field_help"]);
            
        }else{
            $html = $form->field($model,$attribute)->textarea($option)->label($field['ezf_field_label'].$field["ezf_field_help"]);
        }
        return $html;
    }
    public static function activeScale($model,$attribute,$field,$option){
        $modelchoice = EzformChoice::find()->where('ezf_field_id = :ezf_field_id',[':ezf_field_id'=>$field->ezf_field_id])->all();
        $modelsubfield = EzformFields::find()->where('ezf_field_sub_id = :ezf_field_sub_id',[':ezf_field_sub_id'=>$field->ezf_field_id])->all();
        $html = "<b>".$field->ezf_field_label."</b>";
        $html .= $field["ezf_field_help"];
        $html .= "<table class='table table-bordered'>";
        $html .= "<tr>";
        $html .= "<td></td>";
        foreach($modelchoice as $choice){

            $html .= "<td>";
            $html .= $choice["ezf_choicelabel"];
            $html .= "</td>";

        }
        $i=1;
        foreach($modelsubfield as $subfield){
            $modelsubchoice = EzformChoice::find()->where('ezf_field_id = :ezf_field_id',[':ezf_field_id'=>$subfield->ezf_field_sub_id])->all();

            $html .= "<tr>";
            $html .= "<td>";
            $html .= $i." . ".$subfield["ezf_field_label"];
            $html .= "</td>";
            $i++;
            foreach($modelsubchoice as $subchoice){
                $html .= "<td>";
                $html .= HTML::activeRadio($model,$attribute,['label'=>'']);
                $html .= "</td>";
            }
            $html .= "</tr>";
        }
        $html .= "</td>";
        $html .= "</tr>";
        $html .= "</table>";
//        $html .= "<ins style='font-size: 17px;'>คำอธิบายตาราง</ins><br><br>";
//        foreach($modelchoice as $choice){
//            $html .= "<p>".$choice["ezf_choicelabel"]." คือตัวแปร ".$choice["ezf_choicevalue"]."</p>";
//        }
        return $html;

    }
    public static function scaleValidate($form, $model, $attribute, $field, $option)
    {
        $option = [];
        $modelchoice = EzformChoice::find()->where('ezf_field_id = :ezf_field_id',[':ezf_field_id'=>$field->ezf_field_id])->all();

        $modelsubfield = EzformFields::find()->where('ezf_field_sub_id = :ezf_field_sub_id',[':ezf_field_sub_id'=>$field->ezf_field_id])->all();
        $html = "<b>".$field->ezf_field_label."</b>";
        $html .= $field["ezf_field_help"];
        $html .= "<table class='table table-bordered'>";
        $html .= "<tr>";
        $html .= "<td></td>";
        foreach($modelchoice as $choice){

            $html .= "<td style='text-align: center'>";
            $html .= $choice["ezf_choicelabel"];
            $html .= "</td>";
        }
        $html .= "</td>";
        $html .= "</tr>";
        $html .= "<tr>";
        $html .= "</tr>";
        $i=1;
        foreach($modelsubfield as $subfield){
            $modelsubchoice = EzformChoice::find()->where('ezf_field_id = :ezf_field_id',[':ezf_field_id'=>$subfield->ezf_field_sub_id])->all();

            $html .= "<tr>";
            $html .= "<td>";
            $html .= $i." . ".$subfield["ezf_field_label"].((string)$form->attributes['rstat']=='annotated' ? (' <code>'.$subfield["ezf_field_name"].'</code>') : null);
            $html .= "</td>";
            $i++;
            foreach($modelsubchoice as $subchoice){

                $annotatedVal =false;
                if((string)$form->attributes['rstat']=='annotated'){
                    $annotatedVal = '<sub style="color:green;">'.$subchoice['ezf_choicevalue'].'</sub> ';
                }

                $html .= '<td style="text-align: center">'.Html::activeRadio($model, $subfield["ezf_field_name"], ['label'=>$annotatedVal, 'value'=>$subchoice['ezf_choicevalue'], 'uncheck' => null]).'</td>';
                /*
                $html .= $form->field($model, $subfield["ezf_field_name"])->radioList(
                    [$subchoice["ezf_choicevalue"]=>$subchoice["ezf_choicelabel"]],
                    [
                        'item' => function($index, $label, $name, $checked, $value) {

                            return "<td>".Html::radio($name, $checked, ['label' =>'' , 'value' => $value])."</td>";
                        }
                    ]
                )->label(false);
                */
            }
            $html .= "</tr>";
        }
        $html .= "</table>";
//        $html .= "<ins style='font-size: 17px;'>คำอธิบายตาราง</ins><br><br>";
//        foreach($modelchoice as $choice){
//            $html .= "<p>".$choice["ezf_choicelabel"]." คืิอตัวแปร" .$choice["ezf_choicevalue"]."</p>";
//        }
        return $html;
    }
    public static function activeGrid($model,$attribute,$field,$option){
        $modelchoice = EzformChoice::find()->where('ezf_field_id = :ezf_field_id',[':ezf_field_id'=>$field->ezf_field_id])->all();

        $modelquestion = EzformFields::find()->where('ezf_field_sub_id = :ezf_field_id',[':ezf_field_id'=>$field->ezf_field_id])->all();


        $html = "<b>".$field->ezf_field_label."</b>";

        foreach($modelquestion as $question){
            if($question["ezf_field_label"]!==''){
                $havequestion = 1;
            }else{
                $havequestion = 0;
            }
        }

        $html .= "<table class='table'>";
        $html .= "<thead>";
        $html .= "<tr>";
        if($havequestion==1){
            $html .= "<th></th>";
        }else{
            $html .= "";
        }

        foreach($modelchoice as $choice){
            $html .= "<th>".$choice["ezf_choicelabel"]."</th>";
        }
        $html .= "<tr>";
        $html .= "</thead>";
        $html .= "<tbody>";
        if($havequestion==1){
            foreach($modelquestion as $question){
                $modelvalue = EzformFields::find()->where('ezf_field_sub_id = :ezf_field_id',[":ezf_field_id"=>$question["ezf_field_id"]])->all();
                $html .= "<tr>";
                $html .= "<td>".$question["ezf_field_label"]."</td>";
                foreach($modelvalue as $value){
                    if($value["ezf_field_type"]=="251"){
                        $html .= "<td><input type='text' class='form-control'></td>";
                    }else if($value["ezf_field_type"]=="252"){
                        $html .= "<td><textarea class='form-control'></textarea></td>";
                    }else if($value["ezf_field_type"]=="253"){
                        $html .= "<td><input type='text' class='form-control' placeholder='___-__-__'></td>";
                    }else if($value["ezf_field_type"]=="254") {
                        $html .= "<td><label><input type='checkbox'> ".$value["ezf_field_label"]."</label></td>";
                    }
                }
                $html .= "<tr>";
            }
        }else{
            foreach($modelquestion as $question){
                $modelvalue = EzformFields::find()->where('ezf_field_sub_id = :ezf_field_id',[":ezf_field_id"=>$question["ezf_field_id"]])->all();
                $html .= "<tr>";
                foreach($modelvalue as $value){

                    if($value["ezf_field_type"]=="251"){
                        $html .= "<td><input type='text' class='form-control'></td>";
                    }else if($value["ezf_field_type"]=="252"){
                        $html .= "<td><textarea class='form-control'></textarea></td>";
                    }else if($value["ezf_field_type"]=="253"){
                        $html .= "<td><input type='text' class='form-control' placeholder='___-__-__'></td>";
                    }else if($value["ezf_field_type"]=="254") {
                        $html .= "<td><label><input type='checkbox'> ".$value["ezf_field_label"]."</label></td>";
                    }
                }
                $html .= "<tr>";
            }
        }
        $html .= "</tbody>";
        $html .= "</table>";
        return $html;
    }

    public static function gridValidate($form, $model, $attribute, $field, $option)
    {
        $modelchoice = EzformChoice::find()->where('ezf_field_id = :ezf_field_id',[':ezf_field_id'=>$field->ezf_field_id])->all();
        $modelquestion = EzformFields::find()->where('ezf_field_sub_id = :ezf_field_id',[':ezf_field_id'=>$field->ezf_field_id])->all();
        foreach($modelquestion as $question){
            if($question["ezf_field_label"]!==''){
                $havequestion = 1;
            }else{
                $havequestion = 0;
            }
        }
        $html = "<b>".$field->ezf_field_label."</b>";

        $html .= "<div class='table-responsive'><table class='table'>";
        $html .= "<thead>";
        $html .= "<tr>";
        if($havequestion==1){
            $html .= "<th></th>";
        }else{
            $html .= "";
        }

        foreach($modelchoice as $choice){
            $html .= "<th>".$choice["ezf_choicelabel"]."</th>";
        }
        $html .= "<tr>";
        $html .= "</thead>";
        $html .= "<tbody>";

        foreach($modelquestion as $question){
            $modelvalue = EzformFields::find()->where('ezf_field_sub_id = :ezf_field_id',[":ezf_field_id"=>$question["ezf_field_id"]])->all();
            $html .= "<tr>";
            $html .= ($havequestion==1) ? "<td>".$question["ezf_field_label"]."</td>" : null;
            foreach($modelvalue as $value){
                $annotatedVal =false; $ann_sub_textvalue =false;
                if((string)$form->attributes['rstat']=='annotated'){
                    $annotatedVal = '<div style="margin-top: -20px; margin-left: 30px;"><sub style="color:green;">0 = No (ปล่อยว่าง); 1 = Yes (เลือก)</sub></div>';
                    $ann_sub_textvalue = '<code>'.$value["ezf_field_name"].'</code><br>';
                }
                if($value["ezf_field_type"]=="251"){
                    $html .= "<td>".$form->field($model,$value["ezf_field_name"])->label($ann_sub_textvalue)."</td>";
                }else if($value["ezf_field_type"]=="252"){
                    $html .= "<td>".$form->field($model,$value["ezf_field_name"])->textArea()->label($ann_sub_textvalue)."</td>";
                }else if($value["ezf_field_type"]=="253"){
                    $date = $model->{$value["ezf_field_name"]};
                    if($date+0) {
                        $explodeDate = explode('-', $date);
                        //print_r($explodeDate);
                        $formateDate = $explodeDate[2] . "/" . $explodeDate[1] . "/" . ($explodeDate[0] + 543);
                        $model->{$value["ezf_field_name"]} = $formateDate;
                    }
                    $html .= "<td>".$form->field($model, $value["ezf_field_name"])->widget(DMSDateWidget::className(), [
                        ])->label($ann_sub_textvalue)."</td>";
                }else if($value["ezf_field_type"]=="254"){
                    $html .= "<td>" . $ann_sub_textvalue.$form->field($model, $value["ezf_field_name"])->checkbox()->label($value["ezf_field_label"].'').$annotatedVal. "</td>";
                }
            }
            $html .= "<tr>";
        }

        $html .= "</tbody>";
        $html .= "</table></div>";
        return $html;
    }

}

?>
