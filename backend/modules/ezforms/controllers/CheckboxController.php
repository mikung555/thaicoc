<?php

namespace backend\modules\ezforms\controllers;

use Yii;
use backend\modules\ezforms\models\Ezform;
use backend\modules\ezforms\models\EzformSearch;
use backend\modules\ezforms\models\EzformFields;
use backend\modules\ezforms\models\EzformChoice;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\Response;
use yii\data\SqlDataProvider;
use common\models\User;
use common\lib\codeerror\helpers\GenMillisecTime;
use backend\modules\ezforms\components\GenForm;
use backend\modules\ezforms\components\EzformWidget;
use backend\modules\ezforms\components\EzformQuery;

class CheckboxController extends Controller
{
    public function actionInsertfield($forder){

        if (Yii::$app->getRequest()->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $modelform = $this->findModel($_POST["EzformFields"]["ezf_id"]);
            $arrayData = array();
            $validateData = array();
            $result = array();

            //value For Insert
            foreach($_POST['checkbox'] as $value){
                $i=0;
                foreach($value as $key){
                    $arrayData[$i][] = $key;
                    $i++;
                }
            }
            //Value Form Validate
            foreach($_POST['checkbox']['value'] as $value){
               array_push($validateData,$value);
            }
            if(isset($_POST['checkbox']['text'])){
                foreach($_POST["checkbox"]["text"] as $valueText){
                    array_push($validateData,$valueText);
                }
            }

            $resultUnique = $this->setArrayUnique($validateData);
            if($resultUnique==0){
                $result = [
                    'status' => 'uniquevalue',
                    'action' => 'alert',
                    'message' => '<span style="font-size: 20px;"><strong><i class="glyphicon glyphicon-remove-sign"></i> Error!</strong> ' . Yii::t('app', 'ค่าของตัวแปรในสเกลห้ามซ้ำกัน'.'</span>'),
                ];
                Yii::$app->response->format = Response::FORMAT_JSON;
                return $result;
            }else {
                foreach ($validateData as $data) {

                    $sqlColumn = "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS "
                        . "WHERE TABLE_NAME = '" . $modelform->ezf_table . "' "
                        . "AND COLUMN_NAME = '" . $data . "'  ";

                    $num = Yii::$app->db->createCommand($sqlColumn)->execute();
                    array_push($result, $num);
                }
                if (in_array("1", $result) == "1") {
                    $result = [
                        'status' => 'warning',
                        'action' => 'update',
                        'message' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> Error!</strong> ' . Yii::t('app', 'ค่าตัวแปรนี้มีอยู่ในฐานข้อมูลแล้วกรุณาลองใหม่'),
                    ];
                    return $result;
                } else {
                    $modelfield = new EzformFields;
                    if ($modelfield->load(Yii::$app->request->post())) {
                        $modelfield->ezf_field_id = GenMillisecTime::getMillisecTime();
                        $modelfield->ezf_field_head_label = 1;
                        $modelfield->ezf_field_options = json_encode($_POST['ezf_field_options']);
                        $modelfield->ezf_field_default = $_POST['ezf_field_default'];
                        $modelfield->ezf_field_order = $forder;
                        $field_id = $modelfield->ezf_field_id;
                        $ezf_id = $modelfield->ezf_id;
                        if ($modelfield->save()) {
                            foreach ($arrayData as $data) {
                                $modelfield2 = new EzformFields;
                                $modelfield2->ezf_id = $ezf_id;
                                $modelfield2->ezf_field_id = GenMillisecTime::getMillisecTime();
                                $modelfield2->ezf_field_name = $data[0];
                                $modelfield2->ezf_field_label = $data[1];
                                $modelfield2->ezf_field_sub_id = $field_id;
                                $modelfield2->ezf_field_sub_textvalue = $data[2];
                                $modelfield2->ezf_field_type = 0;
                                $modelfield2->ezf_field_lenght = 0;
                                $modelfield2->save();
                                $sql = "ALTER TABLE `" . $modelform->ezf_table . "`"
                                    . " ADD COLUMN $data[0] VARCHAR(5)";
                                Yii::$app->db->createCommand($sql)->execute();
                            }
                            foreach ($arrayData as $data) {

                                if ($data[2] != "") {
                                    $modelfield3 = new EzformFields;
                                    $modelfield3->ezf_id = $ezf_id;
                                    $modelfield3->ezf_field_id = GenMillisecTime::getMillisecTime();
                                    $modelfield3->ezf_field_name = $data[2];
                                    $modelfield3->ezf_field_label = '';
                                    $modelfield3->ezf_field_sub_id = $field_id;
                                    $modelfield3->ezf_field_type = 0;
                                    $modelfield3->save();
                                    $sql = "ALTER TABLE `" . $modelform->ezf_table . "`"
                                        . " ADD COLUMN $data[2] VARCHAR(100)";
                                    Yii::$app->db->createCommand($sql)->execute();
                                }
                            }

                            \backend\modules\ezforms\components\EzformFunc::saveCondition($_POST['conditionFields']);

                            return \backend\modules\ezforms\components\EzformFunc::saveInput($modelfield);
                        } else {
                            return false;
                        }
                    }
                }
            }
        }
    }
    public function actionFormdelete($id){
        if (Yii::$app->getRequest()->isAjax) {
            $field = $this->findFieldModel($id);
            $fieldCheckboxItem = EzformFields::find()->where(['ezf_field_sub_id'=>$field->ezf_field_id])->all();
	           $modelform = Ezform::find()->where('ezf_id = :ezf_id',[':ezf_id'=>$field->ezf_id])->One();
            $field->delete();
            if($fieldCheckboxItem>0){
                foreach($fieldCheckboxItem as $key=>$value){
		        \backend\modules\ezforms\components\EzformFunc::deleteCondition($value);
                    $sql1 = "DELETE FROM `ezform_fields` WHERE ezf_field_sub_id='".$field->ezf_field_id."' ";
                    Yii::$app->db->createCommand($sql1)->execute();
                    $sql2 = "ALTER TABLE `".$modelform->ezf_table."` DROP $value->ezf_field_name ";
                    Yii::$app->db->createCommand($sql2)->execute();
                }

            }
            Yii::$app->response->format = Response::FORMAT_JSON;
            $result = [
                'status' => 'warning',
                'action' => 'update',
                'message' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> Success!</strong> ' . Yii::t('app', 'Deleted completed.'),
                'data' => $id,
            ];
            return $result;
        }
    }
    public function returnjson($id){
            $result = [
                'status' => 'warning',
                'action' => 'update',
                'message' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> Success!</strong> ' . Yii::t('app', 'Deleted completed.'),
                'data' => $id,
            ];
            return $result;
    }
    public function actionFormupdate($id){
            $model2 = $this->findFieldModel($id);
            $nameValue = $model2->ezf_field_name;
//            $numId = EzformFields::find()->where('ezf_field_sub_id = :ezf_field_sub_id',[':ezf_field_sub_id'=>$id])->count();
            $modelcheck = EzformFields::find()->where(['ezf_field_sub_id'=>$model2->ezf_field_id])->andWhere('ezf_field_label <> "" ')->all();
            return $this->renderAjax('/ezform/_editpanel',
                ['model2'=>$model2,
                'modelcheck'=>$modelcheck,
                'nameValue'=>$nameValue,
//                'numId'=>$numId
                    ]);

    }

	private function getIndex($array, $value){
		foreach ($array as $key => $v) {
			if($v == $value){
				return $key;
			}
		}
	}

	private function validateCheckBox($var, $form){
		$columnName = EzformQuery::getColumnName($form->ezf_table, $var);
		if($columnName){
			//ตัวแปรซ้ำ
			$result = [
					'status' => 'uniquevalue',
                    'action' => 'alert',
					'message' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> Error!</strong> ' . Yii::t('app', 'ค่าตัวแปร "'.$var.'" มีอยู่ในฐานข้อมูลแล้วกรุณาลองใหม่'),
						];
			return $result;
		}

		return FALSE;
	}

	public function checkValueUpdate($post,$form)
    {
//		\yii\helpers\VarDumper::dump($post, 10, true);
//		exit();

        foreach($post['checkColumn']['var_old'] as $index => $value){
			if($value == 'new'){//new
				$valid = $this->validateCheckBox($index, $form);
				if(is_array($valid)){
					return $valid;
				}

				$key = $this->getIndex($post['checkbox']['value'], $index);

				$modelfield = new EzformFields;
				$modelfield->ezf_id = $post['EzformFields']['ezf_id'];
				$modelfield->ezf_field_id = GenMillisecTime::getMillisecTime();
				$modelfield->ezf_field_name = $index;
				$modelfield->ezf_field_label = $post['checkbox']['label'][$key];
				$modelfield->ezf_field_sub_id = $post['EzformFields']['ezf_field_id'];
				$modelfield->ezf_field_type = 0;
				$modelfield->ezf_field_lenght = 0;
				$modelfield->ezf_field_sub_textvalue = $post['checkbox']['text'][$key];

				$modelfield->save();

				EzformQuery::AlterAddField($form->ezf_table, $index, 'VARCHAR(100)');

			} elseif ($value == 'old') {//update label
				$modelfield = EzformQuery::getFieldNameInTable($post['EzformFields']['ezf_id'], $index);

				$key = $this->getIndex($post['checkbox']['value'], $index);
				$labelVar = $this->getIndex($post['checkbox']['text'], $index);

				$modelfield->ezf_field_label = $post['checkbox']['label'][$key];
				$modelfield->ezf_field_sub_textvalue = $post['checkbox']['text'][$key];

				$modelfield->save();

			} else {//edit
				$valid = $this->validateCheckBox($index, $form);
				if(is_array($valid)){
					return $valid;
				}

				$modelfield = EzformQuery::getFieldNameInTable($post['EzformFields']['ezf_id'], $value);
				\backend\modules\ezforms\components\EzformFunc::deleteCondition($modelfield);

				$key = $this->getIndex($post['checkbox']['value'], $index);
				$modelfield->ezf_field_label = $post['checkbox']['label'][$key];
				$modelfield->ezf_field_sub_textvalue = $post['checkbox']['text'][$key];
				$modelfield->ezf_field_name = $index;
				$modelfield->save();

				EzformQuery::AlterChangeField($form->ezf_table, $value, $index, 'VARCHAR(100)');
			}
        }

		foreach($post['checkColumn']['label_old'] as $index => $value){
			if($value == 'new'){//new
				$valid = $this->validateCheckBox($index, $form);
				if(is_array($valid)){
					return $valid;
				}

				$modelfield = new EzformFields;
				$modelfield->ezf_id = $post['EzformFields']['ezf_id'];
				$modelfield->ezf_field_id = GenMillisecTime::getMillisecTime();
				$modelfield->ezf_field_name = $index;
				$modelfield->ezf_field_label = '';
				$modelfield->ezf_field_sub_id = $post['EzformFields']['ezf_field_id'];
				$modelfield->ezf_field_type = 0;
				$modelfield->ezf_field_lenght = 0;
				$modelfield->ezf_field_sub_textvalue = '';

				$modelfield->save();

				EzformQuery::AlterAddField($form->ezf_table, $index, 'VARCHAR(100)');
			} elseif ($value == 'old') {//update label
				//none
			} else {//edit
				$valid = $this->validateCheckBox($index, $form);
				if(is_array($valid)){
					return $valid;
				}

				$modelfield = EzformQuery::getFieldNameInTable($post['EzformFields']['ezf_id'], $value);
				$modelfield->ezf_field_label = '';
				$modelfield->ezf_field_name = $index;
				$modelfield->save();

				EzformQuery::AlterChangeField($form->ezf_table, $value, $index, 'VARCHAR(100)');
			}
		}

		if(isset($post['checkColumn']['delete_old']) && !empty($post['checkColumn']['delete_old'])){//del
			$valueSubStr = substr($post['checkColumn']['delete_old'],0,-1);
            $valueDrop = explode(',',$valueSubStr);

			foreach ($valueDrop as $value) {
				$modelfield = EzformQuery::getFieldNameInTable($post['EzformFields']['ezf_id'], $value);
				$modelfield->delete();

				EzformQuery::AlterDropField($form->ezf_table, $value);
			}
		}
		return FALSE;
    }

    public function actionUpdatefield($id){
        if (Yii::$app->getRequest()->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $arrayData = array();
            $validateData = array();
            $result = array();
            $modelfield = $this->findFieldModel($id);

            $jsonOld = json_decode($modelfield->ezf_field_options, true);
            if(count($jsonOld)) {
                foreach ($jsonOld as $key => $val) {
                    $jsonOld[$key] = $_POST['ezf_field_options'][$key];
                }
                $modelfield->ezf_field_options = json_encode($jsonOld);
            }

            foreach($validateData as $data){
                $sqlColumn = "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS "
                    . "WHERE TABLE_NAME = '".$modelform->ezf_table."' "
                    . "AND COLUMN_NAME = '".$data."'  ";
                $num = Yii::$app->db->createCommand($sqlColumn)->execute();
                array_push($result, $num);
            }
            $resultUnique = $this->setArrayUnique($validateData);
            if($resultUnique==0){
                $result = [
                    'status' => 'uniquevalue',
                    'action' => 'alert',
                    'message' => '<span style="font-size: 20px;"><strong><i class="glyphicon glyphicon-remove-sign"></i> Error!</strong> ' . Yii::t('app', 'ค่าของตัวแปรในสเกลห้ามซ้ำกัน'.'</span>'),
                ];
                Yii::$app->response->format = Response::FORMAT_JSON;
                return $result;
            }else{
                $modelform = $this->findModel($_POST["EzformFields"]["ezf_id"]);

				$checkVa = $this->checkValueUpdate($_POST, $modelform);
				if(is_array($checkVa)){
					return $checkVa;
				}
				if($modelfield->load(Yii::$app->request->post())){
					$modelfield->ezf_field_head_label = 1;
          $modelfield->ezf_field_default = $_POST['ezf_field_default'];
					$modelfield->ezf_field_name = $_POST["nameValue"];
					$field_id = $modelfield->ezf_field_id;
					$ezf_id = $modelfield->ezf_id;

					if($modelfield->save()){

						\backend\modules\ezforms\components\EzformFunc::saveCondition($_POST['conditionFields']);
						$result = [
							'status' => 'success',
							  ];
						return $result;
					}else{
						return false;
					}
				}
			}

        }
    }
    protected function setArrayUnique($value){
        $val = array_filter($value);
        $unique = array_unique($val);

        if(count($unique)!=count($val)){
            $result = 0;
        }else{
            $result = 1;
        }

        return $result;

    }
    protected function findFieldModel($id)
    {
        if (($model = EzformFields::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    protected function findModel($id) {
        if (($model = Ezform::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
?>
