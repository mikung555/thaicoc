<?php

namespace backend\modules\ezforms\controllers;

use Yii;
use backend\modules\ezforms\models\Ezform;
use backend\modules\ezforms\models\EzformSearch;
use backend\modules\ezforms\models\EzformFields;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\Response;
use yii\data\SqlDataProvider;
use common\models\User;
use common\lib\codeerror\helpers\GenMillisecTime;
use backend\modules\ezforms\components\GenForm;
use common\lib\ezform\EzformQuery;

class AudioController extends Controller
{
    public function actionSave(){
	Yii::$app->response->format = Response::FORMAT_JSON;
	
        $fileName = $_POST["audio-filename"];

	$rootpath = Yii::$app->basePath . '/../storage/web/audio';
	if (!is_dir($rootpath)) {
		mkdir($rootpath, 0777, true);
		chmod($rootpath, 0777);
		//throw new CHttpException(500, "{$this->path} does not exists.");
	} else if (!is_writable($rootpath)) {
		chmod($rootpath, 0777);
		//throw new CHttpException(500, "{$this->path} is not writable.");
	}
	
        $path = $rootpath . '/'. $_POST["id"].'_'.Yii::$app->user->id;
        if (!is_dir($path)) {
		mkdir($path, 0777, true);
		chmod($path, 0777);
		//throw new CHttpException(500, "{$this->path} does not exists.");
	} else if (!is_writable($path)) {
		chmod($path, 0777);
		//throw new CHttpException(500, "{$this->path} is not writable.");
	}
	$fn = explode('.', $fileName);
	$fileNameCover = $fn[0].'.mp3';
	
	$uploadDirectoryConver = "$path/audiorec_$fileNameCover";
        $uploadDirectory = "$path/audiorec_$fileName";
	
	$linkAudio = \Yii::getAlias('@storageUrl').'/audio/'.$_POST["id"].'_'.Yii::$app->user->id.'/audiorec_'.$fileNameCover;

        array_map('unlink', glob($path."/*"));

	$success = move_uploaded_file($_FILES["audio-blob"]["tmp_name"], $uploadDirectory);
        
	exec("/usr/bin/avconv -y -i $uploadDirectory $uploadDirectoryConver");
	
        if ($success) {
	    chmod($uploadDirectory, 0777);
	    $result = [
		'status' => 'success',
		'action' => 'upload',
		'message' => '<strong><i class="glyphicon glyphicon-info-sign"></i> Success!</strong> ' . Yii::t('app', 'อัพโหลดไฟล์เสร็จแล้ว'),
		'data' => $uploadDirectoryConver,
		'link'=>$linkAudio,
		'name' => "audiorec_$fileNameCover",
	    ];
	    return $result;
	} else {
	    $result = [
		'status' => 'error',
		'action' => 'upload',
		'message' => '<strong><i class="glyphicon glyphicon-warning-sign"></i> Error!</strong> ' . Yii::t('app', 'ไม่สามารถอัพโหลดไฟล์'),
	    ];
	    return $result;
	}

    }

    public function actionInsertfield($forder){
        if (Yii::$app->getRequest()->isAjax) {
                $model2 = new EzFormFields();
                $modelform = Ezform::find()->where('ezf_id = :ezf_id',[':ezf_id'=>$_POST['EzformFields']['ezf_id']])->One();

				$model2->ezf_field_options = json_encode($_POST['ezf_field_options']);
				$model2->ezf_field_order = $forder;
                $model2->ezf_field_id = GenMillisecTime::getMillisecTime();

                if ($model2->load(Yii::$app->request->post())) {
                    Yii::$app->response->format = Response::FORMAT_JSON;
				
                    $sqlColumn = "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS "
                            . "WHERE TABLE_NAME = '".$modelform->ezf_table."' "
                            . "AND COLUMN_NAME = '".$model2->ezf_field_name."'  ";
                    $num = Yii::$app->db->createCommand($sqlColumn)->execute();
                    if($num==1){
                        $result = [
                            'status' => 'warning',
                            'action' => 'update',
                            'message' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> Error!</strong> ' . Yii::t('app', 'ค่าตัวแปรนี้มีอยู่ในฐานข้อมูลแล้วกรุณาลองใหม่'),
                        ];
                        return $result;
                    }else{
                        $sql = "ALTER TABLE `".$modelform->ezf_table."`"
                        . "ADD COLUMN $model2->ezf_field_name VARCHAR(100)";
                        Yii::$app->db->createCommand($sql)->execute();
                        return  \backend\modules\ezforms\components\EzformFunc::saveInput($model2);
                    }
                }

        }
	
    }
    public function actionFormdelete($id){
        if (Yii::$app->getRequest()->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $model2 = $this->findFieldModel($id);
            $modelform = Ezform::find()->where('ezf_id = :ezf_id',[':ezf_id'=>$model2->ezf_id])->One();
			
			$rowData = \backend\modules\ezforms\components\EzformQuery::getCheckData($modelform->ezf_table, $model2->ezf_field_name);
			if($rowData>0){
				$result = [
					'status' => 'error',
					'action' => 'update',
					'message' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> Success!</strong> ' . Yii::t('app', 'ไม่สามารถลบข้อมูลได้เนื่องจากมีข้อมูลในระบบ'),
					'data' => $id,
				];
				return $result;
			} else {
				$model2->delete();
				$sql = "ALTER TABLE `".$modelform->ezf_table."`"
						. "DROP $model2->ezf_field_name ";
				Yii::$app->db->createCommand($sql)->execute();
				$result = [
					'status' => 'warning',
					'action' => 'update',
					'message' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> Success!</strong> ' . Yii::t('app', 'Deleted completed.'),
					'data' => $id,
				];
				return $result;
			}
        }

    }
    public function actionFormupdate($id){
            $model2 = $this->findFieldModel($id);

            return $this->renderAjax('/ezform/_editpanel',['model2'=>$model2]);

    }
    public function actionUpdatefield($id){
        if (Yii::$app->getRequest()->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $modelfield = $this->findFieldModel($id);
			//ezf_field_options
			
            $valueOld = $modelfield->ezf_field_name;
            $modelform = Ezform::find()
                    ->where(['ezf_id'=>$modelfield->ezf_id])->one();
            if($modelfield->load(Yii::$app->request->post())){
                $valueNew = $modelfield->ezf_field_name;
                $exists = EzFormFields::find()
                            ->where(['ezf_field_id' => $modelfield->ezf_field_id])
                            ->andWhere([ 'ezf_field_name' => $valueNew])
                            ->exists();
				
                if($exists==1){
                    //ไม่เปลี่ยนตัวแปร
		
                    $modelfield->save();

                    $result = [
                                'status' => 'success',
                              ];
                    return $result;
                }else{
                    //เปลี่ยนตัวแปร
                    $sqlColumn = "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS "
                            . "WHERE TABLE_NAME = '".$modelform->ezf_table."' "
                            . "AND COLUMN_NAME = '".$valueNew."'  ";
                    $num = Yii::$app->db->createCommand($sqlColumn)->execute();
                    if($num==1){
                        //ตัวแปรซ้ำ
                        $result = [
                                'status' => 'danger',
                                'message' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> Error!</strong> ' . Yii::t('app', 'ค่าตัวแปรนี้มีอยู่ในฐานข้อมูลแล้วกรุณาลองใหม่'),
                                    ];
                        return $result;
                    }else{
                        //ตัวแปรไม่ซ้ำ
						
                        $modelfield->save();

                        $sql = "ALTER TABLE `".$modelform->ezf_table."` "
                                . "CHANGE COLUMN `".$valueOld."` `".$valueNew."` VARCHAR(100)";
                        Yii::$app->db->createCommand($sql)->execute();
                        $result = [
                                'status' => 'success',
                                  ];
                        return $result;
                    }
                }
            }
        }
    }
    protected function findFieldModel($id)
    {
        if (($model = EzformFields::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
?>
