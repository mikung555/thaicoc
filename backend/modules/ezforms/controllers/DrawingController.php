<?php

namespace backend\modules\ezforms\controllers;

use Yii;
use backend\modules\ezforms\models\Ezform;
use backend\modules\ezforms\models\EzformSearch;
use backend\modules\ezforms\models\EzformFields;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\Response;
use yii\data\SqlDataProvider;
use common\models\User;
use common\lib\codeerror\helpers\GenMillisecTime;
use backend\modules\ezforms\components\GenForm;
use common\lib\ezform\EzformQuery;

class DrawingController extends Controller
{
    public function actionSaveImage(){
        if (Yii::$app->getRequest()->isAjax) {
	    Yii::$app->response->format = Response::FORMAT_JSON;
	    // input is in format: data:image/png;base64,...
	    //chmod(Yii::$app->basePath . '/../backend/web/drawing/', 0777);

	    $im = imagecreatefrompng($_POST['image']);
	    imagesavealpha($im, true);
	    // Fill the image with transparent color
	    $color = imagecolorallocatealpha($im,0x00,0x00,0x00,127);
	    imagefill($im, 0, 0, $color);

	    $idName = '';
	    if ($_SERVER["REMOTE_ADDR"] == '::1' || $_SERVER["REMOTE_ADDR"] == '127.0.0.1') {
		    $idName = 'mycom';
	    } else {
		    $idName = $_SERVER["REMOTE_ADDR"];
	    }
	    //date("YmdHis")
	    $nowFileName = $idName . '_'.$_POST['name'].'_tmp.png';
	    $fullPath = Yii::$app->basePath . '/../backend/web/drawing/' . $nowFileName;

	    $success = imagepng($im, $fullPath);
	    if ($success) {
			//chmod(Yii::$app->basePath . '/../backend/web/drawing/'. $nowFileName, 0777);
		    $result = [
			'status' => 'success',
			'action' => 'upload',
			'message' => '<strong><i class="glyphicon glyphicon-info-sign"></i> Success!</strong> ' . Yii::t('app', 'อัพโหลดรูปภาพเสร็จแล้ว'),
			'data' => $nowFileName,
		    ];
		    return $result;
	    } else {
		$result = [
		    'status' => 'error',
		    'action' => 'upload',
		    'message' => '<strong><i class="glyphicon glyphicon-warning-sign"></i> Error!</strong> ' . Yii::t('app', 'ไม่สามารถอัพโหลดรูปภาพ'),
		];
		return $result;
	    }
	    imagedestroy($im);
        } else {
            throw new NotFoundHttpException('Ajax only.');
        }

    }

    public function actionBgImage(){
        if (Yii::$app->getRequest()->isAjax) {
	    Yii::$app->response->format = Response::FORMAT_JSON;
	    //chmod(Yii::$app->basePath . '/../storage/web/drawing/', 0777);
	    $uploadFile = \yii\web\UploadedFile::getInstanceByName('outline-bg');

	    if(isset($_GET['oldfile'])){
			unlink(Yii::$app->basePath . '/../storage/web/drawing/' . $_GET['oldfile']);
	    }

	    if ($uploadFile !== null) {
		$idName = '';
		if ($_SERVER["REMOTE_ADDR"] == '::1' || $_SERVER["REMOTE_ADDR"] == '127.0.0.1') {
			$idName = 'mycom';
		} else {
			$idName = $_SERVER["REMOTE_ADDR"];
		}
		//date("YmdHis")
		$nowFileName = $idName . '_bg_'.$_GET['name'].date('_His').'tmp.png';
		$fullPath = Yii::$app->basePath . '/../storage/web/drawing/' . $nowFileName;

		$uploadFile->saveAs($fullPath);
		//chmod(Yii::$app->basePath . '/../storage/web/drawing/'. $nowFileName, 0777);

		list($width, $height, $type, $attr) = getimagesize($fullPath);

		return ['files'=>[
		    'name'=>$nowFileName,
		    'type'=>$uploadFile->type,
		    'size'=>$uploadFile->size,
		    'url'=>  \Yii::getAlias('@storageUrl').'/drawing/' . $nowFileName,
		    'width'=>$width,
		    'height'=>$height,
		    'newurl'=>  \yii\helpers\Url::to(['//ezforms/drawing/bg-image', 'name'=>$_GET['name'], 'oldfile'=>$nowFileName]),
		]];
	    }

        } else {
            throw new NotFoundHttpException('Ajax only.');
        }

    }

	public function actionOptionImage(){
        if (Yii::$app->getRequest()->isAjax) {
	    Yii::$app->response->format = Response::FORMAT_JSON;
	    //chmod(Yii::$app->basePath . '/../storage/web/drawing/', 0777);
	    $uploadFile = \yii\web\UploadedFile::getInstanceByName('option-bg');

	    if(isset($_GET['oldfile'])){
		unlink(Yii::$app->basePath . '/../storage/web/drawing/' . $_GET['oldfile']);
	    }

	    if ($uploadFile !== null) {
		$idName = '';
		if ($_SERVER["REMOTE_ADDR"] == '::1' || $_SERVER["REMOTE_ADDR"] == '127.0.0.1') {
			$idName = 'mycom';
		} else {
			$idName = $_SERVER["REMOTE_ADDR"];
		}
		//date("YmdHis")
		$nowFileName = $idName . '_option_'.$_GET['name'].date('_His').'tmp.png';
		$fullPath = Yii::$app->basePath . '/../storage/web/drawing/' . $nowFileName;

		$uploadFile->saveAs($fullPath);
		//chmod(Yii::$app->basePath . '/../storage/web/drawing/'. $nowFileName, 0777);

		list($width, $height, $type, $attr) = getimagesize($fullPath);

		return ['files'=>[
		    'name'=>$nowFileName,
		    'type'=>$uploadFile->type,
		    'size'=>$uploadFile->size,
		    'url'=>  \Yii::getAlias('@storageUrl').'/drawing/' . $nowFileName,
		    'width'=>$width,
		    'height'=>$height,
		    'newurl'=>  \yii\helpers\Url::to(['//ezforms/drawing/option-image', 'name'=>$_GET['name'], 'oldfile'=>$nowFileName]),
		]];
	    }

        } else {
            throw new NotFoundHttpException('Ajax only.');
        }

    }

    public function actionInsertfield($forder){
        if (Yii::$app->getRequest()->isAjax) {
                $model2 = new EzFormFields();
                $modelform = Ezform::find()->where('ezf_id = :ezf_id',[':ezf_id'=>$_POST['EzformFields']['ezf_id']])->One();

				$model2->ezf_field_options = json_encode($_POST['ezf_field_options']);
				$model2->ezf_field_order = $forder;
                $model2->ezf_field_id = GenMillisecTime::getMillisecTime();

                if ($model2->load(Yii::$app->request->post())) {
                    Yii::$app->response->format = Response::FORMAT_JSON;
					if(isset($_POST['allow_bg']) && $_POST['allow_bg']==1 ){
						$modelfield->ezf_field_options = @serialize(['allow_bg'=>$_POST['allow_bg']]);
					} else {
						$modelfield->ezf_field_options = '';
					}
					$model2->ezf_field_default = $this->uploadOption($model2);

                    $sqlColumn = "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS "
                            . "WHERE TABLE_NAME = '".$modelform->ezf_table."' "
                            . "AND COLUMN_NAME = '".$model2->ezf_field_name."'  ";
                    $num = Yii::$app->db->createCommand($sqlColumn)->execute();
                    if($num==1){
                        $result = [
                            'status' => 'warning',
                            'action' => 'update',
                            'message' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> Error!</strong> ' . Yii::t('app', 'ค่าตัวแปรนี้มีอยู่ในฐานข้อมูลแล้วกรุณาลองใหม่'),
                        ];
                        return $result;
                    }else{
                        $sql = "ALTER TABLE `".$modelform->ezf_table."`"
                        . "ADD COLUMN $model2->ezf_field_name VARCHAR(255)";
                        Yii::$app->db->createCommand($sql)->execute();
                        return  \backend\modules\ezforms\components\EzformFunc::saveInput($model2);
                    }
                }

        }

    }
    public function actionFormdelete($id){
        if (Yii::$app->getRequest()->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $model2 = $this->findFieldModel($id);
            $modelform = Ezform::find()->where('ezf_id = :ezf_id',[':ezf_id'=>$model2->ezf_id])->One();
			
			$rowData = \backend\modules\ezforms\components\EzformQuery::getCheckData($modelform->ezf_table, $model2->ezf_field_name);
			if($rowData>0){
				$result = [
					'status' => 'error',
					'action' => 'update',
					'message' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> Success!</strong> ' . Yii::t('app', 'ไม่สามารถลบข้อมูลได้เนื่องจากมีข้อมูลในระบบ'),
					'data' => $id,
				];
				return $result;
			} else {
				$model2->delete();
				$sql = "ALTER TABLE `".$modelform->ezf_table."`"
						. "DROP $model2->ezf_field_name ";
				Yii::$app->db->createCommand($sql)->execute();
				$result = [
					'status' => 'warning',
					'action' => 'update',
					'message' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> Success!</strong> ' . Yii::t('app', 'Deleted completed.'),
					'data' => $id,
				];
				return $result;
			}
        }

    }
    public function actionFormupdate($id){
            $model2 = $this->findFieldModel($id);

            return $this->renderAjax('/ezform/_editpanel',['model2'=>$model2]);

    }
    public function actionUpdatefield($id){
        if (Yii::$app->getRequest()->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $modelfield = $this->findFieldModel($id);
			//ezf_field_options
			$jsonOld = json_decode($modelfield->ezf_field_options, true);
			if(count($jsonOld)) {
				foreach ($jsonOld as $key => $val) {
					$jsonOld[$key] = $_POST['ezf_field_options'][$key];
				}
				$modelfield->ezf_field_options = json_encode($jsonOld);
			}
			//
            $valueOld = $modelfield->ezf_field_name;
            $modelform = Ezform::find()
                    ->where(['ezf_id'=>$modelfield->ezf_id])->one();
            if($modelfield->load(Yii::$app->request->post())){
                $valueNew = $modelfield->ezf_field_name;
                $exists = EzFormFields::find()
                            ->where(['ezf_field_id' => $modelfield->ezf_field_id])
                            ->andWhere([ 'ezf_field_name' => $valueNew])
                            ->exists();
				
                if($exists==1){
                    //ไม่เปลี่ยนตัวแปร
					if(isset($_POST['allow_bg']) && $_POST['allow_bg']==1 ){
						$modelfield->ezf_field_options = @serialize(['allow_bg'=>$_POST['allow_bg']]);
					} else {
						$modelfield->ezf_field_options = '';
					}
					
          			$modelfield->ezf_field_default = $this->uploadOption($modelfield);
                    $modelfield->save();

                    $result = [
                                'status' => 'success',
                              ];
                    return $result;
                }else{
                    //เปลี่ยนตัวแปร
                    $sqlColumn = "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS "
                            . "WHERE TABLE_NAME = '".$modelform->ezf_table."' "
                            . "AND COLUMN_NAME = '".$valueNew."'  ";
                    $num = Yii::$app->db->createCommand($sqlColumn)->execute();
                    if($num==1){
                        //ตัวแปรซ้ำ
                        $result = [
                                'status' => 'danger',
                                'message' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> Error!</strong> ' . Yii::t('app', 'ค่าตัวแปรนี้มีอยู่ในฐานข้อมูลแล้วกรุณาลองใหม่'),
                                    ];
                        return $result;
                    }else{
                        //ตัวแปรไม่ซ้ำ
						if(isset($_POST['allow_bg']) && $_POST['allow_bg']==1 ){
							$modelfield->ezf_field_options = @serialize(['allow_bg'=>$_POST['allow_bg']]);
						} else {
							$modelfield->ezf_field_options = '';
						}
            			$modelfield->ezf_field_default = $this->uploadOption($modelfield);
                        $modelfield->save();

                        $sql = "ALTER TABLE `".$modelform->ezf_table."` "
                                . "CHANGE COLUMN `".$valueOld."` `".$valueNew."` VARCHAR(255)";
                        Yii::$app->db->createCommand($sql)->execute();
                        $result = [
                                'status' => 'success',
                                  ];
                        return $result;
                    }
                }
            }
        }
    }
    protected function findFieldModel($id)
    {
        if (($model = EzformFields::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

	public function uploadOption($model){
		if($model['ezf_field_type']==24){

			if(stristr($model['ezf_field_default'], 'tmp.png') == TRUE) {

				$fileName = $model['ezf_field_default'];
				$newFileName = $fileName;
				$nameEdit = false;

				if(stristr($fileName, 'tmp.png') == TRUE){
				    $nameEdit = true;
				    $newFileName = date("Ymd_His") . '.png';
				    @copy(Yii::$app->basePath . '/../storage/web/drawing/' . $fileName, Yii::$app->basePath . '/../storage/web/drawing/bg/' . $newFileName);
				    @unlink(Yii::$app->basePath . '/../storage/web/drawing/' . $fileName);
				}

				$model['ezf_field_default'] = $newFileName;
				$modelTmp = Ezform::find()
                    ->where(['ezf_id'=>$model->ezf_id])->one();

				if(isset($modelTmp['id'])){
					$fileName = $modelTmp['ezf_field_default'];
					if($nameEdit && $fileName!=''){
					    @unlink(Yii::$app->basePath . '/../storage/web/drawing/bg/' . $fileName);
					}
				}
				return $newFileName;
			}

		}
		return $model['ezf_field_default'];
	}
}
?>
