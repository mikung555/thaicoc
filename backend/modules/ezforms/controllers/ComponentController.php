<?php

namespace backend\modules\ezforms\controllers;

use Yii;
use backend\modules\ezforms\models\Ezform;
use backend\modules\ezforms\models\EzformSearch;
use backend\modules\ezforms\models\EzformFields;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\Response;
use yii\data\SqlDataProvider;
use common\models\User;
use common\lib\codeerror\helpers\GenMillisecTime;
use backend\modules\ezforms\components\GenForm;
use common\lib\ezform\EzformQuery;
use backend\modules\component\models\EzformComponent;
use yii\helpers\ArrayHelper;

class ComponentController extends Controller
{
    public function actionInsertfield($forder, $component_id)
    {
        if (Yii::$app->getRequest()->isAjax) {
            $model2 = new EzFormFields();
            $modelform = Ezform::find()->where('ezf_id = :ezf_id',[':ezf_id'=>$_POST['EzformFields']['ezf_id']])->One();
            $model2->ezf_field_order = $forder;
            $model2->ezf_component = $component_id;
            $model2->ezf_field_options = json_encode($_POST['ezf_field_options']);

            $model2->ezf_field_id = GenMillisecTime::getMillisecTime();
            if ($model2->load(Yii::$app->request->post())) {
                Yii::$app->response->format = Response::FORMAT_JSON;

                $columnName = \backend\modules\ezforms\components\EzformQuery::getColumnName($modelform->ezf_table, $model2->ezf_field_name);
                if($columnName){
                    $result = [
                    'status' => 'warning',
                    'action' => 'update',
                    'message' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> Error!</strong> ' . Yii::t('app', 'ค่าตัวแปรนี้มีอยู่ในฐานข้อมูลแล้วกรุณาลองใหม่'),
                    ];
                    return $result;
                } else {
                    $sql = "ALTER TABLE `".$modelform->ezf_table."` "
                    . "ADD COLUMN $model2->ezf_field_name VARCHAR(50)";
                    Yii::$app->db->createCommand($sql)->execute();
                    return \backend\modules\ezforms\components\EzformFunc::saveInput($model2);
                }
                    return  GenForm::saveComponent($model2);
            }

        }

    }
    public function actionFormdelete($id)
    {


        if (Yii::$app->getRequest()->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            $modelfield = EzformFields::find()->where('ezf_field_id = :id',[':id'=>$id])->One();

            $modelform = Ezform::find()->where('ezf_id = :field',[':field'=>$modelfield->ezf_id])->One();

            $model2 = $this->findFieldModel($id);
            $model2->delete();

            $sql = "ALTER TABLE `".$modelform->ezf_table."` "
                    . "DROP $model2->ezf_field_name ";
            Yii::$app->db->createCommand($sql)->execute();

            $result = [
            'status' => 'warning',
            'action' => 'update',
            'message' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> Success!</strong> ' . Yii::t('app', 'Deleted completed.'),
            'data' => $id,
            ];
            return $result;
        }

    }
    public function actionFormupdate($id)
    {
        $model2 = $this->findFieldModel($id);

        $ezformComponents = EzformComponent::find()->all();

        $ezformComponent_maps = ArrayHelper::map($ezformComponents, 'comp_id', 'comp_name');

        return $this->renderAjax('/ezform/_editpanel', [
            'model2'=>$model2,
            'ezformComponent_maps' => $ezformComponent_maps
            ]);


    }
    public function actionUpdatefield($id)
    {
        if (Yii::$app->getRequest()->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            $modelfield = $this->findFieldModel($id);
            //ezf_field_options
            $jsonOld = json_decode($modelfield->ezf_field_options, true);
            if(count($jsonOld)) {
                foreach ($jsonOld as $key => $val) {
                    $jsonOld[$key] = $_POST['ezf_field_options'][$key];
                }
                $modelfield->ezf_field_options = json_encode($jsonOld);
            }
            //

            $valueOld = $modelfield->ezf_field_name;
            $modelform = Ezform::find()
                    ->where(['ezf_id'=>$modelfield->ezf_id])->one();

            if ($modelfield->load(Yii::$app->request->post())) {
                $valueNew = $modelfield->ezf_field_name;
                $exists = EzFormFields::find()
                            ->where(['ezf_field_id' => $modelfield->ezf_field_id])
                            ->andWhere([ 'ezf_field_name' => $valueNew])
                            ->exists();
                if ($exists == 1) {
                    //ไม่เปลี่ยนตัวแปร
                    $modelfield->save();
                    $result = [
                                'status' => 'success',
                              ];
                    return $result;
                } else {
                    //เปลี่ยนตัวแปร
                    $columnName = \backend\modules\ezforms\components\EzformQuery::getColumnName($modelform->ezf_table, $valueNew);
                    if($columnName){
                        //ตัวแปรซ้ำ
                        $result = [
                                'status' => 'danger',
                                'message' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> Error!</strong> ' . Yii::t('app', 'ค่าตัวแปรนี้มีอยู่ในฐานข้อมูลแล้วกรุณาลองใหม่'),
                                    ];
                        return $result;
                    } else {
                        //ตัวแปรไม่ซ้ำ
                        $modelfield->save();
                        $sql = "ALTER TABLE `".$modelform->ezf_table."` "
                                . "CHANGE COLUMN `".$valueOld."` `".$valueNew."` VARCHAR(50)";
                        Yii::$app->db->createCommand($sql)->execute();
                        $result = [
                                'status' => 'success',
                                  ];
                        return $result;
                    }
                }
            }
        }
    }
    protected function findFieldModel($id)
    {
        if (($model = EzformFields::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
?>
