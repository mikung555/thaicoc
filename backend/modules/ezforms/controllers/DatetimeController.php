<?php

namespace backend\modules\ezforms\controllers;

use Yii;
use backend\modules\ezforms\models\Ezform;
use backend\modules\ezforms\models\EzformFields;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use common\lib\codeerror\helpers\GenMillisecTime;
use backend\modules\ezforms\components\EzformQuery;
use backend\modules\ezforms\components\EzformFunc;

class DatetimeController extends Controller
{
    public function actionInsertfield($forder){
    if (Yii::$app->getRequest()->isAjax) {

                $modelform = Ezform::find()->where('ezf_id = :ezf_id',[':ezf_id'=>$_POST['EzformFields']['ezf_id']])->One();

                $model2 = new EzFormFields();
                $model2->ezf_field_order = $forder;
                $model2->ezf_field_id = GenMillisecTime::getMillisecTime();
                $model2->ezf_field_options = json_encode($_POST['ezf_field_options']);
                if ($model2->load(Yii::$app->request->post())) {
                    Yii::$app->response->format = Response::FORMAT_JSON;
                    $columnName = EzformQuery::getColumnName($modelform->ezf_table, $model2->ezf_field_name);

                    if($columnName){
                        $result = [
                            'status' => 'warning',
                            'action' => 'update',
                            'message' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> Error!</strong> ' . Yii::t('app', 'ค่าตัวแปรนี้มีอยู่ในฐานข้อมูลแล้วกรุณาลองใหม่'),
                        ];
                        return $result;
                    }else{
                        EzformQuery::AlterAddField($modelform->ezf_table, $model2->ezf_field_name, 'DATETIME');
                        return  EzformFunc::saveInput($model2);
                    }
                }

        }

    }
    public function actionFormdelete($id){
        if (Yii::$app->getRequest()->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $model2 = $this->findFieldModel($id);
            $modelform = Ezform::find()->where('ezf_id = :ezf_id',[':ezf_id'=>$model2->ezf_id])->One();
            $model2->delete();
            EzformQuery::AlterDropField($modelform->ezf_table, $model2->ezf_field_name);

            $result = [
                'status' => 'warning',
                'action' => 'update',
                'message' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> Success!</strong> ' . Yii::t('app', 'Deleted completed.'),
                'data' => $id,
            ];
            return $result;
        }

    }
    public function actionFormupdate($id){


            $model2 = $this->findFieldModel($id);

            return $this->renderAjax('/ezform/_editpanel',['model2'=>$model2]);


    }
    public function actionUpdatefield($id){
        if (Yii::$app->getRequest()->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $modelfield = $this->findFieldModel($id);
            //ezf_field_options
            $jsonOld = json_decode($modelfield->ezf_field_options, true);
            if(count($jsonOld)) {
                foreach ($jsonOld as $key => $val) {
                    $jsonOld[$key] = $_POST['ezf_field_options'][$key];
                }
                $modelfield->ezf_field_options = json_encode($jsonOld);
            }
            //

            $valueOld = $modelfield->ezf_field_name;
            $modelform = Ezform::find()
                    ->where(['ezf_id'=>$modelfield->ezf_id])->one();
            if($modelfield->load(Yii::$app->request->post())){
                $valueNew = $modelfield->ezf_field_name;
                $exists = EzformQuery::getFieldName($modelfield->ezf_field_id, $valueNew);

                if($exists){
                    //ไม่เปลี่ยนตัวแปร
                    $modelfield->save();
                    $result = [
                                'status' => 'success',
                              ];
                    return $result;
                }else{
                    //เปลี่ยนตัวแปร
                    $columnName = EzformQuery::getColumnName($modelform->ezf_table, $valueNew);
                    if($columnName){
                        //ตัวแปรซ้ำ
                        $result = [
                                'status' => 'danger',
                                'message' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> Error!</strong> ' . Yii::t('app', 'ค่าตัวแปรนี้มีอยู่ในฐานข้อมูลแล้วกรุณาลองใหม่'),
                                    ];
                        return $result;
                    }else{
                        //ตัวแปรไม่ซ้ำ
                        $modelfield->save();
                        EzformQuery::AlterChangeField($modelform->ezf_table, $valueOld, $valueNew, 'VARCHAR(20)');

                        $result = [
                                'status' => 'success',
                                  ];
                        return $result;
                    }
                }
            }
        }
    }
    protected function findFieldModel($id)
    {
        if (($model = EzformFields::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
?>
