<?php

namespace backend\modules\ezforms\controllers;

use Yii;
use backend\modules\ezforms\models\EzformFields;
use backend\modules\ezforms\models\EzformChoice;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use common\lib\codeerror\helpers\GenMillisecTime;
use backend\modules\ezforms\components\GenForm;
use backend\modules\ezforms\models\Ezform;
use backend\modules\ezforms\components\EzformQuery;
use backend\modules\ezforms\components\EzformFunc;

class SelectController extends Controller {

    public function actionInsertfield($forder) {
        if (Yii::$app->getRequest()->isAjax) {
            $model2 = new EzformFields();
            $ezf_tablename = EzformQuery::getFormTableName($_POST['EzformFields']['ezf_id']);
            //$modelform = Ezform::find()->where('ezf_id = :ezf_id',[':ezf_id'=>$_POST['EzformFields']['ezf_id']])->One();
            $model2->ezf_field_order = $forder;
            $model2->ezf_field_id = GenMillisecTime::getMillisecTime();
            $model2->ezf_field_options = json_encode($_POST['ezf_field_options']);
//               \yii\helpers\VarDumper::dump($model2->attributes, 10, TRUE);
//            //echo Yii::$app->user->id;
//            Yii::$app->end();

            if ($model2->load(Yii::$app->request->post())) {
                Yii::$app->response->format = Response::FORMAT_JSON;
				$columnName = EzformQuery::getColumnName($ezf_tablename->ezf_table, $model2->ezf_field_name);
                if ($columnName) {
                    $result = [
                        'status' => 'warning',
                        'action' => 'update',
                        'message' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> Error!</strong> ' . Yii::t('app', 'ค่าตัวแปรนี้มีอยู่ในฐานข้อมูลแล้วกรุณาลองใหม่'),
                    ];
                    return $result;
                } else {
                    if($model2->save()){
                        $strlenght = count($_POST['selectValue']);
                            for ($i = 0; $i < $strlenght; $i++) {
								$choiceModel = new EzformChoice();
								$choiceModel->ezf_choice_id = GenMillisecTime::getMillisecTime();
								$choiceModel->ezf_field_id = $model2->ezf_field_id;
								$choiceModel->ezf_choicevalue = $_POST['selectValue'][$i];
								$i++;
								$choiceModel->ezf_choicelabel = $_POST['selectValue'][$i];
								$choiceModel->save();
                        }
                    }
					EzformFunc::saveCondition($_POST['conditionFields']);
                    EzformQuery::AlterAddField($ezf_tablename->ezf_table, $model2->ezf_field_name, 'VARCHAR(10)');
                    return  EzformFunc::saveInput($model2);
                }
            }
        }
    }

    public function actionTransformfield($ezf_field_id, $ezf_id) {
        $ezform_fields = Yii::$app->db->createCommand("SELECT ezf_field_name FROM ezform_fields WHERE ezf_field_id='$ezf_field_id';")->queryOne();
        $ezf_field_name = $ezform_fields['ezf_field_name'];
        $ezform = Yii::$app->db->createCommand("SELECT ezf_table FROM ezform WHERE ezf_id='$ezf_id';")->queryOne();
        $ezf_table = $ezform['ezf_table'];

        try {
            $sql = "SELECT DISTINCT " . $ezf_field_name . " FROM `" . $ezf_table . "` WHERE LENGTH(" . $ezf_field_name . ")>0;";
            $ezform = Yii::$app->db->createCommand($sql)->queryColumn();
            //return \yii\helpers\VarDumper::dump($ezform, 10, TRUE);
            if(Yii::$app->db->createCommand($sql)->query()->count()) {
                $ezform = Yii::$app->db->createCommand($sql)->queryColumn();
                return $this->renderAjax('/ezform/exfield/select.php', ['modelComponent' => $ezform]);
            }else{
                return $this->renderAjax('/ezform/exfield/select.php');
            }
        } catch (\yii\db\Exception $e) {
            return $this->renderAjax('/ezform/exfield/select.php');
        }
    }

    public function actionFormdelete($id) {
        if (Yii::$app->getRequest()->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $model = $this->findFieldModel($id);
            $ezf_tablename = EzformQuery::getFormTableName($model->ezf_id);

			EzformFunc::deleteCondition($model);
            $model->delete();
			EzformQuery::AlterDropField($ezf_tablename->ezf_table, $model->ezf_field_name);
            $result = [
                'status' => 'warning',
                'action' => 'update',
                'message' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> Success!</strong> ' . Yii::t('app', 'Deleted completed.'),
                'data' => $id,
            ];
            return $result;
        }
    }

    public function actionFormupdate($id) {

        $model2 = $this->findFieldModel($id);
        $modelselect = EzformChoice::find()
                ->where('ezf_field_id = :ezf_field_id', ['ezf_field_id' => $model2->ezf_field_id])
                ->andWhere('ezf_choiceetc is NULL')
                ->all();

        $nameValue = $model2->ezf_field_name;
        return $this->renderAjax('/ezform/_editpanel', ['model2' => $model2,
                    'modelselect' => $modelselect,
                    'nameValue' => $nameValue]);
    }

    public function actionUpdatefield($id, $ezf_id) {
        if (Yii::$app->getRequest()->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            if (isset($_POST['transformfield'])) {
                $ezform_fields = Yii::$app->db->createCommand("SELECT ezf_field_name FROM ezform_fields WHERE ezf_field_id='$id';")->queryOne();
                $ezf_field_name = $ezform_fields['ezf_field_name'];
                $ezform = Yii::$app->db->createCommand("SELECT ezf_table FROM ezform WHERE ezf_id='$ezf_id';")->queryOne();
                $ezf_table = $ezform['ezf_table'];
                //$sql = "SELECT id, ".$ezf_field_name." FROM `".$ezf_table."`;";
                //$ezform_tb = Yii::$app->db->createCommand($sql)->queryAll();
                //$sql = "SELECT DISTINCT ".$ezf_field_name." FROM `".$ezf_table."` WHERE LENGTH(".$ezf_field_name.")>0;";
                //$ezform_tb = Yii::$app->db->createCommand($sql)->queryColumn();
                //$modelfield = new EzformFields();
                $model = $this->findFieldModel($id);
                $modelfield = $this->findFieldModel($id);

                if ($modelfield->load(Yii::$app->request->post())) {

                    //ezf_field_options
                    $jsonOld = json_decode($modelfield->ezf_field_options, true);
                    if(count($jsonOld)) {
                        foreach ($jsonOld as $key => $val) {
                            $jsonOld[$key] = $_POST['ezf_field_options'][$key];
                        }
                        $modelfield->ezf_field_options = json_encode($jsonOld);
                    }
                    //
                    $modelfield->ezf_field_default = $_POST['ezf_field_default'];

                    if ($modelfield->save()) {
						$columnName = EzformQuery::getColumnName($ezf_table, $modelfield->ezf_field_name);
                        if ($columnName) {
							EzformQuery::AlterChangeField($ezf_table, $model->ezf_field_name, $modelfield->ezf_field_name, 'VARCHAR(100)');
                        }//end check
                        else{
							EzformQuery::AlterAddField($ezf_table, $modelfield->ezf_field_name, 'VARCHAR(100)');
                        }

                        EzformFunc::saveCondition($_POST['conditionFields']);

//                        $sql2 = "ALTER TABLE `".$ezf_table."`"
//                        . "ADD $modelfield->ezf_field_name VARCHAR(255)";
//                        Yii::$app->db->createCommand($sql2)->execute();
                    }
                }



                //\yii\helpers\VarDumper::dump($ezform_tb, 10);


                $sql = "DELETE FROM ezform_choice WHERE ezf_field_id ='$id';";
                Yii::$app->db->createCommand($sql)->execute();
				
                $strlenght = count($_POST['selectValue']);
                $indexorg = 0;
                for ($i = 0; $i < $strlenght; $i++) {
					$choiceModel = new EzformChoice();
					$choiceModel->ezf_choice_id = GenMillisecTime::getMillisecTime();
					$choiceModel->ezf_field_id = $modelfield->ezf_field_id;
					$choiceModel->ezf_choicevalue = $_POST['selectValue'][$i];
					$ezf_orglabel = $_POST['orglabel'][$indexorg];
					$indexorg++;
					$i++;
					$choiceModel->ezf_choicelabel = $_POST['selectValue'][$i];
					$choiceModel->save();

                    $sql = "UPDATE `$ezf_table` SET `$ezf_field_name` = '".($choiceModel->ezf_choicevalue)."' WHERE `$ezf_field_name`='$ezf_orglabel';";
                    Yii::$app->db->createCommand($sql)->execute();
                }
                $result = [
                            'status' => 'success',
                            'action' => 'update',
                        ];
                        return $result;
            } else {
                $sql = "DELETE FROM ezform_choice WHERE ezf_field_id ='$id';";
                Yii::$app->db->createCommand($sql)->execute();

                $model = $this->findFieldModel($id);
                $modelfield = $this->findFieldModel($id);

                $ezf_tablename = EzformQuery::getFormTableName($modelfield->ezf_id);

                if ($modelfield->load(Yii::$app->request->post())) {

                    //ezf_field_options
                    $jsonOld = json_decode($modelfield->ezf_field_options, true);
                    if(count($jsonOld)) {
                        foreach ($jsonOld as $key => $val) {
                            $jsonOld[$key] = $_POST['ezf_field_options'][$key];
                        }
                        $modelfield->ezf_field_options = json_encode($jsonOld);
                    }
                    //
                    $modelfield->ezf_field_default = $_POST['ezf_field_default'];

					$columnName = EzformQuery::getColumnName($ezf_tablename->ezf_table, $modelfield->ezf_field_name);
					if ($columnName) {
						EzformQuery::AlterChangeField($ezf_tablename->ezf_table, $model->ezf_field_name, $modelfield->ezf_field_name, 'VARCHAR(100)');
					}//end check
					else{
						EzformQuery::AlterAddField($ezf_tablename->ezf_table, $modelfield->ezf_field_name, 'VARCHAR(100)');
					}
					
                    $sqlUpdate = "UPDATE `ezform_fields` "
                            . "SET `ezf_field_lenght`='".$_POST["ezf_field_lenght"]."' "
                            . "WHERE `ezf_field_id`='".$modelfield->ezf_field_id."' ";
                    Yii::$app->db->createCommand($sqlUpdate)->execute();
					
                    if ($modelfield->save()) {
                        $strlenght = count($_POST['selectValue']);
						EzformFunc::deleteCondition($modelfield);
                        for ($i = 0; $i < $strlenght; $i++) {
							$choiceModel = new EzformChoice();
							$choiceModel->ezf_choice_id = GenMillisecTime::getMillisecTime();
							$choiceModel->ezf_field_id = $modelfield->ezf_field_id;
							$choiceModel->ezf_choicevalue = $_POST['selectValue'][$i];
							$i++;
							$choiceModel->ezf_choicelabel = $_POST['selectValue'][$i];
							$choiceModel->save();
                        }
						EzformFunc::saveCondition($_POST['conditionFields']);
                        $result = [
                                'status' => 'success',
                                'action' => 'update'
                            ];
                        return $result;
                    } else {
                        echo "FALSE";
                    }
                }
            }
        }
    }

    protected function findFieldModel($id) {
        if (($model = EzformFields::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}

?>