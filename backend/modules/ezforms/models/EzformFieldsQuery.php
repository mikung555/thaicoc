<?php

namespace backend\modules\ezforms\models;

/**
 * This is the ActiveQuery class for [[EzformFields]].
 *
 * @see EzformFields
 */
class EzformFieldsQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return EzformFields[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return EzformFields|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}