<?php

namespace backend\modules\ezforms\models;

/**
 * This is the ActiveQuery class for [[Ezform]].
 *
 * @see Ezform
 */
class EzformQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return Ezform[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Ezform|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}