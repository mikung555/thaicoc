<?php

namespace backend\modules\ezforms\models;

use Yii;

/**
 * This is the model class for table "ezform_choice".
 *
 * @property string $ezf_choice_id
 * @property string $ezf_field_id
 * @property string $ezf_choicevalue
 * @property string $ezf_choicelabel
 * @property string $ezf_choiceetc
 * @property integer $ezf_choice_col
 *
 * @property EzformFields $ezfField
 */
class EzformChoice extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ezform_choice';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ezf_choice_id'], 'required'],
            [['ezf_choice_id', 'ezf_field_id', 'ezf_choiceetc', 'ezf_choice_col'], 'integer'],
            [['ezf_choicevalue', 'ezf_choicelabel'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ezf_choice_id' => 'รหัสตัวเลือกคำตอบ',
            'ezf_field_id' => 'รหัสคำถาม',
            'ezf_choicevalue' => 'ค่าตัวเลือกคำตอบ',
            'ezf_choicelabel' => 'ชื่อตัวเลือกคำตอบ',
            'ezf_choiceetc' => 'ค่าตัวเลือกคำตอบอื่นๆ',
            'ezf_choice_col' => 'ความกว้างตัวเลือก',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEzfField()
    {
        return $this->hasOne(EzformFields::className(), ['ezf_field_id' => 'ezf_field_id']);
    }

    /**
     * @inheritdoc
     * @return EzformChoiceQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new EzformChoiceQuery(get_called_class());
    }
}
