<?php

namespace backend\modules\ezforms\models;

use Yii;

/**
 * This is the model class for table "ezform_sql_log".
 *
 * @property string $id
 * @property string $data_id
 * @property string $ezf_id
 * @property string $sql_log
 * @property string $user_id
 * @property string $create_date
 * @property string $xsourcex
 * @property integer $rstat
 */
class EzformSqlLog extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ezform_sql_log';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['data_id', 'ezf_id', 'user_id'], 'required'],
            [['data_id', 'ezf_id', 'user_id', 'rstat'], 'integer'],
            [['sql_log'], 'string'],
            [['create_date'], 'safe'],
            [['xsourcex'], 'string', 'max' => 10]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'data_id' => 'Data ID',
            'ezf_id' => 'Ezf ID',
            'sql_log' => 'Sql Log',
            'user_id' => 'User ID',
            'create_date' => 'Create Date',
            'xsourcex' => 'Xsourcex',
            'rstat' => 'Rstat',
        ];
    }
}
