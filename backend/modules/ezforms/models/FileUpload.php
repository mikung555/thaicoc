<?php

namespace backend\modules\ezforms\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\Expression;
use yii\db\BaseActiveRecord;
/**
 * This is the model class for table "file_upload".
 *
 * @property integer $fid
 * @property string $target
 * @property string $file_name
 * @property integer $file_active
 * @property integer $ezf_id
 * @property integer $ezf_field_id
 * @property integer $created_by
 * @property string $created_at
 * @property integer $tbid
 * @property integer $file_name_old
 * @property integer $mode
 */
class FileUpload extends \yii\db\ActiveRecord
{
    public function behaviors() {
	return [
	    [
		    'class' => TimestampBehavior::className(),
		    'attributes' => [
			BaseActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
		    ],
		    'value' => new Expression('NOW()'),
	    ],
	    [
		    'class' => BlameableBehavior::className(),
		    'attributes' => [
			BaseActiveRecord::EVENT_BEFORE_INSERT => ['created_by'],
		    ],
	    ],
	];
    }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'file_upload';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['file_name', 'tbid', 'ezf_id', 'ezf_field_id'], 'required'],
            [['file_name', 'file_name_old'], 'string'],
            [['file_active', 'ezf_id', 'ezf_field_id', 'created_by'], 'integer'],
            [['target', 'mode', 'created_at'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'fid' => Yii::t('app', 'ID'),
	    'tbid' => Yii::t('app', 'FK ID'),
            'target' => Yii::t('app', 'Target'),
            'file_name' => Yii::t('app', 'File Name'),
	    'file_name_old' => Yii::t('app', 'File Name'),
            'file_active' => Yii::t('app', 'Active'),
            'ezf_id' => Yii::t('app', 'Ezform'),
            'ezf_field_id' => Yii::t('app', 'Field'),
            'created_by' => Yii::t('app', 'By'),
            'created_at' => Yii::t('app', 'วันที่'),
			'mode' => Yii::t('app', 'โหมด'),
        ];
    }
}
