<?php

namespace backend\modules\ezforms\models;

use Yii;

/**
 * This is the model class for table "ezform_data_log".
 *
 * @property string $id
 * @property string $dataid
 * @property string $ezf_id
 * @property string $ezf_field_name
 * @property string $old
 * @property string $change
 * @property string $xsourcex
 * @property string $user_create
 * @property string $create_date
 * @property string $update_date
 */
class EzformDataLog extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ezform_data_log';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['dataid', 'ezf_id', 'user_create'], 'integer'],
            [['old', 'change'], 'string'],
            [['create_date', 'update_date'], 'safe'],
            [['ezf_field_name'], 'string', 'max' => 100],
            [['xsourcex'], 'string', 'max' => 10]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'dataid' => 'Dataid',
            'ezf_id' => 'Ezf ID',
            'ezf_field_name' => 'Ezf Field Name',
            'old' => 'Old',
            'change' => 'Change',
            'xsourcex' => 'Xsourcex',
            'user_create' => 'User Create',
            'create_date' => 'Create Date',
            'update_date' => 'Update Date',
        ];
    }
}
