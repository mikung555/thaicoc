<?php
    use yii\web\View;
    use yii\helpers\Url;
?>
<div class="modal-content">
    <form id="formGridType">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">เลือกประเภทฟิลด์ของหัวข้อที่ <?php echo $row?></h4>
    </div>
    <div class="modal-body">
        <select class="form-control" name="gridType" id="gridType" onChange="changeGridType($('#gridType').val(),<?php echo $row;?>);">
            <option value="251">
                - Text -
            </option>
            <option value="252" selected>
                - Textarea -
            </option>
            <option value="253">
                - Date -
            </option>
            <option value="254">
                - Single checkbox -
            </option>
<!--            <option value="255">-->
<!--                - Multiple Choice --->
<!--            </option>-->
        </select>
    <br>
    <div id="gridChosen" style="display:none;">
        <div class="row">
            <div class="col-md-12">
                <p>ตัวเลือก</p>
            </div>
        </div>
        <div class="row" radio-id="1">
            <div class="col-md-2">
                <input type="number" class="form-control" name="radio[value][]" value="1"/>
            </div>
            <div class="col-md-8">
                <input type="text" class="form-control" name="radio[label][]" value="รายการที่ 1"/>
            </div>
        </div>
        <br>
        <div class="row" radio-id="2">
            <div class="col-md-2">
                <input type="number" class="form-control" name="radio[value][]" value="2"/>
            </div>
            <div class="col-md-8">
                <input type="text" class="form-control" name="radio[label][]" value="รายการที่ 2"/>
            </div>
            <div class="col-md-2">
                <a class="btn btn-default" onclick="deleteGridRadio('2')"><i class="fa fa-close"></i></a>
            </div>
        </div>
        <div id="appendRadio">

        </div>
        <br>
        <div class="row">
            <div class="col-md-12">
                <a class="form-control .btn btn-success" onclick="addGridRadio()"><i class="fa fa-plus"></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;เพิ่มตัวเลือก</a>
            </div>
        </div>
    </div>
    </div>
    <br>
    <div class="modal-footer">
        <a type="button" class="btn btn-primary" onClick="submitGridType($('#gridType').val(),<?php echo $row?>)">ตกลง</a>
    </div>
        </form>
</div><!-- /.modal-content -->
<?php
    $this->registerJs("

            function submitGridType(type,row){



                $('input[id=icon_'+row+']').val(type);
                if(type==='251'){
                    $('span[id=gridTextType_'+row+']').html('Text');
                }else if(type==='252'){
                    $('span[id=gridTextType_'+row+']').html('Textarea');
                }else if(type==='253'){
                    $('span[id=gridTextType_'+row+']').html('Date');
                }else if(type==='254'){
                    $('span[id=gridTextType_'+row+']').html('Single checkbox');
                    $('input[id=valuelabel_'+row+']').show();
                }else{
                    $('input[id=valuelabel_'+row+']').attr('style','display:none;');
                }
                if(type==='255'){

                    $.post('".Url::to(['grid/inputgridchoice'])."',$('form[id=formGridType]').serialize(),function(result){
                        $('span[id=gridTextType_'+row+']').html('Multiple Choice');
                        $('input[id=gridRadio_'+row+']').val(result);
                    });

                }

                $('#modal-gridtype').modal('toggle');

            }

            function addGridRadio(){

                var radioId = $('div[radio-id]').length;
                radioId++;
                var radioGridHtml = '<div class=\"row\" radio-id=\"'+radioId+'\">';
                    radioGridHtml += '<br><div class=\"col-md-2\"><input type=\"number\" class=\"form-control\" name=\"radio[value][]\" value=\"'+radioId+'\"></div>';
                    radioGridHtml += '<div class=\"col-md-8\"><input type=\"text\" class=\"form-control\" name=\"radio[label][]\" value=\"รายการที่ '+radioId+'\"></div>';
                    radioGridHtml += '<div class=\"col-md-2\"><button class=\"btn btn-default\" onclick=\"deleteGridRadio('+radioId+')\"><i class=\"fa fa-close\"></i></button></div>';
                    radioGridHtml += '</div>';
                $('#appendRadio').append(radioGridHtml);
            }
            function deleteGridRadio(radioId){
                $('div[radio-id='+radioId+']').remove();
            }
            function changeGridType(type,row){

                if(type==255){

                    $('#gridChosen').show();

                }
                else
                {

                    $('#gridChosen').hide();

                }

            }

        ",View::POS_READY,4);
?>