
<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->registerJs("
bootbox.alert(\"<div class = 'alert alert-danger h3'>ไม่สามารก Submit ข้อมูลนี้ได้ เนื่องจากมีการ Submit จากหน่วยงานอื่นแล้ว</div>\", function() {
    window.history.back();
});

");
?>

