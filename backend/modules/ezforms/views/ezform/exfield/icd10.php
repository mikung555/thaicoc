<?php
use yii\helpers\Html;
use kartik\widgets\Select2;
/* Test icd9
 */
$this->registerCssFile('/select2/css/select2.css');
$this->registerJsFile('/select2/js/select2.min.js');
?>
<label>ICD10 Lists</label>
<select class="js-example-tags form-control" multiple="multiple" diabled="disabled" placeholder="" disabled="disabled">
<!--          <option selected="selected">Snomed Code 1</option>
          <option selected="selected">Snomed Code 2</option>-->
</select>
<script>
$(".js-example-tags").select2({
  tags: true
})
</script>
