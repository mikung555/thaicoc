<?php 
use yii\helpers\Html;
use kartik\widgets\Select2;
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$this->registerCssFile('/select2/css/select2.css');
$this->registerJsFile('/select2/js/select2.min.js');
?>
<label>Hospital list</label>
<select class="js-example-tags form-control" multiple="multiple" diabled="disabled" placeholder="" disabled="disabled">
<!--          <option selected="selected">Snomed Code 1</option>
          <option selected="selected">Snomed Code 2</option>-->
</select>
<script>
$(".js-example-tags").select2({
  tags: true
})
</script>
