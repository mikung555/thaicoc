<?php
use yii\helpers\Html;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<div class='row' id='inputCheckbox'>
  <div class="col-md-10 col-md-offset-2" style='padding: 10px;'>
      <label >การจัดเรียงของตัวเลือก </label>
      <div class='radio-inline'>
        <label>
          <input type="radio" name="ezf_field_default" id='ezf_field_default' value="1" checked> แนวตั้ง
        </label>
      </div>
      <div class='radio-inline'>
          <label>
            <input type="radio" name="ezf_field_default" id='ezf_field_default' value="2" > แนวนอน
          </label>
      </div>
  </div>
  <div class="col-md-10 col-md-offset-2" style='padding: 10px;'>
        <div class='col-md-2'>
            <label>ตัวเลือก</label>
        </div>
        <div class='col-md-3'>
            <label>คำอธิบาย</label>
        </div>
        <div class='col-md-3'>
            <label>คำถามเพิ่มเติม</label>
        </div>
  </div>
</div>
<?php
    if(isset($modelcheck)){

$i=1;
    foreach($modelcheck as $check){
?>

<div class="row"  id="inputCheckbox" >
<div class="col-md-10 col-md-offset-2" style='padding:10px;' checkbox-id='<?php echo $i;?>'>
		<input type="hidden" name="" id="defaultValueChk_<?php echo $i;?>" value="<?php echo $check["ezf_field_name"];?>"/>
        <input type="hidden" name="checkColumn[var_old][<?php echo $check["ezf_field_name"];?>]" id="checkboxColumn_<?Php echo $i;?>" value="old"/>
    <div class='col-md-2'>

        <input type='text' class='form-control var_text' value='<?php echo $check["ezf_field_name"]?>' id='checkboxValue_<?php echo $i;?>'  name="checkbox[value][]" onfocus="saveVar(<?php echo $i;?>)" onfocusout="editVar('<?php echo $i;?>');"/>
    </div>
    <div class='col-md-3'>
        <input type='text' class='form-control' value='<?php echo $check["ezf_field_label"]?>' id='checkboxlabel_<?php echo $i;?>'  name="checkbox[label][]"/>
    </div>
    <div class='col-md-3'>
        <div id='divCheckBoxText' checkbox-text-id='<?php echo $i;?>'>
    <?php if($check->ezf_field_sub_textvalue!==""){?>
        <input type="hidden" name="" id="defaultValueTxt_<?php echo $i;?>" value="<?php echo $check["ezf_field_sub_textvalue"];?>"/>
        <input type="hidden" name="checkColumn[label_old][<?php echo $check["ezf_field_sub_textvalue"];?>]" id="labelColumn_<?Php echo $i;?>" value="old">
        <input type='text' class='form-control other_text'
                id='checkboxText_<?php echo $i;?>'
                name='checkbox[text][]'
                value="<?php echo $check["ezf_field_sub_textvalue"]?>"
                style='width:150px;'
                onfocus="saveVarLabel('<?php echo $i?>');"
                onfocusout="editVarLabel('<?php echo $i;?>');">
        <i class="fa fa-close" style="position:relative;left: 117px;top:-27px;cursor:pointer;color:#9F9F9F;" onClick="removeCheckboxText('<?php echo $nameValue?>','<?php echo $i;?>')"></i>
    <?php }else{?>
       <input type="hidden" name="checkColumn[label_old][label_<?Php echo $i;?>]" id="labelColumn_<?Php echo $i;?>" value="old" >
        <span style='color:blue;cursor: pointer;font-style: oblique;' onclick="addCheckboxText('<?php echo $nameValue?>','<?php echo $i;?>')">+ คำถามเพิ่มเติม</span>
        <input type='hidden' class='form-control other_text' value=''id='checkboxText_<?php echo $i;?>' name='checkbox[text][]'/>
    <?php }?>
        </div>
    </div>
    <div class="col-md-2">

        <i onclick='deleteCheckbox("<?php echo $i;?>")' item-id='"+numCheckbox+"'class='fa fa-close' style='color:red;font-size:25px;cursor:pointer;'></i>
    </div>
</div>
</div>
<?php
     $i++;   }
    }else{
?>
<div class="row"  id="inputCheckbox" >
    <div class="col-md-10 col-md-offset-2" style='padding:10px;' checkbox-id='1'>

        <div class='col-md-2'>
            <input type='text' class='form-control var_text' value='<?php echo $nameValue;?>_1' id='checkboxValue_1'  name="checkbox[value][]"/>
        </div>
        <div class='col-md-3'>
            <input type='text' class='form-control' value='ตัวเลือกที่ 1' id='checkboxLabel_1'  name='checkbox[label][]'/>
        </div>
        <div class='col-md-3'>
            <div id='divCheckBoxText' checkbox-text-id='1'>
            <span style='color:blue;cursor: pointer;font-style: oblique;' onclick="addCheckboxText('<?php echo $nameValue?>','1')">+ คำถามเพิ่มเติม</span>
            <input type='hidden' class='form-control other_text' value=''id='checkboxText_1' name='checkbox[text][]'/>
            </div>
        </div>

    </div>
</div>
    <?php }?>
<div class='row' id='addFieldCheckbox' >
</div>
<div class='row' id='addEtcFieldCheckbox'>
</div>
<div class='row' id='buttonAddCheckbox'>
    <div class="col-md-10 col-md-offset-2" style='padding:10px;'>
        <div class='col-md-2'>
            <input type='text' class='form-control' value='+' id='addCheckbox' style='border:1px #eee;cursor:pointer;'readonly>
        </div>
        <div class='col-md-5'>
            <input type='text' class='form-control' value='คลิ้กเพื่อเพิ่มตัวเลือก' id='addCheckbox' style='border:1px #eee;cursor:pointer;' readonly>
        </div>

    </div>
</div>
<input type="hidden" class="form-control" id="deleteColumn" name="checkColumn[delete_old]" value="" />
<script>
    
    $('#showPanel').on('blur', '.other_text', function() {
        var value = $(this).val();
        
        if(!value.match(/^[a-z0-9_]+$/i)){
            $(this).parent().parent().find('.help-block').remove();
            $(this).parent().parent().append('<div class=\"help-block help-block-error\"><code>ตัวแปรต้องเป็นภาษาอังกฤษหรือตัวเลขเท่านั้นและห้ามเว้นวรรค</code></div>');
            $(this).focus();
        } else {
            $(this).parent().parent().find('.help-block').remove();
        }
        
    });
    
    $('#showPanel').on('blur', '.var_text', function() {
        var value = $(this).val();
        
        if(!value.match(/^[a-z0-9_]+$/i)){
            $(this).parent().find('.help-block').remove();
            $(this).parent().append('<div class=\"help-block help-block-error\"><code>ตัวแปรต้องเป็นภาษาอังกฤษหรือตัวเลขเท่านั้นและห้ามเว้นวรรค</code></div>');
            $(this).focus();
        } else {
            $(this).parent().find('.help-block').remove();
        }
        
    });
    
    var numData = $('div[checkbox-id]').length+1;
    $('.row').on('click','#addCheckbox',function(){
        var nameCheckbox = '<?php echo $nameValue;?>';
        var numCheckbox = numData++;
        var numCheckboxEtc = $('div[checkbox-etc-id]').length;
        var checkboxHtml  = "<div class='col-md-10 col-md-offset-2' style='padding:10px;' checkbox-id='"+numCheckbox+"'>";
			checkboxHtml += "<input type='hidden' class='form-control' id='checkboxColumn_"+numCheckbox+"' name='checkColumn[var_old]["+nameCheckbox+"_"+numCheckbox+"]' value='new' >";
            checkboxHtml += "<div class='col-md-2'>";

            checkboxHtml += "<input type='text' class='form-control var_text' value='"+nameCheckbox+"_"+numCheckbox+"'  id='checkboxValue_"+numCheckbox+"' name='checkbox[value][]' onfocusout='editVarNew("+numCheckbox+");' >";
            checkboxHtml += "</div>";
            checkboxHtml += "<div class='col-md-3'>";
            checkboxHtml += "<input type='text' class='form-control' value='ตัวเลือกที่ "+numCheckbox+"' id='checkboxLabel_"+numCheckbox+"' name='checkbox[label][]' >";
            checkboxHtml += "</div>";
            checkboxHtml += "<div class='col-md-3'>";
            checkboxHtml += "<div id='divCheckBoxText' checkbox-text-id='"+numCheckbox+"'>";
            checkboxHtml += "";
            checkboxHtml += "<span style='color:blue;cursor: pointer;font-style: oblique;' onclick=addCheckboxText('"+nameCheckbox+"','"+numCheckbox+"')>+ คำถามเพิ่มเติม</span>";
            checkboxHtml += "<input type='hidden' class='form-control other_text' value='' id='checkbox[text][]' name='checkbox[text][]'/>";
            checkboxHtml += "</div>";
            checkboxHtml += "</div>";
            checkboxHtml += "<div class='col-md-2'>";
            checkboxHtml += "<i onclick='deleteCheckbox("+numCheckbox+")' item-id='"+numCheckbox+"'class='fa fa-close' style='color:red;font-size:25px;cursor:pointer;'></i>";
            checkboxHtml += "</div>";
            checkboxHtml += "</div>";
        $('#addFieldCheckbox').append(checkboxHtml);
    });
    function addCheckboxText(value,order){
        var checkboxText = "<input type='hidden' class='form-control' id='labelColumn_"+order+"' name='checkColumn[label_old]["+value+"_"+order+"x]' value='new'>";
            checkboxText += "<input type='text' class='form-control other_text' id='checkboxText_"+order+"' name='checkbox[text][]' value='"+value+"_"+order+"x'  onfocusout='editVarTxtNew("+order+")'; style='width:150px;'>";
            checkboxText += "<i class='fa fa-close' style='position:relative;left: 119px;top:-27px;cursor:pointer;color:#9F9F9F;' onClick=removeCheckboxText('"+value+"','"+order+"')></i>";
        $("#divCheckBoxText[checkbox-text-id='"+order+"']").html(checkboxText);
    }
    function removeCheckboxText(value,i){
        var buttonText = "<span style='color:blue;cursor: pointer;font-style: oblique;' onclick=addCheckboxText('"+value+"','"+i+"')>+ คำถามเพิ่มเติม</span>";
            buttonText += "<input type='hidden' class='form-control other_text' value='' id='checkboxText_"+i+"' name='checkbox[text][]'/>";
        var typeColumn = $('#labelColumn_'+i+'').val();
        if(typeColumn==='new'){
            $("#divCheckBoxText[checkbox-text-id='" + i + "']").html(buttonText);
            $("#divCheckBoxText[checkbox-text-id='" + i + "']").parent().find('.help-block').remove();
        }
        else if (confirm('ถ้าคุณลบคำถามนี้ ข้อมูลที่คุณมีอยู่จะถูกลบด้วย ?') == true) {
            var valueTxt = $('#checkboxText_'+i+'').val()+",";
            $('#deleteColumn').val($('#deleteColumn').val()+valueTxt);
            $("#divCheckBoxText[checkbox-text-id='" + i + "']").html(buttonText);
            $("#divCheckBoxText[checkbox-text-id='" + i + "']").parent().find('.help-block').remove();
        }else{
            return false;
        }
    }
    function deleteCheckbox(i){
        var typeColumn = $('#checkboxColumn_'+i+'').val();
        if(typeColumn==='new'){
            $("div[checkbox-id="+i+"]").remove();
        }
        else if (confirm('ถ้าคุณลบคำถามนี้ ข้อมูลที่คุณมีอยู่จะถูกลบด้วย ?') == true) {
            var valueChk = $('#checkboxValue_'+i+'').val();
            var valueTxt = $('#checkboxText_'+i+'').val();
            if(valueTxt==''){
                var valueDelete = valueChk+",";
            }else{
                var valueDelete = valueChk+","+valueTxt+",";
            }
            $('#deleteColumn').val($('#deleteColumn').val()+valueDelete);
            $("div[checkbox-id="+i+"]").remove();
        }else{
            return false;
        }
        renderTap('div[checkbox-id]');
    }
    function editVar(i) {
        var var_old = $('#checkboxColumn_' + i + '').val();
        var var_new = $('#checkboxValue_' + i + '').val();
        var default_var = $('#defaultValueChk_'+i+'').val();
        console.log(var_old+" "+var_new);
        if(var_old !== var_new){
            $('#checkboxColumn_'+i+'').attr('name','checkColumn[var_old]['+var_new+']');
            $('#checkboxColumn_' + i + '').val(default_var);
        }else{
            $('#checkboxColumn_' + i + '').val('old');
        }
    }
    function editVarNew(i){
        var var_new = $('#checkboxValue_' + i + '').val();
        //console.log(var_new);
        $('#checkboxColumn_'+i+'').attr('name','checkColumn[var_old]['+var_new+']');

    }
    function editVarTxtNew(i){
        var var_new = $('#checkboxText_'+i+'').val();
        $('#labelColumn_'+i+'').attr('name','checkColumn[label_old]['+var_new+']');

    }
    function saveVar(i){
        var var_old = $('#checkboxValue_' + i + '').val();
        $('#checkboxColumn_' + i + '').val(var_old);
    }
    function saveVarLabel(i){
        var var_old = $('#checkboxText_'+i+'').val();
        $('#labelColumn_'+i+'').val(var_old);
    }
    function editVarLabel(i){
        var var_old = $('#labelColumn_' + i + '').val();
        var var_new = $('#checkboxText_' + i + '').val();
        var default_var = $('#defaultValueTxt_'+i+'').val();
        if(var_old !== var_new){
            $('#labelColumn_'+i+'').attr('name','checkColumn[label_old]['+var_new+']');
            $('#labelColumn_' + i + '').val(default_var);
        }else{
            $('#labelColumn_' + i + '').val('old');
        }
    }

</script>
