<?php
    use yii\web\View;
    use yii\helpers\Url;
?>
<div class="row">
    <div class="col-lg-2">
        <label>จำนวนหัวข้อ</label>
        <input type="number" id="colNumber" name="colNumber" class="form-control" value="<?php if(isset($model2)){echo count($choice);}else{echo "3";}?>">
    </div>
    <div class="col-lg-2">
        <label>จำนวนแถว</label>
        <input type="number" id="colRow" name="colRow" class="form-control" value="<?php if(isset($model2)){echo count($question);}else{echo "3";}?>">
    </div>
    <div class="col-lg-1">
        <label>&nbsp;&nbsp;&nbsp;</label>
        <div class="checkbox">
            <label>
                <input type="checkbox" id="Question" name="Question"> คำถาม
            </label>
        </div>
    </div>
    <div class="col-lg-2">
        <label>&nbsp;&nbsp;&nbsp;</label>
        <i class="form-control btn btn-success" onclick="GenerateGrid()">สร้าง Grid</i>
    </div>
</div>
<br><br>
<div class="row" id="divTable">
    <table class="table">
        <thead>
            <tr>
            <?php
	    
                if(isset($model2)){
                    foreach($question as $q){
                        if($q["ezf_field_label"]!=""){
                            $havequestion = 1;
                        }else{
                            $havequestion = 0;
                        }
                    }
                    if($havequestion==1){
                        echo "<th></th>";
                    }else{
                        echo "";
                    }
                    foreach($choice as $c){

                        ?>

                        <td><input type='text' class='form-control' name='labelGrid[]' value='<?php echo $c["ezf_choicelabel"];?>'></td>

                        <?php
                    }
                    ?>


            </tr>
            <tr>
                <?php if($havequestion==1){echo "<th></th>";}else{echo "";}?>
                <?php $i=1; foreach($choice as $c){

                    ?>
                <td><select class="form-control" onchange="changeType($(this).val(),<?php echo $i;?>)">
                        <option>- เลือกประเภท -</option>
                        <option value="251">- Text -</option>
                        <option value="252">- Textarea -</option>
                        <option value="253">- Date -</option>
			<option value="254">- single checkbox -</option>
                    </select>

                </td>
                <?php $i++;}?>
            </tr>
        </thead>
                    <?php
                    }
                    ?>
        <tbody>
            <?php
                if(isset($model2)){
                    if($havequestion==1){
                        $j=1;
                        foreach($question as $q){
                                $value = \backend\modules\ezforms\models\EzformFields::find()
                                    ->where('ezf_field_sub_id = :field_id',[':field_id'=>$q["ezf_field_id"]])
                                    ->all();
                            echo "<tr>";
                            echo "<td><input type='text' class='form-control' name='grid[".$j."][title]' value='".$q["ezf_field_label"]."'></td>";
                            $i=1;
			    
                            foreach($value as $v){
                                echo "<td><input class='form-control other_text' name='grid[".$j."][value][".$i."][]' value=".$v["ezf_field_name"]."><br>";
                                echo "<input type='text' class='form-control' id='valuelabel_".$i."' name='grid[".$j."][value][".$i."][]' value='".(($v["ezf_field_label"]!=='')?$v["ezf_field_label"]:"ตัวเลือกที่ $i")."' ".(($v["ezf_field_type"]==='254')?"":'style="display:none;"').">";
                                echo "<input type='hidden' id='icon_".$i."' class='form-control' name='grid[".$j."][value][".$i."][]' value='".$v["ezf_field_type"]."'></td>";
                                $i++;
                            }
                            $j++;
                            echo "</tr>";
                        }
                    }else{
                        $j=1;
                        foreach($question as $q){
			    
                            $value = \backend\modules\ezforms\models\EzformFields::find()
                                ->where('ezf_field_sub_id = :field_id',[':field_id'=>$q["ezf_field_id"]])
                                ->all();

                            echo "<tr>";
                            echo "<input type='hidden' class='form-control' name='grid[".$j."][title]' value=''>";
                            $i=1;
                            foreach($value as $v){
                                echo "<td><input class='form-control other_text' name='grid[".$j."][value][".$i."][]' value=".$v["ezf_field_name"]."><br>";
                                echo "<input type='text' class='form-control' id='valuelabel_".$i."' name='grid[".$j."][value][".$i."][]' value='".(($v["ezf_field_label"]!=='')?$v["ezf_field_label"]:"ตัวเลือกที่ $i")."' ".(($v["ezf_field_type"]==='254')?"":'style="display:none;"').">";
                                echo "<input type='hidden' id='icon_".$i."' class='form-control' name='grid[".$j."][value][".$i."][]' value='".$v["ezf_field_type"]."' ></td>";
                                $i++;
                            }
                            $j++;
                            echo "</tr>";
                        }
                    }
                }
            ?>
        </tbody>
    </table>
</div>

<?php
    $this->registerJs("
        function GenerateGrid(){
            var colNumber = $('#colNumber').val();
            var colRow = $('#colRow').val();
            var question;
            if($('#Question').is(':checked')) {
                    colNumber-1;
                    colRow-1;
                 question = 1;
            }else{
                 question = 0;
            }
            if(colNumber < 0){
                var noty_id = noty({'text': \"กรุณากรอกตัวเลขมากกว่า 3 ขี้นไป\", 'type': 'warning','layout':'center'});
            }else{
                genTable1st(colNumber,colRow,question);
            }
        }
        function genTable1st(colNumber,colRow,question){
            var table = '<table class=\"table\">';
                table += '<thead>';
                table += '<tr>';
                if(question==1){
                    table += '<th align=\"center\"></th>';
                }
                for(i=1;i<=colNumber;i++){

                    table += '<td>';
                    table += 'Type :: <span id=\"gridTextType_'+i+'\">Textarea</span>';
                    table += '<input type=\"text\" class=\"form-control\" id=\"labelGrid\" name=\"labelGrid[]\" value=\"หัวข้อ c'+i+'\"><br>';
                    table += '<select class=\"form-control\" onchange=\"changeType($(this).val(),'+i+')\" >';
                    table += '<option>- เลือกประเภท -</option><option value=\"251\">- text -</option><option value=\"252\" selected>- textarea -</option><option value=\"253\">- date -</option>';
                    table += '<option value=\"254\">- single checkbox -</option>'
//                    table += '<option value=\"255\">- multiple choice -</option>'
                    table += '</select><br>';
//                    table += '<i class=\"form-control btn btn-success\" onclick=\"changeType('+i+')\">เลือกประเภทฟิลด์</i>';
                    table += '</td>';
                }
                table += '</tr>';
                table += '</thead>';
                table += '<tbody>';
                for(j=1;j<=colRow;j++){
                    table += '<tr>';
                    if(question==1){
                        table += '<td><input type=\"text\" class=\"form-control\" name=\"grid['+j+'][title]\" value=\"คำถามที่ '+j+'\"></td>';
                    }else{
                        table += '<input type=\"hidden\" class=\"form-control\" name=\"grid['+j+'][title]\" value=\"\">';
                    }
                    for(i=1;i<=colNumber;i++){
                        table += '<td id=\"form_'+i+'\">';
                        table += '<input type=\"text\" class=\"form-control other_text\" name=\"grid['+j+'][value]['+i+'][]\" id=\"\" value=\"".$nameValue."r'+j+'c'+i+'\">';
                        table += '<br><input type=\"text\" class=\"form-control\" id=\"valuelabel_'+i+'\" name=\"grid['+j+'][value]['+i+'][]\" value=\"ตัวเลือกที่ '+j+'\" style=\"display:none;\"> ';
//                        table += '<br><input type=\"text\" class=\"form-control\" id=\"gridRadio_'+i+'\" name=\"grid['+j+'][value]['+i+'][]\" value=\"ตัวเลือกที่ '+j+'\" style=\"display:none;\"> ';
                        table += '<input type=\"hidden\" id=\"icon_'+i+'\" name=\"grid['+j+'][value]['+i+'][]\" value=\"252\">';
                        table += '</td>';
                    }
                    table += '<tr>';
                }
                table += '</tr>';
                table += '</tbody>';
                table += '</table>';
            $('#divTable').html(table);
        }
        function changeType(type,i){
            $('input[id=icon_'+i+']').val(type);
            if(type==='254'){
                $('input[id=valuelabel_'+i+']').show();
            }else{
                $('input[id=valuelabel_'+i+']').attr('style','display:none;')
            }
            if(type==='255'){
               $('a[id=choice_'+i+']').show(2);
            }
        }
//        function changeType(i){
//            var gridType = $('#gridTextType_'+i+'').html();
//            var gridRadio = $('#gridRadio_'+i+'').val();
//            modalEzformgridtype('" . Url::to(['grid/typegrid', 'row' => '']) . "'+i);
//
//        }
        function modalEzformgridtype(url) {

            $('#modal-gridtype .modal-content').html('<div class=\"sdloader\"><i class=\"sdloader-icon\"></i></div>');
            $('#modal-gridtype').modal('show')
            .find('.modal-content')
            .load(url);

        }
    ",View::POS_READY,4)
?>

<?php 
// set var top
$this->registerJs(" 
   $('#showPanel').on('blur', '.other_text', function() {
        var value = $(this).val();
        
        if(!value.match(/^[a-z0-9_]+$/i)){
            $(this).parent().find('.help-block').remove();
            $(this).parent().append('<div class=\"help-block help-block-error\"><code>ตัวแปรต้องเป็นภาษาอังกฤษหรือตัวเลขเท่านั้นและห้ามเว้นวรรค</code></div>');
            $(this).focus();
        } else {
            $(this).parent().find('.help-block').remove();
        }
        
    });
");
?>

