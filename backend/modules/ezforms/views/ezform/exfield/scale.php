<?php
/**
 * Created by PhpStorm.
 * User: balz
 * Date: 2/9/2558
 * Time: 15:16
 */
    use yii\web\View;
?>
<!--Panel Update Field-->
<!---->
<!---->

<?php if(isset($model2)){ ?>
    <div class="row">
        <div class="col-lg-6">
            <table class="table">
                <tr>
                    <td>คำถาม <br><code>(ตัวอย่าง คุณพึงพอใจกับกิจกรรมนี้มากน้อยเพียงใด)</code></td>
                    <td>ค่าของคำถาม</td>
                    <td></td>
                </tr>
                <?php $i=1; foreach($modelquestion as $question){?>
                <tr question-id="<?php echo $question["ezf_field_id"];?>">
                    <td>
                        <input type="text" class="form-control" name="question[label][]" value="<?php echo $question["ezf_field_label"]; ?>">
                    </td>
                    <td>
                        <input type="text" class="form-control other_text" name="question[value][]" value="<?php echo $question["ezf_field_name"]; ?>">
                    </td>
                    <td>
                        <i class="fa fa-close" style="cursor: pointer" onclick="deleteQuestionScale('<?php echo $question["ezf_field_id"];?>')"></i>
                    </td>
                </tr>
                <?php $i++;}?>
                <tr id="question">

                </tr>
                <tr>
                    <td colspan="3">
                        <span style="cursor: pointer;"  onclick="addQuestionScale()"><i class="fa fa-plus"><span>&nbsp;&nbsp;&nbsp;เพิ่มคำถาม
                    </td>
                </tr>
            </table>
        </div>
        <div class="col-lg-6">
            <table class="table">
                <tr>
                    <td>ข้อความระดับ <br><code class="">(ตัวอย่าง ดีมาก,ดี,พอใช้,แย่,แย่มาก)</code></td>
                    <td>ค่าของระดับ</td>
                    <td></td>
                </tr>
                <?php $i=1; foreach($modelchoice as $choice){?>
                    <tr grade-id="<?php echo $choice["ezf_choice_id"];?>">
                        <td>
                            <input type="text" class="form-control" name="scale[label][]" value="<?php echo $choice["ezf_choicelabel"]; ?>">
                        </td>
                        <td>
                            <input type="number" class="form-control" name="scale[value][]" value="<?php echo $choice["ezf_choicevalue"]; ?>">
                        </td>
                        <td>
                            <i class="fa fa-close" style="cursor: pointer" onclick="deleteGradeScale('<?php echo $choice["ezf_choice_id"];?>')"></i>
                        </td>
                    </tr>
                    <?php $i++;}?>
                <tr id="grade">

                </tr>
                <tr>
                    <td colspan="3">
                        <span style="cursor: pointer;" onclick="addGradScale()"><i class="fa fa-plus"><span>&nbsp;&nbsp;&nbsp;เพิ่มคำถาม
                    </td>
                </tr>
            </table>
        </div>
    </div>
<!--End Update Panel Field-->
<!---->
<!---->



<!--Add Penel Field-->
<!---->
<!---->
<?php }else{?>
    <div class="row">
        <div class="col-lg-6">
            <table class="table">
                <tr>
                    <td>คำถาม <code>(ตัวอย่าง คุณพึงพอใจกับกิจกรรมนี้มากน้อยเพียงใด)</code></td>
                    <td>ค่าของคำถาม</td>
                    <td></td>
                </tr>
                <tr question-id="1">
                    <td>
                        <input type="text" class="form-control" name="question[label][]" value="คำถามที่ 1">
                    </td>
                    <td>
                        <input type="text" class="form-control other_text" name="question[value][]" value="<?php echo $nameValue?>r1">
                    </td>
                    <td></td>
                </tr>
                <tr id="question">

                </tr>
                <tr>
                    <td colspan="3">
                        <span style="cursor: pointer;" onclick="addQuestionScale()"><i class="fa fa-plus"><span>&nbsp;&nbsp;&nbsp;เพิ่มคำถาม
                    </td>
                </tr>

            </table>
        </div>
        <div class="col-lg-6">
            <table class="table">
                <tr>
                    <td>ข้อความระดับ <code class="">(ตัวอย่าง ดีมาก,ดี,พอใช้,แย่,แย่มาก)</code></td>
                    <td>ค่าของระดับ</td>
                    <td></td>
                </tr>
                <tr grade-id="1">
                    <td>
                        <input type="text" class="form-control" name="scale[label][]" value="ระดับที่ 1"/>
                    </td>
                    <td>
                        <input type="number" class="form-control" name="scale[value][]" value="1" maxlength="2"/>
                    </td>
                    <td>

                    </td>
                </tr>
                <tr id="grade">

                </tr>
                <tr>
                    <td colspan="3">
                        <span style="cursor: pointer;" onclick="addGradScale()"><i class="fa fa-plus"><span>&nbsp;&nbsp;&nbsp;เพิ่มระดับ
                    </td>
                </tr>
            </table>
        </div>
    </div>

<!--End Add Panel Field-->
<!---->
<!---->
<?php }?>
<?php
    $this->registerJs("


            var countQuestion = 1;
            function addQuestionScale(){
                var nameValue = '".$nameValue."';
                countQuestion++;
                var QuestionHtml = '<tr question-id=\"'+countQuestion+'\">';
                    QuestionHtml += '<td><input type=\"text\" class=\"form-control\" name=\"question[label][]\" value=\"คำถามที่ '+countQuestion+'\"></td>';
                    QuestionHtml += '<td><input type=\"text\" class=\"form-control other_text\" name=\"question[value][]\" value=\"'+nameValue+'r'+countQuestion+'\"></td>';
                    QuestionHtml += '<td><i class=\"fa fa-close\" onclick=\"deleteQuestionScale('+countQuestion+')\" style=\"cursor:pointer\"></i></td>';
                    QuestionHtml += '</td></tr>';

                $('#question').before(QuestionHtml);
            }

            function deleteQuestionScale(question){

                $('[question-id=\"'+question+'\"]').remove();
            }

            var countGrade = 1;
            function addGradScale(){
                countGrade++;
                var GradeHtml = '<tr grade-id=\"'+countGrade+'\"><td >';
                    GradeHtml += '<input type=\"text\" class=\"form-control\" name=\"scale[label][]\" value=\"ระดับที่ '+countGrade+'\">';
                    GradeHtml += '</td><td>';
                    GradeHtml += '<input type=\"number\" class=\"form-control\" name=\"scale[value][]\" value=\"'+countGrade+'\">';
                    GradeHtml += '<td><i class=\"fa fa-close\" onclick=\"deleteGradeScale('+countGrade+')\" style=\"cursor:pointer\"></i></td>'
                    GradeHtml += '</td>';

                $('#grade').before(GradeHtml);
            }

            function deleteGradeScale(grade){
                $('[grade-id=\"'+grade+'\"]').remove();
            }



    ",View::POS_READY,4);
?>
<?php 
// set var top
$this->registerJs(" 
   $('#showPanel').on('blur', '.other_text', function() {
        var value = $(this).val();
        
        if(!value.match(/^[a-z0-9_]+$/i)){
            $(this).parent().find('.help-block').remove();
            $(this).parent().append('<div class=\"help-block help-block-error\"><code>ตัวแปรต้องเป็นภาษาอังกฤษหรือตัวเลขเท่านั้นและห้ามเว้นวรรค</code></div>');
            $(this).focus();
        } else {
            $(this).parent().find('.help-block').remove();
        }
        
    });
");
?>

