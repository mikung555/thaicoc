<?php
use yii\helpers\Html;
use kartik\widgets\Select2;

?>

<div class="form-group">
  <label for="js-component" class="col-sm-2 control-label">Component</label>
  <div class="col-sm-10">
    <select id="js-component" class="form-control">
      <?php
      foreach ($ezformComponent_maps as $key => $value) {
        echo "<option value='".$key."'>".$value."</option>";
      }
      ?>
    </select>
  </div>
</div>
