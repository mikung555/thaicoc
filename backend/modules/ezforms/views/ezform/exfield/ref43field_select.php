<?php
use kartik\widgets\Select2;
use yii\helpers\ArrayHelper;

//print_r($table);
$ref_field = array();
$ref_field = ArrayHelper::map($ezf_fields, 'ezf_field_id', 'ezf_field_name');

echo Select2::widget([
    'id' => 'select_field',
    'name' => 'select_field',
    //'value' => $target, // initial value
    'data' => $ref_field,
    'size' => Select2::MEDIUM,
    'options' => ['placeholder' => 'เลือก TDC Field', /*'multiple' => isset($tagMultiple) ? $tagMultiple : FALSE*/],
    'pluginOptions' => [
        'allowClear' => true
    ],
]);
