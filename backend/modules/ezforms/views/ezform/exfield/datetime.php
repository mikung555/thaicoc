<?php

use yii\helpers\Html;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<?php $this->registerCssFile("/datepickerthai/css/datepicker.css"); ?>
<?php $this->registerCssFile("/timepicker/css/bootstrap-timepicker.min.css"); ?>

<?php $this->registerJsFile("/datepickerthai/js/bootstrap-datepicker.js"); ?>
<?php $this->registerJsFile("/datepickerthai/js/bootstrap-datepicker-thai.js"); ?>
<?php $this->registerJsFile("/datepickerthai/js/locales/bootstrap-datepicker.th.js"); ?>
<?php $this->registerJsFile("/timepicker/js/bootstrap-timepicker.min.js"); ?>
<label>วันและเวลา</label>
<div class="row">
    <div class="col-lg-4">
        <input class='input-medium form-control'  type='text' placeholder="__/__/____ __:__" data-provide='datepicker' data-date-language='th-th' readonly>
    </div>
<!--    <div class="col-lg-4">
        <input id='timepicker1' class="form-control" type='text' class='input-small'>
    </div>-->
</div>

<?php
//$this->registerJs("$('.datepicker').datepicker()");
//$this->registerJs("$('#timepicker1').timepicker({
//                minuteStep: 5,
//                showInputs: false,
//                disableFocus: true,
//                showMeridian: false
//            });")
?>
