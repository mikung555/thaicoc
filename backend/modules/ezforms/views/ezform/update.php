<?php

use yii\web\View;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use common\lib\sdii\widgets\SDGridView;
use common\lib\sdii\widgets\SDModalForm;
use common\lib\sdii\components\helpers\SDNoty;
use kartik\widgets\Select2;
use kartik\tree\TreeView;
use kartik\tree\TreeViewInput;
use backend\models\TblTree;
use backend\modules\ezforms\components\GenForm;
use yii\web\JsExpression;

$siteconfig = \common\models\SiteConfig::find()->One();

$this->title = Yii::t('backend', 'ฟอร์ม ', [
            'modelClass' => 'Ezform',
        ]) . ':: ' . $model1->ezf_name;

$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Ezforms'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('backend', 'จัดการฟอร์ม');
$this->registerCssFile('/css/ezform.css');
$this->registerJsFile('/jqueryloading/jloading.js');
//$this->registerJs("
//        $(document).ready(function(){
//            $.blockUI();
//        });
//
//    ");
$op['language'] = 'th';
	
$q = array_filter($op);

$this->registerJsFile('https://maps.google.com/maps/api/js?key=AIzaSyCq1YL-LUao2xYx3joLEoKfEkLXsEVkeuk&'.http_build_query($q), [
    'position'=>\yii\web\View::POS_HEAD,
    'depends'=>'yii\web\YiiAsset',
]);
?>
<div class="ezform-update" >
    <br>
    <?= Html::a('<i class="fa fa-edit"></i>&nbsp;&nbsp;&nbsp;จัดการฟอร์ม', ['/ezforms/ezform/update', 'id' => $ezf_id], ['class' => 'btn btn-info btn-flat  ']) ?>
    <?= Html::a('<i class="fa fa-comments-o"></i>&nbsp;&nbsp;&nbsp;จัดการเป้าหมาย', ['/component/ezform-component/index', 'id' => $ezf_id], ['class' => 'btn btn-primary btn-flat']) ?>
    <?= Html::a('<i class="fa fa-file-excel-o"></i>&nbsp;&nbsp;&nbsp;สร้าง EZForm จาก Excel', ['/file-upload', 'id' => $ezf_id], ['class' => 'btn btn-primary btn-flat']) ?>
    <?= Html::a('<i class="fa fa-globe"></i>&nbsp;&nbsp;&nbsp;ดูฟอร์มออนไลน์', ['/ezforms/ezform/viewform', 'id' => $ezf_id], ['class' => 'btn btn-primary btn-flat']) ?>
    <?= Html::a('<i class="fa fa-table"></i>&nbsp;&nbsp;&nbsp;ดูข้อมูล/ส่งออกข้อมูล', ['/managedata/managedata/view-data', 'id' => $ezf_id], ['class' => 'btn btn-primary btn-flat']) ?>
    <div style='float:right'>
        <?= Html::a('<i class="fa fa-mail-reply"></i>&nbsp;&nbsp;&nbsp;กลับไปหน้าเลือกฟอร์ม', ['/ezforms/ezform/index', 'tab'=>1, 'id' => $ezf_id], ['class' => 'btn btn-warning btn-flat']) ?>
    </div>
    <br>
    <br>
    <div class="box box-primary" >
        <div class="box-body">

            <?php $form = ActiveForm::begin(['id' => $model1->formName()]); ?>
            <div class='row'>
                <div class='col-lg-6'>
		    <?php
//	    echo $form->field($model1, 'ezf_name')->widget(\kartik\widgets\FileInput::className(),[
//                'pluginOptions' => [
//		    'allowedFileExtensions'=>['pdf','png','jpg','jpeg'],
//                    'showRemove' => false,
//                    'showUpload' => false,
//                ],
//                'options' => ['multiple' => TRUE]
//	    ]);
	    ?>
                    <?= Html::activeHiddenInput($model1, 'ezf_id'); ?>
                    <?=
                            $form->field($model1, 'ezf_name')
                            ->textInput([
                                'maxlength' => true,
//                                'onblur' => "autoSave('ezf_name','" . $model1->ezf_id . "')",
                    ]);
                    ?>
                </div>
                <div class='col-lg-6'>
                    <?php
//          $data = [
//    "webmaster",
//    "user1",
//    "user2"
//];
                    ?>

                    <?php
                    $userprofile = \backend\models\UserProfile::findOne(['user_id' => $model1->user_create]);
                    echo $form->field($model1, 'user_create')
                            ->textInput([
                                'maxlength' => true,
//                                'onblur'=>"autoSave($(this).val(),'user_create','".$model1->ezf_id."')",
                                'readonly' => true,
                                'value' => $userprofile->firstname . ' ' . $userprofile->lastname,
                    ]);
                    ?>
                    <?php
// Select2::widget([
//    'name' => 'color_1',
//    'value' => ['red', 'green'], // initial value
//    'data' => $data,
//    'options' => ['placeholder' => 'Select a color ...', 'multiple' => false],
//    'pluginOptions' => [
//        'tags' => true,
//        'maximumInputLength' => 10
//    ],
//]);
                    ?>

                </div>
            </div>
            </div></div>
<?php
    echo SDModalForm::widget([
        'id' => 'modal-query-request',
        'size' => 'modal-lg',
        'tabindexEnable' => false,
	 'options'=>['style'=>'overflow-y:scroll;']
    ]);  
        if ($model1->comp_id_target != "") {
?>
    <div class="panel panel-success">
    <!--<div class="box-header with-border">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<h3><i class="fa fa-file-text-o"></i>  ฟอร์ม --><!--</h3></div>-->
        <div class="panel-heading" style='cursor:pointer' id='btnEzformTarget'>
            <b>ฟอร์มเป้าหมาย</b> <?=$ezformComponents[$model1->comp_id_target];?> <span class="pull-right"><i class="fa fa-eye"></i> กดเพื่อ แสดง/ซ่อน</span>
        </div>
    </div>     
<?php
        }
?>
    <div class='row'>
        <div class='col-lg-12'>
            <p style='cursor:pointer' id='btnSettingGeneral'><i class='fa fa-cog'></i></i> แสดง/ซ่อน การตั้งค่าทั่วไป</p>
        </div>
    </div>

     <div class="box box-primary" id='settingGeneral' style='display:none;'>
        <div class="box-body">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#setting1" aria-controls="setting1" role="tab" data-toggle="tab">ตั้งค่าทั่วไป</a></li>
                <li role="presentation"><a href="#setting2" aria-controls="setting2" role="tab" data-toggle="tab">คุณสมบัติ</a></li>
                <li role="presentation"><a href="#setting3" aria-controls="setting3" role="tab" data-toggle="tab">Code เสริม</a></li>
                <li role="presentation"><a href="#setting4" aria-controls="setting4" role="tab" data-toggle="tab">TDC Mapping</a></li>
		<li role="presentation"><a href="#setting5" aria-controls="setting5" role="tab" data-toggle="tab">Report</a></li>
                <li role="presentation"><a href="#setting6" aria-controls="setting6" role="tab" data-toggle="tab">CustomReport</a></li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
		<div role="tabpanel" class="tab-pane" id="setting5" style="padding-top: 10px;">
		    <?php
		    echo $this->render('@backend/modules/ezforms2/views/ezform-config/_report_config_widget', [
			'dataProvider' => $dpReport,
			'ezf_id' => $ezf_id,
		    ]);
		    ?>
		</div>
                <div role="tabpanel" class="tab-pane" id="setting6" style="padding-top: 10px;">
		    <?php
		    echo $this->render('@backend/modules/ezformreports/views/ezform-custom-report/_template', [
			'dataProvider' => $dpReport,
			'ezf_id' => $ezf_id,
		    ]);
		    ?>
		</div>
		
                <div role="tabpanel" class="tab-pane active" id="setting1" style="padding-top: 10px;">

                    <div class="row">
                        <div class="col-md-6">
<?php
        $items = [
            '0' => 'ไม่มีเป้าหมาย',
            '1' => 'Person profile',
            '2' => 'เลือกเป้าหมายอื่นๆ',
            '3' => 'พิกัด'
        ];
        echo $form->field($model1, 'comp_type')->inline()->radioList($items)->label('ประเภทเป้าหมาย');        
?>
                            <div id="ezform-target">
                            <?= $form->field($model1, 'comp_id_target')->widget(Select2::classname(), [
                                'data' => $ezformComponents,
                                'options' => ['placeholder' => 'กรุณาเลือกคอมโพเนนท์...'],
                                'pluginOptions' => [
                                    'allowClear' => true,
                                    'multiple' => false,
                                ],
                            ]);
                            ?>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <?= $form->field($model1, 'category_id')->widget(TreeViewInput::classname(), [
                                'name' => 'category_id',
                                'id' => 'category_id',
                                'query' => TblTree::find()->where('readonly=1 or userid='.Yii::$app->user->id.' or id IN (select distinct root from tbl_tree where userid='.Yii::$app->user->id.')')->addOrderBy('root, lft'),
                                'headingOptions'=>['label'=>'Categories'],
                                'asDropdown' => true,
                                'multiple' => false,
                                'fontAwesome' => true,
                                'rootOptions' => [
                                    'label'=>'<i class="fa fa-home"></i> '.$siteconfig->sitename,
                                    'class'=>'text-success',
                                    'options' => ['disabled' => false]
                                ],
                            ])
                            ?>
                        </div>
                    </div>

                    <div class="row">
                        <div class='col-lg-6'>
                            <?=
                            $form->field($model1, 'ezf_detail')->textarea(['rows' => 7,
                                //    'onblur' => "autoSave($(this).val(),'ezf_detail','" . $model1->ezf_id . "')"
                            ]);
                            ?>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <?php
                                $items = [
                                    '0' => 'ส่วนตัว (Private)',
                                    '2' => 'ระบุคน (Assign)',
                                    '3' => 'ทุกคนใน Site นั้น',
                                    '1' => 'สาธารณะ (Public)',
                                ];
                                echo $form->field($model1, 'shared')->radioList($items);
                                ?>
                                <div id="ezform-shared_assign" style="display: <?php echo $model1->shared==2 ? ';' : 'none;'; ?>">
                                    <?= $form->field($model1, 'assign')->widget(Select2::className(),[
                                        //'initValueText' => $model1->assign, // set the initial display text
                                        'data' => $userlist,
                                        'options' => ['placeholder' => 'กรุณาเลือกผู้ใช้ฟอร์มนี้...', 'multiple' => true],
                                        'pluginOptions' => [
                                            'allowClear' => true,
                                            'tags' => true,
                                            'tokenSeparators' => [',', ' '],
                                        ]
                                    ]) ?>
                                </div>
                            </div>
                            <label>สิทธิในการใช้ฟอร์ม</label><br>
                            <?php
                            $items = [
                                '1' => 'อนุญาตให้ view List Data table ของฟอร์ม',
                                '2' => 'อนุญาตให้ Edit/Update List Data table ของฟอร์ม',
                                '3' => 'อนุญาตให้ Remove List Data table ของฟอร์ม',
                            ];
                            echo \yii\bootstrap\BaseHtml::checkbox('public_listview', ($model1->public_listview == 1 ? true : false), ['label' =>$items['1'], 'id'=>'public_listview', 'value' => '1']).'<br>';
                            echo \yii\bootstrap\BaseHtml::checkbox('public_edit', ($model1->public_edit == 1 ? true : false), ['label' =>$items['2'], 'id'=>'public_edit', 'value' => '1']).'<br>';
                            echo \yii\bootstrap\BaseHtml::checkbox('public_delete', ($model1->public_delete == 1 ? true : false), ['label' =>$items['2'], 'id'=>'public_delete', 'value' => '1']).'<br>';
                            ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <?php

                            $arrCoDev = [];

                            foreach ($co_devs as $key => $value) {
                                array_push($arrCoDev, $value->user_co);
                            }

                            echo '<label>ผู้สร้างร่วม</label>';
                            echo Select2::widget([
                                'name' => 'ezform-co_dev',
                                'value' => $arrCoDev, // initial value
                                'data' => $userlist,
                                'options' => ['placeholder' => 'Select a color ...', 'multiple' => true, 'class' => 'form-control ezform-co_dev',],
                                'pluginOptions' => [
                                    'tags' => true,
                                    'tokenSeparators' => [',', ' '],
                                ],
                            ]);
                            ?>

                        </div>

                        <div class="col-md-6">
                            <?php
                            echo $form->field($model1, 'field_detail')->widget(Select2::classname(), [
                                'data' => \yii\helpers\ArrayHelper::map($model3,'ezf_field_name','ezf_field_label'),
                                'options' => ['placeholder' => 'กรุณาเลือกตัวแปร...'],
                                'pluginOptions' => [
                                    'allowClear' => true,
                                    'multiple' => true,
                                ],
                            ]);
                            ?>
                        </div>
                    </div>

                </div>
                <div role="tabpanel" class="tab-pane" id="setting2" style="padding-top: 10px;">
                    <div class="row">
                        <div class="col-md-4">
                            <?php $model1->query_tools =='' ? $model1->query_tools=1 : null; echo $form->field($model1, 'query_tools')->radioList(['1' => 'Disable', '2'=>'Enable for check error and no error only to be submitted', '3'=> 'Enable for submission always possible'], ['id'=>'ezform-query_tools']); ?>
                        </div>
                        <div class="col-md-4">
                            <?php $model1->unique_record =='' ? $model1->unique_record=1 : null; echo $form->field($model1, 'unique_record')->radioList(['1' => 'Disable', '2'=>'Enable(เพิ่มได้เพียง 1 Record)','3'=>'Enable(Submit ได้เพียง 1 Record)'], ['id'=>'ezform-unique_record'])->label('Unique Record (patient mode)'); ?>
                        </div>
                        <div class="col-md-4">
                            <?php $model1->reply_tools =='' ? $model1->reply_tools=1 : null; echo $form->field($model1, 'reply_tools')->radioList(['1' => 'Disable', '2'=>'Enable'],
                                [
                                    'id'=>'ezform-reply_tools',
                                    'onchange'=>'$(\'#reply_tools_setting\').toggle();'
                                ])->label('Consult Tools');?>
                            <div id="reply_tools_setting" style="display: <?php echo $model1->reply_tools==2 ? ';' : 'none;'; ?>">
                                <?=$form->field($model1, 'telegram')->textInput()->label('Telegram group');?>
                                <?php
                                $model1->consultant_users = explode(',', $model1->consultant_users);
                                echo $form->field($model1, 'consultant_users')->widget(Select2::className(),[
                                    'id' => 'select-consultant-users',
                                    'data' => $userlist,
                                    'options' => ['placeholder' => 'เลือกผู้ดูแลฟอร์มนี้...', 'multiple' => true],
                                    'pluginOptions' => [
                                        'allowClear' => true,
                                        'tags' => true,
                                        'tokenSeparators' => [',', ' '],
                                    ]
                                ]); ?>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <!--
                                /* ## Button Fix CASCAP Easy Form ## */
                                /* ################################# */
                                /* ############# Start ############# */
                                /* ################################# */
                            -->
                            <button type="button" class='btn btn-danger btn-flat' id='fixForm'><i class="fa fa-cog"></i> ซ่อมแซมฟอร์ม</button>
                            <?php
                            $this->registerJs("
                        $(\"#fixForm\").on('click', function() {
                            $.blockUI({
                            message : 'กำลังซ่อมแซม EzForm ...',
                            css: {
                                border: 'none',
                                padding: '15px',
                                backgroundColor: '#000',
                                '-webkit-border-radius': '10px',
                                '-moz-border-radius': '10px',
                                opacity: .5,
                                color: '#fff'
                            } });

                            var id = $('#ezform-ezf_id').val();
                            $.post('".Url::to(['ezform/fix-form'])."',{id:id},function(result){
                                " . SDNoty::show('result.message', 'result.status') . "
                                $.unblockUI();
                            });

                            return false;
                        });
                    ");
                            ?>
                            <!--
                                /* ################################# */
                                /* ############## End ############## */
                                /* ################################# */
                            -->
                        </div>
                    </div>

                </div>
                <div role="tabpanel" class="tab-pane" id="setting3" style="padding-top: 10px;">
                    <?php if(Yii::$app->user->can('administrator')) { ?>
                        <div class="row">
                            <div class="col-md-6 ">
                                <?= $form->field($model1, 'sql_condition')->widget(
        'appxq\sdii\widgets\AceEditor',
        [
            'mode'=>'mysql', // programing language mode. Default "html"
            'id'=>'sql_condition'
        ]
    ); ?>
                            </div>
                            <div class="col-md-6 sdbox-col">
                                <?= $form->field($model1, 'js')->widget(
        'appxq\sdii\widgets\AceEditor',
        [
            'mode'=>'javascript', // programing language mode. Default "html"
            'id'=>'js_editor'
        ]
    ) ?>
                            </div>
                            <div class="col-md-6 sdbox-col">
                                <?= $form->field($model1, 'sql_announce')->widget(
        'appxq\sdii\widgets\AceEditor',
        [
            'mode'=>'mysql', // programing language mode. Default "html"
            'id'=>'sql_announce'
        ]
    ) ?>
                            </div>
                        </div>
                    <?php }else{ ?>
                        <?php
 echo $form->field($model1, 'sql_condition')->hiddenInput()->label(false);
 echo $form->field($model1, 'sql_announce')->hiddenInput()->label(false);
 
                        ?>
                        <div class="row">
                            <div class="col-md-6 ">
                                &nbsp;
                            </div>
                            <div class="col-md-6 sdbox-col">
                                <?= $form->field($model1, 'js')->widget(
        'appxq\sdii\widgets\AceEditor',
        [
            'mode'=>'javascript', // programing language mode. Default "html"
            'id'=>'js_editor'
        ]
    ) ?>
                            </div>
                        </div>
                    <?php } ?>
                </div>

                <div role="tabpanel" class="tab-pane" id="setting4" style="padding-top: 10px;">
                    <?php
                    echo Html::label('ขั้นตอนที่ 1 เลือก Tables จาก TDC');
                    echo Select2::widget([
                        'id' => 'tcc-bot-mapping',
                        'name' => 'tcc-bot-mapping',
                        'data' => $tccTables,
                        'options' => [
                            'placeholder' => 'Please select tables from TDC',
                        ],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]);

                    echo '<br>';
                    echo Html::label('ขั้นตอนที่ 2 Mapping Fields ระหว่าง TDC กับ EzForm');
                    echo '<div id="div-tcc-bot-mapping"></div>';
                    ?>
                </div>
            </div>


	    </div>
    </div>            

<?php ActiveForm::end(); ?>
<div class="box box-primary">
<!--<div class="box-header with-border">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<h3><i class="fa fa-file-text-o"></i>  ฟอร์ม --><?php //echo $model1->ezf_name;?><!--</h3></div>-->
    <div class="box-body">

    <div class="row dad" id="formPanel">

<?php
                    //trntv\aceeditor\AceEditor::className();
                    
foreach ($model3 as $value) {
    echo \backend\modules\ezforms\components\EzformFunc::getTypeEform($model_gen,$value);
}
?>
    </div>
            <div id='showAddPanel'>

    </div>
    <br>
    <button class='btn btn-default btn-flat' id='addQuestion'>เพิ่มรายการ</button>
</div>
    </div>
</div>

</div>
<?=
SDModalForm::widget([
    'id' => 'modal-ezform',
    'size' => 'modal-lg',
]);
?>

<?php
$jsAddon = '';
if(Yii::$app->user->can('administrator')) {
    $jsAddon = "sql_announce.on('blur', function() {
    var value = sql_announce.getValue();
    
    var attribute = $('#ezform-sql_announce').attr('id');
    var field_id = $('#ezform-ezf_id').val();
    $.post('/ezforms/ezform/formautosave', {
            value: value,
            field: attribute,
            id: field_id
        }, function (result) {
            var noty_id = noty({'text': result.message, 'type': 'success'});
         });
});

sql_condition.on('blur', function() {
    var value = sql_condition.getValue();
    
    var attribute = $('#ezform-sql_condition').attr('id');
    var field_id = $('#ezform-ezf_id').val();
    $.post('/ezforms/ezform/formautosave', {
            value: value,
            field: attribute,
            id: field_id
        }, function (result) {
            var noty_id = noty({'text': result.message, 'type': 'success'});
         });
});

";
}

$this->registerJs("

$jsAddon

js_editor.on('blur', function() {
    var value = js_editor.getValue();
    
    var attribute = $('#ezform-js').attr('id');
    var field_id = $('#ezform-ezf_id').val();
    $.post('/ezforms/ezform/formautosave', {
            value: value,
            field: attribute,
            id: field_id
        }, function (result) {
            var noty_id = noty({'text': result.message, 'type': 'success'});
         });
});



function modalQueryRequest(url) {
    $('#modal-query-request .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
    $('#modal-query-request').modal('show')
    .find('.modal-content')
    .load(url);
}

$('#btnEzformTarget').click(function(){
    var url = '" . Url::to(['annotated', 'ezf_id' => $ezfcomp]) . "'
    modalQueryRequest(url);
});

$(\"#ezform-query_tools\").on('change', function() {
    var status = $('input[name=\"Ezform[query_tools]\"]:checked').val();
    var id = $('#ezform-ezf_id').val();
    $.post('".Url::to(['ezform/use-ezform-option'])."',{status:status, id:id, action: 'query_tools'},function(result){
        " . SDNoty::show('result.message', 'result.status') . "
    });
});
$(\"#ezform-unique_record\").on('change', function() {
    var status = $('input[name=\"Ezform[unique_record]\"]:checked').val();
    var id = $('#ezform-ezf_id').val();
    $.post('".Url::to(['ezform/use-ezform-option'])."',{status:status, id:id,  action: 'unique_record'},function(result){
        " . SDNoty::show('result.message', 'result.status') . "
    });
});
$(\"#ezform-reply_tools\").on('change', function() {
    var status = $('input[name=\"Ezform[reply_tools]\"]:checked').val();
    var id = $('#ezform-ezf_id').val();
    $.post('".Url::to(['ezform/use-ezform-option'])."',{status:status, id:id,  action: 'reply_tools'},function(result){
        " . SDNoty::show('result.message', 'result.status') . "
    });
});
");

if ($_SESSION['showsetting'] != "1") {
    $hidesetting = "$('#settingGeneral').hide();";
}else{
    $hidesetting = "$('#settingGeneral').show();";
    $_SESSION['showsetting']=0;
}

$this->registerJs("
if ($('#ezform-comp_type :radio:checked').val()!='2') {
    $('#ezform-target').hide();
}
$(\"input[name='Ezform[comp_type]']\").on('change',function(){
    var comp_type = $(\"input[name='Ezform[comp_type]']:checked\").val();
    if (comp_type=='2') {
        $('#ezform-target').show();
    }else{
        $('#ezform-target').hide();
    }

    var id = $('#ezform-ezf_id').val();
    $.post('".Url::to(['ezform/updateformcomptype'])."',{comp_type:comp_type,id:id},function(result){
        " . SDNoty::show('result.message', 'result.status') . "
    });
    if (comp_type != '2') setTimeout(function () { window.location.href='update?id='+id}, 500);
});
$('#EzformTarget').hide();
        $('#btnEzformTarget').click(function(){

            $('#EzformTarget').toggle();
        });
        
        $hidesetting
            
        $('#btnSettingGeneral').click(function(){

            $('#settingGeneral').toggle();
        });
$(\"input[name='Ezform[shared]']\").on('change',function(){
    var shared = $(\"input[name='Ezform[shared]']:checked\").val();
    if(shared==2){
        $('#ezform-shared_assign').toggle();
    }else $('#ezform-shared_assign').hide();
    var id = $('#ezform-ezf_id').val();
    $.post('".Url::to(['ezform/updateformstatus'])."',{shared:shared,id:id},function(result){
        " . SDNoty::show('result.message', 'result.status') . "
    });
});
$('#ezform-ezf_name').on('blur',function(){
    var value = $(this).val();
    var attribute = $('#ezform-ezf_name').attr('id');
    var field_id = $('#ezform-ezf_id').val();
    $.post('/ezforms/ezform/formautosave', {
            value: value,
            field: attribute,
            id: field_id
        }, function (result) {
            console.log(result);
            var noty_id = noty({'text': result.message, 'type': 'success'});
         });
});

$('#ezform-sql_announce').on('blur',function(){
    var value = $(this).val();
    var attribute = $('#ezform-sql_announce').attr('id');
    var field_id = $('#ezform-ezf_id').val();
    $.post('/ezforms/ezform/formautosave', {
            value: value,
            field: attribute,
            id: field_id
        }, function (result) {
            console.log(result);
            var noty_id = noty({'text': result.message, 'type': 'success'});
         });
});

$('#ezform-sql_condition').on('blur',function(){
    var value = $(this).val();
    var attribute = $('#ezform-sql_condition').attr('id');
    var field_id = $('#ezform-ezf_id').val();
    $.post('/ezforms/ezform/formautosave', {
            value: value,
            field: attribute,
            id: field_id
        }, function (result) {
            console.log(result);
            var noty_id = noty({'text': result.message, 'type': 'success'});
         });
});

$('#ezform-js').on('blur',function(){
    var value = $(this).val();
    var attribute = $('#ezform-js').attr('id');
    var field_id = $('#ezform-ezf_id').val();
    $.post('/ezforms/ezform/formautosave', {
            value: value,
            field: attribute,
            id: field_id
        }, function (result) {
            console.log(result);
            var noty_id = noty({'text': result.message, 'type': 'success'});
         });
});

$('#ezform-ezf_detail').on('blur',function(){
    var value = $(this).val();
    var attribute = $('#ezform-ezf_detail').attr('id');
    var field_id = $('#ezform-ezf_id').val();
    $.post('/ezforms/ezform/formautosave', {
            value: value,
            field: attribute,
            id: field_id
        }, function (result) {
            console.log(result);
            var noty_id = noty({'text': result.message, 'type': 'success'});
         });
});
$('#public_listview').on('click',function(){
    var public_listview;
    var id = $('#ezform-ezf_id').val();
    if($('#public_listview').is(':checked')){
        public_listview = 1;
    }else{
        public_listview = 0;
    }
    $.post('".Url::to(['ezform/updatepublic1'])."',{public_listview:public_listview,id:id},function(result){
     " . SDNoty::show('result.message', 'result.status') . "
//                  $('div[item-id='+result.data+']').remove();
 });
});
$('#public_edit').on('click',function(){
    var public_edit;
    var id = $('#ezform-ezf_id').val();
    if($('#public_edit').is(':checked')){
        public_edit = 1;
    }else{
        public_edit = 0;
    }
    $.post('".Url::to(['ezform/updatepublic2'])."',{public_edit:public_edit,id:id},function(result){
     " . SDNoty::show('result.message', 'result.status') . "
//                  $('div[item-id='+result.data+']').remove();
 });
});
$('#public_delete').on('click',function(){
    var public_delete;
    var id = $('#ezform-ezf_id').val();
    if($('#public_delete').is(':checked')){
        public_delete = 1;
    }else{
        public_delete = 0;
    }
    $.post('".Url::to(['ezform/updatepublic3'])."',{public_delete:public_delete,id:id},function(result){
        " . SDNoty::show('result.message', 'result.status') . "
    });
});
$('.dad').dad({
    draggable:'.draggable',
    callback:function(e){
      var positionArray = [];
      $('.dad').find('.dads-children').each(function(){
        positionArray.push($(this).attr('item-id'));
    });

    dadModal(positionArray);
}
});
$('#addQuestion').click(function(){
    var ezf_id = $('#ezform-ezf_id').val();
    $('#showAddPanel').load('".Url::to(['ezform/formadd','id'=>''])."'+ezf_id);
});

$('#formPanel').on('click','.btn-edit',function(e){
    var fieldid = $(this).attr('data-id');
    var fieldtype = $(this).attr('type-item');
    $('#showPanel').remove();
    if(fieldtype==1){
        modalEzform('" . Url::to(['text/formupdate', 'id' => '']) . "'+fieldid);
    }
    else if(fieldtype==2){
        modalEzform('" . Url::to(['heading/formupdate', 'id' => '']) . "'+fieldid);
    }else if(fieldtype==3){
        modalEzform('" . Url::to(['textarea/formupdate', 'id' => '']) . "'+fieldid);
    }else if(fieldtype==4){
        modalEzform('" . Url::to(['radio/formupdate', 'id' => '']) . "'+fieldid);
    }else if(fieldtype==19){
        modalEzform('" . Url::to(['checkbox/formupdate', 'id' => '']) . "'+fieldid);
    }else if(fieldtype==6){
        modalEzform('" . Url::to(['select/formupdate', 'id' => '']) . "'+fieldid);
    }else if(fieldtype==7){
        modalEzform('" . Url::to(['date/formupdate', 'id' => '']) . "'+fieldid);
    }else if(fieldtype==8){
        modalEzform('" . Url::to(['time/formupdate', 'id' => '']) . "'+fieldid);
    }else if(fieldtype==9){
        modalEzform('" . Url::to(['datetime/formupdate', 'id' => '']) . "'+fieldid);
    } else if(fieldtype == 10) {
        modalEzform('" . Url::to(['component/formupdate', 'id' => '']) . "'+fieldid);
    }
    else if(fieldtype == 12) {
        modalEzform('" . Url::to(['personal/formupdate', 'id' => '']) . "'+fieldid);
    }
    else if(fieldtype == 11) {
        modalEzform('" . Url::to(['snomed/formupdate', 'id' => '']) . "'+fieldid);
    }
    else if(fieldtype == 14) {
        modalEzform('" . Url::to(['fileinput/formupdate', 'id' => '']) . "'+fieldid);
    }
    else if(fieldtype == 13) {
        modalEzform('" . Url::to(['province/formupdate', 'id' => '']) . "'+fieldid);
    }
    else if(fieldtype == 15) {
        modalEzform('" . Url::to(['question/formupdate', 'id' => '']) . "'+fieldid);
    }
    else if(fieldtype == 16) {
        modalEzform('" . Url::to(['scheckbox/formupdate', 'id' => '']) . "'+fieldid);
    }
    else if(fieldtype == 17 || fieldtype == 32) {
        modalEzform('" . Url::to(['hospital/formupdate', 'id' => '']) . "'+fieldid);
    }else if(fieldtype == 23){
        modalEzform('" . Url::to(['scale/formupdate', 'id' => '']) . "'+fieldid);
    }else if(fieldtype == 24){
        modalEzform('" . Url::to(['drawing/formupdate', 'id' => '']) . "'+fieldid);
    }else if (fieldtype == 25){
        modalEzform('" . Url::to(['grid/formupdate', 'id' => '']) . "'+fieldid);
    }else if (fieldtype == 26){
        modalEzform('" . Url::to(['map/formupdate', 'id' => '']) . "'+fieldid);
    }
    else if(fieldtype == 27) {
        modalEzform('" . Url::to(['icd9/formupdate', 'id' => '']) . "'+fieldid);
    }
    else if(fieldtype == 28) {
        modalEzform('" . Url::to(['icd10/formupdate', 'id' => '']) . "'+fieldid);
    } 
    else if(fieldtype == 30) {
        modalEzform('" . Url::to(['audio/formupdate', 'id' => '']) . "'+fieldid);
    }
});
$('#formPanel').on('click','.btn-delete',function(){

    var fieldid = $(this).attr('data-id');
    var fieldtype = $(this).attr('type-item');
    if (confirm('ยืนยันการลบคำถามนี้ ?') == true) {
        $.blockUI({
            message : 'กำลังลบคำถาม ...',
            css: {
               border: 'none',
               padding: '15px',
               backgroundColor: '#000',
               '-webkit-border-radius': '10px',
               '-moz-border-radius': '10px',
               opacity: .5,
               color: '#fff'
               }
        });
        if(fieldtype==1){
            delTextField(fieldid);
        }else if(fieldtype==2){
            delHeadField(fieldid);
        }else if(fieldtype==3){
            delTextAreaField(fieldid);
        }else if(fieldtype==4){
            delRadioField(fieldid);
        }else if(fieldtype==19){
            delCheckboxField(fieldid);
        }else if(fieldtype==6){
            delSelectField(fieldid);
        }else if(fieldtype==7){
            delDateField(fieldid);
        }else if(fieldtype==8){
            delTimeField(fieldid);
        }else if(fieldtype==9){
            delDateTimeField(fieldid);
        } else if(fieldtype == 10) {
            delComponentField(fieldid);
        }else if(fieldtype == 11) {
            delSnomedField(fieldid);
        }
        else if(fieldtype == 12) {
            delPersonalField(fieldid);
        }
        else if(fieldtype == 14) {
            delPersonalField(fieldid);
        }
        else if(fieldtype == 13) {
            delProvinceField(fieldid);
        }
        else if(fieldtype == 15) {
            delQuestionField(fieldid);
        }
        else if(fieldtype == 16) {
            delScheckboxField(fieldid);
        }
        else if(fieldtype == 17 || fieldtype == 32) {
            delHospitalField(fieldid);
        }else if(fieldtype == 23){
            delScaleField(fieldid);
        }else if(fieldtype == 24){
            delDrawingField(fieldid);
        }
        else if(fieldtype == 25){
            delGridField(fieldid);
        }
	else if(fieldtype == 26){
            delMapField(fieldid);
        }
	else if(fieldtype == 30){
            delAudioField(fieldid);
        }
        else if(fieldtype == 27){

                  delIcd9Field(fieldid);
              }
              else if(fieldtype == 28){

                        delIcd10Field(fieldid);
                    }
    } else {
        x = 'You pressed Cancel!';
    }

});
function modalEzform(url) {
    $('#modal-ezform .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
    $('#modal-ezform').modal('show')
    .find('.modal-content')
    .load(url);
}

function dadModal(positionArray){
    console.log(positionArray);
    $.post('".Url::to(['ezform/formupdateorder'])."',{positionArray:positionArray},function(result){

    });
}


");

//TDC mapping
$this->registerJs("$('#tcc-bot-mapping').on('change',function(){

    var val = $(this).val();
    var ezf_id = $('#ezform-ezf_id').val();

    $.post('".Url::to(['ezform/tcc-bot-mapping',])."',{val:val, ezf_id:ezf_id},function(result){
        $('#div-tcc-bot-mapping').html(result);
    });
});");

//consultant
$this->registerJs("$('#ezform-consultant_users').on('change',function(){

    var val = $(this).val();
    var ezf_id = $('#ezform-ezf_id').val();
    $.post('".Url::to(['ezform/save-consultant', 'type'=>'consultant'])."',{val:val, ezf_id:ezf_id},function(result){
        " . SDNoty::show('result.message', 'result.status') . "
    });
});");

//telegram code
$this->registerJs("$('#ezform-telegram').on('change',function(){

    var val = $(this).val();
    var ezf_id = $('#ezform-ezf_id').val();
    $.post('".Url::to(['ezform/save-consultant', 'type'=>'telegram'])."',{val:val, ezf_id:ezf_id},function(result){
        " . SDNoty::show('result.message', 'result.status') . "
    });
});");

/** ใช้ this.value แทน var val = $(this).val(); by Codeerror ^^ */
$this->registerJs("$('#ezform-assign').on('change',function(){

    var val = $(this).val();
    var ezf_id = $('#ezform-ezf_id').val();
    $.post('".Url::to(['ezform/saveassign'])."',{val:val, ezf_id:ezf_id},function(result){
        " . SDNoty::show('result.message', 'result.status') . "
    });
});");

$this->registerJs("$('#ezform-comp_id_target').on('change',function(){
    var ezf_id = $('#ezform-ezf_id').val();
    $.post('".Url::to(['ezform/update-component-target'])."',
        {
            form_id: ezf_id,
            comp_id: this.value
        },
        function(result){
            " . SDNoty::show('result.message', 'result.status') . "
        });
    setTimeout(function () { window.location.href='update?id='+ezf_id}, 500);
});");

$this->registerJs("
        $( '#category_id' ).change(function() {
        var ezf_id = $('#ezform-ezf_id').val();
          $.post('".Url::to(['ezform/update-category'])."',
        {
            form_id: ezf_id,
            category_id: this.value
        },
        function(result){
            " . SDNoty::show('result.message', 'result.status') . "
        });
        });
    ");

$this->registerJs("$('#ezform-field_detail').on('change',function(){
    var val = $(this).val();
    var ezf_id = $('#ezform-ezf_id').val();
    $.post('".Url::to(['ezform/savefielddetail'])."',{val:val, ezf_id:ezf_id},function(result){
        " . SDNoty::show('result.message', 'result.status') . "
    });
});");

$this->registerJs("$('.ezform-co_dev').on('change',function(){

    var field_id = '';

    $.each( $( '.ezform-co_dev' ).select2('data'), function( key, value ) {
      field_id += value.id + ',';
    });

    var val = $(this).val();
    var ezf_id = $('#ezform-ezf_id').val();
    $.post('".Url::to(['ezform/save-co-dev'])."',{val:field_id.substring(0, (field_id.length - 1)), ezf_id:ezf_id},function(result){
        " . SDNoty::show('result.message', 'result.status') . "
    });
});


");


?>

<?php
    $this->registerJs("var baseUrlTo = '".Url::to(['/ezforms'])."';", 1);
?>
<?php $this->registerJsFile("/js/scriptfield/jstext.js"); ?>
<?php $this->registerJsFile("/js/scriptfield/jscheckbox.js"); ?>
<?php $this->registerJsFile("/js/scriptfield/jsradio.js"); ?>
<?php $this->registerJsFile("/js/scriptfield/jsheading.js"); ?>
<?php $this->registerJsFile("/js/scriptfield/jsselect.js"); ?>
<?php $this->registerJsFile("/js/scriptfield/jstextarea.js"); ?>
<?php $this->registerJsFile("/js/scriptfield/jsdate.js"); ?>
<?php $this->registerJsFile("/js/scriptfield/jstime.js"); ?>
<?php $this->registerJsFile("/js/scriptfield/jsdatetime.js"); ?>
<?php $this->registerJsFile("/js/scriptfield/jscomponent.js"); ?>
<?php $this->registerJsFile("/js/scriptfield/jspersonal.js"); ?>
<?php $this->registerJsFile("/js/scriptfield/jssnomed.js"); ?>
<?php $this->registerJsFile("/js/scriptfield/jsfileinput.js"); ?>
<?php $this->registerJsFile("/js/scriptfield/jsprovince.js"); ?>
<?php $this->registerJsFile("/js/scriptfield/jscheckbox2.js"); ?>
<?php $this->registerJsFile("/js/scriptfield/jsquestion.js"); ?>
<?php $this->registerJsFile("/js/scriptfield/jshospital.js"); ?>
<?php $this->registerJsFile("/js/scriptfield/jsreffield.js"); ?>
<?php $this->registerJsFile("/js/scriptfield/jsref43field.js"); ?>
<?php $this->registerJsFile("/js/scriptfield/jsscale.js"); ?>
<?php $this->registerJsFile("/js/scriptfield/jsdrawing.js"); ?>
<?php $this->registerJsFile("/js/scriptfield/jsaudio.js"); ?>
<?php $this->registerJsFile("/js/scriptfield/jsgrid.js"); ?>
<?php $this->registerJsFile("/js/scriptfield/jsmap.js"); ?>
<?php $this->registerJsFile("/js/scriptfield/jsicd9.js"); ?>
<?php $this->registerJsFile("/js/scriptfield/jsicd10.js"); ?>
<?php $this->registerJsFile("/js/caret.js")?>
<?= SDModalForm::widget([
    'id' => 'modal-condition',
    'size'=>'modal-sm',
]);
?>
<?= SDModalForm::widget([
    'id' => 'modal-gridtype',
    'size'=>'modal-md',
]);
?>

<?php
 

appxq\sdii\widgets\ModalForm::widget([
    'id' => 'modal-ezform',
    'size' => 'modal-lg',
])
?>
<?php
$this->registerJs("
	var eid = '".$_GET['id']."';
	var baseUrl = '".Url::to(['ezform-condition'])."';
	var conditionUrl = '".Url::to(['ezform-condition/condition'])."';
	var fieldsUrl = '".Url::to(['ezform-condition/fields', 'id'=>$_GET['id']])."';
", 3);

backend\assets\EzfAsset::register($this);
?>
