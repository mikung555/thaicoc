<?php
use yii\helpers\Html;
use yii\helpers\Url;
use appxq\sdii\widgets\GridView;
?>

<?= GridView::widget([
    'id' => 'buffe-data-table-field-grid',
    'dataProvider' => $dataprovider,
//    'panelBtn' => Html::a('<span class="fa fa-arrow-circle-o-left"></span> กลับไปหน้าจัดการตาราง', Url::to(['buffe-data-table/']), [
//        'title' => Yii::t('app', 'Go back'),
//        'class'=>'btn btn-success btn-sm',
//    ]),
    'columns' => [
        [
            'class' => 'yii\grid\SerialColumn',
            'headerOptions' => ['style'=>'text-align: center;'],
            'contentOptions' => ['style'=>'width:60px;text-align: center;'],
        ],
        [
            'attribute'=>'fname',
            'label' => 'ชื่อฟิลด์',
        ],
        [
            'attribute'=>'ftype',
            'label' => 'ชนิด',
        ],
        [
            'attribute'=>'pk',
            'label'=>'PK',
            'format'=>'html',
            'headerOptions' => ['style'=>'text-align: center;'],
            'contentOptions' => ['style'=>'width:50px;text-align: center;'],
            'value'=> function($model) {
                if ($model['pk']=="PRI") return '<i class="fa fa-check" aria-hidden="true"></i>';
                return "";
            }
        ],
        [
            'attribute'=>'isnull',
            'label' => 'Null',
            'format'=>'html',
            'headerOptions' => ['style'=>'text-align: center;'],
            'contentOptions' => ['style'=>'width:50px;text-align: center;'],
            'value'=> function($model) {
                if ($model['isnull']=="YES") return '<i class="fa fa-check" aria-hidden="true"></i>';
                return "";
            }
        ],
        [
            'attribute'=>'caption',
            'label' => 'ชื่อฟิลด์',
            'value'=> function ($model) {
                $tbname=$model['tbname'];
                $fname=$model['fname'];
                $sql = "select caption from `buffe_webservice`.`buffe_data_table_field` WHERE tbname=:tbname AND fname=:fname;";
                $fdata = Yii::$app->dbbot->createCommand($sql, [':tbname'=>$tbname, ':fname'=>$fname])->queryOne();
                if ($fdata['caption']) return $fdata['caption'];
                return '';
            }
        ],
        [
            'attribute'=>'description',
            'label' => 'รายละเอียด',
            'format'=>'html',
            'value'=> function ($model) {
                $tbname=$model['tbname'];
                $fname=$model['fname'];
                $sql = "select description from `buffe_webservice`.`buffe_data_table_field` WHERE tbname=:tbname AND fname=:fname;";
                $fdata = Yii::$app->dbbot->createCommand($sql, [':tbname'=>$tbname, ':fname'=>$fname])->queryOne();
                if ($fdata['description']) return nl2br($fdata['description']);
                return '';
            }
        ],
        [
            'attribute' => 'Action',
            'label' => 'Mapping',
            'format'=>'raw',
            'value'=> function ($model, $key, $index, $widget) use ($listField, $ezf_id) {
                $sql = "select field_ezf from `ezform_maping_tcc` where ezf_id=:ezf_id and field_tcc=:field_tcc and tcc_tb=:tcc_tb;";
                $fdata = Yii::$app->db->createCommand($sql, [':ezf_id'=>$ezf_id, ':field_tcc'=>$model['fname'], 'tcc_tb' => $model['tbname'],])->queryOne();

                $html = Html::hiddenInput('tcc-bot-fame-'.$index, $model['fname'], ['id'=>'tcc-bot-fame-'.$index]);
                $html .= \kartik\widgets\Select2::widget([
                    'id' => 'tcc-bot-fname-mapping-'.$index,
                    'name' => 'tcc-bot-fname-mapping-'.$index,
                    'value' => $fdata['field_ezf'], // initial value
                    'data' => $listField,
                    'options' => [
                        'placeholder' => 'Please select variable from EzForm',
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                    'pluginEvents' =>[
                        "select2:select" => "function() { var index = {$index}; mappingFields(index, $(this).val(), 'mapping'); }",
                        "select2:unselect" => "function() { var index = {$index}; mappingFields(index, $(this).val(), 'un-mapping'); }"
                    ],
                ]);
                return $html;
            },
            'headerOptions' => ['style'=>'text-align: center;'],
            'contentOptions' => ['style'=>'width:400px;text-align: center;'],
        ],
        [
            'attribute' => 'Decode',
            'label' => 'Decode',
            'format'=>'raw',
            'value'=> function ($model, $key, $index, $widget) use ($ezf_id){
                $sql = "select id, decode from `ezform_maping_tcc` where ezf_id=:ezf_id and field_tcc=:field_tcc and tcc_tb=:tcc_tb;";
                $fdata = Yii::$app->db->createCommand($sql, [':ezf_id'=>$ezf_id, ':field_tcc'=>$model['fname'], 'tcc_tb' => $model['tbname'],])->queryOne();
                $html = Html::checkbox('tcc-bot-decode-'.$index, $fdata['decode'] ? true : false, [
                    'id'=>'tcc-bot-decode-'.$index, 'style'=> $fdata['id'] ? ';' : 'display:none;',
                    'onclick' => "tccDecode($(this).is(':checked'), {$index});",
                ]);
                return $html;
            },
            'headerOptions' => ['style'=>'text-align: center;'],
            'contentOptions' => ['style'=>'width:100px;text-align: center;'],
        ],
    ],
]);

//tcc bot mapping fields
$this->registerJs("
function mappingFields(index, field_ezf, type){
    var field_tcc = $('#tcc-bot-fame-'+index).val();
    var ezf_id = $('#ezform-ezf_id').val();
    var tcc_tb = $('#tcc-bot-mapping').val();
    $.post('".Url::to(['ezform/tcc-bot-mapping-field',])."',{field_tcc:field_tcc, ezf_id:ezf_id, index:index, field_ezf:field_ezf, tcc_tb:tcc_tb, type:type},function(result){
        if(type == 'mapping'){
            $('#tcc-bot-decode-'+index).fadeIn();
        }else if(type == 'un-mapping'){
            $('#tcc-bot-decode-'+index).fadeOut();
        }
        " . \common\lib\sdii\components\helpers\SDNoty::show('result.message', 'result.status') . "
    });
}
function tccDecode(val, index){
    var field_tcc = $('#tcc-bot-fame-'+index).val();
    var ezf_id = $('#ezform-ezf_id').val();
    var tcc_tb = $('#tcc-bot-mapping').val();
    var decode =  val ? 1 : 0;
    $.post('".Url::to(['ezform/tcc-bot-field-decode',])."',{field_tcc:field_tcc, ezf_id:ezf_id, index:index, tcc_tb:tcc_tb, decode:decode},function(result){
        $('#tcc-bot-decode-'+index).fadeIn();
        " . \common\lib\sdii\components\helpers\SDNoty::show('result.message', 'result.status') . "
    });
}

");
?>