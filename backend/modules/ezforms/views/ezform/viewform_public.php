<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use common\lib\sdii\widgets\SDGridView;
use common\lib\sdii\widgets\SDModalForm;
use common\lib\sdii\components\helpers\SDNoty;
use common\lib\ezform\components\helpers\GenForm;
use kartik\widgets\Select2;
use kartik\widgets\InputWidget;
use yii\widgets\MaskedInput;
use backend\modules\ezforms\components\GenViewForm;
use yii\bootstrap\Modal;
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$this->title = Yii::t('backend', 'ฟอร์ม ', [
        'modelClass' => 'Ezform',
    ]) . ':: ' . $modelform->ezf_name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Ezforms'), 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $model1->ezf_name, 'url' => ['view', 'id' => $model1->ezf_id]];
$this->params['breadcrumbs'][] = Yii::t('backend', 'ดูฟอร์มออนไลน์');

$this->registerCssFile('/css/ezform.css');
//$this->registerCssFile('/checkbox/demo/build.css');
?>
<div class="ezform-update" >
    <br>
    <div style='float:right'>
        <?php
        $url = $_GET['action']=='inputdata' ? '/inputdata': '/ezforms/ezform/';
        echo Html::a('<i class="fa fa-mail-reply"></i>&nbsp;&nbsp;&nbsp;'.($_GET['action']=='inputdata' ? 'กลับไปหน้าบันทึกข้อมูล' : 'กลับไปหน้าเลือกฟอร์ม'), $url, ['class' => 'btn btn-warning btn-flat']); ?>
    </div>
    <br>
    <br>
    <div class="box">
        <div class="box-body">
            <div class='row'>
                <div class='col-lg-12'>
                    <h2><?= $modelform->ezf_name;?></h2>
                </div>
            </div>
            <div class='row'>
                <div class='col-lg-12'>
                    <h4><?= $modelform->ezf_detail;?></h4>
                </div>
            </div>


            <hr>
            <div class="row dad" id="formPanel">
                <?php $form = backend\modules\ezforms\components\EzActiveForm::begin(['action'=>'/ezforms/ezform/savedata', 'options' => ['enctype'=>'multipart/form-data']]); ?>
                <input type='hidden' name='ezf_id' id='ezf_id' value='<?php echo $modelform->ezf_id;?>'>
                <input type='hidden' name='id' id='id' value=''>
                <?php
                foreach($modelfield as $value){
                    $target='';
                    echo \backend\modules\ezforms\components\EzformFunc::getTypeEform($model_gen, $value, $form);
                }

                foreach($modelfield as $value){
                    $inputId = Html::getInputId($model_gen, $value['ezf_field_name']);
                    $inputValue = Html::getAttributeValue($model_gen, $value['ezf_field_name']);

                    $dataCond = backend\modules\ezforms\components\EzformQuery::getCondition($value['ezf_id'], $value['ezf_field_name']);
                    if($dataCond){
                        //Edit Html
                        $fieldId = Html::getInputId($model_gen, $value['ezf_field_name']);
                        if($value['ezf_field_type']==4){
                            $fieldId = $value['ezf_field_name'];
                        }
                        foreach ($dataCond as $index => $cvalue) {
                            $jumpArr = explode(',', $cvalue['cond_jump']);
                            foreach ($jumpArr as $j => $jvalue) {
                                $this->registerJs("
			    var fieldIdj = '".$jvalue."';
			    var inputIdj = '".$fieldId."';
			    var valueIdj = '".$inputValue."';
			    var fixValuej = '".$cvalue['ezf_field_value']."';
			    var fTypej = '".$value['ezf_field_type']."';
			    domHtml(fieldIdj, inputIdj, valueIdj, fixValuej, fTypej, 'block');
			");
                            }

                            $requireArr = explode(',', $cvalue['cond_require']);
                            foreach ($requireArr as $r => $rvalue) {
                                $this->registerJs("
			    var fieldIdr = '".$rvalue."';
			    var inputIdr = '".$fieldId."';
			    var valueIdr = '".$inputValue."';
			    var fixValuer = '".$cvalue['ezf_field_value']."';
			    var fTyper = '".$value['ezf_field_type']."';
			    domHtml(fieldIdr, inputIdr, valueIdr, fixValuer, fTyper, 'none');
			");
                            }
                        }
                        //Add Event
                        if($value['ezf_field_type']==20 || $value['ezf_field_type']==0 || $value['ezf_field_type']==16){
                            $this->registerJs("
		    var dataCond = '".yii\helpers\Json::encode($dataCond)."';
		    var inputId = '".$inputId."';
		    eventCheckBox(inputId, dataCond);
		");
                        }else if($value['ezf_field_type']==6){
                            $this->registerJs("
		    var dataCond = '".yii\helpers\Json::encode($dataCond)."';
		    var inputId = '".$inputId."';
		    eventSelect(inputId, dataCond);
		");
                        } else if($value['ezf_field_type']==4){
                            $this->registerJs("
		    var dataCond = '".yii\helpers\Json::encode($dataCond)."';
		    var inputName = '".$value['ezf_field_name']."';
		    eventRadio(inputName, dataCond);
		");
                        }

                    }
                }
                backend\assets\EzfGenAsset::register($this);
                ?>
                <div class='col-lg-12'>
                    <p style='color:green;font-size: 15px;'><?php echo $message;?></p>
                </div>
                <?php backend\modules\ezforms\components\EzActiveForm::end();?>
            </div
        </div>
    </div>

 
    
   
<?php
Modal::begin([
    'id' => 'modal-add',
    'closeButton' => ['label' => ''],
    'size' => 'modal-lg',
]);
?>
<div id='modalContent' >
    <div class="col-md-4">
        <div class="panel panel-primary">
            <div class="panel-heading">วันที่จ่ายยา</div>
            <div class="panel-body">

<?php
try {
                    if (isset($comp_id_target)) {

                        $special = Yii::$app->db->createCommand('SELECT `ezf_id`,`special` FROM `ezform_component` WHERE `comp_id` = :comp_id')->bindValues([':comp_id' => $comp_id_target])->queryOne();
                    }
                    if ($special) {

                        if ($datamodel_table->cid) {
                            $drug_cid = $datamodel_table->cid;
                        }

                        if ($datamodel_table->hsitecode) {
                            $drug_hsitecode = $datamodel_table->hsitecode;
                        }

                        if ($datamodel_table->hptcode) {
                            $drug_hpcode = $datamodel_table->hptcode;
                        }

                        if ($drug_cid != '') {
                            $sql = "SELECT DISTINCT DATE(`DATETIME_ADMIT`) AS DATETIME_ADMIT FROM f_drug_ipd WHERE HOSPCODE = :HOSPCODE AND (PID = :PID OR ptlink = :ptlink) AND TYPEDRUG = '2' ORDER BY `DATETIME_ADMIT` DESC";
                            $dataDrug = \backend\modules\ckdnet\classes\CkdnetFunc::queryAll($sql, [':HOSPCODE' => $drug_hsitecode, ':PID' => $drug_hpcode, ':ptlink' => base64_encode($drug_cid)]);
                        } else {
                            $sql = "SELECT DISTINCT DATE(`DATETIME_ADMIT`) AS DATETIME_ADMIT FROM f_drug_ipd WHERE HOSPCODE = :HOSPCODE AND PID = :PID AND TYPEDRUG = '2' ORDER BY `DATETIME_ADMIT` DESC";
                            $dataDrug = \backend\modules\ckdnet\classes\CkdnetFunc::queryAll($sql, [':HOSPCODE' => $drug_hsitecode, ':PID' => $drug_hpcode]);
                        }




                        foreach ($dataDrug as $value) {

                            echo Html::a($value['DATETIME_ADMIT'], 'javascript:void(0)', [
                                'class' => 'dataDrug list-group-item',
                                'style' => 'width:100%',
                                'data-url' => Url::to(['/ezforms/ezform/add-drug',
                                    'hospcode' => "'" . $drug_hsitecode . "'",
                                    'pid' => "'" . $drug_hpcode . "'",
                                    'ptlink' => "'" . md5($drug_cid) . "'",
                                    'date_admit' => "'" . $value['DATETIME_ADMIT'] . "'"
                                ])
                            ]);
                        }
                    }
                } catch (\yii\base\Exception $ex) {
                    
                }
?>
            </div>
        </div>
    </div>
    <div class="col-md-8">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="col-md-6">ชื่อยา</div>
                <div class="col-md-3">จำนวน</div>
                <div class="col-md-3">ปริมาณ</div>
                <div class="clearfix"></div>
            </div>
            <div class="panel-body">
                <div id="dataDrug">

                </div>
            </div>
        </div>

    </div>
    <div class="clearfix"></div><hr>
<?=
Html::button("<i class='glyphicon glyphicon-ok'> </i> เพิ่ม", [
    'id' => 'btnAdddrug',
    //'data-url' => Url::to(['/ezforms/ezform/add-drug', 'hospcode' => "'" . $datamodel_table->hsitecode . "'", 'pid' => "'" . $datamodel_table->hptcode . "'", 'ptlink' => "'" . md5($datamodel_table->cid) . "'"]),
    'class' => 'btn btn-success',
]);
?>
    <?=
    Html::button("<i class='glyphicon glyphicon-remove'> </i> ยกเลิก", [
        'id' => 'btnCancle',
        'class' => 'btn btn-danger',
    ]);
    ?>

</div>
    <?php 
    Modal::end();

    ?>


<?php
$this->registerJs("
        
        $('.dataDrug').on('click',function(){
            $('#btnAdddrug').attr('data-url',$(this).attr('data-url'));
            $('#dataDrug').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
            $.ajax({
                url:$(this).attr('data-url'),
                dataType: 'json',
                success: function (data) {
                    $('#dataDrug').html('');
                    if(data.length > 0){
                        $.each(data, function (key, value) {
                            $('#dataDrug').append('<div class=\'col-md-6\'>'+value.DNAME+'</div> <div class=\'col-md-3\'>'+value.AMOUNT+'</div> <div class=\'col-md-3\'>'+value.UNIT_PACKING+'</div> ');
                            
                        });
                    }else{
                        $('#dataDrug').html('<label class=\'alert-danger\'>ไม่พบข้อมูล</label> ');
                    }
                },
                error: function (error) {
                    console.log(error);
                }
            });
            return false;
        });
        
        $('#btnCancle').on('click', function () {
            $('#btnAdddrug').attr('data-url','');
            $('#dataDrug').html('');
            $('#modal-add').modal('hide');
            return false;
        });
        
        ");
?>