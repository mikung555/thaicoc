<?php
use Yii;
use yii\helpers\Html;
use yii\helpers\Url;

\yii\widgets\Pjax::begin(['id'=>$field->ezf_id.'-pjax-'.$field->ezf_field_id]);
echo \yii\grid\GridView::widget([
    'id' => 'research-grid',
    'dataProvider' => $dataProvider,
    'layout' => "{items}\n{pager}",
    'columns' => [
	['class' => 'yii\grid\SerialColumn'],
	[
	    'attribute'=>'file_name',
	    'header'=>'',
	    'value'=>function ($data){
		return '<img src="'.Yii::getAlias('@web/fileinput').'/'.$data['file_name'].'" width="100" height="100">';
	    },
	    'format' => 'raw',	    
	    'headerOptions'=>['style'=>'text-align: center;'],
	    'contentOptions'=>['style'=>'width:100px;text-align: center;'],
	],
	'file_name:ntext',
	[
	    'attribute'=>'created_at',
	    'value'=>function ($data){return Yii::$app->formatter->asDate($data['created_at'], 'd/m/Y');},
	    'headerOptions'=>['style'=>'text-align: center;'],
	    'contentOptions'=>['style'=>'width:100px;text-align: center;'],
	    'filter'=>'',
	],
	[
	    'class' => 'common\lib\sdii\widgets\SDActionColumn',
	    'template'=>'{delete}',
	    'buttons'=>[
		'delete' => function ($url, $data, $key) {
		    //if(Yii::$app->user->id==$data['created_by']){
			return Html::a('<span class="glyphicon glyphicon-trash"></span>', Url::to(['ezforms/fileinput/delete', 'id'=>$data['fid']]), [
			    'data-action' => 'delete',
			    'title' => Yii::t('yii', 'Delete'),
			    'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
			    'data-method' => 'post',
			]);
		    //}
		},
	    ],
	],
    ],
]);
\yii\widgets\Pjax::end();
?>
