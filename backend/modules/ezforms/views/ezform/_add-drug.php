<?php

use Yii;
use yii\helpers\Html;
use appxq\sdii\widgets\GridView;
?>
<div class="panel panel-info">
    <div class="panel-heading">
        แทรกยา
    </div>
    <div class="panel-body">
        <?=
        GridView::widget([
            'dataProvider' => $dataProvider
        ]);
        ?>
        <?=
        Html::button('แทรก', [
            'id' => 'btnAdddrug',
            'class' => 'btn btn-success'
        ]);
        ?>
    </div>
</div>
<script>

    $('[id*=\'add1n9\']').parent().prepend('<button id=\'showDrug\' class=\'btn btn-success\'><i class=\'glyphicon glyphicon-plus\'> </i></button> ');
    $('#showDrug').on('click', function () {
        // $('#modal-add #modalContent').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
        $('#modal-add').modal('show');
        return false;
    });

    $('#btnAdddrug').on('click', function () {
        $.ajax({
            url: $(this).attr('data-url'),
            dataType: 'json',
            success: function (data) {
                $.each(data, function (key, value) {
                    $('[id*=\'add1n9\']').val($('[id*=\'add1n9\']').val()+value.DNAME + ' - ' + value.AMOUNT + ' ' + value.UNIT_PACKING + '\n');
                });
               $('#btnAdddrug').attr('data-url','');
               $('#dataDrug').html('');
               $('#modal-add').modal('hide');
            },
            error: function (error) {
                console.log(error);
            }
        });
        return false;
    });
   

   
 $('#sddynamicmodel-med').parent().prepend('<button id=\'showDrug\' class=\'btn btn-success\'><i class=\'glyphicon glyphicon-plus\'> </i></button> ');
    $('#showDrug').on('click', function () {
        // $('#modal-add #modalContent').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
        $('#modal-add').modal('show');
        return false;
    });

    $('#btnAdddrug').on('click', function () {
        $.ajax({
            url: $(this).attr('data-url'),
            dataType: 'json',
            success: function (data) {
                $.each(data, function (key, value) {
                    $('#sddynamicmodel-med').val($('#sddynamicmodel-med').val()+value.DNAME + ' - ' + value.AMOUNT + ' ' + value.UNIT_PACKING + '\n');
                });
               $('#btnAdddrug').attr('data-url','');
               $('#dataDrug').html('');
               $('#modal-add').modal('hide');
            },
            error: function (error) {
                console.log(error);
            }
        });
        return false;
    });
    
    
</script>