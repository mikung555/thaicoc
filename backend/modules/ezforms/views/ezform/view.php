<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\ezforms\models\Ezform */

$this->title = 'Ezform#'.$model->ezf_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Ezforms'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ezform-view">

  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title" id="itemModalLabel"><?= Html::encode($this->title) ?></h4>
  </div>
  <div class="modal-body">
    <?= DetailView::widget([
      'model' => $model,
      'attributes' => [
      'ezf_id',
      'ezf_name',
      'ezf_detail:ntext',
      //'ezf_table',
      //'user_create',
      [
      'attribute' => 'username',
      'label' => Yii::t('username', 'ผู้สร้าง'),
      ],
      //'create_date',
      //'user_update',
      //'update_date',
      //'status',
      //'shared',
      [
      'attribute' => 'shared',
      'value' => (0 == $model->shared) ? Yii::t('app', 'Private') : Yii::t('app', 'Public'),
      ],
      [
      'attribute' => 'public_listview',
      'value' => (0 == $model->public_listview) ? Yii::t('app', 'No') : Yii::t('app', 'Yes'),
      ],
      [
      'attribute' => 'public_edit',
      'value' => (0 == $model->public_edit) ? Yii::t('app', 'No') : Yii::t('app', 'Yes'),
      ],
      [
      'attribute' => 'public_delete',
      'value' => (0 == $model->public_delete) ? Yii::t('app', 'No') : Yii::t('app', 'Yes'),
      ],
      //'public_listview',
      //'public_edit',
      //'public_delete',
      ],
      ]) ?>
    </div>
  </div>
