<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\lib\sdii\components\helpers\SDNoty;
use kartik\tree\TreeView;
use kartik\tree\TreeViewInput;
use backend\models\TblTree;

/* @var $this yii\web\View */
/* @var $model backend\modules\ezforms\models\Ezform */
/* @var $form yii\widgets\ActiveForm */
$siteconfig = \common\models\SiteConfig::find()->One();
?>

<div class="ezform-form">

    <?php $form = ActiveForm::begin(['id'=>$model->formName()]); ?>
	<div class="modal-header">
	    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	    <h4 class="modal-title" id="itemModalLabel">Ezform</h4>
	</div>

	<div class="modal-body">

		<?= $form->field($model, 'ezf_name')->textInput(['maxlength' => true]) ?>

		<?= $form->field($model, 'ezf_detail')->textarea(['rows' => 6]) ?>

		<?= $form->field($model, 'shared')->radioList([0 => 'ส่วนตัว', 1 => 'สาธารณะ']) ?>

		<?= $form->field($model, 'public_listview')->checkbox() ?>

		<?= $form->field($model, 'public_edit')->checkbox() ?>

		<?= $form->field($model, 'public_delete')->checkbox() ?>

        <?php
            /*echo '<label class="control-label">หมวดกิจกรรม</label>';
            echo TreeViewInput::widget([
            'name' => 'kvTreeInput',
            'query' => TblTree::find()->where('readonly=1 or userid='.Yii::$app->user->id.' or id IN (select distinct root from tbl_tree where userid='.Yii::$app->user->id.')')->addOrderBy('root, lft'),
            'headingOptions'=>['label'=>'Categories'],
            'name' => 'category_id',
            'asDropdown' => true,
            'multiple' => false,
            'fontAwesome' => true,
            'rootOptions' => [
               'label'=>'<i class="fa fa-home"></i> คณะสาธารณสุขศาสตร์',
               'class'=>'text-success'
           ],
            ]);*/
        ?>
        <?= $form->field($model, 'category_id')->widget(TreeViewInput::classname(), [
                'name' => 'category_id',
                'id' => 'category_id',
                'query' => TblTree::find()->where('readonly=1 or userid='.Yii::$app->user->id.' or id IN (select distinct root from tbl_tree where userid='.Yii::$app->user->id.')')->addOrderBy('root, lft'),
                'headingOptions'=>['label'=>'Categories'],
                'asDropdown' => true,
                'multiple' => false,
                'fontAwesome' => true,
                'rootOptions' => [
                'label'=>'<i class="fa fa-home"></i>'.$siteconfig->sitename,
                'class'=>'text-success',
                'options' => ['disabled' => false]
           ],
          ])
        ?>
        <br>
        <?php
        $items = [
            '0' => 'ไม่มีเป้าหมาย',
            '2' => 'เลือกเป้าหมายอื่นๆ',
        ];
        $items['1'] = 'Person profile [ ? view form ]';
        $items['3'] = 'พิกัด';
        
        echo $form->field($model, 'comp_type')->radioList($items, ['id'=>'rdo-comp_type'])->label('ประเภทของฟอร์ม');
        echo $form->field($model, 'comp_id_target')->hiddenInput(['id'=>'hide-comp_id_target'])->label(false);
        ?>
        <div id="other-comp" style="display: none;">
            <?php echo Html::dropDownList('list_comp', null, $ezformComponents, ['prompt'=>'ไม่เลือก', 'id'=>'list_comp', 'class'=>'form-control']) ?>
        </div>
<?php  $this->registerJs("
$('input[name=\'Ezform[comp_type]\']').on('click', function(e){
    if($(this).val()==2){
        $('#other-comp').fadeIn();
    }else{
        $('#other-comp').fadeOut();
    }
    //
    if($(this).val()=='1461911117076186800'){
        $('#hide-comp_id_target').val($(this).val());
    }else{
        $('#hide-comp_id_target').val(null);
        $('#list_comp').val(null);
    }
});
$('#list_comp').on('change', function(e){
    $('#hide-comp_id_target').val($(this).val());
});
"); ?>
	</div>
	
	<div class="modal-footer">
	    <?= Html::submitButton($model->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
	</div>

    <?php ActiveForm::end(); ?>

</div>

<?php  $this->registerJs("
$('form#{$model->formName()}').on('beforeSubmit', function(e){
    var \$form = $(this);
    $.post(
	\$form.attr('action'), //serialize Yii2 form
	\$form.serialize()
    ).done(function(result){
	if(result.status == 'success'){
	    ". SDNoty::show('result.message', 'result.status') ."
	    if(result.action == 'create'){
		$(document).find('#modal-ezform').modal('hide');
		$.pjax.reload({container:'#ezform-grid-pjax'});
		//location.reload();
	    } else if(result.action == 'update'){
		$(document).find('#modal-ezform').modal('hide');
		$.pjax.reload({container:'#ezform-grid-pjax'});
	    }
	} else{
	    ". SDNoty::show('result.message', 'result.status') ."
	}
    }).fail(function(){
	console.log('server error');
    });
    return false;
});

");?>

<?php $this->registerJs('
    $( "input[type=radio]" ).on( "click", function() {
        var value = $("input[type=radio]:checked").val();
        if (0 == value) {
            $("#ezform-public_listview").prop("checked", false);
            $("#ezform-public_edit").prop("checked", false);
            $("#ezform-public_delete").prop("checked", false);
        } else {
            $("#ezform-public_listview").prop("checked", true);
            $("#ezform-public_edit").prop("checked", true);
            $("#ezform-public_delete").prop("checked", true);
        }
    });
'); ?>
