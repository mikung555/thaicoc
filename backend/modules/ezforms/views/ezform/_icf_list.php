<?php
    use yii\helpers\Html;
    use yii\helpers\Url;
    use yii\helpers\VarDumper;

    if(!$hasRow || sizeof($fileSet) == 0){
        exit();
    }
    //echo $a;
    //VarDumper::dump($getIdQry,10,true);
    //echo $a;
    //VarDumper::dump($fileSet,10,true);


    $headed = false;
    foreach($fileSet as $site => $data){

        if( !( $data['confirm'] || sizeof($data['files']) > 0) ){
            continue;
        }

        if(!$headed){
            $headed=true;
            echo '<label>แบบยินยอมอาสาสมัครเข้าร่วมโครงการ (หน่วยบริการอื่น)</label>';
        }

        echo '<div class="alert panel-primary">';

            if($data['confirm']){
                echo '<label class="text-success">';
                    echo '<i class="fa fa-check fa-2x"></i>';
                    echo ' ตรรวจผ่านแล้ว';
                echo '</label>';
            }

            if( sizeof($data['files']) > 0 ){
                foreach($data['files'] as $keyFile => $filename){
                    echo '<p title="แสดงภาพ" class="p-icflist-modal" filename="'.$filename['name'].'">';
                        echo $filename['old'];
                    echo '</p>';
                }
            }

        echo '</div>';
    }

    $this->registerJs('
        $(".p-icflist-modal").click(function(){
            var imagepath = $(this).attr("filename");
            var oldname = $(this).html();
            $("#icf-list-modal-content").html(oldname+"<br/><img style=\'width: 100%\' src=\''.Url::to('../../fileinput').'/"+imagepath+"\'>");
            $("#icf-list-modal-btn").click();
        });
    ');

    $this->registerCss('
        .p-icflist-modal{
            cursor: pointer;
            color: #3C8DBC !important;
        }
        .p-icflist-modal:hover{
            color: #72afd2 !important;
        }
        #icf-list-modal-btn{
            display:none !important;
        }
    ');

?>
<button type="button" data-toggle="modal" data-target="#icf-list-modal" id="icf-list-modal-btn"></button>
<div class="modal fade" id="icf-list-modal" role="dialog">
    <div class="modal-dialog" style="width: 80%;">
        <div class="modal-content">
            <div class="modal-body" id="icf-list-modal-content">

            </div>
        </div>
    </div>
</div>





