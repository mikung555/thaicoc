<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\lib\sdii\components\helpers\SDNoty;
use yii\helpers\ArrayHelper;
use backend\modules\ezforms\components\EzformQuery;
use yii\helpers\Url;

?>
<div id='addPanel' style='display:none;'>
</div>
<div id='showPanel' >
    <div id='addPanel' >
        <?php
        $form2 = ActiveForm::begin([
                    'enableAjaxValidation'=>true,
                    'id' => $model2->formName(),
                    'fieldConfig' => [
                        'horizontalCssClasses' => [
                            'label' => 'col-sm-2',
                            'offset' => 'col-sm-offset-2',
                            'wrapper' => 'col-sm-10'
                        ]
                    ],
                    'layout' => 'horizontal'
        ]);
        ?>
        <?= Html::activeHiddenInput($model2, 'ezf_id'); ?>
	<?= $form2->field($model2, 'ezf_field_label')->textInput(['maxlength' => true, 'value'=>'คำถามไม่ระบุหัวข้อ']) ?>
	<?= $form2->field($model2, 'ezf_field_color')->widget(kartik\widgets\ColorInput::className(),[
	    'options' => ['readonly' => true]
	]) ?>
	<?= $form2->field($model2, 'ezf_field_icon')->checkbox()?>
        <?php

        echo $form2->field($model2, 'ezf_field_help')->widget(vova07\imperavi\Widget::className(), [
            'settings' => [
                'lang' => 'th',
                'minHeight' => 100,
                //'imageManagerJson' => Url::to(['ezform/images-get']),
                'imageUpload' => Url::to(['ezform/image-upload']),
                'plugins' => [
                    'fullscreen',
                    'imagemanager'
                ]
            ]
        ]);
        //echo Yii::getAlias('@backend');
        /*
        use dosamigos\tinymce\TinyMce;
        echo $form2->field($model2, 'ezf_field_help')->widget(TinyMce::className(), [
            'options' => ['rows' => 6],
            'language' => 'th_TH',
            'clientOptions' => [
                'plugins' => [
                    "advlist autolink lists link charmap print preview anchor",
                    "searchreplace visualblocks code fullscreen",
                    "insertdatetime image media table contextmenu paste"
                ],
                'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
            ]
        ]);
        */

        echo $form2->field($model2, 'ezf_field_val')->checkbox(['value' => '99'])->label('แสดงข้อความช่วยเหลือ');
        ?>
		<div class="form-group field-ezformfields-ezf_field_options">
			<div class="col-sm-10 col-sm-offset-2">
				<div class="checkbox">
					<label for="ezformfields-ezf_field_options">
						<input type="checkbox" id="ezformfields-ezf_field_options" name="ezf_field_options[remember_field]" value="1">
						Remember Fields
					</label>
				</div>
				<div class="help-block help-block-error"></div>
			</div>
		</div>

        <?= $form2->field($model2, 'ezf_field_name')->textInput(['value' => $numId, 'style' => 'width:150px;']) ?>
        <?=
        $form2->field($model2, 'ezf_field_type')->dropDownList(ArrayHelper::map(EzformQuery::getInputAll(), 'input_id', 'input_name'));
        ?>
        <?php $model2->ezf_field_lenght = 12 ?>
        <?= $form2->field($model2, 'ezf_field_lenght')->dropDownList(['3' => '25 %', '6' => '50 %', '9' => '75%','12'=>'100%']); ?>
	<?php
	if($model2->ezf_field_type==5 || $model2->ezf_field_type==19 || $model2->ezf_field_type==16){

	} else {
	    echo $form2->field($model2, 'ezf_field_required')->checkbox();
	}
	?>

    <?= Html::hiddenInput('conditionFields', '', ['id'=>'conditionFields'])?>
        <div class='well' id='showExample'>

            <label>คำถาม</label>
            <input type='text' class='form-control' value='คำตอบ' id='exText' name='exText' disabled='disabled'>

        </div>
        <br>
        <div class='row'>
            <div class='col-lg-12'>
                <p style='cursor:pointer' id='btnSetting'><i class='fa fa-cog'></i>  แสดงการตั่งค่าขั้นสูง</p>
            </div>
        </div>

	<div id='advanceSetting' class="well" >

		<br>
		<div id="drawingOption" class="row disabledDisplay">
		<div class="col-sm-12">
		    <a class="fileUpload btn btn-success" >อัพโหลดรูปพื้นหลัง
			<?= $fileUpload = \dosamigos\fileupload\FileUpload::widget([
				'id' => 'option-bg-id',
				'name' => 'option-bg',
				'url' => Url::to(['//ezforms/drawing/option-image', 'name'=>'test111']),
				'plus' => true,
				'options' => ['accept' => 'image/*', 'class'=>'upload'],
				'clientOptions' => [
				'maxFileSize' => 3000000
				],
				// Also, you can specify jQuery-File-Upload events
				// see: https://github.com/blueimp/jQuery-File-Upload/wiki/Options#processing-callback-options
				'clientEvents' => [
				'fileuploaddone' => "function(e, data) {
							var bgsize = 'auto auto';
							if(data.result.files.width > data.result.files.height){
								bgsize = '300px auto';
							} else {
								bgsize = 'auto 200px';
							}
							$('input[name=\"option-bg\"]').attr('data-url', data.result.files.newurl);
							$('input[name=\"option-bg\"]').fileupload({'maxFileSize':3000000,'url':$('input[name=\"option-bg\"]').attr('data-url')});

							$('#ezformfields-ezf_field_default').val(data.result.files.name);

							$('#showImg').css('background-image', 'url('+data.result.files.url+')');
							$('#showImg').css('background-size', bgsize);
							$('#showImg').css('background-position', 'center center');
							$('#showImg').css('background-repeat', 'no-repeat');
							}",
				'fileuploadfail' => "function(e, data) {
							console.log(e);
							console.log(data);
							}",
				],
			]); ?>
			</a>
			<?= $form2->field($model2, 'ezf_field_default')->hiddenInput()->label(false) ?>
			<?= Html::checkbox('allow_bg', ($model2->ezf_field_options!=''), ['label'=> 'อนุญาติให้เปลี่ยนรูปพื้นหลัง']); ?>
			<div id="showImg"></div>
		</div>
	    </div>
	    <br>
	    <div class="row">
		<div class="col-sm-12">
		    <div id="conditionBox">

		    </div>
		</div>
		</div>
      <div class="row" style='display:none;' id='radioLayout' >
    		<div class="col-lg-12" >

    		</div>
	    </div>

	</div>
        <span id="errorAddField"></span><br>
        <?= Html::submitButton($model2->isNewRecord ? Yii::t('backend', 'สร้างคำถาม') : Yii::t('backend', 'Update'), ['class' => $model2->isNewRecord ? 'btn btn-primary btn-flat' : 'btn btn-primary btn-flat']) ?>
        <?php ActiveForm::end(); ?>
    </div>
</div>
<?Php
$this->registerJS("
    newLoad = true;

    $('#advanceSetting').hide();
    $('#btnSetting').on('click',function(){
        $('#advanceSetting').toggle();
    });
    $('#ezformfields-ezf_field_type').on('change',function(){

        var idValue = $(this).val();
        if(idValue == 5){
            $('#radioLayout').attr('style','display:none');
            idValue = 19;
        }

        if(idValue == 24){
          $('#radioLayout').attr('style','display:none');
    			$('#drawingOption').removeClass('disabledDisplay');
          } else {
      			$('#ezformfields-ezf_field_default').val('');
      			$('#drawingOption').addClass('disabledDisplay');
            $('#radioLayout').attr('style','display:none');
      		}
          var ezf_id = $('#ezformfields-ezf_id').val();
          var labelValue = $('#ezformfields-ezf_field_type :selected').html();
          var nameValue = $('#ezformfields-ezf_field_name').val();
          var url = '".Url::to(['ezform/renderfield','idtype'=>''])."'+idValue+'&labelvalue='+labelValue+'&nameValue='+nameValue;
          $.post(
        		url, { ezf_id: ezf_id }
        	    ).done(function(result){
        		$('#showExample').html(result);
        		newLoad = true;
        		if(idValue==19){
        		    renderTap('div[checkbox-id]');
        		} else if(idValue==4 || idValue==6){
        		    renderTap('div[radio-id]');
        		} else if(idValue==16){
        		    renderTap('div.col-md-10.col-md-offset-2');
        		} else {
        		    $('#conditionBox').html('');
        		}
        	    }).fail(function(){
        		console.log('server error');
        	    });
                " . SDNoty::show('labelValue', '"success"') . "
          });
    $('form#{$model2->formName()}').on('beforeSubmit', function(e){
        $.blockUI({
        message : 'กำลังสร้างคำถาม ...',
        css: {
           border: 'none',
           padding: '15px',
           backgroundColor: '#000',
           '-webkit-border-radius': '10px',
           '-moz-border-radius': '10px',
           opacity: .5,
           color: '#fff'
       } });
        var ezf_field_type = $('#ezformfields-ezf_field_type').val();
        if(ezf_field_type==5){
            ezf_field_type=19;
        }
        var rowItem = $('div[data-dad-id]').length+1;
        var form = $(this);
        if(ezf_field_type==1){
            addTextField(rowItem,form);
        }else if(ezf_field_type==2){
            addHeadField(rowItem,form);
        }else if(ezf_field_type==3){
            addTextAreaField(rowItem,form);
        }else if(ezf_field_type==4){
            addRadioField(rowItem,form);
        }else if(ezf_field_type==19){
            addCheckboxField(rowItem,form)
        }else if(ezf_field_type==6){
            addSelectField(rowItem, form);
        }else if(ezf_field_type==7){
            addDateField(rowItem, form);
        }else if(ezf_field_type==8){
            addTimeField(rowItem, form);
        }else if(ezf_field_type==9){
            addDateTimeField(rowItem, form);
        } else if (ezf_field_type == 10) {
            addComponentField(rowItem, form);
        }else if (ezf_field_type == 12) {
            addPersonalField(rowItem, form);
        }else if (ezf_field_type == 11) {
            addSnomedField(rowItem, form);
        }else if (ezf_field_type == 14) {
            addFileinputField(rowItem, form);
        }else if (ezf_field_type == 13) {
            addProvinceField(rowItem, form);
        }else if (ezf_field_type == 16){
            addCheckbox2Field(rowItem,form);
        }else if (ezf_field_type == 15){
            addQuestionField(rowItem,form);
        }else if (ezf_field_type == 17 || ezf_field_type == 32){
            addHospitalField(rowItem,form);
        }else if (ezf_field_type == 18){
            addRefField(rowItem,form);
        }else if(ezf_field_type == 23){
            addScaleField(rowItem,form);
        }else if(ezf_field_type == 24){
            addDrawingField(rowItem,form);
        }else if(ezf_field_type == 30){
            addAudioField(rowItem,form);
        }else if(ezf_field_type == 25){
            addGridField(rowItem,form);
        }else if(ezf_field_type == 26){
            addMapField(rowItem,form);
        }else if (ezf_field_type == 27){
            addIcd9Field(rowItem,form);
      }else if (ezf_field_type == 28){
          addIcd10Field(rowItem,form);
      }else if (ezf_field_type == 31){
          addRef43Field(rowItem,form);
      }
        return false;
    });

");
?>
