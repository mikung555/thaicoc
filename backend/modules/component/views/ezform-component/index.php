<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\helpers\Url;
use common\lib\sdii\widgets\SDGridView;
use common\lib\sdii\widgets\SDModalForm;
use common\lib\sdii\components\helpers\SDNoty;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\component\models\EzformComponentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title = Yii::t('backend', 'ฟอร์ม ', [
            'modelClass' => 'Ezform',
        ]) . ':: จัดการเป้าหมาย';
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Ezforms'), 'url' => ['/ezforms/ezform']];
//$this->params['breadcrumbs'][] = ['label' => $model1->ezf_name, 'url' => ['view', 'id' => $model1->ezf_id]];
$this->params['breadcrumbs'][] = Yii::t('backend', 'จัดการเป้าหมาย');

$ezf_id = $_GET["id"];
?>
<div class="ezform-component-index">
    <br>
    <?= Html::a('<i class="fa fa-edit"></i>&nbsp;&nbsp;&nbsp;จัดการฟอร์ม', ['/ezforms/ezform/update', 'id' => $ezf_id], ['class' => 'btn btn-primary btn-flat']) ?>
    <?= Html::a('<i class="fa fa-comments-o"></i>&nbsp;&nbsp;&nbsp;จัดการเป้าหมาย', ['/component/ezform-component/index', 'id' => $ezf_id], ['class' => 'btn btn-info btn-flat ']) ?>
    <?= Html::a('<i class="fa fa-file-excel-o"></i>&nbsp;&nbsp;&nbsp;สร้าง EZForm จาก Excel', ['/file-upload', 'id' => $ezf_id], ['class' => 'btn btn-primary btn-flat']) ?>
    <?= Html::a('<i class="fa fa-globe"></i>&nbsp;&nbsp;&nbsp;ดูฟอร์มออนไลน์', ['/ezforms/ezform/viewform', 'id' => $ezf_id], ['class' => 'btn btn-primary btn-flat']) ?>
    <?= Html::a('<i class="fa fa-table"></i>&nbsp;&nbsp;&nbsp;ดูข้อมูล/ส่งออกข้อมูล', ['/view-data', 'id' => $ezf_id], ['class' => 'btn btn-primary btn-flat']) ?>
    <div style='float:right'>
        <?= Html::a('<i class="fa fa-mail-reply"></i>&nbsp;&nbsp;&nbsp;กลับไปหน้าเลือกฟอร์ม', ['/ezforms/ezform/index', 'tab'=>1, 'id' => $ezf_id], ['class' => 'btn btn-warning btn-flat']) ?>
    </div>
    <br>
    <br>
    <div class="box">
    <div class="box-body">
    <?php //echo $this->render('_search', ['model' => $searchModel]); ?>

    <!-- <select data-placeholder="Your Favorite Types of Bear" multiple="" class="chosen-select" tabindex="-1">
      <option value=""></option>
      <option>American Black Bear</option>
      <option>Asiatic Black Bear</option>
      <option>Brown Bear</option>
      <option>Giant Panda</option>
      <option>Sloth Bear</option>
      <option disabled="">Sun Bear</option>
      <option selected="">Polar Bear</option>
      <option disabled="">Spectacled Bear</option>
    </select> -->

    <?php  Pjax::begin(['id'=>'ezform-component-grid-pjax']);?>

    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#tab_1" data-toggle="tab">จัดการเป้าหมายที่สร้าง</a></li>
            <?php if (Yii::$app->user->can('administrator')) { ?>
                <li><a href="#tab_2" data-toggle="tab">จัดการเป้าหมายทั้งหมด(Admin)</a></li>
                <?php } ?>
                <li><a href="#tab_3" data-toggle="tab">จัดการเป้าหมายที่ถูกลบ</a></li>
            </ul>
            <div class="tab-content">
              <div class="tab-pane active" id="tab_1">
                <?= SDGridView::widget([
                 'id' => 'ezform-component-grid',
                 'panelBtn' => Html::button(Yii::t('app', '<span class="glyphicon glyphicon-plus"></span>'), ['data-url'=>Url::to(['ezform-component/create?id='.$id]), 'class' => 'btn btn-success btn-sm', 'id'=>'modal-addbtn-ezform-component']),
                 'dataProvider' => $dataProviderMyForm,
             //'filterModel' => $dataProviderMyForm,
                 'columns' => [
                 ['class' => 'yii\grid\SerialColumn'],

             //'comp_id',
                 'comp_name',
             //'comp_type',
                   //'comp_select',
                 [
                 'attribute' => 'comp_select',
                 'format' => 'text',
                 'content' => function ($data) {
                    if (1 == $data->comp_select) {
                        return 'เลือกได้หนึ่ง';
                    } else if (2 == $data->comp_select) {
                        return 'เลือกได้มากกว่าหนึ่ง';
                    } else {
                        return 'ไม่ระบุ';
                    }

                }
                ],
                   //'ezf_name',
                [
                'attribute' => 'ชื่อฟอร์ม',
                'format' => 'text',
                'content' => function ($data) {
                    if (isset($data->ezf_name)) {
                        return $data->ezf_name;
                    } else {
                        return 'ไม่ระบุ';
                    }

                }
                ],
            // 'field_id_key',
            // 'field_id_desc',
            // 'comp_order',
            // 'status',
            // 'user_create',
            // 'create_date',

                ['class' => 'common\lib\sdii\widgets\SDActionColumn'],
                ],
                ]); ?>
            </div><!-- /.tab-pane -->
            <?php if (Yii::$app->user->can('administrator')) { ?>
             <div class="tab-pane" id="tab_2">
                <?= SDGridView::widget([
                 'id' => 'ezform-component-grid',
                 //'panelBtn' => Html::button(Yii::t('app', '<span class="glyphicon glyphicon-plus"></span>'), ['data-url'=>Url::to(['ezform-component/create?id='.$id]), 'class' => 'btn btn-success btn-sm', 'id'=>'modal-addbtn-ezform-component']),
                 'dataProvider' => $dataProvider,
             //'filterModel' => $searchModel,
                 'columns' => [
                 ['class' => 'yii\grid\SerialColumn'],

             //'comp_id',
                 'comp_name',
             //'comp_type',
                       //'comp_select',
                 [
                 'attribute' => 'comp_select',
                 'format' => 'text',
                 'content' => function ($data) {
                    if (1 == $data->comp_select) {
                        return 'เลือกได้หนึ่ง';
                    } else if (2 == $data->comp_select) {
                        return 'เลือกได้มากกว่าหนึ่ง';
                    } else {
                        return 'ไม่ระบุ';
                    }

                }
                ],
                       //'ezf_id',
                [
                'attribute' => 'ชื่อฟอร์ม',
                'format' => 'text',
                'content' => function ($data) {
                    if (isset($data->ezf_name)) {
                        return $data->ezf_name;
                    } else {
                        return 'ไม่ระบุ';
                    }

                }
                ],
            // 'field_id_key',
            // 'field_id_desc',
            // 'comp_order',
            // 'status',
            // 'user_create',
            // 'create_date',

                ['class' => 'common\lib\sdii\widgets\SDActionColumn'],
                ],
                ]); ?>
            </div><!-- /.tab-pane -->
            <?php } ?>
            <div class="tab-pane" id="tab_3">
             <?= SDGridView::widget([
                 'id' => 'ezform-component-grid',
                 //'panelBtn' => Html::button(Yii::t('app', '<span class="glyphicon glyphicon-plus"></span>'), ['data-url'=>Url::to(['ezform-component/create?id='.$id]), 'class' => 'btn btn-success btn-sm', 'id'=>'modal-addbtn-ezform-component']),
                 'dataProvider' => $dataProviderMyFormDelete,
                 'columns' => [
                 ['class' => 'yii\grid\SerialColumn'],
                 'comp_name',
                 [
                 'attribute' => 'comp_select',
                 'format' => 'text',
                 'content' => function ($data) {
                    if (1 == $data->comp_select) {
                        return 'เลือกได้หนึ่ง';
                    } else if (2 == $data->comp_select) {
                        return 'เลือกได้มากกว่าหนึ่ง';
                    } else {
                        return 'ไม่ระบุ';
                    }

                }
                ],
                [
                'attribute' => 'ชื่อฟอร์ม',
                'format' => 'text',
                'content' => function ($data) {
                    if (isset($data->ezf_name)) {
                        return $data->ezf_name;
                    } else {
                        return 'ไม่ระบุ';
                    }

                }
                ],
                ['class' => 'common\lib\sdii\widgets\SDActionColumn' , 'template' => '{undo} {delete}', 'contentOptions' => ['style' => 'width:200px;']],
                ],
                ]); ?>
            </div>
        </div><!-- /.tab-content -->
    </div>

    <?php  Pjax::end();?>
    </div>
    </div>
</div>

<?=  SDModalForm::widget([
    'id' => 'modal-ezform-component',
    'size'=>'modal-lg',
    'tabindexEnable' => false
    ]);
    ?>

    <?php  $this->registerJs("
        $('#ezform-component-grid-pjax').on('click', '#modal-addbtn-ezform-component', function(){
            modalEzformComponent($(this).attr('data-url'));
        });

$('#ezform-component-grid-pjax').on('dblclick', 'tbody tr', function() {
    var id = $(this).attr('data-key');
    modalEzformComponent('".Url::to(['ezform-component/update', 'id'=>''])."'+id);
});

$('#ezform-component-grid-pjax').on('click', 'tbody tr td a', function() {
    var url = $(this).attr('href');
    var action = $(this).attr('data-action');

    if(action === 'update' || action == 'view'){
     modalEzformComponent(url);
 } else if(action === 'delete') {
     yii.confirm('".Yii::t('app', 'Are you sure you want to delete this item?')."', function(){
         $.post(
          url
          ).done(function(result){
              if(result.status == 'success'){
                  ". SDNoty::show('result.message', 'result.status') ."
                  $.pjax.reload({container:'#ezform-component-grid-pjax'});
              } else {
                  ". SDNoty::show('result.message', 'result.status') ."
              }
          }).fail(function(){
              console.log('server error');
          });
})
} else if (action === 'undo') {
    yii.confirm('".Yii::t('app', 'Are you sure you want to undo this item?')."', function(){
        $.post(
            url
            ).done(function(result){
                if(result.status == 'success'){
                    ". SDNoty::show('result.message', 'result.status') ."
                    location.reload();
                    $.pjax.reload({container:'#ezform-grid-pjax'});
                } else {
                    ". SDNoty::show('result.message', 'result.status') ."
                }
            }).fail(function(){
                console.log('server error');
            });
})
}
return false;
});

function modalEzformComponent(url) {
    $('#modal-ezform-component .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
    $('#modal-ezform-component').modal('show')
    .find('.modal-content')
    .load(url);
}

");

?>


