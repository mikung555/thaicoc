<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\lib\sdii\components\helpers\SDNoty;
use kartik\widgets\Select2;
use yii\bootstrap\Button;

/* @var $this yii\web\View */
/* @var $model backend\modules\component\models\EzformComponent */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ezform-component-form">

  <?php $form = ActiveForm::begin(['id'=>$model->formName()]); ?>
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title" id="itemModalLabel">Ezform Component : เป้าหมาย</h4>
  </div>

  <div class="modal-body">

      <?php
      echo $form->field($model, 'ezf_id')->widget(Select2::classname(), [
          'id' =>'ezf_id',
          'data' => $listData,
          'maintainOrder'=>false,
          'options' => ['placeholder' => 'เลือก', 'multiple' => false],
          'pluginOptions' => [
              'tags' => false,
              'allowClear' => true,
              'maximumInputLength' => 10
          ],
      ]);
      ?>

      <?= $form->field($model, 'comp_name')->textInput(['maxlength' => true]) ?>

      <?= $form->field($model, 'comp_select')->dropDownList(
        [1 => 'เลือกได้หนึ่ง', '2' => 'เลือกได้มากกว่าหนึ่ง'])
      ?>

      <?php
      if ($model->field_id_key == "") $model->field_id_key=$defaultField->ezf_field_id;
      
      echo $form->field($model, 'field_id_key')->widget(Select2::classname(), [
          'id' =>'field_id_key',
          'data' => $listFormFields,
          'maintainOrder'=>false,
          'options' => ['placeholder' => 'เลือก', 'multiple' => false],
          'pluginOptions' => [
              'tags' => false,
              'allowClear' => true,
              'maximumInputLength' => 10
          ],
      ]);
      ?>

      <?php /*$form->field($model, 'field_id_desc')->widget(Select2::classname(), [
              'data' => $listFormFields,
              'options' => ['placeholder' => 'เลือก', 'multiple' => true],
              'pluginOptions' => [
                'tags' => true,
                'maximumInputLength' => 10
              ],
          ])*/
      ?>

      <?= $form->field($model, 'field_id')->hiddenInput()->label(false); ?>
      <?php
      /*
        $desc = '';

        try {
          foreach ($model->field_id_desc as $value) {
            $desc .= $value.',';
          }
        } catch (ErrorException $e) {
            Yii::warning("Create component.");
        }
      \yii\helpers\VarDumper::dump($model->field_id_desc, 10, true);
      */
      ?>

        <?php
        echo $form->field($model, 'field_id_desc')->widget(Select2::classname(), [
            'id' =>'field_id_desc',
            'data' => $listFormFields,
            'maintainOrder'=>true,
            'options' => ['placeholder' => 'เลือก', 'multiple' => true],
            'pluginOptions' => [
                'tags' => true,
                'maximumInputLength' => 10
            ],
        ])->label('ตัวเลือกที่ใช้แสดง');
        ?>
      <?php

      echo $form->field($model, 'field_search')->widget(Select2::classname(), [
             'id' =>'field_search',
             'data' => $listFormFields,
            'maintainOrder'=>true,
             'options' => ['placeholder' => 'เลือก', 'multiple' => true],
             'pluginOptions' => [
               'tags' => true,
               'maximumInputLength' => 10
             ],
       ]);

      ?>

      <?= $form->field($model, 'comp_order')->hiddenInput()->label(false) ?>

      <?= $form->field($model, 'find_bysite')->radioList([
          '2' =>'None / เฉพาะใน Module',
          '1' => 'เฉพาะในหน่วยงานที่สังกัด (site)',
          '3' => 'ค้นหาได้ทุก site'
      ])->label('ประเภทการค้นหา')?>

      <?= $form->field($model, 'shared')->radioList([
          '0' => 'ส่วนตัว (Private)',
          '2' => 'ระบุคน (Assign)',
          '3' => 'ทุกคนใน Site นั้น',
          '1' => 'สาธารณะ (Public)',
      ]) ?>

      <?= $form->field($model, 'find_target')->checkbox()?>
      <?= $form->field($model, 'special')->checkbox();?>
        <div id="div-field_search_cid" style="display: <?php echo ($model->special >0) ? '' : 'none;';?>">
            <?php
            echo $form->field($model, 'field_search_cid')->widget(Select2::classname(), [
                'id' =>'field_search_cid',
                'data' => $listFormFields,
                'maintainOrder'=>false,
                'options' => ['placeholder' => 'เลือก', 'multiple' => false],
                'pluginOptions' => [
                    'tags' => false,
                    'allowClear' => true,
                    'maximumInputLength' => 10
                ],
            ]);
            ?>
        </div>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-primary preview" id="preview">Preview</button>

      <?= Html::submitButton($model->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
      <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

    <div id="js-preview" class="modal-body">
    </div>

  </div>

  <?php
$this->registerJs("
$(document).on('change', '#ezformcomponent-special', function(e){
     if ($(this).is(':checked')) {
      $('#div-field_search_cid').slideDown();
    } else {
      $('#div-field_search_cid').slideUp();
    }
    });
");

$this->registerJs("
    $('form#{$model->formName()}').on('beforeSubmit', function(e){
      var \$form = $(this);
      $.post(
        \$form.attr('action'), //serialize Yii2 form
        \$form.serialize()
        ).done(function(result){
          if(result.status == 'success'){
            ". SDNoty::show('result.message', 'result.status') ."
            if(result.action == 'create'){
              $(\$form).trigger('reset');
              $.pjax.reload({container:'#ezform-component-grid-pjax'});
              location.reload();
            } else if(result.action == 'update'){
              $(document).find('#modal-ezform-component').modal('hide');
              $.pjax.reload({container:'#ezform-component-grid-pjax'});
              location.reload();
            }
          } else{
            ". SDNoty::show('result.message', 'result.status') ."
          }
        }).fail(function(){
          console.log('server error');
        });
  return false;
});

");

  $this->registerJs("
      //$('.field-ezformcomponent-field_id_key').hide();
      
      $( '#preview' ).on( 'click', function() {
        //console.log($( '#ezformcomponent-field_id_desc' ).select2('data'));

        var field_id = '';

        $.each( $( '#ezformcomponent-field_id_desc' ).select2('data'), function( key, value ) {
          field_id += value.id + ',';
        });

        //alert(field_id.substring(0, (field_id.length - 1)));
        //$('#ezformcomponent-field_id').val(field_id.substring(0, (field_id.length - 1)));

        $.get( 'preview?type='+ $( '#ezformcomponent-comp_select' ).val() + '&field_id=' + $( '#ezformcomponent-field_id_key' ).val() + '&field_desc=' + field_id.substring(0, (field_id.length - 1)) + '&form_id=' + $( '#ezformcomponent-ezf_id' ).val(), function( data ) {
          $( '#js-preview' ).html( data );
        });
      });
    ");
?>
