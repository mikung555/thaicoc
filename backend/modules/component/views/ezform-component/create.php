<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\component\models\EzformComponent */

$this->title = Yii::t('backend', 'Create Ezform Component');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Ezform Components'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ezform-component-create">

    <?= $this->render('_form', [
        'model' => $model,
        'listData' => $listData,
        'listFormFields' => $listFormFields,
        'defaultField' => $defaultField,
    ]) ?>

</div>
