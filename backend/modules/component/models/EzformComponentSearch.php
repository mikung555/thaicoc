<?php

namespace backend\modules\component\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\component\models\EzformComponent;

/**
 * EzformComponentSearch represents the model behind the search form about `backend\modules\component\models\EzformComponent`.
 */
class EzformComponentSearch extends EzformComponent
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['comp_id', 'comp_type', 'comp_select', 'ezf_id', 'field_id_key', 'comp_order', 'status', 'user_create'], 'integer'],
            [['comp_name', 'field_id_desc', 'create_date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = EzformComponent::find()
                                    ->select('ezform_component.*, ezform.ezf_name')
                                    ->leftJoin('ezform', '`ezform_component`.`ezf_id` = `ezform`.`ezf_id`');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'comp_id' => $this->comp_id,
            'comp_type' => $this->comp_type,
            'comp_select' => $this->comp_select,
            'ezf_id' => $this->ezf_id,
            'field_id_key' => $this->field_id_key,
            'comp_order' => $this->comp_order,
            'status' => $this->status,
            'user_create' => $this->user_create,
            'create_date' => $this->create_date,
        ]);

        $query->andFilterWhere(['like', 'comp_name', $this->comp_name])
            ->andFilterWhere(['like', 'field_id_desc', $this->field_id_desc]);

        return $dataProvider;
    }

    public function searchMyFormComponent($params)
    {
        $query = EzformComponent::find()
                    ->select('ezform_component.*, ezform.ezf_name')
                    ->leftJoin('ezform', '`ezform_component`.`ezf_id` = `ezform`.`ezf_id`')
                    ->where(['ezform_component.user_create' => Yii::$app->user->id])
                    ->andWhere('ezform_component.status <> :status', [':status' => 3]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'comp_id' => $this->comp_id,
            'comp_type' => $this->comp_type,
            'comp_select' => $this->comp_select,
            'ezf_id' => $this->ezf_id,
            'field_id_key' => $this->field_id_key,
            'comp_order' => $this->comp_order,
            'status' => $this->status,
            'user_create' => $this->user_create,
            'create_date' => $this->create_date,
        ]);

        $query->andFilterWhere(['like', 'comp_name', $this->comp_name])
            ->andFilterWhere(['like', 'field_id_desc', $this->field_id_desc]);

        return $dataProvider;
    }

    public function searchMyFormComponentDelete($params)
    {
        $query = EzformComponent::find()
                    ->select('ezform_component.*, ezform.ezf_name')
                    ->leftJoin('ezform', '`ezform_component`.`ezf_id` = `ezform`.`ezf_id`')
                    ->where(['ezform_component.user_create' => Yii::$app->user->id])
                    ->andWhere('ezform_component.status = :status', [':status' => 3]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'comp_id' => $this->comp_id,
            'comp_type' => $this->comp_type,
            'comp_select' => $this->comp_select,
            'ezf_id' => $this->ezf_id,
            'field_id_key' => $this->field_id_key,
            'comp_order' => $this->comp_order,
            'status' => $this->status,
            'user_create' => $this->user_create,
            'create_date' => $this->create_date,
        ]);

        $query->andFilterWhere(['like', 'comp_name', $this->comp_name])
            ->andFilterWhere(['like', 'field_id_desc', $this->field_id_desc]);

        return $dataProvider;
    }
}
