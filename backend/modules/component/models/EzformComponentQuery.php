<?php

namespace backend\modules\component\models;

/**
 * This is the ActiveQuery class for [[EzformComponent]].
 *
 * @see EzformComponent
 */
class EzformComponentQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return EzformComponent[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return EzformComponent|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}