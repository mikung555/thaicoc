<?php

namespace backend\modules\component\models;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

use Yii;

/**
 * This is the model class for table "ezform_component".
 *
 * @property string $comp_id
 * @property string $comp_name
 * @property integer $comp_type
 * @property integer $comp_select
 * @property string $ezf_id
 * @property string $field_id_key
 * @property string $field_id_desc
 * @property integer $comp_order
 * @property integer $status
 * @property integer $find_target
 * @property integer $find_bysite
 * @property integer $special
 * @property string $user_create
 * @property string $create_date
 * @property EzformFields $fieldIdKey
 * @property Ezform $ezf
 * @property integer $field_search_cid
 * @property string $field_search
 */
class EzformComponent extends \yii\db\ActiveRecord
{

    public $ezf_name;
    public $ezf_table;
    public $field_id;
    public $pk_field;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ezform_component';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['comp_id', 'comp_name', 'comp_order', 'status', 'user_create'], 'required'],
            [['comp_id', 'comp_type', 'comp_select', 'ezf_id', 'field_id_key', 'comp_order', 'status', 'user_create', 'shared'], 'integer'],
            [['create_date', 'find_target', 'find_bysite', 'special', 'field_search_cid', 'field_search'], 'safe'],
            [['comp_name'], 'string', 'max' => 80],
            [['field_id_desc'], 'default']
        ];
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'create_date',
                'updatedAtAttribute' => false,
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'comp_id' => Yii::t('app', 'Comp ID'),
            'comp_name' => Yii::t('app', 'ชื่อเป้าหมาย'),
            'comp_type' => Yii::t('app', '1=Regular, 2=Derived'),
            //'comp_select' => Yii::t('app', '1=เลือกได้หนึ่ง (select2), 2=เลือกได้มากกว่าหนึ่ง'),
            'comp_select' => Yii::t('app', 'รูปแบบเป้าหมาย'),
            'ezf_id' => Yii::t('app', 'ฟอร์ม'),
            'field_id_key' => Yii::t('app', 'Field Id Key'),
            'field_id_desc' => Yii::t('app', 'Field Id Desc'),
            'comp_order' => Yii::t('app', 'Comp Order'),
            'status' => Yii::t('app', 'Status'),
            'find_target' => 'เลือกรายการล่าสุด เมื่อเป้าหมายตรงกัน',
            'find_bysite' => 'ค้นได้เฉพาะใน Site ตัวเอง',
            'special' => 'เป้าหมายพิเศษ (ค้นด้วยเลขบัตร ปชช.)',
            'field_search' => 'ตัวแปรสำหรับใช้ค้นหาเป้าหมาย',
            'field_search_cid' => 'ตัวแปรเลขบัตร ปชช. สำหรับใช้ค้นหาเป้าหมาย',
            'user_create' => Yii::t('app', 'User Create'),
            'create_date' => Yii::t('app', 'Create Date'),
            'shared' => Yii::t('app', 'การแชร์'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFieldIdKey()
    {
        return $this->hasOne(EzformFields::className(), ['ezf_field_id' => 'field_id_key']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEzf()
    {
        return $this->hasOne(Ezform::className(), ['ezf_id' => 'ezf_id']);
    }

    /**
     * @inheritdoc
     * @return EzformComponentQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new EzformComponentQuery(get_called_class());
    }
}
