<?php

use yii\helpers\Html;
use yii\grid\GridView;
use backend\modules\project84\controllers\DrillDownSec1Controller;
?>
<?php

foreach ($dataProvider as $value1) {
  $provincecode=$value1[provincecode];
  $dataAmphur=DrillDownSec1Controller::GetAmphur($report_id,$provincecode);
?>
<h3>จังหวัด <?= $value1[province]   ?></h3>
<table width="100%">
<thead>
<tr>
<th>
</th>
<th>
จำนวนผู้ที่ได้รับการตรวจ(คน)
</th>
</tr>
</thead>
<tbody>
  <?php
foreach ($dataAmphur as $value2) {
   ?>
  <tr>
  <td colspan="2">
อำเภอ <?= $value2[amphur]  ?>
</td>
</tr>
<?php
$dataTumbon=DrillDownSec1Controller::GetTumbon($report_id,$provincecode,$value2[amphurcode]);
foreach ($dataTumbon as $value3) {
 ?>
 <tr>
 <td style="background-color:lightblue">
ตำบล <?= $value3[tambon]  ?>
</td>
 <td style="background-color:lightblue">
<?= $value3[total]  ?> คน
</td>
</tr>
<?php
$dataSite=DrillDownSec1Controller::GetSite($report_id,$value3[tamboncode]);
foreach ($dataSite as $value4) {
?>
<tr>
<td>
<?= $value4[name]  ?>
</td>
<td>
<?= $value4[total]  ?> คน
</td>
</tr>
 <?php
  }//site
 }//tumbon
}//amphur
  ?>
</tbody>
</table>
</br>
</br>
<?php
}
 ?>
