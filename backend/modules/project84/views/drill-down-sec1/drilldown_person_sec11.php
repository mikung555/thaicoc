<?php

use yii\helpers\Html;
use yii\grid\GridView;
use backend\modules\project84\controllers\DrillDownSec1Controller;
use backend\modules\project84\controllers\DrillDownSec2Controller;
use backend\modules\project84\controllers\DrillDownSec3Controller;
?>
<?php
if($status=="no"){
echo "ท่านไม่มีสิทธิ์เข้าดูข้อมูลในหน่วยบริการนี้";
exit();
}
$num=1;
$hospital_name=$hosname[0][name];
$amphur_name=$hosname[0][amphur];
$province_name=$hosname[0][province];
?>
<h3><?= $hospital_name  ?>   อำเภอ  <?= $amphur_name  ?>       จังหวัด <?= $province_name ?></h3>
<table class="table table-striped">
<thead>
<tr>
  <th>
  ลำดับที่
  </th>
  <th>
  ชื่อ-สกุล
  </th>
  <th>
  รับการรักษาที่
  </th>
  <th>
  Kato-Katz
  </th>
  <th>
  Parasep
  </th>
  <th>
  FECT
  </th>
  <th>
  Urine
  </th>
  <th>
  ov
  </th>
  <th>
  mif
  </th>
  <th>
  ss
  </th>
  <th>
  ech
  </th>
  <th>
  taenia
  </th>
  <th>
  tt
  </th>
  <th>
  other
  </th>
</tr>
</thead>
<tbody>
  <?php
  foreach ($dataProvider as $value3) {
    $nameperson=$value3[title]." ".$value3[fname]." ".$value3[surname];
    $kpos=DrillDownSec3Controller::GetIcon($value3[kpos]);
    $ppos=DrillDownSec3Controller::GetIcon($value3[ppos]);
    $fpos=DrillDownSec3Controller::GetIcon($value3[fpos]);
    $upos=DrillDownSec3Controller::GetIcon($value3[upos]);
    $ov=DrillDownSec3Controller::GetIcon($value3[ov]);
    $mif=DrillDownSec3Controller::GetIcon($value3[mif]);
    $ss=DrillDownSec3Controller::GetIcon($value3[ss]);
    $ech=DrillDownSec3Controller::GetIcon($value3[ech]);
    $taenia=DrillDownSec3Controller::GetIcon($value3[taenia]);
    $tt=DrillDownSec3Controller::GetIcon($value3[tt]);
    $other=DrillDownSec3Controller::GetIcon($value3[other]);
   ?>
<tr>
<td>
<?= $num   ?>
</td>
<td>
<?=  $nameperson  ?>
</td>
<td>
<?=  DrillDownSec2Controller::ShortName($value3[hospitalname])  ?>
</td>
<td>
<?=  $kpos  ?>
</td>
<td>
<?=  $ppos  ?>
</td>
<td>
<?=  $fpos  ?>
</td>
<td>
<?=  $upos  ?>
</td>
<td>
<?=  $ov  ?>
</td>
<td>
<?=  $mif  ?>
</td>
<td>
<?=  $ss  ?>
</td>
<td>
<?=  $ech  ?>
</td>
<td>
<?=  $taenia  ?>
</td>
<td>
<?=   $tt ?>
</td>
<td>
<?=  $other  ?>
</td>
</tr>
 <?php
 $num++;
 }//foreach
  ?>
</tbody>
</table>
</br>
</br>
