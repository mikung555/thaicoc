<?php

namespace backend\modules\toolservice;

class module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\modules\toolservice\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
