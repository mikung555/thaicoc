<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\helpers\Url;
use appxq\sdii\widgets\GridView;
use appxq\sdii\widgets\ModalForm;
use appxq\sdii\helpers\SDNoty;
use appxq\sdii\helpers\SDHtml;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\toolservice\models\BuffeCommandSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'Buffe Commands');
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="buffe-command-index">

    <div class="sdbox-header">
	<h3><?=  Html::encode($this->title) ?></h3>
    </div>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p style="padding-top: 10px;">
	<span class="label label-primary">Notice</span>
	<?= Yii::t('app', 'You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b> or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.') ?>
    </p>

    <?php  Pjax::begin(['id'=>'buffe-command-grid-pjax']);?>
    <?= GridView::widget([
	'id' => 'buffe-command-grid',
	'panelBtn' => Html::button(SDHtml::getBtnAdd(), ['data-url'=>Url::to(['buffe-command/create']), 'class' => 'btn btn-success btn-sm', 'id'=>'modal-addbtn-buffe-command']). ' ' .
		      Html::button(SDHtml::getBtnDelete(), ['data-url'=>Url::to(['buffe-command/deletes']), 'class' => 'btn btn-danger btn-sm', 'id'=>'modal-delbtn-buffe-command', 'disabled'=>true]),
	'dataProvider' => $dataProvider,
	'filterModel' => $searchModel,
        'columns' => [
	    [
		'class' => 'yii\grid\CheckboxColumn',
		'checkboxOptions' => [
		    'class' => 'selectionBuffeCommandIds'
		],
		'headerOptions' => ['style'=>'text-align: center;'],
		'contentOptions' => ['style'=>'width:40px;text-align: center;'],
	    ],
	    [
		'class' => 'yii\grid\SerialColumn',
		'headerOptions' => ['style'=>'text-align: center;'],
		'contentOptions' => ['style'=>'width:60px;text-align: center;'],
	    ],

            //'id',
            'sitecode',
            //'template_id',
            'priority',
            'ctype',
            'cname',
            //'dadd',
            //'presql:ntext',
            'sql:ntext',
            'status',
            'clienterr:ntext',
            'serverr:ntext',
            'result:ntext',
            'controller',
            'table',
            //'dupdate',

	    [
		'class' => 'appxq\sdii\widgets\ActionColumn',
		'contentOptions' => ['style'=>'width:80px;text-align: center;'],
		'template' => '{view} {update} {delete}',
	    ],
        ],
    ]); ?>
    <?php  Pjax::end();?>

</div>

<?=  ModalForm::widget([
    'id' => 'modal-buffe-command',
    'size'=>'modal-lg',
]);
?>

<?php  $this->registerJs("
$('#buffe-command-grid-pjax').on('click', '#modal-addbtn-buffe-command', function() {
    modalBuffeCommand($(this).attr('data-url'));
});

$('#buffe-command-grid-pjax').on('click', '#modal-delbtn-buffe-command', function() {
    selectionBuffeCommandGrid($(this).attr('data-url'));
});

$('#buffe-command-grid-pjax').on('click', '.select-on-check-all', function() {
    window.setTimeout(function() {
	var key = $('#buffe-command-grid').yiiGridView('getSelectedRows');
	disabledBuffeCommandBtn(key.length);
    },100);
});

$('#buffe-command-grid-pjax').on('click', '.selectionBuffeCommandIds', function() {
    var key = $('input:checked[class=\"'+$(this).attr('class')+'\"]');
    disabledBuffeCommandBtn(key.length);
});

$('#buffe-command-grid-pjax').on('dblclick', 'tbody tr', function() {
    var id = $(this).attr('data-key');
    modalBuffeCommand('".Url::to(['buffe-command/update', 'id'=>''])."'+id);
});	

$('#buffe-command-grid-pjax').on('click', 'tbody tr td a', function() {
    var url = $(this).attr('href');
    var action = $(this).attr('data-action');

    if(action === 'update' || action === 'view') {
	modalBuffeCommand(url);
    } else if(action === 'delete') {
	yii.confirm('".Yii::t('app', 'Are you sure you want to delete this item?')."', function() {
	    $.post(
		url
	    ).done(function(result) {
		if(result.status == 'success') {
		    ". SDNoty::show('result.message', 'result.status') ."
		    $.pjax.reload({container:'#buffe-command-grid-pjax'});
		} else {
		    ". SDNoty::show('result.message', 'result.status') ."
		}
	    }).fail(function() {
		". SDNoty::show("'" . SDHtml::getMsgError() . "Server Error'", '"error"') ."
		console.log('server error');
	    });
	});
    }
    return false;
});

function disabledBuffeCommandBtn(num) {
    if(num>0) {
	$('#modal-delbtn-buffe-command').attr('disabled', false);
    } else {
	$('#modal-delbtn-buffe-command').attr('disabled', true);
    }
}

function selectionBuffeCommandGrid(url) {
    yii.confirm('".Yii::t('app', 'Are you sure you want to delete these items?')."', function() {
	$.ajax({
	    method: 'POST',
	    url: url,
	    data: $('.selectionBuffeCommandIds:checked[name=\"selection[]\"]').serialize(),
	    dataType: 'JSON',
	    success: function(result, textStatus) {
		if(result.status == 'success') {
		    ". SDNoty::show('result.message', 'result.status') ."
		    $.pjax.reload({container:'#buffe-command-grid-pjax'});
		} else {
		    ". SDNoty::show('result.message', 'result.status') ."
		}
	    }
	});
    });
}

function modalBuffeCommand(url) {
    $('#modal-buffe-command .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
    $('#modal-buffe-command').modal('show')
    .find('.modal-content')
    .load(url);
}

");?>