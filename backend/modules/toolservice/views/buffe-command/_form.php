<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use appxq\sdii\helpers\SDNoty;
use appxq\sdii\helpers\SDHtml;

/* @var $this yii\web\View */
/* @var $model backend\modules\toolservice\models\BuffeCommand */
/* @var $form yii\bootstrap\ActiveForm */
?>

<div class="buffe-command-form">

    <?php $form = ActiveForm::begin([
	'id'=>$model->formName(),
    ]); ?>

    <div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	<h4 class="modal-title" id="itemModalLabel">Buffe Command</h4>
    </div>

    <div class="modal-body">
	<?= $form->field($model, 'sitecode')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'template_id')->textInput() ?>

	<?= $form->field($model, 'priority')->textInput() ?>

	<?= $form->field($model, 'ctype')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'cname')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'dadd')->textInput() ?>

	<?= $form->field($model, 'presql')->textarea(['rows' => 6]) ?>

	<?= $form->field($model, 'sql')->textarea(['rows' => 6]) ?>

	<?= $form->field($model, 'status')->textInput() ?>

	<?= $form->field($model, 'clienterr')->textarea(['rows' => 6]) ?>

	<?= $form->field($model, 'serverr')->textarea(['rows' => 6]) ?>

	<?= $form->field($model, 'result')->textarea(['rows' => 6]) ?>

	<?= $form->field($model, 'controller')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'table')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'dupdate')->textInput() ?>

    </div>
    <div class="modal-footer">
	<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	<?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php  $this->registerJs("
$('form#{$model->formName()}').on('beforeSubmit', function(e) {
    var \$form = $(this);
    $.post(
	\$form.attr('action'), //serialize Yii2 form
	\$form.serialize()
    ).done(function(result) {
	if(result.status == 'success') {
	    ". SDNoty::show('result.message', 'result.status') ."
	    if(result.action == 'create') {
		$(\$form).trigger('reset');
		$.pjax.reload({container:'#buffe-command-grid-pjax'});
	    } else if(result.action == 'update') {
		$(document).find('#modal-buffe-command').modal('hide');
		$.pjax.reload({container:'#buffe-command-grid-pjax'});
	    }
	} else {
	    ". SDNoty::show('result.message', 'result.status') ."
	} 
    }).fail(function() {
	". SDNoty::show("'" . SDHtml::getMsgError() . "Server Error'", '"error"') ."
	console.log('server error');
    });
    return false;
});

");?>