<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\toolservice\models\BuffeCommandSearch */
/* @var $form yii\bootstrap\ActiveForm */
?>

<div class="buffe-command-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
	'layout' => 'horizontal',
	'fieldConfig' => [
	    'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
	    'horizontalCssClasses' => [
		'label' => 'col-sm-2',
		'offset' => 'col-sm-offset-3',
		'wrapper' => 'col-sm-6',
		'error' => '',
		'hint' => '',
	    ],
	],
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'sitecode') ?>

    <?= $form->field($model, 'template_id') ?>

    <?= $form->field($model, 'priority') ?>

    <?= $form->field($model, 'ctype') ?>

    <?php // echo $form->field($model, 'cname') ?>

    <?php // echo $form->field($model, 'dadd') ?>

    <?php // echo $form->field($model, 'presql') ?>

    <?php // echo $form->field($model, 'sql') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'clienterr') ?>

    <?php // echo $form->field($model, 'serverr') ?>

    <?php // echo $form->field($model, 'result') ?>

    <?php // echo $form->field($model, 'controller') ?>

    <?php // echo $form->field($model, 'table') ?>

    <?php // echo $form->field($model, 'dupdate') ?>

    <div class="form-group">
	<div class="col-sm-offset-2 col-sm-6">
	    <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
	    <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
	</div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
