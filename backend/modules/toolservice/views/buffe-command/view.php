<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\toolservice\models\BuffeCommand */

$this->title = 'Buffe Command#'.$model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Buffe Commands'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="buffe-command-view">

    <div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	<h4 class="modal-title" id="itemModalLabel"><?= Html::encode($this->title) ?></h4>
    </div>
    <div class="modal-body">
        <?= DetailView::widget([
	    'model' => $model,
	    'attributes' => [
		'id',
		'sitecode',
		'template_id',
		'priority',
		'ctype',
		'cname',
		'dadd',
		'presql:ntext',
		'sql:ntext',
		'status',
		'clienterr:ntext',
		'serverr:ntext',
		'result:ntext',
		'controller',
		'table',
		'dupdate',
	    ],
	]) ?>
    </div>
</div>
