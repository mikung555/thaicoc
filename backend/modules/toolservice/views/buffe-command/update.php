<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\toolservice\models\BuffeCommand */

$this->title = Yii::t('backend', 'Update {modelClass}: ', [
    'modelClass' => 'Buffe Command',
]) . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Buffe Commands'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id, 'sitecode' => $model->sitecode]];
$this->params['breadcrumbs'][] = Yii::t('backend', 'Update');
?>
<div class="buffe-command-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
