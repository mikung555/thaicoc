<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\toolservice\models\BuffeCommand */

$this->title = Yii::t('backend', 'Create Buffe Command');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Buffe Commands'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="buffe-command-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
