<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\helpers\Url;
use appxq\sdii\widgets\GridView;
use appxq\sdii\widgets\ModalForm;
use appxq\sdii\helpers\SDNoty;
use appxq\sdii\helpers\SDHtml;
use yii\widgets\ActiveForm; ?>

<div class="data-test-form">

    <?php $form = ActiveForm::begin([
	
    ]); ?>

    <div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	<h4 class="modal-title" id="itemModalLabel">Data Test</h4>
    </div>

    <div class="modal-body">
	<div class="row">
	<?= Html::textInput('search') ?>

	
	</div>

	
    </div>
    <div class="modal-footer">
	<?= Html::submitButton('search', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	<?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?= GridView::widget([
	'id' => 'fcvd-risk-grid',
	'dataProvider' => $dataProvider,
        'columns' => [
	    [
		'class' => 'yii\grid\CheckboxColumn',
		'checkboxOptions' => [
		    'class' => 'selectionFCvdRiskIds'
		],
		'headerOptions' => ['style'=>'text-align: center;'],
		'contentOptions' => ['style'=>'width:40px;text-align: center;'],
	    ],
	    [
		'class' => 'yii\grid\SerialColumn',
		'headerOptions' => ['style'=>'text-align: center;'],
		'contentOptions' => ['style'=>'width:60px;text-align: center;'],
	    ],
            'id',
            'sitecode',
            'sql',
            'clienterr',

	    'template' => '{view} {update} {delete}',
	    ],
        
    ]); ?>
    
