<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use appxq\sdii\helpers\SDNoty;
use appxq\sdii\helpers\SDHtml;

/* @var $this yii\web\View */
/* @var $model backend\modules\test\models\DataTest */
/* @var $form yii\bootstrap\ActiveForm */
?>

<div class="Tool-command-form">

    <?php
    $form = ActiveForm::begin([
                'id' => 'toolservice',
    ]);
    ?>

    <div class="modal-header">

        <h4 class="modal-title" id="itemModalLabel">Tool Command</h4>
    </div>

    <div class="modal-body">
        <div class="row">


            <?=
            $form->field($model, 'db')->widget(\kartik\select2\Select2::className(),[
                        'data' => $db,
                        'options' => [
                            'placeholder' => 'Select DB ...',
                            'multiple' => true
                        ],
            ])
            ?>

            <?= $form->field($model, 'sql')->textarea([
                'rows'=>20,
            ]) ?>

        </div>
    </div>
    <div class="modal-footer">
        <div id="load"></div>
        <?= Html::submitButton('Query', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-succsess']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-danger']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php $this->registerJs("

$('form#toolservice').on('beforeSubmit', function(e) {
   $('#load').html('<i class=\"sdloader-icon\"></i>');
    var \$form = $(this);
    $.post(
	\$form.attr('action'),
	\$form.serialize()
    ).done(function(result) {
	 console.log(result);
         " . \common\lib\sdii\components\helpers\SDNoty::show('result.message', 'result.status') . ";
             $('#load').html('');
    });
    return false;
});

"); ?>
