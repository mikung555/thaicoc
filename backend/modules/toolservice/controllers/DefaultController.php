<?php

namespace backend\modules\toolservice\controllers;

use yii\web\Controller;
use Yii;

class DefaultController extends Controller {

    public function actionIndex() {

        if (Yii::$app->request->post()) {
            $post = Yii::$app->request->post();
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            foreach ($post['DbConfigProvince']['db'] as $value) {

                $sql = "SELECT server,user,passwd,port FROM db_config_province WHERE db = '" . $value . "'";
                $model = \backend\modules\toolservice\models\DbConfigProvince::findBySql($sql)->one();

                $connection = new \yii\db\Connection([
                    'dsn' => "mysql:host=" . $model->server . ";dbname=" . $value,
                    'username' => $model->user,
                    'password' => $model->passwd,
                    'charset' => 'utf8',
                ]);
                try {
                    $connection->open();
                    if ($connection->createCommand($post['DbConfigProvince']['sql'])->query()) {
                        $result = [
                            'status' => 'success',
                            'action' => 'create',
                            'message' => \appxq\sdii\helpers\SDHtml::getMsgSuccess() . Yii::t('app', 'Data completed.'),
                            'data' => $model,
                        ];
                    } else {
                        $result = [
                            'status' => 'error',
                            'action' => 'create',
                            'message' => \appxq\sdii\helpers\SDHtml::getMsgError() . Yii::t('app', 'Data Error'),
                            'data' => $model,
                        ];
                    }
                } catch (Exception $ex) {
                    $result = [
                        'status' => 'error',
                        'action' => 'create',
                        'message' => \appxq\sdii\helpers\SDHtml::getMsgError() . Yii::t('app', 'Data Error' . $ex),
                        'data' => $model,
                    ];
                }
            }

            return $result;
        }

        $db = \backend\modules\toolservice\models\DbConfigProvince::find()->all();
        $db = \yii\helpers\ArrayHelper::map($db, 'db', 'db');

        $model = new \backend\modules\toolservice\models\DbConfigProvince();
        return $this->render('index', ['model' => $model, 'db' => $db]);
    }

}
