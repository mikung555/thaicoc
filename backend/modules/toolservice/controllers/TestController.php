<?php

namespace backend\modules\toolservice\controllers;

use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\Response;
use appxq\sdii\helpers\SDHtml;

use yii\web\Controller;
use Yii;

class TestController extends Controller {

  public  function actionSqlDbConPovice() {

        $model = \backend\modules\toolservice\classes\ToolSvQuery::sqlDbConPovice('tdc_47');
                \appxq\sdii\utils\VarDumper::dump($model);
    }

    public function actionIndex() {

        if (Yii::$app->request->post()) {
            $post = Yii::$app->request->post();
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            foreach ($post['DbConfigProvince']['db'] as $value) {

            //    $model = \backend\modules\toolservice\models\DbConfigProvince::findBySql($sql)->one();
                $connection  = \backend\modules\toolservice\classes\ToolSvQuery::sqlDbConPovice($value);



                try {
                    $connection->open();
                    if ($connection->createCommand($post['DbConfigProvince']['sql'])->query()) {
                        $result = [
                            'status' => 'success',
                            'action' => 'create',
                            'message' => \appxq\sdii\helpers\SDHtml::getMsgSuccess() . Yii::t('app', 'Data completed.'),
                            'data' => $model,
                        ];
                    } else {
                        $result = [
                            'status' => 'error',
                            'action' => 'create',
                            'message' => \appxq\sdii\helpers\SDHtml::getMsgError() . Yii::t('app', 'Data Error'),
                            'data' => $model,
                        ];
                    }
                } catch (Exception $ex) {
                    $result = [
                        'status' => 'error',
                        'action' => 'create',
                        'message' => \appxq\sdii\helpers\SDHtml::getMsgError() . Yii::t('app', 'Data Error' . $ex),
                        'data' => $model,
                    ];
                }
            }

            return $result;
        }

        $db = \backend\modules\toolservice\models\DbConfigProvince::find()->all();
        $db = \yii\helpers\ArrayHelper::map($db, 'db', 'db');

        $model = new \backend\modules\toolservice\models\DbConfigProvince();
        return $this->render('index', ['model' => $model, 'db' => $db]);
    }

}
