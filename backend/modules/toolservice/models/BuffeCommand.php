<?php

namespace backend\modules\toolservice\models;

use Yii;

/**
 * This is the model class for table "buffe_command".
 *
 * @property string $id
 * @property string $sitecode
 * @property integer $template_id
 * @property integer $priority
 * @property string $ctype
 * @property string $cname
 * @property string $dadd
 * @property string $presql
 * @property string $sql
 * @property integer $status
 * @property string $clienterr
 * @property string $serverr
 * @property string $result
 * @property string $controller
 * @property string $table
 * @property string $dupdate
 */

use backend\modules\toolservice\classes\ToolSvFunc;

class BuffeCommand extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
     public static function tableName()
     {
         return 'buffe_command';
     }

     /**
      * @return \yii\db\Connection the database connection used by this AR class.
      */
     public static function getDb()
     {
       
         return  ToolSvFunc::conDb('61.19.254.8', '3306', 'tdc_webservice','webservice','webservice!@#$%');
     }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sitecode', 'template_id', 'ctype', 'cname', 'dadd', 'presql', 'sql', 'clienterr', 'serverr', 'result', 'controller', 'table', 'dupdate'], 'required'],
            [['template_id', 'priority', 'status'], 'integer'],
            [['dadd', 'dupdate'], 'safe'],
            [['presql', 'sql', 'clienterr', 'serverr', 'result'], 'string'],
            [['sitecode', 'ctype'], 'string', 'max' => 10],
            [['cname'], 'string', 'max' => 30],
            [['controller'], 'string', 'max' => 20],
            [['table'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'sitecode' => Yii::t('app', 'Sitecode'),
            'template_id' => Yii::t('app', 'Template ID'),
            'priority' => Yii::t('app', 'Priority'),
            'ctype' => Yii::t('app', 'Ctype'),
            'cname' => Yii::t('app', 'Cname'),
            'dadd' => Yii::t('app', 'Dadd'),
            'presql' => Yii::t('app', 'Presql'),
            'sql' => Yii::t('app', 'Sql'),
            'status' => Yii::t('app', 'Status'),
            'clienterr' => Yii::t('app', 'Clienterr'),
            'serverr' => Yii::t('app', 'Serverr'),
            'result' => Yii::t('app', 'Result'),
            'controller' => Yii::t('app', 'Controller'),
            'table' => Yii::t('app', 'Table'),
            'dupdate' => Yii::t('app', 'Dupdate'),
        ];
    }
}
