<?php

namespace backend\modules\toolservice\models;

use Yii;

/**
 * This is the model class for table "db_config_province".
 *
 * @property string $province
 * @property string $zonecode
 * @property string $server
 * @property string $user
 * @property string $passwd
 * @property string $port
 * @property string $db
 * @property string $webservice
 */
class DbConfigProvince extends \yii\db\ActiveRecord
{
    
    public $sql;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'db_config_province';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['province', 'zonecode', 'server', 'user', 'passwd', 'port', 'db', 'webservice','sql'], 'required'],
            [['province', 'zonecode'], 'string', 'max' => 2],
            [['server'], 'string', 'max' => 50],
            [['user', 'passwd'], 'string', 'max' => 20],
            [['port'], 'string', 'max' => 10],
            [['webservice'], 'string', 'max' => 100],
            [['db','sql'],'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'province' => 'Province',
            'zonecode' => 'Zonecode',
            'server' => 'Server',
            'user' => 'User',
            'passwd' => 'Passwd',
            'port' => 'Port',
            'db' => 'Db',
            'webservice' => 'Webservice',
            'sql'=>'SQL'
        ];
    }
}
