<?php

namespace backend\modules\toolservice\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\toolservice\models\BuffeCommand;

/**
 * BuffeCommandSearch represents the model behind the search form about `backend\modules\toolservice\models\BuffeCommand`.
 */
class BuffeCommandSearch extends BuffeCommand
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'template_id', 'priority', 'status'], 'integer'],
            [['sitecode', 'ctype', 'cname', 'dadd', 'presql', 'sql', 'clienterr', 'serverr', 'result', 'controller', 'table', 'dupdate'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = BuffeCommand::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'template_id' => $this->template_id,
            'priority' => $this->priority,
            'dadd' => $this->dadd,
            'status' => $this->status,
            'dupdate' => $this->dupdate,
        ]);

        $query->andFilterWhere(['like', 'sitecode', $this->sitecode])
            ->andFilterWhere(['like', 'ctype', $this->ctype])
            ->andFilterWhere(['like', 'cname', $this->cname])
            ->andFilterWhere(['like', 'presql', $this->presql])
            ->andFilterWhere(['like', 'sql', $this->sql])
            ->andFilterWhere(['like', 'clienterr', $this->clienterr])
            ->andFilterWhere(['like', 'serverr', $this->serverr])
            ->andFilterWhere(['like', 'result', $this->result])
            ->andFilterWhere(['like', 'controller', $this->controller])
            ->andFilterWhere(['like', 'table', $this->table]);

        return $dataProvider;
    }
}
