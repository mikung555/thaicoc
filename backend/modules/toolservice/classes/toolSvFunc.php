<?php

    namespace backend\modules\toolservice\classes;
    use yii;
    class ToolSvFunc {
                
        public static function  conDb($host,$port,$db,$user,$pass){
            
          $connection = new \yii\db\Connection([
              'dsn' => "mysql:host=" . $host . "; port=" . $port.";dbname=".$db,
              'username' => $user,
              'password' => $pass,
              'charset' => 'utf8',
          ]);
          $connection->open();
          return $connection;
        }

    }
