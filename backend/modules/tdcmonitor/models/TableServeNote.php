<?php

namespace backend\modules\tdcmonitor\models;

use Yii;

/**
 * This is the model class for table "table_serve_note".
 *
 * @property string $sitecode
 * @property string $comment
 * @property integer $user_create
 * @property string $create_date
 * @property integer $user_update
 * @property string $update_date
 */
class TableServeNote extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'table_serve_note';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sitecode'], 'required'],
            [['comment'], 'string'],
            [['user_create', 'user_update'], 'integer'],
            [['create_date', 'update_date'], 'safe'],
            [['sitecode'], 'string', 'max' => 10]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'sitecode' => 'Sitecode',
            'comment' => 'Comment',
            'user_create' => 'User Create',
            'create_date' => 'Create Date',
            'user_update' => 'User Update',
            'update_date' => 'Update Date',
        ];
    }
}
