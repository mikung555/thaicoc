<?php

namespace backend\modules\tdcmonitor\controllers;

use yii\web\Controller;
use Yii;
use yii\data\ArrayDataProvider;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;
use \backend\modules\tdcmonitor\models\TableServeNote;
use appxq\sdii\helpers\SDHtml;
use yii\web\Response;

class TdccountController extends Controller {

    //public $activetab = "tdccount";
    
    public function actionIndex() {
      
        return $this->render('index');
    }
    public function actionMonitor() {
    //$sitecode = Yii::$app->user->identity->userProfile->sitecode;
        if(\Yii::$app->request->isAjax){
            $search = \Yii::$app->request->get("search","");
            
            if (!empty($search) || $search != ''){
                $condition = "LIKE '%$search%' OR ah.`name` LIKE '%$search%' ";
                
            }else{
                 $condition = "IN ('10980','11020','04571','04070','11052','05277',
                '11076','13958','04938','05274','04944','05330',
                '05318','05329','13964','05278','05279','05326',
                '05320','13957','11078','05275','05331','05214',
                '05319','05317','05321','04059','04061','05276',
                '05273','00473','00478','14859','12414','14775',
                '23891','15166','14861','14860','23936')";
            }
           
            $tdc_count = \Yii::$app->db->createCommand("SELECT c.`id`,ah.`name`,c.last_ping,c.his_type,h.his_name
                ,ah.zone_code,ah.provincecode,ah.province
                ,SUM(ts.his_record) as his_record,SUM(ts.record) as record,SUM(ts.qleft) as qleft
                ,ROUND((SUM(ts.record)/(SUM(ts.his_record)))*100,2) as progress
                ,(last_ping > DATE_ADD(NOW(),INTERVAL -10 MINUTE)) as status
                FROM buffe_config c 
                INNER JOIN buffe_his h ON c.his_type = h.id
                INNER JOIN all_hospital_thai_general ah ON ah.hcode = c.`id`
                INNER JOIN buffe_table_server ts ON ts.sitecode = c.`id` 
                WHERE c.`id` $condition  GROUP BY c.`id` ORDER BY status DESC,last_ping DESC")->queryAll();
            
            foreach ($tdc_count as $val){
                if($val['status'] == '1'){
                    $amount += $val['status'];
                   
                }
            }    
            $online = $amount;
            $offline = count($tdc_count) - $online;
            
//            \appxq\sdii\utils\VarDumper::dump($offline);    
            return $this->renderAjax('_monitor',[
                'tdc_count' => $tdc_count,
                'online' => $online,
                'offline' => $offline
            ]);
        }else{
            return $this->redirect(['/tdc-monitor']);
        }
    }
    public function actionTdcDetail(){
        $dataGet = \Yii::$app->request->get();
        //\appxq\sdii\utils\VarDumper::dump($dataGet);
        $query = \Yii::$app->db->createCommand("SELECT c.sitecode,c.`table`,c.his_record,c.record,c.qleft,c.progress
            FROM buffe_table_server c
            WHERE sitecode = :sitecode")->bindValues([':sitecode'=> $dataGet['sitecode'] ])->queryAll();
        
         
        $dataProvider = new \yii\data\ArrayDataProvider([
            'allModels' => $query,
            'pagination' => [
                'pageSize' => 55,
            ],
            
        ]);
      
        //\appxq\sdii\utils\VarDumper::dump($dataGet['sitecode']);  
        return $this->renderAjax('_tdc_detail',[
            'dataProvider' => $dataProvider,
            'sitecode' => $dataGet['sitecode'],
        ]);
    }
    
    public function actionTdcNote(){
        $dataGet = \Yii::$app->request->get();
        
        $sql_check = self::getCheckNote($dataGet['sitecode']);
        
        if(!empty($sql_check['sitecode'])){ //ถ้า sitecode มีค่า
           return $this->renderAjax('_tdc_note',['sitecode'=> $sql_check['sitecode'],'comment'=> $sql_check['comment']]);
        }else{
           return $this->renderAjax('_tdc_note',['sitecode' => $dataGet['sitecode']]); 
        }
        
        
        //\appxq\sdii\utils\VarDumper::dump($dataGet); 
        return $this->renderAjax('_tdc_note',[
            'sitecode' => $dataGet['sitecode'],
        ]);
    }
    
    public function actionCreateNote(){
        $dataPost = \Yii::$app->request->post();
        $sql_check = self::getCheckNote($dataPost['sitecode']);
        $model = New TableServeNote(); 
        
        if (Yii::$app->getRequest()->isAjax) {
            if(empty($sql_check['sitecode'])){ //ถ้า sitecode ไม่มีค่า
                
                Yii::$app->response->format = Response::FORMAT_JSON;
          
                $model->sitecode = \Yii::$app->request->post('sitecode');
                $model->comment = \Yii::$app->request->post('tdcnote_text');
                $model->user_create = \Yii::$app->user->identity->id;
                $model->create_date = date('Y-m-d H:i:s');
                $model->user_update = \Yii::$app->user->identity->id;
                $model->update_date = date('Y-m-d H:i:s'); 
                
                if ($model->save()) {
		    $result = [
                        'status' => 'success',
			'message' => 'Save Data completed.',
                    ];
                    return $result;//$this->render('index',['result'=> $result]);
                } else {
                    $result = [
                        'status' => 'error',
                        'message' => 'Can not create the data.',
                    ];
                    return $result;//$this->render('index',['result'=> $result]);
		}


            }else{ //ถ้า sitecode มีค่า (note)
                
                $model = $this->findModel($sql_check['sitecode']);
                Yii::$app->response->format = Response::FORMAT_JSON;
                $model->comment = \Yii::$app->request->post('tdcnote_text');
                $model->user_update = \Yii::$app->user->identity->id;
                $model->update_date = date('Y-m-d H:i:s');
                
                if ($model->update()) {
		    $result = ['status' => 'success','message' => 'Edit Data completed.'];
                    return $result; //$this->render('index',['result'=> $result]);
                } else {
                    $result = ['status' => 'error','message' => 'Can not Edit the data.'];
                    return $result; //$this->render('index',['result'=> $result]);
		}

                
            } // close if sql_check_sitecode
        }else{
            throw new NotFoundHttpException('Invalid request. Please do not repeat this request again.');
        } // close if getRequest_isAjax
        
    }
    
    public function actionViewNote(){
        $dataGet = \Yii::$app->request->get();
        
        $view = \Yii::$app->db->createCommand("SELECT * FROM table_serve_note WHERE sitecode = :sitecode ")->bindValues([':sitecode' => $dataGet['sitecode']])->queryOne();
        
        return $this->renderAjax("_view_note",['datanote' => $view,'sitecode'=>$dataGet['sitecode']]);
    }


    public function getCheckNote($pk_sitecode){
        
        $sql_data = \Yii::$app->db->createCommand("SELECT `sitecode`,`comment` FROM table_serve_note WHERE sitecode = :sitecode ")
        ->bindValues([':sitecode' => $pk_sitecode])->queryOne();
        //\appxq\sdii\utils\VarDumper::dump($pk_sitecode); 
        return $sql_data;
    }
    
    protected function findModel($id)
    {
        if (($model = TableServeNote::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
