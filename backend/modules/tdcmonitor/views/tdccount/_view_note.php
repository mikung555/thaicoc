<?php

use Yii;
use yii\helpers\Html;
use common\lib\sdii\components\utils\SDdate;

$sitecode = \backend\modules\ezforms\components\EzformQuery::getHospital($sitecode);
?>
<div class="row" style="padding: 10px;">
    <div class="col-md-12">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title"><i class="fa fa-h-square" aria-hidden="true"></i> <?= Html::tag('label','หน่วยบริการ ('.$sitecode['hcode'] . " : " . $sitecode['name']. " )" ); ?> </h4>

        </div>
        <div class="modal-body">
        <?php if (!empty($datanote)){ ?>
            <div class="row">
                <div class="col-md-6">
                    <i class="fa fa-user" aria-hidden="true"></i> 
                    ผู้บันทึกโน๊ตล่าสุด : <code><?php  $user = common\models\UserProfile::findOne($datanote['user_update']); echo $user['firstname']." ".$user['lastname'] ?></code>
                </div>
                <div class="col-md-6">
                    <i class="fa fa-calendar" aria-hidden="true"></i> 
                    วันที่บันทึกล่าสุด : <code><?php  echo SDdate::mysql2phpThDateSmall($datanote['update_date']); ?></code>

                </div>
            </div>
            </br>
            <div class="row">
                <div class="col-md-12 ">
                    <div class="well"><?= $datanote['comment'] ?></div>
                </div>
            </div>
        <?php } else{ echo "<div class='alert alert-warning' style='text-align:center;'><strong> ไม่พบข้อมูล </strong></div>";} ?>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-danger pull-right"  data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i> ปิด</button>
        </div>
    </div>
</div>