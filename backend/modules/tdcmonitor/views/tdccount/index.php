<?php
use yii\helpers\Html;
use yii\helpers\Url;
use kartik\grid\GridView;
use appxq\sdii\widgets\ModalForm;
use common\lib\sdii\components\utils\SDdate;
use appxq\sdii\helpers\SDNoty;
use appxq\sdii\helpers\SDHtml;

$this->title = 'TDC_MONITOR';
if (Yii::$app->user->can('administrator')) {
?>
<div class="row">
    <div class="col-md-offset-6 col-md-6 ">
        <div class="input-group">
            <input type="text" name="search" id="txt-search" placeholder="ค้นหาข้อมูลโดยกรอก รหัสหน่วยงาน หรือ ชื่อหน่วยงาน" class="form-control">
            <span class="input-group-btn">
                <button class="btn btn-primary" id="Search" type="submit"><i class="glyphicon glyphicon-search"></i> ค้นหา</button>
             </span>
        </div>
    </div>
</div>
<hr>
<div class="row" id="tdcmonitor">
    <div class="col-md-12">
        <div id="data_tdc"></div>
    </div>
</div>
<?php }else{
    echo "<div class='alert alert-warning' style='text-align:center;'><strong> คุณไม่มีสิทธิ์เข้าใช้งานในส่วน TDC Monitor </strong></div>";
} ?>
<?php 
    echo  \appxq\sdii\widgets\ModalForm::widget([
        'id'=> 'modal-detail',
        'size' => 'modal-lg', 
    ]);

    $this->registerJS("
//        setInterval(function (){ //show data update ทุก 10 นาที
//            $.ajax({
//                method: 'POST',
//                url: '".Url::to(['/tdcmonitor/tdccount/index'])."',
//                dataType: 'HTML',
//                success: function(result, textStatus) {
//                location.reload();                    
//                $('#tdcmonitor').reload(true);
//                }
//        count_grid('');
//        },3000);
        
        count_grid('');
        $('#txt-search').keyup(function(){
            setTimeout(function(){
                var search = $('#txt-search').val();
                count_grid(search);
                return false;
            }, 500);
        });
        $('#Search').click(function(){
            var search = $('#txt-search').val();
            count_grid(search);
            return false;
        });
        
        function count_grid(search){
            $.ajax({
                url:'".Url::to(['/tdcmonitor/tdccount/monitor'])."',
                method:'GET',
                data:{search:search},
                success:function(data){
                    $('#data_tdc').html(data);
                }
            })
        }

    ");   
    
?>