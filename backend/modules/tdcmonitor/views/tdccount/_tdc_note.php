<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use appxq\sdii\helpers\SDNoty;
$sitecode = \backend\modules\ezforms\components\EzformQuery::getHospital($sitecode);
//\appxq\sdii\utils\VarDumper::dump($data['sitecode']);
//echo $data['sitecode']." / ".$data['comment'];
?>
<div class="Note-form">
    <form id="addnote">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title"><?= Html::tag('label','หน่วยบริการ ('.$sitecode['hcode'] . " : " . $sitecode['name']. " )" ); ?></h4>
    </div>

    <div class="modal-body"> 
        <div class="row">
            <div class="col-md-12">
                
                <?php
                    echo Html::hiddenInput('sitecode', $sitecode['hcode']);
                    echo dosamigos\ckeditor\CKEditor::widget([
                        'name' => 'tdcnote_text',
                        'preset' => 'normal',
                        'value' => $comment,
                    ]);
                ?>
            </div>  
        </div>
    </div>
    <div class="modal-footer">
        <?= Html::submitButton('เพิ่มโน๊ต', ['class' => 'btn btn-success','id'=>'btn-submit']) ?>
        <button type="button" class="btn btn-danger pull-right"  data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i> ปิด</button>
    </div>
    </form>
</div>
<?php
$this->registerJs("
    $('#addnote').on('submit',function(e){
         e.preventDefault();
         $.ajax({
            url:'".Url::to('/tdcmonitor/tdccount/create-note')."',
            method:'POST',
            type:'HTML',
            data:new FormData(this),
            contentType: false,
            processData: false,
            success:function(result){
                //console.log(result.message);
                $('#modal-detail').modal('hide');
                ". SDNoty::show('result.message', 'result.status') ."
                
            }
            
        });
    });
   
  ");
?>