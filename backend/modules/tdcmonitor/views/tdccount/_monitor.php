<?php
use yii\helpers\Html;
use yii\helpers\Url;
use common\lib\sdii\components\utils\SDdate;
//appxq\sdii\utils\VarDumper::dump($online);
?>
<div class="panel panel-default">
    <div class="panel-heading">
        <label>หน่วยงานที่ติดตั้ง TDC </label>
        <div class="pull-right">
            <label style='color:green'> เปิด :: <span class="badge" style='background-color:green'> <?php echo $online == NULL ? 0 : $online; ?> </span></label>
            &nbsp;&nbsp;
            <label style='color:red'> ปิด :: <span class="badge" style='background-color:red'> <?php echo $offline == NULL ? 0 : $offline; ?> </span></label>
        </div>
    </div>
    <div class="panel-body">
        <div class="col-md-12">
            <table class="table tab-bordered table-hover">
                <thead>
                    <tr>
                        <th width='3%'>#</th>
                        <th width='33%'>หน่วยงาน</th>
                        <th width='14%'>สถานะ</th>
                        <th width='5%'>HIS</th>
                        <th width='12%'>จำนวนข้อมูลใน รพ.</th>
                        <th width='9%'>จำนวนที่นำเข้า</th>
                        <th width='10%'>จำนวนคิวที่เหลือ</th>
                        <th width='8%'>Progress</th>
                        <th width='8%'>Note</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $i=1; foreach ($tdc_count as $val ){  ?>
                    <tr class="title" > 
                        <?php //Html::hiddenInput('sitecode', $val['id'],['id'=> $val['id'] ]);?>
                        <?php //echo Html::hiddenInput('id', $i);?>
                        <td><?=$i;?></td>
                        <td style="cursor:pointer">
                            <?php $sitecode = \backend\modules\ezforms\components\EzformQuery::getHospital($val['id']); 
                            //echo $sitecode['hcode']." ".$sitecode['name'] 
                            echo Html::a($sitecode['hcode']." ".$sitecode['name'] ,'#',[
                                    'id' => 'detail',
                                    'class' => 'model-detail-site ',
                                    'data-url' => Url::to([
                                        '/tdcmonitor/tdccount/tdc-detail',
                                        'sitecode'=>$val['id'],
                                    ]),
                                    'title' => 'Detail'
                                ]); 
                            ?>
                        </td>
                        <td>
                            <?php IF($val['status'] == '1'){
                                    echo "<i class='fa fa-circle fa-lg' aria-hidden='true' style='color:green'></i> ";
                                    echo SDdate::mysql2phpThDateSmall($val['last_ping']);
                                    echo substr($val['last_ping'],10);
                                }ELSE IF($val['status'] == '0'){
                                    echo "<i class='fa fa-circle fa-lg' aria-hidden='true' style='color:red'></i> ";
                                    echo SDdate::mysql2phpThDateSmall($val['last_ping']);
                                    echo substr($val['last_ping'],10);
                                }
                            ?>
                        </td>
                        <td><?=$val['his_name'];?></td>
                        <td class="text-right"><?=number_format($val['his_record']);?></td>
                        <td class="text-right"><?=number_format($val['record']);?></td>
                        <td class="text-right"><?=number_format($val['qleft']);?></td>
                        <?php IF ($val['progress'] > 0){ ?>
                        <td>
                            <div class="progress">
                                <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="<?=$val['progress'];?>"
                                aria-valuemin="0" aria-valuemax="100" style="width:<?=$val['progress'];?>%;min-width: 2em;">
                                  <?=$val['progress'];?> %
                                </div>
                            </div>
                        </td>
                        <?php }else{
                            echo "<td></td>";
                        }?>
                        <td align="center">
                        <?php
                            echo Html::a('<i class="fa fa-pencil-square-o" aria-hidden="true"></i>','#',[
                                    'id' => 'modal-view',
                                    'class' => 'tdc_note modal-note btn btn-primary btn-xs',
                                    'data-url' => Url::to([
                                        '/tdcmonitor/tdccount/tdc-note',
                                        'sitecode'=>$val['id'],
                                    ]),
                                    'title' => 'Add Note'
                                ]); 
                            echo " ";
                            echo Html::a('<i class="fa fa-eye" aria-hidden="true"></i>','#',[
                                    'id' => 'modal-view',
                                    'class' => 'tdc_note modal-note btn btn-primary btn-xs',
                                    'data-url' => Url::to([
                                        '/tdcmonitor/tdccount/view-note',
                                        'sitecode'=>$val['id'],
                                    ]),
                                    'title' => 'View Note'
                                ]); 

                        ?>
                        </td>
                    </tr>
                    <?php $i++; } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<?php 
    $this->registerJS(" 
        
        $('.model-detail-site').click(function(){
            modalShow($(this).attr('data-url'));
        });
        
        $('.modal-note').click(function() {
            modalShow($(this).attr('data-url'));
        });

        function modalShow(url) {
            $('#modal-detail .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
            $('#modal-detail').modal('show')
            .find('.modal-content')
            .load(url);
        }
        ");
?>