<?php

namespace backend\modules\tdcmonitor;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\modules\tdcmonitor\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
