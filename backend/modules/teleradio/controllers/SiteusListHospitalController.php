<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SiteusListHospitalController
 *
 * @author chaiwat
 */

namespace backend\modules\teleradio\controllers;

use yii\helpers\VarDumper;
use yii\web\Controller;
use Yii;

class SiteusListHospitalController extends \yii\web\Controller {
    //put your code here
    public function getList() {
        $sql = "select * from history_us_site order by dateatsite ";
        $out = Yii::$app->db->createCommand($sql)->queryAll();
        return $out;
    }
    
    
    public function getListUsAtSite(){
        $cca02_ezformid='1437619524091524800';
        
        $sql = "select cca02.hsitecode ";
        $sql.= ",count(distinct cca02.ptid) as 'patient' ";
        $sql.= ",count(distinct cca02.ptid,cca02.f2v1) as 'dvisit' ";
        $sql.= ",count(distinct if(cca02.f2v6a3b1='1',cca02.ptid,NULL)) as 'patient_sus' ";
        $sql.= ",count(distinct if(cca02.f2v6a3b1='1',concat(cca02.ptid,'-',cca02.f2v1),NULL)) as 'dvisit_sus' ";
        $sql.= ",count(distinct if(cca02.f2v2a1b2='1' or cca02.f2v2a1b2='2' or cca02.f2v2a1b2='3',cca02.ptid,NULL)) as 'patient_pdf' ";
        $sql.= ",count(distinct if(cca02.f2v2a1b2='1' or cca02.f2v2a1b2='2' or cca02.f2v2a1b2='3',concat(cca02.ptid,'-',cca02.f2v1),NULL)) as 'dvisit_pdf' ";
        $sql.= ",count(distinct if(ezform_reply.type='2',cca02.ptid,NULL)) as 'patient_consult' ";
        $sql.= ",count(distinct if(ezform_reply.type='2',concat(cca02.ptid,'-',cca02.f2v1),NULL)) as 'dvisit_consult' ";
        $sql.= ",count(distinct if(ezform_reply.type='3',cca02.ptid,NULL)) as 'patient_sos' ";
        $sql.= ",count(distinct if(ezform_reply.type='3',concat(cca02.ptid,'-',cca02.f2v1),NULL)) as 'divist_sos' ";
        $sql.= "from tb_data_3 cca02 ";
        $sql.= "inner join history_us_site ussite on ussite.hcode=cca02.hsitecode ";
        $sql.= "left join ezform_reply on ezform_reply.data_id=cca02.id and ezform_reply.`ezf_id`='".$cca02_ezformid."' ";
        $sql.= "where cca02.rstat<>'3' ";
        $sql.= "and length(ussite.hcode)>0 ";
        $sql.= "and cca02.f2v1>=ussite.dateatsite ";
        $sql.= "group by cca02.hsitecode ";
        $sql.= "order by ussite.dateatsite ";
        //echo $sql;
        $out = Yii::$app->db->createCommand($sql)->queryAll();
        if( count($out)>0 ){
            foreach ($out as $k => $v){
                $list[$v[hsitecode]]=$v;
            }
        }
        return $list;
    }
    
    
}
