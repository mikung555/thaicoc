<?php

namespace backend\modules\teleradio\controllers;

use Yii;
use backend\modules\ezforms\models\EzformReply;
use backend\modules\teleradio\models\TeleRadioDiag;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\db\Expression;
use yii\helpers\Url;
use yii\web\Controller;

class DefaultController extends Controller
{
    public function actionIndex()
    {
        if($_GET['type']=='all')
            $type = "type<> 9";
        else if($_GET['type']=='')
            $type ="type =2";
        else
            $type = "type = ".$_GET['type'];
        
        if($_GET['ezf_id'] == ''){
            $type .= " AND ezf_id = '1437619524091524800'";
            $ezf_id = "1437619524091524800";
        }else{
            $type .= " AND ezf_id = ".$_GET['ezf_id'];
            $ezf_id = $_GET['ezf_id'];
        }
        $dataProvider = new ActiveDataProvider([
            'query' => EzformReply::find()->select('id, ezf_id, data_id, ezf_comment, file_upload, user_create, create_date, update_date, xsourcex, rstat, type')->where($type)->groupBy('data_id')->orderBy('create_date DESC'),
            'pagination' => [
                'pageSize' => 50,
            ],
        ]);
        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'ezf_id' => $ezf_id
            
        ]);
    }

    public function actionConsultView($id){
        $model = EzformReply::find()->where('id=:id', [':id'=>$id])->one();
        //->xsourcex
        $sql="SELECT hcode, name, tambon, amphur, province FROM all_hospital_thai WHERE hcode=:hcode";
        $hosp = \Yii::$app->db->createCommand($sql,[':hcode' => $model->xsourcex])->queryOne();
        $model->xsourcex  = $hosp['hcode'].' : '.$hosp['name'].' '.$hosp['province'];
        //
        $date = new \DateTime($model->create_date);
        $model->create_date = $date->format('d/m/Y');
        //
        if($model->type == 1)
            $model->type = 'แสงความคิดเห็นทั่วไป';
        else if($model->type == 2)
            $model->type = 'ขอคำปรึกษา';
        else if($model->type == 3)
            $model->type = 'SOS';
        //
        $model->ezf_comment = mb_substr($model->ezf_comment, 0,50,'UTF-8').'...';
        //
        $sql="SELECT ezf_name FROM ezform WHERE ezf_id=:ezf_id";
        $hosp = \Yii::$app->db->createCommand($sql,[':ezf_id' => $model->ezf_id])->queryOne();
        $model->ezf_id = $hosp['ezf_name'];
        //
        $modelTeleList = TeleRadioDiag::find()->where(['ref_consult_id' => $id]);
        $modelTele = new TeleRadioDiag();

        return $this->render('consult-view', [
            'model' => $model,
            'modelTele' => $modelTele,
            'modelTeleList' => $modelTeleList,
        ]);
    }

    public function actionDiagReply()
    {
        $model = new TeleRadioDiag();
        if($model->load(Yii::$app->request->post())){
            $model->user_create = Yii::$app->user->id;
            $model->create_date = new Expression('NOW()');
            $model->xsourcex = Yii::$app->user->identity->userProfile->sitecode;
            if($model->save()){
                return $this->redirect(Url::to(Yii::$app->request->referrer));
            }
        }
    }

    public function renderDiagReplyList($id)
    {
        $modelReplyData = new ActiveDataProvider([
            'query' => TeleRadioDiag::find()->where(['ref_consult_id' => $id])->orderBy('create_date DESC'),
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        $view = Yii::$app->getView();
        return $view->render('@backend/modules/teleradio/views/default/diag_reply', ['modelReplyData'=>$modelReplyData]);
    }
    
    public function replyUser($ezf_id,$data_id){ //id,ezf_id,data_id,user_create
        return EzformReply::find()->select('user_create,create_date')->where('ezf_id = :ezf_id AND data_id = :data_id', [':ezf_id' => $ezf_id, ':data_id' => $data_id])->orderBy('create_date DESC')->one();
    }

    
    public function actionReportTeleradio(){
         $dataGet = \Yii::$app->request->get();
        
            //\appxq\sdii\utils\VarDumper::dump($dataHos);
            return $this->renderAjax('report-teleradio',[
                'id' => $dataGet['id'],
                'ezf_id' => $dataGet['ezf_id'],
                'type' => $dataGet['type'],
            ]);
        
    }
    
    public function actionReportReply(){
       
        $dataGet = \Yii::$app->request->get();
        
        if($dataGet['ezf_id'] == '' && $dataGet['hcode'] == ''){
            $where_ezf_id = " ezf_id = 1437619524091524800";
            $ezf_id = "1437619524091524800";
        }else{
            
            $where_ezf_id  = "  ezf_id = ".$dataGet['ezf_id'];
            $where_ezf_id .= "  AND ezform_reply.xsourcex = '".$dataGet['hcode']."' ";
            $ezf_id = $dataGet['ezf_id'];
            if($dataGet['type_grid'] == 'all' ){
                $where_ezf_id .= "AND (type<> 9 AND type<>0)";
                $title = "รายการทั้งหมด";
            }else if($dataGet['type_grid'] =='1'){
                $where_ezf_id .= "AND type = 1";
            }else if($dataGet['type_grid'] =='2'){
                $where_ezf_id .= "AND (type = 2 OR type =3 )";
            }else if($dataGet['type_grid'] == 'today'){
                $where_ezf_id .= " AND (type <> 9 AND type != 0) AND DATE(ezform_reply.create_date)=CURDATE()";
            }else if($dataGet['type_grid'] == 'thisWeek'){
                $where_ezf_id .= " AND (type <> 9 AND type != 0) AND YEARWEEK(ezform_reply.create_date) = YEARWEEK(NOW())";
            }else if($dataGet['type_grid'] == 'thisMonth'){
                $where_ezf_id .= " AND (type <> 9 AND type != 0) AND YEAR(ezform_reply.create_date)=YEAR(CURDATE()) AND MONTH(create_date)=MONTH(CURDATE())";
            }else if($dataGet['type_grid'] == 'thisYear'){
                $where_ezf_id .= " AND (type <> 9 AND type != 0 ) AND YEAR(ezform_reply.create_date)=YEAR(CURDATE())";
            }
        }
//        \appxq\sdii\utils\VarDumper::dump($where_ezf_id);
        
    
        $sitecode = \backend\modules\ezforms\components\EzformQuery::getHospital($dataGet['hcode']);
        //$sql_ezform = \Yii::$app->db->createCommand("SELECT ezf_id,ezf_table FROM ezform WHERE ezf_id = :ezf_id")->bindValues([':ezf_id' => $dataGet['ezf_id']])->queryOne();
        
        if($dataGet['drilldown'] == 1){
            $sql_data = \Yii::$app->db->createCommand("SELECT id,data_id,xsourcex,ezf_id,ezf_comment
                FROM ezform_reply WHERE $where_ezf_id GROUP BY data_id ORDER BY create_date DESC")->queryAll();
        
                foreach ($sql_data as $val) {
                    if ($data_id == null) {
                        $data_id .= "'" . $val['data_id'] . "'";
                    } else {
                        $data_id .= ",'" . $val['data_id'] . "'";
                    }
                }
//                \appxq\sdii\utils\VarDumper::dump($data_id); //'\'1467040362070225200\',\'1466733125840\',\'1467337493049110100\',\'1467338010055663600\',\'1467338743073749100\',\'1467338315073362100\',\'1467036856087340900\',\'1467374068091029200\'
             
                if($data_id != '' || $data != NULL){
                    $count = Yii::$app->db->createCommand('SELECT COUNT(DISTINCT data_id) FROM ezform_reply WHERE '.$where_ezf_id.' ')->queryScalar();
                    $tbdata = new \yii\data\SqlDataProvider([
                        'sql' => 'SELECT *,SUM(IF((type =1 OR type = 2 OR type = 3) ,1,0)) as countuserjoin
                        FROM ezform_reply WHERE  ezf_id = :ezf_id AND data_id IN ('.$data_id.') GROUP BY data_id ORDER BY create_date DESC',
                        'params' => [':ezf_id' =>$dataGet['ezf_id']],
                        'totalCount' => $count,
                        'pagination' => [
                            'pageSize' => 20,
                        ],
                    ]);
                    

                } 

        }  

//        \appxq\sdii\utils\VarDumper::dump($tbdata);
       return $this->renderAjax('_report_reply',[
           'tbdata' => $tbdata,
           'title_gridview' => $title,
           'sitecode' => $sitecode,
           'ezf_id' => $ezf_id,
           'type_grid' => $dataGet['type_grid'],
           
           
        ]);
    }
    public function getTbdataEzformReply($data_id,$ezf_id){
          //\appxq\sdii\utils\VarDumper::dump($ezf_id);
        $sql_ezform = \Yii::$app->db->createCommand("SELECT ezf_id,ezf_table FROM ezform WHERE ezf_id = :ezf_id")->bindValues([':ezf_id' => $ezf_id])->queryOne();
        $table_tbdata = $sql_ezform['ezf_table'];
        $sqltb_data = \Yii::$app->db->createCommand("SELECT `id`,xsourcex,hsitecode,hptcode,f2v1 FROM $table_tbdata WHERE id IN (:data_id)",[':data_id' => $data_id])->queryOne();
         //\appxq\sdii\utils\VarDumper::dump($sqltb_data['hsitecode']);
        $tbdata_hsitecode =  \backend\modules\ezforms\components\EzformQuery::getHospital($sqltb_data['hsitecode']);

//\appxq\sdii\utils\VarDumper::dump($sqltb_data);
        return $tbdata_hsitecode;
    }
    public function actionReportReplyDrilldown(){
        
        $dataGet = \Yii::$app->request->get();
        $hcode = $dataGet['hcode'];
        $ezf_id = $dataGet['ezf_id'];
        $data_id = $dataGet['data_id'];
        $type_grid = $dataGet['type_grid'];
//         \appxq\sdii\utils\VarDumper::dump($data_id);
        
//        $sql_driildown = "SELECT id, ezf_id, data_id, ezf_comment, file_upload, user_create, create_date, update_date, xsourcex, rstat, type
//                FROM ezform_reply WHERE  ezf_id = :ezf_id AND data_id = :data_id AND (type <>9 AND type <> 0) ORDER BY create_date ASC ";
        $sql_driildown = "SELECT id, ezf_id, data_id,COUNT(`user_create`) as 'amount',user_create FROM ezform_reply WHERE  ezf_id = :ezf_id AND data_id = :data_id  "
                 . "AND (type <>9 AND type <> 0) GROUP BY `user_create` ORDER BY create_date ASC ";
        $query_driildown = \Yii::$app->db->createCommand($sql_driildown,[':ezf_id' => $dataGet['ezf_id'],':data_id' => $dataGet['data_id'] ])->queryAll();
        //\appxq\sdii\utils\VarDumper::dump($query_driildown);
        
        $data_drilldown = new \yii\data\ArrayDataProvider([
            'allModels' => $query_driildown,
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);
//        \appxq\sdii\utils\VarDumper::dump($data_drilldown);
        return $this->renderAjax('_report_reply_drilldown',[
            'data_drilldown' => $data_drilldown,
            'ezf_id'=> $ezf_id,
            'data_id' => $data_id,
            'hcode' => $hcode,
            'type_grid' => $type_grid,
        ]);
    }


    public function getCountReply($hcode,$ezf_id,$type){ //ตัด data_id ที่ซ้ำออก
    
        // function DISTINCT data_id
        if($type == 1){
            $type = "type = 1";
        }else if($type == 2) {
            $type = "(type = 2 OR type = 3)";
        }else if($type == 0) {
            $type = "type = 0";
        }
        $sqlCount = "SELECT COUNT(DISTINCT data_id) as Count FROM ezform_reply WHERE $type AND ezf_id = $ezf_id AND xsourcex = :xsourcex #GROUP BY data_id";
        $queryCount = \Yii::$app->db->createCommand($sqlCount,[':xsourcex' => $hcode])->queryOne();
       
        
       return $queryCount['Count'];
    }
    
    public function actionReportUser(){
       
        if (Yii::$app->request->isAjax) {
            $dataGet = \Yii::$app->request->get();
            
           // \appxq\sdii\utils\VarDumper::dump($dataPost);
            if($dataGet['id'] == '1'){
                $type = "type = 1";
                $title = "รายการความคิดเห็นทั้งหมด";
            }else if($dataGet['id'] == '2'){
                $type = "type = 2";
                $title = "รายการขอคำปรึกษาทั้งหมด";
            }else if($dataGet['id'] == '3'){
                $type = "type = 3";
                $title = "รายการ SOS ทั้งหมด";
            }
            
            if($dataGet['ezf_id'] == ''){
                $type .= " AND ezf_id = '1437619524091524800'";
                $where_ezf_id = " AND ezf_id = 1437619524091524800";
                $ezf_id = "1437619524091524800";
            }else{
                $type .= " AND ezf_id = ".$dataGet['ezf_id'];
                $where_ezf_id = " AND ezf_id = ".$dataGet['ezf_id'];
                $ezf_id = $dataGet['ezf_id'];
            }

            $sqlReport = "SELECT data_id,user_create as 'User',COUNT(*) AS replyAll
                    ,SUM(DATE(create_date)=CURDATE()) as today
                    ,SUM(YEARWEEK(`create_date`) = YEARWEEK(NOW())) as thisWeek
                    ,SUM(YEAR(create_date)=YEAR(CURDATE()) AND MONTH(create_date)=MONTH(CURDATE())) as thisMonth
                    ,SUM(YEAR(create_date)=YEAR(CURDATE())) as thisYear 
                    FROM ezform_reply 
                    WHERE $type
                    GROUP BY user_create 
                    ORDER BY replyAll DESC ";
            $query = \Yii::$app->db->createCommand($sqlReport)->queryAll();

            $dataProvider = new \yii\data\ArrayDataProvider([
                'allModels' => $query,
                'pagination' => [
                    'pageSize' => 20,
                ],
            ]);
//            
           
//        \appxq\sdii\utils\VarDumper::dump($dataProvider);
            return $this->renderAjax('_report_reply_user',[
                'dataProvider' => $dataProvider,
                'title_gridview' => $title,
                'ezf_id' => $dataGet['ezf_id'],
                'id' => $dataGet['id'],
                'type'=> $dataGet['type'],
            ]);
        }  else {
            return $this->redirect(['index']);
        }
    }
    
    public function actionReportHospital(){
        $dataGet = \Yii::$app->request->get();
        if (Yii::$app->request->isAjax) {
            
            if($dataGet['id'] == '1'){
                $type = "type = 1";
                $title = "รายการความคิดเห็นทั้งหมด";
            }else if($dataGet['id'] == '2'){
                $type = "type = 2";
                $title = "รายการขอคำปรึกษาทั้งหมด";
            }else if($dataGet['id'] == '3'){
                $type = "type = 3";
                $title = "รายการ SOS ทั้งหมด";
            }
            
            if($dataGet['ezf_id'] == ''){
                $where_ezf_id = " AND ezf_id = 1437619524091524800";
                $ezf_id = "437619524091524800";
            }else{
                $where_ezf_id = " AND ezf_id = ".$dataGet['ezf_id'];
                $ezf_id = $dataGet['ezf_id'];
            }         
                  
            $sqlReply = \Yii::$app->db->createCommand("SELECT * FROM ezform_reply_count")->queryAll();
            
          
            $dataAll =  \appxq\sdii\utils\SortArray::sortingArrayDESC($sqlReply, 'Reply_type1');
            $dataHos = new \yii\data\ArrayDataProvider([
                'allModels' => $dataAll,
                'pagination' => [
                    'pageSize' => 50,
                ],
            ]); 
          
//        \appxq\sdii\utils\VarDumper::dump($dataHos);
            return $this->renderAjax('_report_reply_hospital',[
                'dataHos' => $dataHos,
                'ezf_id' => $dataGet['ezf_id'],
                'id' => $dataGet['id'],
                'type'=> $dataGet['type'],
            ]);
        }  else {
            return $this->redirect(['index']);
        }
    }
    
    public function actionReportRefresh(){
        $dataGet = \Yii::$app->request->get();
        
        $sql = \Yii::$app->db->createCommand("CALL ezf_reply_count(:ezf_id)")->bindValues([':ezf_id' => $dataGet['ezf_id']])->execute();
        if($sql){
            $data = self::actionReportHospital($dataGet);
        }
        return $data;
        
    }
    
    

}
