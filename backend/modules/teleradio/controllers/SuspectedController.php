<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace backend\modules\teleradio\controllers;

use yii\data\SqlDataProvider;
use yii\data\ActiveDataProvider;
//use yii\helpers\VarDumper;
//use yii\web\Controller;
use backend\modules\teleradio\classes\QueryUs;
use backend\modules\teleradio\classes\QueryUsSum;
use backend\modules\teleradio\classes\QueryTableDet;
use backend\modules\teleradio\classes\SummarySuspected;
use backend\modules\teleradio\classes\QuerySuspected;
use backend\modules\teleradio\classes\QueryData;
use backend\modules\teleradio\classes\FuncHospital;
use backend\modules\teleradio\classes\QueryTableReportUpdate;
use app\modules\teleradio\models\SuspectedCca;
use Yii;

class SuspectedController extends \yii\web\Controller {
    
    public function actionIndex()
    { 
        $tabdet = QueryTableDet::getDetail('cascapcloud', 'tb_data_3_suspected');
        $overAll = SummarySuspected::getOverAll();
        // manage data provider
        $dataProvider = new ActiveDataProvider([
              'query' => SuspectedCca::find(),
              'pagination' => [
                  'pageSize' => 20,
              ],
              'sort'=> ['defaultOrder' => ['f2v1'=>SORT_DESC]]
          ]);
        return $this->render('index',[
            'dataProvider' => $dataProvider,
            'tabdet' => $tabdet,
            'overAll' => $overAll,
        ]);
    }
    
    public function actionUsfsum() 
    {
        $checkuser = QueryUs::checkUserview();
        if ($checkuser['view'] == TRUE) {
            $find = QueryUsSum::getUssum();
            $dataProvider = new \yii\data\ArrayDataProvider([
                'allModels' => $find,
                'sort' => [
                    'attributes' => [
                        'usmobile',
                        'dusfind',
                        'count',
                        'abnormal',
                        'PDF',
                        'suspected',
                        'Meet1',
                        'Meet2',
                        'Send',
                    ],
                ],
                'pagination' =>
                [
                    'pageSize' => 10
                ],
            ]);
            $countAllRecord = count($find);
            $getLastpage = \Yii::$app->request->get('page');
            $getPerpage = \Yii::$app->request->get('per-page');
            if ($getPerpage < 1) {
                $getPerpage = 10;
            }
            $pageMaxpage = ceil($countAllRecord / $getPerpage);
            $dataArray = $find;
            return $this->render('usfsum', [
                        'checkuser' => $checkuser,
                        'dataProvider' => $dataProvider,
                        'dataArray' => $dataArray,
                        'getLastpage' => $getLastpage,
                        'getCurrentpage' => $getLastpage,
                        'getPerpage' => $getPerpage,
                        'modelAllrow' => $countAllRecord,
                        'pageMaxpage' => $pageMaxpage,
            ]);
        } else {
            return $this->render('usfsum', [
                        'checkuser' => $checkuser,
            ]);
        }
    }

    public function actionUsdoc()
    {
        $checkuser = QueryUs::checkUserview();

        if($checkuser['view']==TRUE){
            if($checkuser['pid']!=FALSE and \Yii::$app->user->can('administrator')==FALSE){
                $find = QueryUs::getUsonedocter($checkuser['user_id']);
            }else{
                $find = QueryUs::getUsdocter();
            }
            $dataProvider = new \yii\data\ArrayDataProvider([
                'allModels' => $find,
                'sort' => [
                    'attributes' => [
                        'doctorfullname',
                        'allsubmit',
                        'doctordate',
                        'count',
                        'suspected',
                        'abnormal',
                        'LiverMass',
                        'Gallbladder',
                        'Kidney',
                        'Meet1',
                        'Meet2',
                        'Send',
                    ],
                ],
                'pagination' =>
                    [
                     'pageSize' => 20
                    ],
            ]);
            $countAllRecord = count($find);
            $getLastpage = \Yii::$app->request->get('page');
            $getPerpage = \Yii::$app->request->get('per-page');
            if ($getPerpage < 1) {
                $getPerpage = 20;
            }
            $pageMaxpage = ceil($countAllRecord / $getPerpage);
            $dataArray=$find;
            return $this->render('usdoc',[
                'checkuser'=> $checkuser,
                'dataProvider'=> $dataProvider,
                'dataArray' => $dataArray,
                'getLastpage' => $getLastpage,
                'getCurrentpage' => $getLastpage,
                'getPerpage' => $getPerpage,
                'modelAllrow' => $countAllRecord,
                'pageMaxpage' => $pageMaxpage,
            ]);
        }else{
            return $this->render('usdoc',[
                'checkuser'=> $checkuser,
            ]);
        }
        
    }
    public function actionDilldownsubmit() {
        $dataGet = \Yii::$app->request->get();
        $dataQuery = QueryUs::getSubmit($dataGet[code]);
        $num = 0;
        foreach ($dataQuery as $key => $value ) {
            if($key=="TotalAll"){
                $name="All";
            }else{
                $name=$key;
            }
            $dataChart[$num][] = $name;
            $dataChart[$num][] = intval($value);
            $num++;
            $sumtotal+=$value;
        }
        return $this->renderAjax('dilldown_doctor',[
            'dataGet' => $dataGet,
            'dataChart' => $dataChart,
            'dataQuery' => $dataQuery,
            'sumtotal' => $sumtotal,
        ]);
    }
    public function actionDilldownabnormal() {
        $dataGet = \Yii::$app->request->get();
        $dataQuery1 = QueryUs::getNormal($dataGet[code]);
        $dataQuery2 = QueryUs::getAbnormal($dataGet[code]);
        $dataPdf = QueryUs::getPdf($dataGet[code]);
        $dataLiver = QueryUs::getFattyliver($dataGet[code]);
        $num = 0;
        foreach ($dataQuery1 as $key => $value ) {
            $dataChart1[$num][] = $key;
            $dataChart1[$num][] = intval($value);
            $num++;
            $sumQuery1+=$value;
        }
        $num = 0;
        foreach ($dataQuery2 as $key => $value ) {
            $dataName2[$num][] = $key;
            $dataChart2[$num][] = intval($value);
            $num++;
            $sumQuery2+=$value;
        }
        $num = 0;
        foreach ($dataPdf as $key => $value ) {
            $dataNamepdf[$num][] = $key;
            $dataChartpdf[$num][] = intval($value);
            $num++;
        }
        $num = 0;
        foreach ($dataLiver as $key => $value ) {
            $dataNameLiver[$num][] = $key;
            $dataChartLiver[$num][] = intval($value);
            $num++;
        }
        return $this->renderAjax('dilldown_doctor',[
            'dataGet' => $dataGet,
            'dataQuery1' => $dataQuery1,
            'sumQuery1' => $sumQuery1,
            'dataQuery2' => $dataQuery2,
            'dataName2' => $dataName2,
            'sumQuery2' => $sumQuery2,
            'dataPdf' => $dataPdf,
            'dataNamepdf' => $dataNamepdf,
            'dataLiver' => $dataLiver,
            'dataChart1' => $dataChart1,
            'dataChart2' => $dataChart2,
            'dataChartpdf' => $dataChartpdf,
            'dataNamepdf' => $dataNamepdf,
            'dataChartLiver' => $dataChartLiver,
            'dataNameLiver' => $dataNameLiver,
        ]);
    }
    public function actionDilldownliver() {
        $dataGet = \Yii::$app->request->get();
        $dataQuery1 = QueryUs::getLivermass($dataGet[code]);
        $dataQuery2 = QueryUs::getMutiMass($dataGet[code]);
        $num = 0;
        foreach ($dataQuery1 as $key => $value ) {
            $dataChart1[$num][] = $key;
            $dataChart1[$num][] = intval($value);
            $num++;
            $sumtotal+=$value;
        }
        $num = 0;
        foreach ($dataQuery2 as $key => $value ) {
            if($key=="mass1"){
                $name="Liver cyst";
            }else if($key=="mass2"){
                $name="Hemangioma";
            }else if($key=="mass3"){
                $name="Calcification";
            }else if($key=="mass4"){
                $name="Intrahepatic duct stone";
            }else if($key=="mass5"){
                $name="High echo";
            }else if($key=="mass6"){
                $name="Low echo";
            }else if($key=="mass7"){
                $name="Mixed echo";
            }
            $nameChart[$num][] = $name;
            $dataChart2[$num][] = intval($value);
            $num++;
            $sumQuery2+=$value;
        }
        return $this->renderAjax('dilldown_doctor',[
            'dataGet' => $dataGet,
            'dataChart1' => $dataChart1,
            'dataChart2' => $dataChart2,
            'nameChart' => $nameChart,
            'dataQuery1' => $dataQuery1,
            'dataQuery2' => $dataQuery2,
            'sumtotal' => $sumtotal,
        ]);
    }
    public function actionDilldowngall() {
        $dataGet = \Yii::$app->request->get();
        $dataQuery = QueryUs::getGallbladder($dataGet[code]);
        $num = 0;
        foreach ($dataQuery as $key => $value ) {
            if($key=="gall1"){
                $name="Normal";
            }else if($key=="gall2"){
                $name="Wall";
            }else if($key=="gall3"){
                $name="Gallstone";
            }else if($key=="gall4"){
                $name="Post cholecystectomy";
            }
            $nameChart[$num][] = $name;
            $dataChart[$num][] = intval($value);
            $num++;
        }
        return $this->renderAjax('dilldown_doctor',[
            'dataGet' => $dataGet,
            'nameChart' => $nameChart,
            'dataChart' => $dataChart,
            'dataQuery' => $dataQuery,
        ]);
    }
    public function actionDilldownkidney() {
        $dataGet = \Yii::$app->request->get();
        $dataQuery = QueryUs::getKidey($dataGet[code]);

        $num = 0;
        foreach ($dataQuery as $key => $value ) {
            if($key=="kid1"){
                $name="Normal";
            }elseif($key=="kid2"){
                $name="Renal cyst";
            }else if($key=="kid3"){
                $name="Parenchymal change";
            }else if($key=="kid4"){
                $name="Renal stone";
            }else if($key=="kid5"){
                $name="Post nephrectomy";
            }
            $nameChart[$num][] = $name;
            $dataChart[$num][] = intval($value);
            $num++;
        }
        return $this->renderAjax('dilldown_doctor',[
            'dataGet' => $dataGet,
            'dataChart' => $dataChart,
            'nameChart' => $nameChart,
            'dataQuery' => $dataQuery,
        ]);
    }
    public function actionDilldownmeet() {
        $dataGet = \Yii::$app->request->get();
        $dataQuery = QueryUs::getMeetdoctor($dataGet[code]);
        
        $num = 0;
        foreach ($dataQuery as $key => $value ) {
            if($key=="Meet1"){
                $name="นัด 1 ปี (กรณีผลตรวจปกติ)";
            }elseif($key=="Meet2"){
                $name="นัด 6 เดือน (กรณีผลตรวจผิดปกติ)";
            }elseif($key=="suspected"){
                $name="Suspected CCA";
            }elseif($key=="other"){
                $name="สาเหตุอื่นๆ";
            }
            $dataChart[$num][] = $name;
            $dataChart[$num][] = intval($value);
            $num++;
            $sumtotal+=$value;
        }
        return $this->renderAjax('dilldown_doctor',[
            'dataGet' => $dataGet,
            'dataChart' => $dataChart,
            'dataQuery' => $dataQuery,
            'sumtotal' => $sumtotal,
        ]);
    }
    public function actionDilldownlist() {
        $dataGet = \Yii::$app->request->get();
//        if($dataGet['page2']!=""){
//            \appxq\sdii\utils\VarDumper::dump($dataGet);
//        }
        if($dataGet['item']=="Submit"){
            $dataSelect ="a.rstat in (2,4)";
        }elseif($dataGet['item']=="TotalAll"){
            $dataSelect ="a.rstat in (1,2,4)";
        }elseif($dataGet['item']=="Meet1"){
            $dataSelect ="a.f2v6 in (1) and f2v6a3b1<>1";
        }elseif($dataGet['item']=="Meet2"){
            $dataSelect ="a.f2v6 in (2) and f2v6a3b1<>1";
        }elseif($dataGet['item']=="suspected"){
            $dataSelect ="a.f2v6a3b1 = 1";
        }elseif($dataGet['item']=="other"){
            $dataSelect = "a.f2v6a3b2 in (1)";
        }elseif($dataGet['item']=="Normal"){
            $dataSelect = "a.f2v2a1 in (0)";
        }elseif($dataGet['item']=="Abnormal"){
            $dataSelect = "a.f2v2a1 in (1)";
        }elseif($dataGet['item']=="FattyLiver"){
            $dataSelect = "a.f2v2a1b1 in (1,2,3)";
        }elseif($dataGet['item']=="PDF"){
            $dataSelect = "a.f2v2a1b2 in (1,2,3)";
        }elseif($dataGet['item']=="Cirrhosis"){
            $dataSelect = "a.f2v2a1b3 in (1)";
        }elseif($dataGet['item']=="ParenchymalChange"){
            $dataSelect = "a.f2v2a1b4 in (1)";
        }elseif($dataGet['item']=="ParenchymalChange"){
            $dataSelect = "a.f2v2a1b4 in (1)";
        }elseif($dataGet['item']=="ParenchymalChange"){
            $dataSelect = "a.f2v2a1b4 in (1)";
        }elseif($dataGet['item']=="PDF1"){
            $dataSelect = "a.f2v2a1b2 in (1)";
        }elseif($dataGet['item']=="PDF2"){
            $dataSelect = "a.f2v2a1b2 in (2)";
        }elseif($dataGet['item']=="PDF3"){
            $dataSelect = "a.f2v2a1b2 in (3)";
        }elseif($dataGet['item']=="MildFattyLiver"){
            $dataSelect = "a.f2v2a1b1 in (1)";
        }elseif($dataGet['item']=="ModerateFattyLiver"){
            $dataSelect = "a.f2v2a1b1 in (2)";
        }elseif($dataGet['item']=="SevereFattyLiver"){
            $dataSelect = "a.f2v2a1b1 in (3)";
        }elseif($dataGet['item']=="SingleMass"){
            $dataSelect = "a.f2v2a2 in (1)";
        }elseif($dataGet['item']=="MultipleMass"){
            $dataSelect = "a.f2v2a2 in (2)";
        }elseif($dataGet['item']=="mass1"){
            $dataSelect = "a.f2v2a2b1c1 in (1)";
        }elseif($dataGet['item']=="mass2"){
            $dataSelect = "a.f2v2a2b2c1 in (1)";
        }elseif($dataGet['item']=="mass3"){
            $dataSelect = "a.f2v2a2b3c1 in (1)";
        }elseif($dataGet['item']=="mass4"){
            $dataSelect = "a.f2v2a2b4c1 in (1)";
        }elseif($dataGet['item']=="mass5"){
            $dataSelect = "a.f2v2a2b5c1 in (1)";
        }elseif($dataGet['item']=="mass6"){
            $dataSelect = "a.f2v2a2b6c1 in (1)";
        }elseif($dataGet['item']=="mass7"){
            $dataSelect = "a.f2v2a2b7c1 in (1)";
        }elseif($dataGet['item']=="gall1"){
            $dataSelect = "a.f2v3a1 in (1)";
        }elseif($dataGet['item']=="gall2"){
            $dataSelect = "a.f2v3a2 in (1)";
        }elseif($dataGet['item']=="gall3"){
            $dataSelect = "a.f2v3a3 in (1)";
        }elseif($dataGet['item']=="gall4"){
            $dataSelect = "a.f2v3a4 in (1)";
        }elseif($dataGet['item']=="gall5"){
            $dataSelect = "a.f2v3a5 in (1)";
        }elseif($dataGet['item']=="kid1"){
            $dataSelect = "a.f2v4a1 in (1)";
        }elseif($dataGet['item']=="kid2"){
            $dataSelect = "a.f2v4a2 in (1)";
        }elseif($dataGet['item']=="kid3"){
            $dataSelect = "a.f2v4a3 in (1)";
        }elseif($dataGet['item']=="kid4"){
            $dataSelect = "a.f2v4a4 in (1)";
        }elseif($dataGet['item']=="kid5"){
            $dataSelect = "a.f2v4a5 in (1)";
        }elseif($dataGet['item']=="kid6"){
            $dataSelect = "a.f2v4a6 in (1)";
        }
        $dataQuery = QueryUs::getListsubmodal($dataGet['doccode'],$dataSelect);
        $dataProvider = new \yii\data\ArrayDataProvider([
            'allModels' => $dataQuery,
            'pagination' =>
                [
                 'pageSize' => 50
                ],
            'sort' => [
                'attributes' => [
                    'hsitecode',
                    'hncode',
                    'cid',
                    'f2v1',
                    'hptcode',
                    'fullname',
                    'hsitecode',
                    'age',
                ],
            ],
        ]);
        $countAllRecord = count($dataQuery);
        $getLastpage = \Yii::$app->request->get('page');
        $getPerpage = \Yii::$app->request->get('per-page');
        if ($getPerpage < 1) {
            $getPerpage = 50;
        }
        $pageMaxpage = ceil($countAllRecord / $getPerpage);
        $sitecode = Yii::$app->user->identity->userProfile->sitecode;
        return $this->renderAjax('dilldown_list',[
            'dataGet' => $dataGet,
            'dataProvider' => $dataProvider,
            'sitecode' => $sitecode,
            'getLastpage' => $getLastpage,
            'getCurrentpage' => $getLastpage,
            'getPerpage' => $getPerpage,
            'modelAllrow' => $countAllRecord,
            'pageMaxpage' => $pageMaxpage,
        ]);
    }
    public function actionTjson(){
        return $this->render('tjson');
    }


    public function actionVerify()
    { 
        ini_set('max_execution_time', 0);
	set_time_limit(0);
	ini_set('memory_limit','512M'); 
        
        
        $tabdet = QueryTableDet::getDetail('cascapcloud', 'tb_data_3');
        # create table
        $cmd1 = SummarySuspected::setTableByCondition();
        $overAll = SummarySuspected::getConditionCount();
        $request = Yii::$app->request;
        // manage data provider
        if( strlen($request->get('list'))>0 ){
            $listVisit = SummarySuspected::getListSuspectedCondition();
        }
        return $this->render('verify',[
            'listVisit' => $listVisit,
            'tabdet' => $tabdet,
            'overAll' => $overAll,
        ]);
    }
    
    public function actionListSuspected(){
        $listData=0;
        return $this->render('listSuspected',
                ['listData' => $listData ]);
    }
    
    public function actionUpdateDataTable(){
        ini_set('max_execution_time', 0);
	set_time_limit(0);
	ini_set('memory_limit','512M'); 
        // call action
        $request = Yii::$app->request;
        $table      = $request->get('table');
        $dbreport   = 'cascap_report1';
        $dbsource   = 'cascapcloud';
        $result     = QueryTableReportUpdate::createTableReport($dbreport,$dbsource,$table);
        
        
        return json_encode($result);
    }
    
    public function actionGetDataSort($fromDate = null, $toDate = null, $colum = null, $sort = null, $sec = null, $div = null, $refresh = null, $page=1)
    {
        if ($fromDate == null){
            $fromDate = '2013-02-09';
        }
        if( $toDate == null) {
            $toDate = date('Y-m-d');
        }
        $datadate[fromDate] = $fromDate;
        $datadate[toDate] = $toDate;
        $limitrec=20;
        $dataProvider=QuerySuspected::getList($fromDate, $toDate,$colum, $sort, $page, $limitrec);
        $paging=QuerySuspected::getAllpage($fromDate, $toDate,$colum, $sort, $page, $limitrec);
        $render='_tablesuspected';
        $url = $this->renderAjax($render, [
            'dataProvider' => $dataProvider,
            'datadate' => $datadate,
            'div' => $div,
            'sort' => $sort,
            'page' => $page,
            'paging' => $paging,
            'limitrec' => $limitrec,
        ]);
        return $url;
    }

        public function actionPdfList()
    { 
        // manage data provider
        $dataProvider = new ActiveDataProvider([
              'query' => SuspectedCca::find()->where(['f2v6a3b1'=>'1']),
              'pagination' => [
                  'pageSize' => 20,
              ]
          ]);
        return $this->render('pdfList',[
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionCheckNewRecord()
    { 
        // clear table suspected
        $sql = "truncate tb_data_3_suspected ";
        Yii::$app->db->createCommand($sql)->execute();
        // insert to suspected
        $sql = "insert into tb_data_3_suspected ";
        $sql.= "select cca02.*,'','','','','','','','','','','','','','','','','','','','','' from tb_data_3 cca02 where cca02.f2v6a3b1='1' ";
        $sql.= "order by f2v1 desc ";
        Yii::$app->db->createCommand($sql)->execute();
        // update name from register
        $sql = "update tb_data_3_suspected susp inner join tb_data_1 reg on reg.ptid=susp.ptid "; 
        $sql.= "set susp.name=reg.name ";
        $sql.= ",susp.surname=reg.surname ";
        $sql.= ",susp.title=reg.title ";
        $sql.= ",susp.cohorttype_chartreview=reg.cohorttype_chartreview ";
        $sql.= "where length(reg.name)>0 ";
        Yii::$app->db->createCommand($sql)->execute();
        // update abnormal
        $sql = "update tb_data_3_suspected  ";
        $sql.= "set usabnormal= ";
        $sql.= "case 1 when f2v2a1='0' then '0: Normal'  ";
        $sql.= "when f2v2a1b1='1' then '1a) Mild fatty liver'  ";
        $sql.= "when f2v2a1b1='2' then '1b) Moderate fatty liver'  ";
        $sql.= "when f2v2a1b1='3' then '1c) Severe fatty liver'  ";
        $sql.= "when f2v2a1b2='1' then '2a) PDF 1'  ";
        $sql.= "when f2v2a1b2='2' then '2b) PDF 2'  ";
        $sql.= "when f2v2a1b2='3' then '2c) PDF 3'  ";
        $sql.= "when f2v2a1b3='1' then '3a) Cirrhosis'  ";
        $sql.= "when f2v2a1b4='1' then '4a) Parenchymal change'  ";
        $sql.= "else '-'  ";
        $sql.= "end ";
        Yii::$app->db->createCommand($sql)->execute();
        $sql = "delete from tb_data_3_suspected ";
        $sql.= "where (hsitecode like '91%' or hsitecode like '92%' or hsitecode like 'A%' or hsitecode like 'Z%' or hsitecode like 'z%') ";
        Yii::$app->db->createCommand($sql)->execute();
        // update suspected
        $sql = "update tb_data_3_suspected  ";
        $sql.= "set ussuspected= ";
        $sql.= "case 1 when f2v2a2b5c1='1' or f2v2a2b6c1='1' or f2v2a2b7c1='1' or f2v2a3b1='1' or f2v2a3b2='1' or f2v2a3b3='1' then '<span style=\"color: blue\">Suspected</span>'  ";
        $sql.= "else '<span style=\"color: blue\">Suspected</span> <span style=\"color: red\">[มี Error]</span>'  ";
        $sql.= "end ";
        Yii::$app->db->createCommand($sql)->execute();
        // Confirm
        $sql = "update tb_data_3_suspected sus ";
        $sql.= "inner join tb_data_4 ctmri on ctmri.ptid=sus.ptid ";
        $sql.= "set ctmri_id=ctmri.id ";
        $sql.= ",ctmri_result= ";
        $sql.= "case 1 when ctmri.f2p1v3='0' then '<span style=\"color: black\">- Normal</span>'  ";
        $sql.= "when ctmri.f2p1v3='1' then '<span style=\"color: blue\">+ Intrahepatic</span>'  ";
        $sql.= "when ctmri.f2p1v3='2' then '<span style=\"color: blue\">+ Perihilar</span>'  ";
        $sql.= "when ctmri.f2p1v3='3' then '<span style=\"color: blue\">+ Distal</span>'  ";
        $sql.= "when ctmri.f2p1v3='4' then '<span style=\"color: black\">- Not CCA</span>'  ";
        //$sql.= "else '<span style=\"color: red\">...</span>'  ";
        $sql.= "end ";
        $sql.= ",ctmri_f2p1v3=ctmri.f2p1v3 ";
        $sql.= "where ctmri.rstat<>'3' ";
        Yii::$app->db->createCommand($sql)->execute();
        // CCA-03
        $sql = "update tb_data_3_suspected sus ";
        $sql.= "inner join tb_data_7 cca03 on cca03.ptid=sus.ptid ";
        $sql.= "set treat_id=cca03.id ";
        $sql.= ",treat_result= ";
        $sql.= "case 1 when cca03.f3v5a1='1' then '<span style=\"color: black\">Surgery</span>'  ";
        $sql.= "when cca03.f3v5a1='0' then '<span style=\"color: black\">- xxx</span>'  ";
        //$sql.= "else '<span style=\"color: red\">...</span>'  ";
        $sql.= "end ";
        $sql.= "where cca03.rstat<>'3' ";
        $sql.= "and f3v5a1='1' ";
        Yii::$app->db->createCommand($sql)->execute();
        // CCA-03 -> f3v5a1b1
        $sql = "update tb_data_3_suspected sus ";
        $sql.= "inner join tb_data_7 cca03 on cca03.ptid=sus.ptid ";
        $sql.= "set treat_f3v5a1b1=cca03.f3v5a1b1 ";
        $sql.= "where cca03.rstat<>'3' ";
        $sql.= "and f3v5a1b1='1' ";
        Yii::$app->db->createCommand($sql)->execute();
        // CCA-03 -> f3v5a1b2
        $sql = "update tb_data_3_suspected sus ";
        $sql.= "inner join tb_data_7 cca03 on cca03.ptid=sus.ptid ";
        $sql.= "set treat_f3v5a1b2=cca03.f3v5a1b2 ";
        $sql.= "where cca03.rstat<>'3' ";
        $sql.= "and f3v5a1b2='1' ";
        Yii::$app->db->createCommand($sql)->execute();
        // CCA-03 -> f3v5a1b6
        $sql = "update tb_data_3_suspected sus ";
        $sql.= "inner join tb_data_7 cca03 on cca03.ptid=sus.ptid ";
        $sql.= "set treat_f3v5a1b6=cca03.f3v5a1b6 ";
        $sql.= "where cca03.rstat<>'3' ";
        $sql.= "and f3v5a1b6='1' ";
        Yii::$app->db->createCommand($sql)->execute();
        // CCA-03 -> f3v5a1b3
        $sql = "update tb_data_3_suspected sus ";
        $sql.= "inner join tb_data_7 cca03 on cca03.ptid=sus.ptid ";
        $sql.= "set treat_f3v5a1b3=cca03.f3v5a1b3 ";
        $sql.= "where cca03.rstat<>'3' ";
        $sql.= "and f3v5a1b3='1' ";
        Yii::$app->db->createCommand($sql)->execute();
        // CCA-03 -> f3v5a1b4
        $sql = "update tb_data_3_suspected sus ";
        $sql.= "inner join tb_data_7 cca03 on cca03.ptid=sus.ptid ";
        $sql.= "set treat_f3v5a1b4=cca03.f3v5a1b4 ";
        $sql.= "where cca03.rstat<>'3' ";
        $sql.= "and f3v5a1b4='1' ";
        Yii::$app->db->createCommand($sql)->execute();
        // CCA-03 -> f3v5a1b5
        $sql = "update tb_data_3_suspected sus ";
        $sql.= "inner join tb_data_7 cca03 on cca03.ptid=sus.ptid ";
        $sql.= "set treat_f3v5a1b5=cca03.f3v5a1b5 ";
        $sql.= "where cca03.rstat<>'3' ";
        $sql.= "and f3v5a1b5='1' ";
        Yii::$app->db->createCommand($sql)->execute();
        // CCA-04
        $sql = "update tb_data_3_suspected sus ";
        $sql.= "inner join tb_data_8 cca04 on cca04.ptid=sus.ptid ";
        $sql.= "set patho_id=cca04.id ";
        $sql.= ",patho_result= ";
        $sql.= "case 1 when cca04.f4v4='1' then '<span style=\"color: blue\">+  Intra</span>'  ";
        $sql.= "when cca04.f4v4='2' then '<span style=\"color: blue\">+ Perihilar</span>'  ";
        $sql.= "when cca04.f4v4='3' then '<span style=\"color: blue\">+ Distal</span>'  ";
        $sql.= "when cca04.f4v4='4' then '<span style=\"color: black\">+ Other</span>'  ";
        $sql.= "when cca04.f4v4='5' then '<span style=\"color: black\">+ not done</span>'  ";
        //$sql.= "else '<span style=\"color: red\">...</span>'  ";
        $sql.= "end ";
        $sql.= ",patho_f4v4=cca04.f4v4 ";
        $sql.= ",patho_staging=case 1 when cca04.f4v4='1' and cca04.f4v8a4='0' then '0' ";
        $sql.= "when cca04.f4v4='1' and cca04.f4v8a4='1' then 'I' ";
        $sql.= "when cca04.f4v4='1' and cca04.f4v8a4='2' then 'II' ";
        $sql.= "when cca04.f4v4='1' and cca04.f4v8a4='3' then 'III' ";
        $sql.= "when cca04.f4v4='1' and cca04.f4v8a4='4' then 'IVA' ";
        $sql.= "when cca04.f4v4='1' and cca04.f4v8a4='5' then 'IVB' ";
        $sql.= "when cca04.f4v4='1' and cca04.f4v8a4='6' then 'UK' ";
        $sql.= "when cca04.f4v4='2' and cca04.f4v8a5='0' then '0' ";
        $sql.= "when cca04.f4v4='2' and cca04.f4v8a5='1' then 'I' ";
        $sql.= "when cca04.f4v4='2' and cca04.f4v8a5='2' then 'II' ";
        $sql.= "when cca04.f4v4='2' and cca04.f4v8a5='3' then 'IIIA' ";
        $sql.= "when cca04.f4v4='2' and cca04.f4v8a5='4' then 'IIIB' ";
        $sql.= "when cca04.f4v4='2' and cca04.f4v8a5='5' then 'IVA' ";
        $sql.= "when cca04.f4v4='2' and cca04.f4v8a5='6' then 'IVB' ";
        $sql.= "when cca04.f4v4='2' and cca04.f4v8a5='7' then 'UK' ";
        $sql.= "when cca04.f4v4='3' and cca04.f4v8a6='0' then '0' ";
        $sql.= "when cca04.f4v4='3' and cca04.f4v8a6='1' then 'I' ";
        $sql.= "when cca04.f4v4='3' and cca04.f4v8a6='2' then 'II' ";
        $sql.= "when cca04.f4v4='3' and cca04.f4v8a6='3' then 'IIIA' ";
        $sql.= "when cca04.f4v4='3' and cca04.f4v8a6='4' then 'IIIB' ";
        $sql.= "when cca04.f4v4='3' and cca04.f4v8a6='5' then 'IVA' ";
        $sql.= "when cca04.f4v4='3' and cca04.f4v8a6='6' then 'IVB' ";
        $sql.= "when cca04.f4v4='3' and cca04.f4v8a6='7' then 'UK' ";
        $sql.= "end ";
        $sql.= "where cca04.rstat<>'3' ";
        $sql.= "and (f4v4='1' or f4v4='2' or f4v4='3' or f4v4='4' or f4v4='5') ";
        Yii::$app->db->createCommand($sql)->execute();
        // Redirect page
        //
        return $this->redirect(['index']);
    }
    
    // ไม่ได้ถูกเรียกใช้
    public function getListSuspected(){
        $count = Yii::$app->db->createCommand('
            SELECT COUNT(*) FROM tb_data_3_suspected WHERE f2v6a3b1=:suspected
        ', [':suspected' => 1])->queryScalar();

        $provider = new SqlDataProvider([
            'sql' => 'SELECT * FROM tb_data_3_suspected WHERE f2v6a3b1=:suspected',
            'params' => [':suspected' => 1],
            'totalCount' => $count,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'attributes' => [
                    'hsitecode',
                    'hptcode',
                    'f2v1',
                ],
            ],
        ]);

        // returns an array of data rows
        //$models = $provider->getModels;
        return $provider;
    }
    
    public function createTableSuspected(){
        $userId = \Yii::$app->user->identity->id;
        $tempTableName = "sespected_".$userId.str_replace('.','',microtime(true));
        // create tem table
        $sqlCreateTable = "CREATE TEMPORARY TABLE ".$tempTableName." ";
        $sqlCreateTable.= "select * from tb_data_3 where rstat<>'3' and f2v6a3b1='1' ";
        $sqlCreateTable.= "and f2v1 between '2013-02-09' and NOW() ";
        Yii::$app->db->createCommand($sqlCreateTable)->execute();
        
        $count = Yii::$app->db->createCommand('SELECT COUNT(*) FROM tb_data_3 ')->queryScalar();
        
        $provider = new SqlDataProvider([
            'sql' => 'SELECT * FROM '.$sqlCreateTable.' order by hsitecode,hptcode,f2v1 ',
            'params' => [':suspected' => 1],
            'totalCount' => $count,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'attributes' => [
                    'hsitecode',
                    'hptcode',
                    'f2v1',
                ],
            ],
        ]);
        
        
        
        return $provider;
    }
    
    
    
    public function actionOpenForm($ezf_id=1437377239070461301,$dataid=1461742236098088400,$ptid=null){
        # default /inputdata/redirect-page?dataid=1461742236098088400&ezf_id=1437377239070461301
        QueryData::updatePTIDView($ezf_id,$ptid,$dataid);
        $url="/inputdata/redirect-page?dataid=$dataid&ezf_id=$ezf_id";
        return $this->redirect([$url]);
    }
    
    public function actionOpenFormNill($ezf_id=1437377239070461301,$dataid=1461742236098088400,$ptid=null){
        # default /inputdata/redirect-page?dataid=1461742236098088400&ezf_id=1437377239070461301
        //QueryData::updatePTIDView($ezf_id,$ptid,$dataid);
        $return = ['ezf_id'=>$ezf_id,'dataid'=>$dataid,'ptid'=>$ptid];
        return \yii\helpers\Json::encode($return);
    }
    
    public function actionData(){
        $listData=0;
        $overAll = SummarySuspected::getOverAll();
        return $this->render('data',
                [
                    'listData' => $listData,
                    'overAll'=>$overAll
                ]);
    }
    
    public function actionGetDataListTemp($colum='f2v1',$sort='desc',$page=1,$limitrec=100,$div = null){
        if( FALSE ){
            echo "<br />limitrec: ".$limitrec;
            echo "<br />page: ".$page;
            echo "<br />colum: ".$colum;
            echo "<br />sort: ".$sort;
            echo "<br />div: ".$div;
        }
        $viewform = "/teleradio/suspected/data";
        $userid = Yii::$app->user->identity->id;
        if( TRUE ){
            #$limitrec   =100;
            #$page       = 1;
            #$colum      = 'f2v1';
            #$sort       = 'desc';
            $dataProvider   = QueryData::getListTemp($colum, $sort, $page, $limitrec);
            $paging         = QueryData::getAllpage($limitrec);
            $viewform       = QueryData::getPTIDViewByUser($viewform,$userid);
        }
        $render = '_datatable';
        $url = $this->renderAjax($render, [
            'dataProvider' => $dataProvider,
            'viewform' => $viewform,
            'div' => $div,
            'colum' => $colum,
            'sort' => $sort,
            'page' => $page,
            'paging' => $paging,
            'limitrec' => $limitrec,
        ]);
        return $url;
    }
    
    public function actionGetDataListAmphur(){
        $request    = Yii::$app->request;
        //$qryAmphur = $this->getAmphur($request->get('provinceCode'));
        $amphur = FuncHospital::getAmphurList($request->get('provinceCode'));
        echo '<option value="" amphurcode="">แสดงทุก อำเภอ</option>';
        if( count($amphur)>0 ){
            foreach ($amphur as $key => $value) {
                echo '<option value="'.$key.'" amphurcode="'.$key.'">'.$value.'</option>';
            }
        }
    }
    public function actionGetDataListAllHospitalThai(){
        $request    = Yii::$app->request;
        $hospital   = FuncHospital::getHospitalByAmphurCode($request->get('provinceCode'), $request->get('amphurCode'));

        echo '<option value="" hcode="">แสดงในทุก โรงพยาบาล</option>';
        if( count($hospital)>0 ){
            foreach ($hospital as $key => $value) {
                echo '<option value="'.$key.'" hcode="'.$key.'">'.$value.'</option>';
            }
        }
    }

    

    public function actionGetDataList($fromDate = null, $toDate = null, $colum = null, $sort = null, $sec = null, $div = null, $refresh = null, $page=1
            ,$_reg=0,$_cca01=0,$_cca02=0,$_cca02p1=0,$_cca03=0,$_cca04=0,$_cca05=0
            ,$_mainreg=0,$_maincca01=0,$_maincca02=0,$_maincca02p1=0,$_maincca03=0,$_maincca04=0,$_maincca05=0
            ,$_pcode='',$_acode='',$_hcode='')
    {
        //echo $_provincecode;
        //echo $_acode;
        //$urlrequest = Yii::$app->getRequest()->serverName;
        //echo $urlrequest;
        if( 0 ){
            echo "<pre align='left'>";
            print_r($_SERVER);
            echo "</pre>";
        }
        $request    = Yii::$app->request;
//        echo $request->get('_pdf1');
//        echo $request->get('_pdf2');
//        echo $request->get('_pdf3');
//        echo $request->get('_scca');
        if ($fromDate == null){
            $fromDate = '2013-02-09';
        }
        if( $toDate == null) {
            $toDate = date('Y-m-d');
        }
//        echo "<br />".strlen($sort);
//        echo "<br />_cca02p1 ".$_cca02p1;
//        echo "<br />".$sort;
//        echo "<br />".$_maincca03;
        
        $datadate['fromDate'] = $fromDate;
        $datadate['toDate'] = $toDate;
        if( 0 ){
            echo "<pre align='left'>";
            print_r($datadate);
            echo "</pre>";
        }
        $conditionTable['_reg']=$_reg;
        $conditionTable['_cca01']=$_cca01;
        $conditionTable['_cca02']=$_cca02;
        $conditionTable['_cca02p1']=$_cca02p1;
        if($_cca03=='1' || $request->get('_sur03')=='1'){
            #$conditionTable['_cca03']=$_cca03;
            $conditionTable['_cca03']='1';
        }
        $conditionTable['_cca04']=$_cca04;
        $conditionTable['_cca05']=$_cca05;
        if($_maincca02==1){
            $mainTable = 'cca02';
            $ezf_id = "1437619524091524800";
            $colum = ( strlen($colum)==0 ) ? 'f2v1' : $colum;
            $sort = ( strlen($sort)==0 ) ? 'desc' : $sort;
        }else if($_maincca02p1==1){
            $mainTable = 'cca02p1';
            $ezf_id = "1454041742064651700";
            $colum = ( strlen($colum)==0 ) ? 'f2p1v1' : $colum;
            $sort = ( strlen($sort)==0 ) ? 'desc' : $sort;
        }else if($_maincca03==1){
            $mainTable = 'cca03';
            $ezf_id = "1451381257025574200";
            $colum = ( strlen($colum)==0 ) ? 'f3v4dvisit' : $colum;
            $sort = ( strlen($sort)==0 ) ? 'desc' : $sort;
        }else if($_maincca04==1){
            $mainTable = 'cca04';
            $ezf_id = "1452061550097822200";
            $colum = ( strlen($colum)==0 ) ? 'f4v1' : $colum;
            $sort = ( strlen($sort)==0 ) ? 'desc' : $sort;
        }else if($_maincca05==1){
            $mainTable = 'cca05';
            $ezf_id = "1440515302053265900";
            $colum = ( strlen($colum)==0 ) ? 'f5v1a1' : $colum;
            $sort = ( strlen($sort)==0 ) ? 'desc' : $sort;
        }else if($_maincca01==1){
            $mainTable = 'cca01';
            $ezf_id = "1437377239070461302";
            $colum = ( strlen($colum)==0 ) ? 'f1vdcomp' : $colum;
            $sort = ( strlen($sort)==0 ) ? 'desc' : $sort;
        }else if($_mainreg==1){
            $mainTable = 'reg';
            $ezf_id = "1437377239070461301";
            $colum = ( strlen($colum)==0 ) ? 'create_date' : $colum;
            $sort = ( strlen($sort)==0 ) ? 'desc' : $sort;
        }else{
            $mainTable = 'cca02';
            $ezf_id = "1437619524091524800";
            $colum = ( strlen($colum)==0 ) ? 'f2p1v1' : $colum;
            $sort = ( strlen($sort)==0 ) ? 'desc' : $sort;
        }
        if( FALSE ){
            echo "<br />mainTable".$mainTable;
            echo "<br />ezf_id".$ezf_id;
            echo "<br />colum".$colum;
            echo "<br />sort".$sort;
        }
        $viewform = "/teleradio/suspected/data";
        $userid = Yii::$app->user->identity->id;
        
        $checkNotCCA        = $request->get('_nccact') + $request->get('_ncca03') + $request->get('_ncca04');
        #if($_mainreg==1 || $_maincca01==1){
        if( FALSE){
            
        }else{
            $limitrec=100;
            $sqlHospital    = QueryData::getHospitalWhere($_pcode,$_acode,$_hcode);
            $sqlPDF         = QueryData::getPDF($request->get('_pdf1'),$request->get('_pdf2'),$request->get('_pdf3'));
            $sqlSuspectedCCA= QueryData::getSuspectedCCA($request->get('_scca'));
            $sqlImage02     = QueryData::getUsImageExist($request->get('_img02'));
            if( $checkNotCCA>1 ){
                $sqlNotCCA      = QueryData::getNotCCA($request->get('_nccact'),$request->get('_ncca03'),$request->get('_ncca04'));
            }else{
                $sqlCCACTMRI    = QueryData::getCCACTMRI($request->get('_nccact'));
                $sqlCCA03       = QueryData::getCCA03($request->get('_ncca03'));
            }
            //$sqlCCA03Dead   = QueryData::getCCA03Dead($request->get('_dead03'));
            //echo $sqlPDF.$sqlSuspectedCCA.$sqlCCACTMRI.$sqlCCA03.$sqlNotCCA.$sqlCCA03Dead;
            $sqlHospital    = $sqlHospital.$sqlPDF.$sqlSuspectedCCA.$sqlImage02.$sqlCCACTMRI.$sqlCCA03.$sqlNotCCA.$sqlCCA03Dead;
            $sqlWhere       = QueryData::getWhereSQL($fromDate, $toDate, $conditionTable, $sqlHospital, $mainTable);
            //echo $sqlWhere;
            $dataProvider   = QueryData::getList($sqlWhere, $mainTable, $ezf_id, $colum, $sort, $page, $limitrec);
//            $paging         = QueryData::getAllpage($sqlWhere, $mainTable,$colum, $sort, $page, $limitrec);
            
            
            $viewform       = QueryData::getPTIDViewByUser($viewform,$userid);
            $paging         = QueryData::getAllpage($limitrec);
        }
        $render = '_datatable';
        $url = $this->renderAjax($render, [
            'dataProvider' => $dataProvider,
            'viewform' => $viewform,
            'datadate' => $datadate,
            'div' => $div,
            'colum' => $colum,
            'sort' => $sort,
            'page' => $page,
            'paging' => $paging,
            'limitrec' => $limitrec,
            'sec' => $sec,
            'refresh' => $refresh,
        ]);
        return $url;
    }
    
    public function actionExportData(){
        $render = 'exportdata';
        $url = $this->render($render, [
            'dataProvider' => $dataProvider,
            'viewform' => $viewform,
            'div' => $div,
            'colum' => $colum,
            'sort' => $sort,
            'page' => $page,
            'paging' => $paging,
            'limitrec' => $limitrec,
        ]);
        return $url;
    }
    
    
}

