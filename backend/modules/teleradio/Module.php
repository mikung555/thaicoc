<?php

namespace backend\modules\teleradio;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\modules\teleradio\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
