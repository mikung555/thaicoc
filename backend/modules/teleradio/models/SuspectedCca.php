<?php

namespace app\modules\teleradio\models;

use Yii;

/**
 * This is the model class for table "tb_data_3_suspected".
 *
 * @property integer $id
 * @property integer $ptid
 * @property integer $target
 * @property string $rstat
 * @property string $xsourcex
 * @property string $xdepartmentx
 * @property string $hsitecode
 * @property string $hptcode
 * @property string $f2v1
 * @property string $f2v2a1
 * @property string $f2v2a1b1
 * @property string $f2v2a1b2
 * @property string $f2v2a1b3
 * @property string $f2v2a1b4
 * @property string $f2v2a2
 * @property string $f2v2a2b1c1
 * @property string $f2v2a2b1c2
 * @property string $f2v2a2b1c3
 * @property string $f2v2a2b1c4
 * @property string $f2v2a2b2c1
 * @property string $f2v2a2b2c2
 * @property string $f2v2a2b2c3
 * @property string $f2v2a2b2c4
 * @property string $f2v2a2b3c1
 * @property string $f2v2a2b3c2
 * @property string $f2v2a2b3c3
 * @property string $f2v2a2b3c4
 * @property string $f2v2a2b4c1
 * @property string $f2v2a2b4c2
 * @property string $f2v2a2b4c3
 * @property string $f2v2a2b4c4
 * @property string $f2v2a2b5c1
 * @property string $f2v2a2b5c2
 * @property string $f2v2a2b5c3
 * @property string $f2v2a2b5c4
 * @property string $f2v2a2b6c1
 * @property string $f2v2a2b6c2
 * @property string $f2v2a2b6c3
 * @property string $f2v2a2b6c4
 * @property string $f2v2a2b7c1
 * @property string $f2v2a2b7c2
 * @property string $f2v2a2b7c3
 * @property string $f2v2a2b7c4
 * @property string $f2v2a3b0
 * @property string $f2v2a3b1
 * @property string $f2v2a3b2
 * @property string $f2v2a3b3
 * @property string $f2v3a1
 * @property string $f2v3a2
 * @property string $f2v3a2b1
 * @property string $f2v3a2b1c1
 * @property string $f2v3a2b1x
 * @property string $f2v3a2b2
 * @property string $f2v3a2b2c1
 * @property string $f2v3a2b2x
 * @property string $f2v3a2b3
 * @property string $f2v3a2b3c1
 * @property string $f2v3a2b3x
 * @property string $f2v3a3
 * @property string $f2v3a3b1
 * @property string $f2v3a4
 * @property string $f2v3a5
 * @property string $f2v4a1
 * @property string $f2v4a2
 * @property string $f2v4a2b1
 * @property string $f2v4a2b1x
 * @property string $f2v4a2b2
 * @property string $f2v4a2b2x
 * @property string $f2v4a3
 * @property string $f2v4a3b1
 * @property string $f2v4a3b2
 * @property string $f2v4a4
 * @property string $f2v4a4b1
 * @property string $f2v4a4b1c1
 * @property string $f2v4a4b1c2
 * @property string $f2v4a4b2
 * @property string $f2v4a4b2c1
 * @property string $f2v4a4b2c2
 * @property string $f2v4a5
 * @property string $f2v4a5b1c1
 * @property string $f2v4a5b1c2
 * @property string $f2v4a6
 * @property string $f2v5a1
 * @property string $f2v5a2
 * @property string $f2v5a3
 * @property string $f2v5a3x
 * @property string $f2v6
 * @property string $f2v6a3
 * @property string $f2v6a3code
 * @property string $f2v6a3b1
 * @property string $f2v6a3b2
 * @property string $f2v6a3b2x
 * @property integer $f2doctorcode
 * @property string $usimages
 * @property string $error
 * @property string $sitecode
 * @property string $ptcode
 * @property string $ptcodefull
 * @property integer $user_create
 * @property string $create_date
 * @property integer $user_update
 * @property string $update_date
 * @property string $target_old
 * @property string $hptcodefull
 * @property string $f2v6a3x
 * @property string $f2v7
 * @property string $f2v7a2x
 * @property string $f2v7_confirm
 * @property string $f2v8x
 * @property string $f2doctorname
 * @property string $f2v1db
 * @property string $f2v3
 * @property string $f2v4
 * @property string $fsupdate
 * @property string $rdoconfirmdate
 * @property integer $rdoconfirm
 * @property string $rdoconfirmstatus
 * @property string $hzone
 * @property string $hprovince
 * @property string $hamphur
 * @property string $htambon
 * @property string $add1n8code
 * @property string $add1n7code
 * @property string $add1n6code
 * @property string $bdatedb
 * @property integer $times
 * @property string $dadd
 * @property integer $pidadd
 * @property string $dupdate
 * @property integer $pidupdate
 * @property integer $pidowner
 * @property string $confirmdate
 * @property integer $confirm
 * @property string $ICF
 * @property integer $sys_pay
 * @property string $sys_paytime
 * @property integer $sys_recieve
 * @property string $sys_recievedate
 * @property string $sys_bankdate
 * @property integer $sys_diag_pay
 * @property string $sys_diag_paytime
 * @property integer $sys_diag_recieve
 * @property string $sys_diag_recievedate
 * @property string $sys_diag_bankdate
 * @property string $usmobile
 * @property string $sys_usimageexist
 * @property string $sys_radiosos
 * @property string $sys_radiososdate
 * @property string $sys_radiososx
 * @property string $sys_fx110
 * @property string $sys_fx120
 * @property string $sys_fx130
 * @property string $sys_fx140
 * @property string $sys_fx210
 * @property string $sys_fx220
 * @property string $sys_fx230
 * @property string $sys_fx240
 * @property string $sys_fx250
 * @property string $sys_fx310
 * @property string $sys_fx320
 * @property string $sys_fx330
 * @property string $sys_fx340
 * @property string $sys_fx350
 * @property string $sys_fx360
 * @property string $sys_fx410
 * @property string $sys_fx420
 * @property string $sys_fx430
 * @property string $v3
 * @property string $isdoctor
 * @property string $f2nodoctor
 * @property string $title
 * @property string $name
 * @property string $surname
 * @property string $usabnormal
 * @property string $ussuspected
 * @property integer $ctmri_id
 * @property string $ctmri_result
 * @property integer $treat_id
 * @property string $treat_result
 * @property integer $patho_id
 * @property string $patho_result
 */
class SuspectedCca extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tb_data_3_suspected';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'target', 'sys_fx110', 'sys_fx120', 'sys_fx130', 'sys_fx140', 'sys_fx210', 'sys_fx220', 'sys_fx230', 'sys_fx240', 'sys_fx250', 'sys_fx310', 'sys_fx320', 'sys_fx330', 'sys_fx340', 'sys_fx350', 'sys_fx360', 'sys_fx410', 'sys_fx420', 'sys_fx430'], 'required'],
            [['id', 'ptid', 'target', 'f2doctorcode', 'user_create', 'user_update', 'rdoconfirm', 'times', 'pidadd', 'pidupdate', 'pidowner', 'confirm', 'sys_pay', 'sys_recieve', 'sys_diag_pay', 'sys_diag_recieve', 'ctmri_id', 'treat_id', 'patho_id'], 'integer'],
            [['f2v1', 'create_date', 'update_date', 'f2v1db', 'rdoconfirmdate', 'bdatedb', 'dadd', 'dupdate', 'confirmdate', 'sys_paytime', 'sys_recievedate', 'sys_bankdate', 'sys_diag_paytime', 'sys_diag_recievedate', 'sys_diag_bankdate', 'sys_radiososdate'], 'safe'],
            [['error'], 'string'],
            [['rstat', 'rdoconfirmstatus', 'hzone', 'hprovince', 'ICF', 'sys_usimageexist', 'sys_radiosos', 'sys_fx110', 'sys_fx120', 'sys_fx130', 'sys_fx140', 'sys_fx210', 'sys_fx220', 'sys_fx230', 'sys_fx240', 'sys_fx250', 'sys_fx310', 'sys_fx320', 'sys_fx330', 'sys_fx340', 'sys_fx350', 'sys_fx360', 'sys_fx410', 'sys_fx420', 'sys_fx430', 'v3'], 'string', 'max' => 2],
            [['xsourcex', 'ptcodefull', 'hptcodefull'], 'string', 'max' => 10],
            [['xdepartmentx'], 'string', 'max' => 20],
            [['hsitecode', 'hptcode'], 'string', 'max' => 5],
            [['f2v2a1', 'f2v2a1b1', 'f2v2a1b2', 'f2v2a2', 'f2v3a2b1c1', 'f2v3a2b2c1', 'f2v3a2b3c1', 'f2v3a3b1', 'f2v6', 'f2v6a3b1', 'f2v6a3b2', 'f2v6a3b2x', 'name', 'surname', 'usabnormal', 'ussuspected', 'ctmri_result', 'treat_result', 'patho_result'], 'string', 'max' => 100],
            [['f2v2a1b3', 'f2v2a2b5c1', 'f2v2a2b5c2', 'f2v2a2b5c3', 'f2v2a2b5c4', 'f2v2a2b6c1', 'f2v2a2b6c2', 'f2v2a2b6c3', 'f2v2a2b6c4', 'f2v2a2b7c1', 'f2v2a2b7c2', 'f2v2a2b7c3', 'f2v2a2b7c4', 'usimages', 'sitecode', 'ptcode', 'target_old'], 'string', 'max' => 255],
            [['f2v2a1b4', 'f2v2a2b1c1', 'f2v2a2b1c2', 'f2v2a2b1c3', 'f2v2a2b1c4', 'f2v2a2b2c1', 'f2v2a2b2c2', 'f2v2a2b2c3', 'f2v2a2b2c4', 'f2v2a2b3c1', 'f2v2a2b3c2', 'f2v2a2b3c3', 'f2v2a2b3c4', 'f2v2a2b4c1', 'f2v2a2b4c2', 'f2v2a2b4c3', 'f2v2a2b4c4', 'f2v2a3b0', 'f2v2a3b1', 'f2v2a3b2', 'f2v2a3b3', 'f2v3a1', 'f2v3a2', 'f2v3a2b1', 'f2v3a2b1x', 'f2v3a2b2', 'f2v3a2b2x', 'f2v3a2b3', 'f2v3a2b3x', 'f2v3a3', 'f2v3a4', 'f2v3a5', 'f2v4a1', 'f2v4a2', 'f2v4a2b1', 'f2v4a2b1x', 'f2v4a2b2', 'f2v4a2b2x', 'f2v4a3', 'f2v4a3b1', 'f2v4a3b2', 'f2v4a4', 'f2v4a4b1', 'f2v4a4b1c1', 'f2v4a4b1c2', 'f2v4a4b2', 'f2v4a4b2c1', 'f2v4a4b2c2', 'f2v4a5', 'f2v4a5b1c1', 'f2v4a5b1c2', 'f2v4a6', 'f2v5a1', 'f2v5a2', 'f2v5a3', 'f2v5a3x', 'f2v6a3', 'f2v6a3code', 'f2v6a3x', 'f2v7', 'f2v7a2x', 'f2v7_confirm', 'f2v8x', 'f2v3', 'f2v4', 'fsupdate'], 'string', 'max' => 40],
            [['f2doctorname'], 'string', 'max' => 200],
            [['hamphur'], 'string', 'max' => 4],
            [['htambon', 'add1n8code', 'add1n7code', 'add1n6code'], 'string', 'max' => 6],
            [['usmobile'], 'string', 'max' => 11],
            [['sys_radiososx'], 'string', 'max' => 250],
            [['isdoctor', 'f2nodoctor', 'title'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ptid' => 'Ptid',
            'target' => 'Target',
            'rstat' => 'สถานะบันทึกข้อมูล',
            'xsourcex' => 'Xsourcex',
            'xdepartmentx' => 'Xdepartmentx',
            'hsitecode' => 'รหัสโรงพยาบาล',
            'hptcode' => 'PID',
            'f2v1' => 'วันที่เข้ารักษา',
            'f2v2a1' => 'F2v2a1',
            'f2v2a1b1' => 'F2v2a1b1',
            'f2v2a1b2' => 'F2v2a1b2',
            'f2v2a1b3' => 'F2v2a1b3',
            'f2v2a1b4' => 'Parenchymal change',
            'f2v2a2' => 'F2v2a2',
            'f2v2a2b1c1' => 'High echo',
            'f2v2a2b1c2' => 'Size of low echo',
            'f2v2a2b1c3' => 'Right low echo',
            'f2v2a2b1c4' => 'Left low echo',
            'f2v2a2b2c1' => 'Low echo',
            'f2v2a2b2c2' => 'Size of low echo',
            'f2v2a2b2c3' => 'Right low echo',
            'f2v2a2b2c4' => 'Left low echo',
            'f2v2a2b3c1' => 'Mixed echo',
            'f2v2a2b3c2' => 'Size of mixed echo',
            'f2v2a2b3c3' => 'Right mixed echo',
            'f2v2a2b3c4' => 'Left mixed echo',
            'f2v2a2b4c1' => 'Liver cyst',
            'f2v2a2b4c2' => 'Size of liver cyst',
            'f2v2a2b4c3' => 'Right liver cyst',
            'f2v2a2b4c4' => 'Left liver cyst',
            'f2v2a2b5c1' => 'F2v2a2b5c1',
            'f2v2a2b5c2' => 'F2v2a2b5c2',
            'f2v2a2b5c3' => 'F2v2a2b5c3',
            'f2v2a2b5c4' => 'F2v2a2b5c4',
            'f2v2a2b6c1' => 'F2v2a2b6c1',
            'f2v2a2b6c2' => 'F2v2a2b6c2',
            'f2v2a2b6c3' => 'F2v2a2b6c3',
            'f2v2a2b6c4' => 'F2v2a2b6c4',
            'f2v2a2b7c1' => 'F2v2a2b7c1',
            'f2v2a2b7c2' => 'F2v2a2b7c2',
            'f2v2a2b7c3' => 'F2v2a2b7c3',
            'f2v2a2b7c4' => 'F2v2a2b7c4',
            'f2v2a3b0' => 'No dilated duct',
            'f2v2a3b1' => 'Dilated bile duct, right lobe',
            'f2v2a3b2' => 'Dilated bile duct, left lobe',
            'f2v2a3b3' => 'Common bile duct',
            'f2v3a1' => 'Gallbladder, normal',
            'f2v3a2' => 'Gallbladder, wall',
            'f2v3a2b1' => 'Wall thickening',
            'f2v3a2b1c1' => 'F2v3a2b1c1',
            'f2v3a2b1x' => 'Size of wall thickening',
            'f2v3a2b2' => 'Wall polyp',
            'f2v3a2b2c1' => 'F2v3a2b2c1',
            'f2v3a2b2x' => 'Size of wall polyp',
            'f2v3a2b3' => 'Wall mass',
            'f2v3a2b3c1' => 'F2v3a2b3c1',
            'f2v3a2b3x' => 'Size of wall mass',
            'f2v3a3' => 'Gallbladder, gallstone',
            'f2v3a3b1' => 'F2v3a3b1',
            'f2v3a4' => 'Gallbladder, post cholecystectomy',
            'f2v3a5' => 'Not seen Gallbladder',
            'f2v4a1' => 'Kidney, normal',
            'f2v4a2' => 'Kidney, renal cyst',
            'f2v4a2b1' => 'Right renal cyst',
            'f2v4a2b1x' => 'Size of right renal cyst',
            'f2v4a2b2' => 'Left renal cyst',
            'f2v4a2b2x' => 'Size of left renal cyst',
            'f2v4a3' => 'Kidney, parenchymal change',
            'f2v4a3b1' => 'Right parenchymal change',
            'f2v4a3b2' => 'Left parenchymal change',
            'f2v4a4' => 'Kidney, renal stone',
            'f2v4a4b1' => 'Renal stone without hydronephrosis',
            'f2v4a4b1c1' => 'Right renal stone without hydronephrosis',
            'f2v4a4b1c2' => 'Left renal stone without hydronephrosis',
            'f2v4a4b2' => 'Renal stone with hydronephrosis',
            'f2v4a4b2c1' => 'Right renal stone with hydronephrosis',
            'f2v4a4b2c2' => 'Left renal stone with hydronephrosis',
            'f2v4a5' => 'Kidney, post nephrectomy',
            'f2v4a5b1c1' => 'Right post nephrectomy',
            'f2v4a5b1c2' => 'Left post nephrectomy',
            'f2v4a6' => 'Not seen kidney',
            'f2v5a1' => 'Ascites finding',
            'f2v5a2' => 'Splenomegaly finding',
            'f2v5a3' => 'Others finding',
            'f2v5a3x' => 'Specific others finding',
            'f2v6' => 'F2v6',
            'f2v6a3' => 'Refer to hospital',
            'f2v6a3code' => 'Code of hospital that refer to',
            'f2v6a3b1' => 'F2v6a3b1',
            'f2v6a3b2' => 'F2v6a3b2',
            'f2v6a3b2x' => 'F2v6a3b2x',
            'f2doctorcode' => 'Doctor',
            'usimages' => 'Usimages',
            'error' => 'Error',
            'sitecode' => 'Sitecode',
            'ptcode' => 'Ptcode',
            'ptcodefull' => 'Ptcodefull',
            'user_create' => 'User Create',
            'create_date' => 'Create Date',
            'user_update' => 'User Update',
            'update_date' => 'Update Date',
            'target_old' => 'Target Old',
            'hptcodefull' => 'Hptcodefull',
            'f2v6a3x' => 'Name of hospital that refer to',
            'f2v7' => 'Reasons for referring',
            'f2v7a2x' => 'Specific another reason that refer to',
            'f2v7_confirm' => 'Confirm referring',
            'f2v8x' => 'Specific another reason that refer to',
            'f2doctorname' => 'F2doctorname',
            'f2v1db' => 'F2v1db',
            'f2v3' => 'Gallbladder',
            'f2v4' => 'Kidney',
            'fsupdate' => 'Complete date',
            'rdoconfirmdate' => 'Rdoconfirmdate',
            'rdoconfirm' => 'Rdoconfirm',
            'rdoconfirmstatus' => 'Rdoconfirmstatus',
            'hzone' => 'Hzone',
            'hprovince' => 'Hprovince',
            'hamphur' => 'Hamphur',
            'htambon' => 'Htambon',
            'add1n8code' => 'Add1n8code',
            'add1n7code' => 'Add1n7code',
            'add1n6code' => 'Add1n6code',
            'bdatedb' => 'Bdatedb',
            'times' => 'Times',
            'dadd' => 'Dadd',
            'pidadd' => 'Pidadd',
            'dupdate' => 'Dupdate',
            'pidupdate' => 'Pidupdate',
            'pidowner' => 'Pidowner',
            'confirmdate' => 'Confirmdate',
            'confirm' => 'Confirm',
            'ICF' => 'Icf',
            'sys_pay' => 'Sys Pay',
            'sys_paytime' => 'Sys Paytime',
            'sys_recieve' => 'Sys Recieve',
            'sys_recievedate' => 'Sys Recievedate',
            'sys_bankdate' => 'Sys Bankdate',
            'sys_diag_pay' => 'Sys Diag Pay',
            'sys_diag_paytime' => 'Sys Diag Paytime',
            'sys_diag_recieve' => 'Sys Diag Recieve',
            'sys_diag_recievedate' => 'Sys Diag Recievedate',
            'sys_diag_bankdate' => 'Sys Diag Bankdate',
            'usmobile' => 'Usmobile',
            'sys_usimageexist' => 'Sys Usimageexist',
            'sys_radiosos' => 'Sys Radiosos',
            'sys_radiososdate' => 'Sys Radiososdate',
            'sys_radiososx' => 'Sys Radiososx',
            'sys_fx110' => 'Sys Fx110',
            'sys_fx120' => 'Sys Fx120',
            'sys_fx130' => 'Sys Fx130',
            'sys_fx140' => 'Sys Fx140',
            'sys_fx210' => 'Sys Fx210',
            'sys_fx220' => 'Sys Fx220',
            'sys_fx230' => 'Sys Fx230',
            'sys_fx240' => 'Sys Fx240',
            'sys_fx250' => 'Sys Fx250',
            'sys_fx310' => 'Sys Fx310',
            'sys_fx320' => 'Sys Fx320',
            'sys_fx330' => 'Sys Fx330',
            'sys_fx340' => 'Sys Fx340',
            'sys_fx350' => 'Sys Fx350',
            'sys_fx360' => 'Sys Fx360',
            'sys_fx410' => 'Sys Fx410',
            'sys_fx420' => 'Sys Fx420',
            'sys_fx430' => 'Sys Fx430',
            'v3' => 'V3',
            'isdoctor' => 'Isdoctor',
            'f2nodoctor' => 'F2nodoctor',
            'title' => 'คำนำหน้าชื่อ',
            'name' => 'ชื่อ',
            'surname' => 'นามสกุล',
            'usabnormal' => 'ผลการตรวจอัลตราซาวด์',
            'ussuspected' => 'Suspected Case',
            'ctmri_id' => 'ID สำหรับ Link CCA-02.1',
            'ctmri_result' => 'CTMRI',
            'treat_id' => 'Treat ID',
            'treat_result' => 'ผลการรักษา',
            'patho_id' => 'Patho ID',
            'patho_result' => 'Patho',
        ];
    }
}
