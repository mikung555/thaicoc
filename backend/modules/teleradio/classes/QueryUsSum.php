<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace backend\modules\teleradio\classes;

/**
 * Description of QueryUsSum
 *
 * @author Admin
 */
class QueryUsSum {
    public static function getUssum()
    {
      $sql = "select usmobile
            ,count(*) as count
            ,max(f2v1) as dusfind
            ,GROUP_CONCAT(DISTINCT (select name as namehos from all_hospital_thai a WHERE a.hcode=hsitecode),' ' ,'<br>') as hospital
            ,count(if(f2v2a1 = 1,1,null))as abnormal
            ,count(if (f2v2a1b2 in (1,2,3),1,null))as PDF
            ,count(if(f2v6a3b1 = 1,1,null))as suspected
            , count(if(f2v6 in (1),1,null)) as Meet1
            , count(if(f2v6 in (2),1,null)) as Meet2
            , count(if(f2v6a3b1 in (1),1,null))+
             count(if(f2v6a3b2 in (1),1,null)) as Send
            from tb_data_3 
            where usmobile is not null 
            group by usmobile 
            order by usmobile+0 desc";
      
      return \Yii::$app->db->createCommand($sql)->queryAll();   
    }
}
