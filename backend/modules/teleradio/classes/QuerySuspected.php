<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace backend\modules\teleradio\classes;

#use yii\helpers\VarDumper;
#use yii\web\Controller;
use Yii;

class QuerySuspected {
    //put your code here
    private static $_sqlIgnoreSiteCCA02="cca02.hsitecode like '91%' OR cca02.hsitecode like '92%' OR cca02.hsitecode like 'Z%' OR cca02.hsitecode like 'A%' OR cca02.hsitecode like 'B%'";
    private static $_rstatIgnoreCCA02="cca02.rstat='3' OR cca02.rstat='0'";
    private static $_rstatIgnoreREG="reg.rstat='3' OR reg.rstat='0'";
    private static $_rstatIgnoreCCA02p1="cca02p1.rstat='3' OR cca02p1.rstat='0'";
    private static $_rstatIgnoreCCA03="cca03.rstat='3' OR cca03.rstat='0'";
    private static $_rstatIgnoreCCA04="cca04.rstat='3' OR cca04.rstat='0'";
    private static $_rstatIgnoreCCA05="cca05.rstat='3' OR cca05.rstat='0'";
    public static function getList($fromDate = null, $toDate = null,$colum, $sort, $page, $limitrec=0)
    {
        $perpagelimit=$limitrec;
        $startpage=($page-1)*$perpagelimit;
        $_sqlIgnoreSite = self::$_sqlIgnoreSiteCCA02;
        $_rstatIgnoreREG = self::$_rstatIgnoreREG;
        $_rstatIgnoreCCA02 = self::$_rstatIgnoreCCA02;
        $_rstatIgnoreCCA02p1 = self::$_rstatIgnoreCCA02p1;
        $_rstatIgnoreCCA03 = self::$_rstatIgnoreCCA03;
        $_rstatIgnoreCCA04 = self::$_rstatIgnoreCCA04;
        $_rstatIgnoreCCA05 = self::$_rstatIgnoreCCA05;
        $sql = "";
        if( $colum==null and $sort==null ){
            $sql.= "SELECT *,round((a.cca02/b.target)*100,1) as percentcca02 FROM   project84_report_sec2 a INNER JOIN project84_report_sec2_target b ON a.provincecode=b.provincecode where report_id='1' ORDER BY percentcca02 desc ";
        }else{
            $sql.= "SELECT distinct cca02.id,cca02.ptid,cca02.hsitecode,cca02.hptcode,cca02.f2v1,cca02.f2v6a3b1 ";
            $sql.= ",case 1 when max(reg.cohorttype_chartreview)=2 then 'Walk-in' ";
            $sql.= "when max(reg.cohorttype_chartreview)=1 then 'Screening' ";
            $sql.= "end as cohorttype_chartreview ";
            $sql.= ",hospital.name as hospitalname ";
            $sql.= ",hospital.province as province ";
            $sql.= ",reg.title, reg.name, reg.surname ";
            $sql.= ",case 1 when cca02.f2v2a1='0' then '0: Normal' ";
            $sql.= "when cca02.f2v2a1b2='1' then '2a) PDF 1' ";
            $sql.= "when cca02.f2v2a1b2='2' then '2b) PDF 2' ";
            $sql.= "when cca02.f2v2a1b2='3' then '2c) PDF 3' ";
            $sql.= "when cca02.f2v2a1b1='1' then '1a) Mild fatty liver' ";
            $sql.= "when cca02.f2v2a1b1='2' then '1b) Moderate fatty liver' ";
            $sql.= "when cca02.f2v2a1b1='3' then '1c) Severe fatty liver' ";
            $sql.= "when cca02.f2v2a1b3='1' then '3a) Cirrhosis' ";
            $sql.= "when cca02.f2v2a1b4='1' then '4a) Parenchymal change' ";
            $sql.= "else 'ไม่ได้ระบุ' ";
            $sql.= "end as 'cca02abnormal' ";
            $sql.= ",concat(if(f2v2a2b4c1='1','d) Intrahepatic duct stone','') ";
            $sql.= ",if(f2v2a2b5c1='1','e) High echo','') ";
            $sql.= ",if(f2v2a2b6c1='1','f) Low echo','') ";
            $sql.= ",if(f2v2a2b7c1='1','g) Mixed echo','') ";
            $sql.= ",if(f2v2a3b1='1','1. Right lobe','') ";
            $sql.= ",if(f2v2a3b2='1','2. Left lobe','') ";
            $sql.= ",if(f2v2a3b3='1','3. Common bile duct','') ";
            $sql.= ") as 'suspectedcca' ";
            $sql.= ",if( length(cca02p1.id)>0 and cca02p1.f2p1v1 is not null AND NOT($_rstatIgnoreCCA02p1),f2p1v1,'') as ctmridate ";
            $sql.= ",case 1 when f2p1v3='0' AND NOT($_rstatIgnoreCCA02p1) then '0. Normal' ";
            $sql.= "when f2p1v3='1' AND NOT($_rstatIgnoreCCA02p1) then '0. Normal' ";
            $sql.= "when f2p1v3='2' AND NOT($_rstatIgnoreCCA02p1) then '1. Intrahepatic' ";
            $sql.= "when f2p1v3='3' AND NOT($_rstatIgnoreCCA02p1) then '2. Perihilar' ";
            $sql.= "when f2p1v3='4' AND NOT($_rstatIgnoreCCA02p1) then '3. Distal' ";
            $sql.= "when f2p1v3='5' AND NOT($_rstatIgnoreCCA02p1) then '4. Not CCA' ";
            $sql.= "when length(cca02p1.id)>0 and cca02p1.f2p1v1 is not null AND NOT($_rstatIgnoreCCA02p1) then '<font style=\"color:red\">ไม่ได้ระบุ</font>' ";
            $sql.= "end ";
            $sql.= " as ctmriresult ";
            $sql.= ",if( length(cca03.id)>0 and length(cca03.f3v4dvisit)>0 AND NOT($_rstatIgnoreCCA03),max(cca03.f3v4dvisit),'') ";
            $sql.= "as cca03date ";
            $sql.= ",case 1 when min(f3v7)=0 AND NOT($_rstatIgnoreCCA03) then 'Dead' ";
            $sql.= "when max(f3v5a1b1)=1 AND NOT($_rstatIgnoreCCA03) then 'Liver resection' ";
            $sql.= "when max(f3v5a1b2)=1 AND NOT($_rstatIgnoreCCA03) then 'Hilar resection' ";
            $sql.= "when max(f3v5a1b6)=1 AND NOT($_rstatIgnoreCCA03) then 'Whipple' ";
            $sql.= "when max(f3v5a1b3)=1 AND NOT($_rstatIgnoreCCA03) then 'Bypass' ";
            $sql.= "when max(f3v5a1b4)=1 AND NOT($_rstatIgnoreCCA03) then 'Explore/Biopsy' ";
            $sql.= "when max(f3v5a1b5)=1 AND NOT($_rstatIgnoreCCA03) then 'Needle biopsy' ";
            $sql.= "when max(f3v5a2)=1 AND NOT($_rstatIgnoreCCA03) then 'Chemotherapy' ";
            $sql.= "when max(f3v5a3)=1 AND NOT($_rstatIgnoreCCA03) then 'PTBD' ";
            $sql.= "when max(f3v5a4)=1 AND NOT($_rstatIgnoreCCA03) then 'Stent' ";
            $sql.= "when max(f3v4a1)=1 AND NOT($_rstatIgnoreCCA03) then 'OPD' ";
            $sql.= "when max(f3v4a1)=2 AND NOT($_rstatIgnoreCCA03) then 'IPD' ";
            $sql.= "when min(f3v6a1)=1 AND NOT($_rstatIgnoreCCA03) then 'Best supportive' ";
            $sql.= "when length(cca03.id)>0 and length(cca03.f3v4dvisit)>0 AND NOT($_rstatIgnoreCCA03) then 'ไม่ได้ระบุ' ";
            $sql.= "end as cca03result ";
            $sql.= "from tb_data_3 cca02 ";
            $sql.= "left join all_hospital_thai hospital on hospital.hcode=cca02.hsitecode ";
            $sql.= "left join tb_data_1 reg on reg.ptid=cca02.ptid ";
            $sql.= "left join tb_data_4 cca02p1 on cca02p1.ptid=cca02.ptid ";
            $sql.= "left join tb_data_7 cca03 on cca03.ptid=cca02.ptid ";
            $sql.= "where f2v6a3=1 and f2v6a3b1='1' AND NOT($_rstatIgnoreREG) ";
            $sql.= "AND f2v1 between '$fromDate' and '$toDate' ";
            $sql.= "AND NOT($_rstatIgnoreCCA02) ";
            $sql.= "AND NOT($_sqlIgnoreSite) ";
            $sql.= "group by cca02.ptid ";
            $sql.= "ORDER BY $colum $sort LIMIT $startpage,$perpagelimit ";
        }
//        echo $sql;
        $dataProvider = Yii::$app->db->createCommand($sql)->queryAll();
        return $dataProvider;
    }
    
    public static function getAllpage($fromDate = null, $toDate = null,$colum, $sort, $page, $limitrec=0)
    {
        $perpagelimit=$limitrec;
        $startpage=($page-1)*$perpagelimit;
        $_sqlIgnoreSite = self::$_sqlIgnoreSiteCCA02;
        $_rstatIgnoreCCA02 = self::$_rstatIgnoreCCA02;
        $sql = "select count(distinct id) as allRecord ";
        $sql.= ",count(distinct ptid) as allPatient ";
        $sql.= ",ceil(count(distinct id)/$limitrec) as countPage ";
        $sql.= "from tb_data_3 cca02 ";
        $sql.= "where f2v6a3b1='1' ";
        $sql.= "AND f2v1 between '$fromDate' and '$toDate' ";
        $sql.= "AND NOT($_rstatIgnoreCCA02) ";
        $sql.= "AND NOT($_sqlIgnoreSite) ";
        //echo $sql;
        $data = Yii::$app->db->createCommand($sql)->queryOne();
        return $data;
    }
    
    
}