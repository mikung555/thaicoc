<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace backend\modules\teleradio\classes;

/**
 * Description of QueryUs
 *
 * @author Admin
 */
class QueryUs {

    public static function getUsdocter()
    {
      $sql = "select 
f2doctorcode,
doctorfullname, 
count(*)as count
, max(f2v1) as doctordate
, count(distinct if(a.rstat in (2,4),ptid,null)) as submit
, count(distinct if(a.rstat in (1,2,4),ptid,null)) as allsubmit
, count(distinct if(a.f2v2a1 in (0),ptid,null)) as normal
, count(distinct if(a.f2v2a1 in (1),ptid,null)) as abnormal
, count(distinct if(a.f2v2a2 in (1,2),ptid,null)) as LiverMass
,count(distinct if(a.f2v3a2 in (1),ptid,null)) + 
count(distinct if(a.f2v3a3 in (1),ptid,null)) + 
count(distinct if(a.f2v3a4 in (1),ptid,null))  as Gallbladder
,count(distinct if(a.f2v4a2 in (1),ptid,null)) +
count(distinct if(a.f2v4a3 in (1),ptid,null)) +
count(distinct if(a.f2v4a4 in (1),ptid,null)) +
count(distinct if(a.f2v4a5 in (1),ptid,null)) as Kidney
,count(distinct if(a.f2v6 in (1)and f2v6a3b1<>1,ptid,null))as Meet1
,count(distinct if(a.f2v6 in (2)and f2v6a3b1<>1,ptid,null)) as Meet2
,count(distinct if(f2v6a3b2 = 1,ptid,null)) +
count(distinct if(f2v6a3b1 = 1,ptid,null)) as Send
,count(distinct if(f2v6a3b1 = 1,ptid,null))as suspected
from tb_data_3 a 
INNER JOIN doctor_all b on b.id = a.f2doctorcode
where f2doctorcode is not null and doctorfullname <>''
and f2v1<=NOW() group by f2doctorcode order by max(f2v1) desc
";
      return \Yii::$app->db->createCommand($sql)->queryAll();   
    }
        public static function getUsonedocter($pid='')
    {
      $sql = "select 
f2doctorcode,
doctorfullname, 
count(*)as count
, max(f2v1) as doctordate
, count(distinct if(a.rstat in (2,4),ptid,null)) as submit
, count(distinct if(a.rstat in (1,2,4),ptid,null)) as allsubmit
, count(distinct if(a.f2v2a1 in (0),ptid,null)) as normal
, count(distinct if(a.f2v2a1 in (1),ptid,null)) as abnormal
, count(distinct if(a.f2v2a2 in (1,2),ptid,null)) as LiverMass
,count(distinct if(a.f2v3a2 in (1),ptid,null)) + 
count(distinct if(a.f2v3a3 in (1),ptid,null)) + 
count(distinct if(a.f2v3a4 in (1),ptid,null))  as Gallbladder
,count(distinct if(a.f2v4a2 in (1),ptid,null)) +
count(distinct if(a.f2v4a3 in (1),ptid,null)) +
count(distinct if(a.f2v4a4 in (1),ptid,null)) +
count(distinct if(a.f2v4a5 in (1),ptid,null)) as Kidney
,count(distinct if(a.f2v6 in (1)and f2v6a3b1<>1,ptid,null))as Meet1
,count(distinct if(a.f2v6 in (2)and f2v6a3b1<>1,ptid,null)) as Meet2
,count(distinct if(f2v6a3b2 = 1,ptid,null)) +
count(distinct if(f2v6a3b1 = 1,ptid,null)) as Send
,count(distinct if(f2v6a3b1 = 1,ptid,null))as suspected
from tb_data_3 a 
INNER JOIN doctor_all b on b.id = a.f2doctorcode
where f2doctorcode is not null and doctorfullname <>'' and b.pid=:pid
and f2v1<=NOW() group by f2doctorcode order by max(f2v1) desc
";
      $params=[
         ':pid'=>$pid,
      ];
      return \Yii::$app->db->createCommand($sql,$params)->queryAll();   
    }
        public static function getSubmit($code='')
    {
      $sql = "select 
count(distinct if(a.rstat in (2,4),ptid,null)) as Submit
,count(distinct if(a.rstat in (1,2,4),ptid,null)) as TotalAll
from tb_data_3 a
where f2doctorcode=:code and f2v1<=NOW()";
      $params=[
         ':code'=>$code,
      ];
      return \Yii::$app->db->createCommand($sql,$params)->queryOne();   
    }
    public static function getNormal($code='')
    {
      $sql = "select 
                count(distinct if(a.f2v2a1 in (0),ptid,null)) as Normal
               ,count(distinct if(a.f2v2a1 in (1),ptid,null)) as Abnormal
               from tb_data_3 a 
               where f2doctorcode=:code and f2v1<=NOW()";
      $params=[
         ':code'=>$code,
      ];
      return \Yii::$app->db->createCommand($sql,$params)->queryOne();   
    }
        public static function getAbnormal($code='')
    {
      $sql = "select 
count(distinct if(a.f2v2a1b1 in (1,2,3),ptid,null)) as FattyLiver
,count(distinct if(a.f2v2a1b2 in (1,2,3),ptid,null)) as PDF
,count(distinct if(a.f2v2a1b3 in (1),ptid,null)) as Cirrhosis
,count(distinct if(a.f2v2a1b4 in (1),ptid,null)) as ParenchymalChange
from tb_data_3 a 
where f2doctorcode=:code and f2v1<=NOW()";
      $params=[
         ':code'=>$code,
      ];
      return \Yii::$app->db->createCommand($sql,$params)->queryOne();   
    }
        public static function getPdf($code='')
    {
      $sql = "select 
count(distinct if(a.f2v2a1b2 in (1),ptid,null)) as PDF1
,count(distinct if(a.f2v2a1b2 in (2),ptid,null)) as PDF2
,count(distinct if(a.f2v2a1b2 in (3),ptid,null)) as PDF3
from tb_data_3 a 
where f2doctorcode=:code and f2v1<=NOW()";
      $params=[
         ':code'=>$code,
      ];
      return \Yii::$app->db->createCommand($sql,$params)->queryOne();   
    }
    public static function getFattyliver($code='')
    {
      $sql = "select 
count(distinct if(a.f2v2a1b1 in (1),ptid,null)) as MildFattyLiver
,count(distinct if(a.f2v2a1b1 in (2),ptid,null)) as ModerateFattyLiver
,count(distinct if(a.f2v2a1b1 in (3),ptid,null)) as SevereFattyLiver
from tb_data_3 a 
where f2doctorcode=:code and f2v1<=NOW()";
      $params=[
         ':code'=>$code,
      ];
      return \Yii::$app->db->createCommand($sql,$params)->queryOne();   
    }
    public static function getLivermass($code='')
    {
      $sql = "select 
               count(distinct if(a.f2v2a2 in (1),ptid,null)) as SingleMass
               ,count(distinct if(a.f2v2a2 in (2),ptid,null)) as MultipleMass
               from tb_data_3 a 
               where f2doctorcode=:code and f2v1<=NOW() ";
      $params=[
         ':code'=>$code,
      ];
      return \Yii::$app->db->createCommand($sql,$params)->queryOne();   
    }
    public static function getMutiMass($code='')
    {
      $sql = "select 
count(distinct if(a.f2v2a2b1c1 in (1),ptid,null)) as mass1
,count(distinct if(a.f2v2a2b2c1 in (1),ptid,null)) as mass2
,count(distinct if(a.f2v2a2b3c1 in (1),ptid,null)) as mass3
,count(distinct if(a.f2v2a2b4c1 in (1),ptid,null)) as mass4
,count(distinct if(a.f2v2a2b5c1 in (1),ptid,null)) as mass5
,count(distinct if(a.f2v2a2b6c1 in (1),ptid,null)) as mass6
,count(distinct if(a.f2v2a2b7c1 in (1),ptid,null)) as mass7
from tb_data_3 a 
where f2doctorcode=:code  and f2v1<=NOW() and f2v2a2=1";
      $params=[
         ':code'=>$code,
      ];
      return \Yii::$app->db->createCommand($sql,$params)->queryOne();   
    }
    public static function getGallbladder($code='')
    {
      $sql = "select 
            count(distinct if(a.f2v3a1 in (1),ptid,null)) as gall1
            ,count(distinct if(a.f2v3a2 in (1),ptid,null)) as gall2
            ,count(distinct if(a.f2v3a3 in (1),ptid,null)) as gall3 
            ,count(distinct if(a.f2v3a4 in (1),ptid,null)) as gall4
            ,count(distinct if(a.f2v3a5 in (1),ptid,null)) as gall5
               from tb_data_3 a 
               where f2doctorcode=:code and f2v1<=NOW() ";
      $params=[
         ':code'=>$code,
      ];
      return \Yii::$app->db->createCommand($sql,$params)->queryOne();   
    }
    public static function getKidey($code='')
    {
      $sql = "select 
            count(distinct if(a.f2v4a1 in (1),ptid,null)) as kid1
            ,count(distinct if(a.f2v4a2 in (1),ptid,null)) as kid2
            ,count(distinct if(a.f2v4a3 in (1),ptid,null)) as kid3
            ,count(distinct if(a.f2v4a4 in (1),ptid,null)) as kid4
            ,count(distinct if(a.f2v4a5 in (1),ptid,null)) as kid5
            ,count(distinct if(a.f2v4a6 in (1),ptid,null)) as kid6
               from tb_data_3 a 
               where f2doctorcode=:code and f2v1<=NOW() ";
      $params=[
         ':code'=>$code,
      ];
      return \Yii::$app->db->createCommand($sql,$params)->queryOne();   
    }
    public static function getMeetdoctor($code='')
    {
      $sql = "select 
count(distinct if(a.f2v6 in (1) and f2v6a3b1<>1,ptid,null)) as Meet1
, count(distinct if(a.f2v6 in (2) and f2v6a3b1<>1,ptid,null)) as Meet2
,count(distinct if(f2v6a3b1 = 1,ptid,null))as suspected
, count(distinct if(a.f2v6a3b2 in (1),ptid,null)) as other
from tb_data_3 a 
where f2doctorcode=:code
and f2v1<=NOW() and a.ptid is not null";
      $params=[
         ':code'=>$code,
      ];
      return \Yii::$app->db->createCommand($sql,$params)->queryOne();   
    }
       public static function getListsubmodal($code='', $param='')
    {
      $sql = "select
b.hsitecode,
b.hncode,
b.cid,
a.f2v1,
b.hptcode,
CONCAT(b.title,b.`name`,'  ',b.surname) as fullname,
b.age
from tb_data_3 a 
INNER JOIN tb_data_1 b ON b.ptid = a.ptid
where a.f2doctorcode=:code and 
$param
and a.f2v1<=NOW()
and b.ptid is not null
GROUP BY b.ptid order by max(a.f2v1) desc";
      $params=[
         ':code'=>$code,
      ];
      return \Yii::$app->db->createCommand($sql,$params)->queryAll();   
    }
    public static function getDoctorpid($user_id='')
    {
      $sql = "select pid from doctor_all where pid=:user_id ";
      $params=[
         ':user_id'=>$user_id,
      ];
      return \Yii::$app->db->createCommand($sql,$params)->queryScalar();
    }
    public static function checkUserview()
    {
        $admin = \Yii::$app->user->can('administrator');
        $user_id = \Yii::$app->user->identity->userProfile->user_id;
        $pid = QueryUs::getDoctorpid($user_id);
        $specialID = [ //user_id
            '1435745159010043377',
            '1435745159010043375',
        ];
        if ($admin==TRUE or in_array($user_id,$specialID) or $pid !=FALSE){ 
            $view=TRUE;
        }else{
            $view=FALSE; 
        }
        $check = ([
            'view' => $view,
            'user_id' => $user_id,
            'pid' => $pid,
        ]);
      return $check;
    }
}
