<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


namespace backend\modules\teleradio\classes;

#use yii\helpers\VarDumper;
#use yii\web\Controller;
use Yii;

class SummarySuspected {
    //put your code here
    private static $_cdofdatareal = "(cca02.rstat<>'3' AND NOT(cca02.hsitecode like 'Z%' OR cca02.hsitecode like 'A%' OR cca02.hsitecode like '91%' OR cca02.hsitecode like '92%' OR cca02.hsitecode like '93%' OR cca02.hsitecode like '94%') 
and length(cca02.hsitecode)>0)";
    private static $_cdofdatacca02timecascap = "(f2v1 between '2013-02-09' and NOW())";
    // เงื่อนไขตามการเลือก หรือเพื่อนับ
    private static $_csuspectedprotocol = "f2v6a3b1='1'";
    private static $_csuspectedprotocolScreening = "f2v6a3b1='1' and cohorttype_chartreview='1' ";
    private static $_csuspectedprotocolWalkin = "f2v6a3b1='1' and cohorttype_chartreview='2' ";
    private static $_csuspectedCTMRIScreening = "ctmri_id>0 and f2v6a3b1='1' and cohorttype_chartreview='1' ";
    private static $_csuspectedCTMRIWalkin = "ctmri_id>0 and f2v6a3b1='1' and cohorttype_chartreview='2' ";
    private static $_csuspectedprotocolpdf = "f2v6a3b1='1' AND (f2v2a1b2='1' OR f2v2a1b2='2' OR f2v2a1b2='3')";
    private static $_csuspectedbycondition = "(f2v2a2b4c1='1' OR f2v2a2b5c1='1' OR f2v2a2b6c1='1' OR f2v2a2b7c1='1' OR f2v2a3b1='1' OR f2v2a3b2='1' OR f2v2a3b1='1')";
    private static $_csuspectedbyconditionpdf = "(f2v2a2b4c1='1' OR f2v2a2b5c1='1' OR f2v2a2b6c1='1' OR f2v2a2b7c1='1' OR f2v2a3b1='1' OR f2v2a3b2='1' OR f2v2a3b1='1') AND (f2v2a1b2='1' OR f2v2a1b2='2' OR f2v2a1b2='3')";
    private static $_csuspectedconditionprotocoltrue = "(f2v2a2b4c1='1' OR f2v2a2b5c1='1' OR f2v2a2b6c1='1' OR f2v2a2b7c1='1' OR f2v2a3b1='1' OR f2v2a3b2='1' OR f2v2a3b1='1') AND f2v6a3b1='1'";
    private static $_csuspectedconditionprotocolinvalid = "(f2v2a2b4c1='1' OR f2v2a2b5c1='1' OR f2v2a2b6c1='1' OR f2v2a2b7c1='1' OR f2v2a3b1='1' OR f2v2a3b2='1' OR f2v2a3b1='1') AND (f2v6a3b1<>'1' OR f2v6a3b1 is null)";
    private static $_csuspectedconditioninvalidprotocol ="NOT (f2v2a2b4c1='1' OR f2v2a2b5c1='1' OR f2v2a2b6c1='1' OR f2v2a2b7c1='1' OR f2v2a3b1='1' OR f2v2a3b2='1' OR f2v2a3b1='1') AND f2v6a3b1='1'";
    public static function getOverAll() {
        $_csuspectedprotocolScreening = self::$_csuspectedprotocolScreening;
        $_csuspectedprotocolWalkin = self::$_csuspectedprotocolWalkin;
        $_csuspectedCTMRIScreening = self::$_csuspectedCTMRIScreening;
        $_csuspectedCTMRIWalkin = self::$_csuspectedCTMRIWalkin;
        $sql = "select hsitecode,count(distinct ptid) as dall
,count(distinct if(f2v6a3b1='1',ptid,null)) as suspected
, count(if(ctmri_id>0,ptid,NULL)) as CTMRI
, round((count(if(ctmri_id>0,ptid,NULL))/count(distinct if(f2v6a3b1='1',ptid,null)))*100,1)as '%CTMRI'
, count(distinct if(".$_csuspectedprotocolScreening.",ptid,NULL)) as '_csuspectedprotocolScreening' 
, count(distinct if(".$_csuspectedprotocolWalkin.",ptid,NULL)) as '_csuspectedprotocolWalkin' 
, count(distinct if(".$_csuspectedCTMRIScreening.",ptid,NULL)) as '_csuspectedCTMRIScreening' 
, count(distinct if(".$_csuspectedCTMRIWalkin.",ptid,NULL)) as '_csuspectedCTMRIWalkin' 
, count(if(ctmri_f2p1v3='1',ptid,NULL)) as Intra
, count(if(ctmri_f2p1v3='2',ptid,NULL)) as Peri
, count(if(ctmri_f2p1v3='3',ptid,NULL)) as Distal
, count(if(treat_id>0,ptid,null)) as Treat
, round((count(if(treat_id>0,ptid,null))/count(distinct if(f2v6a3b1='1',ptid,null)))*100,1)as '%Treat'
, count(if(treat_f3v5a1b5='1',ptid,null)) as 'Needle biopsy'
, count(if(treat_f3v5a1b1='1' OR treat_f3v5a1b2='1' OR treat_f3v5a1b6='1',ptid,null)) as 'Cure'
, round((count(if(treat_f3v5a1b1='1' OR treat_f3v5a1b2='1' OR treat_f3v5a1b6='1',ptid,null))/count(distinct if(f2v6a3b1='1',ptid,null)))*100,1)as '%Cure'
, count(if(treat_f3v5a1b1='1',ptid,null)) as 'Liver resection'
, count(if(treat_f3v5a1b2='1',ptid,null)) as 'Hilar'
, count(if(treat_f3v5a1b6='1',ptid,null)) as 'Whipple'
, count(if(treat_f3v5a1b3='1',ptid,null)) as 'Bypass'
, count(if(treat_f3v5a1b4='1',ptid,null)) as 'Exploratory/Biopsy'
, count(if(patho_id>0,ptid,null)) as PathoChecked
, round((count(if(patho_id>0,ptid,null))/count(distinct if(f2v6a3b1='1',ptid,null)))*100,1)as '%Patho'
, count(if(patho_f4v4='1' OR patho_f4v4='2' OR patho_f4v4='3',ptid,NULL)) as PCCA
, round((count(if(patho_f4v4='1' OR patho_f4v4='2' OR patho_f4v4='3',ptid,null))/count(distinct if(f2v6a3b1='1',ptid,null)))*100,1)as '%PCCA'
, count(if(patho_f4v4='1',ptid,NULL)) as PIntra
, count(if(patho_f4v4='2',ptid,NULL)) as PPeri
, count(if(patho_f4v4='3',ptid,NULL)) as PDistal 
, count(if(patho_staging='0' OR patho_staging='I' OR patho_staging='II',ptid,null)) as 'early-stage'
, count(if(patho_staging like 'III%' OR patho_staging like 'IV%' OR patho_staging like 'V%',ptid,null)) as 'late-stage'
, count(if(patho_staging='UK',ptid,null)) as 'unknown'
, count(if( (patho_f4v4='1' OR patho_f4v4='2' OR patho_f4v4='3') AND (length(patho_staging)=0 or patho_staging is null),ptid,NULL)) as 'TBC'
from tb_data_3_suspected 
where rstat<>'3' and length(hsitecode)>0 and f2v1 between '2013-02-09' and NOW() ";
        $out = Yii::$app->db->createCommand($sql)->queryOne();
        return $out;
    }
    
    public static function setTableByCondition(){
        $_cdofdatareal = self::$_cdofdatareal;
        $_cdofdatacca02timecascap = self::$_cdofdatacca02timecascap;
        $_csuspectedprotocol = self::$_csuspectedprotocol;
        // create record of table suspected by condition
        //echo "<br /><br /><br /><br /><br />";
        $sql = "TRUNCATE cascap_log.cca02_suspected_protocol";
        //echo "<br />".$sql;
        Yii::$app->db->createCommand($sql)->execute();
        $sql = "insert into cascap_log.cca02_suspected_protocol
select * from tb_data_3 cca02
where ".$_cdofdatareal."
and ".$_cdofdatacca02timecascap."
and length(cca02.hsitecode)>0
and ".$_csuspectedprotocol;
        //echo "<br />".$sql;
        Yii::$app->db->createCommand($sql)->execute();
        return $sql;
    }


    public static function getConditionCount(){
        $_csuspectedprotocol = self::$_csuspectedprotocol;
        $_csuspectedprotocolScreening = self::$_csuspectedprotocolScreening;
        $_csuspectedprotocolWalkin = self::$_csuspectedprotocolWalkin;
        $_csuspectedCTMRIScreening = self::$_csuspectedCTMRIScreening;
        $_csuspectedCTMRIWalkin = self::$_csuspectedCTMRIWalkin;
        $_csuspectedprotocolpdf = self::$_csuspectedprotocolpdf;
        $_csuspectedbycondition = self::$_csuspectedbycondition;
        $_csuspectedbyconditionpdf = self::$_csuspectedbyconditionpdf;
        $_csuspectedconditionprotocoltrue = self::$_csuspectedconditionprotocoltrue;
        $_csuspectedconditionprotocolinvalid = self::$_csuspectedconditionprotocolinvalid;
        $_csuspectedconditioninvalidprotocol = self::$_csuspectedconditioninvalidprotocol;
        $sql = "SELECT 
count(distinct ptid) as 'จำนวนคนที่ Ultrasound'
,count(distinct ptid,f2v1) as 'จำนวนครั้งที่ Ultrasound'
,count(distinct if(".$_csuspectedprotocol.",ptid,NULL)) as 'ข้อมูลที่ Click Suspected เข้ามาจริงๆ (f2v6a3b1=1)' 
,count(distinct if(".$_csuspectedprotocolpdf.",ptid,NULL)) as '_csuspectedprotocolpdf' 
,count(distinct if( ".$_csuspectedbycondition.",ptid,null)) as '_csuspectedbycondition'
,count(distinct if( ".$_csuspectedbyconditionpdf.",ptid,null)) as '_csuspectedbyconditionpdf'
,count(distinct if( ".$_csuspectedconditionprotocoltrue.",ptid,NULL)) as '_csuspectedconditionprotocoltrue'
,count(distinct if( ".$_csuspectedconditionprotocoltrue.",id,NULL)) as '_csuspectedconditionprotocoltrue_visit'
,count(distinct if( ".$_csuspectedconditionprotocolinvalid.",ptid,NULL)) as '_csuspectedconditionprotocolinvalid'
,count(distinct if( ".$_csuspectedconditionprotocolinvalid.",id,NULL)) as '_csuspectedconditionprotocolinvalid_visit'
,count(distinct if( ".$_csuspectedconditioninvalidprotocol.",ptid,NULL)) as '_csuspectedconditioninvalidprotocol'
,count(distinct if( ".$_csuspectedconditioninvalidprotocol.",id,NULL)) as '_csuspectedconditioninvalidprotocol_visit'
FROM 
tb_data_3 
WHERE rstat<>'3' AND NOT(hsitecode like 'Z%' OR hsitecode like 'A%' OR hsitecode like '91%' OR hsitecode like '92%' OR hsitecode like '93%' OR hsitecode like '94%') 
and length(hsitecode)>0 and f2v1 between '2013-02-09' and NOW() ";
        $out = Yii::$app->db->createCommand($sql)->queryOne();
        return $out;
    }
    
    public static function getListSuspectedCondition(){
        $request = Yii::$app->request;
        $_csuspectedbyconditionpdf = self::$_csuspectedbyconditionpdf;
        $_csuspectedconditionprotocoltrue = self::$_csuspectedconditionprotocoltrue;
        $_csuspectedconditionprotocolinvalid = self::$_csuspectedconditionprotocolinvalid;
        $_csuspectedconditioninvalidprotocol = self::$_csuspectedconditioninvalidprotocol;
        $sql = "select distinct reg.title,reg.name,reg.surname ";
        $sql.= ",cca02.id,cca02.ptid,cca02.hsitecode,cca02.hptcode,cca02.f2v1 ";
        $sql.= "";
        $sql.= "FROM 
tb_data_3 as cca02
left join tb_data_1 as reg on reg.ptid=cca02.ptid and reg.hsitecode=cca02.hsitecode
WHERE cca02.rstat<>'3' AND NOT(cca02.hsitecode like 'Z%' OR cca02.hsitecode like 'A%' OR cca02.hsitecode like '91%' OR cca02.hsitecode like '92%' OR cca02.hsitecode like '93%' OR cca02.hsitecode like '94%') 
and length(cca02.hsitecode)>0 and f2v1 between '2013-02-09' and NOW() ";
        if( strlen($request->get('list'))>0 ){
            switch ($request->get('list')){
                case '_csuspectedconditionprotocoltrue_visit' :
                    $sql.= "AND ".$_csuspectedconditionprotocoltrue." ";
                    break;
                case '_csuspectedconditionprotocolinvalid_visit' :
                    $sql.= "AND ".$_csuspectedconditionprotocolinvalid." ";
                    break;
                case '_csuspectedconditioninvalidprotocol_visit' :
                    $sql.= "AND ".$_csuspectedconditioninvalidprotocol." ";
                    break;
                default :
                    $sql.="AND 0 ";
                    break;
            }
        }else{
            $sql.="AND 0 ";
        }
        $sql.= "order by f2v1 desc ";
        //echo "<br /><br /><br /><br /><br />";
        //echo $sql;
        $out = Yii::$app->db->createCommand($sql)->queryAll();
        if( 0 ){
            echo "<pre align='left'>";
            print_r($out);
            echo "</pre>";
        }
        return $out;
    }
    
    
}


