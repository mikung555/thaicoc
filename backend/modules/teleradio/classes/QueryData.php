<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace backend\modules\teleradio\classes;

/**
 * Description of QueryData
 *
 * @author chaiwat
 */
use Yii;

class QueryData {
    //put your code here
    public static $_sqlIgnoreSiteCCA02="length(trim(cca02.hsitecode))=0 OR cca02.hsitecode like '00000' OR cca02.hsitecode like '90%' OR cca02.hsitecode like '91%' OR cca02.hsitecode like '92%' OR cca02.hsitecode like 'Z%' OR cca02.hsitecode like 'A%' OR cca02.hsitecode like 'B%'";
    public static $_rstatIgnoreCCA02="cca02.rstat='3' OR cca02.rstat='0'";
    public static $_rstatIgnoreREG="reg.rstat='3' OR reg.rstat='0'";
    public static $_rstatIgnoreCCA01="cca01.rstat='3' OR cca01.rstat='0'";
    public static $_rstatIgnoreCCA02p1="cca02p1.rstat='3' OR cca02p1.rstat='0'";
    public static $_rstatIgnoreCCA03="cca03.rstat='3' OR cca03.rstat='0'";
    public static $_rstatIgnoreCCA04="cca04.rstat='3' OR cca04.rstat='0'";
    public static $_rstatIgnoreCCA05="cca05.rstat='3' OR cca05.rstat='0'";
    public static $_ezf_id_reg      = "1437377239070461301";
    public static $_ezf_id_cca01    = "1437377239070461302";
    public static $_ezf_id_cca02    = "1437619524091524800";
    public static $_ezf_id_cca02p1  = "1454041742064651700";
    public static $_ezf_id_cca03    = "1451381257025574200";
    public static $_ezf_id_cca04    = "1452061550097822200";
    public static $_ezf_id_cca05    = "1440515302053265900";

    public static function getWhereSQL($fromDate = null, $toDate = null, $conditionTable, $sqlHospital, $mainTable){
        
        $dbreport = "cascap_report1";
        //$perpagelimit=$limitrec;
        #$startpage=($page-1)*$perpagelimit;
        $_sqlIgnoreSite = self::$_sqlIgnoreSiteCCA02;
        $_rstatIgnoreREG = self::$_rstatIgnoreREG;
        $_rstatIgnoreCCA01 = self::$_rstatIgnoreCCA01;
        $_rstatIgnoreCCA02 = self::$_rstatIgnoreCCA02;
        $_rstatIgnoreCCA02p1 = self::$_rstatIgnoreCCA02p1;
        $_rstatIgnoreCCA03 = self::$_rstatIgnoreCCA03;
        $_rstatIgnoreCCA04 = self::$_rstatIgnoreCCA04;
        $_rstatIgnoreCCA05 = self::$_rstatIgnoreCCA05;
        
        # $sql for return
        $sql = "";
        
        # main table
        if( $mainTable=='cca02' ){
            $sql.= "from `".$dbreport."`.tb_data_3 cca02 ";
        }else if( $mainTable=='cca02p1' ){
            $sql.= "from `".$dbreport."`.tb_data_4 cca02p1 ";
        }else if( $mainTable=='cca03' ){
            $sql.= "from `".$dbreport."`.tb_data_7 cca03 ";
        }else if( $mainTable=='cca04' ){
            $sql.= "from `".$dbreport."`.tb_data_8 cca04 ";
        }else if( $mainTable=='cca05' ){
            $sql.= "from `".$dbreport."`.tb_data_11 cca05 ";
        }else if( $mainTable=='cca01' ){
            $sql.= "from `".$dbreport."`.tb_data_2 cca01 ";
        }else if( $mainTable=='reg' ){
            $sql.= "from `".$dbreport."`.tb_data_1 reg ";
        }else{
            # default
            $sql.= "from `".$dbreport."`.tb_data_3 cca02 ";
        }
        $sql.= "left join all_hospital_thai hospital on hospital.hcode=$mainTable.hsitecode ";
        # join
        #if( $conditionTable['_reg']=='1' && $mainTable!='reg' ){
        if( $mainTable!='reg' ){
            $sql.= "left join `".$dbreport."`.tb_data_1 reg on reg.ptid=$mainTable.ptid ";
        }
        #if( $conditionTable['_cca01']=='1' && $mainTable!='cca01' ){
        if( $mainTable!='cca01' ){
            $sql.= "left join `".$dbreport."`.tb_data_2 cca01 on cca01.ptid=$mainTable.ptid ";
        }
        #if( $conditionTable['_cca02']=='1' && $mainTable!='cca02' ){
        if( $mainTable!='cca02' ){
            $sql.= "left join `".$dbreport."`.tb_data_3 cca02 on cca02.ptid=$mainTable.ptid ";
        }
        #if( $conditionTable['_cca02p1']=='1' && $mainTable!='cca02p1' ){
        if( $mainTable!='cca02p1' ){
            $sql.= "left join `".$dbreport."`.tb_data_4 cca02p1 on cca02p1.ptid=$mainTable.ptid ";
        }
        if( $mainTable!='cca03' ){
            $sql.= "left join `".$dbreport."`.tb_data_7 cca03 on cca03.ptid=$mainTable.ptid ";
        }
        if( $mainTable!='cca04' ){
            $sql.= "left join `".$dbreport."`.tb_data_8 cca04 on cca04.ptid=$mainTable.ptid ";
        }
        if( $mainTable!='cca05' ){
            $sql.= "left join `".$dbreport."`.tb_data_11 cca05 on cca05.ptid=$mainTable.ptid ";
        }
        # start condition 
        $sql.= "where ";
        # sitecode ที่นำออกจากรายงาน
        $sql.= "NOT(".str_replace("cca02.", $mainTable.".", $_sqlIgnoreSite).") ";
        #$sql.="where NOT($mainTable.rstat ='3' OR $mainTable.rstat='0') ";
        # ignore record 
        if( $mainTable=='reg' || $conditionTable['_reg']=='1' ){
            $sql.= "and NOT(".$_rstatIgnoreREG.") ";
            $sql.= "and reg.id is not null ";
        }
        if( $mainTable=='reg' ){
            $sql.= "AND create_date between '$fromDate' and '$toDate' ";
        }
        if( $mainTable=='cca01' || $conditionTable['_cca01']=='1' ){
            $sql.= "and NOT(".$_rstatIgnoreCCA01.") ";
            $sql.= "and cca01.id is not null ";
        }
        
        if( $mainTable=='cca01' ){
            $sql.= "AND f1vdcomp between '$fromDate' and '$toDate' ";
        }
        if( $mainTable=='cca02' ){
            $sql.= "AND f2v1 between '$fromDate' and '$toDate' ";
        }
        if( $mainTable=='cca02' || $conditionTable['_cca02']=='1' ){
            $sql.= "and NOT(".$_rstatIgnoreCCA02.") ";
            $sql.= "and cca02.id is not null ";
        }
        if( $mainTable=='cca02p1' || $conditionTable['_cca02p1']=='1' ){
            
            $sql.= "and NOT(".$_rstatIgnoreCCA02p1.") ";
            $sql.= "and cca02p1.id is not null ";
        }
        if( $mainTable=='cca03' || $conditionTable['_cca03']=='1' ){
            
            $sql.= "and NOT(".$_rstatIgnoreCCA03.") ";
            $sql.= "and cca03.id is not null ";
        }
        if( $mainTable=='cca04' || $conditionTable['_cca04']=='1' ){
            $sql.= "and NOT(".$_rstatIgnoreCCA04.") ";
            $sql.= "and cca04.id is not null ";
        } 
        if( $mainTable=='cca05' || $conditionTable['_cca05']=='1' ){
            $sql.= "and NOT(".$_rstatIgnoreCCA05.") ";
            $sql.= "and cca05.id is not null ";
        }
        if( 0 ){
            $sql.= "and NOT(".$_rstatIgnoreCCA01.") ";
            $sql.= "and NOT(".$_rstatIgnoreCCA02.") ";
            $sql.= "and NOT(".$_rstatIgnoreCCA02p1.") ";
            $sql.= "and NOT(".$_rstatIgnoreCCA03.") ";
            $sql.= "and NOT(".$_rstatIgnoreCCA04.") ";
            $sql.= "and NOT(".$_rstatIgnoreCCA05.") ";
        }
        $sql.= $sqlHospital;
        $sql.= "and NOT (hospital.hcode like 'A%' or hospital.hcode like 'B%' or hospital.hcode like 'C%' or hospital.hcode like 'Z%' ";
        $sql.= "or hospital.hcode like '90%' or hospital.hcode like '91%' or hospital.hcode like '92%' or hospital.hcode like '93%' ) ";
        $sql.= "group by $mainTable.ptid ";
        
//        echo "<br />".$sql;
        return $sql;
        
    }
    
    
    public static function getList($sqlWhere, $mainTable ,$ezf_id ,$colum, $sort, $page, $limitrec=0)
    {
        $perpagelimit=$limitrec;
        $startpage=($page-1)*$perpagelimit;
        #$_sqlIgnoreSite = self::$_sqlIgnoreSiteCCA02;
        $_rstatIgnoreREG = self::$_rstatIgnoreREG;
        $_rstatIgnoreCCA01 = self::$_rstatIgnoreCCA01;
        $_rstatIgnoreCCA02 = self::$_rstatIgnoreCCA02;
        $_rstatIgnoreCCA02p1 = self::$_rstatIgnoreCCA02p1;
        $_rstatIgnoreCCA03 = self::$_rstatIgnoreCCA03;
        $_rstatIgnoreCCA04 = self::$_rstatIgnoreCCA04;
        $_rstatIgnoreCCA05 = self::$_rstatIgnoreCCA05;
        
        $_ezf_id_reg        = self::$_ezf_id_reg;
        $_ezf_id_cca01      = self::$_ezf_id_cca01;
        $_ezf_id_cca02      = self::$_ezf_id_cca02;
        $_ezf_id_cca02p1    = self::$_ezf_id_cca02p1;
        $_ezf_id_cca03      = self::$_ezf_id_cca03;
        $_ezf_id_cca04      = self::$_ezf_id_cca04;
        $_ezf_id_cca05      = self::$_ezf_id_cca05;

        
        $_usertable = Yii::$app->user->identity->id;
        
        # clear table
        $sqlClear = "drop table IF EXISTS cascap_log.summary_data_$_usertable ";
        Yii::$app->db->createCommand($sqlClear)->execute();
        
        # assign ezf_id from main table
        
        
        $sql = "create table cascap_log.summary_data_$_usertable ";
        $sql.= "SELECT distinct $mainTable.id, $ezf_id as 'ezf_id', '$mainTable' as '_ref_table', $mainTable.ptid,$mainTable.hsitecode,$mainTable.hptcode ";
        $sql.= ",$mainTable.rstat ";
        $sql.= ",hospital.name as hospitalname ";
        $sql.= ",hospital.province as province ";
        $sql.= ",hospital.amphur as amphur ";
        $sql.= ",hospital.zone_code as zone ";
        #if( $conditionTable['_reg']=='1' ){
        if( 1 ){
            $sql.= ",$_ezf_id_reg as _ezf_id_reg ";
            $sql.= ",max(reg.id)as '_id_reg' ";
            $sql.= ",case 1 when max(reg.cohorttype_chartreview)=2 then 'Walk-in' ";
            $sql.= "when max(reg.cohorttype_chartreview)=1 then 'Screening' ";
            $sql.= "end as cohorttype_chartreview ";
            $sql.= ",reg.cid, reg.hncode ";
            $sql.= ",reg.title, reg.name, reg.surname ";
        }
        #if( $conditionTable['_cca02']=='1' ){
        if( 1 ){
            $sql.= ",$_ezf_id_cca01 as _ezf_id_cca01 ";
            $sql.= ",max(cca01.id) as _id_cca01 ";
            $sql.= ",cca01.f1vdcomp ";
        }
        #if( $conditionTable['_cca02']=='1' ){
        if( 1 ){
            $sql.= ",$_ezf_id_cca02 as _ezf_id_cca02 ";
            $sql.= ",if(Not($_rstatIgnoreCCA02),cca02.id,'') as '_id_cca02' ";
            $sql.= ",cca02.f2v1 ";
            $sql.= ",case 1 when cca02.f2v2a1='0' and Not($_rstatIgnoreCCA02) then '0: Normal' ";
            $sql.= "when cca02.f2v2a1b2='1' and Not($_rstatIgnoreCCA02) then '2a) PDF 1' ";
            $sql.= "when cca02.f2v2a1b2='2' and Not($_rstatIgnoreCCA02) then '2b) PDF 2' ";
            $sql.= "when cca02.f2v2a1b2='3' and Not($_rstatIgnoreCCA02) then '2c) PDF 3' ";
            $sql.= "when cca02.f2v2a1b1='1' and Not($_rstatIgnoreCCA02) then '1a) Mild fatty liver' ";
            $sql.= "when cca02.f2v2a1b1='2' and Not($_rstatIgnoreCCA02) then '1b) Moderate fatty liver' ";
            $sql.= "when cca02.f2v2a1b1='3' and Not($_rstatIgnoreCCA02) then '1c) Severe fatty liver' ";
            $sql.= "when cca02.f2v2a1b3='1' and Not($_rstatIgnoreCCA02) then '3a) Cirrhosis' ";
            $sql.= "when cca02.f2v2a1b4='1' and Not($_rstatIgnoreCCA02) then '4a) Parenchymal change' ";
            $sql.= "when Not($_rstatIgnoreCCA02) then '<font style=\"color:red\">ไม่ได้ระบุ</font>' ";
            $sql.= "else '' ";
            $sql.= "end as 'cca02abnormal' ";
            $sql.= ",f2v6a3b1 ";
            $sql.= ",if(f2v6a3='1' and f2v6a3b1='1' and Not($_rstatIgnoreCCA02) ";
            $sql.= ",if(length(concat(if(f2v2a2b4c1='1','d) Intrahepatic duct stone','') ";
            $sql.= ",if(f2v2a2b5c1='1','e) High echo','') ";
            $sql.= ",if(f2v2a2b6c1='1','f) Low echo','') ";
            $sql.= ",if(f2v2a2b7c1='1','g) Mixed echo','') ";
            $sql.= ",if(f2v2a3b1='1','1. Right lobe','') ";
            $sql.= ",if(f2v2a3b2='1','2. Left lobe','') ";
            $sql.= ",if(f2v2a3b3='1','3. Common bile duct','') ";
            $sql.= "))>0";  # Check length result
            $sql.= ",concat(if(f2v2a2b4c1='1','d) Intrahepatic duct stone','') ";
            $sql.= ",if(f2v2a2b5c1='1','e) High echo','') ";
            $sql.= ",if(f2v2a2b6c1='1','f) Low echo','') ";
            $sql.= ",if(f2v2a2b7c1='1','g) Mixed echo','') ";
            $sql.= ",if(f2v2a3b1='1','1. Right lobe','') ";
            $sql.= ",if(f2v2a3b2='1','2. Left lobe','') ";
            $sql.= ",if(f2v2a3b3='1','3. Common bile duct','') ";
            $sql.= ")"; # return result
            $sql.= ",'<font style=\"color:red\">สงสัยมะเร็งท่อน้ำดี แต่ไม่ได้ระบุ</font>') ";
            $sql.= ",'') as 'suspectedcca' ";
            $sql.= ",if(max(cca02.sys_usimageexist)='1','1',cca02.sys_usimageexist) as sys_usimageexist ";
        }
        #if( $conditionTable['_cca02p1']=='1' ){
        if( 1 ){
            $sql.= ",$_ezf_id_cca02p1 as _ezf_id_cca02p1 ";
            $sql.= ",if(Not($_rstatIgnoreCCA02p1),cca02p1.id,'') as '_id_cca02p1' ";
            $sql.= ",if( length(cca02p1.id)>0 and cca02p1.f2p1v1 is not null AND NOT($_rstatIgnoreCCA02p1),f2p1v1,'') as f2p1v1 ";
            $sql.= ",if( length(cca02p1.id)>0 and cca02p1.f2p1v1 is not null AND NOT($_rstatIgnoreCCA02p1),f2p1v1,'') as ctmridate ";
            $sql.= ",case 1 when f2p1v3='0' AND NOT($_rstatIgnoreCCA02p1) then '0. Normal' ";
            $sql.= "when f2p1v3='1' AND NOT($_rstatIgnoreCCA02p1) then '1. Intrahepatic' ";
            $sql.= "when f2p1v3='2' AND NOT($_rstatIgnoreCCA02p1) then '2. Perihilar' ";
            $sql.= "when f2p1v3='3' AND NOT($_rstatIgnoreCCA02p1) then '3. Distal' ";
            $sql.= "when f2p1v3='4' AND NOT($_rstatIgnoreCCA02p1) then '<font style=\"color:red\">4. Not CCA</font>' ";
            $sql.= "when length(cca02p1.id)>0 and cca02p1.f2p1v1 is not null AND NOT($_rstatIgnoreCCA02p1) then '<font style=\"color:red\">ไม่ได้ระบุ</font>' ";
            $sql.= "end ";
            $sql.= " as ctmriresult ";
            $sql.= ",if( length(cca02p1.f2p1_fileupload)>0,cca02p1.f2p1_fileupload,NULL) as f2p1_fileupload ";
        }
        #if( $conditionTable['_cca03']=='1' ){
        if( 1 ){
            $sql.= ",$_ezf_id_cca03 as _ezf_id_cca03 ";
            $sql.= ",if(Not($_rstatIgnoreCCA03),max(cca03.id),'') as '_id_cca03' ";
            $sql.= ",if( length(cca03.id)>0 and length(cca03.f3v4dvisit)>0 AND NOT($_rstatIgnoreCCA03),max(cca03.f3v4dvisit),cca03.f3v4dvisit) as f3v4dvisit ";
            $sql.= ",if( length(cca03.id)>0 and length(cca03.f3v4dvisit)>0 AND NOT($_rstatIgnoreCCA03),max(cca03.f3v4dvisit),cca03.f3v4dvisit) ";
            $sql.= "as cca03date ";
            $sql.= ",case 1 when max(if(NOT($_rstatIgnoreCCA03),cca03.f3notcca,NULL))=1 then '<font style=\"color:red\">Not CCA</font>' ";
            $sql.= "when min(if(NOT($_rstatIgnoreCCA03),f3v7,NULL))=0 then '<font style=\"color:red\">Dead</font>' ";
            $sql.= "when max(if(NOT($_rstatIgnoreCCA03),f3v5a1b1,NULL))=1 then 'Liver resection' ";
            $sql.= "when max(if(NOT($_rstatIgnoreCCA03),f3v5a1b2,NULL))=1 then 'Hilar resection' ";
            $sql.= "when max(if(NOT($_rstatIgnoreCCA03),f3v5a1b6,NULL))=1 then 'Whipple' ";
            $sql.= "when max(if(NOT($_rstatIgnoreCCA03),f3v5a1b3,NULL))=1 then 'Bypass' ";
            $sql.= "when max(if(NOT($_rstatIgnoreCCA03),f3v5a1b4,NULL))=1 then 'Explore/Biopsy' ";
            $sql.= "when max(if(NOT($_rstatIgnoreCCA03),f3v5a1b5,NULL))=1 then 'Needle biopsy' ";
            $sql.= "when max(if(NOT($_rstatIgnoreCCA03),f3v5a2,NULL))=1 then 'Chemotherapy' ";
            $sql.= "when max(if(NOT($_rstatIgnoreCCA03),f3v5a3,NULL))=1 then 'PTBD' ";
            $sql.= "when max(if(NOT($_rstatIgnoreCCA03),f3v5a4,NULL))=1 then 'Stent' ";
            $sql.= "when max(if(NOT($_rstatIgnoreCCA03),f3v4a1,NULL))=1 then 'OPD' ";
            $sql.= "when max(if(NOT($_rstatIgnoreCCA03),f3v4a1,NULL))=2 then 'IPD' ";
            $sql.= "when min(if(NOT($_rstatIgnoreCCA03),f3v6a1,NULL))=1 then 'Best supportive' ";
            $sql.= "when length(cca03.id)>0 and length(cca03.f3v4dvisit)>0 AND NOT($_rstatIgnoreCCA03) then 'ไม่ได้ระบุ' ";
            $sql.= "end as cca03result ";
            $sql.= ",if( length(cca03.f3_fileupload)>0,cca03.f3_fileupload,NULL) as f3_fileupload ";
        }
        #if( $conditionTable['_cca04']=='1' ){
        if( 1 ){
            $sql.= ",$_ezf_id_cca04 as _ezf_id_cca04 ";
            $sql.= ",if(Not($_rstatIgnoreCCA04),cca04.id,'') as '_id_cca04' ";
            $sql.= ",if( length(cca04.id)>0 and length(cca04.f4v1)>0 AND NOT($_rstatIgnoreCCA03),max(cca04.f4v1),'') ";
            $sql.= "as f4v1 ";
            $sql.= ",if( length(cca04.id)>0 and length(cca04.f4v1)>0 AND NOT($_rstatIgnoreCCA03),max(cca04.f4v1),'') ";
            $sql.= "as pathodate ";
            $sql.= ",case 1 when max(f4v3)=1 AND NOT($_rstatIgnoreCCA04) then '<font style=\"color:red\">Not CCA</font>' ";
            $sql.= "when min(f4v4)=1 AND NOT($_rstatIgnoreCCA04) then '1. Intrahepatic bile duct' ";
            $sql.= "when max(f4v4)=2 AND NOT($_rstatIgnoreCCA04) then '2. Perihilar' ";
            $sql.= "when max(f4v4)=3 AND NOT($_rstatIgnoreCCA04) then '3. Distal' ";
            $sql.= "when max(f4v4)=4 AND NOT($_rstatIgnoreCCA04) then '4. Other, non-specified' ";
            $sql.= "when max(f4v4)=5 AND NOT($_rstatIgnoreCCA04) then '5. Hepatectomy not done' ";
            $sql.= "when length(cca04.id)>0 and length(cca04.f4v1)>0 AND NOT($_rstatIgnoreCCA04) then 'ไม่ได้ระบุ' ";
            $sql.= "end as pathoresult ";
            $sql.= ",if( cca04.id>0 AND Not(max(f4v3)=1) AND ((max(f4v8a4)=0 and length(f4v8a4)>0) OR (max(f4v8a5)=0 and length(f4v8a5)>0) OR (max(f4v8a6)=0 and length(f4v8a6)>0)) AND NOT($_rstatIgnoreCCA04),'1','') as stage0 ";
            $sql.= ",if( cca04.id>0 AND Not(max(f4v3)=1) AND (max(f4v8a4)=1 OR max(f4v8a5)=1 OR max(f4v8a6)=1) AND NOT($_rstatIgnoreCCA04),'1','') as stage1 ";
            $sql.= ",if( cca04.id>0 AND Not(max(f4v3)=1) AND (max(f4v8a4)=2 OR max(f4v8a5)=2 OR max(f4v8a6)=2) AND NOT($_rstatIgnoreCCA04),'1','') as stage2 ";
            $sql.= ",if( cca04.id>0 AND Not(max(f4v3)=1) AND (max(f4v8a4)=3) AND NOT($_rstatIgnoreCCA04),'1','') as stage3 ";
            $sql.= ",if( cca04.id>0 AND Not(max(f4v3)=1) AND (max(f4v8a5)=3 OR max(f4v8a6)=3) AND NOT($_rstatIgnoreCCA04),'1','') as stage3a ";
            $sql.= ",if( cca04.id>0 AND Not(max(f4v3)=1) AND (max(f4v8a5)=4 OR max(f4v8a6)=4) AND NOT($_rstatIgnoreCCA04),'1','') as stage3b ";
            $sql.= ",if( cca04.id>0 AND Not(max(f4v3)=1) AND (max(f4v8a4)=4 OR max(f4v8a5)=5 OR max(f4v8a6)=5) AND NOT($_rstatIgnoreCCA04),'1','') as stage4a ";
            $sql.= ",if( cca04.id>0 AND Not(max(f4v3)=1) AND (max(f4v8a4)=5 OR max(f4v8a5)=6 OR max(f4v8a6)=6) AND NOT($_rstatIgnoreCCA04),'1','') as stage4b ";
            $sql.= ",if( cca04.id>0 AND Not(max(f4v3)=1) AND (max(f4v8a4)=6 OR max(f4v8a5)=7 OR max(f4v8a6)=7) AND NOT($_rstatIgnoreCCA04),'1','') as stageuk ";
            $sql.= ",if( length(cca04.f4_fileupload)>0,cca04.f4_fileupload,NULL) as f4_fileupload ";
        }
        #if( $conditionTable['_cca05']=='1' ){
        if( 1 ){
            $sql.= ",$_ezf_id_cca05 as _ezf_id_cca05 ";
            $sql.= ",if(Not($_rstatIgnoreCCA05),cca05.id,'') as '_id_cca05' ";
            $sql.= ",if( length(cca05.id)>0 and length(cca05.f5v1a1)>0 AND NOT($_rstatIgnoreCCA05),max(cca05.f5v1a1),'') ";
            $sql.= "as f5v1a1 ";
            $sql.= ",if( length(cca05.id)>0 and length(cca05.f5v1a1)>0 AND NOT($_rstatIgnoreCCA05),max(cca05.f5v1a1),'') ";
            $sql.= "as cca05lastvisit ";
            $sql.= ",case 1 when max(if(NOT($_rstatIgnoreCCA05),f5v1a3,NULL))=4 then '<font style=\"color:red\">Dead</font>' ";
            $sql.= "when max(if(NOT($_rstatIgnoreCCA05),f5v1a3b1,NULL))=1 then '1.1 Healthy' ";
            $sql.= "when max(if(NOT($_rstatIgnoreCCA05),f5v1a3b1,NULL))=2 then '1.2 Recurrent disease' ";
            $sql.= "when max(if(NOT($_rstatIgnoreCCA05),f5v1a3b1,NULL))=3  then '1.3 Progress disease' ";
            $sql.= "when length(cca05.id)>0 and length(cca05.f5v1a1)>0 AND NOT($_rstatIgnoreCCA05) then 'ไม่ได้ระบุ' ";
            $sql.= "end as cca05result ";
        }
        # Dead
        if( 1 ){
            $sql.= ",case 1 when min(if(NOT($_rstatIgnoreCCA03),f3v7,NULL))=0 then 'Dead (03)' ";
            $sql.= "when max(if(NOT($_rstatIgnoreCCA05),f5v1a3,NULL))=4 then 'Dead (05)' ";
            $sql.= "end as dead ";
        }
        #
        # where
        $sql.= $sqlWhere;
        #
        #$sql.= "order by hospital.zone_code, hospital.provincecode, hospital.hcode, $mainTable.hptcode ";
        #$sql.= "limit 100 ";
        $sql.= "ORDER BY $colum $sort ";
        if( $mainTable=='cca01' ){
            $sql.= "limit 100 ";
        }
        #$sql.= "LIMIT $startpage,$perpagelimit ";
//        echo "<br />".$sql;
        Yii::$app->db->createCommand($sql)->execute();
        
        $request    = Yii::$app->request;   
        # Surgery
        if( $request->get('_sur03')=='1' ){
            $sqlSurgery     = self::getSurgery($request->get('_sur03'));
        }
        # clear ignor data
        if( $request->get('_dead03')=='1' || $request->get('_dead05')=='1' ){
            $sqlDead        = self::clearDead($request->get('_dead03'), $request->get('_dead05'));
        }
        # ทำเป็นชุดข้อมูลเพื่อการเรียกดู ไว้ เพื่อลดภาระ server (จะเข้ามาทำก็ต่อเมื่อ เงื่อนไขตามที่กำหนดให้ ทำใหม่เท่านั้น)
        # คัดแยกเฉพาะ ที่ทำงานช้าออก ส่วนที่ทำเร็ว ก็ทำเป็น real time ได้เลย
        # 
        # 
        # 
        # ดึงข้อมูลที่ Update ล่าสุดส่งไปเพื่อ display
        $sqlData = "select * from cascap_log.summary_data_$_usertable ";
        $sqlData.= "ORDER BY $colum $sort LIMIT $startpage,$perpagelimit ";
        $dataProvider = Yii::$app->db->createCommand($sqlData)->queryAll();
        return $dataProvider;
    }
    
    public static function getHospitalWhere($province,$amphur,$hcode)
    {
        $sql = "";
        if( strlen($province)>0 ){
            $sql.= 'and hospital.provincecode="'.$province.'" ';
            // กรณีมีการเลือก อำเภอเข้ามาด้วย
            if( strlen($amphur)>0 ){
                $sql.= 'and hospital.amphurcode="'.$amphur.'" ';
                // ถ้าเลือก รพ. เข้ามาก็ตรวจสอบ รพ. ด้วย
                if( strlen($hcode)>0 ){
                    $sql.= 'and hospital.hcode="'.$hcode.'" ';
                }
            }
            
        }
        
        
        return $sql;
    }
    
    public static function getPDF($pdf1,$pdf2,$pdf3)
    {
        $sql = "";
        if( $pdf1=='1' || $pdf2=='1' || $pdf3=='1' ){
            if($pdf1=='1'){
                $sql.="cca02.f2v2a1b2='1'";
            }
            
            if($pdf2=='1'){
                if( strlen($sql)>0 ){
                    $sql.=" or cca02.f2v2a1b2='2'";
                }else{
                    $sql.="cca02.f2v2a1b2='2'";
                }
            }
            
            if($pdf3=='1'){
                if( strlen($sql)>0 ){
                    $sql.=" or cca02.f2v2a1b2='3'";
                }else{
                    $sql.="cca02.f2v2a1b2='3'";
                }
            }
        }
        
        if( strlen($sql)>0 ){
            $sql="and cca02.f2v2a1='1' and (".$sql.") ";
        }
        //echo $sql;
        
        return $sql;
    }
    
    public static function getSuspectedCCA($suspectedcca)
    {
        
        $sql = "";
        if( $suspectedcca=='1' ){
            $sql.="and cca02.f2v6a3='1' and cca02.f2v6a3b1='1' ";
        }
        
        return $sql;
    }
    
    public static function getUsImageExist($usimageexist)
    {
        
        $sql = "";
        if( $usimageexist=='1' ){
            $sql.="and cca02.sys_usimageexist='1' ";
        }
        
        return $sql;
    }
    
    public static function getCCACTMRI($notccactmri)
    {
        $_rstatIgnoreCCA02p1 = self::$_rstatIgnoreCCA02p1;
        $sql = "";
        if( $notccactmri=='1' ){
            $sql.="and Not(".$_rstatIgnoreCCA02p1.") and cca02p1.f2p1v3='4' ";
        }
        
        return $sql;
    }
    
    public static function getCCA03($notcca03)
    {
        $_rstatIgnoreCCA03 = self::$_rstatIgnoreCCA03;
        $sql = "";
        if( $notcca03=='1' ){
            $sql.="and Not(".$_rstatIgnoreCCA03.") and cca03.f3notcca='1' ";
        }
        
        return $sql;
    }
    
    public static function getCCA04($notcca04)
    {
        $_rstatIgnoreCCA04 = self::$_rstatIgnoreCCA04;
        $sql = "";
        if( $notcca04=='1' ){
            $sql.="and Not(".$_rstatIgnoreCCA04.") and cca03.f3notcca='1' ";
        }
        
        return $sql;
    }
    
    public static function getNotCCA($notccactmri,$notcca03,$notcca04)
    {
        $_rstatIgnoreCCA02p1 = self::$_rstatIgnoreCCA02p1;
        $_rstatIgnoreCCA03 = self::$_rstatIgnoreCCA03;
        $_rstatIgnoreCCA04 = self::$_rstatIgnoreCCA04;
        $sql = "";
        if( $notccactmri=='1' && $notcca03=='1' && $notcca04=='1'){
            $sql.="and if(cca02p1.id is null,1,Not(".$_rstatIgnoreCCA02p1.")) and if(cca03.id is null,1,Not(".$_rstatIgnoreCCA03.")) and if(cca04.id is null,1,Not(".$_rstatIgnoreCCA04.")) and (if(cca02p1.id is null,0,cca02p1.f2p1v3='4') or if(cca03.id is null,0,cca03.f3notcca='1') or if(cca04.id is null,0,cca04.f4v3='1') ) ";
        }else if( $notccactmri=='1' && $notcca03=='1' ){
            $sql.="and if(cca02p1.id is null,1,Not(".$_rstatIgnoreCCA02p1.")) and if(cca03.id is null,1,Not(".$_rstatIgnoreCCA03.")) and (if(cca02p1.id is null,0,cca02p1.f2p1v3='4') or if(cca03.id is null,0,cca03.f3notcca='1')) ";
        }else if( $notccactmri=='1' && $notcca04=='1' ){
            $sql.="and if(cca02p1.id is null,1,Not(".$_rstatIgnoreCCA02p1.")) and if(cca04.id is null,1,Not(".$_rstatIgnoreCCA04.")) and (if(cca02p1.id is null,0,cca02p1.f2p1v3='4') or if(cca04.id is null,0,cca04.f4v3='1')) ";
        }else if( $notcca04=='1' && $notcca03=='1' ){
            $sql.="and if(cca03.id is null,1,Not(".$_rstatIgnoreCCA03.")) and if(cca04.id is null,1,Not(".$_rstatIgnoreCCA04.")) and (if(cca03.id is null,0,cca03.f3notcca='1') or if(cca04.id is null,0,cca04.f4v3='1')) ";
        }
        
        return $sql;
    }
    
    // Dead
    public static function getCCA03Dead($deadcca03)
    {
        $_rstatIgnoreCCA03 = self::$_rstatIgnoreCCA03;
        $sql = "";
        if( $deadcca03=='1' ){
            //$sql.="and Not(".$_rstatIgnoreCCA03.") and cca03.f3v7='0' ";
            //$sql.="and cca03result='Dead' ";
            $sql.="and min(f3v7)=0 AND NOT($_rstatIgnoreCCA03) ";
        }
        
        return $sql;
    }
    public static function clearDead($deadcca03,$deadcca05)
    {
        //$_rstatIgnoreCCA03 = self::$_rstatIgnoreCCA03;
        $_usertable = Yii::$app->user->identity->id;
        
        if( $deadcca03=='1' && $deadcca05=='1'){
            $sql = "delete from cascap_log.summary_data_$_usertable ";
            $sql.= "where (dead not like '%Dead%' or length(dead)=0 or dead is null) ";
        }else if( $deadcca03=='1' ){
            $sql = "delete from cascap_log.summary_data_$_usertable ";
            $sql.= "where (cca03result not like '%Dead%' or length(cca03result)=0 or cca03result is null) ";
            
        }else if($deadcca05=='1'){
            $sql = "delete from cascap_log.summary_data_$_usertable ";
            $sql.= "where (cca05result not like '%Dead%' or length(cca05result)=0 or cca05result is null) ";
        }
        if( strlen($sql)>0 ){
            Yii::$app->db->createCommand($sql)->execute();
        }
        
       return $sql;
    }
    
    public static function getSurgery($surcca03)
    {
        $_usertable = Yii::$app->user->identity->id;
        $sql = "";
        if( $surcca03=='1' ){
            $sql = "delete from cascap_log.summary_data_$_usertable ";
            $sql.= "where (Not(cca03result like '%Liver%' ";
            $sql.= "or cca03result like '%Hilar%' ";
            $sql.= "or cca03result like '%Whipple%' ";
            $sql.= "or cca03result like '%Bypass%' ";
            $sql.= "or cca03result like '%Explore%') or length(cca03result)=0 or cca03result is null) "; 
        }
        /*
         * Liver resection' ";
            $sql.= "when max(f3v5a1b2)=1 AND NOT($_rstatIgnoreCCA03) then 'Hilar resection' ";
            $sql.= "when max(f3v5a1b6)=1 AND NOT($_rstatIgnoreCCA03) then 'Whipple' ";
            $sql.= "when max(f3v5a1b3)=1 AND NOT($_rstatIgnoreCCA03) then 'Bypass' ";
            $sql.= "when max(f3v5a1b4)=1 AND NOT($_rstatIgnoreCCA03) then 'Explore/Biopsy' ";
         */
        Yii::$app->db->createCommand($sql)->execute();
        return $sql;
    }
    
    
    public static function getListTemp($colum, $sort, $page, $limitrec=0)
    {
        $startpage = ($page-1)*$limitrec;
        $perpagelimit   = $limitrec;
        $_usertable = Yii::$app->user->identity->id;
        $sqlData = "select * from cascap_log.summary_data_$_usertable ";
        $sqlData.= "ORDER BY $colum $sort LIMIT $startpage,$perpagelimit ";
        $dataProvider = Yii::$app->db->createCommand($sqlData)->queryAll();
        return $dataProvider;
    }
    
    
    #public static function getAllpage($sqlWhere, $mainTable,$colum, $sort, $page, $limitrec=0)
    public static function getAllpage($limitrec=0)
    {
        $limitrec;
        $_usertable = Yii::$app->user->identity->id;
        $sql = "select count(distinct id) as allRecord ";
        $sql.= ",count(distinct ptid) as allPatient ";
        $sql.= ",ceil(count(distinct id)/$limitrec) as countPage ";
        #$sql.= "from tb_data_3 cca02 ";
        $sql.= "from cascap_log.summary_data_$_usertable ";
        #$sql.= $sqlWhere;
        //echo $sql;
        $data = Yii::$app->db->createCommand($sql)->queryOne();
        return $data;
    }
    
    
    public static function updatePTIDView($ezf_id,$ptid,$dataid){
        $userId = Yii::$app->user->identity->id;
        
        // check table view
        $_page_view     = FALSE;
        $_page_view_log = FALSE;
        
        
        // table store all history view
        $sql = "show tables like 'ezform_page_view_log' ";
        $check_table_exsit = Yii::$app->db->createCommand($sql)->queryOne();
        if( $check_table_exsit['Tables_in_cascapcloud (ezform_page_view_log)']=='ezform_page_view_log' ){
            $sql = "replace into ezform_page_view_log ";
            $sql.= "set dadd=NOW() ";
            //$sql.= ",viewform='/teleradio/suspected/data' ";
            $sql.= ",viewform=:viewform ";
            $sql.= ",user_id=:user_id ";
            $sql.= ",ezf_id=:ezf_id ";
            $sql.= ",ptid=:ptid ";
            $sql.= ",dataid=:dataid ";
            Yii::$app->db->createCommand($sql,[':user_id'=>$userId,':ezf_id'=>$ezf_id,':ptid'=>$ptid,':dataid'=>$dataid,':viewform'=>$_SERVER['REQUEST_URI']])->execute();
        }
        
        
        
        // table for check viewed
        $sql = "show tables like 'ezform_page_view' ";
        $check_table_exsit = Yii::$app->db->createCommand($sql)->queryOne();
        if( $check_table_exsit['Tables_in_cascapcloud (ezform_page_view)']=='ezform_page_view' ){
            $sql = "replace into ezform_page_view ";
            //$sql.= "set viewform='/teleradio/suspected/data' ";
            $sql.= "set viewform=:viewform ";
            $sql.= ",user_id=:user_id ";
            $sql.= ",ezf_id=:ezf_id ";
            $sql.= ",ptid=:ptid ";
            $sql.= ",dataid=:dataid ";
            $sql.= ",lastview=NOW() ";
            Yii::$app->db->createCommand($sql,[':user_id'=>$userId,':ezf_id'=>$ezf_id,':ptid'=>$ptid,':dataid'=>$dataid,':viewform'=>$_SERVER['REQUEST_URI']])->execute();
        }
        
        return 0;
    }
    
    public static function getPTIDViewByUser($viewform,$userid){
        $sql.= "select distinct ptid ";
        $sql.= "from cascap_log.page_view ";
        $sql.= "where viewform=:viewform ";
        $sql.= "and user_id=:user_id ";
        $dataProvider = Yii::$app->db->createCommand($sql,[':user_id'=>$userid,':viewform'=>$viewform])->queryAll();
        if( count($dataProvider)>0 ){
            foreach($dataProvider as $key){
                $viewptid[$key['ptid']]='1';
            }
        }
        return $viewptid;
    }
}
