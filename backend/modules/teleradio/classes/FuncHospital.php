<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace backend\modules\teleradio\classes;

#use yii\helpers\VarDumper;
#use yii\web\Controller;
use Yii;

class FuncHospital {
    //put your code here
    public static function getHospitalByHCODE($hcode) {
        $sql = "select * from all_hospital_thai
                where hcode=:hcode ";
        
        $out = Yii::$app->db->createCommand($sql, [':hcode'=>$hcode])->queryOne();
        return $out;
    }
    
    public static function getHospitalByAmphurCode($provincecode, $amphurcode) {
        $sql = "select * from all_hospital_thai where provincecode=:provincecode and amphurcode=:amphurcode ";
        $sql.= "order by field(code4,'12','5','11','15','6','7','16','18','3','4','17','13','10','1','2','7','8','80','',NULL), hcode ";
        
        $hospital = Yii::$app->db->createCommand($sql, [':provincecode'=>$provincecode, ':amphurcode'=>$amphurcode])->queryAll();
        if(count($hospital)>0){
            foreach($hospital as $key => $value){
                if( strlen($value['name'])>0 ){
                    $out[$value['hcode']]=$value['name']; 
                }
            }
        }
        return $out;
    }
    
    
    public static function getProvinceList() {
        $sql = "select distinct provincecode,province from all_hospital_thai where zone_code in ('01','06','07','08','09','10') order by zone_code, province ";
        
        $prov = Yii::$app->db->createCommand($sql)->queryAll();
        if(count($prov)>0){
            foreach($prov as $key => $value){
                if( strlen($value['provincecode'])>0 ){
                    //$out[$value['provincecode']]=$value['zone_code'];  
                    $out[$value['provincecode']]=$value['province']; 
                }
            }
        }

        return $out;
    }
    
    public static function getAmphurList($provincecode) {
        $sql = "select distinct amphurcode,amphur from all_hospital_thai where provincecode=:provincecode order by amphur ";
        
        $amphur = Yii::$app->db->createCommand($sql, [':provincecode'=>$provincecode])->queryAll();
        if(count($amphur)>0){
            foreach($amphur as $key => $value){
                if( strlen($value['amphurcode'])>0 ){
                    $out[$value['amphurcode']]=$value['amphur']; 
                }
            }
        }

        return $out;
    }
    
    
}