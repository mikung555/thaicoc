<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace backend\modules\teleradio\classes;

use Yii;

/**
 * Description of QueryUserProfile
 *
 * @author chaiwat
 */
class QueryUserProfile {
    //put your code here
    
    public static function get($userid) {
        $usql = "select * from user_profile
                where user_id=:user_id ";
        
        $userdet = Yii::$app->db->createCommand($usql, [':user_id'=>$userid])->queryOne();
        return $userdet;
    }
    
    public static function getUserProfile($userid, $usersite) {
        $usql = "select * from user_profile
                where user_id=:user_id ";
        
        $userdet = Yii::$app->db->createCommand($usql, [':user_id'=>$userid])->queryOne();
        
        
        $sql = "select * from all_hospital_thai
                where hcode=:hcode ";
        
        $hospital = Yii::$app->db->createCommand($sql, [':hcode'=>$usersite])->queryOne();
        
        $out['fullname']=$userdet['firstname']." ".$userdet['lastname'];
        $out['hospital']=$hospital['name'];
        $out['province']=$hospital['province'];
        $out['provincecode']=$hospital['provincecode'];
        $out['amphurcode']=$hospital['amphurcode'];
        $out['tamboncode']=$hospital['tamboncode'];
        return $out;
    }
    
}
