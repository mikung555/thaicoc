<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace backend\modules\teleradio\classes;

#use yii\helpers\VarDumper;
#use yii\web\Controller;
use Yii;

class QueryTableDet {
    //put your code here
    public static function getDetail($db,$table) {
        $sql = "SHOW TABLE STATUS FROM ".$db." LIKE :table ";
        $out = Yii::$app->db->createCommand($sql, [':table'=>$table])->queryOne();
        return $out;
    }
    
    
    public static function getLogTableUpdate() {
        $sql = "SELECT TABLE_NAME,CREATE_TIME,UPDATE_TIME ";
        $sql.= "FROM INFORMATION_SCHEMA.TABLES ";
        $sql.= "WHERE TABLE_SCHEMA='cascap_report1' ";
        $sql.= "AND (TABLE_NAME='tb_data_1' OR TABLE_NAME='tb_data_2' OR TABLE_NAME='tb_data_3' OR TABLE_NAME='tb_data_4' OR TABLE_NAME='tb_data_7' OR TABLE_NAME='tb_data_8' OR TABLE_NAME='tb_data_11') ";
        $sql.= "order by CREATE_TIME ";
        //echo $sql;
        $listtable = Yii::$app->db->createCommand($sql)->queryAll();
        foreach ($listtable as $key){
            //print_r($key);
            //echo $key['TABLE_NAME'];
            $out[$key['TABLE_NAME']]=$key;
        }
        
        return $out;
    }
    
}


