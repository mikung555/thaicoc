<?php

namespace backend\modules\teleradio\classes;

#use yii\helpers\VarDumper;
#use yii\web\Controller;
use Yii;

class QueryTableReportUpdate {
    //put your code here
    public static function createTableReport($dbreport,$dbsource,$table) {
        /*
         * Example
         *  drop table if exists cascap_report1.tb_data_1;
            create table cascap_report1.tb_data_1
            select * from cascapcloud.tb_data_1;
            ALTER TABLE `cascap_report1`.`tb_data_1` ADD INDEX `ptid` USING BTREE (`ptid`) comment '' ADD INDEX `hsitecode` USING BTREE (`hsitecode`);
         * 
         */
        $sql = "drop table if exists `".$dbreport."`.`".$table."` ";
        Yii::$app->db->createCommand($sql)->execute();
        
        $sql = "create table `".$dbreport."`.`".$table."` ";
        $sql.= "select * from `".$dbsource."`.`".$table."` ";
        Yii::$app->db->createCommand($sql)->execute();
        
        $sql = "ALTER TABLE `".$dbreport."`.`".$table."` ";
        $sql.= "ADD INDEX `ptid` USING BTREE (`ptid`) ";
        $sql.= ",ADD INDEX `hsitecode` USING BTREE (`hsitecode`); ";
        Yii::$app->db->createCommand($sql)->execute();
        
        $out=['table_update'=>date('Y-m-d H:i:s')];
        return $out;
    }
}
