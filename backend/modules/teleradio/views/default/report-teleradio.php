<?php
    use kartik\grid\GridView;
    use appxq\sdii\widgets\ModalForm;
    use yii\helpers\Html;
    use yii\helpers\Url;

    echo Html::hiddenInput('ezf_id', $ezf_id);
    echo Html::hiddenInput('id', $id);
    echo Html::hiddenInput('type', $type);
    if($id == '1'){$title = "แสดงความคิดเห็น"; }elseif($id == '2') { $title = "ขอคำปรึกษา"; }else if($id == '3'){$title = "SOS";}       
?>
<h3>ส่วนที่ 1 รายการ <?=$title?></h3>
<div id="user-grid"></div>
<hr>
<h3>ส่วนที่ 2 ข้อมูลตามหน่วยงาน</h3><button class="btn btn-success pull-right" id="btn-refresh_call"><i class="fa fa-refresh"></i> Refresh</button>
<br><br>
<div id="hos-grid"></div>

<div class="modal fade" id="modal-forms" role="dialog">
    <div class="modal-dialog" style="width:90%">
        <div class="modal-content">

        </div>
    </div>
</div>

<div class="modal fade" id="modal-forms-userdrilldown" role="dialog">
    <div class="modal-dialog" style="width:45%;">
        <div class="modal-content">

        </div>
    </div>
</div>

<?php
    $this->registerJs("
        var id = $('input[name=id]').val();
        var ezf_id = $('input[name=ezf_id]').val();
        var type = $('input[name=type]').val();

        $(document).ready(show_userdata(id,ezf_id,type));
        $(document).ready(show_hosdata(id,ezf_id,type));

        $('#btn-refresh_call').click(function(){
            $('#hos-grid').html('<center><i class=\"fa fa-spinner fa-pulse fa-3x fa-fw\"></i></center>');
            $.ajax({
                url:'" . Url::to(['report-refresh']) . "',
                method:'GET',
                data: {id:id,ezf_id:ezf_id,type:type},
                dataType:'HTML',
                success:function(result){
                   $('#hos-grid').html(result); 
                },error: function (xhr, ajaxOptions, thrownError) {
                   console.log(xhr);
                }
            });
            return false;
        });
        
       
        
        function show_userdata(id,ezf_id,type){
            $('#user-grid').html('<center><i class=\"fa fa-spinner fa-pulse fa-3x fa-fw\"></i></center>');
           // console.log(id,ezf_id,type);
             $.ajax({
                url:'" . Url::to(['report-user']) . "',
                method:'GET',
                data: {id:id,ezf_id:ezf_id,type:type},
                dataType:'HTML',
               
                success:function(result){
                   
                   $('#user-grid').html(result); 
                },error: function (xhr, ajaxOptions, thrownError) {
                   console.log(xhr);
                }
            });
            return false;
        }
        
        function show_hosdata(id,ezf_id,type){
            $('#hos-grid').html('<center><i class=\"fa fa-spinner fa-pulse fa-3x fa-fw\"></i></center>');
            //console.log(id,ezf_id,type);
             $.ajax({
                url:'" . Url::to(['report-hospital']) . "',
                method:'GET',
                data: {id:id,ezf_id:ezf_id,type:type},
                dataType:'HTML',
                    success:function(result){
                    $('#hos-grid').html(result); 
                },error: function (xhr, ajaxOptions, thrownError) {
                   console.log(xhr);
                }
            });
            return false;
        }

        function modalGuideField(url) {
            $('#modal-forms .modal-dialog .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
            $('#modal-forms').modal()
            .find('.modal-content')
            .load(url);
        }    
    
    
    


    ");
    ?>