<?php

use yii\helpers\Html;
use yii\helpers\Url;
use kartik\grid\GridView;

$this->title = 'Tele-Radiology';

echo Html::hiddenInput('ezf_id', $ezf_id)
?>
<ul class="nav nav-tabs">
    <li id="tab-one" class="active"><a href="<?=Url::to(['/teleradio/'])?>" id="all" class="tab1"><i class="fa fa-bars fa-lg"></i> รายการทั้งหมด</a></li>
    <li id="tab-two"><a data-toggle="tab" href="#type1" id="1"  class="tab2"><i class="fa fa-comments-o fa-lg"></i> แสดงความคิดเห็น</a></li>
    <li id="tab-three"><a data-toggle="tab" href="#type2" id="2"  class="tab3"><i class="fa fa-comments-o fa-lg"></i> ขอคำปรึกษา</a></li>
    <li id="tab-four"><a data-toggle="tab"href="#type3" id="3"  class="tab4"><i class="fa fa-comments-o fa-lg"></i> SOS</a></li>
<?php
// <a href="http://tools.cascap.in.th/v4cascap/version01/console/cloud/auth.php?dir=cascaptools&amp;id=3410100805885&amp;auth=c8hlj69f72osvvqci028117ol6W8u3QMXGiwfqDI-95Y8xQ_CNKivL-bO2" target="_blank">เข้าสู่ระบบ CASCAP Tools</a>
if( Yii::$app->user->can('doctorcascap')==TRUE 
    || Yii::$app->user->can('administrator')==TRUE    
    || Yii::$app->user->can('sitemanager')==TRUE    
        ):
?>
    <li id="tab-four"><a data-toggle="tab"href="#type5" id="5"  class="tab5"><i class="glyphicon glyphicon-compressed"></i> จัดการข้อมูล</a></li>
    <li id="tab-four"><a data-toggle="tab"href="#type6" id="6"  class="tab6"><i class="glyphicon glyphicon-compressed"></i> จัดการข้อมูล ใน Tools</a></li>
    <li id="tab-four"><a data-toggle="tab"href="#type7" id="7"  class="tab7"><i class="glyphicon glyphicon-tree-deciduous"></i> Diagram</a></li>
    <li id="tab-four"><a data-toggle="tab"href="#type8" id="8"  class="tab8"><i class="glyphicon glyphicon-grain"></i> Overview</a></li>

<?php
endif;
?>
</ul>
<br>
<div class="tab-content">
    <div id="tele_all">
    <?php echo GridView::widget([
        'dataProvider'=>$dataProvider,
        //'filterModel'=>$searchModel,
        'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => ''],
        'resizableColumns'=>false,
        'columns'=>[
            [
                'class' => 'kartik\grid\SerialColumn',
                'headerOptions' => ['style'=>'text-align: center;'],
                'contentOptions' => ['style'=>'min-width:60px;text-align: center;'],
            ],
            [
                'format' => 'html',
                'label' => 'สถานบริการ',
                'width' => '100px',
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-left'],
                'value' => function($model){
                    $hospital = \backend\modules\ezforms\components\EzformQuery::getHospital($model['xsourcex']);
                    return  '<span class="label label-success" title="'.($hospital['name']. ' ต.'. $hospital['tambon']. ' อ.'. $hospital['amphur']. ' จ.'. $hospital['province']).'">'.$hospital['hcode'].'</span>';;
                }
            ],
            [
                'format' => 'text',
                'label' => 'ชื่อฟอร์ม',
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-left'],
                'value' => function($model){
                    $sql="SELECT ezf_name FROM ezform WHERE ezf_id=:ezf_id";
                    $hosp = \Yii::$app->db->createCommand($sql,[':ezf_id' => $model['ezf_id']])->queryOne();
                    return $hosp['ezf_name'];
                }
            ],
            [
                'label' => 'เรื่อง',
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-left'],
                'value' => function($model){
                    return mb_substr($model['ezf_comment'], 0,50,'UTF-8').'...';
                }
            ],
            [
                'label' => 'ประเภท',
                'format' => 'text',
                'width' => '90px',
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'value' => function($model){
                    $text = '';
                    if($model['type'] == 1)
                        $text =  'แสงความคิดเห็นทั่วไป';
                    else if($model['type'] == 2)
                        $text = 'ขอคำปรึกษา';
                    else if($model['type'] == 3)
                        $text = 'SOS';
                    else if($model['type'] == 9 || $model['type'] == 0)
                        $text = 'บันทึกข้อมูล';

                    return $text;
                }
            ],
            [
                'label' => 'เมื่อ',
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'value' => function($model){
                    $date = new DateTime($model['create_date']);
                    return $date->format('d/m/Y');
                }
            ],
            [
                'label' => 'โดย',
                'width' => '150px',
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-left'],
                'value' => function($model){
                    $user = common\models\UserProfile::findOne($model['user_create']);
                    return $user->firstname . ' ' . $user->lastname;
                }
            ],
            [
                'label' => 'ตอบโดย',
                'width' => '150px',
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-left'],
                'value' => function($model){
                    $user_reply = \backend\modules\teleradio\controllers\DefaultController::replyUser($model['ezf_id'],$model['data_id']);
                    $user = common\models\UserProfile::findOne($user_reply['user_create']);
                    return $user->firstname . ' ' . $user->lastname;
                }
            ],
            [
                'format' => 'raw',
                'width' => '70px',
                'headerOptions' => ['class' => 'text-center'],
                'label' => 'Link',
                'value' => function($model){
                    return Html::a('<span class="fa fa-file-text-o"></span> ดูข้อมูล', ['consult-view', 'id'=>$model['id'], ], ['class'=>'btn btn-success btn-sm', 'data-pjax' => 0, 'target'=> '_blank']);
                }
            ],
        ],

        'headerRowOptions'=>['class'=>'kartik-sheet-style'],

        'pjax'=>true, // pjax is set to always true for this demo
        'pjaxSettings' => ['options' => ['id' => 'pjax-page-consult',], 'enablePushState' => false],
        // set your toolbar
        'toolbar'=> [
            ['content' => Html::dropDownList('typeReply', ($_GET['type'] ? $_GET['type'] : 2), [ '1' => 'แสดงความคิดเห็น', '2' => 'ขอคำปรึกษา', '3' => 'SOS', 'all' => 'ทั้งหมด'], [
                'class'=>'form-control',
                'id' => 'typeReply',
                'onchange'=>'changeType($(this).val());'])
            ],
            '{export}',
            '{toggleData}',
        ],
        // set export properties
        'export'=>[
            'fontAwesome'=>true
        ],
        // parameters from the demo form
        'bootstrap'=>true,
        'bordered'=>true,
        'striped'=>true,
        'condensed'=>true,
        'responsiveWrap' => false,
        'responsive' => true,
        'showPageSummary'=>false,
        'pageSummaryRowOptions'=>['class' => 'text-center bg-warning text-bold'],
        'hover'=>false,
        'panel'=>[
            'type'=>GridView::TYPE_PRIMARY,
            'heading'=>'รายการทั้งหมด',
        ],
        'persistResize'=>false,
    ]); ?>
    </div>
    <div id="output-tab"></div>
</div>
<?php
$urldatamanagement = Url::to('suspected/data');
$this->registerJs("
        var ezf_id = $('input[name=ezf_id]').val();
        var type = $('#typeReply').val();
        
        $('.tab1').click(function(){
            $('#tab-one').addClass('active');
            $('#tab-two').removeClass('active');
            $('#tab-three').removeClass('active');
            $('#tab-four').removeClass('active');
            //$('#tele_all').show();
            //$('#output-tab').empty();
        });
        $('.tab2').click(function(){
            $('#tab-one').removeClass('active');
            $('#tab-two').addClass('active');
            $('#tab-three').removeClass('active');
            $('#tab-four').removeClass('active');
            //alert(ezf_id);
            show_data($(this).attr('id'),ezf_id,type);
        });
        $('.tab3').click(function(){
            $('#tab-one').removeClass('active');
            $('#tab-two').removeClass('active');
            $('#tab-three').addClass('active');
            $('#tab-four').removeClass('active');
            show_data($(this).attr('id'),ezf_id,type);

        });
        
        $('.tab4').click(function(){
            $('#tab-one').removeClass('active');
            $('#tab-two').removeClass('active');
            $('#tab-three').addClass('active');
            $('#tab-four').removeClass('active');
            show_data($(this).attr('id'),ezf_id,type);
        });
        $('.tab5').click(function(){
            var urltab5 = '" . Url::to(['suspected/data']) . "';
            window.open(urltab5,'_blank');
        });
        $('.tab6').click(function(){
            var urltab6 = '" . Url::to(['/verification/open-ls2']) . "';
            window.open(urltab6,'_blank');
        });
        $('.tab7').click(function(){
            var urltab7 = 'http://report.cascap.in.th/main.php';
            window.open(urltab7,'_blank');
        });
        $('.tab8').click(function(){
            var urltab8 = 'https://cloud.cascap.in.th/report/?tab=overview';
            window.open(urltab8,'_blank');
        });

        function show_data(id,ezf_id){
            //$('#output-tab').html('<center><i class=\"fa fa-spinner fa-pulse fa-3x fa-fw\"></i></center>');
            $('#tele_all').empty();
//            console.log(id,ezf_id,type);
             $.ajax({
                url:'" . Url::to(['report-teleradio']) . "',
                method:'GET',
                data: {id:id,ezf_id:ezf_id,type:type},
                dataType:'HTML',
               
                success:function(result){
                   
                   $('#output-tab').html(result); 
                },error: function (xhr, ajaxOptions, thrownError) {
                   console.log(xhr);
                }
            });
            return false;
        }
        
function changeType(val){
    var url = \"?type=\"+$.trim(val); $(location).attr(\"href\",url);
}
",\yii\web\View::POS_END);

