<?php
/**
 * Created by PhpStorm.
 * User: Kongvut Sangkla
 * Date: 1/4/2559
 * Time: 1:25
 * E-mail: kongvut@gmail.com
 */
use yii\helpers\Html;

?>

<div class="media">
    <hr>
    <div class="media-left">
        <img class="media-object" alt="64x64"
             src="<?php echo Yii::$app->user->identity->userProfile->getAvatarById($model->user_create) ?: '/img/anonymous.jpg' ?>"
             data-holder-rendered="true" style="width: 64px; height: 64px;">
        <span><?php $user = common\models\UserProfile::findOne($model->user_create);
            echo $user->firstname . ' ' . $user->lastname; ?></span>
    </div>
    <div class="media-body">
        <?php
        if($model->var1a1==1){
            echo 'เห็นด้วยกับผลการตรวจ<br>';
        }
        if($model->var1a1==2){
            echo 'ไม่เห็นด้วยกับผลการตรวจ<br>-- ';
            if($model->var3a1==1) {
                echo 'Normal<br>';
            }
            else if($model->var3a1==2) {
                echo 'Mile fatty liver<br>';
            }
            else if($model->var3a1==3) {
                echo 'PDF 1<br>';
            }
            else if($model->var3a1==4) {
                echo 'Cirrhosis<br>';
            }else if($model->var3a1==5) {
                echo 'Moderate fatty liver<br>';
            }else if($model->var3a1==6) {
                echo 'PDF 2<br>';
            }else if($model->var3a1==7) {
                echo 'Severe fatty liver<br>';
            }else if($model->var3a1==8) {
                echo 'PDF 3<br>';
            }
            //
            if($model->var3a9==1){
                echo '-- Parenchymal change<br>';
            }
        }
        if($model->var2a1==1){
            echo 'ยืนยันผลการส่งต่อ<br>-- ';
            if($model->var4a1==1) {
                echo 'ส่งต่อ MRI สงสัย CCA<br>';
            }
            else if($model->var4a1==2) {
                echo 'ส่งต่อเรื่อง GALL Stone<br>';
            }
            //
            if($model->var4a3==1) {
                echo '-- ส่งต่อเรื่อง Kidney<br>';
            }
        }
        if($model->var2a2==1){
            echo 'ยกเลิกการส่งต่อ<br>-- ';
            if($model->var4a4==1) {
                echo 'นัด 6 เดือน<br>';
            }
            else if($model->var4a4==2) {
                echo 'นัด 1 ปี<br>';
            }
        }
        if($model->textdet!=''){
            echo 'หมายเหตุ : '.nl2br($model->textdet);
        }
        ?>
    </div>
</div>

