<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\Url;
use appxq\sdii\widgets\ModalForm;
use yii\widgets\LinkPager;
use yii\widgets\ActiveForm;
use appxq\sdii\helpers\SDNoty;
use appxq\sdii\helpers\SDHtml;
?>

<div class="modal-content" >
    <?php // \appxq\sdii\utils\VarDumper::dump($dataCurrent);     ?>


    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h3 class="modal-title "><div class="label label-warning"><?= $sitecode['hcode'] . " " . $sitecode['name']; ?></div></h3>
    </div>
    <div class="modal-body"> 
        <div class="row">
            <!--<div class="col-md-12 list-grid" id="list-grid-pjax" style="height:395px; overflow: auto;">-->
            <?php
//                \appxq\sdii\utils\VarDumper::dump($type_grid);
            if($type_grid == 1){ 
                $typetext = "ไม่พบข้อมูล แสดงความคิดเห็น ของหน่วยงานนี้"; 
            }else if($type_grid == 2) {
                $typetext = "ไม่พบข้อมูล ขอคำปรึกษา ของหน่วยงานนี้"; 
            }else{
                $typetext = "ไม่พบข้อมูลของหน่วยงานนี้"; 
            }
            
            
            if ($tbdata == NULL){
                echo " <div class='col-md-12 list-grid' id='list-grid-pjax' style='height:230px; overflow: auto;' > ";
                echo '<div class="alert alert-danger"><strong><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> '. $typetext .'</strong></div>';
            }else{
                echo " <div class='col-md-12 list-grid' id='list-grid-pjax' style='height:395px; overflow: auto;' > ";
                echo GridView::widget([
                    'dataProvider' => $tbdata,
                    'id' => 'drilldown',
                    'panel' => [
                        'type' => GridView::TYPE_PRIMARY,
                        'heading'=> $title_gridview,
                    ],
                    'pjax' => true,
                    'columns' => [
//                        [
//                            'value' => 'data_id',
//                            'headerOptions' => ['style' => 'text-align: center;'],
//                            'contentOptions' => ['style' => 'width:10%;text-align: center;'],
//                        ],
                        [
                            'class' => 'yii\grid\SerialColumn',
                            'headerOptions' => ['style' => 'text-align: center;'],
                            'contentOptions' => ['style' => 'width:4%;text-align: center;'],
                        ],
                        [
                            'header' => 'หน่วยบริการ',
                            'format' => 'raw',
                            'value' => function($model) use($ezf_id) {
                                $tb_data = \backend\modules\teleradio\controllers\DefaultController::getTbdataEzformReply($model['data_id'],$ezf_id);
                                return $tb_data['hcode'] . "  " . $tb_data['name'];
                            },
//                            'value' => 'tb_hsitecode',
//                            'value' => function($model) {
//                                return $modal['tb_hsitecode'];
//                                //return json_decode($model['ezf_json_new'])->hsitecode;
////                                $hsitecode = \backend\modules\ezforms\components\EzformQuery::getHospital($model['tb_hsitecode']);
////                                return $model['tb_hsitecode'] . "  " . $hsitecode['name'];
//                            },
                            'headerOptions' => ['style' => 'text-align: center;'],
                            'contentOptions' => ['style' => 'width:15.06%; text-align: left; '],
                        ],
                        [
                            'header' => 'ชื่อฟอร์ม',
                            'format' => 'raw',
                            'value' => function($model){
                                $sql="SELECT ezf_name FROM ezform WHERE ezf_id=:ezf_id";
                                $hosp = \Yii::$app->db->createCommand($sql,[':ezf_id' => $model['ezf_id']])->queryOne();
                                return $hosp['ezf_name'];
                            },
                            'headerOptions' => ['style' => 'text-align: center;'],
                            'contentOptions' => ['style' => 'width:18%; text-align: left; '],
                        ],
                        [
                            'header' => 'เรื่อง',
                            'format' => 'raw',
                            'value' => function($model){
                                return mb_substr($model['ezf_comment'], 0,50,'UTF-8').'...';
                            },
                            'headerOptions' => ['style' => 'text-align: center;'],
                            'contentOptions' => ['style' => 'width:18%; text-align: left; '],
                        ],
                        [
                            'header' => 'ผู้เริ่มคำถาม',
                            'format' => 'raw',
                            'value' => function($model) {
                                $user = common\models\UserProfile::findOne($model['user_create']);
                                return $user->firstname . ' ' . $user->lastname . "</br><div style='color:#C8C8C8;font-size:11px;'>(" . $model['create_date'] . ")";
                            },
                            'headerOptions' => ['style' => 'text-align: center;'],
                            'contentOptions' => ['style' => 'width:14%; text-align: left; '],
                        ],
                        [
                            'header' => 'ตอบล่าสุด',
                            'format' => 'raw',
                            'value' => function($model){
                            $user_replyback = \backend\modules\teleradio\controllers\DefaultController::replyUser($model['ezf_id'],$model['data_id']);
                            $userback = common\models\UserProfile::findOne($user_replyback['user_create']);
                                return $userback->firstname . ' ' . $userback->lastname . "</br><div style='color:#C8C8C8;font-size:11px;'>(" . $user_replyback['create_date'] . ")";
                            },
                            'headerOptions' => ['style' => 'text-align: center;'],
                            'contentOptions' => ['style' => 'width:14%; text-align: left; '],
                        ],
                        [
                            'header' => 'จำนวนผู้มีส่วนร่วมทั้งหมด',
                            'format' => 'raw',
                            //'value' => 'rep_countuserjoin',
                            'value' => function($model) use ($ezf_id,$type_grid,$sitecode){
                                return Html::a(number_format($model['countuserjoin']), "javascript:void(0)", [
                                    'id' => 'modal-report-drilldown',
                                    'name' => 'userjoin',
                                    'class'=>'btn btn-primary btn-sm',
                                    'onclick' => "modalGuideFieldUser('" . Url::to([
                                        'report-reply-drilldown',
                                        'ezf_id'=> $ezf_id,
                                        'data_id' => $model['data_id'],
                                        'type_grid' => $type_grid,
                                        'hcode' => $sitecode['hcode']
                                    ]) . "')",
                                    'style' => 'text-align:justify'
                                ]); 
                            },    
                            'headerOptions' => ['style' => 'text-align: center;'],
                            'contentOptions' => ['style' => 'width:13%; text-align: center; '],
                        ],
                        [
                            'format' => 'raw',
                            'headerOptions' => ['class' => 'text-center'],
                            'contentOptions' => ['style' => 'width:7.32%; text-align: center; '],
                            'label' => 'Link',
                            'value' => function($model){
                                return Html::a('<span class="fa fa-file-text-o"></span> ดูข้อมูล', ['consult-view', 'id'=>$model['id'], ], ['class'=>'btn btn-success btn-sm', 'data-pjax' => 0, 'target'=> '_blank']);
                            }
                        ],
//                            
                    ],
                ]);
            }
                ?>

            </div> 
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle" aria-hidden="true"></i> Close</button>
    </div>

</div>

<?php $this->registerJs("
    
    function modalGuideFieldUser(url) {
        $('#modal-forms-userdrilldown .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
        $('#modal-forms-userdrilldown').modal('show')
        .find('.modal-content')
        .load(url);
        return false;
    }  
");
     
?>
