<?php
    use kartik\grid\GridView;
    use appxq\sdii\widgets\ModalForm;
    use yii\helpers\Html;
    use yii\helpers\Url;
//     \appxq\sdii\utils\VarDumper::dump($dataHos);
    echo GridView::widget([
        'dataProvider' => $dataHos,
        'id' => 'hos_grid',
        'panel' => [
            'type'=>GridView::TYPE_PRIMARY,
            //'before'=> "<form id='form-search' name='form-search'><div class='row'><div class='col-md-6 '><div class='input-group'>".Html::input('text','search', '', ['class'=>'form-control','id'=>'text-search','placeholder'=>'ค้นหาข้อมูลที่นี่'])."<span class=\"input-group-btn\"><button class=\"btn btn-primary\" id=\"Search\" type=\"submit\"><i class=\"glyphicon glyphicon-search\"></i> ค้นหา</button></span></div></div></div></form>",
        ],
        'pjax'=>true,
        
        'columns' => [
            
            [
                
                'header'=>'หน่วยงาน',
                'format' => 'raw',
//                'value'=> function($model){
//                    return $model['hcode']." : ".$model['name'];
//                },
                'value'=> function($model) use ($ezf_id,$type) {
                   
                    return Html::a($model['xsourcex']." : ".$model['sitename'], "javascript:void(0)", [
                                'id' => 'modal-forms-report',
                                'name' => 'sitecode',
                                'onclick' => "modalGuideField('" . Url::to([
                                    'report-reply',
                                    'hcode' => $model['xsourcex'],
                                    'ezf_id' => $ezf_id,
                                    'type_grid' => 'all',//$type,
                                    'drilldown'=> '1',
                                ]) . "')",
                                'style' => 'text-align:justify'
                    ]); 
                },
                'headerOptions'=>['style'=>'text-align: center;'],
                'contentOptions'=>['style'=>'width:200px; text-align: left; '],
            ],
            [
                'header'=>'จำนวนครั้งในการถามตอบทั่วไป',
                //'attribute'=> 'reply_a',
                'format' => 'raw',
                'value' => function($model) use ($ezf_id){
                   // $dataCount = \backend\modules\teleradio\controllers\DefaultController::getCountReply($model['xsourcex'],$ezf_id,'1');
                    //return $dataCount;
                    return Html::a(number_format($model['reply_type1']), "javascript:void(0)", [
                                'id' => 'modal-forms-type1', 
                                'name' => 'type_1',
                                'onclick' => "modalGuideField('" . Url::to([
                                    'report-reply',
                                    'hcode' => $model['xsourcex'],
                                    'ezf_id' => $ezf_id,
                                    'type_grid' => '1',
                                    'drilldown'=> '1',
                                ]) . "')",
                                'style' => 'text-align:justify'
                    ]); 
                },
                'headerOptions'=>['style'=>'text-align: center;'],
                'contentOptions'=>['style'=>'width:80px; text-align: center; '],
            ],
            [
                'header'=>'จำนวนครั้งในการขอคำปรึกษา',
                //'attribute'=> 'reply_q',
                'format' => 'raw',
                'value' => function($model) use ($ezf_id){
                    $dataCount2 = \backend\modules\teleradio\controllers\DefaultController::getCountReply($model['xsourcex'],$ezf_id,'2');
//                    return $dataCount2;
                    return Html::a(number_format($model['reply_type2']), "javascript:void(0)", [
                                'id' => 'modal-forms-type2',
                                'name' => 'type_2',
                                'onclick' => "modalGuideField('" . Url::to([
                                    'report-reply',
                                    'hcode' => $model['xsourcex'],
                                    'ezf_id' => $ezf_id,
                                    'type_grid' => '2',
                                    'drilldown'=> '1',
                                ]) . "')",
                                'style' => 'text-align:justify'
                    ]); 
                    
                },
                'headerOptions'=>['style'=>'text-align: center;'],
                'contentOptions'=>['style'=>'width:80px; text-align: center; '],
            ],
//            [
//                'header'=>'อื่นๆ',
//                'format' => 'raw',
//                'value' => function($model) use ($ezf_id){
//                    $dataCount3 = \backend\modules\teleradio\controllers\DefaultController::getCountReply($model['hcode'],$ezf_id,'0');
////                    return $dataCount2;
//                    return Html::a($dataCount3, "javascript:void(0)", [
//                                'id' => 'modal-forms-type0',
//                                'name' => 'type_0',
//                                'onclick' => "modalGuideField('" . Url::to([
//                                    'report-reply',
//                                    'hcode' => $model['hcode'],
//                                    'ezf_id' => $ezf_id,
//                                    'type_grid' => '0',
//                                ]) . "')",
//                                'style' => 'text-align:justify'
//                    ]); 
//                    
//                },
//                'headerOptions'=>['style'=>'text-align: center;'],
//                'contentOptions'=>['style'=>'width:80px; text-align: center; '],
//            ],
            [
                'header'=>'วันนี้',
                'format' => 'raw',
                'value' => function($model) use($ezf_id) {
                    return Html::a(number_format($model['today']), "javascript:void(0)", [
                                'id' => 'modal-forms-today',
                                'name' => 'today',
                                'onclick' => "modalGuideField('" . Url::to([
                                    'report-reply',
                                    'hcode' => $model['xsourcex'],
                                    'ezf_id' => $ezf_id,
                                    'type_grid' => 'today',
                                    'drilldown'=> '1',
                                ]) . "')",
                                'style' => 'text-align:justify'
                    ]); 
                },
                //'attribute'=> 'today',
                'headerOptions'=>['style'=>'text-align: center;'],
                'contentOptions'=>['style'=>'width:80px; text-align: center; '],
            ],
            [
                'header'=>'สัปดาห์นี้',
                'format' => 'raw',
                'value' => function($model) use($ezf_id) {
                    return Html::a(number_format($model['thisWeek']), "javascript:void(0)", [
                                'id' => 'modal-forms-thisWeek',
                                'name' => 'thisWeek',
                                'onclick' => "modalGuideField('" . Url::to([
                                    'report-reply',
                                    'hcode' => $model['xsourcex'],
                                    'ezf_id' => $ezf_id,
                                    'type_grid' => 'thisWeek',
                                    'drilldown'=> '1',
                                ]) . "')",
                                'style' => 'text-align:justify'
                    ]); 
                },
                //'attribute'=> 'thisWeek',
                'headerOptions'=>['style'=>'text-align: center;'],
                'contentOptions'=>['style'=>'width:80px; text-align: center; '],
            ],
            [
                'header'=>'เดือนนนี้',
                'format' => 'raw',
                'value' => function($model) use($ezf_id) {
                    return Html::a(number_format($model['thisMonth']), "javascript:void(0)", [
                                'id' => 'modal-forms-thisYear',
                                'name' => 'thisMonth',
                                'onclick' => "modalGuideField('" . Url::to([
                                    'report-reply',
                                    'hcode' => $model['xsourcex'],
                                    'ezf_id' => $ezf_id,
                                    'type_grid' => 'thisMonth',
                                    'drilldown'=> '1',
                                ]) . "')",
                                'style' => 'text-align:justify'
                    ]); 
                },
                //'attribute'=> 'thisMonth',
                'headerOptions'=>['style'=>'text-align: center;'],
                'contentOptions'=>['style'=>'width:80px; text-align: center; '],
            ],
            [
                'header'=>'ปีนี้',
                'format' => 'raw',
                'value' => function($model) use($ezf_id) {
                    return Html::a(number_format($model['thisYear']), "javascript:void(0)", [
                                'id' => 'modal-forms-thisYear',
                                'name' => 'thisYear',
                                'onclick' => "modalGuideField('" . Url::to([
                                    'report-reply',
                                    'hcode' => $model['xsourcex'],
                                    'ezf_id' => $ezf_id,
                                    'type_grid' => 'thisYear',
                                    'drilldown'=> '1',
                                ]) . "')",
                                'style' => 'text-align:justify'
                    ]); 
                },
                //'attribute'=> 'thisYear',
                'headerOptions'=>['style'=>'text-align: center;'],
                'contentOptions'=>['style'=>'width:80px; text-align: center; '],
            ],
        ],   
     ]);      
         
    