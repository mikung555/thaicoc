
<?php 
    use kartik\grid\GridView;
    use appxq\sdii\widgets\ModalForm;
    use yii\helpers\Html;
    use yii\helpers\Url;
    
    
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'id' => 'user_grid',
        'panel' => [
            'type'=>GridView::TYPE_PRIMARY,
            'heading'=> $title_gridview,
        ],
        'pjax'=>true,
        'columns'=>[
            [
		'class' => 'yii\grid\SerialColumn',
		'headerOptions' => ['style'=>'text-align: center;'],
		'contentOptions' => ['style'=>'width:10px;text-align: center;'],
	    ],
	    [
                'header'=>'รายชื่อ',
		'value' => function($model){
                    $user = common\models\UserProfile::findOne($model['User']);
                    return $user->firstname . ' ' . $user->lastname;
                },
		'headerOptions'=>['style'=>'text-align: center;'],
		'contentOptions'=>['style'=>'width:120px; text-align: left;'],
	    ],
	    [
                'header'=>'จำนวนโพสทั้งสิ้น',
		'value'=>'replyAll',
		'headerOptions'=>['style'=>'text-align: center;'],
		'contentOptions'=>['style'=>'width:80px; text-align: center;'],
	    ],
            [
                'header'=>'วันนี้',
		'value'=>'today',
		'headerOptions'=>['style'=>'text-align: center;'],
		'contentOptions'=>['style'=>'width:80px; text-align: center; '],
	    ],
            [
                'header'=>'สัปดาห์นี้',
		'value'=>'thisWeek',
		'headerOptions'=>['style'=>'text-align: center;'],
		'contentOptions'=>['style'=>'width:80px; text-align: center; '],
	    ],
            [
                'header'=>'เดือนนี้',
		'value'=>'thisMonth',
		'headerOptions'=>['style'=>'text-align: center;'],
		'contentOptions'=>['style'=>'width:80px; text-align: center; '],
	    ],
            [
                'header'=>'ปีนี้',
		'value'=>'thisYear',
		'headerOptions'=>['style'=>'text-align: center;'],
		'contentOptions'=>['style'=>'width:80px; text-align: center; '],
	    ],
            
        ],
    ]);
    
    ?>