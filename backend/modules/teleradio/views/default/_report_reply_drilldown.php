<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\Url;
use appxq\sdii\widgets\ModalForm;
use yii\widgets\LinkPager;
use yii\widgets\ActiveForm;
use appxq\sdii\helpers\SDNoty;
use appxq\sdii\helpers\SDHtml;
?>

<div class="modal-content" >
    <div class="modal-header">
         <h3 class="modal-title "><div class="label label-warning">รายชื่อผู้มีส่วนร่วมทั้งหมด</div></h3>
    </div>
    <div class="modal-body"> 
        <div class="row">
            <div class="col-md-12 list-grid" id="list-grid-pjax">
                <?php
                echo GridView::widget([
                    'dataProvider' => $data_drilldown,
                    'id' => 'user-drilldown',
                    'panel' => [
                        'type' => GridView::TYPE_PRIMARY,
                        'heading' => 'รายชื่อผู้มีส่วนร่วมทั้งหมด',
                    ],
                    'pjax' => true,
                    'columns' => [
                        [
                            'class' => 'yii\grid\SerialColumn',
                            'headerOptions' => ['style' => 'text-align: center;'],
                            'contentOptions' => ['style' => 'width:20%; text-align: center; '],
                        ],                      
                        [
                            'label' => 'รายชื่อผู้มีส่วนร่วมทั้งหมด',
                            'value' => function($model) {
                                $user = common\models\UserProfile::findOne($model['user_create']);
                                return $user->firstname . ' ' . $user->lastname;
                            },
                            'headerOptions' => ['class' => 'text-left'],
                            'contentOptions' => ['style' => 'width:90%; text-align: left; '],
                        ],
                        [
                            'label' => 'จำนวนครั้ง',
                            'value' => function($model){
                                return number_format($model['amount']);
                            },
                            'headerOptions' => ['class' => 'text-left'],
                            'contentOptions' => ['style' => 'width:50%; text-align: center; '],
                        ],
                    ],
                ]);
                ?>

            </div> 
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" id="btn-close"> <i class="fa fa-times-circle" aria-hidden="true"></i> Close</button>
    </div>

</div>

<?php ?>

<?php
//$url = Url::to($urlBackDrilldown);
$this->registerJs("
    $('#btn-close').click(function(){
        $('#modal-forms-userdrilldown').modal('hide');
        $('#modal-forms').focus();
        return false;
    });
    
");
?>

