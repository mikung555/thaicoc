<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use yii\helpers\Html;


$sort=$curentpage = Yii::$app->request->get('sort');
$curentpage = Yii::$app->request->get('page');
$curentURL = Yii::$app->request->url;

//echo $curentURL;
if( strlen($curentpage)==0 ){
    $curentpage=1;
}

$jsAdd = <<< JS
var sort="$sort";
$("#selectUsTour").change(function(){
        var thisOption = $(this).val();
        window.open("list?page="+thisOption+"&sort="+sort,"_self");
    }
)
JS;
$this->registerJs($jsAdd);

?>
<div class="row">
    <div class="col-sm-8">
        <?php
            if( $curentURL== '/teleradio/siteus/list' ){
                $btnClass    = "btn btn-info btn-sm";
            }else{
                $btnClass    = "btn btn-default btn-sm";
            }
            echo Html::a("<span class='glyphicon glyphicon-refresh'> รพ.ได้รับเครื่อง</span>"
                    ,['list']
                    ,['class' =>$btnClass, 'title' => 'Refresh']
                    );
            echo "&nbsp;";
            echo Html::a("<span class='glyphicon glyphicon-refresh'> Suspected Case</span>"
                    ,['/teleradio/suspected/list-suspected']
                    ,['class' =>'btn btn-default btn-sm']
                    ,['title' => 'แสดงรายชื่อคนที่ Suspected ทั้งหมด ตามวันที่ตรวจ']
                    );
            echo "&nbsp;";
            echo Html::a("<span class='glyphicon glyphicon-refresh'> Update data (ใช้เวลาประมาณ 20 วินาที </span>"
                    ,['check-new-record']
                    ,['class' =>'btn btn-default btn-sm']
                    ,['title'=>'Click for check']
                    );
            //echo "&nbsp;Page: ";
            echo "&nbsp;&nbsp;ข้อมูลล่าสุดเมื่อ ".$tabdet[Update_time];
            
            echo "&nbsp;";
            echo Html::a("<span class='glyphicon glyphicon-refresh'> Check Suspected Case</span>"
                    ,['/teleradio/suspected/check-new-record']
                    ,['class' =>'btn btn-default btn-sm']
                    ,['title'=>'Click for check']
                    );
        ?>
        
    </div>
    <div class="col-sm-3">
        <?php
            //echo $reccord;
            //echo $perpage;
            //echo $curentpage;
        ?>
    </div>
    
    <div class="col-sm-1" style="text-align: right;">
        
        <select class="form-control" id="selectUsTour" style="width: 100px; alignment-adjust: right;">
            <option value=""> เลือกหน้า </option>
            <?php
            
            for ($ipage=1; $ipage <= ceil($reccord/$perpage); $ipage++) {
                echo '<option value="'.$ipage.'" page="'.$ipage.'" ';
                if( $ipage==$curentpage ) { echo ' selected="selected" '; }
                echo '>'.'หน้า '.$ipage.'</option>';
            }
            ?>
        </select>
    </div>
</div>