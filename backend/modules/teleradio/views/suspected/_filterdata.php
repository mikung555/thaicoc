<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use kartik\helpers\Html;
use backend\modules\teleradio\classes\FuncHospital;
use backend\modules\teleradio\classes\QueryUserProfile;


$userid     = Yii::$app->user->identity->id;   
$usersite   = Yii::$app->user->identity->userProfile->sitecode;
$request    = Yii::$app->request;
//$selProvince= $request->get('_provincecode');
// user profile
$userDetail = QueryUserProfile::getUserProfile($userid, $usersite);
$provine = FuncHospital::getProvinceList();

//
//if( strlen($selProvince)==0 ){
//    $selProvince    = $userDetail['selectProvince'];
//}
//$amphur = FuncHospital::getAmphurList($selProvince);
//if( strlen($selAmphur)==0 ){
//    $selAmphur    = $userDetail['amphurcode'];
//}
//if( strlen($selHospital)==0 ){
//    $selHospital = $usersite;
//}

$selProvince    = $userDetail['provincecode'];
$amphur = FuncHospital::getAmphurList($selProvince);
$selAmphur    = $userDetail['amphurcode'];
$hospital   = FuncHospital::getHospitalByAmphurCode($selProvince, $selAmphur);
$selHospital = $usersite;

if( 0 ){
    echo "<pre align='left'>";
    //print_r($hospital);
    //print_r($amphur);
    //print_r($provine);
    print_r($userDetail);
    echo $selProvince;
    echo "</pre>";
    
}
?>
<div class="panel panel-default">
    <div class="panel-body">
        <div class="ov-filter-search" style="margin-bottom: 50px;">
            
            <div id="project84-form" class="form-inline col-md-12 col-sm-9" style="margin-bottom: 30px; float:left; text-align: center;">
                <div class="row">
                    <div class="col-sm-3 col-md-6 col-lg-5" style="background-color:#fff;">
                        <h3><p class="text-center">เลือกตัวกรอง</p></h3>
                        <div class="row">
                            <div class="col-sm-6">
                                <label for="project84-fromdate" style="margin-top: 7px; width: 60px;">เริ่มวันที่</label>
                                <div class="input-group date">
                                    <span class="input-group-addon kv-date-calendar" title="Select date">
                                        <i class="glyphicon glyphicon-calendar"></i>
                                    </span>
                                    <input type="date" id="project84-fromdate" class="form-control" name="project84-fromdate" value="2013-02-09" data-datepicker-type="2" data-krajee-datepicker="datepicker_b6ea203c" style="width: 155px;">

                                </div>   
                            </div>  
                            <div class="col-sm-6">
                                <label for="project84-todate" style="margin-top: 7px;">ถึงวันที่</label>
                                <div class="input-group date">
                                    <span class="input-group-addon kv-date-calendar" title="Select date">
                                        <i class="glyphicon glyphicon-calendar"></i>
                                    </span>
                                    <input type="date" id="project84-todate" class="form-control" name="project84-fromdate" value="<?=  date('Y-m-d'); ?>" data-datepicker-type="2" data-krajee-datepicker="datepicker_b6ea203c" style="width: 155px;">
                                </div>  
                                <!--input id="btnsubmit" type="submit" class="btn btn-primary"  value="แสดงรายงาน"-->
                            </div>    
                        </div>
                        <div class="row">
                            <div class="col-sm-offset-1" style="text-align: left">
                                <h4>
                                    เลือกจังหวัด:
                                    <select class="form-control" id="selectProvince" style="width: 200px;">
                                        <option value="" provincecode=''> ทั้งหมด </option>
                                        <?php
                                            if( count($provine)>0 ){
                                                foreach ($provine as $key => $value) {
                                                    if( $selProvince==$key ){
                                                        $selectOption   = " selected=\"selected\"";
                                                    }else{
                                                        $selectOption   = "";
                                                    }
                                        ?>
                                        <option value="<?php echo $value; ?>" provincecode='<?php echo $key; ?>' page="1"<?php echo $selectOption; ?>><?php echo $value; ?></option>
                                        <?php
                                                }
                                            }
                                        ?>       
                                    </select>
                                    
                                    <select class="form-control" id="selectAmphur" style="width: 200px;">
                                        <option value="" amphurcode=''> ทุก อำเภอ </option>
                                        <?php
                                            if( count($amphur)>0 ){
                                                foreach ($amphur as $key => $value) {
                                                    if( $selAmphur==$key ){
                                                        $selectOption   = " selected=\"selected\"";
                                                    }else{
                                                        $selectOption   = "";
                                                    }
                                        ?>
                                        <option value="<?php echo $value; ?>" amphurcode='<?php echo $key; ?>' page="1"<?php echo $selectOption; ?>><?php echo $value; ?></option>
                                        <?php
                                                }
                                            }
                                        ?>       
                                    </select>
                                </h>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-offset-1" style="text-align: left">
                                <h4>
                                    เลือกโรงพยาบาล:
                                    <select class="form-control" id="selectHospital" style="width: 365px;">
                                        <option value="" hcode=''> ไม่กำหนด โรงพยาบาล </option>
                                        <?php
                                            if( count($hospital)>0 ){
                                                foreach ($hospital as $key => $value) {
                                                    if( $selHospital==$key ){
                                                        $selectOption   = " selected=\"selected\"";
                                                    }else{
                                                        $selectOption   = "";
                                                    }
                                        ?>
                                        <option value="<?php echo $value; ?>" hcode='<?php echo $key; ?>' page="1"<?php echo $selectOption; ?>><?php echo $key.": ".$value; ?></option>
                                        <?php
                                                }
                                            }
                                        ?>        
                                    </select>
                                </h>
                            </div>
                        </div>
                        <div class="row">
                            <!-- table ที่ต้องเลือก -->
                            <div class="col-sm-6">
                                
                                <h4 style="text-align: left">กำหนดตารางหลัก</h4>
                                <div class="row justify-content-end">
                                    <div class="col-sm-3"></div>
                                    <div class="col-sm-9" style="text-align: left;">
                                        <?php
                                        echo Html::radio('_maincca', 0, ['id'=>'_mainreg','value'=>'reg']);
                                        echo Html::label('&nbsp;&nbsp;Register', $for='_mainreg');
                                        ?>
                                    </div>
                                </div>
                                <div class="row justify-content-end">
                                    <div class="col-sm-3"></div>
                                    <div class="col-sm-9" style="text-align: left;">
                                        <?php
                                        echo Html::radio('_maincca', 0, ['id'=>'_maincca01','value'=>'cca01']);
                                        echo Html::label('&nbsp;&nbsp;CCA-01', $for='_maincca01');
                                        ?>
                                    </div>
                                </div>
                                <div class="row justify-content-end">
                                    <div class="col-sm-3"></div>
                                    <div class="col-sm-9" style="text-align: left;">
                                        <?php
                                        echo Html::radio('_maincca', 1, ['id'=>'_maincca02','value'=>'cca02']);
                                        echo Html::label('&nbsp;&nbsp;CCA-02', $for='_maincca02');
                                        ?>
                                    </div>
                                </div>
                                <div class="row justify-content-end">
                                    <div class="col-sm-4"></div>
                                    <div class="col-sm-8" style="text-align: left;">
                                        <?php
                                        echo Html::checkbox('_maincca02pdf1', 0, ['id'=>'_maincca02pdf1','value'=>'1']);
                                        echo Html::label('&nbsp;&nbsp;PDF1', $for='_maincca02pdf1');
                                        ?>
                                    </div>
                                </div>
                                <div class="row justify-content-end">
                                    <div class="col-sm-4"></div>
                                    <div class="col-sm-8" style="text-align: left;">
                                        <?php
                                        echo Html::checkbox('_maincca02pdf2', 0, ['id'=>'_maincca02pdf2','value'=>'1']);
                                        echo Html::label('&nbsp;&nbsp;PDF2', $for='_maincca02pdf2');
                                        ?>
                                    </div>
                                </div>
                                <div class="row justify-content-end">
                                    <div class="col-sm-4"></div>
                                    <div class="col-sm-8" style="text-align: left;">
                                        <?php
                                        echo Html::checkbox('_maincca02pdf3', 0, ['id'=>'_maincca02pdf3','value'=>'1']);
                                        echo Html::label('&nbsp;&nbsp;PDF3', $for='_maincca02pdf3');
                                        ?>
                                    </div>
                                </div>
                                <div class="row justify-content-end">
                                    <div class="col-sm-4"></div>
                                    <div class="col-sm-8" style="text-align: left;">
                                        <?php
                                        echo Html::checkbox('_maincca02suspected', 0, ['id'=>'_maincca02suspected','value'=>'1']);
                                        echo Html::label('&nbsp;&nbsp;Suspected', $for='_maincca02suspected');
                                        ?>
                                    </div>
                                </div>
                                <div class="row justify-content-end">
                                    <div class="col-sm-4"></div>
                                    <div class="col-sm-8" style="text-align: left;">
                                        <?php
                                        echo Html::checkbox('_maincca02usimage', 0, ['id'=>'_maincca02usimage','value'=>'1']);
                                        echo Html::label('&nbsp;&nbsp;มีภาพอัลตราซาวด์', $for='_maincca02usimage');
                                        ?>
                                    </div>
                                </div>
                                <div class="row justify-content-end">
                                    <div class="col-sm-3"></div>
                                    <div class="col-sm-9" style="text-align: left;">
                                        <?php
                                        echo Html::radio('_maincca', 0, ['id'=>'_maincca02p1','value'=>'cca02p1']);
                                        echo Html::label('&nbsp;&nbsp;CCA-02.1', $for='_maincca02p1');
                                        ?>
                                    </div>
                                </div>
                                <div class="row justify-content-end">
                                    <div class="col-sm-4"></div>
                                    <div class="col-sm-8" style="text-align: left;">
                                        <?php
                                        echo Html::checkbox('_maincca02p1notcca', 0, ['id'=>'_maincca02p1notcca','value'=>'1']);
                                        echo Html::label('&nbsp;&nbsp;Not CCA', $for='_maincca02p1notcca');
                                        ?>
                                    </div>
                                </div>
                                <!--
                                <div class="row justify-content-end">
                                    <div class="col-sm-4"></div>
                                    <div class="col-sm-8" style="text-align: left;">
                                        <?php
                                        echo Html::checkbox('_maincca02p1cca', 0, ['id'=>'_maincca02p1cca','value'=>'1']);
                                        echo Html::label('&nbsp;&nbsp;CCA Positive', $for='_maincca02p1cca');
                                        ?>
                                    </div>
                                </div>
                                -->
                                <div class="row justify-content-end">
                                    <div class="col-sm-3"></div>
                                    <div class="col-sm-9" style="text-align: left;">
                                        <?php
                                        echo Html::radio('_maincca', 0, ['id'=>'_maincca03','value'=>'cca03']);
                                        echo Html::label('&nbsp;&nbsp;CCA-03', $for='_maincca03');
                                        ?>
                                    </div>
                                </div>
                                <div class="row justify-content-end">
                                    <div class="col-sm-4"></div>
                                    <div class="col-sm-8" style="text-align: left;">
                                        <?php
                                        echo Html::checkbox('_maincca03notcca', 0, ['id'=>'_maincca03notcca','value'=>'1']);
                                        echo Html::label('&nbsp;&nbsp;Not CCA', $for='_maincca03notcca');
                                        ?>
                                    </div>
                                </div>
                                <div class="row justify-content-end">
                                    <div class="col-sm-4"></div>
                                    <div class="col-sm-8" style="text-align: left;">
                                        <?php
                                        echo Html::checkbox('_maincca03sur', 0, ['id'=>'_maincca03sur','value'=>'1']);
                                        echo Html::label('&nbsp;&nbsp;Surgery', $for='_maincca03sur');
                                        ?>
                                    </div>
                                </div>
                                <div class="row justify-content-end">
                                    <div class="col-sm-4"></div>
                                    <div class="col-sm-8" style="text-align: left;">
                                        <?php
                                        echo Html::checkbox('_maincca03dead', 0, ['id'=>'_maincca03dead','value'=>'1']);
                                        echo Html::label('&nbsp;&nbsp;Dead', $for='_maincca03dead');
                                        ?>
                                    </div>
                                </div>
                                <div class="row justify-content-end">
                                    <div class="col-sm-3"></div>
                                    <div class="col-sm-9" style="text-align: left;">
                                        <?php
                                        echo Html::radio('_maincca',0, ['id'=>'_maincca04','value'=>'cca04']);
                                        echo Html::label('&nbsp;&nbsp;CCA-04', $for='_maincca04');
                                        ?>
                                    </div>
                                </div>
                                <div class="row justify-content-end">
                                    <div class="col-sm-4"></div>
                                    <div class="col-sm-8" style="text-align: left;">
                                        <?php
                                        echo Html::checkbox('_maincca04notcca', 0, ['id'=>'_maincca04notcca','value'=>'1']);
                                        echo Html::label('&nbsp;&nbsp;Not CCA', $for='_maincca04notcca');
                                        ?>
                                    </div>
                                </div>
                                <!--
                                <div class="row justify-content-end">
                                    <div class="col-sm-4"></div>
                                    <div class="col-sm-8" style="text-align: left;">
                                        <?php
                                        echo Html::checkbox('_maincca04cca', 0, ['id'=>'_maincca04cca','value'=>'1']);
                                        echo Html::label('&nbsp;&nbsp;CCA Positive', $for='_maincca04cca');
                                        ?>
                                    </div>
                                </div>
                                -->
                                <div class="row justify-content-end">
                                    <div class="col-sm-3"></div>
                                    <div class="col-sm-9" style="text-align: left;">
                                        <?php
                                        echo Html::radio('_maincca', 0, ['id'=>'_maincca05','value'=>'cca05']);
                                        echo Html::label('&nbsp;&nbsp;CCA-05', $for='_maincca05');
                                        ?>
                                    </div>
                                </div>
                                <div class="row justify-content-end">
                                    <div class="col-sm-4"></div>
                                    <div class="col-sm-8" style="text-align: left;">
                                        <?php
                                        echo Html::checkbox('_maincca05dead', 0, ['id'=>'_maincca05dead','value'=>'1']);
                                        echo Html::label('&nbsp;&nbsp;Dead', $for='_maincca05dead');
                                        ?>
                                    </div>
                                </div>
                                
                            </div>
                            <div class="col-sm-6">
                                
                                <h4 style="text-align: left">เลือกชุดข้อมูลที่มีการตอบเข้ามา</h4>
                                <div class="row justify-content-end">
                                    <div class="col-sm-3"></div>
                                    <div class="col-sm-9" style="text-align: left;">
                                        <?php
                                        echo Html::checkbox('_reg', 1, ['id'=>'_reg','value'=>'1']);
                                        echo Html::label('&nbsp;&nbsp;Register', $for='_reg');
                                        ?>
                                    </div>
                                </div>
                                <div class="row justify-content-end">
                                    <div class="col-sm-3"></div>
                                    <div class="col-sm-9" style="text-align: left;">
                                        <?php
                                        echo Html::checkbox('_cca01', 1, ['id'=>'_cca01','value'=>'1']);
                                        echo Html::label('&nbsp;&nbsp;CCA-01', $for='_cca01');
                                        ?>
                                    </div>
                                </div>
                                <div class="row justify-content-end">
                                    <div class="col-sm-3"></div>
                                    <div class="col-sm-9" style="text-align: left;">
                                        <?php
                                        echo Html::checkbox('_cca02', 1, ['id'=>'_cca02','value'=>'1']);
                                        echo Html::label('&nbsp;&nbsp;CCA-02', $for='_cca02');
                                        ?>
                                    </div>
                                </div>
                                <div class="row justify-content-end">
                                    <div class="col-sm-3"></div>
                                    <div class="col-sm-9" style="text-align: left;">
                                        <?php
                                        echo Html::checkbox('_cca02p1', 1, ['id'=>'_cca02p1','value'=>'1']);
                                        echo Html::label('&nbsp;&nbsp;CCA-02.1', $for='_cca02p1');
                                        ?>
                                    </div>
                                </div>
                                <div class="row justify-content-end">
                                    <div class="col-sm-3"></div>
                                    <div class="col-sm-9" style="text-align: left;">
                                        <?php
                                        echo Html::checkbox('_cca03', 1, ['id'=>'_cca03','value'=>'1']);
                                        echo Html::label('&nbsp;&nbsp;CCA-03', $for='_cca03');
                                        ?>
                                    </div>
                                </div>
                                <div class="row justify-content-end">
                                    <div class="col-sm-3"></div>
                                    <div class="col-sm-9" style="text-align: left;">
                                        <?php
                                        echo Html::checkbox('_cca04', 1, ['id'=>'_cca04','value'=>'1']);
                                        echo Html::label('&nbsp;&nbsp;CCA-04', $for='_cca04');
                                        ?>
                                    </div>
                                </div>
                                <div class="row justify-content-end">
                                    <div class="col-sm-3"></div>
                                    <div class="col-sm-9" style="text-align: left;">
                                        <?php
                                        echo Html::checkbox('_cca05', 1, ['id'=>'_cca05','value'=>'1']);
                                        echo Html::label('&nbsp;&nbsp;CCA-05', $for='_cca05');
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php
                            if( Yii::$app->user->can('administrator')==true || Yii::$app->user->can('doctorcascap')==true ){
                        ?>
                        <div class="row">
                            <center>
                                <input id="btnsubmit" type="submit" class="btn btn-primary"  value="แสดงข้อมูลตามเงื่อนไข" title="คลิ๊กเพื่อแสดงตามเงื่อนไขที่เลือก">
                            </center>
                        </div>
                        <?php
                            }
                        ?>
                        <div class="row">
                            <center>
                                <button type="button" id="waiting" data-loading-text="กำลังประมวลผล  กรุณารอซักครู่..." class="btn btn-primary" style="display: none;"></button>
                            </center>
                        </div>
                    </div>
                    
                    <div id="table_staticall" class="col-sm-9 col-md-6 col-lg-7" style="background-color:#fff;">
                        <h3><p class="text-center">ประวัติการ Update ข้อมูล</p></h3>
                        <?php
if( 0 ){
    echo "<pre align='left'>";
    print_r($overAll);
    echo "</pre>";
}
//                            echo $this->render('_overallVerify',
//                                        [
//                                            'overAll'=>$overAll,
//                                            'listVisit' => $listVisit,
//                                        ]);
//                            echo $this->render('_overallShow',
//                                        [
//                                            'overAll'=>$overAll,
//                                        ]);
                            echo $this->render('_dataupdate'
                                    ,'');
                        ?>
                    </div>
                    
                </div>
            </div>
            
        </div>
    </div>
</div>
<!-- section1-->
<div id="table_resultsearch"></div>

