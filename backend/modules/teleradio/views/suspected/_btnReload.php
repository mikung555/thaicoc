<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use yii\helpers\Html;
use yii\helpers\ArrayHelper;


$url="/teleradio/suspected/";

$sort=$curentpage = Yii::$app->request->get('sort');
$curentpage = Yii::$app->request->get('page');

if( strlen($curentpage)==0 ){
    $curentpage=1;
}

$jsAdd = <<< JS
var sort="$sort";
$("#selectUsTour").change(function(){
        var thisOption = $(this).val();
        window.open("index?page="+thisOption+"&sort="+sort,"_self");
    }
)
JS;

$this->registerJs($jsAdd);

?>
<div class="row">
    <div class="col-sm-8">
        <?php
            echo Html::a("<span class='glyphicon glyphicon-refresh'> Refresh page</span>"
                    ,['index']
                    ,['class' =>'btn btn-default btn-sm']
                    ,['title' => 'Refresh']
                    );
            echo "&nbsp;";
            echo Html::a("<span class='glyphicon glyphicon-refresh'> Check Suspected Case</span>"
                    ,['check-new-record']
                    ,['class' =>'btn btn-default btn-sm']
                    ,['title'=>'Click for check']
                    );
            //echo "&nbsp;Page: ";
            echo "&nbsp;&nbsp;ข้อมูลล่าสุดเมื่อ ".$tabdet[Update_time];
            echo "&nbsp;";
            echo Html::a("<span class='glyphicon glyphicon-refresh'> Verify</span>"
                    ,['/teleradio/suspected/verify']
                    ,['class' =>'btn btn-default btn-sm']
                    ,['title'=>'Click for check']
                    );
            echo "&nbsp;";
            echo Html::a("<span class='glyphicon glyphicon-refresh'> เรียกดูคู่มือตามเงื่อนไข</span>"
                    ,['/teleradio/suspected/data']
                    ,['class' =>'btn btn-default btn-sm']
                    ,['title'=>'Click for check']
                    );
            echo "&nbsp;";
//            echo Html::a("<span class='glyphicon glyphicon-refresh'> Report 1</span>"
//                    ,['http://report.cascap.in.th']
//                    ,['class' =>'btn btn-default btn-sm']
//                    ,['title'=>'Click for check']
//                    );
        ?>
        
    </div>
    <div class="col-sm-3">
        <?php
            //echo $reccord;
            //echo $perpage;
            //echo $curentpage;
        ?>
    </div>
    
    <div class="col-sm-1" style="text-align: right;">
        
        <select class="form-control" id="selectUsTour" style="width: 100px; alignment-adjust: right;">
            <option value=""> เลือกหน้า </option>
            <?php
            
            for ($ipage=1; $ipage <= ceil($reccord/$perpage); $ipage++) {
                echo '<option value="'.$ipage.'" page="'.$ipage.'" ';
                if( $ipage==$curentpage ) { echo ' selected="selected" '; }
                echo '>'.'หน้า '.$ipage.'</option>';
            }
            ?>
        </select>
    </div>
</div>

