<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use yii\helpers\Html;
use kartik\grid\GridView;

$this->registerJsFile('@web/js/excellentexport.js', ['depends' => [yii\web\JqueryAsset::className()]]);
$this->title = Yii::t('', 'สรุปผลงานแพทย์ ที่ทำอัลตราซาวด์');

echo $this->render('_btnReloadVerify', [
    'reccord' => $reccord,
    'perpage' => $perpage,
    'curentpage' => $curentpage,
    'tabdet' => $tabdet,
]);
?>
<br>
<script type="text/javascript" >
    function showMsg(obj)
    {
        window.open(obj.options[obj.selectedIndex].value, '_self');
    }
</script>
<?php if($checkuser==TRUE ){   ?>
<div class="row">
    <div class="col-md-3 col-md-offset-7">
        <div class="pull-right">
            <div class="form-group input-group">
                <select id="someCountries" onchange="showMsg(this)" class="form-control">
                    <option label="เลือกหน้าที่ต้องการ" value="" selected >เลือกหน้าที่ต้องการ</option>
                    <?php
                    for ($iIndex = 0; $iIndex < $pageMaxpage; $iIndex++) {
                        $pageEachpage["page"][$iIndex] = "หน้า " . ($iIndex + 1);
                        ?>
                        <option label="<?php echo $pageEachpage["page"][$iIndex]; ?>" 
                                value="/teleradio/suspected/usdoc?page=<?php echo ($iIndex + 1); ?>&per-page=<?php echo $getPerpage; ?>" <?php
                                if (($iIndex + 1) == $getCurrentpage) {
                                    echo "selected";
                                }
                                ?> >หน้า <?php echo ($iIndex + 1); ?>
                        </option>
                        <?php
                    }
                    ?>
                </select>
                <span class="input-group-btn">
                    <?php
                    if ($_GET["page"] == $pageMaxpage) {
                        echo Html::a('หน้าแรก', [
                            '/teleradio/suspected/usdoc',
                                ], ['class' => 'btn btn-info']) . '';
                    } else {
                        echo Html::a('หน้าสุดท้าย', [
                            '/teleradio/suspected/usdoc',
                            'page' => $pageMaxpage,
                            'per-page' => $getPerpage,
                                ], ['class' => 'btn btn-info']) . '';
                    }
                    ?>
                </span>
            </div>
        </div>
    </div>
    <div class="col-md-2">
        <div class="pull-right">
            <a id="report-excel" data-toggle='tooltip' data-original-title='Export รายชื่อทั้งหมด เป็น Excel' class="btn btn-info"><i class="fa fa-file-excel-o" aria-hidden="true"></i> Export to Excel</a>
        </div>
    </div>
</div>
<?php
echo kartik\grid\GridView::widget([
    'dataProvider' => $dataProvider,
    'id' => 'gridview-doctor',
    'toolbar' => [],
        'panel' => [
        'type' => GridView::TYPE_PRIMARY,
        'heading' => 'US-Doctor',
    ],
    'pjax' => true,
    'columns' => [
        [
            'attribute' => 'doctorfullname',
            'label' => 'ชื่อแพทย์',
            'format' => ['text'],
            'headerOptions' => ['style' => 'text-align: center;'],
            'value' => function($model) {
                $name = $model["doctorfullname"];
                return "$name";
            }
        ],
        [
            'attribute' => 'doctordate',
            'label' => 'วันล่าสุด',
            'headerOptions' => ['style' => 'text-align: center;', 'width' => '90'],
            'contentOptions' => ['style' => 'text-align: right;'],
        ],
        [
            'attribute' => 'count',
            'label' => 'จำนวนครั้ง',
            'headerOptions' => ['style' => 'text-align: center;'],
            'contentOptions' => ['style' => 'text-align: right;'],
            'format' => ['raw'],
            'value' => function($model) {
                $count = number_format($model["count"]);
                return "$count";
            }
        ],
        [
            'attribute' => 'allsubmit',
            'label' => 'Submit/All',
            'format' => ['raw'],
            'headerOptions' => ['style' => 'text-align: center;'],
            'contentOptions' => ['style' => 'text-align: right;', 'width'=>'100'],
            'value' => function($model) {
                $docname = $model["doctorfullname"];
                $code = $model["f2doctorcode"];
                $submit = number_format($model["submit"]);
                $allsubmit = number_format($model["allsubmit"]);
                return "<a 
                            id='modal-doctor'
                            code='$code'
                            item='1'
                            docname='$docname'
                            data-toggle='modal'
                            data-url ='" . \yii\helpers\Url::to(['suspected/dilldownsubmit']) . "'
                            data-target='#modaldoctor' >$submit/$allsubmit</a>";
            }
        ],
        [
            'attribute' => 'suspected',
            'label' => 'Suspected',
            'format' => ['raw'],
            'headerOptions' => ['style' => 'text-align: center;'],
            'contentOptions' => ['style' => 'text-align: right;', 'cursor: pointer;'],
            'value' => function($model) {
                $docname = $model["doctorfullname"];
                $code = $model["f2doctorcode"];
                $suspected = number_format($model["suspected"]);
                return "<a 
                            id='modal-doctor'
                            code='$code'
                            item='2'
                            docname='$docname'
                            data-toggle='modal'
                            data-url ='" . \yii\helpers\Url::to(['suspected/dilldownmeet']) . "'
                            data-target='#modaldoctor' >$suspected</a>";
            }
        ],
        [
            'attribute' => 'abnormal',
            'label' => 'Normal/Abnormal',
            'format' => ['raw'],
            'headerOptions' => ['style' => 'text-align: center;'],
            'contentOptions' => ['style' => 'text-align: right;'],
            'value' => function($model) {
                $docname = $model["doctorfullname"];
                $code = $model["f2doctorcode"];
                $abnormal = number_format($model["abnormal"]);
                $normal = number_format($model["normal"]);
                return "<a 
                            id='modal-doctor'
                            code='$code'
                            item='3'
                            docname='$docname'
                            data-toggle='modal'
                            data-url ='" . \yii\helpers\Url::to(['suspected/dilldownabnormal']) . "'
                            data-target='#modaldoctor' >$normal/$abnormal</a>";
            }
        ],
        [
            'attribute' => 'LiverMass',
            'label' => 'Liver Mass',
            'format' => ['raw'],
            'headerOptions' => ['style' => 'text-align: center;'],
            'contentOptions' => ['style' => 'text-align: right;'],
            'value' => function($model) {
                $docname = $model["doctorfullname"];
                $code = $model["f2doctorcode"];
                $LiverMass = number_format($model["LiverMass"]);
                return "<a 
                            id='modal-doctor'
                            code='$code'
                            item='4'
                            docname='$docname'
                            data-toggle='modal'
                            data-url ='" . \yii\helpers\Url::to(['suspected/dilldownliver']) . "'
                            data-target='#modaldoctor' >$LiverMass</a>";
            }
        ],
        [
            'attribute' => 'Gallbladder',
            'label' => 'Gallbladder',
            'format' => ['raw'],
            'headerOptions' => ['style' => 'text-align: center;'],
            'contentOptions' => ['style' => 'text-align: right;'],
            'value' => function($model) {
                $docname = $model["doctorfullname"];
                $code = $model["f2doctorcode"];
                $gall = number_format($model["Gallbladder"]);
                return "<a 
                            id='modal-doctor'
                            code='$code'
                            item='5'
                            docname='$docname'
                            data-toggle='modal'
                            data-url ='" . \yii\helpers\Url::to(['suspected/dilldowngall']) . "'
                            data-target='#modaldoctor' >$gall</a>";
            }
        ],
        [
            'attribute' => 'Kidney',
            'label' => 'Kidney',
            'format' => ['raw'],
            'headerOptions' => ['style' => 'text-align: center;'],
            'contentOptions' => ['style' => 'text-align: right;'],
            'value' => function($model) {
                $docname = $model["doctorfullname"];
                $code = $model["f2doctorcode"];
                $kidney = number_format($model["Kidney"]);
                return "<a 
                            id='modal-doctor'
                            code='$code'
                            item='6'
                            docname='$docname'
                            data-toggle='modal'
                            data-url ='" . \yii\helpers\Url::to(['suspected/dilldownkidney']) . "'
                            data-target='#modaldoctor' >$kidney</a>";
            }
        ],
        [
            'attribute' => 'Send',
            'label' => 'ส่งรักษาต่อ',
            'format' => ['raw'],
            'headerOptions' => ['style' => 'text-align: center;'],
            'contentOptions' => ['style' => 'text-align: right;'],
            'value' => function($model) {
                $docname = $model["doctorfullname"];
                $code = $model["f2doctorcode"];
                $send = number_format($model["Send"]);
                return "<a 
                            id='modal-doctor'
                            code='$code'
                            item='2'
                            docname='$docname'
                            data-toggle='modal'
                            data-url ='" . \yii\helpers\Url::to(['suspected/dilldownmeet']) . "'
                            data-target='#modaldoctor' >$send</a>";
            }
        ],
        [
            'attribute' => 'Meet1',
            'label' => 'นัดครั้งต่อไป(6 เดือน)',
            'format' => ['raw'],
            'headerOptions' => ['style' => 'text-align: center;'],
            'contentOptions' => ['style' => 'text-align: right;'],
            'value' => function($model) {
                $docname = $model["doctorfullname"];
                $code = $model["f2doctorcode"];
                $meet = number_format($model["Meet1"]);
                return "<a 
                            id='modal-doctor'
                            code='$code'
                            item='2'
                            docname='$docname'
                            data-toggle='modal'
                            data-url ='" . \yii\helpers\Url::to(['suspected/dilldownmeet']) . "'
                            data-target='#modaldoctor' >$meet</a>";
            }
        ],
        [
            'attribute' => 'Meet2',
            'label' => 'นัดครั้งต่อไป(1 ปี)',
            'format' => ['raw'],
            'headerOptions' => ['style' => 'text-align: center;'],
            'contentOptions' => ['style' => 'text-align: right;'],
            'value' => function($model) {
                $docname = $model["doctorfullname"];
                $code = $model["f2doctorcode"];
                $meet = number_format($model["Meet2"]);
                return "<a 
                            id='modal-doctor'
                            code='$code'
                            item='2'
                            docname='$docname'
                            data-toggle='modal'
                            data-url ='" . \yii\helpers\Url::to(['suspected/dilldownmeet']) . "'
                            data-target='#modaldoctor' >$meet</a>";
            }
        ],
    ],
]);
?>
<div id="data-report-table" class="hidden">
    <table class="table table-bordered table-striped">
        <thead>
            <tr style="text-align:center">
                <th>#</th>
                <th>ชื่อแพทย์</th>
                <th>วันล่าสุด</th>
                <th>จำนวนครั้ง</th>
                <th>Submit/All</th>
                <th>Suspected CCA</th>
                <th>Normal/Abnormal</th>
                <th>Liver Mass</th>
                <th>Gallbladder</th>
                <th>Kidney</th>
                <th>ส่งรักษาต่อ</th>
                <th>นัดครั้งต่อไป(6 เดือน)</th>
                <th>นัดครั้งต่อไป(1 ปี)</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $num = 1;
            foreach ($dataArray as $key) {
                ?>
                <tr style="text-align:right">
                    <td><?php echo $num ?></td>
                    <td><?php echo $key[doctorfullname] ?></td>
                    <td><?php echo $key[doctordate] ?></td>
                    <td><?php echo $key[count] ?></td>
                    <td><?php echo $key[submit] ?> / <?php echo $key[allsubmit] ?></td>
                    <td><?php echo $key[suspected] ?></td>
                    <td><?php echo $key[normal] ?> / <?php echo $key[abnormal] ?></td>
                    <td><?php echo $key[LiverMass] ?></td>
                    <td><?php echo $key[Gallbladder] ?></td>
                    <td><?php echo $key[Kidney] ?></td>
                    <td><?php echo $key[Send] ?></td>
                    <td><?php echo $key[Meet1] ?></td>
                    <td><?php echo $key[Meet2] ?></td>
                </tr>
                <?php
                $num++;
            }
            ?>
    </table>

</div>
<?php }else{  ?>
<div class="alert alert-info">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    <strong>ข้อความแจ้ง! </strong>
    ท่านยังไม่มีสิทธิ์เข้าดูข้อมูลในส่วนนี่
</div>
<?php }  ?>

<div id="modaldoctor" class="modal fade" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-lg" role="document" style="width:95%">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h3 class="modal-title "><div class="label label-primary" id="Label-doctor"></div></h3>
            </div>
            <div class="modal-body" id="body-doctor">
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal-view" tabindex="-1"  role="dialog">
    <div class="modal-dialog modal-lg" role="document" style="width:90%">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close"  data-dismiss="modal" >
                    <span aria-hidden="true">&times;</span>
                </button>
                <h3 class="modal-title "><div class="label label-primary" id="label-list"></div></h3>
            </div>
            <div class="modal-body" id="body-list">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"  >Close</button>
            </div>
        </div>
    </div>
</div>
<?php
$jsAdd = <<< JS
        
$(document).on('click','#modal-doctor', function() {
    $("#body-doctor").empty();
    $("#Label-doctor").empty();
        
    var code=$(this).attr('code');
    var item=$(this).attr('item');
    var docname=$(this).attr('docname');  
    var url = $(this).attr('data-url');
    $('#body-doctor').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
    $('#Label-doctor').html(docname);
    var dataget={code:code,item:item};  
    $.ajax({
        url:url,
        type:'GET',
        data:dataget,
        success:function(data){
            $("#body-doctor").html(data);
        }
   })
});
        
$(document).on('click','#modal-list', function() {
    $('#body-list').empty();
    $('#label-list').empty();
        
    var doccode= $(this).attr('doccode');
    var item = $(this).attr('item');
    var title = $(this).attr('title');
    var url = $(this).attr('data-url');
    $('#body-list').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
    $('#label-list').html(title);
    var dataget={item:item,doccode:doccode};
        
   $.ajax({
        url:url,
        type:'GET',
        data:dataget,
        success:function(data){
            $('#body-list').html(data);
        }
   });
});
        
        
$('#modal-view').on('hidden.bs.modal', function () {
    $('body').addClass('modal-open');
});
JS;
$this->registerJs($jsAdd);
$css = <<< CSS
a {
    cursor:pointer;
}
CSS;
$this->registerCss($css);
?>

<?php
$this->registerJs('
$("#report-excel").click(function(e){

var dataTable = $("#data-report-table");
var headerName = ("<tr>< th colspan=\'13\'>สรุปผลงานแพทย์ ที่ทำอัลตราซาวด์</th>< /tr>").replace(/< /g,"<");

dataTable.prepend(headerName);
this.download="Report-Doctor.xls" 
ExcellentExport.excel(this, \'data-report-table\');

});
');
?>