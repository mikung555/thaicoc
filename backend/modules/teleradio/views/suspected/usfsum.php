<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use yii\helpers\Html;
use kartik\grid\GridView;

$this->title = Yii::t('', 'สรุปภาพรวมตามการออกอัลตราซาวด์สัญจร');
$this->registerJsFile('@web/js/excellentexport.js', ['depends' => [yii\web\JqueryAsset::className()]]);
echo $this->render('_btnReloadVerify', [
    'reccord' => $reccord,
    'perpage' => $perpage,
    'curentpage' => $curentpage,
    'tabdet' => $tabdet,
]);
?>

<br>
<script type="text/javascript" >
    function showMsg(obj)
    {
        window.open(obj.options[obj.selectedIndex].value, '_self');
    }
</script>
<?php if($checkuser==TRUE ){   ?>
<div class="row" >
    <div class="col-md-3 col-md-offset-7">
        <div class="pull-right">
            <div class="form-group input-group">
                <select id="someCountries" onchange="showMsg(this)" class="form-control">
                    <option label="เลือกหน้าที่ต้องการ" value="" selected >เลือกหน้าที่ต้องการ</option>
                    <?php
                    for ($iIndex = 0; $iIndex < $pageMaxpage; $iIndex++) {
                        $pageEachpage["page"][$iIndex] = "หน้า " . ($iIndex + 1);
                        ?>
                        <option label="<?php echo $pageEachpage["page"][$iIndex]; ?>" 
                                value="/teleradio/suspected/usfsum?page=<?php echo ($iIndex + 1); ?>&per-page=<?php echo $getPerpage; ?>" <?php
                                if (($iIndex + 1) == $getCurrentpage) {
                                    echo "selected";
                                }
                                ?> >หน้า <?php echo ($iIndex + 1); ?>
                        </option>
                        <?php
                    }
                    ?>
                </select>
                <span class="input-group-btn">
                    <?php
                    if ($_GET["page"] == $pageMaxpage) {
                        echo Html::a('หน้าแรก', [
                            '/teleradio/suspected/usfsum',
                                ], ['class' => 'btn btn-info']) . '';
                    } else {
                        echo Html::a('หน้าสุดท้าย', [
                            '/teleradio/suspected/usfsum',
                            'page' => $pageMaxpage,
                            'per-page' => $getPerpage,
                                ], ['class' => 'btn btn-info']) . '';
                    }
                    ?>
                </span>
            </div>
        </div>
    </div>
    <div class="col-md-2">
        <div class="pull-right">
            <a id="report-excel" data-toggle='tooltip' data-original-title='Export ทั้งหมด เป็น Excel' class="btn btn-info"><i class="fa fa-file-excel-o" aria-hidden="true"></i> Export to Excel</a>
        </div>
    </div>
</div>
<?php
echo GridView::widget([
    'dataProvider' => $dataProvider,
    'toolbar' => [],
    'columns' => [
        [
            'attribute' => 'usmobile',
            'label' => 'Time',
            'headerOptions' => ['style' => 'text-align: center;'],
            'contentOptions' => ['width' => '170', 'style' => 'text-align: center;'],
            'value' => function($model) {
                $usmobile = $model["usmobile"];
                return "ครั้งที่ $usmobile";
            }
        ],
        [
            'attribute' => 'dusfind',
            'label' => 'วันที่',
            'headerOptions' => ['style' => 'text-align: center;'],
            'contentOptions' => ['width' => '300', 'style' => 'text-align: center;'],
        ],
        [
            'attribute' => 'hospital',
            'label' => 'โรงพยาบาล',
            'format' => ['raw', 'number', 2],
            'headerOptions' => ['style' => 'text-align: center;'],
            'contentOptions' => ['width' => '500'],
        ],
        [
            'attribute' => 'count',
            'label' => 'คนที่เข้าร่วม',
            'headerOptions' => ['style' => 'text-align: center;'],
            'contentOptions' => ['width' => '200', 'style' => 'text-align: right;'],
            'format' => ['raw', 'number', 2],
            'value' => function($model) {
                $count = number_format($model["count"]);
                $usmobile = $model["usmobile"];
                if (strpos($usmobile, ".") !== false) {
                    return "$count";
                } else {
                    return "<a style='cursor:pointer' target='_blank' href='http://tools.cascap.in.th/usreport/index.php?us_id=$usmobile'>$count</a>";
                }
            }
        ],
        [
            'attribute' => 'abnormal',
            'label' => 'Abnormal',
            'format' => ['raw', 'number', 2],
            'headerOptions' => ['style' => 'text-align: center;'],
            'contentOptions' => ['width' => '250', 'style' => 'text-align: right;'],
            'value' => function($model) {
                $abnormal = $model["abnormal"];
                $count = $model["count"];
                $sum = number_format(($abnormal / $count) * 100, 1, '.', ',');
                return "$abnormal($sum %)";
            }
        ],
        [
            'attribute' => 'PDF',
            'label' => 'PDF',
            'format' => ['raw', 'number', 2],
            'headerOptions' => ['style' => 'text-align: center;','width' => '300'],
            'contentOptions' => ['style' => 'text-align: right;'],
            'value' => function($model) {
                $PDF = $model["PDF"];
                $count = $model["count"];
                $sum = number_format(($PDF / $count) * 100, 1, '.', ',');
                return "$PDF($sum %)";
            }
        ],
        [
            'attribute' => 'suspected',
            'label' => 'Suspected CCA',
            'format' => ['raw', 'number', 2],
            'headerOptions' => ['style' => 'text-align: center;'],
            'contentOptions' => ['style' => 'text-align: right;'],
            'value' => function($model) {
                $suspected = $model["suspected"];
                $count = $model["count"];
                $sum = number_format(($suspected / $count) * 100, 1, '.', ',');
                return "$suspected($sum %)";
            }
        ],
        [
            'attribute' => 'Meet1',
            'label' => 'นัดครั้งต่อไป(6 เดือน)',
            'format' => ['raw', 'number', 2],
            'headerOptions' => ['style' => 'text-align: center;'],
            'contentOptions' => ['style' => 'text-align: right;'],
            'value' => function($model) {
                $meet = $model["Meet1"];
                $count = $model["count"];
                $sum = number_format(($meet / $count) * 100, 1, '.', ',');
                return "$meet($sum %)";
            }
        ],
        [
            'attribute' => 'Meet2',
            'label' => 'นัดครั้งต่อไป(1 ปี)',
            'format' => ['raw', 'number', 2],
            'headerOptions' => ['style' => 'text-align: center;'],
            'contentOptions' => ['style' => 'text-align: right;'],
            'value' => function($model) {
                $meet = $model["Meet2"];
                $count = $model["count"];
                $sum = number_format(($meet / $count) * 100, 1, '.', ',');
                return "$meet($sum %)";
            }
        ],
        [
            'attribute' => 'Send',
            'label' => 'ส่งรักษาต่อ',
            'format' => ['raw', 'number', 2],
            'headerOptions' => ['style' => 'text-align: center;'],
            'contentOptions' => ['style' => 'text-align: right;'],
            'value' => function($model) {
                $send = $model["Send"];
                $count = $model["count"];
                $sum = number_format(($send / $count) * 100, 1, '.', ',');
                return "$send($sum %)";
            }
        ],
    ],
    'pjax' => true,
    'panel' => [
        'type' => GridView::TYPE_PRIMARY,
        'heading' => 'US-Summary',
    ],
    'responsive' => true,
]);
?>
<div id="data-report-table" style="display: none">
    <table class="table table-bordered table-striped">
        <thead>
            <tr style="text-align:center">
                <th>Time</th>
                <th>วันที่</th>
                <th>โรงพยาบาล</th>
                <th>คนที่เข้าร่วม</th>
                <th>Abnormal</th>
                <th>PDF</th>
                <th>Suspected CCA</th>
                <th>นัดครั้งต่อไป(6 เดือน)</th>
                <th>นัดครั้งต่อไป(1 ปี)</th>
                <th>ส่งรักษาต่อ</th>
            </tr>
        </thead>
        <tbody>
            <?php
            foreach ($dataArray as $key) {
                ?>
                <tr style="text-align:right">
                    <td><?php echo $key[usmobile] ?></td>
                    <td><?php echo $key[dusfind] ?></td>
                    <td><?php echo $key[hospital] ?></td>
                    <td><?php echo $key[count] ?></td>
                    <td><?php echo $key[abnormal] ?></td>
                    <td><?php echo $key[PDF] ?></td>
                    <td><?php echo $key[suspected] ?></td>
                    <td><?php echo $key[Meet1] ?></td>
                    <td><?php echo $key[Meet2] ?></td>
                    <td><?php echo $key[Send] ?></td>
                </tr>
                <?php
                $num++;
            }
            ?>
    </table>

</div>
<?php }else{ ?>
<div class="alert alert-info">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    <strong>ข้อความแจ้ง! </strong>
    ท่านยังไม่มีสิทธิ์เข้าดูข้อมูลในส่วนนี่
</div>
<?php } ?>

<?php
$this->registerJs('
$("#report-excel").click(function(e){

var dataTable = $("#data-report-table");
var headerName = ("<tr>< th colspan=\'10\'>สรุปภาพรวมตามการออกอัลตราซาวด์สัญจร</th>< /tr>").replace(/< /g,"<");

dataTable.prepend(headerName);
this.download="Report-Sum.xls" 
ExcellentExport.excel(this, \'data-report-table\', \'Report ov01 Sheet\');

});          
');
?>