<?php

$this->title = Yii::t('', ' รายชื่อคนไข้ที่สงสัยมะเร็งท่อน้ำดีทั้งหมด');


echo $this->render('_btnReloadVerify',
        [
            'reccord'=>$reccord,
            'perpage'=>$perpage,
            'curentpage'=>$curentpage,
            'tabdet' => $tabdet,
        ]);

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

?>
<div class="panel panel-default">
    <div class="panel-body">
        <div class="ov-filter-search" style="margin-bottom: 50px;">
            <div id="project84-form" class="form-inline col-md-12 col-sm-9" style="margin-bottom: 30px; float:left; text-align: center;">
                <h3><p class="text-center">เลือกตัวกรอง</p></h3>
                <label for="project84-fromdate" style="margin-top: 7px; width: 60px;">เริ่มวันที่</label>
                <div class="input-group date">
                    <span class="input-group-addon kv-date-calendar" title="Select date">
                        <i class="glyphicon glyphicon-calendar"></i>
                    </span>
                    <input type="date" id="project84-fromdate" class="form-control" name="project84-fromdate" value="2013-02-09" data-datepicker-type="2" data-krajee-datepicker="datepicker_b6ea203c">
                    
                </div>            
                <label for="project84-todate" style="margin-top: 7px;">ถึงวันที่</label>
                <div class="input-group date">
                    <span class="input-group-addon kv-date-calendar" title="Select date">
                        <i class="glyphicon glyphicon-calendar"></i>
                    </span>
                    <input type="date" id="project84-todate" class="form-control" name="project84-fromdate" value="<?=  date('Y-m-d'); ?>" data-datepicker-type="2" data-krajee-datepicker="datepicker_b6ea203c">
                </div>            
                <input id="btnsubmit" type="submit" class="btn btn-primary"  value="แสดงรายงาน">
                <br><br>
                <label id="project84-lastcal">คำนวนล่าสุดเมื่อวันที่ <?= Yii::$app->formatter->asDate($dataDateTime[0][daylast], 'long')   ?> เวลา <?=  $dataDateTime[0][timelast]  ?></label> 
                <button class="btn btn-primary" id="project84-getnew">
                    <i class="fa fa-refresh"></i>
                </button>
                <br><br>
                <center>
                    <button type="button" id="waiting" data-loading-text="กำลังประมวลผล  กรุณารอซักครู่..." class="btn btn-primary"></button>
                </center>
            </div>
        </div>
    </div>
</div>
<!-- section1-->
<div id="table_secshow1"></div>
<div id="table_sec1">
    ข้อมูล Table 1
</div>

<?php

$jsAdd = <<< JS
init("","","no");
        
function init(fromDate,toDate,refresh)
{
    getdata('1',"f2v1","desc",fromDate,toDate,"sort18",refresh);
    waitingon();
}
function waitingon()
{
    $('#waiting').css('display', 'block');//not show
    $("#waiting").button('loading');
}
function waitingoff()
{
    $('#waiting').css('display', 'none');//not show
}
function getdata(sec,colum,data_sort,fromDate,toDate,div,refresh)
{
    $("#table_sec1").empty();
    var url = "/teleradio/suspected/get-data-sort";
    $.get(url
        , {
            sort:data_sort,
            fromDate:fromDate,
            toDate:toDate,
            sec:sec,
            colum:colum,
            div:div,
            refresh:refresh,
        }
    )
    .done(function( data ) {
        switch (sec) {
            case "1":
                $("#table_sec1").empty();
                $("#table_sec1").html(data);
                break;
            }
            $("#project84-getnew > i").removeClass("fa-spin");
            waitingoff();
        }
    );
}
        
$(document).on("click","#btnsubmit", function() {
    var fromDate=$("#project84-fromdate").val();
    var toDate=$("#project84-todate").val();
    //var fromDate2 = fromDate1.split("/");
    //var fromDate=fromDate2[2]+"-"+fromDate2[1]+"-"+fromDate2[0];
    //var toDate2 = toDate1.split("/");
    //var toDate=toDate2[2]+"-"+toDate2[1]+"-"+toDate2[0];
    //alert(fromDate);
    init(fromDate,toDate,"no");
});
        
$(document).on("click","#project84-getnew", function() {
    $("#project84-getnew > i").addClass("fa-spin");
    $("#project84-getnew").attr("title","กำลังดึงข้อมูลล่าสุด อาจใช้เวลาหลายนาที");
    waitingon();
    var fromDate=$("#project84-fromdate").val();
    var toDate=$("#project84-todate").val();
    init(fromDate,toDate,"yes");
});
        
        
    $(document).on("click", "*[id^=sort]", function() {
        var data_sort=$(this).attr("data-sort");
        var fromDate=$(this).attr("fromDate");
        var toDate=$(this).attr("toDate");
        var sec=$(this).attr("sec");
        var colum=$(this).attr("colum");
        var page=$(this).attr("page");
        var div=$(this).attr('id');


        switch (data_sort) {
            case "asc":
                $(this).attr("data-sort","desc");
                break;
            case "desc":
                $(this).attr("data-sort","asc");
                break;
        }
        waitingon();
        $("#table_sec1").empty();
        var url = "/teleradio/suspected/get-data-sort";
        $.get(url, {
            sort:data_sort,
            fromDate:fromDate,
            toDate:toDate,
            sec:sec,
            colum:colum,
            page:page,
            div:div,
        })
        .done(function( data ) {
            switch (sec) {
                case "1":
                    $("#table_sec1").empty();
                    $("#table_sec1").html(data);
                    waitingoff();
                    break;
            }
        });
    });
        
    $(document).on("change", "#pageselect", function() {
        var data_sort=$(this).find(':selected').attr('sort');
        var fromDate=$(this).find(':selected').attr("fromdate");
        var toDate=$(this).find(':selected').attr("todate");
        var sec=$(this).find(':selected').attr('sec');
        var colum=$(this).find(':selected').attr('colum');
        var page=$(this).find(':selected').attr('page');
        var div=$(this).attr('id');
        var refresh=$(this).find(':selected').attr('refresh');
        /*
        [sort] => desc
        [fromDate] => 
        [toDate] => 
        [sec] => 1
        [colum] => f2v1
        [fromDate] => 2013-02-09
        [toDate] => 2017-02-26
        [sec] => 1
        [colum] => cca02abnormal
        [page] => 1
        [div] => sort4
        [refresh] => no
        */
        //alert($(this).find(':selected').attr('page'));
        waitingon();
        $("#table_sec1").empty();
        var url = "/teleradio/suspected/get-data-sort";
        $.get(url, {
            sort:data_sort,
            fromDate:fromDate,
            toDate:toDate,
            sec:sec,
            colum:colum,
            page:page,
            div:div,
        })
        .done(function( data ) {
            switch (sec) {
                case "1":
                    $("#table_sec1").empty();
                    waitingoff();
                    $("#table_sec1").html(data);
                    break;
            }
        });
    });
        

JS;
$this->registerJs($jsAdd);



$css = <<< CSS
table {
    border-collapse: collapse;
    border-spacing: 0;
    border: 1px solid #ddd;
    table-layout: auto;

}

th, td {
    border:1px solid #ddd;
    text-align: left;
    padding: 8px;
}

tr:nth-child(even){background-color: #f2f2f2;}
.containBody{
    height:500px;
    display:block;
    overflow:auto;
    border-bottom:1px solid #CCC;
}
.tbl_headerFix{
    border-bottom:0px;
}

CSS;
$this->registerCss($css);

?>
