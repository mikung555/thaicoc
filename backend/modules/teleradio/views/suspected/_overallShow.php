<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

?>
<div class="container">
  <h2>แสดงภาพรวมของช้อมูล Screening ในกลุ่มที่สงสัยมะเร็งท่อน้ำดี</h2>           
  <table class="table" width="40%">
    <thead>
      <tr class="active">
        <th style="text-align: right; font-size: 20px;">รายละเอียด</th>
        <th style="text-align: right; font-size: 14px;">จำนวน (คน)</th>
        <th style="text-align: right; font-size: 20px;">%</th>
        <th style="text-align: left; font-size: 20px;">อธิบายเพิ่มเติม</th>
      </tr>
    </thead>
    <tbody>
      <tr class="info">
        <td align="right" width="40%"><b>จำนวน Suspected ทั้งหมด</b></td>
        <td align="right" width="10%"><?php echo number_format($overAll['suspected'],0,'.',','); ?></td>
        <td width="10%" align="right"></td>
        <td width="40%" align="right"></td>
      </tr>
      <tr class="success">
        <td align="right">Screening</td>
        <td align="right"><?php echo number_format($overAll['_csuspectedprotocolScreening'],0,'.',','); ?></td>
        <td align="right"></td>
        <td align="left">(จาก Screening)</td>
      </tr>
      <tr class="success">
        <td align="right">Walk-in</td>
        <td align="right"><?php echo number_format($overAll['_csuspectedprotocolWalkin'],0,'.',','); ?></td>
        <td align="right"></td>
        <td align="left">(จากกลุ่ม Walk-in หรือมาด้วยอาการ)</td>
      </tr>
      <tr style="background-color: linen;">
        <td align="right"><b>ได้รับการ confirm ด้วย CT/MRI</b></td>
        <td align="right"><?php echo number_format($overAll['CTMRI'],0,'.',','); ?></td>
        <td align="right"><?php echo number_format($overAll['%CTMRI'],1,'.',','); ?>%</td>
        <td align="left">(จาก Suspected cases)</td>
      </tr>
      <tr class="success">
        <td align="right">Screening</td>
        <td align="right"><?php echo number_format($overAll['_csuspectedCTMRIScreening'],0,'.',','); ?></td>
        <td align="right"></td>
        <td align="left">(จาก Screening)</td>
      </tr>
      <tr class="success">
        <td align="right">Walk-in</td>
        <td align="right"><?php echo number_format($overAll['_csuspectedCTMRIWalkin'],0,'.',','); ?></td>
        <td align="right"></td>
        <td align="left">(จากกลุ่ม Walk-in หรือมาด้วยอาการ)</td>
      </tr>
      
      <tr>
        <td colspan="1" align="right"> Intrahepatic bile duct</td>
        <td align="right"><?php echo number_format($overAll['Intra'],0,'.',','); ?></td>
        <td></td>
        <td align="left">(จาก CT/MRI)</td>
      </tr>
      <tr>
        <td colspan="1" align="right"> Perihilar</td>
        <td align="right"><?php echo number_format($overAll['Peri'],0,'.',','); ?></td>
        <td></td>
        <td align="left">(จาก CT/MRI)</td>
      </tr>
      <tr>
        <td colspan="1" align="right"> Distal</td>
        <td align="right"><?php echo number_format($overAll['Distal'],0,'.',','); ?></td>
        <td></td>
        <td align="left">(จาก CT/MRI)</td>
      </tr>
      
      <tr class="success">
          <td align="right"><b>เข้าสู่การรักษา</b></td>
        <td align="right"><?php echo number_format($overAll['Treat'],0,'.',','); ?></td>
        <td align="right"><?php echo number_format($overAll['%Treat'],1,'.',','); ?>%</td>
        <td align="left">(จาก Suspected cases)</td>
      </tr>
      <tr>
        <td align="right"><b>รักษาด้วยการผ่าตัด เพื่อหายขาด</b></td>
        <td align="right"><?php echo number_format($overAll['Cure'],0,'.',','); ?></td>
        <td align="right"><?php echo number_format($overAll['%Cure'],1,'.',','); ?>%</td>
        <td align="left">(จาก Suspected cases)</td>
      </tr>
      <tr>
        <td align="right">Liver resection</td>
        <td align="right"><?php echo number_format($overAll['Liver resection'],0,'.',','); ?></td>
        <td align="right"></td>
        <td align="left">(รักษาเพื่อหายขาด)</td>
      </tr>
      <tr>
        <td align="right">Hilar resection</td>
        <td align="right"><?php echo number_format($overAll['Hilar'],0,'.',','); ?></td>
        <td align="right"></td>
        <td align="left">(รักษาเพื่อหายขาด)</td>
      </tr>
      <tr>
        <td align="right">Whipple's operation or PPPD or PD</td>
        <td align="right"><?php echo number_format($overAll['Whipple'],0,'.',','); ?></td>
        <td align="right"></td>
        <td align="left">(รักษาเพื่อหายขาด)</td>
      </tr>
      
      
      <tr class="success">
        <td align="right"><b>ได้รับการตรวจเพื่อหา Final Staging Diagnosis (CCA-04)</b></td>
        <td align="right"><?php echo number_format($overAll['PathoChecked'],0,'.',','); ?></td>
        <td align="right"><?php echo number_format($overAll['%Patho'],1,'.',','); ?>%</td>
        <td align="left">(จาก Suspected cases)</td>
      </tr>
      <tr class="success">
        <td align="right"><b>Diagnosis CCA-04</b></td>
        <td align="right"><?php echo number_format($overAll['PCCA'],0,'.',','); ?></td>
        <td align="right"><?php echo number_format($overAll['%PCCA'],1,'.',','); ?>%</td>
        <td align="left">(จาก Suspected cases)</td>
      </tr>
      <tr>
        <td colspan="1" align="right"> Intrahepatic bile duct</td>
        <td align="right"><?php echo number_format($overAll['PIntra'],0,'.',','); ?></td>
        <td></td>
        <td align="left">(จาก Pathological Diagnosis)</td>
      </tr>
      <tr>
        <td colspan="1" align="right"> Perihilar</td>
        <td align="right"><?php echo number_format($overAll['PPeri'],0,'.',','); ?></td>
        <td></td>
        <td align="left">(จาก Pathological Diagnosis)</td>
      </tr>
      <tr>
        <td colspan="1" align="right"> Distal</td>
        <td align="right"><?php echo number_format($overAll['PDistal'],0,'.',','); ?></td>
        <td></td>
        <td align="left">(จาก Pathological Diagnosis)</td>
      </tr>
      
      <tr class="success">
        <td align="right"><b>Final Staging Diagnosis (CCA-04)</b></td>
        <td align="right"></td>
        <td align="right"></td>
        <td align="left">(จาก Suspected cases)</td>
      </tr>
      <tr>
        <td colspan="1" align="right"> Early-state</td>
        <td align="right"><?php echo number_format($overAll['early-stage'],0,'.',','); ?></td>
        <td></td>
        <td align="left">(จาก Pathological Diagnosis)</td>
      </tr>
      <tr>
        <td colspan="1" align="right"> Late-state</td>
        <td align="right"><?php echo number_format($overAll['late-stage'],0,'.',','); ?></td>
        <td></td>
        <td align="left">(จาก Pathological Diagnosis)</td>
      </tr>
      <tr>
        <td colspan="1" align="right"> Unknown</td>
        <td align="right"><?php echo number_format($overAll['unknown'],0,'.',','); ?></td>
        <td></td>
        <td align="left">(จาก Pathological Diagnosis)</td>
      </tr>
      <tr>
        <td colspan="1" align="right"> To be confirm</td>
        <td align="right"><?php echo number_format($overAll['TBC'],0,'.',','); ?></td>
        <td></td>
        <td align="left">(ยังไม่ได้ระบ Staging จาก Pathological Diagnosis)</td>
      </tr>
      
    </tbody>
  </table>
</div>

