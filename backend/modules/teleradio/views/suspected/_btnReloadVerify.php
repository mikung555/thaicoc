<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use yii\helpers\Html;



$url="/teleradio/suspected/";

$sort=$curentpage = Yii::$app->request->get('sort');
$curentpage = Yii::$app->request->get('page');
$curentURL = Yii::$app->request->url;

//echo $curentURL;
if( strlen($curentpage)==0 ){
    $curentpage=1;
}



?>
<div class="row">
    <div class="col-sm-8">
        <?php
            echo Html::a("<span class='glyphicon glyphicon-refresh'> รพ.ได้รับเครื่อง</span>"
                    ,['/teleradio/siteus/list']
                    ,['class' =>'btn btn-default btn-sm']
                    ,['title' => 'Refresh']
                    );
            echo "&nbsp;";
//  ซ่อนไว้ก่อน เนื่องจากยังไม่เสร็จ       
//            echo Html::a("<span class='glyphicon glyphicon-refresh'> Verify</span>"
//                    ,['verify']
//                    ,['class' =>'btn btn-default btn-sm']
//                    ,['title' => 'Refresh']
//                    );
//            echo "&nbsp;";
//            echo Html::a("<span class='glyphicon glyphicon-refresh'> Check Suspected Case</span>"
//                    ,['check-new-record']
//                    ,['class' =>'btn btn-default btn-sm']
//                    ,['title'=>'Click for check']
//                    );
            //echo "&nbsp;Page: ";
//            echo "&nbsp;&nbsp;ข้อมูลล่าสุดเมื่อ ".$tabdet[Update_time];
//            echo "&nbsp;";
//            echo Html::a("<span class='glyphicon glyphicon-refresh'> มี 04 ไม่มี 02</span>"
//                    ,['check-new-record']
//                    ,['class' =>'btn btn-default btn-sm','title'=>'Click for check']
//                    );
//            echo "&nbsp;";
//            echo Html::a("<span class='glyphicon glyphicon-refresh'> มีข้อมูลถึง 04</span>"
//                    ,['check-new-record']
//                    ,['class' =>'btn btn-default btn-sm','title'=>'Click for check']
//                    );
//            echo "&nbsp;";
//            echo Html::a("<span class='glyphicon glyphicon-refresh'> ไม่มี Staging</span>"
//                    ,['check-new-record']
//                    ,['class' =>'btn btn-default btn-sm','title'=>'Click for check']
//                    );
//            echo "&nbsp;";
//      ซ่อนไว้ก่อน เนื่องจากตัวเลขสับสน      
//            echo Html::a("<span class='glyphicon glyphicon-refresh'> Suspected All</span>"
//                    ,['index']
//                    ,['class' =>'btn btn-default btn-sm','title'=>'แสดงรายชื่อข้อมูล Suspected ทั้งหมด ลำดับตามวันที่']
//                    );
//            echo "&nbsp;";
            if( $curentURL== '/teleradio/suspected/list-suspected' ){
                $btnClass    = "btn btn-info btn-sm";
            }else{
                $btnClass    = "btn btn-default btn-sm";
            }
            echo Html::a("<span class='glyphicon glyphicon-refresh'> Suspected</span>"
                    ,['list-suspected']
                    ,['class' =>$btnClass,'title'=>'แสดงรายชื่อข้อมูล Suspected สามารถกำหนดช่วงได้']
                    );
            
            echo "&nbsp;";
            if( $curentURL== '/teleradio/suspected/data' ){
                $btnClass    = "btn btn-info btn-sm";
            }else{
                $btnClass    = "btn btn-default btn-sm";
            }
            echo Html::a("<span class='glyphicon glyphicon-refresh'> เรียกดูข้อมูลตามเงื่อนไข</span>"
                    ,['data']
                    ,['class' =>$btnClass,'title'=>'Click for check']
                    );
            
//            echo "&nbsp;";
//            echo Html::a("<span class='glyphicon glyphicon-refresh'> Diagram</span>"
//                    ,['url'=>'https://report.cascap.in.th/report1/dg2ver.php']
//                    ,['class' =>'btn btn-default btn-sm','title'=>'Open diagram', 'target'=>'_blank']
//                    );
        ?>
        <a class="btn btn-default btn-sm" href="http://report.cascap.in.th/report1/dg2ver.php" title="Open diagram" target="_blank"><span class="glyphicon glyphicon-refresh"> Diagram</span></a>
        <?php
        
            echo "&nbsp;";
            if( $curentURL== '/teleradio/suspected/usfsum' ){
                $btnClass    = "btn btn-info btn-sm";
            }else{
                $btnClass    = "btn btn-default btn-sm";
            }
            echo Html::a("<span class='glyphicon glyphicon-refresh'> US-Finding Summary</span>"
                    ,['usfsum']
                    ,['class' =>$btnClass,'title'=>'สรุปภาพรวมตามการออกอัลตราซาวด์สัญจร']
                    );
        
            
            echo "&nbsp;";
            if( $curentURL== '/teleradio/suspected/usdoc' ){
                $btnClass    = "btn btn-info btn-sm";
            }else{
                $btnClass    = "btn btn-default btn-sm";
            }
            echo Html::a("<span class='glyphicon glyphicon-refresh'> US Doctor</span>"
                    ,['usdoc']
                    ,['class' =>$btnClass,'title'=>'สรุปผลงานแพทย์ ที่ทำอัลตราซาวด์']
                    );
            
            // test
            
            echo "&nbsp;";
            if( $curentURL== '/report/?tab=overview' ){
                $btnClass    = "btn btn-info btn-sm";
            }else{
                $btnClass    = "btn btn-default btn-sm";
            }
            echo Html::a("<span class='glyphicon glyphicon-refresh'> Overview</span>"
                    ,'https://cloud.cascap.in.th/report/?tab=overview'
                    ,['class' =>$btnClass,'title'=>'สรุปผลงานแพทย์ ที่ทำอัลตราซาวด์']
                    );
        ?>
    </div>
    <div class="col-sm-3">
        <?php
            //echo $reccord;
            //echo $perpage;
            //echo $curentpage;
        ?>
    </div>
    
    <div class="col-sm-1" style="text-align: right;">
        
        
    </div>
</div>

