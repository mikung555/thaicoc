<?php

use kartik\grid\GridView;
?>
<div class="row">
    <div class="pull-right col-md-3">
        <div class="form-group input-group">
            <select id="dropdown-list" class="form-control">
                <option label="เลือกหน้าที่ต้องการ" value="" selected >เลือกหน้าที่ต้องการ</option>
                <?php
                for ($iIndex = 0; $iIndex < $pageMaxpage; $iIndex++) {
                    $pageEachpage["page"][$iIndex] = "หน้า " . ($iIndex + 1);
                    ?>
                    <option label="<?php echo $pageEachpage["page"][$iIndex]; ?>" 
                            value="/teleradio/suspected/dilldownlist?page=<?php echo ($iIndex + 1); ?>&per-page=<?php echo $getPerpage; ?>&item=<?= $dataGet['item']?>&doccode=<?=$dataGet['doccode']?>" 
                            <?php
                            if (($iIndex + 1) == $getCurrentpage) {
                                echo "selected";
                            }
                            ?>
                            >หน้า <?php echo ($iIndex + 1); ?>
                    </option>
                    <?php
                }
                ?>
            </select>
            <span class="input-group-btn">
                <?php 
                if ($_GET["page"] == $pageMaxpage) { ?>
                    <a id='btn-listpage' data-url ="<?= \yii\helpers\Url::to(['suspected/dilldownlist','item'=>$dataGet['item'],'doccode' => $dataGet['doccode']]) ?>" class="btn btn-info">หน้าแรก</a>
                    <?php
                } else {
                    ?>
                    <a id='btn-listpage' data-url ="<?= \yii\helpers\Url::to(['suspected/dilldownlist','page' => $pageMaxpage,'per-page' => $getPerpage,'item'=>$dataGet['item'],'doccode' => $dataGet['doccode']]) ?>" class="btn btn-info">หน้าสุดท้าย</a>
                    <?php
                }
                ?>
            </span>
        </div>
    </div>
</div>
<?php
echo kartik\grid\GridView::widget([
    'dataProvider' => $dataProvider,
    'id' => 'gridview-submodal',
    'toolbar' => [],
        'panel' => [
        'type' => GridView::TYPE_PRIMARY,
        'heading' => 'รายชื่อผู้ป่วย',
    ],
    'pjax' => true,
    'columns' => [
        [
            'class' => 'yii\grid\SerialColumn',
            'headerOptions' => ['style' => 'text-align: center;'],
            'contentOptions' => ['style' => 'text-align: center;'],
        ],
        [
            'attribute' => 'hsitecode',
            'label' => 'Site ID',
            'format' => ['raw'],
            'headerOptions' => ['style' => 'text-align: center;'],
            'contentOptions' => ['style' => 'text-align: center;'],
        ],
        [
            'attribute' => 'hptcode',
            'label' => 'PID',
            'format' => ['raw'],
            'headerOptions' => ['style' => 'text-align: center;'],
            'contentOptions' => ['style' => 'text-align: center;','width' => '150px'],
        ],
        [
            'attribute' => 'f2v1',
            'label' => 'Exam Date',
            'headerOptions' => ['style' => 'text-align: center;'],
            'contentOptions' => ['style' => 'text-align: center;'],
        ],
        [
            'attribute' => 'hncode',
            'label' => 'HN',
            'format' => ['raw'],
            'headerOptions' => ['style' => 'text-align: center;'],
            'contentOptions' => ['style' => 'text-align: center;','width' => '200px'],
            'value'=>function($model) {
                $hncode = $model["hncode"];
                if($hncode==NULL){
                        return "-";
                }else{
                    return $hncode;
                }
            }
        ],
        [
            'attribute' => 'cid',
            'label' => 'CID',
            'format' => ['raw'],
            'headerOptions' => ['style' => 'text-align: center;'],
            'contentOptions' => ['style' => 'text-align: center;'],
            'value'=>function($model) use($sitecode){
                $hsitecode = $model["hsitecode"];
                if($hsitecode==$sitecode){
                        return $model["cid"];
                }else{
                    return "*******";
                }
            }
        ],
        [
            'attribute' => 'hptcode',
            'label' => 'HPTCODE',
            'headerOptions' => ['style' => 'text-align: center;'],
            'contentOptions' => ['style' => 'text-align: center'],
            'format' => ['raw'],
            'value'=>function($model) use($sitecode){
                $hsitecode = $model["hsitecode"];
                if($hsitecode==$sitecode){
                        return $model["hptcode"];
                }else{
                    return "************";
                }
            }
        ],
        [
            'attribute' => 'fullname',
            'label' => 'ชื่อ-นามสกุล',
            'headerOptions' => ['style' => 'text-align: center;'],
            'contentOptions' => ['style' => 'text-align: left'],
            'format' => ['raw'],
            'value'=>function($model) use($sitecode){
                $hsitecode = $model["hsitecode"];
                if($hsitecode==$sitecode){
                        return $model["fullname"];
                }else{
                    return "************";
                }
            }
        ],
        [
            'attribute' => 'age',
            'label' => 'อายุ',
            'headerOptions' => ['style' => 'text-align: center;'],
            'contentOptions' => ['style' => 'text-align: center'],
            'format' => ['raw'],
            'value'=>function($model) use($sitecode){
                $hsitecode = $model["hsitecode"];
                if($hsitecode==$sitecode){
                        return $model["age"];
                }else{
                    return "***";
                }
            }
        ],
    ],
]);
?>
<?php
$jsAdd = <<< JS
        
$('#dropdown-list').change(function(){
    var url = $('#dropdown-list').val();
    $.ajax({
        url: url,
        type: "GET",
        success: function(data){
           $('#body-list').html(data);
        }
    });
});

$('#btn-listpage').click(function(){
    var url = $(this).attr('data-url');
    $.ajax({
        url: url,
        type: "GET",
        success: function(data){
           $('#body-list').html(data);
        }
    });
});
        
JS;
$this->registerJs($jsAdd);
?>
        