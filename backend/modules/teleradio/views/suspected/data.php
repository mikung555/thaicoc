<?= \appxq\sdii\widgets\ModalForm::widget([
    'id' => 'modal-print',
    'size'=>'modal-lg',
    'tabindexEnable'=>false,
    'options'=>['style'=>'overflow-y:scroll;']
]);
?>
<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


# View สำหรับการแสดง Data ทั้งหมด โดยแสดงตามเงื่อนไข
#   1. เลือก form ที่ต้องการตรจสอบ
#   2. เลือก form หลัก (default/ที่เลือกใหม่ได้) => 
#       => จาก form หลักสามารถเลือก ช่วงเวลาได้ด้วย
#   3. มีตารางแสดง สถิติ ที่สำคัญๆ (อาจจะแสดงตาม ตารางหลักที่เราเลือก)
#   4. ส่วนแสดง Data

$this->title = Yii::t('', 'เครื่องสำหรับเรียกดูข้อมูล');

echo $this->render('_btnReloadVerify',
        [
            'reccord'=>$reccord,
            'perpage'=>$perpage,
            'curentpage'=>$curentpage,
            'tabdet' => $tabdet,
        ]);

if( 0 ){
    echo "<pre align='left'>";
    print_r($overAll);
    echo "</pre>";
}

\yii\widgets\Pjax::begin(['id'=>'inv-person-grid-pjax']);
#   filter (1) ทุกอย่างเรียก่าน _GET
echo $this->render('_filterdata',
        [
            'overAll'=>$overAll,
            'listVisit' => $listVisit,
        ]);
\yii\widgets\Pjax::end();

//$homepage = file_get_contents('http://www.cascap.info/th/index.php/events/feed-cascap');
//echo $homepage;
?>
<!--p><iframe src="http://www.cascap.info/th/index.php/events/feed-cascap" width="1024" height="600" frameborder="0" align="center"> </iframe></p-->
<?php

# show examle of table list
//echo $this->render('_datatable',
//        [
//            'overAll'=>$overAll,
//            'listVisit' => $listVisit,
//        ]);

$url = \yii\helpers\Url::to(['/teleradio/suspected/open-form-nill']);
$jsAdd = <<< JS
    init();
    function init()
    {
        waitingoff();
    }
    function waitingon()
    {
        $('#waiting').css('display', 'block');//not show
        $("#waiting").button('loading');
    }
    function waitingoff()
    {
        $('#waiting').css('display', 'none');//not show
    }
        
$('#inv-person-grid-pjax').on('click', '.print-ezform', function() {
    var url = $(this).attr('data-url');
    var ezf_id = $(this).attr('ezf_id');
    var dataid = $(this).attr('dataid');
    var ptid = $(this).attr('ptid');
    var view = $(this).attr('view');
    console.log(this);
    if ( view=='modal' ) {
        $.ajax({
            type    : "GET",
            cache   : false,
            url     : "$url",
            dataType: 'json',
            data    : {
                ezf_id: ezf_id,
                dataid: dataid,
                ptid: ptid,
            },
            success  : function(response) {
                console.log(response);
            },
            error : function(){

            }
        });
        modalEzfrom(url);
    }else{
        window.open(url,'_blank');
    }
});

function modalEzfrom(url) {
    $('#modal-print .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
    $('#modal-print').modal('show');
    $.ajax({
	method: 'POST',
	url: url,
	dataType: 'HTML',
	success: function(result, textStatus) {
	    $('#modal-print .modal-content').html(result);
	    return false;
	}
    });
}  

$('#modal-print').on('hidden.bs.modal', function (e) {
  // do something...
  //console.log($('#modal-print'));
  //alert('x');
  //var dataid = $(this).attr('dataid');
})
        
    
    function getAmphur(provinceCode,value){
        $.ajax({
            type    : "GET",
            cache   : false,
            url     : "/teleradio/suspected/get-data-list-amphur",
            data    : {
                provinceCode: provinceCode
            },
            success  : function(response) {
                $("#selectAmphur").html(response);
                if(value!=""){
                    $("#selectAmphur").val(value);
                }
            },
            error : function(){
                $("#selectAmphur").html("");
            }
        });
    }
        
    function getHospital(provinceCode,amphurCode, value){
        $.ajax({
            type    : "GET",
            cache   : false,
            url     : "/teleradio/suspected/get-data-list-all-hospital-thai",
            data    : {
                provinceCode: provinceCode,
                amphurCode: amphurCode
            },
            success  : function(response) {
                $("#selectHospital").html(response);
                if(value!=""){
                    $("#selectHospital").val(value);
                }
            },
            error : function(){
                $("#selectHospital").html("");
            }
        });
    }
        
        
        
    function getdata(cid,div,refresh)
    {
        $("#table_resultsearch").empty();
        //$("#table_staticall").empty();
        
        var sort=$(this).attr("data-sort");
        var colum=$(this).attr("colum");
        var provincecode = $("#selectProvince").find(':selected').attr('provincecode');
        var acode = $("#selectAmphur").find(':selected').attr('amphurcode');
        var hcode = $("#selectHospital").find(':selected').attr('hcode');
        var _reg = 0;
        var _cca01 = 0;
        var _cca02 = 0;
        var _pdf1 = 0;
        var _pdf2 = 0;
        var _pdf3 = 0;
        var _scca = 0;
        var _img02 = 0;
        var _cca02p1 = 0;
        var _nccact = 0;
        var _ccact = 0;
        var _cca03 = 0;
        var _ncca03 = 0;
        var _sur03 = 0;
        var _dead03 = 0;
        var _cca04 = 0;
        var _ncca04 = 0;
        var _cca05 = 0;
        var _dead05 = 0;
        var _maincca01 = 0;
        var _maincca02 = 0;
        var _maincca02p1 = 0;
        var _maincca03 = 0;
        var _maincca04 = 0;
        var _maincca05 = 0;
        
        if( $("#_reg").is(':checked') ){
            _reg = 1;
        }
        if( $("#_cca01").is(':checked') ){
            _cca01 = 1;
        }
        if( $("#_cca02").is(':checked') ){
            _cca02 = 1;
        }
        if( $("#_maincca02pdf1").is(':checked') ){
            _pdf1 = 1;
        }
        if( $("#_maincca02pdf2").is(':checked') ){
            _pdf2 = 1;
        }
        if( $("#_maincca02pdf3").is(':checked') ){
            _pdf3 = 1;
        }
        if( $("#_maincca02suspected").is(':checked') ){
            _scca = 1;
        }
        if( $("#_maincca02usimage").is(':checked') ){
            _img02 = 1;
        }
        if( $("#_cca02p1").is(':checked') ){
            _cca02p1 = 1;
        }
        if( $("#_maincca02p1notcca").is(':checked') ){
            _nccact = 1;
        }
        if( $("#_maincca02p1cca").is(':checked') ){
            _ccact = 1;
        }
        if( $("#_cca03").is(':checked') ){
            _cca03 = 1;
        }
        if( $("#_maincca03notcca").is(':checked') ){
            _ncca03 = 1;
        }
        if( $("#_maincca03sur").is(':checked') ){
            _sur03 = 1;
        }
        if( $("#_maincca03dead").is(':checked') ){
            _dead03 = 1;
        }
        if( $("#_cca04").is(':checked') ){
            _cca04 = 1;
        }
        if( $("#_maincca04notcca").is(':checked') ){
            _ncca04 = 1;
        }
        if( $("#_cca05").is(':checked') ){
            _cca05 = 1;
        }
        if( $("#_maincca05dead").is(':checked') ){
            _dead05 = 1;
        }
        if( $("#_maincca01").is(':checked') ){
            _maincca01 = 1;
        }
        if( $("#_maincca02").is(':checked') ){
            _maincca02 = 1;
        }
        if( $("#_maincca02p1").is(':checked') ){
            _maincca02p1 = 1;
        }
        if( $("#_maincca03").is(':checked') ){
            _maincca03 = 1;
        }
        if( $("#_maincca04").is(':checked') ){
            _maincca04 = 1;
        }
        if( $("#_maincca05").is(':checked') ){
            _maincca05 = 1;
        }
        //alert(provincecode);
        //alert($("#project84-fromdate").val());
        //alert($("#project84-todate").val());
        var url = "/teleradio/suspected/get-data-list";
        //alert(url);
        var sec = "1";
        $.get(url
            , {
                fromDate:$("#project84-fromdate").val(),
                toDate:$("#project84-todate").val(),
                _reg:_reg,
                _cca01:_cca01,
                _cca02:_cca02,
                _pdf1:_pdf1,
                _pdf2:_pdf2,
                _pdf3:_pdf3,
                _scca:_scca,
                _img02:_img02,
                _cca02p1:_cca02p1,
                _nccact:_nccact,
                _ccact:_ccact,
                _cca03:_cca03,
                _ncca03:_ncca03,
                _sur03:_sur03,
                _dead03:_dead03,
                _cca04:_cca04,
                _ncca04:_ncca04,
                _cca05:_cca05,
                _dead05:_dead05,
                _maincca01:_maincca01,
                _maincca02:_maincca02,
                _maincca02p1:_maincca02p1,
                _maincca03:_maincca03,
                _maincca04:_maincca04,
                _maincca05:_maincca05,
                _pcode:provincecode,
                _acode:acode,
                _hcode:hcode,
                refresh:refresh,
            }
        )
        .done(function( data ) {
            switch (sec) {
                case "1":
                    $("#table_resultsearch").empty();
                    $("#table_resultsearch").html(data);
                    waitingoff();
                    break;
                }
            }
        );
    }
    
    $(document).on("change","#selectProvince", function() {
    //$("#selectProvince").change(function(){
        //alert($("#selectProvince").find(':selected').attr('provincecode'));
        if( $("#selectProvince").find(':selected').attr('provincecode') != "" ){
            getAmphur($("#selectProvince").find(':selected').attr('provincecode'),"");
        }
    });
    
    $(document).on("change","#selectAmphur", function() {
    //$("#inputAmphur").change(function(){
        //if( $("#inputProvence").val()!="" && $("#inputAmphur").val()!="" ){
        if( $("#selectProvince").find(':selected').attr('provincecode') != "" && $("#selectAmphur").find(':selected').attr('amphurcode') != "" ){
            getHospital($("#selectProvince").find(':selected').attr('provincecode'),$("#selectAmphur").find(':selected').attr('amphurcode'),"");
        }
    });
    
    $(document).on("click","#btnsubmit", function() {
        //alert('search');
        //var cid= $("#_cidsearch").val();
        waitingon();
        //alert(cid);
        getdata('','','yes');
    });
        
    $(document).on("click","#btnSave", function() {
        //alert('save');
        var newcid= $("#_newcid").val();
        var oldcid= $("#_oldcid").val();
        //alert(cid);
        getdata(cid,'','yes');
    });
        
        
    $(document).on("click", "*[id^=sort]", function() {
        var data_sort=$(this).attr("data-sort");
        var fromDate=$(this).attr("fromDate");
        var toDate=$(this).attr("toDate");
        var sec=$(this).attr("sec");
        var colum=$(this).attr("colum");
        var page=$(this).attr("page");
        var div=$(this).attr('id');
        var sec="1";


        switch (data_sort) {
            case "asc":
                $(this).attr("data-sort","desc");
                break;
            case "desc":
                $(this).attr("data-sort","asc");
                break;
        }
        //waitingon();
        //$("#table_resultsearch").empty();
        var url = "/teleradio/suspected/get-data-list-temp";
        $.get(url, {
            sort:data_sort,
            fromDate:fromDate,
            toDate:toDate,
            sec:sec,
            colum:colum,
            page:page,
            div:div,
        })
        .done(function( data ) {
            switch (sec) {
                case "1":
                    //$("#table_resultsearch").empty();
                    $("#table_resultsearch").html(data);
                    //waitingoff();
                    break;
            }
        });
    });
    $(document).on("change", "#pageselect", function() {
        var data_sort=$(this).find(':selected').attr('sort');
        var fromDate=$(this).find(':selected').attr("fromdate");
        var toDate=$(this).find(':selected').attr("todate");
        var sec=$(this).find(':selected').attr('sec');
        var colum=$(this).find(':selected').attr('colum');
        var page=$(this).find(':selected').attr('page');
        var div=$(this).attr('id');
        //var refresh=$(this).find(':selected').attr('refresh');
        var sec="1";
        
        //alert($(this).find(':selected').attr('page'));
        waitingon();
        $("#table_resultsearch").empty();
        var url = "/teleradio/suspected/get-data-list-temp";
        $.get(url, {
            sort:data_sort,
            fromDate:fromDate,
            toDate:toDate,
            sec:sec,
            colum:colum,
            page:page,
            div:div,
        })
        .done(function( data ) {
            switch (sec) {
                case "1":
                    $("#table_resultsearch").empty();
                    waitingoff();
                    $("#table_resultsearch").html(data);
                    break;
            }
        });
    });
        
JS;
$this->registerJs($jsAdd);

