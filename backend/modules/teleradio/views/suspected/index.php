<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use yii\helpers\Html;

use kartik\grid\GridView;
//use yii\bootstrap\Nav;
//use yii\bootstrap\NavBar;
//use yii\widgets\LinkPager;
use app\modules\teleradio\models\SuspectedCca;
use backend\modules\teleradio\classes\QueryHospital;
use Yii;

$this->title = Yii::t('', 'Suspected case');






$reccord = SuspectedCca::find()->count();
$perpage = 20;
$curentpage = Yii::$app->request->get('page');

echo $this->render('_btnReload',
        [
            'reccord'=>$reccord,
            'perpage'=>$perpage,
            'curentpage'=>$curentpage,
            'tabdet' => $tabdet,
        ]);



# table overall show
echo $this->render('_overallShow',
        [
            'overAll'=>$overAll,
        ]);

# grid
$condition=['f2v6a3b1'=>'1'];
$model = SuspectedCca::find()->where($condition);
$model->orderBy(['f3v1'=>'desc']);
echo GridView::widget([
    'caption'=>'<h2>รายชื่ออาสมัคร ที่ตรวจอัลตราซาวด์แล้วสงสัยมะเร็งท่อน้ำดี</h2>',
                'showHeader'=>true,
                'layout' => "{summary}\n{items}\n{pager}",
                'options' => array('class' => 'grid-view'),
                'tableOptions' => array('class' => 'table table-striped table-bordered'),
//              'pager'=>'yii\widgets\LinkSorter',
      'dataProvider' => $dataProvider,
      //'filterModel' => $searchModel,
      'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        [
          'attribute' => 'hsitecode',
          'value'=>function($model){
            $hospital = QueryHospital::getHospitalByHCODE($model->hsitecode);
            return $model->hsitecode.': '.$hospital['name'];
          }
        ],
        [
          'attribute' => 'hptcode',
          'value'=>function($model){
            return $model->hptcode;
          }
        ],
        [
            'format' => 'raw',
          'label'=>'ชื่อ-นามสกุล',
          'value'=>function($model){
            return Html::a("<span class='glyphicon glyphicon-new-window'> ".$model->title.$model->name.' '.$model->surname."</span>",
                                [
                                    '/inputdata/step4',
                                    'comp_id_target' => '1437725343023632100',
                                    'ezf_id' => '1437619524091524800',
                                    'dataid' => $model->id,
                                    'target' => base64_encode($model->ptid)
                                ],
                                [
                                    'data-toggle' =>'tooltip', 'data-original-title' => 'คลิกเพื่อเข้าดูรายละเอียดที่ตอบมา',
                                    'data-pjax' => '0',
                                    'target'=>'_blank'
                                ]
                    );
          }
        ],
        [
          'attribute' => 'f2v1',
          'value'=>function($model){
            return $model->f2v1;
          }
        ],
        [
          'attribute' => 'usabnormal',
          'value'=>function($model){
            return $model->usabnormal;
          }
        ],
        [
          'attribute' => 'ussuspected',
          'format'=>'html',
          'value'=>function($model){
            return $model->ussuspected;
          }
        ],
        [
          'attribute' => 'rstat',
          'format'=>'html',
          'value'=>function($model, $key, $index, $column){
            if($model->rstat==2){
                return "<i class=\"glyphicon glyphicon-ok\" style=\"color: green\"> Submited </i>";
            }else if($model->rstat==1){
                return "<i class=\"glyphicon glyphicon-time\" style=\"color: orange\"> Waiting </i>";
            }else{
                return "<i class=\"glyphicon glyphicon-remove\" style=\"color: red\"> - </i>";
            }
          }
        ],
        [
          'attribute' => 'ctmri_result',
          'format'=>'html',
          'value'=>function($model){
            return $model->ctmri_result;
          }
        ],
        [
          'attribute' => 'treat_result',
          'format'=>'html',
          'value'=>function($model){
            return $model->treat_result;
          }
        ],
        [
          'attribute' => 'patho_result',
          'format'=>'html',
          'value'=>function($model){
            return $model->patho_result;
          }
        ],
        // ...
      ],
  ]);

