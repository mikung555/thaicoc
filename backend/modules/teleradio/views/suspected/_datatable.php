<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


use yii\helpers\Html;
use yii\helpers\Url;
use kartik\export\ExportMenu;
use yii\data\ArrayDataProvider;


$_usertable = Yii::$app->user->identity->id;
$url ="http://tools.cascap.in.th/v4cascap/testexcel/PHPExcel_1/Examples/01teleradiosuspecteddata.php?table=summary_data_".$_usertable;
echo Html::a( 
        'Download Excel<span class="glyphicon glyphicon-download-alt" style="color: green"></span><i class="fa fa-file-excel-o fa-2" aria-hidden="true" style="color: green"></i>', 
        Url::to($url),
        ['id'=>'LinkExcel']
        );


if( 0 ){
    echo "<pre align='left'>";
    print_r($dp1);
}

$request = Yii::$app->request;

?>

    <div class="table-responsive">
        <div id="report84-dynagrid-2-pjax">
            <div class="grid">
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                        <div style="text-align: left;">
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            <select id="pageselect" >
                                <?php
                                    for($ipage=1; $ipage<$paging['countPage']+1; $ipage++){
                                ?>
                                <option sort='<?php echo $sort; ?>'
                                        fromDate='<?php echo $datadate['fromDate']; ?>'
                                        toDate='<?php echo $datadate['toDate']; ?>'
                                        sec='<?php echo $sec; ?>'
                                        colum='<?php echo $colum; ?>'
                                        div='<?php echo $div; ?>'
                                        refresh='yes'
                                        page='<?php echo $ipage; ?>'
                                <?php
                                        if($page==$ipage){
                                            echo " selected='true' ";
                                        }
                                ?>
                                        ><?php echo $ipage; ?></option>
                                <?php
                                    }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                        <div style="text-align: right;">
                            <label for="pageselect" style="margin-top: 7px; width: 500px;">จำนวนข้อมูล ทั้งหมด <?php echo $paging['allPatient']; ?> คน และมีจำนวนการตรวจทั้งหมด <?php echo $paging['allRecord']; ?> ครั้ง อยู่ที่หน้า: </label>
                            <select id="pageselect" >
                                <?php
                                    for($ipage=1; $ipage<$paging['countPage']+1; $ipage++){
                                ?>
                                <option sort='<?php echo $sort; ?>'
                                        fromDate='<?php echo $datadate['fromDate']; ?>'
                                        toDate='<?php echo $datadate['toDate']; ?>'
                                        sec='<?php echo $sec; ?>'
                                        colum='<?php echo $colum; ?>'
                                        div='<?php echo $div; ?>'
                                        refresh='yes'
                                        page='<?php echo $ipage; ?>'
                                <?php
                                        if($page==$ipage){
                                            echo " selected='true' ";
                                        }
                                ?>
                                        ><?php echo $ipage; ?></option>
                                <?php
                                    }
                                ?>
                            </select>
                            &nbsp;&nbsp;&nbsp;&nbsp;
                        </div>
                    </div>
                </div>
            </div>
            <div id="report84-dynagrid-2" data-krajee-grid="kvGridInit_adf683c2">
                <div id="report84-dynagrid-2-container" class="table-responsive kv-grid-container">


                    <table class="kv-grid-table table table-bordered table-striped">
                        <thead>
                        <tr style="text-align:center">
                            <th rowspan="2" class="kv-align-center kv-align-middle" style="min-width:60px;text-align:right;">ลำดับที่</th>
                            <th rowspan="2" class="kv-align-center kv-align-middle" style="min-width:40px;text-align:left;">
                                <a href="#table_secshow1" id='sort101' data-sort='asc'
                                    fromDate='<?php echo $datadate['fromDate'] ?>'
                                    toDate='<?php echo $datadate['fromDate'] ?>' sec='1' colum='zone' 
                                    page='<?php echo $page; ?>'>เขต</a>
                            </th>
                            <th rowspan="2" class="kv-align-middle" style="min-width: 150px;text-align:left;">
                                <a href="#table_secshow1" id='sort102' data-sort='asc'
                                    fromDate='<?php echo $datadate['fromDate'] ?>'
                                    toDate='<?php echo $datadate['toDate'] ?>' sec='1' colum='province' 
                                    page='<?php echo $page; ?>'>จังหวัด</a>
                            </th>
                            <th rowspan="2" class="kv-align-middle" style="min-width: 150px;text-align:left;">
                                <a href="#table_secshow1" id='sort103' data-sort='asc'
                                    fromDate='<?php echo $datadate['fromDate'] ?>'
                                    toDate='<?php echo $datadate['toDate'] ?>' sec='1' colum='amphur' 
                                    page='<?php echo $page; ?>'>อำเภอ</a>
                            </th>
                            <th rowspan="2" class="kv-align-middle" style="min-width: 190px;text-align:left;">
                                <a href="#table_secshow1" id='sort104' data-sort='asc'
                                    fromDate='<?php echo $datadate['fromDate'] ?>'
                                    toDate='<?php echo $datadate['toDate'] ?>' sec='1' colum='hsitecode' 
                                    page='<?php echo $page; ?>'>โรงพยาบาล</a>
                            </th>
                            <th rowspan="2" class="kv-align-middle" style="min-width: 90px;text-align:left;">
                                <a href="#table_secshow1" id='sort1041' data-sort='asc'
                                    fromDate='<?php echo $datadate['fromDate'] ?>'
                                    toDate='<?php echo $datadate['toDate'] ?>' sec='1' colum='hsitecode' 
                                    page='<?php echo $page; ?>'>Site ID</a>
                            </th>
                            <th rowspan="2" class="kv-align-middle" style="min-width: 90px;text-align:left;">
                                <a href="#table_secshow1" id='sort1042' data-sort='asc'
                                    fromDate='<?php echo $datadate['fromDate'] ?>'
                                    toDate='<?php echo $datadate['toDate'] ?>' sec='1' colum='hptcode' 
                                    page='<?php echo $page; ?>'>PID</a>
                            </th>
                            <th rowspan="2" class="kv-align-middle" style="min-width: 90px;text-align:left;">
                                <a href="#table_secshow1" id='sort105' data-sort='asc'
                                    fromDate='<?php echo $datadate['fromDate'] ?>'
                                    toDate='<?php echo $datadate['toDate'] ?>' sec='1' colum='hncode' 
                                    page='<?php echo $page; ?>'>HN</a>
                            </th>
                            <th rowspan="2" class="kv-align-middle" style="min-width: 150px;text-align:left;">
                                <a href="#table_secshow1" id='sort106' data-sort='asc'
                                    fromDate='<?php echo $datadate['fromDate'] ?>'
                                    toDate='<?php echo $datadate['toDate'] ?>' sec='1' colum='name' 
                                    page='<?php echo $page; ?>'>ชื่อ-สกุล</a>
                            </th>
                            <th rowspan="2" class="kv-align-middle" style="min-width: 90px;text-align:left;">
                                <a href="#table_secshow1" id='sort107' data-sort='asc'
                                    fromDate='<?php echo $datadate['fromDate'] ?>'
                                    toDate='<?php echo $datadate['toDate'] ?>' sec='1' colum='cohorttype_chartreview' 
                                    page='<?php echo $page; ?>'>กลุ่มคนไข้</a>
                            </th>
                            <th rowspan="2" class="kv-align-middle" style="min-width: 90px;text-align:left;">
                                <a href="#table_secshow1" id='sort108' data-sort='asc'
                                    fromDate='<?php echo $datadate['fromDate'] ?>'
                                    toDate='<?php echo $datadate['toDate'] ?>' sec='1' colum='f1vdcomp' 
                                    page='<?php echo $page; ?>'>CCA-01</a>
                            </th>
                            <th colspan="3" class="kv-align-center kv-align-middle" style="text-align: center">Ultrasound</th>
                            <th colspan="2" class="kv-align-center kv-align-middle" style="text-align: center">CT/MRI</th>
                            <th colspan="2" class="kv-align-center kv-align-middle" style="text-align: center">รักษา</th>
                            <th colspan="11" class="kv-align-center kv-align-middle" style="text-align: center">Patho</th>
                            <th colspan="2" class="kv-align-right kv-align-middle" style="text-align:center;">CCA-05</th>
                            <th rowspan="2" class="kv-align-right kv-align-middle" style="min-width:100px;text-align:left;"><a
                                    href="#table_secshow1" id='sort501' data-sort='asc'
                                    fromDate='<?php echo $datadate['fromDate'] ?>'
                                    toDate='<?php echo $datadate['toDate'] ?>' sec='2' colum='dead' 
                                    page='<?php echo $page; ?>'>Dead</a>
                            </th>
                        </tr>
                        <tr>
                            <th class="kv-align-center kv-align-middle" style="min-width: 140px;text-align:left;">
                                <a href="#table_secshow1" id='sort201' data-sort='asc'
                                    fromDate='<?php echo $datadate['fromDate'] ?>'
                                    toDate='<?php echo $datadate['toDate'] ?>' sec='1' colum='f2v1' 
                                    page='<?php echo $page; ?>'>วันที่ตรวจ</a>
                            </th>
                            <th class="kv-align-center kv-align-middle" style="min-width: 150px;text-align:left;">
                                <a href="#table_secshow1" id='sort202' data-sort='asc'
                                    fromDate='<?php echo $datadate['fromDate'] ?>'
                                    toDate='<?php echo $datadate['toDate'] ?>' sec='1' colum='cca02abnormal' 
                                    page='<?php echo $page; ?>'>Abnormal</a>
                            </th>
                            <th class="kv-align-right kv-align-middle" style="min-width: 150px;text-align:left;"><a
                                    href="#table_secshow1" id='sort203' data-sort='asc'
                                    fromDate='<?php echo $datadate['fromDate'] ?>'
                                    toDate='<?php echo $datadate['toDate'] ?>' sec='1' colum='suspectedcca' 
                                    page='<?php echo $page; ?>'>Suspected</a>
                            </th>
                            
                            <th class="kv-align-right kv-align-middle" style="min-width:120px;text-align:right;">
                                <a href="#table_secshow1" id='sort211' data-sort='desc'
                                    fromDate='<?php echo $datadate['fromDate'] ?>'
                                    toDate='<?php echo $datadate['toDate'] ?>' sec='1' colum='ctmridate' 
                                    page='<?php echo $page; ?>'>Date</a>
                            </th>
                            <th class="kv-align-right kv-align-middle" style="min-width:150px;text-align:left;"><a
                                    href="#table_secshow1" id='sort212' data-sort='desc'
                                    fromDate='<?php echo $datadate['fromDate'] ?>'
                                    toDate='<?php echo $datadate['toDate'] ?>' sec='1' colum='ctmriresult' 
                                    page='<?php echo $page; ?>'>CT/MRI</a>
                            </th>
                            
                            <th class="kv-align-right kv-align-middle" style="min-width:120px;text-align:right;">
                                <a href="#table_secshow1" id='sort301' data-sort='desc'
                                    fromDate='<?php echo $datadate['fromDate'] ?>'
                                    toDate='<?php echo $datadate['toDate'] ?>' sec='1' colum='cca03date' 
                                    page='<?php echo $page; ?>'>Date</a>
                            </th>
                            <th class="kv-align-right kv-align-middle" style="min-width: 175px;text-align:left;">
                                <a
                                    href="#table_secshow1" id='sort302' data-sort='asc'
                                    fromDate='<?php echo $datadate['fromDate'] ?>'
                                    toDate='<?php echo $datadate['toDate'] ?>' sec='1' colum='cca03result' 
                                    page='<?php echo $page; ?>'>หัตถการ</a>
                            </th>
                            
                            <th class="kv-align-right kv-align-middle" style="min-width:120px;text-align:right;">
                                <a href="#table_secshow1" id='sort401' data-sort='desc'
                                    fromDate='<?php echo $datadate['fromDate'] ?>'
                                    toDate='<?php echo $datadate['toDate'] ?>' sec='1' colum='pathodate' 
                                    page='<?php echo $page; ?>'>Date</a>
                            </th>
                            <th class="kv-align-right kv-align-middle" style="min-width:150px;text-align:left;"><a
                                    href="#table_secshow1" id='sort402' data-sort='asc'
                                    fromDate='<?php echo $datadate['fromDate'] ?>'
                                    toDate='<?php echo $datadate['toDate'] ?>' sec='1' colum='pathoresult' 
                                    page='<?php echo $page; ?>'>Patho</a>
                            </th>
                            <th class="kv-align-right kv-align-middle" style="min-width:50px;text-align:left;"><a
                                    href="#table_secshow1" id='sort403' data-sort='desc'
                                    fromDate='<?php echo $datadate['fromDate'] ?>'
                                    toDate='<?php echo $datadate['toDate'] ?>' sec='1' colum='stage0' 
                                    page='<?php echo $page; ?>'>0</a>
                            </th>
                            <th class="kv-align-right kv-align-middle" style="min-width:50px;text-align:left;"><a
                                    href="#table_secshow1" id='sort404' data-sort='desc'
                                    fromDate='<?php echo $datadate['fromDate'] ?>'
                                    toDate='<?php echo $datadate['toDate'] ?>' sec='1' colum='stage1' 
                                    page='<?php echo $page; ?>'>I</a>
                            </th>
                            <th class="kv-align-right kv-align-middle" style="min-width:50px;text-align:left;"><a
                                    href="#table_secshow1" id='sort405' data-sort='desc'
                                    fromDate='<?php echo $datadate['fromDate'] ?>'
                                    toDate='<?php echo $datadate['toDate'] ?>' sec='1' colum='stage2' 
                                    page='<?php echo $page; ?>'>II</a>
                            </th>
                            <th class="kv-align-right kv-align-middle" style="min-width:50px;text-align:left;"><a
                                    href="#table_secshow1" id='sort406' data-sort='desc'
                                    fromDate='<?php echo $datadate['fromDate'] ?>'
                                    toDate='<?php echo $datadate['toDate'] ?>' sec='1' colum='stage3' 
                                    page='<?php echo $page; ?>'>III</a>
                            </th>
                            <th class="kv-align-right kv-align-middle" style="min-width:50px;text-align:left;"><a
                                    href="#table_secshow1" id='sort407' data-sort='desc'
                                    fromDate='<?php echo $datadate['fromDate'] ?>'
                                    toDate='<?php echo $datadate['toDate'] ?>' sec='1' colum='stage3a' 
                                    page='<?php echo $page; ?>'>IIIA</a>
                            </th>
                            <th class="kv-align-right kv-align-middle" style="min-width:50px;text-align:left;"><a
                                    href="#table_secshow1" id='sort408' data-sort='desc'
                                    fromDate='<?php echo $datadate['fromDate'] ?>'
                                    toDate='<?php echo $datadate['toDate'] ?>' sec='1' colum='stage3b' 
                                    page='<?php echo $page; ?>'>IIIB</a>
                            </th>
                            <th class="kv-align-right kv-align-middle" style="min-width:50px;text-align:left;"><a
                                    href="#table_secshow1" id='sort409' data-sort='desc'
                                    fromDate='<?php echo $datadate['fromDate'] ?>'
                                    toDate='<?php echo $datadate['toDate'] ?>' sec='1' colum='stage4a' 
                                    page='<?php echo $page; ?>'>IVA</a>
                            </th>
                            <th class="kv-align-right kv-align-middle" style="min-width:50px;text-align:left;"><a
                                    href="#table_secshow1" id='sort410' data-sort='desc'
                                    fromDate='<?php echo $datadate['fromDate'] ?>'
                                    toDate='<?php echo $datadate['toDate'] ?>' sec='1' colum='stage4b' 
                                    page='<?php echo $page; ?>'>IVB</a>
                            </th>
                            <th class="kv-align-right kv-align-middle" style="min-width:50px;text-align:left;"><a
                                    href="#table_secshow1" id='sort411' data-sort='desc'
                                    fromDate='<?php echo $datadate['fromDate'] ?>'
                                    toDate='<?php echo $datadate['toDate'] ?>' sec='1' colum='stageuk' 
                                    page='<?php echo $page; ?>'>UK</a>
                            </th>
                            <th class="kv-align-right kv-align-middle" style="min-width:120px;text-align:left;"><a
                                    href="#table_secshow1" id='sort411' data-sort='asc'
                                    fromDate='<?php echo $datadate['fromDate'] ?>'
                                    toDate='<?php echo $datadate['toDate'] ?>' sec='1' colum='cca05lastvisit' 
                                    page='<?php echo $page; ?>'>วันที่</a>
                            </th>
                            <th class="kv-align-right kv-align-middle" style="min-width:150px;text-align:left;"><a
                                    href="#table_secshow1" id='sort411' data-sort='asc'
                                    fromDate='<?php echo $datadate['fromDate'] ?>'
                                    toDate='<?php echo $datadate['toDate'] ?>' sec='1' colum='dead' 
                                    page='<?php echo $page; ?>'>ผลการติดตาม</a>
                            </th>
                        </tr>

                        </thead>
                        <tbody>
                        <!-- TABLE DATA -->

                        <?php
                        $num = (($page-1)*$limitrec)+1;
                        if( count($dataProvider)>0 )foreach ($dataProvider as $key) {
                            ?>
                            <tr style="text-align:right; 
                                <?php
                                    if( $viewform[$key['ptid']]=='1' ){
                                        echo "background-color: #e3e3c7;";
                                    }
                                ?>">
                                <td class="kv-align-center kv-align-middle"
                                    style="text-align:right;">
                                        <?php
                                            if( $viewform[$key['ptid']]=='1' ){
                                                echo "<span class='glyphicon glyphicon-ok' style='color: green'></span>";
                                            }
                                        ?>
                                        <?php echo $num ?>
                                </td>
                                <td style="text-align:left;"><?php echo $key['zone'] ?></td>
                                <td style="text-align:left;"><?php echo $key['province'] ?></td>
                                <td style="text-align:left;"><?php echo $key['amphur'] ?></td>
                                <td style="text-align:left;"><?php echo $key['hsitecode'].": ".$key['hospitalname'] ?></td>
                                <td style="text-align:left;"><?php echo $key['hsitecode'] ?></td>
                                <td style="text-align:left;"><?php echo $key['hptcode'] ?></td>
                                <td style="text-align:left;"><?php echo $key['hncode'] ?></td>
                                <td class="kv-align-middle" style="text-align:left;">
                                    <?php
                                    //$urlToReg = "/inputdata/redirect-page?dataid=".$key['_id_reg']."&ezf_id=".$key['_ezf_id_reg'];
                                    $urlToReg = "/teleradio/suspected/open-form?dataid=".$key['_id_reg']."&ezf_id=".$key['_ezf_id_reg']."&ptid=".$key['ptid'];
//                                    echo Html::a( 
//                                            $key['name']." ".$key['surname'], 
//                                            $urlToReg,
//                                            ['target'=>'_blank']
//                                            );
                                    echo Html::a( 
                                            $key['name']." ".$key['surname'], 
                                            NULL,
                                            [
                                                'data-url'=>Url::to(['/inv/inv-person/ezform-print',
                                                    'ezf_id'=>$key['_ezf_id_reg'],
                                                    'dataid'=>$key['_id_reg'],
                                                    'comp_target'=>'1437725343023632100',
                                                    'target'=>base64_encode($key['ptid']),
                                                    'comp_id_target'=> '1437725343023632100',
                                                    'readonly' => '1',
                                                ]),
                                                'ezf_id'=>$key['_ezf_id_reg'],
                                                'dataid'=>$key['_id_reg'],
                                                'ptid'=>$key['ptid'],
                                                'style'=>'cursor: pointer;',
                                                'view'=>'modal',
                                                'title' => Yii::t('app', 'เปิดอ่านเท่านั้น'),
                                                'class'=>'btn btn-link btn-xs print-ezform',
                                                ]
                                            );
                                    ?>
                                </td>
                                <td style="text-align:left;"><?php echo $key['cohorttype_chartreview'] ?></td>
                                <td style="text-align:left;">
                                    <?php 
                                    #echo $key['f1vdcomp'];
                                    //$urlToCca01 = "/inputdata/redirect-page?dataid=".$key['_id_cca01']."&ezf_id=".$key['_ezf_id_cca01'];
                                    $urlToCca01 = "/teleradio/suspected/open-form?dataid=".$key['_id_cca01']."&ezf_id=".$key['_ezf_id_cca01']."&ptid=".$key['ptid'];
                                    echo Html::a( 
                                            $key['f1vdcomp'], 
                                            NULL,
                                            [
                                                'data-url'=>Url::to(['/inv/inv-person/ezform-print',
                                                    'ezf_id'=>$key['_ezf_id_cca01'],
                                                    'dataid'=>$key['_id_cca01'],
                                                    'comp_target'=>'1437725343023632100',
                                                    'target'=>base64_encode($key['ptid']),
                                                    'comp_id_target'=> '1437725343023632100',
                                                    'readonly' => '1',
                                                ]),
                                                'ezf_id'=>$key['_ezf_id_cca01'],
                                                'dataid'=>$key['_id_cca01'],
                                                'ptid'=>$key['ptid'],
                                                'style'=>'cursor: pointer;',
                                                'view'=>'modal',
                                                'title' => Yii::t('app', 'เปิดอ่านเท่านั้น'),
                                                'class'=>'btn btn-link btn-xs print-ezform',
                                                ]
                                            );
                                    ?>
                                </td>
                                <td class="kv-align-center kv-align-middle"
                                    style="text-align:right;">
                                        <?php //echo $key[f2v1] ?>
                                    <?php
                                    if( $key['sys_usimageexist']=='1'){
                                        echo '<span class="glyphicon glyphicon-picture"></span>';
                                    }
                                    //$urlToCca02 = "/inputdata/redirect-page?dataid=".$key['_id_cca02']."&ezf_id=".$key['_ezf_id_cca02'];
                                    $urlToCca02 = "/teleradio/suspected/open-form?dataid=".$key['_id_cca02']."&ezf_id=".$key['_ezf_id_cca02']."&ptid=".$key['ptid'];
                                    echo Html::a( 
                                            $key['f2v1'], 
                                            NULL,
                                            [
                                                'data-url'=>Url::to(['/inv/inv-person/ezform-print',
                                                    'ezf_id'=>$key['_ezf_id_cca02'],
                                                    'dataid'=>$key['_id_cca02'],
                                                    'comp_target'=>'1437725343023632100',
                                                    'target'=>base64_encode($key['ptid']),
                                                    'comp_id_target'=> '1437725343023632100',
                                                    'readonly' => '1',
                                                ]),
                                                'ezf_id'=>$key['_ezf_id_cca02'],
                                                'dataid'=>$key['_id_cca02'],
                                                'ptid'=>$key['ptid'],
                                                'style'=>'cursor: pointer;',
                                                'view'=>'modal',
                                                'title' => Yii::t('app', 'เปิดอ่านเท่านั้น'),
                                                'class'=>'btn btn-link btn-xs print-ezform',

                                            ]
                                            );
                                    //echo $key['sys_usimageexist'];
                                    if( strlen($key['_id_cca02'])>0){
                                        echo Html::a(
                                                '<span class="glyphicon glyphicon-edit"></span>',
                                                NULL,
                                                [
                                                    'data-url'=>Url::to($urlToCca02),
                                                    'ezf_id'=>$key['_ezf_id_cca02'],
                                                    'dataid'=>$key['_id_cca02'],
                                                    'ptid'=>$key['ptid'],
                                                    'style'=>'cursor: pointer;',
                                                    'view'=>'newwindow',
                                                    'title' => Yii::t('app', 'เปิดเพื่อแก้ไข และดูภาพ'),
                                                    'class'=>'btn btn-link btn-xs print-ezform',
                                                    ]
                                                );
                                    }
                                    ?>
                                </td>
                                <td style="text-align:left;"><?php echo $key['cca02abnormal'] ?></td>
                                <td style="text-align:left;">
                                    <?php echo $key['suspectedcca'] ?></td>
                                    <?php 
//                                    if(strlen($key['suspectedcca'])>0){
//                                        echo $key['suspectedcca'];
//                                    }else{
//                                        echo "<font style=\"color:red\">ไม่ได้ระบุ</font>";
//                                    }    
                                    ?>
                                </td>
                                <td class="kv-align-right kv-align-middle"
                                    style="text-align:right;">
                                    <?php 
                                    //echo $key['ctmridate'];
                                    $urlToCca02p1 = "/teleradio/suspected/open-form?dataid=".$key['_id_cca02p1']."&ezf_id=".$key['_ezf_id_cca02p1']."&ptid=".$key['ptid'];
                                    echo Html::a( 
                                            $key['ctmridate'], 
                                            NULL,
                                            [
                                                'data-url'=>Url::to(['/inv/inv-person/ezform-print',
                                                    'ezf_id'=>$key['_ezf_id_cca02p1'],
                                                    'dataid'=>$key['_id_cca02p1'],
                                                    'comp_target'=>'1437725343023632100',
                                                    'target'=>base64_encode($key['ptid']),
                                                    'comp_id_target'=> '1437725343023632100',
                                                    'readonly' => '1',
                                                ]),
                                                'ezf_id'=>$key['_ezf_id_cca02p1'],
                                                'dataid'=>$key['_id_cca02p1'],
                                                'ptid'=>$key['ptid'],
                                                'style'=>'cursor: pointer;',
                                                'view'=>'modal',
                                                'title' => Yii::t('app', 'เปิดอ่านเท่านั้น'),
                                                'class'=>'btn btn-link btn-xs print-ezform',

                                            ]
                                            );
                                    if( strlen($key['_id_cca02p1'])>0){
                                        echo Html::a(
                                                '<span class="glyphicon glyphicon-edit"></span>',
                                                NULL,
                                                [
                                                    'data-url'=>Url::to($urlToCca02p1),
                                                    'ezf_id'=>$key['_ezf_id_cca02p1'],
                                                    'dataid'=>$key['_id_cca02p1'],
                                                    'ptid'=>$key['ptid'],
                                                    'style'=>'cursor: pointer;',
                                                    'view'=>'newwindow',
                                                    'title' => Yii::t('app', 'เปิดเพื่อแก้ไข และดูภาพ'),
                                                    'class'=>'btn btn-link btn-xs print-ezform',
                                                    ]
                                                );
                                    }
                                     
                                    ?>
                                </td>
                                <td class="kv-align-right kv-align-middle"
                                    style="text-align:left;">
                                        <?php 
                                    if( strlen($key['f2p1_fileupload'])>0 ){
                                        echo '<span class="glyphicon glyphicon-save-file"></span>';
                                    }
                                    echo $key['ctmriresult']; 
                                        ?>
                                </td>
                                <td style="text-align:right;">
                                    <?php 
                                    //echo $key['cca03date']; 
                                    $urlToCca03 = "/teleradio/suspected/open-form?dataid=".$key['_id_cca03']."&ezf_id=".$key['_ezf_id_cca03']."&ptid=".$key['ptid'];
                                    echo Html::a( 
                                            $key['cca03date'], 
                                            NULL,
                                            [
                                                'data-url'=>Url::to(['/inv/inv-person/ezform-print',
                                                    'ezf_id'=>$key['_ezf_id_cca03'],
                                                    'dataid'=>$key['_id_cca03'],
                                                    'comp_target'=>'1437725343023632100',
                                                    'target'=>base64_encode($key['ptid']),
                                                    'comp_id_target'=> '1437725343023632100',
                                                    'readonly' => '1',
                                                ]),
                                                'ezf_id'=>$key['_ezf_id_cca03'],
                                                'dataid'=>$key['_id_cca03'],
                                                'ptid'=>$key['ptid'],
                                                'style'=>'cursor: pointer;',
                                                'view'=>'modal',
                                                'title' => Yii::t('app', 'เปิดอ่านเท่านั้น'),
                                                'class'=>'btn btn-link btn-xs print-ezform',

                                            ]
                                            );
                                    if( strlen($key['_id_cca03'])>0){
                                        echo Html::a(
                                                '<span class="glyphicon glyphicon-edit"></span>',
                                                NULL,
                                                [
                                                    'data-url'=>Url::to($urlToCca03),
                                                    'ezf_id'=>$key['_ezf_id_cca03'],
                                                    'dataid'=>$key['_id_cca03'],
                                                    'ptid'=>$key['ptid'],
                                                    'style'=>'cursor: pointer;',
                                                    'view'=>'newwindow',
                                                    'title' => Yii::t('app', 'เปิดเพื่อแก้ไข และดูภาพ'),
                                                    'class'=>'btn btn-link btn-xs print-ezform',
                                                    ]
                                                );
                                    }
                                    ?>
                                </td>
                                <td style="text-align:left;">
                                    <?php 
                                    if( strlen($key['f3_fileupload'])>0 ){
                                        echo '<span class="glyphicon glyphicon-save-file"></span>';
                                    }
                                    ?>
                                    <?php 
                                    
                                    
                                        echo $key['cca03result']; 
                                    ?>
                                </td>
                                <td style="text-align:right;">
                                    <?php 
                                    //echo $key['pathodate']; 
                                    $urlToCca04 = "/teleradio/suspected/open-form?dataid=".$key['_id_cca04']."&ezf_id=".$key['_ezf_id_cca04']."&ptid=".$key['ptid'];
                                    echo Html::a( 
                                            $key['pathodate'], 
                                            NULL,
                                            [
                                                'data-url'=>Url::to(['/inv/inv-person/ezform-print',
                                                    'ezf_id'=>$key['_ezf_id_cca04'],
                                                    'dataid'=>$key['_id_cca04'],
                                                    'comp_target'=>'1437725343023632100',
                                                    'target'=>base64_encode($key['ptid']),
                                                    'comp_id_target'=> '1437725343023632100',
                                                    'readonly' => '1',
                                                ]),
                                                'ezf_id'=>$key['_ezf_id_cca04'],
                                                'dataid'=>$key['_id_cca04'],
                                                'ptid'=>$key['ptid'],
                                                'style'=>'cursor: pointer;',
                                                'view'=>'modal',
                                                'title' => Yii::t('app', 'เปิดอ่านเท่านั้น'),
                                                'class'=>'btn btn-link btn-xs print-ezform',

                                            ]
                                            );
                                    if(strlen($key['_id_cca04'])>0){
                                        echo Html::a(
                                                '<span class="glyphicon glyphicon-edit"></span>',
                                                NULL,
                                                [
                                                    'data-url'=>Url::to($urlToCca04),
                                                    'ezf_id'=>$key['_ezf_id_cca04'],
                                                    'dataid'=>$key['_id_cca04'],
                                                    'ptid'=>$key['ptid'],
                                                    'style'=>'cursor: pointer;',
                                                    'view'=>'newwindow',
                                                    'title' => Yii::t('app', 'เปิดเพื่อแก้ไข และดูภาพ'),
                                                    'class'=>'btn btn-link btn-xs print-ezform',
                                                    ]
                                                );
                                    }
                                    ?>
                                </td>
                                <td style="text-align:left;">
                                    <?php 
                                    if( strlen($key['f4_fileupload'])>0 ){
                                        echo '<span class="glyphicon glyphicon-save-file"></span>';
                                    }
                                    
                                    echo $key['pathoresult']; 
                                    ?>
                                </td>
                                <td style="text-align:left;"><?php echo $key['stage0']; ?></td>
                                <td style="text-align:left;"><?php echo $key['stage1']; ?></td>
                                <td style="text-align:left;"><?php echo $key['stage2']; ?></td>
                                <td style="text-align:left;"><?php echo $key['stage3']; ?></td>
                                <td style="text-align:left;"><?php echo $key['stage3a']; ?></td>
                                <td style="text-align:left;"><?php echo $key['stage3b']; ?></td>
                                <td style="text-align:left;"><?php echo $key['stage4a']; ?></td>
                                <td style="text-align:left;"><?php echo $key['stage4b']; ?></td>
                                <td style="text-align:left;"><?php echo $key['stageuk']; ?></td>
                                <td style="text-align:left;">
                                    <?php 
                                    //echo $key['cca05lastvisit']; 
                                    $urlToCca05 = "/teleradio/suspected/open-form?dataid=".$key['_id_cca05']."&ezf_id=".$key['_ezf_id_cca05']."&ptid=".$key['ptid'];
                                    echo Html::a( 
                                            $key['f5v1a1'], 
                                            NULL,
                                            [
                                                'data-url'=>Url::to(['/inv/inv-person/ezform-print',
                                                    'ezf_id'=>$key['_ezf_id_cca05'],
                                                    'dataid'=>$key['_id_cca05'],
                                                    'comp_target'=>'1437725343023632100',
                                                    'target'=>base64_encode($key['ptid']),
                                                    'comp_id_target'=> '1437725343023632100',
                                                    'readonly' => '1',
                                                ]),
                                                'ezf_id'=>$key['_ezf_id_cca05'],
                                                'dataid'=>$key['_id_cca05'],
                                                'ptid'=>$key['ptid'],
                                                'style'=>'cursor: pointer;',
                                                'view'=>'modal',
                                                'title' => Yii::t('app', 'เปิดอ่านเท่านั้น'),
                                                'class'=>'btn btn-link btn-xs print-ezform',

                                            ]
                                            );
                                    if(strlen($key['_id_cca05'])>0){
                                        echo Html::a(
                                                '<span class="glyphicon glyphicon-edit"></span>',
                                                NULL,
                                                [
                                                    'data-url'=>Url::to($urlToCca05),
                                                    'ezf_id'=>$key['_ezf_id_cca05'],
                                                    'dataid'=>$key['_id_cca05'],
                                                    'ptid'=>$key['ptid'],
                                                    'style'=>'cursor: pointer;',
                                                    'view'=>'newwindow',
                                                    'title' => Yii::t('app', 'เปิดเพื่อแก้ไข และดูภาพ'),
                                                    'class'=>'btn btn-link btn-xs print-ezform',
                                                    ]
                                                );
                                    }
                                    ?>
                                </td>
                                <td style="text-align:left;"><?php echo $key['cca05result']; ?></td>
                                <td style="text-align:left;"><?php echo $key['dead']; ?></td>
                            </tr>
                            <?php
                            $num++;
                        }
                        ?>
                        
                    </table>
                </div>
            </div>
        </div>
    </div>

<?php
$jsAdd = <<< JS
var sort="$sort";
switch (sort) {
    case "asc":
        var data_sortadd=$("#$div").attr("data-sort","desc");
        break;
    case "desc":
        var data_sortadd=$("#$div").attr("data-sort","asc");
}

JS;
$this->registerJs($jsAdd);
?>