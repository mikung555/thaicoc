<?php

use miloschuman\highcharts\Highcharts;
use yii\web\JsExpression;
?>

<?php if ($dataGet[item] == "1") { ?>
    <div class="panel panel-primary" align="center">
        <div class="panel-heading"><h3 class="panel-title">Submit/All</h3></div>
        <div class="panel-body">
            <div class="containner">
                <div id="show-piechart" class="col-md-12">
                    <?php
                    echo Highcharts::widget([
                        'setupOptions' => [
                            'lang' => [
                                'thousandsSep' => ','
                            ],
                        ],
                        'options' => [
                            'title' => ['text' => ''
                            ],
                            'chart' => [
                                'renderTo' => 'show-piechart',
                            ],
                            'plotOptions' => [
                                'pie' => [
                                    'size' => '80%',
                                    'cursor' => 'pointer',
                                    'dataLabels' => [
                                        'enabled' => true,
                                        'format' => new JsExpression("'<b>{point.name}</b>: {point.y:,.0f}/{point.percentage:.1f} %'"),
                                        'style' => [
                                            'color' => new JsExpression("(Highcharts.theme && Highcharts.theme.contrastTextColor) || 'red'")
                                        ]
                                    ],
                                ],
                            ],
                            'series' => [
                                [// new opening bracket
                                    'type' => 'pie',
                                    'name' => 'Elements',
                                    'data' => $dataChart
                                ] // new closing bracket
                            ],
                        ],
                    ]);
                    ?>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="col-md-5 col-md-offset-3">
            <table class="table table-bordered table-striped" >
                <thead>
                    <tr>
                        <th class="text-center">Submit/All</th>
                        <th class="text-center">จำนวน</th>
                        <th class="text-center">%</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $num=1;
                    foreach ($dataQuery as $key => $value) {
                        ?>
                        <tr>
                            <td ><b><?= $num .". ".$key ?></b></td>
                            <td class="text-right"><b>
                                <a id="modal-list"
                                   item="<?= $key ?>"
                                   doccode="<?=$dataGet[code]?>"
                                   title="<?= $key ?>"
                                   data-url ="<?= \yii\helpers\Url::to(['suspected/dilldownlist']) ?>"
                                   data-toggle="modal"
                                   data-target="#modal-view" ><?php echo number_format($value);?> คน
                                </a></b>
                            </td>
                            <td class="text-right"><b>
                                 <?php
                                       if ($value <= 0) {
                                           echo "(". number_format($value) ."%)";
                                       } else {
                                           echo "(" . number_format(($value / $sumtotal) * 100, 1, '.', ',') . " %)";
                                       }
                                       $percen+= ($value / $sumtotal) * 100;
                                       ?></b>
                            </td>
                        </tr>
                        <?php
                        $num++;
                    }
                    ?>
                </tbody>
                <tr>
                    <td class="text-center"><b>รวม</b></td>
                    <td class="text-right"><b><?= number_format($sumtotal) ?> คน</b></td>
                    <td class="text-right"><b><?php if($sumtotal <= 0){echo "(".$sumtotal."%)";}else{echo "(".$percen."%)";} ?></b></td>
                </tr>
            </table>
        </div>
    </div>
<?php } elseif ($dataGet[item] == "2") { ?>
    <div class="panel panel-primary" align="center">
        <div class="panel-heading"><h3 class="panel-title">นัดครั้งต่อไป</h3></div>
        <div class="panel-body">
            <div class="containner">
                <div id="show-piechart" class="col-md-12">
                    <?php
                    echo Highcharts::widget([
                        'setupOptions' => [
                            'lang' => [
                                'thousandsSep' => ','
                            ],
                        ],
                        'options' => [
                            'title' => ['text' => ''
                            ],
                            'chart' => [
                                'renderTo' => 'show-piechart',
                            ],
                            'plotOptions' => [
                                'pie' => [
                                    'size' => '80%',
                                    'cursor' => 'pointer',
                                    'dataLabels' => [
                                        'enabled' => true,
                                        'format' => new JsExpression("'<b>{point.name}</b>: {point.y:,.0f}/{point.percentage:.1f} %'"),
                                        'style' => [
                                            'color' => new JsExpression("(Highcharts.theme && Highcharts.theme.contrastTextColor) || 'red'")
                                        ]
                                    ],
                                ],
                            ],
                            'series' => [
                                [// new opening bracket
                                    'type' => 'pie',
                                    'name' => 'Elements',
                                    'data' => $dataChart
                                ] // new closing bracket
                            ],
                        ],
                    ]);
                    ?>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="col-md-5 col-md-offset-3">
            <table class="table table-bordered table-striped" >
                <thead>
                    <tr>
                        <th class="text-center">นัดครั้งต่อไป</th>
                        <th class="text-center">จำนวน</th>
                        <th class="text-center">%</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $num=1;
                    foreach ($dataQuery as $key => $value) {
                        if ($key == "Meet1") {
                            $name = "นัด 1 ปี (กรณีผลตรวจปกติ)";
                        } elseif ($key == "Meet2") {
                            $name = "นัด 6 เดือน (กรณีผลตรวจผิดปกติ)";
                        } elseif ($key == "suspected") {
                            $name = "Suspected CCA";
                        } elseif ($key == "other") {
                            $name = "สาเหตุอื่นๆ";
                        }
                        ?>
                        <tr>
                            <td><b><?= $num.". ".$name ?></b></td>
                            <td class="text-right"><b>
                                <a id="modal-list"
                                   item="<?= $key ?>"
                                   title="<?= $name ?>"
                                   doccode="<?=$dataGet[code]?>"
                                   data-url ="<?= \yii\helpers\Url::to(['suspected/dilldownlist']) ?>"
                                   data-toggle="modal"
                                   data-target="#modal-view" ><?php echo number_format($value);?> คน
                                </a></b>
                            </td>
                            <td class="text-right"><b>
                                    <?php
                                    if ($value <= "0") {
                                        echo "(".number_format($value).")";
                                    } else {
                                        echo "(" . number_format(($value / $sumtotal) * 100, 1, '.', ',') . " %)";
                                    }
                                    $percen+= ($value / $sumtotal) * 100;
                                    ?></b>
                            </td>
                        </tr>
                        <?php
                        $num++;
                    }
                    ?>
                </tbody>
                <tr>
                    <td class="text-center"><b>รวม</b></td>
                    <td class="text-right"><b><?= number_format($sumtotal) ?> คน</b></td>
                    <td class="text-right"><b><?php if($sumtotal <= 0){echo "(".$sumtotal."%)";}else{echo "(".$percen."%)";} ?></b></td>
                </tr>
            </table>
        </div>
    </div>
<?php } elseif ($dataGet[item] == "3") { ?>
    <div class="col-md-12" align="center">
        <div class="panel panel-primary" >
            <div class="panel-heading"><h3 class="panel-title">Liver Parenchymal ECHO</h3></div>
            <div class="panel-body" id="show-piechart1">
                <?php
                echo Highcharts::widget([
                    'setupOptions' => [
                        'lang' => [
                            'thousandsSep' => ','
                        ],
                    ],
                    'options' => [
                        'title' => ['text' => ''
                        ],
                        'chart' => [
                            'renderTo' => 'show-piechart1',
                        ],
                        'plotOptions' => [
                            'pie' => [
                                'size' => '50%',
                                'cursor' => 'pointer',
                                'dataLabels' => [
                                    'enabled' => true,
                                    'format' => new JsExpression("'<b>{point.name}</b>: {point.y:,.0f}/{point.percentage:.1f} %'"),
                                    'style' => [
                                        'color' => new JsExpression("(Highcharts.theme && Highcharts.theme.contrastTextColor) || 'red'")
                                    ]
                                ],
                            ],
                        ],
                        'series' => [
                            [// new opening bracket
                                'type' => 'pie',
                                'name' => 'Elements',
                                'data' => $dataChart1
                            ] // new closing bracket
                        ],
                    ],
                ]);
                ?>
            </div>
        </div>
    </div>
    <div  class="col-md-4" align="center">
        <div class="panel panel-primary" >
            <div class="panel-heading"><h3 class="panel-title">Abnormal</h3></div>
            <div class="panel-body" id="show-piechart2">
                <?php
                echo Highcharts::widget([
                    'setupOptions' => [
                        'lang' => [
                            'thousandsSep' => ','
                        ],
                    ],
                    'options' => [
                        'title' => [
                            'text' => ''
                        ],
                        'chart' => [
                            'renderTo' => 'show-piechart2',
                        ],
                        'xAxis' => [
                            'categories' => $dataName2,
                        ],
                        'yAxis' => [
                            'title' => ['text' => 'จำนวน(ครั้ง)']
                        ],
                        'plotOptions' => [
                            'series' => ['shadow' => true],
                            'candlestick' => ['lineColor' => '#404048'],
                        ],
                        'colors' => ['#337ab7'],
                        'series' => [
                            [
                                'type' => 'column',
                                'name' => 'Abnormal',
                                'data' => $dataChart2,
                                'dataLabels' => [
                                    'enabled' => true,
                                    'color' => 'red',
                                    'align' => 'center',
                                    'format' => '{point.y:,.0f}',
                                    'style' => [
                                        'fontSize' => '12px',
                                        'fontFamily' => 'Verdana, sans-serif',
                                    ],
                                ],
                            ]
                        ]
                    ]
                ]);
                ?>
            </div>
        </div>
    </div>
    <div  class="col-sm-4" align="center">
        <div class="panel panel-primary" >
            <div class="panel-heading"><h3 class="panel-title">Fatty Liver</h3></div>
            <div class="panel-body" id="show-piechart3">
                <?php
                echo Highcharts::widget([
                    'setupOptions' => [
                        'lang' => [
                            'thousandsSep' => ','
                        ],
                    ],
                    'options' => [
                        'title' => [
                            'text' => ''
                        ],
                        'chart' => [
                            'renderTo' => 'show-piechart3',
                        ],
                        'xAxis' => [
                            'categories' => $dataNameLiver,
                        ],
                        'yAxis' => [
                            'title' => ['text' => 'จำนวน(ครั้ง)']
                        ],
                        'plotOptions' => [
                            'series' => ['shadow' => true],
                            'candlestick' => ['lineColor' => '#404048'],
                        ],
                        'colors' => ['#23a938'],
                        'series' => [
                            [
                                'type' => 'column',
                                'name' => 'Fatty Liver',
                                'data' => $dataChartLiver,
                                'dataLabels' => [
                                    'enabled' => true,
                                    'color' => 'red',
                                    'align' => 'center',
                                    'format' => '{point.y:,.0f}',
                                    'style' => [
                                        'fontSize' => '12px',
                                        'fontFamily' => 'Verdana, sans-serif',
                                    ],
                                ],
                            ]
                        ]
                    ]
                ]);
                ?>
            </div>
        </div>
    </div>
    <div  class="col-sm-4" align="center">
        <div class="panel panel-primary" >
            <div class="panel-heading"><h3 class="panel-title">PDF</h3></div>
            <div class="panel-body" id="show-piechart4">
                <?php
                echo Highcharts::widget([
                    'setupOptions' => [
                        'lang' => [
                            'thousandsSep' => ','
                        ],
                    ],
                    'options' => [
                        'title' => [
                            'text' => ''
                        ],
                        'chart' => [
                            'renderTo' => 'show-piechart4',
                        ],
                        'xAxis' => [
                            'categories' => $dataNamepdf,
                        ],
                        'yAxis' => [
                            'title' => ['text' => 'จำนวน(ครั้ง)']
                        ],
                        'plotOptions' => [
                            'series' => ['shadow' => true],
                            'candlestick' => ['lineColor' => '#404048'],
                        ],
                        'colors' => ['#f5bf22'],
                        'series' => [
                            [
                                'type' => 'column',
                                'name' => 'PDF',
                                'data' => $dataChartpdf,
                                'dataLabels' => [
                                    'enabled' => true,
                                    'color' => 'red',
                                    'align' => 'center',
                                    'format' => '{point.y:,.0f}',
                                    'style' => [
                                        'fontSize' => '12px',
                                        'fontFamily' => 'Verdana, sans-serif',
                                    ],
                                ],
                            ]
                        ]
                    ]
                ]);
                ?>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="col-md-5 col-md-offset-3">
            <table class="table table-bordered table-striped" >
                <thead>
                    <tr>
                        <th class="text-center">Liver Parenchymal ECHO</th>
                        <th class="text-center">จำนวน</th>
                        <th class="text-center">%</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $num = 1;
                    foreach ($dataQuery1 as $key => $value) {
                        ?>
                        <tr>
                            <td><b><?= $num . "." . $key ?></b></td>
                            <td class="text-right"><b>
                                <a id="modal-list"
                                   item="<?= $key ?>"
                                   title="<?= $key ?>"
                                   doccode="<?=$dataGet[code]?>"
                                   data-url ="<?= \yii\helpers\Url::to(['suspected/dilldownlist']) ?>"
                                   data-toggle="modal"
                                   data-target="#modal-view" >
                                       <?php echo number_format($value);?> คน
                                </a></b>
                            </td>
                            <td class="text-right"><b>
                                       <?php
                                        if ($value <= 0) {
                                            echo "(" . number_format($value) . " %)";
                                        } else {
                                            echo "(" . number_format(($value / $sumQuery1) * 100, 1, '.', ',') . " %)";
                                        }
                                        $percen+= ($value / $sumQuery1) * 100;
                                        ?></b>
                            </td>
                        </tr>
                        <?php
                        if ($key == "Abnormal") {
                            $num2 = 1;
                            foreach ($dataQuery2 as $key2 => $value2) {
                                ?>
                                <tr>
                                    <td ><b><?= "&nbsp;&nbsp;&nbsp;" . $num . "." . $num2 . ") " . $key2 ?></b></td>
                                    <td class="text-right">
                                        <a id="modal-list"
                                            item="<?= $key2 ?>"
                                            title="<?= $key2 ?>"
                                            doccode="<?=$dataGet[code]?>"
                                            data-url ="<?= \yii\helpers\Url::to(['suspected/dilldownlist']) ?>"
                                            data-toggle="modal"
                                            data-target="#modal-view" ><b>
                                                <?php echo number_format($value2);?> ครั้ง</b>
                                         </a>
                                    </td>
                                    <td class="text-right"> </td>
                                    
                                </tr>
                                <?php if ($key2 == "FattyLiver") { ?>
                                    <?php
                                    foreach ($dataLiver as $keyliver => $valueliver) {
                                        ?>
                                        <tr>
                                            <td ><?= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" . "- " . $keyliver ?></td>
                                            <td class="text-right">
                                                <a id="modal-list"
                                                    item="<?= $keyliver ?>"
                                                    title="<?= $keyliver ?>"
                                                    doccode="<?=$dataGet[code]?>"
                                                    data-url ="<?= \yii\helpers\Url::to(['suspected/dilldownlist']) ?>"
                                                    data-toggle="modal"
                                                    data-target="#modal-view" >
                                                        <?php echo number_format($valueliver);?> ครั้ง
                                                             
                                                         
                                                 </a>
                                            </td>
                                            <td> </td>
                                        </tr>
                                        <?php
                                    }
                                    ?>

                                    <?php
                                }
                                ?>
                                <?php if ($key2 == "PDF") { ?>
                                    <?php
                                    foreach ($dataPdf as $keypdf => $valuepdf) {
                                        ?>
                                        <tr>
                                            <td ><?= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" . "- " . $keypdf ?></td>
                                            <td class="text-right">
                                                <a id="modal-list"
                                                    item="<?= $keypdf ?>"
                                                    title="<?= $keypdf ?>"
                                                    doccode="<?=$dataGet[code]?>"
                                                    data-url ="<?= \yii\helpers\Url::to(['suspected/dilldownlist']) ?>"
                                                    data-toggle="modal"
                                                    data-target="#modal-view" >
                                                        <?php echo number_format($valuepdf);?> ครั้ง
                                                 </a>
                                            </td>
                                            <td> </td>
                                        </tr>
                                        <?php
                                    }
                                    ?>

                                    <?php
                                }
                                ?>
                                <?php
                                $num2++;
                            }
                        }
                        ?>
                        <?php
                        $num++;
                    }
                    ?>
                </tbody>
                <tr>
                    <td class="text-center"><b>รวม</b></td>
                    <td class="text-right"><b><?= number_format($sumQuery1)?> คน</b></td>
                    <td class="text-right"><b><?php if($sumQuery1 <= 0){echo "(".$sumQuery1."%)";}else{echo "(".$percen."%)";} ?></b></td>
                </tr>
            </table>
        </div>
    </div>
<?php } elseif ($dataGet[item] == "4") { ?>
    <div  class="col-sm-6" align="center">
        <div class="panel panel-primary" >
            <div class="panel-heading"><h3 class="panel-title">Liver Mass</h3></div>
            <div class="panel-body" id="show-piechart1">
                <?php
                echo Highcharts::widget([
                    'setupOptions' => [
                        'lang' => [
                            'thousandsSep' => ','
                        ],
                    ],
                    'options' => [
                        'title' => ['text' => ''
                        ],
                        'chart' => [
                            'renderTo' => 'show-piechart1',
                        ],
                        'plotOptions' => [
                            'pie' => [
                                'size' => '50%',
                                'cursor' => 'pointer',
                                'allowPointSelect' => 'true',
                                'dataLabels' => [
                                    'enabled' => true,
                                    'format' => new JsExpression("'<b>{point.name}</b>: {point.y:,.0f}/{point.percentage:.1f} %'"),
                                    'style' => [
                                        'color' => new JsExpression("(Highcharts.theme && Highcharts.theme.contrastTextColor) || 'red'")
                                    ]
                                ],
                            ],
                        ],
                        'series' => [
                            [// new opening bracket
                                'type' => 'pie',
                                'name' => 'Elements',
                                'data' => $dataChart1
                            ] // new closing bracket
                        ],
                    ],
                ]);
                ?>
            </div>
        </div>
    </div>
    <div  class="col-sm-6" align="center">
        <div class="panel panel-primary">
            <div class="panel-heading"><h3 class="panel-title">Multiple Mass</h3></div>
            <div class="panel-body" id="show-piechart2">
                <?php
                echo Highcharts::widget([
                    'setupOptions' => [
                        'lang' => [
                            'thousandsSep' => ','
                        ],
                    ],
                    'options' => [
                        'title' => [
                            'text' => ''
                        ],
                        'chart' => [
                            'renderTo' => 'show-piechart2',
                        ],
                        'xAxis' => [
                            'categories' => $nameChart,
                        ],
                        'yAxis' => [
                            'title' => ['text' => 'จำนวน(ครั้ง)']
                        ],
                        'plotOptions' => [
                            'series' => ['shadow' => true],
                            'candlestick' => ['lineColor' => '#404048'],
                        ],
                        'colors' => ['#337ab7'],
                        'series' => [
                            [
                                'type' => 'column',
                                'name' => 'Multiple Mass',
                                'data' => $dataChart2,
                                'dataLabels' => [
                                    'enabled' => true,
                                    'color' => 'red',
                                    'align' => 'center',
                                    'format' => '{point.y:,.0f}',
                                    'style' => [
                                        'fontSize' => '12px',
                                        'fontFamily' => 'Verdana, sans-serif',
                                    ],
                                ],
                            ]
                        ]
                    ]
                ]);
                ?>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="col-md-5 col-md-offset-3">
            <table class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th class="text-center">Liver Mass</th>
                        <th class="text-center">จำนวน</th>
                        <th class="text-center">%</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $num = 1;
                    foreach ($dataQuery1 as $key => $value) {
                        ?>
                        <tr>
                            <td ><b><?= $num . "." . $key ?></b></td>
                            <td class="text-right"><b>
                                <a id="modal-list"
                                   item="<?= $key ?>"
                                   title="<?= $key ?>"
                                   doccode="<?=$dataGet[code]?>"
                                   data-url ="<?= \yii\helpers\Url::to(['suspected/dilldownlist']) ?>"
                                   data-toggle="modal"
                                   data-target="#modal-view" >
                                       <?php echo number_format($value);?> คน
                                </a></b>
                            </td>
                            <td class="text-right"><b>
                                <?php
                                    if ($value <= 0) {
                                        echo "(0 %)";
                                    } else {
                                        echo "(" . number_format(($value / $sumtotal) * 100, 1, '.', ',') . " %)";
                                    }
                                    ?></b>
                            </td>
                        </tr>
                        <?php if ($key == "MultipleMass") { ?>
                            <?php
                            foreach ($dataQuery2 as $key2 => $value2) {
                                if ($key2 == "mass1") {
                                    $name = "Liver cyst";
                                } else if ($key2 == "mass2") {
                                    $name = "Hemangioma";
                                } else if ($key2 == "mass3") {
                                    $name = "Calcification";
                                } else if ($key2 == "mass4") {
                                    $name = "Intrahepatic duct stone";
                                } else if ($key2 == "mass5") {
                                    $name = "High echo";
                                } else if ($key2 == "mass6") {
                                    $name = "Low echo";
                                } else if ($key2 == "mass7") {
                                    $name = "Mixed echo";
                                }
                                ?>
                                <tr>
                                    <td ><?= "&nbsp;&nbsp;&nbsp;" . "- " . $name ?></td>
                                    <td class="text-right">
                                        <a id="modal-list"
                                            item="<?= $key2 ?>"
                                            title="<?= $name ?>"
                                            doccode="<?=$dataGet[code]?>"
                                            data-url ="<?= \yii\helpers\Url::to(['suspected/dilldownlist']) ?>"
                                            data-toggle="modal"
                                            data-target="#modal-view" >
                                                <?php echo number_format($value2);?> ครั้ง
                                        </a>
                                    </td>
                                    <td> </td>
                                </tr>
                                <?php
                            }
                            ?>

                            <?php
                        }
                        ?>
                        <?php
                        $num++;
                        $percen+= ($value / $sumtotal) * 100;
                    }
                    ?>
                </tbody>
                <tr>
                    <td class="text-center"><b>รวม</b></td>
                    <td class="text-right"><b> <?= number_format($sumtotal) ?> คน</b></td>
                    <td class="text-right"><b><?php if($sumtotal <= 0){echo "(0 %)";}else{echo "(".$percen."%)";} ?></b></td>
                </tr>
            </table>
        </div>
    </div>
<?php } elseif ($dataGet[item] == "5") { ?> 
    <div class="panel panel-primary" align="center">
        <div class="panel-heading"><h3 class="panel-title">Gallbladder</h3></div>
        <div class="panel-body">
            <div class="containner">
                <div id="show-piechart" class="col-md-12">
                    <?php
                    echo Highcharts::widget([
                        'setupOptions' => [
                            'lang' => [
                                'thousandsSep' => ','
                            ],
                        ],
                        'options' => [
                            'title' => [
                                'text' => ''
                            ],
                            'chart' => [
                                'renderTo' => 'show-piechart',
                            ],
                            'xAxis' => [
                                'categories' => $nameChart,
                            ],
                            'yAxis' => [
                                'title' => ['text' => 'จำนวน(ครั้ง)']
                            ],
                            'plotOptions' => [
                                'series' => ['shadow' => true],
                                'candlestick' => ['lineColor' => '#404048'],
                            ],
                            'colors' => ['#337ab7'],
                            'series' => [
                                [
                                    'type' => 'column',
                                    'name' => 'Gallbladder',
                                    'data' => $dataChart,
                                    'dataLabels' => [
                                        'enabled' => true,
                                        'color' => 'red',
                                        'align' => 'center',
                                        'format' => '{point.y:,.0f}',
                                        'style' => [
                                            'fontSize' => '12px',
                                            'fontFamily' => 'Verdana, sans-serif',
                                        ],
                                    ],
                                ]
                            ]
                        ]
                    ]);
                    ?>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="col-md-5 col-md-offset-3">
            <table class="table table-bordered table-striped" >
                <thead>
                    <tr>
                        <th class="text-center">Gallbladder</th>
                        <th class="text-center">จำนวน</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $num=1;
                    foreach ($dataQuery as $key => $value) {
                        if ($key == "gall1") {
                            $name = "Normal";
                        } else if ($key == "gall2") {
                            $name = "Wall";
                        } else if ($key == "gall3") {
                            $name = "Gallstone";
                        } else if ($key == "gall4") {
                            $name = "Post cholecystectomy";
                        } else if ($key == "gall5") {
                            $name = "Not seen";
                        }
                        ?>
                        <tr>
                            <td ><b><?= $num .". ".$name ?></b></td>
                            <td class="text-right"><b>
                                <a id="modal-list"
                                    item="<?= $key ?>"
                                    title="<?= $name ?>"
                                    doccode="<?=$dataGet[code]?>"
                                    data-url ="<?= \yii\helpers\Url::to(['suspected/dilldownlist']) ?>"
                                    data-toggle="modal"
                                    data-target="#modal-view" >
                                        <?php echo number_format($value);?> ครั้ง
                                </a></b>
                            </td>
                            
                        </tr>
                        <?php
                        $sumtotal += $value;
                        $num++;
                    }
                    ?>
                </tbody>
                <tr>
                    <td class="text-center"><b>รวม</b></td>
                    <td class="text-right"><b><?= number_format($sumtotal) ?> ครั้ง</b></td>
                </tr>
            </table>
        </div>
    </div>
<?php } elseif ($dataGet[item] == "6") { ?> 
    <div class="panel panel-primary" align="center">
        <div class="panel-heading"><h3 class="panel-title">Kidney</h3></div>
        <div class="panel-body">
            <div class="containner">
                <div id="show-piechart" class="col-md-12">
                    <?php
                    echo Highcharts::widget([
                        'setupOptions' => [
                            'lang' => [
                                'thousandsSep' => ','
                            ],
                        ],
                        'options' => [
                            'title' => [
                                'text' => ''
                            ],
                            'chart' => [
                                'renderTo' => 'show-piechart',
                            ],
                            'xAxis' => [
                                'categories' => $nameChart,
                            ],
                            'yAxis' => [
                                'title' => ['text' => 'จำนวน(ครั้ง)']
                            ],
                            'plotOptions' => [
                                'series' => ['shadow' => true],
                                'candlestick' => ['lineColor' => '#404048'],
                            ],
                            'colors' => ['#337ab7'],
                            'series' => [
                                [
                                    'type' => 'column',
                                    'name' => 'Kidney',
                                    'data' => $dataChart,
                                    'dataLabels' => [
                                        'enabled' => true,
                                        'color' => 'red',
                                        'align' => 'center',
                                        'format' => '{point.y:,.0f}',
                                        'style' => [
                                            'fontSize' => '12px',
                                            'fontFamily' => 'Verdana, sans-serif',
                                        ],
                                    ],
                                ]
                            ]
                        ]
                    ]);
                    ?>
                </div>
            </div>

        </div>
    </div>
    <div class="container">
        <div class="col-md-5 col-md-offset-3">
            <table class="table table-bordered table-striped" >
                <thead>
                    <tr>
                        <th class="text-center">Kidney</th>
                        <th class="text-center">จำนวน</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $num=1;
                    foreach ($dataQuery as $key => $value) {
                        if ($key == "kid1") {
                            $name = "Normal";
                        } elseif ($key == "kid2") {
                            $name = "Renal cyst";
                        } else if ($key == "kid3") {
                            $name = "Parenchymal change";
                        } else if ($key == "kid4") {
                            $name = "Renal stone";
                        } else if ($key == "kid5") {
                            $name = "Post nephrectomy";
                        } else if ($key == "kid6") {
                            $name = "Not seen";
                        }
                        ?>
                        <tr>
                            <td ><b><?= $num.". ".$name ?></b></td>
                            <td class="text-right"><b>
                                <a id="modal-list"
                                    item="<?= $key ?>"
                                    title="<?= $name ?>"
                                    doccode="<?=$dataGet[code]?>"
                                    data-url ="<?= \yii\helpers\Url::to(['suspected/dilldownlist']) ?>"
                                    data-toggle="modal"
                                    data-target="#modal-view" >
                                        <?php echo number_format($value);?> ครั้ง
                                </a></b>
                            </td>
                        </tr>
                        <?php
                        $sumtotal += $value;
                        $num++;
                    }
                    ?>
                </tbody>
                <tr>
                    <td class="text-center"><b>รวม</b></td>
                    <td class="text-right"> <b><?= number_format($sumtotal) ?> ครั้ง</b></td>
                </tr>
            </table>
        </div>
    </div>
<?php } ?>

