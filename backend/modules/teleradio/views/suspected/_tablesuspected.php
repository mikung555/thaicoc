<?php

use yii\helpers\Html;

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<?php
if( 0 ){
    echo "<pre align='left'>";
    print_r($dataProvider);
    echo "</pre>";
}

if( 0 ){
    echo "<pre align='left'>";
    print_r($_GET);
    echo "</pre>";
}
$request = Yii::$app->request;
$id = $request->get('sort', 'f2v1');

if(strlen($page)==0){
    $page=1;
}

?>
    <div class="table-responsive">
        <div id="report84-dynagrid-2-pjax">
            <div class="grid">
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                        <div style="text-align: left;">
                            <select id="pageselect" >
                                <?php
                                    for($ipage=1; $ipage<$paging['countPage']+1; $ipage++){
                                ?>
                                <option sort='<?php echo $request->get('sort', 'f2v1'); ?>'
                                        fromDate='<?php echo $request->get('fromDate'); ?>'
                                        toDate='<?php echo $request->get('toDate'); ?>'
                                        sec='<?php echo $request->get('sec'); ?>'
                                        colum='<?php echo $request->get('colum'); ?>'
                                        div='<?php echo $request->get('div'); ?>'
                                        refresh='yes'
                                        page='<?php echo $ipage; ?>'
                                <?php
                                        if($page==$ipage){
                                            echo " selected='true' ";
                                        }
                                ?>
                                        ><?php echo $ipage; ?></option>
                                <?php
                                    }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                        <div style="text-align: right;">
                            <label for="pageselect" style="margin-top: 7px; width: 500px;">คนที่ Ultrasound ทั้งหมด <?php echo $paging['allPatient']; ?> คน และมีจำนวนการตรวจทั้งหมด <?php echo $paging['allRecord']; ?> ครั้ง อยู่ที่หน้า: </label>
                            <select id="pageselect" >
                                <?php
                                    for($ipage=1; $ipage<$paging['countPage']+1; $ipage++){
                                ?>
                                <option sort='<?php echo $request->get('sort', 'f2v1'); ?>'
                                        fromDate='<?php echo $request->get('fromDate'); ?>'
                                        toDate='<?php echo $request->get('toDate'); ?>'
                                        sec='<?php echo $request->get('sec'); ?>'
                                        colum='<?php echo $request->get('colum'); ?>'
                                        div='<?php echo $request->get('div'); ?>'
                                        refresh='yes'
                                        page='<?php echo $ipage; ?>'
                                <?php
                                        if($page==$ipage){
                                            echo " selected='true' ";
                                        }
                                ?>
                                        ><?php echo $ipage; ?></option>
                                <?php
                                    }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div id="report84-dynagrid-2" data-krajee-grid="kvGridInit_adf683c2">
                <div id="report84-dynagrid-2-container" class="table-responsive kv-grid-container">


                    <table class="kv-grid-table table table-bordered table-striped">
                        <thead>
                        <tr style="text-align:center">
                            <th rowspan="2" class="kv-align-center kv-align-middle" style="min-width:60px;text-align:right;">ลำดับที่</th>
                            <th rowspan="2" class="kv-align-center kv-align-middle" style="min-width:100px;text-align:right;">
                                <a href="#table_secshow1" id='sort1' data-sort='asc'
                                    fromDate='<?php echo $datadate[fromDate] ?>'
                                    toDate='<?php echo $datadate[toDate] ?>' sec='1' colum='f2v1' 
                                    page='<?php echo $page; ?>'>วันที่ตรวจ</a>
                            </th>
                            <th rowspan="2" class="kv-align-middle" style="min-width: 190px;text-align:left;">
                                ชื่อ-สกุล
                            </th>
                            <th rowspan="2" class="kv-align-middle" style="min-width: 100px;text-align:left;">
                                
                                <a href="#table_secshow1" id='sort3' data-sort='asc'
                                    fromDate='<?php echo $datadate[fromDate] ?>'
                                    toDate='<?php echo $datadate[toDate] ?>' sec='1' colum='cohorttype_chartreview' 
                                    page='<?php echo $page; ?>'>กลุ่มคนไข้</a>
                            </th>
                            <th rowspan="2" style="vertical-align: bottom; text-align: center; min-width: 340px;">
                                <a href="#table_secshow1" id='sort3' data-sort='asc'
                                    fromDate='<?php echo $datadate[fromDate] ?>'
                                    toDate='<?php echo $datadate[toDate] ?>' sec='1' colum='cca02.hsitecode' 
                                    page='<?php echo $page; ?>'>โรงพยาบาล</a>
                            </th>
                            <th colspan="2" class="kv-align-center kv-align-middle" style="text-align: center">Ultrasound</th>
                            <th colspan="2">CT/MRI</th>
                            <th colspan="2">รักษา</th>
                            <th colspan="2">Patho</th>
                            <th rowspan="2" class="kv-align-right kv-align-middle" style="min-width:100px;text-align:right;"><a
                                    href="#table_secshow1" id='sort13' data-sort='asc'
                                    fromDate='<?php echo $datadate[fromDate] ?>'
                                    toDate='<?php echo $datadate[toDate] ?>' sec='2' colum='death' 
                                    page='<?php echo $page; ?>'>Death</a></th>
                        </tr>
                        <tr>
                            <th class="kv-align-center kv-align-middle" style="min-width: 150px;text-align:left;">
                                <a href="#table_secshow1" id='sort4' data-sort='asc'
                                    fromDate='<?php echo $datadate[fromDate] ?>'
                                    toDate='<?php echo $datadate[toDate] ?>' sec='1' colum='cca02abnormal' 
                                    page='<?php echo $page; ?>'>Abnormal</a>
                            </th>
                            <th class="kv-align-right kv-align-middle" style="min-width: 150px;text-align:left;"><a
                                    href="#table_secshow1" id='sort5' data-sort='asc'
                                    fromDate='<?php echo $datadate[fromDate] ?>'
                                    toDate='<?php echo $datadate[toDate] ?>' sec='1' colum='suspectedcca' 
                                    page='<?php echo $page; ?>'>Suspected</a>
                            </th>
                            <th class="kv-align-right kv-align-middle" style="min-width:100px;text-align:right;">
                                <a href="#table_secshow1" id='sort6' data-sort='desc'
                                    fromDate='<?php echo $datadate[fromDate] ?>'
                                    toDate='<?php echo $datadate[toDate] ?>' sec='1' colum='ctmridate' 
                                    page='<?php echo $page; ?>'>Date</a>
                            </th>
                            <th class="kv-align-right kv-align-middle" style="min-width:150px;text-align:left;"><a
                                    href="#table_secshow1" id='sort7' data-sort='desc'
                                    fromDate='<?php echo $datadate[fromDate] ?>'
                                    toDate='<?php echo $datadate[toDate] ?>' sec='1' colum='ctmriresult' 
                                    page='<?php echo $page; ?>'>CT/MRI</a>
                            </th>
                            <th class="kv-align-right kv-align-middle" style="min-width:100px;text-align:right;">
                                <a href="#table_secshow1" id='sort8' data-sort='desc'
                                    fromDate='<?php echo $datadate[fromDate] ?>'
                                    toDate='<?php echo $datadate[toDate] ?>' sec='1' colum='cca03date' 
                                    page='<?php echo $page; ?>'>Date</a>
                            </th>
                            <th class="kv-align-right kv-align-middle" style="min-width: 175px;text-align:left;">
                                <a
                                    href="#table_secshow1" id='sort9' data-sort='asc'
                                    fromDate='<?php echo $datadate[fromDate] ?>'
                                    toDate='<?php echo $datadate[toDate] ?>' sec='1' colum='cca03result' 
                                    page='<?php echo $page; ?>'>หัตถการ</a>
                            </th>
                            <th class="kv-align-right kv-align-middle" style="min-width:100px;text-align:right;">
                                <a href="#table_secshow1" id='sort10' data-sort='desc'
                                    fromDate='<?php echo $datadate[fromDate] ?>'
                                    toDate='<?php echo $datadate[toDate] ?>' sec='1' colum='pathodate' 
                                    page='<?php echo $page; ?>'>Date</a>
                            </th>
                            <th class="kv-align-right kv-align-middle" style="min-width:150px;text-align:left;"><a
                                    href="#table_secshow1" id='sort11' data-sort='asc'
                                    fromDate='<?php echo $datadate[fromDate] ?>'
                                    toDate='<?php echo $datadate[toDate] ?>' sec='2' colum='pathoresult' 
                                    page='<?php echo $page; ?>'>Patho</a>
                            </th>
                        </tr>

                        </thead>
                        <tbody>
                        <!-- TABLE DATA -->

                        <?php
                        $num = (($page-1)*$limitrec)+1;
                        if( count($dataProvider)>0 )foreach ($dataProvider as $key) {
                            ?>
                            <tr style="text-align:right">
                                <td class="kv-align-center kv-align-middle"
                                    style="text-align:right;"><?php echo $num ?></td>
                                <td class="kv-align-center kv-align-middle"
                                    style="text-align:right;">
                                        <?php //echo $key[f2v1] ?>
                                    <?php
                                    $urlToCca02 = "/inputdata/redirect-page?dataid=".$key['id']."&ezf_id=1437619524091524800";
                                    echo Html::a( $key[f2v1], $urlToCca02,['target'=>'_blank']);
                                    ?>
                                </td>
                                <td class="kv-align-middle" style="text-align:left;">
                                    <?php
                                    $urlToCca02 = "/inputdata/redirect-page?dataid=".$key['id']."&ezf_id=1437619524091524800";
                                    echo Html::a( $key['name']." ".$key['surname'], $urlToCca02,['target'=>'_blank']);
                                    ?>
                                </td>
                                <td style="text-align:left;"><?php echo $key['cohorttype_chartreview'] ?></td>
                                <td style="text-align:left;">
                                    <?php echo $key['hsitecode'].": ".$key['hospitalname']." (".$key['province'].")"; ?>
                                </td>
                                <td style="text-align:left;"><?php echo $key['cca02abnormal'] ?></td>
                                <td style="text-align:left;">
                                    <?php 
                                    if(strlen($key['suspectedcca'])>0){
                                        echo $key['suspectedcca'];
                                    }else{
                                        echo "<font style=\"color:red\">ไม่ได้ระบุ</font>";
                                    }    
                                    ?>
                                </td>
                                <td class="kv-align-right kv-align-middle"
                                    style="text-align:right;"><?php echo $key['ctmridate']; ?>
                                </td>
                                <td class="kv-align-right kv-align-middle"
                                    style="text-align:left;"><?php echo $key['ctmriresult']; ?>
                                </td>
                                <td style="text-align:right;"><?php echo $key['cca03date']; ?></td>
                                <td style="text-align:left;"><?php echo $key['cca03result']; ?></td>
                                <td style="text-align:right;"><?php echo $key['pathodate']; ?></td>
                                <td style="text-align:left;"><?php echo $key['pathoresult']; ?></td>
                                <td style="text-align:right;"><?php echo $key['death']; ?></td>
                            </tr>
                            <?php
                            $num++;
                        }
                        ?>
                        
                    </table>
                </div>
            </div>
        </div>
    </div>

    <?php
    $jsAdd = <<< JS
var sort="$sort";
switch (sort) {
    case "asc":
        var data_sortadd=$("#$div").attr("data-sort","desc");
        break;
    case "desc":
        var data_sortadd=$("#$div").attr("data-sort","asc");
}

JS;
    $this->registerJs($jsAdd);
    ?>