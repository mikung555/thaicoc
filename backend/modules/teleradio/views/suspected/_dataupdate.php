<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use backend\modules\teleradio\classes\QueryTableDet;

$tabledet   = QueryTableDet::getLogTableUpdate();


if( 0 ){
    echo "<pre align='left'>";
    print_r($tabledet);
    echo "</pre>";
}
?>
<div >
    <div class="col-lg-offset-3 col-lg-9">
        <div id="area-reg" class="row" style="text-align: left;">
            <label>Register</label>
            <span class="glyphicon glyphicon-calendar"></span>
            <i class="glyphicon glyphicon-time" ></i>
            <label id="area-reg-txt">
                Update ล่าสุด 
                <?php echo $tabledet['tb_data_1']['CREATE_TIME']; ?>
            </label>
            <?php
                echo \yii\helpers\Html::button('<i class="glyphicon glyphicon-time">Update</i>', [
                    'id'=>'btnUpdateReg',
                    'class'=>'btn btn-link',
                    'data-url'=>'xxx',
                    'table'=>'tb_data_1',
                ]);
                
                
            ?>
            <label id="area-reg-txt-error"></label>
        </div>
        
        <div id="area-cca01" class="row" style="text-align: left;">
            <label>CCA-01</label>
            <span class="glyphicon glyphicon-calendar"></span>
            <i class="glyphicon glyphicon-time" ></i>
            <label id="area-cca01-txt">
                Update ล่าสุด 
                <?php echo $tabledet['tb_data_2']['CREATE_TIME']; ?>
            </label>
            <?php
                echo \yii\helpers\Html::button('<i class="glyphicon glyphicon-time">Update</i>', [
                    'id'=>'btnUpdateCCA01',
                    'class'=>'btn btn-link',
                    'data-url'=>'xxx',
                    'table'=>'tb_data_2',
                ]);
                
                
            ?>
            <label id="area-cca01-txt-error"></label>
        </div>
        
        <div id="area-cca02" class="row" style="text-align: left;">
            <label>CCA-02</label>
            <span class="glyphicon glyphicon-calendar"></span>
            <i class="glyphicon glyphicon-time" ></i>
            <label id="area-cca02-txt">
                Update ล่าสุด 
                <?php echo $tabledet['tb_data_3']['CREATE_TIME']; ?>
            </label>
            <?php
                echo \yii\helpers\Html::button('<i class="glyphicon glyphicon-time">Update</i>', [
                    'id'=>'btnUpdateCCA02',
                    'class'=>'btn btn-link',
                    'data-url'=>'xxx',
                    'table'=>'tb_data_3',
                ]);
                
                
            ?>
            <label id="area-cca02-txt-error"></label>
        </div>
        
        <div id="area-cca02-1" class="row" style="text-align: left;">
            <label>CCA-02.1</label>
            <span class="glyphicon glyphicon-calendar"></span>
            <i class="glyphicon glyphicon-time" ></i>
            <label id="area-cca02-1-txt">
                Update ล่าสุด 
                <?php echo $tabledet['tb_data_4']['CREATE_TIME']; ?>
            </label>
            <?php
                echo \yii\helpers\Html::button('<i class="glyphicon glyphicon-time">Update</i>', [
                    'id'=>'btnUpdateCCA02-1',
                    'class'=>'btn btn-link',
                    'data-url'=>'xxx',
                    'table'=>'tb_data_4',
                ]);
                
                
            ?>
            <label id="area-cca03-txt-error"></label>
        </div>
        
        <div id="area-cca03" class="row" style="text-align: left;">
            <label>CCA-03</label>
            <span class="glyphicon glyphicon-calendar"></span>
            <i class="glyphicon glyphicon-time" ></i>
            <label id="area-cca03-txt">
                Update ล่าสุด 
                <?php echo $tabledet['tb_data_7']['CREATE_TIME']; ?>
            </label>
            <?php
                echo \yii\helpers\Html::button('<i class="glyphicon glyphicon-time">Update</i>', [
                    'id'=>'btnUpdateCCA03',
                    'class'=>'btn btn-link',
                    'data-url'=>'xxx',
                    'table'=>'tb_data_7',
                ]);
                
                
            ?>
            <label id="area-cca03-txt-error"></label>
        </div>
        
        <div id="area-cca04" class="row" style="text-align: left;">
            <label>CCA-04</label>
            <span class="glyphicon glyphicon-calendar"></span>
            <i class="glyphicon glyphicon-time" ></i>
            <label id="area-cca04-txt">
                Update ล่าสุด 
                <?php echo $tabledet['tb_data_8']['CREATE_TIME']; ?>
            </label>
            <?php
                echo \yii\helpers\Html::button('<i class="glyphicon glyphicon-time">Update</i>', [
                    'id'=>'btnUpdateCCA04',
                    'class'=>'btn btn-link',
                    'data-url'=>'xxx',
                    'table'=>'tb_data_8',
                ]);
                
                
            ?>
            <label id="area-cca03-txt-error"></label>
        </div>
        
        <div id="area-cca05" class="row" style="text-align: left;">
            <label>CCA-05</label>
            <span class="glyphicon glyphicon-calendar"></span>
            <i class="glyphicon glyphicon-time" ></i>
            <label id="area-cca05-txt">
                Update ล่าสุด 
                <?php echo $tabledet['tb_data_11']['CREATE_TIME']; ?>
            </label>
            <?php
                echo \yii\helpers\Html::button('<i class="glyphicon glyphicon-time">Update</i>', [
                    'id'=>'btnUpdateCCA05',
                    'class'=>'btn btn-link',
                    'data-url'=>'xxx',
                    'table'=>'tb_data_11',
                ]);
                
                
            ?>
            <label id="area-cca05-txt-error"></label>
        </div>
        
        
    </div>
</div>
<?php 
$cssAdd = <<< CSS
        
CSS;

$this->registerCss($cssAdd);

$jsAdd = <<< JS
        
    $( function() {
        $("#btnUpdateReg").on("click",function(){
            var table = $("#btnUpdateReg").attr("table");
            $("#area-reg > i").addClass("fa fa-refresh fa-spin");
            $.ajax({
                type    : "GET",
                cache   : false,
                url     : "/teleradio/suspected/update-data-table",
                data    : {
                    table: table
                },
                dataType: 'JSON',
                success  : function(response) {
                    console.log(response);
                    $("#area-reg > i").removeClass("fa fa-refresh fa-spin");
                    $("#area-reg > i").addClass("glyphicon glyphicon-time");
                    $("#area-reg-txt").text("Update ล่าสุด "+response.table_update);         
                },
                error : function(){
                    $("#area-reg-txt-error").html("Eorror!");
                }
            }); 
        });
        
        
        $("#btnUpdateCCA01").on("click",function(){
            var table = $("#btnUpdateCCA01").attr("table");
            $("#area-cca01 > i").addClass("fa fa-refresh fa-spin");
            $.ajax({
                type    : "GET",
                cache   : false,
                url     : "/teleradio/suspected/update-data-table",
                data    : {
                    table: table
                },
                dataType: 'JSON',
                success  : function(response) {
                    console.log(response);
                    $("#area-cca01 > i").removeClass("fa fa-refresh fa-spin");
                    $("#area-cca01 > i").addClass("glyphicon glyphicon-time");
                    $("#area-cca01-txt").text("Update ล่าสุด "+response.table_update);         
                },
                error : function(){
                    $("#area-cca01-txt-error").html("Eorror!");
                }
            }); 
        });
        
        $("#btnUpdateCCA02").on("click",function(){
            var table = $("#btnUpdateCCA02").attr("table");
            $("#area-cca02 > i").addClass("fa fa-refresh fa-spin");
            $.ajax({
                type    : "GET",
                cache   : false,
                url     : "/teleradio/suspected/update-data-table",
                data    : {
                    table: table
                },
                dataType: 'JSON',
                success  : function(response) {
                    console.log(response);
                    $("#area-cca02 > i").removeClass("fa fa-refresh fa-spin");
                    $("#area-cca02 > i").addClass("glyphicon glyphicon-time");
                    $("#area-cca02-txt").text("Update ล่าสุด "+response.table_update);         
                },
                error : function(){
                    $("#area-cca02-txt-error").html("Eorror!");
                }
            }); 
        });
        
        $("#btnUpdateCCA02-1").on("click",function(){
            var table = $("#btnUpdateCCA02-1").attr("table");
            $("#area-cca02-1 > i").addClass("fa fa-refresh fa-spin");
            $.ajax({
                type    : "GET",
                cache   : false,
                url     : "/teleradio/suspected/update-data-table",
                data    : {
                    table: table
                },
                dataType: 'JSON',
                success  : function(response) {
                    console.log(response);
                    $("#area-cca02-1 > i").removeClass("fa fa-refresh fa-spin");
                    $("#area-cca02-1 > i").addClass("glyphicon glyphicon-time");
                    $("#area-cca02-1-txt").text("Update ล่าสุด "+response.table_update);         
                },
                error : function(){
                    $("#area-cca02-1-txt-error").html("Eorror!");
                }
            }); 
        });
        
        $("#btnUpdateCCA03").on("click",function(){
            var table = $("#btnUpdateCCA03").attr("table");
            $("#area-cca03 > i").addClass("fa fa-refresh fa-spin");
            $.ajax({
                type    : "GET",
                cache   : false,
                url     : "/teleradio/suspected/update-data-table",
                data    : {
                    table: table
                },
                dataType: 'JSON',
                success  : function(response) {
                    console.log(response);
                    $("#area-cca03 > i").removeClass("fa fa-refresh fa-spin");
                    $("#area-cca03 > i").addClass("glyphicon glyphicon-time");
                    $("#area-cca03-txt").text("Update ล่าสุด "+response.table_update);         
                },
                error : function(){
                    $("#area-cca03-txt-error").html("Eorror!");
                }
            }); 
        });
        
        $("#btnUpdateCCA04").on("click",function(){
            var table = $("#btnUpdateCCA04").attr("table");
            $("#area-cca04 > i").addClass("fa fa-refresh fa-spin");
            $.ajax({
                type    : "GET",
                cache   : false,
                url     : "/teleradio/suspected/update-data-table",
                data    : {
                    table: table
                },
                dataType: 'JSON',
                success  : function(response) {
                    console.log(response);
                    $("#area-cca04 > i").removeClass("fa fa-refresh fa-spin");
                    $("#area-cca04 > i").addClass("glyphicon glyphicon-time");
                    $("#area-cca04-txt").text("Update ล่าสุด "+response.table_update);         
                },
                error : function(){
                    $("#area-cca04-txt-error").html("Eorror!");
                }
            }); 
        });
        
        $("#btnUpdateCCA05").on("click",function(){
            var table = $("#btnUpdateCCA05").attr("table");
            $("#area-cca05 > i").addClass("fa fa-refresh fa-spin");
            $.ajax({
                type    : "GET",
                cache   : false,
                url     : "/teleradio/suspected/update-data-table",
                data    : {
                    table: table
                },
                dataType: 'JSON',
                success  : function(response) {
                    console.log(response);
                    $("#area-cca05 > i").removeClass("fa fa-refresh fa-spin");
                    $("#area-cca05 > i").addClass("glyphicon glyphicon-time");
                    $("#area-cca05-txt").text("Update ล่าสุด "+response.table_update);         
                },
                error : function(){
                    $("#area-cca05-txt-error").html("Eorror!");
                }
            }); 
        });
        
        
    } );
        
JS;
$this->registerJs($jsAdd);

if( 0 ){
    
    echo "<pre align='left'>";
    print_r($_GET);
    echo "</pre>";
    
}
