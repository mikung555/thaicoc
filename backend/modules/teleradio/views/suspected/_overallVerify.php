<?php

use yii\helpers\Html;

?>
<div class="container">
  <h2>สรุปจำนวนข้อมูลตามเงื่อนไข Suspected ในกลุ่ม Screening</h2>           
  <table class="table" width="40%">
    <thead>
      <tr class="active">
        <th style="text-align: right; font-size: 20px;">รายละเอียด</th>
        <th style="text-align: right; font-size: 14px;">จำนวน</th>
        <th style="text-align: right; font-size: 20px;">%</th>
        <th style="text-align: left; font-size: 20px;">อธิบายเพิ่มเติม</th>
      </tr>
    </thead>
    <tbody>
      <tr class="info">
        <td align="right" width="40%"><b>จำนวนคนที่ Ultrasound ทั้งหมด</b></td>
        <td align="right" width="10%"><?php echo number_format($overAll['จำนวนคนที่ Ultrasound'],0,'.',','); ?></td>
        <td width="10%" align="right"></td>
        <td width="40%" align="left">(เป็นรายคน ที่มีการบันทึกในฐานข้อมูล ตั้งแต่ 09/02/2552 หรือ 09/02/2013)</td>
      </tr>
      <tr>
        <td colspan="1" align="right">จำนวนครั้งที่ Ultrasound ทั้งหมด</td>
        <td align="right"><?php echo number_format($overAll['จำนวนครั้งที่ Ultrasound'],0,'.',','); ?></td>
        <td></td>
        <td align="left">(ครั้ง)</td>
      </tr>
      <tr class="info">
        <td align="right"><b>ข้อมูลที่ Click Suspected เข้ามาจริงๆ (f2v6a3b1=1)</b></td>
        <td align="right"><?php echo number_format($overAll['ข้อมูลที่ Click Suspected เข้ามาจริงๆ (f2v6a3b1=1)'],0,'.',','); ?></td>
        <td align="right"></td>
        <td align="left">(คน)</td>
      </tr>  
      <tr>
        <td colspan="1" align="right"> -> PDF</td>
        <td align="right"><?php echo number_format($overAll['_csuspectedprotocolpdf'],0,'.',','); ?></td>
        <td align="right"><?php echo number_format(($overAll['_csuspectedprotocolpdf']/$overAll['ข้อมูลที่ Click Suspected เข้ามาจริงๆ (f2v6a3b1=1)'])*100,1,'.',','); ?>%</td>
        <td align="left">(คน)
            [n=<?php echo number_format($overAll['ข้อมูลที่ Click Suspected เข้ามาจริงๆ (f2v6a3b1=1)'],0,'.',','); ?>]
        </td>
      </tr>
      
      <tr>
        <td colspan="1" align="right"> ไม่เป็นไปตาม Protocol</td>
        <td align="right"><?php echo number_format($overAll['_csuspectedconditioninvalidprotocol'],0,'.',','); ?></td>
        <td></td>
        <td align="left">(คน) 
            =>
            <?php //echo number_format($overAll['_csuspectedconditioninvalidprotocol_visit'],0,'.',','); ?> 
            <?php
                echo Html::a("<span class=''>".number_format($overAll['_csuspectedconditioninvalidprotocol_visit'],0,'.',',')."</span>"
                        ,['/teleradio/suspected/verify?list=_csuspectedconditioninvalidprotocol_visit']
                        ,[
                            'class' =>'',
                            'title'=>'แสดงราย Record ตามเงื่อนไข'
                            ]
                        );
            ?> 
            ครั้ง
        </td>
        
      </tr>
      
      <tr class="success">
        <td align="right"><b>ข้อมูล ที่เข้าข่าย Suspected อาจจะคลิ๊ก Suspected หรือไม่คลิ๊ก หรือไม่คลิ๊กมา</b></td>
        <td align="right"><?php echo number_format($overAll['_csuspectedbycondition'],0,'.',','); ?></td>
        <td align="right"></td>
        <td align="left">(คน)</td>
      </tr>   
      <tr>
        <td colspan="1" align="right"> -> PDF</td>
        <td align="right"><?php echo number_format($overAll['_csuspectedbyconditionpdf'],0,'.',','); ?></td>
        <td align="right"><?php echo number_format(($overAll['_csuspectedbyconditionpdf']/$overAll['_csuspectedbycondition'])*100,1,'.',','); ?>%</td>
        <td align="left">(คน)
            [n=<?php echo number_format($overAll['_csuspectedbycondition'],0,'.',','); ?>]
        </td>
      </tr>
      <tr>
        <td colspan="1" align="right"> ข้อมูล ที่เข้าข่าย Suspected โดยคลิ๊ก Suspected มาถูกต้อง</td>
        <td align="right"><?php echo number_format($overAll['_csuspectedconditionprotocoltrue'],0,'.',','); ?></td>
        <td></td>
        <td align="left">(คน)  
            =>
            <?php //echo number_format($overAll['_csuspectedconditionprotocoltrue_visit'],0,'.',','); ?> 
            <?php
                echo Html::a("<span class=''>".number_format($overAll['_csuspectedconditionprotocoltrue_visit'],0,'.',',')."</span>"
                        ,['/teleradio/suspected/verify?list=_csuspectedconditionprotocoltrue_visit']
                        ,[
                            'class' =>'',
                            'title'=>'แสดงราย Record ตามเงื่อนไข'
                            ]
                        );
            ?>
            ครั้ง
        </td>
      </tr> 
      
      
      
      <tr>
        <td colspan="1" align="right"> มี CT/MRI</td>
        <td align="right"><?php echo number_format($ctmri['_cctmriwithoutsuspected'],0,'.',','); ?></td>
        <td></td>
        <td align="left">(คน)  
            =>
            <?php //echo number_format($overAll['_csuspectedconditionprotocolinvalid_visit'],0,'.',','); ?> 
            <?php
                echo Html::a("<span class=''>".number_format($overAll['_cctmriwithoutsuspected_visit'],0,'.',',')."</span>"
                        ,['/teleradio/suspected/verify?list=_cctmriwithoutsuspected_visit']
                        ,[
                            'class' =>'',
                            'title'=>'แสดงราย Record ตามเงื่อนไข'
                            ]
                        );
            ?>
            ครั้ง
        </td>
      </tr>
      
      <tr>
        <td colspan="1" align="right"> ข้อมูล ที่เข้าข่าย Suspected แต่ไม่ได้คลิ๊ก Suspected เข้มา</td>
        <td align="right"><?php echo number_format($overAll['_csuspectedconditionprotocolinvalid'],0,'.',','); ?></td>
        <td></td>
        <td align="left">(คน)  
            =>
            <?php //echo number_format($overAll['_csuspectedconditionprotocolinvalid_visit'],0,'.',','); ?> 
            <?php
                echo Html::a("<span class=''>".number_format($overAll['_csuspectedconditionprotocolinvalid_visit'],0,'.',',')."</span>"
                        ,['/teleradio/suspected/verify?list=_csuspectedconditionprotocolinvalid_visit']
                        ,[
                            'class' =>'',
                            'title'=>'แสดงราย Record ตามเงื่อนไข'
                            ]
                        );
            ?>
            ครั้ง
        </td>
      </tr>
      
      <tr>
        <td colspan="1" align="right"> มี CT/MRI</td>
        <td align="right"><?php echo number_format($ctmri['_cctmriwithoutsuspected'],0,'.',','); ?></td>
        <td></td>
        <td align="left">(คน)  
            =>
            <?php //echo number_format($overAll['_csuspectedconditionprotocolinvalid_visit'],0,'.',','); ?> 
            <?php
                echo Html::a("<span class=''>".number_format($overAll['_cctmriwithoutsuspected_visit'],0,'.',',')."</span>"
                        ,['/teleradio/suspected/verify?list=_cctmriwithoutsuspected_visit']
                        ,[
                            'class' =>'',
                            'title'=>'แสดงราย Record ตามเงื่อนไข'
                            ]
                        );
            ?>
            ครั้ง
        </td>
      </tr>
      <tr>
        <td colspan="1" align="right"> ไม่มี CT/MRI</td>
        <td align="right"><?php echo number_format($ctmri['_cctmriwithoutsuspected'],0,'.',','); ?></td>
        <td></td>
        <td align="left">(คน)  
            =>
            <?php //echo number_format($overAll['_csuspectedconditionprotocolinvalid_visit'],0,'.',','); ?> 
            <?php
                echo Html::a("<span class=''>".number_format($overAll['_cctmriwithoutsuspected_visit'],0,'.',',')."</span>"
                        ,['/teleradio/suspected/verify?list=_cctmriwithoutsuspected_visit']
                        ,[
                            'class' =>'',
                            'title'=>'แสดงราย Record ตามเงื่อนไข'
                            ]
                        );
            ?>
            ครั้ง
        </td>
      </tr>
      
      <tr>
        <td colspan="1" align="right"> ข้อมูล ที่ไม่เข้าข่าย Suspected แต่คลิ๊ก Suspected เข้มา</td>
        <td align="right"><?php echo number_format($overAll['_csuspectedconditioninvalidprotocol'],0,'.',','); ?></td>
        <td></td>
        <td align="left">(คน) 
            =>
            <?php //echo number_format($overAll['_csuspectedconditioninvalidprotocol_visit'],0,'.',','); ?> 
            <?php
                echo Html::a("<span class=''>".number_format($overAll['_csuspectedconditioninvalidprotocol_visit'],0,'.',',')."</span>"
                        ,['/teleradio/suspected/verify?list=_csuspectedconditioninvalidprotocol_visit']
                        ,[
                            'class' =>'',
                            'title'=>'แสดงราย Record ตามเงื่อนไข'
                            ]
                        );
            ?> 
            ครั้ง
        </td>
        
      </tr>
      
    </tbody>
  </table>
</div>
<?php
$irow=1;
if( count($listVisit)>0 ){
?>
<div class="container">
  <h2>Striped Rows</h2>
  <p>The .table-striped class adds zebra-stripes to a table:</p>            
  <table class="table table-striped">
    <thead>
      <tr>
        <th>No.</th>
        <th>Site ID</th>
        <th>PID</th>
        <th>ชื่อ-สกุล</th>
        <th>Exam Date</th>
        <th></th>
      </tr>
    </thead>
    <tbody>
<?php
    if( count($listVisit)>0 ){
        foreach($listVisit as $k => $v){
        ?>
      <tr>
        <td><?php echo $irow; ?></td>
        <td><?php echo $listVisit[$k]['hsitecode']; ?></td>
        <td><?php echo $listVisit[$k]['hptcode']; ?></td>
        <td><?php echo $listVisit[$k]['title']." ".$listVisit[$k]['name']." ".$listVisit[$k]['surname']; ?></td>
        <td><?php echo $listVisit[$k]['f2v1']; ?></td>
        <td>
            <?php
                $urlToCca02 = "/inputdata/redirect-page?dataid=".$listVisit[$k]['id']."&ezf_id=1437619524091524800&rurl=".base64_encode(Yii::$app->request->url);
                echo Html::a("<span class=''>Open</span>"
                        ,[$urlToCca02]
                        ,[
                            'class' =>'',
                            'target'=>'_blank',
                            'title'=>'แสดงราย Record ตามเงื่อนไข'
                            ]
                        );
            ?> 
        </td>
      </tr> 
        <?php
            $irow++;
        }
    }
?>
    </tbody>
  </table>
</div>

<?php
}

?>