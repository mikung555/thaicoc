<?php
    use yii\bootstrap\Modal;
    use kartik\tabs\TabsX;
    use yii\bootstrap\ActiveForm;
    use yii\helpers\Html;
    use yii\helpers\Url;
    
    $this->title = Yii::t('backend', 'DIABETIC RETINOPATHY');
    $this->params['breadcrumbs'][] = ['label'=>'Modules','url'=>  yii\helpers\Url::to('/tccbots/my-module')];
    $this->params['breadcrumbs'][] = Yii::t('backend', 'DR');
    
    
?>

<!--<div class="dr-default-index">
    <div class='alert alert-danger'><i class="fa fa-wrench" aria-hidden="true"></i> กำลังพัฒนาระบบ EMR </div>
</div>-->
<div class="dr-default-index"></div>

<?php 
    $this->registerJs("
        $(document).ready(function(){
            $('.dr-default-index').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
            $.ajax({
                url: '".Url::to(['/ckd/emr/index1'])."?ptlink=$ptlink&hospcode=$hospcode&idemr=$idemr',
                success:function(result){
                   $('.dr-default-index').html(result);
                },error: function (xhr, ajaxOptions, thrownError) {
                   console.log(xhr);
                }
            });
        });
        
    ");
?>