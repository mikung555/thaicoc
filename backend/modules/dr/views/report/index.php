<?php

    $this->title = Yii::t('backend', 'DIABETIC RETINOPATHY');
    $this->params['breadcrumbs'][] = ['label'=>'Modules','url'=>  yii\helpers\Url::to('/tccbots/my-module')];
    $this->params['breadcrumbs'][] = Yii::t('backend', 'DR');
?>
<div class="dr-default-index">
    <div class='alert alert-danger'><i class="fa fa-wrench" aria-hidden="true"></i> กำลังพัฒนาระบบ </div>
</div>