<?php
use kartik\tabs\TabsX;
use kartik\widgets\Select2;
use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\helpers\Url;
use appxq\sdii\widgets\GridView;
use appxq\sdii\widgets\ModalForm;
use appxq\sdii\helpers\SDNoty;
use appxq\sdii\helpers\SDHtml;
use yii\bootstrap\ActiveForm;

$this->title = Yii::t('backend', 'DIABETIC RETINOPATHY');
$this->params['breadcrumbs'][] = ['label'=>'Modules','url'=>  yii\helpers\Url::to('/tccbots/my-module')];
$this->params['breadcrumbs'][] = Yii::t('backend', 'DR');

?>
<div class="dr-inv-default-index">
<?php
//echo TabsX::widget([
//    'items' => $items,
//    'position' => TabsX::POS_ABOVE,
//    'encodeLabels' => false
//]);
?>
<?php
    $form = ActiveForm::begin([
        'id' => 'jump_menu',
        'action' => ['index', 'state'=>$state],
        'method' => 'get',
        'layout' => 'horizontal',
    ]);
?>

    <div class="row" style="margin-bottom: 15px;">
	<div class="col-sm-6">
	    <?=  Html::label('กุญแจถอดรหัสข้อมูล')?>
	    <?=  Html::passwordInput('key', isset(Yii::$app->session['key_dr'])?Yii::$app->session['key_dr']:'', ['class'=>'form-control', 'placeholder'=> 'กรุณากรอกกุญแจถอดรหัส', 'required'=>true])?>
	</div>
    </div>

	<?= Html::checkbox('save_keydr', isset(Yii::$app->session['save_keydr'])?Yii::$app->session['save_keydr']:false, ['label'=>'จดจำคีย์นี้'])?>
	<?= Html::checkbox('convert', isset(Yii::$app->session['convert_dr'])?Yii::$app->session['convert_dr']:false, ['label'=>'เข้ารหัสแบบ tis620 (กรณีชื่อ-สกุล อ่านไม่ออกให้กาช่องนี้ด้วย)'])?>

    <div class="row" style="margin-bottom: 15px;">
	<div class="col-sm-6">
	    <?= Html::submitButton(Yii::t('app', 'Submit'), ['class' => 'btn btn-primary']) ?>
	</div>
    </div>

    <div class="row" style="margin-bottom: 15px;">
	<div class="col-sm-6">
		<label class="control-label">Workspace</label>

		<?php

		echo Select2::widget([
		    'name' => 'state',
		    'value' => $state,
		    'data' => $dataList,
		    'options' => [
			'class' => 'form-control',
			'multiple' => false,
			'placeholder' => 'Select workspace ...',
			//'prompt' => 'All',
			'onChange' => '$("#jump_menu").submit()'
		    ]
		]);
		?>


	</div>

    </div>

    <?php ActiveForm::end(); ?>

    <?php  Pjax::begin(['id'=>'fperson-grid-pjax']);?>
    <?= GridView::widget([
	'id' => 'fperson-grid',
	'panelBtn' => '',
	'dataProvider' => $dataProvider,
	//'filterModel' => $searchModel,
        'columns' => [
	    [
		'class' => 'yii\grid\CheckboxColumn',
		'checkboxOptions' => [
		    'class' => 'selectionFPersonIds'
		],
		'headerOptions' => ['style'=>'text-align: center;'],
		'contentOptions' => ['style'=>'width:40px;text-align: center;'],
	    ],

	    [
		'class' => 'yii\grid\SerialColumn',
		'headerOptions' => ['style'=>'text-align: center;'],
		'contentOptions' => ['style'=>'width:60px;text-align: center;'],
	    ],
	    [
		'attribute'=>'CID',
		'headerOptions'=>['style'=>'text-align: center;'],
		'contentOptions'=>['style'=>'width:120px; text-align: center;'],
	    ],
	    [
		'attribute'=>'Pname',
		'headerOptions'=>['style'=>'text-align: center;'],
		'contentOptions'=>['style'=>'width:80px; '],
	    ],
	    [
		'attribute'=>'Name',
		'headerOptions'=>['style'=>'text-align: center;'],
		'contentOptions'=>['style'=>'width:200px; '],
	    ],
	    [
		'attribute'=>'Lname',
		'headerOptions'=>['style'=>'text-align: center;'],
		'contentOptions'=>['style'=>'width:200px; '],
	    ],

            [
		'header'=>'EMR',
		'format'=>'raw',
		'value'=>function ($data){
		    return Html::a('EMR', Url::to(['/dr/emr/index', 'ptlink'=>$data['ptlink'], 'hpcode'=>$data['hospcode'],'pid'=>$data['pid'],'idemr'=>'emr_dr']));
		},
		'headerOptions'=>['style'=>'text-align: center;'],
		'contentOptions'=>['style'=>'width:80px; text-align: center;'],
	    ],
            [
                'header'=>'Last eGFR',
                'format'=>'raw',
                'value'=>function ($data){

                    try {                        
                        $egfr =  backend\modules\ckdnet\classes\CkdnetQuery::getPatientProfileHospitalFieldOne($data['HOSPCODE']==null?$data['hospcode']:$data['HOSPCODE'], $data['PID']==null?$data['pid']:$data['PID'], 'egfr');
                        return number_format($egfr['egfr'],1);
                    } catch (\yii\db\Exception $e) {
                        return null;
                    }
                },
                'headerOptions'=>['style'=>'text-align: center;'],
                'contentOptions'=>['style'=>'width:80px; text-align: right;'],
            ],
            [
                'header'=>'CVD Risk',
                'format'=>'raw',
                'value'=>function ($data){
                    try {
                        $cvd_risk = backend\modules\ckdnet\classes\CkdnetQuery::getPatientProfileHospitalFieldOne($data['HOSPCODE']==null?$data['hospcode']:$data['HOSPCODE'], $data['PID']==null?$data['pid']:$data['PID'], 'cvd_risk');
                        return number_format($cvd_risk['cvd_risk'],1);
                    } catch (\yii\db\Exception $e) {
                        return null;
                    }
                },
                'headerOptions'=>['style'=>'text-align: center;'],
                'contentOptions'=>['style'=>'width:80px; text-align: right;'],
            ],
            [
                'header'=>'CVA',
                'format'=>'raw',
                'value'=>function ($data){
                    try {
                        $cva =  backend\modules\ckdnet\classes\CkdnetQuery::getPatientProfileHospitalFieldOne($data['HOSPCODE']==null?$data['hospcode']:$data['HOSPCODE'],  $data['PID']==null?$data['pid']:$data['PID'], 'cva_code');
                        return $cva['cva_code'];
                    } catch (\yii\db\Exception $e) {
                        return null;
                    }
                },
                'headerOptions'=>['style'=>'text-align: center;'],
                'contentOptions'=>['style'=>'width:80px; text-align: right;'],
            ],
            [
                'header'=>'CVA DATE',
                'format'=>'raw',
                'value'=>function ($data){
                    try {
                        $cva_date = backend\modules\ckdnet\classes\CkdnetQuery::getPatientProfileHospitalFieldOne($data['HOSPCODE'], $data['PID'], 'cva_date');
                        return substr($cva_date['cva_date'],0,10);
                    } catch (\yii\db\Exception $e) {
                        return null;
                    }
                },
                'headerOptions'=>['style'=>'text-align: center;'],
                'contentOptions'=>['style'=>'width:80px; text-align: right;'],
            ],
        ],
    ]); ?>
    <?php  Pjax::end();?>

</div>

<?=  ModalForm::widget([
    'id' => 'modal-fperson',
    'size'=>'modal-lg',
]);
?>

<?php  $this->registerJs("
$('#fperson-grid-pjax').on('click', '#modal-addbtn-fperson', function() {
    modalFPerson($(this).attr('data-url'));
});

$('#fperson-grid-pjax').on('click', '#modal-delbtn-fperson', function() {
    selectionFPersonGrid($(this).attr('data-url'));
});

$('#fperson-grid-pjax').on('click', '.select-on-check-all', function() {
    window.setTimeout(function() {
	var key = $('#fperson-grid').yiiGridView('getSelectedRows');
	disabledFPersonBtn(key.length);
    },100);
});

$('#fperson-grid-pjax').on('click', '.selectionFPersonIds', function() {
    var key = $('input:checked[class=\"'+$(this).attr('class')+'\"]');
    disabledFPersonBtn(key.length);
});

function disabledFPersonBtn(num) {
    if(num>0) {
	$('#modal-delbtn-fperson').attr('disabled', false);
    } else {
	$('#modal-delbtn-fperson').attr('disabled', true);
    }
}

$('#fperson-grid-pjax').on('click', '.btn-view', function() {
    var param = jQuery.parseJSON($(this).parent().parent().attr('data-key'));
    modalView(param, '".Url::to(['/managedata/managedata/ezform', 'table'=>'f_person', 'ezf_id'=>'1470198830071534100'])."');
});

$('#fperson-grid-pjax').on('dblclick', 'tbody tr', function() {
    var param = jQuery.parseJSON($(this).attr('data-key'));
    modalView(param, '".Url::to(['/managedata/managedata/ezform', 'table'=>'f_person', 'ezf_id'=>'1470198830071534100'])."');
});

function selectionFPersonGrid(url) {
    yii.confirm('".Yii::t('app', 'Are you sure you want to delete these items?')."', function() {
	$.ajax({
	    method: 'POST',
	    url: url,
	    data: $('.selectionFPersonIds:checked[name=\"selection[]\"]').serialize(),
	    dataType: 'JSON',
	    success: function(result, textStatus) {
		if(result.status == 'success') {
		    ". SDNoty::show('result.message', 'result.status') ."
		    $.pjax.reload({container:'#fperson-grid-pjax'});
		} else {
		    ". SDNoty::show('result.message', 'result.status') ."
		}
	    }
	});
    });
}

function modalView(param, url) {
    $('#modal-fperson .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
    $('#modal-fperson').modal('show');
    $.ajax({
	method: 'POST',
	url: url,
	data: param,
	dataType: 'JSON',
	success: function(result, textStatus) {
	    if(result.status == 'success') {
		". SDNoty::show('result.message', 'result.status') ."
		$('#modal-fperson .modal-content').html(result.html);
	    } else {
		". SDNoty::show('result.message', 'result.status') ."
	    }
	}
    });
}

function modalFPerson(url) {
    $('#modal-fperson .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
    $('#modal-fperson').modal('show')
    .find('.modal-content')
    .load(url);
}

");?>

