<?php
use yii\bootstrap\Html;
use backend\modules\dr\classes\DrFunc;
$this->title = Yii::t('backend', 'DIABETIC RETINOPATHY');
$this->params['breadcrumbs'][] = ['label'=>'Modules','url'=>  yii\helpers\Url::to('/tccbots/my-module')];
$this->params['breadcrumbs'][] = Yii::t('backend', 'DR');
$this->registerCss('
    table.table-diagram{
        border: 1px solid black;
    }
    table.table-diagram > thead.table-title> tr > th {
        text-align: center;
        border: 1px solid black;
    }
    table.table-diagram > tbody > tr > td {
        text-align: center;
        border: 1px solid black;
    }
    table.table-diagram > tbody > tr.dme-content{
        color:red;
    }
    table.table-content {
        height: 100%;width: 100%;
    }
    table.table-content > tbody > tr > td {
        vertical-align: bottom !important;
        color: red;
        text-align: center;
    }
    .p1 {
        left: 27%;
        top: 1.25%;
        width: 24%;
        height: 6%;
        position: absolute;
    }
    .p2 {
        left: 21.5%;
        top: 11%;
        width: 20%;
        height: 6%;
        position: absolute;
    }
    .p2_1 {
        left: 35.5%;
        top: 11%;
        width: 20%;
        height: 6%;
        position: absolute;
    }
    .p3 {
        left: 21.5%;
        top: 20.5%;
        width: 20%;
        height: 6%;
        position: absolute;
    }
    .p3_1 {
        left: 35.5%;
        top: 20.5%;
        width: 20%;
        height: 6%;
        position: absolute;
    }
    .p4 {
        left: 1%;
        top: 31.7%;
        width: 20%;
        height: 6%;
        position: absolute;
    }
    .p4_1 {
        left: 14%;
        top: 31.7%;
        width: 20%;
        height: 6%;
        position: absolute;
    }
    .p5 {
        left: 1%;
        top: 40.2%;
        width: 20%;
        height: 6%;
        position: absolute;
    }
    .p5_1 {
        left: 14%;
        top: 40.2%;
        width: 20%;
        height: 6%;
        position: absolute;
    }
    .p6 {
        left: 41%;
        top: 31.7%;
        width: 20%;
        height: 6%;
        position: absolute;
    }
    .p6_1 {
        left: 55%;
        top: 31.7%;
        width: 20%;
        height: 6%;
        position: absolute;
    }
    .p7 {
        left: 41%;
        top: 40.2%;
        width: 20%;
        height: 6%;
        position: absolute;
    }
    .p7_1 {
        left: 55%;
        top: 40.2%;
        width: 20%;
        height: 6%;
        position: absolute;
    }
    .p8 {
        left: 21.5%;
        top: 48.7%;
        width: 20%;
        height: 6%;
        position: absolute;
    }
    .p8_1 {
        left: 35.5%;
        top: 48.7%;
        width: 20%;
        height: 6%;
        position: absolute;
    }
    .p9 {
        left: 1%;
        top: 80%;
        width: 20%;
        height: 6%;
        position: absolute;
    }
    .p9_1 {
        left: 14%;
        top: 80%;
        width: 20%;
        height: 6%;
        position: absolute;
    }
    .p10 {
        left: 41%;
        top: 80%;
        width: 20%;
        height: 6%;
        position: absolute;
    }
    .p10_1 {
        left: 55%;
        top: 80%;
        width: 20%;
        height: 6%;
        position: absolute;
    }
    #btnSetting{
        margin-left: 0px;float: right;
    }
    .btn-diagram{
        font-size: larger;
        margin-right: 10px;
    }
');
    //appxq\sdii\utils\VarDumper::dump($idcen);
    $drDiagram = DrFunc::getDrDiagram(Yii::$app->user->identity->userProfile->sitecode, '', '',$idcen);
    $mapDiagram = yii\helpers\ArrayHelper::map($drDiagram, 'id', 'value');
    //\appxq\sdii\utils\VarDumper::dump($drDiagram);
?>

<div class="dr-default-index">
    
    <div class="col-md-offset-9 col-md-3 col-sm-offset-9 col-sm-3 col-xs-offset-9 col-xs-3">
        <div class="alert alert-warning text-center" style="padding:10px;">
            ขณะนี้อยู่ใน Mode :: <code><?php if($idcen == 'central'){echo "Central";}else{ echo "Hospital";}?></code>
        </div>
    </div>
    
    <div class="col-md-8 col-sm-8 col-xs-8" style="max-width:1051px;">
        <img class="img-rounded img-responsive" src="<?= \yii\helpers\Url::to(['@web/images/dr/dr-diagram.jpg']) ?>" alt="" >
        <!-- Show data Query-->
        <a class="p1 primary" href="<?= \yii\helpers\Url::to(['inv/index', 'state'=>1,'idcen'=>$idcen]) ?>"><table class="table-content"><tbody><tr><td><?= number_format($mapDiagram[1])?></td></tr></tbody></table></a>
        <a class="p2 primary" href="<?= \yii\helpers\Url::to(['inv/index', 'state'=>2,'idcen'=>$idcen]) ?>"><table class="table-content"><tbody><tr><td><?= number_format($mapDiagram[2])?></td></tr></tbody></table></a>
        <a class="p2_1 primary" href="<?= \yii\helpers\Url::to(['inv/index', 'state'=>2,'idcen'=>$idcen]) ?>"><table class="table-content"><tbody><tr><td><?= DrFunc::getPercent($mapDiagram[2],$mapDiagram[1]);?></td></tr></tbody></table></a>
        <a class="p3 primary" href="<?= \yii\helpers\Url::to(['inv/index', 'state'=>3,'idcen'=>$idcen]) ?>"><table class="table-content"><tbody><tr><td><?= number_format($mapDiagram[3])?></td></tr></tbody></table></a>
        <a class="p3_1 primary" href="<?= \yii\helpers\Url::to(['inv/index', 'state'=>3,'idcen'=>$idcen]) ?>"><table class="table-content"><tbody><tr><td><?= DrFunc::getPercent($mapDiagram[3],$mapDiagram[1]);?></td></tr></tbody></table></a>
        <a class="p4 primary" href="<?= \yii\helpers\Url::to(['inv/index', 'state'=>4,'idcen'=>$idcen]) ?>"><table class="table-content"><tbody><tr><td><?= number_format($mapDiagram[4])?></td></tr></tbody></table></a>
        <a class="p4_1 primary" href="<?= \yii\helpers\Url::to(['inv/index', 'state'=>4,'idcen'=>$idcen]) ?>"><table class="table-content"><tbody><tr><td><?= DrFunc::getPercent($mapDiagram[4],$mapDiagram[3]);?></td></tr></tbody></table></a>
        <a class="p5 primary" href="<?= \yii\helpers\Url::to(['inv/index', 'state'=>5,'idcen'=>$idcen]) ?>"><table class="table-content"><tbody><tr><td><?= number_format($mapDiagram[5])?></td></tr></tbody></table></a>
        <a class="p5_1 primary" href="<?= \yii\helpers\Url::to(['inv/index', 'state'=>5,'idcen'=>$idcen]) ?>"><table class="table-content"><tbody><tr><td><?= DrFunc::getPercent($mapDiagram[5],$mapDiagram[4]);?></td></tr></tbody></table></a>
        <a class="p6 primary" href="<?= \yii\helpers\Url::to(['inv/index', 'state'=>6,'idcen'=>$idcen]) ?>"><table class="table-content"><tbody><tr><td><?= number_format($mapDiagram[6])?></td></tr></tbody></table></a>
        <a class="p6_1 primary" href="<?= \yii\helpers\Url::to(['inv/index', 'state'=>6,'idcen'=>$idcen]) ?>"><table class="table-content"><tbody><tr><td><?= DrFunc::getPercent($mapDiagram[6],$mapDiagram[3]);?></td></tr></tbody></table></a>
        <a class="p7 primary" href="<?= \yii\helpers\Url::to(['inv/index', 'state'=>7,'idcen'=>$idcen]) ?>"><table class="table-content"><tbody><tr><td><?= number_format($mapDiagram[7])?></td></tr></tbody></table></a>
        <a class="p7_1 primary" href="<?= \yii\helpers\Url::to(['inv/index', 'state'=>7,'idcen'=>$idcen]) ?>"><table class="table-content"><tbody><tr><td><?= DrFunc::getPercent($mapDiagram[7],$mapDiagram[6]);?></td></tr></tbody></table></a>
        <a class="p8 primary" href="<?= \yii\helpers\Url::to(['inv/index', 'state'=>8,'idcen'=>$idcen]) ?>"><table class="table-content"><tbody><tr><td><?= number_format($mapDiagram[8])?></td></tr></tbody></table></a>
        <a class="p8_1 primary" href="<?= \yii\helpers\Url::to(['inv/index', 'state'=>8,'idcen'=>$idcen]) ?>"><table class="table-content"><tbody><tr><td><?= DrFunc::getPercent($mapDiagram[8],$mapDiagram[3]);?></td></tr></tbody></table></a>
        <a class="p9 primary" href="<?= \yii\helpers\Url::to(['inv/index', 'state'=>9,'idcen'=>$idcen]) ?>"><table class="table-content"><tbody><tr><td>NA</td></tr></tbody></table></a>
        <a class="p9_1 primary" href="<?= \yii\helpers\Url::to(['inv/index', 'state'=>9,'idcen'=>$idcen]) ?>"><table class="table-content"><tbody><tr><td>NA</td></tr></tbody></table></a>
        <a class="p10 primary" href="<?= \yii\helpers\Url::to(['inv/index', 'state'=>10,'idcen'=>$idcen]) ?>"><table class="table-content"><tbody><tr><td>NA</td></tr></tbody></table></a>
        <a class="p10_1 primary" href="<?= \yii\helpers\Url::to(['inv/index', 'state'=>10,'idcen'=>$idcen]) ?>"><table class="table-content"><tbody><tr><td>NA</td></tr></tbody></table></a>
        
        <table class="table table-bordered table-diagram">
            <thead class="table-title">
                <tr>
                    <th colspan="2">Mild NPDR</th>
                    <th colspan="2">Moderate NPDR</th>
                    <th colspan="2">Severe NPDR</th>
                    <th colspan="2">PDR</th>
                    <th>Unspecified</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>No DME</td><td>DME</td><td>No DME</td><td>DME</td><td>No DME</td><td>DME</td><td>No DME</td><td>DME</td>
                    <td colspan="2" rowspan="2" style="vertical-align: middle;color:red;" ><?= number_format($mapDiagram[8])?> (<?= DrFunc::getPercent($mapDiagram[8],$mapDiagram[3]);?>%)</td>
                </tr>
                <tr class="dme-content">
                    <td>NA</td><td>NA</td><td>NA</td><td>NA</td><td>NA</td><td>NA</td><td>NA</td><td>NA</td>
                </tr>
            </tbody>
        </table>
        <!-- Close show data Query-->
        <img class="img-rounded img-responsive" src="<?= \yii\helpers\Url::to(['@web/images/dr/dr-diagram_1.jpg']) ?>" alt="" >
    </div>
    <div class="col-md-4 col-sm-4 col-xs-4">
        <div id="btnSetting" class="pull-right">
            <!--btnSetting-->
            <?php
                if($idcen == 'central'){
                    echo   Html::a("<span class='glyphicon glyphicon-record'></span> Hospital", 
                        yii\helpers\Url::to(['/dr']),
                            ['class' => 'btn btn-info btn-diagram']
                        ); 
                }else{
                    echo   Html::a("<span class='glyphicon glyphicon-record'></span> Central", 
                        yii\helpers\Url::to(['/dr','idcen'=>'central']),
                            ['class' => 'btn btn-info btn-diagram']
                        ); 
                }
            
                echo   Html::button("<font size='4' class='glyphicon glyphicon-cog'></font>", 
                    [
                        'data-url' => yii\helpers\Url::to(['/ckd/map-lap/index'] ),
                        'class' => 'btn btn-success', 
                        'id'=>'btn-modal-maplap',
                        'title'=>"Mapping Lab"
                    ]); 
            ?>
        </div>
        
    </div>
</div>

<?=  appxq\sdii\widgets\ModalForm::widget([
    'id' => 'modal-lg',
    'size'=>'modal-lg',
    'tabindexEnable' => false,
]);
?>

<?php  $this->registerJs("
$('#btn-modal-maplap').on('click', function() {
    $('#modal-lg .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
    $('#modal-lg').modal('show')
    .find('.modal-content')
    .load($(this).attr('data-url'));
});



");?>
