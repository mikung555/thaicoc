<?php

namespace backend\modules\dr;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\modules\dr\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
        $this->layout = '@backend/views/layouts/dr';
    }
}
