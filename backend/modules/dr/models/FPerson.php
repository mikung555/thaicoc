<?php

namespace backend\modules\dr\models;

use Yii;

/**
 * This is the model class for table "f_person".
 *
 * @property string $sitecode
 * @property string $HOSPCODE
 * @property string $CID
 * @property string $PID
 * @property string $HID
 * @property string $PreName
 * @property string $Pname
 * @property string $Name
 * @property string $Lname
 * @property string $HN
 * @property string $sex
 * @property string $Birth
 * @property string $Mstatus
 * @property string $Occupation_Old
 * @property string $Occupation_New
 * @property string $Race
 * @property string $Nation
 * @property string $Religion
 * @property string $Education
 * @property string $Fstatus
 * @property string $Father
 * @property string $Mother
 * @property string $Couple
 * @property string $Vstatus
 * @property string $MoveIn
 * @property string $Discharge
 * @property string $Ddischarge
 * @property string $ABOGROUP
 * @property string $RHGROUP
 * @property string $Labor
 * @property string $PassPort
 * @property string $TypeArea
 * @property string $D_Update
 * @property string $ptlink
 */
class FPerson extends \yii\db\ActiveRecord
{
    public $lastegfr;
    public $cvdrisk;
    public $cva;
    public $urineprotine;
    
    public $items;//nut
    public $egfr;
    public $cvd_risk;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'f_person';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbdr');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sitecode', 'HOSPCODE', 'PID'], 'required'],
            [['Birth', 'MoveIn', 'Ddischarge', 'D_Update'], 'safe'],
            [['sitecode'], 'string', 'max' => 9],
            [['CID', 'Father', 'Mother', 'Couple'], 'string', 'max' => 26],
            [['PID', 'HID', 'TypeArea'], 'string', 'max' => 11],
            [['PreName', 'Race', 'Nation'], 'string', 'max' => 3],
            [['Pname'], 'string', 'max' => 50],
            [['Name', 'Lname'], 'string', 'max' => 100],
            [['HN'], 'string', 'max' => 18],
            [['sex', 'Mstatus', 'Fstatus', 'Vstatus', 'Discharge'], 'string', 'max' => 1],
            [['Occupation_Old', 'Occupation_New'], 'string', 'max' => 4],
            [['Religion', 'Education'], 'string', 'max' => 2],
            [['ABOGROUP'], 'string', 'max' => 20],
            [['RHGROUP'], 'string', 'max' => 5],
            [['Labor'], 'string', 'max' => 10],
            [['PassPort'], 'string', 'max' => 8],
            [['ptlink'], 'string', 'max' => 32],
	    [['HOSPCODE', 'urineprotine', 'cva', 'cvdrisk', 'lastegfr','egfr','cvd_risk'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'sitecode' => Yii::t('app', 'Sitecode'),
            'HOSPCODE' => Yii::t('app', 'Hospcode'),
            'CID' => Yii::t('app', 'รหัสบัตรประชาชน'),
            'PID' => Yii::t('app', 'Pid'),
            'HID' => Yii::t('app', 'Hid'),
            'PreName' => Yii::t('app', 'Pre Name'),
            'Pname' => Yii::t('app', 'คำนำหน้า'),
            'Name' => Yii::t('app', 'ชื่อ'),
            'Lname' => Yii::t('app', 'นามสกุล'),
            'HN' => Yii::t('app', 'HN'),
            'sex' => Yii::t('app', 'Sex'),
            'Birth' => Yii::t('app', 'Birth'),
            'Mstatus' => Yii::t('app', 'Mstatus'),
            'Occupation_Old' => Yii::t('app', 'Occupation  Old'),
            'Occupation_New' => Yii::t('app', 'Occupation  New'),
            'Race' => Yii::t('app', 'Race'),
            'Nation' => Yii::t('app', 'Nation'),
            'Religion' => Yii::t('app', 'Religion'),
            'Education' => Yii::t('app', 'Education'),
            'Fstatus' => Yii::t('app', 'Fstatus'),
            'Father' => Yii::t('app', 'Father'),
            'Mother' => Yii::t('app', 'Mother'),
            'Couple' => Yii::t('app', 'Couple'),
            'Vstatus' => Yii::t('app', 'Vstatus'),
            'MoveIn' => Yii::t('app', 'Move In'),
            'Discharge' => Yii::t('app', 'Discharge'),
            'Ddischarge' => Yii::t('app', 'Ddischarge'),
            'ABOGROUP' => Yii::t('app', 'Abogroup'),
            'RHGROUP' => Yii::t('app', 'Rhgroup'),
            'Labor' => Yii::t('app', 'Labor'),
            'PassPort' => Yii::t('app', 'Pass Port'),
            'TypeArea' => Yii::t('app', 'Type Area'),
            'D_Update' => Yii::t('app', 'D  Update'),
            'ptlink' => Yii::t('app', 'Ptlink'),
        ];
    }
}
