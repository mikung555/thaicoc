<?php

namespace backend\modules\dr\controllers;

use Yii;
use backend\modules\dr\models\FPersonSearch;

class InvController extends \yii\web\Controller
{
    public $activetab="inv";
    
    public function actionIndex()
    {
        $state = isset($_GET['state']) ? $_GET['state'] : 0;
	
	if (isset($_COOKIE['save_keydr']) && $_COOKIE['save_keydr']==1) {
	    if (isset($_COOKIE['key_dr'])) {
		Yii::$app->session['key_dr'] = $_COOKIE['key_dr'];
	    }
	    if (isset($_COOKIE['convert_dr'])) {
		Yii::$app->session['convert_dr'] = $_COOKIE['convert_dr'];
	    }
	    if (isset($_COOKIE['save_keydr'])) {
		Yii::$app->session['save_keydr'] = $_COOKIE['save_keydr'];
	    }
	} else {
	    Yii::$app->session['key_dr'] = '';
	    Yii::$app->session['convert_dr'] = 0;
	    Yii::$app->session['save_keydr'] = 0;
	    
	    setcookie('save_keydr', null, time()-1, "/", Yii::$app->keyStorage->get('frontend.domain'), false);
	    setcookie('key_dr', null, time()-1, "/", Yii::$app->keyStorage->get('frontend.domain'), false);
	    setcookie('convert_dr', null, time()-1, "/", Yii::$app->keyStorage->get('frontend.domain'), false);
	}
	
	
	    if (isset($_GET['save_keydr']) && $_GET['save_keydr']==1) {
		setcookie("save_keydr", $_GET['save_keydr'], time()+3600*24*30, "/", Yii::$app->keyStorage->get('frontend.domain'), false);
		setcookie("key_dr", $_GET['key'], time()+3600*24*30, "/", Yii::$app->keyStorage->get('frontend.domain'), false);
		setcookie("convert_dr", $_GET['convert'], time()+3600*24*30, "/", Yii::$app->keyStorage->get('frontend.domain'), false);
		
		
	    } 
	
	if(isset($_GET['key'])){    
	    Yii::$app->session['key_dr'] = $_GET['key'];
	    Yii::$app->session['convert_dr'] = $_GET['convert'];
	    Yii::$app->session['save_keydr'] = $_GET['save_keydr'];
	} 
	
	$convert = isset(Yii::$app->session['convert_dr']) ? Yii::$app->session['convert_dr'] : 0;
	$key = isset(Yii::$app->session['key_dr']) ?Yii::$app->session['key_dr'] : '';
	$save_keydr = isset(Yii::$app->session['save_keydr']) ?Yii::$app->session['save_keydr'] : 0;
	
	if(isset($_GET['save_keydr'])){
	    
	}
        $sitecode = Yii::$app->user->identity->userProfile->sitecode;
        $idcen = \Yii::$app->request->get('idcen');       
        $drDiagram = \backend\modules\dr\classes\DrFunc::getDrDiagram($sitecode, '', '',$idcen);
        $dataList = \yii\helpers\ArrayHelper::map($drDiagram, 'id', 'label');
         
	$searchModel = new FPersonSearch();
	$dataProvider = $searchModel->searchState(Yii::$app->request->queryParams, $sitecode, '', '', $state,$idcen);
        
        return $this->render('index',[
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'items' => $items,
            'state' => $state,
            'dataList' => $dataList,
        ]);
    }
   
}
