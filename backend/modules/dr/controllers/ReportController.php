<?php

namespace backend\modules\dr\controllers;

use Yii;

class ReportController extends \yii\web\Controller
{
    public $activetab="report";
    
    public function actionIndex()
    {
        return $this->render('index');
    }
}