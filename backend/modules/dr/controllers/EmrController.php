<?php

namespace backend\modules\dr\controllers;
//use app\models\FPerson;
//use \app\models\FLabfu;
use Yii;
//use yii\helpers\Json;
//use backend\modules\ckdnet\classes\CkdnetQuery;
//use backend\modules\ckdnet\classes\CkdnetFunc;

class EmrController extends \yii\web\Controller
{
    public $activetab="emr";
    
    public function actionIndex()
    {
//        $cid = isset($_GET['cid'])?$_GET['cid']:'';
        $ptlink = isset($_GET['ptlink'])?$_GET['ptlink']:'';
        $hospcode = Yii::$app->user->identity->userProfile->sitecode;
        $pid = isset($_GET['pid'])?$_GET['pid']:'';
        $idemr = isset($_GET['idemr'])?$_GET['idemr']:'';
        
        return $this->render('index',[
            'ptlink' => $ptlink,
            'hospcode' => $hospcode,
            'pid' => $pid,
            'idemr' => $idemr
        ]);
    }
}