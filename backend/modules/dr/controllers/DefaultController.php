<?php

namespace backend\modules\dr\controllers;

use yii\web\Controller;

class DefaultController extends Controller
{
    public $activetab="home";
    
    public function actionIndex()
    {
        $central = \Yii::$app->request->GET('idcen');
        return $this->render('index',['idcen'=>$central]);
    }
    
    public function actionRedirect($tab) {
        return $this->redirect('/dr/'.$tab,302);
    } 
}
