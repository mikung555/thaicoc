<?php

namespace backend\modules\dr\classes;

use Yii;

class DrQuery {
    
    public static function getDbConfig($sitecode) {
	$sql = "SELECT db_config_province.province,
			db_config_province.zonecode,
			db_config_province.`server`,
			db_config_province.`user`,
			db_config_province.passwd,
			db_config_province.`port`,
			db_config_province.db,
			db_config_province.webservice
		FROM db_config_province INNER JOIN all_hospital_thai ON db_config_province.province = all_hospital_thai.provincecode
		WHERE all_hospital_thai.hcode = :sitecode
		";
	return Yii::$app->dbbot_ip8->createCommand($sql, [':sitecode'=>$sitecode])->queryOne();
    }
    
    public static function genSelect() {
        $convert = isset(Yii::$app->session['convert_ckd']) ? Yii::$app->session['convert_ckd'] : 0;
        $key = isset(Yii::$app->session['key_ckd']) ?Yii::$app->session['key_ckd'] : '';

        $selectKey = "`f_person`.HOSPCODE,
                `f_person`.PID,
                `f_person`.sitecode,
                `f_person`.ptlink,
                `f_person`.CID,
                `f_person`.`Pname`,
                `f_person`.`Name`,
                `f_person`.`Lname`";

        if($key!=''){
            $selectKey = "`f_person`.hospcode,
                `f_person`.pid,
                `f_person`.sitecode,
                `f_person`.ptlink,
                decode(unhex(f_person.CID),sha2('$key',256)) AS CID,
                decode(unhex(f_person.Pname),sha2('$key',256)) AS Pname,
                decode(unhex(f_person.`Name`),sha2('$key',256)) AS Name,
                decode(unhex(f_person.Lname),sha2('$key',256)) AS Lname";

            if ($convert==1) {
                $selectKey = "`f_person`.hospcode,
                `f_person`.pid,
                `f_person`.sitecode,
                `f_person`.ptlink,
                convert(decode(unhex(f_person.CID),sha2('$key',256)) using tis620) AS CID,
                convert(decode(unhex(f_person.Pname),sha2('$key',256)) using tis620) AS Pname,
                convert(decode(unhex(f_person.`Name`),sha2('$key',256)) using tis620) AS Name,
                convert(decode(unhex(f_person.Lname),sha2('$key',256)) using tis620) AS Lname";

            }
        }

        return $selectKey;
    }
    
    //====== Condition ======
    private static $sqlReportedDm = "IFNULL(ph.reported_dm,0) = '1'";
    private static $sqlDeath = "IFNULL(ph.`death`,0) = 0";
    private static $sqlTypearea = "(IFNULL(ph.`type_area`,0) = '1') OR (IFNULL(ph.`type_area`,0) = '3')";
    private static $sqlDm = "IFNULL(ph.`dm`,0) >= '1' ";
    private static $sqlIcd10 = "IFNULL(LEFT(ph.dm_last_code,3),'') IN ('E10','E11')";
    private static $sqlRetina = "IFNULL(ph.`retina`,0) IN ('1','2','3','4')";
    private static $sqlRetinaOph = "IFNULL(ph.`retina`,0) IN ('1','3')";
    private static $sqlRetinaAbnOph = "IFNULL(ph.`retina`,0) IN ('3')";
    private static $sqlRetinaFundus = "IFNULL(ph.`retina`,0) IN ('2','4')";
    private static $sqlRetinaAbnFundus = "IFNULL(ph.`retina`,0) IN ('4')";
    private static $sqlRetinaAbnTotal = "IFNULL(ph.`retina`,0) IN ('3','4')";
    private static $sqlNotTreatmentindicated = "";
    private static $sqlTreatmentindicated = "";
    //====== Condition Central ======
    private static $sqlReportedDmCen = "IFNULL(pc.reported_dm,0) = '1'";
    private static $sqlDeathCen = "IFNULL(pc.`death`,0) = 0";
    private static $sqlDmCen = "IFNULL(pc.`dm`,0) >= '1' ";
    private static $sqlIcd10Cen = "IFNULL(LEFT(pc.dm_last_code,3),'') IN ('E10','E11')";
    private static $sqlRetinaCen = "IFNULL(pc.`retina`,0) IN ('1','2','3','4')";
    private static $sqlRetinaOphCen = "IFNULL(pc.`retina`,0) IN ('1','3')";
    private static $sqlRetinaAbnOphCen = "IFNULL(pc.`retina`,0) IN ('3')";
    private static $sqlRetinaFundusCen = "IFNULL(pc.`retina`,0) IN ('2','4')";
    private static $sqlRetinaAbnFundusCen = "IFNULL(pc.`retina`,0) IN ('4')";
    private static $sqlRetinaAbnTotalCen = "IFNULL(pc.`retina`,0) IN ('3','4')";
    private static $sqlNotTreatmentindicatedCen = "";
    private static $sqlTreatmentindicatedCen = "";
    
  
    public static function getAll($sitecode, $sdate, $edate,$idcen,$dp=false){
        if($idcen == 'central'){
            $sql = "SELECT
                        COUNT(*)
                    FROM
                        `patient_profile_hospital` ph
                    LEFT JOIN patient_profile_central pc ON ph.ptlink = pc.ptlink
                    WHERE
                        ph.`hospcode` = :sitecode AND
                        (pc.ptlink <> '' OR ph.ptlink = '')
                    ";
            if($dp){
                $selectKey = self::genSelect();

                $sqlDp = "SELECT
                            $selectKey
                        FROM
                            `patient_profile_hospital` ph
                        INNER JOIN `f_person`
                            ON `ph`.`hospcode` = `f_person`.`HOSPCODE`
                            AND `ph`.`pid` = `f_person`.`PID`
                            AND `ph`.`hospcode` = `f_person`.`sitecode`
                        LEFT JOIN patient_profile_central pc ON ph.ptlink = pc.ptlink
                        WHERE
                            `ph`.`hospcode` = :sitecode AND
                            `f_person`.`sitecode` = :sitecode AND
                            (pc.ptlink <> '' OR ph.ptlink = '')
                    ";

                return [
                    'sql' =>$sqlDp,
                    'param'=>[':sitecode'=>$sitecode],
                    'keyID'=>'ptlink',
                    'total'=>DrFunc::queryScalar($sql, [':sitecode'=>$sitecode]),
                ];
            }
        }else{
            
            $sql = "SELECT
                        COUNT(*)
                    FROM
                        `patient_profile_hospital` ph
                    WHERE
                        ph.`hospcode` = :sitecode
                    ";
            if($dp){
                $selectKey = self::genSelect();

                $sqlDp = "SELECT
                        $selectKey
                    FROM
                        `patient_profile_hospital` ph
                        INNER JOIN `f_person`
                        ON ph.`hospcode` = f_person.`HOSPCODE`
                        AND ph.`pid` = f_person.`PID`
                    WHERE
                        ph.`hospcode` = :sitecode AND
                        f_person.`sitecode` = :sitecode
                    ";

                return [
                    'sql' =>$sqlDp,
                    'param'=>[':sitecode'=>$sitecode],
                    'keyID'=>'ptlink',
                    'total'=>DrFunc::queryScalar($sql, [':sitecode'=>$sitecode]),
                ];
            }
        }
        return DrFunc::queryScalar($sql, [':sitecode'=>$sitecode]);  
        
    }
    
    public static function getTotalPop($sitecode, $sdate, $edate,$idcen,$dp=false){
        # call DrQuery::getTotalPop
        # Dr: TotalPop -> Death -> Typearea -> (1)
        # - Death -
        # - Typearea -> (1)
        $sqlDeath = self::$sqlDeath;
        $sqlTypearea = self::$sqlTypearea;
        $sqlDeathCen = self::$sqlDeathCen;
                
        if($idcen == 'central'){
            $sql = "SELECT
                        COUNT(*)
                    FROM
                        `patient_profile_hospital` ph
                    LEFT JOIN patient_profile_central pc ON ph.ptlink = pc.ptlink
                    WHERE
                        ph.`hospcode` = :sitecode AND
                        (pc.ptlink <> '' OR ph.ptlink = '') AND
                        (".$sqlDeathCen.") AND
                        (".$sqlTypearea.")
                    ";
            if($dp){
                $selectKey = self::genSelect();

                $sqlDp = "SELECT
                            $selectKey
                        FROM
                            `patient_profile_hospital` ph
                        INNER JOIN `f_person`
                            ON `ph`.`hospcode` = `f_person`.`HOSPCODE`
                            AND `ph`.`pid` = `f_person`.`PID`
                            AND `ph`.`hospcode` = `f_person`.`sitecode`
                        LEFT JOIN patient_profile_central pc ON ph.ptlink = pc.ptlink
                        WHERE
                            `ph`.`hospcode` = :sitecode AND
                            `f_person`.`sitecode` = :sitecode AND
                            (pc.ptlink <> '' OR ph.ptlink = '') AND
                            (".$sqlDeathCen.") AND
                            (".$sqlTypearea.")
                    ";

                return [
                    'sql' =>$sqlDp,
                    'param'=>[':sitecode'=>$sitecode],
                    'keyID'=>'ptlink',
                    'total'=>DrFunc::queryScalar($sql, [':sitecode'=>$sitecode]),
                ];
            }
        }else{
            $sql = "SELECT
                        COUNT(*)
                    FROM
                        `patient_profile_hospital` ph
                    WHERE
                        `hospcode` = :sitecode AND
                        (".$sqlDeath.") AND
                        (".$sqlTypearea.")
                    ";
            if($dp){
                $selectKey = self::genSelect();

                $sqlDp = "SELECT
                        $selectKey
                    FROM
                        `patient_profile_hospital` ph
                        INNER JOIN `f_person`
                        ON ph.`hospcode` = `f_person`.`HOSPCODE`
                        AND ph.`pid` = `f_person`.`PID`
                        AND ph.`hospcode` = `f_person`.`sitecode`
                    WHERE
                        ph.`hospcode` = :sitecode AND
                        `f_person`.`sitecode` = :sitecode AND
                        (".$sqlDeath.") AND
                        (".$sqlTypearea.")
                    ";

                return [
                    'sql' =>$sqlDp,
                    'keyID'=> function ($model) {
                        return ['sitecode'=>$model['sitecode'], 'PID'=>$model['PID'], 'HOSPCODE'=>$model['HOSPCODE']];
                    },
                    'param'=>[':sitecode'=>$sitecode],
                    'total'=>DrFunc::queryScalar($sql, [':sitecode'=>$sitecode]),
                ];
            }
        }
        return DrFunc::queryScalar($sql, [':sitecode'=>$sitecode]);   
        
    }
    
    public static function getDm($sitecode, $sdate, $edate,$idcen,$dp=false){
        # call DrQuery::getDm
        # Dr: DM -> Icd10 IN E10,E11 -> (2)
        # - Death -
        # - Typearea -
        # - DM -
        # - ICD10 E10,E11 -> (2)
        $sqlDeath = self::$sqlDeath;
        $sqlTypearea = self::$sqlTypearea;
        $sqlDm = self::$sqlDm;
        $sqlReportedDm = self::$sqlReportedDm;
        $sqlIcd10 = self::$sqlIcd10;
        $sqlDeathCen = self::$sqlDeathCen;
        $sqlDmCen = self::$sqlDmCen;
        $sqlReportedDmCen = self::$sqlReportedDmCen;
        $sqlIcd10Cen = self::$sqlIcd10Cen;
        
        if($idcen == 'central'){
            $sql = "SELECT
                        COUNT(*)
                    FROM
                        `patient_profile_hospital` ph
                    LEFT JOIN patient_profile_central pc ON ph.ptlink = pc.ptlink
                    WHERE
                        ph.`hospcode` = :sitecode AND
                        (pc.ptlink <> '' OR ph.ptlink = '') AND
                        (".$sqlDeathCen.") AND
                        (".$sqlTypearea.") AND
                        (".$sqlDmCen.") AND
                        NOT (".$sqlReportedDmCen.") AND    
                        (".$sqlIcd10Cen.")   
                    ";
            if($dp){
                $selectKey = self::genSelect();

                $sqlDp = "SELECT
                            $selectKey
                        FROM
                            `patient_profile_hospital` ph
                        INNER JOIN `f_person`
                            ON `ph`.`hospcode` = `f_person`.`HOSPCODE`
                            AND `ph`.`pid` = `f_person`.`PID`
                            AND `ph`.`hospcode` = `f_person`.`sitecode`
                        LEFT JOIN patient_profile_central pc ON ph.ptlink = pc.ptlink
                        WHERE
                            `ph`.`hospcode` = :sitecode AND
                            `f_person`.`sitecode` = :sitecode AND
                            (pc.ptlink <> '' OR ph.ptlink = '') AND
                            (".$sqlDeath.") AND
                            (".$sqlTypearea.") AND
                            (".$sqlDm.") AND
                            NOT (".$sqlReportedDmCen.") AND    
                            (".$sqlIcd10.")   
                    ";

                return [
                    'sql' =>$sqlDp,
                    'param'=>[':sitecode'=>$sitecode],
                    'keyID'=>'ptlink',
                    'total'=>DrFunc::queryScalar($sql, [':sitecode'=>$sitecode]),
                ];
            }
        }else{
        
            $sql = "SELECT
                        COUNT(*)
                    FROM
                        `patient_profile_hospital` ph
                    WHERE
                        `hospcode` = :sitecode AND
                        (".$sqlDeath.") AND
                        (".$sqlTypearea.") AND
                        (".$sqlDm.") AND
                        NOT (".$sqlReportedDm.") AND    
                        (".$sqlIcd10.")    
                    ";
            if($dp){
                $selectKey = self::genSelect();

                $sqlDp = "SELECT
                        $selectKey
                    FROM
                        `patient_profile_hospital` ph
                        INNER JOIN `f_person`
                        ON ph.`hospcode` = `f_person`.`HOSPCODE`
                        AND ph.`pid` = `f_person`.`PID`
                        AND ph.`hospcode` = `f_person`.`sitecode`
                    WHERE
                        ph.`hospcode` = :sitecode AND
                        `f_person`.`sitecode` = :sitecode AND
                        (".$sqlDeath.") AND
                        (".$sqlTypearea.") AND
                        (".$sqlDm.") AND
                        NOT (".$sqlReportedDm.") AND
                        (".$sqlIcd10.")
                    ";

                return [
                    'sql' =>$sqlDp,
                    'param'=>[':sitecode'=>$sitecode],
                    'total'=>DrFunc::queryScalar($sql, [':sitecode'=>$sitecode]),
                ];
            }
        }
        return DrFunc::queryScalar($sql, [':sitecode'=>$sitecode]);          
    }
    
    public static function getRetina($sitecode, $sdate, $edate,$idcen,$dp=false){
        # call DrQuery::getRetina
        # Dr: Retina IN (1,2,3,4) -> (3)
        # - Death -
        # - Typearea -
        # - DM -
        # - ICD10 E10,E11 -
        # - Retina IN (1,2,3,4) -> (3)
        $sqlDeath = self::$sqlDeath;
        $sqlTypearea = self::$sqlTypearea;
        $sqlDm = self::$sqlDm;
        $sqlReportedDm = self::$sqlReportedDm;
        $sqlIcd10 = self::$sqlIcd10;
        $sqlRetina = self::$sqlRetina;
        $sqlDeathCen = self::$sqlDeathCen;
        $sqlDmCen = self::$sqlDmCen;
        $sqlReportedDmCen = self::$sqlReportedDmCen;
        $sqlIcd10Cen = self::$sqlIcd10Cen;
        $sqlRetinaCen = self::$sqlRetinaCen;
        
        if($idcen == 'central'){
            $sql = "SELECT
                        COUNT(*)
                    FROM
                        `patient_profile_hospital` ph
                    LEFT JOIN patient_profile_central pc ON ph.ptlink = pc.ptlink
                    WHERE
                        ph.`hospcode` = :sitecode AND
                        (pc.ptlink <> '' OR ph.ptlink = '') AND
                        (".$sqlDeathCen.") AND
                        (".$sqlTypearea.") AND
                        (".$sqlDmCen.") AND
                        NOT (".$sqlReportedDmCen.") AND
                        (".$sqlIcd10Cen.") AND
                        (".$sqlRetinaCen.") 
                    ";
            if($dp){
                $selectKey = self::genSelect();

                $sqlDp = "SELECT
                            $selectKey
                        FROM
                            `patient_profile_hospital` ph
                        INNER JOIN `f_person`
                            ON `ph`.`hospcode` = `f_person`.`HOSPCODE`
                            AND `ph`.`pid` = `f_person`.`PID`
                            AND `ph`.`hospcode` = `f_person`.`sitecode`
                        LEFT JOIN patient_profile_central pc ON ph.ptlink = pc.ptlink
                        WHERE
                            `ph`.`hospcode` = :sitecode AND
                            `f_person`.`sitecode` = :sitecode AND
                            (pc.ptlink <> '' OR ph.ptlink = '') AND
                            (".$sqlDeathCen.") AND
                            (".$sqlTypearea.") AND
                            (".$sqlDmCen.") AND
                            NOT (".$sqlReportedDmCen.") AND
                            (".$sqlIcd10Cen.") AND
                            (".$sqlRetinaCen.") 
                    ";

                return [
                    'sql' =>$sqlDp,
                    'param'=>[':sitecode'=>$sitecode],
                    'keyID'=>'ptlink',
                    'total'=>DrFunc::queryScalar($sql, [':sitecode'=>$sitecode]),
                ];
            }
        }else{
            $sql = "SELECT
                        COUNT(*)
                    FROM
                        `patient_profile_hospital` ph
                    WHERE
                        `hospcode` = :sitecode AND
                        (".$sqlDeath.") AND
                        (".$sqlTypearea.") AND
                        (".$sqlDm.") AND
                        NOT (".$sqlReportedDm.") AND
                        (".$sqlIcd10.") AND
                        (".$sqlRetina.")   
                    ";
            if($dp){
                $selectKey = self::genSelect();

                $sqlDp = "SELECT
                        $selectKey
                    FROM
                        `patient_profile_hospital` ph
                        INNER JOIN `f_person`
                        ON ph.`hospcode` = `f_person`.`HOSPCODE`
                        AND ph.`pid` = `f_person`.`PID`
                        AND ph.`hospcode` = `f_person`.`sitecode`
                    WHERE
                        ph.`hospcode` = :sitecode AND
                        `f_person`.`sitecode` = :sitecode AND
                        (".$sqlDeath.") AND
                        (".$sqlTypearea.") AND
                        (".$sqlDm.") AND
                        NOT (".$sqlReportedDm.") AND
                        (".$sqlIcd10.") AND
                        (".$sqlRetina.")    
                    ";

                return [
                    'sql' =>$sqlDp,
                    'param'=>[':sitecode'=>$sitecode],
                    'total'=>DrFunc::queryScalar($sql, [':sitecode'=>$sitecode]),
                ];
            }
        }
        return DrFunc::queryScalar($sql, [':sitecode'=>$sitecode]);          
    }
    
    public static function getOphalmoscope($sitecode, $sdate, $edate,$idcen,$dp=false){
        # call DrQuery::getOphalmoscope
        # Dr: Ophalmoscope => Retina IN (1,3) -> (4)
        # - Death -
        # - Typearea -
        # - DM -
        # - ICD10 E10,E11 -
        # - Retina IN (1,3) -> (4)
        $sqlDeath = self::$sqlDeath;
        $sqlTypearea = self::$sqlTypearea;
        $sqlDm = self::$sqlDm;
        $sqlReportedDm = self::$sqlReportedDm;
        $sqlIcd10 = self::$sqlIcd10;
        $sqlRetinaOph = self::$sqlRetinaOph;
        $sqlDeathCen = self::$sqlDeathCen;
        $sqlDmCen = self::$sqlDmCen;
        $sqlReportedDmCen = self::$sqlReportedDmCen;
        $sqlIcd10Cen = self::$sqlIcd10Cen;
        $sqlRetinaOphCen = self::$sqlRetinaOphCen;
        
        if($idcen == 'central'){
            $sql = "SELECT
                        COUNT(*)
                    FROM
                        `patient_profile_hospital` ph
                    LEFT JOIN patient_profile_central pc ON ph.ptlink = pc.ptlink
                    WHERE
                        ph.`hospcode` = :sitecode AND
                        (pc.ptlink <> '' OR ph.ptlink = '') AND
                        (".$sqlDeathCen.") AND
                        (".$sqlTypearea.") AND
                        (".$sqlDmCen.") AND
                        NOT (".$sqlReportedDmCen.") AND
                        (".$sqlIcd10Cen.") AND
                        (".$sqlRetinaOphCen.") 
                    ";
            if($dp){
                $selectKey = self::genSelect();

                $sqlDp = "SELECT
                            $selectKey
                        FROM
                            `patient_profile_hospital` ph
                        INNER JOIN `f_person`
                            ON `ph`.`hospcode` = `f_person`.`HOSPCODE`
                            AND `ph`.`pid` = `f_person`.`PID`
                            AND `ph`.`hospcode` = `f_person`.`sitecode`
                        LEFT JOIN patient_profile_central pc ON ph.ptlink = pc.ptlink
                        WHERE
                            `ph`.`hospcode` = :sitecode AND
                            `f_person`.`sitecode` = :sitecode AND
                            (pc.ptlink <> '' OR ph.ptlink = '') AND
                            (".$sqlDeathCen.") AND
                            (".$sqlTypearea.") AND
                            (".$sqlDmCen.") AND
                            NOT (".$sqlReportedDmCen.") AND
                            (".$sqlIcd10Cen.") AND
                            (".$sqlRetinaOphCen.") 
                    ";

                return [
                    'sql' =>$sqlDp,
                    'param'=>[':sitecode'=>$sitecode],
                    'keyID'=>'ptlink',
                    'total'=>DrFunc::queryScalar($sql, [':sitecode'=>$sitecode]),
                ];
            }
        }else{
            $sql = "SELECT
                        COUNT(*)
                    FROM
                        `patient_profile_hospital` ph
                    WHERE
                        `hospcode` = :sitecode AND
                        (".$sqlDeath.") AND
                        (".$sqlTypearea.") AND
                        (".$sqlDm.") AND
                        NOT (".$sqlReportedDm.") AND
                        (".$sqlIcd10.") AND
                        (".$sqlRetinaOph.")   
                    ";
            if($dp){
                $selectKey = self::genSelect();

                $sqlDp = "SELECT
                        $selectKey
                    FROM
                        `patient_profile_hospital` ph
                        INNER JOIN `f_person`
                        ON ph.`hospcode` = `f_person`.`HOSPCODE`
                        AND ph.`pid` = `f_person`.`PID`
                        AND ph.`hospcode` = `f_person`.`sitecode`
                    WHERE
                        ph.`hospcode` = :sitecode AND
                        `f_person`.`sitecode` = :sitecode AND
                        (".$sqlDeath.") AND
                        (".$sqlTypearea.") AND
                        (".$sqlDm.") AND
                        NOT (".$sqlReportedDm.") AND
                        (".$sqlIcd10.") AND
                        (".$sqlRetinaOph.")    
                    ";

                return [
                    'sql' =>$sqlDp,
                    'param'=>[':sitecode'=>$sitecode],
                    'total'=>DrFunc::queryScalar($sql, [':sitecode'=>$sitecode]),
                ];
            }
        }
        return DrFunc::queryScalar($sql, [':sitecode'=>$sitecode]);          
    }
    
    public static function getAbnormalOphalmoscope($sitecode, $sdate, $edate,$idcen,$dp=false){
        # call DrQuery::getAbnormalOphalmoscope
        # Dr: Abnormal Ophalmoscope => Retina IN (3) -> (5)
        # - Death -
        # - Typearea -
        # - DM -
        # - ICD10 E10,E11 -
        # - Retina IN (3) -> (5)
        $sqlDeath = self::$sqlDeath;
        $sqlTypearea = self::$sqlTypearea;
        $sqlDm = self::$sqlDm;
        $sqlReportedDm = self::$sqlReportedDm;
        $sqlIcd10 = self::$sqlIcd10;
        $sqlRetinaAbnOph = self::$sqlRetinaAbnOph;
        $sqlDeathCen = self::$sqlDeathCen;
        $sqlDmCen = self::$sqlDmCen;
        $sqlReportedDmCen = self::$sqlReportedDmCen;
        $sqlIcd10Cen = self::$sqlIcd10Cen;
        $sqlRetinaAbnOphCen = self::$sqlRetinaAbnOphCen;
        
        if($idcen == 'central'){
            $sql = "SELECT
                        COUNT(*)
                    FROM
                        `patient_profile_hospital` ph
                    LEFT JOIN patient_profile_central pc ON ph.ptlink = pc.ptlink
                    WHERE
                        ph.`hospcode` = :sitecode AND
                        (pc.ptlink <> '' OR ph.ptlink = '') AND
                        (".$sqlDeathCen.") AND
                        (".$sqlTypearea.") AND
                        (".$sqlDmCen.") AND
                        NOT (".$sqlReportedDmCen.") AND
                        (".$sqlIcd10Cen.") AND
                        (".$sqlRetinaAbnOphCen.") 
                    ";
            if($dp){
                $selectKey = self::genSelect();

                $sqlDp = "SELECT
                            $selectKey
                        FROM
                            `patient_profile_hospital` ph
                        INNER JOIN `f_person`
                            ON `ph`.`hospcode` = `f_person`.`HOSPCODE`
                            AND `ph`.`pid` = `f_person`.`PID`
                            AND `ph`.`hospcode` = `f_person`.`sitecode`
                        LEFT JOIN patient_profile_central pc ON ph.ptlink = pc.ptlink
                        WHERE
                            `ph`.`hospcode` = :sitecode AND
                            `f_person`.`sitecode` = :sitecode AND
                            (pc.ptlink <> '' OR ph.ptlink = '') AND
                            (".$sqlDeathCen.") AND
                            (".$sqlTypearea.") AND
                            (".$sqlDmCen.") AND
                            NOT (".$sqlReportedDmCen.") AND
                            (".$sqlIcd10Cen.") AND
                            (".$sqlRetinaAbnOphCen.") 
                    ";

                return [
                    'sql' =>$sqlDp,
                    'param'=>[':sitecode'=>$sitecode],
                    'keyID'=>'ptlink',
                    'total'=>DrFunc::queryScalar($sql, [':sitecode'=>$sitecode]),
                ];
            }
        }else{
            $sql = "SELECT
                        COUNT(*)
                    FROM
                        `patient_profile_hospital` ph
                    WHERE
                        `hospcode` = :sitecode AND
                        (".$sqlDeath.") AND
                        (".$sqlTypearea.") AND
                        (".$sqlDm.") AND
                        NOT (".$sqlReportedDm.") AND
                        (".$sqlIcd10.") AND
                        (".$sqlRetinaAbnOph.")   
                    ";
            if($dp){
                $selectKey = self::genSelect();

                $sqlDp = "SELECT
                        $selectKey
                    FROM
                        `patient_profile_hospital` ph
                        INNER JOIN `f_person`
                        ON ph.`hospcode` = `f_person`.`HOSPCODE`
                        AND ph.`pid` = `f_person`.`PID`
                        AND ph.`hospcode` = `f_person`.`sitecode`
                    WHERE
                        ph.`hospcode` = :sitecode AND
                        `f_person`.`sitecode` = :sitecode AND
                        (".$sqlDeath.") AND
                        (".$sqlTypearea.") AND
                        (".$sqlDm.") AND
                        NOT (".$sqlReportedDm.") AND
                        (".$sqlIcd10.") AND
                        (".$sqlRetinaAbnOph.")    
                    ";

                return [
                    'sql' =>$sqlDp,
                    'param'=>[':sitecode'=>$sitecode],
                    'total'=>DrFunc::queryScalar($sql, [':sitecode'=>$sitecode]),
                ];
            }
        }
        return DrFunc::queryScalar($sql, [':sitecode'=>$sitecode]);          
    }
    
    public static function getFundus($sitecode, $sdate, $edate,$idcen,$dp=false){
        # call DrQuery::getFundus
        # Dr: Fundus Camera => Retina IN (2,4) -> (6)
        # - Death -
        # - Typearea -
        # - DM -
        # - ICD10 E10,E11 -
        # - Retina IN (2,4) -> (6)
        $sqlDeath = self::$sqlDeath;
        $sqlTypearea = self::$sqlTypearea;
        $sqlDm = self::$sqlDm;
        $sqlReportedDm = self::$sqlReportedDm;
        $sqlIcd10 = self::$sqlIcd10;
        $sqlRetinaFundus = self::$sqlRetinaFundus;
        $sqlDeathCen = self::$sqlDeathCen;
        $sqlDmCen = self::$sqlDmCen;
        $sqlReportedDmCen = self::$sqlReportedDmCen;
        $sqlIcd10Cen = self::$sqlIcd10Cen;
        $sqlRetinaFundusCen = self::$sqlRetinaFundusCen;
        
        if($idcen == 'central'){
            $sql = "SELECT
                        COUNT(*)
                    FROM
                        `patient_profile_hospital` ph
                    LEFT JOIN patient_profile_central pc ON ph.ptlink = pc.ptlink
                    WHERE
                        ph.`hospcode` = :sitecode AND
                        (pc.ptlink <> '' OR ph.ptlink = '') AND
                        (".$sqlDeathCen.") AND
                        (".$sqlTypearea.") AND
                        (".$sqlDmCen.") AND
                        NOT (".$sqlReportedDmCen.") AND
                        (".$sqlIcd10Cen.") AND
                        (".$sqlRetinaFundusCen.") 
                    ";
            if($dp){
                $selectKey = self::genSelect();

                $sqlDp = "SELECT
                            $selectKey
                        FROM
                            `patient_profile_hospital` ph
                        INNER JOIN `f_person`
                            ON `ph`.`hospcode` = `f_person`.`HOSPCODE`
                            AND `ph`.`pid` = `f_person`.`PID`
                            AND `ph`.`hospcode` = `f_person`.`sitecode`
                        LEFT JOIN patient_profile_central pc ON ph.ptlink = pc.ptlink
                        WHERE
                            `ph`.`hospcode` = :sitecode AND
                            `f_person`.`sitecode` = :sitecode AND
                            (pc.ptlink <> '' OR ph.ptlink = '') AND
                            (".$sqlDeathCen.") AND
                            (".$sqlTypearea.") AND
                            (".$sqlDmCen.") AND
                            NOT (".$sqlReportedDmCen.") AND
                            (".$sqlIcd10Cen.") AND
                            (".$sqlRetinaFundusCen.")
                    ";

                return [
                    'sql' =>$sqlDp,
                    'param'=>[':sitecode'=>$sitecode],
                    'keyID'=>'ptlink',
                    'total'=>DrFunc::queryScalar($sql, [':sitecode'=>$sitecode]),
                ];
            }
        }else{
            $sql = "SELECT
                        COUNT(*)
                    FROM
                        `patient_profile_hospital` ph
                    WHERE
                        `hospcode` = :sitecode AND
                        (".$sqlDeath.") AND
                        (".$sqlTypearea.") AND
                        (".$sqlDm.") AND
                        NOT (".$sqlReportedDm.") AND
                        (".$sqlIcd10.") AND
                        (".$sqlRetinaFundus.")   
                    ";
            if($dp){
                $selectKey = self::genSelect();

                $sqlDp = "SELECT
                        $selectKey
                    FROM
                        `patient_profile_hospital` ph
                        INNER JOIN `f_person`
                        ON ph.`hospcode` = `f_person`.`HOSPCODE`
                        AND ph.`pid` = `f_person`.`PID`
                        AND ph.`hospcode` = `f_person`.`sitecode`
                    WHERE
                        ph.`hospcode` = :sitecode AND
                        `f_person`.`sitecode` = :sitecode AND
                        (".$sqlDeath.") AND
                        (".$sqlTypearea.") AND
                        (".$sqlDm.") AND
                        NOT (".$sqlReportedDm.") AND
                        (".$sqlIcd10.") AND
                        (".$sqlRetinaFundus.")    
                    ";

                return [
                    'sql' =>$sqlDp,
                    'param'=>[':sitecode'=>$sitecode],
                    'total'=>DrFunc::queryScalar($sql, [':sitecode'=>$sitecode]),
                ];
            }
        }
        return DrFunc::queryScalar($sql, [':sitecode'=>$sitecode]);          
    }
      
    public static function getAbnFundus($sitecode, $sdate, $edate,$idcen,$dp=false){
        # call DrQuery::getAbnFundus
        # Dr: Abnormal Fundus Camera => Retina IN (4) -> (7)
        # - Death -
        # - Typearea -
        # - DM -
        # - ICD10 E10,E11 -
        # - Retina IN (4) -> (7)
        $sqlDeath = self::$sqlDeath;
        $sqlTypearea = self::$sqlTypearea;
        $sqlDm = self::$sqlDm;
        $sqlReportedDm = self::$sqlReportedDm;
        $sqlIcd10 = self::$sqlIcd10;
        $sqlRetinaAbnFundus = self::$sqlRetinaAbnFundus;
        $sqlDeathCen = self::$sqlDeathCen;
        $sqlDmCen = self::$sqlDmCen;
        $sqlReportedDmCen = self::$sqlReportedDmCen;
        $sqlIcd10Cen = self::$sqlIcd10Cen;
        $sqlRetinaAbnFundusCen = self::$sqlRetinaAbnFundusCen;
        
        if($idcen == 'central'){
            $sql = "SELECT
                        COUNT(*)
                    FROM
                        `patient_profile_hospital` ph
                    LEFT JOIN patient_profile_central pc ON ph.ptlink = pc.ptlink
                    WHERE
                        ph.`hospcode` = :sitecode AND
                        (pc.ptlink <> '' OR ph.ptlink = '') AND
                        (".$sqlDeathCen.") AND
                        (".$sqlTypearea.") AND
                        (".$sqlDmCen.") AND
                        NOT (".$sqlReportedDmCen.") AND
                        (".$sqlIcd10Cen.") AND
                        (".$sqlRetinaAbnFundusCen.")   
                    ";
            if($dp){
                $selectKey = self::genSelect();

                $sqlDp = "SELECT
                            $selectKey
                        FROM
                            `patient_profile_hospital` ph
                        INNER JOIN `f_person`
                            ON `ph`.`hospcode` = `f_person`.`HOSPCODE`
                            AND `ph`.`pid` = `f_person`.`PID`
                            AND `ph`.`hospcode` = `f_person`.`sitecode`
                        LEFT JOIN patient_profile_central pc ON ph.ptlink = pc.ptlink
                        WHERE
                            `ph`.`hospcode` = :sitecode AND
                            `f_person`.`sitecode` = :sitecode AND
                            (pc.ptlink <> '' OR ph.ptlink = '') AND
                            (".$sqlDeathCen.") AND
                            (".$sqlTypearea.") AND
                            (".$sqlDmCen.") AND
                            NOT (".$sqlReportedDmCen.") AND
                            (".$sqlIcd10Cen.") AND
                            (".$sqlRetinaAbnFundusCen.")   
                    ";

                return [
                    'sql' =>$sqlDp,
                    'param'=>[':sitecode'=>$sitecode],
                    'keyID'=>'ptlink',
                    'total'=>DrFunc::queryScalar($sql, [':sitecode'=>$sitecode]),
                ];
            }
        }else{
            $sql = "SELECT
                        COUNT(*)
                    FROM
                        `patient_profile_hospital` ph
                    WHERE
                        `hospcode` = :sitecode AND
                        (".$sqlDeath.") AND
                        (".$sqlTypearea.") AND
                        (".$sqlDm.") AND
                        NOT (".$sqlReportedDm.") AND
                        (".$sqlIcd10.") AND
                        (".$sqlRetinaAbnFundus.")   
                    ";
            if($dp){
                $selectKey = self::genSelect();

                $sqlDp = "SELECT
                        $selectKey
                    FROM
                        `patient_profile_hospital` ph
                        INNER JOIN `f_person`
                        ON ph.`hospcode` = `f_person`.`HOSPCODE`
                        AND ph.`pid` = `f_person`.`PID`
                        AND ph.`hospcode` = `f_person`.`sitecode`
                    WHERE
                        ph.`hospcode` = :sitecode AND
                        `f_person`.`sitecode` = :sitecode AND
                        (".$sqlDeath.") AND
                        (".$sqlTypearea.") AND
                        (".$sqlDm.") AND
                        NOT (".$sqlReportedDm.") AND
                        (".$sqlIcd10.") AND
                        (".$sqlRetinaAbnFundus.")    
                    ";

                return [
                    'sql' =>$sqlDp,
                    'param'=>[':sitecode'=>$sitecode],
                    'total'=>DrFunc::queryScalar($sql, [':sitecode'=>$sitecode]),
                ];
            }
        }
        return DrFunc::queryScalar($sql, [':sitecode'=>$sitecode]);          
    }
    
    public static function getTotalAbnormal($sitecode, $sdate, $edate,$idcen,$dp=false){
        # call DrQuery::getAbnFundus
        # Dr: Abnormal Fundus Camera => Retina IN (3,4) -> (8)
        # - Death -
        # - Typearea -
        # - DM -
        # - ICD10 E10,E11 -
        # - Retina IN (3,4) -> (8)
        $sqlDeath = self::$sqlDeath;
        $sqlTypearea = self::$sqlTypearea;
        $sqlDm = self::$sqlDm;
        $sqlReportedDm = self::$sqlReportedDm;
        $sqlIcd10 = self::$sqlIcd10;
        $sqlRetinaAbnTotal = self::$sqlRetinaAbnTotal;
        $sqlDeathCen = self::$sqlDeathCen;
        $sqlDmCen = self::$sqlDmCen;
        $sqlReportedDmCen = self::$sqlReportedDmCen;
        $sqlIcd10Cen = self::$sqlIcd10Cen;
        $sqlRetinaAbnTotalCen = self::$sqlRetinaAbnTotalCen;
        
        if($idcen == 'central'){
            $sql = "SELECT
                        COUNT(*)
                    FROM
                        `patient_profile_hospital` ph
                    LEFT JOIN patient_profile_central pc ON ph.ptlink = pc.ptlink
                    WHERE
                        ph.`hospcode` = :sitecode AND
                        (pc.ptlink <> '' OR ph.ptlink = '') AND
                        (".$sqlDeathCen.") AND
                        (".$sqlTypearea.") AND
                        (".$sqlDmCen.") AND
                        NOT (".$sqlReportedDmCen.") AND
                        (".$sqlIcd10Cen.") AND
                        (".$sqlRetinaAbnTotalCen.")   
                    ";
            if($dp){
                $selectKey = self::genSelect();

                $sqlDp = "SELECT
                            $selectKey
                        FROM
                            `patient_profile_hospital` ph
                        INNER JOIN `f_person`
                            ON `ph`.`hospcode` = `f_person`.`HOSPCODE`
                            AND `ph`.`pid` = `f_person`.`PID`
                            AND `ph`.`hospcode` = `f_person`.`sitecode`
                        LEFT JOIN patient_profile_central pc ON ph.ptlink = pc.ptlink
                        WHERE
                            `ph`.`hospcode` = :sitecode AND
                            `f_person`.`sitecode` = :sitecode AND
                            (pc.ptlink <> '' OR ph.ptlink = '') AND
                            (".$sqlDeathCen.") AND
                            (".$sqlTypearea.") AND
                            (".$sqlDmCen.") AND
                            NOT (".$sqlReportedDmCen.") AND
                            (".$sqlIcd10Cen.") AND
                            (".$sqlRetinaAbnTotalCen.")   
                    ";

                return [
                    'sql' =>$sqlDp,
                    'param'=>[':sitecode'=>$sitecode],
                    'keyID'=>'ptlink',
                    'total'=>DrFunc::queryScalar($sql, [':sitecode'=>$sitecode]),
                ];
            }
        }else{
            $sql = "SELECT
                        COUNT(*)
                    FROM
                        `patient_profile_hospital` ph
                    WHERE
                        `hospcode` = :sitecode AND
                        (".$sqlDeath.") AND
                        (".$sqlTypearea.") AND
                        (".$sqlDm.") AND
                        NOT (".$sqlReportedDm.") AND
                        (".$sqlIcd10.") AND
                        (".$sqlRetinaAbnTotal.")   
                    ";
            if($dp){
                $selectKey = self::genSelect();

                $sqlDp = "SELECT
                        $selectKey
                    FROM
                        `patient_profile_hospital` ph
                        INNER JOIN `f_person`
                        ON ph.`hospcode` = `f_person`.`HOSPCODE`
                        AND ph.`pid` = `f_person`.`PID`
                        AND ph.`hospcode` = `f_person`.`sitecode`
                    WHERE
                        ph.`hospcode` = :sitecode AND
                        `f_person`.`sitecode` = :sitecode AND
                        (".$sqlDeath.") AND
                        (".$sqlTypearea.") AND
                        (".$sqlDm.") AND
                        NOT (".$sqlReportedDm.") AND
                        (".$sqlIcd10.") AND
                        (".$sqlRetinaAbnTotal.")    
                    ";

                return [
                    'sql' =>$sqlDp,
                    'param'=>[':sitecode'=>$sitecode],
                    'total'=>DrFunc::queryScalar($sql, [':sitecode'=>$sitecode]),
                ];
            }
        }
        return DrFunc::queryScalar($sql, [':sitecode'=>$sitecode]);          
    }
    
    public static function getNoTreatmentIndicated($sitecode, $sdate, $edate,$idcen,$dp=false){
        # call DrQuery::getNoTreatmentIndicated
        # Dr: NO Treatment Indicated  -> (9)
        # - Death -
        # - Typearea -
        # - DM -
        # - ICD10 E10,E11 -
        # - NO Treatment Indicated -> (9)
        $sqlDeath = self::$sqlDeath;
        $sqlTypearea = self::$sqlTypearea;
        $sqlDm = self::$sqlDm;
        $sqlReportedDm = self::$sqlReportedDm;
        $sqlIcd10 = self::$sqlIcd10;
        $sqlRetinaAbnTotal = self::$sqlRetinaAbnTotal;
        $sqlNotTreatmentindicated = self::$sqlNotTreatmentindicated;
        $sqlDeathCen = self::$sqlDeathCen;
        $sqlDmCen = self::$sqlDmCen;
        $sqlReportedDmCen = self::$sqlReportedDmCen;
        $sqlIcd10Cen = self::$sqlIcd10Cen;
        $sqlRetinaAbnTotalCen = self::$sqlRetinaAbnTotalCen;
        $sqlNotTreatmentindicatedCen = self::$sqlNotTreatmentindicatedCen;
        
        if($idcen == 'central'){
            $sql = "SELECT
                        COUNT(*)
                    FROM
                        `patient_profile_hospital` ph
                    LEFT JOIN patient_profile_central pc ON ph.ptlink = pc.ptlink
                    WHERE
                        ph.`hospcode` = :sitecode AND
                        (pc.ptlink <> '' OR ph.ptlink = '') AND
                        (".$sqlDeathCen.") AND
                        (".$sqlTypearea.") AND
                        (".$sqlDmCen.") AND
                        NOT (".$sqlReportedDmCen.") AND
                        (".$sqlIcd10Cen.") AND
                        (".$sqlRetinaAbnTotalCen.") AND
                        (".$sqlNotTreatmentindicatedCen.") 
                        
                    ";
            if($dp){
                $selectKey = self::genSelect();

                $sqlDp = "SELECT
                            $selectKey
                        FROM
                            `patient_profile_hospital` ph
                        INNER JOIN `f_person`
                            ON `ph`.`hospcode` = `f_person`.`HOSPCODE`
                            AND `ph`.`pid` = `f_person`.`PID`
                            AND `ph`.`hospcode` = `f_person`.`sitecode`
                        LEFT JOIN patient_profile_central pc ON ph.ptlink = pc.ptlink
                        WHERE
                            `ph`.`hospcode` = :sitecode AND
                            `f_person`.`sitecode` = :sitecode AND
                            (pc.ptlink <> '' OR ph.ptlink = '') AND
                            (".$sqlDeathCen.") AND
                            (".$sqlTypearea.") AND
                            (".$sqlDmCen.") AND
                            NOT (".$sqlReportedDmCen.") AND
                            (".$sqlIcd10Cen.") AND
                            (".$sqlRetinaAbnTotalCen.") AND
                            (".$sqlNotTreatmentindicatedCen.") 
                    ";

                return [
                    'sql' =>$sqlDp,
                    'param'=>[':sitecode'=>$sitecode],
                    'keyID'=>'ptlink',
                    'total'=>DrFunc::queryScalar($sql, [':sitecode'=>$sitecode]),
                ];
            }
        }else{
            $sql = "SELECT
                        COUNT(*)
                    FROM
                        `patient_profile_hospital` ph
                    WHERE
                        `hospcode` = :sitecode AND
                        (".$sqlDeath.") AND
                        (".$sqlTypearea.") AND
                        (".$sqlDm.") AND
                        NOT (".$sqlReportedDm.") AND
                        (".$sqlIcd10.") AND
                        (".$sqlRetinaAbnTotal.") AND
                        (".$sqlNotTreatmentindicated.")   
                    ";
            if($dp){
                $selectKey = self::genSelect();

                $sqlDp = "SELECT
                        $selectKey
                    FROM
                        `patient_profile_hospital` ph
                        INNER JOIN `f_person`
                        ON ph.`hospcode` = `f_person`.`HOSPCODE`
                        AND ph.`pid` = `f_person`.`PID`
                        AND ph.`hospcode` = `f_person`.`sitecode`
                    WHERE
                        ph.`hospcode` = :sitecode AND
                        `f_person`.`sitecode` = :sitecode AND
                        (".$sqlDeath.") AND
                        (".$sqlTypearea.") AND
                        (".$sqlDm.") AND
                        NOT (".$sqlReportedDm.") AND
                        (".$sqlRetinaAbnTotal.") AND
                        (".$sqlNotTreatmentindicated.")    
                    ";

                return [
                    'sql' =>$sqlDp,
                    'param'=>[':sitecode'=>$sitecode],
                    'total'=>DrFunc::queryScalar($sql, [':sitecode'=>$sitecode]),
                ];
            }
        }
        return DrFunc::queryScalar($sql, [':sitecode'=>$sitecode]);          
    }
    
    public static function getTreatmentIndicated($sitecode, $sdate, $edate,$idcen,$dp=false){
        # call DrQuery::getTreatmentIndicated
        # Dr: Treatment Indicated  -> (10)
        # - Death -
        # - Typearea -
        # - DM -
        # - ICD10 E10,E11 -
        # - Treatment Indicated -> (10)
        $sqlDeath = self::$sqlDeath;
        $sqlTypearea = self::$sqlTypearea;
        $sqlDm = self::$sqlDm;
        $sqlReportedDm = self::$sqlReportedDm;
        $sqlIcd10 = self::$sqlIcd10;
        $sqlRetinaAbnTotal = self::$sqlRetinaAbnTotal;
        $sqlTreatmentindicated = self::$sqlTreatmentindicated;
        $sqlDeathCen = self::$sqlDeathCen;
        $sqlDmCen = self::$sqlDmCen;
        $sqlReportedDmCen = self::$sqlReportedDmCen;
        $sqlIcd10Cen = self::$sqlIcd10Cen;
        $sqlRetinaAbnTotalCen = self::$sqlRetinaAbnTotalCen;
        $sqlTreatmentindicatedCen = self::$sqlTreatmentindicatedCen;
        
        
        if($idcen == 'central'){
            $sql = "SELECT
                        COUNT(*)
                    FROM
                        `patient_profile_hospital` ph
                    LEFT JOIN patient_profile_central pc ON ph.ptlink = pc.ptlink
                    WHERE
                        ph.`hospcode` = :sitecode AND
                        (pc.ptlink <> '' OR ph.ptlink = '') AND
                        (".$sqlDeathCen.") AND
                        (".$sqlTypearea.") AND
                        (".$sqlDmCen.") AND
                        NOT (".$sqlReportedDmCen.") AND
                        (".$sqlIcd10Cen.") AND
                        (".$sqlRetinaAbnTotalCen.") AND
                        (".$sqlTreatmentindicatedCen.")   
                    ";
            if($dp){
                $selectKey = self::genSelect();

                $sqlDp = "SELECT
                            $selectKey
                        FROM
                            `patient_profile_hospital` ph
                        INNER JOIN `f_person`
                            ON `ph`.`hospcode` = `f_person`.`HOSPCODE`
                            AND `ph`.`pid` = `f_person`.`PID`
                            AND `ph`.`hospcode` = `f_person`.`sitecode`
                        LEFT JOIN patient_profile_central pc ON ph.ptlink = pc.ptlink
                        WHERE
                            `ph`.`hospcode` = :sitecode AND
                            `f_person`.`sitecode` = :sitecode AND
                            (pc.ptlink <> '' OR ph.ptlink = '') AND
                            (".$sqlDeathCen.") AND
                            (".$sqlTypearea.") AND
                            (".$sqlDmCen.") AND
                            NOT (".$sqlReportedDmCen.") AND
                            (".$sqlIcd10Cen.") AND
                            (".$sqlRetinaAbnTotalCen.") AND
                            (".$sqlTreatmentindicatedCen.")    
                    ";

                return [
                    'sql' =>$sqlDp,
                    'param'=>[':sitecode'=>$sitecode],
                    'keyID'=>'ptlink',
                    'total'=>DrFunc::queryScalar($sql, [':sitecode'=>$sitecode]),
                ];
            }
        }else{
            $sql = "SELECT
                    COUNT(*)
                FROM
                    `patient_profile_hospital` ph
                WHERE
                    `hospcode` = :sitecode AND
                    (".$sqlDeath.") AND
                    (".$sqlTypearea.") AND
                    (".$sqlDm.") AND
                    NOT (".$sqlReportedDm.") AND
                    (".$sqlIcd10.") AND
                    (".$sqlRetinaAbnTotal.") AND
                    (".$sqlTreatmentindicated.")   
                ";
            if($dp){
                $selectKey = self::genSelect();

                $sqlDp = "SELECT
                        $selectKey
                    FROM
                        `patient_profile_hospital` ph
                        INNER JOIN `f_person`
                        ON ph.`hospcode` = `f_person`.`HOSPCODE`
                        AND ph.`pid` = `f_person`.`PID`
                        AND ph.`hospcode` = `f_person`.`sitecode`
                    WHERE
                        ph.`hospcode` = :sitecode AND
                        `f_person`.`sitecode` = :sitecode AND
                        (".$sqlDeath.") AND
                        (".$sqlTypearea.") AND
                        (".$sqlDm.") AND
                        NOT (".$sqlReportedDm.") AND
                        (".$sqlIcd10.") AND
                        (".$sqlRetinaAbnTotal.")AND
                        (".$sqlTreatmentindicated.")    
                    ";

                return [
                    'sql' =>$sqlDp,
                    'param'=>[':sitecode'=>$sitecode],
                    'total'=>DrFunc::queryScalar($sql, [':sitecode'=>$sitecode]),
                ];
            }
        }
        
        return DrFunc::queryScalar($sql, [':sitecode'=>$sitecode]);          
    } 
    //Close Query diagram
    
    
}