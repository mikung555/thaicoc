<?php

namespace backend\modules\dr\classes;

use Yii;

class DrFunc {
    public static function getDb() {

        if (isset(\Yii::$app->session['dynamic_connection']) && !empty(\Yii::$app->session['dynamic_connection'])) {
            $sitecode = \Yii::$app->user->identity->userProfile->sitecode;
            //unset(Yii::$app->session['dynamic_connection']);
            if (\Yii::$app->session['dynamic_connection']['sitecode'] != $sitecode) {
                $data_con = DrQuery::getDbConfig($sitecode);

                $obj = [
                    'sitecode' => $sitecode,
                    'db' => $data_con
                ];
                \Yii::$app->session['dynamic_connection'] = $obj;
            }
        } else {
            $sitecode = \Yii::$app->user->identity->userProfile->sitecode;
            $data_con = DrQuery::getDbConfig($sitecode);

            $obj = [
                'sitecode' => $sitecode,
                'db' => $data_con
            ];
            \Yii::$app->session['dynamic_connection'] = $obj;
        }

        $data_config = \Yii::$app->session['dynamic_connection']['db'];

        //\appxq\sdii\utils\VarDumper::dump(Yii::$app->session['dynamic_connection']);

        if (isset($data_config) && !empty($data_config)) {
            $dsn = "mysql:host={$data_config['server']};port={$data_config['port']};dbname={$data_config['db']}";

            $db = new \yii\db\Connection([
                'dsn' => $dsn,
                'username' => $data_config['user'],
                'password' => $data_config['passwd'],
                'charset' => 'utf8',
                    // 'enableSchemaCache' => true,
                    // 'schemaCacheDuration' => 3600,
            ]);
            //$db->open();

            return $db;
        }

        return FALSE;
    }

    public static function rawSql($sql, $param = []) {
        $db = self::getDb();
        if ($db) {
            return $db->createCommand($sql, $param)->rawSql;
        }
        return FALSE;
    }

    public static function execute($sql, $param = []) {
        $db = self::getDb();
        if ($db) {
            return $db->createCommand($sql, $param)->execute();
        }
        return FALSE;
    }

    public static function queryAll($sql, $param = []) {
        $db = self::getDb();
        //\appxq\sdii\utils\VarDumper::dump($db);
        if ($db) {
            return $db->createCommand($sql, $param)->queryAll();
        }
        return FALSE;
    }

    public static function queryOne($sql, $param = []) {
        $db = self::getDb();
        if ($db) {
            return $db->createCommand($sql, $param)->queryOne();
        }
        return FALSE;
    }

    public static function queryScalar($sql, $param = []) {
        $db = self::getDb();
        if ($db) {
            return $db->createCommand($sql, $param)->queryScalar();
        }
        return FALSE;
    }

    public static function convertColumn($colArr) {
        $colArrReturn = [];
        foreach ($colArr as $key => $value) {
            $colArrReturn[$key] = strtolower($value);
        }
        return $colArrReturn;
    }
    
    public static function getPercent($amount,$total){
        //$amount = ตัวตั้ง , $total = ตัวหาร
        $percen = ($amount/$total)*100;
        if(is_nan($percen) == 1){
            $percen = 0 ;
            return number_format($percen,1) ;
        } else {
            return number_format($percen,1) ;
        }
        //return is_nan($percen);
        
  
    }
    
    // Open Query Diagram
    public static function getDrDiagram($sitecode, $sdate, $edate,$idcen) {
//        $paramsdiagram = [
//            'sitecode' => $sitecode,
//            'sdate' => $sdate,
//            'edate' => $edate,
//        ];

        //====== All =====
        try {
            $count = DrQuery::getAll($sitecode, $sdate, $edate,$idcen);
        } catch (\yii\db\Exception $e) {
            $count = 0;
        }
            $obj[] = [
                'id' => 0,
                'label' => "All (".number_format($count).")",
                'value' => $count,
            ];
        //====== Box 1 =====
        try {
            $count = DrQuery::getTotalPop($sitecode, $sdate, $edate,$idcen);
            
        } catch (\yii\db\Exception $e) {
            $count = 0;
        }
            $obj[] = [
                'id' => 1,
                'label' => "1) Total population (".number_format($count).")",
                'value' => $count,
            ];
          
            
        //====== Box 2 =====
        try {
            $count = DrQuery::getDm($sitecode, $sdate, $edate,$idcen);
        } catch (\yii\db\Exception $e) {
            $count = 0;
        }
            $obj[] = [
                'id' => 2,
                'label' => "2) DM (".number_format($count).")",
                'value' => $count,
            ];

        //====== Box 3 =====
        try {
            $count = DrQuery::getRetina($sitecode, $sdate, $edate,$idcen);
        } catch (\yii\db\Exception $e) {
            $count = 0;
        }
            $obj[] = [
                'id' => 3,
                'label' => "3) RETINA (".number_format($count).")",
                'value' => $count,
            ]; 
            
        //====== Box 4 =====
        try {
            $count = DrQuery::getOphalmoscope($sitecode, $sdate, $edate,$idcen);
        } catch (\yii\db\Exception $e) {
            $count = 0;
        }
            $obj[] = [
                'id' => 4,
                'label' => "4) OPTHALMOSCOPE (".number_format($count).")",
                'value' => $count,
            ]; 
            
        //====== Box 5 =====
        try {
            $count = DrQuery::getAbnormalOphalmoscope($sitecode, $sdate, $edate,$idcen);
        } catch (\yii\db\Exception $e) {
            $count = 0;
        }
            $obj[] = [
                'id' => 5,
                'label' => "5) ABNORMAL OPTHALMOSCOPE (".number_format($count).")",
                'value' => $count,
            ];
            
        //====== Box 6 =====
        try {
            $count = DrQuery::getFundus($sitecode, $sdate, $edate,$idcen);
        } catch (\yii\db\Exception $e) {
            $count = 0;
        }
            $obj[] = [
                'id' => 6,
                'label' => "6) FUNDUS CAMERA (".number_format($count).")",
                'value' => $count,
            ]; 
        
        //====== Box 7 =====
        try {
            $count = DrQuery::getAbnFundus($sitecode, $sdate, $edate,$idcen);
        } catch (\yii\db\Exception $e) {
            $count = 0;
        }
            $obj[] = [
                'id' => 7,
                'label' => "7) ABNORMAL FUNDUS CAMERA (".number_format($count).")",
                'value' => $count,
            ]; 
        
        //====== Box 8 =====
        try {
            $count = DrQuery::getTotalAbnormal($sitecode, $sdate, $edate,$idcen);
        } catch (\yii\db\Exception $e) {
            $count = 0;
        }
            $obj[] = [
                'id' => 8,
                'label' => "8) Total Abnormal (".number_format($count).")",
                'value' => $count,
            ];
            
//        //====== Box 9 =====
//        try {
//            $count = DrQuery::getNoTreatmentIndicated($sitecode, $sdate, $edate,$idcen);
//        } catch (\yii\db\Exception $e) {
//            $count = 0;
//        }
//            $obj[] = [
//                'id' => 9,
//                'label' => "9) NO Treatment Indicated (".number_format($count).")",
//                'value' => $count,
//            ];
//            
//        //====== Box 10 =====
//        try {
//            $count = DrQuery::getTreatmentIndicated($sitecode, $sdate, $edate,$idcen);
//        } catch (\yii\db\Exception $e) {
//            $count = 0;
//        }
//            $obj[] = [
//                'id' => 10,
//                'label' => "10) Treatment Indicated ($count)",
//                'value' => $count,
//            ];
            
        return $obj;
           
    } // close function getDrdiagram
    
    

}