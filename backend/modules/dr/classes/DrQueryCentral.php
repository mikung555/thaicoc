<?php

namespace backend\modules\dr\classes;

use Yii;

class DrQueryCentral {
    
    public static function getDiagramCentral(){
        $sql = "SELECT
                        COUNT(*)
                    FROM
                        `patient_profile_hospital` ph
                    WHERE
                        ph.`hospcode` = :sitecode
                    ";
            if($dp){
                $selectKey = self::genSelect();

                $sqlDp = "SELECT
                        $selectKey
                    FROM
                        `patient_profile_hospital` ph
                        INNER JOIN `f_person`
                        ON ph.`hospcode` = f_person.`HOSPCODE`
                        AND ph.`pid` = f_person.`PID`
                    WHERE
                        ph.`hospcode` = :sitecode AND
                        f_person.`sitecode` = :sitecode
                    ";

                return [
                    'sql' =>$sqlDp,
                    'param'=>[':sitecode'=>$sitecode],
                    'keyID'=>'ptlink',
                    'total'=>DrFunc::queryScalar($sql, [':sitecode'=>$sitecode]),
                ];
            }
            return DrFunc::queryScalar($sql, [':sitecode'=>$sitecode]);  
    }

}