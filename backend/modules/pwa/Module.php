<?php

namespace backend\modules\pwa;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\modules\pwa\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
