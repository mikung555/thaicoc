<?php

use yii\helpers\Url;
?>


<div class="pwa-default-index">
     
    <h1><?= $this->context->action->uniqueId ?></h1>
    <p>
        This is the view content for action "<?= $this->context->action->id ?>".
        The action belongs to the controller "<?= get_class($this->context) ?>"
        in the "<?= $this->context->module->id ?>" module.
    </p>
    <p>
        You may customize this page by editing the following file:<br>
        <code><?= __FILE__ ?></code>
    </p>
   <div id="fishImage">
    
</div>
</div>


<?php
$this->registerJs("

       $.ajax({
  method: 'get',
  url: 'http://backend.tccfix.dev/interactiveimage'
})
  .done(function( msg ) {
      $('#fishImage').html(msg);
  });
" );
?>