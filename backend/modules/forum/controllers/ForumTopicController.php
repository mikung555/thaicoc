<?php

namespace backend\modules\forum\controllers;

use Yii;
use backend\modules\forum\models\Forum;
use backend\modules\forum\models\ForumSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\helpers\BaseFileHelper;
use yii\helpers\Url;

/**
 * ForumTopicController implements the CRUD actions for Forum model.
 */
class ForumTopicController extends Controller {

    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Forum models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new ForumSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Forum model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Forum model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new Forum();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Forum model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Forum model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Forum model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Forum the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Forum::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionHelpDesk() {
        return $this->renderAjax('_form_help_desk', [
                    'model' => $model,
                    'helpurl' => Yii::$app->request->referrer,
        ]);
    }

    public function actionAddHelp($forum_type_id, $forum_title, $forum_detail, $forum_image, $url_report, $bookmark, $hits_read, $hits_reply, $status, $created_by, $created_at, $updated_at) {
        $sql = "INSERT INTO `forum` "
                . "(`id`, `forum_type_id`, `forum_title`, `forum_detail`, `forum_image`, `url_report`, `bookmark`, `hits_read`, `hits_reply`, `status`, `created_by`, `created_at`, `updated_at`)"
                . " VALUES "
                . "(NULL,'$forum_type_id','$forum_title','$forum_detail','$forum_image','$url_report','$bookmark','$hits_read','$hits_reply','$status','$created_by','$created_at','$updated_at')";
        Yii::$app->db->createCommand($sql)->execute();
    }

//function actionAddHelp

    public function actionGetTopicUser($page = null) {
        if ($page == null) {
            $page = 1;
        }
        $user_id = Yii::$app->user->identity->id;
        $sqlcount = "SELECT count(*) as total  FROM `forum` WHERE `created_by` LIKE '%$user_id%'   ORDER BY `id`";
        $modelcount = Yii::$app->db->createCommand($sqlcount)->queryAll();

        $totaldata = $modelcount[0][total];
        $totalpage = ceil($totaldata / 10);

        $start_page = ($page - 1) * 10;
        $end_page = 10;


        $searchModel = new ForumSearch();
        $sql = "SELECT *  FROM `forum` WHERE `created_by` LIKE '%$user_id%'   ORDER BY `id` DESC LIMIT $start_page,$end_page ";
        $model = Yii::$app->db->createCommand($sql)->queryAll();

        $content_topic_user_head = <<< HTML
<div class="row">
  <div class="col-md-12">
<h3 class="nodeTitle">กระทู้ปัญหา</h3>
</div>
</div>
      <table class="table">
          <thead>
            <tr>
              <th>No.</th>
              <th></th>
              <th>Title</th>
              <th>Started</th>
              <th>By</th>
              <th>Latest</th>

            </tr>
          </thead>
          <tbody>
HTML;
        $total_count = count($model);
        if ($total_count > 0) {

            $num = 1;
            foreach ($model as $value) {
                $topic_id = $value[id];
                $model2 = \backend\modules\forum\models\ForumReply::find()->where('forum_id=:id', [':id' => $topic_id])->orderBy(['id' => SORT_DESC])->one();

                $topic_detail = $value[forum_detail];
                $user = \common\models\UserProfile::findOne($value[created_by]);

                $user_name = $user->firstname;
                $created_at = $value[created_at];
                $total_count2 = count($model2);
                if ($total_count2 > 0) {
                    $user2 = \common\models\UserProfile::findOne($model2->created_by);
                    $user_name2 = $user2->firstname;
                    $created_at2 = $model2->created_at;
                } else {
                    $user_name2 = "ยังไม่มีคนตอบ";
                    $created_at2 = "";
                }

                $forum_detail_sub = substr($topic_detail, 0, 150);
                $content_topic_user_medium .= <<< HTML
            <tr>
              <td class="no">$num</td>
              <td class="icon"><p class="glyphicon glyphicon-comment"</p></td>
              <td class="description"><a href="$domain/forum/forum-topic/view-detail?id=$topic_id">$forum_detail_sub</a><br><span class="discussion"></span></td>
              <td class="started"><span class="discussion">Date: $created_at </td>
              <td class="repliles">$user_name</td>
              <td class="latest">$user_name2 <br> <span class="discussion"> Dated: $created_at2 </span> </td>
            </tr>
HTML;
                $num++;
            }
            $content_topic_user_foot = <<< HTML
</tbody>
</table>
HTML;
            if ($page == 1) {
                $nav_start = $page;
            } else {
                $nav_start = $page - 1;
            }
            if ($page == $totalpage) {
                $nav_end = $page;
            } elseif (($totalpage - $page) > 1 && ($totalpage - $page) < 3) {
                $nav_end = $page + 2;
            } elseif ($totalpage == $page) {
                $nav_end = $page;
            } else {
                $nav_end = $page + 1;
            }
            for ($i = $nav_start; $i <= $nav_end; $i++) {
                if ($i == $page) {
                    $pageshow .= '<li class="active"><a id="pagenav" href="#" page="' . $i . '">' . $i . '</a></li>';
                } else {
                    $pageshow .= '<li><a id="pagenav" href="#" page="' . $i . '">' . $i . '</a></li>';
                }
            }
            $content_topic_user_page = <<< HTML
<nav aria-label="Page navigation">
  <ul class="pagination">
    <li>
      <a href="#" aria-label="Previous">
        <span aria-hidden="true">&laquo;</span>
      </a>
    </li>
    $pageshow
    <li>
      <a href="#" aria-label="Next">
        <span aria-hidden="true">&raquo;</span>
      </a>
    </li>
  </ul>
</nav>
HTML;
            $content_data_topic_user = $content_topic_user_head . $content_topic_user_medium . $content_topic_user_foot . $content_topic_user_page;
            echo $content_data_topic_user;
        } else {
            echo "คุณยังไม่เคยส่งขอความช่วยเหลือ";
        }
    }

//function actionGetTopicUser

    public function actionAddFaq($forum_id) {
        $sql = "UPDATE `forum` SET `bookmark` = '1' WHERE  `id` ='$forum_id'";
        Yii::$app->db->createCommand($sql)->execute();
        exit();
    }

    public function actionRemoveFaq($forum_id) {
        $sql = "UPDATE `forum` SET `bookmark` = '0' WHERE  `id` ='$forum_id'";
        Yii::$app->db->createCommand($sql)->execute();
        exit();
    }

    public function actionGetFaq() {
        $searchModel = new ForumSearch();

        $sql = "SELECT *  FROM `forum` WHERE `bookmark` = '1'   ORDER BY `id` DESC LIMIT 0,10 ";
        $model = Yii::$app->db->createCommand($sql)->queryAll();

        $content_topic_user_head = <<< HTML
<div class="row">
  <div class="col-md-12">
<h3 class="nodeTitle">ปัญหาที่พบบ่อย</h3>
</div>
</div>
      <table class="table">
          <thead>
            <tr>
              <th>No.</th>
              <th></th>
              <th>Title</th>
              <th>Started</th>
              <th>By</th>
              <th>Latest</th>

            </tr>
          </thead>
          <tbody>
HTML;
        $total_count = count($model);
        if ($total_count > 0) {

            $num = 1;
            foreach ($model as $value) {
                $topic_id = $value[id];
                $model2 = \backend\modules\forum\models\ForumReply::find()->where('forum_id=:id', [':id' => $topic_id])->orderBy(['id' => SORT_DESC])->one();

                $topic_detail = $value[forum_detail];
                $user = \common\models\UserProfile::findOne($value[created_by]);

                $user_name = $user->firstname;
                $created_at = $value[created_at];
                $total_count2 = count($model2);
                if ($total_count2 > 0) {
                    $user2 = \common\models\UserProfile::findOne($model2->created_by);
                    $user_name2 = $user2->firstname;
                    $created_at2 = $model2->created_at;
                } else {
                    $user_name2 = "ยังไม่มีคนตอบ";
                    $created_at2 = "";
                }

                $forum_detail_sub = substr($topic_detail, 0, 150);
                $content_topic_user_medium .= <<< HTML
            <tr>
              <td class="no">$num</td>
              <td class="icon"><p class="glyphicon glyphicon-comment"</p></td>
              <td class="description"><a href="$domain/forum/forum-topic/view-detail?id=$topic_id">$forum_detail_sub</a><br><span class="discussion"></span></td>
              <td class="started"><span class="discussion">Date: $created_at </td>
              <td class="repliles">$user_name</td>
              <td class="latest">$user_name2 <br> <span class="discussion"> Dated: $created_at2 </span> </td>
            </tr>
HTML;
                $num++;
            }
            $content_topic_user_foot = <<< HTML
</tbody>
</table>
HTML;
            $content_data_topic_user = $content_topic_user_head . $content_topic_user_medium . $content_topic_user_foot;
            echo $content_data_topic_user;
        } else {
            echo "No topic";
        }
    }

    public function actionViewDetail($id) {
        $model2 = \backend\modules\forum\models\ForumReply::find()->where('forum_id=:id', [':id' => $id])->all();


        $user_id = Yii::$app->user->identity->id;


        $searchModel = new ForumSearch();

        $sql = "SELECT count(*) as bookmarks  FROM `rbac_auth_assignment` WHERE `user_id` ='$user_id' and `item_name` LIKE '%administrator%' ";
        $model3 = Yii::$app->db->createCommand($sql)->queryAll();
        return $this->render('view_detail', [
                    'model' => $this->findModel($id),
                    'model2' => $model2,
                    'bookmarks' => $model3,
        ]);
    }

    public function actionGetFormForum() {
        $forum_form = <<< HTML
  <div class="container">

  <form id="testform" class="form-horizontal">
  <div class="form-group">
  <label for="inputtitle" class="col-sm-1 control-label">หัวข้อ :</label>
  <div class="col-sm-11">
    <input type="text" id="txt_forum_title" class="form-control"  placeholder="หัวข้อเรื่องที่สอบถาม">
  </div>
  </div>
  <div class="form-group">
  <label for="inputdescrition" class="col-sm-1 control-label">รายอะเลียด :</label>
  <div class="col-sm-11">
  <textarea id="txt_forum_detail"  class="form-control" style="width:40%" rows="4" cols="50"> </textarea>
  </div>
  </div>
  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
      <label for="exampleInputFile">Upload</label>
    <input type="file" id="file" name="file">
    <p class="help-block">อัพโหลดรูปภาพประกอบ</p>
    </div>
    </div>

  <div class="form-group">
  <div class="col-sm-offset-2 col-sm-10">
    <button id="btn-add-forum" type="button" class="btn btn-primary">Submit</button>
    <button type="button" class="btn btn-warning">Cancel</button>
  </div>
  </div>
  </form>





  <!-- End Container-->
  </div>
  <!-- /.container -->
HTML;
        echo $forum_form;
        exit();
    }

    public function actionAddTopic() {

        $forum_type_id = "0";
        $forum_title = $_POST[forum_title];
        $forum_detail = $_POST[forum_detail];


        $url_report = Yii::$app->request->referrer;
        $bookmark = "0";
        $hits_read = "0";
        $hits_reply = "0";
        $status = "0";
        $created_by = Yii::$app->user->identity->id;
        $created_at = date("Y-m-d H:i:s");
        $updated_at = date("Y-m-d H:i:s");
        $forum_image = $_POST[forum_image];



        $this->actionAddHelp($forum_type_id, $forum_title, $forum_detail, $forum_image, $url_report, $bookmark, $hits_read, $hits_reply, $status, $created_by, $created_at, $updated_at);
        $this->PackNotification();
    }

    public function actionTest() {
        $domain = Url::home();
        echo $domain;
    }

    public function actionUpload() {
        $created_at = strtotime("now");
//        $fileName = $_FILES['file']['name'];
//        $fileType = $_FILES['file']['type'];
//        $file = $_FILES["file"]["tmp_name"];
//        $file_exp = explode('.', $fileName);
        // move_uploaded_file($_FILES['file']['tmp_name'], './img/upload/'.$created_at.'.'.$file_exp[1]);

        $file = \yii\web\UploadedFile::getInstanceByName('file');
        if (isset($file)) {
            $filePath = Yii::$app->basePath . '/../backend/web/img/upload/' . $created_at . '.' . $file->extension;
            if ($file->saveAs($filePath)) {

                if (!in_array(strtolower($file->extension), ['xlsx', 'xlx', 'doc', 'docx', 'pdf', 'gif', 'jpg', 'jpeg', 'png']))
                    return false;
                if (isset($file->name)) {
                    echo 'img/upload/' . $created_at . '.' . $file->extension;
                } else {
                    echo "error";
                }
            } else {
                echo "error";
            }
        }
    }

    public function PackNotification() {
        $user = \backend\modules\notification\controllers\NotificationController::CheckAdminNotification();
        $model_topic = Forum::find()->where('created_by=:created_by', [':created_by' => $user[from]])->orderBy(['id' => SORT_DESC])->one();
        $ref_dataid = $model_topic->id;
        $message = $model_topic->forum_title;
        //$message=$topic_title
        $status = 0;
        $read = 0;
        $link_to = $domain . "/forum/forum-topic/view-detail?id=" . $ref_dataid;
        $link[module] = "forum";
        $link[controller] = "WbTopicController";
        $link[action] = "view";
        $options = "";
        $sent_notification = \backend\modules\notification\controllers\NotificationController::SendNotify($ref_dataid, $message, $user, $status, $read, $link, $link_to, $options);
    }

}

//class
