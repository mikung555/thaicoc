<?php

namespace backend\modules\forum\controllers;

use Yii;
use backend\modules\forum\models\ForumReply;
use backend\modules\forum\models\ForumReplySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\bootstrap\Html;
use yii\helpers\Url;
$domain=Url::home();
/**
 * ForumReplyController implements the CRUD actions for ForumReply model.
 */
class ForumReplyController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all ForumReply models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ForumReplySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ForumReply model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ForumReply model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ForumReply();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing ForumReply model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing ForumReply model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ForumReply model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ForumReply the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ForumReply::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionAddReply($forum_id,$created_by,$reply_comment,$reply_image)
    {
 $updated_at=date("Y-m-d H:i:s");
      $created_at=date("Y-m-d H:i:s");
      $sql="INSERT INTO `forum_reply` "
              . "(`id`, `forum_id`, `reply_image`,`reply_comment`, `created_by`, `created_at`, `updated_at`)"
              . " VALUES "
              . "(NULL, '$forum_id', '$reply_image', '$reply_comment', '$created_by', '$created_at', '$updated_at')";
   Yii::$app->db->createCommand($sql)->execute();
   $this->PackDataSentNotification($forum_id);
    }

    public function PackDataSentNotification($forum_id)
    {
    $user_id = Yii::$app->user->identity->userProfile->user_id;
    $model_topic = \backend\modules\forum\models\Forum::find()->where('id=:id', [':id'=>$forum_id])->one();
    $model_reply =ForumReply::find()->select('created_by')->distinct()->where('forum_id=:id', [':id'=>$forum_id])->all();
    $model_reply2 =ForumReply::find()->where('forum_id=:id', [':id'=>$forum_id])->orderBy(['id'=>SORT_DESC])->one();


    $num=0;
    $total=count($model_reply);
    $checkreply=$model_topic->created_by;
    if($total==1 && $checkreply!=$user_id){
    $user[to]=$model_topic->created_by;
    }else{
      foreach ($model_reply as $value) {
        if($model_reply[$num][created_by]!=$user_id){
          $user_to.=$model_reply[$num][created_by].",";

        }
        $num++;
      }
      $user[to]=substr($user_to,0,-1);
    }

    $user[from]=$model_reply2->created_by;
    $ref_dataid=$forum_id;
    $message=$model_reply2->reply_comment;
    $status=0;
    $read=0;
    $link_to=$domain."/forum/forum-topic/view-detail?id=".$forum_id;
    $link[module]="forum";
    $link[controller]="ForumReplyTopicController";
    $link[action]="View";
    $options="";

    $sent_notification = \backend\modules\notification\controllers\NotificationController::SendNotify($ref_dataid,$message,$user,$status,$read,$link,$link_to,$options);
    }

    public function actionTest($id)
    {
      $this->PackDataSentNotification($id);
    }





}//class
