<?php

namespace backend\modules\forum\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\forum\models\Forum;

/**
 * ForumSearch represents the model behind the search form about `backend\modules\forum\models\Forum`.
 */
class ForumSearch extends Forum
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'forum_type_id', 'bookmark', 'hits_read', 'hits_reply', 'status', 'created_by'], 'integer'],
            [['forum_title', 'forum_detail', 'forum_image', 'url_report', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Forum::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'forum_type_id' => $this->forum_type_id,
            'bookmark' => $this->bookmark,
            'hits_read' => $this->hits_read,
            'hits_reply' => $this->hits_reply,
            'status' => $this->status,
            'created_by' => $this->created_by,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'forum_title', $this->forum_title])
            ->andFilterWhere(['like', 'forum_detail', $this->forum_detail])
            ->andFilterWhere(['like', 'forum_image', $this->forum_image])
            ->andFilterWhere(['like', 'url_report', $this->url_report]);

        return $dataProvider;
    }
}
