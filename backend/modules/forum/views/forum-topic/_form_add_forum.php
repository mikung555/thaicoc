<div class="container">

<form class="form-horizontal">
<div class="form-group">
<label for="inputtitle" class="col-sm-1 control-label">หัวข้อ :</label>
<div class="col-sm-11">
  <input type="text" class="form-control" id="inputtext" placeholder="Post title">
</div>
</div>
<div class="form-group">
<label for="inputdescrition" class="col-sm-1 control-label">รายอะเลียด :</label>
<div class="col-sm-11">
<textarea class="form-control" rows="6" placeholder="ใส่รายละเอียดข้อความ"> </textarea>
</div>
</div>
<div class="form-group">
<div class="col-sm-offset-2 col-sm-10">
  <label for="exampleInputFile">Upload</label>
<input type="file" id="exampleInputFile">
<p class="help-block">อัพโหลดโฟล์ภาพประกอบ</p>
</div>
</div>
<div class="form-group">
<div class="col-sm-offset-2 col-sm-10">
  <button type="submit" class="btn btn-primary">Submit</button>
  <button type="submit" class="btn btn-warning">Cancel</button>
</div>
</div>
</form>
