<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model backend\modules\forum\models\Forum */
$domain = Url::home();
$this->title = $model->forum_title;
$this->params['breadcrumbs'][] = ['label' => 'Forums', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$user_post = common\models\UserProfile::findOne($model->created_by);

function getAvatar($avatar_base_url, $avatar_path) {
    if ($avatar_base_url == "") {
        $avatar = "/img/anonymous.jpg";
    } else {
        $avatar = $avatar_base_url . '/' . $avatar_path;
    }
    return $avatar;
}

$check_bookmark = $bookmarks[0][bookmarks];
if ($check_bookmark > 0) {
    $bookmark = '<button type="button" class="btn btn-warning" id="bookmark">Bookmark</button>';
} else {
    $bookmark = "";
}
$check_bookmarkclose = $model->bookmark;
if ($check_bookmarkclose > 0) {
    $bookmark_close = '<button type="button" class="btn btn-default" id="bookmarkclose">Bookmark Close</button>';
} else {
    $bookmark_close = "";
}
?>

<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
<script>tinymce.init({selector: 'textarea'});</script>
<!-- Page Content -->
<div class="container">
    <div class="row">
        <div class="col-md-12"><h3 id="button-bookmark" class="nodeTitle"><?php echo $model->forum_title ?><?php echo $bookmark . $bookmark_close ?></h3></div>
    </div>
    <!-- Post Owner -->
    <div class="row">
        <div class="col-md-3"><center>
                <img src="<?php echo getAvatar($user_post->avatar_base_url, $user_post->avatar_path) ?>" title="avatar" width="80px"/>
                <p id="author">By: <?php echo $user_post->firstname . ' ' . $user_post->lastname ?></p>
                <span class="glyphicon glyphicon-phone"> <?php echo $user_post->telephone ?></span>
                <p id="datepost"><span class="glyphicon glyphicon-envelope" aria-hidden="true"></span> <?php echo $user_post->email ?> </p>
                <p id="datepost"><?php echo $model->created_at ?></p></center>

        </div>
        <div class="col-md-9">
            <b>รายละเอียด</b>
            <hr>
            <p>
                <?php echo $model->forum_detail ?>
            </p>
            <?php
            if ($model->forum_image != "") {
                ?>
                <p>
                    <img src="<?php echo $domain ?>/<?php echo $model->forum_image ?>" width="50%" height="50%" />
                </p>
                <?php
            }
            ?>
        </div>
    </div>
    <?php
    foreach ($model2 as $value) {
        $user = common\models\UserProfile::findOne($value[created_by]);
        ?>
        <hr>
        <!-- Answer -->
        <h3 class="nodeTitlereply">แสดงความคิดเห็น</h3>
        <div class="row">
            <div class="col-md-3"><center>
                    <img src="<?php echo getAvatar($user->avatar_base_url, $user->avatar_path) ?>" title="avatar" width="80px"/>
                    <p id="author">By: <?php echo $user->firstname . ' ' . $user->lastname ?></p>
                    <p id="datepost"><?php echo $value[created_at] ?></p></center>
            </div>
            <div class="col-md-9">
                <b>ความคิดเห็น</b>
                <hr>
                <p>
                    <?php echo $value[reply_comment] ?>
                </p>
                <?php
                if ($value[reply_image] != "") {
                    ?>
                    <p>
                        <img src="<?php echo $domain ?>/<?php echo $value[reply_image] ?>" width="50%" height="50%" />
                    </p>
                    <?php
                }
                ?>
            </div>
        </div>
        <!-- Answer -->
        <?php
    }
    $user_id = Yii::$app->user->identity->userProfile->user_id;
    $user2 = common\models\UserProfile::findOne($user_id);
    $forum_id = $model->id;
    $user_name = Yii::$app->user->identity->userProfile->getFullName();
    $user_img = getAvatar($user2->avatar_base_url, $user2->avatar_path);
    $created_at = date("Y-m-d H:i:s");
    ?>

    <div id="replylast">
    </div>
    <!-- Comment -->
    <hr>
    <form id="testform" class="form-horizontal">
        <h3 class="nodeTitlecomment">แสดงความคิดเห็น</h3>
        <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                    <center><img src="<?php echo $user_img ?>" title="avatar" width="80px"/>
                        <p id="author">By: <?php echo $user_name ?></p>
                </div>
            </div>
            <div class="col-md-9">
                <textarea class="form-control" rows="6" placeholder="ใส่รายละเอียดข้อความ"> </textarea>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-9"></div>
            </br>
            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-5 col-sm-4">
                    <label for="exampleInputFile">อัพโหลดรูปภาพประกอบ</label>
                 <input type="file" id="file" name="file">
                    <!--<div class="well well-small">-->
                        <?php
//                        kartik\widgets\FileInput::widget([
//                            'id' => 'file',
//                            'name' => 'file',
//                            'pluginOptions' => [
//                                'showUpload' => false,
//                                'browseLabel' => 'เลือกไฟล์',
//                                'showRemove' => false,
//                                //'showCaption' => false,
//                                'mainClass' => 'input-group-md'
//                            ]
//                        ])
                        ?>
                    <!--</div>-->
               <p class="help-block">อัพโหลดรูปภาพประกอบ</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-9">
                <button type="button" id="add-reply" user_id="<?php echo $user_id ?>" class="btn btn-primary">Submit</button>
                <button type="button" id="reset" class="btn btn-warning">Cancel</button>
            </div>
        </div>
    </form>




    <!-- End Container-->
</div>
<?php
$jsAddReply = <<< JS
 $(document).on("click", "#add-reply", function() {
   var url = "$domain/forum/forum-reply/add-reply";
var urlimg = "$domain/forum/forum-topic/upload";


/*
   $.get(url, {
       forum_id:$forum_id,
       created_by:user_id,
       reply_comment:comment,

   })
   .done(function( data ) {
     addcommentlast(comment);
     tinyMCE.activeEditor.setContent("");

  });
  */

  var formData = new FormData();
  formData.append('file', $('#file')[0].files[0]);
  $.ajax({
       url : urlimg,
       type : 'POST',
       data : formData,
       processData: false,  // tell jQuery not to process the data
       contentType: false,  // tell jQuery not to set contentType
       success : function(data) {
         addpost(data);
       
       }
        
});
    });


    function addpost(img)
    {
      var url = "$domain/forum/forum-reply/add-reply";
      var user_id=$("#add-reply").attr("user_id");
      var comment =tinyMCE.activeEditor.getContent({format : "raw"});
      if(comment=='<p><br data-mce-bogus="1"></p>'){
        alert("กรุณากรอกข้อมูลก่อนกดปุ่มsubmit");
      }
      else{
        $.get(url, {
            forum_id:$forum_id,
            created_by:user_id,
            reply_comment:comment,
            reply_image:img,

        })
        .done(function( data ) {
          addcommentlast(comment,img);
          tinyMCE.activeEditor.setContent("");
       });
      }

    }





    $(document).on("click", "#bookmark", function() {
      var url = "$domain/forum/forum-topic/add-faq";

      $.get(url, {
          forum_id:$forum_id,

      })
      .done(function( data ) {
alert("ทำการเปิดกระทู้เป็นFaqเรียบร้อย");
$('#button-bookmark').append('<button type="button" class="btn btn-default" id="bookmarkclose">Bookmark Close</button>');

     });
       });

       $(document).on("click", "#bookmarkclose", function() {
         var url = "$domain/forum/forum-topic/remove-faq";

         $.get(url, {
             forum_id:$forum_id,

         })
         .done(function( data ) {
   alert("ทำการปิดกระทู้เป็นFaq ให้เป็นกระทู้ธรรมดาเรียบร้อย");
   $('#bookmarkclose').css('display', 'none');

        });
          });

  $(document).on("click", "#reset", function() {
tinyMCE.activeEditor.setContent("");
   });
   function addcommentlast(comment,img) {
     if(img!=""){
      var img_content='<img src="$domain/'+img+'" width="50%" height="50%" />';
     }else{
      var img_content="";
     }

       var content = '<h3 class="nodeTitlereply">แสดงความคิดเห็น</h3> \
       <div class="row"> \
       <div class="col-md-3"><center> \
       <img src="$user_img" title="avatar" width="80px"/> \
       <p id="author">By: $user_name</p> \
       <p id="datepost">$created_at</p></center> \
       </div> \
       <div class="col-md-9"> \
       <b>ความคิดเห็น</b> \
       <hr> \
       <p> \
       '+comment+' \
       </p> \
       <p> \
         '+img_content+' \
       </p> \
       </div> \
       </div> \
   ';
       $("#replylast").append(content);
}

JS;
$this->registerJs($jsAddReply);





$forum_css = <<< CSS

.thumbnail img {
    width: 100%;
}

.ratings {
    padding-right: 10px;
    padding-left: 10px;
    color: #d17581;
}

.thumbnail {
    padding: 0;
}

.thumbnail .caption-full {
    padding: 9px;
    color: #333;
}

footer {
    margin: 50px 0;
}
/* --- node_link.css --- */

/* tbd */
.nodeTitle {
    font-size: 14px;
    color: rgb(255, 255, 255);
    background-color: rgb(67, 166, 223);
    padding: 15px 12px;
}
span.forumtitle
{
  font-weight: bold;

}
img.icon-file {
  width: 30px;
  height: 30px;
}

td.no { width:10px; padding:5px; }
td.icon { width:20px;}
td.status { width: 10%; }
td.decription { width:35% }
td.latest { width:15%; }
td.repliles { width:15%;}
td.started  { width:12%; }
span.discussion { font-size: 12px; color:rgb(150,150,150); font-variant: inherit;}
input#txt_forum_title.form-control { width: 40%; }
.pageNav { font-size: 13px;
    padding: 2px 0;
    overflow: hidden;
    zoom: 1;
    line-height: 24px;
    word-wrap: normal;
    min-width: 150px;
    white-space: nowrap;
  }
.pageNavHeader {
  display: block;
  float: left;
  margin-right: 3px;
  padding: 1px 0; }
.PageNav a:focus {
    color: rgb(255, 255, 255);
    text-decoration: none;
    background-color: rgb(46, 53, 57);
    border-color: rgb(46, 53, 57);
}
.PageNav a.text {
    width: auto !important;
    padding: 0 4px;
}
p#author {
  color: #43A6DF;
  font-size: 14px;
  font-style:bold;
}
p#datepost { color:#666666; font-size: 12px;}
.nodeTitlereply { background-color:#e4e4e4;font-size: 14px; padding: 15px 12px; }
.nodeTitlecomment {background-color:rgba(91,200,216,0.9); font-size: 14px; padding: 15px 12px; }
.quote { background-color: #e4e4e4; font-style: italic; width:80%;}
#bookmark { float:right;  margin:-10px 0px 0px 0px;}
#bookmarkclose { float:right;  margin:-10px 0px 0px 0px;}
@media (max-width:610px)
{
	.Responsive .node .nodeText
	{
		margin-right: 0;
	}

	.Responsive.Touch .node .nodeDescriptionTooltip,
	.Responsive .node .nodeDescription
	{
		display: none;
	}

	.Responsive .node .nodeLastPost
	{
		position: static;
		height: auto;
		width: auto;
		background: none;
		border: none;
		padding: 0;
		margin: -8px 0 10px 44px;
	}

		.Responsive .node .nodeLastPost .noMessages
		{
			display: none;
		}

		.Responsive .node .nodeLastPost .lastThreadTitle,
		.Responsive .node .nodeLastPost .lastThreadUser
		{
			display: none;
		}

		.Responsive .node .nodeLastPost .lastThreadDate:before
		{
			content: attr(data-latest);
		}

	.Responsive .node .nodeControls
	{
		display: none;
	}

	.Responsive .node .subForumList
	{
		display: none;
	}

	.Responsive .nodeDescriptionTip
	{
		width: auto;
		max-width: 350px;
	}
}

@media (max-width:480px)
{
	.Responsive .subForumsPopup
	{
		display: none;
	}
}
/* --- node_page.css --- */

.nodeList .page .nodeText
{
	margin-right: 10px;
}

/* --- sidebar_share_page.css --- */

.sidebar .sharePage .shareControl
{
	margin-top: 10px;
	min-height: 23px;
}
.sidebar .sharePage iframe
{
	width: 200px;
	height: 20px;
}
.sidebar .sharePage iframe.fb_ltr
{
	_width: 200px !important;
}

.sidebar .sharePage .facebookLike iframe
{
	z-index: 52;
}
.mast .sharePage .secondaryContent
{
	overflow: visible !important;
}
@media (max-width:480px)
{
	.Responsive .sidebar .sharePage
	{
		display: none;
	}
}
CSS;
$this->registerCss($forum_css);
?>
