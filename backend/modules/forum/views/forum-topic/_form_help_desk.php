<?php
/**
 * Created by PhpStorm.
 * User: Kongvut Sangkla
 * Date: 10/3/2559
 * Time: 15:31
 * E-mail: kongvut@gmail.com
 */
?>

<?php
use yii\bootstrap\Html;
use \yii\bootstrap\ActiveForm;
use yii\helpers\Url;
?>
<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>

<div class="modal-content">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="ModalUser">Help Desk (ขอความช่วยเหลือกับผู้ดูแลระบบ)</h4>
    </div>

    <!-- Nav tabs -->
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active"><a href="#tab1" aria-controls="tab1" role="tab" data-toggle="tab"><i class="fa fa-twitch"></i> Line</a></li>
        <li role="presentation"><a href="#tab2" aria-controls="tab2" role="tab" data-toggle="tab"><i class="fa fa-phone"></i> ติดต่อผ่านโทรศัพท์</a></li>
        <li role="presentation"><a href="#tab3" aria-controls="tab3" role="tab" data-toggle="tab"><i class="glyphicon glyphicon-question-sign"></i> ขอช่วยเหลือ</a></li>
        <li role="presentation"><a href="#tab4" aria-controls="tab4" role="tab" data-toggle="tab"><i class="glyphicon glyphicon-list-alt"></i> กระทู้ช่วยเหลือ</a></li>
        <li role="presentation"><a href="#tab5" aria-controls="tab5" role="tab" data-toggle="tab"><i class="glyphicon glyphicon-pushpin"></i> Faq</a></li>
    </ul>

    <!-- Tab panes -->
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="tab1">
            <div class="modal-body">
                <?php
                    $line = json_decode(Yii::$app->keyStorage->get('line_group'));
                    echo Html::a(Html::img('/../img/line/'.$line->img, ['class'=>'img-responsive', 'style'=>'margin: 0 auto;']), $line->url, ['target'=>'_blank']);
                ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">ปิด</button>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane" id="tab2">
            <div class="modal-body">

                <ol>
                    <?php if(Yii::$app->keyStorage->get('frontend.domain') == 'cascap.in.th') { ?>
                    <li>ใบกำกับงาน OV-CCA
                        <ul>
                            <li>ศุภัทร์ มณีวงศ์ (086-8682300)</li>
                            <li>ภาณุวัฒน์ 	ประทุมขำ (080-1841938)</li>
                        </ul>
                    </li>
                    <li>รายงาน CASCAP
                        <ul>
                            <li>ชัยวัฒน์ ทะวะรุ่งเรือง (095-6585669)</li>
                        </ul>
                    </li>                    
                    <?php } ?>
                    <li>TCC Bot
                        <ul>
                            <!-- li>คงวุฒิ แสงกล้า (087-5442559)</li -->
                            <li>ภาณุวัฒน์ 	ประทุมขำ (080-1841938)</li>
                        </ul>
                    </li>
                    <li>ระบบสมัครสมาชิก
                        <ul>
                            <li>สันติ  ต๊อดแก้ว (091-8632115)</li>
                            <li>จารุวรรณ เถื่อนมั่น (083-1451504)</li>
                        </ul>
                    </li>
                    <?php if(Yii::$app->keyStorage->get('frontend.domain') == 'cascap.in.th') { ?>
                    <li>การตรวจพยาธิใบไม้ตับ (OV-CCA)
                        <ul>
                            <li>ดร.กุลธิดา โกพลรัตน์ (091-0643540)</li>
                        </ul>
                    </li>
                    <li>ถามตอบข้อสงสัยเกี่ยวกับ ใบทำบัตร, ICF และ Ultrasound
                        <ul>
                            <li>สันติ (091-8632115)</li>
                            <li>สุวิชชา (095-6585669)</li>
                        </ul>
                    </li>
                    <?php } ?>
                    <li>ติดต่อโครงการ และค่าตอบแทน
                        <ul>
                            <li>แจ้งเรื่องที่ต้องการติดต่อ (043-202691 หรือ 088-5710298)</li>
                        </ul>
                    </li>
                    <li>ข้อเสนอแนะ-ติชม
                        <ul>
                            <li>รศ.ดร.บัณฑิต 	ถิ่นคำรพ (085-0011123)</li>
                        </ul>
                    </li>
                </ol>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">ปิด</button>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane" id="tab3">
          <div class="modal-body">

            <div id="form_add_forum"></div>



        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">ปิด</button>
        </div>
      </div><!-- tab3   -->
      <div role="tabpanel" class="tab-pane" id="tab4">
        <div class="modal-body">
        <div id="topic_user"></div>
      </div>
      <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">ปิด</button>
      </div>
    </div><!-- tab4   -->
    <div role="tabpanel" class="tab-pane" id="tab5">
      <div class="modal-body">
      <div id="topic_faq"></div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">ปิด</button>
    </div>
  </div><!-- tab5   -->
    </div>

</div>

<?php

$user_id=Yii::$app->user->identity->id;
$domain=Url::home();
$url="$domain/forum/forum-topic/get-form-forum";
$jsAddForum =<<< JS


  //tinymce.init({selector:'textarea',width : "40%"});



getFormUser();
getUserTopic(1);
getFaqTopic();
 $(document).on("click", "#btn-add-forum", function() {

   var urlimg = "$domain/forum/forum-topic/upload";



   //var forum_detail =tinyMCE.activeEditor.getContent({format : "text"});
   var formData = new FormData();
   formData.append('file', $('#file')[0].files[0]);
  // alert(formData);
/*
   $.post(url, {
       forum_title:forum_title,
       forum_detail:forum_detail,
       file:formData,

   })
   .done(function( data ) {
    $( "#form_add_forum" ).html('<div class="alert alert-danger" role="alert">ส่งปัญหาเรียบร้อย เจ้าหน้าที่จะตอบกลับภายใน24ชั่วโมง กรุณาตรวจสอบได้ที่กล่องจดหมายหรือกระทู้ช่วยเหลือ</div>');
    getUserTopic();
  });
*/

  $.ajax({
       url : urlimg,
       type : 'POST',
       data : formData,
       processData: false,  // tell jQuery not to process the data
       contentType: false,  // tell jQuery not to set contentType
       success : function(data) {
         addpost(data);
       }
});


  });
  function addpost(img)
  {
    var url = "/forum/forum-topic/add-topic";
    var forum_title=$("#txt_forum_title").val();
    var forum_detail=$('textarea#txt_forum_detail').val();
    if(forum_title=='' || forum_detail==''){
alert("กรุณากรอกข้อมูลให้ครบ");
    }else{
      $.post(url, {
          forum_title:forum_title,
          forum_detail:forum_detail,
          forum_image:img,

      })
      .done(function( data ) {
       $( "#form_add_forum" ).html('<div class="alert alert-danger" role="alert">ส่งปัญหาเรียบร้อย เจ้าหน้าที่จะตอบกลับภายใน24ชั่วโมง กรุณาตรวจสอบได้ที่กล่องจดหมายหรือกระทู้ช่วยเหลือ</div>');
       getUserTopic(1);
     });
    }

  }


  function getFormUser()
  {
    var urlgetform = "$domain/forum/forum-topic/get-form-forum";
    $.get(urlgetform, {
        news_id:'news_id'
    })
    .done(function( data ) {
      $( "#form_add_forum" ).html( data );

    });
  }
function getUserTopic(page)
{
  var urltopic="$domain/forum/forum-topic/get-topic-user";
  $.get(urltopic,{page:page})
  .done(function( data ) {
$( "#topic_user" ).html('');
$( "#topic_user" ).html( data );
 });
}

function getFaqTopic()
{
  var urltopic="$domain/forum/forum-topic/get-faq";
  $.get(urltopic)
  .done(function( data ) {
$( "#topic_faq" ).html( data );
 });
}

$(document).on("click", "#pagenav", function() {
  var page=parseInt($(this).attr('page'));
  getUserTopic(page);
  });
JS;
$this->registerJs($jsAddForum);
$forum_css=<<< CSS

.thumbnail img {
    width: 100%;
}

.ratings {
    padding-right: 10px;
    padding-left: 10px;
    color: #d17581;
}

.thumbnail {
    padding: 0;
}

.thumbnail .caption-full {
    padding: 9px;
    color: #333;
}

footer {
    margin: 50px 0;
}
/* --- node_link.css --- */

/* tbd */
.nodeTitle {
    font-size: 14px;
    color: rgb(255, 255, 255);
    background-color: rgb(67, 166, 223);
    padding: 15px 12px;
}
span.forumtitle
{
  font-weight: bold;

}
img.icon-file {
  width: 30px;
  height: 30px;
}

td.no { width:5%; padding:5px; }
td.icon { width:5%;}
td.status { width: 10%; }
td.description { width:20% }
td.latest { width:15%; }
td.repliles { width:15%;}
td.started  { width:12%; }
span.discussion { font-size: 12px; color:rgb(150,150,150); font-variant: inherit;}
input#txt_forum_title.form-control { width: 40%; }
.pageNav { font-size: 13px;
    padding: 2px 0;
    overflow: hidden;
    zoom: 1;
    line-height: 24px;
    word-wrap: normal;
    min-width: 150px;
    white-space: nowrap;
  }
.pageNavHeader {
  display: block;
  float: left;
  margin-right: 3px;
  padding: 1px 0; }
.PageNav a:focus {
    color: rgb(255, 255, 255);
    text-decoration: none;
    background-color: rgb(46, 53, 57);
    border-color: rgb(46, 53, 57);
}
.PageNav a.text {
    width: auto !important;
    padding: 0 4px;
}
p#author {
  color: #43A6DF;
  font-size: 14px;
  font-style:bold;
}
p#datepost { color:#666666; font-size: 12px;}
.nodeTitlereply { background-color:#e4e4e4;font-size: 14px; padding: 15px 12px; }
.nodeTitlecomment {background-color:rgba(91,200,216,0.9); font-size: 14px; padding: 15px 12px; }
.quote { background-color: #e4e4e4; font-style: italic; width:80%;}
#bookmark { float:right;  margin:-10px 0px 0px 0px;}
@media (max-width:610px)
{
	.Responsive .node .nodeText
	{
		margin-right: 0;
	}

	.Responsive.Touch .node .nodeDescriptionTooltip,
	.Responsive .node .nodeDescription
	{
		display: none;
	}

	.Responsive .node .nodeLastPost
	{
		position: static;
		height: auto;
		width: auto;
		background: none;
		border: none;
		padding: 0;
		margin: -8px 0 10px 44px;
	}

		.Responsive .node .nodeLastPost .noMessages
		{
			display: none;
		}

		.Responsive .node .nodeLastPost .lastThreadTitle,
		.Responsive .node .nodeLastPost .lastThreadUser
		{
			display: none;
		}

		.Responsive .node .nodeLastPost .lastThreadDate:before
		{
			content: attr(data-latest);
		}

	.Responsive .node .nodeControls
	{
		display: none;
	}

	.Responsive .node .subForumList
	{
		display: none;
	}

	.Responsive .nodeDescriptionTip
	{
		width: auto;
		max-width: 350px;
	}
}

@media (max-width:480px)
{
	.Responsive .subForumsPopup
	{
		display: none;
	}
}
/* --- node_page.css --- */

.nodeList .page .nodeText
{
	margin-right: 10px;
}

/* --- sidebar_share_page.css --- */

.sidebar .sharePage .shareControl
{
	margin-top: 10px;
	min-height: 23px;
}
.sidebar .sharePage iframe
{
	width: 200px;
	height: 20px;
}
.sidebar .sharePage iframe.fb_ltr
{
	_width: 200px !important;
}

.sidebar .sharePage .facebookLike iframe
{
	z-index: 52;
}
.mast .sharePage .secondaryContent
{
	overflow: visible !important;
}
@media (max-width:480px)
{
	.Responsive .sidebar .sharePage
	{
		display: none;
	}
}
CSS;
$this->registerCss($forum_css);
?>
