<?php
/**
 * Created by PhpStorm.
 * User: Kongvut Sangkla
 * Date: 9/3/2559
 * Time: 18:13
 * E-mail: kongvut@gmail.com
 */
?>
<div class="media">
    <hr>
    <div class="media-left bg">
        <a href="#">
            <img class="media-object" title="64x64" src="<?php echo Yii::$app->user->identity->userProfile->getAvatar() ?: '/img/anonymous.jpg' ?>" data-holder-rendered="true" style="width: 64px; height: 64px;">
        </a>
    </div>
    <div class="media-body">
        <div class="alert alert-warning" role="alert">
            <?php echo \kartik\helpers\Html::activeLabel($model, 'message'); ?>
            <?php echo \kartik\helpers\Html::activeLabel($model, 'from_user_id'); ?>
            <?php echo \kartik\helpers\Html::activeLabel($model, 'to_user_id'); ?>
            <?php echo \kartik\helpers\Html::activeLabel($model, 'status'); ?>
            <?php echo \kartik\helpers\Html::activeLabel($model, 'create_at'); ?>
            <?php echo \kartik\helpers\Html::activeLabel($model, 'link_to'); ?>
            <?php echo \kartik\helpers\Html::activeLabel($model, 'read_at'); ?>
        </div>
</div>
</div>