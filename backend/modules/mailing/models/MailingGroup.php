<?php

namespace backend\modules\mailing\models;

use Yii;

/**
 * This is the model class for table "mailing_group".
 *
 * @property integer $id
 * @property string $group_name
 * @property integer $user_create
 * @property string $create_date
 */
class MailingGroup extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mailing_group';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_create'], 'integer'],
            [['create_date'], 'safe'],
            [['group_name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'group_name' => 'Group Name',
            'user_create' => 'User Create',
            'create_date' => 'Create Date',
        ];
    }
}
