<?php

namespace backend\modules\mailing;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\modules\mailing\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
