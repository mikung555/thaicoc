<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\mailing\models\MailingGroup */

$this->title = 'Create Mailing Group';
$this->params['breadcrumbs'][] = ['label' => 'Mailing Groups', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mailing-group-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
