<?php

namespace backend\modules\his;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\modules\his\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
