<?php

namespace backend\modules\his\controllers;

use yii\web\Controller;

class DefaultController extends Controller
{
    public $layout='//main2';
    
    public function actionIndex()
    {
        return $this->render('index');
    }
}
