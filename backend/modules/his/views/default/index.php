<?php
\backend\modules\his\assets\HisAsset::register($this);
?>
<div id="app-his">
    <div class="row">
        <div class="col-lg-2">
            <div class="card">
                <div class="card-header">
                    <button type="button" class="btn btn-block btn-outline-success" style="margin-bottom: 5px; ">
                        <i class="glyphicon glyphicon-random"></i> 
                        <strong>Patients Queue</strong>
                    </button>
                </div>
                <div class="card-block">
                    <div style="height: 320px; overflow-y: auto; ">
                        <div class="list-group" style="margin: 0;" > 
                            <a href="#" class="list-group-item active"> 1. ธนากรณ์ ธรรมราช </a> 
                            <a href="#" class="list-group-item">2. พิศมัย บุญศรี</a> 
                            <a href="#" class="list-group-item">3. รัชดาภรณ์ แสงภักดิ์</a> 
                            <a href="#" class="list-group-item">4. อุไรวรรณ ศรีวังพล</a> 
                            <a href="#" class="list-group-item">5. ฐิติ ศรีแก้ว</a>
                            <a href="#" class="list-group-item "> 6. ภานุพงศ์ ศรีศุภเดชะ </a> 
                            <a href="#" class="list-group-item">7. สันติ ต๊อดแก้ว</a> 
                            <a href="#" class="list-group-item">8. วศิน ชคัตตรัยกุล</a> 
                            <a href="#" class="list-group-item">9. พิชเยนทร์ ดวงทองพล</a> 
                            <a href="#" class="list-group-item">10. อลิสรา ศรีนิลทา</a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="card-header">
                    <button type="button" class="btn btn-block btn-warning" style="margin-bottom: 5px;margin-top: 5px;">
                        <i class="glyphicon glyphicon-random"></i> 
                        <strong>Appointment</strong>
                    </button>
                </div>
                <div class="card-block">
                    <div>
                        <div class="list-group" style="margin: 0;"> 
                            <a href="#" class="list-group-item list-group-item-action flex-column align-items-start ">
                                <div class="d-flex w-100 justify-content-between">
                                    <h5 class="mb-1">อุไรวรรณ ศรีวังพล</h5>
                                    <small>01/07/2017 </small>
                                </div>
                                <p class="mb-2">ตรวจร่างกาย Donec id elit non mi porta gravida at eget metus.</p>
                                <small>ชื่อแพทย์ผู้ตรวจ</small>
                            </a>
                            <a href="#" class="list-group-item list-group-item-action flex-column align-items-start ">
                                <div class="d-flex w-100 justify-content-between">
                                    <h5 class="mb-1">อุไรวรรณ ศรีวังพล</h5>
                                    <small>01/07/2017 </small>
                                </div>
                                <p class="mb-2">ตรวจร่างกาย Donec id elit non mi porta gravida at eget metus.</p>
                                <small>ชื่อแพทย์ผู้ตรวจ</small>
                            </a>
                            <a href="#" class="list-group-item list-group-item-action flex-column align-items-start ">
                                <div class="d-flex w-100 justify-content-between">
                                    <h5 class="mb-1">อุไรวรรณ ศรีวังพล</h5>
                                    <small>01/07/2017 </small>
                                </div>
                                <p class="mb-2">ตรวจร่างกาย Donec id elit non mi porta gravida at eget metus.</p>
                                <small>ชื่อแพทย์ผู้ตรวจ</small>
                            </a>
                            <a href="#" class="list-group-item list-group-item-action flex-column align-items-start ">
                                <div class="d-flex w-100 justify-content-between">
                                    <h5 class="mb-1">อุไรวรรณ ศรีวังพล</h5>
                                    <small>01/07/2017 </small>
                                </div>
                                <p class="mb-2">ตรวจร่างกาย Donec id elit non mi porta gravida at eget metus.</p>
                                <small>ชื่อแพทย์ผู้ตรวจ</small>
                            </a>

                        </div>
                    </div>
                </div>
            </div>





        </div>
        <div class="col-lg-6 sdbox-col">

            <div class="card" style="margin-bottom: 10px;">
                <div class="card-block" style="background-color: #33a0ff">
                    <form id="searchform" style="margin-top: 10px; margin-bottom: 10px;position: relative;">
                        <input id="inputString" onkeyup="suggestionsUserLookup(this.value);" type="text" class="form-control" placeholder="Search...">
                        <span class="icon-header fa fa-search"></span>
                        <div id="suggestions" style="display: none;"></div>
                    </form>
                </div>

            </div>

            <div class="card card-success" style="margin-bottom: 10px;">
                <div class="card-header">
                    <ul class="nav nav-tabs card-header-tabs">
                        <li role="presentation" class="active"><a href="#">Register</a></li>
                        <li role="presentation"><a href="#">Admit</a></li>
                        <li role="presentation"><a href="#">Discharge</a></li>
                        <li role="presentation"><a href="#">Transfer</a></li>
                    </ul>
                </div>
                <div class="card-block">
                    <div style=" position: relative;">
                        <div class="media"> 

                            <div class="media-body"> 
                                <table width="100%" class="table table-striped  table-condensed" style="margin-bottom: 0;">
                                    <tbody>
                                        <tr>
                                            <td><strong>HN</strong> 92303</td>
                                            <td><strong>PID</strong> 92303</td>
                                            <td><strong>สิทธิประโยชน์</strong> 92303</td>
                                        </tr>
                                        <tr>
                                            <td><strong>ชื่อ</strong> น.ส. หฤทัย</td>
                                            <td><strong>นามสกุล</strong> พูลวงษา</td>
                                            <td><strong>อายุ</strong> 20</td>
                                        </tr>
                                        <tr>
                                            <td colspan="3"><strong>Address</strong> 
                                                <span class="style7">บ้านเลขที่ : <code>6</code></span> <span class="style7"> หมู่ : <code>08</code></span> <span class="style7">หมู่บ้าน : <code>นอกเขต(สำหรับบันทึกบ้านและคน นอกเขตฯ)</code></span>
                                                <span class="style7"> ต.<code>แสนชาติ   </code> อ.<code>จังหาร   </code> จ.<code>ร้อยเอ็ด   </code> 45000 </span>
                                            </td>

                                        </tr>
                                    </tbody>
                                </table>
                            </div> 
                            <div class="media-right"> <img class="media-object" src="<?= yii\helpers\Url::to(['/img/anonymous.jpg']) ?>" width="100"></div> 
                        </div>




                    </div>
                </div>
            </div>



            <div class="card card-primary" >
                <div class="card-header">
                    <ul class="nav nav-tabs card-header-tabs">
                        <li role="presentation" class="active"><a href="#">OPD ทั่วไป</a></li>
                        <li role="presentation"><a href="#">OPD เคมี</a></li>
                        <li role="presentation"><a href="#">OPD ศัลย์</a></li>
                        <li role="presentation"><a href="#">OPD รังสี</a></li>
                        <li role="presentation"><a href="#">IPD แพทย์</a></li>
                        <li role="presentation"><a href="#">IPD พยาบาล</a></li>
                        <li role="presentation"><a href="#">IPD เภสัช</a></li>
                    </ul>

                </div>
                <div class="card-block">
                    <div class="row">
                        <div class="col-sm-4">
                            <table width="100%" class="table table-striped  table-condensed" style="margin-bottom: 0;">
                                <tbody>
                                    <tr>
                                        <td><strong>CC:</strong> </td>
                                    </tr>
                                    <tr>
                                        <td><strong>PI:</strong> </td>
                                    </tr>
                                    <tr>
                                        <td><strong>PH:</strong> </td>
                                    </tr>
                                    <tr>
                                        <td><strong>FH:</strong> </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-sm-4">
                            <table width="100%" class="table table-condensed" style="margin-bottom: 0;">
                                <tbody>
                                    <tr>
                                        <td><strong>PE:</strong> </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-sm-4">
                            <table width="100%" class="table  table-condensed" style="margin-bottom: 0;">
                                <tbody>
                                    <tr>
                                        <td><strong>Note:</strong> </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card" style="margin-bottom: 10px;">
                <div class="card-block">
                    <div class="form-inline" >
                        <button type="button" class="btn btn-outline-success" style="">
                            <i class="fa fa-user-md" aria-hidden="true"></i>
                            Diagnosis
                        </button>

                        <label>PD</label>
                        <span class="label label-success" style="font-size: 14px;">Cellulocutaneous plague</span>
                        <label>ICD-10, 9-CM</label>
                        <span class="label label-warning" style="font-size: 14px;">F10.00:Alcohol, Acute intoxication, Uncomplicated</span>

                    </div>
                </div>

            </div>

            <div class="card" style="margin-bottom: 10px;">
                <div class="card-block">
                    <div class="form-inline">
                        <button type="button" class="btn btn-outline-success" style="">
                            <i class="fa fa-heartbeat" aria-hidden="true"></i>
                            CA Staging
                        </button>
                        <span class="label label-warning" style="font-size: 14px;">3A</span>
                        <label>T</label>
                        <span class="label label-primary" style="font-size: 14px;">1</span>
                        <label>N</label>
                        <span class="label label-primary" style="font-size: 14px;">1</span>
                        <label>M</label>
                        <span class="label label-primary" style="font-size: 14px;">0</span>
                    </div>
                </div>

            </div>

            <div class="card card-danger" style="margin-bottom: 10px;">
                <div class="card-header">
                    <ul class="nav nav-tabs card-header-tabs">
                        <li role="presentation" class="active"><a href="#">Drug Prescribe</a></li>
                        <li role="presentation"><a href="#">Doctor's Order</a></li>
                        <li role="presentation"><a href="#">Investigation</a></li>
                    </ul>

                </div>
                <div class="card-block">

                    <div>
                        <h4><span class="label label-info">11076 : วันอังคาร 3 พฤษภาคม 2559 [OPD]</span></h4>
                        amitriptyline (D) <code>20</code> เม็ด<br/>
                        ORPHENadine + paracetamol <code>20</code> เม็ด<br/>
                        พญายอ Cream <code>3</code> หลอด (5 g.)<br/>
                    </div>
                    <div>
                        <h4><span class="label label-info">11076 : วันพุธ 4 พฤษภาคม 2559 [OPD]</span></h4>
                        amitriptyline (D) <code>20</code> เม็ด<br/>
                        ORPHENadine + paracetamol <code>20</code> เม็ด<br/>
                        พญายอ Cream <code>3</code> หลอด (5 g.)<br/>
                    </div>
                </div>
            </div>



        </div>
        <div class="col-lg-4 sdbox-col">
            <div class="card" style="margin-bottom: 10px;">
                <div class="card-block">
                    <div class="form-inline" >
                        <button type="button" class="btn btn-outline-danger" style="">
                            <i class="fa fa-stethoscope" aria-hidden="true"></i>
                            VITAL SIGN
                        </button>

                        <label>Bw</label>
                        <span class="label label-warning" style="font-size: 18px;">65</span>
                        <label>Ht</label>
                        <span class="label label-warning" style="font-size: 18px;">180</span>
                        <label>BMI</label>
                        <span class="label label-success" style="font-size: 18px;">21.5</span>
                    </div>
                </div>

            </div>

            <div class="card" style="margin-bottom: 10px;">
                <div class="card-block">
                    <div class="row">
                        <div class="col-md-4">
                            <button type="button" class="btn btn-block btn-outline-primary" style="">
                                <i class="glyphicon glyphicon-user" aria-hidden="true"></i>
                                <strong>Personal EMR</strong>
                            </button>
                            <div style="height: 220px; overflow-y: auto; margin-top: 5px;">
                                <div class="list-group"> 
                                    <a href="#" class="list-group-item active">01/07/2017</a> 
                                    <a href="#" class="list-group-item">01/07/2017</a> 
                                    <a href="#" class="list-group-item">01/07/2017</a> 
                                    <a href="#" class="list-group-item">01/07/2017</a> 
                                    <a href="#" class="list-group-item">01/07/2017</a>

                                </div>
                            </div>
                        </div>

                        <div class="col-md-8 sdbox-col">
                            <button type="button" class="btn btn-block btn-outline-info" style="">
                                <strong>Electronic Medical Record</strong>
                            </button>
                            <div style="height: 220px; overflow-y: auto; margin-top: 5px;" >
                                <dl class="dl-horizontal" >
                                    <dt style="width: 30px;">Dx:</dt>
                                    <dd style="margin-left: 40px;">
                                        <strong>โรคหลัก</strong><br/>
                                        <code>F10.00:</code> Alcohol, Acute intoxication, Uncomplicated
                                        <br/>
                                        <strong>โรคร่วม</strong><br/>
                                        <code>A20.1:</code> Cellulocutaneous plague
                                        <br/>
                                    </dd>
                                </dl>
                                <dl class="dl-horizontal">
                                    <dt style="width: 30px;">Rx:</dt>
                                    <dd style="margin-left: 40px;">
                                        amitriptyline (D) <code>20</code> เม็ด<br/>
                                        ORPHENadine + paracetamol <code>20</code> เม็ด<br/>
                                        พญายอ Cream <code>3</code> หลอด (5 g.)<br/>
                                    </dd>
                                </dl>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <button type="button" class="btn btn-block btn-success" style="">
                                <i class="glyphicon glyphicon-print" aria-hidden="true"></i>
                                <strong>report</strong>
                            </button>
                        </div>

                    </div>
                </div>

            </div>

            <div class="card card-info" style="margin-bottom: 10px;">
                <div class="card-header">
                    <ul class="nav nav-tabs card-header-tabs">
                        <li role="presentation" class="active"><a href="#">XR, CT, MRI</a></li>
                        <li role="presentation"><a href="#">LAB</a></li>
                        <li role="presentation"><a href="#">PATHO</a></li>
                        <li role="presentation"><a href="#">Others</a></li>
                    </ul>

                </div>
                <div class="card-block">
                    <div style=" height: 350px; overflow-y: auto; ">
                        <div class="row">
                            <div class="col-xs-6 col-md-3">
                                <a href="#" class="thumbnail">
                                    <img src="<?= yii\helpers\Url::to(['/img/mri1.jpg']) ?>" alt="..." >
                                </a>
                            </div>
                            <div class="col-xs-6 col-md-3">
                                <a href="#" class="thumbnail">
                                    <img src="<?= yii\helpers\Url::to(['/img/mri2.jpeg']) ?>" alt="..." >
                                </a>
                            </div>
                            <div class="col-xs-6 col-md-3">
                                <a href="#" class="thumbnail">
                                    <img src="<?= yii\helpers\Url::to(['/img/mri3.jpeg']) ?>" alt="..." >
                                </a>
                            </div>
                            <div class="col-xs-6 col-md-3">
                                <a href="#" class="thumbnail">
                                    <img src="<?= yii\helpers\Url::to(['/img/mri4.jpeg']) ?>" alt="..." >
                                </a>
                            </div>
                            <div class="col-xs-6 col-md-3">
                                <a href="#" class="thumbnail">
                                    <img src="<?= yii\helpers\Url::to(['/img/mri5.jpg']) ?>" alt="..." >
                                </a>
                            </div>
                            <div class="col-xs-6 col-md-3">
                                <a href="#" class="thumbnail">
                                    <img src="<?= yii\helpers\Url::to(['/img/mri1.jpg']) ?>" alt="..." >
                                </a>
                            </div>
                            <div class="col-xs-6 col-md-3">
                                <a href="#" class="thumbnail">
                                    <img src="<?= yii\helpers\Url::to(['/img/mri2.jpeg']) ?>" alt="..." >
                                </a>
                            </div>
                            <div class="col-xs-6 col-md-3">
                                <a href="#" class="thumbnail">
                                    <img src="<?= yii\helpers\Url::to(['/img/mri3.jpeg']) ?>" alt="..." >
                                </a>
                            </div>
                            <div class="col-xs-6 col-md-3">
                                <a href="#" class="thumbnail">
                                    <img src="<?= yii\helpers\Url::to(['/img/mri4.jpeg']) ?>" alt="..." >
                                </a>
                            </div>
                            <div class="col-xs-6 col-md-3">
                                <a href="#" class="thumbnail">
                                    <img src="<?= yii\helpers\Url::to(['/img/mri5.jpg']) ?>" alt="..." >
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>





        </div>
    </div>
</div>
