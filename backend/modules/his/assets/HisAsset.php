<?php
 

namespace backend\modules\his\assets;

use yii\web\AssetBundle;
 
class HisAsset extends AssetBundle {

    public $sourcePath='@backend/modules/his/assets';
    
    public $css = [
        //'css/clarity.css',
        'css/style.css',
        
    ];
    public $js = [
        
    ];
    public $depends = [
	'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];

}
