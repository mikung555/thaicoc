<?php

namespace frontend\modules\emobile\controllers;

use Yii;
use dosamigos\qrcode\formats\MailTo;
use dosamigos\qrcode\QrCode;

class QrcodeController extends \yii\web\Controller {

    //put your code here
    public function getImgPath() {
       //return \yii\helpers\Url::to('./images');
       return Yii::$app->basePath . '/../storage/web/source/';
      //return Yii::getAlias('@storageUrl') . '/source/';
    }

    public function getBgPath() {
        //return Yii::$app->basePath . '/images/bg.jpg';
        //return \yii\helpers\Url::to('./images/bg.jpg');
        return Yii::$app->basePath . '/../frontend/web/images/bg.jpg';
    }

    public function getFontPath() {
        // return Yii::$app->basePath . '/../frontend/web/css/THSarabunNew.ttf';
       // return \yii\helpers\Url::to('./css/THSarabunNew.ttf');
        return Yii::$app->basePath . '/../frontend/web/css/THSarabunNew.ttf';
    }

    public function getQrcode($uid) {
        return file_get_contents('https://api.qrserver.com/v1/create-qr-code/?size=180x180&data=' . $uid);
    }

    public function actionIndex() {
         $name = Yii::$app->request->get('name');
         $uid = Yii::$app->request->get('uid');
         $mobile_id = Yii::$app->request->get('mobile_id');
         if(empty($name) || empty($uid) || empty($mobile_id)){
            $name =  'ณัฐพล จันทร์ปาน';
            $uid = '12345'; 
            $mobile_id =  '6789876';
         }
        return $this->renderAjax('index',[
            'name'=>$name,
            'uid'=>$uid,
            'mobile_id'=>$mobile_id
            
        ]);
    }
    public function setFilename($name)
    {
       return  Yii::$app->security->generatePasswordHash($name);
    }

    public function actionCustom() {
        if (empty(Yii::$app->request->get())) {
            $result = [
              "status"=>404,
              "message"=>"Not Found"
            ];
            echo \yii\helpers\Json::encode($result);
        } else {
            //Set the Content Type
            header('Content-type: image/jpeg');

            $name = Yii::$app->request->get('name');
            $uid = Yii::$app->request->get('uid');
            $mobile_id = Yii::$app->request->get('mobile_id');
            if(empty($name) || empty($uid) || empty($mobile_id)){
                $result = [
                    "status"=>404,
                    "message"=>"Not Found"
                  ];
                  echo \yii\helpers\Json::encode($result);
                exit();
            }

            $fullPathBg = Yii::$app->basePath . '/../storage/web/source/bg.jpg';
            $fullPath = Yii::$app->basePath . '/../storage/web/source';
            
            $jpg_image = imagecreatefromjpeg($fullPathBg);//bg
            $im2 = imagecreatefromstring($this->getQrcode($mobile_id));
            $im3 = imagecreatefromstring($this->getQrcode($uid));

            // Allocate A Color For The Text
            $white = imagecolorallocate($jpg_image, 0, 0, 0);

            // Set Path to Font File
            $font_path = $this->getFontPath();


            // Print Text On Image
            imagettftext($jpg_image, 40, 0, 300, 200, $white, $font_path, $name);


            // Send Image to Browser
            //imagejpeg($jpg_image);
            $random = md5(rand(1006, 1000000));
            //Yii::$app->getSecurity()->generatePasswordHash($password)
            

            //imagecopymerge ($dst_im, $src_im, $dst_x, $dst_y, $src_x, $src_y, $src_w, $src_h, $pct)
            imagecopymerge($jpg_image, $im2, 217, 302, 0, 0, 180, 180, 100);
            imagecopymerge($jpg_image, $im3, 613, 302, 0, 0, 180, 180, 100);

            $output = $fullPath . '/' . $random . "nut.jpg";
            //imagejpeg($jpg_image, $output);
            if(!imagejpeg($jpg_image, $output)){
             imagedestroy($jpg_image);   
                $result = [
                    "status"=>404,
                    "message"=>"ไม่สามารถอัพโหลดรูปภาพได้"
                  ];
                echo json_encode($result);
                exit();//\appxq\sdii\utils\VarDumper::dump($result);
                  
            }


            // Clear Memory
            imagedestroy($jpg_image);
            $result = [
              "status"=>200,
              "message"=>"OK",
              "name"=>$name,
              "mobile_id"=>$mobile_id,  
              "uid"=>$uid,
              "imgname"=>$random."nut.jpg",
              "image"=>"https://www.thaicarecloud.org/images/".$random."nut.jpg"
            ];
            echo \yii\helpers\Json::encode($result);
        }
    }

    public function actionCreateQrcode() {

        $random = rand(100, 1000);
        @file_put_contents("output/$random.jpg", base64_decode(explode(",", $_POST['data'])[1]));
        print_r(@file_put_contents);
    }

}
