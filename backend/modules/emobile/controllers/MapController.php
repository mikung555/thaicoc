<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace backend\modules\emobile\controllers;
use backend\modules\emobile\classes\Map;

use Yii;
use yii\web\Controller;
/**
 * Description of Map
 *
 * @author engiball
 */
class MapController extends \yii\base\Controller {

    //put your code here

    public function actionIndex() {

        return $this->render('index');
    }

    public function actionMapJs() {

        return $this->renderPartial('js');
    }

    public function actionMapJs2() {

        return $this->renderPartial('js2');
    }
      public function actionLeafjs() {

        return $this->renderPartial('leafjs');
    }


    public function actionCurve() {

        return $this->renderPartial('curvejs');
    }

    public function actionAnim() {

        return $this->renderPartial('animjs');
    }
    public function actionBIcon() {

        return $this->renderPartial('bicon');
    }
      public function actionBMarker() {

        return $this->renderPartial('bmarker');
    }
       public function actionLabel() {

        return $this->renderPartial('label');
    }
    public function actionPopup() {
               return $this->renderPartial('popup');

    }
        public function actionFullscreen() {
               return $this->renderPartial('fullscreen');

    }
         public function actionFullscreencss() {
               return $this->renderPartial('fullscreencss');

    }
    
    
    


    public function actionGetNewPoints() {

        return "[[16.957551,102.8309166],[16.757551,102.8409166],[16.732551,102.5609166]]";
    }

    public function actionGetPeoplePoint() {
        $datas = array();
        $sitecode = Yii::$app->request->get('sitecode');
        $sql = "select sys_lat as lat,sys_lng as lng from tb_data_coc where hsitecode =:sitecode  and sys_lat is not null and  sys_lat <> '' order by rand() limit 500";
        $data = \Yii::$app->db->createCommand($sql, [':sitecode' => $sitecode])->queryAll();
        $data0 = ["lat" => $data[0]["lat"], "lng" => $data[0]["lng"]];
        array_push($datas, $data0, $data);
        return json_encode($datas);
    }

    public function actionGetPlace() {
        $hcode = \Yii::$app->request->get('hcode');
        $datas = array();
        $sql = "SELECT DISTINCT hsitecode as hcode from tbdata_1462172833035880400 where InputUnit=:hcode  and hsitecode <>  InputUnit and  hsitecode <> '' UNION SELECT DISTINCT InputUnit as hcode from tbdata_1462172833035880400 where hsitecode =:hcode  and hsitecode <>  InputUnit  and InputUnit <>''";
        $data = \Yii::$app->db->createCommand($sql, [':hcode' => $hcode])->queryAll();

        return json_encode($data);
    }

    public function actionHosDetail() {
        $hcode = \Yii::$app->request->get('hcode');
         $sitecode = \Yii::$app->request->get('sitecode');

        $mapData = Map::hDetail($hcode,$sitecode);
        return json_encode($mapData);
    }
    
    

}
