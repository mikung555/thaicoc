<?php

namespace frontend\modules\emobile\controllers;
use yii;
use yii\web\Controller;
use frontend\modules\emobile\models\UploadForm;
use yii\web\UploadedFile;
class ImageController extends Controller
{
    public function actionIndex()
    {
        return $this->render('index');
    }
    
    public function actionUpload2(){
        
        $imageData = \Yii::$app->request->post('imagex');
        
        if($imageData != null){
            
            echo  "have data";
        }
        else
            echo "no data";
          $newFileName = $value['ezf_field_name'].'_'.($i+1).'_'.$model->id.'_'.date("Ymd_His").(microtime(true)*10000) . '.' . $fileBg;
           $fullPath = Yii::$app->basePath . '/../backend/web/fileinput/' . $newFileName;
           $fieldname = 'SDDynamicModel[' . $value['ezf_field_name'] . '][' . $i . ']';

            $file = UploadedFile::getInstanceByName($fieldname);
            $file->saveAs($fullPath);
    
    }
    
    public function  actionUpload(){
        
            $model = new UploadForm();

        if (Yii::$app->request->isPost) {
            $model->imageFile = UploadedFile::getInstance($model, 'imageFile');
            if ($model->upload()) {
                // file is uploaded successfully
                return;
            }
        }

        return $this->render('upload', ['model' => $model]);
    }
    
}
