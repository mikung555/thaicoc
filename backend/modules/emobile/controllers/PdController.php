<?php

namespace frontend\modules\emobile\controllers;
use yii\web\Controller;
use Yii;
use frontend\modules\emobile\models\UploadForm;
use yii\web\UploadedFile;
use common\lib\codeerror\helpers\GenMillisecTime;

class PdController extends \yii\web\Controller
{    
    public $enableCsrfValidation = false;
    
        public function beforeAction($action)
    {
            $this->enableCsrfValidation = false;
       

        if (parent::beforeAction($action)) {
            if (in_array($action->id, array('create', 'update'))) {

            }
            return true;
        } else {
            return false;
        }
       
    }

    public function actionIndex()
    {
        return $this->render('index');
    }
    
    public function actionSentPd(){
         $this->enableCsrfValidation = false;
      //   $request = \Yii::$app->request;
        $cid =  \Yii::$app->request->post('cid');
        $id =   \Yii::$app->request->post('q');
        $pdmode=  \Yii::$app->request->post('pdmode');
        $volumein=   \Yii::$app->request->post('volumein');
       $volumeout=   \Yii::$app->request->post('volumeout');
        //        \appxq\sdii\utils\VarDumper::dump($volume);
        Yii::$app->db->createCommand("SET @run_balqty :=null;")->execute();
       $sql = "INSERT INTO pddata (id, cid, pdmode, volumein,volumeout)
         VALUES (:id, :cid, :pdmode, :volumein , :volumeout); ";
        $data=\Yii::$app->db->createCommand($sql,
                [':id'=>$id,
                 ':cid'=>$cid,
                 ':pdmode'=>$pdmode,
                 ':volumein'=>$volumein,
                 ':volumeout'=>$volumeout   ])->execute();
        
    }
    
        public function actionTest(){
            return "ok";
       
    }
    
      public function actionSent(){
      
        
        
    }
    
    
        public function  actionUpload(){ 
                     $this->enableCsrfValidation = false;
           $uploadForm =  \Yii::$app->request->post('UploadForm');

         $model = new UploadForm();
//         $cid = $model->cid;
        if (Yii::$app->request->isPost) {
            $model->imageFile = UploadedFile::getInstance($model, 'imageFile');
            $u = \Yii::$app->request->post("UploadForm");
            return $model->upload()."-". $uploadForm['cid'];
            
        }

       return $this->render('upload', ['model' => $model]);
    }
    
    
        public function actionUpload2(){
         $this->enableCsrfValidation = false;
      //   $request = \Yii::$app->request;
        $uploadForm =  \Yii::$app->request->post('UploadForm');
             $fileName ="www_pd"
                    ."_"
                    .GenMillisecTime::getMillisecTime()
                    . '.' . $uploadForm['imageFile']->extension;
                    $filepath =Yii::$app->basePath . '/../storage/web/source/'
                  . $fileName;
            $uploadForm['imageFile']->saveAs( $filepath );

    }
    
    public function actionUpload3(){
           $uploadForm =  \Yii::$app->request->post('UploadForm');
        
    return "cid:" . $uploadForm["cid"];
    }
    
        public function actionSmartcard(){
           $ptlink =  \Yii::$app->request->get('ptlink');
           $name =  \Yii::$app->request->get('name');
           
           $indexN = strpos($name,"Mr.") ;
           if($indexN<0)$indexN = strpos($name,"Miss") ;
           $name=substr($name,$indexN,strlen($name));
               return $this->render('smartcard', ['ptlink' => $ptlink,'name'=>$name]);

    }
    
}

//test
