<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace backend\modules\emobile\classes;
use backend\modules\emobile\classes\Map;
use yii\helpers\Url;


/**
 * Description of Map
 *
 * @author engiball
 */
class Mapjs {
    
    public static function getTemplete($hcode){
        
        $jstext = "
$.ajaxQ = (function(){
  var id = 0, Q = {};

  $(document).ajaxSend(function(e, jqx){
    jqx._id = ++id;
    Q[jqx._id] = jqx;
  });
  $(document).ajaxComplete(function(e, jqx){
    delete Q[jqx._id];
  });

  return {
    abortAll: function(){
      var r = [];
      $.each(Q, function(i, jqx){
        r.push(jqx._id);
        jqx.abort();
      });
      return r;
    }
  };

})();

";
$mapData = Map::getPlace($hcode);
foreach ($mapData as $value) {
 $jstext .= "$.ajax({url: ". "'".Url::to(['/emobile/map']) . "/hos-detail?hcode=".$value["hcode"] ."&sitecode=$hcode', success: function(result){
     potMap(result);
    }});";
 ;
}

 $jstext .= "$.ajax({url: ". "'".Url::to(['/emobile/map']) . "/hos-detail?hcode=".$hcode."&sitecode=$hcode', success: function(result){
     potMap(result,true);
    }});";




return $jstext;
        
}


}
