<?php

namespace backend\modules\emobile;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\modules\emobile\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
