<?php

namespace frontend\modules\emobile;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'frontend\modules\emobile\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
