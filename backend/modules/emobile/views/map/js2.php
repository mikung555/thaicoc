<?php
use frontend\modules\emobile\classes\Map;
use yii\helpers\Url;

$sitecode = Yii::$app->user->identity->userProfile->sitecode;//10668
if($sitecode === NULL) $sitecode='10668';
$mapData = Map::getPlace($sitecode);
$mapData0 = $mapData[0];
$hosDetail = Map::hDetail($sitecode,$sitecode);
//appxq\sdii\utils\VarDumper::dump($mapData);
$i = 0;
?>
var centerText="";
var mainSite='<?= $sitecode ?>';
var stopHosAjax= false;
var stopPepleAjax= false;
var hosMode = true;
var clat ='<?= $hosDetail[0]["lat"] ?>';
var clng = '<?= $hosDetail[0]["lng"] ?>';
var mymap = L.map('mapid',{fullscreenControl: true}).setView([clat,clng], 11).on('click', L.bind(onClick, null,1234));


var markers= L.layerGroup().addTo(mymap);
var paths= L.layerGroup().addTo(mymap);
L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
maxZoom: 18,
id: 'mapbox.streets'
}).addTo(mymap);

function resetLayer(){
mymap.removeLayer(markers);
mymap.removeLayer(paths);
markers =null;
paths =null;
markers= L.layerGroup().addTo(mymap); 
paths = L.layerGroup().addTo(mymap);
}

function onClick(newPopup,marker) {
try {
marker.bindPopup(newPopup, {autoClose:false,autoPan:false,offset:[0,-26]})
.openPopup();}
catch(err) {
}

}

function callHos(id,lat,lng,first) {
clat=lat;
clng=lng;
if(!first)$.ajaxQ.abortAll();
mainSite=id;

resetLayer();
 
 var centerUrl ="<?=Url::to(['/emobile/map/hos-detail']) ?>?hcode="+mainSite+"&sitecode=" +mainSite;
$.ajax({url:centerUrl, 
success: function(result){
     potMap(result,true);
    }});

$.ajax({
url: "<?=Url::to(['/emobile/map']) ?>/get-place?hcode="+id
})
.done(function( data ) {
var pointArrayAll = eval("["+data + "]");
pointArray0 =pointArrayAll[0]
for(var i =0 ; i < pointArray0.length;i++){
if(mainSite == id){

 var callUrl ="<?=Url::to(['/emobile/map']) ?>/hos-detail?hcode="+pointArray0[i]['hcode']+"&sitecode=" +mainSite;
$.ajax({url:callUrl, 
success: function(result){
     potMap(result);
    }});
    
}
}
});}

function callPeople(id) {
$.ajaxQ.abortAll();

resetLayer();
$.ajax({
url: "<?=Url::to(['/emobile/map']) ?>/hos-detail?hcode="+id
})
.done(function( data ) {
setCenter(data);
});
}

function setCenter(data){
console.log("center" +data);
}

var pIcon = new L.Icon({
iconUrl: 'https://storage.thaicarecloud.org/img/pdimg/www_pd_1497865633025756900.png',
shadowUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-shadow.png',
iconSize: [20, 25],
iconAnchor: [8, 25],
popupAnchor: [0,0],
shadowSize: [0, 0]
});

var hIcon = new L.Icon({
iconUrl: 'https://storage.thaicarecloud.org/img/pdimg/www_pd_1497865449012356200.png',
shadowUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-shadow.png',
iconSize: [30, 30],
iconAnchor: [12, 30],
popupAnchor: [0,0],
shadowSize: [0, 0],
labelAnchor: [14, 0]
});

function getNewPoints(hcode,mClat,mClng){
$.ajaxQ.abortAll();

clat = mClat;
clng = mClng;
stopHosAjax=true;
//hosMode=false;
mymap.removeLayer(markers);
mymap.removeLayer(paths);
markers =null;
paths =null;
markers= L.layerGroup().addTo(mymap); 
paths = L.layerGroup().addTo(mymap);
$.ajax({
url: "<?=Url::to(['/emobile/map']) ?>/hos-detail?hcode="+hcode+"&sitecode="+mainSite
})
.done(function( result ) {
  potMap(result,true);
});
$.ajax({
url: "<?=Url::to(['/emobile/map']) ?>/get-people-point?sitecode="+hcode
})
.done(function( data ) {
//if(hosMode)return;
var pointArrayAll = eval("["+data + "]");
var pointArray =pointArrayAll[0][1];
console.log("test"+clat);

mymap.panTo(new L.LatLng(clat, clng));
for(var i =1 ; i < pointArray.length;i++){

var marker5555 =L.marker([pointArray[i]["lat"],pointArray[i]["lng"]], {icon: pIcon}).addTo(markers).on('click', L.bind(onClick,null, 1234));
var mLng = (parseFloat(pointArray[i]["lng"])+parseFloat(clng))/2;
var path = L.curve(['M',[clat, clng],
'Q',[pointArray[i]["lat"],mLng],
    [pointArray[i]["lat"],pointArray[i]["lng"]],
], {dashArray: 5, animate: {duration: 10000, iterations: Infinity}}).addTo(paths);
}

});

}
function potMap(data,isCenter) {
//console.log(data);
var pointArrayAll = eval("["+data + "]");
//console.log(pointArrayAll[0][0]['pcount']);
if(!hosMode)return;

if(pointArrayAll[0][0]['sitecode'].length<5)pointArrayAll[0][0]['sitecode']="0"+pointArrayAll[0][0]['sitecode'];
if(  !isCenter && mainSite != pointArrayAll[0][0]['sitecode']){
console.log( isCenter + " " + mainSite + " " +  pointArrayAll[0][0]['sitecode']);
return;
}

if(isCenter){
clat =pointArrayAll[0][0]['lat'];
 clng =pointArrayAll[0][0]['lng'];
}


if ((typeof(pointArrayAll[0][0]['lat']) !== 'undefined') && (pointArrayAll[0][0]['lat'] !== null)){

var detailText = pointArrayAll[0][0]['hname']

+ "(<a href=\'#maptdc-report\' onclick=\"callHos('"
+ pointArrayAll[0][0]['hcode'] +"'"
+','
+pointArrayAll[0][0]['lat']
+','
+pointArrayAll[0][0]['lng']
+ ')\">ค้นหาข้อมูลไซต์นี้</a>) ' + '</br>'
+"ดูแลผู้ป่วยทั้งหมด<a href=\'#maptdc-report\' onclick=\"getNewPoints(\'" 
                       +pointArrayAll[0][0]['hcode']+"\'"
                       +','
                       +pointArrayAll[0][0]['lat']
                       +','
                       +pointArrayAll[0][0]['lng']
                       +")\"> "
    + pointArrayAll[0][0]['pcount'] +" </a>คน </br>"
+"รับผู้ป่วย "+pointArrayAll[0][0]['pin'] +" คน (รวม " +pointArrayAll[0][0]['rin']  +" ครั้ง)</br>"
+"ส่งผู้ป่วย "+pointArrayAll[0][0]['pout'] +" คน (รวม " +pointArrayAll[0][0]['rout'] +" ครั้ง)"
;

var detailText2=pointArrayAll[0][0]['hcode'];
var marker5555 =L.marker([pointArrayAll[0][0]["lat"],pointArrayAll[0][0]["lng"]], {icon: hIcon})
.addTo(markers)
.bindPopup(detailText,{offset:[0,-15]})
.bindTooltip(detailText2, {permanent: false, className: "my-label", offset: [0, 0],opacity:0.5 });
//marker5555.on('click', L.bind(onClick,null,detailText, marker5555));


var mLng = (parseFloat(pointArrayAll[0][0]["lng"])+parseFloat(clng))/2;
var path = L.curve(['M',[clat, clng],
'Q',[pointArrayAll[0][0]["lat"],mLng],
[pointArrayAll[0][0]["lat"],pointArrayAll[0][0]["lng"]],
], {dashArray: 10}).addTo(paths);
}
if(isCenter){
centerText=pointArrayAll[0][0]['hname'];
var popup = L.popup({offset:[0,-15]})
    .setLatLng([clat,clng])
    .setContent(centerText)
    .openOn(mymap);

}

}

function centerPlot(hcode,hname,lat,lng,pin,rin,pout,rout,pcount){
clat=lat;
clng=lng;
var detailText = hname
+ "(<a href=\'#maptdc-report\' onclick=\"callHos("
+ hcode+','
+ lat+','
+ lng
+ ')\">ค้นหาข้อมูลไซต์นี้</a>) ' + '</br>'
+"ดูแลผู้ป่วยทั้งหมด<a href=\'#maptdc-report\' onclick=\"getNewPoints(\'" 
                       +hcode+"\'"
                       +','
                       +lat
                       +','
                       +lng
                       +")\"> "
    + pcount +" </a>คน </br>"
+"รับผู้ป่วย "+pin +" คน (รวม " +rin  +" ครั้ง)</br>"
+"ส่งผู้ป่วย "+pout +" คน (รวม " +rout +" ครั้ง)"
;
var detailText2 = hname;
centerText=hname;
//var marker5555 =L.marker([lat,lng], {icon: hIcon}).addTo(markers)
//marker5555.on('click', L.bind(onClick,null, detailText,marker5555));


}

function searchCenter(mSitecode){
 resetLayer();
$.ajax({
url: "<?=Url::to(['/emobile/map']) ?>/hos-detail?hcode="+mSitecode+"&sitecode="+mSitecode
})
.done(function( result ) {
  potMap(result,true);
  callHos(mSitecode,clat,clng);

});

console.log("clat:" +clat+" clng:"+clng);


}
<?php
echo "centerPlot($sitecode,'" 
        .$hosDetail[0][hname]."',"
          .$hosDetail[0][lat] .","
        . $hosDetail[0][lng].","
        . $hosDetail[0][pin].","
        . $hosDetail[0][rin] .","
        . $hosDetail[0][pout].","
        . $hosDetail[0][rout].","
        . $hosDetail[0][pcount]
        .");";
