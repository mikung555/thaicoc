<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\modules\emobile\models\InvUser */

$this->title = Yii::t('backend', 'Create Inv User');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Inv Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="inv-user-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
