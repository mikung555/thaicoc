<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\modules\emobile\models\InvUser */

$this->title = Yii::t('backend', 'Update {modelClass}: ', [
    'modelClass' => 'Inv User',
]) . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Inv Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('backend', 'Update');
?>
<div class="inv-user-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
