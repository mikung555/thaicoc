<?php
/* @var $this yii\web\View */
?>
<h1>ckd/index</h1>

<?php

    use miloschuman\highcharts\Highcharts;

echo Highcharts::widget([
        'options' => [
            'title' => ['text' => 'ค่าระดับนํ้าตาลส่วนบุคล'],
            'xAxis' => [
                'categories' => ['28/02', '01/02', '02/03']
            ],
            'yAxis' => [
                'title' => ['text' => 'ค่าระดับนํ้าตาล']
            ],
            'series' => [
                ['name' => 'ระดับนํ้าตาล', 'data' => [180, 190, 204]],
                ['name' => 'ระดับนํ้าตาลสะสม', 'data' => [215, 127, 130]]
            ]
        ]
    ]);
    ?>