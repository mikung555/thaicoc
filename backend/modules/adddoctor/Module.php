<?php
/*
 * Module Add doctor.
 * Developed by Mark.
 * Date : 2016-03-25
*/

namespace backend\modules\adddoctor;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\modules\adddoctor\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
