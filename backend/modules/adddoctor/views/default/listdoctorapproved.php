<?php
/**
 * Created by PhpStorm.
 * User: Mark
 * Date: 29-Apr-16
 * Time: 16:21
 */

use yii\helpers\Url;

$this->registerJs('
    $(".searchDoctorApproved").keyup(function(){
        var pincodeSearchDoctorApproved = $(".pincodeSearchDoctorApproved").val();
        var doctorNameSearchDoctorApproved = $(".doctorNameSearchDoctorApproved").val();
        if(pincodeSearchDoctorApproved!="" || doctorNameSearchDoctorApproved!=""){
            $.ajax({
                type    : "POST",
                cache   : false,
                url     : "'.Url::to('/adddoctor/default/search-doctor-approved').'",
                data    : {
                    pincodeSearchDoctorApproved: pincodeSearchDoctorApproved,
                    doctorNameSearchDoctorApproved: doctorNameSearchDoctorApproved
                },
                success  : function(response) {
                    $(".listDoctorApproved").html(response);
                    actionButtonApprovedViewCommand();
                    checkboxAssignForm();
                },
                error : function(){
                    var strShowErr = "<div class=\"text-center\"><br><br><br><br><br><h1>Error!!!!!</h1><br><br><br><br><br></div>;"
                    $(".listDoctorApproved").html(strShowErr);
                }
            });
        }else{
            console.log("Please. Input data in field.");
        }
    });
    
    $(".btnDelete").click(function(){
        var doctorId = $(".viewDoctorDetail_doctorId").val();
        var pincode = $(".viewDoctorDetail_pincode").val();
        var doctorfullname = $(".viewDoctorDetail_doctorfullname").val();
        var dadd = $(".viewDoctorDetail_dadd").val();
        var adddby = $(".viewDoctorDetail_addby").val();
        var hospcode = $(".viewDoctorDetail_hospcode").val();
        var hospname = $(".viewDoctorDetail_hospname").val();
        var docgroup = $(".viewDoctorDetail_docgroup").val();
        var rstat = $(".viewDoctorDetail_rstat").val();

        $(".deleteDoctorApproved_doctorId").val(doctorId);
        $(".deleteDoctorApproved_pincode").val(pincode);
        $(".deleteDoctorApproved_doctorfullname").val(doctorfullname);
        $(".deleteDoctorApproved_dadd").val(dadd);
        $(".deleteDoctorApproved_addby").val(adddby);
        $(".deleteDoctorApproved_hospcode").val(hospcode);
        $(".deleteDoctorApproved_hospname").val(hospname);
        $(".deleteDoctorApproved_docgroup").val(docgroup);
        $(".deleteDoctorApproved_rstat").val(rstat);
        
        document.getElementById("doctorNameInModalDeleteDoctorApproved").innerHTML = doctorfullname;

        $(".bs-viewDoctorDetails-modal-lg").modal("hide");
        $(".bs-delete-modal-sm").modal("show");
    });
    
    $(".btnDeleteConfirm").click(function(){
        var doctorId = $(".deleteDoctorApproved_doctorId").val();
        var pincode = $(".deleteDoctorApproved_pincode").val();
        var doctorfullname = $(".deleteDoctorApproved_doctorfullname").val();
        var dadd = $(".deleteDoctorApproved_dadd").val();
        var adddby = $(".deleteDoctorApproved_addby").val();
        var hospcode = $(".deleteDoctorApproved_hospcode").val();
        var hospname = $(".deleteDoctorApproved_hospname").val();
        var docgroup = $(".deleteDoctorApproved_docgroup").val();
        var rstat = $(".deleteDoctorApproved_rstat").val();
        
        $.ajax({
            type    : "POST",
            cache   : false,
            url     : "'.Url::to('/adddoctor/default/delete-doctor-approved').'",
            data    : {
                doctorId: doctorId,
                pincode: pincode,
                doctorfullname: doctorfullname,
                dadd: dadd,
                adddby: adddby,
                hospcode: hospcode,
                hospname: hospname,
                docgroup: docgroup,
                rstat: rstat
            },
            success  : function(response) {
                console.log(response);
                if(response=="1"){
                    $("table.tableListDoctorApproved .doctorId"+doctorId).fadeOut(
                        "fast", 
                        function() { 
                            $(this).remove(); 
                        }
                    );
                    $(".bs-delete-modal-sm").modal("hide");
                }else{
                    $(".btnDeleteConfirm").attr("Disabled","");
                    document.getElementById("delete-modal-error").innerHTML = "Error!!!!";
                }
            },
            error : function(){
                $(".btnDeleteConfirm").attr("Disabled","");
                document.getElementById("delete-modal-error").innerHTML = "Error!!!!";

            }
        });
        
    });
    
    function actionButtonApprovedEditCommand(){
        $(".btnEditDoctorApprovedInModal").click(function(){
            var doctorId = $(".viewDoctorDetail_doctorId").val();
            var pincode = $(".viewDoctorDetail_pincode").val();
            var doctorfullname = $(".viewDoctorDetail_doctorfullname").val();
            var dadd = $(".viewDoctorDetail_dadd").val();
            var adddby = $(".viewDoctorDetail_addby").val();
            var hospcode = $(".viewDoctorDetail_hospcode").val();
            var hospname = $(".viewDoctorDetail_hospname").val();
            var docgroup = $(".viewDoctorDetail_docgroup").val();
            var rstat = $(".viewDoctorDetail_rstat").val();
            $(".bs-viewDoctorDetails-modal-lg").modal("hide");
            
            $.ajax({
                type    : "POST",
                cache   : false,
                url     : "'.Url::to('/adddoctor/default/request-data-for-edit-doctor-approved').'",
                data    : {
                    doctorId: doctorId,
                    source: "approved"
                },
                success  : function(response) {
                    $(".editDoctorApprovedDiv").html(response);
                    submitEditDoctor();
                },
                error : function(){
                }
            });
        });
    }
    
    function actionButtonApprovedViewCommand(){
        $(".btnViewDetail").click(function(){
            var current =  $(this);
            var doctorId = current.parent().parent().attr("doctorId");
            $.ajax({
                type    : "POST",
                cache   : false,
                url     : "'.Url::to('/adddoctor/default/request-data-for-edit-doctor').'",
                data    : {
                    doctorId: doctorId,
                    command: "showDoctorDetails"
                },
                success  : function(response) {
                    $(".viewDoctorDetailsDiv").html(response);
                    actionButtonApprovedEditCommand();
                    $(".bs-viewDoctorDetails-modal-lg").modal("show");
                },
                error : function(){
                }
            });
        });
    }
    
    function checkboxAssignForm(){
        $(".ccaCheckbox").change(function () {
            var current =  $(this);
            var doctorId = current.parent().parent().attr("doctorId");
            var fieldName = current.attr("id");
            var status = current.prop("checked");
            
            $(this).attr("disabled","");
            $.ajax({
                type    : "POST",
                cache   : false,
                url     : "'.Url::to('/adddoctor/default/assign-form').'",
                data    : {
                    doctorId: doctorId,
                    fieldName: fieldName,
                    status: status
                },
                success  : function(response) {
//                    console.log(response);
                    current.removeAttr("disabled");
                },
                error : function(){
                    alert("ไม่สามารถกำหนดงานได้");
                    current.removeAttr("disabled");
                }
            });
        });
    }
');
?>
<div class="adddoctor-default-listdoctorapproved">
    <div class="text-center">
        <h1>ค้นหาแพทย์</h1>
    </div>
    <div class="col-md-offset-3 col-md-6 form-group">
        <form id="searchDoctorApproved" class="searchDoctorApproved form-group">
            <dl class="dl-horizontal">
                <dt>เลข ว.</dt>
                <dd><input class="form-control pincodeSearchDoctorApproved" id="pincodeSearchDoctorApproved" maxlength="5" type="text"><br></dd>
                <dt>ชื่อแพทย์</dt>
                <dd><input class="form-control doctorNameSearchDoctorApproved" id="doctorNameSearchDoctorApproved" type="text"><br></dd>
            </dl>
        </form>
    </div>
    <div class="row">
        <div class="listDoctorApproved">
        </div>
    </div>
</div>

<!-- Alert modal for delete doctor approved -->
<div class="modal fade bs-delete-modal-sm" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="deleteModalLabel" style="color: red">Warning</h4>
            </div>
            <div class="modal-body">
                <mark style="background-color: red; color: white">ท่านต้องการลบ <strong><ins>แพทย์</ins></strong> รายนี้ ?</mark>
                <h3><p class="doctorNameInModalDeleteDoctorApproved" id="doctorNameInModalDeleteDoctorApproved"></p></h3>
                <h4><div id="delete-modal-error" class="delete-modal-error" style="background-color: red; color: white"></div></h4>

                <input type="text" id="deleteDoctorApproved_doctorId" class="deleteDoctorApproved_doctorId" disabled style="display: none;">
                <input type="text" id="deleteDoctorApproved_pincode" class="deleteDoctorApproved_pincode" disabled style="display: none;">
                <input type="text" id="deleteDoctorApproved_doctorfullname" class="deleteDoctorApproved_doctorfullname" disabled style="display: none;">
                <input type="text" id="deleteDoctorApproved_dadd" class="deleteDoctorApproved_dadd" disabled style="display: none;">
                <input type="text" id="deleteDoctorApproved_addby" class="deleteDoctorApproved_addby" disabled style="display: none;">
                <input type="text" id="deleteDoctorApproved_hospcode" class="deleteDoctorApproved_hospcode" disabled style="display: none;">
                <input type="text" id="deleteDoctorApproved_hospname" class="deleteDoctorApproved_hospname" disabled style="display: none;">
                <input type="text" id="deleteDoctorApproved_docgroup" class="deleteDoctorApproved_docgroup" disabled style="display: none;">
                <input type="text" id="deleteDoctorApproved_rstat" class="deleteDoctorApproved_rstat" disabled style="display: none;">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger btnDeleteConfirm">Confirm delete</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>