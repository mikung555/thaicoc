<!-- /*
 * Module Add doctor.
 * Developed by Mark.
 * Date : 2016-04-25
*/ -->
<?php
use yii\helpers\Url;
use kartik\tabs\TabsX;

$this->title = Yii::t('', 'ส่งคำร้องขอเพิ่มแพทย์');

echo TabsX::widget([
    'items'=>$menuitems,
    'position'=>TabsX::POS_ABOVE,
    'align'=>TabsX::ALIGN_LEFT,
    'encodeLabels'=>false,
    'bordered'=>false
]);

$this->registerJs('    
    function submitEditDoctor(){
        $(".editDoctorRequest").submit(function(){
            var doctorId = $("#editDoctorIdDoctorRequest").val();
            var pincode = $("#editPincodeDoctorRequest").val();
            var doctorName = $("#editDoctorNameDoctorRequest").val();
            var hospital = $("#select2-editHospitalDoctorRequest-container").attr("title");
            var docgroup = $("#editDocgroupDoctorRequest").val();
            if(docgroup=="other"){
                var docgroupOther = $("#editDocgroupOtherDoctorRequest").val();
                docgroup = docgroupOther;
                console.log(doctorId+" "+pincode+" "+doctorName+" "+hospital+" "+docgroup+" "+docgroupOther);
            }else{
                console.log(doctorId+" "+pincode+" "+doctorName+" "+hospital+" "+docgroup);
            }
            
            $.ajax({
                type    : "POST",
                cache   : false,
                url     : "'.Url::to('/adddoctor/default/update-doctor-request').'",
                data    : {
                    doctorId: doctorId,
                    pincode: pincode,
                    doctorName: doctorName,
                    hospital: hospital,
                    addby: "'.(Yii::$app->user->identity->id).'",
                    docgroup: docgroup
                },
                success  : function(response) {
                    if(response=="1"){
                        window.location = "'.Yii::$app->request->url.'";
                    }
                },
                error : function(){
                }
            });
            
            return false;
        });
        
        $(".btnEditDoctorApproved").click(function(){
            var oldSource = $("#oldDataSourceDoctorApproved").val();
            var oldDoctorId = $("#oldDataDoctorIdDoctorApproved").val();
            var oldPincode = $("#oldDataPincodeDoctorApproved").val();
            var oldDoctorfullname = $("#oldDataDoctorfullnameDoctorApproved").val();
            var oldHospital = $("#oldDataHospitalDoctorApproved").val();
            var oldDocgroup = $("#oldDataDocgroupDoctorApproved").val();
            
            var pincode = $("#editPincodeDoctorApproved").val();
            var doctorName = $("#editDoctorNameDoctorApproved").val();
            var hospital = $("#select2-editHospitalDoctorApproved-container").attr("title");
            var docgroup = $("#editDocgroupDoctorApproved").val();
            if(docgroup=="other"){
                var docgroupOther = $("#editDocgroupOtherDoctorApproved").val();
                docgroup = docgroupOther;
            }            
            
            $.ajax({
                type    : "POST",
                cache   : false,
                url     : "'.Url::to('/adddoctor/default/receive-request-edit-doctor-approved').'",
                data    : {
                    oldSource: oldSource,
                    oldDoctorId: oldDoctorId,
                    oldPincode: oldPincode,
                    oldDoctorfullname: oldDoctorfullname,
                    oldHospital: oldHospital,
                    oldDocgroup: oldDocgroup,
                    pincode: pincode,
                    doctorName: doctorName,
                    hospital: hospital,
                    docgroup: docgroup
                },
                success  : function(response) {
                    console.log(response);
                    if(response=="1"){
                        window.location = "'.Yii::$app->request->url.'";
                    }
                },
                error : function(){
                }
            });
            
            return false;
        });
    }
');
?>

<!-- Edit doctor -->
<!--tabindex="-1"-->
<div class="modal fade bs-editDoctorRequest-modal-lg" id="bs-editDoctorRequest-modal-lg" role="dialog" aria-labelledby="editDoctorRequestModalLabel">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="editDoctorRequestModalLabel">Edit doctor request.</h4>
            </div>
            <div class="modal-body">
                <div class="editDoctorRequestDiv"></div>
            </div>
            <!--            <div class="modal-footer">-->
            <!--                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
            <!--                <button type="button" class="btn btn-success">Approve</button>-->
            <!--            </div>-->
        </div>
    </div>
</div>


<!-- View doctor -->
<!--tabindex="-1"-->
<div class="modal fade bs-viewDoctorDetails-modal-lg" id="bs-viewDoctorDetails-modal-lg" role="dialog" aria-labelledby="viewDoctorDetailsModalLabel">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="viewDoctorDetailsModalLabel">View doctor detail.</h4>
            </div>
            <div class="modal-body">
                <div class="viewDoctorDetailsDiv"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <?= (Yii::$app->user->can('adminsite')=='1'?('<button type="button" class="btn btn-danger btnDelete" id="btnDelete">Delete</button>'):"") ?>
            </div>
        </div>
    </div>
</div>


<!-- Edit doctor approved -->
<!--tabindex="-1"-->
<div class="modal fade bs-editDoctorApproved-modal-lg" id="bs-editDoctorApproved-modal-lg" role="dialog" aria-labelledby="editDoctorApprovedModalLabel">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="editDoctorApprovedModalLabel">Request for edit doctor.</h4>
            </div>
            <div class="modal-body">
                <div class="editDoctorApprovedDiv"></div>
            </div>
            <!--            <div class="modal-footer">-->
            <!--                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
            <!--                <button type="button" class="btn btn-success">Approve</button>-->
            <!--            </div>-->
        </div>
    </div>
</div>