<?php
/**
 * Created by PhpStorm.
 * User: Mark
 * Date: 23-May-16
 * Time: 14:21
 */

use app\modules\adddoctor\models\AllHospitalThai;

use kartik\widgets\Select2;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;
?>
<form class="editDoctorRequest form-horizontal" id="editDoctorRequest" method="get">
    <input class="form-control editDoctorIdDoctorRequest" id="editDoctorIdDoctorRequest" type="text" maxlength="5" value="<?= $qryDoctor[0]['id'] ?>" style="display: none">
    <div class="form-group">
        <label for="editPincodeDoctorRequest" class="col-sm-3 control-label">เลข ว.</label>
        <div class="col-sm-9">
            <input class="form-control editPincodeDoctorRequest" id="editPincodeDoctorRequest" type="text" maxlength="5" value="<?= $qryDoctor[0]['pincode'] ?>">
        </div>
    </div>
    <div class="form-group">
        <label for="editDoctorNameDoctorRequest" class="col-sm-3 control-label">ชื่อแพทย์</label>
        <div class="col-sm-9">
            <input class="form-control editDoctorNameDoctorRequest" id="editDoctorNameDoctorRequest" type="text" value="<?= $qryDoctor[0]['doctorfullname'] ?>" required>
        </div>
    </div>
    <div class="form-group">
        <label for="editHospitalDoctorRequest" class="col-sm-3 control-label">โรงพยาบาล</label>
        <div class="col-sm-9">
            <?php
            //                            \yii\helpers\VarDumper::dump($qryDoctor,10,true);
            // The controller action that will render the list
            $url = \yii\helpers\Url::to(['default/hospital-list']);
            //
            //                            // Get the initial city description
            $model->hcode = $qryDoctor[0]['hospcode'];
            $hospitalName = empty($model->hcode) ? '' : AllHospitalThai::findOne($model->hcode)->name;
            //                            echo $hospitalName;
            //
            $formEditHospital = ActiveForm::begin();
            //
            echo $formEditHospital->field($model, 'hcode')->widget(Select2::className(), [
                'initValueText' => $model->hcode." : ".$hospitalName, // set the initial display text
                'options' => [
                    'name' => 'editHospitalDoctorRequest',
                    'id' => 'editHospitalDoctorRequest',
                    'class' => 'editHospitalDoctorRequest',
                    'placeholder' => 'Search for a hospital ...'
                ],
                'pluginOptions' => [
                    'allowClear' => true,
                    'minimumInputLength' => 3,
                    'language' => [
                        'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
                    ],
                    'ajax' => [
                        'url' => $url,
                        'dataType' => 'json',
                        'data' => new JsExpression('function(params) { return {keyName:params.term}; }')
                    ],
                    'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                    'templateResult' => new JsExpression('function(AllHospitalThai) { return AllHospitalThai.text; }'),
                    'templateSelection' => new JsExpression('function (AllHospitalThai) { return AllHospitalThai.text; }'),
                ],
            ])->label(false);
            ?>
        </div>
    </div>
    <div class="form-group">
        <label for="editDocgroupDoctorRequest" class="col-sm-3 control-label">ประเภทความถนัด</label>
        <div class="col-sm-9">
            <select class="form-control editDocgroupDoctorRequest" id="editDocgroupDoctorRequest" >
                <option value="">Select doctor group</option>
                <option value="chemo" <?= $qryDoctor[0]['docgroup']=='chemo'?"selected":"" ?>>Chemotherapy</option>
                <option value="general" <?= $qryDoctor[0]['docgroup']=='general'?"selected":"" ?>>General Medical</option>
                <option value="radio" <?= $qryDoctor[0]['docgroup']=='radio'?"selected":"" ?>>Radiologist</option>
                <option value="surgery" <?= $qryDoctor[0]['docgroup']=='surgery'?"selected":"" ?>>Surgery</option>
                <option value="other" <?= (
                    $qryDoctor[0]['docgroup']!='chemo'&&
                    $qryDoctor[0]['docgroup']!='general'&&
                    $qryDoctor[0]['docgroup']!='radio'&&
                    $qryDoctor[0]['docgroup']!='surgery'
                )?"selected":"" ?>>Other</option>
            </select>
            <input class="form-control editDocgroupOtherDoctorRequest" id="editDocgroupOtherDoctorRequest" type="text" value="<?= $qryDoctor[0]['docgroup'] ?>" placeholder="please specify doctor group." <?=
            (
                $qryDoctor[0]['docgroup']!='chemo'&&
                $qryDoctor[0]['docgroup']!='general'&&
                $qryDoctor[0]['docgroup']!='radio'&&
                $qryDoctor[0]['docgroup']!='surgery'
            )?"":"style='display: none;'" ?>>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label"></label>
        <div class="col-sm-9">
            <button type="submit" class="form-control btn btn-primary btnEditDoctorRequest" id="btnEditDoctorRequest">Edit</button>
        </div>
    </div>
</form>
<?php
$formEditHospital = ActiveForm::end();
?>