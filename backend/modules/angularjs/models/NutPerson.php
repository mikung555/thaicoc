<?php

namespace backend\modules\angularjs\models;

use Yii;

/**
 * This is the model class for table "nut_person".
 *
 * @property integer $id
 * @property string $fname
 * @property string $lname
 */
class NutPerson extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'nut_person';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fname', 'lname'], 'required'],
            [['fname', 'lname'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fname' => 'Fname',
            'lname' => 'Lname',
        ];
    }
}
