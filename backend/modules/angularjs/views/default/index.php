<?php
    use yii\helpers\Html;
    use yii\helpers\Url;
    $this->title="Angularjs";
?>
<div class="panel panel-primary">
    <div class="panel-heading"><?= Html::encode($this->title); ?></div>
    <div class="panel-body">
        <?php echo $this->renderAjax('test',['model'=>$model])?>
    </div>
</div>