<?php

namespace backend\modules\angularjs\controllers;

use yii\web\Controller;

class DefaultController extends Controller
{
    public function actionIndex()
    {
        $user = \common\models\User::find()->one();
         
        return $this->render('test',[
            'user'=>$user
        ]);
    } 
    public function actionView2(){
        
        return $this->renderPartial('view2');
    }
    
    
}
