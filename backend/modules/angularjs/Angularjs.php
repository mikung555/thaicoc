<?php

namespace backend\modules\angularjs;

class Angularjs extends \yii\base\Module
{
    public $controllerNamespace = 'backend\modules\angularjs\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
