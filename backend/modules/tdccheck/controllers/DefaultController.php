<?php

namespace backend\modules\tdccheck\controllers;

use yii\web\Controller;
use Yii;

class DefaultController extends Controller {
    public function actionIndex() {
    
        //$sitecode = Yii::$app->user->identity->userProfile->sitecode;
        $connection = new \yii\db\Connection([
            'dsn' => "mysql:host=61.19.254.8"  . ";dbname=buffe_webservice"  ,
            'username' => 'webservice',
            'password' => 'webservice!@#$%',
            'charset' => 'utf8',
        ]);
        $connection->open();
        $sitecode = "'10980','11020','04571','04070','11052','05277',
            '11076','13958','04938','05274','04944','05330',
            '05318','05329','13964','05278','05279','05326',
            '05320','13957','11078','05275','05331','05214',
            '05319','05317','05321','04059','04061','05276',
            '05273','00473','00478'";
        $tdc_count = $connection->createCommand("SELECT c.`id`,ah.`name`,c.last_ping,c.his_type,h.his_name,ah.zone_code,ah.provincecode,ah.province
,SUM(ts.his_record) as his_record,SUM(ts.record) as record,SUM(ts.qleft) as qleft,ROUND((SUM(ts.his_record)/(SUM(ts.record)+SUM(ts.qleft)))*100,2) as progress
            ,(last_ping > DATE_ADD(NOW(),INTERVAL -10 MINUTE)) as status
            FROM buffe_config c 
            INNER JOIN buffe_his h ON c.his_type = h.id
            INNER JOIN buffe_webservice.all_hospital_thai_general ah ON ah.hcode = c.`id`
INNER JOIN buffe_webservice.buffe_table_server ts ON ts.sitecode = c.`id` 
            WHERE c.`id` IN ($sitecode) AND date(last_ping) = CURDATE() GROUP BY c.`id` ORDER BY status DESC #AND last_ping > DATE_ADD(NOW(),INTERVAL -10 MINUTE)")->queryAll();
                
        //$tdc_count = $connection->createCommand("SELECT c.`id`,ah.`name`,c.last_ping,c.his_type,h.his_name,ah.zone_code,ah.provincecode,ah.province
          //  ,(last_ping > DATE_ADD(NOW(),INTERVAL -10 MINUTE)) as status
           // FROM buffe_config c 
            //INNER JOIN buffe_his h ON c.his_type = h.id
            //INNER JOIN buffe_webservice.all_hospital_thai_general ah ON ah.hcode = c.`id`
            //WHERE c.`id` IN ($sitecode) ORDER BY status DESC #AND last_ping > DATE_ADD(NOW(),INTERVAL -10 MINUTE)")->queryAll();
        
        //$query = $connection->createCommand("SELECT c.sitecode,c.`table`,c.his_record,c.record,c.qleft,c.progress
          //  FROM buffe_table_server c
            //WHERE sitecode = :sitecode")->bindValues([':sitecode'=> $dataGet['sitecode'] ])->queryAll();
        
        
//        \appxq\sdii\utils\VarDumper::dump($dataProvider);    
        return $this->render('index',[
//            'dataProvider' => $dataProvider
            'tdc_count' => $tdc_count
        ]);
    }
    
    public function actionTdcDetail(){
        $dataGet = \Yii::$app->request->get();
        $connection = new \yii\db\Connection([
            'dsn' => "mysql:host=61.19.254.8"  . ";dbname=buffe_webservice"  ,
            'username' => 'webservice',
            'password' => 'webservice!@#$%',
            'charset' => 'utf8',
        ]);
        $connection->open();
        $query = $connection->createCommand("SELECT c.sitecode,c.`table`,c.his_record,c.record,c.qleft,c.progress
            FROM buffe_table_server c
            WHERE sitecode = :sitecode")->bindValues([':sitecode'=> $dataGet['sitecode'] ])->queryAll();
        
//         \appxq\sdii\utils\VarDumper::dump($query);
        $dataProvider = new \yii\data\ArrayDataProvider([
            'allModels' => $query,
            'pagination' => [
                'pageSize' => 50,
            ],
            
        ]);
        
               
        return $this->renderAjax('_detail',[
            'dataProvider' => $query,
            'sitecode' => $dataGet['sitcode'],
        ]);
    }
    
    
}