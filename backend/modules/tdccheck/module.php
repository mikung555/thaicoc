<?php

namespace backend\modules\tdccheck;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\modules\tdccheck\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
