<?php foreach ($dataProvider as $valdetail) {  //style="display:none"   ?>
    <tr class="detail" id="<?= $valdetail['sitecode']; ?>" style="background-color: #FFEFC1;">
        <td></td>
        <td colspan="3"><?= $valdetail['table'] ?></td>
        <td><?= $valdetail['his_record'] ?></td>
        <td><?= $valdetail['record'] ?></td>
        <td><?= $valdetail['qleft'] ?></td>
        <?php IF ($valdetail['progress'] > 0) { ?>
            <td>
                <div class="progress">
                    <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="<?= $valdetail['progress']; ?>"
                         aria-valuemin="0" aria-valuemax="100" style="width:<?= $valdetail['progress']; ?>%;min-width: 2em;">
                        <?= $valdetail['progress']; ?> %
                    </div>
                </div>
            </td>
        <?php } ?>
    <tr>
<?php } ?>