<?php
use yii\helpers\Html;
use yii\helpers\Url;
use kartik\grid\GridView;
use appxq\sdii\widgets\ModalForm;
use common\lib\sdii\components\utils\SDdate;

$this->title = 'TDC_CHECK';

?>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                TDC 
                <div class="pull-right">
                    <label> สถานะ :: </label>
                    <i class='fa fa-circle fa-lg' aria-hidden='true' style='color:green'></i> <label style='color:green'> เปิด TDC </label>
                    <i class='fa fa-circle fa-lg' aria-hidden='true' style='color:red'></i> <label style='color:red'> ปิด TDC </label>
                </div>
            </div>
            <div class="panel-body">
                <div class="col-md-12">
                    <table class="table tab-bordered table-hover">
                        <thead>
                            <tr>
                                <th width='3%'>#</th>
                                <th width='33%'>หน่วยงาน</th>
                                <th width='14%'>status</th>
                                <th width='5%'>his</th>
                                <th width='12%'>จำนวนข้อมูลใน รพ.</th>
                                <th width='10%'>จำนวนที่นำเข้า</th>
                                <th width='10%'>จำนวนคิวที่เหลือ</th>
                                <th width='8%'>Progress</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i=1; foreach ($tdc_count as $val ){  ?>
                            <tr class="title" id="<?=$val['id'];?>" style="cursor:pointer"> 
                                <?= Html::hiddenInput('sitecode', $val['id']);?>
                                <?php //Html::hiddenInput('id', $i);?>
                                <td><?=$i;?></td>
                                <td><?php $sitecode = \backend\modules\ezforms\components\EzformQuery::getHospital($val['id']); echo $sitecode['hcode']." ".$sitecode['name'] ?></td>
                                <td>
                                    <?php IF($val['status'] == '1'){
                                            echo "<i class='fa fa-circle fa-lg' aria-hidden='true' style='color:green'></i> ";
                                            echo SDdate::mysql2phpThDateSmall($val['last_ping']);
                                            echo substr($val['last_ping'],10);
                                        }ELSE IF($val['status'] == '0'){
                                            echo "<i class='fa fa-circle fa-lg' aria-hidden='true' style='color:red'></i> ";
                                            echo SDdate::mysql2phpThDateSmall($val['last_ping']);
                                            echo substr($val['last_ping'],10);
                                        }
                                    ?>
                                </td>
                                <td><?=$val['his_name'];?></td>
                                <td align="right"><?=number_format($val['his_record']);?></td>
                                <td align="right"><?=number_format($val['record']);?></td>
                                <td align="right"><?=number_format($val['qleft']);?></td>
                                <?php IF ($val['progress'] > 0){ ?>
                                <td>
                                    <div class="progress">
                                        <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="<?=$val['progress'];?>"
                                        aria-valuemin="0" aria-valuemax="100" style="width:<?=$val['progress'];?>%;min-width: 2em;">
                                          <?=$val['progress'];?> %
                                        </div>
                                    </div>
                                </td>
                                
                                <?php }?>
                            </tr>
                            <?php $i++; } ?>
                        </tbody>
                    </table>
                </div>
<!--                <div class="col-md-5">
                    
                </div>-->
            </div>
        </div>
    </div>
    <div class="col-md-6">
        
    </div>
</div>
<!--<div class="modal fade" id="modal-detail" role="dialog">
    <div class="modal-dialog" style="width:50%;">
        <div class="modal-content">

        </div>
    </div>
</div> -->
<?php echo  \appxq\sdii\widgets\ModalForm::widget([
    'id'=> 'modal-detail',
    'size' => 'modal-lg', 
]);
        ?>
<?php 
//    echo GridView::widget([
//        'dataProvider'=>$dataProvider,
//        
//    ]); 
?>
<?php
    $this->registerJS("
        //var sitecode = $('input[name=sitecode]').val();
      
        var x = $('input[name=i]').val();
        var url = '".Url::to(['/toolservice/tdccount/tdc-detail'])."';
                                        
        $('tbody tr.title').click(function(){
            // alert('OK');
            // var id = $(this).attr('id');
            var sitecode = $(this).attr('id');
            //alert(sitecode);
           modalGuideFieldUser(url,sitecode);
        });
        
        function modalGuideFieldUser(url,sitecode) {
            $.ajax({
                url:url,
                method:'GET',
                data: {sitecode:sitecode},
                    success:function(result){
                     //console.log(result);
                     $('#modal-detail').modal('show');
                     $('#modal-detail .modal-content').html(result);
                },error: function (xhr, ajaxOptions, thrownError) {
                   console.log(xhr);
                }
            });
        } 





//            var url = $(this).attr('url');
//            $('#modal-detail .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
//            $('#modal-detail').modal('show')
//            .find('.modal-content')
//            .load(url);

//            $.ajax({
//                url:'" . Url::to(['/toolservice/tdccount/tdc-detail']) . "',
//                method:'GET',
//                data: {sitecode:sitecode},
//                dataType:'HTML',
//                    success:function(result){
//                    $('div #detail').html(result); 
//                },error: function (xhr, ajaxOptions, thrownError) {
//                   console.log(xhr);
//                }
//            });

        
   

    ");   
    
?>