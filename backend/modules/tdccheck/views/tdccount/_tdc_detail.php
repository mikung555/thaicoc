<?php

use kartik\grid\GridView;
use Yii;
use yii\helpers\Html;

$sitecode = \backend\modules\ezforms\components\EzformQuery::getHospital($sitecode);
?>

<div class="row" style="padding: 10px;">
    <div class="col-md-12">
        <?=
        GridView::widget([
            'id' => 'buffe-table-server',
            'dataProvider' => $dataProvider,
            'tableOptions' => ['class' => 'table table-striped table-hover'],
            'panel' => [
                'heading' => '<h3 class="panel-title"><i class="fa fa-h-square" aria-hidden="true"></i> ' . $sitecode['hcode'] . " : " . $sitecode['name'] . '</h3>',
                'type' => GridView::TYPE_SUCCESS,
                'footer' => '<button type="button" class="btn btn-danger pull-right"  data-dismiss="modal">Close</button>'
            ],
            'columns' => [
                [
                    'class' => 'yii\grid\SerialColumn',
                    'headerOptions' => ['style' => 'text-align: center;'],
                    'contentOptions' => ['style' => 'width:10%; text-align: center; '],
                ], 
                [
                    'attribute' => 'table',
                    'header' => 'ชื่อตาราง',
                    'value' => function ($data) {

                        if ($data['record'] > 0) {
                            return $data['table'];
                        } else {
                            return $data['table'];
                        }
                    },
                    'format' => 'raw',
                    'contentOptions' => ['style' => 'min-width:30px;'],
                ],
                [
                    'attribute' => 'his_record',
                    'header' => 'ข้อมูลใน รพ.',
                    'value' => function ($data) {
                        return number_format($data['his_record']);
                    },
                    'headerOptions' => ['style' => 'text-align: right;'],
                    'contentOptions' => ['style' => 'min-width:80px; text-align: right;'],
                ],
                [
                    'attribute' => 'record',
                    'header' => 'นำเข้าแล้ว',
                    'value' => function ($data) {
                        return number_format($data['record']);
                    },
                    'headerOptions' => ['style' => 'text-align: right;'],
                    'contentOptions' => ['style' => 'min-width:80px; text-align: right;'],
                ],
                [
                    'attribute' => 'qleft',
                    'header' => 'คิว',
                    'value' => function ($data) {
                        return number_format($data['qleft']);
                    },
                    'headerOptions' => ['style' => 'text-align: right;'],
                    'contentOptions' => ['style' => 'min-width:80px; text-align: right;'],
                ],
                [
                    'attribute' => 'progress',
                    'header' => 'Progress',
                    'value' => function ($data) {
                        $percen = $data['progress'];
                        if ($data['progress'] > 0) {
                            $data['progress'];
                        } else {
                            return '';
                        }
                        //               
                        return '<div class="progress">
                                <div class="progress-bar" role="progressbar" aria-valuenow="' . $data['progress'] . '" aria-valuemin="0" aria-valuemax="100" style="width: ' . $data['progress'] . '%;">
                                ' . $data['progress'] . ' %
                                </div> 
                              </div>';
                    },
                    'format' => 'raw',
                    'headerOptions' => ['style' => 'text-align: center;'],
                    'contentOptions' => ['style' => 'min-width:100px;text-align: center;'],
                ],
            ],
        ]);
        ?>
    </div>
</div>
<?php 
    $this->registerJS("$('.btn-toolbar').hide();")
?>
