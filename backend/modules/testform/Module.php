<?php

namespace backend\modules\testform;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\modules\testform\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
