<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\testform\models\GuideField */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Guide Field',
]) . ' ' . $model->auto_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Guide Fields'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->auto_id, 'url' => ['view', 'id' => $model->auto_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="guide-field-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
