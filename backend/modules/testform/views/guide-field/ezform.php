<?php
use yii\helpers\Html;
use yii\widgets\Pjax;

echo $id;
?>

<div class="row">
	<div class="col-md-6 ">
	    <div class="panel panel-primary"> 
		<div class="panel-heading"> 
		    <div class="row">
			<div class="col-md-6"><h3 class="panel-title">Vitalsign</h3></div>
			<div class="col-md-6 text-right">
			    <?=  yii\helpers\Html::a('<span class="glyphicon glyphicon-plus"></span>', yii\helpers\Url::to(['/inputdata/insert-record',
				'insert'=>'url',
				'ezf_id'=>$ezform['ezf_id'],
				'target'=>base64_encode($data['ptid']),
				'comp_id_target'=>$ezform['comp_id_target'],
				'rurl'=>base64_encode(Yii::$app->request->url),
			    ]), [
				'class'=>'btn btn-success btn-sm'
				 ])?>
				<?=  yii\helpers\Html::a('<span class="glyphicon glyphicon-pencil"></span>', yii\helpers\Url::to(['/inputdata/redirect-page',
				'ezf_id'=>$ezform['ezf_id'],
				'dataid'=>$data['dataid'],
				'rurl'=>base64_encode(Yii::$app->request->url),
			    ]), [
				'class'=>'btn btn-warning btn-sm'
				 ])?>
			    <?=  yii\helpers\Html::a('<span class="glyphicon glyphicon-th-list"></span>', yii\helpers\Url::to(['/inputdata/step4',
				'ezf_id'=>$ezform['ezf_id'],
				'target'=>base64_encode($data['ptid']),
				'comp_id_target'=>$ezform['comp_id_target'],
				'rurl'=>base64_encode(Yii::$app->request->url),
			    ]), [
				'class'=>'btn btn-default btn-sm'
				 ])?>
				
			</div>
		    </div>
		</div> 
		<div class="panel-body"> 
		    <table class="table table-condensed">
			<tr>
			    <td>BP</td>
			    <th><?=$data['bp']?></th>
			    <td>/ mmHg</td>
			    <td></td>
			    <td>Pulse</td>
			    <th><?=$data['pulse']?></th>
			    <td>/min</td>
			</tr>
			<tr>
			    <td>RR</td>
			    <th><?=$data['rr']?></th>
			    <td>/min</td>
			    <td></td>
			    <td>Temp</td>
			    <th><?=$data['temp']?></th>
			    <td>°C</td>
			</tr>
		    </table>
		</div> 
	    </div>
	</div>
	<div class="col-md-6 sdbox-col">
	    <div class="panel panel-primary"> 
		<div class="panel-heading"> 
		    <div class="row">
			<div class="col-md-6"><h3 class="panel-title">BMI = ?</h3> </div>
			<div class="col-md-6 text-right">
				<button type="button" class="btn btn-success btn-sm"><span class="glyphicon glyphicon-plus"></span></button> 
				<button type="button" class="btn btn-warning btn-sm"><span class="glyphicon glyphicon-pencil"></span></button>
				<button type="button" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-th-list"></span></button>
			</div>
		    </div>
		</div> 
		<div class="panel-body"> 
		    <table class="table table-condensed">
			<tr>
			    <td>น้ำหนัก</td>
			    <th>77</th>
			    <td>ก.ก.</td>
			    <td></td>
			    <td>ส่วนสูง</td>
			    <th>180</th>
			    <td>ซ.ม.</td>
			</tr>
		    </table>
		</div> 
	    </div>
	</div>
    </div>

<?php

$sql = "SELECT * 
	FROM tb_data_1
	WHERE tb_data_1.hsitecode = :hsitecode 
	";
$sitecode = Yii::$app->user->identity->userProfile->sitecode;

$count = Yii::$app->db->createCommand('
    SELECT COUNT(*) FROM tb_data_1 WHERE tb_data_1.hsitecode = :hsitecode 
', [':hsitecode' => $sitecode])->queryScalar();

$dataProvider = new \yii\data\SqlDataProvider([
    'sql' => $sql,
    'params' => [':hsitecode' => $sitecode],
    'totalCount' => $count,
    'sort' => [
//        'attributes' => [
//            'age',
//            'name' => [
//                'asc' => ['first_name' => SORT_ASC, 'last_name' => SORT_ASC],
//                'desc' => ['first_name' => SORT_DESC, 'last_name' => SORT_DESC],
//                'default' => SORT_DESC,
//                'label' => 'Name',
//            ],
//        ],
    ],
    'pagination' => [
        'pageSize' => 100,
    ],
]);



?>
<?php  Pjax::begin(['id'=>'inv-fields-grid-pjax']);?>
<?=appxq\sdii\widgets\GridView::widget([
	'id' => 'test_regis',
	'panelBtn' => '',
	'dataProvider' => $dataProvider,
        'columns' => [
	    
	    [
			    'attribute'=>'name',
			    'label'=>'ชื่อ',
			    'headerOptions'=>['style'=>'text-align: center;'],
			    'contentOptions'=>['style'=>'width:200px; text-align: center;'],
	    ],
	    [
			    'attribute'=>'surname',
			    'label'=>'นามสกุล',
			    'headerOptions'=>['style'=>'text-align: center;'],
			    'contentOptions'=>['style'=>'width:200px; text-align: center;'],
	    ],
	    [
			    'attribute'=>'v3',
			    'label'=>'เพศ',
			    'value'=>function ($data){ 
				return isset($data['v3']) && $data['v3']==1?'ชาย':($data['v3']==2?'หญิง':''); 
				
			    },
			    'headerOptions'=>['style'=>'text-align: center;'],
			    'contentOptions'=>['style'=>'width:100px; text-align: center;'],
	    ],
	   
	    [
		'header' => 'ตรวจภายใน',
		'value' => function ($model) {
		    
		    if ($model['v3']==2) {
			return Html::button('<i class="glyphicon glyphicon-ok"></i>', [
			    'class' => 'manager-btn btn btn-xs btn-primary',
			    'data-id' => $model['id'],
			    'data-url' => yii\helpers\Url::to(['status', 'id' => $model['id']])
			]);
		    } else {
			return '';
		    }
		},
		'format' => 'raw',
		'headerOptions'=>['style'=>'text-align: center;'],
		'contentOptions'=>['style'=>'width:60px;text-align: center;'],
	    ],
        ],
    ]); ?>
<?php  Pjax::end();?>