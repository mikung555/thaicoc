<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\helpers\Url;
use common\lib\sdii\widgets\SDGridView;
use common\lib\sdii\widgets\SDModalForm;
use common\lib\sdii\components\helpers\SDNoty;
use backend\modules\article\components\ArticleFunc;
use yii\bootstrap\ActiveForm;
use appxq\sdii\helpers\SDHtml;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\article\models\ResearchSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Researches');
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="research-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php  Pjax::begin(['id'=>'research-grid-pjax']);?>
	<h4 class="page-header">งานวิจัยของฉัน</h4>
    <?= SDGridView::widget([
	'id' => 'research-grid',
	'panelBtn' => Html::a(Yii::t('app', '<span class="glyphicon glyphicon-plus"></span>'), Url::to(['research/create']), ['class' => 'btn btn-success btn-sm']),
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            [
		'class' => 'yii\grid\SerialColumn',
		'contentOptions'=>['style'=>'width:70px;text-align: center;'],
	    ],
            'res_topic:ntext',
			[
				'attribute'=>'author',
				'headerOptions'=>['style'=>'text-align: center;'],
				'contentOptions'=>['style'=>'width:120px;'],
			],
			[
				'attribute'=>'author_email',
				'headerOptions'=>['style'=>'text-align: center;'],
				'contentOptions'=>['style'=>'width:160px;'],
			],
			[
				'attribute'=>'co_auther',
				'value'=>function ($data){
					$arr = explode(',', $data['co_auther']);
					$r = '';
					if(is_array($arr)){
						$r = implode(', ', $arr);
					}
					return $r;
				},
				'headerOptions'=>['style'=>'text-align: center;'],
				'contentOptions'=>['style'=>'width:160px;'],
				//'filter'=> '',
			],
			[
				'attribute'=>'expected_completion',
				'headerOptions'=>['style'=>'text-align: center;'],
				'contentOptions'=>['style'=>'width:120px;text-align: center;'],
			],
            [
				'attribute'=>'status',
				'value'=>function ($data){
					return ArticleFunc::itemAlias('status', $data['status']);
				},
				'headerOptions'=>['style'=>'text-align: center;'],
				'contentOptions'=>['style'=>'width:140px;text-align: center;'],
				'filter'=> Html::activeDropDownList($searchModel, 'status', ArticleFunc::itemAlias('status'), ['class'=>'form-control', 'prompt'=>'All']),
			],
//			[
//				'attribute'=>'show',
//				'value'=>function ($data){
//					return ArticleFunc::itemAlias('show', $data['show']);
//				},
//				'headerOptions'=>['style'=>'text-align: center;'],
//				'contentOptions'=>['style'=>'width:140px;text-align: center;'],
//				'filter'=> Html::activeDropDownList($searchModel, 'show', ArticleFunc::itemAlias('show'), ['class'=>'form-control', 'prompt'=>'All']),
//			],
			[
				'attribute'=>'updated_at',
				'value'=>function ($data){return Yii::$app->formatter->asDate($data['updated_at'], 'd/m/Y');},
				'headerOptions'=>['style'=>'text-align: center;'],
				'contentOptions'=>['style'=>'width:100px;text-align: center;'],
				'filter'=>'',
			],
            [
				'class' => 'common\lib\sdii\widgets\SDActionColumn',
				'template'=>'{mbf} {mmf} {update} {delete}',
				'contentOptions'=>['style'=>'width:100px;text-align: center;'],
				'buttons'=>[
					
					'mbf' => function ($url, $data, $key) {
							return Html::a('<span class="fa fa-whatsapp"></span>', 'http://61.19.254.15:9001/p/mbf_'.$data['rid'], [
							'title' => 'Mock Abstract Forum',
							'target'=>'_blank'    
							]);
					},	
					'mmf' => function ($url, $data, $key) {
							return Html::a('<span class="fa fa-weixin"></span>', 'http://61.19.254.15:9001/p/mmf_'.$data['rid'], [
							'title' => 'Mock Manuscript Forum',
							'target'=>'_blank'    
							]);
					},	
				]
			],
        ],
    ]); ?>
	 
	<h4 class="page-header">งานวิจัยร่วม</h4>
	<?= SDGridView::widget([
	'id' => 'research-co-grid',
	'panelBtn' => Html::a(Yii::t('app', '<span class="glyphicon glyphicon-plus"></span>'), Url::to(['research/create']), ['class' => 'btn btn-success btn-sm']),
        'dataProvider' => $dataProviderCo,
        //'filterModel' => $searchModel,
        'columns' => [
            [
		'class' => 'yii\grid\SerialColumn',
		'contentOptions'=>['style'=>'width:70px;text-align: center;'],
	    ],

            'res_topic:ntext',
			[
				'attribute'=>'author',
				'headerOptions'=>['style'=>'text-align: center;'],
				'contentOptions'=>['style'=>'width:120px;'],
			],
			[
				'attribute'=>'author_email',
				'headerOptions'=>['style'=>'text-align: center;'],
				'contentOptions'=>['style'=>'width:160px;'],
			],
			[
				'attribute'=>'co_auther',
				'value'=>function ($data){
					$arr = explode(',', $data['co_auther']);
					$r = '';
					if(is_array($arr)){
						$r = implode(', ', $arr);
					}
					return $r;
				},
				'headerOptions'=>['style'=>'text-align: center;'],
				'contentOptions'=>['style'=>'width:160px;'],
				//'filter'=> '',
			],
			[
				'attribute'=>'expected_completion',
				'headerOptions'=>['style'=>'text-align: center;'],
				'contentOptions'=>['style'=>'width:120px;text-align: center;'],
			],
            [
				'attribute'=>'status',
				'value'=>function ($data){
					return ArticleFunc::itemAlias('status', $data['status']);
				},
				'headerOptions'=>['style'=>'text-align: center;'],
				'contentOptions'=>['style'=>'width:140px;text-align: center;'],
				'filter'=> Html::activeDropDownList($searchModel, 'status', ArticleFunc::itemAlias('status'), ['class'=>'form-control', 'prompt'=>'All']),
			],
//			[
//				'attribute'=>'show',
//				'value'=>function ($data){
//					return ArticleFunc::itemAlias('show', $data['show']);
//				},
//				'headerOptions'=>['style'=>'text-align: center;'],
//				'contentOptions'=>['style'=>'width:140px;text-align: center;'],
//				'filter'=> Html::activeDropDownList($searchModel, 'show', ArticleFunc::itemAlias('show'), ['class'=>'form-control', 'prompt'=>'All']),
//			],
			[
				'attribute'=>'updated_at',
				'value'=>function ($data){return Yii::$app->formatter->asDate($data['updated_at'], 'd/m/Y');},
				'headerOptions'=>['style'=>'text-align: center;'],
				'contentOptions'=>['style'=>'width:100px;text-align: center;'],
				'filter'=>'',
			],
            [
				'class' => 'common\lib\sdii\widgets\SDActionColumn',
				'template'=>'{mbf} {mmf} {update} {delete}',
				'buttons'=>[
					'delete' => function ($url, $data, $key) {
						if(Yii::$app->user->id==$data['created_by']){
							return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
							'data-action' => 'delete',
							'title' => Yii::t('yii', 'Delete'),
							'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
							'data-method' => 'post',
							'data-pjax' => isset($this->pjax_id)?$this->pjax_id:'0',
							]);
						}
					},
					'mbf' => function ($url, $data, $key) {
							return Html::a('<span class="fa fa-whatsapp"></span>', 'http://61.19.254.15:9001/p/mbf_'.$data['rid'], [
							'title' => 'Mock Abstract Forum',
							'target'=>'_blank'    
							]);
					},	
					'mmf' => function ($url, $data, $key) {
							return Html::a('<span class="fa fa-weixin"></span>', 'http://61.19.254.15:9001/p/mmf_'.$data['rid'], [
							'title' => 'Mock Manuscript Forum',
							'target'=>'_blank'    
							]);
					},	
				]
			],
        ],
    ]); ?>
	
	
	<?php $form = ActiveForm::begin([
		'id' => 'jump_menu',
		'action' => ['index'],
		'method' => 'get',
		'layout' => 'inline',
		'options' => ['style'=>'display: inline-block;', 'class'=>'col-md-12']	    
	    ]); 
    ?>
	<h4 class="page-header">งานวิจัย 
	    <div class="btn-group" role="group" aria-label="...">
		<a href="<?=Url::to(['/article/research/index', 'cca'=>0])?>" class="btn btn-<?=$cca==0?'success':'default'?>">ทั้งหมด</a>
		<a href="<?=Url::to(['/article/research/index', 'cca'=>1])?>" class="btn btn-<?=$cca==1?'success':'default'?>">เฉพาะที่เกี่ยวกับ CCA</a>
	    </div>
	
	<?= \yii\bootstrap\Html::dropDownList('tree', $tree, $dataTree, ['class'=>'form-control', 'prompt'=>'โครงการทั้งหมด', 'onChange'=>'$("#jump_menu").submit()'])?>
	
	</h4>	
	
	<?php ActiveForm::end(); ?>
	
	<?= SDGridView::widget([
		'id' => 'research-All-grid',
		'panelBtn' => '',
        'dataProvider' => $dataProviderAll,
        'filterModel' => $searchModel,
        'columns' => [
            [
		'class' => 'yii\grid\SerialColumn',
		'contentOptions'=>['style'=>'width:70px;text-align: center;'],
	    ],
            'res_topic:ntext',
			[
				'attribute'=>'author',
				'headerOptions'=>['style'=>'text-align: center;'],
				'contentOptions'=>['style'=>'width:120px;'],
			],
			[
				'attribute'=>'author_email',
				'headerOptions'=>['style'=>'text-align: center;'],
				'contentOptions'=>['style'=>'width:160px;'],
			],
			[
				'attribute'=>'co_auther',
				'value'=>function ($data){
					$arr = explode(',', $data['co_auther']);
					$r = '';
					if(is_array($arr)){
						$r = implode(', ', $arr);
					}
					return $r;
				},
				'headerOptions'=>['style'=>'text-align: center;'],
				'contentOptions'=>['style'=>'width:160px;'],
				//'filter'=> '',
			],
			[
				'attribute'=>'expected_completion',
				'headerOptions'=>['style'=>'text-align: center;'],
				'contentOptions'=>['style'=>'width:120px;text-align: center;'],
			],
            [
				'attribute'=>'status',
				'value'=>function ($data){
					return ArticleFunc::itemAlias('status', $data['status']);
				},
				'headerOptions'=>['style'=>'text-align: center;'],
				'contentOptions'=>['style'=>'width:140px;text-align: center;'],
				'filter'=> Html::activeDropDownList($searchModel, 'status', ArticleFunc::itemAlias('status'), ['class'=>'form-control', 'prompt'=>'All']),
			],
//			[
//				'attribute'=>'show',
//				'value'=>function ($data){
//					return ArticleFunc::itemAlias('show', $data['show']);
//				},
//				'headerOptions'=>['style'=>'text-align: center;'],
//				'contentOptions'=>['style'=>'width:140px;text-align: center;'],
//				'filter'=> Html::activeDropDownList($searchModel, 'show', ArticleFunc::itemAlias('show'), ['class'=>'form-control', 'prompt'=>'All']),
//			],
			[
				'attribute'=>'updated_at',
				'value'=>function ($data){return Yii::$app->formatter->asDate($data['updated_at'], 'd/m/Y');},
				'headerOptions'=>['style'=>'text-align: center;'],
				'contentOptions'=>['style'=>'width:100px;text-align: center;'],
				'filter'=>'',
			],
            [
				'class' => 'common\lib\sdii\widgets\SDActionColumn',
				'template'=>'{view} {mbf} {mmf}',
				'contentOptions'=>['style'=>'width:90px;text-align: center;'],
				'buttons'=>[
					'view' => function ($url, $data, $key) {
							return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
							'data-action' => 'view',
							'title' => Yii::t('yii', 'View'),
							'data-method' => 'post',
							'target'=>'_blank',    
							'data-pjax' => isset($this->pjax_id)?$this->pjax_id:'0',
							]);
					},
					'mbf' => function ($url, $data, $key) {
							return Html::a('<span class="fa fa-whatsapp"></span>', 'http://61.19.254.15:9001/p/mbf_'.$data['rid'], [
							'title' => 'Mock Abstract Forum',
							'target'=>'_blank'    
							]);
					},	
					'mmf' => function ($url, $data, $key) {
							return Html::a('<span class="fa fa-weixin"></span>', 'http://61.19.254.15:9001/p/mmf_'.$data['rid'], [
							'title' => 'Mock Manuscript Forum',
							'target'=>'_blank'    
							]);
					},	
				]
			],
        ],
    ]); ?>
    <?php  Pjax::end();?>
</div>

<?=  SDModalForm::widget([
    'id' => 'modal-research',
    'size'=>'modal-lg',
]);
?>

<?php  $this->registerJs("

$('#research-grid-pjax').on('click', '#modal-addbtn-research', function(){
	modalResearch($(this).attr('data-url'));
});

$('#research-grid-pjax').on('click', 'tbody tr td a', function() {
    var url = $(this).attr('href');
    var action = $(this).attr('data-action');

    if(action === 'update' || action === 'view') {
	
    } else if(action === 'delete') {
	yii.confirm('".Yii::t('app', 'Are you sure you want to delete this item?')."', function() {
	    $.post(
		url
	    ).done(function(result) {
		if(result.status == 'success') {
		    ". SDNoty::show('result.message', 'result.status') ."
		    $.pjax.reload({container:'#research-grid-pjax'});
		} else {
		    ". SDNoty::show('result.message', 'result.status') ."
		}
	    }).fail(function() {
		". SDNoty::show("'" . SDHtml::getMsgError() . "Server Error'", '"error"') ."
		console.log('server error');
	    });
	});
	
	return false;
    }
    
});

function modalResearch(url) {
    $('#modal-research .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
    $('#modal-research').modal('show')
    .find('.modal-content')
    .load(url);
}

");?>