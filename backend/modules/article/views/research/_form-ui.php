<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\lib\sdii\components\helpers\SDNoty;
use yii\web\JsExpression;
/* @var $this yii\web\View */
/* @var $model app\modules\core\models\CoreGenerate */
/* @var $form yii\bootstrap\ActiveForm */
?>
<div class="auther-form">

    <?php $form = ActiveForm::begin(['id'=>$model->formName()]); ?>
	<div class="modal-header">
	    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	    <h4 class="modal-title" id="itemModalLabel">Author</h4>
	</div>

	<div class="modal-body">
	    <?= $form->field($model, 'co_auther')->widget(\yii\jui\AutoComplete::className(),[
		//'initValueText' => $research, // set the initial display text
		'options' => ['placeholder' => 'ค้นหาผู้แต่ง'],
		'clientOptions' => [
			'autoFill'=>true,
			'minLength'=>'1',
			'source'=>new JsExpression("function(request, response) {
			    $.getJSON('".\yii\helpers\Url::to(['author-list2'])."', {
				q: request.term
			    }, response);
			}"),
			'open' => new JsExpression("function( event, ui ) {
			    $(this).autocomplete('widget').css('z-index', 3000);
			    return false;
			 }"),
			'select' => new JsExpression("function( event, ui ) {
			    $('#autherform-co_auther_id').val(ui.item.id);
			    $('#autherform-co_auther_email').val(ui.item.email);
			 }"),
			 'change' => new JsExpression("function( event, ui ) {
			    if (!ui.item) {
				$('#autherform-co_auther_id').val('');
				//$('#autherform-co_auther_email').val('');
			    }
			 }"),
			
		],
		'options'=>[
		    'class'=>'form-control',  'placeholder'=>'ค้นหาชื่อผู้ร่วมแต่ง'
		],
	    ]) ?>
	    <?= $form->field($model, 'co_auther_email')->textInput(['placeholder'=>'E-Mail']) ?>
	</div>
    
	<?= Html::activeHiddenInput($model, 'co_auther_id') ?>
    
	<div class="modal-footer">
	    <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-primary']) ?>
	    <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
	</div>

    <?php ActiveForm::end(); ?>

</div>

<?php  $this->registerJs("

$('form#{$model->formName()}').on('beforeSubmit', function(e){
    var \$form = $(this);
    $.post(
		\$form.attr('action'), //serialize Yii2 form
		\$form.serialize()
    ).done(function(result){
		if(result.status == 'success'){
			". SDNoty::show('result.message', 'result.status') ."
			if(result.action == 'create'){
				$(document).find('#modal-auther-form').modal('hide');
				$.pjax.reload({container:'#auther-form-grid-pjax'});
			} else if(result.action == 'update'){
				$(document).find('#modal-auther-form').modal('hide');
				$.pjax.reload({container:'#auther-form-grid-pjax'});
			}
		} else{
			". SDNoty::show('result.message', 'result.status') ."
		} 
    }).fail(function(){
		". SDNoty::show('"<strong><i class=\"glyphicon glyphicon-warning-sign\"></i> Error!</strong> "+e.responseJSON.message', '"error"') ."
		console.log('server error');
    });
    return false;
});

");?>