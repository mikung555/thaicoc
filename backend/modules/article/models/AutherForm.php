<?php

namespace backend\modules\article\models;

use Yii;
use yii\data\ArrayDataProvider;
/**
 *
 * @property string $co_auther
 * @property string $co_auther_email
 */
class AutherForm extends \yii\base\Model {

	public $co_auther_id;
	public $co_auther;
	public $co_auther_email;

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[['co_auther', 'co_auther_email'], 'required'],
			[['co_auther_id'], 'integer'],
			[['co_auther'], 'string'],
			[['co_auther_email'], 'email'],
			[['co_auther_email'], 'unique']
		];
	}

	public function attributeLabels() {
		return [
			'co_auther_id' => Yii::t('app', 'รหัสผู้แต่งร่วม'),
			'co_auther' => Yii::t('app', 'ชื่อผู้แต่งร่วม'),
			'co_auther_email' => Yii::t('app', 'E-Mail'),
		];
	}

	public function autherTmp() {
		$arr = [];
		$session = Yii::$app->session;
		if (isset($session['auther_tmp'])) {
			$arr = $session['auther_tmp'];
		}

		$dataProvider = new ArrayDataProvider([
			'allModels' => $arr,
			'key' => 'co_auther_email',
			'sort' => [
				'attributes' => ['co_auther'],
			],
			'pagination' => [
				'pageSize' => 20,
			],
		]);
		return $dataProvider;
	}

}
