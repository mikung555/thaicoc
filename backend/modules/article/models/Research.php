<?php

namespace backend\modules\article\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\behaviors\AttributeBehavior;
use common\lib\codeerror\helpers\GenMillisecTime;
use frontend\modules\user\models\SignupForm;
use backend\models\UserProfile;
use common\models\User;

/**
 * This is the model class for table "research".
 *
 * @property integer $rid
 * @property string $res_topic
 * @property string $projid
 * @property integer $author_id
 * @property string $author
 * @property string $author_email
 * @property string $co_auther_id
 * @property string $co_auther
 * @property string $co_auther_email
 * @property string $contact_name
 * @property string $contact_address
 * @property string $contact_tel
 * @property string $contact_fax
 * @property string $contact_email
 * @property string $mabstract
 * @property string $mmanuscript
 * @property string $expected_completion
 * @property integer $status
 * @property integer $show
 * @property integer $cca_status
 * @property integer $mabstract_show
 * @property integer $mmanuscript_show
 * @property integer $author_auth
 * @property integer $author_user
 * @property integer $author_public
 * @property integer $contact_auth
 * @property integer $contact_user
 * @property integer $contact_public
 * @property string $icon
 * @property string $manuscript_file
 * @property string $abstract_file
 * @property string $res_size
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $created_at
 * @property integer $created_by
 */
class Research extends ActiveRecord
{
	public function behaviors() {
		return [
			[
				'class' => TimestampBehavior::className(),
				'value' => new Expression('NOW()'),
			],
			[
				'class' => BlameableBehavior::className(),
			],
			[
				'class' => AttributeBehavior::className(),
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => 'rid',
				],
				'value' => function ($event) {
					return GenMillisecTime::getMillisecTime();
				},
			],
			[
				'class' => AttributeBehavior::className(),
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => 'projid',
					ActiveRecord::EVENT_BEFORE_UPDATE => 'projid',
				],
				'value' => function ($event) {
					$r = '';
					if(isset($event->sender->projid) && is_array($event->sender->projid)){
						$r = implode(',', $event->sender->projid);
					}
					return $r;
				},
			],	
			[
				'class' => AttributeBehavior::className(),
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => 'co_auther_id',
					ActiveRecord::EVENT_BEFORE_UPDATE => 'co_auther_id',
				],
				'value' => function ($event) {
					$session = Yii::$app->session;
					$arr = [];

					if (isset($session['auther_tmp'])) {
						$arr = $session['auther_tmp'];
					}
					
					foreach ($arr as $key => $value) {
					     if($value['co_auther_id']==''){
						//auto add user
						$username = explode('@', $value['co_auther_email']);
						 
						$user = new User();
						$user->username = $value['co_auther_email'];
						$user->email = $value['co_auther_email'];
						$user->setPassword($value['co_auther_email']);
						$action = $user->save();
						
						if($action){
						    $profile = new UserProfile();
						    $name = explode(' ', $value['co_auther']);
						    $profile->user_id = $user->id;
						    $profile->firstname = $name[0];
						    $profile->lastname = isset($name[1])?$name[1]:'';
						    $profile->email = $value['co_auther_email'];
						    $profile->avatar_path = '1/wUllf9ldPUhNNzGCdzrZIsSEyLWBGX6d.jpg';
						    $profile->avatar_base_url = 'http://storage.phkku.damasac.com/source';
						    $profile->gender = 1;
						    $profile->save();
						    
						    $sendMail = Yii::$app->commandBus->handle(new \common\commands\command\SendEmailCommand([
							'from' => [Yii::$app->params['adminEmail'] => Yii::$app->keyStorage->get('sender')],
							'to' => $value['co_auther_email'],
							'subject' => 'User invite',
							'view' => 'register',
							'params' => ['user' => $profile, 'username'=>$user->username, 'password'=>$value['co_auther_email']]
						    ]));

						    $sendMailCC = Yii::$app->commandBus->handle(new \common\commands\command\SendEmailCommand([
							'from' => [Yii::$app->params['adminEmail'] => Yii::$app->keyStorage->get('sender')],
							'to' => \backend\modules\ezforms\components\EzformFunc::getEmailSite(),
							'subject' => 'New user invite of '.Yii::$app->keyStorage->get('sender'),
							'view' => 'sign_up',
							'params' => ['user' => $profile, 'username'=>$user->username]
						    ]));
						    
						    $auth =  Yii::$app->authManager;
						    $auth->assign($auth->getRole(User::ROLE_USER), $user->id);
						    $auth->assign($auth->getRole('research'), $user->id);
						    
						    $arr[$key]['co_auther_id'] = $user->id;
						    
						    //send email account
						    
						} else {
						    \yii\helpers\ArrayHelper::remove($arr, $key);
						    $session['auther_tmp'] = $arr;
						}
						
					     }
					}
					
					$data = implode(',', \yii\helpers\ArrayHelper::getColumn($arr, 'co_auther_id'));
					
					return $data;
				},
			],
			[
				'class' => AttributeBehavior::className(),
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => 'co_auther',
					ActiveRecord::EVENT_BEFORE_UPDATE => 'co_auther',
				],
				'value' => function ($event) {
					$session = Yii::$app->session;
					$arr = [];

					if (isset($session['auther_tmp'])) {
						$arr = $session['auther_tmp'];
					}
					
					return implode(',', \yii\helpers\ArrayHelper::getColumn($arr, 'co_auther'));
				},
			],
			[
				'class' => AttributeBehavior::className(),
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => 'co_auther_email',
					ActiveRecord::EVENT_BEFORE_UPDATE => 'co_auther_email',
				],
				'value' => function ($event) {
					$session = Yii::$app->session;
					$arr = [];

					if (isset($session['auther_tmp'])) {
						$arr = $session['auther_tmp'];
					}
					
					return  implode(',', \yii\helpers\ArrayHelper::getColumn($arr, 'co_auther_email'));
				},
			],
		];
	}
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'research';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['res_topic', 'author_id', 'author', 'author_email', 'show', 'status'], 'required'],
            [['author_id', 'author_auth', 'author_user', 'author_public', 'contact_auth', 'contact_user', 'contact_public', 'cca_status', 'mabstract_show', 'mmanuscript_show', 'rid', 'status', 'show', 'updated_by', 'created_by'], 'integer'],
            [['res_topic', 'co_auther_id', 'co_auther', 'co_auther_email', 'contact_address', 'mabstract', 'mmanuscript'], 'string'],
            [['icon', 'manuscript_file', 'abstract_file', 'projid', 'updated_at', 'created_at'], 'safe'],
            [['author', 'author_email'], 'string', 'max' => 255],
            [['res_size','contact_name', 'contact_email'], 'string', 'max' => 150],
            [['contact_tel', 'contact_fax'], 'string', 'max' => 50],
            [['expected_completion'], 'integer'],
	    [['author_email', 'contact_email'], 'email'],
	    [['icon'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'rid' => Yii::t('app', 'Rid'),
            'res_topic' => Yii::t('app', 'ชื่องานวิจัย'),
            'projid' => Yii::t('app', 'ภายใต้โครงการ'),
	    'author_id' => Yii::t('app', 'รหัสผู้แต่ง'),
            'author' => Yii::t('app', 'ชื่อผู้แต่ง'),
            'author_email' => Yii::t('app', 'E-mail'),
	    'co_auther_id' => Yii::t('app', 'รหัสผู้แต่งร่วม'),
            'co_auther' => Yii::t('app', 'ชื่อผู้แต่งร่วม'),
            'co_auther_email' => Yii::t('app', 'E-mail'),
            'contact_name' => Yii::t('app', 'ชื่อ'),
            'contact_address' => Yii::t('app', 'ที่อยู่'),
            'contact_tel' => Yii::t('app', 'เบอร์โทร'),
            'contact_fax' => Yii::t('app', 'Fax'),
            'contact_email' => Yii::t('app', 'E-Mail'),
            'mabstract' => Yii::t('app', 'Mock Abstract'),
            'mmanuscript' => Yii::t('app', 'Mock Manuscript'),
            'expected_completion' => Yii::t('app', 'ปีที่คาดว่าจะเสร็จ'),
            'status' => Yii::t('app', 'สถานะบทความ'),
	    'show' => Yii::t('app', 'ชื่อเรื่องและผู้แต่ง'),
	    'cca_status' => Yii::t('app', 'เกี่ยวข้องกับ CCA หรือไม่'),
	    'mabstract_show' => Yii::t('app', 'อนุญาตให้สาธารณะเห็นได้'),
            'mmanuscript_show' => Yii::t('app', 'อนุญาตให้สาธารณะเห็นได้'),
	    'author_auth' => Yii::t('app', 'ตนและผู้แต่งร่วม'),
            'author_user' => Yii::t('app', 'สมาชิก'),
            'author_public' => Yii::t('app', 'สาธารณะ'),
            'contact_auth' => Yii::t('app', 'ตนและผู้แต่งร่วม'),
            'contact_user' => Yii::t('app', 'สมาชิก'),
            'contact_public' => Yii::t('app', 'สาธารณะ'),
	    'icon' => Yii::t('app', 'ICON'),
	    'manuscript_file' => Yii::t('app', 'Manuscript File'),
	    'abstract_file' => Yii::t('app', 'Abstract File'),
	    'res_size' => Yii::t('app', 'ขนาดตัวอย่าง'),
            'updated_at' => Yii::t('app', 'เวลาล่าสุด'),
            'updated_by' => Yii::t('app', 'แก้ไขโดย'),
            'created_at' => Yii::t('app', 'สร้างเวลา'),
            'created_by' => Yii::t('app', 'สร้างโดย'),
        ];
    }
}
