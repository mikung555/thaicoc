<?php

namespace backend\modules\article\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\article\models\Research;

/**
 * ResearchSearch represents the model behind the search form about `app\modules\article\models\Research`.
 */
class ResearchSearch extends Research
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['author_id', 'author_auth', 'author_user', 'author_public', 'contact_auth', 'contact_user', 'contact_public', 'cca_status', 'mabstract_show', 'mmanuscript_show', 'rid', 'show', 'status', 'updated_by', 'created_by'], 'integer'],
            [['icon', 'co_auther_id', 'res_topic', 'projid', 'author', 'author_email', 'co_auther', 'co_auther_email', 'contact_name', 'contact_address', 'contact_tel', 'contact_fax', 'contact_email', 'mabstract', 'mmanuscript', 'expected_completion', 'updated_at', 'created_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
	
        $query = Research::find()->where('created_by = :userid OR author_email = :email', [':userid' =>  Yii::$app->user->id, ':email'=>  Yii::$app->user->identity->email]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'rid' => $this->rid,
            'status' => $this->status,
	    'show' => $this->show,
	    'cca_status' => $this->cca_status,
	    'mabstract_show' => $this->mabstract_show,
	    'mmanuscript_show' => $this->mmanuscript_show,
	    'author_id' => $this->author_id,
	    'author_auth' => $this->author_auth,
	    'author_user' => $this->author_user,
	    'author_public' => $this->author_public,
	    'contact_auth' => $this->contact_auth,
	    'contact_user' => $this->contact_user,
	    'contact_public' => $this->contact_public,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
        ]);

        $query->andFilterWhere(['like', 'res_topic', $this->res_topic])
            ->andFilterWhere(['like', 'projid', $this->projid])
            ->andFilterWhere(['like', 'author', $this->author])
            ->andFilterWhere(['like', 'author_email', $this->author_email])
	    ->andFilterWhere(['like', 'co_auther_id', $this->co_auther_id])
            ->andFilterWhere(['like', 'co_auther', $this->co_auther])
            ->andFilterWhere(['like', 'co_auther_email', $this->co_auther_email])
            ->andFilterWhere(['like', 'contact_name', $this->contact_name])
            ->andFilterWhere(['like', 'contact_address', $this->contact_address])
            ->andFilterWhere(['like', 'contact_tel', $this->contact_tel])
            ->andFilterWhere(['like', 'contact_fax', $this->contact_fax])
            ->andFilterWhere(['like', 'contact_email', $this->contact_email])
            ->andFilterWhere(['like', 'mabstract', $this->mabstract])
            ->andFilterWhere(['like', 'mmanuscript', $this->mmanuscript])
            ->andFilterWhere(['like', 'expected_completion', $this->expected_completion]);

        return $dataProvider;
    }
	
	public function searchCo($params)
    {
        $query = Research::find()->andWhere('FIND_IN_SET(:email, co_auther_email)', [':email'=>  Yii::$app->user->identity->email]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'rid' => $this->rid,
            'status' => $this->status,
	    'show' => $this->show,
	    'cca_status' => $this->cca_status,
	    'mabstract_show' => $this->mabstract_show,
	    'mmanuscript_show' => $this->mmanuscript_show,
	    'author_id' => $this->author_id,
	    'author_auth' => $this->author_auth,
	    'author_user' => $this->author_user,
	    'author_public' => $this->author_public,
	    'contact_auth' => $this->contact_auth,
	    'contact_user' => $this->contact_user,
	    'contact_public' => $this->contact_public,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
        ]);

        $query->andFilterWhere(['like', 'res_topic', $this->res_topic])
            ->andFilterWhere(['like', 'projid', $this->projid])
            ->andFilterWhere(['like', 'author', $this->author])
            ->andFilterWhere(['like', 'author_email', $this->author_email])
	    ->andFilterWhere(['like', 'co_auther_id', $this->co_auther_id])
            ->andFilterWhere(['like', 'co_auther', $this->co_auther])
            ->andFilterWhere(['like', 'co_auther_email', $this->co_auther_email])
            ->andFilterWhere(['like', 'contact_name', $this->contact_name])
            ->andFilterWhere(['like', 'contact_address', $this->contact_address])
            ->andFilterWhere(['like', 'contact_tel', $this->contact_tel])
            ->andFilterWhere(['like', 'contact_fax', $this->contact_fax])
            ->andFilterWhere(['like', 'contact_email', $this->contact_email])
            ->andFilterWhere(['like', 'mabstract', $this->mabstract])
            ->andFilterWhere(['like', 'mmanuscript', $this->mmanuscript])
            ->andFilterWhere(['like', 'expected_completion', $this->expected_completion]);

        return $dataProvider;
    }
	
	public function searchAll($params, $tree)
    {
	$query;
	if (Yii::$app->user->isGuest) {
	    $query = Research::find()->andWhere('`show` = :show AND (created_by = :created_by OR (author_public = 1 AND author_user = 0))', [':created_by' =>  Yii::$app->user->id, ':show' =>  1]);
	} else {
	    $query = Research::find()->andWhere('`show` = :show AND (created_by = :created_by OR author_public = 1 OR author_user = 1 OR (FIND_IN_SET(:auth, co_auther_email)))', [':created_by' =>  Yii::$app->user->id, ':show' =>  1, ':auth'=>Yii::$app->user->identity->email]);
	    
	}
	
	if($tree!=0){
	    $query->andWhere('FIND_IN_SET(:tree, projid)', [':tree'=>$tree]);
	}
        
	
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'rid' => $this->rid,
            'status' => $this->status,
	    'show' => $this->show,
	    'cca_status' => $this->cca_status,
	    'mabstract_show' => $this->mabstract_show,
	    'mmanuscript_show' => $this->mmanuscript_show,
	    'author_id' => $this->author_id,
	    'author_auth' => $this->author_auth,
	    'author_user' => $this->author_user,
	    'author_public' => $this->author_public,
	    'contact_auth' => $this->contact_auth,
	    'contact_user' => $this->contact_user,
	    'contact_public' => $this->contact_public,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
        ]);

        $query->andFilterWhere(['like', 'res_topic', $this->res_topic])
            ->andFilterWhere(['like', 'projid', $this->projid])
            ->andFilterWhere(['like', 'author', $this->author])
            ->andFilterWhere(['like', 'author_email', $this->author_email])
	    ->andFilterWhere(['like', 'co_auther_id', $this->co_auther_id])
            ->andFilterWhere(['like', 'co_auther', $this->co_auther])
            ->andFilterWhere(['like', 'co_auther_email', $this->co_auther_email])
            ->andFilterWhere(['like', 'contact_name', $this->contact_name])
            ->andFilterWhere(['like', 'contact_address', $this->contact_address])
            ->andFilterWhere(['like', 'contact_tel', $this->contact_tel])
            ->andFilterWhere(['like', 'contact_fax', $this->contact_fax])
            ->andFilterWhere(['like', 'contact_email', $this->contact_email])
            ->andFilterWhere(['like', 'mabstract', $this->mabstract])
            ->andFilterWhere(['like', 'mmanuscript', $this->mmanuscript])
            ->andFilterWhere(['like', 'expected_completion', $this->expected_completion]);

        return $dataProvider;
    }
}
