<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\helpdesk\models\Tester */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tester-form">

    <?php $form = ActiveForm::begin(
        [
            'options' =>[
                'enctype' => 'multipart/form-data'
            ],
            'method' =>'post',
        ]
    ); ?>
    <?= Html::activeHiddenInput($model, 'requirement_id') ;?>
    <?= $form->field($model, 'user_id')->dropDownList($model->getTesterList()) ?>
    <?= $form->field($model, 'status_approve')->radioList(array('1'=>'Yes','0'=>'No'))
    ?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Add' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>