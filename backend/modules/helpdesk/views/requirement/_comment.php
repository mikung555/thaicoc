<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\HtmlPurifier;
?>

<?= Html::activeHiddenInput($model, 'requirement_id') ;?>
<?= $form->field($model, 'comment')->textarea(['maxlength' => true]) ?>
<?= $form->field($model, 'file')->fileInput() ?>
<?= Html::activeHiddenInput($model, 'post_by') ;?>
<?php if ($model->files_upload) {?>
    <?= Html::img(Url::to(Yii::$app->request->baseUrl.'/'.$model->files_upload),
        ['class' => 'thumbnail']) ?>
    <?= Html::a('<i class="glyphicon glyphicon-trash"></i>',
        ['deleteimage','id' => $model->id, 'field' => 'files_upload'],
        ['class' => 'btn btn-danger'])?>
<?php } ?>
<div class="form-group">
    <?= Html::submitButton($model->isNewRecord ? 'Add' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
</div>
