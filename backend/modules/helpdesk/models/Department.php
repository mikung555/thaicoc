<?php

namespace backend\modules\helpdesk\models;

use Yii;

/**
 * This is the model class for table "help_desk_department".
 *
 * @property integer $id
 * @property string $department_name
 *
 * @property HelpDeskRequirement[] $helpDeskRequirements
 */
class Department extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'help_desk_department';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['department_name'], 'required'],
            [['department_name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'department_name' => 'Department Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHelpDeskRequirements()
    {
        return $this->hasMany(HelpDeskRequirement::className(), ['department' => 'id']);
    }
}
