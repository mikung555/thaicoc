<?php

namespace backend\modules\helpdesk\models;

use Yii;
use yii\helpers\ArrayHelper;
use common\models\User;
use yii\helpers\Url;

/**
 * This is the model class for table "help_desk_requirement".
 *
 * @property integer $id
 * @property string $user_assign
 * @property string $requirement
 * @property integer $department
 * @property string $ownership333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333
 * @property string $assign_date
 * @property string $deadline_date
 * @property string $left_date
 * @property integer $status
 *
 * @property HelpDeskDatetime[] $helpDeskDatetimes
 * @property HelpDeskDepartment $department0
 * @property HelpDeskStatus $status0
 * @property HelpDeskRequirementComment[] $helpDeskRequirementComments
 * @property HelpDeskTester[] $helpDeskTesters
 */
class Requirement extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'help_desk_requirement';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_assign', 'department', 'ownership', 'status'], 'integer'],
            [['assign_date', 'deadline_date'], 'safe'],
            [['requirement', 'left_date'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_assign' => 'User Assign',
            'requirement' => 'Requirement',
            'department' => 'Department',
            'ownership' => 'Ownership',
            'assign_date' => 'Assign Date',
            'deadline_date' => 'Deadline Date',
            'left_date' => 'Left Date',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHelpDeskDatetimes()
    {
        return $this->hasMany(DateAssign::className(), ['requirement_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDepartment0()
    {
        return $this->hasOne(Department::className(), ['id' => 'department']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus0()
    {
        return $this->hasOne(Status::className(), ['id' => 'status']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHelpDeskRequirementComments()
    {
        return $this->hasMany(Comment::className(), ['requirement_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHelpDeskTesters()
    {
        return $this->hasMany(Tester::className(), ['requirement_id' => 'id']);
    }
    public function getDepartmentList()
    {
        $list =Department::find()->orderBy('id')->all();
        return ArrayHelper::map($list,'id','department_name');
    }
    public function getStatusList()
    {
        $list=Status::find()->orderBy('id')->all();
        return ArrayHelper::map($list,'id','status_name');
    }
    public function getOwnership0()
    {
        return $this->hasOne(User::className(), ['id' => 'ownership']);
    }
    public function getUserAssign()
    {
        return $this->hasOne(User::className(), ['id' => 'user_assign']);
    }
    public function getTesters()
    {
        return $this->hasMany(Tester::className(), ['requirement_id' => 'id']);
    }
    public function getUserAssignList()
    {
        $list =User::find()->orderBy('id')->all();
        return ArrayHelper::map($list,'id','username');
    }
    public function getOwnershipList()
    {
        $list =User::find()->orderBy('id')->all();
        return ArrayHelper::map($list,'id','username');
    }
    public  function getPostByList()
    {
        $list =User::find()->orderBy('id')->all();
        return ArrayHelper::map($list,'id','username');
    }
    public  function getImageUrl()
    {
        return Url::to(Yii::$app->request->baseUrl.'/'.$this->files_upload);
    }
}
