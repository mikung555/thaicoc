<?php

namespace backend\modules\helpdesk\models;

use Yii;

/**
 * This is the model class for table "help_desk_project".
 *
 * @property integer $id
 * @property string $title
 * @property string $start
 * @property string $due
 * @property string $duration
 * @property integer $project_status
 * @property string $complete
 * @property integer $site
 */
class Project extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'help_desk_project';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['start', 'due'], 'safe'],
            [['project_status', 'site'], 'integer'],
            [['title', 'duration', 'complete'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'start' => 'Start',
            'due' => 'Due',
            'duration' => 'Duration',
            'project_status' => 'Project Status',
            'complete' => 'Complete',
            'site' => 'Site',
        ];
    }
}
