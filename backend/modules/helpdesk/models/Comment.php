<?php

namespace backend\modules\helpdesk\models;

use common\models\User;
use Yii;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
//use backend\models\UserProfile;
//use backend\models\UserForm;;

/**
 * This is the model class for table "help_desk_requirement_comment".
 *
 * @property integer $id
 * @property integer $requirement_id
 * @property string $comment
 * @property string $files_upload
 * @property string $post_by
 * @property string $post_datetime
 * @property HelpDeskRequirement $requirement
 */
class Comment extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $file;
    public static function tableName()
    {
        return 'help_desk_requirement_comment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['requirement_id', 'post_by'], 'integer'],
            [['comment', 'post_by'], 'required'],
            [['comment', 'files_upload'], 'string', 'max' => 255],
            [['file'], 'safe'],
            [['post_datetime'], 'safe'],
            [['file'], 'file','extensions' =>'jpg, png, gif, pdf']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'requirement_id' => 'Requirement ID',
            'comment' => 'Comment',
            'file' => 'Files Upload',
            'post_by' => 'Post By',
            'post_datetime' => 'Post Datetime',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRequirement()
    {
        return $this->hasOne(Requirement::className(), ['id' => 'requirement_id']);
    }
    public function getPostBy()
    {
        return $this->hasOne(User::className(), ['id' => 'post_by']);
    }
    public  function getImageUrl()
    {
        return Url::to(Yii::$app->request->baseUrl.'/'.$this->files_upload);
    }
    public  function getPostByList()
    {
        $list =User::find()->orderBy('user_id')->all();
        return ArrayHelper::map($list,'user_id','username');
    }
    public function getUser0()
    {
        return $this->hasOne(User::className(), ['id' => 'post_by']);

    }
}
