<?php

namespace backend\modules\helpdesk\controllers;

use yii\web\Controller;

class DefaultController extends Controller
{
    public function actionIndex()
    {
        //return $this->redirect('/helpdesk/requirement');
        return $this->render('index');
    }
}
