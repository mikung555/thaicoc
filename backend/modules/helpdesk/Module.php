<?php

namespace backend\modules\helpdesk;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\modules\helpdesk\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
