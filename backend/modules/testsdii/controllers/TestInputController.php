<?php

namespace backend\modules\testsdii\controllers;

use Yii;
use backend\modules\testsdii\models\TestInput;
use backend\modules\testsdii\models\TestInputSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\Response;
use appxq\sdii\helpers\SDHtml;

/**
 * TestInputController implements the CRUD actions for TestInput model.
 */
class TestInputController extends Controller
{
    public function behaviors()
    {
        return [
	    'access' => [
		'class' => AccessControl::className(),
		'rules' => [
		    [
			'allow' => true,
			'actions' => ['index', 'view'], 
			'roles' => ['?', '@'],
		    ],
		    [
			'allow' => true,
			'actions' => ['view', 'create', 'update', 'delete', 'deletes'], 
			'roles' => ['@'],
		    ],
		],
	    ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public function beforeAction($action) {
	if (parent::beforeAction($action)) {
	    if (in_array($action->id, array('create', 'update'))) {
		
	    }
	    return true;
	} else {
	    return false;
	}
    }
    
    /**
     * Lists all TestInput models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TestInputSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single TestInput model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
	if (Yii::$app->getRequest()->isAjax) {
	    return $this->renderAjax('view', [
		'model' => $this->findModel($id),
	    ]);
	} else {
	    return $this->render('view', [
		'model' => $this->findModel($id),
	    ]);
	}
    }

    /**
     * Creates a new TestInput model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
	if (Yii::$app->getRequest()->isAjax) {
	    $model = new TestInput();
            $file_old = $model->file;
            
	    if ($model->load(Yii::$app->request->post())) {
		Yii::$app->response->format = Response::FORMAT_JSON;

                $model->file = \yii\web\UploadedFile::getInstance($model, 'file');
                if ($model->file !== null) {
		    $nameArr = explode('/', $model->file->type);
		    $lname = $nameArr[1];
		    
		    if($file_old!=''){
			unlink(Yii::$app->basePath . '/../storage/web/source/' . $file_old);
		    }

		    $nowFileName = 'test_'.\common\lib\codeerror\helpers\GenMillisecTime::getMillisecTime().'.'.$lname;
		    $fullPath = Yii::$app->basePath . '/../storage/web/source/' . $nowFileName;

		    $model->file->saveAs($fullPath);
		    $model->file = $nowFileName;
		} else {
		    $model->file = $file_old;
		}
                
		if ($model->save()) {
		    $result = [
			'status' => 'success',
			'action' => 'create',
			'message' => SDHtml::getMsgSuccess() . Yii::t('app', 'Data completed.'),
			'data' => $model,
		    ];
		    return $result;
		} else {
		    $result = [
			'status' => 'error',
			'message' => SDHtml::getMsgError() . Yii::t('app', 'Can not create the data.'),
			'data' => $model,
		    ];
		    return $result;
		}
	    } else {
		return $this->renderAjax('create', [
		    'model' => $model,
		]);
	    }
	} else {
	    throw new NotFoundHttpException('Invalid request. Please do not repeat this request again.');
	}
    }

    /**
     * Updates an existing TestInput model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
	if (Yii::$app->getRequest()->isAjax) {
	    $model = $this->findModel($id);
            $file_old = $model->file;
            
	    if ($model->load(Yii::$app->request->post())) {
		Yii::$app->response->format = Response::FORMAT_JSON;
                $model->file = \yii\web\UploadedFile::getInstance($model, 'file');
                if ($model->file !== null) {
		    $nameArr = explode('/', $model->file->type);
		    $lname = $nameArr[1];
		    
		    if($file_old!=''){
			unlink(Yii::$app->basePath . '/../storage/web/source/' . $file_old);
		    }

		    $nowFileName = 'test_'.\common\lib\codeerror\helpers\GenMillisecTime::getMillisecTime().'.'.$lname;
		    $fullPath = Yii::$app->basePath . '/../storage/web/source/' . $nowFileName;

		    $model->file->saveAs($fullPath);
		    $model->file = $nowFileName;
		} else {
		    $model->file = $file_old;
		}
                
		if ($model->save()) {
		    $result = [
			'status' => 'success',
			'action' => 'update',
			'message' => SDHtml::getMsgSuccess() . Yii::t('app', 'Data completed.'),
			'data' => $model,
		    ];
		    return $result;
		} else {
		    $result = [
			'status' => 'error',
			'message' => SDHtml::getMsgError() . Yii::t('app', 'Can not update the data.'),
			'data' => $model,
		    ];
		    return $result;
		}
	    } else {
		return $this->renderAjax('update', [
		    'model' => $model,
		]);
	    }
	} else {
	    throw new NotFoundHttpException('Invalid request. Please do not repeat this request again.');
	}
    }

    /**
     * Deletes an existing TestInput model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
	if (Yii::$app->getRequest()->isAjax) {
	    Yii::$app->response->format = Response::FORMAT_JSON;
            $model = $this->findModel($id);
            $file_old = $model->file;
            
	    if ($model->delete()) {
                if($file_old!=''){
                    unlink(Yii::$app->basePath . '/../storage/web/source/' . $file_old);
                }
		$result = [
		    'status' => 'success',
		    'action' => 'update',
		    'message' => SDHtml::getMsgSuccess() . Yii::t('app', 'Deleted completed.'),
		    'data' => $id,
		];
		return $result;
	    } else {
		$result = [
		    'status' => 'error',
		    'message' => SDHtml::getMsgError() . Yii::t('app', 'Can not delete the data.'),
		    'data' => $id,
		];
		return $result;
	    }
	} else {
	    throw new NotFoundHttpException('Invalid request. Please do not repeat this request again.');
	}
    }

    public function actionDeletes() {
	if (Yii::$app->getRequest()->isAjax) {
	    Yii::$app->response->format = Response::FORMAT_JSON;
	    if (isset($_POST['selection'])) {
		foreach ($_POST['selection'] as $id) {
		    $model = $this->findModel($id);
                    $file_old = $model->file;

                    if ($model->delete()) {
                        if($file_old!=''){
                            unlink(Yii::$app->basePath . '/../storage/web/source/' . $file_old);
                        }
                    }
		}
		$result = [
		    'status' => 'success',
		    'action' => 'deletes',
		    'message' => SDHtml::getMsgSuccess() . Yii::t('app', 'Deleted completed.'),
		    'data' => $_POST['selection'],
		];
		return $result;
	    } else {
		$result = [
		    'status' => 'error',
		    'message' => SDHtml::getMsgError() . Yii::t('app', 'Can not delete the data.'),
		    'data' => $id,
		];
		return $result;
	    }
	} else {
	    throw new NotFoundHttpException('Invalid request. Please do not repeat this request again.');
	}
    }
    
    public function actionGenamphur(){
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            //\appxq\sdii\utils\VarDumper::dump($_POST['depdrop_parents'],1,0);
            $id = end($_POST['depdrop_parents']);
            $sql = "SELECT `AMPHUR_CODE` as id,`AMPHUR_NAME` as name FROM `const_amphur` WHERE `AMPHUR_CODE` LIKE :id ";
            $list = Yii::$app->db->createCommand($sql, [':id'=>"$id%"])->queryAll();
            $selected  = null;
            if ($id != null && count($list) > 0) {
                $selected = '';
                if (!empty($_POST['depdrop_params'])) {
                    $params = $_POST['depdrop_params'];
                    $selected = $params[0]; // get the value of input-type-1
                }
                foreach ($list as $i => $amphur) {
                    $out[] = ['id' => $amphur['id'], 'name' => $amphur['name']];
                }
                
                // Shows how you can preselect a value
                echo \yii\helpers\Json::encode(['output' => $out, 'selected'=>$selected]);
                return;
            }
        }
        echo \yii\helpers\Json::encode(['output' => '', 'selected'=>'']);
    }
    
    public function actionTestSql(){
        $data = \backend\modules\testsdii\classes\Testsdii::testDaoDelete('1487836203033416100');
        
        \appxq\sdii\utils\VarDumper::dump($data);
        
    }
    /**
     * Finds the TestInput model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TestInput the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TestInput::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
