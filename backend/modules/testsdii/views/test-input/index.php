<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\helpers\Url;
use appxq\sdii\widgets\GridView;
use appxq\sdii\widgets\ModalForm;
use appxq\sdii\helpers\SDNoty;
use appxq\sdii\helpers\SDHtml;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\testsdii\models\TestInputSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'Test Inputs');
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="test-input-index">

    <?php  Pjax::begin(['id'=>'test-input-grid-pjax']);?>
    <?= GridView::widget([
	'id' => 'test-input-grid',
	'panelBtn' => Html::button(SDHtml::getBtnAdd(), ['data-url'=>Url::to(['test-input/create']), 'class' => 'btn btn-success btn-sm', 'id'=>'modal-addbtn-test-input']). ' ' .
		      Html::button(SDHtml::getBtnDelete(), ['data-url'=>Url::to(['test-input/deletes']), 'class' => 'btn btn-danger btn-sm', 'id'=>'modal-delbtn-test-input', 'disabled'=>true]),
	'dataProvider' => $dataProvider,
	'filterModel' => $searchModel,
        'columns' => [
	    [
		'class' => 'yii\grid\CheckboxColumn',
		'checkboxOptions' => [
		    'class' => 'selectionTestInputIds'
		],
		'headerOptions' => ['style'=>'text-align: center;'],
		'contentOptions' => ['style'=>'width:40px;text-align: center;'],
	    ],
	    [
		'class' => 'yii\grid\SerialColumn',
		'headerOptions' => ['style'=>'text-align: center;'],
		'contentOptions' => ['style'=>'width:60px;text-align: center;'],
	    ],

            'id',
            'file',
            'province',
            'amphur',
            //'created_by',
            //'created_at',
            'firstname',
            //'updated_at',
            [
		'attribute'=>'updated_at',
		'filter'=> '',
		'value'=>function ($data){ 
                    return common\lib\sdii\components\utils\SDdate::mysql2phpThDateTime($data['updated_at']); 
                    
                },
		'headerOptions'=>['style'=>'text-align: center;'],
		'contentOptions'=>['style'=>'width:180px; text-align: center;'],
            ],
	    [
		'class' => 'appxq\sdii\widgets\ActionColumn',
		'contentOptions' => ['style'=>'width:80px;text-align: center;'],
		'template' => '{view} {update} {delete}',
	    ],
        ],
    ]); ?>
    <?php  Pjax::end();?>

</div>

<?=  ModalForm::widget([
    'id' => 'modal-test-input',
    //'size'=>'modal-sm',
    'tabindexEnable' => false,
]);
?>

<?php  

$this->registerJs("
$('#test-input-grid-pjax').on('click', '#modal-addbtn-test-input', function() {
    modalTestInput($(this).attr('data-url'));
});

$('#test-input-grid-pjax').on('click', '#modal-delbtn-test-input', function() {
    selectionTestInputGrid($(this).attr('data-url'));
});

$('#test-input-grid-pjax').on('click', '.select-on-check-all', function() {
    window.setTimeout(function() {
	var key = $('#test-input-grid').yiiGridView('getSelectedRows');
	disabledTestInputBtn(key.length);
    },100);
});

$('#test-input-grid-pjax').on('click', '.selectionTestInputIds', function() {
    var key = $('input:checked[class=\"'+$(this).attr('class')+'\"]');
    disabledTestInputBtn(key.length);
});

$('#test-input-grid-pjax').on('dblclick', 'tbody tr', function() {
    var id = $(this).attr('data-key');
    modalTestInput('".Url::to(['test-input/update', 'id'=>''])."'+id);
});	

$('#test-input-grid-pjax').on('click', 'tbody tr td a', function() {
    var url = $(this).attr('href');
    var action = $(this).attr('data-action');

    if(action === 'update' || action === 'view') {
	modalTestInput(url);
    } else if(action === 'delete') {
	yii.confirm('".Yii::t('app', 'Are you sure you want to delete this item?')."', function() {
	    $.post(
		url
	    ).done(function(result) {
		if(result.status == 'success') {
		    ". SDNoty::show('result.message', 'result.status') ."
		    $.pjax.reload({container:'#test-input-grid-pjax'});
		} else {
		    ". SDNoty::show('result.message', 'result.status') ."
		}
	    }).fail(function() {
		". SDNoty::show("'" . SDHtml::getMsgError() . "Server Error'", '"error"') ."
		console.log('server error');
	    });
	});
    }
    return false;
});

function disabledTestInputBtn(num) {
    if(num>0) {
	$('#modal-delbtn-test-input').attr('disabled', false);
    } else {
	$('#modal-delbtn-test-input').attr('disabled', true);
    }
}

function selectionTestInputGrid(url) {
    yii.confirm('".Yii::t('app', 'Are you sure you want to delete these items?')."', function() {
	$.ajax({
	    method: 'POST',
	    url: url,
	    data: $('.selectionTestInputIds:checked[name=\"selection[]\"]').serialize(),
	    dataType: 'JSON',
	    success: function(result, textStatus) {
		if(result.status == 'success') {
		    ". SDNoty::show('result.message', 'result.status') ."
		    $.pjax.reload({container:'#test-input-grid-pjax'});
		} else {
		    ". SDNoty::show('result.message', 'result.status') ."
		}
	    }
	});
    });
}

function modalTestInput(url) {
    $('#modal-test-input .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
    $('#modal-test-input').modal('show')
    .find('.modal-content')
    .load(url);
}

");?>