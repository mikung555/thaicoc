<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\testsdii\models\TestInput */

$this->title = 'Test Input#'.$model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Test Inputs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="test-input-view">

    <div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	<h4 class="modal-title" id="itemModalLabel"><?= Html::encode($this->title) ?></h4>
    </div>
    <div class="modal-body">
        <?= DetailView::widget([
	    'model' => $model,
	    'attributes' => [
		'id',
		'file',
		'province',
		'amphur',
		'created_by',
		'created_at',
		'updated_by',
		'updated_at',
	    ],
	]) ?>
    </div>
</div>
