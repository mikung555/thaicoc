<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use appxq\sdii\helpers\SDNoty;
use appxq\sdii\helpers\SDHtml;

/* @var $this yii\web\View */
/* @var $model backend\modules\testsdii\models\TestInput */
/* @var $form yii\bootstrap\ActiveForm */
?>

<div class="test-input-form">

    <?php $form = ActiveForm::begin([
	'id'=>$model->formName(),
        'options' => ['enctype' => 'multipart/form-data'],
    ]); ?>

    <div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	<h4 class="modal-title" id="itemModalLabel">Test Input</h4>
    </div>

    <div class="modal-body">
	
        <?php
        $imgPath = Yii::getAlias('@storageUrl') . '/source/';
        $initialPreview = ($model->file!='')?Html::img($imgPath.$model->file, ['class'=>'file-preview-image']):'';
        ?>
        <?= $form->field($model, 'file')->widget(kartik\widgets\FileInput::classname(),[
            'pluginOptions' => [
                'previewFileType' => 'image',
                'initialPreview'=>$initialPreview,
                'overwriteInitial'=>true,
                'showRemove' => false,
                'showUpload' => false,
                'allowedFileExtensions'=>['png','jpg','jpeg'],
                'maxFileSize' => 5000,
            ],
            'options' => ['multiple' => false]//,'accept' => 'image/*'
        ]);?>
        
        <?php
        echo $form->field($model, 'province')->widget(kartik\widgets\Select2::className(), [
                'options' => ['placeholder' => 'จังหวัด','id'=>'province'],
                'data' => \yii\helpers\ArrayHelper::map(\backend\modules\ezforms\components\EzformQuery::getProvince(),'PROVINCE_CODE','PROVINCE_NAME'),
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
        ?>
        
        <?php
        echo Html::hiddenInput('input-amphur', $model->amphur, ['id'=>'input-amphur']);
        
        echo $form->field($model,'amphur')->widget(kartik\widgets\DepDrop::classname(),[
            'type'=> kartik\widgets\DepDrop::TYPE_SELECT2,
            'options'=>['id'=>'amphur'],
            'pluginOptions'=>[
                'depends'=>['province'],
                'initialize' => true,
                'initDepends'=>['province'],
                'params'=>['input-amphur'],
                'placeholder'=>'อำเภอ',
                'url'=>'/testsdii/test-input/genamphur',
            ]
        ]);
        ?>
        
        <?= $form->field($model, 'id')->hiddenInput()->label(false) ?>
	<?= $form->field($model, 'created_by')->hiddenInput()->label(false) ?>
	<?= $form->field($model, 'created_at')->hiddenInput()->label(false) ?>
	<?= $form->field($model, 'updated_by')->hiddenInput()->label(false) ?>
	<?= $form->field($model, 'updated_at')->hiddenInput()->label(false) ?>

    </div>
    <div class="modal-footer">
	<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	<?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php  $this->registerJs("
$('form#{$model->formName()}').on('beforeSubmit', function(e) {
    var \$form = $(this);
    var formData = new FormData($(this)[0]);
    
    $.ajax({
	    method: 'POST',
	    url:\$form.attr('action'),
	    data: formData,
	    dataType: 'JSON',
            enctype: 'multipart/form-data',
	    processData: false,  // tell jQuery not to process the data
	    contentType: false,   // tell jQuery not to set contentType
	    success: function(result, textStatus) {
		if(result.status == 'success') {
                    ". SDNoty::show('result.message', 'result.status') ."
                    if(result.action == 'create') {
                        $(\$form).trigger('reset');
                        $.pjax.reload({container:'#test-input-grid-pjax'});
                    } else if(result.action == 'update') {
                        $(document).find('#modal-test-input').modal('hide');
                        $.pjax.reload({container:'#test-input-grid-pjax'});
                    }
                } else {
                    ". SDNoty::show('result.message', 'result.status') ."
                } 
	    }
    });
    
    return false;
});

");?>