<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\testsdii\models\TestInput */

$this->title = Yii::t('backend', 'Create Test Input');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Test Inputs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="test-input-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
