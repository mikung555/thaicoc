<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\testsdii\models\TestInput */

$this->title = Yii::t('backend', 'Update {modelClass}: ', [
    'modelClass' => 'Test Input',
]) . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Test Inputs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('backend', 'Update');
?>
<div class="test-input-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
