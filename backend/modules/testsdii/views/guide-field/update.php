<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\testsdii\models\GuideField */

$this->title = Yii::t('backend', 'Update {modelClass}: ', [
    'modelClass' => 'Guide Field',
]) . ' ' . $model->auto_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Guide Fields'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->auto_id, 'url' => ['view', 'id' => $model->auto_id]];
$this->params['breadcrumbs'][] = Yii::t('backend', 'Update');
?>
<div class="guide-field-update">

    <?= $this->render('_form_1', [
        'model' => $model,
    ]) ?>

</div>
