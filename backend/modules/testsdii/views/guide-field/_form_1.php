<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use backend\modules\guide\models\GuideList;
use yii\bootstrap\ActiveForm;
use appxq\sdii\helpers\SDNoty;
use appxq\sdii\helpers\SDHtml;

/* @var $this yii\web\View */
/* @var $model backend\modules\testsdii\models\GuideField */
/* @var $form yii\bootstrap\ActiveForm */
?>

<div class="guide-field-form">

    <?php $form = ActiveForm::begin([
	'id'=>$model->formName(),
    ]); ?>

    <div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	<h4 class="modal-title" id="itemModalLabel">Guide Field</h4>
    </div>

    <div class="modal-body">
     <?php
      
     ?>
    <?php echo $form->field($model, 'uid')->textInput(['readonly'=>true, 'data-uid'=>$model->uid]) ?>

    <?php echo $form->field($model, 'text_input')->textInput(['maxlength' => true, 'placeholder'=>'Text Input (placeholder)']) ?>

    <?php echo $form->field($model, 'select2')->textInput() ?>

    <?php echo $form->field($model, 'file')->fileInput() ?>

    <?= Html::activeHiddenInput($model, 'datetime') ?>
    <?= Html::activeHiddenInput($model, 'create_by') ?>
    <?= Html::activeHiddenInput($model, 'create_time') ?>
    <?= Html::activeHiddenInput($model, 'update_by') ?>
    <?= Html::activeHiddenInput($model, 'update_time') ?>

    </div>
    <div class="modal-footer">
	<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	<?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php  $this->registerJs("
$('form#{$model->formName()}').on('beforeSubmit', function(e) {
    var \$form = $(this);
    $.post(
	\$form.attr('action'), //serialize Yii2 form
	\$form.serialize()
    ).done(function(result) {
	if(result.status == 'success') {
	    ". SDNoty::show('result.message', 'result.status') ."
	    if(result.action == 'create') {
		$(\$form).trigger('reset');
		$.pjax.reload({container:'#guide-field-grid-pjax'});
	    } else if(result.action == 'update') {
		$(document).find('#modal-guide-field').modal('hide');
		$.pjax.reload({container:'#guide-field-grid-pjax'});
	    }
	} else {
	    ". SDNoty::show('result.message', 'result.status') ."
	} 
    }).fail(function() {
	". SDNoty::show("'" . SDHtml::getMsgError() . "Server Error'", '"error"') ."
	console.log('server error');
    });
    return false;
});

");?>