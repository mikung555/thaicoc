<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\testsdii\models\GuideFieldSearch */
/* @var $form yii\bootstrap\ActiveForm */
?>

<div class="guide-field-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
	'layout' => 'horizontal',
	'fieldConfig' => [
	    'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
	    'horizontalCssClasses' => [
		'label' => 'col-sm-2',
		'offset' => 'col-sm-offset-3',
		'wrapper' => 'col-sm-6',
		'error' => '',
		'hint' => '',
	    ],
	],
    ]); ?>

    <?= $form->field($model, 'auto_id') ?>

    <?= $form->field($model, 'uid') ?>

    <?= $form->field($model, 'text_input') ?>

    <?= $form->field($model, 'textarea') ?>

    <?= $form->field($model, 'email') ?>

    <?php // echo $form->field($model, 'number') ?>

    <?php // echo $form->field($model, 'int_input') ?>

    <?php // echo $form->field($model, 'checkbox') ?>

    <?php // echo $form->field($model, 'checkbox_list') ?>

    <?php // echo $form->field($model, 'radio') ?>

    <?php // echo $form->field($model, 'multiple') ?>

    <?php // echo $form->field($model, 'dropdown') ?>

    <?php // echo $form->field($model, 'readonly') ?>

    <?php // echo $form->field($model, 'disabled') ?>

    <?php // echo $form->field($model, 'textmask_input') ?>

    <?php // echo $form->field($model, 'optional_icons') ?>

    <?php // echo $form->field($model, 'select2') ?>

    <?php // echo $form->field($model, 'dropdown_db') ?>

    <?php // echo $form->field($model, 'file') ?>

    <?php // echo $form->field($model, 'date') ?>

    <?php // echo $form->field($model, 'time') ?>

    <?php // echo $form->field($model, 'datetime') ?>

    <?php // echo $form->field($model, 'create_by') ?>

    <?php // echo $form->field($model, 'create_time') ?>

    <?php // echo $form->field($model, 'update_by') ?>

    <?php // echo $form->field($model, 'update_time') ?>

    <div class="form-group">
	<div class="col-sm-offset-2 col-sm-6">
	    <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
	    <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
	</div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
