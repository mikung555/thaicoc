<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\testsdii\models\GuideField */

$this->title = Yii::t('backend', 'Create Guide Field');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Guide Fields'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="guide-field-create">

    <?= $this->render('_form_1', [
        'model' => $model,
    ]) ?>

</div>
