<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\helpers\Url;
use appxq\sdii\widgets\GridView;
use appxq\sdii\widgets\ModalForm;
use appxq\sdii\helpers\SDNoty;
use appxq\sdii\helpers\SDHtml;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\testsdii\models\GuideFieldSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'Guide Fields');
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="guide-field-index">


    <?php  Pjax::begin(['id'=>'guide-field-grid-pjax']);?>
    <?= GridView::widget([
	'id' => 'guide-field-grid',
	'panelBtn' => Html::button(SDHtml::getBtnAdd(), ['data-url'=>Url::to(['guide-field/create']), 'class' => 'btn btn-success btn-sm', 'id'=>'modal-addbtn-guide-field']). ' ' .
		      Html::button(SDHtml::getBtnDelete(), ['data-url'=>Url::to(['guide-field/deletes']), 'class' => 'btn btn-danger btn-sm', 'id'=>'modal-delbtn-guide-field', 'disabled'=>true]),
	'dataProvider' => $dataProvider,
	'filterModel' => $searchModel,
        'columns' => [
	    [
		'class' => 'yii\grid\CheckboxColumn',
		'checkboxOptions' => [
		    'class' => 'selectionGuideFieldIds'
		],
		'headerOptions' => ['style'=>'text-align: center;'],
		'contentOptions' => ['style'=>'width:40px;text-align: center;'],
	    ],
	    [
		'class' => 'yii\grid\SerialColumn',
		'headerOptions' => ['style'=>'text-align: center;'],
		'contentOptions' => ['style'=>'width:60px;text-align: center;'],
	    ],

            //'auto_id',
            [
		'attribute'=>'uid',
		'filter'=> yii\bootstrap\Html::activeDropDownList($searchModel, 'uid', ['yes','no'], ['prompt'=>'all', 'class'=>'form-control']),
		'value'=>function ($data){ return $data['uid']; },
		'headerOptions'=>['style'=>'text-align: center;'],
		'contentOptions'=>['style'=>'width:100px; text-align: center;'],
            ],
            'text_input',
            'textarea:ntext',
            'email:email',
            // 'number',
            // 'int_input',
            // 'checkbox',
            // 'checkbox_list:ntext',
            // 'radio',
            // 'multiple:ntext',
            // 'dropdown',
            // 'readonly',
            // 'disabled',
            // 'textmask_input',
            // 'optional_icons',
            // 'select2',
            // 'dropdown_db',
            // 'file:ntext',
            // 'date',
            // 'time',
            // 'datetime',
            // 'create_by',
            // 'create_time',
            // 'update_by',
            // 'update_time',

	    [
		'class' => 'appxq\sdii\widgets\ActionColumn',
		'contentOptions' => ['style'=>'width:80px;text-align: center;'],
		'template' => '{view} {update} {delete}',
	    ],
        ],
    ]); ?>
    <?php  Pjax::end();?>

</div>

<?=  ModalForm::widget([
    'id' => 'modal-guide-field',
    'size'=>'modal-lg',
]);
?>

<?php  $this->registerJs("
$('#guide-field-grid-pjax').on('click', '#modal-addbtn-guide-field', function() {
    modalGuideField($(this).attr('data-url'));
});

$('#guide-field-grid-pjax').on('click', '#modal-delbtn-guide-field', function() {
    selectionGuideFieldGrid($(this).attr('data-url'));
});

$('#guide-field-grid-pjax').on('click', '.select-on-check-all', function() {
    window.setTimeout(function() {
	var key = $('#guide-field-grid').yiiGridView('getSelectedRows');
	disabledGuideFieldBtn(key.length);
    },100);
});

$('#guide-field-grid-pjax').on('click', '.selectionGuideFieldIds', function() {
    var key = $('input:checked[class=\"'+$(this).attr('class')+'\"]');
    disabledGuideFieldBtn(key.length);
});

$('#guide-field-grid-pjax').on('dblclick', 'tbody tr', function() {
    var id = $(this).attr('data-key');
    modalGuideField('".Url::to(['guide-field/update', 'id'=>''])."'+id);
});	

$('#guide-field-grid-pjax').on('click', 'tbody tr td a', function() {
    var url = $(this).attr('href');
    var action = $(this).attr('data-action');

    if(action === 'update' || action === 'view') {
	modalGuideField(url);
    } else if(action === 'delete') {
	yii.confirm('".Yii::t('app', 'Are you sure you want to delete this item?')."', function() {
	    $.post(
		url
	    ).done(function(result) {
		if(result.status == 'success') {
		    ". SDNoty::show('result.message', 'result.status') ."
		    $.pjax.reload({container:'#guide-field-grid-pjax'});
		} else {
		    ". SDNoty::show('result.message', 'result.status') ."
		}
	    }).fail(function() {
		". SDNoty::show("'" . SDHtml::getMsgError() . "Server Error'", '"error"') ."
		console.log('server error');
	    });
	});
    }
    return false;
});

function disabledGuideFieldBtn(num) {
    if(num>0) {
	$('#modal-delbtn-guide-field').attr('disabled', false);
    } else {
	$('#modal-delbtn-guide-field').attr('disabled', true);
    }
}

function selectionGuideFieldGrid(url) {
    yii.confirm('".Yii::t('app', 'Are you sure you want to delete these items?')."', function() {
	$.ajax({
	    method: 'POST',
	    url: url,
	    data: $('.selectionGuideFieldIds:checked[name=\"selection[]\"]').serialize(),
	    dataType: 'JSON',
	    success: function(result, textStatus) {
		if(result.status == 'success') {
		    ". SDNoty::show('result.message', 'result.status') ."
		    $.pjax.reload({container:'#guide-field-grid-pjax'});
		} else {
		    ". SDNoty::show('result.message', 'result.status') ."
		}
	    }
	});
    });
}

function modalGuideField(url) {
    $('#modal-guide-field .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
    $('#modal-guide-field').modal('show')
    .find('.modal-content')
    .load(url);
}

");?>