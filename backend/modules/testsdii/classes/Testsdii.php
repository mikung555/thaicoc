<?php
namespace backend\modules\testsdii\classes;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
use Yii;

/**
 * Description of Testsdii
 *
 * @author appxq
 */
class Testsdii {
    //put your code here
    public static function testDaoAll(){
        $sql = "SELECT
            `test_input`.*,
            `user_profile`.`firstname`,
            `user_profile`.`lastname`
        FROM
            `test_input`
        JOIN `user_profile`
        ON `test_input`.`updated_by` = `user_profile`.`user_id` 
        ";
        
        return Yii::$app->db->createCommand($sql)->queryAll();
    }
    
    public static function testDaoOne($id){
        $sql = "SELECT
            `test_input`.*,
            `user_profile`.`firstname`,
            `user_profile`.`lastname`
        FROM
            `test_input`
        JOIN `user_profile`
        ON `test_input`.`updated_by` = `user_profile`.`user_id` 
        Where id=:id
        ";
        
        return Yii::$app->db->createCommand($sql, [':id'=>$id])->queryAll();
    }
    
    public static function testQbAll(){
        $query = new \yii\db\Query();
       
        $query->select(['`test_input`.*', '`user_profile`.`firstname`', '`user_profile`.`lastname`']);
        
        $query->from('test_input')
                ->innerJoin('user_profile', '`test_input`.`updated_by` = `user_profile`.`user_id`');
        
        return $query->createCommand()->queryAll();
    }
    
    public static function testQbOne($id){
        $query = new \yii\db\Query();
       
        $query->select(['`test_input`.*', '`user_profile`.`firstname`', '`user_profile`.`lastname`']);
        
        $query->from('test_input')
                ->innerJoin('user_profile', '`test_input`.`updated_by` = `user_profile`.`user_id`')
                ->where('test_input.id=:id', [':id'=>$id]);
        
        return $query->createCommand()->queryOne();
    }
    
    public static function testArAll(){
        $model = \backend\modules\testsdii\models\TestInput::find()
                ->select(['`test_input`.*', '`user_profile`.`firstname`', '`user_profile`.`lastname`'])
                ->innerJoin('user_profile', '`test_input`.`updated_by` = `user_profile`.`user_id`')
                ->all();
        return $model;
    }
    
    public static function testDaoInsert(){
        
        return Yii::$app->db->createCommand()->insert('test_input', [
            'id'=> \common\lib\codeerror\helpers\GenMillisecTime::getMillisecTime(),
            'created_by'=>Yii::$app->user->id,//date('Y-m-d H:i:s')
            'created_at'=>date('Y-m-d H:i:s')
        ])->execute();
    }
    
    public static function testDaoUpdate($id='1487836226095520800'){
        
        return Yii::$app->db->createCommand()->update('test_input', [
            'file'=>'none.png'
        ], 'id=:id',[':id'=>$id])->execute();
    }
    
    public static function testDaoDelete($id='1487836226095520800'){
        
        return Yii::$app->db->createCommand()->delete('test_input',
                'id=:id',[':id'=>$id])->execute();
    }
    
}
