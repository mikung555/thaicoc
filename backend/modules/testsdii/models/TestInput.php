<?php

namespace backend\modules\testsdii\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\behaviors\AttributeBehavior;
use common\lib\codeerror\helpers\GenMillisecTime;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "test_input".
 *
 * @property integer $id
 * @property string $file
 * @property string $province
 * @property string $amphur
 * @property integer $created_by
 * @property string $created_at
 * @property integer $updated_by
 * @property string $updated_at
 */
class TestInput extends ActiveRecord
{
    public $firstname;
    public $lastname;
    
    public function behaviors() {
	    return [
		    [
			    'class' => TimestampBehavior::className(),
			    'value' => new Expression('NOW()'),
		    ],
		    [
			    'class' => BlameableBehavior::className(),
		    ],
                    [
                            'class' => AttributeBehavior::className(),
                            'attributes' => [
                                    ActiveRecord::EVENT_BEFORE_INSERT => 'id',
                            ],
                            'value' => function ($event) {
                                    return GenMillisecTime::getMillisecTime();
                            },
                    ],
	    ];
    }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'test_input';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            //[['id'], 'required'],
            [['id', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['file'], 'string', 'max' => 255],
            //[['file'], 'file', 'skipOnEmpty' => true, 'extensions' => ['png','jpg','jpeg']],
            [['province', 'amphur'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'file' => Yii::t('app', 'รูปภาพ'),
            'province' => Yii::t('app', 'จังหวัด'),
            'amphur' => Yii::t('app', 'อำเภอ'),
            'created_by' => Yii::t('app', 'สร้างโดย'),
            'created_at' => Yii::t('app', 'สร้างเวลา'),
            'updated_by' => Yii::t('app', 'แก้ไขโดย'),
            'updated_at' => Yii::t('app', 'แก้ไขเวลา'),
        ];
    }
}
