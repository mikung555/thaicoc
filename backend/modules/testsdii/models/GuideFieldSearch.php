<?php

namespace backend\modules\testsdii\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\testsdii\models\GuideField;

/**
 * GuideFieldSearch represents the model behind the search form about `backend\modules\testsdii\models\GuideField`.
 */
class GuideFieldSearch extends GuideField
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['auto_id', 'int_input', 'checkbox', 'radio', 'dropdown', 'select2', 'dropdown_db', 'create_by', 'update_by'], 'integer'],
            [['uid', 'text_input', 'textarea', 'email', 'checkbox_list', 'multiple', 'readonly', 'disabled', 'textmask_input', 'optional_icons', 'file', 'date', 'time', 'datetime', 'create_time', 'update_time'], 'safe'],
            [['number'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = GuideField::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'auto_id' => $this->auto_id,
            'number' => $this->number,
            'int_input' => $this->int_input,
            'checkbox' => $this->checkbox,
            'radio' => $this->radio,
            'dropdown' => $this->dropdown,
            'select2' => $this->select2,
            'dropdown_db' => $this->dropdown_db,
            'date' => $this->date,
            'time' => $this->time,
            'datetime' => $this->datetime,
            'create_by' => $this->create_by,
            'create_time' => $this->create_time,
            'update_by' => $this->update_by,
            'update_time' => $this->update_time,
        ]);

        $query->andFilterWhere(['like', 'uid', $this->uid])
            ->andFilterWhere(['like', 'text_input', $this->text_input])
            ->andFilterWhere(['like', 'textarea', $this->textarea])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'checkbox_list', $this->checkbox_list])
            ->andFilterWhere(['like', 'multiple', $this->multiple])
            ->andFilterWhere(['like', 'readonly', $this->readonly])
            ->andFilterWhere(['like', 'disabled', $this->disabled])
            ->andFilterWhere(['like', 'textmask_input', $this->textmask_input])
            ->andFilterWhere(['like', 'optional_icons', $this->optional_icons])
            ->andFilterWhere(['like', 'file', $this->file]);

        return $dataProvider;
    }
}
