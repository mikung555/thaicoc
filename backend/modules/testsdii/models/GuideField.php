<?php

namespace backend\modules\testsdii\models;

use Yii;

/**
 * This is the model class for table "guide_field".
 *
 * @property integer $auto_id
 * @property string $uid
 * @property string $text_input
 * @property string $textarea
 * @property string $email
 * @property double $number
 * @property integer $int_input
 * @property integer $checkbox
 * @property string $checkbox_list
 * @property integer $radio
 * @property string $multiple
 * @property integer $dropdown
 * @property string $readonly
 * @property string $disabled
 * @property string $textmask_input
 * @property string $optional_icons
 * @property integer $select2
 * @property integer $dropdown_db
 * @property string $file
 * @property string $date
 * @property string $time
 * @property string $datetime
 * @property integer $create_by
 * @property string $create_time
 * @property integer $update_by
 * @property string $update_time
 */
class GuideField extends \yii\db\ActiveRecord
{
    public $test;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'guide_field';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['uid', 'text_input'], 'required'],
            [['int_input', 'checkbox', 'radio', 'dropdown', 'select2', 'dropdown_db', 'create_by', 'update_by'], 'integer'],
            [['textarea',  'file'], 'string'],
            [['number'], 'number'],
	    [['uid'], 'unique'],
	    [['email'], 'email'],
            [['checkbox_list', 'multiple', 'date', 'time', 'datetime', 'create_time', 'update_time'], 'safe'],
            [['uid', 'text_input'], 'string', 'max' => 100],
            [['readonly', 'disabled', 'textmask_input', 'optional_icons'], 'string', 'max' => 50],
            [['uid'], 'unique']
        ];
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'auto_id' => Yii::t('app', 'Auto ID'),
            'uid' => Yii::t('app', 'รหัส'),
            'text_input' => Yii::t('app', 'Text Input'),
            'textarea' => Yii::t('app', 'Textarea'),
            'email' => Yii::t('app', 'Email'),
            'number' => Yii::t('app', 'Number'),
            'int_input' => Yii::t('app', 'Int Input'),
            'checkbox' => Yii::t('app', 'Checkbox'),
            'checkbox_list' => Yii::t('app', 'Checkbox List'),
            'radio' => Yii::t('app', 'Radio'),
            'multiple' => Yii::t('app', 'Multiple'),
            'dropdown' => Yii::t('app', 'Dropdown'),
            'readonly' => Yii::t('app', 'Readonly'),
            'disabled' => Yii::t('app', 'Disabled'),
            'textmask_input' => Yii::t('app', 'Textmask Input'),
            'optional_icons' => Yii::t('app', 'Optional Icons'),
            'select2' => Yii::t('app', 'Select2'),
            'dropdown_db' => Yii::t('app', 'Dropdown Db'),
            'file' => Yii::t('app', 'File'),
            'date' => Yii::t('app', 'Date'),
            'time' => Yii::t('app', 'Time'),
            'datetime' => Yii::t('app', 'Datetime'),
            'create_by' => Yii::t('app', 'Create By'),
            'create_time' => Yii::t('app', 'Create Time'),
            'update_by' => Yii::t('app', 'Update By'),
            'update_time' => Yii::t('app', 'Update Time'),
        ];
    }
}
