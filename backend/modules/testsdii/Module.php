<?php

namespace backend\modules\testsdii;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\modules\testsdii\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
