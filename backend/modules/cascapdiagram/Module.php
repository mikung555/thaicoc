<?php

namespace backend\modules\cascapdiagram;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\modules\cascapdiagram\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
