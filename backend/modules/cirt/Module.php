<?php

namespace backend\modules\cirt;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\modules\cirt\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
