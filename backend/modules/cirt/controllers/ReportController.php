<?php

namespace backend\modules\cirt\controllers;

use yii\web\Controller;

class ReportController extends Controller
{
    public function actionIndex()
    {
        return $this->renderAjax('index');
    }

}
