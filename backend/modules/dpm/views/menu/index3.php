<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
$domain=Url::home();

?>
<?php

$roles="อาจารย์";
 

$this->title = Yii::t('app', 'Module สำหรับ '.$roles);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Module','/tccbots/my-module'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

 
<!-- dpm cloud and mobile responsive-->
<!-- row 1   อาจารย์ -->
<?php if (\Yii::$app->user->can("Physician_P")): ?>
<div class="box-body">
 
  <div class="modal-header" style="margin-bottom: 15px;">
      <h3 class="modal-title" id="itemModalLabel"><?= Html::encode($this->title)?></h3>
  </div>
  <div class="modal-body">
    

    <div class="row" style="background-color:rgba(219, 225, 224, 0.46)">


        <div class="col-lg-4 col-md-6 col-xs-12" style="">
            <div class="media-left">
                <a href="http://backend.dpmcloud.org/surgery-form/data-management?ezfdata=1471504881038428900">
                    <div style="margin: 10px;"><img src="<?= $domain ?>/img/icondpm/evaluate.png" width="115"></div>
                </a>
            </div><div class="media-body"><h4 class="media-heading text-bold" style="margin-top:25px">แบบประเมิน</h4></div>
        </div> <!-- ประเมิน -->
        <div class="col-lg-4 col-md-6 col-xs-12">
            <div class="media-left">
                <a href="http://backend.dpmcloud.org/surgery-form/data-management?ezfdata=1471505042031565900">
                    <div style="margin: 10px;"><img src="<?= $domain ?>/img/icondpm/journalclub.png" width="115"></div>
                </a>
            </div>
            <div class="media-body"><h4 class="media-heading text-bold" style="margin-top:25px">Journal Club</h4></div>
        </div> <!-- Journal club -->


        <div class="col-lg-4 col-md-6 col-xs-12" style="">
            <div class="media-left">
                <a href="http://backend.dpmcloud.org/surgery-form/data-management?ezfdata=1490342148003178500">
                    <div style="margin: 10px;"><img src="<?= $domain ?>/img/icondpm/morbidity.png" width="115"></div>
                </a>
            </div>
            <div class="media-body"><h4 class="media-heading text-bold" style="margin-top:25px">Morbidity Mortallity</h4></div>
        </div> <!-- morbidity -->
    </div>  <!-- row -->
    <div class="row" style="background-color:rgba(219, 225, 224, 0.46)">
        <div class="col-lg-4 col-md-6 col-xs-12" style="">
            <div class="media-left">
                <a href="http://backend.dpmcloud.org/surgery-form/data-management?ezfdata=1471505819015097200">
                    <div style="margin: 10px;"><img src="<?= $domain ?>/img/icondpm/interestingcase.png" width="115"></div>
                </a>
            </div>
            <div class="media-body"><h4 class="media-heading text-bold" style="margin-top:25px">Interesting Case</h4></div>
        </div> <!--interesting case 1.7-->
        
        <div class="col-lg-4 col-md-6 col-xs-12" style="">
            <div class="media-left">
                <a href="http://backend.dpmcloud.org/ezdata/input-form/data-management?ezfdata=1504159325091922100">
                    <div style="margin: 10px;"><img src="<?= $domain ?>/img/icondpm/nut03.png" width="115"></div>
                </a>
            </div>
            <div class="media-body"><h4 class="media-heading text-bold" style="margin-top:25px">ประเมินการสอนโดยรวม</h4></div>
        </div> <!--ประเมินการสอนโดยรวม-->
        <div class="col-lg-4 col-md-6 col-xs-12" style="">
            <div class="media-left">
                <a href="http://backend.dpmcloud.org/ezdata/input-form/data-management?ezfdata=1504161176011880100">
                    <div style="margin: 10px;"><img src="<?= $domain ?>/img/icondpm/nut04.png" width="115"></div>
                </a>
            </div>
            <div class="media-body"><h4 class="media-heading text-bold" style="margin-top:25px">ประเมินหน่วย</h4></div>
        </div> <!--ประเมินหน่วย-->
        
    </div>  <!-- row -->
 
    <div class="row" style="background-color:rgb(229, 217, 241)">
        <div class="col-lg-4 col-md-6 col-xs-12">
            <div class="media-left">
                <a href="http://backend.dpmcloud.org/surgery-form/data-management?ezfdata=1476364204008401300">
                    <div style="margin: 10px;"><img src="<?= $domain ?>/img/icondpm/consultation.png" width="115"></div>
                </a></div>
            <div class="media-body"><h4 class="media-heading text-bold" style="margin-top:25px">Consultation</h4>การให้คำปรึกษา<br>
            </div>
        </div> <!-- consult 2.1 -->
        <div class="col-lg-4 col-md-6 col-xs-12" style="">
            <div class="media-left">
                <a href="http://backend.dpmcloud.org/surgery-form/data-management?ezfdata=1476370108094090300">
                    <div style="margin: 10px;"><img src="<?= $domain ?>/img/icondpm/schedule.png" width="115"></div>
                </a>
            </div>
            <div class="media-body"><h4 class="media-heading text-bold" style="margin-top:25px">ตารางเวร</h4>	ภาคศัลย์<br></div>
        </div> <!--schedule- 2.2-->

    </div> <!-- row -->
  
<div class="row" style="background-color:#fcf8e3">
  <div class="col-lg-4 col-md-6 col-xs-12" style="">
      <div class="media-left">
    <a href="http://backend.dpmcloud.org/surgery-form/data-management?ezfdata=1476444477013402500">
        <div style="margin: 10px;"><img src="<?=$domain ?>/img/icondpm/admission.png" width="115"></div>
    </a>
      </div>
      <div class="media-body"><h4 class="media-heading text-bold" style="margin-top:25px">Admission Record</h4></div>
  </div> 
    
    <div class="col-lg-4 col-md-6 col-xs-12" style="">
      <div class="media-left">
    <a href="http://backend.dpmcloud.org/surgery-form/data-management?ezfdata=1476446093049617400">
        <div style="margin: 10px;"><img src="<?=$domain ?>/img/icondpm/operative.png" width="115"></div>
    </a>
      </div>
      <div class="media-body"><h4 class="media-heading text-bold" style="margin-top:25px">Operative Note</h4></div>
  </div> 
    
    <div class="col-lg-4 col-md-6 col-xs-12" style="">
      <div class="media-left">
    <a href="http://backend.dpmcloud.org/surgery-form/data-management?ezfdata=1471506480061189000">
        <div style="margin: 10px;"><img src="<?=$domain ?>/img/icondpm/ot.png" width="115"></div>
    </a>
      </div>
      <div class="media-body"><h4 class="media-heading text-bold" style="margin-top:25px">OT Consult</h4></div>
  </div> 
   
    <div class="col-lg-4 col-md-6 col-xs-12" style="">
      <div class="media-left">
    <a href="http://backend.dpmcloud.org/surgery-form/data-management?ezfdata=1476445023085090200">
        <div style="margin: 10px;"><img src="<?=$domain ?>/img/icondpm/discharge.png" width="115"></div>
    </a>
      </div>
      <div class="media-body"><h4 class="media-heading text-bold" style="margin-top:25px">Discharge Summary</h4></div>
  </div> 
    
    
    
    
    
    <div class="col-lg-4 col-md-6 col-xs-12" style="">
      <div class="media-left">
    <a href="http://backend.dpmcloud.org/surgery-form/data-management?ezfdata=1476701380033198800">
        <div style="margin: 10px;"><img src="<?=$domain ?>/img/icondpm/ov.png" width="115"></div>
    </a>
      </div>
      <div class="media-body"><h4 class="media-heading text-bold" style="margin-top:25px">ส่งตรวจทางพยาธิวิทยา</h4></div>
  </div>
    
    <div class="col-lg-4 col-md-6 col-xs-12" style="">
      <div class="media-left">
    <a href="http://backend.dpmcloud.org/surgery/appoint/index">
        <div style="margin: 10px;"><img src="<?=$domain ?>/img/icondpm/admit.png" height="115"></div>
    </a>
      </div>
      <div class="media-body"><h4 class="media-heading text-bold" style="margin-top:25px">สมุดนัดหมาย Admit</h4></div>
  </div>
     
    <div class="col-lg-4 col-md-6 col-xs-12" style="">
      <div class="media-left">
    <a href="http://backend.dpmcloud.org/surgery/appoint/index?person=2&dept=0">
        <div style="margin: 10px;"><img src="<?=$domain ?>/img/icondpm/endoscopy.png" height="115"></div>
    </a>
      </div>
      <div class="media-body"><h4 class="media-heading text-bold" style="margin-top:25px">สมุดนัดหมาย Endoscopy</h4></div>
  </div>
    
    <div class="col-lg-4 col-md-6 col-xs-12" style="">
      <div class="media-left">
    <a href="http://backend.dpmcloud.org/surgery/appoint/index?person=3&dept=0">
        <div style="margin: 10px;"><img src="<?=$domain ?>/img/icondpm/orminor.png" height="115"></div>
    </a>
      </div>
      <div class="media-body"><h4 class="media-heading text-bold" style="margin-top:25px">สมุดนัดหมาย OR Minor</h4></div>
  </div>
    
    <div class="col-lg-4 col-md-6 col-xs-12" style="">
      <div class="media-left">
    <a href="http://backend.dpmcloud.org/surgery/appoint/index?person=4&dept=0">
        <div style="margin: 10px;"><img src="<?=$domain ?>/img/icondpm/eswl.png" height="115"></div>
    </a>
      </div>
      <div class="media-body"><h4 class="media-heading text-bold" style="margin-top:25px">สมุดนัดหมาย ESWL</h4></div>
  </div>
    
   <div class="col-lg-4 col-md-6 col-xs-12" style="">
      <div class="media-left">
            <a href="http://backend.dpmcloud.org/ezdata/input-form/data-management?ezfdata=1476696513076161100">
                    <div style="margin: 10px;"><img src="<?= $domain ?>/img/icondpm/nut05.png" width="115"></div>
                </a>
      </div>
      <div class="media-body"> <h4 class="media-heading text-bold" style="margin-top:25px">วันหยุดนักขัตฤกษ์</h4></div>
  </div> 
    
</div>  <!-- row -->
 
  <div class="row" style="background-color:rgba(186, 226, 230, 0.55)">
    <div class="col-lg-4 col-md-6 col-xs-12">
    <div class="media-left">
    <a href="http://backend.dpmcloud.org/surgery-form/data-management?ezfdata=1475657454095297700">
    <div style="margin: 10px;"><img src="<?=$domain ?>/img/icondpm/file.png" width="115"></div>
    </a></div>
      <div class="media-body">
      <h4 class="media-heading text-bold" style="margin-top:25px">เวชระเบียน</h4>
          <br>

      </div>
    </div> <!-- file -->
    <div class="col-lg-4 col-md-6 col-xs-12" style="">
       <div class="media-left">
      <a href="http://backend.dpmcloud.org/surgery-form/data-management?ezfdata=1471495957074379700">
      <div style="margin: 10px;"><img src="<?=$domain ?>/img/icondpm/document.png" width="115"></div>
      </a>
        </div>
        <div class="media-body">
      <h4 class="media-heading text-bold" style="margin-top:25px">เอกสารรับ ส่ง</h4>
    </div>
    </div>
    <div class="col-lg-4 col-md-6 col-xs-12">
            <div class="media-left">
          <a href="http://backend.dpmcloud.org/surgery-form/data-management?ezfdata=1471501380014927900">
          <div style="margin: 10px;"><img src="<?=$domain ?>/img/icondpm/drugs.png" width="115"></div>
          </a>
            </div>
            <div class="media-body"><h4 class="media-heading text-bold" style="margin-top:25px">ทะเบียนจัดหายา</h4>List Drugs<br></div>
        </div> <!-- drug 3.3-->
    </div> <!-- row -->
  
<div class="row"  style="background-color:rgba(186, 226, 230, 0.55)">
  <div class="col-lg-4 col-md-6 col-xs-12" style="">
        <div class="media-left">
      <a href="http://backend.dpmcloud.org/surgery-form/data-management?ezfdata=1471501903082542200">
          <div style="margin: 10px;"><img src="<?=$domain ?>/img/icondpm/insure.png" width="115"></div>
      </a>
        </div>
        <div class="media-body"><h4 class="media-heading text-bold" style="margin-top:25px">ใบประกันชีวิต</h4>  </div>
    </div>    <!--insure 3.4 -->
    <div class="col-lg-4 col-md-6 col-xs-12" style="">
  	    <div class="media-left">
  		<a href="http://backend.dpmcloud.org/surgery-form/data-management?ezfdata=1471504108010467500">
  		    <div style="margin: 10px;"><img src="<?=$domain ?>/img/icondpm/activity.png" width="115"></div>
  		</a>
  	    </div>
  	    <div class="media-body">
  		<h4 class="media-heading text-bold" style="margin-top:25px">กิจกรรมทางวิชาการ</h4>
  	    </div>
  	</div> <!-- กิจกรรมทางวิชาการ 3.5-->
    <div class="col-lg-4 col-md-6 col-xs-12" >
            <div class="media-left">
          <a href="http://backend.dpmcloud.org/surgery-form/data-management?ezfdata=1471504685096828800">
              <div style="margin: 10px;"><img src="<?=$domain ?>/img/icondpm/ads.png" width="115"></div>
          </a>
            </div>
            <div class="media-body">
          <h4 class="media-heading text-bold" style="margin-top:25px">คำสั่ง ประกาศ</h4>
            </div>
    </div> <!--  ประกาศ 3.6-->
    
    
   
    
      </div> <!-- row-->
        </div> <!-- body -->
</div> <!-- box body --> 
<?php else: ?> 
  <?= $this->render("404.php",["role"=>"อาจารย์"]); ?> 


<?php endif; ?> 