<?php

namespace backend\modules\dpm\controllers;

use yii\web\Controller;

class MenuController extends Controller
{
    public function actionIndex()
    {
        return $this->render('index');
    }
    public function actionIndex2()
    {
        $page = \Yii::$app->request->get("page",'');
        if($page == '2'){
            $page = "index2";
        }else if($page == '3'){
            $page = "index3";
        }else if($page == '4'){
            $page = "index4";
        }else{
           $page = "index2"; 
        }
        return $this->render($page);
    }
}
