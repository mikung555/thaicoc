<?php
/* @var $this yii\web\View */
use kartik\grid\GridView;
use yii\helpers\Html;
$this->title = Yii::t('app', 'Refer');
$this->params['breadcrumbs'][] = $this->title;
?>
<?php
echo Html::a('สถานะการส่ง Refer', [
        '/refer/refer',
    ],['class' =>'btn btn-danger btn-lg']).' ';
echo Html::a('สถานะการรับ Refer',[
    '/refer/refer',
    'action' =>'accept'
    ],['class' =>'btn btn-success btn-lg']).'<br><br>';
echo GridView::widget([
    'dataProvider'=>$referSqlProvider,
    //'filterModel'=>$searchModel,
    // 'columns'=>$gridColumns,
    'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => ''],
    'resizableColumns'=>true,
    'columns'=>[
        [
            'class' => 'yii\grid\SerialColumn',
            'headerOptions' => ['style'=>'text-align: center;'],
            'contentOptions' => ['style'=>'min-width:60px;text-align: center;'],
        ],
        [
            'format' => 'text',
            'label' => 'ชื่อ - สกุล',
            'headerOptions' => ['class' => 'text-center'],
            'contentOptions' => ['class' => 'text-left'],
            'value' => function($model){
                $sql="SELECT hsitecode,
                            hptcode,
                            `name`,
                            surname
                      FROM tbdata_1
                      WHERE ptid=:ptid
                      AND rstat <>'3'
                      AND rstat <>'0'
                      ORDER BY id DESC LIMIT 1";
                $register = \Yii::$app->db->createCommand($sql,[':ptid' => $model['ptid']])->queryOne();
               return $register['hsitecode'].' '.$register['hptcode'].' '.$register['name'].' '.$register['surname'];
            }
        ],
        [
            'format' => 'raw',
            'label' => 'ส่งจาก',
            'headerOptions' => ['class' => 'text-center'],
            'contentOptions' => ['class' => 'text-center'],
            'value' => function($model){
                $sql="SELECT
                      hcode,name
                      FROM all_hospital_thai
                      WHERE hcode=:xsourcex
                      LIMIT 1";
                $hospital = \Yii::$app->db->createCommand($sql,[':xsourcex' => $model['xsourcex']])->queryOne();
                return '<h5><i class="fa fa-ambulance"></i> '.Html::label($hospital['hcode'],null,['title' =>$hospital['name'],'class'=>'label label-danger']).'</h5>';

            }
        ],
        [
            'label' => 'วันที่ส่ง',
            'headerOptions' => ['class' => 'text-center'],
            'contentOptions' => ['class' => 'text-center'],
            'value' => function($model){
                $date = new DateTime($model['create_date']);
                return $date->format('d/m/').($date->format('Y')+543);
            }
        ],
        [
            'format' => 'raw',
            'label' => 'ส่งต่อไปที่',
            'headerOptions' => ['class' => 'text-center'],
            'contentOptions' => ['class' => 'text-center'],
            'value' => function($model){
                $sql="SELECT
                      hcode,name
                      FROM all_hospital_thai
                      WHERE hcode=:hospitalName2
                      LIMIT 1";
                $hospital = \Yii::$app->db->createCommand($sql,[':hospitalName2' => $model['hospitalName2']])->queryOne();
                if($model['hospitalName2']==null)
                    return '<p class="text-muted">ไม่ระบุ</p>';
                else
                    return '<h5><i class="fa fa-heartbeat"></i> '.Html::label($hospital['hcode'],null,['title' =>$hospital['name'],'class'=>'label label-success']).'</h5>';
            }
        ],
        [
            'label' => 'วันที่รับ Refer',
            'headerOptions' => ['class' => 'text-center'],
            'contentOptions' => ['class' => 'text-center'],
            'value' =>function($model){
                $sql="SELECT
                      update_date
                      FROM refer_to
                      WHERE dataid=:id
                      LIMIT 1";
                $refer = \Yii::$app->db->createCommand($sql,[':id' => $model['id']])->queryOne();
                $date = new DateTime($refer['update_date']);
                return $refer['update_date'] ? $date->format('d/m/').($date->format('Y')+543) : 'ยังไม่รับ';
            },

        ],
        [
            'format' => 'raw',
            'label' => 'สถานะ',
            'headerOptions' => ['class' => 'text-center'],
            'contentOptions' => ['class' => 'text-center'],

            'value' =>function($model)
            {
                $sql="SELECT
                      dataid
                      FROM refer_to
                      WHERE dataid=:id
                      LIMIT 1";
                $refer = \Yii::$app->db->createCommand($sql,[':id' => $model['id']])->queryOne();
                if(\Yii::$app->request->get('action')) {
                    $html = Html::a('รับ Refer', [
                        'add-refer',
                        'refer_id' => $model['id']
                    ], ['class' => 'btn btn-success btn-md', 'disabled' => $refer['dataid'] ? true : false]);
                }
                else {
                    $html = '<h5>'.Html::label($refer['dataid'] ? 'รับแล้ว' : 'รอรับ Refer...', null, ['class' => $refer['dataid'] ? 'label label-primary' : 'label label-success',]).'</h5>';
                }
                return $html;
//                return Html::a('<i '.$icon.'></i>', Url::to(['ov-person/status', 'id'=>$data->id]), [
//                    'class' => 'btn btn-xs btn-'.$btn,
//                    'data-method' => 'post',
//                    'data-action' => 'ov01-status',
//                ]
            }
        ],
        [
            'format' => 'raw',
            'label' => 'ดูฟอร์ม',
            'headerOptions' => ['class' => 'text-center'],
            'contentOptions' => ['class' => 'text-center'],
            'value' =>function($model)
            {
                return Html::a("<span class='fa fa-file-text-o fa-2x text-success'></span>",
                    [
                        '/inputdata/redirect-page',
                        'dataid' => $model['id'], 'ezf_id' => '1450928555015607100','rurl'=> base64_encode(Yii::$app->request->url)
                    ],
                    [
                        'title' => 'ดูฟอร์ม',
                        'data-pjax' => '0'
                    ]);
            }
        ],
    ],
    'containerOptions'=>['style'=>'overflow: auto'], // only set when $responsive = false
    'headerRowOptions'=>['class'=>'kartik-sheet-style'],
    'filterRowOptions'=>['class'=>'kartik-sheet-style'],
    'pjax'=>true, // pjax is set to always true for this demo
    // set your toolbar
    'toolbar'=> [
//        ['content'=>
//            Html::button('<i class="glyphicon glyphicon-plus"></i>', ['type'=>'button', 'title'=>Yii::t('kvgrid', 'Add Book'), 'class'=>'btn btn-success', 'onclick'=>'alert("This will launch the book creation form.\n\nDisabled for this demo!");']) . ' '.
//            Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['grid-demo'], ['data-pjax'=>0, 'class'=>'btn btn-default', 'title'=>Yii::t('kvgrid', 'Reset Grid')])
//        ],
        '{export}',
        '{toggleData}',
    ],
    // set export properties
    'export'=>[
        'fontAwesome'=>true
    ],
    // parameters from the demo form
    'bordered'=>$bordered,
    'striped'=>$striped,
    'condensed'=>$condensed,
    'responsive'=>true,
    'hover'=>$hover,
    //'showPageSummary' => true,
    'showPageSummary'=>$pageSummary,
    'panel'=>[
        'type'=>\Yii::$app->request->get('action') ? GridView::TYPE_SUCCESS : GridView::TYPE_DANGER,
        'heading'=>$heading,
    ],
    'persistResize'=>false,
    'exportConfig'=>$exportConfig,
]);
?>