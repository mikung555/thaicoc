<?php

namespace backend\modules\refer;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\modules\refer\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
