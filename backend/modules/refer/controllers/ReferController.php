<?php

namespace backend\modules\refer\controllers;

use yii\data\SqlDataProvider;
use yii\helpers\VarDumper;

class ReferController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $action = \Yii::$app->request->get('action');
            $userProfile = \Yii::$app->user->identity->userProfile;
            $sql = "SELECT id,
                    ptid,
                    xsourcex,
                    create_date,
                    hospitalName2,
                    user_update
                FROM tbdata_4
                where `".($action=="accept" ? 'hospitalName2' : 'xsourcex')."`=:sitecode2
                AND rstat <>'3'
                AND rstat <>'0'
               ORDER  BY create_date";
            $count = \Yii::$app->db->createCommand($sql, [':sitecode2' => $userProfile->sitecode])->query()->count();

            $referSqlProvider = new SqlDataProvider([
                'sql' => $sql,
                'params' => [':sitecode2' => $userProfile->sitecode],
                'pagination' => [
                    'pageSize' => 50,
                ],
                'totalCount' => $count
            ]);
            //echo $sql; exit;
            return $this->render('index', [
                'referSqlProvider' => $referSqlProvider,
            ]);

        }

    public function actionAddRefer()
    {
        $refer_id =\Yii::$app->request->get('refer_id');

        \Yii::$app->db->createCommand("INSERT into refer_to (dataid, hsitecode,hptcode, target, sitefrom, create_date, siteto,update_date, status)
 SELECT id,hsitecode,hptcode,target,xsourcex,create_date,hospitalName2,now(),2  FROM tbdata_4 WHERE id=:id", [':id'=>$refer_id])->execute();
        $this->redirect('/refer');
    }


}
