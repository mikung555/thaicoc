<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace backend\modules\cpay\classes;
use Yii;

/**
 * Description of ListQuery
 *
 * @author chaiwat
 */
class QueryGetSummary {
    public static function getData($period, $view, $zone, $prov='', $amp='', $hosp='') {
        if( strlen($period)==0 ){
            $period = "20170301";
        }
        $sql = "select * from cascap_cpay.period".$period."_all ";
        $sql.= "where length(username)>0 and length(trim(fullname))>0 ";
        if( strlen($zone)>0 && $view=='summaryzone'){
            $sql.= "and zone_code=\"".addslashes($zone)."\" ";
        }
        if( strlen($prov)>0 && ($view=='summaryprov') ){
            $sql.= "and provincecode=\"".addslashes($prov)."\" ";
        }
        if( strlen($amp)>0 && ($view=='summaryamp') ){
            $sql.= "and provincecode=\"".addslashes($prov)."\" and amphurcode=\"".addslashes($amp)."\" ";
        }
        if( strlen($hosp)>0 && ($view=='summaryhosp') ){
            $sql.= "and hsitecode=\"".addslashes($hosp)."\" ";
        }
        $sql.="order by zone_code, provincecode, amphurcode, tamboncode, hsitecode, username ";
        
        //echo $sql;
        
        $dataProvider = Yii::$app->db->createCommand($sql)->queryAll();
        
        if( count($dataProvider)>0 ){
            foreach($dataProvider as $k => $v){
                $outsum = 0;
                $tablename['record'][$dataProvider[$k][zone_code]][$dataProvider[$k][provincecode]][$dataProvider[$k][amphurcode]][$dataProvider[$k][hsitecode]][$dataProvider[$k][username]]=$v;
                $tablename['province'][$dataProvider[$k][provincecode]]=$v['province'];
                $tablename['amphur'][$dataProvider[$k][provincecode]][$dataProvider[$k][amphurcode]]=$v['amphur'];
                $tablename['hospital'][$dataProvider[$k][provincecode]][$dataProvider[$k][amphurcode]][$dataProvider[$k][hsitecode]]=$v['hospital'];
                $tablename['fullname'][$dataProvider[$k][username]]=$v['fullname'];
                
                
                
                
                if( $v['paytype'] == 'keyin' ){
                    // รวมค่าใช้จ่าย (key - CCA-01)
                    $outsum = $outsum + self::getCCA01KeyPay($v['cca01_confirm']);
                    // รวมค่าใช้จ่าย (key - CCA-02)
                    $outsum = $outsum + self::getCCA02KeyPay($v['cca02_confirm']);
                    // รวมค่าใช้จ่าย (key - CCA-02.1)
                    $outsum = $outsum + self::getCCA02p1KeyPay($v['cca02p1_confirm']);
                    // รวมค่าใช้จ่าย (key - CCA-03)
                    $outsum = $outsum + self::getCCA03KeyPay($v['cca03_confirm']);
                    $outsum = $outsum + self::getCCA04KeyPay($v['cca04_confirm']);
                    $outsum = $outsum + self::getCCA05KeyPay($v['cca05_confirm']);
                }

                // ค่าตอบแทนแพทย์ผู้ตรวจ
                if( $v['paytype'] == 'service' ){
//                    $outsum = $outsum + self::getCCA02ServNoneImgPay($v['cca02_confirm_cascap_noimage']) + self::getCCA02ServNoneImgPay($v['cca02_confirm_person_noimage']);
//                    $outsum = $outsum + self::getCCA02ServImgPay($v['cca02_confirm_cascap_image']) + self::getCCA02ServImgPay($v['cca02_confirm_person_image']);
                    $outsum = $outsum + self::getCCA02ServNoneImgPay($v['cca02_service_noimage']);
                    $outsum = $outsum + self::getCCA02ServImgPay($v['cca02_service_image']);
                    $outsum = $outsum + self::getCCA02p1ServPay($v['cca02p1_service']);
                    $outsum = $outsum + self::getCCA03ServPayFirstvisit($v['cca03_confirm_service_first']);
                    $outsum = $outsum + self::getCCA03ServPayFollowup($v['cca03_confirm_service_followup']);
                    $outsum = $outsum + self::getCCA04ServPayBiopsy($v['cca04_confirm_service_biopsy']);
                    $outsum = $outsum + self::getCCA04ServPayLiverResection($v['cca04_confirm_service_liverresection']);
                }
                // เพื่อแยกประเภท การจ่าย [เจ้าหน้าที่ CASCAP] หรือ [คนพื้นที่]
                if( $v['usergroup_type']=='1' ){
                    $tablename['record'][$dataProvider[$k][zone_code]][$dataProvider[$k][provincecode]][$dataProvider[$k][amphurcode]][$dataProvider[$k][hsitecode]][$dataProvider[$k][username]]['s_pay']=$outsum;
                }else{
                    $tablename['record'][$dataProvider[$k][zone_code]][$dataProvider[$k][provincecode]][$dataProvider[$k][amphurcode]][$dataProvider[$k][hsitecode]][$dataProvider[$k][username]]['u_pay']=$outsum;
                }
                
            }
            if( 0 ){
                echo "<pre align='left'>";
                print_r($tablename);
                echo "</pre>";
            }
        }
        $out    = $tablename;
        return $out;
    }
    
    public static function getDataByUsername($period,$username,$hsitecode) {
        if( strlen($username)>0 && strlen($period)>0 ){
            //
            $sql = "select * from cascap_cpay.period".$period."_all ";
            $sql.= "where username=:username ";
            $sql.= "and hsitecode=:hsitecode ";
            $sql.=  "limit 1 ";
            $provider   = Yii::$app->db->createCommand($sql,[':username'=>$username,':hsitecode'=>$hsitecode])->queryOne();
        }
        $out    = $provider;
        return $out;
    }
    public static function getCCA01KeyPay($amount){
        $out    = $amount*20;
        return $out;
    }
    public static function getCCA02KeyPay($amount){
        $out    = $amount*20;
        return $out;
    }
    public static function getCCA02ServNoneImgPay($amount){
        $out    = $amount*50;
        return $out;
    }
    public static function getCCA02ServImgPay($amount){
        $out    = $amount*100;
        return $out;
    }
    public static function getCCA02p1KeyPay($amount){
        $out    = $amount*20;
        return $out;
    }
    public static function getCCA02p1ServPay($amount){
        $out    = $amount*300;
        return $out;
    }
    public static function getCCA03KeyPay($amount){
        $out    = $amount*20;
        return $out;
    }
    public static function getCCA03ServPayFirstvisit($amount){
        $out    = $amount*300;
        return $out;
    }
    public static function getCCA03ServPayFollowup($amount){
        $out    = $amount*100;
        return $out;
    }
    
    public static function getCCA04KeyPay($amount){
        $out    = $amount*20;
        return $out;
    }
    public static function getCCA04ServPayBiopsy($amount){
        $out    = $amount*200;
        return $out;
    }
    public static function getCCA04ServPayLiverResection($amount){
        $out    = $amount*300;
        return $out;
    }
    public static function getCCA05KeyPay($amount){
        $out    = $amount*20;
        return $out;
    }
}