<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace backend\modules\cpay\classes;

use Yii;
/**
 * Description of LogUserAccess
 *
 * @author chaiwat
 */
class LogUserAccess {
    //put your code here
    public static function openY60(){
        $period = '';
        if( strlen($period)==0 ){
            $period='20170301';
        }
        $userid         = Yii::$app->user->identity->id;
        $usersitecode   =Yii::$app->user->identity->userProfile->sitecode;
        $session        = Yii::$app->session;
        // if session is not open, open session
        if ( !$session->isActive) { $session->open(); }

        // get session id
        $session->getId();
        //$userid=1;
        $sql = "replace into cascap_cpay.log_access_by_user ";
        $sql.= "set session_id=:session_id ";
        $sql.= ",user_id=:userid ";
        $sql.= ",sitecode=:sitecode ";
        $sql.= ",dadd=NOW() ";
        $sql.= ",period=:period ";
        $result =  Yii::$app->db->createCommand($sql,[':session_id'=>$session->getId(), ':userid'=>$userid, ':sitecode'=>$usersitecode , ':period'=>$period ])->execute();
        return $result; 
    }
    
    public static function openIndvReport($dataofuser, $paytype = null){
        $period = '';
        if( strlen($period)==0 ){
            $period='20170301';
        }
        if( strlen($paytype)== 0 ){
            $paytype = 'keyin';
        }
        $userid         = Yii::$app->user->identity->id;
        $userusername   = Yii::$app->user->identity->username;
        $usersitecode   = Yii::$app->user->identity->userProfile->sitecode;
        $session        = Yii::$app->session;
        // if session is not open, open session
        if ( !$session->isActive) { $session->open(); }

        // get session id
        $session->getId();
        //$userid=1;
        $sql = "replace into cascap_cpay.log_access_report_user_view ";
        $sql.= "set session_id=:session_id ";
        $sql.= ",sitecode=:sitecode ";
        $sql.= ",user_id=:userid ";
        $sql.= ",username=:userusername ";
        $sql.= ",dataofuser=:dataofuser ";
        $sql.= ",paytype=:paytype ";
        $sql.= ",dadd=NOW() ";
        $sql.= ",period=:period ";
        $result =  Yii::$app->db->createCommand($sql
                ,[
                    ':session_id'=>$session->getId(), 
                    ':sitecode'=>$usersitecode ,
                    ':userid'=>$userid,  
                    ':userusername'=>$userusername ,
                    ':dataofuser'=>$dataofuser ,
                    ':paytype'=>$paytype ,
                    ':period'=>$period 
                ])->execute();
        return $result; 
    }
}
