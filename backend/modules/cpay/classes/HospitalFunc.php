<?php
namespace backend\modules\cpay\classes;

use Yii;
//use yii\helpers\Html;

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class HospitalFunc {
    
    public static function menuICFAll() {
	//$txtMenu[];
       
        $txtMenu[0]=  Html::a('Register', ['/icf/reg'], ['class'=>'btn btn-info', 'role'=>'button']);
	return $txtMenu;
    }
    
    public static function userOnHospital($sitecode){
        
        //$userid=1;
        $sql = "select * from all_hospital_thai where hcode=\"".addslashes($sitecode)."\" ";
        $result =  Yii::$app->db->createCommand($sql)->queryAll();
        return $result; 
    }
    
    public static function getProvinceByZonecode($zone){
        if(strlen($zone)>0 ){
            $sql =  "select distinct provincecode,province ";
            $sql.=  "from cascap_cpay.`period00_all_hospital_active` ";
            $sql.=  "where zone_code=:zone ";
            $result =  Yii::$app->db->createCommand($sql,[':zone'=>$zone])->queryAll();
            return $result; 
        }
    }
    public static function getAmphurByProvincecode($prov){
        if(strlen($prov)>0 ){
            $sql =  "select distinct amphurcode,amphur ";
            $sql.=  "from cascap_cpay.`period00_all_hospital_active` ";
            $sql.=  "where provincecode=:prov ";
            $result =  Yii::$app->db->createCommand($sql,[':prov'=>$prov])->queryAll();
            return $result; 
        }
    }
    public static function getHospitalByAmphur($prov,$xax){
        if(strlen($prov)>0 ){
            $sql =  "select distinct hcode,name ";
            $sql.=  "from cascap_cpay.`period00_all_hospital_active` ";
            $sql.=  "where provincecode=:prov ";
            $sql.=  "and amphurcode=:xax ";
            $sql.=  "order by hcode ";
            $result =  Yii::$app->db->createCommand($sql,[':prov'=>$prov, ':xax'=>$xax])->queryAll();
            return $result; 
        }
    }
}

