<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace backend\modules\cpay\classes;

use Yii;


/**
 * Description of UserCanView
 *
 * @author chaiwat
 * 
 * กำหนดกลุ่ม User ที่สามารถดูข้อมูลได้
 * 
 *  1. individual    => ดูเป็นรายคน ทุกคนมีสิทธิ์เข้าดู
 *  2. Admin site    => Admin ประจำโรงพยาบาล ดูได้ทุกคนในโรงพยาบาล
 *  3. อำเภอ        => เจ้าหน้าที่ที่สามารถดู สรุป ในรายอำเภอ และ แสดงเป็นรายชื่อได้ กรณี มีข้อมูลในหน่วยบริการของตัวเอง
 *  4. จังหวัด         => สสจ. สามารถเข้าดูได้ ทั้งหมดทุกคน ภายในเขตตัวเอง
 *  5. ประเทศ        => ดูภาพรวม รายจังหวัด ได้ทั้งประเทศ
 *  6. การเงิน        => สามารถเลือก เป็นรายโรงพยาบาล หรือ เลือกดูเป็นรายจังหวัดได้ 
 * 
 * 
 *  สิทธิ์ในการเข้าดู
 *      1. ดูจำนวนยอดรวมตัวเลขสรุปยอดเงินได้              4, 6
 *      2. ดูข้อมูลตัวเองได้ (คลิ๊กเล้วเห็นข้อมูลตัวเอง)      1, 2, 3, 4, 5, 6
 *      3. เห็น ข้อมูลของสมาชิกทั้ง รพ. ได้                2, 4, 6
 *      4. ดูสรุป ของแต่ละ รพ.                           2, 3, 4, 6
 *      5. ดูสรุป ข้อมูลของ รพ. อื่นๆ                       3, 4, 6
 *      6. ดูสรุป ของอำเภอ                              3, 4, 6
 *      7. ดูสรุป ของ อ. อื่น                              4, 6
 *      8. ดูสรุป ของแต่ละ จังหวัด                         4, 5, 6
 * 
 *      20. ดู ข้อมูล โรงพยาบาลอื่นได้                      4, 6
 *      21. ดู ข้อมูล อำเภออื่นได้                         4, 6
 *      22. ดู ข้อมูล จังหวัดอื่นได้                          6
 *      
 */


class UserCanView {
    //put your code here
    
    public static function getUserGroup($userid, $userprofile = null){
        unset($_gvalue);
        
        $_gvalue['userpriv']['centraladmin'][]="1015";
        $_gvalue['userpriv']['centraladmin'][]="1435745159010043375";   // กี้
        $_gvalue['userpriv']['centraladmin'][]="1052";
        $_gvalue['userpriv']['centraladmin'][]="1435745159010043405";
        $_gvalue['userpriv']['centraladmin'][]="1459167103058663203";   // น้อย
        $_gvalue['userpriv']['centraladmin'][]="1465958599053698650";   // ตี้ วีรวัฒน์ 
        $_gvalue['userpriv']['centraladmin'][]="1459167103058663205";   // ตาล
        $_gvalue['userpriv']['centraladmin'][]="1465958599053698985";   // แบงค์
        $_gvalue['userpriv']['centraladmin'][]="660";                   // เบิ้ม
        $_gvalue['userpriv']['centraladmin'][]="1435745159010043443";   // เบิ้ม
        $_gvalue['userpriv']['centraladmin'][]="1491188730027938000";    // น้องการเงินคนใหม่ 3410500093131 น.ส.นาฎลัดดา  ติงสศรีภาคย์
        
        //echo $userid;
        
        if( strlen($userid)>0 ){
            // ดึงข้อมูลที่เกี่ยวข้อง
            if( in_array($userid, $_gvalue['userpriv']['centraladmin'] ) ){
                $_usergroup = 6;
            }
        }
        
        // fix assign group
//        if( $userid=='1435745159010043375' ){
//            $_usergroup = 6;
//        }else{
//            $_usergroup = 6; # froce to central admin (การเงิน)
//        }
        
        // สสจ
        if($_usergroup>0){
            //
        }else{
            if( $userprofile[hospital][0][code4]=='1' ){
                $_usergroup = 4;
            }
        }
        return $_usergroup;
    }
    
    public static function getPrivilege($userid, $userprofile = null){
        // การเงิน
        // ส่วนกลาง
        $_usergroup     = self::getUserGroup($userid, $userprofile);
        // Over rule
        
        $out[1] = false;
        $out[2] = false;
        $out[3] = false;
        $out[4] = false;
        $out[5] = false;
        $out[6] = false;
        $out[7] = false;
        $out[8] = false;
        $out[9] = false;
        $out[10] = false;
        $out[20] = false;
        //echo $_usergroup;
        switch ($_usergroup){
            case 1 :
                $out[1] = false;
                $out[2] = true;
                $out[3] = false;
                $out[4] = true;
                $out[5] = false;
                $out[6] = false;
                $out[7] = false;
                $out[8] = false;
                $out[9] = false;
                $out[10] = false;
                break;
            case 2 :
                $out[1] = false;
                $out[2] = true;
                $out[3] = true;
                $out[4] = true;
                $out[5] = false;
                $out[6] = false;
                $out[7] = false;
                $out[8] = false;
                $out[9] = false;
                $out[10] = false;
                break;
            case 3 :
                $out[1] = false;
                $out[2] = true;
                $out[3] = false;
                $out[4] = true;
                $out[5] = true;
                $out[6] = true;
                $out[7] = false;
                $out[8] = false;
                $out[9] = false;
                $out[10] = false;
                break;
            case 4 :
                $out[1] = true;
                $out[2] = true;
                $out[3] = true;
                $out[4] = true;
                $out[5] = true;
                $out[6] = true;
                $out[7] = true;
                $out[8] = true;
                $out[9] = true;
                $out[10] = true;
                $out[20] = true;
                break;
            case 6 :
                $out[1] = true;
                $out[2] = true;
                $out[3] = true;
                $out[4] = true;
                $out[5] = true;
                $out[6] = true;
                $out[7] = true;
                $out[8] = true;
                $out[9] = true;
                $out[10] = true;
                $out[20] = true;
                break;
        }
//        $out[1] = true;
//        $out[2] = true;
//        $out[3] = true;
//        $out[4] = true;
//        $out[5] = true;
//        $out[6] = true;
//        $out[7] = true;
//        $out[8] = true;
//        $out[9] = true;
//        $out[10] = true;
        
        return $out;
    }
    
    public static function checkKeyin($period, $username, $keytype = null) {
        $checkKeyin = false;
        if( strlen($period)==0 ){
            $period = "20170301";
        }
        if( strlen($keytype)==0 ){
            $keytype = 'keyin';
        }
        $sql = "select count(*) as recs from INFORMATION_SCHEMA.TABLES ";
        $sql.= "where TABLE_SCHEMA='cascap_cpay' ";
        $sql.= "AND TABLE_NAME LIKE 'period".$period."_all' ";
        $tableexist = Yii::$app->db->createCommand($sql)->queryOne();
        if($tableexist[recs]>0){
            $tablename = 'period'.$period.'_all';
        }
        if( strlen($tablename)>0 ){
            $sql = 'select count(*) as recs from cascap_cpay.'.$tablename;
            $sql.= ' where username=:username ';
            $sql.= 'and paytype=:keytype ';
            $userkeyin = Yii::$app->db->createCommand($sql ,[':username'=>$username, ':keytype'=>$keytype] )->queryOne();
        }
        
        if( $userkeyin[recs]>0 ){
            $checkKeyin = true;
        }
        return $checkKeyin;
    }
}
