<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace backend\modules\cpay\classes;

use Yii;

/**
 * Description of ListQuery
 *
 * @author chaiwat
 */
class ListQuery {
    public static $cca02_period3 = 'period03_tb_data_3_201611_image';
    public static $cca02_period4 = 'period04_tb_data_3_201612_image';
    //put your code here
    public static function getTable($period,$crf) {
        $cca02_period3 = self::$cca02_period3;
        $cca02_period4 = self::$cca02_period4;
	$tablename = "tb_data_2";
        if( $period=='2.1' && $crf=='cca02'){
            $tablename="cascap_cpay.period02_tb_data_3_201610_image";
        }else if( $period=='2' && $crf=='cca02'){
            $tablename="cascap_cpay.period02_tb_data_3_201610";
        }else if( $period=='3' && $crf=='cca02'){
            $tablename="cascap_cpay.".$cca02_period3;
        }else if( $period=='4' && $crf=='cca02'){
            $tablename="cascap_cpay.".$cca02_period4;
        }
        return $tablename;
    }
    
    public static function getEzForm($crf){
        /*
         *            
            $linkREG="https://cloudbackend.cascap.in.th/inputdata/redirect-page/?ezf_id=1437377239070461301&dataid=".$___data["reg"]["id"];
            $linkCCA01="https://cloudbackend.cascap.in.th/inputdata/redirect-page/?ezf_id=1437377239070461302&dataid=".$___data["cca01"]['id'];
            $linkCCA02="https://cloudbackend.cascap.in.th/inputdata/redirect-page/?ezf_id=1437619524091524800&dataid=".$___data["cca02"]["id"];
            $linkCCA02p1="https://cloudbackend.cascap.in.th/inputdata/redirect-page/?ezf_id=1454041742064651700&dataid=".$___data["cca02.1"]['id'];
            $linkCCA03="https://cloudbackend.cascap.in.th/inputdata/redirect-page/?ezf_id=1451381257025574200&dataid=".$___data["cca03"]['id'];
            $linkCCA04="https://cloudbackend.cascap.in.th/inputdata/redirect-page/?ezf_id=1452061550097822200&dataid=".$___data["cca04"]['id'];
            $linkCCA05="https://cloudbackend.cascap.in.th/inputdata/redirect-page/?ezf_id=1440515302053265900&dataid=".$___data["cca05"]['id'];
         */
        $ezf_id='0';
        if( $crf=='cca02' ){
            $ezf_id = '1437619524091524800';
        }
        return $ezf_id;
    }


    # กำหนดเงื่อนไข เพื่อ List ข้อมูลออกมาจาก $_GET['stage']
    public static function getMainCondition($stage) {
	$maincondition = "0";
        switch ($stage){
            case 'cca02cfimg':
                $maincondition = "sys_usimageexist='1' ";
                $maincondition.= "and hsitecode=:hsitecode ";
                $maincondition.= "and (rstat='2' or rstat='4') ";
                $maincondition.= "and usmobile is null and (length(error)=0 and ICF='1') and rstat='2' ";
                $maincondition.= "and (length(zone_code)>0 and length(provincecode)>0 and length(amphurcode)>0 and length(hsitecode)>0 and length(username)>0 and length(f2doctorcode)>0) ";
                $maincondition.= "and f2v1 between '2015-10-01' and '2016-09-30' ";
                break;
            case 'cca02cfnoimg':
                $maincondition = "(sys_usimageexist='0' or sys_usimageexist is null or length(trim(sys_usimageexist))=0) ";
                $maincondition.= "and hsitecode=:hsitecode ";
                $maincondition.= "and (rstat='2' or rstat='4') ";
                $maincondition.= "and usmobile is null and (length(error)=0 and ICF='1') and rstat='2' ";
                $maincondition.= "and (length(zone_code)>0 and length(provincecode)>0 and length(amphurcode)>0 and length(hsitecode)>0 and length(username)>0 and length(f2doctorcode)>0) ";
                $maincondition.= "and f2v1 between '2015-10-01' and '2016-09-30' ";
                break;
        }
        return $maincondition;
    }
    
    # SQL for get list by maincondition
    public static function getSQL($condition) {
	$sql = "";
        $sql.= "select id,ptid,hsitecode,hptcode,f2v1 as txt,'f2v1' as var ";
        $sql.= ", '".$condition['ezf_id']."' as ezf_id ";
        $sql.= "from ".$condition['table']." ";
        $sql.= "where ".$condition['maincondition']." ";
        $sql.= "order by hsitecode, hptcode ";
        return $sql;
    }
    
    
    public static function getDataProvider($sql, $hsitecode)
    {
        if(strlen(trim($sql))>0 ){
//            echo "<br /><br /><br /><br />";
//            echo $sql;
            $dataProvider = Yii::$app->db->createCommand($sql,[":hsitecode"=>$hsitecode])->queryAll();
            return $dataProvider;
        }
    }
}
