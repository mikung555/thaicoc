<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace backend\modules\cpay\controllers;

use yii\web\Controller;
use Yii;

use backend\modules\cpay\classes\HospitalFunc;
use backend\modules\cpay\classes\QueryGetSummary;
use backend\modules\cpay\classes\LogUserAccess;
/**
 * Description of Y60Controller
 *
 * @author chaiwat
 */
class Y60Controller extends Controller {
    //put your code here
    
    public function actionIndex()
    {
        # Configuration of table
        $request = Yii::$app->request;
        $usersitecode=Yii::$app->user->identity->userProfile->sitecode;
        $userprofile['hospital'] = HospitalFunc::userOnHospital($usersitecode);
        
        // log view
        LogUserAccess::openY60();
        
        return $this->render('index'
                ,[
                    'request' => $request,
                    'userprofile' => $userprofile,
                    ]
                );
    }
    
    public function actionSummaryTable()
    {
        ini_set("memory_limit","512M");
        $request = Yii::$app->request;
        if( 0 ){
            echo "<pre align='left'>";
            print_r($_GET);
            echo "</pre>";
        }
        
        if( strlen($request->get('zone'))>0 && strlen($request->get('prov'))>0 ){
            $period = $request->get('period');
            $zone   = $request->get('zone');
            $prov   = $request->get('prov');
            $amp   = $request->get('xax');
            $hosp   = $request->get('hosp');
            $view   = $request->get('view');
            $data   = QueryGetSummary::getData($period, $view, $zone, $prov, $amp, $hosp);
            $viewg  = 'province';
        }
        # Configuration of table
        return $this->renderAjax('_summarytable'
                ,[
                    'data'      => $data, 
                    'viewg'    => $viewg,
                    ]
                );
    }
    
    public function actionShowDetail(){
        # Configuration of table
        $request = Yii::$app->request;
        
        if( 0 ){
            echo "<pre align='left'>";
            print_r($_GET);
            echo "</pre>";
        }
        if( strlen($request->get('hsitecode'))>0 ){
            $usersitecode=$request->get('hsitecode');
        }else{
            $usersitecode=Yii::$app->user->identity->userProfile->sitecode;
        }
        $userprofile['hospital'] = HospitalFunc::userOnHospital($usersitecode);
        $data   = QueryGetSummary::getDataByUsername($request->get('period'), $request->get('username'),$usersitecode);
        
        // log for view user
        LogUserAccess::openIndvReport($request->get('username'), $request->get('paytype'));
        
        if( $request->get('paytype')=='keyin' ){
            return $this->renderAjax('_tableShowKeyin'
                    ,[
                        'request'       => $request,
                        'userprofile'   => $userprofile,
                        'data'          => $data,
                        ]
                    );
        }else{
            return $this->renderAjax('_tableShowDetail'
                    ,[
                        'request'       => $request,
                        'userprofile'   => $userprofile,
                        'data'          => $data,
                        ]
                    );
        }
    }
    
    
}
