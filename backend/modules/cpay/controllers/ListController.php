<?php

namespace backend\modules\cpay\controllers;

use yii\web\Controller;
use Yii;
use backend\modules\cpay\classes\ListQuery;
use backend\modules\cpay\classes\HospitalFunc;


class ListController extends Controller
{
    public function actionIndex()
    {
        # Configuration of table
        $request = Yii::$app->request;
//        echo $request->get('tperiod');
//        echo $request->get('hsitecode');
//        echo $request->get('stage');
        //$listQuery = new ListQuery();
        $urlget = HospitalFunc::userOnHospital($request->get('hsitecode'));
        
        $configlist['table'] = ListQuery::getTable($request->get('tperiod'), $request->get('crf'));
        $configlist['ezf_id'] = ListQuery::getEzForm($request->get('crf'));
        $configlist['maincondition'] = ListQuery::getMainCondition($request->get('stage'));
        $sql['sql'] = ListQuery::getSQL($configlist);
        $provider = ListQuery::getDataProvider($sql['sql'], $request->get('hsitecode'));
        
        return $this->render('index',[
                'configlist' => $configlist,
                'sql' => $sql,
                'provider' => $provider,
                'urlget' => $urlget,
            ]);
    }
    
    
    
    // sub function 
    public function userDetail($userid){
        //$userid=1;
        $sql = "select * from user where id=\"".addslashes($userid)."\" ";
        $result =  Yii::$app->db->createCommand($sql)->queryAll();
        return $result;
    }
    
    public function userDetailFull($userid){
        
        //$userid=1;
        $sql = "select * from user_profile where user_id=\"".addslashes($userid)."\" ";
        $result =  Yii::$app->db->createCommand($sql)->queryAll();
        return $result;
        
    }
}
