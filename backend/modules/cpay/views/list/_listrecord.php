<?php

use yii\helpers\Html;
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
    <div class="table-responsive">
        <div id="report84-dynagrid-2-pjax">
            <div class="grid">
                <div class="row">
                    <div class="col-lg-12" style="text-align: right;">
                        <label for="pageselect" style="margin-top: 7px; width: 400px;">จำนวนข้อมูลทั้งหมด <?php echo count($dataProvider); ?> records </label>

                    </div>
                </div>
            </div>
            <div id="report84-dynagrid-2" data-krajee-grid="kvGridInit_adf683c2">
                <div id="report84-dynagrid-2-container" class="table-responsive kv-grid-container">


                    <table class="kv-grid-table table table-bordered table-striped">
                        <thead>
                        <tr style="text-align:center">
                            <th rowspan="1" colspan="1" class="kv-align-center kv-align-middle" style="min-width:60px;text-align:right;">ลำดับที่</th>
                            <th rowspan="1" class="kv-align-middle" style="min-width: 60px;text-align:left;">
                                Site ID
                            </th>
                            <th rowspan="1" class="kv-align-middle" style="min-width: 60px;text-align:left;">
                                PID
                            </th>
                            <th rowspan="1" colspan="1" class="kv-align-center kv-align-middle" style="min-width:90px;text-align:right;">
                                <a href="#table_secshow1" id='sort1' data-sort='asc'
                                    fromDate='<?php echo $datadate[fromDate] ?>'
                                    toDate='<?php echo $datadate[toDate] ?>' sec='1' colum='f2v1' 
                                    page='<?php echo $page; ?>'>วันที่ตรวจ</a>
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        <!-- TABLE DATA -->

                        <?php
                        $num = (($page-1)*$limitrec)+1;
                        if( count($dataProvider)>0 )foreach ($dataProvider as $key) {
                            ?>
                            <tr style="text-align:right">
                                <td class="kv-align-center kv-align-middle"
                                    style="text-align:right;"><?php echo $num ?>
                                </td>
                                <td style="text-align:left;"><?php echo $key['hsitecode'] ?></td>
                                <td style="text-align:left;"><?php echo $key['hptcode'] ?></td>
                                <!--td style="text-align:left;"><img src="https://tools.cascap.in.th/usimage.php?ptid=<?php echo $key['ptid'] ?>" height="30" style="vertical-align: text-top;"></td-->
                                <td class="kv-align-center kv-align-middle"
                                    style="text-align:right;">
                                    <?php
                                    $urlToCca02 = "/inputdata/redirect-page?dataid=".$key['id']."&ezf_id=".$key['ezf_id'];
                                    echo Html::a( $key['txt'], $urlToCca02,['target'=>'_blank']);
                                    ?>
                                </td>
                            </tr>
                            <?php
                            $num++;
                        }
                        ?>
                        
                    </table>
                </div>
            </div>
        </div>
    </div>

