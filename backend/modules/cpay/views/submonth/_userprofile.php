<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<table width="100%" align="center" border="0" cellpadding="2" cellspacing="0">
    <tbody><tr>
        <td colspan="2"></td>
        <td colspan="7">
            <h4>ข้อมูลส่วนตัว</h4>
        </td>
    </tr>
    <tr>
        <td colspan="3" align="right">
            <h5>หน่วยบริการ:</h5>
        </td>
        <td></td>
        <td colspan="5" align="left">
            <span class="text-primary">
                24821: 
                โรงพยาบาลนาเยีย            </span>
        </td>
    </tr>
    <tr>
        <td colspan="3" align="right">
            <h5>ประเภท:</h5>
        </td>
        <td></td>
        <td colspan="5" align="left">
            <span class="text-primary">
                โรงพยาบาลชุมชน (7)             </span>
        </td>
    </tr>
    
    <tr>
        <td colspan="3" align="right">
            <h5>เข้าสู่ระบบโดย User:</h5>
        </td>
        <td></td>
        <td colspan="5" align="left">
            <span class="text-primary">
                <?php
                    echo Yii::$app->user->identity->username;
                ?>
            </span>
        </td>
    </tr>
    <tr>
        <td colspan="3" align="right">
            <h5>ชื่อ-สกุล:</h5>
        </td>
        <td></td>
        <td colspan="5" align="left">
            <span class="text-primary">
                <?php
                    echo Yii::$app->user->identity->userProfile->firstname;
                    echo " ";
                    echo Yii::$app->user->identity->userProfile->lastname;
                ?>
            </span>
        </td>
    </tr>
    <tr>
        <td width="8%"></td>
        <td width="2%"></td>
        <td width="10%"></td>
        <td width="1%"></td>
        <td width="1%"></td>
        <td width="1%"></td>
        <td width="1%"></td>
        <td width="1%"></td>
        <td width="75%"></td>
    </tr>
</tbody></table>
<?php

?>

