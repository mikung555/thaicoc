<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


use backend\modules\cpay\classes\QueryGetSummary;


if( 0 ){
    echo "<pre align='left'>";
    print_r($data);
    echo "</pre>";
}

$request = Yii::$app->request;

//echo $request->get('period');

if( $request->get('period') == '20170625' ){
    //
    $table_title    = "ค่าตอบแทนในการลงข้อมูล ชุดละ 20 บาท (เดือน มิถุนายน ปีงบประมาณ 2560 ตัดยอดเมื่อวันที่ 25 มิถุนายน 2560)";
}else{
    $table_title    = "ค่าตอบแทนในการลงข้อมูล ชุดละ 20 บาท (เดือน มีนาคม ปีงบประมาณ 2560)";
}

$key_paypercase = 20;


?>
<div class="modal-header">
    <button type="button" class="close"  data-dismiss="modal" >
        <span aria-hidden="true">&times;</span>
    </button>
    <h4 class="modal-title" id="label-list"></h4>
</div>
<div class="modal-body" id="body-list">
<div class="table-responsive" style="padding:50px 50px; background-color: #FFFFFF;">
    <br />
    <div class="row" style="margin: 0">
        <div class="panel panel-primary" style="width: 100%;">
        <div class="panel-heading"><h3><b>ค่าตอบแทนในการลงข้อมูล</b></h3></div>
        <div class="panel-body">
            <div id="datalist" style="align-content: flex-start;">
            <?php
            //print_r($request);
            echo $this->render('_profileHeader',
                [
                    'userprofile'   => $userprofile,
                    'data'          => $data,
                ]);
            ?>   
            </div>
            <br />
            <div class="row" style="margin-left: 20px;width: 1200px;">
                <h3>
                    <i class="glyphicon glyphicon-list-alt" style="color: green;"></i>
                    <?php echo $table_title; ?>
                </h3>
            </div>
        </div>
        <div>
            <div class="row" style="width: 1200px;">
                <table width="90%" align="center" border="1" bordercolor="#000000" cellspacing="2" cellpadding="2" style="border-collapse: collapse;">
                    <tbody>
                        <tr>
                            <td rowspan="2" align="center" valign="top">
                                <span class="lead">ชื่อผู้ลงข้อมูล</span>
                            </td>
                            <td colspan="2" align="center" valign="top">
                                <span class="">จำนวนข้อมูล</span>
                            </td>
                            <td rowspan="2" align="center" valign="top">
                                <span class="">ข้อมูลไม่สมบูรณ์ (ไม่เข้าตรวจ)</span>
                            </td>
                            <td colspan="4" align="center" valign="top">
                                <span class="">ข้อมูลสมบูรณ์ผ่านการตรวจ</span>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" valign="top">
                                <span class="">จากการสัญจร</span>
                            </td>
                            <td align="center" valign="top">
                                <span class="">จากการคีย์</span>
                            </td>
                            <td align="center" valign="top">
                                <span class="" alt="ข้อมูลที่ผ่านการ Submit เข้ามาเท่านั้น" title="ข้อมูลที่ผ่านการ Submit เข้ามาเท่านั้น">ผ่าน</span>
                            </td>
                            <td align="center" valign="top">
                                <span class="">ค่าตอบแทน</span>
                            </td>
                            <td align="center" valign="top">
                                <span class="" alt="เหลือเพียงขั้นตอนการกด Submit จากผู้ลงข้อมูล" title="เหลือเพียงขั้นตอนการกด Submit จากผู้ลงข้อมูล">save draft</span>
                            </td>
                            <td align="center" valign="top">
                                <span class="">ไม่ผ่าน</span>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="1" align="left" valign="top">
                                <span class="">&nbsp;แสดงข้อมูลทั้งหมดของ เมืองน่าน&nbsp;</span>
                            </td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td align="left" valign="top">
                                <span class="">
                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                    Register+ICF+CCA-01&nbsp;
                                </span>
                            </td>
                            <td align="right" valign="top">
                                <span class="">
                                    <?php echo number_format($data['cca01_usmobile'],0,'.',','); ?>
                                    &nbsp;
                                </span>
                            </td>
                            <td align="right" valign="top">
                                <span class="">
                                    <?php echo number_format($data['cca01_keyin'],0,'.',','); ?>
                                    &nbsp;
                                </span>
                            </td>
                            <td align="right" valign="top">
                                <span class="">
                                    <?php echo number_format($data['cca01_notconfirm'],0,'.',','); ?>
                                    &nbsp;
                                </span>
                            </td>
                            <td align="right" valign="top">
                                <span class="">
                                    <?php echo number_format($data['cca01_confirm'],0,'.',','); ?>
                                    &nbsp;
                                </span>
                            </td>
                            <td align="right" valign="top">
                                <span class="">
                                    <?php 
                                        $data['cca01_confirm_pay']  = QueryGetSummary::getCCA01KeyPay($data['cca01_confirm']);
                                        echo number_format($data['cca01_confirm_pay'],0,'.',',');
                                    ?>
                                    &nbsp;
                                </span>
                            </td>
                            <td align="right" valign="top">
                                <span class="">
                                    <?php echo number_format($data['cca01_savedraftnotconfirm'],0,'.',','); ?>
                                    &nbsp;
                                </span>
                            </td>
                            <td align="right" valign="top">
                                <span class="">
                                    <?php echo number_format($data['cca01_submitnotconfirm'],0,'.',','); ?>
                                    &nbsp;
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" valign="top">
                                <span class="">
                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                    CCA-02&nbsp;
                                </span>
                            </td>
                            <td align="right" valign="top">
                                <span class="">
                                    <?php echo number_format($data['cca02_usmobile'],0,'.',','); ?>
                                    &nbsp;
                                </span>
                            </td>
                            <td align="right" valign="top">
                                <span class="">
                                    <?php echo number_format($data['cca02_keyin'],0,'.',','); ?>
                                    &nbsp;
                                </span>
                            </td>
                            <td align="right" valign="top">
                                <span class="">
                                    <?php echo number_format($data['cca02_notcomplete'],0,'.',','); ?>
                                    &nbsp;
                                </span>
                            </td>
                            <td align="right" valign="top">
                                <span class="">
                                    <?php echo number_format($data['cca02_confirm'],0,'.',','); ?>
                                    &nbsp;
                                </span>
                            </td>
                            <td align="right" valign="top">
                                <span class="">
                                    <?php 
                                        $data['cca02_confirm_pay']  = QueryGetSummary::getCCA02KeyPay($data['cca02_confirm']);
                                        echo number_format($data['cca02_confirm_pay'],0,'.',',');
                                    ?>
                                    &nbsp;
                                </span>
                            </td>
                            <td align="right" valign="top">
                                <span class="">
                                    <?php echo number_format($data['cca02_savedraft'],0,'.',','); ?>
                                    &nbsp;
                                </span>
                            </td>
                            <td align="right" valign="top">
                                <span class="">
                                    <?php echo number_format($data['cca02_submitnotconfirm'],0,'.',','); ?>
                                    &nbsp;
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" valign="top">
                                <span class="">
                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                    CCA-02.1&nbsp;
                                </span>
                            </td>
                            <td></td>
                            <td align="right" valign="top">
                                <span class="">
                                    <?php echo number_format($data['cca02p1_keyin'],0,'.',','); ?>
                                    &nbsp;
                                </span>
                            </td>
                            <td align="right" valign="top">
                                <span class="">
                                    <?php echo number_format($data['cca02p1_notcomplete'],0,'.',','); ?>
                                    &nbsp;
                                </span>
                            </td>
                            <td align="right" valign="top">
                                <span class="">
                                    <?php echo number_format($data['cca02p1_confirm'],0,'.',','); ?>
                                    &nbsp;
                                </span>
                            </td>
                            <td align="right" valign="top">
                                <span class="">
                                    <?php 
                                        $data['cca02p1_confirm_pay']  = QueryGetSummary::getCCA02p1KeyPay($data['cca02p1_confirm']);
                                        echo number_format($data['cca02p1_confirm_pay'],0,'.',',');
                                    ?>
                                    &nbsp;
                                </span>
                            </td>
                            <td align="right" valign="top">
                                <span class="">
                                    <?php echo number_format($data['cca02p1_savedraftnotconfirm'],0,'.',','); ?>
                                    &nbsp;
                                </span>
                            </td>
                            <td align="right" valign="top">
                                <span class="">
                                    <?php echo number_format($data['cca02p1_submitnotconfirm'],0,'.',','); ?>
                                    &nbsp;
                                </span>
                            </td>
                        </tr>

                        <tr>
                            <td align="left" valign="top">
                                <span class="">
                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                    CCA-03&nbsp;
                                </span>
                            </td>
                            <td></td>
                            <td align="right" valign="top">
                                <span class="">
                                    <?php echo number_format($data['cca03_keyin'],0,'.',','); ?>
                                    &nbsp;
                                </span>
                            </td>
                            <td align="right" valign="top">
                                <span class="">
                                    <?php echo number_format($data['cca03_notcomplete'],0,'.',','); ?>
                                    &nbsp;
                                </span>
                            </td>
                            <td align="right" valign="top">
                                <span class="">
                                    <?php echo number_format($data['cca03_confirm'],0,'.',','); ?>
                                    &nbsp;
                                </span>
                            </td>
                            <td align="right" valign="top">
                                <span class="">
                                    <?php 
                                        $data['cca03_confirm_pay']  = QueryGetSummary::getCCA03KeyPay($data['cca03_confirm']);
                                        echo number_format($data['cca03_confirm_pay'],0,'.',',');
                                    ?>
                                    &nbsp;
                                </span>
                            </td>
                            <td align="right" valign="top">
                                <span class="">
                                    <?php echo number_format($data['cca03_savedraftnotconfirm'],0,'.',','); ?>
                                    &nbsp;
                                </span>
                            </td>
                            <td align="right" valign="top">
                                <span class="">
                                    <?php echo number_format($data['cca03_submitnotconfirm'],0,'.',','); ?>
                                    &nbsp;
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" valign="top">
                                <span class="">
                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                    CCA-04&nbsp;
                                </span>
                            </td>
                            <td></td>
                            <td align="right" valign="top">
                                <span class="">
                                    <?php echo number_format($data['cca04_keyin'],0,'.',','); ?>
                                    &nbsp;
                                </span>
                            </td>
                            <td align="right" valign="top">
                                <span class="">
                                    <?php echo number_format($data['cca04_notcomplete'],0,'.',','); ?>
                                    &nbsp;
                                </span>
                            </td>
                            <td align="right" valign="top">
                                <span class="">
                                    <?php echo number_format($data['cca04_confirm'],0,'.',','); ?>
                                    &nbsp;
                                </span>
                            </td>
                            <td align="right" valign="top">
                                <span class="">
                                    <?php 
                                        $data['cca04_confirm_pay']  = QueryGetSummary::getCCA04KeyPay($data['cca04_confirm']);
                                        echo number_format($data['cca04_confirm_pay'],0,'.',',');
                                    ?>
                                    &nbsp;
                                </span>
                            </td>
                            <td align="right" valign="top">
                                <span class="">
                                    <?php echo number_format($data['cca04_savedraftnotconfirm'],0,'.',','); ?>
                                    &nbsp;
                                </span>
                            </td>
                            <td align="right" valign="top">
                                <span class="">
                                    <?php echo number_format($data['cca04_submitnotconfirm'],0,'.',','); ?>
                                    &nbsp;
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" valign="top">
                                <span class="">
                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                    CCA-05&nbsp;
                                </span>
                            </td>
                            <td></td>
                            <td align="right" valign="top">
                                <span class="">
                                    <?php echo number_format($data['cca05_keyin'],0,'.',','); ?>
                                    &nbsp;
                                </span>
                            </td>
                            <td align="right" valign="top">
                                <span class="">
                                    <?php echo number_format($data['cca05_notcomplete'],0,'.',','); ?>
                                    &nbsp;
                                </span>
                            </td>
                            <td align="right" valign="top">
                                <span class="">
                                    <?php echo number_format($data['cca05_confirm'],0,'.',','); ?>
                                    &nbsp;
                                </span>
                            </td>
                            <td align="right" valign="top">
                                <span class="">
                                    <?php 
                                        $data['cca05_confirm_pay']  = QueryGetSummary::getCCA03KeyPay($data['cca05_confirm']);
                                        echo number_format($data['cca05_confirm_pay'],0,'.',',');
                                    ?>
                                    &nbsp;
                                </span>
                            </td>
                            <td align="right" valign="top">
                                <span class="">
                                    <?php echo number_format($data['cca05_savedraftnotconfirm'],0,'.',','); ?>
                                    &nbsp;
                                </span>
                            </td>
                            <td align="right" valign="top">
                                <span class="">
                                    <?php echo number_format($data['cca05_submitnotconfirm'],0,'.',','); ?>
                                    &nbsp;
                                </span>
                            </td>
                        </tr>

                        <tr>
                            <td width="25%" align="left">
                                <span class="">รวม</span>
                            </td>
                            <td width="10%" align="left">
                                <span class=""></span>
                            </td>
                            <td width="10%" align="left">
                                <span class=""></span>
                            </td>

                            <td width="15%" align="left">
                                <span class=""></span>
                            </td>
                            <td width="10%" align="left">
                                <span class=""></span>
                            </td>
                            <td width="10%" align="right" valign="top">
                                <span class="">
                                    <?php 
                                        $summary_confirm_pay = $data['cca01_confirm_pay']
                                                + $data['cca02_confirm_pay']
                                                + $data['cca02p1_confirm_pay']
                                                + $data['cca03_confirm_pay']
                                                + $data['cca04_confirm_pay']
                                                + $data['cca05_confirm_pay'];
                                        echo number_format($summary_confirm_pay,0,'.',',');
                                    ?>
                                    &nbsp;
                                </span>
                            </td>
                            <td width="10%" align="left">
                                <span class=""></span>
                            </td>
                            <td width="10%" align="left">
                                <span class=""></span>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <p></p>
                <p></p>
            </div>
        </div>
    </div>
</div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal"  >Close</button>
</div>

