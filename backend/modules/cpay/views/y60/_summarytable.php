<?php
use yii\helpers\Html;

use backend\modules\cpay\classes\UserCanView;
use backend\modules\cpay\classes\HospitalFunc;
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//echo "<br />";


$request    = Yii::$app->request;

$userid     = Yii::$app->user->identity->id;
$usersitecode=Yii::$app->user->identity->userProfile->sitecode;
$myuserprofile['hospital'] = HospitalFunc::userOnHospital($usersitecode);

if( 0 ){
    echo "<pre align='left'>";
    print_r($myuserprofile);
    echo "</pre>";
}

$priv = UserCanView::getPrivilege($userid,$myuserprofile);

if( $viewg == 'province' ){
    $table_title    = "จังหวัด ".$data['province'][$request->get('prov')];
}




$js = <<< JS
$(document).on("click", "*[id^=openUser]", function() {
    var url             = $(this).attr('data-url');
    var period          = $(this).attr('period');
    var paytype         = $(this).attr('paytype');
    var username        = $(this).attr('username');
    var hsitecode       = $(this).attr('hsitecode');
    url = url+'?paytype='+paytype;
    url = url+'&period='+period;
    url = url+'&username='+username;
    url = url+'&hsitecode='+hsitecode;
    //alert(url);
    modalShowPopup(url);
});
function modalShowPopup(url) {
    $('#modal-popupreg .modal-dialog').attr('style','width:90%');
    $('#modal-popupreg .modal-content').html('<div class="sdloader "><i class="sdloader-icon"></i></div>');
    $('#modal-popupreg').modal('show')
    .find('.modal-content')
    .load(url);
}
JS;

        
        
// register your javascript
$this->registerJs($js, \yii\web\View::POS_END);


$scriptCss = <<< CSS

    // CSS code here 
    .table-box thead > tr > th {
        border-bottom: 1px solid #d8e6ec;
    }

    .table-box {
        border-collapse: separate;
        border: 1px solid #b7c7cf;
    }
CSS;
$this->registerCss($scriptCss);


// rader selection for admin
echo $this->render('_linkSelectByCenter',
    [
        'userprofile'=>$myuserprofile,
    ]);





if( 0 ){
    echo "<pre align='left'>";
    //print_r($data);
    print_r($priv);
    echo "</pre>";
}

if( 0 ){
    echo $request->get('period');
}
?>
<div class="table-responsive">
    <div style="margin-left: 30px; margin-right: 50px;">
        <div class="row" style="text-align: left;">
            <div>
                <div style="text-align: left;">
                    <h3><?php //echo $table_title; ?></h3>
                </div>
            </div>
        </div>
        <div class="row" style="text-align: left; background-color: white;">
            <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <td colspan="16" height="0"></td>
                    </tr>
                </thead>
                
                <?php if( 0 ) { ?>
                <thead>
                    <tr> 
                        <th rowspan="2" style="text-align: left; min-width: 400px;">รายละเอียด</th>
                        <th colspan="4" style="text-align: center;">ให้บริการ (ตรวจ/รักษา)</th>
                        <th colspan="6" style="text-align: center;">นำเข้าข้อมูล</th>
                        <th colspan="2" style="text-align: center;">ค่าตอบแทน</th>
                    </tr>
                    <tr> 
                        <th style="text-align: right; min-width: 70px;">CCA-02</th>
                        <th style="text-align: right; min-width: 80px;">CCA-02.1</th>
                        <th style="text-align: right; min-width: 70px;">CCA-03</th>
                        <th style="text-align: right; min-width: 70px;">CCA-04</th>
                        
                        <th style="text-align: right; min-width: 70px;">CCA-01</th>
                        <th style="text-align: right; min-width: 70px;">CCA-02</th>
                        <th style="text-align: right; min-width: 80px;">CCA-02.1</th>
                        <th style="text-align: right; min-width: 70px;">CCA-03</th>
                        <th style="text-align: right; min-width: 70px;">CCA-04</th>
                        <th style="text-align: right; min-width: 70px;">CCA-05</th>
                        
                        <th style="text-align: right; min-width: 140px;">เจ้าหน้าที่ CASCAP</th>
                        <th style="text-align: right; min-width: 120px;">เจ้าหน้าที่ รพ.</th>
                    </tr>
                </thead>
                <?php } ?>
<?php
if( count($data)>0 ){
    // รายเขต
    $spancolall = 15;
    // สามารถดูยอดได้
    if( $priv[1] ){
        $spancolall = $spancolall - 0;
    }else{
        $spancolall = $spancolall - 2;
    }
    foreach( $data['record'] as $kzone => $vzone ){
        
        $zone_cascap_pay                           = 0;
        $zone_user_pay                             = 0;
        $zone_cca02_service_image                  = 0;
        $zone_cca02_service_noimage                = 0;
        $zone_cca02p1_service                      = 0;
        $zone_cca03_confirm_service_first          = 0;
        $zone_cca03_confirm_service_followup       = 0;
        $zone_cca04_confirm_service_biopsy         = 0;
        $zone_cca04_confirm_service_liverresection = 0;
        $zone_cca01_confirm                        = 0;
        $zone_cca02_confirm                        = 0;
        $zone_cca02p1_confirm                      = 0;
        $zone_cca03_confirm                        = 0;
        $zone_cca04_confirm                        = 0;
        $zone_cca05_confirm                        = 0;
?>
                <tr>
                    <td colspan="<?php echo $spancolall+1; ?>"><h3><b>เขต <?php echo $kzone; ?></b></h3></td>
                </tr>
<?php
            // รายจังหวัด
            foreach( $vzone as $kprov => $vprov ){
                // Clear ค่าเป็น 0
                $prov_cascap_pay                           = 0;
                $prov_user_pay                             = 0;
                $prov_cca02_service_image                  = 0;
                $prov_cca02_service_noimage                = 0;
                $prov_cca02p1_service                      = 0;
                $prov_cca03_confirm_service_first          = 0;
                $prov_cca03_confirm_service_followup       = 0;
                $prov_cca04_confirm_service_biopsy         = 0;
                $prov_cca04_confirm_service_liverresection = 0;
                $prov_cca01_confirm                        = 0;
                $prov_cca02_confirm                        = 0;
                $prov_cca02p1_confirm                      = 0;
                $prov_cca03_confirm                        = 0;
                $prov_cca04_confirm                        = 0;
                $prov_cca05_confirm                        = 0;
?>
                <tr>
                    <td colspan="<?php echo $spancolall+1; ?>">
                        <h3><b>&nbsp;&nbsp;จังหวัด <?php echo $data['province'][$kprov]; ?></b></h3>
                    </td>
                </tr>
<?php
                    // รายอำเภอ
                    foreach( $vprov as $kamp => $vamp ){
                        
                        if( $priv[7] || $kamp == $myuserprofile['hospital'][0]['amphurcode'] ){
                            //  $priv[7] มีสิทธิ์ดู สรุปข้อมูล อำเภอ อื่น
                            //  $priv[6] เห็น ทุก รพ. ในอำเภอ (สสอ.)
                            if( ($request->get('view')=='summaryprov' || $request->get('view')=='summaryamp') && !$priv[3] ){
                                // แสดง Column สำหรับ อำเภอ begin
?>
                <thead>
                    <tr> 
                        <th rowspan="3" style="text-align: left; min-width: 400px;">
                            <h3>
                                <b>&nbsp;&nbsp;&nbsp;&nbsp;
                                    <?php echo "อำเภอ ".$data['amphur'][$kprov][$kamp]; ?>
                                </b>
                            </h3>
                        </th>
                        <th colspan="7" style="text-align: center;">ให้บริการ (ตรวจ/รักษา)</th>
                        <th colspan="6" style="text-align: center;">นำเข้าข้อมูล</th>
                        <?php
                        // สามารถดูยอดได้
                        if( $priv[1] ){
                        ?>
                        <th colspan="2" style="text-align: center;">ค่าตอบแทน</th>
                        <?php
                        }
                        // สามารถดูยอดได้
                        ?>
                    </tr>
                    <tr> 
                        <th colspan="2" style="text-align: center; min-width: 125px;">CCA-02</th>
                        <th rowspan="2" style="text-align: right; min-width: 85px;">CCA-02.1</th>
                        <th colspan="2" style="text-align: center; min-width: 125px;">CCA-03</th>
                        <th colspan="2" style="text-align: center; min-width: 155px;">CCA-04</th>
                        
                        <th rowspan="2" style="text-align: right; min-width: 70px;">CCA-01</th>
                        <th rowspan="2" style="text-align: right; min-width: 70px;">CCA-02</th>
                        <th rowspan="2" style="text-align: right; min-width: 85px;">CCA-02.1</th>
                        <th rowspan="2" style="text-align: right; min-width: 70px;">CCA-03</th>
                        <th rowspan="2" style="text-align: right; min-width: 70px;">CCA-04</th>
                        <th rowspan="2" style="text-align: right; min-width: 70px;">CCA-05</th>
                        <?php
                        // สามารถดูยอดได้
                        if( $priv[1] ){
                        ?>
                        <th rowspan="2" style="text-align: right; min-width: 135px;">เจ้าหน้าที่ CASCAP</th>
                        <th rowspan="2" style="text-align: right; min-width: 100px;">เจ้าหน้าที่ รพ.</th>
                        <?php
                        }
                        // สามารถดูยอดได้
                        ?>
                    </tr>
                    <tr>
                        <th style="text-align: right; min-width: 55px;">มีภาพ</th>
                        <th style="text-align: right; min-width: 70px;">ไม่มีภาพ</th>
                        <th style="text-align: right; min-width: 65px;">ครั้งแรก</th>
                        <th style="text-align: right; min-width: 60px;">ติดตาม</th>
                        <th style="text-align: right; min-width: 65px;">Biopsy</th>
                        <th style="text-align: right; min-width: 90px;">Resection</th>
                    </tr>
                </thead>
                    
<?php                               
                                // แสดง Column สำหรับ อำเภอ end 
                            }else{
?>
                <tr>
                    <td colspan="<?php echo $spancolall+1; ?>">
                        <h4>&nbsp;&nbsp;&nbsp;&nbsp;
                        <?php echo "อำเภอ ".$data['amphur'][$kprov][$kamp]; ?></h4>
                    </td>
                </tr>
<?php
                            }
                        }
                        
                        if( 1 ){
                            // การประมวลผล
                            
                            $amp_cascap_pay                           = 0;
                            $amp_user_pay                             = 0;
                            $amp_cca02_service_image                  = 0;
                            $amp_cca02_service_noimage                = 0;
                            $amp_cca02p1_service                      = 0;
                            $amp_cca03_confirm_service_first          = 0;
                            $amp_cca03_confirm_service_followup       = 0;
                            $amp_cca04_confirm_service_biopsy         = 0;
                            $amp_cca04_confirm_service_liverresection = 0;
                            $amp_cca01_confirm                        = 0;
                            $amp_cca02_confirm                        = 0;
                            $amp_cca02p1_confirm                      = 0;
                            $amp_cca03_confirm                        = 0;
                            $amp_cca04_confirm                        = 0;
                            $amp_cca05_confirm                        = 0;
                            
                            // รายโรงพยาบาล
                            foreach( $vamp as $khosp => $vhosp ){
                                //echo $khosp;
                                
                                
                                
                                if( $priv[5] || $khosp==$usersitecode ){
                                        //  $priv[5] มีสิทธิ์ดู รพ. อื่น
                                    if( ($request->get('view')=='summaryprov' || $request->get('view')=='summaryamp') && !$priv[3] ){
                                        // ไม่แสดง Column
                                    }else{
                                        // แสดง Column begin
?>
                <tr>
                    <td style=" font-size: 14px;" colspan="<?php echo $spancolall+1; ?>">
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <i class="fa fa-hospital-o" style="font-size:25px;color:red"></i>
                        <?php echo "".$khosp.": "; ?>
                        <?php echo "".$data['hospital'][$kprov][$kamp][$khosp]; ?>
                    </td>
                </tr>
                <thead>
                    <tr> 
                        <th rowspan="3" style="text-align: left; min-width: 400px;">
                            <b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    แยกตามรายชื่อภายในโรงพยาบาล</b>
                        </th>
                        <th colspan="7" style="text-align: center;">ให้บริการ (ตรวจ/รักษา)</th>
                        <th colspan="6" style="text-align: center;">นำเข้าข้อมูล</th>
                        <?php
                        // สามารถดูยอดได้
                        if( $priv[1] ){
                        ?>
                        <th colspan="2" style="text-align: center;">ค่าตอบแทน</th>
                        <?php
                        }
                        // สามารถดูยอดได้
                        ?>
                    </tr>
                    <tr> 
                        <th colspan="2" style="text-align: center; min-width: 125px;">CCA-02</th>
                        <th rowspan="2" style="text-align: right; min-width: 85px;">CCA-02.1</th>
                        <th colspan="2" style="text-align: center; min-width: 125px;">CCA-03</th>
                        <th colspan="2" style="text-align: center; min-width: 155px;">CCA-04</th>
                        
                        <th rowspan="2" style="text-align: right; min-width: 70px;">CCA-01</th>
                        <th rowspan="2" style="text-align: right; min-width: 70px;">CCA-02</th>
                        <th rowspan="2" style="text-align: right; min-width: 85px;">CCA-02.1</th>
                        <th rowspan="2" style="text-align: right; min-width: 70px;">CCA-03</th>
                        <th rowspan="2" style="text-align: right; min-width: 70px;">CCA-04</th>
                        <th rowspan="2" style="text-align: right; min-width: 70px;">CCA-05</th>
                        <?php
                        // สามารถดูยอดได้
                        if( $priv[1] ){
                        ?>
                        <th rowspan="2" style="text-align: right; min-width: 135px;">เจ้าหน้าที่ CASCAP</th>
                        <th rowspan="2" style="text-align: right; min-width: 100px;">เจ้าหน้าที่ รพ.</th>
                        <?php
                        }
                        // สามารถดูยอดได้
                        ?>
                    </tr>
                    <tr>
                        <th style="text-align: right; min-width: 55px;">มีภาพ</th>
                        <th style="text-align: right; min-width: 70px;">ไม่มีภาพ</th>
                        <th style="text-align: right; min-width: 65px;">ครั้งแรก</th>
                        <th style="text-align: right; min-width: 60px;">ติดตาม</th>
                        <th style="text-align: right; min-width: 65px;">Biopsy</th>
                        <th style="text-align: right; min-width: 90px;">Resection</th>
                    </tr>
                </thead>
                    
<?php
                                    }
                                }
                                
                                if( 1 ){
                                // แสดง Column end
                                
                                    // รายโรงพยาบาล
                                    $h_cascap_pay                           = 0;
                                    $h_user_pay                             = 0;
                                    $h_cca02_service_image                  = 0;
                                    $h_cca02_service_noimage                = 0;
                                    $h_cca02p1_service                      = 0;
                                    $h_cca03_confirm_service_first          = 0;
                                    $h_cca03_confirm_service_followup       = 0;
                                    $h_cca04_confirm_service_biopsy         = 0;
                                    $h_cca04_confirm_service_liverresection = 0;
                                    $h_cca01_confirm                        = 0;
                                    $h_cca02_confirm                        = 0;
                                    $h_cca02p1_confirm                      = 0;
                                    $h_cca03_confirm                        = 0;
                                    $h_cca04_confirm                        = 0;
                                    $h_cca05_confirm                        = 0;
                                    foreach( $vhosp as $kuser => $vuser ){
                                        $cascap_pay   = $cascap_pay + $vuser['s_pay'];          // เจ้าหน้าที่ CASCAP
                                        $user_pay     = $user_pay + $vuser['u_pay'];            // เจ้าหน้าที่ของ รพ.
                                        
                                        $h_cca02_service_image      = $h_cca02_service_image + $vuser['cca02_service_image'];
                                        $h_cca02_service_noimage    = $h_cca02_service_noimage + $vuser['cca02_service_noimage'];
                                        $h_cca02p1_service          = $h_cca02p1_service + $vuser['cca02p1_service'];
                                        $h_cca03_confirm_service_first          = $h_cca03_confirm_service_first + $vuser['cca03_confirm_service_first'];
                                        $h_cca03_confirm_service_followup       = $h_cca03_confirm_service_followup + $vuser['cca03_confirm_service_followup'];
                                        
                                        if( $vuser['cca04_service']>0 ){
                                            //
                                        }else{
                                            $vuser['cca04_confirm_service_biopsy'] = 0;
                                            $vuser['cca04_confirm_service_liverresection'] = 0;
                                        }
                                        $h_cca04_confirm_service_biopsy         = $h_cca04_confirm_service_biopsy + $vuser['cca04_confirm_service_biopsy'];
                                        $h_cca04_confirm_service_liverresection = $h_cca04_confirm_service_liverresection + $vuser['cca04_confirm_service_liverresection'];
                                        
                                        $h_cca01_confirm            = $h_cca01_confirm + $vuser['cca01_confirm'];
                                        $h_cca02_confirm            = $h_cca02_confirm + $vuser['cca02_confirm'];
                                        $h_cca02p1_confirm          = $h_cca02p1_confirm + $vuser['cca02p1_confirm'];
                                        $h_cca03_confirm            = $h_cca03_confirm + $vuser['cca03_confirm'];
                                        $h_cca04_confirm            = $h_cca04_confirm + $vuser['cca04_confirm'];
                                        $h_cca05_confirm            = $h_cca05_confirm + $vuser['cca05_confirm'];
                                        $h_cascap_pay = $h_cascap_pay + $vuser['s_pay'];
                                        $h_user_pay   = $h_user_pay + $vuser['u_pay'];
                                        
                                        
                                        // แสดงเป็นรายคน
                                        if( $priv[3] || $vuser['userid']==$userid ){
                                            
?>
                <tr>
                    <td style=" font-size: 14px;">
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <!--i class="glyphicon glyphicon-user" style="font-size:20px;color: blue;"></i-->
                        <!--i class="glyphicon glyphicon-zoom-in"></i-->
                        <?php //echo "".$data['fullname'][$kuser]; ?>
                        <?php
                            echo Html::a('<i class="glyphicon glyphicon-user" style="font-size:20px;color:blue;"><span style="color: black; font-size:14px; font-family: arial;"> '.$data['fullname'][$kuser].'</span></i>'
                                    ,NULL 
                                    ,[
                                        'id'            => 'openUser'.$kuser,
                                        'data-url'      => '/cpay/y60/show-detail',
                                        'period'        => $request->get('period'),
                                        'zone_code'     => $vuser['zone_code'],
                                        'provincecode'  => $vuser['provincecode'],
                                        'amphurcode'    => $vuser['amphurcode'],
                                        'tamboncode'    => $vuser['tamboncode'],
                                        'hsitecode'     => $vuser['hsitecode'],
                                        'username'      => $vuser['username'],
                                        'paytype'       => $vuser['paytype'],
                                        'userid'        => $vuser['userid'],
                                        'fullname'      => $vuser['fullname'],
                                        'class'         => 'btn btn-link'
                                    ]);
                        ?>
                    </td>
                    <td style="text-align: right;">
                        <?php
                            echo number_format($vuser['cca02_service_image']);
                        ?>
                    </td>
                    <td style="text-align: right;">
                        <?php
                            echo number_format($vuser['cca02_service_noimage']);
                        ?>
                    </td>
                    <td style="text-align: right;">
                        <?php
                            echo number_format($vuser['cca02p1_service']);
                        ?>
                    </td>
                    <td style="text-align: right;">
                        <?php
                            echo number_format($vuser['cca03_confirm_service_first']);
                        ?>
                    </td>
                    <td style="text-align: right;">
                        <?php
                            echo number_format($vuser['cca03_confirm_service_followup']);
                        ?>
                    </td>
                    <td style="text-align: right;">
                        <?php
                            echo number_format($vuser['cca04_confirm_service_biopsy']);
                        ?>
                    </td>
                    <td style="text-align: right;">
                        <?php
                            echo number_format($vuser['cca04_confirm_service_liverresection']);
                        ?>
                    </td>
                    <td style="text-align: right;">
                        <?php
                            echo number_format($vuser['cca01_confirm']);
                        ?>
                    </td>
                    <td style="text-align: right;">
                        <?php
                            echo number_format($vuser['cca02_confirm']);
                        ?>
                    </td>
                    <td style="text-align: right;">
                        <?php
                            echo number_format($vuser['cca02p1_confirm']);
                        ?>
                    </td>
                    <td style="text-align: right;">
                        <?php
                            echo number_format($vuser['cca03_confirm']);
                        ?>
                    </td>
                    <td style="text-align: right;">
                        <?php
                            echo number_format($vuser['cca04_confirm']);
                        ?>
                    </td>
                    <td style="text-align: right;">
                        <?php
                            echo number_format($vuser['cca05_confirm']);
                        ?>
                    </td>
                    <?php
                        // สามารถดูยอดได้
                        if( $priv[1] ){
                    ?>
                    <td style="text-align: right;">
                        <?php
                            echo number_format($vuser['s_pay']);    // site ค่าตอบแทนที่ต้องจ่าย แต่ยกเว้น (เจ้าหน้าที่ CASCAP)
                        ?>
                    </td>
                    <td style="text-align: right;">
                        <?php
                            echo number_format($vuser['u_pay']);    // user ค่าตอบแทน ของเจ้าหน้าที่ หรือแพทย์ 
                        ?>
                    </td>
                    <?php
                        // สามารถดูยอดได้
                        }
                    ?>
                </tr>
                    
<?php
                                            
                                        }
                                        // แสดงเป็นรายคน
                                        
                                        
                                        
                                    }
                                    // รายโรงพยาบาล
                                    
                                    // สรุป รายโรงพยาบาล begin
                                    if( $priv[4] ){
                ?>
                <tr>
                    <td style=" font-size: 14px;">
                        <b>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <u>
                                รวมทั้งหมดใน <?php echo $khosp.": ".str_replace("โรงพยาบาล", "รพ.", $data['hospital'][$kprov][$kamp][$khosp]); ?>
                            </u>
                        </b>
                    </td>
                    <td style="text-align: right;">
                        <?php
                            echo number_format($h_cca02_service_image);
                        ?>
                    </td>
                    <td style="text-align: right;">
                        <?php
                            echo number_format($h_cca02_service_noimage);
                        ?>
                    </td>
                    <td style="text-align: right;">
                        <?php
                            echo number_format($h_cca02p1_service);
                        ?>
                    </td>
                    <td style="text-align: right;">
                        <?php
                            echo number_format($h_cca03_confirm_service_first);
                        ?>
                    </td>
                    <td style="text-align: right;">
                        <?php
                            echo number_format($h_cca03_confirm_service_followup);
                        ?>
                    </td>
                    <td style="text-align: right;">
                        <?php
                            echo number_format($h_cca04_confirm_service_biopsy);
                        ?>
                    </td>
                    <td style="text-align: right;">
                        <?php
                            echo number_format($h_cca04_confirm_service_liverresection);
                        ?>
                    </td>
                    <td style="text-align: right;">
                        <?php
                            echo number_format($h_cca01_confirm);
                        ?>
                    </td>
                    <td style="text-align: right;">
                        <?php
                            echo number_format($h_cca02_confirm);
                        ?>
                    </td>
                    <td style="text-align: right;">
                        <?php
                            echo number_format($h_cca02p1_confirm);
                        ?>
                    </td>
                    <td style="text-align: right;">
                        <?php
                            echo number_format($h_cca03_confirm);
                        ?>
                    </td>
                    <td style="text-align: right;">
                        <?php
                            echo number_format($h_cca04_confirm);
                        ?>
                    </td>
                    <td style="text-align: right;">
                        <?php
                            echo number_format($h_cca05_confirm);
                        ?>
                    </td>
                    <?php
                        // สามารถดูยอดได้
                        if( $priv[1] ){
                    ?>
                    <td style="text-align: right;">
                        <?php
                            echo number_format($h_cascap_pay);    // site ค่าตอบแทนที่ต้องจ่าย แต่ยกเว้น (เจ้าหน้าที่ CASCAP)
                        ?>
                    </td>
                    <td style="text-align: right;">
                        <?php
                            echo number_format($h_user_pay);    // user ค่าตอบแทน ของเจ้าหน้าที่ หรือแพทย์ 
                        ?>
                    </td>
                    <?php
                        // สามารถดูยอดได้
                        }
                    ?>
                </tr>
                <?php
                                    }
                                    // สรุป รายโรงพยาบาล end
                
                                    $amp_cascap_pay                           = $amp_cascap_pay + $h_cascap_pay;
                                    $amp_user_pay                             = $amp_user_pay + $h_user_pay;
                                    $amp_cca02_service_image                  = $amp_cca02_service_image + $h_cca02_service_image;
                                    $amp_cca02_service_noimage                = $amp_cca02_service_noimage + $h_cca02_service_noimage;
                                    $amp_cca02p1_service                      = $amp_cca02p1_service + $h_cca02p1_service;
                                    $amp_cca03_confirm_service_first          = $amp_cca03_confirm_service_first + $h_cca03_confirm_service_first;
                                    $amp_cca03_confirm_service_followup       = $amp_cca03_confirm_service_followup + $h_cca03_confirm_service_followup;
                                    $amp_cca04_confirm_service_biopsy         = $amp_cca04_confirm_service_biopsy + $h_cca04_confirm_service_biopsy;
                                    $amp_cca04_confirm_service_liverresection = $amp_cca04_confirm_service_liverresection + $h_cca04_confirm_service_liverresection;
                                    $amp_cca01_confirm                        = $amp_cca01_confirm + $h_cca01_confirm;
                                    $amp_cca02_confirm                        = $amp_cca02_confirm + $h_cca02_confirm;
                                    $amp_cca02p1_confirm                      = $amp_cca02p1_confirm + $h_cca02p1_confirm;
                                    $amp_cca03_confirm                        = $amp_cca03_confirm + $h_cca03_confirm;
                                    $amp_cca04_confirm                        = $amp_cca04_confirm + $h_cca04_confirm;
                                    $amp_cca05_confirm                        = $amp_cca05_confirm + $h_cca05_confirm;
                                    
                                    
                                }
                    
                            }
                        }
                        
                        
                        if( 1 ){
                ?>
                <tr>
                    <td style=" font-size: 14px;">
                        <b>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <u>
                        รวมทั้งหมดใน<?php echo "อำเภอ ".$data['amphur'][$kprov][$kamp]; ?>
                            </u>
                        </b>
                    </td>
                    <td style="text-align: right;">
                        <?php
                            echo number_format($amp_cca02_service_image);
                        ?>
                    </td>
                    <td style="text-align: right;">
                        <?php
                            echo number_format($amp_cca02_service_noimage);
                        ?>
                    </td>
                    <td style="text-align: right;">
                        <?php
                            echo number_format($amp_cca02p1_service);
                        ?>
                    </td>
                    <td style="text-align: right;">
                        <?php
                            echo number_format($amp_cca03_confirm_service_first);
                        ?>
                    </td>
                    <td style="text-align: right;">
                        <?php
                            echo number_format($amp_cca03_confirm_service_followup);
                        ?>
                    </td>
                    <td style="text-align: right;">
                        <?php
                            echo number_format($amp_cca04_confirm_service_biopsy);
                        ?>
                    </td>
                    <td style="text-align: right;">
                        <?php
                            echo number_format($amp_cca04_confirm_service_liverresection);
                        ?>
                    </td>
                    <td style="text-align: right;">
                        <?php
                            echo number_format($amp_cca01_confirm);
                        ?>
                    </td>
                    <td style="text-align: right;">
                        <?php
                            echo number_format($amp_cca02_confirm);
                        ?>
                    </td>
                    <td style="text-align: right;">
                        <?php
                            echo number_format($amp_cca02p1_confirm);
                        ?>
                    </td>
                    <td style="text-align: right;">
                        <?php
                            echo number_format($amp_cca03_confirm);
                        ?>
                    </td>
                    <td style="text-align: right;">
                        <?php
                            echo number_format($amp_cca04_confirm);
                        ?>
                    </td>
                    <td style="text-align: right;">
                        <?php
                            echo number_format($amp_cca05_confirm);
                        ?>
                    </td>
                    <?php
                        // สามารถดูยอดได้
                        if( $priv[1] ){
                    ?>
                    <td style="text-align: right;">
                        <?php
                            echo number_format($amp_cascap_pay);    // site ค่าตอบแทนที่ต้องจ่าย แต่ยกเว้น (เจ้าหน้าที่ CASCAP)
                        ?>
                    </td>
                    <td style="text-align: right;">
                        <?php
                            echo number_format($amp_user_pay);    // user ค่าตอบแทน ของเจ้าหน้าที่ หรือแพทย์ 
                        ?>
                    </td>
                    <?php
                        // สามารถดูยอดได้ 
                        }
                    ?>
                </tr>
                <?php
                        }
                        
                        
                        
                        
                        /// รวมแต่ละ อำเภอ 
                        ///
                        $prov_cascap_pay                           = $prov_cascap_pay + $amp_cascap_pay;
                        $prov_user_pay                             = $prov_user_pay + $amp_user_pay;
                        $prov_cca02_service_image                  = $prov_cca02_service_image + $amp_cca02_service_image;
                        $prov_cca02_service_noimage                = $prov_cca02_service_noimage + $amp_cca02_service_noimage;
                        $prov_cca02p1_service                      = $prov_cca02p1_service + $amp_cca02p1_service;
                        $prov_cca03_confirm_service_first          = $prov_cca03_confirm_service_first + $amp_cca03_confirm_service_first;
                        $prov_cca03_confirm_service_followup       = $prov_cca03_confirm_service_followup + $amp_cca03_confirm_service_followup;
                        $prov_cca04_confirm_service_biopsy         = $prov_cca04_confirm_service_biopsy + $amp_cca04_confirm_service_biopsy;
                        $prov_cca04_confirm_service_liverresection = $prov_cca04_confirm_service_liverresection + $amp_cca04_confirm_service_liverresection;
                        $prov_cca01_confirm                        = $prov_cca01_confirm + $amp_cca01_confirm;
                        $prov_cca02_confirm                        = $prov_cca02_confirm + $amp_cca02_confirm;
                        $prov_cca02p1_confirm                      = $prov_cca02p1_confirm + $amp_cca02p1_confirm;
                        $prov_cca03_confirm                        = $prov_cca03_confirm + $amp_cca03_confirm;
                        $prov_cca04_confirm                        = $prov_cca04_confirm + $amp_cca04_confirm;
                        $prov_cca05_confirm                        = $prov_cca05_confirm + $amp_cca05_confirm;
                        ///
                        ///
                    }
                    // รายอำเภอ
                    // สรุป ราย อำเภอ รายอำเภอ end
                    
                    
                    // สรุป ราย อำเภอ รายอำเภอ begin
                    if( $request->get('view')=='summaryhosp' ){
                        // ถ้าแสดงเป็น รพ. ไม่ต้องสรุป อำเภอ
                    }else{
//                        if( $priv[7] 
//                            || ( $priv[6] && $kamp == $myuserprofile['hospital'][0]['amphurcode'] ) ){
                        
                        
                    }
                    //
                    
                // สรุป ราย จังหวัด รายอำเภอ begin
                // display on summaryprov
                if( $request->get('view')=='summaryprov' || $request->get('view')=='summaryzone' ){
                ?>
                <tr>
                    <td style=" font-size: 14px;">
                        <b>
                        &nbsp;&nbsp;
                            <u>
                        รวมทั้งหมดใน<?php echo "จังหวัด ".$data['province'][$kprov]; ?>
                            </u>
                        </b>
                    </td>
                    <td style="text-align: right;">
                        <?php
                            echo number_format($prov_cca02_service_image);
                        ?>
                    </td>
                    <td style="text-align: right;">
                        <?php
                            echo number_format($prov_cca02_service_noimage);
                        ?>
                    </td>
                    <td style="text-align: right;">
                        <?php
                            echo number_format($prov_cca02p1_service);
                        ?>
                    </td>
                    <td style="text-align: right;">
                        <?php
                            echo number_format($prov_cca03_confirm_service_first);
                        ?>
                    </td>
                    <td style="text-align: right;">
                        <?php
                            echo number_format($prov_cca03_confirm_service_followup);
                        ?>
                    </td>
                    <td style="text-align: right;">
                        <?php
                            echo number_format($prov_cca04_confirm_service_biopsy);
                        ?>
                    </td>
                    <td style="text-align: right;">
                        <?php
                            echo number_format($prov_cca04_confirm_service_liverresection);
                        ?>
                    </td>
                    <td style="text-align: right;">
                        <?php
                            echo number_format($prov_cca01_confirm);
                        ?>
                    </td>
                    <td style="text-align: right;">
                        <?php
                            echo number_format($prov_cca02_confirm);
                        ?>
                    </td>
                    <td style="text-align: right;">
                        <?php
                            echo number_format($prov_cca02p1_confirm);
                        ?>
                    </td>
                    <td style="text-align: right;">
                        <?php
                            echo number_format($prov_cca03_confirm);
                        ?>
                    </td>
                    <td style="text-align: right;">
                        <?php
                            echo number_format($prov_cca04_confirm);
                        ?>
                    </td>
                    <td style="text-align: right;">
                        <?php
                            echo number_format($prov_cca05_confirm);
                        ?>
                    </td>
                    <?php
                        // สามารถดูยอดได้
                        if( $priv[1] ){
                    ?>
                    <td style="text-align: right;">
                        <?php
                            echo number_format($prov_cascap_pay);    // site ค่าตอบแทนที่ต้องจ่าย แต่ยกเว้น (เจ้าหน้าที่ CASCAP)
                        ?>
                    </td>
                    <td style="text-align: right;">
                        <?php
                            echo number_format($prov_user_pay);    // user ค่าตอบแทน ของเจ้าหน้าที่ หรือแพทย์ 
                        ?>
                    </td>
                    <?php
                        // สามารถดูยอดได้ 
                        }
                    ?>
                </tr>
                <?php
                }
                // สรุป ราย จังหวัด รายอำเภอ end
                // 
                // รวมจาก จังหวัด => เขต
                $zone_cascap_pay                           = $zone_cascap_pay + $prov_cascap_pay;
                $zone_user_pay                             = $zone_user_pay + $prov_user_pay;
                $zone_cca02_service_image                  = $zone_cca02_service_image + $prov_cca02_service_image;
                $zone_cca02_service_noimage                = $zone_cca02_service_noimage + $prov_cca02_service_noimage;
                $zone_cca02p1_service                      = $zone_cca02p1_service + $prov_cca02p1_service;
                $zone_cca03_confirm_service_first          = $zone_cca03_confirm_service_first + $prov_cca03_confirm_service_first;
                $zone_cca03_confirm_service_followup       = $zone_cca03_confirm_service_followup + $prov_cca03_confirm_service_followup;
                $zone_cca04_confirm_service_biopsy         = $zone_cca04_confirm_service_biopsy + $prov_cca04_confirm_service_biopsy;
                $zone_cca04_confirm_service_liverresection = $zone_cca04_confirm_service_liverresection + $prov_cca04_confirm_service_liverresection;
                $zone_cca01_confirm                        = $zone_cca01_confirm + $prov_cca01_confirm;
                $zone_cca02_confirm                        = $zone_cca02_confirm + $prov_cca02_confirm;
                $zone_cca02p1_confirm                      = $zone_cca02p1_confirm + $prov_cca02p1_confirm;
                $zone_cca03_confirm                        = $zone_cca03_confirm + $prov_cca03_confirm;
                $zone_cca04_confirm                        = $zone_cca04_confirm + $prov_cca04_confirm;
                $zone_cca05_confirm                        = $zone_cca05_confirm + $prov_cca05_confirm;
                
                    
            }
       
            // รายจังหวัด
            
        // สรุป ราย เขต begin
        // display on summaryzone
        if( $request->get('view')=='summaryzone' ){
            ?>
                <tr>
                    <td style=" font-size: 14px;">
                        <b>
                        &nbsp;&nbsp;
                            <u>
                        รวมทั้งหมดใน<?php echo "เขต ".$kzone; ?>
                            </u>
                        </b>
                    </td>
                    <td style="text-align: right;">
                        <?php
                            echo number_format($zone_cca02_service_image);
                        ?>
                    </td>
                    <td style="text-align: right;">
                        <?php
                            echo number_format($zone_cca02_service_noimage);
                        ?>
                    </td>
                    <td style="text-align: right;">
                        <?php
                            echo number_format($zone_cca02p1_service);
                        ?>
                    </td>
                    <td style="text-align: right;">
                        <?php
                            echo number_format($zone_cca03_confirm_service_first);
                        ?>
                    </td>
                    <td style="text-align: right;">
                        <?php
                            echo number_format($zone_cca03_confirm_service_followup);
                        ?>
                    </td>
                    <td style="text-align: right;">
                        <?php
                            echo number_format($zone_cca04_confirm_service_biopsy);
                        ?>
                    </td>
                    <td style="text-align: right;">
                        <?php
                            echo number_format($zone_cca04_confirm_service_liverresection);
                        ?>
                    </td>
                    <td style="text-align: right;">
                        <?php
                            echo number_format($zone_cca01_confirm);
                        ?>
                    </td>
                    <td style="text-align: right;">
                        <?php
                            echo number_format($zone_cca02_confirm);
                        ?>
                    </td>
                    <td style="text-align: right;">
                        <?php
                            echo number_format($zone_cca02p1_confirm);
                        ?>
                    </td>
                    <td style="text-align: right;">
                        <?php
                            echo number_format($zone_cca03_confirm);
                        ?>
                    </td>
                    <td style="text-align: right;">
                        <?php
                            echo number_format($zone_cca04_confirm);
                        ?>
                    </td>
                    <td style="text-align: right;">
                        <?php
                            echo number_format($zone_cca05_confirm);
                        ?>
                    </td>
                    <?php
                        // สามารถดูยอดได้
                        if( $priv[1] ){
                    ?>
                    <td style="text-align: right;">
                        <?php
                            echo number_format($zone_cascap_pay);    // site ค่าตอบแทนที่ต้องจ่าย แต่ยกเว้น (เจ้าหน้าที่ CASCAP)
                        ?>
                    </td>
                    <td style="text-align: right;">
                        <?php
                            echo number_format($zone_user_pay);    // user ค่าตอบแทน ของเจ้าหน้าที่ หรือแพทย์ 
                        ?>
                    </td>
                    <?php
                        // สามารถดูยอดได้ 
                        }
                    ?>
                </tr>
                <?php
        }
        // สรุป ราย เขต end 
        // 
        // 
        // รวมจาก เขต => all
        $all_cascap_pay                           = $all_cascap_pay + $zone_cascap_pay;
        $all_user_pay                             = $all_user_pay + $zone_user_pay;
        $all_cca02_service_image                  = $all_cca02_service_image + $zone_cca02_service_image;
        $all_cca02_service_noimage                = $all_cca02_service_noimage + $zone_cca02_service_noimage;
        $all_cca02p1_service                      = $all_cca02p1_service + $zone_cca02p1_service;
        $all_cca03_confirm_service_first          = $all_cca03_confirm_service_first + $zone_cca03_confirm_service_first;
        $all_cca03_confirm_service_followup       = $all_cca03_confirm_service_followup + $zone_cca03_confirm_service_followup;
        $all_cca04_confirm_service_biopsy         = $all_cca04_confirm_service_biopsy + $zone_cca04_confirm_service_biopsy;
        $all_cca04_confirm_service_liverresection = $all_cca04_confirm_service_liverresection + $zone_cca04_confirm_service_liverresection;
        $all_cca01_confirm                        = $all_cca01_confirm + $zone_cca01_confirm;
        $all_cca02_confirm                        = $all_cca02_confirm + $zone_cca02_confirm;
        $all_cca02p1_confirm                      = $all_cca02p1_confirm + $zone_cca02p1_confirm;
        $all_cca03_confirm                        = $all_cca03_confirm + $zone_cca03_confirm;
        $all_cca04_confirm                        = $all_cca04_confirm + $zone_cca04_confirm;
        $all_cca05_confirm                        = $all_cca05_confirm + $zone_cca05_confirm;
                
        // รวมจาก เขต => all
    }
    // รายเขต
    
    
    // สรุป ทั้งหมด begin
            ?>
                <tr>
                    <td style=" font-size: 14px;">
                        <h3>
                            <b>
                                <u>
                                    รวมทั้งหมด
                                </u>
                            </b>
                        </h3>
                    </td>
                    <td style="text-align: right;">
                        <?php
                            echo number_format($all_cca02_service_image);
                        ?>
                    </td>
                    <td style="text-align: right;">
                        <?php
                            echo number_format($all_cca02_service_noimage);
                        ?>
                    </td>
                    <td style="text-align: right;">
                        <?php
                            echo number_format($all_cca02p1_service);
                        ?>
                    </td>
                    <td style="text-align: right;">
                        <?php
                            echo number_format($all_cca03_confirm_service_first);
                        ?>
                    </td>
                    <td style="text-align: right;">
                        <?php
                            echo number_format($all_cca03_confirm_service_followup);
                        ?>
                    </td>
                    <td style="text-align: right;">
                        <?php
                            echo number_format($all_cca04_confirm_service_biopsy);
                        ?>
                    </td>
                    <td style="text-align: right;">
                        <?php
                            echo number_format($all_cca04_confirm_service_liverresection);
                        ?>
                    </td>
                    <td style="text-align: right;">
                        <?php
                            echo number_format($all_cca01_confirm);
                        ?>
                    </td>
                    <td style="text-align: right;">
                        <?php
                            echo number_format($all_cca02_confirm);
                        ?>
                    </td>
                    <td style="text-align: right;">
                        <?php
                            echo number_format($all_cca02p1_confirm);
                        ?>
                    </td>
                    <td style="text-align: right;">
                        <?php
                            echo number_format($all_cca03_confirm);
                        ?>
                    </td>
                    <td style="text-align: right;">
                        <?php
                            echo number_format($all_cca04_confirm);
                        ?>
                    </td>
                    <td style="text-align: right;">
                        <?php
                            echo number_format($all_cca05_confirm);
                        ?>
                    </td>
                    <?php
                        // สามารถดูยอดได้
                        if( $priv[1] ){
                    ?>
                    <td style="text-align: right;">
                        <?php
                            echo number_format($all_cascap_pay);    // site ค่าตอบแทนที่ต้องจ่าย แต่ยกเว้น (เจ้าหน้าที่ CASCAP)
                        ?>
                    </td>
                    <td style="text-align: right;">
                        <?php
                            echo number_format($all_user_pay);    // user ค่าตอบแทน ของเจ้าหน้าที่ หรือแพทย์ 
                        ?>
                    </td>
                    <?php
                        // สามารถดูยอดได้ 
                        }
                    ?>
                </tr>
                <?php
    // สรุป ทั้งหมด end
}
?>
                
            </table>
        </div>
    </div>
</div>
<?php
    if( 0 ){
        echo "<pre align='left'>";
        print_r($data);
        echo "</pre>";
    }

