<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\modules\cpay\classes\UserCanView;
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$this->registerJsFile('/js/excellentexport.js',['position'=>\yii\web\View::POS_BEGIN]);
$request    = Yii::$app->request;


if( 0 ){
    echo "<pre align='left'>";
    print_r($userprofile[hospital][0]);
    echo "</pre>";
}
if( strlen($request->get('period'))==0 ){
    $_period        = "20170301";       // initial for current period
}else{
    $_period        = $request->get('period');
}
    
    $_btnAll        = "<i class=\"glyphicon glyphicon-saved\"></i>แสดงข้อมูลทั้งหมด";
    $_btnZone       = "<i class=\"glyphicon glyphicon-saved\"></i>เขต ".($userprofile['hospital'][0]['zone_code']*1);
    $_btnProvince   = "<i class=\"glyphicon glyphicon-saved\"></i>".$userprofile['hospital'][0]['provincecode'].": ".$userprofile['hospital'][0]['province']."";
    $_zonevalue     = $userprofile['hospital'][0]['zone_code'];
    $_urlZone       = "/cpay/y60?view=summary&zone=".$_zonevalue;
    $_provvalue     = $userprofile['hospital'][0]['provincecode'];
    $_urlProvince   = "/cpay/y60?view=summary&zone=".$_zonevalue."&prov=".$_provvalue;
    $_btnAmphur     = "<i class=\"glyphicon glyphicon-saved\"></i>".$userprofile['hospital'][0]['amphurcode'].": ".$userprofile['hospital'][0]['amphur']."";
    $_ampvalue      = $userprofile['hospital'][0]['amphurcode'];
    $_btnHospital   = "<i class=\"glyphicon glyphicon-saved\"></i>".$userprofile['hospital'][0]['hcode'].": ".$userprofile['hospital'][0]['name']."";
    $_hospvalue     = $userprofile['hospital'][0]['hcode'];
    
    
    $userid     = Yii::$app->user->identity->id;
    $_usergroup     = UserCanView::getUserGroup($userid , $userprofile);
    
?>


<?php
            echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
            echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
            echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
            echo "&nbsp;&nbsp;";
            if( $_usergroup == 6 ){
                echo Html::a($_btnAll, 
                        NULL,
                        [
                            'class' => 'btn btn-info' ,
                            'id'    => 'bListAll',
                            'period'=> $_period,
                            'view'  => 'summaryall',
                            'title' => 'แสดงข้อมูลทั้งหมด',
                            ]
                        );
                echo "&nbsp;";
            }else{
                echo Html::a($_btnAll, 
                        NULL,
                        [
                            'class' => 'btn btn-default active' ,
                            'id'    => 'disbListAll',
                            'disabled' => 'true',
                            'title' => 'ไม่สามารถแสดงข้อมูลทั้งหมด',
                            ]
                        );
                echo "&nbsp;";
            }
            
            echo "&nbsp;";
            
            if( $_usergroup == 6 ){
                echo Html::a($_btnZone, 
                        NULL,
                        [
                            'class' => 'btn btn-info' ,
                            'id'    => 'bListZone',
                            'period'=> $_period,
                            'view'  => 'summaryzone',
                            'zone'  => $_zonevalue ,
                            'title' => 'แสดงภายในเขต',
                            ]
                        );
                echo "&nbsp;";
            }else{
                echo Html::a($_btnZone, 
                        NULL,
                        [
                            'class' => 'btn btn-default active' ,
                            'id'    => 'disbListZone',
                            'disabled' => 'true',
                            'title' => 'ไม่สามารถแสดงภายในเขต',
                            ]
                        );
                echo "&nbsp;";
            }
            
            
            echo "&nbsp;&nbsp;";
            
            if( $_usergroup == 6 || $_usergroup == 4 ){
                echo Html::a($_btnProvince, 
                        NULL,
                        [
                            'class' => 'btn btn-warning' ,
                            'id'    => 'bListProvince',
                            'period'=> $_period,
                            'view'  => 'summaryprov',
                            'zone'  => $_zonevalue ,
                            'prov'  => $_provvalue ,
                            'title' => 'แสดงภายในจังหวัด',
                            ]
                        );
                echo "&nbsp;";
            }else{
                echo Html::a($_btnProvince, 
                        NULL,
                        [
                            'class' => 'btn btn-default active' ,
                            'id'    => 'disbListProvince',
                            'disabled' => 'true',
                            'title' => 'ไม่สามารถแสดงภายในจังหวัด',
                            ]
                        );
                echo "&nbsp;";
            }
            
            
                echo Html::a($_btnAmphur, 
                        NULL,
                        [
                            'class' => 'btn btn-info' ,
                            'id'    => 'bListAmphur',
                            'period'=> $_period,
                            'view'  => 'summaryamp',
                            'zone'  => $_zonevalue ,
                            'prov'  => $_provvalue ,
                            'xax'   => $_ampvalue ,
                            'title' => 'แสดงภายในอำเภอ',
                            ]
                        );
                echo "&nbsp;";
                echo Html::a($_btnHospital, 
                        NULL,
                        [
                            'class' => 'btn btn-info' ,
                            'id'    => 'bListHospital',
                            'period'=> $_period,
                            'view'  => 'summaryhosp',
                            'zone'  => $_zonevalue ,
                            'prov'  => $_provvalue ,
                            'xax'   => $_ampvalue ,
                            'hosp'  => $_hospvalue ,
                            'title' => 'แสดงภายในโรงพยาบาล',
                            ]
                        );
                echo "&nbsp;";
                 echo Html::a("Export excel",
                        NULL,
                        [
                            'class' => 'btn btn-success pull-rigth' ,
                            'id'    => 'export-excel',
                            'title' => 'นำออกรายงาน',
                            ]
                        );
?>

<?php
$jsAdd = <<< JS
      
//$(document).on("click","#btnProvince", function() {
$(document).on("click", "*[id^=bList]", function() {
    $("*[id^=bList]").attr("class","btn btn-info");
    $(this).attr("class","btn btn-warning");
    var period  = $(this).attr("period");
    var view    = $(this).attr("view");
    var zone    = $(this).attr("zone");
    var prov    = $(this).attr("prov");
    var xax    = $(this).attr("xax");
    var hosp    = $(this).attr("hosp");
    var url     = "/cpay/y60/summary-table?period="+period+"&view="+view+"&zone="+zone+"&prov="+prov+"&xax="+xax+"&hosp="+hosp+"";
    
    $("#summary_table").html('<div><i class="fa fa-circle-o-notch fa-spin" style="font-size:48px;color:red></i></div>');
    $.get(url, {
        period:period,
        view:view,
        zone:zone,
        prov:prov,
    })
    .done(function( data ) {
        $("#summary_table").html(data);
    });
});
        
 $('#export-excel').click(function(){
   var sum_table=$("#summary_table");
   var sum_clone=$("#summary_clone");
    var copy = sum_table.clone();
    copy.attr('id', 'summary_clone');
    sum_clone.replaceWith(copy);
    $("#summary_clone").hide();
    this.download = "cpay-y60-report.xls";
   ExcellentExport.excel(this,sum_clone.attr('id'), 'Cpay y60 report');
 });


JS;
$this->registerJs($jsAdd);

