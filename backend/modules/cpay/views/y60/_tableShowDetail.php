<?php



/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
use backend\modules\cpay\classes\QueryGetSummary;


if( 0 ){
    echo "<pre align='left'>";
    print_r($data);
    echo "</pre>";
}


$request = Yii::$app->request;

//echo $request->get('period');

if( $request->get('period') == '20170625' ){
    //
    $table_title    = "ค่าตอบแทนสำหรับแพทย์ผู้ตรวจ ตามประกาศกระทรวง (เดือน มิถุนายน ปีงบประมาณ 2560 ตัดยอดเมื่อวันที่ 25 มิถุนายน 2560)";
}else{
    $table_title    = "ค่าตอบแทนสำหรับแพทย์ผู้ตรวจ ตามประกาศกระทรวง (เดือน มีนาคม ปีงบประมาณ 2560)";
}

    
?>

<div class="modal-header">
    <button type="button" class="close"  data-dismiss="modal" >
        <span aria-hidden="true">&times;</span>
    </button>
    <h4 class="modal-title" id="label-list"></h4>
</div>
<div class="modal-body" id="body-list">
<div class="table-responsive" style="padding:50px 50px; background-color: #FFFFFF;">
    <br />
    <br />
    <br />
    <br />
    <div class="row" style="margin: 0">
        <div id="datalist" style="align-content: flex-start;">
    <?php
    //print_r($request);
    echo $this->render('_profileHeader',
        [
            'userprofile'=>$userprofile,
            'data'          => $data,
        ]);
    ?>   
        </div>
    </div>
    <br />
    <div class="row" style="margin-left: 20px;width: 1200px;">
        <h3>
            <i class="glyphicon glyphicon-list-alt" style="color: green;"></i>
            <?php echo $table_title; ?>
        </h3>
    </div>
    <div class="row" style="width: 1200px;">
        <table width="90%" align="center" border="1" bordercolor="#000000" cellspacing="2" cellpadding="2" style="border-collapse: collapse;">
            <thead>
                <tr>
                    <td rowspan="2" align="center" valign="top">
                        <span class="lead">ชื่อผู้ตรวจ</span>
                    </td>
                    <td colspan="2" align="center" valign="top">
                        <span class="">จำนวนข้อมูล</span>
                    </td>
                    <td rowspan="2" align="center" valign="top">
                        <span class="">ข้อมูลไม่สมบูรณ์ (ไม่เข้าตรวจ)</span>
                    </td>
                    <td colspan="4" align="center" valign="top">
                        <span class="">ข้อมูลสมบูรณ์ผ่านการตรวจ</span>
                    </td>
                </tr>
                <tr>
                    <td align="center" valign="top">
                        <span class="">จากการสัญจร</span>
                    </td>
                    <td align="center" valign="top">
                        <span class="">จากการคีย์</span>
                    </td>
                    <td align="center" valign="top">
                        <span class="" alt="ข้อมูลที่ผ่านการ Submit เข้ามาเท่านั้น" title="ข้อมูลที่ผ่านการ Submit เข้ามาเท่านั้น">ผ่าน</span>
                    </td>
                    <td align="center" valign="top">
                        <span class="">ค่าตอบแทน <br>(บาท)</span>
                    </td>
                    <td align="center" valign="top">
                        <span class="" alt="เหลือเพียงขั้นตอนการกด Submit จากผู้ลงข้อมูล" title="เหลือเพียงขั้นตอนการกด Submit จากผู้ลงข้อมูล">save draft</span>
                    </td>
                    <td align="center" valign="top">
                        <span class="">ไม่ผ่าน</span>
                    </td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td colspan="1" align="left" valign="top">
                        <span class="">&nbsp;แสดงข้อมูลทั้งหมดของ เมืองน่าน&nbsp;</span>
                    </td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td align="left" valign="top">
                        <span class="">
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            CCA-02&nbsp;
                        </span>
                    </td>
                    <td align="right" valign="top">
                        <span class=""><?php echo number_format($data['cca02_usmobile'], 0, '.', ','); ?>&nbsp;</span>
                    </td>
                    <td align="right" valign="top">
                        <span class=""><?php echo number_format($data['cca02_keyin'], 0, '.', ','); ?>&nbsp;</span>
                    </td>
                    <td align="right" valign="top">
                        <span class=""><?php echo number_format($data['cca02_notcomplete'], 0, '.', ','); ?>&nbsp;</span>
                    </td>
                    <td align="right" valign="top">
                        <span class=""><?php echo number_format($data['cca02_service'], 0, '.', ','); ?>&nbsp;</span>
                    </td>
                    <td></td>
                    <td align="right" valign="top">
                        <span class=""><?php echo number_format($data['cca02_savedraft'], 0, '.', ','); ?>&nbsp;</span>
                    </td>
                    <td align="right" valign="top">
                        <span class="" title="อาจมีปัญหา เรื่องใบยินยอมที่ไม่ถูกต้อง"><?php echo number_format($data['cca02_submitnotconfirm'], 0, '.', ','); ?>&nbsp;</span>
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top">
                        <span class="">
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            มีภาพ
                        </span>
                    </td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td align="right" valign="top">
                        <span><?php echo number_format($data['cca02_confirm_person_image'], 0, '.', ','); ?>&nbsp;</span>        </td>
                    <td align="right" valign="top">
                        <span class="">
                            <?php
                                $data['cca02_confirm_person_image_pay'] = QueryGetSummary::getCCA02ServImgPay($data['cca02_confirm_person_image']);
                                echo number_format($data['cca02_confirm_person_image_pay'], 0, '.', ',');
                            ?>&nbsp;</span>
                    </td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td align="left" valign="top">
                        <span class="">
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            ไม่มีภาพ
                        </span>
                    </td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td align="right" valign="top">
                        <span><?php echo number_format($data['cca02_confirm_person_noimage'], 0, '.', ','); ?>&nbsp;</span>        
                    </td>
                    <td align="right" valign="top">
                        <span class="">
                            <?php
                                $data['cca02_confirm_person_noimage_pay'] = QueryGetSummary::getCCA02ServNoneImgPay($data['cca02_confirm_person_noimage']);
                                echo number_format($data['cca02_confirm_person_noimage_pay'], 0, '.', ',');
                            ?>&nbsp;</span>
                    </td>
                    <td></td>
                    <td></td>
                </tr>

                <tr>
                    <td align="left" valign="top">
                        <span class="">
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            CCA-02.1&nbsp;
                        </span>
                    </td>
                    <td></td>
                    <td align="right" valign="top">
                        <span class=""><?php echo number_format($data['cca02p1_keyin'], 0, '.', ','); ?>&nbsp;</span>
                    </td>
                    <td align="right" valign="top">
                        <span class=""><?php echo number_format($data['cca02p1_notcomplete'], 0, '.', ','); ?>&nbsp;</span>
                    </td>
                    <td align="right" valign="top">
                        <span class=""><?php echo number_format($data['cca02p1_service'], 0, '.', ','); ?>&nbsp;</span>
                    </td>
                    <td align="right" valign="top">
                        <span class=""><?php
                                $data['cca02p1_service_pay'] = QueryGetSummary::getCCA02p1ServPay($data['cca02p1_service']);
                                echo number_format($data['cca02p1_service_pay'], 0, '.', ',');
                            ?>&nbsp;</span>
                    </td>
                    <td align="right" valign="top">
                        <span class=""><?php echo number_format($data['cca02p1_savedraft'], 0, '.', ','); ?>&nbsp;</span>
                    </td>
                    <td align="right" valign="top">
                        <span class=""><?php echo number_format($data['cca02p1_submitnotconfirm'], 0, '.', ','); ?>&nbsp;</span>
                    </td>
                </tr>

                <tr>
                    <td align="left" valign="top">
                        <span class="">
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            CCA-03&nbsp;
                        </span>
                    </td>
                    <td></td>
                    <td align="right" valign="top">
                        <span class=""><?php echo number_format($data['cca03_keyin'], 0, '.', ','); ?>&nbsp;</span>
                    </td>
                    <td align="right" valign="top">
                        <span class=""><?php echo number_format($data['cca03_notcomplete'], 0, '.', ','); ?>&nbsp;</span>
                    </td>
                    <td align="right" valign="top">
                        <span class=""><?php echo number_format($data['cca03_service'], 0, '.', ','); ?>&nbsp;</span>
                    </td>
                    <td></td>
                    <td align="right" valign="top">
                        <span class=""><?php echo number_format($data['cca03_savedraft'], 0, '.', ','); ?>&nbsp;</span>
                    </td>
                    <td align="right" valign="top">
                        <span class=""><?php echo number_format($data['cca03_submitnotconfirm'], 0, '.', ','); ?>&nbsp;</span>
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top">
                        <span class="">
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            ครั้งแรก
                        </span>
                    </td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td align="right" valign="top">
                        <span class=""><?php echo number_format($data['cca03_confirm_service_first'], 0, '.', ','); ?>&nbsp;</span>
                    </td>
                    <td align="right" valign="top">
                        <span class=""><?php
                                $data['cca03_confirm_service_first_pay'] = QueryGetSummary::getCCA03ServPayFirstvisit($data['cca03_confirm_service_first']);
                                echo number_format($data['cca03_confirm_service_first_pay'], 0, '.', ',');
                            ?>&nbsp;</span>
                    </td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td align="left" valign="top">
                        <span class="">
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            ติดตามการรักษา
                        </span>
                    </td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td align="right" valign="top">
                        <span class=""><?php echo number_format($data['cca03_confirm_service_followup'], 0, '.', ','); ?>&nbsp;</span>
                    </td>
                    <td align="right" valign="top">
                        <span class=""><?php
                                $data['cca03_confirm_service_followup_pay'] = QueryGetSummary::getCCA03ServPayFollowup($data['cca03_confirm_service_followup']);
                                echo number_format($data['cca03_confirm_service_followup_pay'], 0, '.', ',');
                            ?>&nbsp;</span>
                    </td>
                    <td></td>
                    <td></td>
                </tr>

                <tr>
                    <td align="left" valign="top">
                        <span class="">
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            CCA-04&nbsp;
                        </span>
                    </td>
                    <td></td>
                    <td align="right" valign="top">
                        <span class=""><?php echo number_format($data['cca04_keyin'], 0, '.', ','); ?>&nbsp;</span>
                    </td>
                    <td align="right" valign="top">
                        <span class=""><?php echo number_format($data['cca04_notcomplete'], 0, '.', ','); ?>&nbsp;</span>
                    </td>
                    <td align="right" valign="top">
                        <span class=""><?php echo number_format($data['cca04_service'], 0, '.', ','); ?>&nbsp;</span>
                    </td>
                    <td></td>
                    <td align="right" valign="top">
                        <span class=""><?php echo number_format($data['cca04_savedraft'], 0, '.', ','); ?>&nbsp;</span>
                    </td>
                    <td align="right" valign="top">
                        <span class=""><?php echo number_format($data['cca04_submitnotconfirm'], 0, '.', ','); ?>&nbsp;</span>
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top">
                        <span class="">
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            Biopsy
                        </span>
                    </td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td align="right" valign="top">
                        <span class=""><?php echo number_format($data['cca04_confirm_service_biopsy'], 0, '.', ','); ?>&nbsp;</span>
                    </td>
                    <td align="right" valign="top">
                        <span class=""><?php
                                $data['cca04_confirm_service_biopsy_pay'] = QueryGetSummary::getCCA04ServPayBiopsy($data['cca04_confirm_service_biopsy']);
                                echo number_format($data['cca04_confirm_service_biopsy_pay'], 0, '.', ',');
                            ?>&nbsp;</span>
                    </td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td align="left" valign="top">
                        <span class="">
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            Liver resection
                        </span>
                    </td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td align="right" valign="top">
                        <span class=""><?php echo number_format($data['cca04_confirm_service_liverresection'], 0, '.', ','); ?>&nbsp;</span>
                    </td>
                    <td align="right" valign="top">
                        <span class=""><?php
                                $data['cca04_confirm_service_liverresection_pay'] = QueryGetSummary::getCCA04ServPayLiverResection($data['cca04_confirm_service_liverresection']);
                                echo number_format($data['cca04_confirm_service_liverresection_pay'], 0, '.', ',');
                            ?>&nbsp;</span>
                    </td>
                    <td></td>
                    <td></td>
                </tr>


                <tr>
                    <td width="20%" align="left">
                        <span class="">รวม</span>
                    </td>
                    <td width="10%" align="left">
                        <span class=""></span>
                    </td>
                    <td width="10%" align="left">
                        <span class=""></span>
                    </td>
                    <td width="15%" align="left">
                        <span class=""></span>
                    </td>
                    <td width="10%" align="left">
                        <span class=""></span>
                    </td>
                    <td width="10%" align="right" valign="top">
                        <span class=""><?php
                                $data['sum_pay'] = QueryGetSummary::getCCA02ServImgPay($data['cca02_confirm_person_image']) 
                                        + QueryGetSummary::getCCA02ServNoneImgPay($data['cca02_confirm_person_noimage']) 
                                        + QueryGetSummary::getCCA02p1ServPay($data['cca02p1_service']) 
                                        + QueryGetSummary::getCCA03ServPayFirstvisit($data['cca03_confirm_service_first']) 
                                        + QueryGetSummary::getCCA03ServPayFollowup($data['cca03_confirm_service_followup']) 
                                        + QueryGetSummary::getCCA04ServPayBiopsy($data['cca04_confirm_service_biopsy']) 
                                        + QueryGetSummary::getCCA04ServPayLiverResection($data['cca04_confirm_service_liverresection']);
                                echo number_format($data['sum_pay'], 0, '.', ',');
                            ?>&nbsp;</span>
                    </td>
                    <td width="10%" align="left">
                        <span class=""></span>
                    </td>
                    <td width="10%" align="left">
                        <span class=""></span>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <br />
    <br />
    <br />
    <br />
    <br />
</div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal"  >Close</button>
</div>
