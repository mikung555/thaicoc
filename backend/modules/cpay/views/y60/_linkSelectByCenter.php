<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use backend\modules\cpay\classes\HospitalFunc;
use backend\modules\cpay\classes\UserCanView;

$request    = Yii::$app->request;

$userid     = Yii::$app->user->identity->id;
$_usergroup     = UserCanView::getUserGroup($userid , $userprofile);

if( strlen($request->get('period'))==0 ){
    $_period        = "20170301";
}else{
    $_period        = $request->get('period');
}

if( strlen($request->get('zone'))>0 ){
    $lsProv     = HospitalFunc::getProvinceByZonecode($request->get('zone'));
}

if( strlen($request->get('prov'))>0 ){
    $lsAmp      = HospitalFunc::getAmphurByProvincecode($request->get('prov'));
}

if( strlen($request->get('prov'))>0 && strlen($request->get('xax'))>0 ){
    $lsHosp      = HospitalFunc::getHospitalByAmphur($request->get('prov'), $request->get('xax'));
}

if( 0 ){  
    echo "<pre align='left'>";
    print_r($lsHosp);
    echo "</pre>";
}

?>
<div class="row" style="height: 10px;"></div>
<div class="row">
    <p>
    <span class="" style="position: absolute; margin-left: 20px; margin-top: 0px;">
        <?php
            if($request->get('period')=='20170301' ){
                echo "<b>เดือนกันยายน ถึง มีนาคม 2560</b>";
            }else if($request->get('period')=='20170625' ){
                echo "<b>ตัดยอดที่เดือนมิถุนายน 2560</b>";
            }
        ?>
    </span>
        <select id="selectzone" name="selectzone" class="btn-info" style="position: absolute; margin-left: 265px; margin-top: 0px;">
            <option value="" id="selectzone" name="selectzone" >เลือกเขต</option>
            <?php
            if( $_usergroup==6 ){
            ?>
            <option value="01" zone="01" period="<?php echo $_period; ?>" view="summaryzone" <?php if($request->get('zone')=='01') { echo "selected"; } ?> >เขต 01</option>
            <option value="06" zone="06" period="<?php echo $_period; ?>" view="summaryzone" <?php if($request->get('zone')=='06') { echo "selected"; } ?> >เขต 06</option>
            <option value="07" zone="07" period="<?php echo $_period; ?>" view="summaryzone" <?php if($request->get('zone')=='07') { echo "selected"; } ?> >เขต 07</option>
            <option value="08" zone="08" period="<?php echo $_period; ?>" view="summaryzone" <?php if($request->get('zone')=='08') { echo "selected"; } ?> >เขต 08</option>
            <option value="09" zone="09" period="<?php echo $_period; ?>" view="summaryzone" <?php if($request->get('zone')=='09') { echo "selected"; } ?> >เขต 09</option>
            <option value="10" zone="10" period="<?php echo $_period; ?>" view="summaryzone" <?php if($request->get('zone')=='10') { echo "selected"; } ?> >เขต 10</option>
            <?php
            }else{
            ?>
            <option value="<?php echo $userprofile[hospital][0][zone_code]; ?>" zone="<?php echo $userprofile[hospital][0][zone_code]; ?>" period="<?php echo $_period; ?>" view="summaryzone" selected >เขต <?php echo $userprofile[hospital][0][zone_code]; ?> </option>
            <?php
            }
            ?>
        </select>
        <select id="selectprovince" name="selectprovince" class="btn-info" style=" position: absolute; margin-left: 350px; margin-top: 0px; width: 135px;">
            <?php if(count($lsProv)>0){ ?>
            <option value="">เลือกจังหวัด ในเขต <?php echo $request->get('zone'); ?></option>
            <?php
            if( $_usergroup==6 ){
                 foreach ($lsProv as $_pvalue ){
                     echo "<option ";
                     echo "value=\"".$_pvalue['provincecode']."\" ";
                     echo "period=\"".$_period."\" ";
                     echo "view=\"summaryprov\" ";
                     echo "zone=\"".$request->get('zone')."\" ";
                     echo "prov=\"".$_pvalue['provincecode']."\" ";
                     if( $request->get('prov')==$_pvalue['provincecode'] ){
                         echo "selected ";
                     }
                     echo ">".$_pvalue['provincecode'].": ".$_pvalue['province']."</option>";
                 }
            }else{
            ?>
            <option value="<?php echo $userprofile[hospital][0][provincecode]; ?>" period="<?php echo $_period; ?>" view="summaryprov" zone="<?php echo $userprofile[hospital][0][zone_code]; ?>" prov="<?php echo $userprofile[hospital][0][provincecode]; ?>" selected><?php echo $userprofile[hospital][0][provincecode]; ?>: <?php echo $userprofile[hospital][0][province]; ?></option>
            <?php
            }
            ?>
            <?php } ?>
        </select>
    
        <select id="selectamphur" name="selectamphur" class="btn-info" style=" position: absolute; margin-left: 490px; margin-top: 0px; width: 160px;">
            <?php if(count($lsAmp)>0){ ?>
            <option value=""> (เลือกอำเภอ) </option>
                <!-- id="bListAmphur" class="btn btn-info" title="แสดงภายในอำเภอ" period="20170301" view="summaryamp" zone="10" prov="34" xax="01" -->
            <?php
                foreach ($lsAmp as $_avalue ){
                    echo "<option ";
                    echo "value=\"".$_avalue['amphurcode']."\" ";
                    echo "period=\"".$_period."\" ";
                    echo "view=\"summaryamp\" ";
                    echo "zone=\"".$request->get('zone')."\" ";
                    echo "prov=\"".$request->get('prov')."\" ";
                    echo "xax=\"".$_avalue['amphurcode']."\" ";
                    if( $request->get('xax')==$_avalue['amphurcode'] ){
                        echo "selected ";
                    }
                    echo ">".$_avalue['amphurcode'].": ".$_avalue['amphur']."</option>";
                }
            ?>
            <?php } ?>
        </select>
    
        <select id="selecthospital" name="selecthospital" class="btn-info" style=" position: absolute; margin-left: 655px; margin-top: 0px; width: 300px;">
            <?php if(count($lsHosp)>0){ ?>
            <option value="">เลือก โรงพยาบาลที่อยู่ในภายในอำเภอ</option>
            <?php
                foreach ($lsHosp as $_hvalue ){
                    echo "<option ";
                    echo "value=\"".$_hvalue['hcode']."\" ";
                    echo "period=\"".$_period."\" ";
                    echo "view=\"summaryhosp\" ";
                    echo "zone=\"".$request->get('zone')."\" ";
                    echo "prov=\"".$request->get('prov')."\" ";
                    echo "xax=\"".$request->get('xax')."\" ";
                    echo "hosp=\"".$_hvalue['hcode']."\" ";
                    if( $request->get('hosp')==$_hvalue['hcode'] ){
                        echo "selected ";
                    }
                    echo ">".$_hvalue['hcode'].": ".$_hvalue['name']."</option>";
                }
            ?>
            <?php } ?>
        </select>
    </p>
    <p></p>
    <p></p>
</div>
<?php
$jsAdd = <<< JS
$('select').on('change', function() {
    //alert($(this).attr("id"));
    //alert($("select option:selected").attr("view"));
    //alert( $(this).find(":selected").attr("view"));
    var period  = $(this).find(":selected").attr("period");
    var view    = $(this).find(":selected").attr("view");
    var zone    = $(this).find(":selected").attr("zone");
    var prov    = $(this).find(":selected").attr("prov");
    var xax     = $(this).find(":selected").attr("xax");
    var hosp    = $(this).find(":selected").attr("hosp");
    
        
    var url     = "/cpay/y60/summary-table?period="+period+"&view="+view+"&zone="+zone+"&prov="+prov+"&xax="+xax+"&hosp="+hosp+"";
    $.get(url, {
        period:period,
        view:view,
        zone:zone,
        prov:prov,
    })
    .done(function( data ) {
        $("#summary_table").html(data);
    });
});


JS;
$this->registerJs($jsAdd);