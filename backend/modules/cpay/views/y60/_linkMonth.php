<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
use backend\modules\cpay\classes\UserCanView;

$request    = Yii::$app->request;


if( 0 ){
    echo "<pre align='left'>";
    print_r($userprofile);
    echo "</pre>";
}

$userid     = Yii::$app->user->identity->id;
$_usergroup     = UserCanView::getUserGroup($userid , $userprofile);

if( strlen($request->get('period'))==0 ){
    $_period        = "20170301";       // initial for current period
}else{
    $_period        = $request->get('period');
}


?>
<button type="button" id="month03" class="btn btn-primary" title=" 25 มีนาคม 2560" period="20170301" >
    <span class="glyphicon glyphicon-saved"></span> มีนาคม
</button>
<?php
//if( 0 ){
if( $_usergroup == 6 ){
?>
<button type="button" id="month04" class="btn btn-primary" title="ตัดข้อมูล 25 มิถุนายน 2560" period="20170625" >
    <span class="glyphicon glyphicon-saved"></span> มิถุนายน
</button>
<?php
}

$jsAdd = <<< JS
      
$(document).on("click","*[id^=month]", function() {
    var period  = $(this).attr("period");
    var url = "/cpay/y60?&period="+period+"&tyear=60&x=1&x2=0";
    window.open(url,'_self');
        
    
})

JS;
$this->registerJs($jsAdd);