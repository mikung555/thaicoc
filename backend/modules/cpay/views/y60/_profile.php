<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$this->registerCss('

');







$userId         = Yii::$app->user->identity->id;
$userUsername   = Yii::$app->user->identity->username;
$fullname       = Yii::$app->user->identity->userProfile->firstname." ".Yii::$app->user->identity->userProfile->lastname;

if( 0 ){
    echo '<pre align="left">';
    print_r($userprofile['hospital'][0]);
    echo '</pre>';
}

?>
<div>
    <div class="row">
        <div class="col-lg-offset-1 col-lg-11">
            	<h4>ข้อมูลส่วนตัว</h>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-offset-2 col-lg-1" style="text-align:right">
            	<h5>หน่วยบริการ:</h>
        </div>
        <div class="col-lg-9 text-primary" style="text-align:left">
            <h5 class="text-primary">
            <?php 
                echo $userprofile['hospital'][0]['hcode'];
                echo ": ";
                echo $userprofile['hospital'][0]['name'];
            ?>
            </h>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-offset-2 col-lg-1" style="text-align:right">
            	<h5>ประเภท:</h>
        </div>
        <div class="col-lg-9 text-primary" style="text-align:left">
            <h5 class="text-primary">
            <?php
                //echo $userprofile['hospital'][0]['code4'];
                echo $userprofile['hospital'][0]['name5'];
            ?>
            </h>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-offset-1 col-lg-2" style="text-align:right">
            	<h5>เข้าสู่ระบบโดย User:</h>
        </div>
        <div class="col-lg-9 text-primary" style="text-align:left">
            <h5 class="text-primary">
            <?php
                echo $userUsername;
            ?>
            </h>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-offset-1 col-lg-2" style="text-align:right">
            	<h5>ชื่อ-สกุล:</h>
        </div>
        <div class="col-lg-9 text-primary" style="text-align:left">
            <h5 class="text-primary">
            <?php
                echo $fullname;
            ?>
            </h>
        </div>
    </div>
    
    
    <div class="row" style="text-align:left">
        <div class="btn-group">
            <button type="button" class="btn btn-link" style="text-align: left; font-size: 30px; color: #000000;">ปีงบประมาณ 2560</button>
            <button type="button" class="btn btn-link dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="font-size: 30px; color: #000000;">
                <span class="caret" ></span>
                <span class="sr-only">Toggle Dropdown</span>
            </button>
            <ul class="dropdown-menu">
                <li><a href="/cpay" style="text-align: left; font-size: 30px; color: #000000;">ปีงบประมาณ 2559</a></li>
                <li><a href="/cpay/y60" style="text-align: left; font-size: 30px; color: #000000;">ปีงบประมาณ 2560</a></li>
                <li role="separator" class="divider"></li>
                <li><a href="/cpay" style="text-align: left; font-size: 30px; color: #000000;">ปีงบประมาณ 2557-2558</a></li>
            </ul>
        </div>
        <button type="button" class="btn btn-info")" onclick='javascript:window.location.href = "/cpay?act1=ver2&act2=unit59";'>
            <span class="glyphicon glyphicon-th-list"></span> ค่าตอบแทนสำหรับหน่วยบริการ
        </button>
        <button type="button" class="btn btn-danger")" onclick='javascript:window.location.href = "/cpay?act1=ver2&act2=informprotocol";'>
            <span class="glyphicon glyphicon-th-list"></span> ขั้นตอนกำดำเนินการเบิกจ่ายค่าตอบแทนสำหรับสมาชิก
        </button>
        <button type="button" class="btn btn-danger")" onclick='javascript:window.location.href = "/cpay?act1=ver2&act2=ruleprotocal58";'>
            <span class="glyphicon glyphicon-th-list"></span> หลักเกณฑ์เงื่อนไข
        </button>
    </div>
</div>

<?php

if( 0 ){
    echo "<pre align='left'>";
    print_r($userprofile);
    echo "</pre>";
}


$jsAdd = <<< JS
function DropDown(el) {
    this.dd = el;
    this.initEvents();
}
        
        
DropDown.prototype = {
    initEvents : function() {
        var obj = this;

        obj.dd.on('click', function(event){
            $(this).toggleClass('active');
            event.stopPropagation();
        }); 
    }
}      
JS;
$this->registerJs($jsAdd);
?>
