<?php
namespace backend\modules\ckdnet\classes;

use Yii;

/**
 * newPHPClass class file UTF-8
 * @author SDII <iencoded@gmail.com>
 * @copyright Copyright &copy; 2015 AppXQ
 * @license http://www.appxq.com/license/
 * @version 1.0.0 Date: 4 พ.ย. 2559 7:53:01
 * @link http://www.appxq.com/
 * @example 
 */
class CkdnetQueryRight {
 ////////////////////////////////////////////// For Hospital Join Central ////////////////////////////////////////      
    private static $CsqlDeath = "`ph`.`death` =0";
//    private static $CsqlTypeareaCKD="`ph`.`type_area` = '1' OR `ph`.`type_area` = '3'";
    private static $CsqlDMHT="(IFNULL(pc.dm,0) >=1) OR ((IFNULL(pc.ht,0)) >=1)";
    
    private static $CsqlCkdReportedCKD  =  "IFNULL(`pc`.reported_ckd,0)=1";
    private static $CsqlCkdReportedCKDNull  =  "`pc`.reported_ckd IS NULL";
    private static $CsqlCkdScreen="IFNULL(`pc`.screened,0) = 1";
    private static $CsqlCkdScreenNon="IFNULL(`pc`.screened,0) = 0 or `pc`.screened is null";
    private static $CsqlConfirmCkdNo="`pc`.ckd_final=1 and `pc`.ckd_final_code=''";
    private static $CsqlConfirmCkd="`pc`.ckd_final=1 and `pc`.ckd_final_code<>''";
    private static $CsqlConfirmCkdWait="(`pc`.ckd_final is null or `pc`.ckd_final=0)"; # ยังไม่สรุปผลใดๆ ทั้งสิ้น
    private static $CsqlCflab = "(`pc`.`cf_lab` =1 or `pc`.`cf_icd` = 1)";
    private static $CsqlCflabNon = "(`pc`.cf_lab is null AND `pc`.cf_icd  is null)";
    private static $CsqlTarget = "`pc`.target = '1'";
    private static $CsqlCkdTarget="IFNULL(`pc`.reported_ckd,0) = 0";
    private static $csqlHd="`pc`.`hd` is not null";
    private static $csqlKt="`pc`.`kt` is not null";
    
 ////////////////////////////////////////////// For Hospital ////////////////////////////////////////   
    private static $sqlDeath = "`ph`.`death` =0";
    private static $sqlDMHT="IFNULL(ph.dm,0) = 1 OR IFNULL(ph.dm,0) = 2 OR IFNULL(ph.ht,0) = 1 OR IFNULL(ph.ht,0) = 2";
    private static $sqlHd="`ph`.`hd` is not null";
    private static $sqlKt="`ph`.`kt` is not null";
    #private static $sqlCkdTarget="target = 1";
    #private static $sqlCkdDMHTReported = "(patient_profile_hospital.dm = 1 or patient_profile_hospital.dm = 2 or patient_profile_hospital.ht = 1) AND patient_profile_hospital.reported_ckd=1";
    private static $sqlCkdReportedCKD  =  "IFNULL(ph.reported_ckd,0)=1";
    private static $sqlCkdReportedCKDNull  =  "ph.reported_ckd IS NULL";
    #private static $sqlCkcDMHTTarget = "(patient_profile_hospital.dm=1 or patient_profile_hospital.dm=2 or patient_profile_hospital.ht) AND (patient_profile_hospital.reported_ckd=0 or patient_profile_hospital.reported_ckd is null)";
    #private static $sqlCkdTarget="patient_profile_hospital.target=1 and (patient_profile_hospital.death_date>='2015-10-01' or patient_profile_hospital.death=0)";
    private static $sqlCkdTarget="IFNULL(ph.reported_ckd,0) = 0";
    #private static $sqlCkdTargetNon="patient_profile_hospital.target = 0 OR patient_profile_hospital.target is null";
    private static $sqlCkdScreen="IFNULL(ph.screened,0) = 1";
    private static $sqlCkdScreenNon="IFNULL(ph.screened,0) = 0 or ph.screened is null";
    //private static $sqlConfirmCkdNo="IFNULL(patient_profile_hospital.cf_lab,0) = 0 OR IFNULL(patient_profile_hospital.cf_doctor,0) = 0 OR IFNULL(patient_profile_hospital.cf_icd,0) = 0 OR IFNULL(patient_profile_hospital.ckd_final,0)=0";
    //private static $sqlConfirmCkd="patient_profile_hospital.cf_lab = 1 OR patient_profile_hospital.cf_doctor = 1 OR patient_profile_hospital.cf_icd = 1 OR patient_profile_hospital.ckd_final=1";
    //private static $sqlConfirmCkdWait="patient_profile_hospital.ckd_final is null or patient_profile_hospital.ckd_final=0";
    private static $sqlConfirmCkdNo="ph.ckd_final=1 and ph.ckd_final_code=''";
    private static $sqlConfirmCkd="ph.ckd_final=1 and ph.ckd_final_code<>''";

    //private static $sqlConfirmCkdWait="patient_profile_hospital.cf_lab is null AND patient_profile_hospital.cf_doctor is null AND patient_profile_hospital.cf_icd is null AND patient_profile_hospital.ckd_final is null "; # ยังไม่สรุปผลใดๆ ทั้งสิ้น
    private static $sqlConfirmCkdWait="(ph.ckd_final is null or ph.ckd_final=0)"; # ยังไม่สรุปผลใดๆ ทั้งสิ้น
    # insufficient ยังไม่ชัว
    private static $sqlTypeareaCKD="`ph`.`type_area` = '1' OR `ph`.`type_area` = '3'";
    private static $sqlCflab = "( `ph`.`cf_lab` =1 or `ph`.`cf_icd` = 1)";
    private static $sqlCflabNon = "(`ph`.cf_lab is null AND `ph`.cf_icd  is null)";
    private static $sqlTarget = "`ph`.target = '1'";
    
    #=========
    private static $field_genSelect = "egfr,cvd_risk,cva_code,cva_date";
    private static $Cen_field_genSelect = "IFNULL(pc.egfr,'') as egfr,IFNULL(pc.cvd_risk,'') as cvd_risk,IFNULL(pc.cva_code,'') as cva_code ,IFNULL(pc.cva_date,'') as cva_date";



    # Begin Left
    public static function getNonDMHTCkdConfirm($sitecode, $sdate, $edate,$idcen,$type, $dp=false) {
        # call CkdnetQueryRight::getNonDMHTCkdConfirm
        # ckd: Non DM/HT -> Reported CKD => Confirmed CKD -> (15)
        # - Non DM/HT -
        # - Reported CKD -
        # - Confirmed CKD -> (15)
        $CsqlDMHT = self::$CsqlDMHT;
        $CsqlCkdReportedCKD = self::$CsqlCkdReportedCKD;
	$CsqlConfirmCkd = self::$CsqlConfirmCkd;
//        $CsqlTypeareaCKD = self::$CsqlTypeareaCKD;
        $CsqlDeath = self::$CsqlDeath;
        
        $sqlDMHT = self::$sqlDMHT;
        $sqlCkdReportedCKD = self::$sqlCkdReportedCKD;
	$sqlConfirmCkd = self::$sqlConfirmCkd;
//        $sqlTypeareaCKD = self::$sqlTypeareaCKD;
        $sqlDeath = self::$sqlDeath;
        $field_genSelect = self::$field_genSelect;
        $Cen_field_genSelect = self::$Cen_field_genSelect; 
        
            if($idcen == 'central'){
            
                 $sql = "SELECT
                            COUNT(*)
                        FROM
                          `patient_profile_hospital` ph
                        LEFT JOIN patient_profile_central pc ON ph.ptlink = pc.ptlink
                        WHERE
                            (".$CsqlDeath.") AND 
                            `hospcode` = :sitecode AND
                            (pc.ptlink <> '' OR ph.ptlink = '') AND
                            `ph`.`type_area` IN ($type) AND    
                             NOT(".$CsqlDMHT.") AND  
                            (".$CsqlCkdReportedCKD.") AND
                            (".$CsqlConfirmCkd.") 
                    ";

                if($dp){
                    $selectKey = CkdnetQuery::genSelect();

                    $sqlDp ="SELECT
                                $selectKey,$Cen_field_genSelect
                            FROM
                                `patient_profile_hospital` ph
                            INNER JOIN `f_person`
                                ON `ph`.`hospcode` = `f_person`.`HOSPCODE`
                                AND `ph`.`pid` = `f_person`.`PID`
                                AND `ph`.`hospcode` = `f_person`.`sitecode`
                            LEFT JOIN patient_profile_central pc ON ph.ptlink = pc.ptlink
                            WHERE
                                (".$CsqlDeath.") AND 
                                ph.`hospcode` = :sitecode AND
                                `f_person`.`sitecode` = :sitecode AND
                                (pc.ptlink <> '' OR ph.ptlink = '') AND
                                `ph`.`type_area` IN ($type) AND
                                NOT(".$CsqlDMHT.") AND  
                                (".$CsqlCkdReportedCKD.") AND
                                (".$CsqlConfirmCkd.") ";

                    return [
                        'sql' =>$sqlDp,
                        'param'=>[':sitecode'=>$sitecode],
                        'total'=>CkdnetFunc::queryScalar($sql, [':sitecode'=>$sitecode]),
                    ];
                }
                
        }else{
        
                $sql = "SELECT
                            COUNT(*)
                        FROM
                            `patient_profile_hospital` ph
                        WHERE
                            (".$sqlDeath.") AND 
                            `hospcode` = :sitecode AND
                            Not (".$sqlDMHT.") AND
                            (".$sqlCkdReportedCKD.") AND
                            (".$sqlConfirmCkd.") AND
                            `ph`.`type_area` IN ($type)   
                        ";

                if($dp){
                    $selectKey = CkdnetQuery::genSelect();

                    $sqlDp = "SELECT
                            $selectKey,$field_genSelect
                        FROM
                            `patient_profile_hospital` ph
                            INNER JOIN `f_person`
                            ON `ph`.`hospcode` = `f_person`.`HOSPCODE`
                            AND `ph`.`pid` = `f_person`.`PID`
                        WHERE
                            (".$sqlDeath.") AND 
                            `ph`.`hospcode` = :sitecode AND
                            `f_person`.`sitecode` = :sitecode AND
                            Not (".$sqlDMHT.") AND
                            (".$sqlCkdReportedCKD.") AND
                            (".$sqlConfirmCkd.") AND
                            `ph`.`type_area` IN ($type)

                        ";

                    return [
                        'sql' =>$sqlDp,
                        'param'=>[':sitecode'=>$sitecode],

                        'total'=>CkdnetFunc::queryScalar($sql, [':sitecode'=>$sitecode]),
                    ];
                }
        }
	return CkdnetFunc::queryScalar($sql, [':sitecode'=>$sitecode]);//':sdate' => $sdate, ':edate' => $edate, 
    }
    
    
    # End Left
    
    # call CkdnetQueryRight::getCkdScreenedConfirmCkd
    #==============================#
    # Begin Added Rigth
    
    public static function getCkdNonDMHT($sitecode, $sdate, $edate,$idcen,$type, $dp=false) {
    
        # ckd: Non DM/HT -> (13)
        # - Non DM/HT - (13)
        $CsqlDMHT = self::$CsqlDMHT;
        $CsqlCkdReportedCKD = self::$CsqlCkdReportedCKD;
//        $CsqlTypeareaCKD = self::$CsqlTypeareaCKD;
        $CsqlDeath = self::$CsqlDeath;
        
        $sqlDMHT = self::$sqlDMHT;
        $sqlCkdReportedCKD = self::$sqlCkdReportedCKD;
//        $sqlTypeareaCKD = self::$sqlTypeareaCKD;
        $sqlDeath = self::$sqlDeath;
        $field_genSelect = self::$field_genSelect;
        $Cen_field_genSelect = self::$Cen_field_genSelect;
        
            
           if($idcen== 'central'){
            
                 $sql = "SELECT
                            COUNT(*)
                        FROM
                          `patient_profile_hospital` ph
                        LEFT JOIN patient_profile_central pc ON ph.ptlink = pc.ptlink
                        WHERE
                            (".$CsqlDeath.") AND 
                            `hospcode` = :sitecode AND
                            (pc.ptlink <> '' OR ph.ptlink = '') AND
                            `ph`.`type_area` IN ($type) AND 
                            Not (".$CsqlDMHT.") 

                    ";

                if($dp){
                    $selectKey = CkdnetQuery::genSelect();

                    $sqlDp =   "SELECT
                                    $selectKey,$Cen_field_genSelect
                                FROM
                                    `patient_profile_hospital` ph
                                INNER JOIN `f_person`
                                    ON `ph`.`hospcode` = `f_person`.`HOSPCODE`
                                    AND `ph`.`pid` = `f_person`.`PID`
                                    AND `ph`.`hospcode` = `f_person`.`sitecode`
                                LEFT JOIN patient_profile_central pc ON ph.ptlink = pc.ptlink
                                WHERE
                                    (".$CsqlDeath.") AND 
                                    ph.`hospcode` = :sitecode AND
                                     `f_person`.`sitecode` = :sitecode AND
                                    (pc.ptlink <> '' OR ph.ptlink = '') AND
                                    `ph`.`type_area` IN ($type) AND 
                                    Not (".$CsqlDMHT.") 

                        ";

                    return [
                        'sql' =>$sqlDp,
                        'param'=>[':sitecode'=>$sitecode],
                        'total'=>CkdnetFunc::queryScalar($sql, [':sitecode'=>$sitecode]),
                    ];
                }
                
        }else{
        
                    $sql = "SELECT
                                COUNT(*)
                            FROM
                                `patient_profile_hospital` ph
                            WHERE
                                (".$sqlDeath.") AND 
                                `hospcode` = :sitecode AND
                                Not (".$sqlDMHT.") AND
                                `ph`.`type_area` IN ($type)    
                            ";

                    if($dp){
                        $selectKey = CkdnetQuery::genSelect();

                        $sqlDp = "SELECT
                                $selectKey,$field_genSelect
                            FROM
                                `patient_profile_hospital` ph
                                INNER JOIN `f_person`
                                ON `ph`.`hospcode` = `f_person`.`HOSPCODE`
                                AND `ph`.`pid` = `f_person`.`PID`
                            WHERE
                                (".$sqlDeath.") AND 
                                `ph`.`hospcode` = :sitecode AND
                                `f_person`.`sitecode` = :sitecode AND
                                Not (".$sqlDMHT.") AND
                                `ph`.`type_area` IN ($type)
                            ";

                        return [
                            'sql' =>$sqlDp,
                            'param'=>[':sitecode'=>$sitecode],

                            'total'=>CkdnetFunc::queryScalar($sql, [':sitecode'=>$sitecode]),
                        ];
                    }
        }
	return CkdnetFunc::queryScalar($sql, [':sitecode'=>$sitecode]);//':sdate' => $sdate, ':edate' => $edate, 
    }
    
    public static function getCkdNonDMHTReportedCKD($sitecode, $sdate, $edate,$idcen,$type, $dp=false) {
        # ckd: Non DM/HT => Reported CKD -> (14)
        # - Non DM/HT -
        # - Reported CKD -
        $CsqlDMHT = self::$CsqlDMHT;
        $CsqlCkdReportedCKD = self::$CsqlCkdReportedCKD;
//        $CsqlTypeareaCKD = self::$CsqlTypeareaCKD;
        $CsqlDeath = self::$CsqlDeath;
        
        $sqlDMHT = self::$sqlDMHT;
        $sqlCkdReportedCKD = self::$sqlCkdReportedCKD;
//        $sqlTypeareaCKD = self::$sqlTypeareaCKD;
        $sqlDeath = self::$sqlDeath;
        $field_genSelect = self::$field_genSelect;
        $Cen_field_genSelect = self::$Cen_field_genSelect;
        
        if($idcen== 'central'){
            
                 $sql = "SELECT
                            COUNT(*)
                        FROM
                          `patient_profile_hospital` ph
                        LEFT JOIN patient_profile_central pc ON ph.ptlink = pc.ptlink
                        WHERE
                              (".$CsqlDeath.") AND 
                              `hospcode` = :sitecode AND
                              (pc.ptlink <> '' OR ph.ptlink = '') AND
                              `ph`.`type_area` IN ($type) AND 
                              NOT(".$CsqlDMHT.") AND  
                              (".$CsqlCkdReportedCKD.") ";

                if($dp){
                    $selectKey = CkdnetQuery::genSelect();

                    $sqlDp =   "SELECT
                                    $selectKey,$Cen_field_genSelect
                                FROM
                                    `patient_profile_hospital` ph
                                INNER JOIN `f_person`
                                    ON `ph`.`hospcode` = `f_person`.`HOSPCODE`
                                    AND `ph`.`pid` = `f_person`.`PID`
                                    AND `ph`.`hospcode` = `f_person`.`sitecode`
                                LEFT JOIN patient_profile_central pc ON ph.ptlink = pc.ptlink
                                WHERE
                                    (".$CsqlDeath.") AND 
                                    ph.`hospcode` = :sitecode AND
                                     `f_person`.`sitecode` = :sitecode AND
                                    (pc.ptlink <> '' OR ph.ptlink = '') AND
                                    `ph`.`type_area` IN ($type) AND
                                    NOT(".$CsqlDMHT.") AND  
                                    (".$CsqlCkdReportedCKD.")

                        ";

                    return [
                        'sql' =>$sqlDp,
                        'param'=>[':sitecode'=>$sitecode],
                        'total'=>CkdnetFunc::queryScalar($sql, [':sitecode'=>$sitecode]),
                    ];
                }
                
        }else{
                $sql = "SELECT
                            COUNT(*)
                        FROM
                            `patient_profile_hospital` ph
                        WHERE
                            (".$sqlDeath.") AND 
                            `hospcode` = :sitecode AND
                            Not (".$sqlDMHT.") AND
                            (".$sqlCkdReportedCKD.") AND
                            `ph`.`type_area` IN ($type)
                        ";

                if($dp){
                    $selectKey = CkdnetQuery::genSelect();

                    $sqlDp = "SELECT
                            $selectKey,$field_genSelect
                        FROM
                            `patient_profile_hospital` ph
                            INNER JOIN `f_person`
                            ON `ph`.`hospcode` = `f_person`.`HOSPCODE`
                            AND `ph`.`pid` = `f_person`.`PID`
                        WHERE
                            (".$sqlDeath.") AND 
                            `ph`.`hospcode` = :sitecode AND
                            `f_person`.`sitecode` = :sitecode AND
                            Not (".$sqlDMHT.") AND
                            (".$sqlCkdReportedCKD.") AND
                            `ph`.`type_area` IN ($type)
                        ";

                    return [
                        'sql' =>$sqlDp,
                        'param'=>[':sitecode'=>$sitecode],

                        'total'=>CkdnetFunc::queryScalar($sql, [':sitecode'=>$sitecode]),
                    ];
                }
        }
	return CkdnetFunc::queryScalar($sql, [':sitecode'=>$sitecode]);//':sdate' => $sdate, ':edate' => $edate, 
    }
    
    public static function getCkdReport($sitecode, $sdate, $edate,$idcen,$type, $dp=false) {
        # ckd: DM/HT => Reported CKD -> (3)
        # - DM/HT -
        # - Reported CKD - (3)
        $CsqlDMHT = self::$CsqlDMHT;
        $CsqlCkdReportedCKD = self::$CsqlCkdReportedCKD;
//        $CsqlTypeareaCKD = self::$CsqlTypeareaCKD;
        $CsqlDeath = self::$CsqlDeath;
        
        $sqlDMHT = self::$sqlDMHT;
        $sqlCkdReportedCKD = self::$sqlCkdReportedCKD;
//        $sqlTypeareaCKD = self::$sqlTypeareaCKD;
        $sqlDeath = self::$sqlDeath;
        $field_genSelect = self::$field_genSelect;
        $Cen_field_genSelect = self::$Cen_field_genSelect;
        
         if($idcen== 'central'){
            
                 $sql = "SELECT
                            COUNT(*)
                        FROM
                          `patient_profile_hospital` ph
                        LEFT JOIN patient_profile_central pc ON ph.ptlink = pc.ptlink
                        WHERE
                            (".$CsqlDeath.") AND 
                            `hospcode` = :sitecode AND
                            (pc.ptlink <> '' OR ph.ptlink = '') AND
                            `ph`.`type_area` IN ($type) AND 
                            (".$CsqlDMHT.") AND
                            (".$CsqlCkdReportedCKD.") 
                    ";

                if($dp){
                    $selectKey = CkdnetQuery::genSelect();

                    $sqlDp =   "SELECT
                                    $selectKey,$Cen_field_genSelect
                                FROM
                                    `patient_profile_hospital` ph
                                INNER JOIN `f_person`
                                    ON `ph`.`hospcode` = `f_person`.`HOSPCODE`
                                    AND `ph`.`pid` = `f_person`.`PID`
                                    AND `ph`.`hospcode` = `f_person`.`sitecode`
                                LEFT JOIN patient_profile_central pc ON ph.ptlink = pc.ptlink
                                WHERE
                                    (".$CsqlDeath.") AND 
                                    ph.`hospcode` = :sitecode AND
                                     `f_person`.`sitecode` = :sitecode AND
                                    (pc.ptlink <> '' OR ph.ptlink = '') AND
                                    `ph`.`type_area` IN ($type) AND
                                    (".$CsqlDMHT.") AND
                                    (".$CsqlCkdReportedCKD.") 
                        ";

                    return [
                        'sql' =>$sqlDp,
                        'param'=>[':sitecode'=>$sitecode],
                        'total'=>CkdnetFunc::queryScalar($sql, [':sitecode'=>$sitecode]),
                    ];
                }
                
        }else{
	$sql = "SELECT
                    COUNT(*)
                FROM
                    `patient_profile_hospital` ph
                WHERE
                    (".$sqlDeath.") AND 
                    `hospcode` = :sitecode AND
                    (".$sqlDMHT.") AND
                    (".$sqlCkdReportedCKD.") AND
                    `ph`.`type_area` IN ($type)
                ";
        
        if($dp){
            $selectKey = CkdnetQuery::genSelect();
            
            $sqlDp = "SELECT
                    $selectKey,$field_genSelect
                FROM
                    `patient_profile_hospital` ph
                    INNER JOIN `f_person`
                    ON `ph`.`hospcode` = `f_person`.`HOSPCODE`
                    AND `ph`.`pid` = `f_person`.`PID`
                WHERE
                    (".$sqlDeath.") AND 
                    `ph`.`hospcode` = :sitecode AND
                    `f_person`.`sitecode` = :sitecode AND
                    (".$sqlDMHT.") AND
                    (".$sqlCkdReportedCKD.") AND
                    `ph`.`type_area` IN ($type)
                ";
            
            return [
                'sql' =>$sqlDp,
                'param'=>[':sitecode'=>$sitecode],
                'total'=>CkdnetFunc::queryScalar($sql, [':sitecode'=>$sitecode]),

            ];
        }
        }    
	return CkdnetFunc::queryScalar($sql, [':sitecode'=>$sitecode]);//':sdate' => $sdate, ':edate' => $edate, 
    }
    
    public static function getCkdReportConfirm($sitecode, $sdate, $edate,$idcen,$type, $dp=false) {
        # ckd: DM/HT -> Reported CKD  => Confirmed CKD -> (16)
        # - DM/HT -
        # - Reported CKD  -
        # - Confirmed CKD - (16)
        $CsqlDMHT = self::$CsqlDMHT;
        $CsqlCkdReportedCKD = self::$CsqlCkdReportedCKD;
	$CsqlConfirmCkd = self::$CsqlConfirmCkd;
//        $CsqlTypeareaCKD = self::$CsqlTypeareaCKD;
        $CsqlDeath = self::$CsqlDeath;
        
        $sqlDMHT = self::$sqlDMHT;
        $sqlCkdReportedCKD = self::$sqlCkdReportedCKD;
	$sqlConfirmCkd = self::$sqlConfirmCkd;
//        $sqlTypeareaCKD = self::$sqlTypeareaCKD;
        $sqlDeath = self::$sqlDeath;
        $field_genSelect = self::$field_genSelect;
        $Cen_field_genSelect = self::$Cen_field_genSelect;
        
        if($idcen== 'central'){
            
                 $sql = "SELECT
                            COUNT(*)
                        FROM
                          `patient_profile_hospital` ph
                        LEFT JOIN patient_profile_central pc ON ph.ptlink = pc.ptlink
                        WHERE
                            (".$CsqlDeath.") AND 
                            `hospcode` = :sitecode AND
                            (pc.ptlink <> '' OR ph.ptlink = '') AND
                            `ph`.`type_area` IN ($type) AND 
                            (".$CsqlDMHT.") AND  
                            (".$CsqlCkdReportedCKD.") AND
                            (".$CsqlConfirmCkd.")
                    ";

                if($dp){
                    $selectKey = CkdnetQuery::genSelect();

                    $sqlDp =   "SELECT
                                    $selectKey,$Cen_field_genSelect
                                FROM
                                    `patient_profile_hospital` ph
                                INNER JOIN `f_person`
                                    ON `ph`.`hospcode` = `f_person`.`HOSPCODE`
                                    AND `ph`.`pid` = `f_person`.`PID`
                                    AND `ph`.`hospcode` = `f_person`.`sitecode`
                                LEFT JOIN patient_profile_central pc ON ph.ptlink = pc.ptlink
                                WHERE
                                    (".$CsqlDeath.") AND 
                                    ph.`hospcode` = :sitecode AND
                                     `f_person`.`sitecode` = :sitecode AND
                                    (pc.ptlink <> '' OR ph.ptlink = '') AND
                                    `ph`.`type_area` IN ($type) AND
                                    (".$CsqlDMHT.") AND  
                                    (".$CsqlCkdReportedCKD.") AND
                                    (".$CsqlConfirmCkd.")

                        ";

                    return [
                        'sql' =>$sqlDp,
                        'param'=>[':sitecode'=>$sitecode],
                        'total'=>CkdnetFunc::queryScalar($sql, [':sitecode'=>$sitecode]),
                    ];
                }
                
        }else{
        
                $sql = "SELECT
                            COUNT(*)
                        FROM
                            `patient_profile_hospital` ph
                        WHERE
                            (".$sqlDeath.") AND 
                            `hospcode` = :sitecode AND
                            (".$sqlDMHT.") AND
                            (".$sqlCkdReportedCKD.") AND
                            (".$sqlConfirmCkd.") AND
                            `ph`.`type_area` IN ($type)   
                        ";

                if($dp){
                    $selectKey = CkdnetQuery::genSelect();

                    $sqlDp = "SELECT
                            $selectKey,$field_genSelect
                        FROM
                            `patient_profile_hospital` ph
                            INNER JOIN `f_person`
                            ON `ph`.`hospcode` = `f_person`.`HOSPCODE`
                            AND `ph`.`pid` = `f_person`.`PID`
                        WHERE
                            (".$sqlDeath.") AND 
                            `ph`.`hospcode` = :sitecode AND
                            `f_person`.`sitecode` = :sitecode AND
                            (".$sqlDMHT.") AND
                            (".$sqlCkdReportedCKD.") AND
                            (".$sqlConfirmCkd.") AND
                            `ph`.`type_area` IN ($type)
                        ";

                    return [
                        'sql' =>$sqlDp,
                        'param'=>[':sitecode'=>$sitecode],

                        'total'=>CkdnetFunc::queryScalar($sql, [':sitecode'=>$sitecode]),
                    ];
                }
        }
	return CkdnetFunc::queryScalar($sql, [':sitecode'=>$sitecode]);//':sdate' => $sdate, ':edate' => $edate, 
    }
    
    public static function getCkdTotalConfirmCKD($sitecode, $sdate, $edate,$idcen,$type, $dp=false) {
        # ckd: Non DM/HT + DM/HT -> Reported CKD  => Confirmed CKD -> (17)
        # - Non DM/HT + DM/HT -
        # - Reported CKD  -
        # - Confirmed CKD - 
        # - Total Confirmed CKD -> (17)
        $CsqlDMHT = self::$CsqlDMHT;
        $CsqlCkdReportedCKD = self::$CsqlCkdReportedCKD;
	$CsqlConfirmCkd = self::$CsqlConfirmCkd;
//        $CsqlTypeareaCKD = self::$CsqlTypeareaCKD;
        $CsqlDeath = self::$CsqlDeath;
        
        $sqlDMHT = self::$sqlDMHT;
        $sqlCkdReportedCKD = self::$sqlCkdReportedCKD;
	$sqlConfirmCkd = self::$sqlConfirmCkd;
//        $sqlTypeareaCKD = self::$sqlTypeareaCKD;
        $sqlDeath = self::$sqlDeath;
        $field_genSelect = self::$field_genSelect;
        $Cen_field_genSelect = self::$Cen_field_genSelect;
        
         if($idcen== 'central'){
            
                 $sql = "SELECT
                            COUNT(*)
                        FROM
                          `patient_profile_hospital` ph
                        LEFT JOIN patient_profile_central pc ON ph.ptlink = pc.ptlink
                        WHERE
                            (".$CsqlDeath.") AND 
                            `hospcode` = :sitecode AND
                            (pc.ptlink <> '' OR ph.ptlink = '') AND
                            `ph`.`type_area` IN ($type) AND  
                            (".$CsqlCkdReportedCKD.") AND
                            (".$CsqlConfirmCkd.") 
                    ";

                if($dp){
                    $selectKey = CkdnetQuery::genSelect();

                    $sqlDp =   "SELECT
                                    $selectKey,$Cen_field_genSelect
                                FROM
                                    `patient_profile_hospital` ph
                                INNER JOIN `f_person`
                                    ON `ph`.`hospcode` = `f_person`.`HOSPCODE`
                                    AND `ph`.`pid` = `f_person`.`PID`
                                    AND `ph`.`hospcode` = `f_person`.`sitecode`
                                LEFT JOIN patient_profile_central pc ON ph.ptlink = pc.ptlink
                                WHERE
                                    (".$CsqlDeath.") AND 
                                    ph.`hospcode` = :sitecode AND
                                     `f_person`.`sitecode` = :sitecode AND
                                    (pc.ptlink <> '' OR ph.ptlink = '') AND
                                    `ph`.`type_area` IN ($type) AND
                                    (".$CsqlCkdReportedCKD.") AND
                                    (".$CsqlConfirmCkd.") 

                        ";

                    return [
                        'sql' =>$sqlDp,
                        'param'=>[':sitecode'=>$sitecode],
                        'total'=>CkdnetFunc::queryScalar($sql, [':sitecode'=>$sitecode]),
                    ];
                }
                
        }else{
        
                $sql = "SELECT
                            COUNT(*)
                        FROM
                            `patient_profile_hospital` ph
                        WHERE
                            (".$sqlDeath.") AND 
                            `hospcode` = :sitecode AND
                            (".$sqlCkdReportedCKD.") AND
                            (".$sqlConfirmCkd.") AND
                            `ph`.`type_area` IN ($type)    
                        ";

                if($dp){
                    $selectKey = CkdnetQuery::genSelect();

                    $sqlDp = "SELECT
                            $selectKey,$field_genSelect
                        FROM
                            `patient_profile_hospital` ph
                            INNER JOIN `f_person`
                            ON `ph`.`hospcode` = `f_person`.`HOSPCODE`
                            AND `ph`.`pid` = `f_person`.`PID`
                        WHERE
                            (".$sqlDeath.") AND 
                            `ph`.`hospcode` = :sitecode AND
                            `f_person`.`sitecode` = :sitecode AND
                            (".$sqlCkdReportedCKD.") AND
                            (".$sqlConfirmCkd.") AND
                            `ph`.`type_area` IN ($type)  
                        ";

                    return [
                        'sql' =>$sqlDp,
                        'param'=>[':sitecode'=>$sitecode],

                        'total'=>CkdnetFunc::queryScalar($sql, [':sitecode'=>$sitecode]),
                    ];
                }
        }
	return CkdnetFunc::queryScalar($sql, [':sitecode'=>$sitecode]);//':sdate' => $sdate, ':edate' => $edate, 
    }
    
    public static function getCkdTotalAllConfirmCKD($sitecode, $sdate, $edate,$idcen,$type, $dp=false) {
        # ckd: Non DM/HT + DM/HT -> Reported CKD  + Target  => Confirmed CKD -> (18)
        # - Non DM/HT + DM/HT -
        # - Reported CKD + Target  -
        # - Confirmed CKD - 
        # - Total Confirmed CKD -> (18)
        $CsqlConfirmCkd = self::$CsqlConfirmCkd;
//        $CsqlTypeareaCKD = self::$CsqlTypeareaCKD;
        $CsqlDeath = self::$CsqlDeath;
        
	$sqlConfirmCkd = self::$sqlConfirmCkd;
//        $sqlTypeareaCKD = self::$sqlTypeareaCKD;
        $sqlDeath = self::$sqlDeath;
        $field_genSelect = self::$field_genSelect;
        $Cen_field_genSelect = self::$Cen_field_genSelect;
        
            if($idcen== 'central'){
            
                 $sql = "SELECT
                            COUNT(*)
                        FROM
                          `patient_profile_hospital` ph
                        LEFT JOIN patient_profile_central pc ON ph.ptlink = pc.ptlink
                        WHERE
                            (".$CsqlDeath.") AND 
                            `hospcode` = :sitecode AND
                            (pc.ptlink <> '' OR ph.ptlink = '') AND
                            `ph`.`type_area` IN ($type) AND 
                            (".$CsqlConfirmCkd.") 
                    ";

                if($dp){
                    $selectKey = CkdnetQuery::genSelect();

                    $sqlDp =   "SELECT
                                    $selectKey,$Cen_field_genSelect
                                FROM
                                    `patient_profile_hospital` ph
                                INNER JOIN `f_person`
                                    ON `ph`.`hospcode` = `f_person`.`HOSPCODE`
                                    AND `ph`.`pid` = `f_person`.`PID`
                                    AND `ph`.`hospcode` = `f_person`.`sitecode`
                                LEFT JOIN patient_profile_central pc ON ph.ptlink = pc.ptlink
                                WHERE
                                      (".$CsqlDeath.") AND 
                                      ph.`hospcode` = :sitecode AND
                                       `f_person`.`sitecode` = :sitecode AND
                                      (pc.ptlink <> '' OR ph.ptlink = '') AND
                                      `ph`.`type_area` IN ($type) AND 
                                      (".$CsqlConfirmCkd.") 

                        ";

                    return [
                        'sql' =>$sqlDp,
                        'param'=>[':sitecode'=>$sitecode],
                        'total'=>CkdnetFunc::queryScalar($sql, [':sitecode'=>$sitecode]),
                    ];
                }
                
        }else{
            $sql = "SELECT
                        COUNT(*)
                    FROM
                        `patient_profile_hospital` ph
                    WHERE
                        (".$sqlDeath.") AND 
                        `hospcode` = :sitecode AND
                        (".$sqlConfirmCkd.") AND
                        `ph`.`type_area` IN ($type)
                    ";

            if($dp){
                $selectKey = CkdnetQuery::genSelect();

                $sqlDp = "SELECT
                        $selectKey,$field_genSelect
                    FROM
                        `patient_profile_hospital` ph
                        INNER JOIN `f_person`
                        ON `ph`.`hospcode` = `f_person`.`HOSPCODE`
                        AND `ph`.`pid` = `f_person`.`PID`
                    WHERE
                        (".$sqlDeath.") AND 
                        `ph`.`hospcode` = :sitecode AND
                        `f_person`.`sitecode` = :sitecode AND
                        (".$sqlConfirmCkd.") AND
                        `ph`.`type_area` IN ($type)
                    ";

                return [
                    'sql' =>$sqlDp,
                    'param'=>[':sitecode'=>$sitecode],

                    'total'=>CkdnetFunc::queryScalar($sql, [':sitecode'=>$sitecode]),
                ];
            }   
        }
	return CkdnetFunc::queryScalar($sql, [':sitecode'=>$sitecode]);//':sdate' => $sdate, ':edate' => $edate, 
    }
    #Stage
    public static function  getCkdStage($sitecode, $sdate, $edate,$idcen,$type,$state,$dp=false) {
 
        # - Stage -> (19)  
        $CsqlConfirmCkd = self::$CsqlConfirmCkd;
//        $CsqlTypeareaCKD = self::$CsqlTypeareaCKD;
        $CsqlDeath = self::$CsqlDeath;
        //$nstate = "";
        $sqlDeath = self::$sqlDeath;
        $field_genSelect = self::$field_genSelect;
         $Cen_field_genSelect = self::$Cen_field_genSelect;
//        \appxq\sdii\utils\VarDumper::dump($state);
     
        if(!empty($state)){
            //"OK";exit();
            switch($state){
                case "19.1":$nstate = 'N181'; break;
                case "19.2":$nstate = 'N182'; break;
                case "19.3":$nstate = 'N183'; break;
                case "19.4":$nstate = 'N184'; break;
                case "19.5":$nstate = 'N185'; break;
                default : $nstate = ""; break;
            }    
        }

       
        if($idcen== 'central'){
            
            $sql = "SELECT 
                    IFNULL(SUM(`pc`.ckd_final_code='N181'),0) as `N181`,
                    IFNULL(SUM(`pc`.ckd_final_code='N182'),0) as `N182`,
                    IFNULL(SUM(`pc`.ckd_final_code='N183'),0) as `N183`,
                    IFNULL(SUM(`pc`.ckd_final_code='N184'),0) as `N184`,
                    IFNULL(SUM(`pc`.ckd_final_code='N185'),0) as `N185`,
                    IFNULL(SUM(`pc`.ckd_final_code='N189'),0) as `N189`
                FROM
                `patient_profile_hospital` ph
                LEFT JOIN patient_profile_central pc ON ph.ptlink = pc.ptlink
                WHERE
                    (".$CsqlDeath.") AND 
                    `hospcode` = :sitecode AND
                    (pc.ptlink <> '' OR ph.ptlink = '') AND
                    `ph`.`type_area`IN ($type) AND 
                    (".$CsqlConfirmCkd.")";
            $sqltotal = "SELECT COUNT(*)
                FROM
                    `patient_profile_hospital` ph
                    LEFT JOIN patient_profile_central pc ON ph.ptlink = pc.ptlink
                    INNER JOIN `f_person`
                    ON `ph`.`hospcode` = `f_person`.`HOSPCODE`
                    AND `ph`.`pid` = `f_person`.`PID`
                WHERE
                    (".$CsqlDeath.") AND 
                    `ph`.`hospcode` = :sitecode AND
                    (pc.ptlink <> '' OR ph.ptlink = '') AND
                    `ph`.`type_area`IN ($type) AND 
                    (".$CsqlConfirmCkd.") AND
                    `ph`.`ckd_final_code` = '$nstate'                
                    ";
        if($dp){
                $selectKey = CkdnetQuery::genSelect();

                $sqlDp = "SELECT
                            $selectKey,$Cen_field_genSelect
                          FROM
                            `patient_profile_hospital` ph
                            LEFT JOIN patient_profile_central pc ON ph.ptlink = pc.ptlink
                            INNER JOIN `f_person`
                            ON `ph`.`hospcode` = `f_person`.`HOSPCODE`
                            AND `ph`.`pid` = `f_person`.`PID`
                          WHERE
                            (".$CsqlDeath.") AND 
                            `ph`.`hospcode` = :sitecode AND
                            (pc.ptlink <> '' OR ph.ptlink = '') AND
                            `ph`.`type_area`IN ($type) AND 
                            (".$CsqlConfirmCkd.") AND
                            `ph`.`ckd_final_code` = '$nstate'                
                            ";
//                \appxq\sdii\utils\VarDumper::dump($sqlDp);
                return [
                    'sql' =>$sqlDp,
                    'param'=>[':sitecode'=>$sitecode],

                    'total'=>CkdnetFunc::queryScalar($sqltotal, [':sitecode'=>$sitecode]),
                ];
            }
            
           
            
                
        }else{
        
            $sql = "SELECT
                    IFNULL(SUM(ckd_final_code='N181'),0) as `N181`,
                    IFNULL(SUM(ckd_final_code='N182'),0) as `N182`,
                    IFNULL(SUM(ckd_final_code='N183'),0) as `N183`,
                    IFNULL(SUM(ckd_final_code='N184'),0) as `N184`,
                    IFNULL(SUM(ckd_final_code='N185'),0) as `N185`,
                    IFNULL(SUM(ckd_final_code='N189'),0) as `N189`
                FROM
                `patient_profile_hospital` ph

                WHERE
                (".$sqlDeath.") AND
                `ph`.`HOSPCODE` = :sitecode AND 
                `ph`.`ckd_final`= '1' AND
                `ph`.`ckd_final_code` <> '0' AND
                `ph`.`type_area`IN ($type)              
            ";
            
            if($dp){
                $selectKey = CkdnetQuery::genSelect();

                $sqlDp = "SELECT
                        $selectKey,$field_genSelect
                    FROM
                        `patient_profile_hospital` ph
                        INNER JOIN `f_person`
                        ON `ph`.`hospcode` = `f_person`.`HOSPCODE`
                        AND `ph`.`pid` = `f_person`.`PID`
                    WHERE
                (".$sqlDeath.") AND
                `ph`.`HOSPCODE` = :sitecode AND 
                `ph`.`ckd_final`= '1' AND
                `ph`.`ckd_final_code` <> '0' AND
                `ph`.`type_area`IN ($type) AND
                 `ph`.`ckd_final_code` = '$nstate'
                    
                    ";
                $sqltotal = "SELECT
                        count(*)
                    FROM
                        `patient_profile_hospital` ph
                        INNER JOIN `f_person`
                        ON `ph`.`hospcode` = `f_person`.`HOSPCODE`
                        AND `ph`.`pid` = `f_person`.`PID`
                    WHERE
                (".$sqlDeath.") AND
                `ph`.`HOSPCODE` = :sitecode AND 
                `ph`.`ckd_final`= '1' AND
                `ph`.`ckd_final_code` <> '0' AND
                `ph`.`type_area`IN ($type) AND
                 `ph`.`ckd_final_code` = '$nstate'
                    
                    ";

                return [
                    'sql' =>$sqlDp,
                    'param'=>[':sitecode'=>$sitecode],

                    'total'=>CkdnetFunc::queryScalar($sqltotal, [':sitecode'=>$sitecode]),
                ];
            }
        
        }
            return  $value = CkdnetFunc::queryAll($sql, [':sitecode' => $sitecode]);

    }
    
    public static function  getCkdPd($sitecode, $sdate, $edate) {
        # - Stage -> (20)   
        $sql = "SELECT COUNT(*) as pd from tbdata_1484589286074665600
        where sitecode = :sitecode  and rstat in (1,2)";
        
            return  $value = Yii::$app->db->createCommand($sql)->bindValues([':sitecode' => $sitecode])->queryScalar();

    }
    
    public static function  getCkdHd($sitecode, $sdate, $edate,$idcen,$type,$dp=false) {
        # - Stage -> (20.2)
        
//           \appxq\sdii\utils\VarDumper::dump($type);
        
        $sqlHd = self::$sqlHd;
        $csqlHd = self::$csqlHd;
        $sqlDeath = self::$sqlDeath;
        $CsqlDeath = self::$CsqlDeath;
        $field_genSelect = self::$field_genSelect;
        $Cen_field_genSelect = self::$Cen_field_genSelect;
        
        if($idcen== 'central'){
            
            $sql = "SELECT
                        COUNT(*)
                    FROM
                        `patient_profile_hospital` ph
                        LEFT JOIN patient_profile_central pc ON ph.ptlink = pc.ptlink
                    WHERE
                        (".$CsqlDeath.") AND 
                        `ph`.`hospcode` = :sitecode AND
                        (pc.ptlink <> '' OR ph.ptlink = '') AND
                        `ph`.`type_area`IN ($type) AND 
                        (".$csqlHd.")";
            if($dp){
                $selectKey = CkdnetQuery::genSelect();

                $sqlDp= "SELECT
                            $selectKey,$Cen_field_genSelect
                        FROM
                            `patient_profile_hospital` ph
                            LEFT JOIN patient_profile_central pc ON ph.ptlink = pc.ptlink
                            INNER JOIN `f_person`
                            ON `ph`.`hospcode` = `f_person`.`HOSPCODE`
                            AND `ph`.`pid` = `f_person`.`PID`
                        WHERE
                            (".$CsqlDeath.") AND 
                            `ph`.`hospcode` = :sitecode AND
                            (pc.ptlink <> '' OR ph.ptlink = '') AND
                            `ph`.`type_area`IN ($type) AND 
                            (".$csqlHd.")";
//                \appxq\sdii\utils\VarDumper::dump($sqlDp);
                return [
                    'sql' =>$sqlDp,
                    'param'=>[':sitecode'=>$sitecode],

                    'total'=>CkdnetFunc::queryScalar($sql, [':sitecode'=>$sitecode]),
                ];
            }           
                                
            
        } else{
            $selectKey = CkdnetQuery::genSelect();
            
            $sql = "SELECT
                        COUNT(*)
                    FROM
                        `patient_profile_hospital` ph
                    WHERE
                        (".$sqlDeath.") AND 
                        `hospcode` = :sitecode AND
                        (".$sqlHd.") AND
                        `ph`.`type_area` IN ($type)
                        ";
            $sqlDp = "SELECT
                        $selectKey,$field_genSelect
                    FROM
                        `patient_profile_hospital` ph
                        INNER JOIN `f_person`
                        ON `ph`.`hospcode` = `f_person`.`HOSPCODE`
                        AND `ph`.`pid` = `f_person`.`PID`
                    WHERE
                        (".$sqlDeath.") AND 
                        `ph`.`hospcode` = :sitecode AND
                        (".$sqlHd.") AND
                        `ph`.`type_area` IN ($type)
                    ";
        
        if($dp){   
            return [
                    'sql' =>$sqlDp,
                    'param'=>[':sitecode'=>$sitecode],

                    'total'=>CkdnetFunc::queryScalar($sql, [':sitecode'=>$sitecode]),
                ];
            }
        } 
        
	return CkdnetFunc::queryScalar($sql, [':sitecode'=>$sitecode]);//':sdate' => $sdate, ':edate' => $edate,


    }
    
    
    public static function  getCkdKt($sitecode, $sdate, $edate,$idcen,$type,$dp=false) {
        # - Stage -> (20.2)
        
//           \appxq\sdii\utils\VarDumper::dump($type);
        
        $sqlKt = self::$sqlKt;
        $csqlKt = self::$csqlKt;
        $sqlDeath = self::$sqlDeath;
        $CsqlDeath = self::$CsqlDeath;
        $field_genSelect = self::$field_genSelect;
        $Cen_field_genSelect = self::$Cen_field_genSelect;
        
        if($idcen== 'central'){
            
            $sql = "SELECT
                        COUNT(*)
                    FROM
                        `patient_profile_hospital` ph
                        LEFT JOIN patient_profile_central pc ON ph.ptlink = pc.ptlink
                    WHERE
                        (".$CsqlDeath.") AND 
                        `ph`.`hospcode` = :sitecode AND
                        (pc.ptlink <> '' OR ph.ptlink = '') AND
                        `ph`.`type_area`IN ($type) AND 
                        (".$csqlKt.")";
            if($dp){
                $selectKey = CkdnetQuery::genSelect();

                $sqlDp =    "SELECT
                                $selectKey,$Cen_field_genSelect
                            FROM
                                `patient_profile_hospital` ph
                                LEFT JOIN patient_profile_central pc ON ph.ptlink = pc.ptlink
                                INNER JOIN `f_person`
                                ON `ph`.`hospcode` = `f_person`.`HOSPCODE`
                                AND `ph`.`pid` = `f_person`.`PID`
                            WHERE
                                (".$CsqlDeath.") AND 
                                `ph`.`hospcode` = :sitecode AND
                                (pc.ptlink <> '' OR ph.ptlink = '') AND
                                `ph`.`type_area`IN ($type) AND 
                                (".$csqlKt.")";

                return [
                    'sql' =>$sqlDp,
                    'param'=>[':sitecode'=>$sitecode],

                    'total'=>CkdnetFunc::queryScalar($sql, [':sitecode'=>$sitecode]),
                ];
            }           
//                   \appxq\sdii\utils\VarDumper::dump($dp);                             
            
        } else{
            $selectKey = CkdnetQuery::genSelect();
            
            $sql = "SELECT
                        COUNT(*)
                    FROM
                        `patient_profile_hospital` ph
                    WHERE
                        (".$sqlDeath.") AND 
                        `hospcode` = :sitecode AND
                        (".$sqlKt.") AND
                        `ph`.`type_area` IN ($type)
                        ";
            $sqlDp = "SELECT
                        $selectKey,$field_genSelect
                    FROM
                        `patient_profile_hospital` ph
                        INNER JOIN `f_person`
                        ON `ph`.`hospcode` = `f_person`.`HOSPCODE`
                        AND `ph`.`pid` = `f_person`.`PID`
                    WHERE
                        (".$sqlDeath.") AND 
                        `ph`.`hospcode` = :sitecode AND
                        (".$sqlKt.") AND
                        `ph`.`type_area` IN ($type)
                    ";
        
        if($dp){   
            return [
                    'sql' =>$sqlDp,
                    'param'=>[':sitecode'=>$sitecode],

                    'total'=>CkdnetFunc::queryScalar($sql, [':sitecode'=>$sitecode]),
                ];
            }
        } 
        
	return CkdnetFunc::queryScalar($sql, [':sitecode'=>$sitecode]);//':sdate' => $sdate, ':edate' => $edate,


    }
    
    public static function  getCkdHomeCare($sitecode, $sdate, $edate) {
        # - Stage -> (28)   
        $sql = "SELECT COUNT(*) FROM tbdata_1462172833035880400
        where ckd = '1' 
        and hsitecode = :sitecode 
        and sitecode = :sitecode";
//        
            return  $value = Yii::$app->db->createCommand($sql)->bindValues([':sitecode' => $sitecode])->queryScalar();

    }
    
    
    # Target for screening
    public static function getCkdTargetScreening($sitecode, $sdate, $edate,$idcen,$type, $dp=false) {
        # ckd: DM/HT -> CKD Screening Target -> (4)
        # - DM/HT -
        # - CKD Screening Target - (4)
        $CsqlDMHT = self::$CsqlDMHT;
        $CsqlCkdReportedCKD = self::$CsqlCkdReportedCKD;
//        $CsqlTypeareaCKD = self::$CsqlTypeareaCKD;
        $CsqlDeath = self::$CsqlDeath;
        
        $sqlDMHT = self::$sqlDMHT;
        $sqlCkdReportedCKD = self::$sqlCkdReportedCKD;
//        $sqlTypeareaCKD = self::$sqlTypeareaCKD;
        $sqlDeath = self::$sqlDeath;
        $field_genSelect = self::$field_genSelect;
        $Cen_field_genSelect = self::$Cen_field_genSelect;
        
        
        if($idcen== 'central'){
            
                 $sql = "SELECT
                            COUNT(*)
                        FROM
                          `patient_profile_hospital` ph
                        LEFT JOIN patient_profile_central pc ON ph.ptlink = pc.ptlink
                        WHERE
                              (".$CsqlDeath.") AND 
                              `hospcode` = :sitecode AND
                              (pc.ptlink <> '' OR ph.ptlink = '') AND
                              `ph`.`type_area` IN ($type) AND
                              (".$CsqlDMHT.") AND
                               NOT (".$CsqlCkdReportedCKD.") 
                                     
                    ";

                if($dp){
                    $selectKey = CkdnetQuery::genSelect();

                    $sqlDp =   "SELECT
                                    $selectKey,$Cen_field_genSelect
                                FROM
                                    `patient_profile_hospital` ph
                                INNER JOIN `f_person`
                                    ON `ph`.`hospcode` = `f_person`.`HOSPCODE`
                                    AND `ph`.`pid` = `f_person`.`PID`
                                    AND `ph`.`hospcode` = `f_person`.`sitecode`
                                LEFT JOIN patient_profile_central pc ON ph.ptlink = pc.ptlink
                                WHERE
                                    (".$CsqlDeath.") AND 
                                    ph.`hospcode` = :sitecode AND
                                     `f_person`.`sitecode` = :sitecode AND
                                    (pc.ptlink <> '' OR ph.ptlink = '') AND
                                    `ph`.`type_area` IN ($type) AND
                                    (".$CsqlDMHT.") AND
                                    NOT (".$CsqlCkdReportedCKD.") 
                      ";

                    return [
                        'sql' =>$sqlDp,
                        'param'=>[':sitecode'=>$sitecode],
                        'total'=>CkdnetFunc::queryScalar($sql, [':sitecode'=>$sitecode]),
                    ];
                }
                
        }else{
                $sql = "SELECT
                            COUNT(*)
                        FROM
                            `patient_profile_hospital` ph
                        WHERE
                            (".$sqlDeath.") AND 
                            `hospcode` = :sitecode AND
                            (".$sqlDMHT.") AND
                            NOT(".$sqlCkdReportedCKD.") AND
                            `ph`.`type_area` IN ($type)   
                        ";

                if($dp){
                    $selectKey = CkdnetQuery::genSelect();

                    $sqlDp = "SELECT
                            $selectKey,$field_genSelect
                        FROM
                            `patient_profile_hospital` ph 
                            INNER JOIN `f_person`
                            ON `ph`.`hospcode` = `f_person`.`HOSPCODE`
                            AND `ph`.`pid` = `f_person`.`PID`
                        WHERE
                            (".$sqlDeath.") AND 
                            `ph`.`hospcode` = :sitecode AND
                            `f_person`.`sitecode` = :sitecode AND
                            (".$sqlDMHT.") AND
                            NOT(".$sqlCkdReportedCKD.") AND
                            `ph`.`type_area` IN ($type)                       
                        ";

                    return [
                        'sql' =>$sqlDp,
                        'param'=>[':sitecode'=>$sitecode],

                        'total'=>CkdnetFunc::queryScalar($sql, [':sitecode'=>$sitecode]),
                    ];
                }
        }  
	return CkdnetFunc::queryScalar($sql, [':sitecode'=>$sitecode]);//':sdate' => $sdate, ':edate' => $edate, 
    }
    
    # Screening (screened)
    public static function getCkdScreened($sitecode, $sdate, $edate,$idcen,$type, $dp=false) {
        # ckd: DM/HT -> CKD Screening Target  => Screened -> (5)
        # - DM/HT  -
        # - CKD Screening Target -
        # - Screened -  (5)
        
        $CsqlDMHT = self::$CsqlDMHT;
        $CsqlCkdTarget = self::$CsqlCkdTarget;
        $CsqlCkdScreen = self::$CsqlCkdScreen;
//        $CsqlTypeareaCKD = self::$CsqlTypeareaCKD;
        $CsqlDeath = self::$CsqlDeath;
        
        $sqlDMHT = self::$sqlDMHT;
        $sqlCkdTarget = self::$sqlCkdTarget;
        $sqlCkdScreen = self::$sqlCkdScreen;
//        $sqlTypeareaCKD = self::$sqlTypeareaCKD;
        $sqlDeath = self::$sqlDeath;
        #$sqlCkdReportedCKD = self::$sqlCkdReportedCKD;
	$field_genSelect = self::$field_genSelect;
        $Cen_field_genSelect = self::$Cen_field_genSelect;
     
         
        if($idcen== 'central'){
            
                 $sql = "SELECT
                            COUNT(*)
                        FROM
                          `patient_profile_hospital` ph
                        LEFT JOIN patient_profile_central pc ON ph.ptlink = pc.ptlink
                        WHERE
                            (".$CsqlDeath.") AND 
                            `hospcode` = :sitecode AND
                            (pc.ptlink <> '' OR ph.ptlink = '') AND
                            `ph`.`type_area` IN ($type) AND 
                            (".$CsqlDMHT.") AND 
                            (".$CsqlCkdTarget.") AND 
                            (".$CsqlCkdScreen.") 

                    ";

                if($dp){
                    $selectKey = CkdnetQuery::genSelect();

                    $sqlDp =   "SELECT
                                    $selectKey,$Cen_field_genSelect
                                FROM
                                    `patient_profile_hospital` ph
                                INNER JOIN `f_person`
                                    ON `ph`.`hospcode` = `f_person`.`HOSPCODE`
                                    AND `ph`.`pid` = `f_person`.`PID`
                                    AND `ph`.`hospcode` = `f_person`.`sitecode`
                                LEFT JOIN patient_profile_central pc ON ph.ptlink = pc.ptlink
                                WHERE
                                    (".$CsqlDeath.") AND 
                                    ph.`hospcode` = :sitecode AND
                                     `f_person`.`sitecode` = :sitecode AND
                                    (pc.ptlink <> '' OR ph.ptlink = '') AND
                                    `ph`.`type_area` IN ($type) AND
                                    (".$CsqlDMHT.") AND
                                    (".$CsqlCkdTarget.") AND
                                    (".$CsqlCkdScreen.") 
                        ";

                    return [
                        'sql' =>$sqlDp,
                        'param'=>[':sitecode'=>$sitecode],
                        'total'=>CkdnetFunc::queryScalar($sql, [':sitecode'=>$sitecode]),
                    ];
                }
                
        }else{
        
                $sql = "SELECT
                            COUNT(*)
                        FROM
                            `patient_profile_hospital` ph
                        WHERE
                            (".$sqlDeath.") AND 
                            `hospcode` = :sitecode AND
                            (".$sqlDMHT.") AND
                            (".$sqlCkdTarget.") AND
                            (".$sqlCkdScreen.") AND
                            `ph`.`type_area` IN ($type)   
                        ";

                if($dp){
                    $selectKey = CkdnetQuery::genSelect();

                    $sqlDp = "SELECT
                            $selectKey,$field_genSelect
                        FROM
                            `patient_profile_hospital` ph
                            INNER JOIN `f_person`
                            ON `ph`.`hospcode` = `f_person`.`HOSPCODE`
                            AND `ph`.`pid` = `f_person`.`PID`
                        WHERE
                            (".$sqlDeath.") AND 
                            `ph`.`hospcode` = :sitecode AND
                            `f_person`.`sitecode` = :sitecode AND
                            (".$sqlDMHT.") AND
                            (".$sqlCkdTarget.") AND
                            (".$sqlCkdScreen.") AND
                            `ph`.`type_area` IN ($type)
                        ";

                    return [
                        'sql' =>$sqlDp,
                        'param'=>[':sitecode'=>$sitecode],

                        'total'=>CkdnetFunc::queryScalar($sql, [':sitecode'=>$sitecode]),
                    ];
                }
        }
	return CkdnetFunc::queryScalar($sql, [':sitecode'=>$sitecode]);//':sdate' => $sdate, ':edate' => $edate, 
    }
    public static function getCkdScreenedNotCkd($sitecode, $sdate, $edate,$idcen,$type, $dp=false) {
        # ckd: DM/HT -> CKD Screening Target  -> Screened => Not CKD -> (6)
        # - DM/HT  -
        # - CKD Screening Target -
        # - Screened -  
        # - Not CKD - (6)
        
        $CsqlDMHT = self::$CsqlDMHT;
        $CsqlCkdScreen = self::$CsqlCkdScreen;
        $CsqlCkdTarget = self::$CsqlCkdTarget;
        $CsqlConfirmCkdNo = self::$CsqlConfirmCkdNo;
//        $CsqlTypeareaCKD = self::$CsqlTypeareaCKD;
        $CsqlDeath = self::$CsqlDeath;
        
        $sqlDMHT = self::$sqlDMHT;
        $sqlCkdScreen = self::$sqlCkdScreen;
        $sqlCkdTarget = self::$sqlCkdTarget;
        $sqlConfirmCkdNo = self::$sqlConfirmCkdNo;
//        $sqlTypeareaCKD = self::$sqlTypeareaCKD;
        $sqlDeath = self::$sqlDeath;
        $field_genSelect = self::$field_genSelect;
        $Cen_field_genSelect = self::$Cen_field_genSelect;
        
         if($idcen == 'central'){
            
                 $sql = "SELECT
                            COUNT(*)
                        FROM
                          `patient_profile_hospital` ph
                        LEFT JOIN patient_profile_central pc ON ph.ptlink = pc.ptlink
                        WHERE
                              (".$CsqlDeath.") AND 
                              `hospcode` = :sitecode AND
                              (pc.ptlink <> '' OR ph.ptlink = '') AND
                              `ph`.`type_area` IN ($type) AND
                              (".$CsqlDMHT.") AND
                              (".$CsqlCkdTarget.") AND
                              (".$CsqlCkdScreen.") AND
                              (".$CsqlConfirmCkdNo.") 
                           

                    ";

                if($dp){
                    $selectKey = CkdnetQuery::genSelect();

                    $sqlDp =   "SELECT
                                    $selectKey,$Cen_field_genSelect
                                FROM
                                    `patient_profile_hospital` ph
                                INNER JOIN `f_person`
                                    ON `ph`.`hospcode` = `f_person`.`HOSPCODE`
                                    AND `ph`.`pid` = `f_person`.`PID`
                                    AND `ph`.`hospcode` = `f_person`.`sitecode`
                                LEFT JOIN patient_profile_central pc ON ph.ptlink = pc.ptlink
                                WHERE
                                      (".$CsqlDeath.") AND 
                                      ph.`hospcode` = :sitecode AND
                                       `f_person`.`sitecode` = :sitecode AND
                                      (pc.ptlink <> '' OR ph.ptlink = '') AND
                                      `ph`.`type_area` IN ($type) AND
                                      (".$CsqlDMHT.") AND
                                      (".$CsqlCkdTarget.") AND
                                      (".$CsqlCkdScreen.") AND
                                      (".$CsqlConfirmCkdNo.") 
                        ";

                    return [
                        'sql' =>$sqlDp,
                        'param'=>[':sitecode'=>$sitecode],
                        'total'=>CkdnetFunc::queryScalar($sql, [':sitecode'=>$sitecode]),
                    ];
                }
                
        }else{
        
                $sql = "SELECT
                            COUNT(*)
                        FROM
                            `patient_profile_hospital` ph
                        WHERE
                            (".$sqlDeath.") AND 
                            `hospcode` = :sitecode AND
                            (".$sqlDMHT.") AND
                            (".$sqlCkdTarget.") AND
                            (".$sqlCkdScreen.") AND
                            (".$sqlConfirmCkdNo.") AND
                            `ph`.`type_area` IN ($type) 
                        ";

                if($dp){
                    $selectKey = CkdnetQuery::genSelect();

                    $sqlDp = "SELECT
                            $selectKey,$field_genSelect
                        FROM
                            `patient_profile_hospital` ph
                            INNER JOIN `f_person`
                            ON `ph`.`hospcode` = `f_person`.`HOSPCODE`
                            AND `ph`.`pid` = `f_person`.`PID`
                        WHERE
                            (".$sqlDeath.") AND 
                            `ph`.`hospcode` = :sitecode AND
                            `f_person`.`sitecode` = :sitecode AND
                            (".$sqlDMHT.") AND
                            (".$sqlCkdTarget.") AND
                            (".$sqlCkdScreen.") AND
                            (".$sqlConfirmCkdNo.") AND
                            `ph`.`type_area` IN ($type)    
                        ";

                    return [
                        'sql' =>$sqlDp,
                        'param'=>[':sitecode'=>$sitecode],

                        'total'=>CkdnetFunc::queryScalar($sql, [':sitecode'=>$sitecode]),
                    ];
                }
        }
	return CkdnetFunc::queryScalar($sql, [':sitecode'=>$sitecode]);//':sdate' => $sdate, ':edate' => $edate, 
    }
    public static function getCkdScreenedWaitVer($sitecode, $sdate, $edate,$idcen,$type, $dp=false) {
        # call CkdnetQueryRight::getCkdScreenedWaitVer
        # ckd: DM/HT -> CKD Screening Target  -> Screened => Waiting for verification -> (7)
        # - DM/HT  -
        # - CKD Screening Target -
        # - Screened - 
        # - Waiting for verification - (7)
        
        $CsqlDMHT = self::$CsqlDMHT;
        $CsqlCkdScreen = self::$CsqlCkdScreen;
        $CsqlCkdTarget = self::$CsqlCkdTarget;
        $CsqlConfirmCkdWait = self::$CsqlConfirmCkdWait;
//        $CsqlTypeareaCKD = self::$CsqlTypeareaCKD;
        $CsqlDeath = self::$CsqlDeath;
        
        $sqlDMHT = self::$sqlDMHT;
        $sqlCkdScreen = self::$sqlCkdScreen;
        $sqlCkdTarget = self::$sqlCkdTarget;
        $sqlConfirmCkdWait = self::$sqlConfirmCkdWait;
//        $sqlTypeareaCKD = self::$sqlTypeareaCKD;
        $sqlDeath = self::$sqlDeath;
        $field_genSelect = self::$field_genSelect;
        $Cen_field_genSelect = self::$Cen_field_genSelect;
        
        if($idcen== 'central'){
            
                 $sql = "SELECT
                            COUNT(*)
                        FROM
                          `patient_profile_hospital` ph
                        LEFT JOIN patient_profile_central pc ON ph.ptlink = pc.ptlink
                        WHERE
                              (".$CsqlDeath.") AND 
                              `hospcode` = :sitecode AND
                              (pc.ptlink <> '' OR ph.ptlink = '') AND
                              `ph`.`type_area` IN ($type) AND
                              (".$CsqlDMHT.") AND
                              (".$CsqlCkdTarget.") AND
                              (".$CsqlCkdScreen.") AND
                              (".$CsqlConfirmCkdWait.") 
                                  
                    ";

                if($dp){
                    $selectKey = CkdnetQuery::genSelect();

                    $sqlDp =   "SELECT
                                    $selectKey,$Cen_field_genSelect
                                FROM
                                    `patient_profile_hospital` ph
                                INNER JOIN `f_person`
                                    ON `ph`.`hospcode` = `f_person`.`HOSPCODE`
                                    AND `ph`.`pid` = `f_person`.`PID`
                                    AND `ph`.`hospcode` = `f_person`.`sitecode`
                                LEFT JOIN patient_profile_central pc ON ph.ptlink = pc.ptlink
                                WHERE
                                    (".$CsqlDeath.") AND 
                                    ph.`hospcode` = :sitecode AND
                                     `f_person`.`sitecode` = :sitecode AND
                                    (pc.ptlink <> '' OR ph.ptlink = '') AND
                                    `ph`.`type_area` IN ($type) AND
                                    (".$CsqlDMHT.") AND
                                    (".$CsqlCkdTarget.") AND
                                    (".$CsqlCkdScreen.") AND
                                    (".$CsqlConfirmCkdWait.")  
                        ";

                    return [
                        'sql' =>$sqlDp,
                        'param'=>[':sitecode'=>$sitecode],
                        'total'=>CkdnetFunc::queryScalar($sql, [':sitecode'=>$sitecode]),
                    ];
                }
                
        }else{
        
                $sql = "SELECT
                            COUNT(*)
                        FROM
                            `patient_profile_hospital` ph
                        WHERE
                            (".$sqlDeath.") AND 
                            `hospcode` = :sitecode AND
                            (".$sqlDMHT.") AND
                            (".$sqlCkdTarget.") AND
                            (".$sqlCkdScreen.") AND
                            (".$sqlConfirmCkdWait.") AND
                            `ph`.`type_area` IN ($type)
                        ";

                if($dp){
                    $selectKey = CkdnetQuery::genSelect();

                    $sqlDp = "SELECT
                            $selectKey,$field_genSelect
                        FROM
                            `patient_profile_hospital` ph
                            INNER JOIN `f_person`
                            ON `ph`.`hospcode` = `f_person`.`HOSPCODE`
                            AND `ph`.`pid` = `f_person`.`PID`
                        WHERE
                            (".$sqlDeath.") AND 
                            `ph`.`hospcode` = :sitecode AND
                            `f_person`.`sitecode` = :sitecode AND
                            (".$sqlDMHT.") AND
                            (".$sqlCkdTarget.") AND
                            (".$sqlCkdScreen.") AND
                            (".$sqlConfirmCkdWait.") AND
                            `ph`.`type_area` IN ($type)    
                        ";

                    return [
                        'sql' =>$sqlDp,
                        'param'=>[':sitecode'=>$sitecode],

                        'total'=>CkdnetFunc::queryScalar($sql, [':sitecode'=>$sitecode]),
                    ];
                }
        } 
	return CkdnetFunc::queryScalar($sql, [':sitecode'=>$sitecode]);//':sdate' => $sdate, ':edate' => $edate, 
    }
    public static function getCkdScreenedConfirmCkd($sitecode, $sdate, $edate,$idcen,$type, $dp=false) {
        # ckd: DM/HT -> CKD Screening Target  -> Screened => Confirmed CKD -> (8)
        # - DM/HT  -
        # - CKD Screening Target -
        # - Screened - 
        # - Confirmed CKD - (8)
        $CsqlDMHT = self::$CsqlDMHT;
        $CsqlCkdScreen = self::$CsqlCkdScreen;
        $CsqlCkdTarget = self::$CsqlCkdTarget;
        $CsqlConfirmCkd = self::$CsqlConfirmCkd;
//        $CsqlTypeareaCKD = self::$CsqlTypeareaCKD;
        $CsqlDeath = self::$CsqlDeath;
        
        $sqlDMHT = self::$sqlDMHT;
        $sqlCkdScreen = self::$sqlCkdScreen;
        $sqlCkdTarget = self::$sqlCkdTarget;
        $sqlConfirmCkd = self::$sqlConfirmCkd;
//        $sqlTypeareaCKD = self::$sqlTypeareaCKD;
        $sqlDeath = self::$sqlDeath;
        $field_genSelect = self::$field_genSelect;
        $Cen_field_genSelect = self::$Cen_field_genSelect;
        
         if($idcen== 'central'){
            
                 $sql = "SELECT
                            COUNT(*)
                        FROM
                          `patient_profile_hospital` ph
                        LEFT JOIN patient_profile_central pc ON ph.ptlink = pc.ptlink
                        WHERE
                              (".$CsqlDeath.") AND 
                              `hospcode` = :sitecode AND
                              (pc.ptlink <> '' OR ph.ptlink = '') AND
                              `ph`.`type_area` IN ($type) AND
                              (".$CsqlDMHT.") AND
                              (".$CsqlCkdTarget.") AND
                              (".$CsqlCkdScreen.") AND
                              (".$CsqlConfirmCkd.") 
                                                       
                    ";

                if($dp){
                    $selectKey = CkdnetQuery::genSelect();

                    $sqlDp =   "SELECT
                                    $selectKey,$Cen_field_genSelect
                                FROM
                                    `patient_profile_hospital` ph
                                INNER JOIN `f_person`
                                    ON `ph`.`hospcode` = `f_person`.`HOSPCODE`
                                    AND `ph`.`pid` = `f_person`.`PID`
                                    AND `ph`.`hospcode` = `f_person`.`sitecode`
                                LEFT JOIN patient_profile_central pc ON ph.ptlink = pc.ptlink
                                WHERE
                                    (".$CsqlDeath.") AND 
                                    ph.`hospcode` = :sitecode AND
                                     `f_person`.`sitecode` = :sitecode AND
                                    (pc.ptlink <> '' OR ph.ptlink = '') AND
                                    `ph`.`type_area` IN ($type) AND
                                    (".$CsqlDMHT.") AND
                                    (".$CsqlCkdTarget.") AND
                                    (".$CsqlCkdScreen.") AND
                                    (".$CsqlConfirmCkd.") 
                        ";

                    return [
                        'sql' =>$sqlDp,
                        'param'=>[':sitecode'=>$sitecode],
                        'total'=>CkdnetFunc::queryScalar($sql, [':sitecode'=>$sitecode]),
                    ];
                }
                
        }else{
        
                $sql = "SELECT
                            COUNT(*)
                        FROM
                            `patient_profile_hospital` ph
                        WHERE
                            (".$sqlDeath.") AND 
                            `hospcode` = :sitecode AND
                            (".$sqlDMHT.") AND
                            (".$sqlCkdTarget.") AND
                            (".$sqlCkdScreen.") AND
                            (".$sqlConfirmCkd.") AND
                            `ph`.`type_area` IN ($type)    
                        ";

                if($dp){
                    $selectKey = CkdnetQuery::genSelect();

                    $sqlDp = "SELECT
                            $selectKey,$field_genSelect
                        FROM
                            `patient_profile_hospital` ph
                            INNER JOIN `f_person`
                            ON `ph`.`hospcode` = `f_person`.`HOSPCODE`
                            AND `ph`.`pid` = `f_person`.`PID`
                        WHERE
                            (".$sqlDeath.") AND 
                            `ph`.`hospcode` = :sitecode AND
                            `f_person`.`sitecode` = :sitecode AND
                            (".$sqlDMHT.") AND
                            (".$sqlCkdTarget.") AND
                            (".$sqlCkdScreen.") AND
                            (".$sqlConfirmCkd.") AND
                            `ph`.`type_area` IN ($type)
                        ";

                    return [
                        'sql' =>$sqlDp,
                        'param'=>[':sitecode'=>$sitecode],

                        'total'=>CkdnetFunc::queryScalar($sql, [':sitecode'=>$sitecode]),
                    ];
                }
        }
	return CkdnetFunc::queryScalar($sql, [':sitecode'=>$sitecode]);//':sdate' => $sdate, ':edate' => $edate, 
    }
    #   -> Target not screen
    public static function getCkdNotScreened($sitecode, $sdate, $edate,$idcen,$type, $dp=false) {
        # call CkdnetQueryRight::getCkdNotScreened
        # ckd: DM/HT -> CKD Not Screened -> (9)
        # - CKD Not Screened - (9)
        $CsqlDMHT = self::$CsqlDMHT;
        $CsqlCkdTarget = self::$CsqlCkdTarget;
        $CsqlCkdScreenNon = self::$CsqlCkdScreenNon;
//        $CsqlTypeareaCKD = self::$CsqlTypeareaCKD;
        $CsqlDeath = self::$CsqlDeath;
        
        $sqlDMHT = self::$sqlDMHT;
        $sqlCkdTarget = self::$sqlCkdTarget;
        $sqlCkdScreenNon = self::$sqlCkdScreenNon;
//        $sqlTypeareaCKD = self::$sqlTypeareaCKD;
        $sqlDeath = self::$sqlDeath;
        $field_genSelect = self::$field_genSelect;
        $Cen_field_genSelect = self::$Cen_field_genSelect;
        
         if($idcen == 'central'){
            
                 $sql = "SELECT
                            COUNT(*)
                        FROM
                          `patient_profile_hospital` ph
                        LEFT JOIN patient_profile_central pc ON ph.ptlink = pc.ptlink
                        WHERE
                              (".$CsqlDeath.") AND 
                              `hospcode` = :sitecode AND
                              (pc.ptlink <> '' OR ph.ptlink = '') AND
                              `ph`.`type_area` IN ($type) AND
                              (".$CsqlDMHT.") AND
                              (".$CsqlCkdTarget.") AND
                              (".$CsqlCkdScreenNon.")
                    ";

                if($dp){
                    $selectKey = CkdnetQuery::genSelect();

                    $sqlDp =   "SELECT
                                    $selectKey,$Cen_field_genSelect
                                FROM
                                    `patient_profile_hospital` ph
                                INNER JOIN `f_person`
                                    ON `ph`.`hospcode` = `f_person`.`HOSPCODE`
                                    AND `ph`.`pid` = `f_person`.`PID`
                                    AND `ph`.`hospcode` = `f_person`.`sitecode`
                                LEFT JOIN patient_profile_central pc ON ph.ptlink = pc.ptlink
                                WHERE
                                    (".$CsqlDeath.") AND 
                                    ph.`hospcode` = :sitecode AND
                                     `f_person`.`sitecode` = :sitecode AND
                                    (pc.ptlink <> '' OR ph.ptlink = '') AND
                                    `ph`.`type_area` IN ($type) AND
                                    (".$CsqlDMHT.") AND
                                    (".$CsqlCkdTarget.") AND
                                    (".$CsqlCkdScreenNon.")
                                      
                        ";

                    return [
                        'sql' =>$sqlDp,
                        'param'=>[':sitecode'=>$sitecode],
                        'total'=>CkdnetFunc::queryScalar($sql, [':sitecode'=>$sitecode]),
                    ];
                }
                
        }else{
        
                $sql = "SELECT
                            COUNT(*)
                        FROM
                            `patient_profile_hospital` ph
                        WHERE
                            (".$sqlDeath.") AND 
                            `hospcode` = :sitecode AND
                            (".$sqlDMHT.") AND
                            (".$sqlCkdTarget.") AND
                            (".$sqlCkdScreenNon.") AND
                            `ph`.`type_area` IN ($type)
                        ";

                if($dp){
                    $selectKey = CkdnetQuery::genSelect();

                    $sqlDp = "SELECT
                            $selectKey,$field_genSelect
                        FROM
                            `patient_profile_hospital` ph
                            INNER JOIN `f_person`
                            ON `ph`.`hospcode` = `f_person`.`HOSPCODE`
                            AND `ph`.`pid` = `f_person`.`PID`
                        WHERE
                            (".$sqlDeath.") AND 
                            `ph`.`hospcode` = :sitecode AND
                            `f_person`.`sitecode` = :sitecode AND
                            (".$sqlDMHT.") AND
                            (".$sqlCkdTarget.") AND
                            (".$sqlCkdScreenNon.") AND
                            `ph`.`type_area` IN ($type)
                        ";

                    return [
                        'sql' =>$sqlDp,
                        'param'=>[':sitecode'=>$sitecode],

                        'total'=>CkdnetFunc::queryScalar($sql, [':sitecode'=>$sitecode]),
                    ];
                }
        }
	return CkdnetFunc::queryScalar($sql, [':sitecode'=>$sitecode]);//':sdate' => $sdate, ':edate' => $edate, 
    }
    public static function getCkdNotScreenedNotCkd($sitecode, $sdate, $edate,$idcen,$type, $dp=false) {
        # call CkdnetQueryRight::getCkdNotScreened
        # ckd: DM/HT -> CKD Not Screened => Insufficient info for diagosis of CKD -> (10)
        # - Insufficient info for diagosis of CKD - (10)
        
        $CsqlDMHT = self::$CsqlDMHT;
        $CsqlCkdScreenNon = self::$CsqlCkdScreenNon;
//        $CsqlTypeareaCKD = self::$CsqlTypeareaCKD;
        $CsqlDeath = self::$CsqlDeath;
        $CsqlCflabNon = self::$CsqlCflabNon;
        $CsqlTarget = self::$CsqlTarget;
        $CsqlCkdReportedCKDNull = self::$CsqlCkdReportedCKDNull;
        
        $sqlDMHT = self::$sqlDMHT;
        $sqlCkdScreenNon = self::$sqlCkdScreenNon;
//        $sqlTypeareaCKD = self::$sqlTypeareaCKD;
        $sqlDeath = self::$sqlDeath;
        $sqlCflabNon = self::$sqlCflabNon;
        $sqlTarget = self::$sqlTarget;
        $sqlCkdReportedCKDNull = self::$sqlCkdReportedCKDNull;
        $field_genSelect = self::$field_genSelect;
        $Cen_field_genSelect = self::$Cen_field_genSelect;
        
        if($idcen == 'central'){
            
                 $sql = "SELECT
                            COUNT(*)
                        FROM
                          `patient_profile_hospital` ph
                        LEFT JOIN patient_profile_central pc ON ph.ptlink = pc.ptlink
                        WHERE
                            (".$CsqlDeath.") AND 
                            `hospcode` = :sitecode AND
                            (pc.ptlink <> '' OR ph.ptlink = '') AND
                            `ph`.`type_area` IN ($type) AND
                            (".$CsqlDMHT.") AND
                            (".$CsqlTarget.") AND
                            (".$CsqlCkdScreenNon.") AND
                            (".$CsqlCkdReportedCKDNull.") AND 
                            (".$CsqlCflabNon.") 
                                                                   
                    ";

                if($dp){
                    $selectKey = CkdnetQuery::genSelect();

                    $sqlDp =   "SELECT
                                    $selectKey,$Cen_field_genSelect
                                FROM
                                    `patient_profile_hospital` ph
                                INNER JOIN `f_person`
                                    ON `ph`.`hospcode` = `f_person`.`HOSPCODE`
                                    AND `ph`.`pid` = `f_person`.`PID`
                                    AND `ph`.`hospcode` = `f_person`.`sitecode`
                                LEFT JOIN patient_profile_central pc ON ph.ptlink = pc.ptlink
                                WHERE
                                      (".$CsqlDeath.") AND 
                                      ph.`hospcode` = :sitecode AND
                                       `f_person`.`sitecode` = :sitecode AND
                                      (pc.ptlink <> '' OR ph.ptlink = '') AND
                                      `ph`.`type_area` IN ($type) AND
                                      (".$CsqlDMHT.") AND
                                      (".$CsqlTarget.") AND
                                      (".$CsqlCkdScreenNon.") AND
                                      (".$CsqlCkdReportedCKDNull.") AND 
                                      (".$CsqlCflabNon.") 
                        ";

                    return [
                        'sql' =>$sqlDp,
                        'param'=>[':sitecode'=>$sitecode],
                        'total'=>CkdnetFunc::queryScalar($sql, [':sitecode'=>$sitecode]),
                    ];
                }
                
        }else{
        
                $sql = "SELECT 
                            COUNT(*) 
                        FROM 
                            patient_profile_hospital ph
                        WHERE 
                            `hospcode` = :sitecode AND
                            (".$sqlTarget.") AND
                            (".$sqlDMHT.") AND
                            `ph`.`type_area` IN ($type) AND
                            (".$sqlCkdScreenNon.") AND
                            (".$sqlDeath.") AND
                            (".$sqlCkdReportedCKDNull.") AND 
                            (".$sqlCflabNon.") 
                        ";

                if($dp){
                    $selectKey = CkdnetQuery::genSelect();

                    $sqlDp = "SELECT
                            $selectKey,$field_genSelect
                        FROM
                            `patient_profile_hospital` ph 
                            INNER JOIN `f_person`
                            ON `ph`.`hospcode` = `f_person`.`HOSPCODE`
                            AND `ph`.`pid` = `f_person`.`PID`
                        WHERE
                            (".$sqlDeath.") AND 
                            `ph`.`hospcode` = :sitecode AND
                            `f_person`.`sitecode` = :sitecode AND
                            (".$sqlDMHT.") AND
                            (".$sqlTarget.") AND
                            `ph`.`type_area` IN ($type) AND
                            (".$sqlCkdScreenNon.") AND
                            (".$sqlCkdReportedCKDNull.") AND 
                            (".$sqlCflabNon.")
                        ";

                    return [
                        'sql' =>$sqlDp,
                        'param'=>[':sitecode'=>$sitecode],

                        'total'=>CkdnetFunc::queryScalar($sql, [':sitecode'=>$sitecode]),
                    ];
                }
        }
	return CkdnetFunc::queryScalar($sql, [':sitecode'=>$sitecode]);//':sdate' => $sdate, ':edate' => $edate, 
    }
    public static function getCkdNotScreenedWaitVer($sitecode, $sdate, $edate,$idcen,$type, $dp=false) {
        # call CkdnetQueryRight::getCkdNotScreenedWaitVer
        # ckd: DM/HT -> CKD Not Screened => Waiting for verification -> (11)
        # -  DM/HT -
        # - CKD Not Screened -
        # - Waiting for verification - (11)
        
        $CsqlDMHT = self::$CsqlDMHT;
        $CsqlCkdScreenNon = self::$CsqlCkdScreenNon;
//        $CsqlTypeareaCKD = self::$CsqlTypeareaCKD;
        $CsqlDeath = self::$CsqlDeath;
        $CsqlCflab = self::$CsqlCflab;
        $CsqlTarget = self::$CsqlTarget;
        $CsqlCkdReportedCKDNull = self::$CsqlCkdReportedCKDNull;
        $CsqlConfirmCkdWait = self::$CsqlConfirmCkdWait;
        
        $sqlDMHT = self::$sqlDMHT;
        $sqlCkdScreenNon = self::$sqlCkdScreenNon;
//        $sqlTypeareaCKD = self::$sqlTypeareaCKD;
        $sqlDeath = self::$sqlDeath;
        $sqlCflab = self::$sqlCflab;
        $sqlTarget = self::$sqlTarget;
        $sqlCkdReportedCKDNull = self::$sqlCkdReportedCKDNull;
        $sqlConfirmCkdWait = self::$sqlConfirmCkdWait;
        $field_genSelect = self::$field_genSelect;
        $Cen_field_genSelect = self::$Cen_field_genSelect;

          if($idcen == 'central'){
            
                 $sql = "SELECT
                            COUNT(*)
                        FROM
                          `patient_profile_hospital` ph
                        LEFT JOIN patient_profile_central pc ON ph.ptlink = pc.ptlink
                        WHERE
                            (".$CsqlDeath.") AND 
                            `hospcode` = :sitecode AND
                            (pc.ptlink <> '' OR ph.ptlink = '') AND
                            `ph`.`type_area` IN ($type) AND 
                            (".$CsqlDMHT.") AND
                            (".$CsqlCkdScreenNon.") AND  
                            (".$CsqlTarget.") AND
                            (".$CsqlCkdReportedCKDNull.") AND 
                            (".$CsqlCflab.") AND
                            (".$CsqlConfirmCkdWait.") 

                    ";

                if($dp){
                    $selectKey = CkdnetQuery::genSelect();

                    $sqlDp =   "SELECT
                                    $selectKey,$Cen_field_genSelect
                                FROM
                                    `patient_profile_hospital` ph
                                INNER JOIN `f_person`
                                    ON `ph`.`hospcode` = `f_person`.`HOSPCODE`
                                    AND `ph`.`pid` = `f_person`.`PID`
                                    AND `ph`.`hospcode` = `f_person`.`sitecode`
                                LEFT JOIN patient_profile_central pc ON ph.ptlink = pc.ptlink
                                WHERE
                                    (".$CsqlDeath.") AND 
                                    ph.`hospcode` = :sitecode AND
                                     `f_person`.`sitecode` = :sitecode AND
                                    (pc.ptlink <> '' OR ph.ptlink = '') AND
                                    `ph`.`type_area` IN ($type) AND
                                    (".$CsqlDMHT.") AND
                                    (".$CsqlCkdScreenNon.") AND  
                                    (".$CsqlTarget.") AND
                                    (".$CsqlCkdReportedCKDNull.") AND 
                                    (".$CsqlCflab.") AND
                                    (".$CsqlConfirmCkdWait.") 

                        ";

                    return [
                        'sql' =>$sqlDp,
                        'param'=>[':sitecode'=>$sitecode],
                        'total'=>CkdnetFunc::queryScalar($sql, [':sitecode'=>$sitecode]),
                    ];
                }
                
        }else{
                    $sql = "SELECT 
                                COUNT(*) 
                            FROM 
                                patient_profile_hospital ph
                            WHERE 
                                `hospcode` = :sitecode AND
                                (".$sqlTarget.") AND
                                (`ph`.`type_area` IN ($type) AND
                                (".$sqlDMHT.") AND
                                (".$sqlCkdScreenNon.") AND
                                (".$sqlDeath.") AND
                                (".$sqlCkdReportedCKDNull.") AND 
                                (".$sqlCflab.") AND
                                (".$sqlConfirmCkdWait.")
                            ";
                    if($dp){
                        $selectKey = CkdnetQuery::genSelect();

                        $sqlDp = "SELECT
                                $selectKey,$field_genSelect
                            FROM
                                `patient_profile_hospital` ph
                                INNER JOIN `f_person`
                                ON `ph`.`hospcode` = `f_person`.`HOSPCODE`
                                AND `ph`.`pid` = `f_person`.`PID`
                            WHERE
                                (".$sqlDeath.") AND 
                                `ph`.`hospcode` = :sitecode AND
                                `f_person`.`sitecode` = :sitecode AND
                                (".$sqlDMHT.") AND
                                (".$sqlTarget.") AND
                                `ph`.`type_area` IN ($type) AND
                                (".$sqlCkdScreenNon.") AND
                                (".$sqlCkdReportedCKDNull.") AND 
                                (".$sqlCflab.") 
                            ";

                        return [
                            'sql' =>$sqlDp,
                            'param'=>[':sitecode'=>$sitecode],

                            'total'=>CkdnetFunc::queryScalar($sql, [':sitecode'=>$sitecode]),
                        ];
                    }
        }
	return CkdnetFunc::queryScalar($sql, [':sitecode'=>$sitecode]);//':sdate' => $sdate, ':edate' => $edate, 
    }
    public static function getCkdNotScreenedConfirmCkd($sitecode, $sdate, $edate,$idcen,$type, $dp=false) {
    
        # call CkdnetQueryRight::getCkdNotScreenedConfirmCkd
        # ckd: DM/HT -> CKD Not Screened => Waiting for verification -> (12)
        # -  DM/HT -
        # - CKD Not Screened -
        # - Confirmed CKD - (12)
        
        $CsqlDMHT = self::$CsqlDMHT;
        $CsqlCkdTarget = self::$CsqlCkdTarget;
        $CsqlCkdScreenNon = self::$CsqlCkdScreenNon;
        $CsqlConfirmCkdNo = self::$CsqlConfirmCkdNo;
        $CsqlConfirmCkd = self::$CsqlConfirmCkd;
//        $CsqlTypeareaCKD = self::$CsqlTypeareaCKD;
        $CsqlDeath = self::$CsqlDeath;
        
        $sqlDMHT = self::$sqlDMHT;
        $sqlCkdTarget = self::$sqlCkdTarget;
        $sqlCkdScreenNon = self::$sqlCkdScreenNon;
        $sqlConfirmCkdNo = self::$sqlConfirmCkdNo;
        $sqlConfirmCkd = self::$sqlConfirmCkd;
//        $sqlTypeareaCKD = self::$sqlTypeareaCKD;
        $sqlDeath = self::$sqlDeath;
        $field_genSelect = self::$field_genSelect;
        $Cen_field_genSelect = self::$Cen_field_genSelect;
        
        
           if($idcen== 'central'){
            
                 $sql = "SELECT
                            COUNT(*)
                        FROM
                          `patient_profile_hospital` ph
                        LEFT JOIN patient_profile_central pc ON ph.ptlink = pc.ptlink
                        WHERE
                            (".$CsqlDeath.") AND 
                            `hospcode` = :sitecode AND
                            (pc.ptlink <> '' OR ph.ptlink = '') AND
                            `ph`.`type_area` IN ($type) AND 
                            (".$CsqlDMHT.") AND
                            (".$CsqlCkdTarget.") AND
                            (".$CsqlCkdScreenNon.") AND
                            (".$CsqlConfirmCkd.")  

                    ";

                if($dp){
                    $selectKey = CkdnetQuery::genSelect();

                    $sqlDp =   "SELECT
                                    $selectKey,$Cen_field_genSelect
                                FROM
                                    `patient_profile_hospital` ph
                                INNER JOIN `f_person`
                                    ON `ph`.`hospcode` = `f_person`.`HOSPCODE`
                                    AND `ph`.`pid` = `f_person`.`PID`
                                    AND `ph`.`hospcode` = `f_person`.`sitecode`
                                LEFT JOIN patient_profile_central pc ON ph.ptlink = pc.ptlink
                                WHERE
                                    (".$CsqlDeath.") AND 
                                    ph.`hospcode` = :sitecode AND
                                    `f_person`.`sitecode` = :sitecode AND
                                    (pc.ptlink <> '' OR ph.ptlink = '') AND
                                    `ph`.`type_area` IN ($type) AND
                                    (".$CsqlDMHT.") AND
                                    (".$CsqlCkdTarget.") AND
                                    (".$CsqlCkdScreenNon.") AND
                                    (".$CsqlConfirmCkd.") 

                        ";

                    return [
                        'sql' =>$sqlDp,
                        'param'=>[':sitecode'=>$sitecode],
                        'total'=>CkdnetFunc::queryScalar($sql, [':sitecode'=>$sitecode]),
                    ];
                }
                
        }else{
        
                    $sql = "SELECT
                                COUNT(*)
                            FROM
                                `patient_profile_hospital` ph
                            WHERE
                                (".$sqlDeath.") AND 
                                `hospcode` = :sitecode AND
                                (".$sqlDMHT.") AND
                                (".$sqlCkdTarget.") AND
                                (".$sqlCkdScreenNon.") AND
                                (".$sqlConfirmCkd.") AND
                                `ph`.`type_area` IN ($type) 
                            ";

                    if($dp){
                        $selectKey = CkdnetQuery::genSelect();

                        $sqlDp = "SELECT
                                $selectKey,$field_genSelect
                            FROM
                                `patient_profile_hospital` ph
                                INNER JOIN `f_person`
                                ON `ph`.`hospcode` = `f_person`.`HOSPCODE`
                                AND `ph`.`pid` = `f_person`.`PID`
                            WHERE
                                (".$sqlDeath.") AND 
                                `ph`.`hospcode` = :sitecode AND
                                `f_person`.`sitecode` = :sitecode AND
                                (".$sqlDMHT.") AND
                                (".$sqlCkdTarget.") AND
                                (".$sqlCkdScreenNon.") AND
                                (".$sqlConfirmCkd.") AND
                                `ph`.`type_area` IN ($type)
                            ";

                        return [
                            'sql' =>$sqlDp,
                            'param'=>[':sitecode'=>$sitecode],

                            'total'=>CkdnetFunc::queryScalar($sql, [':sitecode'=>$sitecode]),
                        ];
                    }
        }
	return CkdnetFunc::queryScalar($sql, [':sitecode'=>$sitecode]);//':sdate' => $sdate, ':edate' => $edate, 
    }
    # End Added
    #==============================#
    
}
