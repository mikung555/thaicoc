<?php

namespace backend\modules\ckdnet\classes;
use backend\modules\ckdnet\classes\CkdnutQuery;
use backend\modules\ckdnet\classes\CkdnetFunc;
use Yii;
 
class CkdnutFunc {

    public static function getDb() {

        if (isset(Yii::$app->session['dynamic_connection']) && !empty(Yii::$app->session['dynamic_connection'])) {
            $sitecode = Yii::$app->user->identity->userProfile->sitecode;
            //unset(Yii::$app->session['dynamic_connection']);
            if (Yii::$app->session['dynamic_connection']['sitecode'] != $sitecode) {
                $data_con = CkdnetQuery::getDbConfig($sitecode);

                $obj = [
                    'sitecode' => $sitecode,
                    'db' => $data_con
                ];
                Yii::$app->session['dynamic_connection'] = $obj;
            }
        } else {
            $sitecode = Yii::$app->user->identity->userProfile->sitecode;
            $data_con = CkdnetQuery::getDbConfig($sitecode);

            $obj = [
                'sitecode' => $sitecode,
                'db' => $data_con
            ];
            Yii::$app->session['dynamic_connection'] = $obj;
        }

        $data_config = Yii::$app->session['dynamic_connection']['db'];

        //\appxq\sdii\utils\VarDumper::dump(Yii::$app->session['dynamic_connection']);

        if (isset($data_config) && !empty($data_config)) {
            $dsn = "mysql:host={$data_config['server']};port={$data_config['port']};dbname={$data_config['db']}";

            $db = new \yii\db\Connection([
                'dsn' => $dsn,
                'username' => $data_config['user'],
                'password' => $data_config['passwd'],
                'charset' => 'utf8',
                    // 'enableSchemaCache' => true,
                    // 'schemaCacheDuration' => 3600,
            ]);
            //$db->open();

            return $db;
        }

        return FALSE;
    }

    public static function rawSql($sql, $param = []) {
        $db = self::getDb();
        if ($db) {
            return $db->createCommand($sql, $param)->rawSql;
        }
        return FALSE;
    }

    public static function execute($sql, $param = []) {
        $db = self::getDb();
        if ($db) {
            return $db->createCommand($sql, $param)->execute();
        }
        return FALSE;
    }

    public static function queryAll($sql, $param = []) {
        $db = self::getDb();
        //\appxq\sdii\utils\VarDumper::dump($db);
        if ($db) {
            return $db->createCommand($sql, $param)->queryAll();
        }
        return FALSE;
    }

    public static function queryOne($sql, $param = []) {
        $db = self::getDb();
        if ($db) {
            return $db->createCommand($sql, $param)->queryOne();
        }
        return FALSE;
    }

    public static function queryScalar($sql, $param = []) {
        $db = self::getDb();
        if ($db) {
            return $db->createCommand($sql, $param)->queryScalar();
        }
        return FALSE;
    }

    public static function convertColumn($colArr) {
        $colArrReturn = [];
        foreach ($colArr as $key => $value) {
            $colArrReturn[$key] = strtolower($value);
        }
        return $colArrReturn;
    }

    public static function getCkdDiagram($sitecode, $sdate, $edate) {
        try {
            $count = CkdnetQuery::getAll($sitecode, $sdate, $edate);
        } catch (\yii\db\Exception $e) {
            $count = 0;
        }
        $obj[] = [
            'id' => 0,
            'label' => "All ($count)",
            'value' => $count,
        ];

        try {
            $count = CkdnetQuery::getTotalPop($sitecode, $sdate, $edate);
        } catch (\yii\db\Exception $e) {
            $count = 0;
        }
        $obj[] = [
            'id' => 1,
            'label' => "1) Total population ($count)",
            'value' => $count,
        ];



        try {
            $count = CkdnetQueryRight::getCkdNonDMHT($sitecode, $sdate, $edate);
        } catch (\yii\db\Exception $e) {
            $count = 0;
        }
        $obj[] = [
            'id' => 13,
            'label' => "13) NON DM/HT ($count)",
            'value' => $count,
        ];



        try {
            $count = CkdnetQueryRight::getCkdNonDMHTReportedCKD($sitecode, $sdate, $edate);
        } catch (\yii\db\Exception $e) {
            $count = 0;
        }
        $obj[] = [
            'id' => 14,
            'label' => "14) Reported CKD ($count)",
            'value' => $count,
        ];

        try {
            $countNDM = CkdnetQueryRight::getNonDMHTCkdConfirm($sitecode, $sdate, $edate);
        } catch (\yii\db\Exception $e) {
            $countNDM = 0;
        }
        $obj[] = [
            'id' => 15,
            'label' => "15) Confirmed CKD ($countNDM)",
            'value' => $countNDM,
        ];





        try {
            $count = CkdnetQuery::getDmHt($sitecode, $sdate, $edate);
        } catch (\yii\db\Exception $e) {
            $count = 0;
        }
        $obj[] = [
            'id' => 2,
            'label' => "2) DM/HT ($count)",
            'value' => $count,
        ];

        try {
            $count = CkdnetQueryRight::getCkdReport($sitecode, $sdate, $edate);
        } catch (\yii\db\Exception $e) {
            $count = 0;
        }
        $obj[] = [
            'id' => 3,
            'label' => "3) Reported CKD ($count)",
            'value' => $count,
        ];

        try {
            $countRCON = CkdnetQueryRight::getCkdReportConfirm($sitecode, $sdate, $edate);
        } catch (\yii\db\Exception $e) {
            $countRCON = 0;
        }
        $obj[] = [
            'id' => 16,
            'label' => "16) Confirmed CKD ($countRCON)",
            'value' => $countRCON,
        ];

        $countLT = $countNDM + $countRCON;
        try {
            $count = CkdnetQueryRight::getCkdTotalConfirmCKD($sitecode, $sdate, $edate);
        } catch (\yii\db\Exception $e) {
            $count = 0;
        }
        $obj[] = [
            'id' => 17,
            'label' => "17) Total Confirmed CKD ($countLT)",
            'value' => $count,
        ];

        try {
            $count = CkdnetQueryRight::getCkdTargetScreening($sitecode, $sdate, $edate);
        } catch (\yii\db\Exception $e) {
            $count = 0;
        }
        $obj[] = [
            'id' => 4,
            'label' => "4) CKD Screened target ($count)",
            'value' => $count,
        ];


        //----
        try {
            $count = CkdnetQueryRight::getCkdScreened($sitecode, $sdate, $edate);
        } catch (\yii\db\Exception $e) {
            $count = 0;
        }
        $obj[] = [
            'id' => 5,
            'label' => "5) Screenend (Both eGFR & UA available) ($count)",
            'value' => $count,
        ];

        try {
            $count = CkdnetQueryRight::getCkdScreenedNotCkd($sitecode, $sdate, $edate);
        } catch (\yii\db\Exception $e) {
            $count = 0;
        }
        $obj[] = [
            'id' => 6,
            'label' => "6) Not CKD ($count)",
            'value' => $count,
        ];

        try {
            $count = CkdnetQueryRight::getCkdScreenedWaitVer($sitecode, $sdate, $edate);
        } catch (\yii\db\Exception $e) {
            $count = 0;
        }
        $obj[] = [
            'id' => 7,
            'label' => "7) Waiting for verification ($count)",
            'value' => $count,
        ];

        try {
            $count = CkdnetQueryRight::getCkdScreenedConfirmCkd($sitecode, $sdate, $edate);
        } catch (\yii\db\Exception $e) {
            $count = 0;
        }
        $obj[] = [
            'id' => 8,
            'label' => "8) Confirmed CKD ($count)",
            'value' => $count,
        ];

        try {
            $count = CkdnetQueryRight::getCkdNotScreened($sitecode, $sdate, $edate);
        } catch (\yii\db\Exception $e) {
            $count = 0;
        }
        $obj[] = [
            'id' => 9,
            'label' => "9) Not Screenend (Some/none eGFR, UA available) ($count)",
            'value' => $count,
        ];


        try {
            $count = CkdnetQueryRight::getCkdNotScreenedNotCkd($sitecode, $sdate, $edate);
        } catch (\yii\db\Exception $e) {
            $count = 0;
        }
        $obj[] = [
            'id' => 10,
            'label' => "10) Insufficient info for diagnosis of CKD ($count)",
            'value' => $count,
        ];

        try {
            $count = CkdnetQueryRight::getCkdNotScreenedWaitVer($sitecode, $sdate, $edate);
        } catch (\yii\db\Exception $e) {
            $count = 0;
        }
        $obj[] = [
            'id' => 11,
            'label' => "11) Waiting for verification ($count)",
            'value' => $count,
        ];

        try {
            $count = CkdnetQueryRight::getCkdNotScreenedConfirmCkd($sitecode, $sdate, $edate);
        } catch (\yii\db\Exception $e) {
            $count = 0;
        }
        $obj[] = [
            'id' => 12,
            'label' => "12) Confirmed CKD ($count)",
            'value' => $count,
        ];



        $countRT = $countLT + $countS + $countNS;
        try {
            $count = CkdnetQueryRight::getCkdTotalAllConfirmCKD($sitecode, $sdate, $edate);
        } catch (\yii\db\Exception $e) {
            $count = 0;
        }

        $obj[] = [
            'id' => 18,
            'label' => "18) Total Confirmed CKD ($count)",
            'value' => $count,
        ];

        return $obj;
    }
    
            public static function getCkdStageDiagram($sitecode, $sdate, $edate) {
            try {
                $count = CkdnetQueryRight::getCkdStage($sitecode, $sdate, $edate);
            } catch (\yii\db\Exception $e) {
                $count = 0;
            }
            // echo count($count);exit();
            //\appxq\sdii\utils\VarDumper::dump($count);
            $obj[] = [
                'id' => 19,
                'label' => "19) Stage CKD (count($count))",
                'value' => $count,
            ];


            return $obj;

        }

    public static function itemAlias($code, $key = NULL) {
        $items = [
            'DiagType' => [
                '1' => 'PDx (การวินิจฉัยโรคหลัก )',
                '2' => 'CO-MORBIDITY (การวินิจฉัยโรคร่วม)',
                '3' => 'COMPLICATION (การวินิจฉัยโรคแทรก)',
                '4' => 'OTHER (อื่นๆ)',
                '5' => 'EXTERNAL CAUSE (สาเหตุภายนอก)',
                '6' => 'Additional Code (รหัสเสริม)',
                '7' => 'Morphology Code (รหัสเกี่ยวกับเนื้องอก)',
            ],
            'TYPEDX' => [
                '1' => 'certain',
                '2' => 'probable',
                '3' => 'possible',
                '4' => 'unlikely',
                '5' => 'unclassified',
            ],
            'ALEVEL' => [
                '1' => 'ไม่ร้ายแรง (Non-serious)',
                '2' => 'ร้ายแรง - เสียชีวิต (Death)',
                '3' => 'ร้ายแรง - อัตรายถึงชีวิต (Life-threatening)',
                '4' => 'ร้ายแรง - ต้องเข้ารับการรักษาในโรงพยาบาล (Hospitalization-initial)',
                '5' => 'ร้ายแรง - ทำให้เพิ่มระยะเวลาในการรักษานานขึ้น',
                '6' => 'ร้ายแรง - พิการ (Disability)',
                '7' => 'ร้ายแรง - เป็นเหตุให้เกิดความผิดปกติแต่กำเนิด (Congenital anomaly)',
                '8' => 'ร้ายแรง - อื่นๆ',
            ],
            'typeout' => [
                '1' => 'OPD',
                '2' => 'IPD',
                '3' => 'REFER',
                '4' => 'เสียชีวิต',
                '5' => 'เสียชีวิต',
                '6' => 'เสียชีวิต',
                '7' => 'อื่นๆ',
                '8' => 'อื่นๆ',
                '9' => 'อื่นๆ',
            ],
        ];

        $return = $items[$code];

        if (isset($key)) {
            return isset($return[$key]) ? $return[$key] : false;
        } else {
            return isset($return) ? $return : false;
        }
    }

    public static function getLabItemHighcharts($data) {


        if (isset($data) && !empty($data)) {
            $map_chart = '[';
            $comma = '';
            foreach ($data as $key_c => $value_c) {
                $value = $value_c['result'] + 0;

                $arr = date_parse($value_c['date_serv']);
                $yy = $arr['year'];
                $mm = $arr['month'];
                $dd = $arr['day'];

                $map_chart .= $comma . "[Date.UTC($yy, $mm, $dd), $value]";
                $comma = ',';
            }
            $map_chart .= ']';

            return $map_chart;
        }
        return FALSE;
    }

    public static function getLab($ptlink, $hospcode, $pid) {
       // $data_lab = CkdnetQuery::getMapLab($hospcode);
        
         //label graph
	$sql = "SELECT  GROUP_CONCAT(LABCODE) as labcode, graph_label as labname , tdc_lab_items_code as labid,hos_lab_unit as labunit
                FROM lab_map 
                where hospcode = :hospcode AND (labcode<>'' and labcode is not null)
                GROUP BY tdc_lab_items_code
                ORDER BY hos_lab_items_name
		";
        $data = CkdnetFunc::queryAll($sql, [':hospcode'=>$hospcode]);
        $sql2 = "SELECT  GROUP_CONCAT(tdc_lab_code) as labcode, graph_label as labname , tdc_lab_code as labid,tdc_lab_unit as labunit , display_type 
                FROM tdc_lab_items_site 
                where hospcode = :hospcode AND (tdc_lab_code<>'' and tdc_lab_code is not null) AND display_type=3
                GROUP BY tdc_lab_code
                ORDER BY graph_label
		";
        $data2 = CkdnetFunc::queryAll($sql2, [':hospcode'=>$hospcode]);
        $data_lab = \yii\helpers\ArrayHelper::merge($data, $data2); 
       
        //\appxq\sdii\utils\VarDumper::dump($data_lab);
        $series = [];
        if ($data_lab) {
            foreach ($data_lab as $key => $value) {
                $display_site = $value['display_type'];
                if(empty($display_site)){
                    $display_site=1;
                }
                $data = CkdnutQuery::getlabCode2($ptlink, $hospcode, $pid, $value['labcode'], $display_site, $value['labname']);
               //\appxq\sdii\utils\VarDumper::dump($data);
                $dataLab = CkdnetFunc::getLabItemHighcharts($data);
                if ($dataLab) {
                    $series[] = ['name' => $value['labname'], 'tooltip' => ['valueSuffix' => ' ' . $value['labunit']], 'id' => $value['labid'], 'data' => new \yii\web\JsExpression($dataLab)];
                } else {
                    $series[] = ['name' => $value['labname'], 'tooltip' => ['valueSuffix' => ' ' . $value['labunit']], 'id' => $value['labid'], 'data' => []];
                }
            }
        }

        return $series;
    }

    public static function getLab2($ptlink, $hospcode, $pid) {
//        $group_id=3;
//        $sql = "SELECT * FROM lab_graph_list_site WHERE hospcode=:hospcode AND group_id=':group_id'";
//        $querys = CkdnetFunc::queryAll($sql,[':hospcode'=>$hospcode,':group_id'=>$group_id]);
//        \appxq\sdii\utils\VarDumper::dump($querys);
//
       $data_lab = CkdnetQuery::getMapLab2($hospcode,$pid);
         //\appxq\sdii\utils\VarDumper::dump($data_lab);
        $series = [];
        if ($data_lab) {
            foreach ($data_lab as $key => $value) {
                
                $data = CkdnetQuery::getserviceCode($ptlink, $hospcode, $pid, $value['labname']);//labcode
                $dataLab = CkdnetFunc::getLabItemHighcharts($data);
                if ($dataLab) {
                    $series[] = ['name' => $value['labname'], 'tooltip' => ['valueSuffix' => ' ' . $value['labunit']], 'id' => $value['labid'], 'data' => new \yii\web\JsExpression($dataLab)];
                } else {
                    $series[] = ['name' => $value['labname'], 'tooltip' => ['valueSuffix' => ' ' . $value['labunit']], 'id' => $value['labid'], 'data' => []];
                }
            }
        }

        return $series;
    }

}
