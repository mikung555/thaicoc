<?php
namespace backend\modules\ckdnet\classes;

use Yii;

/**
 * newPHPClass class file UTF-8
 * @author SDII <iencoded@gmail.com>
 * @copyright Copyright &copy; 2015 AppXQ
 * @license http://www.appxq.com/license/
 * @version 1.0.0 Date: 4 พ.ย. 2559 7:53:01
 * @link http://www.appxq.com/
 * @example 
 */
use backend\modules\ckdnet\classes\CkdnetFunc;
class CkdnutQuery {
    
    public static function getCkdPatientCentralField($hospcode, $ptlink, $field) {
	$sql = "SELECT ckd.$field FROM ckd_patient_central ckd WHERE ckd.ptlink = :ptlink AND ckd.hospcode = :hospcode";
	
	return Yii::$app->db->createCommand($sql, [':hospcode'=>$hospcode, ':ptlink'=>$ptlink])->queryScalar();
    }
    
    public static function getDbConfig($sitecode) {
	$sql = "SELECT db_config_province.province, 
			db_config_province.zonecode, 
			db_config_province.`server`, 
			db_config_province.`user`, 
			db_config_province.passwd, 
			db_config_province.`port`, 
			db_config_province.db, 
			db_config_province.webservice
		FROM db_config_province INNER JOIN all_hospital_thai ON db_config_province.province = all_hospital_thai.provincecode
		WHERE all_hospital_thai.hcode = :sitecode
		";
	return Yii::$app->dbbot_ip8->createCommand($sql, [':sitecode'=>$sitecode])->queryOne();
    }
    
    public static function getColumn($table) {
	$sql = "SELECT COLUMN_NAME AS `column` FROM INFORMATION_SCHEMA.COLUMNS
		WHERE TABLE_NAME = :tbname AND table_schema = :tbschema";
	return CkdnetFunc::queryAll($sql, [':tbname' => $table, ':tbschema' => 'tdc_data']);
    }
    
    public static function genSelect() {
        $convert = isset(Yii::$app->session['convert_ckd']) ? Yii::$app->session['convert_ckd'] : 0;
        $key = isset(Yii::$app->session['key_ckd']) ?Yii::$app->session['key_ckd'] : '';

        $selectKey = "`f_person`.HOSPCODE,
                `f_person`.PID,
                `f_person`.sitecode,
                `f_person`.ptlink,
                `f_person`.CID,
                `f_person`.`Pname`,
                `f_person`.`Name`,
                `f_person`.`Lname`";

        if($key!=''){
            $selectKey = "`f_person`.hospcode,
                `f_person`.pid,
                `f_person`.sitecode,
                `f_person`.ptlink,
                decode(unhex(f_person.CID),sha2('$key',256)) AS CID,
                decode(unhex(f_person.Pname),sha2('$key',256)) AS Pname,
                decode(unhex(f_person.`Name`),sha2('$key',256)) AS Name,
                decode(unhex(f_person.Lname),sha2('$key',256)) AS Lname";

            if ($convert==1) {
                $selectKey = "`f_person`.hospcode,
                `f_person`.pid,
                `f_person`.sitecode,
                `f_person`.ptlink,
                convert(decode(unhex(f_person.CID),sha2('$key',256)) using tis620) AS CID,
                convert(decode(unhex(f_person.Pname),sha2('$key',256)) using tis620) AS Pname,
                convert(decode(unhex(f_person.`Name`),sha2('$key',256)) using tis620) AS Name,
                convert(decode(unhex(f_person.Lname),sha2('$key',256)) using tis620) AS Lname";

            }
        }
        
        return $selectKey;
    }
    public static function getAll($sitecode, $sdate, $edate, $dp=false) {
	$sql = "SELECT
                    COUNT(*)
                FROM
                    `patient_profile_hospital`
                WHERE
                    `hospcode` = :sitecode
                ";
        if($dp){
            $selectKey = self::genSelect();
            
            $sqlDp = "SELECT
                    $selectKey
                FROM
                    `patient_profile_hospital`
                    INNER JOIN `f_person`
                    ON `patient_profile_hospital`.`hospcode` = `f_person`.`HOSPCODE`
                    AND `patient_profile_hospital`.`pid` = `f_person`.`PID`
                WHERE
                    `patient_profile_hospital`.`hospcode` = :sitecode
                    AND `f_person`.`sitecode` = :sitecode
                ";
            
            return [
                'sql' =>$sqlDp,
                'param'=>[':sitecode'=>$sitecode],
                'keyID'=>'ptlink',
                'total'=>CkdnetFunc::queryScalar($sql, [':sitecode'=>$sitecode]),
            ];
        } 
        
	return CkdnetFunc::queryScalar($sql, [':sitecode'=>$sitecode]);//':sdate' => $sdate, ':edate' => $edate, 
    }
    
    public static function getTotalPop($sitecode, $sdate, $edate, $dp=false) {
	$sql = "SELECT
                    COUNT(*)
                FROM
                    `patient_profile_hospital`
                WHERE
                    `death` = 0 AND 
                    `hospcode` = :sitecode
                ";
        if($dp){
            $selectKey = self::genSelect();
            
            $sqlDp = "SELECT
                    $selectKey
                FROM
                    `patient_profile_hospital`
                    INNER JOIN `f_person`
                    ON `patient_profile_hospital`.`hospcode` = `f_person`.`HOSPCODE`
                    AND `patient_profile_hospital`.`pid` = `f_person`.`PID`
                    AND `patient_profile_hospital`.`hospcode` = `f_person`.`sitecode`
                WHERE
                    `patient_profile_hospital`.`death` = 0 AND 
                    `patient_profile_hospital`.`hospcode` = :sitecode
                    AND `f_person`.`sitecode` = :sitecode
                ";
            
            return [
                'sql' =>$sqlDp,
                'keyID'=> function ($model) {
                    return ['sitecode'=>$model['sitecode'], 'PID'=>$model['PID'], 'HOSPCODE'=>$model['HOSPCODE']];
                },
                'param'=>[':sitecode'=>$sitecode],
                'total'=>CkdnetFunc::queryScalar($sql, [':sitecode'=>$sitecode]),
            ];
        } 
        
	return CkdnetFunc::queryScalar($sql, [':sitecode'=>$sitecode]);//':sdate' => $sdate, ':edate' => $edate, 
    }

    public static function getDmHt($sitecode, $sdate, $edate, $dp=false) {
	$sql = "SELECT
                    COUNT(*)
                FROM
                    `patient_profile_hospital`
                WHERE
                    `death` <> 1 AND (dm >= 1 OR ht >= 1) AND
                    `hospcode` = :sitecode
                ";
        
        if($dp){
            $selectKey = self::genSelect();
            
            $sqlDp = "SELECT
                    $selectKey
                FROM
                    `patient_profile_hospital`
                    INNER JOIN `f_person`
                    ON `patient_profile_hospital`.`hospcode` = `f_person`.`HOSPCODE`
                    AND `patient_profile_hospital`.`pid` = `f_person`.`PID`
                WHERE
                    `patient_profile_hospital`.`death` <> 1 AND (dm >= 1 OR ht >= 1) AND
                    `patient_profile_hospital`.`hospcode` = :sitecode
                    AND `f_person`.`sitecode` = :sitecode
                ";
            
            return [
                'sql' =>$sqlDp,
                'param'=>[':sitecode'=>$sitecode],
                'total'=>CkdnetFunc::queryScalar($sql, [':sitecode'=>$sitecode]),
            ];
        } 
        
	return CkdnetFunc::queryScalar($sql, [':sitecode'=>$sitecode]);//':sdate' => $sdate, ':edate' => $edate, 
    }
    
    public static function getNotDmHt($sitecode, $sdate, $edate, $dp=false) {
	$sql = "SELECT
                    COUNT(*)
                FROM
                    `patient_profile_hospital`
                WHERE
                    `patient_profile_hospital`.`death` = 0 AND 
                    `patient_profile_hospital`.`hospcode` = :sitecode AND
                    ((dm = 0 OR dm is null) AND (ht = 0 OR dm is null))
                ";
        
        if($dp){
            $selectKey = self::genSelect();
            
            $sqlDp = "SELECT
                    $selectKey
                FROM
                    `patient_profile_hospital`
                    INNER JOIN `f_person`
                    ON `patient_profile_hospital`.`hospcode` = `f_person`.`HOSPCODE`
                    AND `patient_profile_hospital`.`pid` = `f_person`.`PID`
                WHERE
                    `patient_profile_hospital`.`death` = 0 AND 
                    `patient_profile_hospital`.`hospcode` = :sitecode AND
                    AND `f_person`.`sitecode` = :sitecode
                    ((dm = 0 OR dm is null) AND (ht = 0 OR dm is null))
                ";
            return [
                'sql' =>$sqlDp,
                'param'=>[':sitecode'=>$sitecode],
                'total'=>CkdnetFunc::queryScalar($sql, [':sitecode'=>$sitecode]),
            ];
        } 
        
	return CkdnetFunc::queryScalar($sql, [':sitecode'=>$sitecode]);//':sdate' => $sdate, ':edate' => $edate, 
    }
    public static function getRckd($sitecode, $sdate, $edate, $dp=false) {
	$sql = "SELECT
                    COUNT(*)
                FROM
                    `patient_profile_hospital`
                WHERE
                    `patient_profile_hospital`.`death` = 0 AND 
                    `patient_profile_hospital`.`hospcode` = :sitecode AND
                    ((dm = 0 OR dm is null) AND (ht = 0 OR dm is null))  AND (reported_ckd = 1)
                ";
        
        if($dp){
            $selectKey = self::genSelect();
            
            $sqlDp = "SELECT
                    $selectKey
                FROM
                    `patient_profile_hospital`
                    INNER JOIN `f_person`
                    ON `patient_profile_hospital`.`hospcode` = `f_person`.`HOSPCODE`
                    AND `patient_profile_hospital`.`pid` = `f_person`.`PID`
                WHERE
                    `patient_profile_hospital`.`death` = 0 AND 
                    `patient_profile_hospital`.`hospcode` = :sitecode AND
                    AND `f_person`.`sitecode` = :sitecode
                    ((dm = 0 OR dm is null) AND (ht = 0 OR dm is null))  AND (reported_ckd = 1)
                ";
            
            return [
                'sql' =>$sqlDp,
                'param'=>[':sitecode'=>$sitecode],
                'total'=>CkdnetFunc::queryScalar($sql, [':sitecode'=>$sitecode]),
            ];
        } 
        
	return CkdnetFunc::queryScalar($sql, [':sitecode'=>$sitecode]);//':sdate' => $sdate, ':edate' => $edate, 
    }
    
    public static function getNRckd($sitecode, $sdate, $edate, $dp=false) {
	$sql = "SELECT
                    COUNT(*)
                FROM
                    `patient_profile_hospital`
                WHERE
                    `death` = 0 AND 
                    `hospcode` = :sitecode AND
                    (dm = 1 OR ht = 1) AND (reported_ckd = 1 OR reported_ckd is null)
                ";
        
        if($dp){
            $selectKey = self::genSelect();
            
            $sqlDp = "SELECT
                    $selectKey
                FROM
                    `patient_profile_hospital`
                    INNER JOIN `f_person`
                    ON `patient_profile_hospital`.`hospcode` = `f_person`.`HOSPCODE`
                    AND `patient_profile_hospital`.`pid` = `f_person`.`PID`
                WHERE
                    `patient_profile_hospital`.`death` = 0 AND 
                    `patient_profile_hospital`.`hospcode` = :sitecode AND
                    AND `f_person`.`sitecode` = :sitecode
                    (dm = 1 OR ht = 1) AND (reported_ckd = 1 OR reported_ckd is null)
                ";
            
            return [
                'sql' =>$sqlDp,
                'param'=>[':sitecode'=>$sitecode],
                'total'=>CkdnetFunc::queryScalar($sql, [':sitecode'=>$sitecode]),
            ];
        } 
        
	return CkdnetFunc::queryScalar($sql, [':sitecode'=>$sitecode]);//':sdate' => $sdate, ':edate' => $edate, 
    }
    
    public static function getPatientUnhex($ptlink, $hospcode) {
        $select = self::genSelectFull();
        $query = new \yii\db\Query();
        
        $query->select($select)->from('f_person')->where('f_person.hospcode = :hospcode AND f_person.ptlink=:ptlink', [':hospcode'=>$hospcode, ':ptlink'=>$ptlink]);
        $query->innerJoin('f_address', 'f_address.pid = f_person.pid AND f_person.hospcode = f_address.hospcode');
	//$sql = "SELECT * from f_person where hospcode = :hospcode AND ptlink=:ptlink";
        $sql = $query->createCommand()->rawSql;
        
	return CkdnetFunc::queryOne($sql);
    }
    
    public static function getPatientUnhexAll($ptlink, $hospcode) {
        $select = self::genSelectFull();
        $query = new \yii\db\Query();

        $query->select($select)->from('f_person')->where('f_person.hospcode = :hospcode AND f_person.ptlink=:ptlink', [':hospcode'=>$hospcode, ':ptlink'=>$ptlink]);
        $query->leftJoin('f_address', 'f_address.pid = f_person.pid AND f_person.hospcode = f_address.hospcode');
    //$sql = "SELECT * from f_person where hospcode = :hospcode AND ptlink=:ptlink";
        $sql = $query->createCommand()->rawSql;

    return CkdnetFunc::queryOne($sql);
    }

    public static function getPatient($ptlink, $hospcode) {
	$sql = "SELECT * from f_person where hospcode = :hospcode AND ptlink=:ptlink
		";
	return CkdnetFunc::queryOne($sql, [':hospcode'=>$hospcode, ':ptlink'=>$ptlink]);
    }
    
    public static function getlabCreatinine($ptlink, $hospcode, $labtest) {
	$sql = "SELECT * from f_labfu where labtest = :labtest AND hospcode = :hospcode AND ptlink=:ptlink
		";
	return CkdnetFunc::queryAll($sql, [':labtest'=>$labtest, ':hospcode'=>$hospcode, ':ptlink'=>$ptlink]);
    }
    
    public static function getlabCode($ptlink, $hospcode, $pid, $labcode) {
        //เส้นกราฟ
	$sql = "SELECT LABRESULT as result, DATE_SERV as date_serv, LABUNIT as unit 
                from f_labfu 
                where ((ptlink=:ptlink AND ptlink<>'' AND ptlink is not null) OR (sitecode = :hospcode AND hospcode = :hospcode AND pid=:pid)) AND find_in_set(labcode, '$labcode') AND LABRESULT+0 > 0
                ORDER BY date_serv
		";
	return CkdnetFunc::queryAll($sql, [':hospcode'=>$hospcode, ':ptlink'=>$ptlink, ':pid'=>$pid]);
    }
    
    public static function getlabCode2($ptlink, $hospcode, $pid, $labid, $display_type,$labname='') {
        $data1;
        $data2;
        if($display_type == '1'){
//          $sql = "SELECT LABRESULT as result, DATE_SERV as date_serv, LABUNIT as unit 
//                from f_labfu 
//                where ((ptlink=:ptlink AND ptlink<>'' AND ptlink is not null) OR (sitecode = :hospcode AND hospcode = :hospcode AND pid=:pid)) AND find_in_set(labcode, '$labcode') AND LABRESULT+0 > 0
//                ORDER BY date_serv
//		";
          $sql = "SELECT LABRESULT as result, DATE_SERV as date_serv, LABUNIT as unit FROM f_labfu l 
            WHERE( l.sitecode = :hospcode and l.hospcode = :hospcode and l.pid = :pid and 
            (l.LABCODE in (SELECT m.LABCODE from lab_map m WHERE m.tdc_lab_items_code = '$labid' and m.HOSPCODE = l.SITECODE) ))
             OR
            ((l.LABCODE in (SELECT m.LABCODE from lab_map m WHERE m.tdc_lab_items_code = '$labid' and m.HOSPCODE = l.SITECODE ) ) 
            AND l.ptlink = :ptlink) ORDER BY date_serv
		";
            $data = CkdnetFunc::queryAll($sql, [':hospcode'=>$hospcode, ':ptlink'=>$ptlink, ':pid'=>$pid]); 

        }else if($display_type == '3'){
           // $labcode = CkdnetFunc::queryOne("SELECT * FROM lab_graph_list_site WHERE ");
            $sql="select tdc_lab_unit from tdc_lab_items where graph_label = '".$labname."'";
            $query= CkdnetFunc::queryOne($sql);
            $tdc_lab_unit = $query['tdc_lab_unit']; 
            $sql = "SELECT f_service.$labname as result, f_service.DATE_SERV as date_serv,'$tdc_lab_unit' as LABUNIT 
                    from f_service 
                    where ((ptlink='".$ptlink."' AND ptlink<>'' AND ptlink is not null) 
                             OR (sitecode = '".$hospcode."' AND hospcode = '".$hospcode."' AND pid='".$pid."')) and ( f_service.$labname <> '' and f_service.$labname is not null)
                    ORDER BY date_serv asc
                    ";
            $data = CkdnetFunc::queryAll($sql);
        }
        //$data = \yii\helpers\ArrayHelper::merge($data1, $data2);
        return $data;
        
	 
    }
    public static function getserviceCode($ptlink, $hospcode, $pid, $gafcode) {
      //เส้นกราฟ
        $sql="select tdc_lab_items.tdc_lab_unit from tdc_lab_items where tdc_lab_items.graph_label = '".$gafcode."'";
        $query= CkdnetFunc::queryOne($sql);
        $tdc_lab_unit = $query['tdc_lab_unit']; 
	$sql = "SELECT f_service.$gafcode as result, f_service.DATE_SERV as date_serv,'$tdc_lab_unit' as LABUNIT 
                from f_service 
                where ((ptlink='e71d11c01aba43642ca9fa5537967d77' AND ptlink<>'' AND ptlink is not null) 
                         OR (sitecode = '10980' AND hospcode = '10980' AND pid='100')) and ( f_service.$gafcode <> '' and f_service.$gafcode is not null)
                ORDER BY date_serv asc
		";
        $data = CkdnetFunc::queryAll($sql, [':hospcode'=>$hospcode, ':ptlink'=>$ptlink, ':pid'=>$pid]);
//        if(!$data){
//           $sql = "SELECT LABRESULT as result, DATE_SERV as date_serv, LABUNIT as unit 
//                from f_labfu 
//                ORDER BY date_serv
//		";
//           $data = CkdnetFunc::queryAll($sql, [':hospcode'=>$hospcode, ':ptlink'=>$ptlink, ':pid'=>$pid]); 
//        }
	return $data;
    }
    
    public static function getlabAll($ptlink) {
	$sql = "SELECT * from f_labfu where ptlink=:ptlink
		";
	return CkdnetFunc::queryAll($sql, [':ptlink'=>$ptlink]);
    }
    
    public static function genSelectFull() {
        $convert = isset(Yii::$app->session['convert_ckd']) ? Yii::$app->session['convert_ckd'] : 0;
        $key = isset(Yii::$app->session['key_ckd']) ?Yii::$app->session['key_ckd'] : '';
        if($key!=''){
        $selectList = [
			'f_person.sitecode',
			'f_person.HOSPCODE',
			'f_person.PID',
			'f_person.HID',
			'f_person.PreName',
			'f_person.sex',
			'f_person.Birth',
			'f_person.Mstatus',
			'f_person.Occupation_Old',
			'f_person.Occupation_New',
			'f_person.Race',
			'f_person.Nation',
			'f_person.Religion',
			'f_person.Education',
			'f_person.Fstatus',
			'f_person.Couple',
			'f_person.Vstatus',
			'f_person.MoveIn',
			'f_person.Discharge',
			'f_person.Ddischarge',
			'f_person.RHGROUP',
                        'f_person.ABOGROUP',
			'f_person.Labor',
			'f_person.PassPort',
			'f_person.TypeArea',
			'f_person.D_Update',
                        'f_person.DEATH',
                        'f_person.DDEATH',
            
                        'f_address.addresstype',
                        'f_address.housetype',
                        'f_address.ROOMNO',
                        'f_address.CONDO',
                        'f_address.SOISUB',
                        'f_address.SOIMAIN',
                        'f_address.Road',
                        'f_address.villaname',
                        'f_address.Village',
                        'f_address.Tambon',
                        'f_address.Ampur',
                        'f_address.Changwat',
                        
//			'(SELECT ckd.lastegfr FROM ckd_patient_central ckd WHERE ckd.ptlink = f_person.ptlink AND ckd.hospcode = f_person.HOSPCODE) AS lastegfr',
//			'(SELECT ckd.cvdrisk FROM ckd_patient_central ckd WHERE ckd.ptlink = f_person.ptlink AND ckd.hospcode = f_person.HOSPCODE) AS cvdrisk',
//			'(SELECT ckd.cva FROM ckd_patient_central ckd WHERE ckd.ptlink = f_person.ptlink AND ckd.hospcode = f_person.HOSPCODE) AS cva',
//			'(SELECT ckd.urineprotine FROM ckd_patient_central ckd WHERE ckd.ptlink = f_person.ptlink AND ckd.hospcode = f_person.HOSPCODE) AS urineprotine',
			'f_person.ptlink'
		    ];
        
	    $selectKey = [
                "decode(unhex(f_address.`House_id`),sha2('$key',256)) AS `House_id`",
                "decode(unhex(f_address.`HouseNo`),sha2('$key',256)) AS `HouseNo`",
                "decode(unhex(f_address.`TelePhone`),sha2('$key',256)) AS `TelePhone`",
                "decode(unhex(f_address.`Mobile`),sha2('$key',256)) AS `Mobile`",
                
		"decode(unhex(f_person.Father),sha2('$key',256)) AS Father",
		"decode(unhex(f_person.Mother),sha2('$key',256)) AS Mother",
		"decode(unhex(f_person.CID),sha2('$key',256)) AS CID",
		"decode(unhex(f_person.HN),sha2('$key',256)) AS HN",
		"decode(unhex(f_person.Pname),sha2('$key',256)) AS Pname",
		"decode(unhex(f_person.`Name`),sha2('$key',256)) AS `Name`",
		"decode(unhex(f_person.Lname),sha2('$key',256)) AS Lname"
	    ];
	    
	    if ($convert==1) {
		$selectKey = [
                    "convert(decode(unhex(f_address.House_id),sha2('$key',256)) using tis620) AS House_id",
                    "convert(decode(unhex(f_address.HouseNo),sha2('$key',256)) using tis620) AS HouseNo",
                    "convert(decode(unhex(f_address.TelePhone),sha2('$key',256)) using tis620) AS TelePhone",
                    "convert(decode(unhex(f_address.Mobile),sha2('$key',256)) using tis620) AS Mobile",
                    
		    "convert(decode(unhex(f_person.Father),sha2('$key',256)) using tis620) AS Father",
		    "convert(decode(unhex(f_person.Mother),sha2('$key',256)) using tis620) AS Mother",
		    "convert(decode(unhex(f_person.CID),sha2('$key',256)) using tis620) AS CID",
		    "convert(decode(unhex(f_person.HN),sha2('$key',256)) using tis620) AS HN",
		    "convert(decode(unhex(f_person.Pname),sha2('$key',256)) using tis620) AS Pname",
		    "convert(decode(unhex(f_person.`Name`),sha2('$key',256)) using tis620) AS `Name`",
		    "convert(decode(unhex(f_person.Lname),sha2('$key',256)) using tis620) AS Lname"
		];
	    }
	    
	    $selectList = array_merge($selectList, $selectKey);
        } else {
            $selectList = [
//		'(SELECT ckd.lastegfr FROM ckd_patient_central ckd WHERE ckd.ptlink = f_person.ptlink AND ckd.hospcode = f_person.HOSPCODE) AS lastegfr',
//		'(SELECT ckd.cvdrisk FROM ckd_patient_central ckd WHERE ckd.ptlink = f_person.ptlink AND ckd.hospcode = f_person.HOSPCODE) AS cvdrisk',
//		'(SELECT ckd.cva FROM ckd_patient_central ckd WHERE ckd.ptlink = f_person.ptlink AND ckd.hospcode = f_person.HOSPCODE) AS cva',
//		'(SELECT ckd.urineprotine FROM ckd_patient_central ckd WHERE ckd.ptlink = f_person.ptlink AND ckd.hospcode = f_person.HOSPCODE) AS urineprotine',
		'f_person.*'
	    ];
        }
        return $selectList;
    }
    
    public static function getService($ptlink, $hospcode) {
	$sql = "SELECT * 
                FROM f_service 
                WHERE (ptlink=:ptlink AND ptlink<>'' AND ptlink is not null) OR (hospcode = :hospcode AND pid=:pid)
                ORDER BY date_serv desc
		";
	return CkdnetFunc::queryAll($sql, [':ptlink'=>$ptlink, ':hospcode'=>$hospcode, ':pid'=>Yii::$app->session['emr_pid']]);
    }
    
    public static function getServiceLimit($ptlink, $hospcode, $limit=0) {
	$sql = "SELECT * 
                FROM f_service 
                WHERE (ptlink=:ptlink AND ptlink<>'' AND ptlink is not null) OR (hospcode = :hospcode AND pid=:pid)
                ORDER BY date_serv desc
                limit $limit, 10
		";
	return CkdnetFunc::queryAll($sql, [':ptlink'=>$ptlink, ':hospcode'=>$hospcode, ':pid'=>Yii::$app->session['emr_pid']]);
    }
    
    public static function getDiagDM($ptlink, $hospcode) {
	$sql = "SELECT * , CHRONIC as diag_code
                FROM f_chronic 
                WHERE (ptlink=:ptlink AND ptlink<>'' AND ptlink is not null) OR (hospcode = :hospcode AND pid=:pid)
                AND (CHRONIC LIKE 'E10%' OR CHRONIC LIKE 'E11%' OR CHRONIC LIKE 'E12%' OR CHRONIC LIKE 'E13%' OR CHRONIC LIKE 'E14%') 
                ORDER BY D_UPDATE desc
		";
        $data = CkdnetFunc::queryOne($sql, [':ptlink'=>$ptlink, ':hospcode'=>$hospcode, ':pid'=>Yii::$app->session['emr_pid']]);
        if($data){
            return $data;
        } else {
            $sql = "SELECT * , DiagCode as diag_code
                    FROM f_diagnosis_opd 
                    WHERE (ptlink=:ptlink AND ptlink<>'' AND ptlink is not null) OR (hospcode = :hospcode AND pid=:pid)
                    AND (DiagCode LIKE 'E10%' OR DiagCode LIKE 'E11%' OR DiagCode LIKE 'E12%' OR DiagCode LIKE 'E13%' OR DiagCode LIKE 'E14%') 
                    ORDER BY date_serv desc
                    ";
            $data = CkdnetFunc::queryOne($sql, [':ptlink'=>$ptlink, ':hospcode'=>$hospcode, ':pid'=>Yii::$app->session['emr_pid']]);
            
            if($data){
                return $data;
            } else {
                $sql = "SELECT * , DiagCode as diag_code
                        FROM f_diagnosis_ipd 
                        WHERE (ptlink=:ptlink AND ptlink<>'' AND ptlink is not null) OR (hospcode = :hospcode AND pid=:pid)
                        AND (DiagCode LIKE 'E10%' OR DiagCode LIKE 'E11%' OR DiagCode LIKE 'E12%' OR DiagCode LIKE 'E13%' OR DiagCode LIKE 'E14%')
                        ORDER BY D_UPDATE desc
                        ";
                $data = CkdnetFunc::queryOne($sql, [':ptlink'=>$ptlink, ':hospcode'=>$hospcode, ':pid'=>Yii::$app->session['emr_pid']]);
                
                if($data){
                    return $data;
                } 
            }
        }
        
	return $data;
    }
    
    public static function getDiagHT($ptlink, $hospcode) {
	$sql = "SELECT * , CHRONIC as diag_code
                FROM f_chronic 
                WHERE (ptlink=:ptlink AND ptlink<>'' AND ptlink is not null) OR (hospcode = :hospcode AND pid=:pid)
                AND (CHRONIC LIKE 'I10%' OR CHRONIC LIKE 'I11%' OR CHRONIC LIKE 'I12%' OR CHRONIC LIKE 'I13%' OR CHRONIC LIKE 'I14%' OR CHRONIC LIKE 'I15%') 
                ORDER BY D_UPDATE desc
		";
        $data = CkdnetFunc::queryOne($sql, [':ptlink'=>$ptlink, ':hospcode'=>$hospcode, ':pid'=>Yii::$app->session['emr_pid']]);
        if($data){
            return $data;
        } else {
            $sql = "SELECT * , DiagCode as diag_code
                    FROM f_diagnosis_opd 
                    WHERE (ptlink=:ptlink AND ptlink<>'' AND ptlink is not null) OR (hospcode = :hospcode AND pid=:pid)
                    AND (DiagCode LIKE 'I10%' OR DiagCode LIKE 'I11%' OR DiagCode LIKE 'I12%' OR DiagCode LIKE 'I13%' OR DiagCode LIKE 'I14%' OR DiagCode LIKE 'I15%') 
                    ORDER BY date_serv desc
                    ";
            $data = CkdnetFunc::queryOne($sql, [':ptlink'=>$ptlink, ':hospcode'=>$hospcode, ':pid'=>Yii::$app->session['emr_pid']]);
            
            if($data){
                return $data;
            } else {
                $sql = "SELECT * , DiagCode as diag_code
                        FROM f_diagnosis_ipd 
                        WHERE (ptlink=:ptlink AND ptlink<>'' AND ptlink is not null) OR (hospcode = :hospcode AND pid=:pid)
                        AND (DiagCode LIKE 'I10%' OR DiagCode LIKE 'I11%' OR DiagCode LIKE 'I12%' OR DiagCode LIKE 'I13%' OR DiagCode LIKE 'I14%' OR DiagCode LIKE 'I15%') 
                        ORDER BY D_UPDATE desc
                        ";
                $data = CkdnetFunc::queryOne($sql, [':ptlink'=>$ptlink, ':hospcode'=>$hospcode, ':pid'=>Yii::$app->session['emr_pid']]);
                
                if($data){
                    return $data;
                } 
            }
        }
        
	return $data;
    }
    
    public static function getDrugallergy($ptlink, $hospcode) {
	$sql = "SELECT * 
                FROM f_drugallergy 
                WHERE (ptlink=:ptlink AND ptlink<>'' AND ptlink is not null) OR (hospcode = :hospcode AND pid=:pid)
                ORDER BY D_UPDATE desc
		";
	return CkdnetFunc::queryAll($sql, [':ptlink'=>$ptlink, ':hospcode'=>$hospcode, ':pid'=>Yii::$app->session['emr_pid']]);
    }
    
    public static function getDrugOpd($ptlink, $hospcode, $date_serv) {
	$sql = "SELECT * 
                FROM f_drug_opd 
                WHERE (hospcode = :hospcode AND pid=:pid AND Date_Serv = :date_serv ) 
                ORDER BY date_serv desc
		";
	return CkdnetFunc::queryAll($sql, [':hospcode'=>$hospcode, ':date_serv'=>$date_serv, ':pid'=>Yii::$app->session['emr_pid']]);
    }
    
    public static function getDrugOpdAll($ptlink, $hospcode) {
	$sql = "SELECT * 
                FROM f_drug_opd 
                WHERE (ptlink=:ptlink AND ptlink<>'' AND ptlink is not null) OR (hospcode = :hospcode AND pid=:pid)
                ORDER BY date_serv desc
                limit 1000
		";
	return CkdnetFunc::queryAll($sql, [':ptlink'=>$ptlink, ':hospcode'=>$hospcode, ':pid'=>Yii::$app->session['emr_pid']]);
    }
    
    
    
    public static function getDrugIpd($ptlink, $hospcode, $date_serv) {
	$sql = "SELECT *,DATETIME_ADMIT as DATE_SERV 
                FROM f_drug_ipd 
                WHERE (hospcode = :hospcode AND pid=:pid AND DATE(DATETIME_ADMIT) = :date_serv ) 
                ORDER BY DATETIME_ADMIT desc
		";
	return CkdnetFunc::queryAll($sql, [':hospcode'=>$hospcode, ':date_serv'=>$date_serv, ':pid'=>Yii::$app->session['emr_pid']]);
    }
    
    public static function getDrugIpdAll($ptlink, $hospcode) {
	$sql = "SELECT * ,DATETIME_ADMIT as DATE_SERV
                FROM f_drug_ipd 
                WHERE (ptlink=:ptlink AND ptlink<>'' AND ptlink is not null) OR (hospcode = :hospcode AND pid=:pid)
                ORDER BY DATETIME_ADMIT desc
                limit 1000
		";
	return CkdnetFunc::queryAll($sql, [':ptlink'=>$ptlink, ':hospcode'=>$hospcode, ':pid'=>Yii::$app->session['emr_pid']]);
    }
    
    public static function getProvinceAmphur($code) {
	$sql = "SELECT
                `const_province`.`PROVINCE_NAME` as province,
                `const_amphur`.`AMPHUR_NAME` as amphur,
                `const_district`.`DISTRICT_NAME` as district,
                `const_zipcodes`.`zipcode`
                FROM
                `const_province`
                JOIN `const_amphur`
                ON `const_province`.`PROVINCE_ID` = `const_amphur`.`PROVINCE_ID` 
                JOIN `const_district`
                ON `const_amphur`.`AMPHUR_ID` = `const_district`.`AMPHUR_ID` 
                JOIN `const_zipcodes`
                ON `const_district`.`DISTRICT_CODE` = `const_zipcodes`.`district_code`
                WHERE
                `const_district`.`DISTRICT_CODE`=:code
		";
	return Yii::$app->db->createCommand($sql, [':code'=>$code])->queryOne();
    }
    
    public static function getDiagOne($serv, $ptlink, $hospcode) {
	$sql = "SELECT * , DiagCode as diag_code
                FROM f_diagnosis_opd 
                WHERE (hospcode = :hospcode AND pid=:pid AND Date_Serv = :serv)
                ORDER BY DiagType, Date_Serv
		";
        $data = CkdnetFunc::queryAll($sql, [':serv'=>$serv, ':hospcode'=>$hospcode, ':pid'=>Yii::$app->session['emr_pid']]);
        
	return $data;
    }
    
    public static function getIcd10($code) {
	$sql = "SELECT *
                FROM
                    icd10
                WHERE
                `code`=:code
		";
	return Yii::$app->db->createCommand($sql, [':code'=>$code])->queryOne();
    }
    
    public static function getMapLab($hospcode) {
        //label graph
	$sql = "SELECT  GROUP_CONCAT(LABCODE) as labcode, graph_label as labname , tdc_lab_items_code as labid,hos_lab_unit as labunit
                FROM lab_map 
                where hospcode = :hospcode AND (labcode<>'' and labcode is not null)
                GROUP BY tdc_lab_items_code
                ORDER BY hos_lab_items_name
		";
        $data = CkdnetFunc::queryAll($sql, [':hospcode'=>$hospcode]);
        $sql2 = "SELECT  GROUP_CONCAT(tdc_lab_code) as labcode, graph_label as labname , tdc_lab_code as labid,tdc_lab_unit as labunit
                FROM tdc_lab_items_site 
                where hospcode = :hospcode AND (tdc_lab_code<>'' and tdc_lab_code is not null) AND display_type=3
                GROUP BY tdc_lab_code
                ORDER BY graph_label
		";
        $data2 = CkdnetFunc::queryAll($sql2, [':hospcode'=>$hospcode]);
        $data = array_merge($data, $data2); 

	return $data;
    }
    
    public static function getMapLab2($hospcode,$pid) {
//	$sql = "SELECT  GROUP_CONCAT(LABCODE) as labcode, graph_label as labname , tdc_lab_items_code as labid,hos_lab_unit as labunit
//                FROM lab_map 
//                where hospcode = :hospcode AND (labcode<>'' and labcode is not null)
//                GROUP BY tdc_lab_items_code
//                ORDER BY hos_lab_items_name
//		";
        $sql = "SELECT  GROUP_CONCAT(tdc_lab_code) as labcode, graph_label as labname , tdc_lab_code as labid,tdc_lab_unit as labunit
                FROM tdc_lab_items_site 
                where hospcode = :hospcode AND (tdc_lab_code<>'' and tdc_lab_code is not null) AND display_type=3
                GROUP BY tdc_lab_code
                ORDER BY graph_label
		";
        $data = CkdnetFunc::queryAll($sql, [':hospcode'=>$hospcode]);
        
         
        
	return $data;
    }
    
    
    
    public static function getListGraphOne($gid,$id) {
   
        if($gid == 99){
           $sql = "SELECT
                `lab_graph_group_site`.`group`,
                `lab_graph_list_site`.`id`,
                `lab_graph_list_site`.`graph_name`,
                `lab_graph_list_site`.`cols`,
                `lab_graph_list_site`.`tdc_lab_item_code`
                FROM
                `lab_graph_group_site`
                JOIN `lab_graph_list_site`
                ON `lab_graph_group_site`.`id` = `lab_graph_list_site`.`group_id`
                WHERE
                `lab_graph_list_site`.`id` = '".$id."'
                order by `lab_graph_list_site`.`id` DESC
                 
		";
            $data = CkdnetFunc::queryAll($sql);  
        }else{
             $sql = "SELECT
                `lab_graph_group_site`.`group`,
                `lab_graph_list_site`.`id`,
                `lab_graph_list_site`.`graph_name`,
                `lab_graph_list_site`.`cols`,
                `lab_graph_list_site`.`tdc_lab_item_code`
                FROM
                `lab_graph_group_site`
                JOIN `lab_graph_list_site`
                ON `lab_graph_group_site`.`id` = `lab_graph_list_site`.`group_id`
                WHERE
                `lab_graph_list_site`.`group_id` = '".$gid."' AND `lab_graph_list_site`.`id` = '".$id."'
                order by `lab_graph_list_site`.`id` DESC
		";
            $data = CkdnetFunc::queryAll($sql);
        }    
        
//          $sql = "SELECT
//                `lab_graph_group_site`.`group`,
//                `lab_graph_list_site`.`id`,
//                `lab_graph_list_site`.`graph_name`,
//                `lab_graph_list_site`.`cols`,
//                `lab_graph_list_site`.`tdc_lab_item_code`
//                FROM
//                `lab_graph_group_site`
//                JOIN `lab_graph_list_site`
//                ON `lab_graph_group_site`.`id` = `lab_graph_list_site`.`group_id`
//                WHERE
//                `lab_graph_list_site`.`group_id` = '".$gid."' AND `lab_graph_list_site`.`id` = '".$id."'
//		";
//            $data = CkdnetFunc::queryAll($sql);
	return $data;
    }
    
    public static function getListGraph($gid,$number = null) {
        if(empty($number) || $number == null){
            $number=2;
        }
        if($gid == 99){
           $sql = "SELECT
                `lab_graph_group_site`.`group`,
                `lab_graph_list_site`.`id`,
                `lab_graph_list_site`.`graph_name`,
                `lab_graph_list_site`.`cols`,
                `lab_graph_list_site`.`tdc_lab_item_code`
                FROM
                `lab_graph_group_site`
                JOIN `lab_graph_list_site`
                ON `lab_graph_group_site`.`id` = `lab_graph_list_site`.`group_id`
                AND `lab_graph_list_site`.`hospcode`=:hospcode
                GROUP BY `lab_graph_list_site`.`graph_name`
                order by `lab_graph_list_site`.`forder` ASC
               
		";
           //LIMIT 1,4 // LIMIT 0,$number
            $data = CkdnetFunc::queryAll($sql,[
                ':hospcode'=>\Yii::$app->session['dynamic_connection']['sitecode']
            ]);  
        }else{
             $sql = "SELECT
                `lab_graph_group_site`.`group`,
                `lab_graph_list_site`.`id`,
                `lab_graph_list_site`.`graph_name`,
                `lab_graph_list_site`.`cols`,
                `lab_graph_list_site`.`tdc_lab_item_code`
                FROM
                `lab_graph_group_site`
                JOIN `lab_graph_list_site`
                ON `lab_graph_group_site`.`id` = `lab_graph_list_site`.`group_id`
                WHERE
                `lab_graph_list_site`.`group_id` = :gid
                AND `lab_graph_list_site`.`hospcode`=:hospcode
                GROUP BY `lab_graph_list_site`.`graph_name`
                order by `lab_graph_list_site`.`forder` ASC
             
		";
            $data = CkdnetFunc::queryAll($sql, [':gid'=>$gid,':hospcode'=>\Yii::$app->session['dynamic_connection']['sitecode']]);
        }
         
             //WHERE `lab_graph_group_site`.`hospcode`=:hospcode
            if(!$data){
                $sql = "SELECT
                `lab_graph_group_site`.`group`,
                `lab_graph_list`.`id`,
                `lab_graph_list`.`graph_name`,
                `lab_graph_list`.`cols`,
                `lab_graph_list`.`tdc_lab_item_code`
                FROM
                `lab_graph_group_site`
                JOIN `lab_graph_list`
                ON `lab_graph_group_site`.`id` = `lab_graph_list`.`group_id`
                WHERE
                `lab_graph_list`.`group_id` = :gid
                AND `lab_graph_group_site`.`hospcode`=:hospcode
		";
            $data = CkdnetFunc::queryAll($sql, [
                ':gid'=>$gid,
                ':hospcode'=>\Yii::$app->session['dynamic_connection']['sitecode']
            ]);
            }
          
        
        
	
	return $data;
    }
    
    
    
    
    //อันเดียว
    
    public static function getListGraph2($gid,$id) {
        
             $sql = "SELECT
                `lg`.`group`,
                `lgs`.`id`,
                `lgs`.`graph_name`,
                `lgs`.`cols`,
                `lgs`.`tdc_lab_item_code`
                FROM
                `lab_graph_group_site` as `lg`
                JOIN `lab_graph_list_site` as `lgs`
                ON `lg`.`id` = `lgs`.`group_id`
                WHERE
                `lgs`.`group_id` = '".$gid."'
                AND 
                `lgs`.`id` = '".$id."'
                
		";
            $data = CkdnetFunc::queryAll($sql);
        
	
	return $data;
    }
    
    public static function getGraph() {
        $sql = "SELECT
                *
                FROM
                `lab_graph_group_site` order by forder asc
		";
        $data = CkdnetFunc::queryAll($sql);
        if(empty($data)){
           $sql = "SELECT
                *
                FROM
                `lab_graph_group`
		";
            $data = CkdnetFunc::queryAll($sql); 
        }
	
        
	return $data;
    }
    
    public static function getUrine($ptid) {
	$sql = "SELECT ckdpd03_date AS date_serv, urine_vol AS result
                FROM
                    tbdata_1484472652049461000
                WHERE 
                    ckdpd03_date is not null AND urine_vol is not null AND urine_vol+0 > 0 AND ptid = :ptid
		";
	return Yii::$app->dbwebs1->createCommand($sql, [':ptid'=>$ptid])->queryAll();
    }
    
    public static function getBw($ptid) {
	$sql = "SELECT ckdpd03_date AS date_serv, weight AS result
                FROM
                    tbdata_1484472652049461000
                WHERE 
                    ckdpd03_date is not null AND weight is not null AND weight+0 > 0 AND ptid = :ptid
		";
	return Yii::$app->dbwebs1->createCommand($sql, [':ptid'=>$ptid])->queryAll();
    }
    
    public static function getUf($ptid) {
	$sql = "SELECT ckdpd02date AS date_serv,
                   (IFNULL(r1outvol,0)+IFNULL(r2outvol,0)+IFNULL(r3outvol,0)+IFNULL(r4outvol,0)+IFNULL(r5outvol,0)+IFNULL(r6outvol,0)+IFNULL(r7outvol,0)+IFNULL(r8outvol,0)+IFNULL(r9outvol,0) ) - (IFNULL(r1invol,0)+IFNULL(r2invol,0)+IFNULL(r3invol,0)+IFNULL(r4invol,0)+IFNULL(r5invol,0)+IFNULL(r6invol,0)+IFNULL(r7invol,0)+IFNULL(r8invol,0)+IFNULL(r9invol,0)) AS result
                FROM
                    tbdata_1484716303035847800
                WHERE 
                    ckdpd02date is not null AND ptid = :ptid
		";
	return Yii::$app->dbwebs1->createCommand($sql, [':ptid'=>$ptid])->queryAll();
    }
    
    public static function getPerson($cid) {
	$sql = "SELECT ptid
                FROM
                    tb_data_coc
                WHERE     
                    cid =:cid
		";
        
	return Yii::$app->dbwebs1->createCommand($sql, [':cid'=>$cid])->queryScalar();
    }
    
  public static function getPatientProfileHospitalField($hospcode, $pid, $field) {
        
 $sql = "SELECT ckd.$field FROM patient_profile_hospital ckd WHERE ckd.hospcode = $hospcode AND ckd.pid = $pid";
        //$datatest = Yii::$app->getDb()->createCommand($sql)->queryAll();
        $datatest = CkdnetFunc::queryScalar($sql,[':hospcode'=>$hospcode,':pid'=>$pid]);
//        \yii\helpers\VarDumper::dump($datatest); exit();
        return $datatest; 
    }
    
    
    public static function getCvdrisk($pid, $hospcode, $date_serv, $field) { 
    $sql = "SELECT ckd.$field from f_cvd_risk ckd where hospcode = $hospcode AND pid = $pid AND date_serv <= '$date_serv' and ckd.$field is not null order by ckd.date_serv desc limit 1"; 
        $data = CkdnetFunc::queryOne($sql); 
        return $data ; 
    }
    
    public static function getCvdall($ptlink, $hospcode,$limit=0) { 
    $sql = "SELECT *  
                   FROM f_cvd_risk 
                   WHERE (ptlink=:ptlink AND ptlink<>'' AND ptlink is not null) OR (hospcode = :hospcode AND pid=:pid) 
                   ORDER BY date_serv desc limit $limit, 10
     "; 
    return CkdnetFunc::queryAll($sql, [':ptlink'=>$ptlink, ':hospcode'=>$hospcode, ':pid'=>Yii::$app->session['emr_pid']]); 
    }
    
    
}
