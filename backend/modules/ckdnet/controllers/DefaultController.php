<?php

namespace backend\modules\ckdnet\controllers;

use yii\web\Controller;
use Yii;


class DefaultController extends Controller
{
    public function actionIndex()
    {
        $active['home']=true;
        $items = [
            [
                'label'=>'<i class="fa fa-home"></i> หน้าแรก',
                'active'=>$active['home'],
                'linkOptions'=>['data-url'=>\yii\helpers\Url::to(['/ckdnet/default/redirect'])]                           
            ],
            [
                'label'=>'<i class="fa fa-check-square-o"></i> กำกับงาน',
                'active'=>$active['workspace'],
                'linkOptions'=>['data-url'=>\yii\helpers\Url::to(['/ckdnet/workspace/redirect'])]                
            ],
            [
                'label'=>'<i class="fa fa-male"></i> EMR',
                'active'=>$active['emr'],
                'linkOptions'=>['data-url'=>\yii\helpers\Url::to(['/ckdnet/emr/redirect'])]
            ],
            [
                'label'=>'<i class="fa fa-area-chart"></i> รายงาน',
                'active'=>$active['report'],
                'linkOptions'=>['data-url'=>\yii\helpers\Url::to(['/ckdnet/report/redirect'])]                
            ],
            [
                'label'=>'<i class="fa fa-map-marker"></i> แผนที่',
                'active'=>$active['map'],
                'linkOptions'=>['data-url'=>\yii\helpers\Url::to(['/ckdnet/map/redirect'])]                
            ],
            [
                'label'=>'<i class="fa fa-table"></i> Matrix',
                'active'=>$active['matrix'],
                'linkOptions'=>['data-url'=>\yii\helpers\Url::to(['/ckdnet/matrix/redirect'])]
            ],
        ];        
        return $this->render('index_new',
            [
                'items' =>  $items,
            ]);
    }
    public function actionRedirect() {
        return $this->redirect('/ckdnet/',302);
    }    
}
