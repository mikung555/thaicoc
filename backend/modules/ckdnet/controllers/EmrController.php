<?php

namespace backend\modules\ckdnet\controllers;
use Yii;
use yii\helpers\Json;
use appxq\sdii\helpers\SDHtml;
use yii\web\Response;

class EmrController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $request = Yii::$app->request;
        
        $hospcode=$request->get('hospcode');
        $pid=$request->get('pid');
        
        $active['emr']=true;
        $items = [
            [
                'label'=>'<i class="fa fa-home"></i> หน้าแรก',
                'active'=>$active['home'],
                'linkOptions'=>['data-url'=>\yii\helpers\Url::to(['/ckdnet/default/redirect'])]                           
            ],
            [
                'label'=>'<i class="fa fa-check-square-o"></i> กำกับงาน',
                'active'=>$active['workspace'],
                'linkOptions'=>['data-url'=>\yii\helpers\Url::to(['/ckdnet/workspace/redirect'])]                
            ],
            [
                'label'=>'<i class="fa fa-male"></i> EMR',
                'active'=>$active['emr'],
                'linkOptions'=>['data-url'=>\yii\helpers\Url::to(['/ckdnet/emr/redirect'])]
            ],
            [
                'label'=>'<i class="fa fa-area-chart"></i> รายงาน',
                'active'=>$active['report'],
                'linkOptions'=>['data-url'=>\yii\helpers\Url::to(['/ckdnet/report/redirect'])]                
            ],
            [
                'label'=>'<i class="fa fa-map-marker"></i> แผนที่',
                'active'=>$active['map'],
                'linkOptions'=>['data-url'=>\yii\helpers\Url::to(['/ckdnet/map/redirect'])]                
            ],
            [
                'label'=>'<i class="fa fa-table"></i> Matrix',
                'active'=>$active['matrix'],
                'linkOptions'=>['data-url'=>\yii\helpers\Url::to(['/ckdnet/matrix/redirect'])]
            ],
        ];        
        $tab = Yii::$app->request->get('tab');
        return $this->render('index',
            [
                'items' =>  $items,
                'tab'   =>  $tab,
                'hospcode' => $hospcode,
                'pid' => $pid,
            ]);
    }
    public function actionRedirect() {
        return $this->redirect('/ckdnet/emr',302);
    }
    public function actionPhr() {
        $request = Yii::$app->request;
        $hospcode=$request->get('hospcode');
        $pid=$request->get('pid');
        
        return Json::encode($this->renderPartial('phr',
                [
                'hospcode' => $hospcode,
                'pid' => $pid,
                ]));
    }
    public function actionCkd() {
        $request = Yii::$app->request;
        $hospcode=$request->get('hospcode');
        $pid=$request->get('pid');
        
        return Json::encode($this->renderPartial('ckd',
                [
                'hospcode' => $hospcode,
                'pid' => $pid,
                ]));                
                
    }
    
    public function actionAddkey() {
	if (Yii::$app->getRequest()->isAjax) {
            
            if (isset($_COOKIE['save_keyckd']) && $_COOKIE['save_keyckd']==1) {
                if (isset($_COOKIE['key_ckd'])) {
                    Yii::$app->session['key_ckd'] = $_COOKIE['key_ckd'];
                }
                if (isset($_COOKIE['convert_ckd'])) {
                    Yii::$app->session['convert_ckd'] = $_COOKIE['convert_ckd'];
                }
                if (isset($_COOKIE['save_keyckd'])) {
                    Yii::$app->session['save_keyckd'] = $_COOKIE['save_keyckd'];
                }
            } else {
                Yii::$app->session['key_ckd'] = '';
                Yii::$app->session['convert_ckd'] = 0;
                Yii::$app->session['save_keyckd'] = 0;
            }
	    
	    if (isset($_POST['key'])) {
		Yii::$app->response->format = Response::FORMAT_JSON;

                if (isset($_POST['save_key']) && $_POST['save_key']==1) {
                    setcookie("save_keyckd", $_POST['save_key'], time()+3600*24*30, "/", Yii::$app->keyStorage->get('frontend.domain'), false);
                    setcookie("key_ckd", $_POST['key'], time()+3600*24*30, "/", Yii::$app->keyStorage->get('frontend.domain'), false);
                    setcookie("convert_ckd", $_POST['convert'], time()+3600*24*30, "/", Yii::$app->keyStorage->get('frontend.domain'), false);


                } else {
                    Yii::$app->session['key_ckd'] = '';
                    Yii::$app->session['convert_ckd'] = 0;
                    Yii::$app->session['save_keyckd'] = 0;

                    setcookie('save_keyckd', null, time()-1, "/", Yii::$app->keyStorage->get('frontend.domain'), false);
                    setcookie('key_ckd', null, time()-1, "/", Yii::$app->keyStorage->get('frontend.domain'), false);
                    setcookie('convert_ckd', null, time()-1, "/", Yii::$app->keyStorage->get('frontend.domain'), false);
		}
		
		Yii::$app->session['key_ckd'] = $_POST['key'];
                Yii::$app->session['convert_ckd'] = $_POST['convert'];
                Yii::$app->session['save_keyckd'] = $_POST['save_key'];

		$result = [
		    'status' => 'success',
		    'action' => 'create',
		    'message' => SDHtml::getMsgSuccess() . Yii::t('app', 'Data completed.'),
		];
		return $result;
	    } else {
		return $this->renderAjax('_form_key', [
			    'model' => $model,
		]);
	    }
	} else {
            $result = [
                'status' => 'error',
                'action' => 'request',
                'message' => \appxq\sdii\helpers\SDHtml::getMsgError() . Yii::t('app', 'Invalid request. Please do not repeat this request again.'),
            ];
            return $result;
	}
    }
    
    
    
}
