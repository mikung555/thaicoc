<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use appxq\sdii\helpers\SDNoty;
use appxq\sdii\helpers\SDHtml;

/* @var $this yii\web\View */
/* @var $model backend\modules\ovcca\models\TmpPerson */
/* @var $form yii\bootstrap\ActiveForm */
?>

<div class="tmp-person-form">

    <?php $form = ActiveForm::begin([
	'id'=>'add-key',
    ]); ?>

    <div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	<h4 class="modal-title" id="itemModalLabel">โปรดกรอกรหัสที่กำหนดใน TDC</h4>
    </div>

    <div class="modal-body">
    <?=  Html::img('/img/add_key_tcc.png', ['class'=>'img-responsive'])?>
    <hr>
    
    <?=  Html::label('กุญแจถอดรหัสข้อมูล')?>
	<?=  Html::passwordInput('key', isset(Yii::$app->session['key_ckd'])?Yii::$app->session['key_ckd']:'', ['class'=>'form-control', 'placeholder'=> 'กรุณากรอกกุญแจถอดรหัส'])?>
	<br>
	<?= Html::checkbox('save_key', isset($_COOKIE['save_keyckd'])?$_COOKIE['save_keyckd']:false, ['label'=>'จดจำคีย์นี้'])?>
	 <?= Html::checkbox('convert', isset(Yii::$app->session['convert_ckd'])?Yii::$app->session['convert_ckd']:false, ['label'=>'เข้ารหัสแบบ tis620 (กรณีชื่อ-สกุล อ่านไม่ออกให้กาช่องนี้ด้วย)'])?>
    </div>
    
    <div class="modal-footer">
	<?= Html::submitButton(Yii::t('app', 'ถอดรหัส'), ['class' => 'btn btn-primary']) ?>
	<?= Html::resetButton(Yii::t('app', 'ยกเลิก'), ['class' => 'btn btn-default', 'data-dismiss'=>'modal']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php
 
$this->registerJs("
$('form#add-key').on('beforeSubmit', function(e) {
    var \$form = $(this);
    $.post(
	\$form.attr('action'), //serialize Yii2 form
	\$form.serialize()
    ).done(function(result) {
	if(result.status == 'success') {
	    ". SDNoty::show('result.message', 'result.status') ."
	    $(document).find('#modal-decode').modal('hide');
	    window.location.reload();
	} else {
	    ". SDNoty::show('result.message', 'result.status') ."
	} 
    }).fail(function() {
	". SDNoty::show("'" . SDHtml::getMsgError() . "Server Error'", '"error"') ."
	console.log('server error');
    });
    return false;
});

");?>