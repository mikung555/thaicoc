<?php
use kartik\tabs\TabsX;

/* @var $this yii\web\View */
$this->title = Yii::t('backend', 'Module: CKDNET');
$this->params['breadcrumbs'][] = ['label'=>'Modules','url'=>  yii\helpers\Url::to('/tccbots/my-module')];
$this->params['breadcrumbs'][] = Yii::t('backend', 'CKDNET');



?>
<div class="ckdnet-default-index">
<?php
echo TabsX::widget([
    'items'=>$items,
    'position'=>TabsX::POS_ABOVE,
    'encodeLabels'=>false
]);
?>
</div>
<iframe src="http://hdcservice.moph.go.th/hdc/reports/page.php?cat_id=e71a73a77b1474e63b71bccf727009ce" style="border: 0;" width="100%" height="1000px"> </iframe>