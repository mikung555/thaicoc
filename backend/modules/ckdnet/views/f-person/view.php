<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\ckdnet\models\FPerson */

$this->title = 'Fperson#'.$model->Name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Fpeople'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="fperson-view">

    <div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	<h4 class="modal-title" id="itemModalLabel"><?= Html::encode($this->title) ?></h4>
    </div>
    <div class="modal-body">
        <?= DetailView::widget([
	    'model' => $model,
	    'attributes' => [
		'sitecode',
		'HOSPCODE',
		'CID',
		'PID',
		'HID',
		'PreName',
		'Pname',
		'Name',
		'Lname',
		'HN',
		'sex',
		'Birth',
		'Mstatus',
		'Occupation_Old',
		'Occupation_New',
		'Race',
		'Nation',
		'Religion',
		'Education',
		'Fstatus',
		'Father',
		'Mother',
		'Couple',
		'Vstatus',
		'MoveIn',
		'Discharge',
		'Ddischarge',
		'ABOGROUP',
		'RHGROUP',
		'Labor',
		'PassPort',
		'TypeArea',
		'D_Update',
		'ptlink',
	    ],
	]) ?>
    </div>
</div>
