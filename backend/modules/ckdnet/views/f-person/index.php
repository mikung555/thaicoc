<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\helpers\Url;
use appxq\sdii\widgets\GridView;
use appxq\sdii\widgets\ModalForm;
use appxq\sdii\helpers\SDNoty;
use appxq\sdii\helpers\SDHtml;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\ckdnet\models\FPersonSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Fpeople');
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="fperson-index">
    

    <?php  Pjax::begin(['id'=>'fperson-grid-pjax']);?>
    <?= GridView::widget([
	'id' => 'fperson-grid',
	'panelBtn' => Html::button(SDHtml::getBtnAdd(), ['data-url'=>Url::to(['fperson/create']), 'class' => 'btn btn-success btn-sm', 'id'=>'modal-addbtn-fperson']). ' ' .
		      Html::button(SDHtml::getBtnDelete(), ['data-url'=>Url::to(['fperson/deletes']), 'class' => 'btn btn-danger btn-sm', 'id'=>'modal-delbtn-fperson', 'disabled'=>true]),
	'dataProvider' => $dataProvider,
	'filterModel' => $searchModel,
        'columns' => [
	    [
		'class' => 'yii\grid\CheckboxColumn',
		'checkboxOptions' => [
		    'class' => 'selectionFPersonIds'
		],
		'headerOptions' => ['style'=>'text-align: center;'],
		'contentOptions' => ['style'=>'width:40px;text-align: center;'],
	    ],
	    [
		'class' => 'yii\grid\SerialColumn',
		'headerOptions' => ['style'=>'text-align: center;'],
		'contentOptions' => ['style'=>'width:60px;text-align: center;'],
	    ],

            'sitecode',
            'HOSPCODE',
            'CID',
            'PID',
            'HID',
            // 'PreName',
            // 'Pname',
            // 'Name',
            // 'Lname',
            // 'HN',
            // 'sex',
            // 'Birth',
            // 'Mstatus',
            // 'Occupation_Old',
            // 'Occupation_New',
            // 'Race',
            // 'Nation',
            // 'Religion',
            // 'Education',
            // 'Fstatus',
            // 'Father',
            // 'Mother',
            // 'Couple',
            // 'Vstatus',
            // 'MoveIn',
            // 'Discharge',
            // 'Ddischarge',
            // 'ABOGROUP',
            // 'RHGROUP',
            // 'Labor',
            // 'PassPort',
            // 'TypeArea',
            // 'D_Update',
            // 'ptlink',

	    [
		'class' => 'appxq\sdii\widgets\ActionColumn',
		'contentOptions' => ['style'=>'width:80px;text-align: center;'],
		'template' => '{view} {update} {delete}',
	    ],
        ],
    ]); ?>
    <?php  Pjax::end();?>

</div>

<?=  ModalForm::widget([
    'id' => 'modal-fperson',
    'size'=>'modal-lg',
]);
?>

<?php  $this->registerJs("
$('#fperson-grid-pjax').on('click', '#modal-addbtn-fperson', function() {
    modalFPerson($(this).attr('data-url'));
});

$('#fperson-grid-pjax').on('click', '#modal-delbtn-fperson', function() {
    selectionFPersonGrid($(this).attr('data-url'));
});

$('#fperson-grid-pjax').on('click', '.select-on-check-all', function() {
    window.setTimeout(function() {
	var key = $('#fperson-grid').yiiGridView('getSelectedRows');
	disabledFPersonBtn(key.length);
    },100);
});

$('#fperson-grid-pjax').on('click', '.selectionFPersonIds', function() {
    var key = $('input:checked[class=\"'+$(this).attr('class')+'\"]');
    disabledFPersonBtn(key.length);
});

$('#fperson-grid-pjax').on('dblclick', 'tbody tr', function() {
    var id = $(this).attr('data-key');
    modalFPerson('".Url::to(['fperson/update', 'id'=>''])."'+id);
});	

$('#fperson-grid-pjax').on('click', 'tbody tr td a', function() {
    var url = $(this).attr('href');
    var action = $(this).attr('data-action');

    if(action === 'update' || action === 'view') {
	modalFPerson(url);
    } else if(action === 'delete') {
	yii.confirm('".Yii::t('app', 'Are you sure you want to delete this item?')."', function() {
	    $.post(
		url
	    ).done(function(result) {
		if(result.status == 'success') {
		    ". SDNoty::show('result.message', 'result.status') ."
		    $.pjax.reload({container:'#fperson-grid-pjax'});
		} else {
		    ". SDNoty::show('result.message', 'result.status') ."
		}
	    }).fail(function() {
		". SDNoty::show("'" . SDHtml::getMsgError() . "Server Error'", '"error"') ."
		console.log('server error');
	    });
	});
    }
    return false;
});

function disabledFPersonBtn(num) {
    if(num>0) {
	$('#modal-delbtn-fperson').attr('disabled', false);
    } else {
	$('#modal-delbtn-fperson').attr('disabled', true);
    }
}

function selectionFPersonGrid(url) {
    yii.confirm('".Yii::t('app', 'Are you sure you want to delete these items?')."', function() {
	$.ajax({
	    method: 'POST',
	    url: url,
	    data: $('.selectionFPersonIds:checked[name=\"selection[]\"]').serialize(),
	    dataType: 'JSON',
	    success: function(result, textStatus) {
		if(result.status == 'success') {
		    ". SDNoty::show('result.message', 'result.status') ."
		    $.pjax.reload({container:'#fperson-grid-pjax'});
		} else {
		    ". SDNoty::show('result.message', 'result.status') ."
		}
	    }
	});
    });
}

function modalFPerson(url) {
    $('#modal-fperson .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
    $('#modal-fperson').modal('show')
    .find('.modal-content')
    .load(url);
}

");?>