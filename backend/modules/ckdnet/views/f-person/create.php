<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\ckdnet\models\FPerson */

$this->title = Yii::t('app', 'Create Fperson');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Fpeople'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="fperson-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
