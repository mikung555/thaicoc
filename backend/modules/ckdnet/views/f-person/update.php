<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\ckdnet\models\FPerson */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Fperson',
]) . ' ' . $model->Name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Fpeople'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->Name, 'url' => ['view', 'sitecode' => $model->sitecode, 'HOSPCODE' => $model->HOSPCODE, 'PID' => $model->PID]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="fperson-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
