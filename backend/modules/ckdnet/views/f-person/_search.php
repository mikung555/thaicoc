<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\ckdnet\models\FPersonSearch */
/* @var $form yii\bootstrap\ActiveForm */
?>

<div class="fperson-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
	'layout' => 'horizontal',
	'fieldConfig' => [
	    'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
	    'horizontalCssClasses' => [
		'label' => 'col-sm-2',
		'offset' => 'col-sm-offset-3',
		'wrapper' => 'col-sm-6',
		'error' => '',
		'hint' => '',
	    ],
	],
    ]); ?>

    <?= $form->field($model, 'sitecode') ?>

    <?= $form->field($model, 'HOSPCODE') ?>

    <?= $form->field($model, 'CID') ?>

    <?= $form->field($model, 'PID') ?>

    <?= $form->field($model, 'HID') ?>

    <?php // echo $form->field($model, 'PreName') ?>

    <?php // echo $form->field($model, 'Pname') ?>

    <?php // echo $form->field($model, 'Name') ?>

    <?php // echo $form->field($model, 'Lname') ?>

    <?php // echo $form->field($model, 'HN') ?>

    <?php // echo $form->field($model, 'sex') ?>

    <?php // echo $form->field($model, 'Birth') ?>

    <?php // echo $form->field($model, 'Mstatus') ?>

    <?php // echo $form->field($model, 'Occupation_Old') ?>

    <?php // echo $form->field($model, 'Occupation_New') ?>

    <?php // echo $form->field($model, 'Race') ?>

    <?php // echo $form->field($model, 'Nation') ?>

    <?php // echo $form->field($model, 'Religion') ?>

    <?php // echo $form->field($model, 'Education') ?>

    <?php // echo $form->field($model, 'Fstatus') ?>

    <?php // echo $form->field($model, 'Father') ?>

    <?php // echo $form->field($model, 'Mother') ?>

    <?php // echo $form->field($model, 'Couple') ?>

    <?php // echo $form->field($model, 'Vstatus') ?>

    <?php // echo $form->field($model, 'MoveIn') ?>

    <?php // echo $form->field($model, 'Discharge') ?>

    <?php // echo $form->field($model, 'Ddischarge') ?>

    <?php // echo $form->field($model, 'ABOGROUP') ?>

    <?php // echo $form->field($model, 'RHGROUP') ?>

    <?php // echo $form->field($model, 'Labor') ?>

    <?php // echo $form->field($model, 'PassPort') ?>

    <?php // echo $form->field($model, 'TypeArea') ?>

    <?php // echo $form->field($model, 'D_Update') ?>

    <?php // echo $form->field($model, 'ptlink') ?>

    <div class="form-group">
	<div class="col-sm-offset-2 col-sm-6">
	    <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
	    <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
	</div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
