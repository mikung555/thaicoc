<?php
use kartik\tabs\TabsX;
use yii\helpers\Html;
use yii\bootstrap\Modal;
/* @var $this yii\web\View */
$this->title = Yii::t('backend', 'Module: CKDNET');
$this->params['breadcrumbs'][] = ['label'=>'Modules','url'=>  yii\helpers\Url::to('/tccbots/my-module')];
$this->params['breadcrumbs'][] = Yii::t('backend', 'CKDNET');



?>
<div class="ckdnet-default-index">
<?php
echo TabsX::widget([
    'items'=>$items,
    'position'=>TabsX::POS_ABOVE,
    'encodeLabels'=>false
]);
?>
</div>
<div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
<?php
    echo Html::img('../img/ckd_matrix.png',['class'=>'img-rounded img-responsive',"data-toggle"=>"modal", "data-target"=>"#modal-move-site2"]);
?>
</div>


<div class="row">
    <div class="col-md-8">
	<div id="calendar" class="fullcalendar fc fc-unthemed fc-ltr" data-plugin-name="fullCalendar"><div class="fc-toolbar"><div class="fc-left"><div class="fc-button-group"><button type="button" class="fc-prev-button fc-button fc-state-default fc-corner-left"><span class="fc-icon fc-icon-left-single-arrow"></span></button><button type="button" class="fc-next-button fc-button fc-state-default fc-corner-right"><span class="fc-icon fc-icon-right-single-arrow"></span></button></div><button type="button" class="fc-today-button fc-button fc-state-default fc-corner-left fc-corner-right fc-state-disabled" disabled="">วันนี้</button></div><div class="fc-right"></div><div class="fc-center"><h2>ตุลาคม 2016</h2></div><div class="fc-clear"></div></div><div class="fc-view-container" style=""><div class="fc-view fc-month-view fc-basic-view"><table><thead class="fc-head"><tr><td class="fc-head-container fc-widget-header"><div class="fc-row fc-widget-header"><table><thead><tr><th class="fc-day-header fc-widget-header fc-sun">อาทิตย์</th><th class="fc-day-header fc-widget-header fc-mon">จันทร์</th><th class="fc-day-header fc-widget-header fc-tue">อังคาร</th><th class="fc-day-header fc-widget-header fc-wed">พุธ</th><th class="fc-day-header fc-widget-header fc-thu">พฤหัส</th><th class="fc-day-header fc-widget-header fc-fri">ศุกร์</th><th class="fc-day-header fc-widget-header fc-sat">เสาร์</th></tr></thead></table></div></td></tr></thead><tbody class="fc-body"><tr><td class="fc-widget-content"><div class="fc-scroller fc-day-grid-container" style="overflow: hidden; height: 613px;"><div class="fc-day-grid fc-unselectable"><div class="fc-row fc-week fc-widget-content" style="height: 102px;"><div class="fc-bg"><table><tbody><tr><td class="fc-day fc-widget-content fc-sun fc-other-month fc-past" data-date="2016-09-25"></td><td class="fc-day fc-widget-content fc-mon fc-other-month fc-past" data-date="2016-09-26"></td><td class="fc-day fc-widget-content fc-tue fc-other-month fc-past" data-date="2016-09-27"></td><td class="fc-day fc-widget-content fc-wed fc-other-month fc-past" data-date="2016-09-28"></td><td class="fc-day fc-widget-content fc-thu fc-other-month fc-past" data-date="2016-09-29"></td><td class="fc-day fc-widget-content fc-fri fc-other-month fc-past" data-date="2016-09-30"></td><td class="fc-day fc-widget-content fc-sat fc-past" data-date="2016-10-01"></td></tr></tbody></table></div><div class="fc-content-skeleton"><table><thead><tr><td class="fc-day-number fc-sun fc-other-month fc-past" data-date="2016-09-25">25</td><td class="fc-day-number fc-mon fc-other-month fc-past" data-date="2016-09-26">26</td><td class="fc-day-number fc-tue fc-other-month fc-past" data-date="2016-09-27">27</td><td class="fc-day-number fc-wed fc-other-month fc-past" data-date="2016-09-28">28</td><td class="fc-day-number fc-thu fc-other-month fc-past" data-date="2016-09-29">29</td><td class="fc-day-number fc-fri fc-other-month fc-past" data-date="2016-09-30">30</td><td class="fc-day-number fc-sat fc-past" data-date="2016-10-01">1</td></tr></thead><tbody><tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr></tbody></table></div></div><div class="fc-row fc-week fc-widget-content" style="height: 102px;"><div class="fc-bg"><table><tbody><tr><td class="fc-day fc-widget-content fc-sun fc-past" data-date="2016-10-02"></td><td class="fc-day fc-widget-content fc-mon fc-past" data-date="2016-10-03"></td><td class="fc-day fc-widget-content fc-tue fc-past" data-date="2016-10-04"></td><td class="fc-day fc-widget-content fc-wed fc-past" data-date="2016-10-05"></td><td class="fc-day fc-widget-content fc-thu fc-today fc-state-highlight" data-date="2016-10-06"></td><td class="fc-day fc-widget-content fc-fri fc-future" data-date="2016-10-07"></td><td class="fc-day fc-widget-content fc-sat fc-future" data-date="2016-10-08"></td></tr></tbody></table></div><div class="fc-content-skeleton"><table><thead><tr><td class="fc-day-number fc-sun fc-past" data-date="2016-10-02">2</td><td class="fc-day-number fc-mon fc-past" data-date="2016-10-03">3</td><td class="fc-day-number fc-tue fc-past" data-date="2016-10-04">4</td><td class="fc-day-number fc-wed fc-past" data-date="2016-10-05">5</td><td class="fc-day-number fc-thu fc-today fc-state-highlight" data-date="2016-10-06">6</td><td class="fc-day-number fc-fri fc-future" data-date="2016-10-07">7</td><td class="fc-day-number fc-sat fc-future" data-date="2016-10-08">8</td></tr></thead><tbody><tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr></tbody></table></div></div><div class="fc-row fc-week fc-widget-content" style="height: 102px;"><div class="fc-bg"><table><tbody><tr><td class="fc-day fc-widget-content fc-sun fc-future" data-date="2016-10-09"></td><td class="fc-day fc-widget-content fc-mon fc-future" data-date="2016-10-10"></td><td class="fc-day fc-widget-content fc-tue fc-future" data-date="2016-10-11"></td><td class="fc-day fc-widget-content fc-wed fc-future" data-date="2016-10-12"></td><td class="fc-day fc-widget-content fc-thu fc-future" data-date="2016-10-13"></td><td class="fc-day fc-widget-content fc-fri fc-future" data-date="2016-10-14"></td><td class="fc-day fc-widget-content fc-sat fc-future" data-date="2016-10-15"></td></tr></tbody></table></div><div class="fc-content-skeleton"><table><thead><tr><td class="fc-day-number fc-sun fc-future" data-date="2016-10-09">9</td><td class="fc-day-number fc-mon fc-future" data-date="2016-10-10">10</td><td class="fc-day-number fc-tue fc-future" data-date="2016-10-11">11</td><td class="fc-day-number fc-wed fc-future" data-date="2016-10-12">12</td><td class="fc-day-number fc-thu fc-future" data-date="2016-10-13">13</td><td class="fc-day-number fc-fri fc-future" data-date="2016-10-14">14</td><td class="fc-day-number fc-sat fc-future" data-date="2016-10-15">15</td></tr></thead><tbody><tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr></tbody></table></div></div><div class="fc-row fc-week fc-widget-content" style="height: 102px;"><div class="fc-bg"><table><tbody><tr><td class="fc-day fc-widget-content fc-sun fc-future" data-date="2016-10-16"></td><td class="fc-day fc-widget-content fc-mon fc-future" data-date="2016-10-17"></td><td class="fc-day fc-widget-content fc-tue fc-future" data-date="2016-10-18"></td><td class="fc-day fc-widget-content fc-wed fc-future" data-date="2016-10-19"></td><td class="fc-day fc-widget-content fc-thu fc-future" data-date="2016-10-20"></td><td class="fc-day fc-widget-content fc-fri fc-future" data-date="2016-10-21"></td><td class="fc-day fc-widget-content fc-sat fc-future" data-date="2016-10-22"></td></tr></tbody></table></div><div class="fc-content-skeleton"><table><thead><tr><td class="fc-day-number fc-sun fc-future" data-date="2016-10-16">16</td><td class="fc-day-number fc-mon fc-future" data-date="2016-10-17">17</td><td class="fc-day-number fc-tue fc-future" data-date="2016-10-18">18</td><td class="fc-day-number fc-wed fc-future" data-date="2016-10-19">19</td><td class="fc-day-number fc-thu fc-future" data-date="2016-10-20">20</td><td class="fc-day-number fc-fri fc-future" data-date="2016-10-21">21</td><td class="fc-day-number fc-sat fc-future" data-date="2016-10-22">22</td></tr></thead><tbody><tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr></tbody></table></div></div><div class="fc-row fc-week fc-widget-content" style="height: 102px;"><div class="fc-bg"><table><tbody><tr><td class="fc-day fc-widget-content fc-sun fc-future" data-date="2016-10-23"></td><td class="fc-day fc-widget-content fc-mon fc-future" data-date="2016-10-24"></td><td class="fc-day fc-widget-content fc-tue fc-future" data-date="2016-10-25"></td><td class="fc-day fc-widget-content fc-wed fc-future" data-date="2016-10-26"></td><td class="fc-day fc-widget-content fc-thu fc-future" data-date="2016-10-27"></td><td class="fc-day fc-widget-content fc-fri fc-future" data-date="2016-10-28"></td><td class="fc-day fc-widget-content fc-sat fc-future" data-date="2016-10-29"></td></tr></tbody></table></div><div class="fc-content-skeleton"><table><thead><tr><td class="fc-day-number fc-sun fc-future" data-date="2016-10-23">23</td><td class="fc-day-number fc-mon fc-future" data-date="2016-10-24">24</td><td class="fc-day-number fc-tue fc-future" data-date="2016-10-25">25</td><td class="fc-day-number fc-wed fc-future" data-date="2016-10-26">26</td><td class="fc-day-number fc-thu fc-future" data-date="2016-10-27">27</td><td class="fc-day-number fc-fri fc-future" data-date="2016-10-28">28</td><td class="fc-day-number fc-sat fc-future" data-date="2016-10-29">29</td></tr></thead><tbody><tr><td class="fc-event-container"><a class="fc-day-grid-event fc-h-event fc-event fc-start fc-end" style="background-color:#FFF;border-color:#FFF;color:#FF0000"><div class="fc-content"> <span class="fc-title">วันปิยมหาราช</span></div></a></td><td></td><td></td><td></td><td></td><td></td><td></td></tr></tbody></table></div></div><div class="fc-row fc-week fc-widget-content" style="height: 103px;"><div class="fc-bg"><table><tbody><tr><td class="fc-day fc-widget-content fc-sun fc-future" data-date="2016-10-30"></td><td class="fc-day fc-widget-content fc-mon fc-future" data-date="2016-10-31"></td><td class="fc-day fc-widget-content fc-tue fc-other-month fc-future" data-date="2016-11-01"></td><td class="fc-day fc-widget-content fc-wed fc-other-month fc-future" data-date="2016-11-02"></td><td class="fc-day fc-widget-content fc-thu fc-other-month fc-future" data-date="2016-11-03"></td><td class="fc-day fc-widget-content fc-fri fc-other-month fc-future" data-date="2016-11-04"></td><td class="fc-day fc-widget-content fc-sat fc-other-month fc-future" data-date="2016-11-05"></td></tr></tbody></table></div><div class="fc-content-skeleton"><table><thead><tr><td class="fc-day-number fc-sun fc-future" data-date="2016-10-30">30</td><td class="fc-day-number fc-mon fc-future" data-date="2016-10-31">31</td><td class="fc-day-number fc-tue fc-other-month fc-future" data-date="2016-11-01">1</td><td class="fc-day-number fc-wed fc-other-month fc-future" data-date="2016-11-02">2</td><td class="fc-day-number fc-thu fc-other-month fc-future" data-date="2016-11-03">3</td><td class="fc-day-number fc-fri fc-other-month fc-future" data-date="2016-11-04">4</td><td class="fc-day-number fc-sat fc-other-month fc-future" data-date="2016-11-05">5</td></tr></thead><tbody><tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr></tbody></table></div></div></div></div></td></tr></tbody></table></div></div><div class="fc-loading" style="display:none;">Loading ...</div></div>
    </div>
    <div class="col-md-4">
	<br>
	<h4><code>หมายเหตุ</code></h4>
	
	    </div>
</div>

<?php
        Modal::begin([
            'id' => 'modal-move-site2',
            'header' => '<a class="btn btn-danger" href="/ckdnet/workspace">กำกับงาน CKD Registry</a>',
            'closeButton' => ['label' => 'ปิด','class' => 'btn btn-success btn-sm pull-right'],
            'options' => ['class' => 'modal-wide'],
        ]);
?>
<div class="summary">Showing <strong>1-100</strong> of <strong>15,421</strong> items.</div>
<ul class="pagination">
<li class="prev disabled">&laquo;</li>
<li class="active"><a href="../inv-person/index?module=46&amp;page=1&amp;per-page=100" data-page="0">1</a></li>
<li><a href="../inv-person/index?module=46&amp;page=2&amp;per-page=100" data-page="1">2</a></li>
<li><a href="../inv-person/index?module=46&amp;page=3&amp;per-page=100" data-page="2">3</a></li>
<li><a href="../inv-person/index?module=46&amp;page=4&amp;per-page=100" data-page="3">4</a></li>
<li><a href="../inv-person/index?module=46&amp;page=5&amp;per-page=100" data-page="4">5</a></li>
<li><a href="../inv-person/index?module=46&amp;page=6&amp;per-page=100" data-page="5">6</a></li>
<li><a href="../inv-person/index?module=46&amp;page=7&amp;per-page=100" data-page="6">7</a></li>
<li><a href="../inv-person/index?module=46&amp;page=8&amp;per-page=100" data-page="7">8</a></li>
<li><a href="../inv-person/index?module=46&amp;page=9&amp;per-page=100" data-page="8">9</a></li>
<li><a href="../inv-person/index?module=46&amp;page=10&amp;per-page=100" data-page="9">10</a></li>
<li class="next"><a href="../inv-person/index?module=46&amp;page=2&amp;per-page=100" data-page="1">&raquo;</a></li>
</ul>
<table class="table table-striped table-bordered table-hover">
<thead>
<tr>
<th style="text-align: center;">#</th>
<th style="text-align: left;"><a href="../inv-person/index?module=46&amp;sort=name" data-sort="name">ชื่อ</a></th>
<th style="text-align: left;"><a href="../inv-person/index?module=46&amp;sort=surname" data-sort="surname">นามสกุล</a></th>
<th style="text-align: center;">EMR</th>
<th style="text-align: center;">Last eGFR</th>
</tr>
<tr id="inv-person-grid-filters" class="filters">
<td>&nbsp;</td>
<td><input class="form-control" name="TbdataSearch[name]" type="text" /></td>
<td><input class="form-control" name="TbdataSearch[surname]" type="text" /></td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
</thead>
<tbody>
<tr data-key="1471355765038233300">
<td style="min-width: 60px; text-align: center;">1</td>
<td style="min-width: 110px; text-align: left;">วิทสนา</td>
<td style="min-width: 110px; text-align: left;">มับ</td>
<td style="min-width: 80px; text-align: center;"><a href="../../ckdnet/emr">EMR</a></td>
<td style="min-width: 110px; text-align: center;">60.5&nbsp;</td>
</tr>
<tr data-key="1471355765041164300">
<td style="min-width: 60px; text-align: center;">2</td>
<td style="min-width: 110px; text-align: left;">คอตี</td>
<td style="min-width: 110px; text-align: left;">โฉม</td>
<td style="min-width: 80px; text-align: center;"><a href="../../ckdnet/emr">EMR</a>&nbsp;</td>
<td style="min-width: 110px; text-align: center;">70.2&nbsp;</td>
</tr>
<tr data-key="1471355765044099700">
<td style="min-width: 60px; text-align: center;">3</td>
<td style="min-width: 110px; text-align: left;">น้อย</td>
<td style="min-width: 110px; text-align: left;">สานพิลา</td>
<td style="min-width: 80px; text-align: center;"><a href="../../ckdnet/emr">EMR</a>&nbsp;</td>
<td style="min-width: 110px; text-align: center;">53.2&nbsp;</td>
</tr>
<tr data-key="1471355765050939600">
<td style="min-width: 60px; text-align: center;">4</td>
<td style="min-width: 110px; text-align: left;">กันยา</td>
<td style="min-width: 110px; text-align: left;">ลาย</td>
<td style="min-width: 80px; text-align: center;"><a href="../../ckdnet/emr">EMR</a>&nbsp;</td>
<td style="min-width: 110px; text-align: center;">&nbsp;64.1</td>
</tr>
<tr data-key="1471355765054851400">
<td style="min-width: 60px; text-align: center;">5</td>
<td style="min-width: 110px; text-align: left;">เอเอออง</td>
<td style="min-width: 110px; text-align: left;">ไม่มีนามสกุล</td>
<td style="min-width: 80px; text-align: center;"><a href="../../ckdnet/emr">EMR</a>&nbsp;</td>
<td style="min-width: 110px; text-align: center;">70&nbsp;</td>
</tr>
<tr data-key="1471355765058762800">
<td style="min-width: 60px; text-align: center;">6</td>
<td style="min-width: 110px; text-align: left;">เอือ</td>
<td style="min-width: 110px; text-align: left;">เบียร์</td>
<td style="min-width: 80px; text-align: center;"><a href="../../ckdnet/emr">EMR</a>&nbsp;</td>
<td style="min-width: 110px; text-align: center;">80.3&nbsp;</td>
</tr>
<tr data-key="1471355765062671700">
<td style="min-width: 60px; text-align: center;">7</td>
<td style="min-width: 110px; text-align: left;">เชรี๊ยท</td>
<td style="min-width: 110px; text-align: left;">จอน</td>
<td style="min-width: 80px; text-align: center;"><a href="../../ckdnet/emr">EMR</a>&nbsp;</td>
<td style="min-width: 110px; text-align: center;">90&nbsp;</td>
</tr>
<tr data-key="1471355765066582100">
<td style="min-width: 60px; text-align: center;">8</td>
<td style="min-width: 110px; text-align: left;">เอ๋</td>
<td style="min-width: 110px; text-align: left;">ผงาตุนัด</td>
<td style="min-width: 80px; text-align: center;"><a href="../../ckdnet/emr">EMR</a>&nbsp;</td>
<td style="min-width: 110px; text-align: center;">60.5&nbsp;&nbsp;</td>
</tr>
<tr data-key="1471355765071474900">
<td style="min-width: 60px; text-align: center;">9</td>
<td style="min-width: 110px; text-align: left;">แต๋ว</td>
<td style="min-width: 110px; text-align: left;">แวน</td>
<td style="min-width: 80px; text-align: center;"><a href="../../ckdnet/emr">EMR</a></td>
<td style="min-width: 110px; text-align: center;">&nbsp;60.5&nbsp;</td>
</tr>
<tr data-key="1471355765075383800">
<td style="min-width: 60px; text-align: center;">10</td>
<td style="min-width: 110px; text-align: left;">ดา</td>
<td style="min-width: 110px; text-align: left;">ไม่มีนามสกุล</td>
<td style="min-width: 80px; text-align: center;">&nbsp;<a href="../../ckdnet/emr">EMR</a></td>
<td style="min-width: 110px; text-align: center;">90&nbsp;</td>
</tr>
<tr data-key="1471355765091021100">
<td style="min-width: 60px; text-align: center;">11</td>
<td style="min-width: 110px; text-align: left;">ตู</td>
<td style="min-width: 110px; text-align: left;">ยอง</td>
<td style="min-width: 80px; text-align: center;">&nbsp;<a href="../../ckdnet/emr">EMR</a></td>
<td style="min-width: 110px; text-align: center;">100&nbsp;</td>
</tr>
<tr data-key="1471355765099818900">
<td style="min-width: 60px; text-align: center;">12</td>
<td style="min-width: 110px; text-align: left;">โซเฟีย</td>
<td style="min-width: 110px; text-align: left;">ตี</td>
<td style="min-width: 80px; text-align: center;">&nbsp;<a href="../../ckdnet/emr">EMR</a></td>
<td style="min-width: 110px; text-align: center;">100&nbsp;</td>
</tr>
<tr data-key="1471355765103728300">
<td style="min-width: 60px; text-align: center;">13</td>
<td style="min-width: 110px; text-align: left;">สเรือน</td>
<td style="min-width: 110px; text-align: left;">กัน</td>
<td style="min-width: 80px; text-align: center;">&nbsp;<a href="../../ckdnet/emr">EMR</a></td>
<td style="min-width: 110px; text-align: center;">100&nbsp;</td>
</tr>
<tr data-key="1471355765109599400">
<td style="min-width: 60px; text-align: center;">14</td>
<td style="min-width: 110px; text-align: left;">ไล</td>
<td style="min-width: 110px; text-align: left;">ซู</td>
<td style="min-width: 80px; text-align: center;"><a href="../../ckdnet/emr">EMR</a>&nbsp;</td>
<td style="min-width: 110px; text-align: center;">100&nbsp;</td>
</tr>
<tr data-key="1471355765114490700">
<td style="min-width: 60px; text-align: center;">15</td>
<td style="min-width: 110px; text-align: left;">ตังงิม</td>
<td style="min-width: 110px; text-align: left;">ยืน</td>
<td style="min-width: 80px; text-align: center;"><a href="../../ckdnet/emr">EMR</a>&nbsp;</td>
<td style="min-width: 110px; text-align: center;">100&nbsp;</td>
</tr>
<tr data-key="1471355765118391800">
<td style="min-width: 60px; text-align: center;">16</td>
<td style="min-width: 110px; text-align: left;">เจิน</td>
<td style="min-width: 110px; text-align: left;">ศรี</td>
<td style="min-width: 80px; text-align: center;">&nbsp;<a href="../../ckdnet/emr">EMR</a></td>
<td style="min-width: 110px; text-align: center;">100&nbsp;</td>
</tr>
<tr data-key="1471355765120347300">
<td style="min-width: 60px; text-align: center;">17</td>
<td style="min-width: 110px; text-align: left;">แดง</td>
<td style="min-width: 110px; text-align: left;">ไชยยะแสง</td>
<td style="min-width: 80px; text-align: center;"><a href="../../ckdnet/emr">EMR</a>&nbsp;</td>
<td style="min-width: 110px; text-align: center;">100&nbsp;</td>
</tr>
<tr data-key="1471355765124259700">
<td style="min-width: 60px; text-align: center;">18</td>
<td style="min-width: 110px; text-align: left;">คาโคตี</td>
<td style="min-width: 110px; text-align: left;">สรอ</td>
<td style="min-width: 80px; text-align: center;"><a href="../../ckdnet/emr">EMR</a>&nbsp;</td>
<td style="min-width: 110px; text-align: center;">100&nbsp;</td>
</tr>
<tr data-key="1471355765127190700">
<td style="min-width: 60px; text-align: center;">19</td>
<td style="min-width: 110px; text-align: left;">สินา</td>
<td style="min-width: 110px; text-align: left;">ดวง</td>
<td style="min-width: 80px; text-align: center;"><a href="../../ckdnet/emr">EMR</a>&nbsp;</td>
<td style="min-width: 110px; text-align: center;">100&nbsp;</td>
</tr>
<tr data-key="1471355765129146200">
<td style="min-width: 60px; text-align: center;">20</td>
<td style="min-width: 110px; text-align: left;">เหงี๊ยน</td>
<td style="min-width: 110px; text-align: left;">โกนงา</td>
<td style="min-width: 80px; text-align: center;"><a href="../../ckdnet/emr">EMR</a>&nbsp;</td>
<td style="min-width: 110px; text-align: center;">100&nbsp;</td>
</tr>
<tr data-key="1471355765132083500">
<td style="min-width: 60px; text-align: center;">21</td>
<td style="min-width: 110px; text-align: left;">จนา</td>
<td style="min-width: 110px; text-align: left;">แซด</td>
<td style="min-width: 80px; text-align: center;"><a href="../../ckdnet/emr">EMR</a>&nbsp;</td>
<td style="min-width: 110px; text-align: center;">100&nbsp;</td>
</tr>
<tr data-key="1471355765135012100">
<td style="min-width: 60px; text-align: center;">22</td>
<td style="min-width: 110px; text-align: left;">แสงจันทร์</td>
<td style="min-width: 110px; text-align: left;">ไม่มีนามสกุล</td>
<td style="min-width: 80px; text-align: center;"><a href="../../ckdnet/emr">EMR</a>&nbsp;</td>
<td style="min-width: 110px; text-align: center;">100&nbsp;</td>
</tr>
<tr data-key="1471355765137943500">
<td style="min-width: 60px; text-align: center;">23</td>
<td style="min-width: 110px; text-align: left;">ซารอส</td>
<td style="min-width: 110px; text-align: left;">โอ๊ต</td>
<td style="min-width: 80px; text-align: center;"><a href="../../ckdnet/emr">EMR</a>&nbsp;</td>
<td style="min-width: 110px; text-align: center;">100&nbsp;</td>
</tr>
<tr data-key="1471355765142831500">
<td style="min-width: 60px; text-align: center;">24</td>
<td style="min-width: 110px; text-align: left;">ดวงสนิท</td>
<td style="min-width: 110px; text-align: left;">จันทร์</td>
<td style="min-width: 80px; text-align: center;">&nbsp;<a href="../../ckdnet/emr">EMR</a></td>
<td style="min-width: 110px; text-align: center;">100&nbsp;</td>
</tr>
<tr data-key="1471355765145764900">
<td style="min-width: 60px; text-align: center;">25</td>
<td style="min-width: 110px; text-align: left;">สินา</td>
<td style="min-width: 110px; text-align: left;">เลง</td>
<td style="min-width: 80px; text-align: center;">&nbsp;<a href="../../ckdnet/emr">EMR</a></td>
<td style="min-width: 110px; text-align: center;">100&nbsp;</td>
</tr>
<tr data-key="1471355765148701300">
<td style="min-width: 60px; text-align: center;">26</td>
<td style="min-width: 110px; text-align: left;">วุฒิ</td>
<td style="min-width: 110px; text-align: left;">จันทร์</td>
<td style="min-width: 80px; text-align: center;">&nbsp;<a href="../../ckdnet/emr">EMR</a></td>
<td style="min-width: 110px; text-align: center;">100&nbsp;</td>
</tr>
<tr data-key="1471355765150657200">
<td style="min-width: 60px; text-align: center;">27</td>
<td style="min-width: 110px; text-align: left;">เลือม</td>
<td style="min-width: 110px; text-align: left;">โลย</td>
<td style="min-width: 80px; text-align: center;">&nbsp;<a href="../../ckdnet/emr">EMR</a></td>
<td style="min-width: 110px; text-align: center;">100&nbsp;</td>
</tr>
<tr data-key="1471355765153586300">
<td style="min-width: 60px; text-align: center;">28</td>
<td style="min-width: 110px; text-align: left;">ลีน่า</td>
<td style="min-width: 110px; text-align: left;">ยอด</td>
<td style="min-width: 80px; text-align: center;">&nbsp;<a href="../../ckdnet/emr">EMR</a></td>
<td style="min-width: 110px; text-align: center;">100&nbsp;</td>
</tr>
<tr data-key="1471355765157501500">
<td style="min-width: 60px; text-align: center;">29</td>
<td style="min-width: 110px; text-align: left;">สะวิช</td>
<td style="min-width: 110px; text-align: left;">โซ</td>
<td style="min-width: 80px; text-align: center;"><a href="../../ckdnet/emr">EMR</a>&nbsp;</td>
<td style="min-width: 110px; text-align: center;">100&nbsp;</td>
</tr>
<tr data-key="1471355765160437900">
<td style="min-width: 60px; text-align: center;">30</td>
<td style="min-width: 110px; text-align: left;">นาลอน</td>
<td style="min-width: 110px; text-align: left;">สุดทะวิไล</td>
<td style="min-width: 80px; text-align: center;">&nbsp;<a href="../../ckdnet/emr">EMR</a></td>
<td style="min-width: 110px; text-align: center;">100&nbsp;</td>
</tr>
<tr data-key="1471355765165316000">
<td style="min-width: 60px; text-align: center;">31</td>
<td style="min-width: 110px; text-align: left;">ไชเลียง</td>
<td style="min-width: 110px; text-align: left;">แง้ม</td>
<td style="min-width: 80px; text-align: center;"><a href="../../ckdnet/emr">EMR</a>&nbsp;</td>
<td style="min-width: 110px; text-align: center;">100&nbsp;</td>
</tr>
<tr data-key="1471355765170204400">
<td style="min-width: 60px; text-align: center;">32</td>
<td style="min-width: 110px; text-align: left;">โน</td>
<td style="min-width: 110px; text-align: left;">ฮุน</td>
<td style="min-width: 80px; text-align: center;"><a href="../../ckdnet/emr">EMR</a>&nbsp;</td>
<td style="min-width: 110px; text-align: center;">100&nbsp;</td>
</tr>
<tr data-key="1471355765172159900">
<td style="min-width: 60px; text-align: center;">33</td>
<td style="min-width: 110px; text-align: left;">เทวี</td>
<td style="min-width: 110px; text-align: left;">จวน</td>
<td style="min-width: 80px; text-align: center;"><a href="../../ckdnet/emr">EMR</a>&nbsp;</td>
<td style="min-width: 110px; text-align: center;">100&nbsp;</td>
</tr>
<tr data-key="1471355765175094300">
<td style="min-width: 60px; text-align: center;">34</td>
<td style="min-width: 110px; text-align: left;">ซกเนีย</td>
<td style="min-width: 110px; text-align: left;">เนอว</td>
<td style="min-width: 80px; text-align: center;"><a href="../../ckdnet/emr">EMR</a>&nbsp;</td>
<td style="min-width: 110px; text-align: center;">100&nbsp;</td>
</tr>
<tr data-key="1471355765178026700">
<td style="min-width: 60px; text-align: center;">35</td>
<td style="min-width: 110px; text-align: left;">เอี้ยง</td>
<td style="min-width: 110px; text-align: left;">อาน</td>
<td style="min-width: 80px; text-align: center;"><a href="../../ckdnet/emr">EMR</a>&nbsp;</td>
<td style="min-width: 110px; text-align: center;">100&nbsp;</td>
</tr>
</tbody>
</table>
<ul class="pagination">
<li class="prev disabled">&laquo;</li>
<li class="active"><a href="../inv-person/index?module=46&amp;page=1&amp;per-page=100" data-page="0">1</a></li>
<li><a href="../inv-person/index?module=46&amp;page=2&amp;per-page=100" data-page="1">2</a></li>
<li><a href="../inv-person/index?module=46&amp;page=3&amp;per-page=100" data-page="2">3</a></li>
<li><a href="../inv-person/index?module=46&amp;page=4&amp;per-page=100" data-page="3">4</a></li>
<li><a href="../inv-person/index?module=46&amp;page=5&amp;per-page=100" data-page="4">5</a></li>
<li><a href="../inv-person/index?module=46&amp;page=6&amp;per-page=100" data-page="5">6</a></li>
<li><a href="../inv-person/index?module=46&amp;page=7&amp;per-page=100" data-page="6">7</a></li>
<li><a href="../inv-person/index?module=46&amp;page=8&amp;per-page=100" data-page="7">8</a></li>
<li><a href="../inv-person/index?module=46&amp;page=9&amp;per-page=100" data-page="8">9</a></li>
<li><a href="../inv-person/index?module=46&amp;page=10&amp;per-page=100" data-page="9">10</a></li>
<li class="next"><a href="../inv-person/index?module=46&amp;page=2&amp;per-page=100" data-page="1">&raquo;</a></li>
</ul>
<?php
        Modal::end();        
?>