<?php
use kartik\tabs\TabsX;
use kartik\widgets\Select2;
use yii\helpers\Html;
use yii\bootstrap\Modal;
/* @var $this yii\web\View */
$this->title = Yii::t('backend', 'Module: CKDNET');
$this->params['breadcrumbs'][] = ['label'=>'Modules','url'=>  yii\helpers\Url::to('/tccbots/my-module')];
$this->params['breadcrumbs'][] = Yii::t('backend', 'CKDNET');



?>
<div class="ckdnet-default-index">
<?php
echo TabsX::widget([
    'items'=>$items,
    'position'=>TabsX::POS_ABOVE,
    'encodeLabels'=>false
]);
?>
</div>
<div class="row">
<div class="col-sm-6">
    <div class="form-group field-state_12 required">
            <label class="control-label">Workspace</label>
    <?php
    $data = ['CKD Regitry [421641 ราย]','CKD Screening [983829 ราย]','AKI Registry [468490 ราย]','Screening targets [2345 ราย]'];
    echo Select2::widget([
        'name' => 'state_2',
        'value' => '0',
        'data' => $data,
        'options' => ['class'=>'form-control','multiple' => false, 'placeholder' => 'Select workspace ...']
    ]);
    ?>
    </div>
</div>
<div class="col-sm-6 pull-right">
    <div class="form-group field-state_12 required pull-right">
        <label class="control-label">Summary</label><br>
       <a class="btn btn-success" data-toggle="modal" data-target="#modal-move-site2">Survival</a> 
    </div>
</div>    
</div>
<div id="divToScroll" class="table-responsive">
<div id="inv-person-grid" class="grid-view">
<div class="summary">Showing <strong>1-100</strong> of <strong>15,421</strong> items.</div>
<ul class="pagination">

<li class="next"><a href="../inv-person/index?module=46&amp;page=2&amp;per-page=100" data-page="1">&laquo;</a></li>
<li class="active"><a href="../inv-person/index?module=46&amp;page=1&amp;per-page=100" data-page="0">1</a></li>
<li><a href="../inv-person/index?module=46&amp;page=2&amp;per-page=100" data-page="1">2</a></li>
<li><a href="../inv-person/index?module=46&amp;page=3&amp;per-page=100" data-page="2">3</a></li>
<li><a href="../inv-person/index?module=46&amp;page=4&amp;per-page=100" data-page="3">4</a></li>
<li><a href="../inv-person/index?module=46&amp;page=5&amp;per-page=100" data-page="4">5</a></li>
<li><a href="../inv-person/index?module=46&amp;page=6&amp;per-page=100" data-page="5">6</a></li>
<li><a href="../inv-person/index?module=46&amp;page=7&amp;per-page=100" data-page="6">7</a></li>
<li><a href="../inv-person/index?module=46&amp;page=8&amp;per-page=100" data-page="7">8</a></li>
<li><a href="../inv-person/index?module=46&amp;page=9&amp;per-page=100" data-page="8">9</a></li>
<li><a href="../inv-person/index?module=46&amp;page=10&amp;per-page=100" data-page="9">10</a></li>
<li class="next"><a href="../inv-person/index?module=46&amp;page=2&amp;per-page=100" data-page="1">&raquo;</a></li>
</ul>
<table class="table table-striped table-bordered table-hover">
<thead>
<tr>
<th style="text-align: center;"><input class="select-on-check-all" name="selection_all" type="checkbox" value="1" /></th>
<th style="text-align: center;">#</th>
<th style="text-align: left;"><a href="../inv-person/index?module=46&amp;sort=cid" data-sort="cid">รหัสประจำตัวประชาชน</a></th>
<th style="text-align: left;"><a href="../inv-person/index?module=46&amp;sort=name" data-sort="name">ชื่อ</a></th>
<th style="text-align: left;"><a href="../inv-person/index?module=46&amp;sort=surname" data-sort="surname">นามสกุล</a></th>
<th style="text-align: center;">EMR</th>
<th style="text-align: center;">Last eGFR</th>
<th style="text-align: center;">CVD Risk</th>
<th style="text-align: center;">CVA</th>
<th style="text-align: center;">Urine Protine</th>
<th style="text-align: center;">Action A</th>
<th style="text-align: center;">ActionB</th>
</tr>
<tr id="inv-person-grid-filters" class="filters">
<td>&nbsp;</td>
<td>&nbsp;</td>
<td><input class="form-control" name="TbdataSearch[cid]" type="text" /></td>
<td><input class="form-control" name="TbdataSearch[name]" type="text" /></td>
<td><input class="form-control" name="TbdataSearch[surname]" type="text" /></td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
</thead>
<tbody>
<tr data-key="1471355765038233300">
<td style="min-width: 40px; text-align: center;"><input class="selectionOvPersonIds" name="selection[]" type="checkbox" value="1471355765038233300" /></td>
<td style="min-width: 60px; text-align: center;">1</td>
<td style="min-width: 110px; text-align: left;">0002011011591</td>
<td style="min-width: 110px; text-align: left;">วิทสนา</td>
<td style="min-width: 110px; text-align: left;">มับ</td>
<td style="min-width: 80px; text-align: center;"><a href="../../ckdnet/emr">EMR</a></td>
<td style="min-width: 110px; text-align: center;">60.5&nbsp;</td>
<td style="min-width: 110px; text-align: center;">&nbsp;10</td>
<td style="min-width: 110px; text-align: center;">&nbsp;/</td>
<td style="min-width: 110px; text-align: center;">&nbsp;50</td>
<td style="min-width: 110px; text-align: center;"><a title="" href="../../inputdata/step4?ezf_id=1463624356048989900&amp;target=MTQ3MTM1NTc2NTAzODIzMzMwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" data-toggle="tooltip" data-original-title="(Submit/Save Draft) คลิกเพื่อแสดงข้อมูล"><span class="label label-default" style="font-size: 13px;">0 / 0</span> </a><a href="/inputdata/insert-record?insert=url&amp;ezf_id=1470104222001313900&amp;target=MTQ3MTM1NTc2NTAzODIzMzMwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" title="" data-toggle="tooltip" data-original-title="เพิ่มข้อมูล"><i class="glyphicon glyphicon-plus" style="color:#00a65a;font-size: 18px;"></i> </a></td>
<td style="min-width: 110px; text-align: center;"><a title="" href="../../inputdata/step4?ezf_id=1471483982079544300&amp;target=MTQ3MTM1NTc2NTAzODIzMzMwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" data-toggle="tooltip" data-original-title="(Submit/Save Draft) คลิกเพื่อแสดงข้อมูล"><span class="label label-default" style="font-size: 13px;">0 / 0</span> </a><a href="/inputdata/insert-record?insert=url&amp;ezf_id=1470104222001313900&amp;target=MTQ3MTM1NTc2NTAzODIzMzMwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" title="" data-toggle="tooltip" data-original-title="เพิ่มข้อมูล"><i class="glyphicon glyphicon-plus" style="color:#00a65a;font-size: 18px;"></i> </a></td>
</tr>
<tr data-key="1471355765041164300">
<td style="min-width: 40px; text-align: center;"><input class="selectionOvPersonIds" name="selection[]" type="checkbox" value="1471355765041164300" /></td>
<td style="min-width: 60px; text-align: center;">2</td>
<td style="min-width: 110px; text-align: left;">0010051588714</td>
<td style="min-width: 110px; text-align: left;">คอตี</td>
<td style="min-width: 110px; text-align: left;">โฉม</td>
<td style="min-width: 80px; text-align: center;"><a href="../../ckdnet/emr">EMR</a>&nbsp;</td>
<td style="min-width: 110px; text-align: center;">70.2&nbsp;</td>
<td style="min-width: 110px; text-align: center;">8&nbsp;</td>
<td style="min-width: 110px; text-align: center;">&nbsp;</td>
<td style="min-width: 110px; text-align: center;">43&nbsp;</td>
<td style="min-width: 110px; text-align: center;"><a title="" href="../../inputdata/step4?ezf_id=1463624356048989900&amp;target=MTQ3MTM1NTc2NTA0MTE2NDMwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" data-toggle="tooltip" data-original-title="(Submit/Save Draft) คลิกเพื่อแสดงข้อมูล"><span class="label label-default" style="font-size: 13px;">0 / 0</span> </a><a href="/inputdata/insert-record?insert=url&amp;ezf_id=1470104222001313900&amp;target=MTQ3MTM1NTc2NTAzODIzMzMwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" title="" data-toggle="tooltip" data-original-title="เพิ่มข้อมูล"><i class="glyphicon glyphicon-plus" style="color:#00a65a;font-size: 18px;"></i> </a></td>
<td style="min-width: 110px; text-align: center;"><a title="" href="../../inputdata/step4?ezf_id=1471483982079544300&amp;target=MTQ3MTM1NTc2NTA0MTE2NDMwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" data-toggle="tooltip" data-original-title="(Submit/Save Draft) คลิกเพื่อแสดงข้อมูล"><span class="label label-default" style="font-size: 13px;">0 / 0</span> </a><a href="/inputdata/insert-record?insert=url&amp;ezf_id=1470104222001313900&amp;target=MTQ3MTM1NTc2NTAzODIzMzMwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" title="" data-toggle="tooltip" data-original-title="เพิ่มข้อมูล"><i class="glyphicon glyphicon-plus" style="color:#00a65a;font-size: 18px;"></i> </a></td>
</tr>
<tr data-key="1471355765044099700">
<td style="min-width: 40px; text-align: center;"><input class="selectionOvPersonIds" name="selection[]" type="checkbox" value="1471355765044099700" /></td>
<td style="min-width: 60px; text-align: center;">3</td>
<td style="min-width: 110px; text-align: left;">0010061048232</td>
<td style="min-width: 110px; text-align: left;">น้อย</td>
<td style="min-width: 110px; text-align: left;">สานพิลา</td>
<td style="min-width: 80px; text-align: center;"><a href="../../ckdnet/emr">EMR</a>&nbsp;</td>
<td style="min-width: 110px; text-align: center;">53.2&nbsp;</td>
<td style="min-width: 110px; text-align: center;">7&nbsp;</td>
<td style="min-width: 110px; text-align: center;">/&nbsp;</td>
<td style="min-width: 110px; text-align: center;">22&nbsp;</td>
<td style="min-width: 110px; text-align: center;"><a title="" href="../../inputdata/step4?ezf_id=1463624356048989900&amp;target=MTQ3MTM1NTc2NTA0NDA5OTcwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" data-toggle="tooltip" data-original-title="(Submit/Save Draft) คลิกเพื่อแสดงข้อมูล"><span class="label label-default" style="font-size: 13px;">0 / 0</span> </a><a href="/inputdata/insert-record?insert=url&amp;ezf_id=1470104222001313900&amp;target=MTQ3MTM1NTc2NTAzODIzMzMwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" title="" data-toggle="tooltip" data-original-title="เพิ่มข้อมูล"><i class="glyphicon glyphicon-plus" style="color:#00a65a;font-size: 18px;"></i> </a></td>
<td style="min-width: 110px; text-align: center;"><a title="" href="../../inputdata/step4?ezf_id=1471483982079544300&amp;target=MTQ3MTM1NTc2NTA0NDA5OTcwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" data-toggle="tooltip" data-original-title="(Submit/Save Draft) คลิกเพื่อแสดงข้อมูล"><span class="label label-default" style="font-size: 13px;">0 / 0</span> </a><a href="/inputdata/insert-record?insert=url&amp;ezf_id=1470104222001313900&amp;target=MTQ3MTM1NTc2NTAzODIzMzMwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" title="" data-toggle="tooltip" data-original-title="เพิ่มข้อมูล"><i class="glyphicon glyphicon-plus" style="color:#00a65a;font-size: 18px;"></i> </a></td>
</tr>
<tr data-key="1471355765050939600">
<td style="min-width: 40px; text-align: center;"><input class="selectionOvPersonIds" name="selection[]" type="checkbox" value="1471355765050939600" /></td>
<td style="min-width: 60px; text-align: center;">4</td>
<td style="min-width: 110px; text-align: left;">0010101423888</td>
<td style="min-width: 110px; text-align: left;">กันยา</td>
<td style="min-width: 110px; text-align: left;">ลาย</td>
<td style="min-width: 80px; text-align: center;"><a href="../../ckdnet/emr">EMR</a>&nbsp;</td>
<td style="min-width: 110px; text-align: center;">&nbsp;64.1</td>
<td style="min-width: 110px; text-align: center;">9&nbsp;</td>
<td style="min-width: 110px; text-align: center;">&nbsp;</td>
<td style="min-width: 110px; text-align: center;">67&nbsp;</td>
<td style="min-width: 110px; text-align: center;"><a title="" href="../../inputdata/step4?ezf_id=1463624356048989900&amp;target=MTQ3MTM1NTc2NTA1MDkzOTYwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" data-toggle="tooltip" data-original-title="(Submit/Save Draft) คลิกเพื่อแสดงข้อมูล"><span class="label label-default" style="font-size: 13px;">0 / 0</span> </a><a href="/inputdata/insert-record?insert=url&amp;ezf_id=1470104222001313900&amp;target=MTQ3MTM1NTc2NTAzODIzMzMwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" title="" data-toggle="tooltip" data-original-title="เพิ่มข้อมูล"><i class="glyphicon glyphicon-plus" style="color:#00a65a;font-size: 18px;"></i> </a></td>
<td style="min-width: 110px; text-align: center;"><a title="" href="../../inputdata/step4?ezf_id=1471483982079544300&amp;target=MTQ3MTM1NTc2NTA1MDkzOTYwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" data-toggle="tooltip" data-original-title="(Submit/Save Draft) คลิกเพื่อแสดงข้อมูล"><span class="label label-default" style="font-size: 13px;">0 / 0</span> </a><a href="/inputdata/insert-record?insert=url&amp;ezf_id=1470104222001313900&amp;target=MTQ3MTM1NTc2NTAzODIzMzMwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" title="" data-toggle="tooltip" data-original-title="เพิ่มข้อมูล"><i class="glyphicon glyphicon-plus" style="color:#00a65a;font-size: 18px;"></i> </a></td>
</tr>
<tr data-key="1471355765054851400">
<td style="min-width: 40px; text-align: center;"><input class="selectionOvPersonIds" name="selection[]" type="checkbox" value="1471355765054851400" /></td>
<td style="min-width: 60px; text-align: center;">5</td>
<td style="min-width: 110px; text-align: left;">0010101440421</td>
<td style="min-width: 110px; text-align: left;">เอเอออง</td>
<td style="min-width: 110px; text-align: left;">ไม่มีนามสกุล</td>
<td style="min-width: 80px; text-align: center;"><a href="../../ckdnet/emr">EMR</a>&nbsp;</td>
<td style="min-width: 110px; text-align: center;">70&nbsp;</td>
<td style="min-width: 110px; text-align: center;">5&nbsp;</td>
<td style="min-width: 110px; text-align: center;">&nbsp;</td>
<td style="min-width: 110px; text-align: center;">87&nbsp;</td>
<td style="min-width: 110px; text-align: center;"><a title="" href="../../inputdata/step4?ezf_id=1463624356048989900&amp;target=MTQ3MTM1NTc2NTA1NDg1MTQwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" data-toggle="tooltip" data-original-title="(Submit/Save Draft) คลิกเพื่อแสดงข้อมูล"><span class="label label-default" style="font-size: 13px;">0 / 0</span> </a><a href="/inputdata/insert-record?insert=url&amp;ezf_id=1470104222001313900&amp;target=MTQ3MTM1NTc2NTAzODIzMzMwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" title="" data-toggle="tooltip" data-original-title="เพิ่มข้อมูล"><i class="glyphicon glyphicon-plus" style="color:#00a65a;font-size: 18px;"></i> </a></td>
<td style="min-width: 110px; text-align: center;"><a title="" href="../../inputdata/step4?ezf_id=1471483982079544300&amp;target=MTQ3MTM1NTc2NTA1NDg1MTQwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" data-toggle="tooltip" data-original-title="(Submit/Save Draft) คลิกเพื่อแสดงข้อมูล"><span class="label label-default" style="font-size: 13px;">0 / 0</span> </a><a href="/inputdata/insert-record?insert=url&amp;ezf_id=1470104222001313900&amp;target=MTQ3MTM1NTc2NTAzODIzMzMwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" title="" data-toggle="tooltip" data-original-title="เพิ่มข้อมูล"><i class="glyphicon glyphicon-plus" style="color:#00a65a;font-size: 18px;"></i> </a></td>
</tr>
<tr data-key="1471355765058762800">
<td style="min-width: 40px; text-align: center;"><input class="selectionOvPersonIds" name="selection[]" type="checkbox" value="1471355765058762800" /></td>
<td style="min-width: 60px; text-align: center;">6</td>
<td style="min-width: 110px; text-align: left;">0010211377066</td>
<td style="min-width: 110px; text-align: left;">เอือ</td>
<td style="min-width: 110px; text-align: left;">เบียร์</td>
<td style="min-width: 80px; text-align: center;"><a href="../../ckdnet/emr">EMR</a>&nbsp;</td>
<td style="min-width: 110px; text-align: center;">80.3&nbsp;</td>
<td style="min-width: 110px; text-align: center;">2&nbsp;</td>
<td style="min-width: 110px; text-align: center;">/&nbsp;</td>
<td style="min-width: 110px; text-align: center;">54&nbsp;</td>
<td style="min-width: 110px; text-align: center;"><a title="" href="../../inputdata/step4?ezf_id=1463624356048989900&amp;target=MTQ3MTM1NTc2NTA1ODc2MjgwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" data-toggle="tooltip" data-original-title="(Submit/Save Draft) คลิกเพื่อแสดงข้อมูล"><span class="label label-default" style="font-size: 13px;">0 / 0</span> </a><a href="/inputdata/insert-record?insert=url&amp;ezf_id=1470104222001313900&amp;target=MTQ3MTM1NTc2NTAzODIzMzMwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" title="" data-toggle="tooltip" data-original-title="เพิ่มข้อมูล"><i class="glyphicon glyphicon-plus" style="color:#00a65a;font-size: 18px;"></i> </a></td>
<td style="min-width: 110px; text-align: center;"><a title="" href="../../inputdata/step4?ezf_id=1471483982079544300&amp;target=MTQ3MTM1NTc2NTA1ODc2MjgwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" data-toggle="tooltip" data-original-title="(Submit/Save Draft) คลิกเพื่อแสดงข้อมูล"><span class="label label-default" style="font-size: 13px;">0 / 0</span> </a><a href="/inputdata/insert-record?insert=url&amp;ezf_id=1470104222001313900&amp;target=MTQ3MTM1NTc2NTAzODIzMzMwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" title="" data-toggle="tooltip" data-original-title="เพิ่มข้อมูล"><i class="glyphicon glyphicon-plus" style="color:#00a65a;font-size: 18px;"></i> </a></td>
</tr>
<tr data-key="1471355765062671700">
<td style="min-width: 40px; text-align: center;"><input class="selectionOvPersonIds" name="selection[]" type="checkbox" value="1471355765062671700" /></td>
<td style="min-width: 60px; text-align: center;">7</td>
<td style="min-width: 110px; text-align: left;">0010261551469</td>
<td style="min-width: 110px; text-align: left;">เชรี๊ยท</td>
<td style="min-width: 110px; text-align: left;">จอน</td>
<td style="min-width: 80px; text-align: center;"><a href="../../ckdnet/emr">EMR</a>&nbsp;</td>
<td style="min-width: 110px; text-align: center;">90&nbsp;</td>
<td style="min-width: 110px; text-align: center;">1&nbsp;</td>
<td style="min-width: 110px; text-align: center;">&nbsp;</td>
<td style="min-width: 110px; text-align: center;">56&nbsp;</td>
<td style="min-width: 110px; text-align: center;"><a title="" href="../../inputdata/step4?ezf_id=1463624356048989900&amp;target=MTQ3MTM1NTc2NTA2MjY3MTcwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" data-toggle="tooltip" data-original-title="(Submit/Save Draft) คลิกเพื่อแสดงข้อมูล"><span class="label label-default" style="font-size: 13px;">0 / 0</span> </a><a href="/inputdata/insert-record?insert=url&amp;ezf_id=1470104222001313900&amp;target=MTQ3MTM1NTc2NTAzODIzMzMwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" title="" data-toggle="tooltip" data-original-title="เพิ่มข้อมูล"><i class="glyphicon glyphicon-plus" style="color:#00a65a;font-size: 18px;"></i> </a></td>
<td style="min-width: 110px; text-align: center;"><a title="" href="../../inputdata/step4?ezf_id=1471483982079544300&amp;target=MTQ3MTM1NTc2NTA2MjY3MTcwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" data-toggle="tooltip" data-original-title="(Submit/Save Draft) คลิกเพื่อแสดงข้อมูล"><span class="label label-default" style="font-size: 13px;">0 / 0</span> </a><a href="/inputdata/insert-record?insert=url&amp;ezf_id=1470104222001313900&amp;target=MTQ3MTM1NTc2NTAzODIzMzMwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" title="" data-toggle="tooltip" data-original-title="เพิ่มข้อมูล"><i class="glyphicon glyphicon-plus" style="color:#00a65a;font-size: 18px;"></i> </a></td>
</tr>
<tr data-key="1471355765066582100">
<td style="min-width: 40px; text-align: center;"><input class="selectionOvPersonIds" name="selection[]" type="checkbox" value="1471355765066582100" /></td>
<td style="min-width: 60px; text-align: center;">8</td>
<td style="min-width: 110px; text-align: left;">0010291022430</td>
<td style="min-width: 110px; text-align: left;">เอ๋</td>
<td style="min-width: 110px; text-align: left;">ผงาตุนัด</td>
<td style="min-width: 80px; text-align: center;"><a href="../../ckdnet/emr">EMR</a>&nbsp;</td>
<td style="min-width: 110px; text-align: center;">60.5&nbsp;&nbsp;</td>
<td style="min-width: 110px; text-align: center;">2&nbsp;</td>
<td style="min-width: 110px; text-align: center;">&nbsp;</td>
<td style="min-width: 110px; text-align: center;">76&nbsp;</td>
<td style="min-width: 110px; text-align: center;"><a title="" href="../../inputdata/step4?ezf_id=1463624356048989900&amp;target=MTQ3MTM1NTc2NTA2NjU4MjEwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" data-toggle="tooltip" data-original-title="(Submit/Save Draft) คลิกเพื่อแสดงข้อมูล"><span class="label label-default" style="font-size: 13px;">0 / 0</span> </a><a href="/inputdata/insert-record?insert=url&amp;ezf_id=1470104222001313900&amp;target=MTQ3MTM1NTc2NTAzODIzMzMwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" title="" data-toggle="tooltip" data-original-title="เพิ่มข้อมูล"><i class="glyphicon glyphicon-plus" style="color:#00a65a;font-size: 18px;"></i> </a></td>
<td style="min-width: 110px; text-align: center;"><a title="" href="../../inputdata/step4?ezf_id=1471483982079544300&amp;target=MTQ3MTM1NTc2NTA2NjU4MjEwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" data-toggle="tooltip" data-original-title="(Submit/Save Draft) คลิกเพื่อแสดงข้อมูล"><span class="label label-default" style="font-size: 13px;">0 / 0</span> </a><a href="/inputdata/insert-record?insert=url&amp;ezf_id=1470104222001313900&amp;target=MTQ3MTM1NTc2NTAzODIzMzMwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" title="" data-toggle="tooltip" data-original-title="เพิ่มข้อมูล"><i class="glyphicon glyphicon-plus" style="color:#00a65a;font-size: 18px;"></i> </a></td>
</tr>
<tr data-key="1471355765071474900">
<td style="min-width: 40px; text-align: center;"><input class="selectionOvPersonIds" name="selection[]" type="checkbox" value="1471355765071474900" /></td>
<td style="min-width: 60px; text-align: center;">9</td>
<td style="min-width: 110px; text-align: left;">0011011209144</td>
<td style="min-width: 110px; text-align: left;">แต๋ว</td>
<td style="min-width: 110px; text-align: left;">แวน</td>
<td style="min-width: 80px; text-align: center;"><a href="../../ckdnet/emr">EMR</a></td>
<td style="min-width: 110px; text-align: center;">&nbsp;60.5&nbsp;</td>
<td style="min-width: 110px; text-align: center;">3&nbsp;</td>
<td style="min-width: 110px; text-align: center;">&nbsp;</td>
<td style="min-width: 110px; text-align: center;">98&nbsp;</td>
<td style="min-width: 110px; text-align: center;"><a title="" href="../../inputdata/step4?ezf_id=1463624356048989900&amp;target=MTQ3MTM1NTc2NTA3MTQ3NDkwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" data-toggle="tooltip" data-original-title="(Submit/Save Draft) คลิกเพื่อแสดงข้อมูล"><span class="label label-default" style="font-size: 13px;">0 / 0</span> </a><a href="/inputdata/insert-record?insert=url&amp;ezf_id=1470104222001313900&amp;target=MTQ3MTM1NTc2NTAzODIzMzMwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" title="" data-toggle="tooltip" data-original-title="เพิ่มข้อมูล"><i class="glyphicon glyphicon-plus" style="color:#00a65a;font-size: 18px;"></i> </a></td>
<td style="min-width: 110px; text-align: center;"><a title="" href="../../inputdata/step4?ezf_id=1471483982079544300&amp;target=MTQ3MTM1NTc2NTA3MTQ3NDkwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" data-toggle="tooltip" data-original-title="(Submit/Save Draft) คลิกเพื่อแสดงข้อมูล"><span class="label label-default" style="font-size: 13px;">0 / 0</span> </a><a href="/inputdata/insert-record?insert=url&amp;ezf_id=1470104222001313900&amp;target=MTQ3MTM1NTc2NTAzODIzMzMwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" title="" data-toggle="tooltip" data-original-title="เพิ่มข้อมูล"><i class="glyphicon glyphicon-plus" style="color:#00a65a;font-size: 18px;"></i> </a></td>
</tr>
<tr data-key="1471355765075383800">
<td style="min-width: 40px; text-align: center;"><input class="selectionOvPersonIds" name="selection[]" type="checkbox" value="1471355765075383800" /></td>
<td style="min-width: 60px; text-align: center;">10</td>
<td style="min-width: 110px; text-align: left;">0012011398274</td>
<td style="min-width: 110px; text-align: left;">ดา</td>
<td style="min-width: 110px; text-align: left;">ไม่มีนามสกุล</td>
<td style="min-width: 80px; text-align: center;">&nbsp;<a href="../../ckdnet/emr">EMR</a></td>
<td style="min-width: 110px; text-align: center;">&nbsp;</td>
<td style="min-width: 110px; text-align: center;">&nbsp;</td>
<td style="min-width: 110px; text-align: center;">&nbsp;</td>
<td style="min-width: 110px; text-align: center;">&nbsp;</td>
<td style="min-width: 110px; text-align: center;"><a title="" href="../../inputdata/step4?ezf_id=1463624356048989900&amp;target=MTQ3MTM1NTc2NTA3NTM4MzgwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" data-toggle="tooltip" data-original-title="(Submit/Save Draft) คลิกเพื่อแสดงข้อมูล"><span class="label label-default" style="font-size: 13px;">0 / 0</span> </a><a href="/inputdata/insert-record?insert=url&amp;ezf_id=1470104222001313900&amp;target=MTQ3MTM1NTc2NTAzODIzMzMwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" title="" data-toggle="tooltip" data-original-title="เพิ่มข้อมูล"><i class="glyphicon glyphicon-plus" style="color:#00a65a;font-size: 18px;"></i> </a></td>
<td style="min-width: 110px; text-align: center;"><a title="" href="../../inputdata/step4?ezf_id=1471483982079544300&amp;target=MTQ3MTM1NTc2NTA3NTM4MzgwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" data-toggle="tooltip" data-original-title="(Submit/Save Draft) คลิกเพื่อแสดงข้อมูล"><span class="label label-default" style="font-size: 13px;">0 / 0</span> </a><a href="/inputdata/insert-record?insert=url&amp;ezf_id=1470104222001313900&amp;target=MTQ3MTM1NTc2NTAzODIzMzMwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" title="" data-toggle="tooltip" data-original-title="เพิ่มข้อมูล"><i class="glyphicon glyphicon-plus" style="color:#00a65a;font-size: 18px;"></i> </a></td>
</tr>
<tr data-key="1471355765091021100">
<td style="min-width: 40px; text-align: center;"><input class="selectionOvPersonIds" name="selection[]" type="checkbox" value="1471355765091021100" /></td>
<td style="min-width: 60px; text-align: center;">11</td>
<td style="min-width: 110px; text-align: left;">0025011165523</td>
<td style="min-width: 110px; text-align: left;">ตู</td>
<td style="min-width: 110px; text-align: left;">ยอง</td>
<td style="min-width: 80px; text-align: center;">&nbsp;<a href="../../ckdnet/emr">EMR</a></td>
<td style="min-width: 110px; text-align: center;">&nbsp;</td>
<td style="min-width: 110px; text-align: center;">&nbsp;</td>
<td style="min-width: 110px; text-align: center;">&nbsp;</td>
<td style="min-width: 110px; text-align: center;">&nbsp;</td>
<td style="min-width: 110px; text-align: center;"><a title="" href="../../inputdata/step4?ezf_id=1463624356048989900&amp;target=MTQ3MTM1NTc2NTA5MTAyMTEwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" data-toggle="tooltip" data-original-title="(Submit/Save Draft) คลิกเพื่อแสดงข้อมูล"><span class="label label-default" style="font-size: 13px;">0 / 0</span> </a><a href="/inputdata/insert-record?insert=url&amp;ezf_id=1470104222001313900&amp;target=MTQ3MTM1NTc2NTAzODIzMzMwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" title="" data-toggle="tooltip" data-original-title="เพิ่มข้อมูล"><i class="glyphicon glyphicon-plus" style="color:#00a65a;font-size: 18px;"></i> </a></td>
<td style="min-width: 110px; text-align: center;"><a title="" href="../../inputdata/step4?ezf_id=1471483982079544300&amp;target=MTQ3MTM1NTc2NTA5MTAyMTEwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" data-toggle="tooltip" data-original-title="(Submit/Save Draft) คลิกเพื่อแสดงข้อมูล"><span class="label label-default" style="font-size: 13px;">0 / 0</span> </a><a href="/inputdata/insert-record?insert=url&amp;ezf_id=1470104222001313900&amp;target=MTQ3MTM1NTc2NTAzODIzMzMwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" title="" data-toggle="tooltip" data-original-title="เพิ่มข้อมูล"><i class="glyphicon glyphicon-plus" style="color:#00a65a;font-size: 18px;"></i> </a></td>
</tr>
<tr data-key="1471355765099818900">
<td style="min-width: 40px; text-align: center;"><input class="selectionOvPersonIds" name="selection[]" type="checkbox" value="1471355765099818900" /></td>
<td style="min-width: 60px; text-align: center;">12</td>
<td style="min-width: 110px; text-align: left;">0032011001323</td>
<td style="min-width: 110px; text-align: left;">โซเฟีย</td>
<td style="min-width: 110px; text-align: left;">ตี</td>
<td style="min-width: 80px; text-align: center;">&nbsp;<a href="../../ckdnet/emr">EMR</a></td>
<td style="min-width: 110px; text-align: center;">&nbsp;</td>
<td style="min-width: 110px; text-align: center;">&nbsp;</td>
<td style="min-width: 110px; text-align: center;">&nbsp;</td>
<td style="min-width: 110px; text-align: center;">&nbsp;</td>
<td style="min-width: 110px; text-align: center;"><a title="" href="../../inputdata/step4?ezf_id=1463624356048989900&amp;target=MTQ3MTM1NTc2NTA5OTgxODkwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" data-toggle="tooltip" data-original-title="(Submit/Save Draft) คลิกเพื่อแสดงข้อมูล"><span class="label label-default" style="font-size: 13px;">0 / 0</span> </a><a href="/inputdata/insert-record?insert=url&amp;ezf_id=1470104222001313900&amp;target=MTQ3MTM1NTc2NTAzODIzMzMwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" title="" data-toggle="tooltip" data-original-title="เพิ่มข้อมูล"><i class="glyphicon glyphicon-plus" style="color:#00a65a;font-size: 18px;"></i> </a></td>
<td style="min-width: 110px; text-align: center;"><a title="" href="../../inputdata/step4?ezf_id=1471483982079544300&amp;target=MTQ3MTM1NTc2NTA5OTgxODkwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" data-toggle="tooltip" data-original-title="(Submit/Save Draft) คลิกเพื่อแสดงข้อมูล"><span class="label label-default" style="font-size: 13px;">0 / 0</span> </a><a href="/inputdata/insert-record?insert=url&amp;ezf_id=1470104222001313900&amp;target=MTQ3MTM1NTc2NTAzODIzMzMwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" title="" data-toggle="tooltip" data-original-title="เพิ่มข้อมูล"><i class="glyphicon glyphicon-plus" style="color:#00a65a;font-size: 18px;"></i> </a></td>
</tr>
<tr data-key="1471355765103728300">
<td style="min-width: 40px; text-align: center;"><input class="selectionOvPersonIds" name="selection[]" type="checkbox" value="1471355765103728300" /></td>
<td style="min-width: 60px; text-align: center;">13</td>
<td style="min-width: 110px; text-align: left;">0032011005639</td>
<td style="min-width: 110px; text-align: left;">สเรือน</td>
<td style="min-width: 110px; text-align: left;">กัน</td>
<td style="min-width: 80px; text-align: center;">&nbsp;<a href="../../ckdnet/emr">EMR</a></td>
<td style="min-width: 110px; text-align: center;">&nbsp;</td>
<td style="min-width: 110px; text-align: center;">&nbsp;</td>
<td style="min-width: 110px; text-align: center;">&nbsp;</td>
<td style="min-width: 110px; text-align: center;">&nbsp;</td>
<td style="min-width: 110px; text-align: center;"><a title="" href="../../inputdata/step4?ezf_id=1463624356048989900&amp;target=MTQ3MTM1NTc2NTEwMzcyODMwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" data-toggle="tooltip" data-original-title="(Submit/Save Draft) คลิกเพื่อแสดงข้อมูล"><span class="label label-default" style="font-size: 13px;">0 / 0</span> </a><a href="/inputdata/insert-record?insert=url&amp;ezf_id=1470104222001313900&amp;target=MTQ3MTM1NTc2NTAzODIzMzMwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" title="" data-toggle="tooltip" data-original-title="เพิ่มข้อมูล"><i class="glyphicon glyphicon-plus" style="color:#00a65a;font-size: 18px;"></i> </a></td>
<td style="min-width: 110px; text-align: center;"><a title="" href="../../inputdata/step4?ezf_id=1471483982079544300&amp;target=MTQ3MTM1NTc2NTEwMzcyODMwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" data-toggle="tooltip" data-original-title="(Submit/Save Draft) คลิกเพื่อแสดงข้อมูล"><span class="label label-default" style="font-size: 13px;">0 / 0</span> </a><a href="/inputdata/insert-record?insert=url&amp;ezf_id=1470104222001313900&amp;target=MTQ3MTM1NTc2NTAzODIzMzMwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" title="" data-toggle="tooltip" data-original-title="เพิ่มข้อมูล"><i class="glyphicon glyphicon-plus" style="color:#00a65a;font-size: 18px;"></i> </a></td>
</tr>
<tr data-key="1471355765109599400">
<td style="min-width: 40px; text-align: center;"><input class="selectionOvPersonIds" name="selection[]" type="checkbox" value="1471355765109599400" /></td>
<td style="min-width: 60px; text-align: center;">14</td>
<td style="min-width: 110px; text-align: left;">0032011011167</td>
<td style="min-width: 110px; text-align: left;">ไล</td>
<td style="min-width: 110px; text-align: left;">ซู</td>
<td style="min-width: 80px; text-align: center;"><a href="../../ckdnet/emr">EMR</a>&nbsp;</td>
<td style="min-width: 110px; text-align: center;">&nbsp;</td>
<td style="min-width: 110px; text-align: center;">&nbsp;</td>
<td style="min-width: 110px; text-align: center;">&nbsp;</td>
<td style="min-width: 110px; text-align: center;">&nbsp;</td>
<td style="min-width: 110px; text-align: center;"><a title="" href="../../inputdata/step4?ezf_id=1463624356048989900&amp;target=MTQ3MTM1NTc2NTEwOTU5OTQwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" data-toggle="tooltip" data-original-title="(Submit/Save Draft) คลิกเพื่อแสดงข้อมูล"><span class="label label-default" style="font-size: 13px;">0 / 0</span> </a><a href="/inputdata/insert-record?insert=url&amp;ezf_id=1470104222001313900&amp;target=MTQ3MTM1NTc2NTAzODIzMzMwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" title="" data-toggle="tooltip" data-original-title="เพิ่มข้อมูล"><i class="glyphicon glyphicon-plus" style="color:#00a65a;font-size: 18px;"></i> </a></td>
<td style="min-width: 110px; text-align: center;"><a title="" href="../../inputdata/step4?ezf_id=1471483982079544300&amp;target=MTQ3MTM1NTc2NTEwOTU5OTQwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" data-toggle="tooltip" data-original-title="(Submit/Save Draft) คลิกเพื่อแสดงข้อมูล"><span class="label label-default" style="font-size: 13px;">0 / 0</span> </a><a href="/inputdata/insert-record?insert=url&amp;ezf_id=1470104222001313900&amp;target=MTQ3MTM1NTc2NTAzODIzMzMwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" title="" data-toggle="tooltip" data-original-title="เพิ่มข้อมูล"><i class="glyphicon glyphicon-plus" style="color:#00a65a;font-size: 18px;"></i> </a></td>
</tr>
<tr data-key="1471355765114490700">
<td style="min-width: 40px; text-align: center;"><input class="selectionOvPersonIds" name="selection[]" type="checkbox" value="1471355765114490700" /></td>
<td style="min-width: 60px; text-align: center;">15</td>
<td style="min-width: 110px; text-align: left;">0032011013330</td>
<td style="min-width: 110px; text-align: left;">ตังงิม</td>
<td style="min-width: 110px; text-align: left;">ยืน</td>
<td style="min-width: 80px; text-align: center;"><a href="../../ckdnet/emr">EMR</a>&nbsp;</td>
<td style="min-width: 110px; text-align: center;">&nbsp;</td>
<td style="min-width: 110px; text-align: center;">&nbsp;</td>
<td style="min-width: 110px; text-align: center;">&nbsp;</td>
<td style="min-width: 110px; text-align: center;">&nbsp;</td>
<td style="min-width: 110px; text-align: center;"><a title="" href="../../inputdata/step4?ezf_id=1463624356048989900&amp;target=MTQ3MTM1NTc2NTExNDQ5MDcwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" data-toggle="tooltip" data-original-title="(Submit/Save Draft) คลิกเพื่อแสดงข้อมูล"><span class="label label-default" style="font-size: 13px;">0 / 0</span> </a><a href="/inputdata/insert-record?insert=url&amp;ezf_id=1470104222001313900&amp;target=MTQ3MTM1NTc2NTAzODIzMzMwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" title="" data-toggle="tooltip" data-original-title="เพิ่มข้อมูล"><i class="glyphicon glyphicon-plus" style="color:#00a65a;font-size: 18px;"></i> </a></td>
<td style="min-width: 110px; text-align: center;"><a title="" href="../../inputdata/step4?ezf_id=1471483982079544300&amp;target=MTQ3MTM1NTc2NTExNDQ5MDcwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" data-toggle="tooltip" data-original-title="(Submit/Save Draft) คลิกเพื่อแสดงข้อมูล"><span class="label label-default" style="font-size: 13px;">0 / 0</span> </a><a href="/inputdata/insert-record?insert=url&amp;ezf_id=1470104222001313900&amp;target=MTQ3MTM1NTc2NTAzODIzMzMwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" title="" data-toggle="tooltip" data-original-title="เพิ่มข้อมูล"><i class="glyphicon glyphicon-plus" style="color:#00a65a;font-size: 18px;"></i> </a></td>
</tr>
<tr data-key="1471355765118391800">
<td style="min-width: 40px; text-align: center;"><input class="selectionOvPersonIds" name="selection[]" type="checkbox" value="1471355765118391800" /></td>
<td style="min-width: 60px; text-align: center;">16</td>
<td style="min-width: 110px; text-align: left;">0032011015634</td>
<td style="min-width: 110px; text-align: left;">เจิน</td>
<td style="min-width: 110px; text-align: left;">ศรี</td>
<td style="min-width: 80px; text-align: center;">&nbsp;<a href="../../ckdnet/emr">EMR</a></td>
<td style="min-width: 110px; text-align: center;">&nbsp;</td>
<td style="min-width: 110px; text-align: center;">&nbsp;</td>
<td style="min-width: 110px; text-align: center;">&nbsp;</td>
<td style="min-width: 110px; text-align: center;">&nbsp;</td>
<td style="min-width: 110px; text-align: center;"><a title="" href="../../inputdata/step4?ezf_id=1463624356048989900&amp;target=MTQ3MTM1NTc2NTExODM5MTgwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" data-toggle="tooltip" data-original-title="(Submit/Save Draft) คลิกเพื่อแสดงข้อมูล"><span class="label label-default" style="font-size: 13px;">0 / 0</span> </a><a href="/inputdata/insert-record?insert=url&amp;ezf_id=1470104222001313900&amp;target=MTQ3MTM1NTc2NTAzODIzMzMwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" title="" data-toggle="tooltip" data-original-title="เพิ่มข้อมูล"><i class="glyphicon glyphicon-plus" style="color:#00a65a;font-size: 18px;"></i> </a></td>
<td style="min-width: 110px; text-align: center;"><a title="" href="../../inputdata/step4?ezf_id=1471483982079544300&amp;target=MTQ3MTM1NTc2NTExODM5MTgwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" data-toggle="tooltip" data-original-title="(Submit/Save Draft) คลิกเพื่อแสดงข้อมูล"><span class="label label-default" style="font-size: 13px;">0 / 0</span> </a><a href="/inputdata/insert-record?insert=url&amp;ezf_id=1470104222001313900&amp;target=MTQ3MTM1NTc2NTAzODIzMzMwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" title="" data-toggle="tooltip" data-original-title="เพิ่มข้อมูล"><i class="glyphicon glyphicon-plus" style="color:#00a65a;font-size: 18px;"></i> </a></td>
</tr>
<tr data-key="1471355765120347300">
<td style="min-width: 40px; text-align: center;"><input class="selectionOvPersonIds" name="selection[]" type="checkbox" value="1471355765120347300" /></td>
<td style="min-width: 60px; text-align: center;">17</td>
<td style="min-width: 110px; text-align: left;">0032011015855</td>
<td style="min-width: 110px; text-align: left;">แดง</td>
<td style="min-width: 110px; text-align: left;">ไชยยะแสง</td>
<td style="min-width: 80px; text-align: center;"><a href="../../ckdnet/emr">EMR</a>&nbsp;</td>
<td style="min-width: 110px; text-align: center;">&nbsp;</td>
<td style="min-width: 110px; text-align: center;">&nbsp;</td>
<td style="min-width: 110px; text-align: center;">&nbsp;</td>
<td style="min-width: 110px; text-align: center;">&nbsp;</td>
<td style="min-width: 110px; text-align: center;"><a title="" href="../../inputdata/step4?ezf_id=1463624356048989900&amp;target=MTQ3MTM1NTc2NTEyMDM0NzMwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" data-toggle="tooltip" data-original-title="(Submit/Save Draft) คลิกเพื่อแสดงข้อมูล"><span class="label label-default" style="font-size: 13px;">0 / 0</span> </a><a href="/inputdata/insert-record?insert=url&amp;ezf_id=1470104222001313900&amp;target=MTQ3MTM1NTc2NTAzODIzMzMwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" title="" data-toggle="tooltip" data-original-title="เพิ่มข้อมูล"><i class="glyphicon glyphicon-plus" style="color:#00a65a;font-size: 18px;"></i> </a></td>
<td style="min-width: 110px; text-align: center;"><a title="" href="../../inputdata/step4?ezf_id=1471483982079544300&amp;target=MTQ3MTM1NTc2NTEyMDM0NzMwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" data-toggle="tooltip" data-original-title="(Submit/Save Draft) คลิกเพื่อแสดงข้อมูล"><span class="label label-default" style="font-size: 13px;">0 / 0</span> </a><a href="/inputdata/insert-record?insert=url&amp;ezf_id=1470104222001313900&amp;target=MTQ3MTM1NTc2NTAzODIzMzMwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" title="" data-toggle="tooltip" data-original-title="เพิ่มข้อมูล"><i class="glyphicon glyphicon-plus" style="color:#00a65a;font-size: 18px;"></i> </a></td>
</tr>
<tr data-key="1471355765124259700">
<td style="min-width: 40px; text-align: center;"><input class="selectionOvPersonIds" name="selection[]" type="checkbox" value="1471355765124259700" /></td>
<td style="min-width: 60px; text-align: center;">18</td>
<td style="min-width: 110px; text-align: left;">0032011016258</td>
<td style="min-width: 110px; text-align: left;">คาโคตี</td>
<td style="min-width: 110px; text-align: left;">สรอ</td>
<td style="min-width: 80px; text-align: center;"><a href="../../ckdnet/emr">EMR</a>&nbsp;</td>
<td style="min-width: 110px; text-align: center;">&nbsp;</td>
<td style="min-width: 110px; text-align: center;">&nbsp;</td>
<td style="min-width: 110px; text-align: center;">&nbsp;</td>
<td style="min-width: 110px; text-align: center;">&nbsp;</td>
<td style="min-width: 110px; text-align: center;"><a title="" href="../../inputdata/step4?ezf_id=1463624356048989900&amp;target=MTQ3MTM1NTc2NTEyNDI1OTcwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" data-toggle="tooltip" data-original-title="(Submit/Save Draft) คลิกเพื่อแสดงข้อมูล"><span class="label label-default" style="font-size: 13px;">0 / 0</span> </a><a href="/inputdata/insert-record?insert=url&amp;ezf_id=1470104222001313900&amp;target=MTQ3MTM1NTc2NTAzODIzMzMwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" title="" data-toggle="tooltip" data-original-title="เพิ่มข้อมูล"><i class="glyphicon glyphicon-plus" style="color:#00a65a;font-size: 18px;"></i> </a></td>
<td style="min-width: 110px; text-align: center;"><a title="" href="../../inputdata/step4?ezf_id=1471483982079544300&amp;target=MTQ3MTM1NTc2NTEyNDI1OTcwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" data-toggle="tooltip" data-original-title="(Submit/Save Draft) คลิกเพื่อแสดงข้อมูล"><span class="label label-default" style="font-size: 13px;">0 / 0</span> </a><a href="/inputdata/insert-record?insert=url&amp;ezf_id=1470104222001313900&amp;target=MTQ3MTM1NTc2NTAzODIzMzMwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" title="" data-toggle="tooltip" data-original-title="เพิ่มข้อมูล"><i class="glyphicon glyphicon-plus" style="color:#00a65a;font-size: 18px;"></i> </a></td>
</tr>
<tr data-key="1471355765127190700">
<td style="min-width: 40px; text-align: center;"><input class="selectionOvPersonIds" name="selection[]" type="checkbox" value="1471355765127190700" /></td>
<td style="min-width: 60px; text-align: center;">19</td>
<td style="min-width: 110px; text-align: left;">0032011016592</td>
<td style="min-width: 110px; text-align: left;">สินา</td>
<td style="min-width: 110px; text-align: left;">ดวง</td>
<td style="min-width: 80px; text-align: center;"><a href="../../ckdnet/emr">EMR</a>&nbsp;</td>
<td style="min-width: 110px; text-align: center;">&nbsp;</td>
<td style="min-width: 110px; text-align: center;">&nbsp;</td>
<td style="min-width: 110px; text-align: center;">&nbsp;</td>
<td style="min-width: 110px; text-align: center;">&nbsp;</td>
<td style="min-width: 110px; text-align: center;"><a title="" href="../../inputdata/step4?ezf_id=1463624356048989900&amp;target=MTQ3MTM1NTc2NTEyNzE5MDcwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" data-toggle="tooltip" data-original-title="(Submit/Save Draft) คลิกเพื่อแสดงข้อมูล"><span class="label label-default" style="font-size: 13px;">0 / 0</span> </a><a href="/inputdata/insert-record?insert=url&amp;ezf_id=1470104222001313900&amp;target=MTQ3MTM1NTc2NTAzODIzMzMwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" title="" data-toggle="tooltip" data-original-title="เพิ่มข้อมูล"><i class="glyphicon glyphicon-plus" style="color:#00a65a;font-size: 18px;"></i> </a></td>
<td style="min-width: 110px; text-align: center;"><a title="" href="../../inputdata/step4?ezf_id=1471483982079544300&amp;target=MTQ3MTM1NTc2NTEyNzE5MDcwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" data-toggle="tooltip" data-original-title="(Submit/Save Draft) คลิกเพื่อแสดงข้อมูล"><span class="label label-default" style="font-size: 13px;">0 / 0</span> </a><a href="/inputdata/insert-record?insert=url&amp;ezf_id=1470104222001313900&amp;target=MTQ3MTM1NTc2NTAzODIzMzMwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" title="" data-toggle="tooltip" data-original-title="เพิ่มข้อมูล"><i class="glyphicon glyphicon-plus" style="color:#00a65a;font-size: 18px;"></i> </a></td>
</tr>
<tr data-key="1471355765129146200">
<td style="min-width: 40px; text-align: center;"><input class="selectionOvPersonIds" name="selection[]" type="checkbox" value="1471355765129146200" /></td>
<td style="min-width: 60px; text-align: center;">20</td>
<td style="min-width: 110px; text-align: left;">0032011017670</td>
<td style="min-width: 110px; text-align: left;">เหงี๊ยน</td>
<td style="min-width: 110px; text-align: left;">โกนงา</td>
<td style="min-width: 80px; text-align: center;"><a href="../../ckdnet/emr">EMR</a>&nbsp;</td>
<td style="min-width: 110px; text-align: center;">&nbsp;</td>
<td style="min-width: 110px; text-align: center;">&nbsp;</td>
<td style="min-width: 110px; text-align: center;">&nbsp;</td>
<td style="min-width: 110px; text-align: center;">&nbsp;</td>
<td style="min-width: 110px; text-align: center;"><a title="" href="../../inputdata/step4?ezf_id=1463624356048989900&amp;target=MTQ3MTM1NTc2NTEyOTE0NjIwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" data-toggle="tooltip" data-original-title="(Submit/Save Draft) คลิกเพื่อแสดงข้อมูล"><span class="label label-default" style="font-size: 13px;">0 / 0</span> </a><a href="/inputdata/insert-record?insert=url&amp;ezf_id=1470104222001313900&amp;target=MTQ3MTM1NTc2NTAzODIzMzMwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" title="" data-toggle="tooltip" data-original-title="เพิ่มข้อมูล"><i class="glyphicon glyphicon-plus" style="color:#00a65a;font-size: 18px;"></i> </a></td>
<td style="min-width: 110px; text-align: center;"><a title="" href="../../inputdata/step4?ezf_id=1471483982079544300&amp;target=MTQ3MTM1NTc2NTEyOTE0NjIwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" data-toggle="tooltip" data-original-title="(Submit/Save Draft) คลิกเพื่อแสดงข้อมูล"><span class="label label-default" style="font-size: 13px;">0 / 0</span> </a><a href="/inputdata/insert-record?insert=url&amp;ezf_id=1470104222001313900&amp;target=MTQ3MTM1NTc2NTAzODIzMzMwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" title="" data-toggle="tooltip" data-original-title="เพิ่มข้อมูล"><i class="glyphicon glyphicon-plus" style="color:#00a65a;font-size: 18px;"></i> </a></td>
</tr>
<tr data-key="1471355765132083500">
<td style="min-width: 40px; text-align: center;"><input class="selectionOvPersonIds" name="selection[]" type="checkbox" value="1471355765132083500" /></td>
<td style="min-width: 60px; text-align: center;">21</td>
<td style="min-width: 110px; text-align: left;">0032011018102</td>
<td style="min-width: 110px; text-align: left;">จนา</td>
<td style="min-width: 110px; text-align: left;">แซด</td>
<td style="min-width: 80px; text-align: center;"><a href="../../ckdnet/emr">EMR</a>&nbsp;</td>
<td style="min-width: 110px; text-align: center;">&nbsp;</td>
<td style="min-width: 110px; text-align: center;">&nbsp;</td>
<td style="min-width: 110px; text-align: center;">&nbsp;</td>
<td style="min-width: 110px; text-align: center;">&nbsp;</td>
<td style="min-width: 110px; text-align: center;"><a title="" href="../../inputdata/step4?ezf_id=1463624356048989900&amp;target=MTQ3MTM1NTc2NTEzMjA4MzUwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" data-toggle="tooltip" data-original-title="(Submit/Save Draft) คลิกเพื่อแสดงข้อมูล"><span class="label label-default" style="font-size: 13px;">0 / 0</span> </a><a href="/inputdata/insert-record?insert=url&amp;ezf_id=1470104222001313900&amp;target=MTQ3MTM1NTc2NTAzODIzMzMwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" title="" data-toggle="tooltip" data-original-title="เพิ่มข้อมูล"><i class="glyphicon glyphicon-plus" style="color:#00a65a;font-size: 18px;"></i> </a></td>
<td style="min-width: 110px; text-align: center;"><a title="" href="../../inputdata/step4?ezf_id=1471483982079544300&amp;target=MTQ3MTM1NTc2NTEzMjA4MzUwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" data-toggle="tooltip" data-original-title="(Submit/Save Draft) คลิกเพื่อแสดงข้อมูล"><span class="label label-default" style="font-size: 13px;">0 / 0</span> </a><a href="/inputdata/insert-record?insert=url&amp;ezf_id=1470104222001313900&amp;target=MTQ3MTM1NTc2NTAzODIzMzMwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" title="" data-toggle="tooltip" data-original-title="เพิ่มข้อมูล"><i class="glyphicon glyphicon-plus" style="color:#00a65a;font-size: 18px;"></i> </a></td>
</tr>
<tr data-key="1471355765135012100">
<td style="min-width: 40px; text-align: center;"><input class="selectionOvPersonIds" name="selection[]" type="checkbox" value="1471355765135012100" /></td>
<td style="min-width: 60px; text-align: center;">22</td>
<td style="min-width: 110px; text-align: left;">0032011018579</td>
<td style="min-width: 110px; text-align: left;">แสงจันทร์</td>
<td style="min-width: 110px; text-align: left;">ไม่มีนามสกุล</td>
<td style="min-width: 80px; text-align: center;"><a href="../../ckdnet/emr">EMR</a>&nbsp;</td>
<td style="min-width: 110px; text-align: center;">&nbsp;</td>
<td style="min-width: 110px; text-align: center;">&nbsp;</td>
<td style="min-width: 110px; text-align: center;">&nbsp;</td>
<td style="min-width: 110px; text-align: center;">&nbsp;</td>
<td style="min-width: 110px; text-align: center;"><a title="" href="../../inputdata/step4?ezf_id=1463624356048989900&amp;target=MTQ3MTM1NTc2NTEzNTAxMjEwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" data-toggle="tooltip" data-original-title="(Submit/Save Draft) คลิกเพื่อแสดงข้อมูล"><span class="label label-default" style="font-size: 13px;">0 / 0</span> </a><a href="/inputdata/insert-record?insert=url&amp;ezf_id=1470104222001313900&amp;target=MTQ3MTM1NTc2NTAzODIzMzMwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" title="" data-toggle="tooltip" data-original-title="เพิ่มข้อมูล"><i class="glyphicon glyphicon-plus" style="color:#00a65a;font-size: 18px;"></i> </a></td>
<td style="min-width: 110px; text-align: center;"><a title="" href="../../inputdata/step4?ezf_id=1471483982079544300&amp;target=MTQ3MTM1NTc2NTEzNTAxMjEwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" data-toggle="tooltip" data-original-title="(Submit/Save Draft) คลิกเพื่อแสดงข้อมูล"><span class="label label-default" style="font-size: 13px;">0 / 0</span> </a><a href="/inputdata/insert-record?insert=url&amp;ezf_id=1470104222001313900&amp;target=MTQ3MTM1NTc2NTAzODIzMzMwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" title="" data-toggle="tooltip" data-original-title="เพิ่มข้อมูล"><i class="glyphicon glyphicon-plus" style="color:#00a65a;font-size: 18px;"></i> </a></td>
</tr>
<tr data-key="1471355765137943500">
<td style="min-width: 40px; text-align: center;"><input class="selectionOvPersonIds" name="selection[]" type="checkbox" value="1471355765137943500" /></td>
<td style="min-width: 60px; text-align: center;">23</td>
<td style="min-width: 110px; text-align: left;">0032011019001</td>
<td style="min-width: 110px; text-align: left;">ซารอส</td>
<td style="min-width: 110px; text-align: left;">โอ๊ต</td>
<td style="min-width: 80px; text-align: center;"><a href="../../ckdnet/emr">EMR</a>&nbsp;</td>
<td style="min-width: 110px; text-align: center;">&nbsp;</td>
<td style="min-width: 110px; text-align: center;">&nbsp;</td>
<td style="min-width: 110px; text-align: center;">&nbsp;</td>
<td style="min-width: 110px; text-align: center;">&nbsp;</td>
<td style="min-width: 110px; text-align: center;"><a title="" href="../../inputdata/step4?ezf_id=1463624356048989900&amp;target=MTQ3MTM1NTc2NTEzNzk0MzUwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" data-toggle="tooltip" data-original-title="(Submit/Save Draft) คลิกเพื่อแสดงข้อมูล"><span class="label label-default" style="font-size: 13px;">0 / 0</span> </a><a href="/inputdata/insert-record?insert=url&amp;ezf_id=1470104222001313900&amp;target=MTQ3MTM1NTc2NTAzODIzMzMwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" title="" data-toggle="tooltip" data-original-title="เพิ่มข้อมูล"><i class="glyphicon glyphicon-plus" style="color:#00a65a;font-size: 18px;"></i> </a></td>
<td style="min-width: 110px; text-align: center;"><a title="" href="../../inputdata/step4?ezf_id=1471483982079544300&amp;target=MTQ3MTM1NTc2NTEzNzk0MzUwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" data-toggle="tooltip" data-original-title="(Submit/Save Draft) คลิกเพื่อแสดงข้อมูล"><span class="label label-default" style="font-size: 13px;">0 / 0</span> </a><a href="/inputdata/insert-record?insert=url&amp;ezf_id=1470104222001313900&amp;target=MTQ3MTM1NTc2NTAzODIzMzMwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" title="" data-toggle="tooltip" data-original-title="เพิ่มข้อมูล"><i class="glyphicon glyphicon-plus" style="color:#00a65a;font-size: 18px;"></i> </a></td>
</tr>
<tr data-key="1471355765142831500">
<td style="min-width: 40px; text-align: center;"><input class="selectionOvPersonIds" name="selection[]" type="checkbox" value="1471355765142831500" /></td>
<td style="min-width: 60px; text-align: center;">24</td>
<td style="min-width: 110px; text-align: left;">0032011020697</td>
<td style="min-width: 110px; text-align: left;">ดวงสนิท</td>
<td style="min-width: 110px; text-align: left;">จันทร์</td>
<td style="min-width: 80px; text-align: center;">&nbsp;<a href="../../ckdnet/emr">EMR</a></td>
<td style="min-width: 110px; text-align: center;">&nbsp;</td>
<td style="min-width: 110px; text-align: center;">&nbsp;</td>
<td style="min-width: 110px; text-align: center;">&nbsp;</td>
<td style="min-width: 110px; text-align: center;">&nbsp;</td>
<td style="min-width: 110px; text-align: center;"><a title="" href="../../inputdata/step4?ezf_id=1463624356048989900&amp;target=MTQ3MTM1NTc2NTE0MjgzMTUwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" data-toggle="tooltip" data-original-title="(Submit/Save Draft) คลิกเพื่อแสดงข้อมูล"><span class="label label-default" style="font-size: 13px;">0 / 0</span> </a><a href="/inputdata/insert-record?insert=url&amp;ezf_id=1470104222001313900&amp;target=MTQ3MTM1NTc2NTAzODIzMzMwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" title="" data-toggle="tooltip" data-original-title="เพิ่มข้อมูล"><i class="glyphicon glyphicon-plus" style="color:#00a65a;font-size: 18px;"></i> </a></td>
<td style="min-width: 110px; text-align: center;"><a title="" href="../../inputdata/step4?ezf_id=1471483982079544300&amp;target=MTQ3MTM1NTc2NTE0MjgzMTUwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" data-toggle="tooltip" data-original-title="(Submit/Save Draft) คลิกเพื่อแสดงข้อมูล"><span class="label label-default" style="font-size: 13px;">0 / 0</span> </a><a href="/inputdata/insert-record?insert=url&amp;ezf_id=1470104222001313900&amp;target=MTQ3MTM1NTc2NTAzODIzMzMwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" title="" data-toggle="tooltip" data-original-title="เพิ่มข้อมูล"><i class="glyphicon glyphicon-plus" style="color:#00a65a;font-size: 18px;"></i> </a></td>
</tr>
<tr data-key="1471355765145764900">
<td style="min-width: 40px; text-align: center;"><input class="selectionOvPersonIds" name="selection[]" type="checkbox" value="1471355765145764900" /></td>
<td style="min-width: 60px; text-align: center;">25</td>
<td style="min-width: 110px; text-align: left;">0032011020905</td>
<td style="min-width: 110px; text-align: left;">สินา</td>
<td style="min-width: 110px; text-align: left;">เลง</td>
<td style="min-width: 80px; text-align: center;">&nbsp;<a href="../../ckdnet/emr">EMR</a></td>
<td style="min-width: 110px; text-align: center;">&nbsp;</td>
<td style="min-width: 110px; text-align: center;">&nbsp;</td>
<td style="min-width: 110px; text-align: center;">&nbsp;</td>
<td style="min-width: 110px; text-align: center;">&nbsp;</td>
<td style="min-width: 110px; text-align: center;"><a title="" href="../../inputdata/step4?ezf_id=1463624356048989900&amp;target=MTQ3MTM1NTc2NTE0NTc2NDkwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" data-toggle="tooltip" data-original-title="(Submit/Save Draft) คลิกเพื่อแสดงข้อมูล"><span class="label label-default" style="font-size: 13px;">0 / 0</span> </a><a href="/inputdata/insert-record?insert=url&amp;ezf_id=1470104222001313900&amp;target=MTQ3MTM1NTc2NTAzODIzMzMwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" title="" data-toggle="tooltip" data-original-title="เพิ่มข้อมูล"><i class="glyphicon glyphicon-plus" style="color:#00a65a;font-size: 18px;"></i> </a></td>
<td style="min-width: 110px; text-align: center;"><a title="" href="../../inputdata/step4?ezf_id=1471483982079544300&amp;target=MTQ3MTM1NTc2NTE0NTc2NDkwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" data-toggle="tooltip" data-original-title="(Submit/Save Draft) คลิกเพื่อแสดงข้อมูล"><span class="label label-default" style="font-size: 13px;">0 / 0</span> </a><a href="/inputdata/insert-record?insert=url&amp;ezf_id=1470104222001313900&amp;target=MTQ3MTM1NTc2NTAzODIzMzMwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" title="" data-toggle="tooltip" data-original-title="เพิ่มข้อมูล"><i class="glyphicon glyphicon-plus" style="color:#00a65a;font-size: 18px;"></i> </a></td>
</tr>
<tr data-key="1471355765148701300">
<td style="min-width: 40px; text-align: center;"><input class="selectionOvPersonIds" name="selection[]" type="checkbox" value="1471355765148701300" /></td>
<td style="min-width: 60px; text-align: center;">26</td>
<td style="min-width: 110px; text-align: left;">0032011021472</td>
<td style="min-width: 110px; text-align: left;">วุฒิ</td>
<td style="min-width: 110px; text-align: left;">จันทร์</td>
<td style="min-width: 80px; text-align: center;">&nbsp;<a href="../../ckdnet/emr">EMR</a></td>
<td style="min-width: 110px; text-align: center;">&nbsp;</td>
<td style="min-width: 110px; text-align: center;">&nbsp;</td>
<td style="min-width: 110px; text-align: center;">&nbsp;</td>
<td style="min-width: 110px; text-align: center;">&nbsp;</td>
<td style="min-width: 110px; text-align: center;"><a title="" href="../../inputdata/step4?ezf_id=1463624356048989900&amp;target=MTQ3MTM1NTc2NTE0ODcwMTMwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" data-toggle="tooltip" data-original-title="(Submit/Save Draft) คลิกเพื่อแสดงข้อมูล"><span class="label label-default" style="font-size: 13px;">0 / 0</span> </a><a href="/inputdata/insert-record?insert=url&amp;ezf_id=1470104222001313900&amp;target=MTQ3MTM1NTc2NTAzODIzMzMwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" title="" data-toggle="tooltip" data-original-title="เพิ่มข้อมูล"><i class="glyphicon glyphicon-plus" style="color:#00a65a;font-size: 18px;"></i> </a></td>
<td style="min-width: 110px; text-align: center;"><a title="" href="../../inputdata/step4?ezf_id=1471483982079544300&amp;target=MTQ3MTM1NTc2NTE0ODcwMTMwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" data-toggle="tooltip" data-original-title="(Submit/Save Draft) คลิกเพื่อแสดงข้อมูล"><span class="label label-default" style="font-size: 13px;">0 / 0</span> </a><a href="/inputdata/insert-record?insert=url&amp;ezf_id=1470104222001313900&amp;target=MTQ3MTM1NTc2NTAzODIzMzMwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" title="" data-toggle="tooltip" data-original-title="เพิ่มข้อมูล"><i class="glyphicon glyphicon-plus" style="color:#00a65a;font-size: 18px;"></i> </a></td>
</tr>
<tr data-key="1471355765150657200">
<td style="min-width: 40px; text-align: center;"><input class="selectionOvPersonIds" name="selection[]" type="checkbox" value="1471355765150657200" /></td>
<td style="min-width: 60px; text-align: center;">27</td>
<td style="min-width: 110px; text-align: left;">0032011021499</td>
<td style="min-width: 110px; text-align: left;">เลือม</td>
<td style="min-width: 110px; text-align: left;">โลย</td>
<td style="min-width: 80px; text-align: center;">&nbsp;<a href="../../ckdnet/emr">EMR</a></td>
<td style="min-width: 110px; text-align: center;">&nbsp;</td>
<td style="min-width: 110px; text-align: center;">&nbsp;</td>
<td style="min-width: 110px; text-align: center;">&nbsp;</td>
<td style="min-width: 110px; text-align: center;">&nbsp;</td>
<td style="min-width: 110px; text-align: center;"><a title="" href="../../inputdata/step4?ezf_id=1463624356048989900&amp;target=MTQ3MTM1NTc2NTE1MDY1NzIwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" data-toggle="tooltip" data-original-title="(Submit/Save Draft) คลิกเพื่อแสดงข้อมูล"><span class="label label-default" style="font-size: 13px;">0 / 0</span> </a><a href="/inputdata/insert-record?insert=url&amp;ezf_id=1470104222001313900&amp;target=MTQ3MTM1NTc2NTAzODIzMzMwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" title="" data-toggle="tooltip" data-original-title="เพิ่มข้อมูล"><i class="glyphicon glyphicon-plus" style="color:#00a65a;font-size: 18px;"></i> </a></td>
<td style="min-width: 110px; text-align: center;"><a title="" href="../../inputdata/step4?ezf_id=1471483982079544300&amp;target=MTQ3MTM1NTc2NTE1MDY1NzIwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" data-toggle="tooltip" data-original-title="(Submit/Save Draft) คลิกเพื่อแสดงข้อมูล"><span class="label label-default" style="font-size: 13px;">0 / 0</span> </a><a href="/inputdata/insert-record?insert=url&amp;ezf_id=1470104222001313900&amp;target=MTQ3MTM1NTc2NTAzODIzMzMwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" title="" data-toggle="tooltip" data-original-title="เพิ่มข้อมูล"><i class="glyphicon glyphicon-plus" style="color:#00a65a;font-size: 18px;"></i> </a></td>
</tr>
<tr data-key="1471355765153586300">
<td style="min-width: 40px; text-align: center;"><input class="selectionOvPersonIds" name="selection[]" type="checkbox" value="1471355765153586300" /></td>
<td style="min-width: 60px; text-align: center;">28</td>
<td style="min-width: 110px; text-align: left;">0032011022118</td>
<td style="min-width: 110px; text-align: left;">ลีน่า</td>
<td style="min-width: 110px; text-align: left;">ยอด</td>
<td style="min-width: 80px; text-align: center;">&nbsp;<a href="../../ckdnet/emr">EMR</a></td>
<td style="min-width: 110px; text-align: center;">&nbsp;</td>
<td style="min-width: 110px; text-align: center;">&nbsp;</td>
<td style="min-width: 110px; text-align: center;">&nbsp;</td>
<td style="min-width: 110px; text-align: center;">&nbsp;</td>
<td style="min-width: 110px; text-align: center;"><a title="" href="../../inputdata/step4?ezf_id=1463624356048989900&amp;target=MTQ3MTM1NTc2NTE1MzU4NjMwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" data-toggle="tooltip" data-original-title="(Submit/Save Draft) คลิกเพื่อแสดงข้อมูล"><span class="label label-default" style="font-size: 13px;">0 / 0</span> </a><a href="/inputdata/insert-record?insert=url&amp;ezf_id=1470104222001313900&amp;target=MTQ3MTM1NTc2NTAzODIzMzMwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" title="" data-toggle="tooltip" data-original-title="เพิ่มข้อมูล"><i class="glyphicon glyphicon-plus" style="color:#00a65a;font-size: 18px;"></i> </a></td>
<td style="min-width: 110px; text-align: center;"><a title="" href="../../inputdata/step4?ezf_id=1471483982079544300&amp;target=MTQ3MTM1NTc2NTE1MzU4NjMwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" data-toggle="tooltip" data-original-title="(Submit/Save Draft) คลิกเพื่อแสดงข้อมูล"><span class="label label-default" style="font-size: 13px;">0 / 0</span> </a><a href="/inputdata/insert-record?insert=url&amp;ezf_id=1470104222001313900&amp;target=MTQ3MTM1NTc2NTAzODIzMzMwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" title="" data-toggle="tooltip" data-original-title="เพิ่มข้อมูล"><i class="glyphicon glyphicon-plus" style="color:#00a65a;font-size: 18px;"></i> </a></td>
</tr>
<tr data-key="1471355765157501500">
<td style="min-width: 40px; text-align: center;"><input class="selectionOvPersonIds" name="selection[]" type="checkbox" value="1471355765157501500" /></td>
<td style="min-width: 60px; text-align: center;">29</td>
<td style="min-width: 110px; text-align: left;">0032011022142</td>
<td style="min-width: 110px; text-align: left;">สะวิช</td>
<td style="min-width: 110px; text-align: left;">โซ</td>
<td style="min-width: 80px; text-align: center;"><a href="../../ckdnet/emr">EMR</a>&nbsp;</td>
<td style="min-width: 110px; text-align: center;">&nbsp;</td>
<td style="min-width: 110px; text-align: center;">&nbsp;</td>
<td style="min-width: 110px; text-align: center;">&nbsp;</td>
<td style="min-width: 110px; text-align: center;">&nbsp;</td>
<td style="min-width: 110px; text-align: center;"><a title="" href="../../inputdata/step4?ezf_id=1463624356048989900&amp;target=MTQ3MTM1NTc2NTE1NzUwMTUwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" data-toggle="tooltip" data-original-title="(Submit/Save Draft) คลิกเพื่อแสดงข้อมูล"><span class="label label-default" style="font-size: 13px;">0 / 0</span> </a><a href="/inputdata/insert-record?insert=url&amp;ezf_id=1470104222001313900&amp;target=MTQ3MTM1NTc2NTAzODIzMzMwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" title="" data-toggle="tooltip" data-original-title="เพิ่มข้อมูล"><i class="glyphicon glyphicon-plus" style="color:#00a65a;font-size: 18px;"></i> </a></td>
<td style="min-width: 110px; text-align: center;"><a title="" href="../../inputdata/step4?ezf_id=1471483982079544300&amp;target=MTQ3MTM1NTc2NTE1NzUwMTUwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" data-toggle="tooltip" data-original-title="(Submit/Save Draft) คลิกเพื่อแสดงข้อมูล"><span class="label label-default" style="font-size: 13px;">0 / 0</span> </a><a href="/inputdata/insert-record?insert=url&amp;ezf_id=1470104222001313900&amp;target=MTQ3MTM1NTc2NTAzODIzMzMwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" title="" data-toggle="tooltip" data-original-title="เพิ่มข้อมูล"><i class="glyphicon glyphicon-plus" style="color:#00a65a;font-size: 18px;"></i> </a></td>
</tr>
<tr data-key="1471355765160437900">
<td style="min-width: 40px; text-align: center;"><input class="selectionOvPersonIds" name="selection[]" type="checkbox" value="1471355765160437900" /></td>
<td style="min-width: 60px; text-align: center;">30</td>
<td style="min-width: 110px; text-align: left;">0032011022282</td>
<td style="min-width: 110px; text-align: left;">นาลอน</td>
<td style="min-width: 110px; text-align: left;">สุดทะวิไล</td>
<td style="min-width: 80px; text-align: center;">&nbsp;<a href="../../ckdnet/emr">EMR</a></td>
<td style="min-width: 110px; text-align: center;">&nbsp;</td>
<td style="min-width: 110px; text-align: center;">&nbsp;</td>
<td style="min-width: 110px; text-align: center;">&nbsp;</td>
<td style="min-width: 110px; text-align: center;">&nbsp;</td>
<td style="min-width: 110px; text-align: center;"><a title="" href="../../inputdata/step4?ezf_id=1463624356048989900&amp;target=MTQ3MTM1NTc2NTE2MDQzNzkwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" data-toggle="tooltip" data-original-title="(Submit/Save Draft) คลิกเพื่อแสดงข้อมูล"><span class="label label-default" style="font-size: 13px;">0 / 0</span> </a><a href="/inputdata/insert-record?insert=url&amp;ezf_id=1470104222001313900&amp;target=MTQ3MTM1NTc2NTAzODIzMzMwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" title="" data-toggle="tooltip" data-original-title="เพิ่มข้อมูล"><i class="glyphicon glyphicon-plus" style="color:#00a65a;font-size: 18px;"></i> </a></td>
<td style="min-width: 110px; text-align: center;"><a title="" href="../../inputdata/step4?ezf_id=1471483982079544300&amp;target=MTQ3MTM1NTc2NTE2MDQzNzkwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" data-toggle="tooltip" data-original-title="(Submit/Save Draft) คลิกเพื่อแสดงข้อมูล"><span class="label label-default" style="font-size: 13px;">0 / 0</span> </a><a href="/inputdata/insert-record?insert=url&amp;ezf_id=1470104222001313900&amp;target=MTQ3MTM1NTc2NTAzODIzMzMwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" title="" data-toggle="tooltip" data-original-title="เพิ่มข้อมูล"><i class="glyphicon glyphicon-plus" style="color:#00a65a;font-size: 18px;"></i> </a></td>
</tr>
<tr data-key="1471355765165316000">
<td style="min-width: 40px; text-align: center;"><input class="selectionOvPersonIds" name="selection[]" type="checkbox" value="1471355765165316000" /></td>
<td style="min-width: 60px; text-align: center;">31</td>
<td style="min-width: 110px; text-align: left;">0032011023351</td>
<td style="min-width: 110px; text-align: left;">ไชเลียง</td>
<td style="min-width: 110px; text-align: left;">แง้ม</td>
<td style="min-width: 80px; text-align: center;"><a href="../../ckdnet/emr">EMR</a>&nbsp;</td>
<td style="min-width: 110px; text-align: center;">&nbsp;</td>
<td style="min-width: 110px; text-align: center;">&nbsp;</td>
<td style="min-width: 110px; text-align: center;">&nbsp;</td>
<td style="min-width: 110px; text-align: center;">&nbsp;</td>
<td style="min-width: 110px; text-align: center;"><a title="" href="../../inputdata/step4?ezf_id=1463624356048989900&amp;target=MTQ3MTM1NTc2NTE2NTMxNjAwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" data-toggle="tooltip" data-original-title="(Submit/Save Draft) คลิกเพื่อแสดงข้อมูล"><span class="label label-default" style="font-size: 13px;">0 / 0</span> </a><a href="/inputdata/insert-record?insert=url&amp;ezf_id=1470104222001313900&amp;target=MTQ3MTM1NTc2NTAzODIzMzMwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" title="" data-toggle="tooltip" data-original-title="เพิ่มข้อมูล"><i class="glyphicon glyphicon-plus" style="color:#00a65a;font-size: 18px;"></i> </a></td>
<td style="min-width: 110px; text-align: center;"><a title="" href="../../inputdata/step4?ezf_id=1471483982079544300&amp;target=MTQ3MTM1NTc2NTE2NTMxNjAwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" data-toggle="tooltip" data-original-title="(Submit/Save Draft) คลิกเพื่อแสดงข้อมูล"><span class="label label-default" style="font-size: 13px;">0 / 0</span> </a><a href="/inputdata/insert-record?insert=url&amp;ezf_id=1470104222001313900&amp;target=MTQ3MTM1NTc2NTAzODIzMzMwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" title="" data-toggle="tooltip" data-original-title="เพิ่มข้อมูล"><i class="glyphicon glyphicon-plus" style="color:#00a65a;font-size: 18px;"></i> </a></td>
</tr>
<tr data-key="1471355765170204400">
<td style="min-width: 40px; text-align: center;"><input class="selectionOvPersonIds" name="selection[]" type="checkbox" value="1471355765170204400" /></td>
<td style="min-width: 60px; text-align: center;">32</td>
<td style="min-width: 110px; text-align: left;">0032011024382</td>
<td style="min-width: 110px; text-align: left;">โน</td>
<td style="min-width: 110px; text-align: left;">ฮุน</td>
<td style="min-width: 80px; text-align: center;"><a href="../../ckdnet/emr">EMR</a>&nbsp;</td>
<td style="min-width: 110px; text-align: center;">&nbsp;</td>
<td style="min-width: 110px; text-align: center;">&nbsp;</td>
<td style="min-width: 110px; text-align: center;">&nbsp;</td>
<td style="min-width: 110px; text-align: center;">&nbsp;</td>
<td style="min-width: 110px; text-align: center;"><a title="" href="../../inputdata/step4?ezf_id=1463624356048989900&amp;target=MTQ3MTM1NTc2NTE3MDIwNDQwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" data-toggle="tooltip" data-original-title="(Submit/Save Draft) คลิกเพื่อแสดงข้อมูล"><span class="label label-default" style="font-size: 13px;">0 / 0</span> </a><a href="/inputdata/insert-record?insert=url&amp;ezf_id=1470104222001313900&amp;target=MTQ3MTM1NTc2NTAzODIzMzMwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" title="" data-toggle="tooltip" data-original-title="เพิ่มข้อมูล"><i class="glyphicon glyphicon-plus" style="color:#00a65a;font-size: 18px;"></i> </a></td>
<td style="min-width: 110px; text-align: center;"><a title="" href="../../inputdata/step4?ezf_id=1471483982079544300&amp;target=MTQ3MTM1NTc2NTE3MDIwNDQwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" data-toggle="tooltip" data-original-title="(Submit/Save Draft) คลิกเพื่อแสดงข้อมูล"><span class="label label-default" style="font-size: 13px;">0 / 0</span> </a><a href="/inputdata/insert-record?insert=url&amp;ezf_id=1470104222001313900&amp;target=MTQ3MTM1NTc2NTAzODIzMzMwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" title="" data-toggle="tooltip" data-original-title="เพิ่มข้อมูล"><i class="glyphicon glyphicon-plus" style="color:#00a65a;font-size: 18px;"></i> </a></td>
</tr>
<tr data-key="1471355765172159900">
<td style="min-width: 40px; text-align: center;"><input class="selectionOvPersonIds" name="selection[]" type="checkbox" value="1471355765172159900" /></td>
<td style="min-width: 60px; text-align: center;">33</td>
<td style="min-width: 110px; text-align: left;">0032011024471</td>
<td style="min-width: 110px; text-align: left;">เทวี</td>
<td style="min-width: 110px; text-align: left;">จวน</td>
<td style="min-width: 80px; text-align: center;"><a href="../../ckdnet/emr">EMR</a>&nbsp;</td>
<td style="min-width: 110px; text-align: center;">&nbsp;</td>
<td style="min-width: 110px; text-align: center;">&nbsp;</td>
<td style="min-width: 110px; text-align: center;">&nbsp;</td>
<td style="min-width: 110px; text-align: center;">&nbsp;</td>
<td style="min-width: 110px; text-align: center;"><a title="" href="../../inputdata/step4?ezf_id=1463624356048989900&amp;target=MTQ3MTM1NTc2NTE3MjE1OTkwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" data-toggle="tooltip" data-original-title="(Submit/Save Draft) คลิกเพื่อแสดงข้อมูล"><span class="label label-default" style="font-size: 13px;">0 / 0</span> </a><a href="/inputdata/insert-record?insert=url&amp;ezf_id=1470104222001313900&amp;target=MTQ3MTM1NTc2NTAzODIzMzMwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" title="" data-toggle="tooltip" data-original-title="เพิ่มข้อมูล"><i class="glyphicon glyphicon-plus" style="color:#00a65a;font-size: 18px;"></i> </a></td>
<td style="min-width: 110px; text-align: center;"><a title="" href="../../inputdata/step4?ezf_id=1471483982079544300&amp;target=MTQ3MTM1NTc2NTE3MjE1OTkwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" data-toggle="tooltip" data-original-title="(Submit/Save Draft) คลิกเพื่อแสดงข้อมูล"><span class="label label-default" style="font-size: 13px;">0 / 0</span> </a><a href="/inputdata/insert-record?insert=url&amp;ezf_id=1470104222001313900&amp;target=MTQ3MTM1NTc2NTAzODIzMzMwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" title="" data-toggle="tooltip" data-original-title="เพิ่มข้อมูล"><i class="glyphicon glyphicon-plus" style="color:#00a65a;font-size: 18px;"></i> </a></td>
</tr>
<tr data-key="1471355765175094300">
<td style="min-width: 40px; text-align: center;"><input class="selectionOvPersonIds" name="selection[]" type="checkbox" value="1471355765175094300" /></td>
<td style="min-width: 60px; text-align: center;">34</td>
<td style="min-width: 110px; text-align: left;">0032011028086</td>
<td style="min-width: 110px; text-align: left;">ซกเนีย</td>
<td style="min-width: 110px; text-align: left;">เนอว</td>
<td style="min-width: 80px; text-align: center;"><a href="../../ckdnet/emr">EMR</a>&nbsp;</td>
<td style="min-width: 110px; text-align: center;">&nbsp;</td>
<td style="min-width: 110px; text-align: center;">&nbsp;</td>
<td style="min-width: 110px; text-align: center;">&nbsp;</td>
<td style="min-width: 110px; text-align: center;">&nbsp;</td>
<td style="min-width: 110px; text-align: center;"><a title="" href="../../inputdata/step4?ezf_id=1463624356048989900&amp;target=MTQ3MTM1NTc2NTE3NTA5NDMwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" data-toggle="tooltip" data-original-title="(Submit/Save Draft) คลิกเพื่อแสดงข้อมูล"><span class="label label-default" style="font-size: 13px;">0 / 0</span> </a><a href="/inputdata/insert-record?insert=url&amp;ezf_id=1470104222001313900&amp;target=MTQ3MTM1NTc2NTAzODIzMzMwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" title="" data-toggle="tooltip" data-original-title="เพิ่มข้อมูล"><i class="glyphicon glyphicon-plus" style="color:#00a65a;font-size: 18px;"></i> </a></td>
<td style="min-width: 110px; text-align: center;"><a title="" href="../../inputdata/step4?ezf_id=1471483982079544300&amp;target=MTQ3MTM1NTc2NTE3NTA5NDMwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" data-toggle="tooltip" data-original-title="(Submit/Save Draft) คลิกเพื่อแสดงข้อมูล"><span class="label label-default" style="font-size: 13px;">0 / 0</span> </a><a href="/inputdata/insert-record?insert=url&amp;ezf_id=1470104222001313900&amp;target=MTQ3MTM1NTc2NTAzODIzMzMwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" title="" data-toggle="tooltip" data-original-title="เพิ่มข้อมูล"><i class="glyphicon glyphicon-plus" style="color:#00a65a;font-size: 18px;"></i> </a></td>
</tr>
<tr data-key="1471355765178026700">
<td style="min-width: 40px; text-align: center;"><input class="selectionOvPersonIds" name="selection[]" type="checkbox" value="1471355765178026700" /></td>
<td style="min-width: 60px; text-align: center;">35</td>
<td style="min-width: 110px; text-align: left;">0032011028736</td>
<td style="min-width: 110px; text-align: left;">เอี้ยง</td>
<td style="min-width: 110px; text-align: left;">อาน</td>
<td style="min-width: 80px; text-align: center;"><a href="../../ckdnet/emr">EMR</a>&nbsp;</td>
<td style="min-width: 110px; text-align: center;">&nbsp;</td>
<td style="min-width: 110px; text-align: center;">&nbsp;</td>
<td style="min-width: 110px; text-align: center;">&nbsp;</td>
<td style="min-width: 110px; text-align: center;">&nbsp;</td>
<td style="min-width: 110px; text-align: center;"><a title="" href="../../inputdata/step4?ezf_id=1463624356048989900&amp;target=MTQ3MTM1NTc2NTE3ODAyNjcwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" data-toggle="tooltip" data-original-title="(Submit/Save Draft) คลิกเพื่อแสดงข้อมูล"><span class="label label-default" style="font-size: 13px;">0 / 0</span> </a><a href="/inputdata/insert-record?insert=url&amp;ezf_id=1470104222001313900&amp;target=MTQ3MTM1NTc2NTAzODIzMzMwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" title="" data-toggle="tooltip" data-original-title="เพิ่มข้อมูล"><i class="glyphicon glyphicon-plus" style="color:#00a65a;font-size: 18px;"></i> </a></td>
<td style="min-width: 110px; text-align: center;"><a title="" href="../../inputdata/step4?ezf_id=1471483982079544300&amp;target=MTQ3MTM1NTc2NTE3ODAyNjcwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" data-toggle="tooltip" data-original-title="(Submit/Save Draft) คลิกเพื่อแสดงข้อมูล"><span class="label label-default" style="font-size: 13px;">0 / 0</span> </a><a href="/inputdata/insert-record?insert=url&amp;ezf_id=1470104222001313900&amp;target=MTQ3MTM1NTc2NTAzODIzMzMwMA%3D%3D&amp;comp_id_target=1461911117076186800&amp;rurl=L2ludi9pbnYtcGVyc29uL2luZGV4P21vZHVsZT00Ng%3D%3D" title="" data-toggle="tooltip" data-original-title="เพิ่มข้อมูล"><i class="glyphicon glyphicon-plus" style="color:#00a65a;font-size: 18px;"></i> </a></td>
</tr>
</tbody>
</table>
<ul class="pagination">
<li class="next"><a href="../inv-person/index?module=46&amp;page=2&amp;per-page=100" data-page="1">&laquo;</a></li>
<li class="active"><a href="../inv-person/index?module=46&amp;page=1&amp;per-page=100" data-page="0">1</a></li>
<li><a href="../inv-person/index?module=46&amp;page=2&amp;per-page=100" data-page="1">2</a></li>
<li><a href="../inv-person/index?module=46&amp;page=3&amp;per-page=100" data-page="2">3</a></li>
<li><a href="../inv-person/index?module=46&amp;page=4&amp;per-page=100" data-page="3">4</a></li>
<li><a href="../inv-person/index?module=46&amp;page=5&amp;per-page=100" data-page="4">5</a></li>
<li><a href="../inv-person/index?module=46&amp;page=6&amp;per-page=100" data-page="5">6</a></li>
<li><a href="../inv-person/index?module=46&amp;page=7&amp;per-page=100" data-page="6">7</a></li>
<li><a href="../inv-person/index?module=46&amp;page=8&amp;per-page=100" data-page="7">8</a></li>
<li><a href="../inv-person/index?module=46&amp;page=9&amp;per-page=100" data-page="8">9</a></li>
<li><a href="../inv-person/index?module=46&amp;page=10&amp;per-page=100" data-page="9">10</a></li>
<li class="next"><a href="../inv-person/index?module=46&amp;page=2&amp;per-page=100" data-page="1">&raquo;</a></li>
</ul>
</div>
</div>

<?php
        Modal::begin([
            'id' => 'modal-move-site2',
            'header' => '<a class="btn btn-danger" href="/ckdnet/workspace">Survival</a>',
            'closeButton' => ['label' => 'ปิด','class' => 'btn btn-success btn-sm pull-right'],
            'options' => ['class' => 'modal-wide'],
        ]);
        
        echo Html::img('../img/ckd_kpm.png',['class'=>'img-rounded img-responsive']);
?>
    
<?php
        Modal::end();
?>