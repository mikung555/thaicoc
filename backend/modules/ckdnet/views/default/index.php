<?php
use kartik\tabs\TabsX;
use yii\helpers\Html;
use yii\bootstrap\Modal;

/* @var $this yii\web\View */
$this->title = Yii::t('backend', 'Module: CKDNET');
$this->params['breadcrumbs'][] = ['label'=>'Modules','url'=>  yii\helpers\Url::to('/tccbots/my-module')];
$this->params['breadcrumbs'][] = Yii::t('backend', 'CKDNET');



?>
<div class="ckdnet-default-index">
<?php
echo TabsX::widget([
    'items'=>$items,
    'position'=>TabsX::POS_ABOVE,
    'encodeLabels'=>false
]);
?>
</div>
<style>
.containerx {
    position: relative;
}
img {
    width: 100%;
}

.p1 {
    left: 56%;
    top: 1%;
    position: absolute;
}
.p2 {
    left: 3%;
    top: 7.5%;
    position: absolute;
}
.p3 {
    left: 36%;
    top: 7.5%;
    position: absolute;
}  
.p4 {
    left: 67%;
    top: 7.5%;
    position: absolute;
}  
.p5 {
    left: 9%;
    top: 40%;
    position: absolute;
}
.p6 {
    left: 35%;
    top: 42%;
    position: absolute;
}
.p7 {
    left: 52%;
    top: 42%;
    position: absolute;
}
.p8 {
    left: 64%;
    top: 42%;
    position: absolute;
}
.p9 {
    left: 20%;
    top: 30%;
    position: absolute;
}
.p10 {
    left: 76%;
    top: 42%;
    position: absolute;
}
.p11 {
    left: 86%;
    top: 42%;
    position: absolute;
}
.p12 {
    left: 64%;
    top: 55%;
    position: absolute;
}
.p13 {
    left: 76%;
    top: 55%;
    position: absolute;
}
.p14 {
    left: 86%;
    top: 55%;
    position: absolute;
}
</style>    
<div class="row">
<div class="col-md-8 col-sm-8 col-xs-8">
<?php
    echo Html::img('../img/ckd_diagram.png',['class'=>'img-rounded img-responsive']);
?>
<a class="p1 primary" data-toggle="modal" data-target="#modal-move-site2">[N = 2342452]</a>
<a class="p2 primary" data-toggle="modal" data-target="#modal-move-site2">[42% N = 983829]</a>
<a class="p3 primary" data-toggle="modal" data-target="#modal-move-site2">[20% N = 468490]</a>
<a class="p4 primary" data-toggle="modal" data-target="#modal-move-site2">[18% N = 421641]</a>
<a class="p5 primary" data-toggle="modal" data-target="#modal-move-site2">98382</a>
<a class="p9 primary" data-toggle="modal" data-target="#modal-move-site2">885446</a>
<a class="p6 primary" data-toggle="modal" data-target="#modal-move-site2">421641</a>
<a class="p7 primary" data-toggle="modal" data-target="#modal-move-site2">46849</a>
<a class="p8 primary" data-toggle="modal" data-target="#modal-move-site2">337312</a>
<a class="p10 primary" data-toggle="modal" data-target="#modal-move-site2">53147</a>
<a class="p11 primary" data-toggle="modal" data-target="#modal-move-site2">31181</a>
<a class="p12 primary" data-toggle="modal" data-target="#modal-move-site2">42517</a>
<a class="p13 primary" data-toggle="modal" data-target="#modal-move-site2">7972</a>
<a class="p14 primary" data-toggle="modal" data-target="#modal-move-site2">2657</a>
<a class="center"></a>
<a class="right" href="#"></a>    
</div>
<div class="col-md-4 col-sm-4 col-xs-4">
    <br><br>
    <?php
        echo Html::img('../img/ckd_graph.png',['class'=>'img-rounded img-responsive',"data-toggle"=>"modal", "data-target"=>"#modal-move-site2"]);
    ?> 
</div>
</div>
<div class="row">
<div class="col-xs-2 col-md-2 center">    
    <a class="btn btn-success" data-toggle="modal" data-target="#modal-move-site2">CKD Screening</a> 
</div> 
<div class="col-xs-2 col-md-2 center">    
    <a class="btn btn-info" data-toggle="modal" data-target="#modal-move-site2">AKI Registry</a> 
</div> 
<div class="col-xs-2 col-md-2 center">    
    <a class="btn btn-danger" data-toggle="modal" data-target="#modal-move-site2">CKD Registry</a>
</div>   
<div class="col-xs-2 col-md-2 center">    
    <a class="btn btn-danger" data-toggle="modal" data-target="#modal-move-site2">Screening targets</a>
</div>       
</div>    
<?php
        Modal::begin([
            'id' => 'modal-move-site2',
            'header' => '<a class="btn btn-danger" href="/ckdnet/workspace">กำกับงาน CKD Registry</a>',
            'closeButton' => ['label' => 'ปิด','class' => 'btn btn-success btn-sm pull-right'],
            'options' => ['class' => 'modal-wide'],
        ]);
?>
<div class="form-inline pull-right">    
    <div class="input-group margin-bottom-sm">
      <span class="input-group-addon"><i class="fa fa-user fa-fw"></i></span>
      <input class="form-control" type="text" name="cid" placeholder="บัตร ชื่อ นามสกุล">
    </div>     
    <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
</div><br>
<div class="summary">Showing <strong>1-100</strong> of <strong>15,421</strong> items.</div>
<ul class="pagination">
<li class="next"><a href="../inv-person/index?module=46&amp;page=2&amp;per-page=100" data-page="1">&laquo;</a></li>
<li class="active"><a href="../inv-person/index?module=46&amp;page=1&amp;per-page=100" data-page="0">1</a></li>
<li><a href="../inv-person/index?module=46&amp;page=2&amp;per-page=100" data-page="1">2</a></li>
<li><a href="../inv-person/index?module=46&amp;page=3&amp;per-page=100" data-page="2">3</a></li>
<li><a href="../inv-person/index?module=46&amp;page=4&amp;per-page=100" data-page="3">4</a></li>
<li><a href="../inv-person/index?module=46&amp;page=5&amp;per-page=100" data-page="4">5</a></li>
<li><a href="../inv-person/index?module=46&amp;page=6&amp;per-page=100" data-page="5">6</a></li>
<li><a href="../inv-person/index?module=46&amp;page=7&amp;per-page=100" data-page="6">7</a></li>
<li><a href="../inv-person/index?module=46&amp;page=8&amp;per-page=100" data-page="7">8</a></li>
<li><a href="../inv-person/index?module=46&amp;page=9&amp;per-page=100" data-page="8">9</a></li>
<li><a href="../inv-person/index?module=46&amp;page=10&amp;per-page=100" data-page="9">10</a></li>
<li class="next"><a href="../inv-person/index?module=46&amp;page=2&amp;per-page=100" data-page="1">&raquo;</a></li>
</ul>
<table class="table table-striped table-bordered table-hover">
<thead>
<tr>
<th style="text-align: center;">#</th>
<th style="text-align: left;"><a href="../inv-person/index?module=46&amp;sort=name" data-sort="name">ชื่อ</a></th>
<th style="text-align: left;"><a href="../inv-person/index?module=46&amp;sort=surname" data-sort="surname">นามสกุล</a></th>
<th style="text-align: center;">EMR</th>
<th style="text-align: center;">Last eGFR</th>
</tr>
<tr id="inv-person-grid-filters" class="filters">
<td>&nbsp;</td>
<td><input class="form-control" name="TbdataSearch[name]" type="text" /></td>
<td><input class="form-control" name="TbdataSearch[surname]" type="text" /></td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
</thead>
<tbody>
<tr data-key="1471355765038233300">
<td style="min-width: 60px; text-align: center;">1</td>
<td style="min-width: 110px; text-align: left;" class="canencode">วิทสนา</td>
<td style="min-width: 110px; text-align: left;" class="canencode">มับ</td>
<td style="min-width: 80px; text-align: center;"><a href="../../ckdnet/emr">EMR</a></td>
<td style="min-width: 110px; text-align: center;">60.5&nbsp;</td>
</tr>
<tr data-key="1471355765041164300">
<td style="min-width: 60px; text-align: center;">2</td>
<td style="min-width: 110px; text-align: left;" class="canencode">คอตี</td>
<td style="min-width: 110px; text-align: left;" class="canencode">โฉม</td>
<td style="min-width: 80px; text-align: center;"><a href="../../ckdnet/emr">EMR</a>&nbsp;</td>
<td style="min-width: 110px; text-align: center;">70.2&nbsp;</td>
</tr>
<tr data-key="1471355765044099700">
<td style="min-width: 60px; text-align: center;">3</td>
<td style="min-width: 110px; text-align: left;" class="canencode">น้อย</td>
<td style="min-width: 110px; text-align: left;" class="canencode">สานพิลา</td>
<td style="min-width: 80px; text-align: center;"><a href="../../ckdnet/emr">EMR</a>&nbsp;</td>
<td style="min-width: 110px; text-align: center;">53.2&nbsp;</td>
</tr>
<tr data-key="1471355765050939600">
<td style="min-width: 60px; text-align: center;">4</td>
<td style="min-width: 110px; text-align: left;" class="canencode">กันยา</td>
<td style="min-width: 110px; text-align: left;" class="canencode">ลาย</td>
<td style="min-width: 80px; text-align: center;"><a href="../../ckdnet/emr">EMR</a>&nbsp;</td>
<td style="min-width: 110px; text-align: center;">&nbsp;64.1</td>
</tr>
<tr data-key="1471355765054851400">
<td style="min-width: 60px; text-align: center;">5</td>
<td style="min-width: 110px; text-align: left;" class="canencode">เอเอออง</td>
<td style="min-width: 110px; text-align: left;" class="canencode">ไม่มีนามสกุล</td>
<td style="min-width: 80px; text-align: center;"><a href="../../ckdnet/emr">EMR</a>&nbsp;</td>
<td style="min-width: 110px; text-align: center;">70&nbsp;</td>
</tr>
<tr data-key="1471355765058762800">
<td style="min-width: 60px; text-align: center;">6</td>
<td style="min-width: 110px; text-align: left;" class="canencode">เอือ</td>
<td style="min-width: 110px; text-align: left;" class="canencode">เบียร์</td>
<td style="min-width: 80px; text-align: center;"><a href="../../ckdnet/emr">EMR</a>&nbsp;</td>
<td style="min-width: 110px; text-align: center;">80.3&nbsp;</td>
</tr>
<tr data-key="1471355765062671700">
<td style="min-width: 60px; text-align: center;">7</td>
<td style="min-width: 110px; text-align: left;" class="canencode">เชรี๊ยท</td>
<td style="min-width: 110px; text-align: left;" class="canencode">จอน</td>
<td style="min-width: 80px; text-align: center;"><a href="../../ckdnet/emr">EMR</a>&nbsp;</td>
<td style="min-width: 110px; text-align: center;">90&nbsp;</td>
</tr>
<tr data-key="1471355765066582100">
<td style="min-width: 60px; text-align: center;">8</td>
<td style="min-width: 110px; text-align: left;" class="canencode">เอ๋</td>
<td style="min-width: 110px; text-align: left;" class="canencode">ผงาตุนัด</td>
<td style="min-width: 80px; text-align: center;"><a href="../../ckdnet/emr">EMR</a>&nbsp;</td>
<td style="min-width: 110px; text-align: center;">60.5&nbsp;&nbsp;</td>
</tr>
<tr data-key="1471355765071474900">
<td style="min-width: 60px; text-align: center;">9</td>
<td style="min-width: 110px; text-align: left;" class="canencode">แต๋ว</td>
<td style="min-width: 110px; text-align: left;" class="canencode">แวน</td>
<td style="min-width: 80px; text-align: center;"><a href="../../ckdnet/emr">EMR</a></td>
<td style="min-width: 110px; text-align: center;">&nbsp;60.5&nbsp;</td>
</tr>
<tr data-key="1471355765075383800">
<td style="min-width: 60px; text-align: center;">10</td>
<td style="min-width: 110px; text-align: left;" class="canencode">ดา</td>
<td style="min-width: 110px; text-align: left;" class="canencode">ไม่มีนามสกุล</td>
<td style="min-width: 80px; text-align: center;">&nbsp;<a href="../../ckdnet/emr">EMR</a></td>
<td style="min-width: 110px; text-align: center;">90&nbsp;</td>
</tr>
<tr data-key="1471355765091021100">
<td style="min-width: 60px; text-align: center;">11</td>
<td style="min-width: 110px; text-align: left;" class="canencode">ตู</td>
<td style="min-width: 110px; text-align: left;" class="canencode">ยอง</td>
<td style="min-width: 80px; text-align: center;">&nbsp;<a href="../../ckdnet/emr">EMR</a></td>
<td style="min-width: 110px; text-align: center;">100&nbsp;</td>
</tr>
<tr data-key="1471355765099818900">
<td style="min-width: 60px; text-align: center;">12</td>
<td style="min-width: 110px; text-align: left;" class="canencode">โซเฟีย</td>
<td style="min-width: 110px; text-align: left;" class="canencode">ตี</td>
<td style="min-width: 80px; text-align: center;">&nbsp;<a href="../../ckdnet/emr">EMR</a></td>
<td style="min-width: 110px; text-align: center;">100&nbsp;</td>
</tr>
<tr data-key="1471355765103728300">
<td style="min-width: 60px; text-align: center;">13</td>
<td style="min-width: 110px; text-align: left;" class="canencode">สเรือน</td>
<td style="min-width: 110px; text-align: left;" class="canencode">กัน</td>
<td style="min-width: 80px; text-align: center;">&nbsp;<a href="../../ckdnet/emr">EMR</a></td>
<td style="min-width: 110px; text-align: center;">100&nbsp;</td>
</tr>
<tr data-key="1471355765109599400">
<td style="min-width: 60px; text-align: center;">14</td>
<td style="min-width: 110px; text-align: left;" class="canencode">ไล</td>
<td style="min-width: 110px; text-align: left;" class="canencode">ซู</td>
<td style="min-width: 80px; text-align: center;"><a href="../../ckdnet/emr">EMR</a>&nbsp;</td>
<td style="min-width: 110px; text-align: center;">100&nbsp;</td>
</tr>
<tr data-key="1471355765114490700">
<td style="min-width: 60px; text-align: center;">15</td>
<td style="min-width: 110px; text-align: left;" class="canencode">ตังงิม</td>
<td style="min-width: 110px; text-align: left;" class="canencode">ยืน</td>
<td style="min-width: 80px; text-align: center;"><a href="../../ckdnet/emr">EMR</a>&nbsp;</td>
<td style="min-width: 110px; text-align: center;">100&nbsp;</td>
</tr>
<tr data-key="1471355765118391800">
<td style="min-width: 60px; text-align: center;">16</td>
<td style="min-width: 110px; text-align: left;" class="canencode">เจิน</td>
<td style="min-width: 110px; text-align: left;" class="canencode">ศรี</td>
<td style="min-width: 80px; text-align: center;">&nbsp;<a href="../../ckdnet/emr">EMR</a></td>
<td style="min-width: 110px; text-align: center;">100&nbsp;</td>
</tr>
<tr data-key="1471355765120347300">
<td style="min-width: 60px; text-align: center;">17</td>
<td style="min-width: 110px; text-align: left;" class="canencode">แดง</td>
<td style="min-width: 110px; text-align: left;" class="canencode">ไชยยะแสง</td>
<td style="min-width: 80px; text-align: center;"><a href="../../ckdnet/emr">EMR</a>&nbsp;</td>
<td style="min-width: 110px; text-align: center;">100&nbsp;</td>
</tr>
<tr data-key="1471355765124259700">
<td style="min-width: 60px; text-align: center;">18</td>
<td style="min-width: 110px; text-align: left;" class="canencode">คาโคตี</td>
<td style="min-width: 110px; text-align: left;" class="canencode">สรอ</td>
<td style="min-width: 80px; text-align: center;"><a href="../../ckdnet/emr">EMR</a>&nbsp;</td>
<td style="min-width: 110px; text-align: center;">100&nbsp;</td>
</tr>
<tr data-key="1471355765127190700">
<td style="min-width: 60px; text-align: center;">19</td>
<td style="min-width: 110px; text-align: left;" class="canencode">สินา</td>
<td style="min-width: 110px; text-align: left;" class="canencode">ดวง</td>
<td style="min-width: 80px; text-align: center;"><a href="../../ckdnet/emr">EMR</a>&nbsp;</td>
<td style="min-width: 110px; text-align: center;">100&nbsp;</td>
</tr>
<tr data-key="1471355765129146200">
<td style="min-width: 60px; text-align: center;">20</td>
<td style="min-width: 110px; text-align: left;" class="canencode">เหงี๊ยน</td>
<td style="min-width: 110px; text-align: left;" class="canencode">โกนงา</td>
<td style="min-width: 80px; text-align: center;"><a href="../../ckdnet/emr">EMR</a>&nbsp;</td>
<td style="min-width: 110px; text-align: center;">100&nbsp;</td>
</tr>
<tr data-key="1471355765132083500">
<td style="min-width: 60px; text-align: center;">21</td>
<td style="min-width: 110px; text-align: left;" class="canencode">จนา</td>
<td style="min-width: 110px; text-align: left;" class="canencode">แซด</td>
<td style="min-width: 80px; text-align: center;"><a href="../../ckdnet/emr">EMR</a>&nbsp;</td>
<td style="min-width: 110px; text-align: center;">100&nbsp;</td>
</tr>
<tr data-key="1471355765135012100">
<td style="min-width: 60px; text-align: center;">22</td>
<td style="min-width: 110px; text-align: left;" class="canencode">แสงจันทร์</td>
<td style="min-width: 110px; text-align: left;" class="canencode">ไม่มีนามสกุล</td>
<td style="min-width: 80px; text-align: center;"><a href="../../ckdnet/emr">EMR</a>&nbsp;</td>
<td style="min-width: 110px; text-align: center;">100&nbsp;</td>
</tr>
<tr data-key="1471355765137943500">
<td style="min-width: 60px; text-align: center;">23</td>
<td style="min-width: 110px; text-align: left;" class="canencode">ซารอส</td>
<td style="min-width: 110px; text-align: left;" class="canencode">โอ๊ต</td>
<td style="min-width: 80px; text-align: center;"><a href="../../ckdnet/emr">EMR</a>&nbsp;</td>
<td style="min-width: 110px; text-align: center;">100&nbsp;</td>
</tr>
<tr data-key="1471355765142831500">
<td style="min-width: 60px; text-align: center;">24</td>
<td style="min-width: 110px; text-align: left;" class="canencode">ดวงสนิท</td>
<td style="min-width: 110px; text-align: left;" class="canencode">จันทร์</td>
<td style="min-width: 80px; text-align: center;">&nbsp;<a href="../../ckdnet/emr">EMR</a></td>
<td style="min-width: 110px; text-align: center;">100&nbsp;</td>
</tr>
<tr data-key="1471355765145764900">
<td style="min-width: 60px; text-align: center;">25</td>
<td style="min-width: 110px; text-align: left;" class="canencode">สินา</td>
<td style="min-width: 110px; text-align: left;" class="canencode">เลง</td>
<td style="min-width: 80px; text-align: center;">&nbsp;<a href="../../ckdnet/emr">EMR</a></td>
<td style="min-width: 110px; text-align: center;">100&nbsp;</td>
</tr>
<tr data-key="1471355765148701300">
<td style="min-width: 60px; text-align: center;">26</td>
<td style="min-width: 110px; text-align: left;" class="canencode">วุฒิ</td>
<td style="min-width: 110px; text-align: left;" class="canencode">จันทร์</td>
<td style="min-width: 80px; text-align: center;">&nbsp;<a href="../../ckdnet/emr">EMR</a></td>
<td style="min-width: 110px; text-align: center;">100&nbsp;</td>
</tr>
<tr data-key="1471355765150657200">
<td style="min-width: 60px; text-align: center;">27</td>
<td style="min-width: 110px; text-align: left;" class="canencode">เลือม</td>
<td style="min-width: 110px; text-align: left;" class="canencode">โลย</td>
<td style="min-width: 80px; text-align: center;">&nbsp;<a href="../../ckdnet/emr">EMR</a></td>
<td style="min-width: 110px; text-align: center;">100&nbsp;</td>
</tr>
<tr data-key="1471355765153586300">
<td style="min-width: 60px; text-align: center;">28</td>
<td style="min-width: 110px; text-align: left;" class="canencode">ลีน่า</td>
<td style="min-width: 110px; text-align: left;" class="canencode">ยอด</td>
<td style="min-width: 80px; text-align: center;">&nbsp;<a href="../../ckdnet/emr">EMR</a></td>
<td style="min-width: 110px; text-align: center;">100&nbsp;</td>
</tr>
<tr data-key="1471355765157501500">
<td style="min-width: 60px; text-align: center;">29</td>
<td style="min-width: 110px; text-align: left;" class="canencode">สะวิช</td>
<td style="min-width: 110px; text-align: left;" class="canencode">โซ</td>
<td style="min-width: 80px; text-align: center;"><a href="../../ckdnet/emr">EMR</a>&nbsp;</td>
<td style="min-width: 110px; text-align: center;">100&nbsp;</td>
</tr>
<tr data-key="1471355765160437900">
<td style="min-width: 60px; text-align: center;">30</td>
<td style="min-width: 110px; text-align: left;" class="canencode">นาลอน</td>
<td style="min-width: 110px; text-align: left;" class="canencode">สุดทะวิไล</td>
<td style="min-width: 80px; text-align: center;">&nbsp;<a href="../../ckdnet/emr">EMR</a></td>
<td style="min-width: 110px; text-align: center;">100&nbsp;</td>
</tr>
<tr data-key="1471355765165316000">
<td style="min-width: 60px; text-align: center;">31</td>
<td style="min-width: 110px; text-align: left;" class="canencode">ไชเลียง</td>
<td style="min-width: 110px; text-align: left;" class="canencode">แง้ม</td>
<td style="min-width: 80px; text-align: center;"><a href="../../ckdnet/emr">EMR</a>&nbsp;</td>
<td style="min-width: 110px; text-align: center;">100&nbsp;</td>
</tr>
<tr data-key="1471355765170204400">
<td style="min-width: 60px; text-align: center;">32</td>
<td style="min-width: 110px; text-align: left;" class="canencode">โน</td>
<td style="min-width: 110px; text-align: left;" class="canencode">ฮุน</td>
<td style="min-width: 80px; text-align: center;"><a href="../../ckdnet/emr">EMR</a>&nbsp;</td>
<td style="min-width: 110px; text-align: center;">100&nbsp;</td>
</tr>
<tr data-key="1471355765172159900">
<td style="min-width: 60px; text-align: center;">33</td>
<td style="min-width: 110px; text-align: left;" class="canencode">เทวี</td>
<td style="min-width: 110px; text-align: left;" class="canencode">จวน</td>
<td style="min-width: 80px; text-align: center;"><a href="../../ckdnet/emr">EMR</a>&nbsp;</td>
<td style="min-width: 110px; text-align: center;">100&nbsp;</td>
</tr>
<tr data-key="1471355765175094300">
<td style="min-width: 60px; text-align: center;">34</td>
<td style="min-width: 110px; text-align: left;" class="canencode">ซกเนีย</td>
<td style="min-width: 110px; text-align: left;" class="canencode">เนอว</td>
<td style="min-width: 80px; text-align: center;"><a href="../../ckdnet/emr">EMR</a>&nbsp;</td>
<td style="min-width: 110px; text-align: center;">100&nbsp;</td>
</tr>
<tr data-key="1471355765178026700">
<td style="min-width: 60px; text-align: center;">35</td>
<td style="min-width: 110px; text-align: left;" class="canencode">เอี้ยง</td>
<td style="min-width: 110px; text-align: left;" class="canencode">อาน</td>
<td style="min-width: 80px; text-align: center;"><a href="../../ckdnet/emr">EMR</a>&nbsp;</td>
<td style="min-width: 110px; text-align: center;">100&nbsp;</td>
</tr>
</tbody>
</table>
<ul class="pagination">
<li class="next"><a href="../inv-person/index?module=46&amp;page=2&amp;per-page=100" data-page="1">&laquo;</a></li>
<li class="active"><a href="../inv-person/index?module=46&amp;page=1&amp;per-page=100" data-page="0">1</a></li>
<li><a href="../inv-person/index?module=46&amp;page=2&amp;per-page=100" data-page="1">2</a></li>
<li><a href="../inv-person/index?module=46&amp;page=3&amp;per-page=100" data-page="2">3</a></li>
<li><a href="../inv-person/index?module=46&amp;page=4&amp;per-page=100" data-page="3">4</a></li>
<li><a href="../inv-person/index?module=46&amp;page=5&amp;per-page=100" data-page="4">5</a></li>
<li><a href="../inv-person/index?module=46&amp;page=6&amp;per-page=100" data-page="5">6</a></li>
<li><a href="../inv-person/index?module=46&amp;page=7&amp;per-page=100" data-page="6">7</a></li>
<li><a href="../inv-person/index?module=46&amp;page=8&amp;per-page=100" data-page="7">8</a></li>
<li><a href="../inv-person/index?module=46&amp;page=9&amp;per-page=100" data-page="8">9</a></li>
<li><a href="../inv-person/index?module=46&amp;page=10&amp;per-page=100" data-page="9">10</a></li>
<li class="next"><a href="../inv-person/index?module=46&amp;page=2&amp;per-page=100" data-page="1">&raquo;</a></li>
</ul>
<?php
        Modal::end();        
?>
