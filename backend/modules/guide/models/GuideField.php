<?php

namespace backend\modules\guide\models;

use Yii;

/**
 * This is the model class for table "guide_field".
 *
 * @property integer $auto_id
 * @property integer $uid
 * @property string $text_input
 * @property string $textarea
 * @property string $email
 * @property double $number
 * @property integer $int_input
 * @property integer $checkbox
 * @property string $checkbox_list
 * @property integer $radio
 * @property string $multiple
 * @property integer $dropdown
 * @property string $readonly
 * @property string $disabled
 * @property string $textmask_input
 * @property string $optional_icons
 * @property integer $select2
 * @property integer $dropdown_db
 * @property string $file
 * @property string $date
 * @property string $time
 * @property string $datetime
 * @property integer $create_by
 * @property string $create_time
 * @property integer $update_by
 * @property string $update_time
 */
class GuideField extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'guide_field';
    }

    /**
     * @inheritdoc
     * http://www.bsourcecode.com/yiiframework2/validation-rules-for-model-attributes-in-yiiframework-2-0/
     */
    public function rules()
    {
        return [
            [['uid', 'text_input'], 'required'],
            [['int_input', 'checkbox', 'radio', 'dropdown', 'select2', 'dropdown_db', 'create_by', 'update_by'], 'integer'],
            [['textarea',  'file'], 'string'],
            [['number'], 'number'],
	    [['uid'], 'unique'],
	    [['email'], 'email'],
            [['checkbox_list', 'multiple', 'date', 'time', 'datetime', 'create_time', 'update_time'], 'safe'],
            [['uid', 'text_input'], 'string', 'max' => 100],
            [['readonly', 'disabled', 'textmask_input', 'optional_icons'], 'string', 'max' => 50],
            [['uid'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'auto_id' => 'Auto ID',
            'uid' => 'Uid',
            'text_input' => 'Text Input',
            'textarea' => 'Textarea',
            'email' => 'Email',
            'number' => 'Number',
            'int_input' => 'Int Input',
            'checkbox' => 'Checkbox',
            'checkbox_list' => 'Checkbox List',
            'radio' => 'Radio',
            'multiple' => 'Multiple',
            'dropdown' => 'Dropdown',
            'readonly' => 'Readonly',
            'disabled' => 'Disabled',
            'textmask_input' => 'Textmask Input',
            'optional_icons' => 'Optional Icons',
            'select2' => 'Select2',
            'dropdown_db' => 'Dropdown Db',
            'file' => 'File',
            'date' => 'Date',
            'time' => 'Time',
            'datetime' => 'Datetime',
            'create_by' => 'Create By',
            'create_time' => 'Create Time',
            'update_by' => 'Update By',
            'update_time' => 'Update Time',
        ];
    }
}
