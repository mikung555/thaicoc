<?php

namespace backend\modules\guide\models;

/**
 * This is the ActiveQuery class for [[GuideField]].
 *
 * @see GuideField
 */
class GuideFieldQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return GuideField[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return GuideField|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}