<?php

namespace backend\modules\guide\models;

use Yii;

/**
 * This is the model class for table "guide_list".
 *
 * @property integer $id
 * @property string $name
 * @property string $type
 * @property integer $order
 */
class GuideList extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'guide_list';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'type', 'order'], 'required'],
            [['order'], 'integer'],
            [['name'], 'string', 'max' => 100],
            [['type'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'type' => 'Type',
            'order' => 'Order',
        ];
    }
}
