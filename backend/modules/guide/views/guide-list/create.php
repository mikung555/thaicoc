<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\guide\models\GuideList */

$this->title = 'Create Guide List';
$this->params['breadcrumbs'][] = ['label' => 'Guide Lists', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="guide-list-create">

    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
