<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\guide\models\GuideField */

$this->title = 'Update Guide Field: ' . ' ' . $model->auto_id;
$this->params['breadcrumbs'][] = ['label' => 'Guide Fields', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->auto_id, 'url' => ['view', 'id' => $model->auto_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="guide-field-update">

    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
