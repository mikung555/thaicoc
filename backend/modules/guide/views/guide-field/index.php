<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\guide\models\GuideFieldSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Guide Fields';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="guide-field-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    

    
    
    <p>
        <?php echo Html::a('Create Guide Field', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'auto_id',
            'uid',
            'text_input',
            'textarea:ntext',
            'email:email',
            // 'number',
            // 'int_input',
            // 'checkbox',
            // 'checkbox_list:ntext',
            // 'radio',
            // 'multiple:ntext',
            // 'dropdown',
            // 'readonly',
            // 'disabled',
            // 'textmask_input',
            // 'optional_icons',
            // 'select2',
            // 'dropdown_db',
            // 'file:ntext',
            // 'date',
            // 'time',
            // 'datetime',
            // 'create_by',
            // 'create_time',
            // 'update_by',
            // 'update_time',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
