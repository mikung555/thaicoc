<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;
use backend\modules\guide\models\GuideList;
/* @var $this yii\web\View */
/* @var $model backend\modules\guide\models\GuideField */
/* @var $form yii\bootstrap\ActiveForm */
?>

<div class="guide-field-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php echo $form->errorSummary($model); ?>

    <?php echo $form->field($model, 'uid')->textInput(['readonly'=>true]) ?>

    <?php echo $form->field($model, 'text_input')->textInput(['maxlength' => true, 'placeholder'=>'Text Input (placeholder)']) ?>

    <?php echo $form->field($model, 'textarea')->textarea(['rows' => 3]) ?>

    <?php echo $form->field($model, 'email')->textInput(['maxlength' => true, 'placeholder'=>'ex@email.com']) ?>

    <?php echo $form->field($model, 'number')->textInput(['type'=>'number', 'step'=>0.1]) ?>

    <?php echo $form->field($model, 'int_input')->textInput(['type'=>'number', 'min'=>0, 'max'=>10]) ?>

    <?php echo $form->field($model, 'checkbox')->checkbox() ?>

    <?php echo $form->field($model, 'checkbox_list')->checkboxList(ArrayHelper::map(GuideList::findAll(['type'=>'games_console']), 'id', 'name')) ?>

    <div class="form-group field-guidefield-checkbox_list">
	<?= Html::activeRadioList($model, 'radio', ['iPhone3', 'iPhones', 'iPhone4', 'iPhone4s', 'iPhone5', 'iPhone5s', 'iPhone6'], ['itemOptions'=>['labelOptions'=>['class'=>'radio-inline']]]) ?>
	<?= Html::error($model, 'radio') ?>
    </div>
    
    <?php echo $form->field($model, 'multiple')->dropDownList(['ac1'=>'ตัวอย่าง1', 'ac2'=>'ตัวอย่าง2', 'ac3'=>'ตัวอย่าง3', 'ac4'=>'ตัวอย่าง4'],['multiple' => true]) ?>

    <?php echo $form->field($model, 'dropdown')->dropDownList(['1'=>'ตัวอย่าง1', '2'=>'ตัวอย่าง2', '3'=>'ตัวอย่าง3', '4'=>'ตัวอย่าง4']) ?>

    <?php echo $form->field($model, 'readonly')->textInput(['maxlength' => true, 'readonly'=>true]) ?>

    <?php echo $form->field($model, 'disabled')->textInput(['maxlength' => true, 'disabled'=>true]) ?>

    <?php echo $form->field($model, 'textmask_input')->widget(\yii\widgets\MaskedInput::className(), [
    'mask' => '999-999-9999']) ?>

    <div class="form-group field-guidefield-optional_icons">
	<label class="control-label" for="guidefield-optional_icons">Optional Icons</label>
	<div class="input-group">
	    <div class="input-group-addon">$</div>
	    <?= Html::activeTextInput($model, 'optional_icons', ['maxlength' => true, 'class'=>'form-control']) ?>
	    <div class="input-group-addon">.00</div>
	</div>
	<?= Html::error($model, 'radio') ?>
    </div>

    <?php echo $form->field($model, 'select2')->textInput() ?>

    <?php echo $form->field($model, 'dropdown_db')->dropDownList(ArrayHelper::map(GuideList::findAll(['type'=>'mail']), 'id', 'name'), ['prompt'=>'กรุณาเลือกเมลที่ต้องการ']) ?>

    <?php echo $form->field($model, 'file')->fileInput() ?>

    <?= $form->field($model, 'date')->widget(\yii\jui\DatePicker::classname(), [
	'language' => 'th',
	'dateFormat' => 'yyyy-MM-dd',
	'options'=>['class'=>'form-control']
    ]) ?>
    
    <?= $form->field($model, 'time')->widget(trntv\yii\datetimepicker\DatetimepickerWidget::className(), [
	'phpDatetimeFormat' => 'HH:mm',
	'momentDatetimeFormat' =>'HH:mm',
	'clientOptions' => [
	    'locale'=>'th',
	    'sideBySide' => true,
	]
    ]) ?>
    <?php // $form->field($model, 'datetime')->widget(trntv\yii\datetimepicker\DatetimepickerWidget::className(), [
//	'phpDatetimeFormat' => 'dd/MM/yyyy HH:mm',
//	'clientOptions' => [
//	    'locale'=>'th',
//	    'sideBySide' => true,
//	]
//    ]) ?>
    <?= Html::activeHiddenInput($model, 'datetime') ?>
    <?= Html::activeHiddenInput($model, 'create_by') ?>
    <?= Html::activeHiddenInput($model, 'create_time') ?>
    <?= Html::activeHiddenInput($model, 'update_by') ?>
    <?= Html::activeHiddenInput($model, 'update_time') ?>

    <div class="form-group">
        <?php echo Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
