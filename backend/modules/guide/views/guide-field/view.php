<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\guide\models\GuideField */

$this->title = $model->auto_id;
$this->params['breadcrumbs'][] = ['label' => 'Guide Fields', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="guide-field-view">

    <p>
        <?php echo Html::a('Update', ['update', 'id' => $model->auto_id], ['class' => 'btn btn-primary']) ?>
        <?php echo Html::a('Delete', ['delete', 'id' => $model->auto_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?php echo DetailView::widget([
        'model' => $model,
        'attributes' => [
            'auto_id',
            'uid',
            'text_input',
            'textarea:ntext',
            'email:email',
            'number',
            'int_input',
            'checkbox',
            'checkbox_list:ntext',
            'radio',
            'multiple:ntext',
            'dropdown',
            'readonly',
            'disabled',
            'textmask_input',
            'optional_icons',
            'select2',
            'dropdown_db',
            'file:ntext',
            'date',
            'time',
            'datetime',
            'create_by',
            'create_time',
            'update_by',
            'update_time',
        ],
    ]) ?>

</div>
