<?php
use yii\helpers\Html;
/* @var $this \yii\web\View */
/* @var $content string */

\frontend\assets\FrontendAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?php echo Yii::$app->language ?>">
<head>
    <meta charset="<?php echo Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <?php echo Html::csrfMetaTags() ?>
    <?php 
    $ga = Yii::$app->keyStorage->get('google-analytics');
    if(isset($ga) && $ga!='0'):?>
    <script>
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	ga('create', '<?=$ga?>', 'auto');
	ga('send', 'pageview');

      </script>
      <?php endif;?>
      <?php
      $domain=Yii::$app->keyStorage->get('frontend.domain');
    $siteprefix= trim(str_replace($domain, "", $_SERVER['HTTP_HOST']));
    $siteprefix= trim(str_replace(".", "",$siteprefix));
    $sitecode=$siteprefix+0;
    if ($sitecode==0) $sitecode="";
    $sqlSiteURL = "SELECT code,`url`,`name`, status, favicon FROM site_url WHERE status in (0,3) AND (code='".($sitecode)."' or url='".$siteprefix."')";
    $dataSiteURL = Yii::$app->db->createCommand($sqlSiteURL)->queryOne();
    
    if($dataSiteURL['favicon']!=''){
      ?>
      <link href="<?=Yii::getAlias('@storageUrl/form-upload/').$dataSiteURL['favicon'];?>" rel="icon">
      <?php
    }
      ?>
</head>
<body>
    <img src="<?=  \yii\helpers\Url::to(['@web/img/black_ribbon_top_left.png'])?>" class="black-ribbon stick-top stick-left">
<?php $this->beginBody() ?>
    <?php echo $content ?>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
