<?php

use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use backend\models\SiteUrl;
use yii\helpers\Url;

/* @var $this \yii\web\View */
/* @var $content string */
$this->registerCss('
@media (min-width: 768px){
    .navbar-header  {
        width: 350px;
    }
}
@media (min-width: 992px){
    .navbar-header  {
        width: 350px;
    }
}
@media (max-width: 1010px){
    .navbar-header {
        float: none;
        width: 100%;
    }
    .navbar-toggle {
        display: block;
    }
    .navbar-collapse {
        border-top: 1px solid transparent;
        box-shadow: inset 0 1px 0 rgba(255,255,255,0.1);
    }
    .navbar-collapse.collapse {
        display: none!important;
    }
    /* since 3.1.0 */
    .navbar-collapse.collapse.in { 
        display: block!important;
    }
    .collapsing {
        overflow: hidden!important;
    }
}
');
$this->beginContent('@frontend/views/layouts/_clear_1.php');

?>

<?php
    
    /*
    $sqlSiteURL = "SELECT code,`url`,`name` FROM site_url WHERE code='".$model->sitecode."' ";
    $dataSiteURL = Yii::$app->db->createCommand($sqlSiteURL)->queryOne();
    $sqlHospital = "SELECT `code`,`name` FROM const_hospital WHERE code='".$model->sitecode."' ";
    $dataHospital = Yii::$app->db->createCommand($sqlHospital)->queryOne();

    if ($dataSiteURL['code']=="") {
        $SiteURL = new SiteUrl;
        $SiteURL->code=$model->sitecode;
        $SiteURL->name=$dataHospital['name'];
        $SiteURL->url=$model->sitecode;
        $SiteURL->save();
    }
      */

    $sql = "SELECT * FROM `site_config` ";
    $data = Yii::$app->db->createCommand($sql)->queryOne();
    NavBar::begin([
        'brandLabel' => '<li class="fa fa-home"></li>  <b>'.($data["sitename"]).'</b>'.
            (in_array(Yii::$app->keyStorage->get('frontend.domain'), ['thaicarecloud.org', 'cascap.in.th', 'yii2-starter-kit.dev']) ?
            '&nbsp; <a href="http://www.kku.ac.th" target="_blank" title="มหาวิทยาลัยขอนแก่น"><img style="margin-top: 5px;" src="'.Yii::getAlias('@frontendUrl').'/img/kku_logo.png" width="25"></a>&nbsp;<a href="http://www.moph.go.th" target="_blank" title="กระทรวงสาธารณสุข"><img style="margin-top: 5px;" src="'.Yii::getAlias('@frontendUrl').'/img/moph_logo.png" width="33"></a>' : null),
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
        'innerContainerOptions' => ['class'=>'container-fluid'],
    ]); ?>
    <?php echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
            ['label' => Yii::t('frontend', 'หน้าหลัก'), 'url' => ['/site/index']],
            
	    
//            ['label' => Yii::t('frontend', 'บทความ'), 'url' => ['/article/index']],
	    ['label' => Yii::t('frontend', 'งานวิจัย'), 'url' => ['//article/research/index']],
	    
	    (!Yii::$app->user->isGuest?'<ul class="navbar-nav nav" id="calNav" role="menu">
		<li id="badgeCal" class="dropdown">
		    <a class="dropdown-toggle" data-toggle="dropdown" href="#"><i class="fa fa-stethoscope" aria-hidden="true"></i> ผู้ป่วยที่ให้บริการ <span class="nav badge">0</span></a>
		    <ul id="calItem" role="menu" class="dropdown-menu">
			<div class="title">
			    กล่องข้อความ
			</div>
			<div class="content"></div>
			<div class="footer">
			    '.  \yii\helpers\Html::a('เพิ่มผู้ป่วย', ['/volunteer/volunteer-job/create']).'
			</div>
		    </ul>
		</li>
	    </ul>':''),
	    ['label' => '<i class="fa fa-ambulance" aria-hidden="true"></i> '.Yii::t('frontend', 'เพิ่มผู้ป่วย'), 'url' => ['/volunteer/volunteer-job/create'], 'encode'=>false, 'visible'=>!Yii::$app->user->isGuest],
	    ['label' => '<i class="fa fa-heartbeat" aria-hidden="true"></i> '.Yii::t('frontend', 'ประวัติการรักษา'), 'url' => ['/volunteer/default/myemr'], 'encode'=>false, 'visible'=>!Yii::$app->user->isGuest],//, 'visible'=>!Yii::$app->user->isGuest
	   
            [
                'label' => 'DPM menu',
                'visible'=> Yii::$app->keyStorage->get('frontend.domain')=='dpmcloud.org',
                'items'=>[
                    [
                        'label' => 'แบบประเมิน',
                        'url' => ['/volunteer/fix-app/ezform-view']
                    ],
                    [
                        'label' => 'แบบประเมินการสอนโดยรวม ภาควิชาศัลยศาสตร์',
                        'url' => ['/volunteer/fix-app/ezform-view2']
                    ],
                    [
                        'label' => 'Consultation',
                        'url' => ['/volunteer/fix-app/ezform-view3']
                    ],
                    [
                        'label' => 'ตารางเวร',
                        'url' => ['/volunteer/fix-app/data-ot']
                    ],
                    [
                        'label' => 'คำสั่ง ประกาศ',
                        'url' => ['/volunteer/fix-app/data-comnd']
                    ],
                ]
            ],
	    
//	    ['label' => Yii::t('backend', 'บันทึกข้อมูล'), 'visible'=>(Yii::$app->keyStorage->get('frontend.domain')=='thaicarecloud.org' ? true : false),
//		'url' => 'http://member.thaicarecloud.org/detoxthai_lte/login/chk-login.php?murl='.  base64_encode(Url::to(['/site/msg'], true)).'&rurl='.base64_encode(Url::to(['/user/sign-in/login'], true)).'&gurl='.base64_encode('http://member.thaicarecloud.org/detoxthai_lte/form/'), 'encode' => false],
//	    ['label' => Yii::t('backend', 'อัลบั้ม'), 'visible'=>(Yii::$app->keyStorage->get('frontend.domain')=='thaicarecloud.org' ? true : false),
//		'url' => 'http://member.thaicarecloud.org/detoxthai_lte/login/chk-login.php?murl='.  base64_encode(Url::to(['/site/msg'], true)).'&rurl='.base64_encode(Url::to(['/user/sign-in/login'], true)).'&gurl='.base64_encode('http://member.thaicarecloud.org/detoxthai_lte/form/album.php'), 'encode' => false],
//	    ['label' => Yii::t('backend', 'Timeline'), 'visible'=>(Yii::$app->keyStorage->get('frontend.domain')=='thaicarecloud.org' ? true : false),
//		'url' => 'http://member.thaicarecloud.org/detoxthai_lte/login/chk-login.php?murl='.  base64_encode(Url::to(['/site/msg'], true)).'&rurl='.base64_encode(Url::to(['/user/sign-in/login'], true)).'&gurl='.base64_encode('http://member.thaicarecloud.org/detoxthai_lte/timeline/'), 'encode' => false],
//	    ['label' => Yii::t('backend', 'รายงาน'), 'visible'=>(Yii::$app->keyStorage->get('frontend.domain')=='thaicarecloud.org' ? true : false),
//		'url' => 'http://member.thaicarecloud.org/detoxthai_lte/login/chk-login.php?murl='.  base64_encode(Url::to(['/site/msg'], true)).'&rurl='.base64_encode(Url::to(['/user/sign-in/login'], true)).'&gurl='.base64_encode('http://member.thaicarecloud.org/detoxthai_lte/report/'), 'encode' => false],
//	    
//            ['label' => Yii::t('frontend', 'ติดต่อเรา'), 'url' => ['/site/contact']],
            ['label' => Yii::t('frontend', 'สมัครสมาชิก'), 'url' => ['/user/sign-in/signup'], 'visible'=>Yii::$app->user->isGuest],
            ['label' => Yii::t('frontend', 'เข้าสู่ระบบ'), 'url' => Yii::getAlias('@backendUrl'), 'visible'=>Yii::$app->user->isGuest],
//            ['label' => Yii::t('frontend', 'เข้าสู่ระบบ'), 'url' => ['/user/sign-in/login'], 'visible'=>Yii::$app->user->isGuest],
            [
                'label' => Yii::$app->user->isGuest ? '' : Yii::$app->user->identity->getPublicIdentity(),
                'visible'=>!Yii::$app->user->isGuest,
                'items'=>[
                    [
                        'label' => Yii::t('frontend', 'ตั้งค่าผู้ใช้'),
                        'url' => Yii::getAlias('@backendUrl').'/sign-in/profile'// ['/user/default/index']
                    ],
                    [
                        'label' => Yii::t('frontend', 'เข้าทำงาน'),
                        'url' => Yii::getAlias('@backendUrl'),
                        'visible'=>Yii::$app->user->can('manager')
                    ],
                    [
                        'label' => Yii::t('frontend', 'ออกจากระบบ'),
                        'url' => ['/user/sign-in/logout'],
                        'linkOptions' => ['data-method' => 'post']
                    ]
                ]
            ],
//            [
//                'label'=>Yii::t('frontend', 'ภาษา'),
//                'items'=>array_map(function ($code) {
//                    return [
//                        'label' => Yii::$app->params['availableLocales'][$code],
//                        'url' => ['/site/set-locale', 'locale'=>$code],
//                        'active' => Yii::$app->language === $code
//                    ];
//                }, array_keys(Yii::$app->params['availableLocales']))
//            ]
        ]
    ]); ?>
    <?php NavBar::end(); ?>

<div class="container-fluid">
    <div class="pull-left" style="margin-top: 60px; margin-bottom: 2px;">

           
    </div>
</div>

    <?php echo $content ?>

<footer class="footer">
    <div class="container">
        <p class="pull-left"><?php echo Yii::$app->keyStorage->get('frontend.copyleft'); ?></p>
        <p class="pull-right"><?php echo Yii::$app->keyStorage->get('frontend.copyright'); ?></p>
    </div>
</footer>
<?php
$countVo = '';
if(!Yii::$app->user->isGuest){
    $countVo = "countPerson();
	function countPerson(){
	    $.ajax({
		method: 'POST',
		url: '".\yii\helpers\Url::to(['/volunteer/default/count'])."',
		dataType: 'JSON',
		success: function(result, textStatus) {
		    $('.nav.badge').html(result.num);
		}
	    });

	}";
}

        $this->registerJs("
	$('#calNav').click(function(){
		getCalContent();
	});
	
	$countVo

	function getCalContent(){
	    $('#calItem .content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
	    $.ajax({
		method: 'POST',
		url: '".Url::to(['/volunteer/default/content-alert'])."',
		dataType: 'JSON',
		success: function(result, textStatus) {
		    $('#calItem .content').html(result.html);
		}
	    });
	   
	}
	
	
	");
    ?>

<?php $this->endContent() ?>

