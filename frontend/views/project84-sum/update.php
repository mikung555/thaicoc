<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\Project84Sum */

$this->title = 'Update Project84 Sum: ' . ' ' . $model->zone;
$this->params['breadcrumbs'][] = ['label' => 'Project84 Sums', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->zone, 'url' => ['view', 'zone' => $model->zone, 'address' => $model->address]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="project84-sum-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
