<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\Project84SumSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="project84-sum-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'zone') ?>

    <?= $form->field($model, 'province') ?>

    <?= $form->field($model, 'amphur') ?>

    <?= $form->field($model, 'tambon') ?>

    <?= $form->field($model, 'address') ?>

    <?php // echo $form->field($model, 'nall') ?>

    <?php // echo $form->field($model, 'ov') ?>

    <?php // echo $form->field($model, 'ovpos') ?>

    <?php // echo $form->field($model, 'cca02') ?>

    <?php // echo $form->field($model, 'abnormal') ?>

    <?php // echo $form->field($model, 'suspected') ?>

    <?php // echo $form->field($model, 'ctmri') ?>

    <?php // echo $form->field($model, 'treatment') ?>

    <?php // echo $form->field($model, 'dead') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
