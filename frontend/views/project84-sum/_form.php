<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\Project84Sum */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="project84-sum-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'zone')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'province')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'amphur')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tambon')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nall')->textInput() ?>

    <?= $form->field($model, 'ov')->textInput() ?>

    <?= $form->field($model, 'ovpos')->textInput() ?>

    <?= $form->field($model, 'cca02')->textInput() ?>

    <?= $form->field($model, 'abnormal')->textInput() ?>

    <?= $form->field($model, 'suspected')->textInput() ?>

    <?= $form->field($model, 'ctmri')->textInput() ?>

    <?= $form->field($model, 'treatment')->textInput() ?>

    <?= $form->field($model, 'dead')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
