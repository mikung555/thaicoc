<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\Project84SumSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Project84 Sums';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="project84-sum-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Project84 Sum', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'zone',
            'province',
            'amphur',
            'tambon',
            'address',
            // 'nall',
            // 'ov',
            // 'ovpos',
            // 'cca02',
            // 'abnormal',
            // 'suspected',
            // 'ctmri',
            // 'treatment',
            // 'dead',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
