<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\Project84Sum */

$this->title = $model->zone;
$this->params['breadcrumbs'][] = ['label' => 'Project84 Sums', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="project84-sum-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'zone' => $model->zone, 'address' => $model->address], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'zone' => $model->zone, 'address' => $model->address], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'zone',
            'province',
            'amphur',
            'tambon',
            'address',
            'nall',
            'ov',
            'ovpos',
            'cca02',
            'abnormal',
            'suspected',
            'ctmri',
            'treatment',
            'dead',
        ],
    ]) ?>

</div>
