<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\Project84Sum */

$this->title = 'Create Project84 Sum';
$this->params['breadcrumbs'][] = ['label' => 'Project84 Sums', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="project84-sum-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
