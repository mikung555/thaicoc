<?php
    use yii\helpers\Html;

    //echo \yii\helpers\VarDumper::dump($result,10,true);
    echo '<div id="report84-ajax-report-div">';
        if(sizeof($result)==1){
            $result = $result[0];
            echo '<h4>';
                //echo 'รายละเอียดการตรวจพยาธิ';
            echo '</h4>';

            echo '<div class="report84-ajax-table-div">';
                echo '<table class="report84-detail-table table table-hover" rules="all" frame="box">';
                    echo '<tr>';
                        echo '<th>ประเภทการตรวจ</th>';
                        echo '<th>จำนวนผู้รับบริการ (ราย)</th>';
                        echo '<th>จำนวนผู้ติดพยาธิ OV (ราย)</th>';
                        echo '<th>ร้อยละ</th>';
                    echo '</tr>';

                    echo '<tr>';
                        echo '<td>Kato-Katz</td>';
                        echo '<td>'.number_format($result['k']).'</td>';
                        echo '<td>'.number_format($result['kpos']).'</td>';
                        echo '<td>'.number_format(intval($result['k'])==0 ? 0 : (intval($result['kpos'])/intval($result['k']))*100 ,1).'</td>';
                    echo '</tr>';

                    echo '<tr>';
                        echo '<td>Parasep</td>';
                        echo '<td>'.number_format($result['p']).'</td>';
                        echo '<td>'.number_format($result['ppos']).'</td>';
                        echo '<td>'.number_format(intval($result['p'])==0 ? 0 : (intval($result['ppos'])/intval($result['p']))*100 ,1).'</td>';
                    echo '</tr>';

                    echo '<tr>';
                        echo '<td>FECT</td>';
                        echo '<td>'.number_format($result['f']).'</td>';
                        echo '<td>'.number_format($result['fpos']).'</td>';
                        echo '<td>'.number_format(intval($result['f'])==0 ? 0 : (intval($result['fpos'])/intval($result['f']))*100 ,1).'</td>';
                    echo '</tr>';

                    echo '<tr>';
                        echo '<td>Urine</td>';
                        echo '<td>'.number_format($result['u']).'</td>';
                        echo '<td>'.number_format($result['upos']).'</td>';
                        echo '<td>'.number_format(intval($result['u'])==0 ? 0 : (intval($result['upos'])/intval($result['u']))*100 ,1).'</td>';
                    echo '</tr>';

                echo '</table>';
            echo '</div>';

            if(sizeof($result['amphur'])>0){
                echo '<h4>';
                    echo 'ค. จำแนกตามภูมิลำเนาของผู้รับบริการ (Community-based) ดังนี้';
                echo '</h4>';

                echo '<div class="report84-ajax-table-div">';
                    echo '<table class="report84-detail-table table table-hover" rules="all" frame="box">';
                        echo '<tr>';
                            echo '<th>อำเภอ</th>';
                            echo '<th>จำนวน (ราย)</th>';
                        echo '</tr>';
                        foreach($result['amphur'] as $key => $value){
                            echo '<tr>';
                            echo '<td>'.$value['amphur'].'</td>';
                            echo '<td>'.number_format($value['cca02']).'</td>';
                            echo '</tr>';
                        }
                    echo '</table>';
                echo '</div>';
            }

        }
    echo '</div>';

    $this->registerCss('
        #report84-ajax-report-div{
            text-align:left;
        }
        .report84-detail-table{
            border : 1px solid #d9d9d9;
        }
        .report84-detail-table{
            border : 1px solid #d9d9d9;
            width: 100%;
        }
        .report84-detail-table tr:first-child{
            background-color: #e2e2e2 !important;
        }
        .report84-detail-table tr:nth-child(odd) {
            background: #fafafa;
        }
        .report84-detail-table tr:first-child>th{
            text-align: center;
        }
        .report84-detail-table tr:first-child > th:first-child{
            width: 30%;
        }
        .report84-detail-table tr>td:nth-child(2),
        .report84-detail-table tr>td:nth-child(3),
        .report84-detail-table tr>td:last-child{
            text-align: right;
        }
        .report84-detail-table th, .report84-detail-table td{
            padding: 3px;
        }
        .report84-ajax-table-div {
            padding: 10px 5% 10px 5%;
        }
    ');

//    $this->registerJs('
//        //$("#report84-modal-header").html("'.$result['province']['province'].'");
//    ');