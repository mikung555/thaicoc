<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//$data=yii\helpers\ArrayHelper::map($us, 'times', 'name');
//
//echo \kartik\select2\Select2::widget([
//    'name' => 'state_2',
//    'value' => '',
//    'data' => $data,
//    'options' => ['multiple' => true, 'placeholder' => 'Select states ...']
//]);
    use kartik\social\FacebookPlugin;
    use yii\helpers\Html;
    use yii\helpers\Url;
    
    $sharesetting = [
        'data-href'   =>  "?tab=ultrasound",
        'data-layout' =>  "button_count",
        'data-size'   =>  "small",  
        'data-mobile-iframe'  =>  "true",
    ];

    echo FacebookPlugin::widget(['type'=>FacebookPlugin::SHARE, 'settings' => $sharesetting]);    
    $permlink = Url::base(true) . Url::toRoute(['report/index', 'tab' => 'ultrasound']);
    echo Html::a('<p class="btn btn-xs btn-info">Permanent Link</p> ' . $permlink,$permlink); 
    
?>
<br><?php echo FacebookPlugin::widget(['type'=>FacebookPlugin::LIKE, 'settings' => ['data-href'=>$permlink,'data-layout'=>'box_count']]);?>
<script language="JavaScript">
<!--
function autoResize(id){
    var newheight;
    var newwidth;
    if(document.getElementById){
        newheight=document.getElementById(id).contentWindow.document.body.scrollHeight;
        newwidth=document.getElementById(id).contentWindow.document.body.scrollWidth;
    }
    document.getElementById(id).height= (newheight) + "px";
    document.getElementById(id).width= (newwidth) + "px";
}
//-->
</script>
<style>
#wrapper { width: 100%; height: 1100px; padding: 0; overflow: hidden; }
#wrapper2 { width: 100%; height: 1400px; padding: 0; overflow: hidden; }
#iframe2 { width: 150%; height: 1800px; border: 0px; }
#iframe2 {
    zoom: 0.71;
    -moz-transform: scale(0.71);
    -moz-transform-origin: 0 0;
    -o-transform: scale(0.71);
    -o-transform-origin: 0 0;
    -webkit-transform: scale(0.71);
    -webkit-transform-origin: 0 0;
}

@media screen and (-webkit-min-device-pixel-ratio:0) {
 #scaled-frame  { zoom: 1;  }
}
</style>

<div id="wrapper" style=" text-align:center;">
  <iframe src="https://tools.cascap.in.th/v4cascap/version01/test1.php?task=formkey&act=ptreport&act2=cca02sum&ssite=10964&dstart=11/11/2559&dend=11/11/2559&dfix=41&uxxx=10964" id="iframe2" scrolling="no" frameborder="0" onload="autoResize('iframe2');" width="100%" height="1900"></iframe>
</div>
<!-- div id="wrapper2" style=" text-align:center;">
  <iframe src="https://www.cascap.in.th/v4cascap/version01/test2.php?task=formkey&act=ptreport&act2=cca02sum&ssite=10705&dstart=12/02/2559&dend=12/02/2559&dfix=41" id="iframe3" scrolling="no" frameborder="0" onload="autoResize('iframe2');" width="100%" height="1400"></iframe>
</div -->