<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
    use kartik\grid\GridView;
    use kartik\dynagrid\DynaGrid;
    use yii\helpers\Html;
    use common\lib\sdii\components\helpers\SDNoty;
    use appxq\sdii\helpers\SDHtml;
    use kartik\widgets\DatePicker;
    use yii\bootstrap\ActiveForm;
    use common\lib\sdii\components\utils\SDdate;
    use yii\helpers\Url;
    use kartik\social\FacebookPlugin;
    
    $sharesetting = [
        'data-href'   =>  "?tab=main",
        'data-layout' =>  "button_count",
        'data-size'   =>  "small",  
        'data-mobile-iframe'  =>  "true",
    ];

    echo FacebookPlugin::widget(['type'=>FacebookPlugin::SHARE, 'settings' => $sharesetting]);    
    
    $permlink = Url::base(true) . Url::toRoute(['report/index', 'tab' => 'main']);
    echo Html::a('<p class="btn btn-xs btn-info">Permanent Link</p> ' . $permlink,$permlink); 
    ?>
<br><?php echo FacebookPlugin::widget(['type'=>FacebookPlugin::LIKE, 'settings' => ['data-href'=>$permlink,'data-layout'=>'box_count']]);?>
<?php
    /*
    $this->registerJS('
        function getValueChecked(){
            var allVals = [];
            $("#inputZone:checked").each(function(){
                allVals.push($(this).val());
            });
            $.ajax({
                type    : "GET",
                cache   : false,
                url     : "'.Url::to('get-province-by-zone').'",
                data    : {
                    zones: allVals
                },
                success  : function(response) {
                    $("#inputProvence").html(response);
                },
                error : function(){
                    $("#inputProvence").html("<option>Error</option>");
                }
            });
        }
        $(function(){
            $("#divZone input").click(getValueChecked);
            getValueChecked();
        });
    ',\yii\web\View::POS_END);
    */


    $this->registerJs('
        var report84getnew = false;
        function getProject84Report(){
            var tmpFromDate = $("#project84-fromdate").val();
            tmpFromDate = tmpFromDate.split("/");
            tmpFromDate = tmpFromDate[2]+"-"+tmpFromDate[1]+"-"+tmpFromDate[0];

            var tmpToDate = $("#project84-todate").val();
            tmpToDate = tmpToDate.split("/");
            tmpToDate = tmpToDate[2]+"-"+tmpToDate[1]+"-"+tmpToDate[0];

            var project84FormData = {};
            project84FormData["fromDate"] = tmpFromDate;
            project84FormData["toDate"] = tmpToDate;
            project84FormData["refresh"] = report84getnew;

            $("#project84-getnew > i").addClass("fa-spin");
            $("#project84-getnew").attr("title","กำลังดึงข้อมูลล่าสุด อาจใช้เวลาหลายนาที");

            $.ajax({
                type    : "GET",
                cache   : false,
                url     : "'.yii\helpers\Url::to('ajax-report84/').'",
                data    : project84FormData,
                success :   function(response) {
                    //$("#cca02ReportSubmitAjax").removeAttr("disabled");
                    //$("#cca02-report-div").html(response);
                    //$("#excel-cca02").show();
                    $("#report84-view").html(response);

                    //$("#report84-dynagrid-3 table th:nth-child(3)").css("min-width","300px");

                    report84getnew = false;
                    $("#project84-getnew > i").removeClass("fa-spin");
                    $("#project84-getnew").removeAttr("title");
                },
                error   :   function(){
                    //$("#cca02ReportSubmitAjax").removeAttr("disabled");
                    //$("#cca02-report-div").html("การแสดงข้อมูลผิดพลาด");
                    report84getnew = false;
                    $("#project84-getnew > i").removeClass("fa-spin");
                    $("#project84-getnew").removeAttr("title");
                }
            });

        }

        function validateReport84(){
            if($("[id^=\'project84-checkbox\']:checked").length == 0){
                return false;
            }
        }
    ',1);

    $this->registerJs('
        getProject84Report();

        /*
        $("#project84-fromdate").change(function(){
            $("#project84-todate").attr("min",$("#project84-fromdate").val());
        });

        $("#project84-todate").change(function(){
            $("#project84-fromdate").attr("max",$("#project84-todate").val());
        });
        */

        /*
        $("[id^=\'project84-checkbox\']").change(function(){
            var checkBoxZone = $("[id^=\'project84-checkbox\']");
            checkBoxZone.each(function(i){

                var thisval = $(this).val();
                var thisoptgroup = $("optgroup[zone=\'"+thisval+"\']");

                if($(this).prop("checked")){
                    thisoptgroup.show().removeAttr("disabled");
                }else{
                    thisoptgroup.hide().attr("disabled",true);
                }
            });

            if($("[id^=\'project84-checkbox\']:checked").length == 0){
                $("#project84-submit").attr("disabled",true);
            }else{
                $("#project84-submit").removeAttr("disabled");
            }

            $("#project84-province")[0].selectedIndex = 0;
        });

        $("#project84-province").change(function(){
            if($(this)[0].selectedIndex==0){
                $("[id^=\'project84-checkbox\']").prop("checked",true);
                $("optgroup[zone]").show().removeAttr("disabled");
                return false;
            }
            var selectedZone = $("option[value=\'"+$(this).val()+"\']").attr("zone");
            //console.log( $("option[value=\'"+$(this).val()+"\']") );
            //console.log( $("option[value=\'"+$(this).val()+"\']").attr("zone") );
            $("[id^=\'project84-checkbox\']").prop("checked",false);
            $("optgroup[zone]").hide().attr("disabled",true);

            $("#project84-checkbox-"+selectedZone).prop("checked",true);
            $("optgroup[zone=\'"+selectedZone+"\']").show().removeAttr("disabled");

        });

        $("#project84-allzone").click(function(){
            $("[id^=\'project84-checkbox\']").prop("checked",true);
            $("#project84-province")[0].selectedIndex = 0;
            $("optgroup[zone]").show();
            $("#project84-submit").removeAttr("disabled");
        });

        */

        $("#project84-submit").click(function(){
            //validateReport84();
            getProject84Report();
        });

        $("#project84-getnew").click(function(){
            report84getnew = true;
            getProject84Report();
            return false;
        });

        $("#project84-reset").click(function(){
            $("#project84-submit").removeAttr("disabled");
            $("#project84-fromdate").attr("min","2015-10-01").attr("min","")
        });

        $("#project84-form").submit(function(e){
            e.preventDefault();
            //validateReport84();
            //getProject84Report();
            return false;
        });
    ');

    ?>

    <div class="ov-filter-search" style="margin-bottom: 50px;">
        <form id="project84-form" class="form-inline col-md-12 col-sm-9" style="margin-bottom: 30px; float:left; text-align: center;">
            <h3><p class="text-center">เลือกตัวกรอง</p></h3>

            <label for="project84-fromdate" style="margin-top: 7px; width: 60px;">เริ่มวันที่</label>
<!--            <input type="date" class="form-control" id="project84-fromdate" min="2015-10-01" max="--><?//= date("Y-m-d"); ?><!--" value="2015-10-01" required>-->
            <?= kartik\widgets\DatePicker::widget([
                'id'=>'project84-fromdate',
                'name' => 'project84-fromdate',
                'value' => '01/10/2015',
                'removeButton' => false,
                'pluginOptions' => [
                    'autoclose'=>true,
                    'format' => 'dd/mm/yyyy',
                    'todayHighlight' => true,
                    //'startDate'=>'01/10/2015',
                    //'endDate'=>date('d/m/Y')
                ]
            ]) ?>
            <label for="project84-todate" style="margin-top: 7px;">ถึงวันที่</label>
<!--            <input type="date" class="form-control" id="project84-todate" min="2015-10-01" max="--><?//= date("Y-m-d") ?><!--" value="--><?//= date("Y-m-d") ?><!--" required>-->
            <?= kartik\widgets\DatePicker::widget([
                'id'=>'project84-todate',
                'name' => 'project84-fromdate',
                'value' => '30/09/2016', //date('d/m/Y'),
                'removeButton' => false,
                'pluginOptions' => [
                    'autoclose'=>true,
                    'format' => 'dd/mm/yyyy',
                    'todayHighlight' => true,
                    //'startDate'=>'01/10/2015',
                    //'endDate'=>date('d/m/Y')
                ]
            ]) ?>
            <input type="submit" class="btn btn-primary" id="project84-submit" value="แสดงรายงาน" />
            <br/><br/>
            <?php
            $sqlControl = 'SELECT * ';
            $sqlControl.= 'FROM project84_report_control ';
            $sqlControl.= 'WHERE fromDate = "2015-10-01" ';
            //$sqlControl.= 'AND toDate = "'.date('Y-m-d').'" ';
            $sqlControl.= 'AND toDate = "2016-09-30" ';
            $sqlControl.= 'LIMIT 1';

            $queryControl = Yii::$app->db->createCommand($sqlControl)->queryOne();

            echo '<label id="project84-lastcal">';
                //echo 'คำนวนล่าสุดเมื่อ '.$queryControl['lastcal'];

            echo '</label>';
            echo ' <button class="btn btn-primary" id="project84-getnew" /><i class="fa fa-refresh"></i></button>';

            ?>


<!--            <div class="col-sm-9 col-md-9" style="margin-bottom: 15px;">-->
<!--                -->
<!--            </div>-->

<!--            <div class="col-sm-9 col-md-9" style="margin-bottom: 15px;">-->
<!--                <label for="project84-zone" style="margin-top: 7px; width: 60px;">เขต</label>-->
<!---->
<!--                --><?php
//                $sqlZoneProject84 = 'SELECT project84_zone.zone+0 AS zonecode FROM project84_zone GROUP BY project84_zone.zone ORDER BY project84_zone.zone+0 ASC';
//                $qryZoneProject84 = Yii::$app->db->createCommand($sqlZoneProject84)->queryAll();
//                foreach ($qryZoneProject84 as $row) {
//                    echo '<label class="checkbox-inline">';
//                        echo '<input type="checkbox" id="project84-checkbox-'.$row['zonecode'].'"
//                            value="'.$row['zonecode'].'" checked>เขต '.trim($row['zonecode']);
//                    echo '</label>';
//                }
//                ?>
<!--                <input type="button" class="btn btn-default" id="project84-allzone" value="เลือกทุกเขต" />-->
<!--                <br/>-->
<!--            </div>-->

<!--            <div class="col-sm-9 col-md-9" style="margin-bottom: 15px;">-->
<!--                <label for="input-province" style="margin-top: 7px; width: 60px;">จังหวัด</label>-->
<!---->
<!--                <select class="form-control" id="project84-province">-->
<!--                    <option value="">ทุกจังหวัด</option>-->
<!--                    --><?php
//
//                    $optgroupZone = '';
//                    $grouped = false;
//
//                    $sqlProvinceProject84 = 'SELECT
//                        zone+0 AS zonecode,
//                        LEFT(address,2) AS provincecode,
//                        province
//                    FROM project84_zone
//                    GROUP BY province
//                    ORDER BY zone+0, province';
//                    $qryProvinceProject84 = Yii::$app->db->createCommand($sqlProvinceProject84)->queryAll();
//                    foreach ($qryProvinceProject84 as $row) {
//                        if($optgroupZone != $row['zonecode']){
//                            $optgroupZone = $row['zonecode'];
//                            if($grouped){
//                                echo '</optgroup>';
//                            }
//                            $grouped = true;
//                            echo '<optgroup zone="'.$optgroupZone.'" label="เขต '.$optgroupZone.'">';
//                        }
//                        echo '<option value="'.$row['provincecode'].'" zone="'.$optgroupZone.'">';
//                        echo $row['province'];
//                        echo '</option>';
//                    }
//                    ?>
<!--                </select>-->
<!--                <br/>-->
<!--            </div>-->

<!--            <div class="col-sm-9 col-md-9">-->
<!--                <input type="submit" class="btn btn-primary" id="project84-submit" value="แสดงรายงาน" style="margin-left: 65px;" />-->
<!--                <input type="reset" class="btn btn-warning" id="project84-reset" value="ล้างตัวกรอง"/>-->
<!--            </div>-->

        </form>

        <?php
        /*

        $form = ActiveForm::begin([
            'id' => 'report84',
            'action'=>['find84'],
            'layout' => 'inline',
        ]);
        echo '<a id="all15" class="btn btn-default btn-sm">ปี 2015 ทั้งหมด</a>  &nbsp;';
        echo $form->field($model, 'month15')->checkboxList([9=>"ต.ค.", 10=>"พ.ย.",11=>"ธ.ค."]);
        echo '<br/>';
        echo '<a id="all16" class="btn btn-default btn-sm">ปี 2016 ทั้งหมด</a> &nbsp;';
        echo $form->field($model, 'month16')->checkboxList(SDdate::$thaimonth);
        echo '<br><br>';
        echo Html::submitButton(Yii::t('app', 'Submit'), ['class' => 'btn btn-primary']);
        echo Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']);
        ActiveForm::end();

        */
        ?>

    </div>
    <div class="row">
        <div class="col-md-12">
<!--            <div class="progress">-->
<!--                <div class="progress-bar progress-bar-success" style="width: 35%">-->
<!--                    35% Complete (success)-->
<!--                </div>-->
<!--                <div class="progress-bar progress-bar-warning" style="width: 20%">-->
<!--                    20% Complete (warning)-->
<!--                </div>-->
<!--                <div class="progress-bar progress-bar-danger" style="width: 10%">-->
<!--                    10% Complete (danger)-->
<!--                </div>-->
<!--            </div>-->
<!--            <div class="progress">-->
<!--                <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 45%">-->
<!--                    <span class="sr-only">45% Complete</span>-->
<!--                </div>-->
<!--            </div>-->
        </div>
    </div>


    <div id="report84-view">

        <?php
        /*echo $this->render('_project84_grid',[
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'model'=>$model
        ]);*/
    ?>
    </div>

    <?php

    $this->registerJs("
        $('#all15').click(function() {
            var checkbox = $('input:checkbox[name=\"Project84Sum[month15][]\"]');

            if (! checkbox.prop('checked')) {
            checkbox.prop('checked', true);
            } else {
            checkbox.prop('checked', false);
            }
        });

        $('#all16').click(function() {
            var checkbox = $('input:checkbox[name=\"Project84Sum[month16][]\"]');

            if (! checkbox.prop('checked')) {
            checkbox.prop('checked', true);
            } else {
            checkbox.prop('checked', false);
            }
        });

        $('form#report84').on('beforeSubmit', function(e) {
            //$('#report84-view').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
            var \$form = $(this);
            $.post(
            \$form.attr('action'), //serialize Yii2 form
            \$form.serialize()
            ).done(function(result) {
            $('#report84-view').html(result);
            return false;
            }).fail(function() {
            console.log('server error');
            });
            return false;
        });

    ");


?>