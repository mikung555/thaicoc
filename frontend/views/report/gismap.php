<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
    use kartik\social\FacebookPlugin;
    use yii\helpers\Html;
    use yii\helpers\Url;
    
    $sharesetting = [
        'data-href'   =>  "?tab=gismap",
        'data-layout' =>  "button_count",
        'data-size'   =>  "small",  
        'data-mobile-iframe'  =>  "true",
    ];

    echo FacebookPlugin::widget(['type'=>FacebookPlugin::SHARE, 'settings' => $sharesetting]);   

    $permlink = Url::base(true) . Url::toRoute(['report/index', 'tab' => 'gismap']);
    echo Html::a('<p class="btn btn-xs btn-info">Permanent Link</p> ' . $permlink,$permlink);
    
?>
<br><?php echo FacebookPlugin::widget(['type'=>FacebookPlugin::LIKE, 'settings' => ['data-href'=>$permlink,'data-layout'=>'box_count']]);?>
<br><br>
<iframe src="/report/map" scrolling="no" frameborder="no" width="100%" height="800" ></iframe>
