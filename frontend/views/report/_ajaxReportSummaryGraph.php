<?php
use yii\helpers\Html;
if(!isset($LastUpdateTime)) exit();

$datetime1 = strtotime($LastUpdateTime);
$datetime2 = strtotime(date("Y-m-d H:i:s"));
$interval  = abs($datetime2 - $datetime1);
$mDiff   = round($interval / 60);
if($mDiff <= 60)
    $btnColor = "primary";
elseif($mDiff > 60 && $mDiff <= 120)
    $btnColor = "info";
elseif($mDiff > 120 && $mDiff <= 1440)
    $btnColor = "warning";
else
    $btnColor = "danger";
?>
<div class="well" align="right">
    <label id="project84-lastcal">คำนวนกราฟล่าสุดเมื่อ <?=$LastUpdateTime;?></label>&nbsp;
    <button class="btn btn-<?=$btnColor;?>" id="graph-getnew" title="คลิกเพื่อโหลดข้อมูลล่าสุด(อาจใช้เวลาหลายวินาที)" alt="คลิกเพื่อโหลดข้อมูลล่าสุด(อาจใช้เวลาหลายวินาที)"><i class="fa fa-refresh"></i></button>
</div>
<div align="left" class="alert alert-info" role="alert"><h4>ก. จำแนกตามเวลา</h4></div>
<div id="G1" style="height: 400px; margin: 0 auto"></div>
<div id="G2" style="height: 400px; margin: 0 auto"></div>    
<div align="left" class="alert alert-info" role="alert"><h4>ข. จำแนกตามเขตตรวจราชการสาธารณสุข (ระหว่าง ก.พ. 2556 ถึง ปัจจุบัน)</h4></div>
<div class="row">
  <div class="col-md-6" id="G3" style="height: 400px; margin: 0 auto"></div>
  <div class="col-md-6" id="G4" style="height: 400px; margin: 0 auto"></div>
</div> 
<div align="left" class="alert alert-info" role="alert"><h4>ค. จำแนกตามจังหวัด (ระหว่าง ก.พ. 2556 ถึง ปัจจุบัน)</h4></div>
<div id="G5" style="height: 400px; margin: 0 auto"></div>
<div id="G6" style="height: 400px; margin: 0 auto"></div>
<div align="left" class="alert alert-info" role="alert"><h4>ง. แสดงจำนวนการเข้ารับการรักษา&nbsp;</h4></div>
<div id="G7" style="height: 400px; margin: 0 auto"></div>

<script>
function timeout() {
    $(window).resize();
}
window.setTimeout(function() {
    timeout();
},2000);
</script>
<script type="text/javascript">
$(function () {
    $('#G1').highcharts({
        chart: {
            type: 'column'
        },        
        title: {
            text: 'จำนวนกลุ่มเสี่ยงที่มีข้อมูลพื้นฐาน (CCA-01)'
        },/*
        subtitle: {
            text: 'Source: <a href="http://en.wikipedia.org/wiki/List_of_cities_proper_by_population">Wikipedia</a>'
        },*/
        xAxis: {
            type: 'category',
            labels: {
                rotation: -45,
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'จำนวนประชากร(คน)'
            }
        },
        legend: {
            enabled: false
        },
        tooltip: {
            pointFormat: 'จำนวนประชากร: <b>{point.y:.0f} ราย</b>'
        },
        series: [{
            name: 'Population',
            data: [
                <?php
                for($i=0;$i<count($g1data);$i++){
                    echo "['".$g1label[$i]."',".$g1data[$i]."],";
                }
                ?>                
            ],
            dataLabels: {
                enabled: false,
                rotation: -90,
                color: '#FFFFFF',
                align: 'right',
                format: '{point.y:.1f}', // one decimal
                y: 10, // 10 pixels down from the top
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        }]
    });
});        
</script>
<script type="text/javascript">
$(function () {
    $('#G2').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'จำนวนกลุ่มเสี่ยงที่ได้รับการตรวจอัลตราซาวด์ (CCA-02)'
        },/*
        subtitle: {
            text: 'Source: <a href="http://en.wikipedia.org/wiki/List_of_cities_proper_by_population">Wikipedia</a>'
        },*/
        xAxis: {
            type: 'category',
            labels: {
                rotation: -45,
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'จำนวนประชากร(คน)'
            }
        },
        legend: {
            enabled: false
        },
        tooltip: {
            pointFormat: 'จำนวนประชากร: <b>{point.y:.0f} ราย</b>'
        },
        series: [{
            name: 'Population',
            data: [
                <?php
                for($i=0;$i<count($g2data);$i++){
                    echo "['".$g1label[$i]."',".$g2data[$i]."],";
                }
                ?>                
            ],
            color: '#f7a35c',
            dataLabels: {
                enabled: false,
                rotation: -90,
                color: '#FFFFFF',
                align: 'right',
                format: '{point.y:.1f}', // one decimal
                y: 10, // 10 pixels down from the top
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        }]
    });
});        
</script>
<script type="text/javascript">
$(function () {
    $('#G3').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'จำนวนกลุ่มเสี่ยงที่มีข้อมูลพื้นฐาน (CCA-01)'
        },
        /*
        subtitle: {
            text: 'Source: <a href="http://en.wikipedia.org/wiki/List_of_cities_proper_by_population">Wikipedia</a>'
        },
        */
        xAxis: {
            type: 'category',
            labels: {
                rotation: -45,
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'จำนวนประชากร(คน)'
            }
        },
        legend: {
            enabled: false
        },
        tooltip: {
            pointFormat: 'จำนวนประชากร: <b>{point.y:.0f} ราย</b>'
        },
        series: [{
            name: 'Population',
            data: [
                <?php
                for($i=0;$i<count($g3data);$i++){
                    echo "['".$g3label[$i]."',".$g3data[$i]."],";
                }
                ?>            
            ],
            dataLabels: {
                enabled: true,
                rotation: -90,
                color: '#FFFFFF',
                align: 'right',
                format: '{point.y:.0f}', // one decimal
                y: 10, // 10 pixels down from the top
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        }]
    });
});
</script>
<script type="text/javascript">
$(function () {
    $('#G4').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'จำนวนกลุ่มเสี่ยงที่ได้รับการตรวจอัลตราซาวด์ (CCA-02)'
        },
        /*
        subtitle: {
            text: 'Source: <a href="http://en.wikipedia.org/wiki/List_of_cities_proper_by_population">Wikipedia</a>'
        },
        */
        xAxis: {
            type: 'category',
            labels: {
                rotation: -45,
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'จำนวนประชากร(คน)'
            }
        },
        legend: {
            enabled: false
        },
        tooltip: {
            pointFormat: 'จำนวนประชากร: <b>{point.y:.0f} ราย</b>'
        },
        series: [{
            name: 'Population',
            data: [
                <?php
                for($i=0;$i<count($g4data);$i++){
                    echo "['".$g4label[$i]."',".$g4data[$i]."],";
                }
                ?>
            ],
            color: '#f7a35c',
            dataLabels: {
                enabled: true,
                rotation: -90,
                color: '#FFFFFF',
                align: 'right',
                format: '{point.y:.0f}', // one decimal
                y: 10, // 10 pixels down from the top
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        }]
    });
});
</script>
<script type="text/javascript">
$(function () {
    $('#G5').highcharts({
        chart: {
            type: 'column'
        },
        plotOptions: {
            series: {
                colorByPoint: true
            }
        },        
        title: {
            text: 'จำนวนกลุ่มเสี่ยงที่มีข้อมูลพื้นฐาน (CCA-01)'
        },
        /*
        subtitle: {
            text: 'Source: <a href="http://en.wikipedia.org/wiki/List_of_cities_proper_by_population">Wikipedia</a>'
        },
        */
        xAxis: {
            type: 'category',
            labels: {
                rotation: -45,
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'จำนวนประชากร(คน)'
            }
        },
        legend: {
            enabled: false
        },
        tooltip: {
            pointFormat: 'จำนวนประชากร: <b>{point.y:.0f} ราย</b>'
        },
        series: [{
            name: 'Population',
            data: [
                <?php
                for($i=0;$i<count($g5data);$i++){
                    echo "['".$g5label[$i]."',".$g5data[$i]."],";
                }
                ?>            
            ],
            dataLabels: {
                enabled: false,
                rotation: -90,
                color: '#FFFFFF',
                align: 'right',
                format: '{point.y:.0f}', // one decimal
                y: 10, // 10 pixels down from the top
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        }]
    });
});
</script>
<script type="text/javascript">
$(function () {
    $('#G6').highcharts({
        chart: {
            type: 'column'
        },
        plotOptions: {
            series: {
                colorByPoint: true
            }
        },        
        title: {
            text: 'จำนวนกลุ่มเสี่ยงที่ได้รับการตรวจอัลตราซาวด์ (CCA-02)'
        },
        /*
        subtitle: {
            text: 'Source: <a href="http://en.wikipedia.org/wiki/List_of_cities_proper_by_population">Wikipedia</a>'
        },
        */
        xAxis: {
            type: 'category',
            labels: {
                rotation: -45,
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'จำนวนประชากร(คน)'
            }
        },
        legend: {
            enabled: false
        },
        tooltip: {
            pointFormat: 'จำนวนประชากร: <b>{point.y:.0f} ราย</b>'
        },
        series: [{
            name: 'Population',
            data: [
                <?php
                for($i=0;$i<count($g6data);$i++){
                    echo "['".$g6label[$i]."',".$g6data[$i]."],";
                }
                ?>            
            ],
            dataLabels: {
                enabled: false,
                rotation: -90,
                color: '#FFFFFF',
                align: 'right',
                format: '{point.y:.0f}', // one decimal
                y: 10, // 10 pixels down from the top
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        }]
    });
});
</script>
<script type="text/javascript">
$(function () {
    $('#G7').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'แสดงจำนวนผลการตรวจรักษาที่โรงพยาบาลเครือข่ายหลัก 9 แห่ง'
        },
        subtitle: {
            text: 'แยกตามฟอร์มนำเข้าข้อมูล CCA-02.1, CCA-03, CCA-04, CCA-05'
        },
        xAxis: {
            categories: [
                <?php
                for($i=0;$i<count($g7data);$i++){
                    echo "'".$g7data[$i][0]."',";
                }
                ?>
            ],
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: 'จำนวนประชากร(คน)'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:16px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.0f} ราย</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [{
            name: 'CCA-02.1',
            data: [
                <?php
                for($i=0;$i<count($g7data);$i++){
                    echo $g7data[$i][2].",";
                }
                ?>
            ]
        }, {
            name: 'CCA-03',
            data: [
                <?php
                for($i=0;$i<count($g7data);$i++){
                    echo $g7data[$i][3].",";
                }
                ?>
            ]
        }, {
            name: 'CCA-04',
            data: [
                <?php
                for($i=0;$i<count($g7data);$i++){
                    echo $g7data[$i][4].",";
                }
                ?>            
            ]
        }, {
            name: 'CCA-05',
            data: [
                <?php
                for($i=0;$i<count($g7data);$i++){
                    echo $g7data[$i][5].",";
                }
                ?>            
            ]
        }]
    });
});    
</script>   