<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
use kartik\grid\GridView;
use common\lib\sdii\components\utils\SDdate;
use yii\bootstrap\Progress;
use yii\helpers\Url;
//use Yii;
/*
    if(isset($model->month15) && !empty($model->month15)){
        echo 'ปี 2015 : ';
        $spc = '';
        foreach ($model->month15 as $key => $value) {
        echo $spc. SDdate::$thaimonthFull[$value];
        $spc = ', ';
        }
        echo '<br>';
    }

    if(isset($model->month16) && !empty($model->month16)){
        echo 'ปี 2016 : ';
        $spc = '';
        foreach ($model->month16 as $key => $value) {
        echo $spc. SDdate::$thaimonthFull[$value];
        $spc = ', ';
        }
        echo '<br>';
    }
*/

    $loadIconData = '\'<i class="fa fa-spinner fa-spin fa-3x" style="margin:50px 0;"></i>\'';

    $thaiMonth = ['',
        1=>['abvt'=>'ม.ค.', 'full'=>'มกราคม'],
        2=>['abvt'=>'ก.พ.', 'full'=>'กุมภาพันธ์'],
        3=>['abvt'=>'มี.ค.', 'full'=>'มีนาคม'],
        4=>['abvt'=>'เม.ย.', 'full'=>'เมษายน'],
        5=>['abvt'=>'พ.ค.', 'full'=>'พฤษภาคม'],
        6=>['abvt'=>'มิ.ย.', 'full'=>'มิถุนายน'],
        7=>['abvt'=>'ก.ค.', 'full'=>'กรกฎาคม'],
        8=>['abvt'=>'ส.ค.', 'full'=>'สิงหาคม'],
        9=>['abvt'=>'ก.ย.', 'full'=>'กันยายน'],
        10=>['abvt'=>'ต.ค.', 'full'=>'ตุลาคม'],
        11=>['abvt'=>'พ.ย.', 'full'=>'พฤศจิกายน'],
        12=>['abvt'=>'ธ.ค.', 'full'=>'ธันวาคม']
    ];

    $fromDateTmp = explode('-',$date['f']);
    $toDateTmp = explode('-',$date['t']);
    $fromDateShow = intval($fromDateTmp[2]).' '.$thaiMonth[intval($fromDateTmp[1])]['full'].' '.(intval($fromDateTmp[0])+543);
    $toDateShow = intval($toDateTmp[2]).' '.$thaiMonth[intval($toDateTmp[1])]['full'].' '.(intval($toDateTmp[0])+543);

    $lastcalDate = explode('-',(explode(' ',$lastcal)[0]));

    $lastcalTimeShow = explode(' ',$lastcal)[1];
    $lastcalDateShow = intval($lastcalDate[2]).' '.$thaiMonth[intval($lastcalDate[1])]['full'].' '.(intval($lastcalDate[0])+543);


    $this->registerJs('
        $("#project84-lastcal").html("คำนวนล่าสุดเมื่อวันที่ '.$lastcalDateShow.' เวลา '.$lastcalTimeShow.'");
    ');

    $primaryPctAll = ($sum['ovall']/76000)*100;
    $primaryPct = ($sum['ov']/76000)*100;

    $secondaryPctAll = ($sum['cca02all']/135000)*100;
    $secondaryPct = ($sum['cca02']/135000)*100;

    ?>
    <br>
    <div class="alert alert-info" role="alert"><h4>ส่วนที่ 1 : รายงาน Primary prevention (คัดกรองพยาธิใบไม้ตับด้วยการตรวจอุจจาระและปัสสาวะ)</h4></div>
    <h5>
        ผลงานตรวจอุจจาระและปัสสาวะ
        <label class="label label-primary"><?=number_format($sum['ovall'])?></label>
        ราย คิดเป็น
        <label class="label label-primary"><?=number_format($primaryPctAll,1)?> %</label> จากเป้าหมายรวมทั้งประเทศ 76,000 ราย
    </h5>
    <div
        class="progress overall-progress"
        onmouseover="$(this).popover('show')"
        onmouseout="$(this).popover('hide')"
        data-toggle="popover"
        data-html="true"
        data-placement="top"
        data-content="<?= number_format($sum['ovall']) .' ('.number_format($primaryPctAll,1) .' %)'?>"
    >
        <div class="progress-bar progress-bar-success"
             role="progressbar"
             aria-valuenow="<?=$primaryPctAll?>"
             aria-valuemin="0"
             aria-valuemax="76000"
             style="width: <?=$primaryPctAll?>%;">
            <?= ''//$sum['ovall'].' / '.'76000'.' ('.(number_format( ($sum['ovall']/76000)*100,1)).'%)' ?>
        </div>
    </div>
    <h5>
        ผลงานในช่วงวันที่ <?=$fromDateShow.' ถึงวันที่ '.$toDateShow?> จำนวน
        <label class="label label-primary"><?=number_format($sum['ov'])?></label>
        ราย คิดเป็น
        <label class="label label-primary"><?=number_format($primaryPct,1)?> %</label>
    </h5>
    <div
        class="progress overall-progress"
        onmouseover="$(this).popover('show')"
        onmouseout="$(this).popover('hide')"
        data-toggle="popover"
        data-html="true"
        data-placement="top"
        data-content="<?= number_format($sum['ov']) .' ('.number_format($primaryPct,1) .' %)'?>"
    >
        <div class="progress-bar progress-bar-success"
             role="progressbar"
             aria-valuenow="<?=$primaryPct?>"
             aria-valuemin="0"
             aria-valuemax="76000"
             style="width: <?=$primaryPct?>%"
        >
            <?= ''//$sum['ov'].' / '.'76000'.' ('.(number_format( ($sum['ov']/76000)*100,1)).'%)' ?>
        </div>
    </div>
    <div id="report84-ov-desc" class="alert alert-warning report84-desc">
        <?php
            $ovPosPct = ($sum['ovpos']/$sum['ovall'])*100;
            echo 'ผลงานตรวจคัดกรองพยาธิใบไม้ตับ รวม '.number_format($sum['ovall']).' ราย ติดเชื้อ '.number_format($sum['ovpos']).' ('.number_format($ovPosPct,1).' %) ราย ';
        ?>
    </div>
    <?php
    global $ovGlobal;
    $ovGlobal = 0;
    echo GridView::widget([
        'columns' => [
            [
                'class'=>'kartik\grid\SerialColumn',
                'hAlign'=>'center',
                'width'=>'80px',
            ],
            [
                'attribute'=>'zone',
                'hAlign'=>'center',
                'width'=>'80px',
                'label'=>'เขต',
                'vAlign'=>'middle',
                'noWrap'=>true
            ],
            [
                'attribute'=>'province',
                'width'=>'150px',
                'label'=>'จังหวัด',
                'vAlign'=>'middle',
                'noWrap'=>true
            ],
            [
                'attribute'=>'amphur',
                'width'=>'150px',
                'label'=>'อำเภอ',
                'vAlign'=>'middle',
                'noWrap'=>true
            ],
            [
                'attribute'=>'tambon',
                'width'=>'150px',
                'label'=>'ตำบล',
                'vAlign'=>'middle',
                'pageSummary'=>'รวม
                    <script>
                        rebindedTambon1 = false;
                        rebindEventTambon1();
                        rebindedOv1 = false;
                        rebindEventOv1();
                    </script>',
                'noWrap'=>true
            ],
            [
                'attribute'=>'progress',
                'format'=>'raw',
                'noWrap'=>false,
                'width'=>'20%',
                'label'=>'ความก้าวหน้า (จากเป้าหมาย 905 ราย)',
                'vAlign'=>'middle',
                'hAlign'=>'center',
                //'width'=>'100px',
                'value'=> function ($model) {
                    //data-original-title="ต.'.$model['tambon'].' อ.'.$model['amphur'].' จ.'.$model['province'].'<br/><b>Progress : '.$pct.'%</b>"
                    //data-content="ตรวจ OV ทั้งหมด <b>'.number_format($model['ov']).'</b> ราย<br>เป้าหมายตำบลละ 905 ราย<br>คิดเป็น <b>'.$pct.' %</b>">'.
                    $pct = number_format(($model['ov']/905)*100,1);
                    $str = '<div
                        onmouseover="$(this).popover(\'show\')"
                        onmouseout="$(this).popover(\'hide\')"
                        data-toggle="popover"
                        data-html="true"
                        data-placement="top"
                        data-content="'.number_format($model['ov']).' ('.$pct.' %)">'.

                        Progress::widget([
                            'percent' => $pct ,
                            'barOptions' => [
                                'class' => 'progress-bar-success',
                                'style'=>'/*min-width: 2em;*/ max-width: 100%; width: '.$pct.'%;'
                            ],
                            //'label' => round( ($model['ov']/905)*100).' %',
                        ]).
                        '</div>';
                    return $str;
                },

            ],
            [
                'attribute'=>'tccbot',
                'label'=>'ประชากร',
                'hAlign'=>'right',
                'vAlign'=>'middle',
                'format'=>'decimal',
                'pageSummary'=>true,
                'noWrap'=>true
//                'value'=>function ($model) {
//                    $tcc = Yii::$app->dbbot->createCommand("select count(*) as ncount from person where sitecode IN (select hcode from buffe_webservice.all_hospital_thai where addresscode='{$model->address}')")->queryOne();
//                    return $tcc['ncount'];
//                }
            ],
            [
                'attribute'=>'nall',
                'format'=>'decimal',
                'hAlign'=>'right',
                'vAlign'=>'middle',
                //'label'=>'ลงทะเบียน',
                'label'=>'เลือกกลุ่มเสี่ยง',
                'pageSummary'=>true,
                'noWrap'=>false
            ],
            [
                'attribute'=>'icf',
                'format'=>'decimal',
                'hAlign'=>'right',
                'vAlign'=>'middle',
                //'label'=>'ลงทะเบียน',
                'label'=>'มีใบยินยอม',
                'pageSummary'=>true,
                'noWrap'=>false
            ],
            [
                'attribute'=>'cca01',
                //'label'=>'เก็บข้อมูลพื้นฐาน',
                'label'=>'ลงทะเบียน',
                'format'=>'decimal',
                'hAlign'=>'right',
                'vAlign'=>'middle',
                'pageSummary'=>true,
                'noWrap'=>true
            ],
            [
                'attribute'=>'ov',
                'format'=>'raw',
                'hAlign'=>'right',
                'vAlign'=>'middle',
                'label'=>'ตรวจ OV',
                'pageSummary'=>function(){

                    return '<div id="div-sum-ov"></div>
                        <script>
                            var divSumOv = 0;
                            $().ready(function(){
                                $("[ov-1]").each(function(){
                                    divSumOv += parseInt($(this).attr("value"));
                                })
                                $("#div-sum-ov").html(divSumOv.toLocaleString());
                                var divSumOvpos = parseInt($("#div-sum-ovpos").html());
                                var divSumOvposPct = (divSumOvpos/divSumOv)*100;
                                $("#div-sum-ovpos").html( (divSumOvpos.toLocaleString())+" ("+divSumOvposPct.toFixed(1)+" %)" );
                            });
                        </script>';
                },
                'noWrap'=>true,
                'value'=>function($model){
                    //return $model['ov'];
                    $str = '<a
                        value="'.$model['ov'].'"
                        href="#"
                        ov-1="'.$model['address'].'"
                        tambon="'.$model['tambon'].'"
                        amphur="'.$model['amphur'].'"
                        province="'.$model['province'].'"
                        class="ov-1"
                        data-html="true"
                        title="แสดงรายละเอียดการตรวจ OV ในตำบล'.trim($model['tambon']).'"
                    >'.
                        number_format($model['ov']).
                        '</a>';
                    return $str;
                }
            ],
            [
                'attribute'=>'ovpos',
                'format'=>'raw',
                'hAlign'=>'right',
                'vAlign'=>'middle',
                'label'=>'ติดเชื้อ OV',
                'pageSummary'=>function($model){

                    return '<div id="div-sum-ovpos">'.$model.'</div>';
                },
                'noWrap'=>true,
                'value'=>function($model){
                    $pct = number_format( $model['ov']==0 ? 0 :($model['ovpos']/$model['ov'])*100 ,1);
//                    $str = '<div style="width: 84px; height: 41px; position: absolute; margin: -30px 0 0 -8px;"
//                        onmouseover="$(this).popover(\'show\')"
//                        onmouseout="$(this).popover(\'hide\')"
//                        data-toggle="popover"
//                        data-html="true"
//                        data-placement="top"
//                        data-content="'.number_format($model['ovpos']).' ('.$pct.' %)">'.
//
//                        '</div>';
                    return number_format($model['ovpos']).' ('.$pct.' %)';//$model['ovpos'].$str;
                }
            ],
            [
                'attribute'=>'ov02',
                'format'=>'decimal',
                'hAlign'=>'right',
                'vAlign'=>'middle',
                'label'=>'ให้สุขศึกษา',
                'pageSummary'=>true,
                'noWrap'=>0
            ],
            [
                'attribute'=>'ov03',
                'format'=>'decimal',
                'hAlign'=>'right',
                'vAlign'=>'middle',
                'label'=>'ให้การรักษา',
                'pageSummary'=>true,
                'noWrap'=>0
            ],

        ],
        'pjax'=>true,
        'pjaxSettings' => [
            'options' => [
                'enablePushState'=>false
            ]
        ],
        'dataProvider'=>$primaryDataProvider,
        'options' => [
            'id'=>'report84-dynagrid-1'
        ],
        'showPageSummary'=>true,
        'responsive'=>true,
        'responsiveWrap'=>false,
    ]);

    ?>
    <div class="alert alert-info" role="alert"><h4>ส่วนที่ 2 : รายงาน Secondary prevention (คัดกรองมะเร็งท่อน้ำดีด้วยการตรวจอัลตราซาวด์)</h4></div>
    <h5>
        ผลงานตรวจอัลตร้าซาวด์
        <label class="label label-primary"><?=number_format($sum['cca02all'])?></label>
        ราย คิดเป็น
        <label class="label label-primary"><?=number_format($secondaryPctAll,1)?> %</label> จากเป้าหมายรวมทั้งประเทศ 135,000 ราย
    </h5>
    <div
         class="progress overall-progress"
         onmouseover="$(this).popover('show')"
         onmouseout="$(this).popover('hide')"
         data-toggle="popover"
         data-html="true"
         data-placement="top"
         data-content="<?= number_format($sum['cca02all']) .' ('.number_format($secondaryPctAll,1) .' %)'?>"
    >
        <div class="progress-bar progress-bar-success"
             role="progressbar"
             aria-valuenow="<?=$secondaryPctAll?>"
             aria-valuemin="0"
             aria-valuemax="135000"
             style="width: <?=$secondaryPctAll?>%"
        >
            <?= ''//$sum['cca02all'].' / '.'84000'.' ('.(number_format( ($sum['cca02all']/84000)*100,1)).'%)' ?>
        </div>
    </div>

    <h5>
        ผลงานในช่วงวันที่ <?=$fromDateShow.' ถึงวันที่ '.$toDateShow?> จำนวน
        <label class="label label-primary"><?=number_format($sum['cca02'])?></label>
        ราย คิดเป็น
        <label class="label label-primary"><?=number_format($secondaryPct,1)?> %</label>
    </h5>
    <div
        class="progress overall-progress"
        onmouseover="$(this).popover('show')"
        onmouseout="$(this).popover('hide')"
        data-toggle="popover"
        data-html="true"
        data-placement="top"
        data-content="<?= number_format($sum['cca02']) .' ('.number_format($secondaryPct,1) .' %)'?>"
    >
        <div class="progress-bar progress-bar-success"
             role="progressbar"
             aria-valuenow="<?=$secondaryPct?>"
             aria-valuemin="0"
             aria-valuemax="135000"
             style="width: <?=$secondaryPct?>%"
        >
            <?= ''//$sum['cca02'].' / '.'84000'.' ('.(number_format( ($sum['cca02']/84000)*100,1)).'%)' ?>
        </div>
    </div>
    <div id="report84-us-desc" class="alert alert-warning report84-desc">
        <?php
        $abnormalPct = ($usoverall['abnormal']/$usoverall['cca02'])*100;
        $suspectedPct = ($usoverall['suspected']/$usoverall['cca02'])*100;
        $ctmriPct = ($usoverall['ctmri']/$usoverall['cca02'])*100;
        $ccaCtmriPct = ($usoverall['cca']/$usoverall['ctmri'])*100;
        $ccaUsPct = ($usoverall['cca']/$usoverall['cca02'])*100;
        //print_r($usoverall);

        echo 'ผลงานตรวจคัดกรองมะเร็งท่อน้ำดี รวม '.number_format($usoverall['cca02']).' ราย ผิดปกติจำนวน '.number_format($usoverall['abnormal']).' ('.number_format($abnormalPct,1).' %) ราย สงสัย CCA จำนวน '.number_format($usoverall['suspected']).' ('.number_format($suspectedPct,1).' %) ราย<br/>เข้ารับ CT/MRI จำนวน '.number_format($usoverall['ctmri']).' ('.number_format($ctmriPct,1).' %) ราย ผลยืนยันเป็น CCA จำนวน '.number_format($usoverall['cca']).' ราย คิดเป็น '.number_format($ccaCtmriPct,1).' % ของผู้มาตรวจ CT/MRI และคิดเป็น '.number_format($ccaUsPct,1).' % ของผู้มาตรวจอัลตร้าซาวด์';
        ?>
    </div>

    <?php
	global $ccaGlobal,$cca02Global;
	$ccaGlobal = $cca02Global = 0;
    echo GridView::widget([
        'columns' => [

            [
                'class'=>'kartik\grid\SerialColumn',
                'hAlign'=>'center',
                'width'=>'80px',
            ],
            [
                'attribute'=>'zone_2',
                'hAlign'=>'center',
                'vAlign'=>'middle',
                'width'=>'80px',
                'label'=>'เขต',
            ],
            [
                'attribute'=>'province_2',
                'label'=>'จังหวัด',
                'vAlign'=>'middle',
                'pageSummary'=>'รวม
                    <script>
                        rebindedProvince2 = false;
                        rebindEventProvince2();
                    </script>',
                'format'=>'raw',
                'value'=>function($model){
                    //return $model['province_2'];
                    $str = '<a
                        href="#"
                        province-2="'.$model['provincecode'].'"
                        class="province-2"
                        data-html="true"
                        title="แสดงรายละเอียดการตรวจอัลตร้าซาวด์ในจังหวัด'.trim($model['province_2']).'"
                    >'.
                        trim($model['province_2']).
                    '</a>';
                    return $str;
                }
            ],
            [
                'attribute'=>'progress',
                'label'=>'ความก้าวหน้า (จากเป้าหมาย 5,000 ราย)',
                'vAlign'=>'middle',
                'width'=>'10%',
                'value'=> function ($model) {
                    $pct = number_format(($model['cca02']/5000)*100,1);

                    //data-original-title="จังหวัด'.$model['province_2'].'<br><b>Progress : '.$pct.'%</b>"
                    //data-content="ตรวจอัลตร้าซาวด์ทั้งหมด <b>'.number_format($model['cca02']).'</b> ราย<br>เป้าหมายจังหวัดละ 5,000 ราย<br>คิดเป็น <b>'.$pct.' %</b>">'.
                    $str = '<div
                        onmouseover="$(this).popover(\'show\')"
                        onmouseout="$(this).popover(\'hide\')"
                        data-toggle="popover"
                        data-html="true"
                        data-placement="top"
                        data-content="'.number_format($model['cca02']).' ('.$pct.' %)">'.

                        Progress::widget([
                            'percent' => $pct ,
                            'barOptions' => [
                                'class' => 'progress-bar-success',
                                'style'=>'/*min-width: 2em;*/ max-width: 100%; width: '.$pct.'%;'
                            ],
                            //'label' => round( ($model['cca02']/5000)*100).'%'
                        ]).
                        '</div>';
                    return $str;
                },
                'format'=>'raw','hAlign'=>'center','vAlign'=>'middle'
            ],
            [
                'attribute'=>'nall_2',
                'format'=>'decimal',
                'hAlign'=>'right',
                'vAlign'=>'middle',
                'label'=>'ลงทะเบียน',
                'pageSummary'=>true
            ],
            [
                'attribute'=>'cca02',
                'format'=>'decimal',
                'hAlign'=>'right',
                'vAlign'=>'middle',
                'label'=>'อัลตร้าซาวด์',
                'pageSummary'=>function($model){
                    global $cca02Global;
                    $cca02Global = $model;
                    return number_format($cca02Global);
                },
            ],
            [
                'attribute'=>'abnormal',
                'format'=>'raw',
                'hAlign'=>'right',
                'vAlign'=>'middle',
                'label'=>'ผิดปกติ',
                'pageSummary'=>function($model){
                    return number_format($model);
                },
                'value'=>function($model){
                    $pct = number_format( $model['cca02']==0 ? 0 :($model['abnormal']/$model['cca02'])*100 ,1);
                    $str = '<div style="height: 41px; width: 82px; position: absolute; margin: -30px 0px 0px -8px;"
                        onmouseover="$(this).popover(\'show\')"
                        onmouseout="$(this).popover(\'hide\')"
                        data-toggle="popover"
                        data-html="true"
                        data-placement="top"
                        data-content="'.number_format($model['abnormal']).' ('.$pct.' %)">'.
                    '</div>';
                    return $model['abnormal'].$str;
                }
            ],
            [
                'attribute'=>'suspected',
                'format'=>'raw',
                'hAlign'=>'right',
                'vAlign'=>'middle',
                'label'=>'สงสัย CCA',
                'pageSummary'=>function($model){
                    global $cca02Global;
                    return number_format($model).' ('.number_format(($model/$cca02Global)*100,1).' %)';
                },
                'width'=>'130px',
                'value'=>function($model){
                    $pct = number_format( $model['cca02']==0 ? 0 :($model['suspected']/$model['cca02'])*100 ,1);
                    return number_format($model['suspected']).' ('.$pct.'%)';

                    $str = '<div
                        onmouseover="$(this).popover(\'show\')"
                        onmouseout="$(this).popover(\'hide\')"
                        data-toggle="popover"
                        data-html="true"
                        data-placement="top"
                        data-content="'.number_format($model['suspected']).' ('.$pct.' %)">'.
                        number_format( $model['suspected']).
                        '</div>';
                    echo $str;
                    return $model['suspected'];
                }
            ],
            [
                'attribute'=>'ctmri',
                'format'=>'decimal',
                'hAlign'=>'right',
                'vAlign'=>'middle',
                'label'=>'CT / MRI',
                'pageSummary'=>true
            ],
            [
                'attribute'=>'cca',
                'format'=>'decimal',
                'hAlign'=>'right',
                'vAlign'=>'middle',
                'label'=>'พบเป็นมะเร็ง',
                'pageSummary'=>function($model){
                    global $ccaGlobal;
                    $ccaGlobal = $model;
                    return number_format($ccaGlobal);
				}
            ],
            [
                'attribute'=>'treated',
                'format'=>'raw',
                'hAlign'=>'right',
                'vAlign'=>'middle',
                'label'=>'ได้รับการรักษา',
				'pageSummary'=>function($model){
                    global $ccaGlobal;
                    return number_format($model).' ('.number_format($model/$ccaGlobal*100,1).'%)';
                },
				'value'=>function($model){
                    $pct = number_format( $model['cca']==0 ? 0 :($model['treated']/$model['cca'])*100 ,1);
                    return number_format($model['treated']).' ('.$pct.'%)';
				}
            ]

        ],
        'options' => [
            'id'=>'report84-dynagrid-2',
        ],
        'pjax'=>true,
        'pjaxSettings' => [
            'options' => [
                'enablePushState'=>false
            ]
        ],
        'dataProvider'=>$secondaryDataProvider,
        'showPageSummary'=>true,
        'responsive'=>true,
        'responsiveWrap'=>false,

    ]);

    $tertiaryPctAll = ($sum['treatmentall']['all_surgery']/600)*100;
    $tertiaryPct = ($sum['treatmentrid']['all_surgery']/600)*100;

?>
<div class="alert alert-info" role="alert"><h4>ส่วนที่ 3 : รายงาน Tertiary care (รักษามะเร็งท่อน้ำดีด้วยการผ่าตัด)</h4></div>
<h5 title="เป้าหมายผ่าตัดรวมทั้งประเทศ 600 รายนับเฉพาะ Liver resection, Hilar resection, Bypass, Exploratory laparotomy ± biopsy และ Whipple’s operation">
    ผลงานผ่าตัด
    <label class="label label-primary"><?=number_format($sum['treatmentall']['all_surgery'])?></label>
    ราย คิดเป็น
    <label class="label label-primary"><?=number_format($tertiaryPctAll,1)?> %</label> จากเป้าหมายรวมทั้งประเทศ 600 ราย
</h5>
<div
    class="progress overall-progress"
    onmouseover="$(this).popover('show')"
    onmouseout="$(this).popover('hide')"
    data-toggle="popover"
    data-html="true"
    data-placement="top"
    data-content="<?= number_format($sum['treatmentall']['all_surgery']) .' ('.number_format($tertiaryPctAll,1) .' %)'?>"
>
    <div class="progress-bar progress-bar-success"
         role="progressbar"
         aria-valuenow="<?=$tertiaryPctAll?>"
         aria-valuemin="0"
         aria-valuemax="600"
         style="width: <?=$tertiaryPctAll?>%"
    >
        <?= ''//($sum['sugall']+0).' / '.'600'.' ('.(number_format( ($sum['sugall']/600)*100,1)).'%)' ?>
    </div>
</div>

<h5 title="เป้าหมายผ่าตัดรวมทั้งประเทศ 600 รายนับเฉพาะ Liver resection, Hilar resection, Bypass, Exploratory laparotomy ± biopsy และ Whipple’s operation">
    ผลงานในช่วงวันที่ <?=$fromDateShow.' ถึงวันที่ '.$toDateShow?> จำนวน
    <label class="label label-primary"><?=number_format($sum['treatmentrid']['all_surgery'])?></label>
    ราย คิดเป็น
    <label class="label label-primary"><?=number_format($tertiaryPct,1)?> %</label>
</h5>
<div
    class="progress overall-progress"
    onmouseover="$(this).popover('show')"
    onmouseout="$(this).popover('hide')"
    data-toggle="popover"
    data-html="true"
    data-placement="top"
    data-content="<?= number_format($sum['treatmentrid']['all_surgery']) .' ('.number_format($tertiaryPct,1) .' %)'?>"
>
    <div class="progress-bar progress-bar-success"
         role="progressbar"
         aria-valuenow="<?=$tertiaryPct?>"
         aria-valuemin="0"
         aria-valuemax="600"
         style="width: <?=$tertiaryPct?>%"
    >
        <?= ''//($sum['sug']+0).' / '.'600'.' ('.(number_format( ($sum['sug']/600)*100,1)).'%)' ?>
    </div>
</div>
<div id="report84-cca-desc" class="alert alert-warning report84-desc" style="">
    <?php
        $paliativeTreatment = $sum['treatmentall']['treatment'] - $sum['treatmentall']['c_surgery'];
        echo 'ผลงานการรักษาทั้งหมด '.number_format($sum['treatmentall']['treatment']).' ราย จำแนกเป็นผ่าตัด '.number_format($sum['treatmentall']['all_surgery']).' ราย คิดเป็น '.number_format(($sum['treatmentall']['all_surgery']/$sum['treatmentall']['treatment'])*100,1).' % ของทั้งหมด และคิดเป็น '.number_format(($sum['treatmentall']['all_surgery']/600)*100,1).' % จากเป้าหมายผ่าตัด 600 คน ในจำนวนที่ผ่าตัดนี้ เป็นผ่าตัดให้หายขาด '.number_format($sum['treatmentall']['c_surgery']).' ราย และผ่าตัดเพื่อการประคับประคอง '.number_format($sum['treatmentall']['p_surgery']).' ราย สำหรับการรักษาแบบประคับประคองทั้งสิ้น '.number_format(intval($paliativeTreatment)).' ราย คิดเป็น '.number_format(($paliativeTreatment/$sum['treatmentall']['treatment'])*100,1).' % ของผู้ป่วยทั้งหมด ทั้งนี้ ได้รวมการผ่าตัดเพื่อการประคับประคองไว้ในจำนวนนี้ด้วยแล้ว';
        //แก้ด้วย ตาม miester task
    ?>
</div>
    <?php

    echo GridView::widget([
        'columns' => [
            [
                'hAlign'=>'left',
                'vAlign'=>'middle',
                'value'=>function(){
                    return '#';
                }
            ],
            [
                'class'=>'kartik\grid\SerialColumn',
                'hAlign'=>'center',
                'width'=>'80px'
            ],
//            [
//                'hAlign'=>'left',
//                'vAlign'=>'middle',
//                'value'=>function(){
//                    return 'รหัสสถานบริการ';
//                }
//            ],
//            [
//                'attribute'=>'sitecode',
//                'label'=>'รหัสสถานบริการ',
//                'hAlign'=>'center',
//                'width'=>'80px',
//                'noWrap'=>true
//            ],
            [
                'hAlign'=>'left',
                'vAlign'=>'middle',
                'value'=>function(){
                    return 'ชื่อโรงพยาบาล';
                }
            ],

            [
                'attribute'=>'hospital',
                'label'=>'ชื่อโรงพยาบาล',
                'noWrap'=>true,
                'vAlign'=>'middle',
                'pageSummary'=>'รวม
                    <script>
                        rebindedHospital3 = false;
                        rebindEventHospital3();
                    </script>',
                'format'=>'raw',
                'value'=>function($model){
                    $str = '<a
                        href="#"
                        hospital-3="'.$model['sitecode'].'"
                        class="hospital-3"
                        data-html="true"
                        title="แสดงรายละเอียดการรักษา ณ '.trim($model['hospital']).'"
                    >'.
                        trim($model['hospital']).
                        '</a>';
                    return $str;
                }
            ],
            [
                
                'hAlign'=>'left',
                'vAlign'=>'middle',
                'pageSummary'=>'รวมลงทะเบียน',
                'value'=>function() {
                    return 'ลงทะเบียน';
                }
            ],
            [
                'attribute'=>'nall_3',
                'label'=>'ลงทะเบียน',
                'format'=>'decimal',
                'hAlign'=>'right',
                'vAlign'=>'middle',
                'pageSummary'=>true,
                'noWrap'=>true
            ],
            [
                'hAlign'=>'left',
                'vAlign'=>'middle',
                'pageSummary'=>'รวมรักษา',
                'value'=>function(){
                    return 'รักษารวม';
                }
            ],
            [
                'attribute'=>'treatment',
                'label'=>'รักษารวม',
                'format'=>'decimal',
                'hAlign'=>'right',
                'vAlign'=>'middle',
                'pageSummary'=>true,
                'noWrap'=>true
            ],


            [
                'hAlign'=>'left',
                'vAlign'=>'middle',
                'pageSummary'=>'ผ่าตัดรวม',
                'value'=>function(){
                    return 'รวมผ่าตัดให้หายขาด';
                }
            ],
            [
                'attribute'=>'all_surgery',
                'label'=>'รวมผ่าตัด',
                'format'=>'decimal',
                'hAlign'=>'right',
                'vAlign'=>'middle',
                'pageSummary'=>true,
                'noWrap'=>true

            ],



            [
                'hAlign'=>'left',
                'vAlign'=>'middle',
                'pageSummary'=>'รวมผ่าตัดให้หายขาด',
                'value'=>function(){
                    return 'ผ่าตัดให้หายขาด';
                }
            ],
            [
                'attribute'=>'c_surgery',
                'label'=>'ผ่าตัดให้หายขาด',
                'format'=>'decimal',
                'hAlign'=>'right',
                'vAlign'=>'middle',
                'pageSummary'=>true,
                'noWrap'=>true

            ],
            [
                'hAlign'=>'left',
                'vAlign'=>'middle',
                'pageSummary'=>'รวมผ่าตัดเพื่อการประคับประคอง',
                'value'=>function(){
                    return 'ผ่าตัดเพื่อการประคับประคอง';
                }
            ],
            [
                'attribute'=>'p_surgery',
                'label'=>'ผ่าตัดเพื่อการประคับประคอง',
                'format'=>'decimal',
                'hAlign'=>'right',
                'vAlign'=>'middle',
                'pageSummary'=>true,
                'noWrap'=>true

            ],
            [

                'hAlign'=>'left',
                'vAlign'=>'middle',
                'pageSummary'=>'รวม Liver resection',
                'value'=>function(){
                    return 'Liver resection';
                }
            ],
            [
                'attribute'=>'liver',
                'label'=>'Liver resection',
                'format'=>'decimal',
                'hAlign'=>'right',
                'vAlign'=>'middle',
                'pageSummary'=>true,
                'noWrap'=>true
            ],



            [

                'hAlign'=>'left',
                'vAlign'=>'middle',
                'pageSummary'=>'รวม Liver + Hilar',
                'value'=>function(){
                    return 'Liver + Hilar';
                }
            ],
            [
                'attribute'=>'liverhilar',
                'label'=>'Liver + Hilar',
                'format'=>'decimal',
                'hAlign'=>'right',
                'vAlign'=>'middle',
                'pageSummary'=>true,
                'noWrap'=>true
            ],



            [

                'hAlign'=>'left',
                'vAlign'=>'middle',
                'pageSummary'=>'รวม Whipple’s operation',
                'value'=>function(){
                    return 'Whipple’s operation';
                }
            ],
            [
                'attribute'=>'whipple',
                'label'=>'Whipple’s operation',
                'format'=>'decimal',
                'hAlign'=>'right',
                'vAlign'=>'middle',
                'pageSummary'=>true,
                'noWrap'=>true
            ],
            [

                'hAlign'=>'left',
                'vAlign'=>'middle',
                'pageSummary'=>'รวม Hilar resection',
                'value'=>function(){
                    return 'Hilar resection';
                }
            ],
            [
                'attribute'=>'hilar',
                'label'=>'Hilar resection',
                'format'=>'decimal',
                'hAlign'=>'right',
                'vAlign'=>'middle',
                'pageSummary'=>true,
                'noWrap'=>true
            ],
            [

                'hAlign'=>'left',
                'vAlign'=>'middle',
                'pageSummary'=>'รวม Bypass',
                'value'=>function(){
                    return 'Bypass';
                }
            ],
            [
                'attribute'=>'bypass',
                'label'=>'Bypass',
                'format'=>'decimal',
                'hAlign'=>'right',
                'vAlign'=>'middle',
                'pageSummary'=>true,
                'noWrap'=>true
            ],
            [

                'hAlign'=>'left',
                'vAlign'=>'middle',
                'pageSummary'=>'รวม Biopsy (EL+Bx)',
                'value'=>function(){
                    return 'Biopsy (EL+Bx)';
                }
            ],
            [
                //'attribute'=>'biopsy',
                'attribute'=>'elbx',
                'label'=>'Biopsy (EL+Bx)',
                'format'=>'decimal',
                'hAlign'=>'right',
                'vAlign'=>'middle',
                'pageSummary'=>true,
                'noWrap'=>true
            ],
            [

                'hAlign'=>'left',
                'vAlign'=>'middle',
                'pageSummary'=>'รวม Needle Biopsy',
                'value'=>function(){
                    return 'Needle Biopsy';
                }
            ],
            [
                //'attribute'=>'biopsy',
                'attribute'=>'ndbx',
                'label'=>'Needle Biopsy',
                'format'=>'decimal',
                'hAlign'=>'right',
                'vAlign'=>'middle',
                'pageSummary'=>true,
                'noWrap'=>true
            ],
            [

                'hAlign'=>'left',
                'vAlign'=>'middle',
                'pageSummary'=>'รวม Chemotherapy (All)',
                'value'=>function(){
                    return 'Chemotherapy (All)';
                }
            ],
            [
                'attribute'=>'chemo_all',
                'label'=>'Chemotherapy (All)',
                'format'=>'decimal',
                'hAlign'=>'right',
                'vAlign'=>'middle',
                'pageSummary'=>true,
                'noWrap'=>true
            ],
            [

                'hAlign'=>'left',
                'vAlign'=>'middle',
                'pageSummary'=>'รวม Chemotherapy (Adjuvant)',
                'value'=>function(){
                    return 'Chemotherapy (Adjuvant)';
                }
            ],
            [
                'attribute'=>'chemo_adjuvant',
                'label'=>'Chemotherapy (Adjuvant)',
                'format'=>'decimal',
                'hAlign'=>'right',
                'vAlign'=>'middle',
                'pageSummary'=>true,
                'noWrap'=>true
            ],
            [

                'hAlign'=>'left',
                'vAlign'=>'middle',
                'pageSummary'=>'รวม PTBD',
                'value'=>function(){
                    return 'PTBD';
                }
            ],
            [
                'attribute'=>'ptbd',
                'label'=>'PTBD',
                'format'=>'decimal',
                'hAlign'=>'right',
                'vAlign'=>'middle',
                'pageSummary'=>true,
                'noWrap'=>true
            ],
            [

                'hAlign'=>'left',
                'vAlign'=>'middle',
                'pageSummary'=>'รวม Endoscopic Stent',
                'value'=>function(){
                    return 'Endoscopic Stent';
                }
            ],
            [
                'attribute'=>'endoscopic',
                'label'=>'Endoscopic Stent',
                'format'=>'decimal',
                'hAlign'=>'right',
                'vAlign'=>'middle',
                'pageSummary'=>true,
                'noWrap'=>true
            ],
            [

                'hAlign'=>'left',
                'vAlign'=>'middle',
                'pageSummary'=>'รวม Medication Treatment',
                'value'=>function(){
                    return 'Medication Treatment';
                }
            ],
            [
                'attribute'=>'medication',
                'label'=>'Medication Treatment',
                'format'=>'decimal',
                'hAlign'=>'right',
                'vAlign'=>'middle',
                'pageSummary'=>true,
                'noWrap'=>true
            ],
        ],
        'options' => [
            'id'=>'report84-dynagrid-3'
        ],
        'pjax'=>true,
        'pjaxSettings' => [
            'options' => [
                'enablePushState'=>false
            ]
        ],

        'dataProvider'=>$tertiaryDataProvider,
        'showPageSummary'=>true,
        'responsive'=>true,
        'responsiveWrap'=>false,
    ]);
    ?>

    <div id="report84-excel" class="report84-bottom">
        <a id="report84-excel-ov" class="btn btn-success" title="นำออกรายงาน &#10;ส่วนที่ 1 Primary prevention &#10;ในรูปแบบ Excel"><i class="fa fa-file-excel-o fa-2x"></i> OV</a>
        <a id="report84-excel-us" class="btn btn-success" title="นำออกรายงาน ส่วนที่ 2 Secondary prevention ในรูปแบบ Excel"><i class="fa fa-file-excel-o fa-2x"></i> US</a>
        <a id="report84-excel-cca" class="btn btn-success" title="นำออกรายงาน ส่วนที่ 3 Tertiary care ในรูปแบบ Excel"><i class="fa fa-file-excel-o fa-2x"></i> CCA</a>
    </div>
    <div id="report84-stat" class="report84-bottom">
        <?php

            //\yii\helpers\VarDumper::dump($_SERVER,10,true);


            if(isset(Yii::$app->user->id)){
                //echo ' AND : '.Yii::$app->user->id;

                $sqlGetViewsUser = 'SELECT ( session_name IS NOT NULL AND session_date = DATE(NOW()) ) AS is_present
                        FROM project84_report_views_user
                        WHERE session_name LIKE "'.trim(Yii::$app->user->id).'"';
                $queryViewsUser = Yii::$app->db->createCommand($sqlGetViewsUser)->queryOne();

                if(!$queryViewsUser['is_present']){
                    $sqlUpdateViewsUser = 'INSERT INTO project84_report_views_user
                        VALUES("'.trim(Yii::$app->user->id).'",NOW(),1)
                        ON DUPLICATE KEY UPDATE session_date = NOW(), session_views = session_views + 1';
                    Yii::$app->db->createCommand($sqlUpdateViewsUser)->execute();
                }


            }else{
                if(isset($_COOKIE['PHPSESSID'])){

                    $sqlGetViewsPhpSession = 'SELECT ( session_name IS NOT NULL AND session_date = DATE(NOW()) ) AS is_present
                    FROM project84_report_views_user
                    WHERE session_name LIKE "'.trim($_COOKIE['PHPSESSID']).'"';
                    $queryViewsPhpSession = Yii::$app->db->createCommand($sqlGetViewsPhpSession)->queryOne();

                    if(!$queryViewsPhpSession['is_present']){
                        $sqlUpdateViewsPhpSession = 'INSERT INTO project84_report_views_user
                        VALUES("'.trim($_COOKIE['PHPSESSID']).'",NOW(),1)
                        ON DUPLICATE KEY UPDATE session_date = NOW(), session_views = session_views + 1';
                        Yii::$app->db->createCommand($sqlUpdateViewsPhpSession)->execute();
                    }
                }
            }

            $sqlGetSessionViewsCount = 'SELECT SUM(session_views) AS session_views FROM project84_report_views_user LIMIT 1';
            $SessionViewsCount = Yii::$app->db->createCommand($sqlGetSessionViewsCount)->queryOne();

            Yii::$app->db->createCommand('UPDATE project84_report_views SET views = (views+1)')->execute();
            $viewCount = Yii::$app->db->createCommand('SELECT views FROM project84_report_views')->queryOne();


        ?>
        <i class="fa fa-user fa-2x text-info" title="จำนวนผู้เข้าดูรายงานนี้"> <?= number_format(intval($SessionViewsCount['session_views'])); ?></i>&emsp;
        <i class="fa fa-eye fa-2x text-info" title="จำนวนครั้งของการเข้าดูรายงานนี้"> <?= number_format(intval($viewCount['views'])); ?></i>
    </div>

    <?php

    //exit();
    $this->registerJs('
        /*
        $("div[data-toggle=\'popover\']").mouseover(function(){
            $(this).popover("show");
        });

        $("div[data-toggle=\'popover\']").mouseout(function(){
            $(this).popover("hide");
        });
        */
    ');

    $this->registerJs('
        $("#report84-excel-ov").click(function(e){
            var dataTable = $("#report84-dynagrid-1 table").clone();
            dataTable.removeAttr("class").removeAttr("id").removeAttr("style").removeAttr("border");
            dataTable.find("*").removeAttr("class").removeAttr("id").removeAttr("style");
            var excelString = dataTable.prop("outerHTML");
            excelString = encodeURIComponent(excelString);

            this.href = "data:application/vnd.ms-excel,"+excelString;
            this.download = "report84-primary-prevention.xls";
            return true;
        });

        $("#report84-excel-us").click(function(e){
            var dataTable = $("#report84-dynagrid-2 table").clone();

            var descLine = $("<tr><td colspan=\'10\'>"+$("#report84-us-desc").html()+"</td></tr>");
            dataTable.children("thead").prepend(descLine);


            dataTable.removeAttr("class").removeAttr("id").removeAttr("style").removeAttr("border");
            dataTable.find("*").removeAttr("class").removeAttr("id").removeAttr("style");
            var excelString = dataTable.prop("outerHTML");
            excelString = encodeURIComponent(excelString);

            this.href = "data:application/vnd.ms-excel,"+excelString;
            this.download = "report84-secondary-prevention.xls";
            return true;
        });

        $("#report84-excel-cca").click(function(e){
            var dataTable = $("#report84-dynagrid-3 table").clone();

            var descLine = $("<tr><td colspan=\'14\'>"+$("#report84-cca-desc").html()+"</td></tr>");
            dataTable.children("thead").prepend(descLine);

            dataTable.removeAttr("class").removeAttr("id").removeAttr("style").removeAttr("border");
            dataTable.find("*")
                .removeAttr("class")
                .removeAttr("id")
                .removeAttr("style")
                /*.removeAttr("href")
                .removeAttr("data-sort")
                .removeAttr("data-key")
                .removeAttr("href")
                .removeAttr("href")*/;
            var excelString = dataTable.prop("outerHTML");
            console.log(excelString);
            excelString = encodeURIComponent(excelString);

            this.href = "data:application/vnd.ms-excel,"+excelString;
            this.download = "report84-tertiary-care.xls";
            return true;
        });
    ');

    $this->registerCss('
        /*
        div.popover h3.popover-title{
            color: #fff;
            background-color: #00a65a;
        }
        */
        .progress.overall-progress{
            height:30px;
        }
        .report84-bottom{
            margin-top: 20px;
            text-align: center;
        }
        #report84-excel > a{
            color: #4cae4c;
            background-color: #FFF;
        }
        #report84-excel > a:hover{
            color: #fff;
            background-color: #449d44;
        }
        .report84-desc{
            max-height: 230px;
            text-align: justify;
            padding-top: 4px;
            color: #000 !important;
            background-color: #FFFF00 !important;
        }
        #report84-modal-w{
            width : 90%;
        }
        #report84-modal-body{
            text-align : center;
        }
        .close{
            color:#F00 !important;
        }
    ');

    //modal
    $this->registerCss('
        #report84-modal-footer,#report84-modal-header{
            /*display: none;*/
        }
    ');

    $this->registerCss('
        #report84-dynagrid-3 tr > td:nth-child(odd),
        #report84-dynagrid-3 tr > th:nth-child(odd){
            display: none;
            color: #3c8dbc;
            font-weight: bold;
        }

        @media screen and (max-width: 767px){
            #report84-modal-w{
                width : 100% !important;
            }
            #report84-dynagrid-3 thead{
                display: none;
            }
            #report84-dynagrid-3 td{
                display: inline-flex;
                white-space: normal;
                width: 39.8% !important;
            }
            #report84-dynagrid-3 tr > td{
                display: inline-block;
            }
            #report84-dynagrid-3 tr > td:nth-child(odd){
                display: inline-block;
                width: 60% !important;
                white-space: normal;
            }
            #report84-dynagrid-3 tr > td.kv-align-right{
                text-align: right !important;
            }
            #report84-dynagrid-3 tr > td.kv-align-center{
                text-align: left !important;
            }
            #report84-dynagrid-3 tbody tr:nth-child(odd)>td:nth-child(odd){
                color: #72afd2;
            }

            #report84-dynagrid-3 tfoot td:nth-child(1),
            #report84-dynagrid-3 tfoot td:nth-child(2),
            #report84-dynagrid-3 tfoot td:nth-child(3),
            #report84-dynagrid-3 tfoot td:nth-child(4){
                display: none;
            }
            #report84-dynagrid-3 tfoot td:nth-child(even){
                text-align: right !important;
            }
            #report84-dynagrid-3 tfoot td{
                height: 57px;
            }
        }
    ');

    $this->registerJs('
        rebindEventTambon1();
        rebindEventProvince2();
        rebindEventHospital3();
    ');

    $this->registerJs('
        var rebindedTambon1 = false;


        function rebindEventTambon1(){
            if(rebindedTambon1) return;
            rebindedTambon1 = true;

            var ol = $("#report84-dynagrid-1 tr>th:nth-child(5)").offset().left- $("#report84-dynagrid-1-container").offset().left;

            $("#report84-dynagrid-1-container").scroll(function(){
                var sc = $("#report84-dynagrid-1-container").scrollLeft();

                if(sc>ol){
                    var l = sc-ol;
                    $("#report84-dynagrid-1 tr>th:nth-child(5)").css({"position":"relative","left":l,"background-color":"#FFF"});
                    $("#report84-dynagrid-1 tr>td:nth-child(5)").css({"position":"relative","left":l,"border-right":"1px solid #F4F4F4"});
                    $("#report84-dynagrid-1 tr:nth-child(even)>td:nth-child(5)").css({"background-color":"#FFFFFF"});
                    $("#report84-dynagrid-1 tr:nth-child(odd)>td:nth-child(5)").css({"background-color":"#F9F9F9"});
                }else{
                    $("#report84-dynagrid-1 tr>th:nth-child(5)").css({"position":"static","left":"auto"});
                    $("#report84-dynagrid-1 tr>td:nth-child(5)").css({"position":"static","left":"auto"});
                }
            })
            /*
            $(".province-2").off().click(function(e){
                
                e.preventDefault();
                var provinceCode = $(this).attr("province-2");
                console.log(provinceCode);
                var provinceName = $(this).html();
                //$("#report84-modal-body").html(response);
                $("#report84-modal-body").html('.$loadIconData.');
                $("#report84-modal-header").html("รายละเอียดการทำอัลตร้าซาวด์ จังหวัด"+provinceName);
                $("#report84-modal-body").focus();

                $.ajax({
                    type    : "GET",
                    cache   : false,
                    url     : "'.Url::to('/report/report84-detail-province/').'",
                    data    : {
                        report  :   2,
                        provincecode    :   provinceCode
                    },provinceCode
                    success  : function(response) {
                        //console.log(response);
                        $("#report84-modal-body").html(response);
                    },
                    error : function(){
                        $("#report84-modal-body").html("การเรียกดูข้อมูลผิดพลาด");
                    }
                });

                $("#report84-modal-trig").click();
                //console.log()
            });
            */
        }

        var rebindedOv1 = false;


        function rebindEventOv1(){
            $(".ov-1").off().click(function(e){
                e.preventDefault();
                var tambonCode = $(this).attr("ov-1");
                var tambonName = $(this).attr("tambon");
                var amphurName = $(this).attr("amphur");
                var provinceName = $(this).attr("province");

                $("#report84-modal-body").html('.$loadIconData.');
                $("#report84-modal-header").html("รายละเอียดการตรวจ OV ต."+tambonName+" อ."+amphurName+" จ."+provinceName);
                $("#report84-modal-body").focus();

                $.ajax({
                    type    : "GET",
                    cache   : false,
                    url     : "'.Url::to('/report/report84-detail-ov/').'",
                    data    : {
                        report  :   1,
                        tamboncode    :   tambonCode
                    },
                    success  : function(response) {
                        //console.log(response);
                        $("#report84-modal-body").html(response);
                    },
                    error : function(){
                        $("#report84-modal-body").html("การเรียกดูข้อมูลผิดพลาด");
                    }
                });

                $("#report84-modal-trig").click();
                //console.log()
            });
        }

        var rebindedProvince2 = false;


        function rebindEventProvince2(){
            if(rebindedProvince2) return;
            rebindedProvince2 = true;

            var ol = $("#report84-dynagrid-2 tr>th:nth-child(3)").offset().left- $("#report84-dynagrid-2-container").offset().left;

            $("#report84-dynagrid-2-container").scroll(function(){
                var sc = $("#report84-dynagrid-2-container").scrollLeft();
                if(sc>ol){
                    var l = sc-ol;
                    $("#report84-dynagrid-2 tr>th:nth-child(3)").css({"position":"relative","left":l,"background-color":"#FFF"});
                    $("#report84-dynagrid-2 tr>td:nth-child(3)").css({"position":"relative","left":l,"border-right":"1px solid #F4F4F4"});
                    $("#report84-dynagrid-2 tr:nth-child(even)>td:nth-child(3)").css({"background-color":"#FFFFFF"});
                    $("#report84-dynagrid-2 tr:nth-child(odd)>td:nth-child(3)").css({"background-color":"#F9F9F9"});
                }else{
                    $("#report84-dynagrid-2 tr>th:nth-child(3)").css({"position":"static","left":"auto"});
                    $("#report84-dynagrid-2 tr>td:nth-child(3)").css({"position":"static","left":"auto"});
                }
            })
            
            $(".province-2").off().click(function(e){
                e.preventDefault();
                var provinceCode = $(this).attr("province-2");
                var provinceName = $(this).html();
                //$("#report84-modal-body").html(response);
                $("#report84-modal-body").html('.$loadIconData.');
                $("#report84-modal-header").html("รายละเอียดการทำอัลตร้าซาวด์ จังหวัด"+provinceName);
                $("#report84-modal-body").focus();
    
                $.ajax({
                    type    : "GET",
                    cache   : false,
                    url     : "'.Url::to('/report/report84-detail-province/').'",
                    data    : {
                        report  :   2,
                        provincecode    :   provinceCode
                    },
                    success  : function(response) {
                        //console.log(response);
                        $("#report84-modal-body").html(response);
                    },
                    error : function(){
                        $("#report84-modal-body").html("การเรียกดูข้อมูลผิดพลาด");
                    }
                });
    
                $("#report84-modal-trig").click();
                //console.log()
            });
        }

        var rebindedHospital3 = false;

        function rebindEventHospital3(){
            if(rebindedHospital3) return;
            rebindedHospital3 = true;

            $(".hospital-3").off().click(function(e){
                e.preventDefault();
                var hsiteCode = $(this).attr("hospital-3");
                var hospital = $(this).html();
                //$("#report84-modal-body").html(response);
                $("#report84-modal-body").html('.$loadIconData.');
                $("#report84-modal-header").html("รายละเอียดการรักษา ณ "+hospital);
                $("#report84-modal-body").focus();

                $.ajax({
                    type    : "GET",
                    cache   : false,
                    url     : "'.Url::to('/report/report84-detail-hospital/').'",
                    data    : {
                        report  :   3,
                        hsitecode    :   hsiteCode
                    },
                    success  : function(response) {
                        //console.log(response);
                        $("#report84-modal-body").html(response);
                    },
                    error : function(){
                        $("#report84-modal-body").html("การเรียกดูข้อมูลผิดพลาด");
                    }
                });

                $("#report84-modal-trig").click();
                //console.log()
            });
        }
    ',1);
?>
<button type="button" id="report84-modal-trig" data-toggle="modal" data-target="#report84-modal" style="display: none;"></button>
<div class="modal fade" id="report84-modal" role="dialog">
    <div class="modal-dialog" id="report84-modal-w">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="report84-modal-header">Modal Header</h4>
            </div>
            <div class="modal-body" id="report84-modal-body">
                <p>Some text in the modal.</p>
            </div>
            <div class="modal-footer" id="report84-modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">ปิด</button>
            </div>
        </div>
    </div>
</div>
