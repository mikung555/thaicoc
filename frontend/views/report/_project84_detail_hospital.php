<?php
    use yii\helpers\Html;


    //echo \yii\helpers\VarDumper::dump($result,10,true);
    echo '<div id="report84-ajax-report-div">';
        echo '<h4>';
            echo 'ก. จำนวนรวมรักษาทั้งหมด <label class="label label-primary">'.number_format( $result['hospital']['treatment'] ).'</label> ราย';
        echo '</h4>';
        echo '<br/>';
        echo '<h4>';
            echo 'ข. จำนวนที่รับการรักษาด้วยการผ่าตัดทั้งหมด <label class="label label-primary">'.number_format( $result['hospital']['all_surgery'] ).'</label> ราย';
        echo '</h4>';
        echo '<h4 style="padding-left: 22px;">';
            echo 'จำนวนที่รับการผ่าตัดให้หายขาด <label class="label label-primary">'.number_format( $result['hospital']['c_surgery'] ).'</label> ราย';
        echo '</h4>';
        echo '<h4 style="padding-left: 22px;">';
            echo 'จำนวนที่รับการผ่าตัดเพื่อการประคับประคอง <label class="label label-primary">'.number_format( $result['hospital']['all_surgery']-$result['hospital']['c_surgery'] ).'</label> ราย';
        echo '</h4>';
        echo '<br/>';
        if( sizeof( $result['community'] ) > 0){
            echo '<h4>';
                echo 'ค. จำแนกตามภูมิลำเนาของผู้รับบริการ (Community-based) ดังนี้';
            echo '</h4>';

            echo '<div class="report84-ajax-table-div">';
                echo '<table id="tertiary-detail-table" class="report84-detail-table table table-hover" rules="all" frame="box">';
                    $province = '';
                    $amphur = '';
                    $tambon = '';
                    $s = 0;
                    foreach($result['community'] as $key => $value){
                        if($province!=$value['province']){
                            $province = $value['province'];
                            echo '<tr>';
                                echo '<th colspan="2" class="section-tertiary-province">จังหวัด'.$province.'</th>';
                            echo '</tr>';
                        }
                        if($amphur!=$value['amphur']){
                            $amphur = $value['amphur'];
                            echo '<tr>';
                                echo '<th class="section-tertiary-amphur">อำเภอ'.str_replace('อำเภอ','', $amphur).'</th>';
                                echo '<th class="section-tertiary-amphur-header">จำนวน (ราย)</th>';
                            echo '</tr>';
                        }
                        echo '<tr>';
                            echo '<td style="padding-left: 60px;">'.'ตำบล'.str_replace('ตำบล', '', $value['tambon']).'</td>';
                            echo '<td>'.number_format($value['treatment']).'</td>';
                        $s+=intval($value['treatment']);
                        echo '</tr>';
                    }
                echo '</table>';
            echo '</div>';
        }


    echo '</div>';
//echo $s;

    $this->registerCss('
        .section-tertiary-province{
            text-align: left !important;
            color: #FFF;
            background-color: #000;
        }
        .section-tertiary-amphur{
            text-align: left !important;
            padding-left: 30px !important;
            background-color: #888;
            color: #FFF;
        }
        .section-tertiary-amphur-header{
            text-align: center !important;
            background-color: #888;
            color: #FFF;
        }
        #tertiary-detail-table tr>th:first-child,
        #tertiary-detail-table tr>td:first-child{
            width: 75% !important;
        }

    ');
    $this->registerCss('
        #report84-ajax-report-div{
            text-align:left;
        }
        .report84-detail-table{
            border : 1px solid #d9d9d9;
        }
        .report84-detail-table{
            border : 1px solid #d9d9d9;
            width: 100%;
        }
        .report84-detail-table tr:first-child{
            background-color: #e2e2e2 !important;
        }
        .report84-detail-table tr:nth-child(odd) {
            background: #fafafa;
        }
        .report84-detail-table tr:first-child>th{
            text-align: center;
        }
        .report84-detail-table tr:first-child > th:first-child{
            width: 70%;
        }
        .report84-detail-table tr>td:last-child{
            text-align: right;
        }
        .report84-detail-table th, .report84-detail-table td{
            padding: 3px;
        }
        .report84-ajax-table-div {
            padding: 10px 15% 10px 10%;;
        }
    ');

//    $this->registerJs('
//        //$("#report84-modal-header").html("'.$result['province']['province'].'");
//    ');