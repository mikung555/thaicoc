
<?php
$this->registerJsFile('@web/js/excellentexport.js', ['depends' => [yii\web\JqueryAsset::className()]]);
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
    use yii\helpers\Html;
    use yii\helpers\Url;    
	use common\lib\sdii\components\utils\SDdate;
	use frontend\controllers\ReportController;
	use frontend\controllers\OVSummary;

    use kartik\social\FacebookPlugin;
    use yii\bootstrap\Progress;
    $sharesetting = [
        'data-href'   =>  "?tab=overview",
        'data-layout' =>  "button_count",
        'data-size'   =>  "small",  
        'data-mobile-iframe'  =>  "true",
    ];

    echo FacebookPlugin::widget(['type'=>FacebookPlugin::SHARE, 'settings' => $sharesetting]);    
    $permlink = Url::base(true) . Url::toRoute(['report/index', 'tab' => 'overview']);
    echo Html::a('<p class="btn btn-xs btn-info">Permanent Link</p> ' . $permlink,$permlink); 
?>
<br><?php echo FacebookPlugin::widget(['type'=>FacebookPlugin::LIKE, 'settings' => ['data-href'=>$permlink,'data-layout'=>'box_count']]);?>
<?php
    $debug = false;
    //$debug = true;

    $loadIconData = '\'<i class="fa fa-spinner fa-spin fa-3x" style="margin:50px 0;"></i>\'';
    if($debug || Yii::$app->keyStorage->get('frontend.domain')=='cascap.in.th' || Yii::$app->keyStorage->get('frontend.domain')=='dpmcloud.dev' || Yii::$app->keyStorage->get('frontend.domain')=='yii2-starter-kit.dev' ) {
        echo '<div class="row">';
            echo '<div class="col-md-12" >';
                echo '<h3>ส่วนที่ 1 : ภาพรวม</h3>';
                echo '<div class="table-responsive" id="frontend-overview-report" style="text-align: center;"></div>';
            echo '</div>';
        echo '</div>';

        $this->registerJs('
            getReportOverview();
        ');

        $this->registerJs('

            function getReportOverview(){
                $("#frontend-overview-report").html('.$loadIconData.');

                $.ajax({
                    type    : "GET",
                    cache   : false,
                    url     : "'.yii\helpers\Url::to('ajax-report-overview/').'",
                    data    : {},
                    success :   function(response) {
                        $("#frontend-overview-report").html(response);
                    },
                    error   :   function(){
                        $("#frontend-overview-report").html("การแสดงข้อมูลผิดพลาด");
                    }
                });
            }
        ',1);

        $currentYear = intval(date('Y'));

        $maxFiscalyear = (int)(intval(date('m')) < 10 ? $currentYear : $currentYear + 1);
        $fiscalYearList = array();
        for ($y = 2013; $y <= $maxFiscalyear; ++$y) {
            $fiscalYearList[$y] = $y + 543;
        }

        $thaiMonth = ['',
            1=>['abvt'=>'ม.ค.', 'full'=>'มกราคม'],
            2=>['abvt'=>'ก.พ.', 'full'=>'กุมภาพันธ์'],
            3=>['abvt'=>'มี.ค.', 'full'=>'มีนาคม'],
            4=>['abvt'=>'เม.ย.', 'full'=>'เมษายน'],
            5=>['abvt'=>'พ.ค.', 'full'=>'พฤษภาคม'],
            6=>['abvt'=>'มิ.ย.', 'full'=>'มิถุนายน'],
            7=>['abvt'=>'ก.ค.', 'full'=>'กรกฎาคม'],
            8=>['abvt'=>'ส.ค.', 'full'=>'สิงหาคม'],
            9=>['abvt'=>'ก.ย.', 'full'=>'กันยายน'],
            10=>['abvt'=>'ต.ค.', 'full'=>'ตุลาคม'],
            11=>['abvt'=>'พ.ย.', 'full'=>'พฤศจิกายน'],
            12=>['abvt'=>'ธ.ค.', 'full'=>'ธันวาคม']
        ];

        $reverted = false;
        for ($m = 10; $m < 13; $m++) {
            $yearText = ($m < 10 ? $currentYear : $currentYear - 1);
            $fiscalMonthList[$yearText . '_' . $m] = $thaiMonth[$m]['full'] . ' ' . (intval($yearText) + 543);
            if (!$reverted) {
                if ($m == 12) {
                    $reverted = true;
                    $m = 0;
                }
            } else {
                if ($m == 9) {
                    break;
                }
            }
        }
        $loadIconData = '\'<i class="fa fa-spinner fa-spin fa-3x" style="margin:50px 0;"></i>\'';

        echo '<hr style="border: 1.5px solid #AAA;width:95%;">';

        $divControlsOv01 = Html::tag(
            'div',
            '<h3>ส่วนที่ 2 : ความครอบคลุมการตรวจรักษาพยาธิใบไม้ตับ (Primary prevention)</h3>

            <br/>
            <h3 class="section-text">ขั้นที่ 1 : เลือกประเภทรายงาน</h3>
            <div style="padding-left: 50px;">
                <select class="form-control" id="ov01-type">
                    <option value="0">Basic</option>
                    <option value="1">Advance</option>
                </select>
                <select class="form-control" id="ov01-level">
                    <option value="0">ทั้งหมด</option>
                    <option value="1">ระดับจังหวัด</option>
                    <option value="2">ระดับอำเภอ</option>
                    <option value="3">ระดับสถานบริการ</option>
                </select>
            </div>

            <br/>
     
            <div id="ov01-controls">
                <h3 class="section-text">ขั้นที่ 2 : เลือกเขตบริการสุขภาพ</h3>
                <div style="padding-left: 50px;">
                    <input type="checkbox" id="checkboxOv01Zone" name="zone" value="1" checked>
                    <label for="checkboxOv01Zone1">เขต 1</label>
                    <input type="checkbox" id="checkboxOv01Zone" name="zone" value="6" checked>
                    <label for="checkboxOv01Zone6">เขต 6</label>
                    <input type="checkbox" id="checkboxOv01Zone" name="zone" value="7" checked>
                    <label for="checkboxOv01Zone7">เขต 7</label>
                    <input type="checkbox" id="checkboxOv01Zone" name="zone" value="8" checked>
                    <label for="checkboxOv01Zone8">เขต 8</label>
                    <input type="checkbox" id="checkboxOv01Zone" name="zone" value="9" checked>
                    <label for="checkboxOv01Zone9">เขต 9</label>
                    <input type="checkbox" id="checkboxOv01Zone" name="zone" value="10" checked>
                    <label for="checkboxOv01Zone10">เขต 10</label>
                </div>
               
                <br/><br/>
                <h3 class="section-text">ขั้นที่ 3 : เลือกปีงบประมาณ</h3>
            <div style="padding-left: 50px;">'.
            Html::dropDownList(
                'list',
                $maxFiscalyear,
                $fiscalYearList,
                [
                    'id' => 'ov01FiscalYear',
                    'class' => 'form-control'
                ]
            ).
            '
            </div>
            <br/><br/>
            </div>
           
            <div style="padding-left: 50px;">
                <button type="button" id="ov01ReportSubmitAjax" class="btn btn-info form-control">แสดงรายงาน</button>
                <br/><br/>
                <a id="excel-ov01"  class="text-success" style="cursor:pointer;display:none;">
                    <i class="fa fa-2x fa-file-excel-o"></i> นำออกรายงานนี้ในรูปแบบ Excel
                </a>
            </div>',
            [
                'class' => 'form-inline',
                'style' => [
                    'text-align' => 'left'
                ]
            ]
        );
        echo Html::tag(
            'div',
            Html::tag(
                'div',

                $divControlsOv01.
                '<div class="row">
                    <div class="col-md-12">
                        <div id="ov01-report-div">
                        </div>
                    </div>
                </div>',
                [
                    'class'=>'col-md-12',
                    'style'=>[
                        'padding-bottom' => '20px'
                    ]
                ]
            ),
            [
                'class'=>'row'
            ]
        );

        echo '<hr style="border: 1.5px solid #AAA;width:95%;">';

        $divControlsCca02 = Html::tag(
            'div',
            '<h3>ส่วนที่ 3 : ความครอบคลุมการตรวจคัดกรองมะเร็งท่อน้ำดีด้วยอัลตร้าซาวด์ (Secondary prevention)</h3>

            <br/>
            <h3 class="section-text">ขั้นที่ 1 : เลือกประเภทรายงาน</h3>
            <div style="padding-left: 50px;">
                <select class="form-control" id="cca02-type">
                    <option value="0">Basic</option>
                    <option value="1">Advance</option>
                </select>
                <select class="form-control" id="cca02-level">
                    <option value="0">ทั้งหมด</option>
                    <option value="1">ระดับจังหวัด</option>
                    <option value="2">ระดับอำเภอ</option>
                    <option value="3">ระดับสถานบริการ</option>
                </select>
            </div>

            <br/><br/>

            <div id="cca02-controls">
                <h3 class="section-text">ขั้นที่ 2 : เลือกเขตบริการสุขภาพ</h3>
                <div style="padding-left: 50px;">
                    <input type="checkbox" id="checkboxCca02Zone" name="zone" value="1" checked>
                    <label for="checkboxCca02Zone1">เขต 1</label>
                    <input type="checkbox" id="checkboxCca02Zone" name="zone" value="6" checked>
                    <label for="checkboxCca02Zone6">เขต 6</label>
                    <input type="checkbox" id="checkboxCca02Zone" name="zone" value="7" checked>
                    <label for="checkboxCca02Zone7">เขต 7</label>
                    <input type="checkbox" id="checkboxCca02Zone" name="zone" value="8" checked>
                    <label for="checkboxCca02Zone8">เขต 8</label>
                    <input type="checkbox" id="checkboxCca02Zone" name="zone" value="9" checked>
                    <label for="checkboxCca02Zone9">เขต 9</label>
                    <input type="checkbox" id="checkboxCca02Zone" name="zone" value="10" checked>
                    <label for="checkboxCca02Zone10">เขต 10</label>
                </div>
                <br/><br/>
                <h3 class="section-text">ขั้นที่ 3 : เลือกปีงบประมาณ</h3>
                <div style="padding-left: 50px;">'.
                Html::dropDownList(
                    'list',
                    $maxFiscalyear,
                    $fiscalYearList,
                    [
                        'id' => 'cca02FiscalYear',
                        'class' => 'form-control'
                    ]
                ).
                '
                </div>
                <br/><br/>
            </div>
            <div style="padding-left: 50px;">
                <button type="button" id="cca02ReportSubmitAjax" class="btn btn-info form-control">แสดงรายงาน</button>
                <br/><br/>
                <a download="cca02-report.xls" id="excel-cca02" onclick="ExcellentExport.excel(this, \'cca02-report-table\', \'Report cca02 Sheet\');" class="text-success" style="cursor:pointer;display:none;">
                    <i class="fa fa-2x fa-file-excel-o"></i> นำออกรายงานนี้ในรูปแบบ Excel
                </a>
            </div>',
            [
                'class' => 'form-inline',
                'style' => [
                    'text-align' => 'left'
                ]
            ]
        );
        echo Html::tag(
            'div',
            Html::tag(
                'div',

                $divControlsCca02.
                '<div class="row">
                    <div class="col-md-12">
                        <div id="cca02-report-div">
                        </div>
                    </div>
                </div>',
                [
                    'class'=>'col-md-12',
                    'style'=>[
                        'padding-bottom' => '20px'
                    ]
                ]
            ),
            [
                'class'=>'row'
            ]
        );

?>
<div class="row">
    <div class="col-md-12" style="padding-bottom: 20px;">
        <div class="form-inline" style="text-align: left;">
            <h3>ส่วนที่ 4 : ﻿ผลการตรวจอัลตร้าซาวด์</h3>
            <a href="http://report.cascap.in.th/report1/cc_overview.php?s=xyz123" target="_blank">Click</a>
<?php
/*
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, "http://report.cascap.in.th/report1/cc_overview.php?s=xyz123");
curl_setopt($ch, CURLOPT_HEADER, 0);
curl_exec($ch);
curl_close($ch);
 */
?>
            <br/><br/>
        </div>
    </div>
</div>
<?php
        $this->registerJs('
                
            // ov01 ##########
            $("#ov01ReportSubmitAjax").click(function(){

                ov01FormData["refresh"] = false;

                if( $("#ov01-type").val()==0 ){

                    ov01FormData["fiscalYear"] = $("#ov01FiscalYear").children("[selected]").val();
                    ov01FormData["ov01Level"] = $("#ov01-level").val();
                    ov01FormData["zone1"] = 1;
                    ov01FormData["zone6"] = 6;
                    ov01FormData["zone7"] = 7;
                    ov01FormData["zone8"] = 8;
                    ov01FormData["zone9"] = 9;
                    ov01FormData["zone10"] = 10;

                    getReportOv();
                    return;
                }

                ov01FormData["fiscalYear"] = $("#ov01FiscalYear").val();
                ov01FormData["ov01Level"] = $("#ov01-level").val();
                
                $("[id^=\'checkboxOv01Zone\']").each(function(){
                    delete ov01FormData["zone"+$(this).val()];
                });

                $("[id=checkboxOv01Zone]:checked").each(function(index, element){
                    ov01FormData["zone"+$(this).val()] = $(this).val();
                });

                getReportOv();

            });
            $("#ov01-type").change(function(){
                if( $(this).val()==0 ){
                    $("#ov01-controls").hide();
                }else{
                    $("#ov01-controls").show();
                }
            });

            
            $("#excel-ov01").click(function(e){
                var dataTable = $("#data-report-table");
                var headerName = ("<tr>< th colspan=\'10\'>รายงานความครอบคลุมการตรวจรักษาพยาธิใบไม้ตับ</th>< /tr>").replace(/< /g,"<");

                var headerLastcal = ("< tr>< td colspan=\'10\'>"+ $("#excel-data-header-lastcal").html() + "< /td>< /tr>").replace(/< /g,"<");

                dataTable.prepend(headerLastcal);
                dataTable.prepend(headerName);
                this.download="ov01-primary-prevention.xls" 
                ExcellentExport.excel(this, \'data-report-table\', \'Report ov01 Sheet\');
  
                //var ov01  = document.createElement("a");
                //dataTable.removeAttr("class").removeAttr("id").removeAttr("style").removeAttr("border");
                //dataTable.find("*").removeAttr("class").removeAttr("id").removeAttr("style");

                //var headerRange = ("< tr>< td colspan=\'8\'>"+ $("#excel-cca02-header-range").html() + "< /td>< /tr>").replace(/< /g,"<");

                //var excelString = dataTable.prop("outerHTML").replace(/colspan/g,"align=\"center\" colspan");
                //var excelString = dataTable.innerHTML;
                //console.log(excelString);

                //excelString = encodeURIComponent(excelString);
                //ov01.href = "data:application/vnd.ms-excel,"+excelString;
                //window.open("data:application/vnd.ms-excel," + excelString);
                //ov01.download = "ov01-primary-prevention.xls";
                //ov01.click();
                //ExcellentExport.excel(ov01, "data-report-table", "Report ov01 Sheet");
                //e.preventDefault();
               }); 
            
            
            // cca02 ##########
            $("#cca02ReportSubmitAjax").click(function(){

                cca02FormData["refresh"] = false;

                if( $("#cca02-type").val()==0 ){

                    cca02FormData["fiscalYear"] = $("#cca02FiscalYear").children("[selected]").val();
                    cca02FormData["cca02Level"] = $("#cca02-level").val();
                    cca02FormData["zone1"] = 1;
                    cca02FormData["zone6"] = 6;
                    cca02FormData["zone7"] = 7;
                    cca02FormData["zone8"] = 8;
                    cca02FormData["zone9"] = 9;
                    cca02FormData["zone10"] = 10;

                    getReportCca02();
                    return;
                }

                cca02FormData["fiscalYear"] = $("#cca02FiscalYear").val();
                cca02FormData["cca02Level"] = $("#cca02-level").val();
                $("[id^=\'checkboxCca02Zone\']").each(function(){
                    delete cca02FormData["zone"+$(this).val()];
                });

                $("[id=checkboxCca02Zone]:checked").each(function(){
                    cca02FormData["zone"+$(this).val()] = $(this).val();
                });

                getReportCca02();

            });

            $("#cca02-type").change(function(){
                if( $(this).val()==0 ){
                    $("#cca02-controls").hide();
                }else{
                    $("#cca02-controls").show();
                }
            });
            
            

            $("#excel-cca02").click(function(e){

                var dataTable = $("#cca02-report-table");
                var headerName = ("< tr>< th colspan=\'8\'>รายงานความครอบคลุมการตรวจคัดกรองมะเร็งท่อน้ำดีด้วยอัลตร้าซาวด์< /th>< /tr>").replace(/< /g,"<");

                var headerLastcal = ("< tr>< td colspan=\'8\'>"+ $("#excel-cca02-header-lastcal").html() + "< /td>< /tr>").replace(/< /g,"<");

                var headerRange = ("< tr>< td colspan=\'8\'>"+ $("#excel-cca02-header-range").html() + "< /td>< /tr>").replace(/< /g,"<");
                dataTable.prepend(headerLastcal);
                dataTable.prepend(headerRange);
                dataTable.prepend(headerName);

                this.download="cca02-secondary-prevention.xls" 
                ExcellentExport.excel(this, \'cca02-report-table\', \'Report cca02 Sheet\');

                //var cca02 = document.createElement("a");

                //dataTable.removeAttr("class").removeAttr("id").removeAttr("style").removeAttr("border");
                // dataTable.find("*").removeAttr("class").removeAttr("id").removeAttr("style");

                //var excelString = dataTable.prop("outerHTML").replace(/colspan/g,"align=\"center\" colspan");
                //console.log(excelString);

                //excelString = encodeURIComponent(excelString);
                //cca02.href = "data:application/csv;charset=UTF-8,"+excelString;
                //window.open("data:application/vnd.ms-excel," + excelString);
                //cca02.download = "cca02-secondary-prevention.xls";
                //cca02.click();
                //download(excelString, "cca02-primary-prevention.csv", "text/csv");
                //e.preventDefault();
            });
           
        ');

        $this->registerJs('

            var cca02FormData = {};
            var cca02Xhr;

            function getReportCca02(){

                //console.log(cca02FormData);
                //return;

                if(cca02Xhr!=undefined && cca02Xhr.readyState!=0){
                    cca02Xhr.abort();
                }

                $("#cca02-report-div").html('.$loadIconData.');
                $("#cca02ReportSubmitAjax").attr("disabled",true);
                $("#excel-cca02").hide();

                cca02Xhr = $.ajax({
                    type    : "GET",
                    cache   : false,
                    url     : "'.yii\helpers\Url::to('ajax-report-cca02/').'",
                    data    : cca02FormData,
                    success :   function(response) {
                        $("#cca02ReportSubmitAjax").removeAttr("disabled");
                        $("#cca02-report-div").html(response);
                        $("#excel-cca02").show();

                    },
                    error   :   function(){
                        $("#cca02ReportSubmitAjax").removeAttr("disabled");
                        $("#cca02-report-div").html("การแสดงข้อมูลผิดพลาด");
                    }
                });

            }

            var ov01FormData = {};
            var ovXhr;

            function getReportOv(){
                //console.log(ov01FormData);
                //return;
                if(ovXhr!=undefined && ovXhr.readyState!=0){
                    ovXhr.abort();
                }

                $("#ov01-report-div").html('.$loadIconData.');
                $("#ov01ReportSubmitAjax").attr("disabled",true);
                $("#excel-ov01").hide();

                cca02Xhr = $.ajax({
                    type    : "GET",
                    cache   : false,
                    url     : "'.yii\helpers\Url::to('ajax-report-ov/').'",
                    data    : ov01FormData,
                    success :   function(response) {
                        $("#ov01ReportSubmitAjax").removeAttr("disabled");
                        $("#ov01-report-div").html(response);
                        $("#excel-ov01").show();

                    },
                    error   :   function(){
                        $("#ov01ReportSubmitAjax").removeAttr("disabled");
                        $("#ov01-report-div").html("การแสดงข้อมูลผิดพลาด");
                    }
                });

            }

        ',1);


    }
        $this->registerCss('
            #data-report-table,
            #cca02-report-table,
            #allproof-report-table
            {
                background-color: #FFFFFF; border-radius: 5px;
                word-break: normal;
                /*transform: rotateX(180deg);*/
            }

            #data-report-table th,
            #cca02-report-table th,
            #allproof-report-table th
            {
                text-align:center !important;
                vertical-align:middle;

            }


            #data-report-table td,#data-report-table th,
            #cca02-report-table td,#cca02-report-table th,
            #allproof-report-table td,#allproof-report-table td
            {
                text-align:right;
                border-left:1px solid #f4f4f4;
            }

            #data-report-div,
            #cca02-report-div,
            #allproof-report-div
            {
                overflow-x:overlay;
                /*transform: rotateX(180deg);*/
                text-align:center;
            }

            .unit-section
            {
                background-color:#000;
                color:#FFF;
            }

            .unit-separator
            {
                background-color:#555;
                color:#FFF;
                text-align:left !important;
            }

            .unit-typesplit
            {
                background-color:#AAA;
                color:#FFF;
                text-align:left !important;
            }

            .unit-name
            {
                text-align:left !important;
            }

            .unit-heading
            {
                text-align:center !important;
                vertical-align:middle !important;
                font-weight:bold;
                background-color:#f5f5f5;
            }

            .unit-sum
            {
                text-align:right !important;
                color:blue;
            }

            .unit-sum.name
            {
                text-align:left !important;
            }

            [id$="-controls"]
            {
                display:none;
            }

            [id$="refresh"]
            {
                cursor:pointer;
                color: #3c8dbc;
            }

            [id$="refresh"]:hover
            {
                color: #72afd2;
            }

            .tmp-section
            {
                background-color:#eee !important;
            }

            .section-text
            {
                text-align: left;
                margin-left: 3%;
            }

            .row
            {
                margin-left: 0 !important;
                margin-right: 0 !important;
            }

            #ov01-report-div{
                overflow-x: auto;
                text-align: center;
            }
        ');
?>
