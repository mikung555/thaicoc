<?php
    use yii\helpers\Html;
    use frontend\controllers\Project84SumController;

    //echo \yii\helpers\VarDumper::dump($result,10,true);
    
    $province_code = $result['province']['provincecode'];

    //$amphur = Project84SumController::GetAmphur(1, $province_code);
    //$amplur = $result['amphur'];
    $sumTotal = 0;
     foreach($result['amphur'] as $key => $value){
         $sumTotal += number_format($value["cca02"]);
     }
    
    echo '<div id="report84-ajax-report-div">';
        echo '<h4>';
            echo 'ก. จำนวนรวมอัลตร้าซาวด์ทั้งหมด <label class="label label-primary">'.$sumTotal.'</label> ราย';
        echo '</h4>';
        
        if($result['province']['cca02']>0){
            echo '<h4>';
                echo 'ข. จำแนกตามหน่วยบริการที่ให้บริการ (Hospital-based) ดังนี้';
            echo '</h4>';

            echo '<div  class="" style="overflow-x:auto;">';
                echo '<table class="report84-detail-table table table-hover" rules="all" >';
                    echo '<tr>';
                        echo '<th style="width:25%">หน่วยบริการ</th>';
                        echo '<th width="">ลงทะเบียน</th>';
                        echo '<th width="">อัลตร้าซาวด์</th>';
                        echo '<th width="">ผิดปกติอย่างใดอย่างหนึ่ง</th>';
                        echo '<th width="">PDF</th>';
                        echo '<th width="">สงสัย CCA</th>';
                        echo '<th width="">CT/MRI</th>';
                        echo '<th width="">พบเป็นมะเร็ง</th>';
                        echo '<th width="">ได้รับการรักษา</th>';
                    echo '</tr>';
                    
                    $tamchk="";
                    $amchk = "";
                    $regis = 0;
                    $cca02 = 0;
                    $f2v2a1 = 0;
                    $f2v2a1b2 = 0;
                    $f2v6a3b1 = 0;
                    $f2p1v2 = 0;
                    $f2p1v3 = 0;
                    $f3v1 = 0;
                    $regisSumTotal = 0;
                    $cca02SumTotal = 0;
                    $f2v2a1SumTotal = 0;
                    $f2v2a1b2SumTotal = 0;
                    $f2v6a3b1SumTotal = 0;
                    $f2p1v2SumTotal = 0;
                    $f2p1v3SumTotal = 0;
                    $f3v1SumTotal = 0;
                    foreach($result['amphur'] as $key => $value){
                        
                           if($amchk != $value['amphurcode']){
                               $amchk = $value['amphurcode'];
                                echo '<tr class="section-secondary-amphur">';
                                    echo '<th>อำเภอ'.$value['amphur'].'</th>';
                                echo '</tr>';
                            
                            //$hospital = Project84SumController::GetHospital(1, $province_code, $value['amphurcode']);
                            foreach($result['amphur'] as $key => $value2){
                                if($value2['tamboncode'] && $amchk == $value2['amphurcode']){
                                    echo '<tr>';
                                        $name = str_replace('โรงพยาบาลส่งเสริมสุขภาพตำบล','รพ.สต.',$value2['name']);
                                        $name = str_replace('โรงพยาบาล','รพ.',$name);
                                        $name = str_replace('สำนักงานสาธารณสุขจังหวัด','สสจ.',$name);
                                        $name = str_replace('สำนักงานสาธารณสุขอำเภอ','สสอ.',$name);
                                        $name = str_replace('สถานีอนามัย','สอ.',$name);
                                        $name = str_replace('ตำบล','ต.',$name);
                                        
                                        $regis += number_format($value2['regis']);
                                        $cca02 += number_format($value2['cca02']);
                                        $f2v2a1 += number_format($value2['f2v2a1']);
                                        $f2v2a1b2 += number_format($value2['f2v2a1b2']);
                                        $f2v6a3b1 += number_format($value2['f2v6a3b1']);
                                        $f2p1v2 += number_format($value2['f2p1v2']);
                                        $f2p1v3 += number_format($value2['f2p1v3']);
                                        $f3v1 += number_format($value2['f3v1']);

                                        echo '<td>'.$name.' ตำบล'.$value2['tambon'].'</td>';
                                        echo '<td>'.number_format($value2['regis']).'</td>';
                                        echo '<td>'.number_format($value2['cca02']).'</td>';
                                        echo '<td>'.number_format($value2['f2v2a1']).'</td>';
                                        echo '<td>'.number_format($value2['f2v2a1b2']).'</td>';
                                        echo '<td>'.number_format($value2['f2v6a3b1']).'</td>';
                                        echo '<td>'.number_format($value2['f2p1v2']).'</td>';
                                        echo '<td>'.number_format($value2['f2p1v3']).'</td>';
                                        echo '<td>'.number_format($value2['f3v1']).'</td>';
                                        echo '</tr>';
                                    }
                            }
                            echo '<tr>';
                                echo '<td> <b>รวมทั้งหมด อำเภอ'.$value['amphur'].'</b></td>';
                                echo '<td>'.$regis.'</td>';
                                echo '<td>'.$cca02.'</td>';
                                echo '<td>'.$f2v2a1.'</td>';
                                echo '<td>'.$f2v2a1b2.'</td>';
                                echo '<td>'.$f2v6a3b1.'</td>';
                                echo '<td>'.$f2p1v2.'</td>';
                                echo '<td>'.$f2p1v3.'</td>';
                                echo '<td>'.$f3v1.'</td>';
                            echo '</tr>';
                                  
                                    $regisSumTotal += $regis;
                                    $cca02SumTotal += $cca02;
                                    $f2v2a1SumTotal += $f2v2a1;
                                    $f2v2a1b2SumTotal += $f2v2a1b2;
                                    $f2v6a3b1SumTotal += $f2v6a3b1;
                                    $f2p1v2SumTotal += $f2p1v2;
                                    $f2p1v3SumTotal += $f2p1v3;
                                    $f3v1SumTotal += $f3v1;
                                    $regis = 0;
                                    $cca02 = 0;
                                    $f2v2a1 = 0;
                                    $f2v2a1b2 = 0;
                                    $f2v6a3b1 = 0;
                                    $f2p1v2 = 0;
                                    $f2p1v3 = 0;
                                    $f3v1 = 0;
                        }
                  }
                echo '<tr>';
                    echo '<td> <b>รวมทั้งหมด</b> </td>';
                    echo '<td><b>'.$regisSumTotal.'</b></td>';
                    echo '<td><b>'.$cca02SumTotal.'</b></td>';
                    echo '<td><b>'.$f2v2a1SumTotal.'</b></td>';
                    echo '<td><b>'.$f2v2a1b2SumTotal.'</b></td>';
                    echo '<td><b>'.$f2v6a3b1SumTotal.'</b></td>';
                    echo '<td><b>'.$f2p1v2SumTotal.'</b></td>';
                    echo '<td><b>'.$f2p1v3SumTotal.'</b></td>';
                    echo '<td><b>'.$f3v1SumTotal.'</b></td>';
                echo '</tr>';
                  
                echo '</table>';
            echo '</div>';

            if(sizeof($result['amphur'])>0){
                echo '<h4>';
                    echo 'ค. จำแนกตามภูมิลำเนาของผู้รับบริการ (Community-based) ดังนี้';
                echo '</h4>';

                echo '<div class="report84-ajax-table-div">';
                    echo '<table id="secondary-detail-table" class="table table-hover" rules="all" frame="box">';

                        $amphur = '';
                        foreach($result['amphur'] as $key => $value){
                            if($amphur != $value['amphurcode']){
                                $amphur = $value['amphurcode'];
                                echo '<tr class="section-secondary-amphur">';
                                    echo '<th>อำเภอ'.$value['amphur'].'</th>';
                                    echo '<th>จำนวน (ราย)</th>';
                                echo '</tr>';
                            }
                            echo '<tr>';
                                echo '<td>ตำบล'.$value['tambon'].'</td>';
                                echo '<td>'.number_format($value['cca02']).'</td>';
                            echo '</tr>';
                        }
                    echo '</table>';
                echo '</div>';
            }

        }
    echo '</div>';

    $this->registerCss('

        #secondary-detail-table{
            border : 1px solid #d9d9d9;
        }
        #secondary-detail-table tr:nth-child(odd) {
            background-color: #fafafa;
        }
        #secondary-detail-table .section-secondary-amphur{
            background-color: #e2e2e2 !important;
        }
        #secondary-detail-table .section-secondary-amphur th{
            text-align: center;
            vertical-align: middle;
        }
        #secondary-detail-table tr > td:last-child{
            text-align: right;
        }


        #report84-ajax-report-div{
            text-align:left;
        }
        .report84-detail-table{
            border : 1px solid #d9d9d9;
        }
        .report84-detail-table{
            border : 1px solid #d9d9d9;
            width: 100%;
        }
        .report84-detail-table tr:first-child{
            background-color: #e2e2e2 !important;
        }
        .report84-detail-table tr:nth-child(odd) {
            background: #fafafa;
        }
        .report84-detail-table tr:first-child>th{
            text-align: center;
        }
        .report84-detail-table tr:first-child > th:first-child{
            width: 70%;
        }
        .report84-detail-table tr>td:last-child{
            text-align: right;
        }
        .report84-detail-table th, .report84-detail-table td{
            padding: 3px;
        }
        .report84-ajax-table-div {
            padding: 10px 15% 10px 10%;;
        }
    ');

//    $this->registerJs('
//        //$("#report84-modal-header").html("'.$result['province']['province'].'");
//    ');