<?php

use yii\helpers\Html;
use yii\helpers\Url;
use miloschuman\highcharts\Highcharts;
use kartik\social\FacebookPlugin;

?>


<?php
$sharesetting = [
    'data-href'   =>  "?tab=summary",
    'data-layout' =>  "button_count",
    'data-size'   =>  "small",  
    'data-mobile-iframe'  =>  "true",
];

echo FacebookPlugin::widget(['type'=>FacebookPlugin::SHARE, 'settings' => $sharesetting]);    

$permlink = Url::base(true) . Url::toRoute(['report/index', 'tab' => 'summary']);
echo Html::a('<p class="btn btn-xs btn-info">Permanent Link</p> ' . $permlink,$permlink); 
?>


<br><?php echo FacebookPlugin::widget(['type'=>FacebookPlugin::LIKE, 'settings' => ['data-href'=>$permlink,'data-layout'=>'box_count']]);?>
<?php
echo "<br><br>";
$loadIconData = '\'<i class="fa fa-spinner fa-spin fa-3x" style="margin:50px 0;"></i>\'';

echo '<div class="row">';
    echo '<div class="col-md-12" >';
        echo '<div class="table-responsive" id="frontend-summary-report" style="text-align: center;">';

//$this->registerJs('
//    getReportSummary();
//');
//$this->registerJs('
//    function getReportSummary(){
//        
//        $("#frontend-summary-report").html('.$loadIconData.');
//        
//        $.ajax({
//            type    : "GET",
//            cache   : false,
//
//            url     : "'.yii\helpers\Url::to('ajax-report-summary/').'",
//            data    : {},
//            success :   function(response) {
//                    $("#frontend-summary-report").html(response);
//                    $(window).resize();
//            },
//            error   :   function(){
//                    $("#frontend-summary-report").html("Sumething wrong!");
//            }
//        });
//    }
//',1);

echo Highcharts::widget();
                
/*************** Graph section ***************/
$ShortMonth = array('','ม.ค.','ก.พ.','มี.ค.','เม.ย.','พ.ค.','มิ.ย.','ก.ค.','ส.ค.','ก.ย.','ต.ค.','พ.ย.','ธ.ค.');
$g1data = array();$g2data = array();$g3data = array();$g4data = array();$g5data = array();$g6data = array();
$g1label = array();$g2label = array();$g3label = array();$g4label = array();$g5label = array();$g6label = array();$g7data = array();

$sql = "SELECT UPDATE_TIME FROM information_schema.tables WHERE TABLE_NAME LIKE 'summary_graph1'";
$rs = Yii::$app->db->createCommand($sql)->queryOne();
$GraphLastUpdate = $rs['UPDATE_TIME'];
/* G1 */
$sql = "SELECT * FROM summary_graph1";
$rs1 = Yii::$app->db->createCommand($sql)->queryAll();
$FDate = (date("Y") - 2).'-'.date("m").'-01';
$TempDate = date("Y-m-d",strtotime(date("Y-m-d", strtotime($FDate)) . " +1 month"));
$FDate = date("Y-m-d",strtotime(date("Y-m-d", strtotime($TempDate)) . " -1 day"));
$index = 0;
foreach($rs1 as $row) {
    $date_run = $row['Y'].'-'.str_pad($row['M'],2,'0',STR_PAD_LEFT).'-01';
    if($date_run<=$FDate){
        $g1label[0] = $row['M'].' '.$row['Y'];
        $g1data[0] += $row['P_CNT'];
        $index = 1;
    }else{
        $g1label[$index] = $row['M'].' '.$row['Y'];
        $g1data[$index++] = $row['P_CNT'];
    }            
}
/* G2 */
$sql = "SELECT * FROM summary_graph2";
$rs2 = Yii::$app->db->createCommand($sql)->queryAll();
$index = 0;
foreach($rs2 as $row) {
    $date_run = $row['Y'].'-'.str_pad($row['M'],2,'0',STR_PAD_LEFT).'-01';
    if($date_run<=$FDate){
        $g2label[0] = $row['M'].' '.$row['Y'];
        $g2data[0] += $row['P_CNT'];
        $index = 1;
    }else{
        $g2label[$index] = $row['M'].' '.$row['Y'];
        $g2data[$index++] = $row['P_CNT'];
    }            
}         
/* G3 */
$sql = "SELECT * FROM summary_graph3";
$rs3 = Yii::$app->db->createCommand($sql)->queryAll();
$index = 0;
foreach($rs3 as $row) {
    $g3label[$index] = 'เขต '.$row['ZONE'];
    $g3data[$index++] = $row['P_CNT'];
}       
/* G4 */
$sql = "SELECT * FROM summary_graph4";
$rs4 = Yii::$app->db->createCommand($sql)->queryAll();
$index = 0;
foreach($rs4 as $row) {
    $g4label[$index] = 'เขต '.$row['ZONE'];
    $g4data[$index++] = $row['P_CNT'];
}        
/* G5 */
$sql = "SELECT * FROM summary_graph5";
$rs5 = Yii::$app->db->createCommand($sql)->queryAll();
$index = 0;
foreach($rs5 as $row){
    $g5label[$index] = $row['PROVINCE'];
    $g5data[$index++] = $row['P_CNT'];
}
/* G6 */
$sql = "SELECT * FROM summary_graph6";
$rs6 = Yii::$app->db->createCommand($sql)->queryAll();
$index = 0;
foreach($rs6 as $row){
    $g6label[$index] = $row['PROVINCE'];
    $g6data[$index++] = $row['P_CNT'];
}
/* G7 */
$sql = "SELECT b.`name`,a.* FROM summary_graph7 a LEFT JOIN all_hospital_thai b
        ON a.hsitecode=b.hcode ORDER BY BINARY b.`name`";
$rs7 = Yii::$app->db->createCommand($sql)->queryAll();
$index = 0;
foreach($rs7 as $row){
    $g7data[$index][0] = $row['name'];
    $g7data[$index][2] = $row['cca21'];
    $g7data[$index][3] = $row['cca3'];
    $g7data[$index][4] = $row['cca4'];
    $g7data[$index][5] = $row['cca5'];
    $index++;
}

for($i=0;$i<count($g1label);$i++){
    $x = explode(' ',$g1label[$i]);
    $g1label[$i] = ($i==0)?'ปี 56 ถึง '.$ShortMonth[$x[0]+0].' '.substr($x[1]+543,-2):$ShortMonth[$x[0]+0].' '.substr($x[1]+543,-2);
}
/*********************************************/
?>

<br>




</div>

<!--//////////////////////////// Graph Display Panel ////////////////////////-->
<div id="summarygraph-view">

<?php
$datetime1 = strtotime($GraphLastUpdate);
$datetime2 = strtotime(date("Y-m-d H:i:s"));
$interval  = abs($datetime2 - $datetime1);
$mDiff   = round($interval / 60);
if($mDiff <= 60)
    $btnColor = "primary";
elseif($mDiff > 60 && $mDiff <= 120)
    $btnColor = "info";
elseif($mDiff > 120 && $mDiff <= 1440)
    $btnColor = "warning";
else
    $btnColor = "danger";
?>
    

    <div class="container" style="margin-bottom: 15px">
        <div class="row">

               <div style="text-align: right; "><a id="summaryresult" class = "btn btn-success" title = "คลิกเพื่อโหลดข้อมูลล่าสุด" style="margin-right:25px;"><i class="fa fa-refresh"></i></a></div>
               <div id="btn-Preview-Image" data-toggle = "modal" data-target = "#ModalImage" title = "คลิกเพื่อดูรูป"   style="cursor:zoom-in;">

               <div  id="contentsum" >     
                    <div id="toptext" class="col-xs-12 col-sm-12 col-md-12"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;โครงการรณรงค์กำจัดปัญหาพยาธิใบไม้ตับ และมะเร็งท่อน้ำดี เพื่อรำลึกในพระมหากรุณาธิคุณ <br>ของพระบาทสมเด็จพระปรมินทรมหาภูมิพลอดุลยเดช พร้อมทั้งถวายเป็นพระราชกุศลแด่สมเด็จพระนางเจ้าฯ <br>พระบรมราชินีนาถ ทรงเจริญพระชนมพรรษา 84 พรรษา </div>
               

                    <div  id="getimage">
                    <?php 
                    //echo $this->render('/project84/graph-summary/paint');
                    ?>

                        <div class="img-container"> <img class="img-responsive" src="/img/bgsummary1.png" /></div> 

                            <div id="text8" class="col-xs-12 col-sm-8 col-md-12"><?= number_format($summaryshow['data12']) ?> ราย </div>
                            <div id="textin8" class="col-xs-12 col-sm-6 col-md-12"">(คิดเป็น <?= number_format((($summaryshow['data16']/$summaryshow['confirm'])*100),1)?>% ของรายที่ผล CT/MRI/MRCP เป็น confirmed ทั้งหมด)</div>
                            
                            <div id="textin7" class="col-xs-12 col-sm-8 col-md-12">(จากกลุ่ม Confirm CT/MRI ผ่าตัดเพื่อหายขาด <?= number_format($summaryshow['data16'])?> ราย)</div>
                            <div id="text7" class="col-xs-8 col-sm-8 col-md-12">(ผ่าตัดเพื่อหายขาด <?= number_format($summaryshow['data13'])?> ราย/ ผ่าตัดประคับประคอง <?= number_format($summaryshow['data14'])?> ราย/ อื่นๆ <?= number_format($summaryshow['data15'])?> ราย)</div>
                            
                            <div id="text6" class="col-xs-12 col-sm-8 col-md-12"><?= number_format($summaryshow['data10'])?> ราย / <?= number_format($summaryshow['data11'])?> ครั้ง (positive = <?= number_format($summaryshow['postb8'])?> ราย)</div>
                            <div id="textin6" class="col-xs-12 col-sm-8 col-md-12">(คิดเป็น <?= number_format((($summaryshow['postb8']/$summaryshow['data10'])*100),1)?>% ของรายที่ตรวจทางพยาธิวิทยาทั้งหมด)</div>
                            
                            <div id="text5" class="col-xs-12 col-sm-8 col-md-12"><?= number_format($summaryshow['data8'])?> ราย / <?= number_format($summaryshow['data9'])?> ครั้ง   (confirmed = <?= number_format($summaryshow['confirm'])?> ราย)</div> 
                            <div id="textin5" class="col-xs-12 col-sm-8 col-md-12">(คิดเป็น <?= number_format((($summaryshow['confirm']/$summaryshow['data8'])*100),1)?>% ของรายที่ตรวจ CT/MRT/MRCP ทั้งหมด)</div>
                            
                            <div id="text4" class="col-xs-12 col-sm-12 col-md-12"><?= number_format($summaryshow['data6'])?> ราย / <?= number_format($summaryshow['data7'])?> ครั้ง  (suspected = <?= number_format($summaryshow['suspect'])?> ราย)</div>
                            <div id="textin4" class="col-xs-12 col-sm-8 col-md-12">(คิดเป็น <?= number_format((($summaryshow['suspect']/$summaryshow['data6'])*100),1)?>% ของรายที่ตรวจอัลตร้าซาวด์ทั้งหมด)</div>
                            
                            <div id="text3" class="col-xs-12 col-sm-8  col-md-12"><?= number_format($summaryshow['data4'])?> ราย / <?= number_format($summaryshow['data4'])?> ครั้ง (positive = <?= number_format($summaryshow['posov'])?> ราย)</div>
                            <div id="textin3" class="col-xs-12 col-sm-8 col-md-12">(คิดเป็น <?= number_format((($summaryshow['posov']/$summaryshow['data4'])*100),1)?>% ของรายที่ตรวจอุจจาระ/ปัสสาวะทั้งหมด)</div>
                            
                            <div id="text2" class="col-xs-12 col-sm-8 col-md-12">ลงทะเบียน <?= number_format($summaryshow['data2']) ?> ราย / ข้อมูลพื้นฐาน <?= number_format($summaryshow['data3'])?> ราย</div>
                            <div id="textin2" class="col-xs-12 col-sm-8 col-md-12">(คิดเป็น <?= number_format((($summaryshow['data3']/$summaryshow['data2'])*100),1)?>% ของรายที่ลงทะเบียนทั้งหมด)</div>
                            
                            <div id="text1" class="col-xs-12 col-sm-8 col-md-12"><?= number_format($summaryshow['data1'])?> ราย</div>

                    </div> 
                        <div id="calsum" class="col-xs-8 col-sm-8 col-lg-12">
                            <b>[&nbsp;คำนวนกราฟล่าสุดเมื่อ&nbsp;<?= $summaryshow['lastupdate']?>&nbsp;]</b> 
                        </div>          

                </div> 
            </div>

                
                <div class="modal fade" id="ModalImage" role="dialog">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title"><b>Show</b></h4>
                            </div>

                            <div class="modal-body">
                             
                                <img id="previewImage" class="img-responsive"  src=""/>
                            </div> 

                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <!-- /.modal -->
                
                
                
                
        </div> 
       
    </div>
    
</div>

<?php 

$this->registerJsFile("https://html2canvas.hertzen.com/build/html2canvas.min.js");

$this->registerJS("
    function numFormat(n){
    return parseFloat(n).toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, '$1,');
    }
    function percentage(n){
    return n.toFixed(1).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
}

        $('#text8').fadeIn(3500);
        $('#textin8').fadeIn(3500);
        $('#textin7').fadeIn(3300);
        $('#text7').fadeIn(3200);
        $('#text6').fadeIn(3000);
        $('#textin6').fadeIn(2900);
        $('#text5').fadeIn(2800);
        $('#textin5').fadeIn(2500);
        $('#text4').fadeIn(2200);
        $('#textin4').fadeIn(2000);
        $('#text3').fadeIn(1800);
        $('#textin3').fadeIn(1600);
        $('#text2').fadeIn(1500);
        $('#textin2').fadeIn(1300);
        $('#text1').fadeIn(1000); 
        
    $(document).ready(function(){

        $('#btnsummary').on('click', function () {
             var show = $('#contentsum').html();
             $('.modal-body').html(show);

             
        });
    });
  

var element = $('#contentsum'); // global variable
var getCanvas; // global variable
 
    $('#btn-Preview-Image').on('click', function () {
         html2canvas(element, {
         onrendered: function (canvas) {
                var url = canvas.toDataURL();
                $('#previewImage').attr('src',url);
				console.log(url);
             }
         });
    });
 ");

$this->registerJs(" 
       var bNew = false;
        function getSummaryResult(){ 
        $('#summaryresult > i').addClass('fa-spin');
        $('#summaryresult').attr('title','กำลังดึงข้อมูลล่าสุด อาจใช้เวลาหลายวินาที');
        $.ajax({
            url:'".\yii\helpers\Url::to(['summary-refresh'])."',
            type:'GET',
            data: {refresh:true},
            dataType:'JSON',
            success:function(data){
            
                if(data){
                    console.log(data);    
                    $('#text8').html(numFormat(data.data12)+' ราย');
                    $('#textin8').html('(คิดเป็น '+percentage(((data.data16)/(data.confirm))*100)+'% ของรายที่ผล CT/MRI/MRCP เป็น confirmed ทั้งหมด)');
                    
                    $('#textin7').html('(จากกลุ่ม Confirm CT/MRI ผ่าตัดเพื่อหายขาด '+numFormat(data.data16)+' ราย)');       
                    $('#text7').html('(ผ่าตัดเพื่อหายขาด '+numFormat(data.data13)+' ราย/ ผ่าตัดประคับประคอง '+numFormat(data.data14)+' ราย/ อื่นๆ '+numFormat(data.data15)+' ราย)');
                    
                    $('#text6').html(numFormat(data.data10)+' ราย / '+numFormat(data.data11)+' ครั้ง (positive = '+numFormat(data.postb8)+' ราย)');
                    $('#textin6').html('(คิดเป็น '+percentage(((data.postb8)/(data.data10))*100)+'% ของรายที่ตรวจทางพยาธิวิทยาทั้งหมด)');

                    $('#text5').html(numFormat(data.data8)+' ราย / '+numFormat(data.data9)+' ครั้ง   (confirmed = '+numFormat(data.confirm)+' ราย)');
                    $('#textin5').html('(คิดเป็น '+percentage(((data.confirm)/(data.data8))*100)+'% ของรายที่ตรวจ CT/MRI/MRCP ทั้งหมด)');

                    $('#text4').html(numFormat(data.data6)+' ราย / '+numFormat(data.data7)+' ครั้ง  (suspected = '+numFormat(data.suspect)+' ราย)');
                    $('#textin4').html('(คิดเป็น '+percentage(((data.suspect)/(data.data6))*100)+'% ของรายที่ตรวจอัลตร้าซาวด์ทั้งหมด)');

                    $('#text3').html(numFormat(data.data4)+' ราย / '+numFormat(data.data5)+' ครั้ง (positive = '+numFormat(data.posov)+' ราย)');
                    $('#textin3').html('(คิดเป็น '+percentage(((data.posov)/(data.data4))*100)+'% ของรายที่ตรวจอุจจาระ/ปัสสาวะทั้งหมด)');
    
                    $('#text2').html('ลงทะเบียน '+numFormat(data.data2)+' ราย / ข้อมูลพื้นฐาน '+numFormat(data.data3)+' ราย');
                    $('#textin2').html('(คิดเป็น '+percentage(((data.data3)/(data.data2))*100)+'% ของรายที่ลงทะเบียนทั้งหมด)');
                    
                    $('#text1').html(numFormat(data.data1)+' ราย');
                    $('#calsum').html('[ คำนวนกราฟล่าสุดเมื่อ '+(data.lastupdate)+' ]');
                }
                    bNew = false;
                    $('#summaryresult > i').removeClass('fa-spin');
                    $('#summaryresult').removeAttr('title');
                },
                error   :   function(){
                    $('#contentsum').html('Something Wrong!!');
                    bNew = false;
                    $('#summaryresult > i').removeClass('fa-spin');
                    $('#summaryresult').removeAttr('title');
                            }
            });
        }
   
 ");
$this->registerJs("
        $('#summaryresult').click(function(){
            bNew = true;
            getSummaryResult();
            return false;
        });
");
?>

<style type="text/css">
#getimage img {
    width: 100%;
    height: auto;
   
}
#getimage{
   border: 3px solid #EAEDED  ;
   border-radius: 12px;
}
#contentsum{
   position: relative;
   margin: 5px 0px 5px 0px;
}
#contentmodal{
   position: relative;   
}
.modal-dialog {
  width: 80%;
}
#tallModal .modal-body p { margin-bottom: 900px }
#toptext {
 font-size: 1.50vw;
 color:	#009fff;
text-align: justify;
 margin: 10px 15px 0px 5px;
 padding: 5px 0px 0px 23px;
 
}  
#text8 {
  text-align: justify;
  z-index: 100;
  position: absolute;
  color: black;
  font-weight: bold;
  left: 53%;
  top: 24%;
  display:none;
  font-size: 1.15vw;
 
}  
#textin8{
  text-align: justify;
  z-index: 100;
  position: absolute;
  color: #ff5a1d;
  font-weight: bold;
  left: 53%;
  font-size: 1.02vw;
  top: 34%;
  display:none;
}
#textin7{
  text-align: justify;
  z-index: 100;
  position: absolute;
  color: black;
  font-weight: bold;
  left: 53%;
  font-size: 1.02vw;
  top: 31%;
  display:none;
}
#text7 {
  text-align: justify;
  z-index: 100;
  position: absolute;
  color: black;
  left: 53%;
  font-size: 1.00vw;
  top: 28%;
  display:none;
}

#text6 {
  text-align: justify;
  z-index: 100;
  position: absolute;
  color: black;
  font-weight: bold;
  font-size: 1.15vw;
  left: 53%;
  top: 39%;
  display:none;
} 
#textin6{
  text-align: justify;
  z-index: 100;
  position: absolute;
  color: #ff5a1d;
  font-weight: bold;
  left: 53%;
  font-size: 1.03vw;
  top: 43%;
  display:none;
}
#text5 {
  text-align: justify;
  z-index: 100;
  position: absolute;
  color: black;
  font-weight: bold;
  font-size: 1.15vw;
  left: 53%;
  top: 49%;
  display:none;
} 
#textin5{
  text-align: justify;
  z-index: 100;
  position: absolute;
  color: #ff5a1d;
  font-weight: bold;
  left: 53%;
  font-size: 1.03vw;
  top: 53%;
  display:none;
}
#text4 {
  text-align: justify;
  z-index: 100;
  position: absolute;
  color: black;
  font-weight: bold;
  font-size: 1.15vw;
  left: 53%;
  top: 59%;
  display:none;
}
#textin4{
  text-align: justify;
  z-index: 100;
  position: absolute;
  color: #ff5a1d;
  font-weight: bold;
  left: 53%;
  font-size: 1.03vw;
  top: 63%;
  display:none;
}
#text3 {
  text-align: justify;
  z-index: 100;
  position: absolute;
  color: black;
  font-weight: bold;
  font-size: 1.15vw;
  left: 53%;
  top: 70%;
  display:none;
} 
#textin3{
  text-align: justify;
  z-index: 100;
  position: absolute;
  color: #ff5a1d;
  font-weight: bold;
  left: 53%;
  font-size: 1.03vw;
  top: 74%;
  display:none;
}
#text2 {
  text-align: justify;
  z-index: 100;
  position: absolute;
  color: black;
  font-weight: bold;
  font-size: 1.15vw;
  left: 53%;
  top: 80%;
  display:none;
} 
#textin2{
  text-align: justify;
  z-index: 100;
  position: absolute;
  color: #ff5a1d;
  font-weight: bold;
  left: 53%;
  font-size: 1.03vw;
  top: 84%;
  display:none;
}
#text1 {
  text-align: justify;
  z-index: 100;
  position: absolute;
  color: black;
  font-size: 1.15vw;
  font-weight: bold;
  left: 53%;
  top: 91%;
  display:none;
} 
#calsum{ 
  text-align: justify;
  z-index: 100;
  background-color:#8ae429;
  color: black;
  font-size: 1.3vw;
  font-weight: bold;
  text-align: right;
  padding: 4px 15px 4px 4px;
  border-radius: 2px;  
}
@media (min-width: 1200px) {
  #text7 {
     font-size: 15.5px;
  }
  #text1,#text2,#text3,#text4,#text5,#text6,#text8,#calsum,#textin2,#textin3,#textin4,#textin5,#textin6,#textin7,#textin8 {
     font-size: 16px;
  }
  #toptext{
      font-size: 25px;
  }
}

</style>

    
      

<div align="left" class="alert alert-info" role="alert"><h4>ก. จำแนกตามเวลา</h4></div>
<div id="G1" style="height: 400px; margin: 0 auto"></div>
<div id="G2" style="height: 400px; margin: 0 auto"></div>    
<div align="left" class="alert alert-info" role="alert"><h4>ข. จำแนกตามเขตตรวจราชการสาธารณสุข (ระหว่าง ก.พ. 2556 ถึง ปัจจุบัน)</h4></div>
<div class="row">
  <div class="col-md-6" id="G3" style="height: 400px; margin: 0 auto"></div>
  <div class="col-md-6" id="G4" style="height: 400px; margin: 0 auto"></div>
</div> 
<div align="left" class="alert alert-info" role="alert"><h4>ค. จำแนกตามจังหวัด (ระหว่าง ก.พ. 2556 ถึง ปัจจุบัน)</h4></div>
<div id="G5" style="height: 400px; margin: 0 auto"></div>
<div id="G6" style="height: 400px; margin: 0 auto"></div>
<div align="left" class="alert alert-info" role="alert"><h4>ง. แสดงจำนวนการเข้ารับการรักษา&nbsp;</h4></div>
<div id="G7" style="height: 400px; margin: 0 auto"></div>

</div>
<!--/////////////////////////////////////////////////////////////////////////-->
</div></div></div>

<script>    
function timeout() {
    $(window).resize();
}
window.setTimeout(function() {
    timeout();
},2000);
</script>
<script type="text/javascript">
$(function () {
    $('#G1').highcharts({
        chart: {
            type: 'column'
        },          
        title: {
            text: 'จำนวนกลุ่มเสี่ยงที่มีข้อมูลพื้นฐาน (CCA-01)'
        },/*
        subtitle: {
            text: 'Source: <a href="http://en.wikipedia.org/wiki/List_of_cities_proper_by_population">Wikipedia</a>'
        },*/
        xAxis: {
            type: 'category',
            labels: {
                rotation: -45,
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'จำนวนประชากร(คน)'
            }
        },
        legend: {
            enabled: false
        },
        tooltip: {
            pointFormat: 'จำนวนประชากร: <b>{point.y:.0f} ราย</b>'
        },
        series: [{
            name: 'Population',
            data: [
                <?php
                for($i=0;$i<count($g1data);$i++){
                    echo "['".$g1label[$i]."',".$g1data[$i]."],";
                }
                ?>                
            ],
            dataLabels: {
                enabled: false,
                rotation: -90,
                color: '#FFFFFF',
                align: 'right',
                format: '{point.y:.1f}', // one decimal
                y: 10, // 10 pixels down from the top
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        }]
    });
});        
</script>
<script type="text/javascript">
$(function () {
    $('#G2').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'จำนวนกลุ่มเสี่ยงที่ได้รับการตรวจอัลตราซาวด์ (CCA-02)'
        },/*
        subtitle: {
            text: 'Source: <a href="http://en.wikipedia.org/wiki/List_of_cities_proper_by_population">Wikipedia</a>'
        },*/
        xAxis: {
            type: 'category',
            labels: {
                rotation: -45,
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'จำนวนประชากร(คน)'
            }
        },
        legend: {
            enabled: false
        },
        tooltip: {
            pointFormat: 'จำนวนประชากร: <b>{point.y:.0f} ราย</b>'
        },
        series: [{
            name: 'Population',
            data: [
                <?php
                for($i=0;$i<count($g2data);$i++){
                    echo "['".$g1label[$i]."',".$g2data[$i]."],";
                }
                ?>                
            ],
            color: '#f7a35c',
            dataLabels: {
                enabled: false,
                rotation: -90,
                color: '#FFFFFF',
                align: 'right',
                format: '{point.y:.1f}', // one decimal
                y: 10, // 10 pixels down from the top
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        }]
    });
});        
</script>
<script type="text/javascript">
$(function () {
    $('#G3').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'จำนวนกลุ่มเสี่ยงที่มีข้อมูลพื้นฐาน (CCA-01)'
        },
        /*
        subtitle: {
            text: 'Source: <a href="http://en.wikipedia.org/wiki/List_of_cities_proper_by_population">Wikipedia</a>'
        },
        */
        xAxis: {
            type: 'category',
            labels: {
                rotation: -45,
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'จำนวนประชากร(คน)'
            }
        },
        legend: {
            enabled: false
        },
        tooltip: {
            pointFormat: 'จำนวนประชากร: <b>{point.y:.0f} ราย</b>'
        },
        series: [{
            name: 'Population',
            data: [
                <?php
                for($i=0;$i<count($g3data);$i++){
                    echo "['".$g3label[$i]."',".$g3data[$i]."],";
                }
                ?>            
            ],
            dataLabels: {
                enabled: true,
                rotation: -90,
                color: '#FFFFFF',
                align: 'right',
                format: '{point.y:.0f}', // one decimal
                y: 10, // 10 pixels down from the top
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        }]
    });
});
</script>
<script type="text/javascript">
$(function () {
    $('#G4').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'จำนวนกลุ่มเสี่ยงที่ได้รับการตรวจอัลตราซาวด์ (CCA-02)'
        },
        /*
        subtitle: {
            text: 'Source: <a href="http://en.wikipedia.org/wiki/List_of_cities_proper_by_population">Wikipedia</a>'
        },
        */
        xAxis: {
            type: 'category',
            labels: {
                rotation: -45,
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'จำนวนประชากร(คน)'
            }
        },
        legend: {
            enabled: false
        },
        tooltip: {
            pointFormat: 'จำนวนประชากร: <b>{point.y:.0f} ราย</b>'
        },
        series: [{
            name: 'Population',
            data: [
                <?php
                for($i=0;$i<count($g4data);$i++){
                    echo "['".$g4label[$i]."',".$g4data[$i]."],";
                }
                ?>
            ],
            color: '#f7a35c',
            dataLabels: {
                enabled: true,
                rotation: -90,
                color: '#FFFFFF',
                align: 'right',
                format: '{point.y:.0f}', // one decimal
                y: 10, // 10 pixels down from the top
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        }]
    });
});
</script>
<script type="text/javascript">
$(function () {
    $('#G5').highcharts({
        chart: {
            type: 'column'
        },
        plotOptions: {
            series: {
                colorByPoint: true
            }
        },        
        title: {
            text: 'จำนวนกลุ่มเสี่ยงที่มีข้อมูลพื้นฐาน (CCA-01)'
        },
        /*
        subtitle: {
            text: 'Source: <a href="http://en.wikipedia.org/wiki/List_of_cities_proper_by_population">Wikipedia</a>'
        },
        */
        xAxis: {
            type: 'category',
            labels: {
                rotation: -45,
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'จำนวนประชากร(คน)'
            }
        },
        legend: {
            enabled: false
        },
        tooltip: {
            pointFormat: 'จำนวนประชากร: <b>{point.y:.0f} ราย</b>'
        },
        series: [{
            name: 'Population',
            data: [
                <?php
                for($i=0;$i<count($g5data);$i++){
                    echo "['".$g5label[$i]."',".$g5data[$i]."],";
                }
                ?>            
            ],
            dataLabels: {
                enabled: false,
                rotation: -90,
                color: '#FFFFFF',
                align: 'right',
                format: '{point.y:.0f}', // one decimal
                y: 10, // 10 pixels down from the top
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        }]
    });
});
</script>
<script type="text/javascript">
$(function () {
    $('#G6').highcharts({
        chart: {
            type: 'column'
        },
        plotOptions: {
            series: {
                colorByPoint: true
            }
        },        
        title: {
            text: 'จำนวนกลุ่มเสี่ยงที่ได้รับการตรวจอัลตราซาวด์ (CCA-02)'
        },
        /*
        subtitle: {
            text: 'Source: <a href="http://en.wikipedia.org/wiki/List_of_cities_proper_by_population">Wikipedia</a>'
        },
        */
        xAxis: {
            type: 'category',
            labels: {
                rotation: -45,
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'จำนวนประชากร(คน)'
            }
        },
        legend: {
            enabled: false
        },
        tooltip: {
            pointFormat: 'จำนวนประชากร: <b>{point.y:.0f} ราย</b>'
        },
        series: [{
            name: 'Population',
            data: [
                <?php
                for($i=0;$i<count($g6data);$i++){
                    echo "['".$g6label[$i]."',".$g6data[$i]."],";
                }
                ?>            
            ],
            dataLabels: {
                enabled: false,
                rotation: -90,
                color: '#FFFFFF',
                align: 'right',
                format: '{point.y:.0f}', // one decimal
                y: 10, // 10 pixels down from the top
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        }]
    });
});
</script>
<script type="text/javascript">
$(function () {
    $('#G7').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'แสดงจำนวนผลการตรวจรักษาที่โรงพยาบาลเครือข่ายหลัก 9 แห่ง'
        },
        subtitle: {
            text: 'แยกตามฟอร์มนำเข้าข้อมูล CCA-02.1, CCA-03, CCA-04, CCA-05'
        },
        xAxis: {
            categories: [
                <?php
                for($i=0;$i<count($g7data);$i++){
                    echo "'".$g7data[$i][0]."',";
                }
                ?>
            ],
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: 'จำนวนประชากร(คน)'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:16px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.0f} ราย</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [{
            name: 'CCA-02.1',
            data: [
                <?php
                for($i=0;$i<count($g7data);$i++){
                    echo $g7data[$i][2].",";
                }
                ?>
            ]
        }, {
            name: 'CCA-03',
            data: [
                <?php
                for($i=0;$i<count($g7data);$i++){
                    echo $g7data[$i][3].",";
                }
                ?>
            ]
        }, {
            name: 'CCA-04',
            data: [
                <?php
                for($i=0;$i<count($g7data);$i++){
                    echo $g7data[$i][4].",";
                }
                ?>            
            ]
        }, {
            name: 'CCA-05',
            data: [
                <?php
                for($i=0;$i<count($g7data);$i++){
                    echo $g7data[$i][5].",";
                }
                ?>            
            ]
        }]
    });
});    
</script>   