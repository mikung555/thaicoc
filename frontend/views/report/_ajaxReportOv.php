<?php
    use yii\helpers\Html;

    if(!isset($ovAllZone))exit();
    //var_dump($dataAllZone);
    $lastCal = explode(' ',$lastcal);
    $lastCalDate = explode('-',$lastCal[0]);


//var_dump($thaimonth);
//exit();
//var_dump($ovAllZone);
//return;
//
//    $fromDate = ($fiscalYear-1).'-10-01';
//    $toDate = ($fiscalYear).'-09-30';
//    if(strtotime($fromDate) < strtotime('2013-02-09'))$fromDate = '2013-02-09';
//    if(strtotime($toDate) > strtotime(date('Y-m-d H:i:s')))$toDate = date('Y-m-d');
//
    $fd = explode('-',$getFromDate);
    $td = explode('-',$getToDate);
//
//
    echo '<br/>';
    echo '<div class="col-md-12" style="text-align: center;" id="datarefreshdiv">';

//    if($getType!=null){
//        echo '<div id="excel-data-header-range">';
//        if($getType == 'year'){
//            echo 'แสดงข้อมูลในช่วงปีงบประมาณ '.(intval($str)+543).' (';
//        }else if($getType == 'all'){
//            echo 'แสดงข้อมูลทั้งหมด '.$str.' (';
//        }else if($getType == 'day'){
//            echo 'แสดงข้อมูล'.$str;
//        }
//    }
//
//    echo 'ระหว่างวันที่ '.intval($fd[2]).' '.$thaimonth[intval($fd[1])]['full'].' '.(intval($fd[0])+543);
//    echo ' ถึงวันที่ '.intval($td[2]).' '.$thaimonth[intval($td[1])]['full'].' '.(intval($td[0])+543);
//    if($getType!=null){
//        if($getType == 'year' || $getType == 'month'){
//            echo ')';
//        }
//        echo '</div>';
//    }



    echo '<br/><br/>';
    echo '<div id="excel-data-header-lastcal">';
        echo 'ยอดสรุป ณ วันที่ '.intval($lastCalDate[2]).' '.$thaimonth[intval($lastCalDate[1])]['full'].' '.(intval($lastCalDate[0])+543).' เวลา '.$lastCal[1];
    echo '</div>';
    echo '<br/><br/>';
    echo '<a id="datarefresh" title="">(หากต้องการรายงาน ณ ปัจจุบัน คลิกที่นี่ ซึ่งอาจใช้เวลาหลายนาที)</a><br/><br/>';
    echo '</div>';
    echo '<br/><br/>';

//    var_dump($dataAllZone);
//    exit();

    $sectionColspan = intval((sizeof($allMonthList)*11)+2);
    $headingString = '';
    foreach ($allMonthList as $key => $eachMonth) {
       
        $showMonth = explode('-', $eachMonth['Ym']);
        $headingString.='<td colspan="11" align="center" class="unit-heading">'.
            $thaimonth[intval($showMonth[1])]['full'].' '.(543+intval($showMonth[0])).
        '</td>';
    }
//exit();
    ?>  
        <div class="wrapper1" style="width: 100%; overflow: scroll; cursor: grab; cursor : -o-grab; cursor : -moz-grab; cursor : -webkit-grab;">
                <div class="div1" style="width: 1000%;height: 20px;"></div>
        </div>
        <div class="dragscroll" style="width: 100%; overflow: scroll; cursor: grab; cursor : -o-grab; cursor : -moz-grab; cursor : -webkit-grab;">
    <?php
    echo '<table class="table table-hover table-responsive" id="data-report-table" style="" border="1">';

        echo '<tbody>';
        foreach($ovAllZone as $key => $row){
            if(isset($row['label']) && $row['label']=='รวม'){
                echo '<tr style="color: blue;">';
            }else{
                echo '<tr>';
            }

            foreach($row as $colIndex => $col){


                if( preg_match('/heading/',$colIndex ) ){
                    echo '<td rowspan="2" class="unit-heading">'.$col.'</td>';
                    echo '<td rowspan="2" class="unit-heading">'.Total.'</td>';

                    echo $headingString;

                    echo '</tr>';
                    echo '<tr>';

                    foreach ($allMonthList as $key => $eachMonth) {

                        echo '<td class="unit-heading" style="border-left: 1px solid #AAA">Register</td>';
//                        echo '<td class="unit-heading">ICF</td>';
//                        echo '<td class="unit-heading">CCA-01</td>';
                        echo '<td class="unit-heading">K</td>';
                        echo '<td class="unit-heading">K+</td>';
                        echo '<td class="unit-heading">P</td>';
                        echo '<td class="unit-heading">P+</td>';
                        echo '<td class="unit-heading">F</td>';
                        echo '<td class="unit-heading">F+</td>';
                        echo '<td class="unit-heading">U</td>';
                        echo '<td class="unit-heading">U+</td>';
                        echo '<td class="unit-heading">OV02</td>';
                        echo '<td class="unit-heading">OV03</td>';
                    }

                }
                else if( preg_match('/section/',$colIndex ) ){
                    echo '<th class="unit-section" colspan="'.$sectionColspan.'" style="text-align:left;">'.$col.'</th>';
                }
                else if( preg_match('/separator/',$colIndex ) ){
                    echo '<td class="unit-separator" colspan="'.$sectionColspan.'">'.$col.'</td>';
                }
                else if( preg_match('/typesplit/',$colIndex ) ){
                    echo '<td class="unit-typesplit" colspan="'.$sectionColspan.'">'.$col.'</td>';
                }
                else if( preg_match('/label/',$colIndex ) ){
                    $col = str_replace('โรงพยาบาลส่งเสริมสุขภาพตำบล','รพ.สต.',$col);
                    $col = str_replace('โรงพยาบาล','รพ.',$col);

                    echo '<td class="unit-name" style="border-left: 1px solid #AAA;">'.$col.'</td>';


                }else if( preg_match('/sumRegis/',$colIndex )){
                    echo '<td style="border-left: 1px solid #AAA;">'.number_format($col).'</td>';
                }
                else if(isset($row['summation'])){
                    echo '<td class="unit-sum'.($colIndex == 'summation'?' name':'').'">'.$col.'</td>';
                }else if($col==null){
                    echo '<td style="border-left: 1px solid #AAA;">0</td>';
//                    echo '<td>0</td>';
//                    echo '<td>0</td>';
                    echo '<td>0</td>';
                    echo '<td>0</td>';
                    echo '<td>0</td>';
                    echo '<td>0</td>';
                    echo '<td>0</td>';
                    echo '<td>0</td>';
                    echo '<td>0</td>';
                    echo '<td>0</td>';
                    echo '<td>0</td>';
                    echo '<td>0</td>';
                }else{

                    foreach($col as $keyData => $data){
                        if(preg_match('/provincecode|amphurcode|sitecode/',$keyData ))continue;
                        if(preg_match('/registerCount/',$keyData)){
                            echo '<td style="border-left: 1px solid #AAA;">'.number_format($data).'</td>';
                        }else{
                            echo '<td>'.number_format($data).'</td>';
                        }

                    }

                }

            }
            echo '</tr>';
        }
        echo '</tbody>';
    echo '</table>';
    echo '</div>';

    $this->registerJs('
        $("#datarefresh").click(function(){
            dataMonitorFormData["refresh"] = "true";
            setCheckedZone();
        });
    ');

    $this->registerJS("
$(function(){
  $('.wrapper1').scroll(function(){
    $('.dragscroll').scrollLeft($('.wrapper1').scrollLeft());
  });
  $('.dragscroll').scroll(function(){
    $('.wrapper1').scrollLeft($('.dragscroll').scrollLeft());
  });
});
    ");