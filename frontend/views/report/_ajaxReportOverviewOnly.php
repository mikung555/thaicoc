<?php
    use yii\helpers\Html;
    use common\lib\sdii\components\utils\SDdate;
    if(!isset($dataAll))exit();

?>
    <table class="table table-hover" style="text-align: left;">
        <tbody>
            <?php
            
		echo "<div class=\"alert alert-warning report84-desc\" role=\"alert\"><b>สรุปยอดข้อมูล</b> ณ ".SDdate::$thaiweekFull[date("w")]."ที่ ".date("j")." ".SDdate::$thaimonthFull[date("n")-1]." พ.ศ. ".SDdate::yearEn2yearTh(date("Y"))."	 เวลา ".date("H:i:s")." น.<br>
		<li>มีหน่วยบริการ <font color=red><b>".number_format($dataAllP['All']['All'])."</b></font> แห่ง (ซึ่งเป็น รพศ. รพท. <font color=red><b>".number_format($dataAllP["รพศ."]['All']+$dataAllP["รพท."]['All'])."</b></font> แห่ง; รพช. รพร. <font color=red><b>".number_format($dataAllP["รพช. รพร."]['All'])."</b></font> แห่ง; รพ.สต. <font color=red><b>".number_format($dataAllP["รพ.สต. สถานีอนามัย"]['All'])."</b></font> แห่ง; สสจ., สสอ. <font color=red><b>".($dataAllP["สำนักงานสาธารณะสุขจังหวัด"]['All']+$dataAllP["สำนักงานสาธารณะสุขอำเภอ"]['All'])."</b></font> แห่ง)</li>
                <li>ประชากรจาก TDC รวมทั้งสิ้น <font color=red><b>".number_format($dataAllP['TCC']+$dataAllP['NEMO'])."</b></font> คน</div>";

                foreach($dataAll as $key => $row){ // $units
                    if(preg_match('/section/',$row[0])){ // $key
                        echo '<tr class="tmp-section">';
                        echo '<th>'.$row[1][0].'</th>'; // $row[0]
                        echo '<th style="text-align: right;">'.$row[1][1].'</th>'; // $row[1]
                        echo '<th style="text-align: right;">'.$row[1][2].'</th>'; // $row[2]
                        echo '<th style="text-align: right;">'.$row[1][3].'</th>'; // $row[3]
                        echo '<th style="text-align: right;">'.$row[1][4].'</th>'; // $row[4]
                        echo '<th style="text-align: right;">'.$row[1][5].'</th>';
                        echo '<th style="text-align: right;">'.$row[1][6].'</th>';
                        echo '</tr>';
                        continue;
                    }

                    echo '<tr>';
                    foreach($row[1] as $colIndex => $col){
                        if($colIndex == 0){
                            echo '<td>'.$col.'</td>';
                            continue;
                        }
                        echo '<td style="text-align: right;">'.$col.'</td>';
                    }
                    echo '</tr>';
                }
            ?>
        </tbody>
    </table>  
<?php
    $this->registerCss('
        /*
        div.popover h3.popover-title{
            color: #fff;
            background-color: #00a65a;
        }
        */
        .progress.overall-progress{
            height:30px;
        }
        .report84-bottom{
            margin-top: 20px;
            text-align: center;
        }
        #report84-excel > a{
            color: #4cae4c;
            background-color: #FFF;
        }
        #report84-excel > a:hover{
            color: #fff;
            background-color: #449d44;
        }
        .report84-desc{
            max-height: 230px;
            text-align: justify;
            padding-top: 4px;
            color: #000 !important;
            background-color: #FFFF00 !important;
        }
        #report84-modal-w{
            width : 50%;
        }
        #report84-modal-body{
            text-align : center;
        }
        .close{
            color:#F00 !important;
        }
    ');
        $this->registerCss('
            #data-report-table,
            #cca02-report-table,
            #allproof-report-table
            {
                background-color: #FFFFFF; border-radius: 5px;
                word-break: normal;
                /*transform: rotateX(180deg);*/
            }

            #data-report-table th,
            #cca02-report-table th,
            #allproof-report-table th
            {
                text-align:center !important;
                vertical-align:middle;

            }


            #data-report-table td,#data-report-table th,
            #cca02-report-table td,#cca02-report-table th,
            #allproof-report-table td,#allproof-report-table td
            {
                text-align:right;
                border-left:1px solid #f4f4f4;
            }

            #data-report-div,
            #cca02-report-div,
            #allproof-report-div
            {
                overflow-x:overlay;
                /*transform: rotateX(180deg);*/
                text-align:center;
            }

            .unit-section
            {
                background-color:#000;
                color:#FFF;
            }

            .unit-separator
            {
                background-color:#555;
                color:#FFF;
                text-align:left !important;
            }

            .unit-typesplit
            {
                background-color:#AAA;
                color:#FFF;
                text-align:left !important;
            }

            .unit-name
            {
                text-align:left !important;
            }

            .unit-heading
            {
                text-align:center !important;
                vertical-align:middle !important;
                font-weight:bold;
                background-color:#f5f5f5;
            }

            .unit-sum
            {
                text-align:right !important;
                color:blue;
            }

            .unit-sum.name
            {
                text-align:left !important;
            }

            [id$="-controls"]
            {
                display:none;
            }

            [id$="refresh"]
            {
                cursor:pointer;
                color: #3c8dbc;
            }

            [id$="refresh"]:hover
            {
                color: #72afd2;
            }

            .tmp-section
            {
                background-color:#eee !important;
            }

            .section-text
            {
                text-align: left;
                margin-left: 3%;
            }

            .row
            {
                margin-left: 0 !important;
                margin-right: 0 !important;
            }

            #ov01-report-div{
                overflow-x: auto;
                text-align: center;
            }
        ');
?>    
            