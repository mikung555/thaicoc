<?php
    use yii\helpers\Html;

    if(!isset($cca02AllZone))exit();

    $lastCal = explode(' ',$lastcal);
    $lastCalDate = explode('-',$lastCal[0]);

    $fromDate = ($fiscalYear-1).'-10-01';
    $toDate = ($fiscalYear).'-09-30';
    if(strtotime($fromDate) < strtotime('2013-02-09'))$fromDate = '2013-02-09';
    if(strtotime($toDate) > strtotime(date('Y-m-d H:i:s')))$toDate = date('Y-m-d');

    $fd = explode('-',$fromDate);
    $td = explode('-',$toDate);


    echo '<br/>';
    echo '<div class="col-md-12" style="text-align: center;" id="cca02refreshdiv">';

        echo '<div id="excel-cca02-header-range">';
            echo 'แสดงข้อมูลในช่วงปีงบประมาณ '.($fiscalYear+543);
            echo ' (ระหว่างวันที่ '.intval($fd[2]).' '.$thaimonth[intval($fd[1])]['full'].' '.(intval($fd[0])+543);
            echo ' ถึงวันที่ '.intval($td[2]).' '.$thaimonth[intval($td[1])]['full'].' '.(intval($td[0])+543).')';
        echo '</div>';

        echo '<br/><br/>';

        echo '<div id="excel-cca02-header-lastcal">';
            echo 'ยอดสรุป ณ วันที่ '.intval($lastCalDate[2]).' '.$thaimonth[intval($lastCalDate[1])]['full'].' '.(intval($lastCalDate[0])+543).' เวลา '.$lastCal[1];
        echo '</div>';

        echo '<br/><br/>';
        echo '<a id="cca02refresh" title="">(หากต้องการรายงาน ณ ปัจจุบัน คลิกที่นี่ ซึ่งอาจใช้เวลาหลายนาที)</a><br/><br/>';
    echo '</div>';
    echo '<br/><br/>';

    echo '<table class="table table-hover table-responsive" id="cca02-report-table" style="">';

    echo '<tbody>';
    //\appxq\sdii\utils\VarDumper::dump($cca02AllZone);
    foreach($cca02AllZone as $key => $row){
        echo '<tr>';
        foreach($row as $colIndex => $col){
            if(preg_match('/pct/',$colIndex)) {
                $col = number_format( ($col == null || floatval($col)==0 ? 0 : $col),2);
            }else if(preg_match('/^m[\d]{2}|total|registerCount|regisYearCount/',$colIndex)){
                $col = number_format( ($col == null || intval($col)==0 ? 0 : $col),0);
            }



            if( preg_match('/heading/',$colIndex ) ){
                echo '<td class="unit-heading">'.$col.'</td>';
                echo '<td class="unit-heading" title="ข้อมูลจาก CCA-01">ลงทะเบียน</td>';
                echo '<td class="unit-heading" title="ข้อมูลจาก CCA-01">ลงทะเบียนปีงบนี้</td>';
                echo '<td class="unit-heading">'.$thaimonth[10]['abvt'].' '.($fiscalYear+542).'</td>';
                echo '<td class="unit-heading">'.$thaimonth[11]['abvt'].' '.($fiscalYear+542).'</td>';
                echo '<td class="unit-heading">'.$thaimonth[12]['abvt'].' '.($fiscalYear+542).'</td>';
                echo '<td class="unit-heading">'.$thaimonth[1]['abvt'].' '.($fiscalYear+543).'</td>';
                echo '<td class="unit-heading">'.$thaimonth[2]['abvt'].' '.($fiscalYear+543).'</td>';
                echo '<td class="unit-heading">'.$thaimonth[3]['abvt'].' '.($fiscalYear+543).'</td>';
                echo '<td class="unit-heading">'.$thaimonth[4]['abvt'].' '.($fiscalYear+543).'</td>';
                echo '<td class="unit-heading">'.$thaimonth[5]['abvt'].' '.($fiscalYear+543).'</td>';
                echo '<td class="unit-heading">'.$thaimonth[6]['abvt'].' '.($fiscalYear+543).'</td>';
                echo '<td class="unit-heading">'.$thaimonth[7]['abvt'].' '.($fiscalYear+543).'</td>';
                echo '<td class="unit-heading">'.$thaimonth[8]['abvt'].' '.($fiscalYear+543).'</td>';
                echo '<td class="unit-heading">'.$thaimonth[9]['abvt'].' '.($fiscalYear+543).'</td>';
                echo '<td class="unit-heading">รวม</td>';
                echo '<td class="unit-heading">ร้อยละ</td>';
            }
            else if( preg_match('/section/',$colIndex ) ){
                echo '<th class="unit-section" colspan="17">'.$col.'</th>';
            }
            else if( preg_match('/separator/',$colIndex ) ){
                echo '<td class="unit-separator" colspan="17">'.$col.'</td>';
            }
            else if( preg_match('/typesplit/',$colIndex ) ){
                echo '<td class="unit-typesplit" colspan="17">'.$col.'</td>';
            }
            else if( preg_match('/zone|province|amphur|name/',$colIndex ) ){
                $col = str_replace('โรงพยาบาลส่งเสริมสุขภาพตำบล','รพ.สต.',$col);
                $col = str_replace('โรงพยาบาล','รพ.',$col);
                echo '<td class="unit-name">'.$col.'</td>';
            }
            else if(isset($row['summation'])){
                echo '<td class="unit-sum'.($colIndex == 'summation'?' name':'').'">'.$col.'</td>';
            }else{
                echo '<td>'.$col.'</td>';
            }

        }
        echo '</tr>';
    }
    echo '</tbody>';
    echo '</table>';

    $this->registerJs('
        $("#cca02refresh").click(function(){
            cca02FormData["refresh"] = "true";
            getReportCca02();
        });

        var allRowUnit = $("#cca02-report-table tr:has(.unit-name)");
        allRowUnit.each(function(){
            var thisRow = $(this).eq(0);
            var thisPct = thisRow.children("td:last");
            var thisSum = thisRow.children("td:nth-child(16)").html();

            var sumAmphur = $(this).nextAll(":has(.unit-sum)").eq(0).children("td:nth-child(16)").html();

            thisSum = thisSum.replace(",","");
            sumAmphur = sumAmphur.replace(",","");
            //console.log(thisSum);
            //console.log(sumAmphur);

            var final = (sumAmphur==0?"0.00":((thisSum/sumAmphur)*100).toFixed(2));

            //console.log(final);
            thisPct.html( final );
        });
        delete allRowUnit;
        $("#cca02-report-table tr:has(.unit-sum)").each(function(){
            $(this).children(\'td:last\').html(\'100.00\');
        });
        $("#cca02-report-table tr > td:nth-child(2)").attr("title","ข้อมูลจาก CCA-01");
    ');