<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

?>
<meta name="viewport"></meta>
<style type="text/css">
html, body {
  height: 100%;
  margin: 0;
  padding: 0;
  width: 100%;
}

#Filter {
  background-color: white;
  position: absolute;
  padding: 10px;
  float: right;
  left: 10px;
  top: 10px;  
  width: 250px;
  z-index: 2;
 }
#googft-legend {
  background-color: #fff;
  border: 1px solid #000;
  font-family: Arial, sans-serif;
  font-size: 12px;
  margin: 5px;
  padding: 10px 10px 8px;
}
#googft-legend p {
  font-weight: bold;
  margin-top: 0;
}
#googft-legend div {
  margin-bottom: 5px;
}
.googft-legend-swatch {
  border: 1px solid;
  float: left;
  height: 12px;
  margin-right: 8px;
  width: 20px;
}
.googft-legend-range {
  margin-left: 0;
}
.googft-dot-icon {
  margin-right: 8px;
}
.googft-paddle-icon {
  height: 24px;
  left: -8px;
  margin-right: -8px;
  position: relative;
  vertical-align: middle;
  width: 24px;
}
.googft-legend-source {
  margin-bottom: 0;
  margin-top: 8px;
}
.googft-legend-source a {
  color: #666666;
  font-size: 11px;
}
</style>
<style type="text/css">
label {
    display: inline;
}
 
.regular-checkbox {
    display: none;
}

.regular-checkbox-r {
    display: none;
}
.regular-checkbox-p {
    display: none;
}
.regular-checkbox-g {
    display: none;
}
.regular-checkbox-y {
    display: none;
}
.regular-checkbox-y + label {
    background-color: yellow;
    border: 2px solid #000000;
    box-shadow: 0 1px 2px rgba(0,0,0,0.05), inset 0px -15px 10px -12px rgba(0,0,0,0.05);
    padding: 9px;
    border-radius: 3px;
    display: inline-block;
    position: relative;
}
.regular-checkbox-y:checked + label {
    background-color: yellow;
    border: 2px solid #000000;
    box-shadow: 0 1px 2px rgba(0,0,0,0.05), inset 0px -15px 10px -12px rgba(0,0,0,0.05), inset 15px 10px -12px rgba(255,255,255,0.1);
    color: #99a1a7;
}
.regular-checkbox-y:checked + label:after {
    content: '\2715';
    font-size: 16px;
    font-weight: bold;
    position: absolute;
    top: 0px;
    left: 3px;
    color: #000000;
}

.regular-checkbox-g + label {
    background-color: purple;
    border: 2px solid #000000;
    box-shadow: 0 1px 2px rgba(0,0,0,0.05), inset 0px -15px 10px -12px rgba(0,0,0,0.05);
    padding: 9px;
    border-radius: 3px;
    display: inline-block;
    position: relative;
}
.regular-checkbox-g:checked + label {
    background-color: yellow;
    border: 2px solid #000000;
    box-shadow: 0 1px 2px rgba(0,0,0,0.05), inset 0px -15px 10px -12px rgba(0,0,0,0.05), inset 15px 10px -12px rgba(255,255,255,0.1);
    color: #99a1a7;
}
.regular-checkbox-g:checked + label:after {
    content: '\2715';
    font-size: 16px;
    font-weight: bold;
    position: absolute;
    top: 0px;
    left: 3px;
    color: #000000;
}

.regular-checkbox-p + label {
    background-color: purple;
    border: 2px solid #000000;
    box-shadow: 0 1px 2px rgba(0,0,0,0.05), inset 0px -15px 10px -12px rgba(0,0,0,0.05);
    padding: 9px;
    border-radius: 3px;
    display: inline-block;
    position: relative;
}
.regular-checkbox-p:checked + label {
    background-color: purple;
    border: 2px solid #000000;
    box-shadow: 0 1px 2px rgba(0,0,0,0.05), inset 0px -15px 10px -12px rgba(0,0,0,0.05), inset 15px 10px -12px rgba(255,255,255,0.1);
    color: #99a1a7;
}
.regular-checkbox-p:checked + label:after {
    content: '\2715';
    font-size: 16px;
    font-weight: bold;
    position: absolute;
    top: 0px;
    left: 3px;
    color: #000000;
}

.regular-checkbox-r + label {
    background-color: red;
    border: 2px solid #000000;
    box-shadow: 0 1px 2px rgba(0,0,0,0.05), inset 0px -15px 10px -12px rgba(0,0,0,0.05);
    padding: 9px;
    border-radius: 3px;
    display: inline-block;
    position: relative;
}
.regular-checkbox-r:checked + label {
    background-color: red;
    border: 2px solid #000000;
    box-shadow: 0 1px 2px rgba(0,0,0,0.05), inset 0px -15px 10px -12px rgba(0,0,0,0.05), inset 15px 10px -12px rgba(255,255,255,0.1);
    color: #99a1a7;
}
.regular-checkbox-r:checked + label:after {
    content: '\2715';
    font-size: 16px;
    font-weight: bold;
    position: absolute;
    top: 0px;
    left: 3px;
    color: #000000;
}
.regular-checkbox-g + label {
    background-color: green;
    border: 2px solid #000000;
    box-shadow: 0 1px 2px rgba(0,0,0,0.05), inset 0px -15px 10px -12px rgba(0,0,0,0.05);
    padding: 9px;
    border-radius: 3px;
    display: inline-block;
    position: relative;
}
.regular-checkbox-g:checked + label {
    background-color: green;
    border: 2px solid #000000;
    box-shadow: 0 1px 2px rgba(0,0,0,0.05), inset 0px -15px 10px -12px rgba(0,0,0,0.05), inset 15px 10px -12px rgba(255,255,255,0.1);
    color: #99a1a7;
}
.regular-checkbox-g:checked + label:after {
    content: '\2715';
    font-size: 16px;
    font-weight: bold;
    position: absolute;
    top: 0px;
    left: 3px;
    color: #000000;
}

.regular-checkbox + label {
    background-color: #ffffff;
    border: 2px solid #000000;
    box-shadow: 0 1px 2px rgba(0,0,0,0.05), inset 0px -15px 10px -12px rgba(0,0,0,0.05);
    padding: 9px;
    border-radius: 3px;
    display: inline-block;
    position: relative;
}
 
.regular-checkbox + label:active, .regular-checkbox:checked + label:active {
    box-shadow: 0 1px 2px rgba(0,0,0,0.05), inset 0px 1px 3px rgba(0,0,0,0.1);
}
 
.regular-checkbox:checked + label {
    background-color: #ffffff;
    border: 2px solid #000000;
    box-shadow: 0 1px 2px rgba(0,0,0,0.05), inset 0px -15px 10px -12px rgba(0,0,0,0.05), inset 15px 10px -12px rgba(255,255,255,0.1);
    color: #99a1a7;
}
 
.regular-checkbox:checked + label:after {
    content: '\2715';
    font-size: 16px;
    font-weight: bold;
    position: absolute;
    top: 0px;
    left: 3px;
    color: #000000;
}
 
 
.big-checkbox + label {
    padding: 18px;
}
 
.big-checkbox:checked + label:after {
    font-size: 28px;
    left: 6px;
}
 
.tag {
    font-family: Arial, sans-serif;
    width: 200px;
    position: relative;
    top: 5px;
    font-weight: bold;
    text-transform: uppercase;
    display: block;
    float: left;
}
</style>
  <script src="//code.jquery.com/jquery-1.9.1.js"></script>
  <script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
  

  <script>
  $(function() {
    $( "#Filter" ).draggable();
  });
  </script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?v=3&libraries=visualization,drawing&sensor=false&language=th"></script>
<script type="text/javascript" src="/js/keyzoom.js"></script>
<script type="text/javascript">
  var markers = [];
  function initialize() {
    //var tableId = '1x0X_vUNo1FWB6x5pT5ruBiwJW6zmffuekzrsvMJL';
   var tableId = '1iBBKqXTCFWtOel_qMr99448ko7HkQNaUdSCipR1Z';
    google.maps.visualRefresh = true;
    var isMobile = (navigator.userAgent.toLowerCase().indexOf('android') > -1) ||
      (navigator.userAgent.match(/(iPod|iPhone|iPad|BlackBerry|Windows Phone|iemobile)/));
    if (isMobile) {
      var viewport = document.querySelector("meta[name=viewport]");
      viewport.setAttribute('content', 'initial-scale=1.0, user-scalable=no');
    }
    var mapDiv = document.getElementById('googft-mapCanvas');
    mapDiv.style.width = isMobile ? '100%' : '100%';
    mapDiv.style.height = isMobile ? '100%' : '100%';
    var map = new google.maps.Map(mapDiv, {
      center: new google.maps.LatLng(14.7246005,100.6331108),
      zoom: 7,
	  draggable: true,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
    mapTypeControlOptions: {
      style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR
    },
    panControl: true,
    panControlOptions: {
        position: google.maps.ControlPosition.TOP_RIGHT
    },
	zoomControl: true,
    zoomControlOptions: {
        style: google.maps.ZoomControlStyle.LARGE,
        position: google.maps.ControlPosition.RIGHT_TOP
	  }
    });
	map.enableKeyDragZoom();

    map.controls[google.maps.ControlPosition.RIGHT_BOTTOM].push(document.getElementById('googft-legend-open'));
    map.controls[google.maps.ControlPosition.RIGHT_BOTTOM].push(document.getElementById('googft-legend'));

    layer = new google.maps.FusionTablesLayer({
      map: map,
      heatmap: { enabled: false },
      query: {
        select: "location2",
        from: tableId,
        where: ""
      },
      options: {
        styleId: 3,
        templateId: 3,
      }
    });
/*
var returnGeom = '101.75135,16.67024|101.78193,16.63902|101.83249,16.65944|101.95568,16.63285|101.98066,16.58569|102.04999,16.51777|102.26500,16.46666|102.41414,16.43444|102.45853,16.39965|102.45027,16.35083|102.41054,16.25055|102.32776,16.12695|102.34304,16.02166|102.39197,15.99031|102.36192,15.90042|102.33473,15.80553|102.37025,15.74277|102.41929,15.69833|102.51333,15.71792|102.67665,15.71195|102.78905,15.63937|102.86290,15.63319|102.88985,15.69403|102.89166,15.75277|102.94637,15.78916|102.94109,15.85250|102.91443,15.99055|102.89429,16.03250|102.87109,16.11305|102.85096,16.21291|102.85124,16.27118|102.95888,16.41638|103.09236,16.51444|103.12747,16.55638|103.16251,16.64343|103.15942,16.70638|103.16498,16.75750|103.19123,16.79514|103.18581,16.85833|103.11298,16.91263|102.98915,16.84805|102.94039,16.84194|102.83554,16.87145|102.82805,16.94695|102.71706,17.07485|102.69178,17.03430|102.68414,16.96069|102.61375,16.77290|102.55970,16.76000|102.41541,16.79069|102.33472,16.81888|102.27547,16.89076|102.07106,16.88514|101.98067,16.84153|101.79046,16.76649|101.75135,16.67024'; 
var geomAry = new Array(); 
geomAry = returnGeom.split('|'); 
var XY = new Array(); 
var points = []; 
for (var i = 0; i < geomAry.length; i++) 
{ 
XY = geomAry[i].split(','); 
points.push( new  google.maps.LatLng(parseFloat(XY[1]),parseFloat(XY[0]))) ; 
} 
var dynRegionPolygon = new google.maps.Polygon({paths: points,strokeColor: '#f33f00', strokeWeight: 1, strokeOpacity:1, fillColor:'#ff0000', fillOpacity:0.2}); 
dynRegionPolygon.setMap(map);
*/
/*
var kmzLayer = new google.maps.KmlLayer({
	url: 'http://dhds.nha.co.th/arcgis/rest/services/NHA_RendererAdmin/MapServer/2/query?where=PROV_NAMT%3D%27%E0%B8%82%E0%B8%AD%E0%B8%99%E0%B9%81%E0%B8%81%E0%B9%88%E0%B8%99%27&text=&objectIds=&time=&geometry=&geometryType=esriGeometryEnvelope&inSR=&spatialRel=esriSpatialRelIntersects&relationParam=&outFields=&returnGeometry=true&maxAllowableOffset=&geometryPrecision=&outSR=&returnIdsOnly=false&returnCountOnly=false&orderByFields=&groupByFieldsForStatistics=&outStatistics=&returnZ=false&returnM=false&gdbVersion=&returnDistinctValues=false&f=kmz',
	suppressInfoWindows: true,
	zIndex: 0,
    map: map
  });
//kmzLayer.setMap(map);
*/

    //filterMap(layer, tableId, map);

  var drawingManager = new google.maps.drawing.DrawingManager({
    drawingControl: true,
    drawingControlOptions: {
      position: google.maps.ControlPosition.TOP_CENTER,
      drawingModes: [
        google.maps.drawing.OverlayType.MARKER,
        google.maps.drawing.OverlayType.CIRCLE,
        google.maps.drawing.OverlayType.POLYGON,
        google.maps.drawing.OverlayType.POLYLINE,
        google.maps.drawing.OverlayType.RECTANGLE
      ]
    },
    markerOptions: {
      icon: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABf0lEQVR42mNgwAKspEUNeNhY1pmLCV3QEOQ7wcTEVM9ALDASFsyuNNT6/z894v//zOj//7Oi/98M9/6vJcD35n93CTdezalaqgYVUM1/kkMROCnk/7+MyH/KfDxn8RogwsG+/X96JFjT14Tg/zNsTf6fD3L7/zcl9P8/IG4z0/8frSKnhdMAXSH+myAngwwAaWZgYvrPycb2/38GxNC9fs7/gcq8cBqgwMd9FmYAyGaQ5mhVhf//08KB3gj9v8LV+j8HC4sDTgMkuDlLTwW5gw0AORtkM1gzkP8/O/q/m4zEn////zPiDQdgQN36AtTwGykQ/6dF/O+yMPgPjNZggtEItIHZSETww//MKLgB+32d/msL8NUQnRaCFRTM2831gSEfBkwHUf+1BfnvMpAK1Ph5j//PiPq/GhhwWgICfiQbIMjJFnEYGBNBCjJ/GcgB/xvSuJqMdf6rCvBtYiAX2EqKfRbhZC8m2wABdrZ76vz8oWQbIMXFeTRbS9UcnxoAPzq7Z6dDZg8AAAAASUVORK5CYII='
    },
    circleOptions: {
      fillColor: '#ffff00',
      fillOpacity: 0.2,
      strokeWeight: 3,
      clickable: false,
      editable: true,
      zIndex: 1
    }
  });
  drawingManager.setMap(map);

    google.maps.event.addDomListener(document.getElementById('ptype1'),
        'click', function() {
          filterMap(layer, tableId, map);
    });
    google.maps.event.addDomListener(document.getElementById('ptype2'),
        'click', function() {
          filterMap(layer, tableId, map);
    });
    google.maps.event.addDomListener(document.getElementById('ptype3'),
        'click', function() {
          filterMap(layer, tableId, map);
    });
    google.maps.event.addDomListener(document.getElementById('ptype4'),
        'click', function() {
          filterMap(layer, tableId, map);
    });
/*    
    google.maps.event.addDomListener(document.getElementById('pecho1'),
        'click', function() {
          filterMap2(layer, tableId, map);
    });
    google.maps.event.addDomListener(document.getElementById('pecho2'),
        'click', function() {
          filterMap2(layer, tableId, map);
    });        
    google.maps.event.addDomListener(document.getElementById('pecho3'),
        'click', function() {
          filterMap2(layer, tableId, map);
    });
*/
    google.maps.event.addDomListener(document.getElementById('sethome'),
        'click', function() {
          deleteMarkers();

          var hotsite = document.getElementById("site");
          site.selectedIndex=0;
          var hotsite = document.getElementById("hotsite");
          hotsite.selectedIndex=0;
          
          var center = new google.maps.LatLng(13.7246005,100.6331108);
          map.setZoom(7);
          map.panTo(center);
    });
 
    google.maps.event.addDomListener(document.getElementById('case1'),  
        'click', function() {
          //PTID=1399078172725
          var center = new google.maps.LatLng(15.3189873, 103.8715336);
          map.setZoom(18);
          map.panTo(center);
    });
    google.maps.event.addDomListener(document.getElementById('case2'),
        'click', function() {
          var center = new google.maps.LatLng(17.6289929, 104.2505962 );
          map.setZoom(18);
          map.panTo(center);
    });
    google.maps.event.addDomListener(document.getElementById('case3'),
        'click', function() {
          var center = new google.maps.LatLng(16.4127583, 102.8234899);
          map.setZoom(18);
          map.panTo(center);
    });
    google.maps.event.addDomListener(document.getElementById('case4'),
        'click', function() {
          var center = new google.maps.LatLng(16.4142837, 102.8739405);
          map.setZoom(18);
          map.panTo(center);
    });        
/*
    google.maps.event.addDomListener(document.getElementById('case3'),
        'click', function() {
          var center = new google.maps.LatLng(16.1332024, 103.5986836);
          map.setZoom(18);
          map.panTo(center);
    });
    google.maps.event.addDomListener(document.getElementById('case4'),
        'click', function() {
          var center = new google.maps.LatLng(16.1332024, 103.5986836);
          map.setZoom(18);
          map.panTo(center);
    });        
*/
    google.maps.event.addDomListener(document.getElementById('site'),
        'change', function() {
          deleteMarkers();
          var geocoder = new google.maps.Geocoder();
          
          var hotsite = document.getElementById("hotsite");
          hotsite.selectedIndex=0;
          
          var address = document.getElementById("site").value;
          if (address == "") {
            var center = new google.maps.LatLng(13.7246005,100.6331108);
            map.setZoom(7);
            map.panTo(center);
            return;
          }
          geocoder.geocode( { 'address': address}, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
              map.panTo(results[0].geometry.location);
              map.setZoom(11);
              addMarker(results[0].geometry.location);
            } else {
              alert("Geocode was not successful for the following reason: " + status);
            }
          });          
    });

    google.maps.event.addDomListener(document.getElementById('hotsite'),
        'change', function() {
          deleteMarkers();
          var geocoder = new google.maps.Geocoder();
          
          var site = document.getElementById("site");
          site.selectedIndex=0;          

          var address = document.getElementById("hotsite").value;
          if (address == "") {
            var center = new google.maps.LatLng(13.7246005,100.6331108);
            map.setZoom(7);
            map.panTo(center);
            return;
          }
          geocoder.geocode( { 'address': address}, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
              map.panTo(results[0].geometry.location);
              map.setZoom(11);
              //deleteMarkers();
              addMarker(results[0].geometry.location);              
            } else {
              alert("Geocode was not successful for the following reason: " + status);
            }
          });          
    });

    google.maps.event.addDomListener(document.getElementById('search'),
        'click', function() {
          deleteMarkers();
          var geocoder = new google.maps.Geocoder();
          
          var hotsite = document.getElementById("site");
          site.selectedIndex=0;
          var hotsite = document.getElementById("hotsite");
          hotsite.selectedIndex=0;

          var address = document.getElementById("address").value;
          geocoder.geocode( { 'address': address}, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
              map.panTo(results[0].geometry.location);
              map.setZoom(11);
              //deleteMarkers();
              addMarker(results[0].geometry.location);              
            } else {
              alert("Geocode was not successful for the following reason: " + status);
            }
          });          
    });
    function addMarker(location) {
      var marker = new google.maps.Marker({
        position: location,
        map: map
      });
      markers.push(marker);
    }
    function deleteMarkers() {
      clearMarkers();
      markers = [];
    }
    function clearMarkers() {
      setAllMap(null);
    }
    function setAllMap(map) {
      for (var i = 0; i < markers.length; i++) {
        markers[i].setMap(map);
      }
    }
    
    if (isMobile) {
      var legend = document.getElementById('googft-legend');
      var legendOpenButton = document.getElementById('googft-legend-open');
      var legendCloseButton = document.getElementById('googft-legend-close');
      legend.style.display = 'none';
      legendOpenButton.style.display = 'block';
      legendCloseButton.style.display = 'block';
      legendOpenButton.onclick = function() {
        legend.style.display = 'block';
        legendOpenButton.style.display = 'none';
      }
      legendCloseButton.onclick = function() {
        legend.style.display = 'none';
        legendOpenButton.style.display = 'block';
      }
    }
  }

  function filterMap(layer, tableId, map) {
    var where = generateWhere();

    if (where) {
      if (!layer.getMap()) {
        layer.setMap(map);
      }
      layer.setOptions({
        query: {
          select: 'location2',
          from: tableId,
          where: where
        }
      });
    } else {
      layer.setMap(null);
    }
  }

  function generateWhere() {
    var filter = [];
    var ptypes = document.getElementsByName('ptype');

    for (var i = 0, ptype; ptype = ptypes[i]; i++) {
      if (ptype.checked) {
        var ptypeVal = ptype.value.replace(/'/g, '\\\'');
        filter.push("'" + ptypeVal + "'");
      }
    }
    var where = '';
    if (filter.length) {
      where = "'ptype' IN (" + filter.join(',') + ')';
    }
    return where;
  }
  
  function filterMap2(layer, tableId, map) {
    var where = generateWhere2();

    if (where) {
      if (!layer.getMap()) {
        layer.setMap(map);
      }
      layer.setOptions({
        query: {
          select: 'location2',
          from: tableId,
          where: where
        }
      });
    } else {
      layer.setMap(null);
    }
  }

  function generateWhere2() {
    var filter = [];
    var pechos = document.getElementsByName('pecho');

    for (var i = 0, pecho; pecho = pechos[i]; i++) {
      if (pecho.checked) {
        var pechoVal = pecho.value.replace(/'/g, '\\\'');
        filter.push("'" + pechoVal + "'");
      }
    }
    var where = '';
    if (filter.length) {
      where = " AND 'pecho' IN (" + filter.join(',') + ')';
    }
    return where;
  }  
  google.maps.event.addDomListener(window, 'load', initialize);
  
</script>

</head>

<body>
  <div id="googft-mapCanvas"></div>
  <div id="Filter" style="top: 100px;">
    <img src="https://tools.cascap.in.th/v4cascap/version01/template/cascap_v06/image/cascap_logo_center2.png" width=200><br>
    <img src="https://tools.cascap.in.th/maps/icons/32x32/row%205/14.png" width=24 id=sethome>&nbsp;&nbsp;
    <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABf0lEQVR42mNgwAKspEUNeNhY1pmLCV3QEOQ7wcTEVM9ALDASFsyuNNT6/z894v//zOj//7Oi/98M9/6vJcD35n93CTdezalaqgYVUM1/kkMROCnk/7+MyH/KfDxn8RogwsG+/X96JFjT14Tg/zNsTf6fD3L7/zcl9P8/IG4z0/8frSKnhdMAXSH+myAngwwAaWZgYvrPycb2/38GxNC9fs7/gcq8cBqgwMd9FmYAyGaQ5mhVhf//08KB3gj9v8LV+j8HC4sDTgMkuDlLTwW5gw0AORtkM1gzkP8/O/q/m4zEn////zPiDQdgQN36AtTwGykQ/6dF/O+yMPgPjNZggtEItIHZSETww//MKLgB+32d/msL8NUQnRaCFRTM2831gSEfBkwHUf+1BfnvMpAK1Ph5j//PiPq/GhhwWgICfiQbIMjJFnEYGBNBCjJ/GcgB/xvSuJqMdf6rCvBtYiAX2EqKfRbhZC8m2wABdrZ76vz8oWQbIMXFeTRbS9UcnxoAPzq7Z6dDZg8AAAAASUVORK5CYII=" border=0 id="case1">&nbsp;&nbsp;
    <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABf0lEQVR42mNgwAKspEUNeNhY1pmLCV3QEOQ7wcTEVM9ALDASFsyuNNT6/z894v//zOj//7Oi/98M9/6vJcD35n93CTdezalaqgYVUM1/kkMROCnk/7+MyH/KfDxn8RogwsG+/X96JFjT14Tg/zNsTf6fD3L7/zcl9P8/IG4z0/8frSKnhdMAXSH+myAngwwAaWZgYvrPycb2/38GxNC9fs7/gcq8cBqgwMd9FmYAyGaQ5mhVhf//08KB3gj9v8LV+j8HC4sDTgMkuDlLTwW5gw0AORtkM1gzkP8/O/q/m4zEn////zPiDQdgQN36AtTwGykQ/6dF/O+yMPgPjNZggtEItIHZSETww//MKLgB+32d/msL8NUQnRaCFRTM2831gSEfBkwHUf+1BfnvMpAK1Ph5j//PiPq/GhhwWgICfiQbIMjJFnEYGBNBCjJ/GcgB/xvSuJqMdf6rCvBtYiAX2EqKfRbhZC8m2wABdrZ76vz8oWQbIMXFeTRbS9UcnxoAPzq7Z6dDZg8AAAAASUVORK5CYII=" border=0 id="case2">&nbsp;&nbsp;
    <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABf0lEQVR42mNgwAKspEUNeNhY1pmLCV3QEOQ7wcTEVM9ALDASFsyuNNT6/z894v//zOj//7Oi/98M9/6vJcD35n93CTdezalaqgYVUM1/kkMROCnk/7+MyH/KfDxn8RogwsG+/X96JFjT14Tg/zNsTf6fD3L7/zcl9P8/IG4z0/8frSKnhdMAXSH+myAngwwAaWZgYvrPycb2/38GxNC9fs7/gcq8cBqgwMd9FmYAyGaQ5mhVhf//08KB3gj9v8LV+j8HC4sDTgMkuDlLTwW5gw0AORtkM1gzkP8/O/q/m4zEn////zPiDQdgQN36AtTwGykQ/6dF/O+yMPgPjNZggtEItIHZSETww//MKLgB+32d/msL8NUQnRaCFRTM2831gSEfBkwHUf+1BfnvMpAK1Ph5j//PiPq/GhhwWgICfiQbIMjJFnEYGBNBCjJ/GcgB/xvSuJqMdf6rCvBtYiAX2EqKfRbhZC8m2wABdrZ76vz8oWQbIMXFeTRbS9UcnxoAPzq7Z6dDZg8AAAAASUVORK5CYII=" border=0 id="case3">&nbsp;&nbsp;
    <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABf0lEQVR42mNgwAKspEUNeNhY1pmLCV3QEOQ7wcTEVM9ALDASFsyuNNT6/z894v//zOj//7Oi/98M9/6vJcD35n93CTdezalaqgYVUM1/kkMROCnk/7+MyH/KfDxn8RogwsG+/X96JFjT14Tg/zNsTf6fD3L7/zcl9P8/IG4z0/8frSKnhdMAXSH+myAngwwAaWZgYvrPycb2/38GxNC9fs7/gcq8cBqgwMd9FmYAyGaQ5mhVhf//08KB3gj9v8LV+j8HC4sDTgMkuDlLTwW5gw0AORtkM1gzkP8/O/q/m4zEn////zPiDQdgQN36AtTwGykQ/6dF/O+yMPgPjNZggtEItIHZSETww//MKLgB+32d/msL8NUQnRaCFRTM2831gSEfBkwHUf+1BfnvMpAK1Ph5j//PiPq/GhhwWgICfiQbIMjJFnEYGBNBCjJ/GcgB/xvSuJqMdf6rCvBtYiAX2EqKfRbhZC8m2wABdrZ76vz8oWQbIMXFeTRbS9UcnxoAPzq7Z6dDZg8AAAAASUVORK5CYII=" border=0 id="case4">&nbsp;&nbsp;
    
  
    <br>
    กลุ่มประชากร<hr>
    <div>
      <input type="checkbox" checked="checked" name="ptype"
          id="ptype1" value="1" class="regular-checkbox"><label for="ptype1"></label>
      <label><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAkAAAAJCAYAAADgkQYQAAAAi0lEQVR42mNgQIAoIF4NxGegdCCSHAMzEC+NijL7v3p1+v8zZ6rAdGCg4X+g+EyYorS0NNv////PxMCxsRYghbEgRQcOHCjGqmjv3kKQor0gRQ8fPmzHquj27WaQottEmxQLshubopAQI5CiEJjj54N8t3FjFth369ZlwHw3jQENgMJpIzSc1iGHEwB8p5qDBbsHtAAAAABJRU5ErkJggg=="> รอตรวจอัลตราซาวด์</label>
      <br>
      <input type="checkbox" checked="checked" name="ptype"
          id="ptype2" value="2" class="regular-checkbox"><label for="ptype2"></label>
      <label><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAkAAAAJCAYAAADgkQYQAAAAiElEQVR42mNgQIAoIF4NxGegdCCSHAMzEC81izL7n746/X/VmSowbRho+B8oPhOmKM02zfb/TCzQItYCpDAWpOhA8YFirIoK9xaCFO0FKXrY/rAdq6Lm280gRbeJNikWZDc2RUYhRiBFITDHzwf5LmtjFth3GesyYL6bxoAGQOG0ERpO65DDCQDX7ovT++K9KQAAAABJRU5ErkJggg=="> ผลอัลตราซาวด์ปกติ</label>
      <br>
      <input type="checkbox" checked="checked" name="ptype"
          id="ptype3" value="3" class="regular-checkbox"><label for="ptype3"></label>
      <label><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAkAAAAJCAYAAADgkQYQAAAAi0lEQVR42mNgQIAoIF4NxGegdCCSHAMzEC+NMov6vzp99f8zVWfAdKBh4H+g+EyYorQ027T//2f+x8CxFrEghbEgRQcOFB/Aqmhv4V6Qor0gRQ8ftj/Equh2822QottEmxQLshubohCjEJCiEJjj54N8tzFrI9h36zLWwXw3jQENgMJpIzSc1iGHEwBt95qDejjnKAAAAABJRU5ErkJggg=="> อัลตราซาวด์ผิดปกติ</label>
      <br>
      <input type="checkbox" checked="checked" name="ptype"
          id="ptype4" value="4" class="regular-checkbox"><label for="ptype4"></label>
      <label><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAkAAAAJCAYAAADgkQYQAAAAiklEQVR42mNgQIAoIF4NxGegdCCSHAMzEC+NUlH5v9rF5f+ZoCAwHaig8B8oPhOmKC1NU/P//7Q0DByrqgpSGAtSdOCAry9WRXt9fECK9oIUPXwYFYVV0e2ICJCi20SbFAuyG5uiECUlkKIQmOPng3y30d0d7Lt1bm4w301jQAOgcNoIDad1yOEEAFm9fSv/VqtJAAAAAElFTkSuQmCC"> สงสัยเป็นมะเร็งท่อน้ำดี</label>
      <br>
      <input type="checkbox" checked="checked" name="ptype"
          id="ptype5" value="5" class="regular-checkbox"><label for="ptype5"></label>
      <label><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABf0lEQVR42mNgwAKspEUNeNhY1pmLCV3QEOQ7wcTEVM9ALDASFsyuNNT6/z894v//zOj//7Oi/98M9/6vJcD35n93CTdezalaqgYVUM1/kkMROCnk/7+MyH/KfDxn8RogwsG+/X96JFjT14Tg/zNsTf6fD3L7/zcl9P8/IG4z0/8frSKnhdMAXSH+myAngwwAaWZgYvrPycb2/38GxNC9fs7/gcq8cBqgwMd9FmYAyGaQ5mhVhf//08KB3gj9v8LV+j8HC4sDTgMkuDlLTwW5gw0AORtkM1gzkP8/O/q/m4zEn////zPiDQdgQN36AtTwGykQ/6dF/O+yMPgPjNZggtEItIHZSETww//MKLgB+32d/msL8NUQnRaCFRTM2831gSEfBkwHUf+1BfnvMpAK1Ph5j//PiPq/GhhwWgICfiQbIMjJFnEYGBNBCjJ/GcgB/xvSuJqMdf6rCvBtYiAX2EqKfRbhZC8m2wABdrZ76vz8oWQbIMXFeTRbS9UcnxoAPzq7Z6dDZg8AAAAASUVORK5CYII=">ผู้ป่วยมะเร็งท่อน้ำดี</label>
    </div>
    <br>
    
    <div>
      งานคัดกรองมะเร็งท่อน้ำดีสัญจร<hr>
      <select name="site" id="site">
        <option value="">กรุณาเลือกพื้นที่</option>
<option value="โรงพยาบาลวารินชำราบ อ.วารินชำราบ จ.อุบลราชธานี">ครั้งที่ 1 อ.วารินชำราบ จ.อุบลราชธานี</option>
<option value="โรงพยาบาลหนองหาน อ.หนองหาน จ.อุดรธานี">ครั้งที่ 2 อ.หนองหาน จ.อุดรธานี</option>
<option value="โรงพยาบาลสีชมพู อ.สีชมพู จ.ขอนแก่น">ครั้งที่ 3 อ.สีชมพู จ.ขอนแก่น</option>
<option value="โรงพยาบาลศรีสงคราม อ.ศรีสงคราม จ.นครพนม">ครั้งที่ 4 อ.ศรีสงคราม จ.นครพนม</option>
<option value="โรงพยาบาลชนบท อ.ชนบท จ.ขอนแก่น">ครั้งที่ 5 อ.ชนบท จ.ขอนแก่น</option>
<option value="โรงพยาบาลกมลาไสย">ครั้งที่ 6 อ.กมลาไสย จ.กาฬสินธุ์</option>
<option value="โรงพยาบาลสมเด็จพระยุพราชบ้านดุง อ.บ้านดุง จ.อุดรธานี">ครั้งที่ 7 อ.บ้านดุง จ.อุดรธานี</option>
<option value="โรงพยาบาลจังหาร อ.จังหาร จ.ร้อยเอ็ด">ครั้งที่ 8 อ.จังหาร จ.ร้อยเอ๊ด</option>
<option value="โรงพยาบาลมหาชนะชัย อ.มหาชนะชัย จ.ร้อยเอ็ด">ครั้งที่ 9 อ.มหาชนะชัย จ.ยโสธร (มหาชนะชัย)</option>
<option value="โรงพยาบาลป่าติ้ว ">ครั้งที่ 9 อ.ป่าติ้ว จ.ยโสธร (ป่าติ้ว)</option>
<option value="โรงพยาบาลคำเขื่อนแก้ว">ครั้งที่ 9 อ.คำเขื่อนแก้ว จ.ยโสธร (คำเขื่อนแก้ว)</option>
<option value="โรงพยาบาลกุดชุม อ.กุดชุม จ.ร้อยเอ็ด">ครั้งที่ 9 อ.กุดชุม จ.ยโสธร (กุดชุม)</option>
<option value="โรงพยาบาลยโสธร อ.เมืองยโสธร จ.ร้อยเอ็ด">ครั้งที่ 9 อ.เมือง จ.ยโสธร (เมือง)</option>
<option value="โรงพยาบาลโกสุมพิสัย อ.โกสุมพิสัย จ.มหาสารคาม">ครั้งที่ 10 อ.โกสุมพิสัย จ.มหาสารคาม</option>
<option value="โรงพยาบาลบ้านผือ">ครั้งที่ 11 อ.บ้านผือ จ.อุดรธานี</option>
<option value="โรงพยาบาลเพ็ญ">ครั้งที่ 12 อ.เพ็ญ จ.อุดรธานี</option>
<option value="โรงพยาบาลอุดรธานี">ครั้งที่ 13 อ.เมือง จ.อุดรธานี</option>
<option value="โรงพยาบาลลืออำนาจ">ครั้งที่ 14 อ.ลืออำนาจ จ.อำนาจเจริญ</option>
<option value="โรงพยาบาลมะเร็งอุดรธานี ตำบล หนองไผ่ อุดรธานี">ครั้งที่ 15 อ.เมือง จ.อุดรฯ (รพ.มะเร็งอุดร)</option>
<option value="โรงพยาบาลบ้านไฟ่">ครั้งที่ 16 อ.บ้านไผ่ จ.ขอนแก่น โรงพยาบาลบ้านไผ่</option>
<option value="โรงพยาบาลบรบือ">ครั้งที่ 17 อ.บรบือ จ.สารคาม</option>
<option value="โรงพยาบาลศรีสะเกษ">ครั้งที่ 18 อ.เมือง จ.ศรีสะเกษ</option>
<option value="โรงพยาบาลรัตนบุรี">ครั้งที่ 19 อ.รัตนบุรี จ.สุรินทร์</option>
<option value="โรงพยาบาลคูเมือง">ครั้งที่ 20 อ.คูเมือง จ.บุรีรัมย์</option>
<option value="โรงพยาบาลหนองวัวซอ">ครั้งที่ 21 อ.หนองวัวซอ จ.อุดรธานี</option>
<option value="โรงพยาบาลส่งเสริมสุขภาพตำบลดอนช้าง">ครั้งที่ 22 ดอนช้าง อ.เมือง จ.ขอนแก่น</option>
<option value="โรงพยบาลพุทไธสง">ครั้งที่ 23 อ.พุทไธสง จ.บุรีรัมย์</option>
<option value="โรงพยาบาลขุมพลบุรี">ครั้งที่ 24 อ.ชุมพลบุรี จ.สุรินทร์</option>
<option value="โรงพยาบาลศรีบุญเรือง">ครั้งที่ 25 อ.ศรีบุญเรือง จ.หนองบัวลำภู</option>
<option value="โรงพยาบาลสกลนคร">ครั้งที่ 26 อ.อากาศอำนวย จ.สกลนคร</option>
<option value="เทศบาลนครขอนแก่น">ครั้งที่ 27 เทศบาลนครขอนแก่น</option>
<option value="โรงพยาบาล พรเจริญ">ครั้งที่ 28 อ.พรเจริญ จ.บึงกาฬ</option>
<option value="โรงพยาบาลจตุรัส">ครั้งที่ 29 อ.จตุรัส จ.ชัยภูมิ</option>
<option value="โรงพยาบาลส่งเสริมสุขภาพตำบลบ้านหว้า">ครั้งที่ 30 บ้านหว้า อ.เมือง จ.ขอนแก่น</option>
<option value="โรงพยาบาลท่าตูม">ครั้งที่ 31 อ.ท่าตูม จ.สุรินทร์</option>
<option value="โรงพยาบาลเซกา">ครั้งที่ 32 อ.เซกา จ.บึงกาฬ</option>
<option value="โรงพยาบาลโพนพิสัย">ครั้งที 33 อ.โพนพิสัย จ.หนองคาย</option>
<option value="โรงพยาบาลศูนย์ขอนแก่น">ครั้งที่ 34 เทศบาลเมืองศิลา จ.ขอนแก่น</option>
<option value="โรงพยาบาลศูนย์ขอนแก่น">ครั้งที่ 35 เทศบาลเมืองศิลา จ.ขอนแก่น</option>
<option value="โรงพยาบาลสกลนคร">ครั้งที่ 36 อ.เมือง จ.สกลนคร</option>
<option value="รพ.๕๐ พรรษา มหาวชิราลงกรณ์">ครั้งที่ 37 รพ.๕๐ พรรษา มหาวชิราลงกรณ</option>
      </select>
      <br>
    </div>
    
    
    <div><BR>
      หน่วยงานที่ลงทะเบียนกลุ่มเสี่ยงมาก 40 อันดับแรก<hr>
      <select name="hotsite" id="hotsite">
        <option value="">กรุณาเลือกโรงพยาบาล</option>  
        <option value="โรงพยาบาลเสนางคนิคม อ.เสนางคนิคม จ.อำนาจเจริญ">รพ.เสนางคนิคม จ.อำนาจเจริญ</option>
        <option value="โรงพยาบาลลืออำนาจ อ.ลืออำนาจ จ.อำนาจเจริญ">รพ.ลืออำนาจ จ.อำนาจเจริญ</option>
        <option value="โรงพยาบาลบึงบูรพ์ อ.บึงบูรพ์ จ.ศรีสะเกษ">รพ.บึงบูรพ์ จ.ศรีสะเกษ</option>
        <option value="โรงพยาบาลศรีสงคราม อ.ศรีสงคราม จ.นครพนม">รพ.ศรีสงคราม จ.นครพนม</option>
        <option value="โรงพยาบาลทรายมูล อ.ทรายมูล จ.ยโสธร">รพ.ทรายมูล จ.ยโสธร</option>
        <option value="โรงพยาบาลกุดชุม อ.กุดชุม จ.ยโสธร">รพ.กุดชุม จ.ยโสธร</option>
        <option value="โรงพยาบาลน้ำยืน อ.น้ำยืน จ.อุบลราชธานี">รพ.น้ำยืน จ.อุบลราชธานี</option>
        <option value="โรงพยาบาลชนบท อ.ชนบท จ.ขอนแก่น">รพ.ชนบท จ.ขอนแก่น</option>
        <option value="โรงพยาบาลหนองหาน อ.หนองหาน จ.อุดรธานี">รพ.หนองหาน จ.อุดรธานี</option>
        <option value="สำนักงานสาธารณสุขอำเภอเมืองอำนาจเจริญ อ.เมืองอำนาจเจริญ จ.อำนาจเจริญ">สสอ.อำนาจเจริญ จ.อำนาจเจริญ</option>
        <option value="โรงพยาบาลวารินชำราบ อ.วารินชำราบ จ.อุบลราชธานี">รพ.วารินชำราบ จ.อุบลราชธานี</option>
        <option value="โรงพยาบาลดอนมดแดง อ.ดอนมดแดง จ.อุบลราชธานี">รพ.ดอนมดแดง จ.อุบลราชธานี</option>
        <option value="โรงพยาบาลสีชมพู อ.สีชมพู จ.ขอนแก่น">รพ.สีชมพู จ.ขอนแก่น</option>
        <option value="โรงพยาบาลโนนคูณ อ.โนนคูณ จ.ศรีสะเกษ">รพ.โนนคูณ จ.ศรีสะเกษ</option>
        <option value="โรงพยาบาลสมเด็จพระยุพราชเลิงนกทา อ.เลิงนกทา จ.ยโสธร">รพ.สมเด็จพระยุพราชเลิงนกทา จ.ยโสธร</option>
        <option value="โรงพยาบาลพนา อ.พนา จ.อำนาจเจริญ">รพ.พนา จ.อำนาจเจริญ</option>
        <option value="โรงพยาบาลศรีรัตนะ อ.ศรีรัตนะ จ.ศรีสะเกษ">รพ.ศรีรัตนะ จ.ศรีสะเกษ</option>
        <option value="โรงพยาบาลศรีเมืองใหม่ อ.ศรีเมืองใหม่ จ.อุบลราชธานี">รพ.ศรีเมืองใหม่ จ.อุบลราชธานี</option>
        <option value="โรงพยาบาลขุขันธ์ อ.ขุขันธ์ จ.ศรีสะเกษ">รพ.ขุขันธ์ จ.ศรีสะเกษ</option>
        <option value="โรงพยาบาล๕๐ พรรษา มหาวชิราลงกรณ์ อ.เมืองอุบลราชธานี จ.อุบลราชธานี">รพ.มหาวชิราลงกรณ์ จ.อุบลราชธานี</option>
        <option value="โรงพยาบาลอุทุมพรพิสัย อ.อุทุมพรพิสัย จ.ศรีสะเกษ">รพ.อุทุมพรพิสัย จ.ศรีสะเกษ</option>
        <option value="โรงพยาบาลหนองสูง อ.หนองสูง จ.มุกดาหาร">รพ.หนองสูง จ.มุกดาหาร</option>
        <option value="โรงพยาบาลยโสธร อ.เมืองยโสธร จ.ยโสธร">รพ.ยโสธร จ.ยโสธร</option>
        <option value="โรงพยาบาลศรีสะเกษ อ.เมืองศรีสะเกษ จ.ศรีสะเกษ">รพ.ศรีสะเกษ จ.ศรีสะเกษ</option>
        <option value="โรงพยาบาลกันทรลักษ์ อ.กันทรลักษ์ จ.ศรีสะเกษ">รพ.กันทรลักษ์ จ.ศรีสะเกษ</option>
        <option value="โรงพยาบาลมหาชนะชัย อ.มหาชนะชัย จ.ยโสธร">รพ.มหาชนะชัย จ.ยโสธร</option>
        <option value="โรงพยาบาลไพรบึง อ.ไพรบึง จ.ศรีสะเกษ">รพ.ไพรบึง จ.ศรีสะเกษ</option>
        <option value="โรงพยาบาลยางชุมน้อย อ.ยางชุมน้อย จ.ศรีสะเกษ">รพ.ยางชุมน้อย จ.ศรีสะเกษ</option>
        <option value="โรงพยาบาลอำนาจเจริญ อ.เมืองอำนาจเจริญ จ.อำนาจเจริญ">รพ.อำนาจเจริญ จ.อำนาจเจริญ</option>
        <option value="โรงพยาบาลกันทรารมย์ อ.กันทรารมย์ จ.ศรีสะเกษ">รพ.กันทรารมย์ จ.ศรีสะเกษ</option>
        <option value="โรงพยาบาลนายูง อ.นายูง จ.อุดรธานี">รพ.นายูง จ.อุดรธานี</option>
        <option value="โรงพยาบาลพยุห์ อ.พยุห์ จ.ศรีสะเกษ">รพ.พยุห์ จ.ศรีสะเกษ</option>
        <option value="โรงพยาบาลส่งเสริมสุขภาพตำบลบ้านโพนเมือง ตำบลฟ้าห่วน อ.ค้อวัง จ.ยโสธร">รพสต.โพนเมือง อ.ค้อวัง จ.ยโสธร</option>
        <option value="โรงพยาบาลสิรินธร อ.สิรินธร จ.อุบลราชธานี">รพ.สิรินธร จ.อุบลราชธานี</option>
        <option value="โรงพยาบาลค้อวัง อ.ค้อวัง จ.ยโสธร">รพ.ค้อวัง จ.ยโสธร</option>
        <option value="โรงพยาบาลนาเยีย อ.นาเยีย จ.อุบลราชธานี">รพ.นาเยีย จ.อุบลราชธานี</option>
        <option value="โรงพยาบาลปรางค์กู่ อ.ปรางค์กู่ จ.ศรีสะเกษ">รพ.ปรางค์กู่ จ.ศรีสะเกษ</option>
        <option value="โรงพยาบาลชานุมาน อ.ชานุมาน จ.อำนาจเจริญ">รพ.ชานุมาน จ.อำนาจเจริญ</option>
        <option value="โรงพยาบาลหัวตะพาน อ.หัวตะพาน จ.อำนาจเจริญ">รพ.หัวตะพาน จ.อำนาจเจริญ</option>
        <option value="โรงพยาบาลปทุมราชวงศา อ.ปทุมราชวงศา จ.อำนาจเจริญ">รพ.ปทุมราชวงศา จ.อำนาจเจริญ</option>
      </select>
    </div>

    <div>
    <br>
    ค้นหาสถานที่<hr>
      <input type=text name=address id=address size=30 onkeydown="if (event.keyCode == 13) document.getElementById('search').click()"> <input type=submit name=search id=search value=" Search ">
    </div>
  </div>
