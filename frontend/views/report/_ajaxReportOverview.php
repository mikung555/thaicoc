<?php
    use yii\helpers\Html;
    use common\lib\sdii\components\utils\SDdate;
    if(!isset($dataAll))exit();

?>
    <table class="table table-hover" style="text-align: left;">
        <tbody>
            <?php
            
		echo "<div class=\"alert alert-warning report84-desc\" role=\"alert\"><b>สรุปยอดข้อมูล Isan Cohort</b> ณ ".SDdate::$thaiweekFull[date("w")]."ที่ ".date("j")." ".SDdate::$thaimonthFull[date("n")-1]." พ.ศ. ".SDdate::yearEn2yearTh(date("Y"))."	 เวลา ".date("H:i:s")." น.<br>
		<li>มีหน่วยบริการ <font color=red><b>".number_format($dataAllP['All']['All'])."</b></font> แห่ง (ซึ่งเป็น รพศ. รพท. <font color=red><b>".number_format($dataAllP["โรงพยาบาลศูนย์"]['All']+$dataAllP["โรงพยาบาลทั่วไป"]['All'])."</b></font> แห่ง; รพช. รพร. <font color=red><b>".number_format($dataAllP["โรงพยาบาลชุมชน หรือโรงพยาบาลสมเด็จพระยุพราช"]['All'])."</b></font> แห่ง; รพ.สต. <font color=red><b>".number_format($dataAllP["โรงพยาบาลส่งเสริมสุขภาพประจำตำบล"]['All'])."</b></font> แห่ง; สสจ., สสอ. <font color=red><b>".($dataAllP["สำนักงานสาธารณสุขจังหวัด"]['All']+$dataAllP["สำนักงานสาธารณสุขอำเภอ"]['All'])."</b></font> แห่ง)</li>
                <li>ประชากรจาก TDC รวมทั้งสิ้น <font color=red><b>".number_format($dataAllP['TCC']+$dataAllP['NEMO'])."</b></font> คน; ลงทะเบียนเข้า Isan Cohort (มีข้อมูลพื้นฐานส่วนบุคคล) <font color=red><b>".number_format($dataAllP['Form']['Register'])."</b></font> คน</li>
		<li>มีข้อมูลด้านปัจจัยเสี่ยง (CCA-01) <font color=red><b>".number_format($dataAllP['Form']['CCA-01'])."</b></font> (<font color=red><b>".number_format($dataAllP['Form']['CCA-01']/$dataAllP['Form']['Register']*100,1)."%</b></font>) คน; ตรวจอัลตราซาวด์ <font color=red><b>".number_format($dataAllP['Form']['CCA-02T'])."</b></font> ครั้ง (<font color=red><b>".number_format($dataAllP['Form']['CCA-02P'])."</b></font> คน); สงสัยเป็นมะเร็งท่อน้ำดี <font color=red><b>".number_format($dataAllP['Form']['CCA-02S'])."</b></font> คน (<font color=red><b>".number_format($dataAllP['Form']['CCA-02S']/$dataAllP['Form']['CCA-02P']*100,1)."%</b></font> หรือ <font color=red><b>".number_format($dataAllP['Form']['CCA-02S']/$dataAllP['Form']['CCA-02P']*100000)."/100,000 ประชากร</b></font>)</li></div>";

                foreach($dataAll as $key => $row){ // $units

                    if(preg_match('/section/',$row[0])){ // $key
                        echo '<tr class="tmp-section">';
                        echo '<th>'.$row[1][0].'</th>'; // $row[0]
                        echo '<th style="text-align: right;">'.$row[1][1].'</th>'; // $row[1]
                        echo '<th style="text-align: right;">'.$row[1][2].'</th>'; // $row[2]
                        echo '<th style="text-align: right;">'.$row[1][3].'</th>'; // $row[3]
                        echo '<th style="text-align: right;">'.$row[1][4].'</th>'; // $row[4]
                        echo '<th style="text-align: right;">'.$row[1][5].'</th>';
                        echo '</tr>';
                        continue;
                    }

                    echo '<tr>';
                    foreach($row[1] as $colIndex => $col){
                        if($colIndex == 0){
                            echo '<td>'.$col.'</td>';
                            continue;
                        }
                        echo '<td style="text-align: right;">'.number_format($col).'</td>';
                    }
                    echo '</tr>';
                }
            ?>
        </tbody>
    </table>