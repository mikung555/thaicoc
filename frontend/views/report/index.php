<?php

use kartik\tabs\TabsX;
/* @var $this yii\web\View */
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'รายงาน'), 'url' => ['index']];

$this->title = "Report : " . $siteconfig->sitename;
?>
<?php
    //$debug = false;
    $debug = true;
    if($debug || Yii::$app->keyStorage->get('frontend.domain')=='cascap.in.th')
    {
        echo '<div id="w0" style="display: none;"></div>';
        echo TabsX::widget([
            'items'=>$menuitems,
            'position'=>TabsX::POS_ABOVE,
            'align'=>TabsX::ALIGN_LEFT,
            'encodeLabels'=>false,
            'bordered'=>true,
            //'id'=>'frontend-report-tabx'
        ]);
    }

$jsAdd =<<< JS
var url = "https://cloud.cascap.in.th/project84/report2/index";
$.get(url)
.done(function( data ) {
  $("#project84").html(data);
});
JS;
$this->registerJs($jsAdd);
?>
