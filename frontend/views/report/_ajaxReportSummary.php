<?php
use yii\helpers\Html;

if(!isset($myData)) exit();
/**************** Image summary section ********/
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, "http://61.19.254.16/report2/summary_maker.php?a=".number_format($myData['TCC']+$myData['NEMO'])."&b=".number_format($myData['Register'])."&c=".number_format($myData['CCA-01'])."&d=".number_format($myData['CCA-02P'])."&e=".number_format($myData['CCA-02T'])."&f=".number_format($myData['CTMRI_P'])."&g=".number_format($myData['CTMRI_T'])."&h=".number_format($myData['TREATED_P'])."&i=".number_format($myData['MENU3_P'])."&j=".number_format($myData['MENU3_T'])."&k=".number_format($myData['PHO_P'])."&l=".number_format($myData['PHO_T'])."&m=".number_format($myData['T1_P'])."&n=".number_format($myData['T2_P'])."&o=".number_format($myData['T3_P']));
curl_setopt($ch, CURLOPT_HEADER, 0);
curl_exec($ch);
curl_close($ch);

?>
<div class="well" align="center">
<img class="img-responsive" src="http://61.19.254.16/report2/userdata/summary.png?x=<?=microtime(true);?>">
</div>

<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script> 