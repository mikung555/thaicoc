<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
use kartik\dynagrid\DynaGrid;
use common\lib\sdii\components\utils\SDdate;
use yii\bootstrap\Progress;
    if(isset($model->month15) && !empty($model->month15)){
        echo 'ปี 2015 : ';
        $spc = '';
        foreach ($model->month15 as $key => $value) {
        echo $spc. SDdate::$thaimonthFull[$value];
        $spc = ', ';
        }
        echo '<br>';
    }

    if(isset($model->month16) && !empty($model->month16)){
        echo 'ปี 2016 : ';
        $spc = '';
        foreach ($model->month16 as $key => $value) {
        echo $spc. SDdate::$thaimonthFull[$value];
        $spc = ', ';
        }
        echo '<br>';
    }

    ?>
    <br>
    <div class="alert alert-info" role="alert"><h4>ส่วนที่ 1 : รายงาน Primary prevention</h4></div>
        <?php
    echo DynaGrid::widget([
        'columns' => [
            ['class'=>'kartik\grid\SerialColumn','hAlign'=>'center', 'width'=>'80px','order'=>DynaGrid::ORDER_FIX_LEFT],
            ['attribute'=>'zone','hAlign'=>'center','width'=>'80px'],
            ['attribute'=>'province'],
            ['attribute'=>'amphur'],
            ['attribute'=>'tambon'],
            ['attribute'=>'nall','format'=>'raw','hAlign'=>'right','vAlign'=>'middle'],
            ['attribute'=>'ov','format'=>'raw','hAlign'=>'right','vAlign'=>'middle'],
            ['attribute'=>'ovpos','format'=>'raw','hAlign'=>'right','vAlign'=>'middle'],
            ['attribute'=>'progress',
             'width'=>'200px',
             'value'=> function ($model) {
                return Progress::widget([
                    'percent' => 65,
                    'barOptions' => ['class' => 'progress-bar-danger'],
                    'label' => '65%'
                ]);
             },
             'format'=>'raw','hAlign'=>'right','vAlign'=>'middle'
            ],
//            ['attribute'=>'cca02','format'=>'raw','hAlign'=>'right','vAlign'=>'middle'],
//            ['attribute'=>'suspected','format'=>'raw','hAlign'=>'right','vAlign'=>'middle'],
//            ['attribute'=>'ctmri','format'=>'raw','hAlign'=>'right','vAlign'=>'middle'],
//            ['attribute'=>'treatment','format'=>'raw','hAlign'=>'right','vAlign'=>'middle'],
//            ['attribute'=>'dead','format'=>'raw','hAlign'=>'right','vAlign'=>'middle'],
        ],
        'storage'=>DynaGrid::TYPE_COOKIE,
        'showFilter'=>false,
        'theme'=>'panel-warning',
        'options' => ['id' => 'dynagrid_report84'],
        'gridOptions'=>[
        'pjax'=>true,
            'dataProvider'=>$dataProvider,
            'filterModel'=>$searchModel,
            'showPageSummary'=>false,
            'toolbar'=> [
                [
                'content'=>'',

                ],
            ],
        ],
    ]);
    ?>
    <div class="alert alert-info" role="alert"><h4>ส่วนที่ 2 : รายงาน Secondary prevention</h4></div>
    <?php
    echo DynaGrid::widget([
        'columns' => [
            ['class'=>'kartik\grid\SerialColumn','hAlign'=>'center', 'width'=>'80px','order'=>DynaGrid::ORDER_FIX_LEFT],
            ['attribute'=>'zone','hAlign'=>'center','width'=>'80px'],
            ['attribute'=>'province'],
            ['attribute'=>'amphur'],
            ['attribute'=>'tambon'],
            ['attribute'=>'nall','format'=>'raw','hAlign'=>'right','vAlign'=>'middle'],
    //        ['attribute'=>'ov','format'=>'raw','hAlign'=>'right','vAlign'=>'middle'],
    //        ['attribute'=>'ovpos','format'=>'raw','hAlign'=>'right','vAlign'=>'middle'],
            ['attribute'=>'cca02','format'=>'raw','hAlign'=>'right','vAlign'=>'middle'],
            ['attribute'=>'suspected','format'=>'raw','hAlign'=>'right','vAlign'=>'middle'],
            ['attribute'=>'ctmri','format'=>'raw','hAlign'=>'right','vAlign'=>'middle'],
            ['attribute'=>'treatment','format'=>'raw','hAlign'=>'right','vAlign'=>'middle'],
            ['attribute'=>'dead','format'=>'raw','hAlign'=>'right','vAlign'=>'middle'],
        ],
        'storage'=>DynaGrid::TYPE_COOKIE,
        'showFilter'=>false,
        'theme'=>'panel-warning',
        'options' => ['id' => 'dynagrid_report84'],
        'gridOptions'=>[
        'pjax'=>true,
            'dataProvider'=>$dataProvider,
            'filterModel'=>$searchModel,
            'showPageSummary'=>false,
            'toolbar'=> [
                [
                'content'=>'',

                ],
            ],
        ],
    ]);
?>