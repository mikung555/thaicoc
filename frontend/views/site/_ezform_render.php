<div class="modal-content">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h3 class="modal-title" id="ModalUser"><i class="fa fa-plus" aria-hidden="true"></i> เพิ่มข้อมูล</h3>
    </div>


    <!-- content -->
    <div class="modal-body">
        <div class="row">
        <?php $form = backend\modules\ezforms\components\EzActiveForm::begin(['action' => '/ezform/savedata2','options'=>['name'=> 'frmSaveData', 'enctype'=>'multipart/form-data']]); ?>
        <?php
        foreach ($modelfield as $field) {

            //reference field
            $options_input = [];
            if($field->ezf_field_type == 18) {
                //ถ้ามีการกำหนดเป้าหมาย

                if ($field->ezf_field_val == 1) {
                    //แสดงอย่างเดียวแก้ไขไม่ได้ (Read-only)
                    $modelDynamic = \backend\modules\ezforms\components\EzformQuery::getFormTableName($field->ezf_field_ref_table);
                    //set form disable
                    $options_input['readonly'] = true;
                    $form->attributes['rstat'] = 0;
                } else if ($field->ezf_field_val == 2) {
                    //ถ้ามีการแก้ไขค่า จะอัพเดจค่านั้นทั้งตารางต้นทาง - ปลายทาง
                        $modelDynamic = \backend\modules\ezforms\components\EzformQuery::getFormTableName($field->ezf_id);
                    $form->attributes['rstat'] = $datamodel_table->rstat;
                } else if ($field->ezf_field_val == 3) {
                    //แก้ไขเฉพาะตารางปลายทาง
                    $modelDynamic = \backend\modules\ezforms\components\EzformQuery::getFormTableName($field->ezf_field_ref_table);
                    //\yii\helpers\VarDumper::dump($ezfField, 10, true);
                    //echo $field->ezf_field_ref_field,'-';
                    $options_input['readonly'] = true;
                    $form->attributes['rstat'] = 0;
                }
                $ezfTable = $modelDynamic->ezf_table;

                $ezfField = \backend\modules\ezforms\models\EzformFields::find()->select('ezf_field_name')->where('ezf_field_id = :ezf_field_id', [':ezf_field_id' => $field->ezf_field_ref_field])->one();
                if(!$ezfField->ezf_field_name){
                    $ezfField = \backend\modules\ezforms\models\EzformFields::find()->select('ezf_field_name, ezf_field_label')->where('ezf_field_id = :ezf_field_id', [':ezf_field_id' => $field->ezf_field_id])->one();
                    $html = '<h1>Error</h1><hr>';
                    $html .= '<h3>เนื่องการเชื่อมโยงของประเภทคำถาม Reference field ผิดพลาด การแก้ไขคือ ลบคำถามออกแล้วสร้างใหม่</h3>';
                    $html .= '<h4>คำถามที่ผิดพลาดคือ : </h4>'. $ezfField->ezf_field_name.' ('. $ezfField->ezf_field_label.')';
                    echo $html;
                    Yii::$app->end();
                }
                $modelDynamic = new backend\modules\ezforms\models\EzformDynamic($ezfTable);

                //กำหนดตัวแปร
                $field_name = $field->ezf_field_name; //field name ต้นทาง
                $field_name_ref = $ezfField->ezf_field_name; //field name ปลายทาง

                //ดูตาราง Ezform ฟิลด์ target ที่มีเป้าหมายเดียวกัน
                if ($input_target == 'byme' || $input_target == 'skip' || $input_target == 'all') {
                    $target = \backend\modules\ezforms\components\EzformQuery::getTargetFormEzf($input_ezf_id, $input_dataid);
                    $target = $target['ptid'];
                } else {
                    $target = base64_decode($input_target);
                }
                //หา target ใน site ตัวเองก่อน (ก็ไม่แนะนำ)
                $res = $modelDynamic->find()->select($field_name_ref)->where('ptid = :target AND ' . $field_name_ref . ' <> "" AND xsourcex = :xsourcex AND rstat <>3', [':target' => $target, ':xsourcex' => $datamodel_table->xsourcex])->orderBy('create_date DESC');

                //echo $target.' -';
                //หาที่ target ก่อน
                if ($res->count()) {
                    $model_table = $res->One();
                    if ($field->ezf_field_val == 1) {
                        //echo 'target 1-1<br>';
                        //เปลี่ยนค่า field name ระหว่างต้นทาง และปลายทาง
                        $model_gen->$field_name = $model_table->$field_name_ref;
                        $model_gen->attributes[$field_name] = $model_table->$field_name_ref;
                    } else if ($field->ezf_field_val == 2) {

                    } else if ($field->ezf_field_val == 3) {
                        if ($input_dataid) {
                            //echo 'target 1<br>';
                            $res = \backend\modules\ezforms\components\EzformQuery::getReferenceData($field->ezf_id, $field->ezf_field_id, $input_dataid, $field_name_ref);
                            //\yii\helpers\VarDumper::dump($model_table->attributes, 10,true);
                            if ($res[$field_name_ref]) {
                                $model_table->$field_name_ref = $res[$field_name_ref];
                            }
                            //เปลี่ยนค่า field name ระหว่างต้นทาง และปลายทาง
                            $model_gen->$field_name = $model_table->$field_name_ref;
                            $model_gen->attributes[$field_name] = $model_table->$field_name_ref;
                        } else {
                            //\yii\helpers\VarDumper::dump($model_table->attributes, 10,true);
                            // echo 'target else 1<br>';
                            //เปลี่ยนค่า field name ระหว่างต้นทาง และปลายทาง
                            $model_gen->$field_name = $model_table->$field_name_ref;
                            $model_gen->attributes[$field_name] = $model_table->$field_name_ref;
                        }
                    }

                } else {
                    //กรณีหาไม่เจอ ให้ดู primary key
                    $res = $modelDynamic->find()->where('id = :id', [':id' => $target]);
                    if ($res->count()) {
                        $model_table = $res->One();

                        if ($field->ezf_field_val == 1) {
                            //echo 'target 2-1<br>';
                            //เปลี่ยนค่า field name ระหว่างต้นทาง และปลายทาง
                            $model_gen->$field_name = $model_table->$field_name_ref;
                            $model_gen->attributes[$field_name] = $model_table->$field_name_ref;
                        } else if ($field->ezf_field_val == 2) {

                        } else if ($field->ezf_field_val == 3) {
                            if ($input_dataid) {
                                //echo 'target 2<br>';
                                $res = \backend\modules\ezforms\components\EzformQuery::getReferenceData($field->ezf_id, $field->ezf_field_id, $input_dataid, $field_name_ref);
                                if ($res[$field_name_ref]) {
                                    $model_table->$field_name_ref = $res[$field_name_ref];
                                }
                                //เปลี่ยนค่า field name ระหว่างต้นทาง และปลายทาง
                                $model_gen->$field_name = $model_table->$field_name_ref;
                                $model_gen->attributes[$field_name] = $model_table->$field_name_ref;
                            } else {
                                //echo 'target else 2<br>';
                                //\yii\helpers\VarDumper::dump($model_table->attributes, 10,true);

                                //เปลี่ยนค่า field name ระหว่างต้นทาง และปลายทาง
                                $model_gen->$field_name = $model_table->$field_name_ref;
                                $model_gen->attributes[$field_name] = $model_table->$field_name_ref;
                            }
                        }
                    }
                }

                // yii\helpers\VarDumper::dump($model_table, 10, true);
                //Yii::$app->end();
            }else if($modelEzform->query_tools == 2 && Yii::$app->keyStorage->get('frontend.domain') == "cascap.in.th" && ($input_ezf_id == "1437377239070461301" || $input_ezf_id == "1437377239070461302")){
                //not doing every thing
            }else if($modelEzform->query_tools == 2){
                $form->attributes['rstat'] = $datamodel_table->rstat;
            }
            //end reference field

            echo backend\modules\ezforms\components\EzformFunc::getTypeEform($model_gen, $field, $form, $options_input);
            //echo 'zzz<br>';
        }
        ?>

        </div>

    </div>

    <div class="modal-footer">
        <button id="btn-importFromBot" type="submit" class="btn btn-primary"><i class="glyphicon glyphicon-import"></i> บันทึก</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i> ยกเลิก</button>
    </div>
    <?php backend\modules\ezforms\components\EzActiveForm::end(); ?>

</div>