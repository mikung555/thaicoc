<?php
use backend\models\SiteUrl;
use kartik\tabs\TabsX;
use miloschuman\highcharts\Highcharts;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
$this->title = $siteconfig->sitename;
?>
<div class="site-index">
    <!-- div class="jumbotron" -->
        <?php /* ?>
        <h1>Welcome to <?php echo $siteconfig->sitename;?></h1>

        <h2><?php echo Yii::$app->keyStorage->get('frontend.topic');?></h2>
        <hr>
        <?php */ ?>
        <?php
        /*
         *
         echo common\widgets\DbMenu::widget([
            'key'=>'frontend-index',
            'options'=>[
                'tag'=>'p'
            ]
        ]);
        */
        ?>

    <!-- /div -->
    
<?php
//    echo "<br>############################################################<br>";
//
//    echo "<br>############################################################<br>";

    $domain=Yii::$app->keyStorage->get('frontend.domain');
    $siteprefix= trim(str_replace($domain, "", $_SERVER['HTTP_HOST']));
    $siteprefix= trim(str_replace(".", "",$siteprefix));
    if (is_numeric(substr($siteprefix,0,1))) {
        $sitecode=$siteprefix+0;
    }else{
        $sitecode=$siteprefix;
    }
    if ($sitecode==0) $sitecode="";
    $sqlSiteURL = "SELECT code,`url`,`name` FROM site_url WHERE status in (0,3) AND (lower(code)=lower('".($sitecode)."') or lower(url)=lower('".$siteprefix."'))";
    $dataSiteURL = Yii::$app->db->createCommand($sqlSiteURL)->queryOne();
    
    if($dataSiteURL['url']!="" && $dataSiteURL['code']!='0'){
        $frontendUrl=$dataSiteURL['url'].".".Yii::$app->keyStorage->get('frontend.domain');
        echo '<span class="fa fa-home"></span> ';
        echo str_pad($dataSiteURL['code'],5,"0",STR_PAD_LEFT) ." : " . $dataSiteURL['name']." ";
        echo '<span class="fa fa-globe"></span> ';
        if (Yii::$app->keyStorage->get('ssl.enable')=="1") {
            $http="https";
        }else{
            $http="http";
        }        
        echo \yii\helpers\BaseHtml::a("{$http}://".$frontendUrl, "{$http}://".$frontendUrl);
    }
    else if ($dataSiteURL['code']!="") if ($dataSiteURL['code']!="0") {
        $sqlHospital = "SELECT `code`,`name` FROM const_hospital WHERE code+0='".($siteprefix+0)."' ";
        $dataHospital = Yii::$app->db->createCommand($sqlHospital)->queryOne();
        $frontendUrl=$dataSiteURL['url'].".".Yii::$app->keyStorage->get('frontend.domain');
        echo '<span class="fa fa-home"></span> ';
        echo str_pad($dataSiteURL['code'],5,"0",STR_PAD_LEFT) ." : " . $dataHospital['name']." ";
        echo '<span class="fa fa-globe"></span> ';
        echo \yii\helpers\BaseHtml::a("http://".$frontendUrl, "http://".$frontendUrl);        
    }
    if(isset($menuitems)){
	echo TabsX::widget([
	    'items'=>$menuitems,
	    'position'=>TabsX::POS_ABOVE,
	    'align'=>TabsX::ALIGN_RIGHT,
	    'encodeLabels'=>false,
	    'bordered'=>true
	]);
    } else{
	?>
	<div class="alert alert-info" role="alert" style="font-size: 20px;"> 
	    <strong>OOPS!</strong> ไม่พบไซต์นี้
	</div>
	<?php
    }
    
    
?>
    <br>
</div>
<div class="site-index vertical-center">
    <?php echo \common\widgets\DbCarousel::widget([
        'key'=>'index'
    ]) ?>


    <div class="body-content">

        <?php if(isset($_SESSION['__id'])): ?>
            <div class="row">
                <div class="col-md-6" >
                    <?php
//                    var_dump($table2month['count']);
                    if(!is_null($table2month['count'])) {
                        echo Highcharts::widget([
                            'scripts' => [
                                'modules/exporting',
                                'themes/default',
                            ],
                            'options' => $table2month['options'],
                        ]);
                    }
                    ?>
                </div>
                <div class="col-md-6" >
                    <?php
//                    var_dump($table3month['count']);
                    if(!is_null($table3month['count'])) {
                        echo Highcharts::widget([
                            'scripts' => [
                                'modules/exporting',
                                'themes/default',
                            ],
                            'options' => $table3month['options'],
                        ]);
                    }
                    ?>
                </div>
            </div>
        <?php else: ?>
            <div class="row">
                <div class="col-md-6" >
                    <?php
//                    var_dump($query['count']);
                    if(!is_null($query['count'])) {
                        echo Highcharts::widget([
                            'scripts' => [
                                'modules/exporting',
                                'themes/default',
                            ],
                            'options' => $query['options'],
                        ]);
                    }
                    //        echo "<pre>";
                    //        print_r(Yii::$app->user->identity->userProfile);
                    ?>
                </div>
                <div class="col-md-6" >
                    <?php
//                    var_dump($query3['count']);
                    if(!is_null($query3['count'])) {
                        echo Highcharts::widget([
                            'scripts' => [
                                'modules/exporting',
                                'themes/default',
                            ],
                            'options' => $query3['options'],
                        ]);
                    }
                    ?>
                </div>
            </div>
        <?php endif; ?>

        <div class="row">
            <div class="col-md-6" >
                <?php
//                var_dump($graphSiteOrder['count']);
                if(!is_null($graphSiteOrder['count'])) {
                    echo Highcharts::widget([
                        'scripts' => [
                            'modules/exporting',
                            'themes/default',
                        ],
                        'options' => $graphSiteOrder['options'],
                    ]);
                }
                ?>
            </div>
            <div class="col-md-6" >
                <?php
//                var_dump($graphSiteOrder2['count']);
                if(!is_null($graphSiteOrder2['count'])) {
                    echo Highcharts::widget([
                        'scripts' => [
                            'modules/exporting',
                            'themes/default',
                        ],
                        'options' => $graphSiteOrder2['options'],
                    ]);
                }
                ?>
            </div>
        </div>
        <br />

        <div class="row">
            <div class="col-md-6">
                <?php if(isset($memberRegis)): ?>
                    <div class="panel panel-default">
                        <div class="panel-heading" style="text-align:center;"><label>สถิติสมาชิกและจำนวนข้อมูล</label></div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th>ประเภทหน่วยบริการ</th>
                                        <th>ทั้งหมด</th>
                                        <th>ปีนี้</th>
                                        <th>เดือนนี้</th>
                                        <th>สัปดาห์นี้</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    foreach ($memberRegis as $key => $hospitalType) {
                                        echo '<tr>';
                                        echo '<td>'.($key=='central'?'โรงพยาบาลศูนย์':($key=='general'?'โรงพยาบาลทั่วไป':($key=='rural'?'โรงพยาบาลชุมชน':'โรงพยาบาลสริมสุขภาพตำบล'))).'</td>';
                                        echo '<td>'.$hospitalType['all'].'</td>';
                                        echo '<td>'.$hospitalType['year'].'</td>';
                                        echo '<td>'.$hospitalType['month'].'</td>';
                                        echo '<td>'.$hospitalType['week'].'</td>';
                                        echo '</tr>';
                                    }
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </div>







            <!---->
<!--        <div class="row">-->
<!--            <div class="col-lg-4">-->
<!--                <h2>Heading</h2>-->
<!---->
<!--                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et-->
<!--                    dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip-->
<!--                    ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu-->
<!--                    fugiat nulla pariatur.</p>-->
<!---->
<!--                <p><a class="btn btn-default" href="/doc/">เอกสารสำคัญ &raquo;</a></p>-->
<!--            </div>-->
<!--            <div class="col-lg-4">-->
<!--                <h2>Heading</h2>-->
<!---->
<!--                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et-->
<!--                    dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip-->
<!--                    ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu-->
<!--                    fugiat nulla pariatur.</p>-->
<!---->
<!--                <p><a class="btn btn-default" href="/forum/">กระดานข่าว &raquo;</a></p>-->
<!--            </div>-->
<!--            <div class="col-lg-4">-->
<!--                <h2>Heading</h2>-->
<!---->
<!--                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et-->
<!--                    dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip-->
<!--                    ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu-->
<!--                    fugiat nulla pariatur.</p>-->
<!---->
<!--                <p><a class="btn btn-default" href="/extensions/">แลกเปลี่ยน เรียนรู้ &raquo;</a></p>-->
<!--            </div>-->
<!--        </div>-->

    </div>
</div>
<img src="<?=  Url::to(['site/send-mail']) ?>">
