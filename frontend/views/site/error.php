<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

$this->title = $name;
?>
<div class="site-error">

    <div class="alert alert-danger">
	<?php 
			$code = property_exists($exception, 'statusCode') ? $exception->statusCode : 500;
			$msg = Yii::$app->keyStorage->get('error_403');
			if($code==403 && !empty($msg)){
			    echo $msg; 
			} else {
			    echo nl2br(Html::encode($message));
			}
                    ?>
    </div>
</div>
