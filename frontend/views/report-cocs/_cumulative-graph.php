
<?php
use miloschuman\highcharts\Highcharts;
use miloschuman\highcharts\HighchartsAsset;
use appxq\sdii\widgets\GridView;
use yii\helpers\Url;
use yii\helpers\Html;
use appxq\sdii\widgets\ModalForm;
use appxq\sdii\utils\ToDate;

if($sitename != null){
    $name = "<h3>หน่วยงาน ".$sitename."</h3>";
}

if($startDate != null || $endDate != null){
    $parseStartDate = ToDate::ToThaiDate($startDate);
        $parsEndeDate = ToDate::ToThaiDate($endDate);
    $name .= "<br/><h4>วันที่ " . $parseStartDate['d'].' '.$parseStartDate['m'].' '.$parseStartDate['y'] . " ถึง " . $parsEndeDate['d'].' '.$parsEndeDate['m'].' '.$parsEndeDate['y']."</h4>";
}
//$name = "ประจำเดือน มิถุนายน ปี พ.ศ. 2560";
HighchartsAsset::register($this)->withScripts(['highstock', 'modules/exporting', 'modules/drilldown', 'highcharts-more']);
$this->registerJs("
    console.log('year $year');
    Highcharts.chart('cumulative-graph', {
    chart: {
        type: 'spline'
    },
    title: {
        text: ''
    },
    subtitle: {
        text: ''
    },
    xAxis: {
        type: 'datetime',
        labels: {
            overflow: 'justify'
        }
    },
    yAxis: {
        title: {
            text: 'จำนวน'
        },
        minorGridLineWidth: 2,
        gridLineWidth: 2,
        alternateGridColor: null
       
    },
    tooltip: {
        valueSuffix: ' N'
    },
    plotOptions: {
        spline: {
            lineWidth: 4,
            states: {
                hover: {
                    lineWidth: 5
                }
            },
            marker: {
                enabled: false
            },
            pointInterval: 86400000, // one day
            pointStart: Date.UTC('2014', '$month', '$day', 0, 0, 0)
        }
    },
    series: [{
        name: 'จำนวนเข้ารักษา',
        color:'#2E9AFE',
        data:[$dataCare]

    }, {
        name: 'จำนวนผู้ป่วย',
        color:'#8258FA',
        data:[$dataPatient]
    }],
    navigation: {
        menuItemStyle: {
            fontSize: '10px'
        }
    }
});
    ");


echo "<div class='col-md-12' id='cumulative-graph'></div>";
