<?php

use yii\helpers\Html;
use yii\helpers\Url;
use common\lib\sdii\components\utils\SDdate;
?>

<div class="table-responsive" id="container" style="text-align: center;">
    <i class="fa fa-spinner fa-pulse fa-3x"></i>
</div>
<?php
$this->registerJs("
    $(document).ready(function(){
        console.log('Graph Loading...');
        var contain = $('#container');
         $.ajax({
                url:'" . Url::to(['report-cocs/cumulative-graph']) . "',
                method:'POST',
                data: {
                    module_id:'$module_id'
                },
                dataType:'HTML',
                success:function(result){
                   contain.empty();
                   contain.html(result);
                },error: function (xhr, ajaxOptions, thrownError) {
                   console.log(xhr);
                }
            });
    });
    ");
