<?php

use yii\helpers\Html;
use yii\helpers\Url;
use common\lib\sdii\components\utils\SDdate;

$this->title = "Report Thai Care Cloud";
?>


<div class="row">
    <div class="col-md-12">
        <h3><i class="fa fa-cloud" aria-hidden="true"></i> Overview of Modules</h3>
        <div class="table-responsive" id="modules-report" style="text-align: center;">
            <i class="fa fa-spinner fa-pulse fa-3x"></i>
        </div>
    </div>
</div>
<hr style="border: 1.5px solid #AAA;width:95%;">
 
<?php
$this->registerJs("
    $(function(){
        divModuls = $('#modules-report');
            $.ajax({
                url:'" . Url::to(['report-cocs/modules-report']) . "',
                method:'POST',
                data: 'id',
                dataType:'HTML',
                success:function(result){
                   divModuls.empty();
                   divModuls.html(result);
                },error: function (xhr, ajaxOptions, thrownError) {
                   console.log(xhr);
                }
            });
            return false;
        }); 
        
 
        $('#tab3').click(function(){
            $('#tab-one').removeClass('active');
            $('#tab-two').removeClass('active');
            $('#tab-four').removeClass('active');
            $('#tab-three').addClass('active');
        });

        function showModuleReport(mod_id){
            $('#tab-one').removeClass('active');
            $('#tab-two').removeClass('active');
            $('#tab-three').removeClass('active');
            $('#tab-four').addClass('active');
            
            $('#output-tab').html('<center><i class=\"fa fa-spinner fa-pulse fa-3x fa-fw\"></i></center>');
             $.ajax({
                url:'" . Url::to(['report-cocs/show-module-report']) . "',
                method:'POST',
                data: {
                    module_id:mod_id
                },
                dataType:'HTML',
                success:function(result){
                   //console.log(result);
                   $('#output-tab').html(result); 
                },error: function (xhr, ajaxOptions, thrownError) {
                   console.log(xhr);
                }
            });
            return false;
        }
");

?>