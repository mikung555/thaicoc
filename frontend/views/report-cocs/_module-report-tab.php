<?php

use yii\helpers\Html;
use yii\helpers\Url;
use common\lib\sdii\components\utils\SDdate;
use kartik\widgets\Select2;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;
?>

<div class="row">
    <h3><p class="text-center">เลือกตัวกรอง</p></h3> 
    <div class="col-md-12">
        <div class="col-md-5">
            <label >หน่วยงาน</label>
            <?php
            echo Select2::widget([
                'name' => 'sitecode',
                'id' => 'sitecode',
                'options' => ['placeholder' => 'เลือกหน่วยงาน...'],
                'pluginOptions' => [
                    'allowClear' => true,
                    'minimumInputLength' => 3, //ต้องพิมพ์อย่างน้อย 3 อักษร ajax จึงจะทำงาน
                    'ajax' => [
                        'url' => 'report-cocs/get-site',
                        'dataType' => 'json', //รูปแบบการอ่านคือ json
                        'data' => new JsExpression('function(params) { return {q:params.term}; }'),
                    ],
                    'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                    'templateResult' => new JsExpression('function(city) { return city.text; }'),
                    'templateSelection' => new JsExpression('function (city) { return city.text; }'),
                ]
            ]);
            ?>
        </div>
        <div class="col-md-3">
            <label class="control-label">เริ่มวันที่</label>
            <div class="input-group date">
                <span class="input-group-addon kv-date-calendar" title="Select date"><i class="glyphicon glyphicon-calendar"></i></span>
<input type="date" id="startdate" class="form-control" name="startdate" value="<?= date($begin_date); ?>" data-datepicker-type="2" data-krajee-datepicker="datepicker_b6ea203c">
            </div>

        </div>
        <div class="col-md-3">
            <label class="control-label">ถึงวันที่</label>
            <div class="input-group date">
                <span class="input-group-addon kv-date-calendar" title="Select date"><i class="glyphicon glyphicon-calendar"></i></span>
                <input type="date" id="endstart" class="form-control" name="enddate" value="<?= date('Y-m-d'); ?>" data-datepicker-type="2" data-krajee-datepicker="datepicker_b6ea203c">
            </div> 
        </div>
        <div class="col-md-1">
            <?=
            Html::button('<i class="fa fa-search"></i> ค้นหา', [
                'class' => 'btn btn-primary',
                'id' => 'btnSearch',
                'style' => 'margin-top:25px;'
            ]);
            ?>
        </div>

    </div>

</div>

<div class="row">
    <div class="col-md-12">
        <h3><i class="fa fa-area-chart" aria-hidden="true"></i> Forms Report </h3>
        <div class="table-responsive" id="forms-report" style="text-align: center;">
<?= $this->renderAjax('_modules_part1_2', ['module_id' => $module_id]); ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <h3><i class="fa fa-area-chart" aria-hidden="true"></i> Cumulative Curve</h3>
        <div class="table-responsive" id="cucurve-report" style="text-align: center;">
<?= $this->renderAjax('_modules_part2', ['module_id' => $module_id]); ?>
       
        </div> </div> </div>
<hr style="border: 1.5px solid #AAA;width:95%;">

            <div class="row">
    <div class="col-md-12">
        <h3><i class="fa fa-globe" aria-hidden="true"></i> MAP</h3>
        <div class="table-responsive" id="maptdc-report" style="text-align: center;">
<?php echo $this->renderAjax('_map') ?>
        </div>
    </div>
</div>
 
<hr style="border: 1.5px solid #AAA;width:95%;">
<div class="row">
    <div class="col-md-12">
        <h3><i class="fa fa-history" aria-hidden="true"></i> Timeline</h3>
        <div class="table-responsive" id="timeline-report" style="text-align: center;">
<?= $this->renderAjax('_modules_part3', ['module_id' => $module_id]); ?>   
        </div>
    </div>
</div>
<hr style="border: 1.5px solid #AAA;width:95%;">

<?php
$this->registerJs("
        
//        $('#show_timeline').ready(function(){
//                 var data ={
//                     module_id: '$module_id',
//                    startdate: $('#startdate').val(),   
//                    enddate: $('#endstart').val()
//                 };
//                var url = '" . Url::to(['report-cocs/timeline']) . "';
//                getTimeline(url,data);
//        });
        

    
        $('#btnSearch').click(function(){
        console.log('Click');
        var div_report = $('#container-forms-report');
        div_report.empty();
        div_report.html('<i class=\'fa fa-spinner fa-pulse fa-3x\'></i>');
        var sitecode = $('#sitecode').val();
        searchCenter(sitecode);
        console.log(sitecode);
            $.ajax({
                url:'" . Url::to(['report-cocs/forms-report']) . "',
                method:'POST',
                data:{
                    sitecode:sitecode,
                    module_id:'$module_id',
                    startdate: $('#startdate').val(),   
                    enddate: $('#endstart').val(),
                },
                dataType:'HTML',
                success:function(data){
                    div_report.empty();
                    div_report.html(data);
                },error: function (xhr, ajaxOptions, thrownError) {
                   console.log(xhr);
                }
            });
            
        var div_graph = $('#cumulative-graph');
        div_graph.empty();
        div_graph.html('<i class=\'fa fa-spinner fa-pulse fa-3x\'></i>');
            $.ajax({
                url:'" . Url::to(['report-cocs/cumulative-graph']) . "',
                method:'POST',
                data:{
                    sitecode:sitecode,
                    module_id:'$module_id',
                    startdate: $('#startdate').val(),   
                    enddate: $('#endstart').val(),
                },
                dataType:'HTML',
                success:function(data){
                    div_graph.empty();
                    div_graph.html(data);
                },error: function (xhr, ajaxOptions, thrownError) {
                   console.log(xhr);
                }
            });
            

            //timeline
            
                var data ={
                     module_id: '$module_id',
                    startdate: $('#startdate').val(),   
                    enddate: $('#endstart').val()
                 };
                var url = '" . Url::to(['report-cocs/timeline']) . "';
                getTimeline(url,data);
                

        });
        
        function pageSet(page){
            var data ={
                    module_id: '$module_id',
                    startdate: $('#startdate').val(),   
                    enddate: $('#endstart').val(),
                    page:page
                 };
                var url = '" . Url::to(['report-cocs/timeline']) . "';
                getTimeline(url,data);
        }
        
        function getTimeline(url,data){
        
            $('#show_timeline').html('<i class=\"fa fa-spinner fa-pulse fa-fw fa-3x\"></i>');
             $.ajax({
                url:url,
                method:'POST',
                dataType:'json',
                data:data,
                success:function(result){
                  
                    $('#show_timeline').html(result.timeline); 
                    $('#dataPagination').html(result.page);
                    
                    //ConcatTimeLine(); //ไป concat เอา icon 
               }
            });
        }
    ");
?>