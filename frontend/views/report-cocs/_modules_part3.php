<?php
    use yii\helpers\Html;
    use yii\bootstrap\ActiveForm;
    use yii\helpers\Url;
    use common\lib\sdii\components\utils\SDdate; 
    use kartik\widgets\DatePicker;
    use common\models\TimelineEvent;
    
?>
<?php
    $form = ActiveForm::begin([
        'id' => 'ChktimelineSearch',
        
    ]);
?>

<!--<div class="row">
    <div id="s_timeline" class="form-inline col-md-12 col-sm-9" style="margin-bottom: 30px; float:left; text-align: center;">
        <h3><p class="text-center">เลือกตัวกรอง</p></h3> 
        <label style="margin-top: 7px; width: 60px;">เริ่มวันที่</label>
        <div class="input-group date">
            <span class="input-group-addon kv-date-calendar" title="Select date"><i class="glyphicon glyphicon-calendar"></i></span>
            <input type="date" id="s_timeline-startdate" class="form-control" name="s_timeline-startdate" value="<?=  date('Y-m-d'); ?>" data-datepicker-type="2" data-krajee-datepicker="datepicker_b6ea203c">
        </div>
        <label style="margin-top: 7px;">ถึงวันที่</label>
        <div class="input-group date">
            <span class="input-group-addon kv-date-calendar" title="Select date"><i class="glyphicon glyphicon-calendar"></i></span>
            <input type="date" id="s_timeline-endstart" class="form-control" name="s_timeline-enddate" value="<?=  date('Y-m-d'); ?>" data-datepicker-type="2" data-krajee-datepicker="datepicker_b6ea203c">
        </div>            
        <br><br> 
        <label  style="margin-top: 7px; width: 60px;">เวลา</label>
        <div class="input-group time">
            <span class="input-group-addon kv-time" title="Select time"><i class="fa fa-clock-o" aria-hidden="true"></i></span>
            <input type="time" id="s_timeline-starttime" class="form-control" name="s_timeline-starttime">
        </div>
        <label  style="margin-top: 7px;">ถึงเวลา</label>
        <div class="input-group time">
            <span class="input-group-addon kv-time" title="Select time"><i class="fa fa-clock-o" aria-hidden="true"></i></span>
            <input type="time" id="s_timeline-endtime" class="form-control" name="s_timeline-endtime"  >
        </div>            
        <br><br>

        <?// Html::button('<i class="fa fa-search"></i> ค้นหา', [
            'class' => 'btn btn-primary',
            'id' => 'btntimelineSearch',
        ]); ?>
    </div>
</div>-->
 <?php ActiveForm::end() ?>
<hr>
<div class="row showtimeline"  id="show_timeline">
    
</div>
<hr>
<div id="dataPagination">
</div>

<!--    <div class="scorebar" id="show_timeline"></div>-->



<?php
    $this->registerCss("
//    .showtimeline{
//        height: 800px;
//    }
//    #timeline-report{
//        overflow-x: hidden;
//    }
//    .scorebar{
//        overflow: scroll;
//        height: 800px;
//            
//    }
 
"); 
?>

<?php
    $this->registerJS("
        
        $(document).ready(function(){
                 var data ={
                     module_id: '$module_id',
                    startdate: $('#start').val(),   
                    enddate: $('#start').val()
                 };
                var url = '" . Url::to(['report-cocs/timeline']) . "';
                getTimeline(url,data);
        });
        

function getTimeline(url,data){
        
            $('#show_timeline').html('<i class=\"fa fa-spinner fa-pulse fa-fw fa-3x\"></i>');
             $.ajax({
                url:url,
                method:'POST',
                dataType:'json',
                data:data,
                success:function(result){
                  console.log(result);
                    $('#show_timeline').html(result.timeline); 
                    $('#dataPagination').html(result.page);
                    
                    //ConcatTimeLine(); //ไป concat เอา icon 
               },
               error:function(err, text){
                console.log(err);
               }
            });
        }
    


            
        
        

        
    ");
?>

