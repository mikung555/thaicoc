<?php

use common\lib\sdii\components\utils\SDdate;

// appxq\sdii\utils\VarDumper::dump($module_id);
?>
<?php
$date = '';

if ($result_timeline != NULL) {
    ?>
    <div class="col-md-12 scorebar">
        <ul class="timeline">
            <?php
            foreach ($result_timeline as $value) {              
                if ($date != $value['date']) {
                    $date = $value['date'];
                    ?>
                    <li class="time-label" style="text-align: left;">
                        <span class="bg-blue"><?php echo SDdate::mysql2phpThDateSmall($value['date']); ?></span>
                    </li>

        <?php }  
                if($module_id == '46'){
                    if($value['ezf_id'] == '1462172833035880400'){ 
                        $color = "background: rgba(92, 151, 168, 0.53)!important;"; 

                    } else {
                        $color = "background: rgba(30, 241, 4, 0.53)!important;"; 
                    }
                }else{
                     $color = "#f1f1f1;"; 
                }  
        ?>
                <li>
                    <div class="timeline-item"  style="<?=$color;?>">
                        <span class="times" style="float: left;padding:10px;color:black;"><i class="fa fa-clock-o"></i> <?= $value['time']; ?> | </span>
                        <div class='timeline-header' style='text-align: left;color:black;'><?= $value['data']; ?></div>
                            
                    </div>                    
                </li>
    <?php } ?>
    <?php // }  ?>
            <li>
                <i class="fa fa-clock-o">
                </i>
            </li>
        </ul>

    </div>
<?php } else if ($result_timeline == '' || $result_timeline == NULL) { ?>
    <div class="col-md-12">
        <ul class="timeline">
            <li class="time-label" style="text-align: left;">
                <span class="bg-blue"> <?php echo date("d-m-Y"); ?></span>
            </li>
            <li>
                <div class="timeline-item" style="background: #f1f1f1;" >
                    <h3 class="timeline-header" style="text-align: left;">ไม่พบข้อมูล  </h3>
                </div>                    
            </li>
            <li>
                <i class="fa fa-clock-o">
                </i>
            </li>
        </ul>
    </div>

<?php } ?>