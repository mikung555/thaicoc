<?php

use yii\helpers\Url;
use yii\helpers\Html;

$this->title = "Report ThaiCareCloud"; 
?>
<br>

<ul class="nav nav-tabs">
    <li id="tab-one" class="active"><a data-toggle="tab" href="#overview" id="tab1"><i class="fa fa-line-chart fa-lg"></i> Overview</a></li>
    <li id="tab-two"><a data-toggle="tab" href="#modules" id="tab2"><i class="fa fa-desktop fa-lg"></i> HIS Monitor</a></li>
    <li id="tab-three"><a data-toggle="tab" href="#modules" id="tab3"><i class="fa fa-archive fa-lg"></i> Modules</a></li>
    <li id="tab-four" class="disabled"><a href="#module-report" id="tab4" disabled="true"><i class="fa fa-bar-chart fa-lg"></i> Modules Report</a></li>
</ul>
<div class="tab-content">
<div id="output-tab"></div>
</div>
<?php
    $this->registerJs("
        show_overview();
        $('#tab1').click(function(){
            $('#tab-one').addClass('active');
            show_overview();
            $('#tab-two').removeClass('active');
            $('#tab-three').removeClass('active');
            $('#tab-four').removeClass('active');
        });

        function show_overview(){
            $('#output-tab').html('<center><i class=\"fa fa-spinner fa-pulse fa-3x fa-fw\"></i></center>');
             $.ajax({
                url:'" . Url::to(['report-cocs/show-overview']) . "',
                method:'POST',
                data: 'id',
                dataType:'HTML',
                success:function(result){
                   //console.log(result);
                   $('#output-tab').html(result); 
                },error: function (xhr, ajaxOptions, thrownError) {
                   console.log(xhr);
                }
            });
            return false;
        }

        $('#tab2').click(function(){
            
            $.ajax({
                url:'".Url::to(['/hismonitor'])."',
                method:'get',
                success:function(data){
                    $('#output-tab').html(data);
                }
            });
            
            $('#tab-one').removeClass('active');
            $('#tab-four').removeClass('active');
            $('#tab-three').removeClass('active');
            $('#tab-two').addClass('active');
            
        });
        
        $('#tab3').click(function(){
            $('#tab-one').removeClass('active');
            $('#tab-four').removeClass('active');
            $('#tab-three').addClass('active');
            $('#tab-two').removeClass('active');
            show_modules();
        });

        
        function show_modules(){
            $('#output-tab').html('<center><i class=\"fa fa-spinner fa-pulse fa-3x fa-fw\"></i></center>');
             $.ajax({
                url:'" . Url::to(['report-cocs/show-modules']) . "',
                method:'POST',
                data: 'id',
                dataType:'HTML',
                success:function(result){
                   $('#output-tab').html(result); 
                },error: function (xhr, ajaxOptions, thrownError) {
                   console.log(xhr);
                }
            });
            return false;
        }
        
        function show_monitor(){
            
        }
       
    ");

    $this->registerCss('

    .tab-content {
        border-left: 1px solid #ddd;
        border-right: 1px solid #ddd;
        padding: 25px;
    }
    
    ');
?>
