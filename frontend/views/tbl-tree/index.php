<?php

use kartik\tree\TreeView;
use kartik\tree\TreeViewInput;
use frontend\models\TblTree;

echo \kartik\tree\TreeView::widget([
    'query' => TblTree::find()->addOrderBy('root, lft'),
    'headingOptions' => ['label' => 'หมวดกิจกรรม'],
    'rootOptions' => ['label'=>'<span class="text-primary">Root</span>'],
    'fontAwesome' => true,
    'isAdmin' => true,
    'displayValue' => 1,
    'iconEditSettings'=> [
        'show' => 'text',
        'listData' => [
            'folder' => 'กิจกรรม',
            'file' => 'File',
            'mobile' => 'Phone',
            'bell' => 'Bell',
        ]
    ],
    'softDelete' => true,
    'cacheSettings' => ['enableCache' => true]

]);

echo "<br><br>";

//echo TreeViewInput::widget([
//    // single query fetch to render the tree
//    // use the Product model you have in the previous step
//    'query' => TblTree::find()->addOrderBy('root, lft'), 
//    'headingOptions'=>['label'=>'Categories'],
//    'name' => 'kv-product', // input name
//    'value' => '1,2,3',     // values selected (comma separated for multiple select)
//    'asDropdown' => true,   // will render the tree input widget as a dropdown.
//    'multiple' => true,     // set to false if you do not need multiple selection
//    'fontAwesome' => true,  // render font awesome icons
//    'rootOptions' => [
//        'label'=>'<i class="fa fa-home"></i>',  // custom root label
//        'class'=>'text-success'
//    ], 
//    //'options'=>['disabled' => true],
//]);
?>
