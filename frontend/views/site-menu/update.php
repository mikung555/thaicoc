<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\SiteMenu */

?>
<div class="site-menu-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model, 
        'modelmainmenu' => $modelmainmenu,
        'modelmenu'=>$modelmenu,
    ]) ?>

</div>
