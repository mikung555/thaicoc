<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\SiteMenu */
/*
$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Site Menus', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
 * 
 */
?>
<div class="site-menu-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'code' => $model->code, 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'code' => $model->code, 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'code',
            'id',
            'parentid',
            'name',
            'content:ntext',
            'active',
            'order',
        ],
    ]) ?>

</div>
