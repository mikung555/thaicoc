<?php

use yii\helpers\Html;

//yii\helpers\Json::encode()
/* @var $this yii\web\View */
/* @var $model frontend\models\SiteMenu */


?>
<div class="site-menu-create">

    <h1><?= Html::encode('สร้างเมนูใหม่') ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modelmainmenu' => $modelmainmenu,
        'modelmenu'=>$modelmenu,
    ]) ?>

</div>
