<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use backend\modules\ezforms\components\EzformFunc;
use kartik\social\FacebookPlugin;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model frontend\models\SiteMenu */
/*
$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Site Menus', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
 * 
 */

?>
<div class="site-menu-view">
<?php
$sharesetting = [
    'data-href'   =>  "/?id=".$model->id,
    'data-layout' =>  "button_count",
    'data-size'   =>  "small",  
    'data-mobile-iframe'  =>  "true",
];

echo FacebookPlugin::widget(['type'=>FacebookPlugin::SHARE, 'settings' => $sharesetting]);  
$permlink = Url::base(true) . Url::toRoute(['site/index', 'id' => $model->id]);
?>
    <?php echo Html::a('<p class="btn btn-xs btn-info">Permanent Link</p> ' . $permlink,$permlink); ?>
    <br><?php echo FacebookPlugin::widget(['type'=>FacebookPlugin::LIKE, 'settings' => ['data-href'=>$permlink,'data-layout'=>'box_count']]);?>
    <h1><?= Html::encode($this->title) ?></h1>
<?php
    $domain=Yii::$app->keyStorage->get('frontend.domain');
    $siteprefix= trim(str_replace($domain, "", $_SERVER['HTTP_HOST']));
    $siteprefix= trim(str_replace(".", "",$siteprefix));
    $sitecode=$siteprefix+0;
    if ($sitecode==0) $sitecode="";
    $sqlSiteURL = "SELECT code,`url`,`name`, status FROM site_url WHERE code='".($sitecode)."' or url='".$siteprefix."'";
    $dataSiteURL = Yii::$app->db->createCommand($sqlSiteURL)->queryOne();
    
    $sitecode = 0;
    if($dataSiteURL && isset($dataSiteURL['code'])){
	$sitecode = $dataSiteURL['code'];
    }
    $mySite = Yii::$app->user->identity->userProfile->sitecode;
    if (Yii::$app->user->can('administrator') || (EzformFunc::chekauth('adminsite') &&  $mySite == $sitecode) || (EzformFunc::chekauth('frontend') &&  $mySite == $sitecode) ) {
	if($dataSiteURL['status']==3){
	?>
    <p class="text-right">
        <?= Html::a('&nbsp;&nbsp;&nbsp;', ['site-menu/update', 'code' => $model->code, 'id' => $model->id], ['class' => 'fa fa-pencil-square-o fa-2x']) ?>
        <?php if ($model->active != 1) echo Html::a('&nbsp;&nbsp;&nbsp;', ['site-menu/delete', 'code' => $model->code, 'id' => $model->id], [
            'class' => 'fa fa-trash-o fa-2x',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        <?= Html::a('&nbsp;&nbsp;&nbsp;', ['site-menu/create', 'code' => $model->code], ['class' => 'fa fa-plus fa-2x']) ?>
    </p>
    <?php
    }
    }
    
    ?>
    <?php echo $model->content;?>

</div>
