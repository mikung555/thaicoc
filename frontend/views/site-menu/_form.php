<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $model frontend\models\SiteMenu */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="site-menu-form">
    <?php
    //asort($modelmenu->order);
    if ($model->order == "") $model->order=10000;

    $orderitems = ArrayHelper::merge(ArrayHelper::map($modelmenu, 'order', 'name'), [10000=>'ท้ายสุด']);
    
    foreach ($modelmenu as $key => $menu) {
        if ($menu->parentid != 0) $orderitems[$menu->order]="+++++".$menu->name;
    }

    $submenuitems = ArrayHelper::merge([0=>"เป็นเมนูหลัก"],ArrayHelper::map($modelmainmenu, 'id', 'name'));
    ?>
    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    
    <?= $form->field($model, 'parentid')->dropDownList($submenuitems) ?>
    

    <?= $form->field($model, 'order')->dropDownList($orderitems) ?>

    
    <div class="row" style="position: relative;">
    <?= $form->field($model, 'content')->widget(dosamigos\tinymce\TinyMce::className(),[
	    'options' => ['rows' => 20],
	    'language' => 'th_TH',
	    'clientOptions' => [
		    'fontsize_formats' => '8pt 9pt 10pt 11pt 12pt 26pt 36pt',
		    'plugins' => [
			    "advlist autolink lists link image charmap print preview hr anchor pagebreak",
			    "searchreplace wordcount visualblocks visualchars code fullscreen",
			    "insertdatetime media nonbreaking save table contextmenu directionality",
			    "emoticons template paste textcolor colorpicker textpattern",
		    ],
		    'toolbar' => "undo redo | styleselect fontsizeselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media | forecolor backcolor emoticons",
		    'content_css' => Yii::getAlias('@backendUrl').'/css/bootstrap.min.css',
		    'image_advtab' => true,
		    'filemanager_crossdomain' => true,
		    'external_filemanager_path' => Yii::getAlias('@storageUrl').'/filemanager/',
		    'filemanager_title' => 'Responsive Filemanager',
		    'external_plugins' => array('filemanager' => Yii::getAlias('@storageUrl').'/filemanager/plugin.min.js')
	    ]
    ]) ?>
    </div>
    

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
