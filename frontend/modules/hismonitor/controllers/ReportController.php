<?php

namespace frontend\modules\hismonitor\controllers;
use Yii;
use yii\web\Controller;

class ReportController extends Controller
{
    public function actionIndex()
    {
//        $values = 1;
//        $key = "(01) person";
//        echo (int)substr($key,1,2);
//        //exit();
        $org = $person = \frontend\modules\hismonitor\classest\ReportFunc::getOrgCount();
        $person = \frontend\modules\hismonitor\classest\ReportFunc::getCount();
        $count_patient = \frontend\modules\hismonitor\classest\ReportFunc::getCountPatient(); 
        $sum_patint = \frontend\modules\hismonitor\classest\ReportFunc::getSum('','',"SUM(npatient) AS sum_patint");
        $count_tdc = \frontend\modules\hismonitor\classest\ReportFunc::getCountTDC();
        $count_all_hospital_th = \frontend\modules\hismonitor\classest\ReportFunc::getCount("SELECT count(*) FROM all_hospital_thai where code2 not like '%demo%'and (code4 <> 15 and code4 <> 16 AND code4 <> 80 )");
        
        return $this->renderAjax('index',[
             'org'=>$org,
             'person'=>$person,
             'count_patient'=>$count_patient,
             'sum_patint'=>$sum_patint,
             'count_tdc'=>$count_tdc,
             'count_all_hospital_th'=>$count_all_hospital_th
        ]);
    }
    public function getSumTdcCount($condition=""){
        $sql = "SELECT $condition FROM buffe_tdc_count";
        $buffe_tdc_count = \Yii::$app->db->createCommand($sql)->queryAll();
        return $buffe_tdc_count;
    }
    public function actionTdcCountAll(){
        //echo 'TEST';exit();
        $query = \common\lib\nut\Lib::ArrayToObject($this->getSumTdcCount("sum(item) as sum "));
        $sum=0;
        foreach($query as $q){
            $sum += $q->sum;
        }
        return $this->renderAjax('tdc-count-all',[
            'sum'=>$sum
        ]);
    }
    public function actionTdcCount(){
        if(\Yii::$app->request->isAjax){
            
        $search = \Yii::$app->request->get("search","");
        $condition=""; 
        $params=[];
        if(!empty($search) || $search != ""){
            $condition=" AND `pac`.`sitecode` LIKE '%$search%' OR  `aht`.`name` LIKE '%$search%' ";
            
            // echo $condition;exit();
        }
       $query = \frontend\modules\hismonitor\classest\ReportFunc::getTdcCount($condition);
         
        $dataProvider = new \yii\data\ArrayDataProvider([
            'allModels' => $query,
            'pagination'=>[
                'pageSize'=>1000
            ]
        ]);
        return $this->renderAjax('tdc-count',[
            'dataProvider'=>$dataProvider
        ]);
        } else {
            return $this->redirect(['/tdc-monitor']);
        }
    }
    public function getSql($sql="", $condition=""){
        return $sql = "SELECT * FROM buffe_tdc_count $condition GROUP BY table_name";
    }
    public function actionDetail(){
         
        $sitecode = \Yii::$app->request->get("hcode");
  
        $sql="
             SELECT '".$sitecode."' as sitecode
            ,IFNULL((SELECT bc.table_name from buffe_tdc_count bc WHERE bc.table_name = f43.f43 and sitecode = '".$sitecode."'),f43.f43) as `table_name`
            ,IFNULL((SELECT bc.`row` from buffe_tdc_count bc WHERE bc.table_name = f43.f43 and sitecode = '".$sitecode."'),0) as `row`
            ,IFNULL((SELECT bc.`field` from buffe_tdc_count bc WHERE bc.table_name = f43.f43 and sitecode = '".$sitecode."'),0) as `field`
            ,IFNULL((SELECT bc.`item` from buffe_tdc_count bc WHERE bc.table_name = f43.f43 and sitecode = '".$sitecode."'),0) as `item`

            from buffe_data.ezform_map_f43_tdc f43 ORDER BY `item` DESC 

           ";
        $sql="
            SELECT 
                '".$sitecode."' as sitecode
                ,n.`ezf_name` as `table_name`
                ,IFNULL((SELECT bc.`row` from buffe_tdc_count bc WHERE bc.table_name = f.f43 and sitecode = '".$sitecode."'),0) as `row`
                ,IFNULL((SELECT bc.`field` from buffe_tdc_count bc WHERE bc.table_name = f.f43 and sitecode = '".$sitecode."'),0) as `field`
                ,IFNULL((SELECT bc.`item` from buffe_tdc_count bc WHERE bc.table_name = f.f43 and sitecode = '".$sitecode."'),0) as `item`
                from `ezform` n
                INNER JOIN `ezform_map_f43_tdc` f on f.`ezf_id` = n.`ezf_id` ORDER BY n.`ezf_name` asc
        ";
         //create talbe xxx(sitecode,tablename,rows,fields,items)
            $query = \Yii::$app->db->createCommand($sql)->queryAll(); 
            $dataProvider = new \yii\data\ArrayDataProvider([
                'allModels' => $query,
                'pagination'=>[
                    'pageSize'=>500
                ]
            ]);
            return $this->renderAjax('detail',[
                'dataProvider'=>$dataProvider
            ]);
 
        
 
    }
    public function actionSumTable(){
        $condition="";
        $model = \frontend\modules\hismonitor\classest\ReportFunc::getTdcCount($condition);
        return $this->renderAjax("_sumtable",['model'=>$model]);
    }
}
