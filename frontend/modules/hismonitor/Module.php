<?php

namespace frontend\modules\hismonitor;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'frontend\modules\hismonitor\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
