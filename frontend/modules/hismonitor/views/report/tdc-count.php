<?php 
    use kartik\grid\GridView;
    use kartik\grid\SerialColumn;
?>
<?php \yii\widgets\Pjax::begin(['id' => 'hismornitor','timeout'=>5000]) ?>
 

 
<div class="clearfix"></div
<?php echo GridView::widget([
      'dataProvider' => $dataProvider,
      //'filterModel' => $searchModel,
     'showPageSummary' => true,
    'id'=>'grid-hismonitor',
      'columns' => [
           [
               'class' => \frontend\modules\hismonitor\classest\SerialColumns::className(), 'header' => 'ลำดับที่' 
               
           ],
          
//            [
//                'label'=>'test',
//                'attribute'=>'table_name',
//                'value'=>function($model){
//                   // \appxq\sdii\utils\VarDumper::dump($model);
//                    $ezf_name = frontend\modules\hismonitor\classest\ReportFunc::getTableName($model['hnames']);
//                    return $model["hnames"];
//                }
//            ],
           [
              
             'label'=>'ชื่อโรงพยาบาล',   
             'format'=>'raw',
             'attribute'=>'hnames',
             'value'=>function($model){
                $model = \common\lib\nut\Lib::ArrayToObject($model);
                return \yii\helpers\Html::a($model->hcodes." : ".$model->hnames,'#',[
                    'onClick'=>"return ShowDetail('".$model->hcodes."')"
                ]);
             }
           ],
           [
             //'pageSummary' => true,
             'label'=>'จำนวนผู้รับบริการ',  
             'attribute'=>'count_patient',
             'format'=>'raw',
             'format'=>'decimal',
              'value'=>function($model){
                $sql = "SELECT SUM(`npatient`) FROM `buffe_patient_count` WHERE `sitecode`=:sitecode";
                return $query = \Yii::$app->db->createCommand($sql,[":sitecode"=>$model['hcodes']])->queryScalar();
              },
                       
               
           ],
           [
               
               'label'=>'จำนวนตาราง',
               'attribute'=>'count_table',
               'format'=>'decimal',
               
           ],
           [
                
             'label'=>'จำนวนฟิลด์',  
             'attribute'=>'fields',
             'format'=>'decimal',  
           ],
           [
             //'pageSummary' => true,  
             'label'=>'จำนวนแถว',   
             'attribute'=>'rows',
             'format'=>'decimal',
           ],        
           [
             //'pageSummary' => true,  
             'attribute'=>'items',
             'label'=>'ผลรวม',
             'format'=>'decimal',
           ],
      ],
]); ?>
 

<?php 
 \yii\bootstrap\Modal::begin([
        'id'=>'modal-mornitor'
    ]);
 
    \yii\bootstrap\Modal::end();           
?>

<?php $this->registerJS("
 
 $.ajax({
    url:'".yii\helpers\Url::to(['sum-table'])."',
    success:function(data){
        $('#grid-hismonitor-container table').prepend(data);  
    }
});
 ShowDetail = function(hcode){
   $.ajax({
        url:'".yii\helpers\Url::to(['report/detail'])."',
        method:'GET',
        data:{hcode:hcode},
        beforeSend: function() {
            $('#modal-mornitor').modal('show');
            $('#modal-mornitor .modal-header').html('แสดงรายละเอียดของแต่ละแฟ้ม'+'<button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>');
            $('#modal-mornitor .modal-body').html('Loading...');
        },
        success:function(data){
            //console.log(data);
            $('#modal-mornitor .modal-body').html(data);
            
        },error:function(err){
            
            $('#modal-mornitor .modal-body').html('<div class=\'alert alert-danger\'>'+err+'</div>');
        }
   });  
   $('#modal-mornitor').modal('show');
   return false;
 }        
         
")?>
<?php \yii\widgets\Pjax::end() ?>