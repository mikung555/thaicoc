<?php 
    use kartik\grid\GridView;
?>
 
<?= GridView::widget([
      'dataProvider' => $dataProvider,
      //'filterModel' => $searchModel,
     //'showPageSummary' => true,
      'columns' => [
           [
               'label'=>'ชื่อตาราง',
               'attribute'=>'table_name',
               'format'=>'raw',
               'value'=>function($model){
                    return $model['table_name'];
               }
           ],
           [
               'label'=>'จำนวนแถว',
               'attribute'=>'row',
               'format'=>"decimal"
           ],
           [
               'label'=>'จำนวนฟิลด์',
               'attribute'=>'field',
               'format'=>"decimal"
           ],
           [
               'label'=>'Item',
               'attribute'=>'item',
               'format'=>"decimal"
           ],
      ],
]); ?> 
