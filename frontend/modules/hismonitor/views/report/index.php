<?php 
    $this->title="Report";
    use yii\helpers\Url;
    use yii\helpers\Html;
    $url = Yii::$app->urlManager->createUrl("report/tdc-count");
    
     
?>
<div>
    <div>
        
        <div class="alert alert-warning report84-desc" role="alert" >
            <ul>
                <li>จากหน่วยบริการทั้งหมด <strong class="color-red"><?= number_format($count_all_hospital_th);?></strong> หน่วยงาน</li>
                <li> สมัครเข้าใช้งาน Thai Care Cloud แล้ว  <strong class="color-red"><?= number_format($org)?> </strong> หน่วยงาน (ร้อยละ <?= number_format($org/$count_all_hospital_th*100,1);?>) รวมทั้งสิ้น <strong class="color-red"><?= number_format($person)?></strong> คน</li>
                <li>มีการติดตั้ง TDC แล้ว  <strong class="color-red"><?= number_format($count_tdc);?></strong> แห่ง</li>
                <li>และมีจำนวนผู้รับบริการทั้งสิ้น <strong class="color-red"><?= number_format($count_patient)?></strong>  คน</li>                
                <li>โดยได้นำเข้าแฟ้มมาตรฐานจำนวนทั้งสิ้น <strong class="color-red"><span id="count_fperson">100</span></strong> Items</li>
                
            </ul>
            
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="col-md-6"></div>
    <div class="col-md-6">
    <div class="input-group">
        <input type="text" name="search" id="txt-search" placeholder="ค้นหาข้อมูลที่นี่" class="form-control">
        <span class="input-group-btn">
            <button class="btn btn-primary" id="Search" type="submit"><i class="glyphicon glyphicon-search"></i> ค้นหา</button>
         </span>
    </div>
    </div>
    <div id="count_grid"></div>
</div>
<?php 
$this->registerJS("
    count_all();
    count_grid('');
    function count_all(){
        $.ajax({
            url:'".Url::to(['report/tdc-count-all'])."',
            method:'GET',
            success:function(data){
                $('#count_fperson').html(data);
            }
        })
    }
    
    $('#txt-search').keyup(function(){
        setTimeout(function(){
            var search = $('#txt-search').val();
            count_grid(search);
            return false;
        }, 500);
    });
    $('#Search').click(function(){
        var search = $('#txt-search').val();
        count_grid(search);
         
        return false;
    });
    function count_grid(search){
        $.ajax({
            url:'".Url::to(['report/tdc-count'])."',
            method:'GET',
            data:{search:search},
            success:function(data){
                $('#count_grid').html(data);
            }
        })
    }

");
$this->registerCSS("
    .report84-desc{
        background:#FFFF00 !important
    }
    .report84-desc ul li{
       color: #000000;
    }
    .color-red{
        color:#ff0000;
    }
    .table thead tr th{ background:#c9d7da; }
");
?>
