<?php
 namespace frontend\modules\hismonitor\classest;
class ReportFunc {
    public static function getOrgCount($sql=""){
        if(empty($sql)){
            $sql="SELECT count(distinct sitecode) FROM user u inner join user_profile p on u.id=p.user_id inner join all_hospital_thai h on p.sitecode=h.hcode where h.code2 not like '%demo%' and status_del=0 and (code4 <> 15 and code4 <> 16 AND code4 <> 80 )";
        }
        $query = \Yii::$app->db->createCommand($sql)->queryScalar();
        return $query;
    }
    public static function getCount($sql=""){
        if(empty($sql)){
            $sql="SELECT count(*) FROM user u inner join user_profile p on u.id=p.user_id inner join all_hospital_thai h on p.sitecode=h.hcode where h.code2 not like '%demo%' and status_del=0 and volunteer_status=0 and (code4 <> 15 and code4 <> 16 AND code4 <> 80 )";
        }
        $query = \Yii::$app->db->createCommand($sql)->queryScalar();
        return $query;
    }
    public static function getSql($option,$condition=""){
        if(empty($condition)){
            $condition="sitecode IS NOT NULL
            AND sitecode != 0.
            AND sitecode NOT LIKE 'A%'
            AND sitecode NOT LIKE 'Z%'
            AND sitecode NOT LIKE '90%'
            AND sitecode NOT LIKE '91%'
            AND sitecode NOT LIKE '92%'  ";
        }
        return $sql="SELECT $option FROM 
            buffe_patient_count
            WHERE $condition
       ";
    }
    public static function getCountPatient(){
        $sql = "SHOW TABLE STATUS LIKE 'person'";
        $data = \Yii::$app->dbbot->createCommand($sql)->queryOne();
        $dataAllP['TCC'] = $data['Rows'];
        $sql = "SELECT count(*) as nall from person";
        $sql = "SHOW TABLE STATUS LIKE 'person'";
        $data = \Yii::$app->dbnemo->createCommand($sql)->queryOne();
        $dataAllP['NEMO'] = $data['Rows'];
        return $dataAllP['TCC']+$dataAllP['NEMO'];
    }
    public static function getCountTDC(){
        $sql1 = "SELECT id FROM `buffe_webservice`.`buffe_config`";
        $data1 = \Yii::$app->dbbot->createCommand($sql1)->queryAll();
        $sql2 = "SELECT id FROM `tdc_webservice`.`buffe_config`";
        $data2 = \Yii::$app->dbbot->createCommand($sql2)->queryAll();
        $data1 = \yii\helpers\ArrayHelper::map($data1, 'id','id');
        $data2 = \yii\helpers\ArrayHelper::map($data2, 'id','id');
        $data3 = array_unique(\yii\helpers\ArrayHelper::merge($data1, $data2));
 
        return count($data3);
   
    }
    public static function getSum($sql="",$params=[], $option=""){
        
        $sql= ReportFunc::getSql($option);
        $query = \Yii::$app->db->createCommand($sql,$params)->queryAll();
        return $query;
    }
    public static function getField($sql="", $params=[], $option=""){
        $query ="";
    }
    public static function getTableName($table_name) {
        $sql = "SELECT ezf_name FROM ezform where ezf_id IN (SELECT ezf_id from ezform_map_f43 where f43=':f43');";
        $query = \Yii::$app->db->createCommand($sql, [
                    ':f43' => $table_name,
        ])->queryOne();
        return $query;
    }
    public static function getTdcCount($condition=""){
        $params=[];
        $sql = "
            SELECT 
                `pac`.`sitecode` as `hcodes`,
                `aht`.`name` as `hnames`,
                #`pac`.`npatient` as `count_patient`,
                count(`btc`.`table_name`) as `count_table`,
                IFNULL(sum(`btc`.`row`), 0) as `rows`,
                IFNULL(sum(`btc`.`field`), 0) as `fields`,
                IFNULL(sum(`btc`.`item`), 0) as `items`
                FROM `buffe_patient_count` as `pac` 
                LEFT JOIN buffe_tdc_count as `btc` ON `btc`.`sitecode` = `pac`.`sitecode`
                INNER JOIN `all_hospital_thai` as `aht` ON `pac`.`sitecode` = `aht`.`hcode`
                WHERE aht.code2 not like '%demo%' $condition
                #JOIN `buffe_patient_count` as `pac` ON `btc`.`sitecode` = `pac`.`sitecode`
                GROUP BY `pac`.`sitecode`
                ORDER BY `items` DESC;
        ";
        $query = \Yii::$app->db->createCommand($sql,$params)->queryAll(); 
        return $query;
    }
//    public static function getTables($sitecode){
//        $sql="
//            SELECT '$sitecode' as sitecode
//            ,IFNULL((SELECT bc.table_name from buffe_tdc_count bc WHERE bc.table_name = f43.f43 and sitecode = '$sitecode'),f43.f43) as `table_name`
//           from buffe_data.ezform_map_f43_tdc f43 
//        ";
//        return \Yii::$app->db->createCommand($sql)->queryAll();
//    }
//    public static function getTableAll($sitecode){
//       $sql="
//            SELECT '$sitecode' as sitecode
//            ,IFNULL((SELECT bc.table_name from buffe_tdc_count bc WHERE bc.table_name = f43.f43 and sitecode = '$sitecode'),f43.f43) as `table_name`
//            ,IFNULL((SELECT bc.`row` from buffe_tdc_count bc WHERE bc.table_name = f43.f43 and sitecode = '$sitecode'),0) as `row`
//            ,IFNULL((SELECT bc.`field` from buffe_tdc_count bc WHERE bc.table_name = f43.f43 and sitecode = '$sitecode'),0) as `field`
//            ,IFNULL((SELECT bc.`item` from buffe_tdc_count bc WHERE bc.table_name = f43.f43 and sitecode = '$sitecode'),0) as `item`
//            ,(SELECT `ezf_name` from `ezform` where  `ezf_id` IN (SELECT `ezf_id` from `ezform_map_f43` where `f43`='f_person')) as table_names
//           from buffe_data.ezform_map_f43_tdc f43 
//           ORDER BY `item` DESC 
//        "; 
//    }

}
