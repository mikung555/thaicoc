<?php

namespace app\modules\cpay;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\cpay\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
