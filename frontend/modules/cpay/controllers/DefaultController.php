<?php

namespace app\modules\cpay\controllers;

use yii\web\Controller;

class DefaultController extends Controller
{
    public function actionIndex()
    {
        //return $this->render('index');
        $this->redirect('https://cloudbackend.cascap.in.th/cpay/y60');
    }
}
