<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace frontend\modules\purify\controllers;

use yii\web\Controller;
use common\models\User;

class CkdUserDataController  extends Controller{
    //put your code here
    public function behaviors()
    {
        return [
            [
                'class' => \yii\filters\ContentNegotiator::className(),
                'formats' => [
                    'application/json' => \yii\web\Response::FORMAT_JSON,
                ],
            ],
        ];
    }
    
    // ปรับ ให้อัตโนมัติ จาก ezform
    public function actionGetListDataCkd($token) {
        $user = User::findOne(['password_hash' => $token]);
        $userid = $user->id;
        if ($userid > 1) {
            $profile = \common\models\UserProfile::findOne(['user_id'=>$userid]);
            $cid = $profile->cid;
            $sql = "SELECT id,ptid,rstat,cid1,hsitecode,hptcode FROM `tbdata_1484405827048616900` where replace(cid1,'-','')='$cid' ";
            $data[register] = \Yii::$app->db->createCommand($sql)->queryAll();
            $ptid=$data[register][0][ptid];
//            $profile='1';
            // table : 1484367872091935400
            // CKD-PD 01  => ฟอร์มติดตามจำนวนน้ำยาล้างไตที่มีอยู่ ณ วันที่ลงทะเบียน
            $sql = "SELECT * FROM `tbdata_1484367872091935400` where ptid='$ptid' ";
            $data[table][ckd_pd_01] = "tbdata_1484367872091935400";
            $data[ckd_pd_01] = \Yii::$app->db->createCommand($sql)->queryAll();
            // table: 1484371056098885300
            // CKD-PD 02  => ฟอร์มติดตามน้ำยาล้างไตรอบปัจจุบัน
            $data[table][ckd_pd_02] = "tbdata_1484371056098885300";
            $sql = "SELECT * FROM `tbdata_1484371056098885300` where ptid='$ptid' ";
            $data[ckd_pd_02] = \Yii::$app->db->createCommand($sql)->queryAll();
            return $data;
        } else {
            return False;
        }
    }
    
    
}

