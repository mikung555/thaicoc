<?php

namespace frontend\modules\purify\controllers;

use frontend\modules\purify\classes\CID;

class CidController extends \yii\web\Controller
{
    
    public function behaviors()
    {
        return [
            [
                'class' => \yii\filters\ContentNegotiator::className(),
                'formats' => [
                    'application/json' => \yii\web\Response::FORMAT_JSON,
                ],
            ],
        ];
    }
    
    public function actionIndex()
    {
        return $this->render('index');
    }
    
    public function actionOnline() {
        
        if ( 1) {
            $data[status] = TRUE;
//            $profile='1';
            return $data;
        } else {
            return False;
        }
    }
    
    public function actionSearch($cid = null, $hcode = null) {
        
        if ( 1) {
            //$data = CID::getListRegister($cid);
            $data = CID::getICFValid($cid, $hcode);
            return $data;
        } else {
            return False;
        }
    }
    
    public function actionFetchReg($cid = null, $hcode = null) {
        
        if ( 1) {
            $data = CID::getListRegister($cid);
            //$data = CID::getICFValid($cid, $hcode);
            return $data;
        } else {
            return False;
        }
    }

}
