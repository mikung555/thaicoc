<?php

namespace frontend\modules\purify\controllers;

use yii\web\Controller;
use common\models\User;
use Yii;
class MobileLoginController extends Controller
{
    public function behaviors()
    {
        return [
            [
                'class' => \yii\filters\ContentNegotiator::className(),
                'formats' => [
                    'application/json' => \yii\web\Response::FORMAT_JSON,
                ],
            ],
        ];
    }
    
    public function actionIndex()
    {
        return;
    }
    
    
    public function actionAuth($username, $password)
    {
        $user = User::findByUsername($username);
        if($user != null){
            $usr[userlogin] = $user->validatePassword($password) ? $user : null;
            
            $out[id]=$usr[userlogin][id];
            $out[username]=$usr[userlogin][username];
            $out[token]=$usr[userlogin][auth_key];
            /*
            $out[email]=$usr[userlogin][email];
            $profile=\common\models\UserProfile::findOne(['user_id'=>$usr[userlogin][id]]);
            $url = self::getURL($profile->sitecode);
            $out[url]= $url;
            $out[firstname]=$profile->firstname;
            $out[middlename]=$profile->middlename;
            $out[lastname]=$profile->lastname;
            $out[avatar]=$profile->avatar;
            $out[avatar_path]=$profile->avatar_path;
            $out[avatar_base_url]=$profile->avatar_base_url;
            $out[cid]=$profile->cid;
            $out[gender]=$profile->gender;  
            $out[sitecode]=$profile->sitecode;
            $out[telephone]=$profile->telephone;
            $out[department]=$profile->department;
            $out[address_province]=$profile->address_province;
            $out[address_amphur]=$profile->address_amphur;
            $out[address_tambon]=$profile->address_tambon;
            $out[address_text]=$profile->address_text;
             */
            return $out;
        }
        return null;
    }
    
    public function actionUserDet($username, $hsitecode, $token)
    {
        /*
         * $username: คือเลข 13 หลักของคนไข้ หรือสมาชิก
         * $hsitecode: คือรหัสของสถานบริการที่คนไข้เข้ารับการรักษา
         * $token: คือSession ของการ Login
         */
        $user_token = User::findOne(['auth_key' => $token]);
        $user_login = User::findByUsername($username);
        if($user_token != null && $user_login->username==$user_token->username){
            $usr[userlogin] = $user_login;
            
            $out[id]=$usr[userlogin][id];
            $out[username]=$usr[userlogin][username];
            $out[token]=$usr[userlogin][auth_key];
            $out[email]=$usr[userlogin][email];
            $profile=\common\models\UserProfile::findOne(['user_id'=>$usr[userlogin][id]]);
//            $url = self::getURL($profile->sitecode);
            $url = self::getURL($hsitecode);
            $out[url]= $url;
            $out[firstname]=$profile->firstname;
            $out[middlename]=$profile->middlename;
            $out[lastname]=$profile->lastname;
            $out[avatar]=$profile->avatar;
            $out[avatar_path]=$profile->avatar_path;
            $out[avatar_base_url]=$profile->avatar_base_url;
            $out[cid]=$profile->cid;
            $out[gender]=$profile->gender;  
            $out[sitecode]=$profile->sitecode;
            $out[telephone]=$profile->telephone;
            $out[department]=$profile->department;
            $out[address_province]=$profile->address_province;
            $out[address_amphur]=$profile->address_amphur;
            $out[address_tambon]=$profile->address_tambon;
            $out[address_text]=$profile->address_text;
            return $out;
        }
        return null;
    }
    
    
    public function getURL($sitecode){
        
        // โรงพยาบาลที่เข้ารับการศึกษา
        $sql = "select provincecode from all_hospital_thai where hcode='$sitecode' ";
        $hospital = \Yii::$app->db->createCommand($sql)->queryOne();
        
        $sql = "select * from ";
        
        if( Yii::$app->keyStorage->get('frontend.domain')=='cascap.in.th' ){
            $url = "https://cloud.cascap.in.th";
        }else if( Yii::$app->keyStorage->get('frontend.domain')=='yii2-starter-kit.dev' ){
            $url="http://yii2-starter-kit.dev";
        }else{
            $url="https://www.thaicarecloud.org";
        }
        return $url;
    }
    
}

