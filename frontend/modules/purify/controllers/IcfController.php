<?php

namespace frontend\modules\purify\controllers;

use yii\web\Controller;
use common\models\User;
class IcfController extends \yii\web\Controller
{
    
    public function behaviors()
    {
        return [
            [
                'class' => \yii\filters\ContentNegotiator::className(),
                'formats' => [
                    'application/json' => \yii\web\Response::FORMAT_JSON,
                ],
            ],
        ];
    }
    
    public function actionIndex()
    {
        return $this->render('index');
    }
    
    public function actionTest($token, $table)
    {
        $user = User::findOne(['password_hash'=>$token]);
        $userid = $user->id;
        $profile = \common\models\UserProfile::findOne(['user_id'=>$userid]);
        $sitecode = $profile->sitecode;
        
        $sql="SELECT * FROM `$table` limit 10";
        $data = \Yii::$app->db->createCommand($sql)->queryAll();
        return $data;

    }
    public function actionAuth($username, $password)
    {
        $user = User::findByUsername($username);
        if($user != null){
            return $user->validatePassword($password) ? $user : null;
        }
        return null;
    }
    
    
    public function actionChecked($token, $id)
    {
        $user = User::findOne(['password_hash'=>$token]);
        $userid = $user->id;
        $profile = \common\models\UserProfile::findOne(['user_id'=>$userid]);
        $sitecode = $profile->sitecode;
        // get ptid
        //if( strlen($userid)>0 ){
        if( 1 ){
            $sql="SELECT ptid,confirm,rstat FROM `tb_data_1` where id='".$id."' ";
            $data = \Yii::$app->db->createCommand($sql)->queryOne();

            if ( $data['confirm']*1>0 ){
                $datar['status']='1';
                $datar['message']='';
            }else{
                $datar['status']='0';
                $datar['message']='';
                // ตรวจสอบข้อมูลการตรวจ (ข้อความใน query) //
                /*
                    $sql = "select * from cascap_log.query_confirmq where ptid='".$data['ptid']."' ";
                    $sql.= "and qtype='a' order by qid ";
                    //echo $sql;
                    $data = \Yii::$app->db->createCommand($sql)->queryAll();
                    $irow=0;
                    if(count($data)>0){
                        foreach ($data as $k => $v){
                            $datar['message'][$irow]['comment']=$data[$k]['comment'];
                            $irow++;
                        }
                    }   
                 * 
                 */
                $datar['message'] = self::queryListAllQuery($data['ptid']);
            }
        }
        header('Access-Control-Allow-Origin: *');
        header("content-type:text/javascript;charset=utf-8");
        echo json_encode($datar);
        return ;
        
        
        //$datar = self::queryListAllQuery($data['ptid']);
        
        //return $data;

    }
    
    
    public function actionCheckedTest($token, $id)
    {
        $user = User::findOne(['password_hash'=>$token]);
        $userid = $user->id;
        $profile = \common\models\UserProfile::findOne(['user_id'=>$userid]);
        $sitecode = $profile->sitecode;
        // get ptid
        //if( strlen($userid)>0 ){
        if( 1 ){
            $sql="SELECT ptid,confirm,rstat FROM `tb_data_1` where id='".$id."' ";
            $data = \Yii::$app->db->createCommand($sql)->queryOne();

            if ( $data['confirm']*1>0 ){
                $datar['status']='1';
                $datar['message']='';
            }else{
                $datar['status']='0';
                $datar['message']='';
                // ตรวจสอบข้อมูลการตรวจ (ข้อความใน query) //
                $sql = "select * from query_confirmq where ptid='".$data['ptid']."' ";
                $sql.= "and qtype='a' order by qid ";
                //echo $sql;
                $data = \Yii::$app->dbcascap->createCommand($sql)->queryAll();
                $irow=0;
                if(count($data)>0){
                    foreach ($data as $k => $v){
                        $datar['message'][$irow]['comment']=iconv('TIS-620','UTF-8',$data[$k]['comment']);
                        $irow++;
                    }
                }   
            }
        }
        return $datar;
        
        
        //$datar = self::queryListAllQuery($data['ptid']);
        
        //return $data;

    }
    
    public static function queryListAllQuery($ptid){
        $sql = "select distinct query_confirmq.dadd ";
        $sql.= ", query_confirmq.comment ";
        $sql.= ", puser.name, puser.surname ";
        $sql.= "from query_confirmq ";
        $sql.= "left join puser ";
        $sql.= "on puser.pid=query_confirmq.uadd ";
        $sql.= "where ptid='".$ptid."' ";
        $sql.= "and qtype='a' order by qid ";
        //echo $sql;
        $data = \Yii::$app->dbcascap->createCommand($sql)->queryAll();
        $irow=0;
        if(count($data)>0){
            foreach ($data as $k => $v){
                $datar[$irow]['informdate']=$data[$k]['dadd'];
                $datar[$irow]['informby']=iconv('TIS-620','UTF-8',$data[$k]['name']." ".$data[$k]['surname']);
                $datar[$irow]['comment']=iconv('TIS-620','UTF-8',$data[$k]['comment']);
                $irow++;
            }
        } 
        return $datar;
    }
    
}
