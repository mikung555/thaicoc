<?php

namespace frontend\modules\purify\controllers;

use yii\web\Controller;
use common\models\User;
class DefaultController extends Controller
{
    public function behaviors()
    {
        return [
            [
                'class' => \yii\filters\ContentNegotiator::className(),
                'formats' => [
                    'application/json' => \yii\web\Response::FORMAT_JSON,
                ],
            ],
        ];
    }
    
    public function actionIndex()
    {
        return;
    }
    
    public function actionTest($token, $table)
    {
        $user = User::findOne(['password_hash'=>$token]);
        $userid = $user->id;
        $profile = \common\models\UserProfile::findOne(['user_id'=>$userid]);
        $sitecode = $profile->sitecode;
        
        $sql="SELECT * FROM `$table` limit 10";
        $data[sql][query]=$sql;
        $data[sql][limit]='10';
        $data[sql][recstart]='0';
        $data[datarow] = \Yii::$app->db->createCommand($sql)->queryAll();
        return $data;

    }
    public function actionAuth($username, $password)
    {
        $user = User::findByUsername($username);
        if($user != null){
            return $user->validatePassword($password) ? $user : null;
        }
        return null;
    }
    
}
