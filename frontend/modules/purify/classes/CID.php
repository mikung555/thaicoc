<?php

namespace frontend\modules\purify\classes;

use Yii;

/**
 * Description of CID
 *
 * @author chaiwat
 */
class CID {
    public static function getICFValid($cid = null, $hsitecode = null)
    {
        if(strlen(trim($cid))>0 ){
            $sql = "select reg.*, min(cca03.f3v4dvisit) as cca03 from ";
            $sql.= "(select reg.*, min(cca02.f2v1) as cca02 from ";
            $sql.= "(select reg.*, min(cca01.f1vdcomp) as cca01 from ";
            $sql.= "(select id,ptid,hsitecode,hptcode,cid,title,name,surname ";
            $sql.= ",case 1 ";
            $sql.= "when max(reg.confirm)>0 then '2: ICF ตรวจผ่านแล้ว' ";
            $sql.= "when max(reg.confirm)=0 then '1: ตรวจไม่ผ่าน เอกสารไม่สมบูรณ์' ";
            $sql.= "when reg.confirm is null or length(reg.confirm)=0 then '0: ยังไม่ Upload ใบยินยอม หรือข้อมูลใน CCA-01 ไม่สมบูรณ์ (รอตรวจ)' ";
            $sql.= "end as 'icf_check' ";
            $sql.= ",case 1 ";
            $sql.= "when reg.rstat='0' then '0: ข้อมูลใหม่' ";
            $sql.= "when reg.rstat='1' then '1: Wating' when reg.rstat='2' then '2: Submitted' ";
            $sql.= "when reg.rstat>3 then '4: Submitted' ";
            $sql.= "else ' - ' ";
            $sql.= "end as rstat ";
            $sql.= ",create_date, update_date ";
            $sql.= "from tb_data_1 reg ";
            $sql.= "where cid=:cid ) reg ";
            $sql.= "left join tb_data_2 cca01 ";
            $sql.= "on reg.ptid = cca01.ptid ) reg ";
            $sql.= "left join tb_data_3 cca02 ";
            $sql.= "on reg.ptid = cca02.ptid ) reg ";
            $sql.= "left join tb_data_7 cca03 ";
            $sql.= "on reg.ptid = cca03.ptid ";
            //echo $sql;
            $dataProvider = Yii::$app->db->createCommand($sql,[":cid"=>$cid])->queryAll();
            return $dataProvider;
        }
    }
    
    public static function getListRegister($cid = null)
    {
        if(strlen(trim($cid))>0 ){
            $sql = "select reg.*, cca01.f1vdcomp from ";
            $sql.= "(SELECT reg.id, reg.ptid,reg.confirm,reg.rstat,reg.hsitecode,reg.hptcode,reg.title,reg.name,reg.surname ";
            $sql.= ",case 1 ";
            $sql.= "when reg.confirm>0 then '2: ICF ตรวจผ่านแล้ว' ";
            $sql.= "when reg.confirm='0' then '1: ตรวจไม่ผ่าน เอกสารไม่สมบูรณ์' ";
            $sql.= "when reg.confirm is null or length(reg.confirm)=0 then '0: ยังไม่ Upload ใบยินยอม หรือข้อมูลใน CCA-01 ไม่สมบูรณ์ (รอตรวจ)' ";
            $sql.= "end as 'icf_check' ";
            //$sql.= ", cca01.f1vdcomp ";
            $sql.= "FROM `tb_data_1` reg ";
            //$sql.= "left join tb_data_2 cca01 ";
            //$sql.= "on cca01.ptid=reg.ptid and cca01.hsitecode=reg.hsitecode ";
            $sql.= "where reg.cid='".$cid."' ";
            //$sql.= "and cca01.rstat in (1,2,4) ";
            $sql.= "and reg.rstat in (1,2,4) ";
            $sql.= "order by reg.hsitecode, reg.hptcode ";
            $sql.= ") reg ";
            $sql.= "left join tb_data_2 cca01 ";
            $sql.= "on reg.ptid = cca01.ptid ";
            $data = \Yii::$app->db->createCommand($sql)->queryAll();
            return $data;
        }
    }
}
