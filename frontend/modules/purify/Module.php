<?php

namespace frontend\modules\purify;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'frontend\modules\purify\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
