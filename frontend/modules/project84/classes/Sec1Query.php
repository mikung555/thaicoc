<?php

namespace frontend\modules\project84\classes;

use Yii;

/**
 * Description of NmmReport
 *
 * @author NNN
 */
class Sec1Query {
    static $rikgroup = '';

    public static function getData($report_id, $tamboncode, $sitecode, $col) {
        $user_id = Yii::$app->user->identity->userProfile->user_id;
        
        if( strlen($report_id)>0 && strlen($tamboncode)>0 && strlen($sitecode)>0 && strlen($col)>0){
            $sql = 'select hospital.name as hospitalname, report.* ';
            $sql.= 'from all_hospital_thai hospital inner join ';
            $sql.= '(select distinct reg.id ';
            $sql.= ', if(reg.hsitecode=:sitecode,reg.title,reg.ptcodefull) as title ';
            $sql.= ', if(reg.hsitecode=:sitecode,reg.name,"") as fname ';
            $sql.= ', if(reg.hsitecode=:sitecode,reg.surname,"") as surname ';
            $sql.= ', report.* ';
            $sql.= 'from tb_data_1 reg inner join ';
            $sql.= '(select * from `project84_tb1_data` ';
            $sql.= 'where `report_id` =:report_id AND `tamboncode`=:tamboncode ';
            if( $col=='cca01' ){
                $sql.= 'and (cca01ontime=1 or (treatov=1 and cca01=1)) ) report ';
            }else{
                $sql.= 'and `'.$col.'`="1") report ';
            }
            $sql.= 'on report.ptid=reg.ptid and report.hsitecode=reg.hsitecode ';
            $sql.= 'where report.ptid is not null and report.hsitecode is not null ';
            $sql.= 'and reg.rstat<>"3") report ';
            $sql.= 'on report.hsitecode=hospital.hcode ';
            $sql.= 'where report.hsitecode is not null and hospital.name is not null ';
            if( $user_id == '1435745159010043375' ){
                echo Yii::$app->db->createCommand($sql,
                        [
                            ':report_id'=>$report_id,
                            ':tamboncode'=>$tamboncode,
                            ':sitecode'=>$sitecode,
                        ])->rawSql;
            }
            $dataProvider = Yii::$app->db->createCommand($sql,
                    [
                        ':report_id'=>$report_id,
                        ':tamboncode'=>$tamboncode,
                        ':sitecode'=>$sitecode,
                    ])->queryAll();
            
        }
            
        return $dataProvider;
    }
}
