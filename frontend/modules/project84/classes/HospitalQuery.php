<?php

namespace frontend\modules\project84\classes;

use Yii;

/**
 * Description of NmmReport
 *
 * @author NNN
 */
class HospitalQuery {
    static $rikgroup = '';

    public static function getTambonCode($sitecode) {
        
        if( strlen($sitecode)>0 ){
            $sql = 'select * ';
            $sql.= 'from project84_zone_60_hcode ';
            $sql.= 'where hcode=:sitecode ';
            $dataProvider = Yii::$app->db->createCommand($sql,
                    [
                        ':sitecode'=>$sitecode,
                    ])->queryOne();
        }
            
        return $dataProvider;
    }
}
