<?php
 
namespace frontend\modules\project84\classes;
 
class Drilldown {
    public static function getDrilldownPerson($zone_code='',$report_id='',$provincecode='', $param='')
    {
      $sql = "SELECT 
            CONCAT(c.ptcode,c.hptcode)as fullid,
            CONCAT(c.title,c.name,' ',c.surname)as fullname,
            a.*
            FROM tb_data_1 c
            INNER JOIN
            (select
            report_id,hsitecode,ptid,abnormal,suspected,cca02,ctmri,cca,
            treated
            from project84_tb2_data a 
            INNER JOIN all_hospital_thai b ON a.hsitecode=b.hcode
            WHERE a.report_id=:report_id
            and b.zone_code=:zone_code 
            and b.provincecode=:provincecode $param) as a
            on a.ptid=c.ptid 
            where a.hsitecode=c.hsitecode and a.ptid is not null GROUP BY a.ptid";
      $params=[
         ':zone_code'=>$zone_code,
         ':report_id'=>$report_id,
         ':provincecode'=>$provincecode
      ];
      return \Yii::$app->db->createCommand($sql,$params)->queryAll();   
    }
    public static function getProvincename($provincecode='')
    {
        $sql = "SELECT PROVINCE_NAME FROM const_province where PROVINCE_CODE=:province";
        $params=[
         ':province'=>$provincecode
      ];
      return \Yii::$app->db->createCommand($sql,$params)->queryOne();   
    }
    public static function findProvince($provincecode='')
    {
        $sql = "select count(PROVINCE_CODE)as total from project84_report_province_password 
                    where PROVINCE_CODE =:province ";
        $params=[
         ':province'=>$provincecode
      ];
      return \Yii::$app->db->createCommand($sql,$params)->queryOne();   
    }
    public static function getPassword($provincecode='')
    {
        $sql = "SELECT PROVINCE_NAME,hash_password FROM project84_report_province_password
                  where PROVINCE_CODE=:province";
        $params=[
         ':province'=>$provincecode
      ];
      return \Yii::$app->db->createCommand($sql,$params)->queryOne();   
    }
    public static function updatePassword($provincecode='',$hashpassword='')
    {
        $sql = "UPDATE project84_report_province_password
                set hash_password=:hashpassword
                where PROVINCE_CODE=:code";
        $params=[
            ':code'=>$provincecode,
            ':hashpassword'=>$hashpassword,
      ];
      return \Yii::$app->db->createCommand($sql,$params)->execute();   
    }
    public static function insertLog($type='',$provincecode='',$new_key='',$user_id='')
    {
        $sql="INSERT INTO project84_log(F_type,Date_add,F_key,new_password,user_id)
                     VALUES(:type,NOW(),:provincecode,:new_key,:user_id)";
        $params=[
            ':type' =>$type,
            ':provincecode' =>$provincecode,
            ':new_key' =>$new_key,
            ':user_id'=>$user_id,
      ];
      return \Yii::$app->db->createCommand($sql,$params)->execute();
      
    }
        public static function insertPassword($provincecode='',$provincename='',$password='')
    {
        $sql="INSERT INTO project84_report_province_password(PROVINCE_CODE,PROVINCE_NAME,hash_password)
                     VALUES(:provincecode,:provincename,:password)";
        $params=[
            ':provincecode' =>$provincecode,
            ':provincename' =>$provincename,
            ':password'=>$password,
      ];
      return \Yii::$app->db->createCommand($sql,$params)->execute();
      
    }
    public static function findName($ptid='')
    {
        $sql = "SELECT CONCAT(title,name,' ',surname)as fullname
                 FROM tb_data_1 where ptid=:ptid";
        $params=[
         ':ptid'=>$ptid
      ];
      return \Yii::$app->db->createCommand($sql,$params)->queryOne();   
    }
      
}
