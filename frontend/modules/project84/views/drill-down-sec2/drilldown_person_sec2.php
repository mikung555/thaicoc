<?php

use yii\helpers\Html;
use yii\grid\GridView;
use frontend\modules\project84\controllers\DrillDownSec2Controller;
use frontend\modules\project84\controllers\DrillDownSec3Controller;

if ($status == "no") {
    //echo "ท่านไม่มีสิทธิ์เข้าดูข้อมูลในหน่วยบริการนี้";
?>
    <div class="alert alert-info">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>ข้อความแจ้ง! </strong>
        ท่านไม่มีสิทธิ์เข้าดูข้อมูลในหน่วยบริการนี้ หรือ ไม่มีข้อมูลในหน่วยบริการของท่าน
    </div>
<?php
    exit();
}
$num = 1;
$hospital_name = $hosname[0][name];
$amphur_name = $hosname[0][amphur];
$province_name = $hosname[0][province];
?>
<h3><?= $hospital_name ?> อำเภอ <?= $amphur_name ?> จังหวัด <?= $province_name ?></h3>
<div id="report84-ajax-report-div">
    <h4>ก. จำนวนรวมอัลตร้าซาวด์ทั้งหมด
        <label class="label label-primary"> <?= number_format(count($dataProvider)) ?>  </label> ราย</h4>
    <h4>ข. จำแนกตามรายชื่อดังนี้ ดังนี้</h4>
</div>
<table class="table table-striped">
    <thead>
    <tr>
        <th>
            ลำดับที่
        </th>
        <th>
            ชื่อ-สกุล
        </th>
        <th>
            รับการรักษาที่
        </th>
        <th>อัลตร้าซาวด์</th>
        <th>ผิดปกติอย่างใดอย่างหนึ่ง</th>
        <th>สงสัย CCA</th>
        <th>CT / MRI</th>
        <th>พบเป็นมะเร็ง</th>
        <th>ได้รับการรักษา</th>
    </tr>
    </thead>
    <tbody>
    <?php

    foreach ($dataProvider as $value3) {
        $target = base64_encode($value3[target]);
        $nameperson = $value3[hptcode] . " " . $value3[title] . " " . $value3[fname] . " " . $value3[surname];
        $cca02 = DrillDownSec3Controller::GetIcon($value3[cca02]);
        $abnormal = DrillDownSec3Controller::GetIcon($value3[abnormal]);
        $suspected = DrillDownSec3Controller::GetIcon($value3[suspected]);
        $ctmri = DrillDownSec3Controller::GetIcon($value3[ctmri]);
        $cca = DrillDownSec3Controller::GetIcon($value3[cca]);
        $treated = DrillDownSec3Controller::GetIcon($value3[treated]);
        ?>
        <tr>
            <td>
                <?= $num ?>
            </td>
            <td>
                <?php
                    $url_id="https://cloudbackend.cascap.in.th/inputdata/redirect-page/?ezf_id=1437377239070461301&dataid=".$value3['id'];
                ?>
                <a href="<?php echo $url_id; ?>"
                   target="_blank"><?= $nameperson ?></a>
            </td>
            <td>
                <?= DrillDownSec2Controller::ShortName($value3[hospitalname]) ?>
            </td>
            <td>
                <?= $cca02 ?>
            </td>
            <td>
                <?= $abnormal ?>
            </td>
            <td>
                <?= $suspected ?>
            </td>
            <td>
                <?= $ctmri ?>
            </td>
            <td>
                <?= $cca ?>
            </td>
            <td>
                <?= $treated ?>
            </td>
        </tr>
        <?php
        $num++;
    }
    ?>
    </tbody>
</table>
</br>
</br>
