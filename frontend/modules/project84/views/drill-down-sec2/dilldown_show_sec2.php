<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use \frontend\modules\project84\classes\Drilldown;
if($status=="success"){
    $alert_class="success";
    $alert_text="แก้ไขรหัสผ่านเรียบร้อยแล้ว";
}elseif($status=="noPass"){
    $alert_class="warning";
    $alert_text="ยังไม่มีรหัสผ่านในจังหวัดนี่ กรุณาติดต่อ Administrator เพื่อตั้งรหัสผ่านที่จะใช้ในการปลดล็อครายชื่อ";
}elseif($status=="error"){
    $alert_text="รหัสผ่านไม่ถูกต้อง";
    $alert_class="danger";
}elseif($status=="createPass"){
    $alert_text="เพื่มรหัสผ่านเรียบร้อยแล้ว";
    $alert_class="success";
}elseif($status=="adminNopass"){
    $alert_text="จังหวัดนี่ยังไม่มีรหัสผ่าน กรุณาตั้งรหัสผ่าน เพื่อปลดล็อครายชื่อ";
    $alert_class="warning";
}
?>
<div class="table-responsive" id="report1">
    <div class="tab-content">
        <div  class="tab-pane fade in active">
            <div id="report84-ajax-report-div" class="col-sm-8">
                <h4>ผู้ป่วย<?=$show?> 
                    <label class="label label-primary"> <?= number_format($dataGet[sumtotal]) ?> &nbsp;</label> ราย</h4>
            </div>
            <div class="form-inline">
                <div class="input-group">
                    <span class="input-group-addon kv-date-calendar" >
                        <i class="fa fa-key"></i>
                    </span>
                    <?=
                    Html::input('password', 'key', "", [
                        'class' => 'form-control',
                        'id' => 'input_pass',
                        'placeholder' => 'กรุณากรอกรหัสผ่าน',
                        'autocomplete'=> 'off',
                    ]);
                    ?>
                </div>
                <button class="btn btn-primary" id="submit-button">ถอดรหัส</button>
                <?php if(Yii::$app->user->can('administrator')==TRUE and $status!="adminNopass"){ ?>
                <label>
                    <input type="checkbox" id="save_key" name="save_key">Reset password 
                </label>
                <?php } ?>
            </div>
        <?php if($status !="ok"){ ?>
        <div class="alert alert-<?=$alert_class?>">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <strong><?=$alert_text?></strong>
        </div>
        <?php } ?>
            <h3>จังหวัด <?= $objResult['PROVINCE_NAME'] ?></h3>

            <div style="overflow-x:auto;">
                <?php
                    echo kartik\grid\GridView::widget([
                    'dataProvider' => $dataProvider,
                    'columns' => [
                        [
                         'class' => 'yii\grid\SerialColumn',
                         'contentOptions'=>['width'=>'50'],
                        ],
                        [
                            'attribute'=>'fullid',
                            'label'=>'รหัสผู้ป่วย',
                            'headerOptions'=>['style'=>'text-align: center;'],
                            'contentOptions'=>['width'=>'100'],
                        ],
                        [
                            'attribute'=>'fullname',
                            'label'=>'ชื่อ-นามสกุล',
                            'headerOptions'=>['style'=>'text-align: center;'],
                            'contentOptions'=>['width'=>'300'],
                            'value'=>function($model) use($key){
                                $name = $model["fullname"];
                                if($key==TRUE){
                                    if($model["fullname"] == ''){
                                        $tbname =Drilldown::findName($model["ptid"]);
                                        return $tbname['fullname'];
                                    }else{
                                        return $name;
                                    }
                                }else{
                                    return "XXXXXXXX-XXXXXXXXX";
                                }
                            }
                        ],
                        [
                            'attribute' => 'abnormal',
                            'format'=>'html',
                            'label'=>'ผิดปกติอย่างใดอย่างหนึ่ง',
                            'headerOptions'=>['style'=>'text-align: center;'],
                            'contentOptions'=>['style'=>'text-align: center;'],
                            'value'=>function($model){
                                $number = $model["abnormal"];
                                if($number==='1'){
                                    return "<i class=\"glyphicon glyphicon-ok\"></i>";
                                }else{
                                    return "";
                                }
                            }
                        ],
                        [
                            'attribute' => 'suspected',
                            'format'=>'html',
                            'label'=>'สงสัย CCA',
                            'headerOptions'=>['style'=>'text-align: center;'],
                            'contentOptions'=>['style'=>'text-align: center;'],
                            'value'=>function($model){
                                $number = $model["suspected"];
                                if($number==='1'){
                                    return "<i class=\"glyphicon glyphicon-ok\"></i>";
                                }else{
                                    return "";
                                }
                            }
                        ],
                        [
                            'attribute' => 'ctmri',
                            'format'=>'html',
                            'label'=>'CT / MRI',
                            'headerOptions'=>['style'=>'text-align: center;'],
                            'contentOptions'=>['style'=>'text-align: center;'],
                            'value'=>function($model){
                                $number = $model["ctmri"];
                                if($number==='1'){
                                    return "<i class=\"glyphicon glyphicon-ok\"></i>";
                                }else{
                                    return "";
                                }
                            }
                        ],
                        [
                            'attribute' => 'cca',
                            'format'=>'html',
                            'label'=>'พบเป็นมะเร็ง',
                            'headerOptions'=>['style'=>'text-align: center;'],
                            'contentOptions'=>['style'=>'text-align: center;'],
                            'value'=>function($model){
                                $number = $model["cca"];
                                if($number==='1'){
                                    return "<i class=\"glyphicon glyphicon-ok\"></i>";
                                }else{
                                    return "";
                                }
                            }
                        ],
                        [
                            'attribute' => 'treated',
                            'format'=>'html',
                            'label'=>'ได้รับการรักษา',
                            'headerOptions'=>['style'=>'text-align: center;'],
                            'contentOptions'=>['style'=>'text-align: center;'],
                            'value'=>function($model){
                                $number = $model["treated"];
                                if($number==='1'){
                                    return "<i class=\"glyphicon glyphicon-ok\"></i>";
                                }else{
                                    return "";
                                }
                            }
                        ],
                    ],
                    'pjax'=>true,
                ]); 
                        ?>
            </div>
        </div>
        
        <div id="report2" class="tab-pane fade">

        </div>
    </div>
</div>
</div>
<?php  
$this->registerJS("
     var report_id= ".$dataGet['report_id'].";
     var zone= ".$dataGet['zone'].";
     var show= ".$dataGet['show'].";
     var provincecode= ".$dataGet['provincecode'].";
     var sumtotal= ".$dataGet['sumtotal'].";

   $('#submit-button').click(()=>{
       var save_key = $(\"#save_key\").is(':checked'); 
       var input_pass = $('#input_pass').val();
       var status =0;
       if(save_key == true){
          status =1;
       }
        if (input_pass  === '') {
            alert('กรุณากรอกรหัสผ่าน');
            return false;
        }
       let url = '".\yii\helpers\Url::to(['/project84/drill-down-sec2/checkpassword'])."';
       $.ajax({
            url:url,
            type:'GET',
            data:{status:status,input_pass:input_pass, report_id:report_id,zone:zone,show:show,provincecode:provincecode,sumtotal:sumtotal },
            success:function(data){
                $('#report1').html(data);
            }
       })
   });   
");
?>