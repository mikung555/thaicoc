<?php

use yii\helpers\Html;
use yii\grid\GridView;
use frontend\modules\project84\controllers\DrillDownSec2Controller;


$amphur = DrillDownSec2Controller::GetAmphur($report_id, $provincecode);

?>
<div class="table-responsive">
    <ul class="nav nav-tabs" id="myTab">
        <li id="listitemreport" class="active">
            <a href="#report1" data-toggle="tab" setshowpersonactive="0" >รายงาน</a>
        </li>
        <li id="listitemdepart">
            <a id="drilldownperson" report_id="<?= $report_id ?>" provincecode="<?php echo $provincecode; ?>" div="#report2" sec="2" href="#report2" tabmenu="yes"
               data-toggle="tab" setshowpersonactive="0">รายงานรายบุคคลของสถานบริการ</a>
        </li>
        <li id="listitempersion" style="display: none;">
            <a id="showpersonactive" div="#report2" sec="2" href="#report2" tabmenu="yes"
               data-toggle="tab">รายรายชื่อ</a>
        </li>
    </ul>
    <div class="tab-content">
        <div id="report1" class="tab-pane fade in active">
            <div id="report84-ajax-report-div">
                <h4>ก. จำนวนรวมอัลตร้าซาวด์ทั้งหมด
                    <label class="label label-primary"> <?= number_format($sumtotal,0,'.',','); ?>  </label> ราย</h4>
                <h4>ข. จำแนกตามหน่วยบริการ (Hospital-based) ที่ตรวจ Ultrasound ดังนี้</h4>
            </div>

            <h3>จังหวัด <?= $province_name ?></h3>
            <div style="overflow-x:auto;">
                <table>
                    <thead>
                    <tr>
                        <th width="30%">
                        </th>
                        <th width="auto">
                            ลงทะเบียน
                        </th>
                        <th width="auto">อัลตร้าซาวด์</th>
                        <th width="auto">ผิดปกติอย่างใดอย่างหนึ่ง</th>
                        <th width="auto">สงสัย CCA</th>
                        <th width="auto">CT / MRI</th>
                        <th width="auto">พบเป็นมะเร็ง</th>
                        <th width="auto">ได้รับการรักษา</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $irow = 0;
                    foreach ($amphur as $value2) {
                        $amphur_name = $value2[amphur];
                        $amphurcode = $value2[amphurcode];
                        $hospital = DrillDownSec2Controller::GetHospital($report_id, $provincecode, $amphurcode);
                        ?>
                        <tr>
                            <td colspan="17">
                                <b>อำเภอ <?= $amphur_name ?></b>
                            </td>
                        </tr>
                        <?php
                       
                        foreach ($hospital as $value3) {
                            $irow++;
                            $sitecode = $value3[hsitecode];
                            $hospital_name = DrillDownSec2Controller::ShortName($value3[name]);

                            ?>
                            <tr>
                                <td style="text-align:left;">
                                    <?php
                                    if ($checkadmin == "yes") {
                                        $linkID = "drilldownperson".$irow;
                                        ?>
                                        <a href="#report2" id="drilldownperson" report_id="<?= $report_id ?>"
                                           sitecode="<?= $sitecode ?>" hsitecode="<?= $sitecode ?>"
                                           title="<?= $hospital_name ?>" sec="2" div="#report2"
                                           data-toggle="tab" setshowpersonactive="1" ><?= $sitecode . " " . $hospital_name ?></a>
                                        <?php
                                    } else {
                                        echo $sitecode . " " . $hospital_name;
                                    }
                                    ?>
                                </td>
                                <td style="text-align:right;">
                                    <?= number_format($value3[nall],0,'.',','); ?>
                                </td>
                                <td style="text-align:right;">
                                    <?= number_format($value3[cca02],0,'.',','); ?>
                                </td>
                                <td style="text-align:right;">
                                    <?= number_format($value3[abnormal],0,'.',','); ?>
                                </td>
                                <td style="text-align:right;">
                                    <?= number_format($value3[suspected],0,'.',','); ?>
                                </td>
                                <td style="text-align:right;">
                                    <?= number_format($value3[ctmri],0,'.',','); ?>
                                </td>
                                <td style="text-align:right;">
                                    <?= number_format($value3[cca],0,'.',','); ?>
                                </td>
                                <td style="text-align:right;">
                                    <?= number_format($value3[treated],0,'.',','); ?>
                                </td>
                            </tr>
                            <?php
                        }
                    }
                    ?>
                    </tbody>
                </table>
            </div>
            
            <div id="report84-ajax-report-div">
                <br />
                <br />
                <h4>ค. จำแนกตามหน่วยบริการ ที่ลงทะเบียนครั้งแรก (CCA-01 และ เอกสารรักษาความลับ) ดังนี้</h4>
                <label style="color: red">*หมายเหตุ คนไข้บางคนอาจจะทำการอัลตราซาวด์ข้าม อำเภอ</label>
            </div>
            
            <div style="overflow-x:auto;">
                <table>
                    <thead>
                    <tr>
                        <th width="30%">
                        </th>
                        <th width="auto">
                            ลงทะเบียน
                        </th>
                        <th width="auto">อัลตร้าซาวด์</th>
                        <th width="auto">ผิดปกติอย่างใดอย่างหนึ่ง</th>
                        <th width="auto">สงสัย CCA</th>
                        <th width="auto">CT / MRI</th>
                        <th width="auto">พบเป็นมะเร็ง</th>
                        <th width="auto">ได้รับการรักษา</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    // count all of province
                    $sump_nall       = 0;
                    $sump_cca02      = 0;
                    $sump_abnormal   = 0;
                    $sump_suspected  = 0;
                    $sump_ctmri      = 0;
                    $sump_cca        = 0;
                    $sump_treated    = 0;
                    foreach ($amphur as $value2) {
                        $amphur_name = $value2[amphur];
                        $amphurcode = $value2[amphurcode];
                        $hospital = DrillDownSec2Controller::GetHospitalCCA01ICF($report_id, $provincecode, $amphurcode);
                        ?>
                        <tr>
                            <td colspan="17">
                                <b>อำเภอ <?= $amphur_name ?></b>
                            </td>
                        </tr>
                        <?php
                        // count group by amphur
                        $sum_nall       = 0;
                        $sum_cca02      = 0;
                        $sum_abnormal   = 0;
                        $sum_suspected  = 0;
                        $sum_ctmri      = 0;
                        $sum_cca        = 0;
                        $sum_treated    = 0;
                        foreach ($hospital as $value3) {
                            $sitecode = $value3[sitecode];
                            $hsitecode = $value3[hsitecode];
                            $hospital_name = DrillDownSec2Controller::ShortName($value3[name]);
                            $sum_nall       = $sum_nall     + $value3[nall];
                            $sum_cca02      = $sum_cca02    + $value3[cca02];
                            $sum_abnormal   = $sum_abnormal + $value3[abnormal];
                            $sum_suspected  = $sum_suspected+ $value3[suspected];
                            $sum_ctmri      = $sum_ctmri    + $value3[ctmri];
                            $sum_cca        = $sum_cca      + $value3[cca];
                            $sum_treated    = $sum_treated  + $value3[treated];
                            
                            $sump_nall       = $sump_nall     + $value3[nall];
                            $sump_cca02      = $sump_cca02    + $value3[cca02];
                            $sump_abnormal   = $sump_abnormal + $value3[abnormal];
                            $sump_suspected  = $sump_suspected+ $value3[suspected];
                            $sump_ctmri      = $sump_ctmri    + $value3[ctmri];
                            $sump_cca        = $sump_cca      + $value3[cca];
                            $sump_treated    = $sump_treated  + $value3[treated];

                            ?>
                            <tr>
                                <td style="text-align:left;">
                                    <?php
                                    if ($checkadmin == "yes") {
                                        ?>
                                        <a href="#report2" id="drilldownperson" report_id="<?= $report_id ?>"
                                           sitecode="<?= $sitecode ?>" hsitecode="<?= $hsitecode ?>"
                                           varsite="sitecode"
                                           title="<?= $hospital_name ?>" sec="2" div="#report2"
                                           data-toggle="tab"><?= $sitecode . " " . $hospital_name ?></a>
                                        <?php
                                    } else {
                                        echo $sitecode . " " . $hospital_name;
                                    }
                                    ?>
                                </td>
                                <td style="text-align:right;">
                                    <?= number_format($value3[nall],0,'.',','); ?>
                                </td>
                                <td style="text-align:right;">
                                    <?= number_format($value3[cca02],0,'.',','); ?>
                                </td>
                                <td style="text-align:right;">
                                    <?= number_format($value3[abnormal],0,'.',','); ?>
                                </td>
                                <td style="text-align:right;">
                                    <?= number_format($value3[suspected],0,'.',','); ?>
                                </td>
                                <td style="text-align:right;">
                                    <?= number_format($value3[ctmri],0,'.',','); ?>
                                </td>
                                <td style="text-align:right;">
                                    <?= number_format($value3[cca],0,'.',','); ?>
                                </td>
                                <td style="text-align:right;">
                                    <?= number_format($value3[treated],0,'.',','); ?>
                                </td>
                            </tr>
                            <?php
                        }
                        ?>
                            <tr>
                                <td style="text-align:left;">
                                    รวม ทั้งอำเภอ <?php echo $amphur_name; ?>
                                </td>
                                <td style="text-align:right;">
                                    <?= number_format($sum_nall,0,'.',','); ?>
                                </td>
                                <td style="text-align:right;">
                                    <?= number_format($sum_cca02,0,'.',','); ?>
                                </td>
                                <td style="text-align:right;">
                                    <?= number_format($sum_abnormal,0,'.',','); ?>
                                </td>
                                <td style="text-align:right;">
                                    <?= number_format($sum_suspected,0,'.',','); ?>
                                </td>
                                <td style="text-align:right;">
                                    <?= number_format($sum_ctmri,0,'.',','); ?>
                                </td>
                                <td style="text-align:right;">
                                    <?= number_format($sum_cca,0,'.',','); ?>
                                </td>
                                <td style="text-align:right;">
                                    <?= number_format($sum_treated,0,'.',','); ?>
                                </td>
                            </tr>
                        
                        <?php
                    }
                    ?>
                            <tr>
                                <td style="text-align:left;">
                                    รวมทั้งจังหวัด
                                </td>
                                <td style="text-align:right;">
                                    <?= number_format($sump_nall,0,'.',','); ?>
                                </td>
                                <td style="text-align:right;">
                                    <?= number_format($sump_cca02,0,'.',','); ?>
                                </td>
                                <td style="text-align:right;">
                                    <?= number_format($sump_abnormal,0,'.',','); ?>
                                </td>
                                <td style="text-align:right;">
                                    <?= number_format($sump_suspected,0,'.',','); ?>
                                </td>
                                <td style="text-align:right;">
                                    <?= number_format($sump_ctmri,0,'.',','); ?>
                                </td>
                                <td style="text-align:right;">
                                    <?= number_format($sump_cca,0,'.',','); ?>
                                </td>
                                <td style="text-align:right;">
                                    <?= number_format($sump_treated,0,'.',','); ?>
                                </td>
                            </tr>
                    </tbody>
                </table>
            </div>
            
        </div>
        </br>
        </br>
        <div id="report2" class="tab-pane fade">

        </div>
    </div>
</div>
</div>
