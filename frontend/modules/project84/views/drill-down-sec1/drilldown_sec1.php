<?php

use yii\helpers\Html;
use yii\grid\GridView;
use frontend\modules\project84\classes\Sec1Query;
use frontend\modules\project84\controllers\DrillDownSec1Controller;
use frontend\modules\project84\controllers\ReportController;

$request = Yii::$app->getRequest();

$report1panel = 'class="tab-pane fade"';
$report2panel = 'class="tab-pane fade"';
$report3panel = 'class="tab-pane fade"';
$report4panel = 'class="tab-pane fade"';

$report4 = ' style="display: none;"';
$report4title = 'ยังไม่เลือก';

//echo $request->get('col');

if( strlen($request->get('col'))>0 ){
    // กรณีเลือกชุดข้อมูล
    $report4 = ' ';
    if( $request->get('col')=='riskgroup' ){
        $report4title = 'เลือกกลุ่มเสี่ยง';
    }else{
        $report4title = 'ข้อมูลตามเงื่อนไขที่เลือก';
    }
    $report4clasactive = 'class="active"';
    $report4panel = 'class="tab-pane fade in active"';
}else{
    // default
    $report1clasactive = 'class="active"';
    $report1panel = 'class="tab-pane fade in active"';
}


?>
<div class="table-responsive">
    <ul class="nav nav-tabs" id="myTab">
        <li <?php echo $report1clasactive; ?>>
            <a href="#report1" data-toggle="tab">รายงาน</a>
        </li>
        <li>
            <a id="drilldownperson" report_id="<?= $report_id ?>" tamboncode="<?= $tamboncode ?>" 
               div="#report2" sec="1" href="#report2" 
               data-toggle="tab">รายงานตามพื้นที่อยู่อาศัย</a>
        </li>
        <li>
            <a id="drilldownperson" report_id="<?= $report_id ?>" tamboncode="<?= $tamboncode ?>" 
               div="#report3" sec="1" href="#report3" tabmenu="yes"
               data-toggle="tab">รายงานรายบุคคลของสถานบริการ</a>
        </li>

        <li <?php echo $report4clasactive; ?>>
            <a id="drilldownpersonOption01" report_id="<?= $report_id ?>" tamboncode="<?= $tamboncode ?>" 
               div="#report4" sec="1" href="#report4" tabmenu="yes" col="<?php echo $request->get('col'); ?>"
               data-toggle="tab" <?php echo $report4; ?> ><?php echo $report4title; ?></a>
        </li>
    </ul>
<?php
    
    if( 1 ){
?>
    <div class="tab-content">
        <div id="report1" <?php echo $report1panel; ?>>
            <h4>
               <?php
                if( strlen($request->get('col'))>0 ){
                    echo 'รายชื่อตาม'.$report4title.' ทั้งหมด';
                }else{
                    echo 'ตำบล '.$dataArrayDrillSec1[0][tambon].' อำเภอ '.$dataArrayDrillSec1[0][amphur].'จังหวัด '.$dataArrayDrillSec1[0][province];
                }
                ?> 
            </h4>
            <div style="overflow-x:auto;">
                <table>
                    <thead>
                    <tr>
                        <th style="min-width: 120px;">ประเภทการตรวจ</th>
                        <th style="min-width: 120px;">ผู้รับบริการ (ราย)</th>
                        <th style="min-width: 100px;">OV (ราย)</th>
                        <th style="min-width: 80px;">ร้อยละ</th>
                        <th style="min-width: 80px;">mif (ราย)</th>
                        <th style="min-width: 80px;">ร้อยละ</th>
                        <th style="min-width: 80px;">ss (ราย)</th>
                        <th style="min-width: 80px;">ร้อยละ</th>
                        <th style="min-width: 80px;">ech (ราย)</th>
                        <th style="min-width: 80px;">ร้อยละ</th>
                        <th style="min-width: 100px;">taenia (ราย)</th>
                        <th style="min-width: 80px;">ร้อยละ</th>
                        <th style="min-width: 80px;">tt (ราย)</th>
                        <th style="min-width: 80px;">ร้อยละ</th>
                        <th style="min-width: 100px;">other (ราย)</th>
                        <th style="min-width: 80px;">ร้อยละ</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>Kato-Katz</td>
                        <td style="text-align:right;"><?= number_format($dataArrayDrillSec1[0][kpos],0,'.',','); ?></td>
                        <td style="text-align:right;"><?= number_format($datakpos[0][ov],0,'.',','); ?></td>
                        <td style="text-align:right;"><?= ReportController::calpercent($datakpos[0][ov], $dataArrayDrillSec1[0][kpos], "yes") ?></td>
                        <td style="text-align:right;"><?= number_format($datakpos[0][mif],0,'.',','); ?></td>
                        <td style="text-align:right;"><?= ReportController::calpercent($datakpos[0][mif], $dataArrayDrillSec1[0][kpos], "yes") ?></td>
                        <td style="text-align:right;"><?= number_format($datakpos[0][ss],0,'.',','); ?></td>
                        <td style="text-align:right;"><?= ReportController::calpercent($datakpos[0][ss], $dataArrayDrillSec1[0][kpos], "yes") ?></td>
                        <td style="text-align:right;"><?= number_format($datakpos[0][ech],0,'.',','); ?></td>
                        <td style="text-align:right;"><?= ReportController::calpercent($datakpos[0][ech], $dataArrayDrillSec1[0][kpos], "yes") ?></td>
                        <td style="text-align:right;"><?= number_format($datakpos[0][taenia],0,'.',','); ?></td>
                        <td style="text-align:right;"><?= ReportController::calpercent($datakpos[0][taenia], $dataArrayDrillSec1[0][kpos], "yes") ?></td>
                        <td style="text-align:right;"><?= number_format($datakpos[0][tt],0,'.',','); ?></td>
                        <td style="text-align:right;"><?= ReportController::calpercent($datakpos[0][tt], $dataArrayDrillSec1[0][kpos], "yes") ?></td>
                        <td style="text-align:right;"><?= number_format($datakpos[0][other],0,'.',','); ?></td>
                        <td style="text-align:right;"><?= ReportController::calpercent($datakpos[0][other], $dataArrayDrillSec1[0][kpos], "yes") ?></td>
                    </tr>
                    <tr>
                        <td>Parasep</td>
                        <td style="text-align:right;"><?= number_format($dataArrayDrillSec1[0][ppos],0,'.',','); ?></td>
                        <td style="text-align:right;"><?= number_format($datappos[0][ov],0,'.',','); ?></td>
                        <td style="text-align:right;"><?= ReportController::calpercent($datappos[0][ov], $dataArrayDrillSec1[0][ppos], "yes") ?></td>
                        <td style="text-align:right;"><?= number_format($datappos[0][mif],0,'.',','); ?></td>
                        <td style="text-align:right;"><?= ReportController::calpercent($datappos[0][mif], $dataArrayDrillSec1[0][ppos], "yes") ?></td>
                        <td style="text-align:right;"><?= number_format($datappos[0][ss],0,'.',','); ?></td>
                        <td style="text-align:right;"><?= ReportController::calpercent($datappos[0][ss], $dataArrayDrillSec1[0][ppos], "yes") ?></td>
                        <td style="text-align:right;"><?= number_format($datappos[0][ech],0,'.',','); ?></td>
                        <td style="text-align:right;"><?= ReportController::calpercent($datappos[0][ech], $dataArrayDrillSec1[0][ppos], "yes") ?></td>
                        <td style="text-align:right;"><?= number_format($datappos[0][taenia],0,'.',','); ?></td>
                        <td style="text-align:right;"><?= ReportController::calpercent($datappos[0][taenia], $dataArrayDrillSec1[0][ppos], "yes") ?></td>
                        <td style="text-align:right;"><?= number_format($datappos[0][tt],0,'.',','); ?></td>
                        <td style="text-align:right;"><?= ReportController::calpercent($datappos[0][tt], $dataArrayDrillSec1[0][ppos], "yes") ?></td>
                        <td style="text-align:right;"><?= number_format($datappos[0][other],0,'.',','); ?></td>
                        <td style="text-align:right;"><?= ReportController::calpercent($datappos[0][other], $dataArrayDrillSec1[0][ppos], "yes") ?></td>
                    </tr>
                    <tr>
                        <td>FECT</td>
                        <td style="text-align:right;"><?= number_format($dataArrayDrillSec1[0][fpos],0,'.',','); ?></td>
                        <td style="text-align:right;"><?= number_format($datafpos[0][ov],0,'.',','); ?></td>
                        <td style="text-align:right;"><?= ReportController::calpercent($datafpos[0][ov], $dataArrayDrillSec1[0][fpos], "yes") ?></td>
                        <td style="text-align:right;"><?= number_format($datafpos[0][mif],0,'.',','); ?></td>
                        <td style="text-align:right;"><?= ReportController::calpercent($datafpos[0][mif], $dataArrayDrillSec1[0][fpos], "yes") ?></td>
                        <td style="text-align:right;"><?= number_format($datafpos[0][ss],0,'.',','); ?></td>
                        <td style="text-align:right;"><?= ReportController::calpercent($datafpos[0][ss], $dataArrayDrillSec1[0][fpos], "yes") ?></td>
                        <td style="text-align:right;"><?= number_format($datafpos[0][ech],0,'.',','); ?></td>
                        <td style="text-align:right;"><?= ReportController::calpercent($datafpos[0][ech], $dataArrayDrillSec1[0][fpos], "yes") ?></td>
                        <td style="text-align:right;"><?= number_format($datafpos[0][taenia],0,'.',','); ?></td>
                        <td style="text-align:right;"><?= ReportController::calpercent($datafpos[0][taenia], $dataArrayDrillSec1[0][fpos], "yes") ?></td>
                        <td style="text-align:right;"><?= number_format($datafpos[0][tt],0,'.',','); ?></td>
                        <td style="text-align:right;"><?= ReportController::calpercent($datafpos[0][tt], $dataArrayDrillSec1[0][fpos], "yes") ?></td>
                        <td style="text-align:right;"><?= number_format($datafpos[0][other],0,'.',','); ?></td>
                        <td style="text-align:right;"><?= ReportController::calpercent($datafpos[0][other], $dataArrayDrillSec1[0][fpos], "yes") ?></td>
                    </tr>
                    <tr>
                        <td>Urine</td>
                        <td style="text-align:right;"><?= number_format($dataArrayDrillSec1[0][upos],0,'.',','); ?></td>
                        <td style="text-align:right;"><?= number_format($dataupos[0][ov],0,'.',','); ?></td>
                        <td style="text-align:right;"><?= ReportController::calpercent($dataupos[0][ov], $dataArrayDrillSec1[0][upos], "yes") ?></td>
                        <td style="text-align:right;"><?= number_format($dataupos[0][mif],0,'.',','); ?></td>
                        <td style="text-align:right;"><?= ReportController::calpercent($dataupos[0][mif], $dataArrayDrillSec1[0][upos], "yes") ?></td>
                        <td style="text-align:right;"><?= number_format($dataupos[0][ss],0,'.',','); ?></td>
                        <td style="text-align:right;"><?= ReportController::calpercent($dataupos[0][ss], $dataArrayDrillSec1[0][upos], "yes") ?></td>
                        <td style="text-align:right;"><?= number_format($dataupos[0][ech],0,'.',','); ?></td>
                        <td style="text-align:right;"><?= ReportController::calpercent($dataupos[0][ech], $dataArrayDrillSec1[0][upos], "yes") ?></td>
                        <td style="text-align:right;"><?= number_format($dataupos[0][taenia],0,'.',','); ?></td>
                        <td style="text-align:right;"><?= ReportController::calpercent($dataupos[0][taenia], $dataArrayDrillSec1[0][upos], "yes") ?></td>
                        <td style="text-align:right;"><?= number_format($dataupos[0][tt],0,'.',','); ?></td>
                        <td style="text-align:right;"><?= ReportController::calpercent($dataupos[0][tt], $dataArrayDrillSec1[0][upos], "yes") ?></td>
                        <td style="text-align:right;"><?= number_format($dataupos[0][other],0,'.',','); ?></td>
                        <td style="text-align:right;"><?= ReportController::calpercent($dataupos[0][other], $dataArrayDrillSec1[0][upos], "yes") ?></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
        </br>
        </br>
        <div id="report2" <?php echo $report2panel; ?>>
        </div>
        <div id="report3" <?php echo $report3panel; ?>>
        </div>
        <div id="report4" <?php echo $report4panel; ?>>
            <?php
            if( strlen($request->get('col'))>0 ){ //=='riskgroup' ){
                // show data
                $sitecode = Yii::$app->user->identity->userProfile->sitecode;
                if( 0 ){
                    echo "<br />col: ".$request->get('col');
                    echo "<br />_GET[hsitecode]:".$request->get('hsitecode');
                    echo "<br />sitecode: ".$sitecode;
                    echo "<br />tamboncode:".$request->get('tamboncode');
                    echo "<br />report_id: ".$request->get('report_id');
                }
                $render = "drilldown_person_sec11";
                $dataProvider = Sec1Query::getData($request->get('report_id'), $request->get('tamboncode'), $sitecode, $request->get('col'));
                $hosname = '';
                $status = '';
                echo $this->render($render, [
                    'dataProvider' => $dataProvider,
                    'hosname' => $hosname,
                    'status' => $status,
                ]);
            }
            ?>
        </div>
    </div>
<?php
    }
?>
</div>
</div>
