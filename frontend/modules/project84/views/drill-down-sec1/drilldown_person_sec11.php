<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use frontend\modules\project84\controllers\DrillDownSec1Controller;
use frontend\modules\project84\controllers\DrillDownSec2Controller;
use frontend\modules\project84\controllers\DrillDownSec3Controller;

$request = Yii::$app->getRequest();
$usersitecode = Yii::$app->user->identity->userProfile->sitecode;
if( $request->get('col')=='riskgroup' ){
    // กรณีเลือกชุดข้อมูล
    $report4title = 'เลือกกลุ่มเสี่ยง ที่ลงทะเบียนเข้าสู่ CASCAP (Isan Cohort) แล้ว';
}else if( $request->get('col')=='icf' ){
    // กรณีเลือกชุดข้อมูล
    $report4title = 'ใบยินยอม ได้ตับการตรวจสอบจากส่วนกลางแล้วว่าถูกต้อง';
}else if( $request->get('col')=='cca01' ){
    // กรณีเลือกชุดข้อมูล
    $report4title = 'ข้อมูลที่ลงทะเบียนในปีงบ 2560 ที่มี CCA-01 (หรือ มีผลตรวจ OV ในปีงบประมาณ 2560 พร้อม CCA-01)';
}else if( $request->get('col')=='treatov' ){
    // กรณีเลือกชุดข้อมูล
    $report4title = 'มีผลตรวจ OV ในปีงบประมาณ 2560';
}else if( $request->get('col')=='ov' ){
    // กรณีเลือกชุดข้อมูล
    $report4title = 'มีผลตรวจ OV เป็น + (Positive)';
}else if( $request->get('col')=='ov03' ){
    // กรณีเลือกชุดข้อมูล
    $report4title = 'ได้รับการรักษา หรือได้รับยา';
}
?>
<?php
if ($status == "no") {
    //echo "ท่านไม่มีสิทธิ์เข้าดูข้อมูลในหน่วยบริการนี้ หรือ ไม่มีข้อมูลในหน่วยบริการของท่าน ";
?>

    <div class="alert alert-info">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>ข้อความแจ้ง! </strong>
        ท่านไม่มีสิทธิ์เข้าดูข้อมูลในหน่วยบริการนี้ หรือ ไม่มีข้อมูลในหน่วยบริการของท่าน
    </div>

<?php
    exit();
}
$num = 1;
$hospital_name = $hosname[0][name];
$amphur_name = $hosname[0][amphur];
$province_name = $hosname[0][province];
?>
<!-- h3><?= $hospital_name ?> อำเภอ <?= $amphur_name ?> จังหวัด <?= $province_name ?></h3 -->
<h3>
<?php
    if( strlen($request->get('col'))>0 ){
        echo 'รายชื่อตาม'.$report4title.' ทั้งหมด '.count($dataProvider).' คน';
    }else{
        echo 'ตำบล '.$dataArrayDrillSec1[0][tambon].' อำเภอ '.$dataArrayDrillSec1[0][amphur].'จังหวัด '.$dataArrayDrillSec1[0][province];
    }
?> 
</h3>
<table class="table table-striped">
    <thead>
    <tr>
        <th style="min-width: 70px; text-align: right;">
            ลำดับที่
        </th>
        <th style="min-width: 200px; text-align: left;">
            ชื่อ-สกุล
        </th>
        <th style="min-width: 250px; text-align: left;">
            รับการรักษาที่
        </th>
        <th style="min-width: 100px; text-align: left;">
            Kato-Katz
        </th>
        <th>
            Parasep
        </th>
        <th>
            FECT
        </th>
        <th>
            Urine
        </th>
        <th>
            ov
        </th>
        <th>
            mif
        </th>
        <th>
            ss
        </th>
        <th>
            ech
        </th>
        <th>
            taenia
        </th>
        <th>
            tt
        </th>
        <th>
            other
        </th>
    </tr>
    </thead>
    <tbody>
    <?php
    foreach ($dataProvider as $value3) {
        $nameperson = $value3[title] . " " . $value3[fname] . " " . $value3[surname];
        $kpos = DrillDownSec3Controller::GetIcon($value3[kpos]);
        $ppos = DrillDownSec3Controller::GetIcon($value3[ppos]);
        $fpos = DrillDownSec3Controller::GetIcon($value3[fpos]);
        $upos = DrillDownSec3Controller::GetIcon($value3[upos]);
        $ov = DrillDownSec3Controller::GetIcon($value3[ov]);
        $mif = DrillDownSec3Controller::GetIcon($value3[mif]);
        $ss = DrillDownSec3Controller::GetIcon($value3[ss]);
        $ech = DrillDownSec3Controller::GetIcon($value3[ech]);
        $taenia = DrillDownSec3Controller::GetIcon($value3[taenia]);
        $tt = DrillDownSec3Controller::GetIcon($value3[tt]);
        $other = DrillDownSec3Controller::GetIcon($value3[other]);
        ?>
        <tr>
            <td style="text-align: right;">
                <?= $num ?>
            </td>
            <td style="text-align: left;">
                <?php
                    if( $value3['hsitecode']==$usersitecode ){
                        $url_id="https://cloudbackend.cascap.in.th/inputdata/redirect-page/?ezf_id=1437377239070461301&dataid=".$value3['id'];
                        echo Html::a($nameperson
                                , 'https://cloudbackend.cascap.in.th'.Url::to([
                                    '/inputdata/redirect-page',
                                    'ezf_id'=>'1437377239070461301',
                                    'dataid'=>$value3['id'],
                                    ])
                                , [
                                    'target'=>'_blank',
                                ]);
                    }else{
                        echo $nameperson;
                    }
                ?>
            </td>
            <td style="text-align: left;">
                <?php
                    echo $value3['sitecode'].': ';
                    echo DrillDownSec2Controller::ShortName($value3[hospitalname]) 
                ?>
            </td>
            <td>
                <?= $kpos ?>
            </td>
            <td>
                <?= $ppos ?>
            </td>
            <td>
                <?= $fpos ?>
            </td>
            <td>
                <?= $upos ?>
            </td>
            <td>
                <?= $ov ?>
            </td>
            <td>
                <?= $mif ?>
            </td>
            <td>
                <?= $ss ?>
            </td>
            <td>
                <?= $ech ?>
            </td>
            <td>
                <?= $taenia ?>
            </td>
            <td>
                <?= $tt ?>
            </td>
            <td>
                <?= $other ?>
            </td>
        </tr>
        <?php
        $num++;
    }//foreach
    ?>
    </tbody>
</table>
</br>
</br>
