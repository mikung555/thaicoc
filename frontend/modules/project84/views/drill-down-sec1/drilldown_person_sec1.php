<?php

use yii\helpers\Html;
use yii\grid\GridView;
use frontend\modules\project84\controllers\DrillDownSec1Controller;

?>
<?php

if( 0 ){
    echo "<pre align='left'>";
    print_r($dataProvider);
    echo "</pre>";
}
if( count($dataProvider)==0 ){
    //
?>
    <div class="alert alert-warning">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>ข้อความแจ้ง! </strong>
        ไม่พบข้อมูล
    </div>
<?php
}else{
    foreach ($dataProvider as $value1) {
        $provincecode = $value1[provincecode];
        $dataAmphur = DrillDownSec1Controller::GetAmphur($report_id, $provincecode);
        ?>
        <h3>จังหวัด <?= $value1[province] ?></h3>
        <table width="80%" align="center">
            <thead>
            <tr>
                <th>
                </th>
                <th style="text-align: right;">
                    จำนวนผู้ที่ได้รับการตรวจ(คน)
                </th>
            </tr>
            </thead>
            <tbody>
            <?php
            foreach ($dataAmphur as $value2) {
                ?>
                <tr>
                    <td colspan="2">
                        อำเภอ <?= $value2[amphur] ?>
                    </td>
                </tr>
                <?php
                $dataTumbon = DrillDownSec1Controller::GetTumbon($report_id, $provincecode, $value2[amphurcode]);
                foreach ($dataTumbon as $value3) {
                    ?>
                    <tr>
                        <td style="background-color:lightblue">
                            ตำบล <?= $value3[tambon] ?>
                        </td>
                        <td style="text-align: right; background-color:lightblue">
                            <?= number_format($value3[total],0,'.',','); ?> คน
                        </td>
                    </tr>
                    <?php
                    $dataSite = DrillDownSec1Controller::GetSite($report_id, $value3[tamboncode]);
                    foreach ($dataSite as $value4) {
                        ?>
                        <tr>
                            <td>
                                <?= $value4[sitecode]." ".$value4[name] ?>
                            </td>
                            <td style="text-align: right;">
                                <?= number_format($value4[total],0,'.',','); ?> คน
                            </td>
                        </tr>
                        <?php
                    }//site
                }//tumbon
            }//amphur
            ?>
            </tbody>
        </table>
        </br>
        </br>
        <?php
    }
}
?>
