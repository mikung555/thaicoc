<?php

use yii\helpers\Html;
use yii\grid\GridView;
use frontend\modules\project84\controllers\ReportController;
use frontend\modules\project84\classes\HospitalQuery;

Yii::$app->formatter->locale = 'th-TH';

$user_sitecode = Yii::$app->user->identity->userProfile->sitecode;
$_target_report1 = 120000;
if( strlen($user_sitecode)>0 ){
    $hdet = HospitalQuery::getTambonCode($user_sitecode);
}



?>
<div class="container" style="width: 100%">
    <div class="alert alert-info" role="alert">
        <h4>ส่วนที่ 1 : รายงาน Primary prevention (คัดกรองพยาธิใบไม้ตับด้วยการตรวจอุจจาระและปัสสาวะ)</h4>
    </div>
    <div class="row">
        <div class="col-md-7">
            <h5>
                ผลงานตรวจอุจจาระและปัสสาวะ
                <label class="label label-primary"><?php echo number_format($dataReportSumAllSec1[0]["sum(treatov)"],0,'.',',') ?></label>ราย
                คิดเป็น
                <label
                    class="label label-primary"><?php echo ReportController::calpercent($dataReportSumAllSec1[0]["sum(treatov)"], $_target_report1, "no") ?>
                    %</label> จากเป้าหมายรวมทั้งประเทศ <?php echo number_format($_target_report1,0,'.',','); ?> ราย
            </h5>
        </div>
        <div class="col-md-5" style="text-align: right;">
            <?php
//                echo Html::a('Update เฉพาะส่วน OV', 
//                        $url, 
//                        [
//                            'class'=>'btn btn-default',
//                            'id'=>'btnBySecRefreshAllSec1',
//                            ]
//                        );
            ?>
            <button type="button" class="btn btn-info" onclick="javascript:window.open('http://tools.cascap.in.th/v4cascap/testexcel/PHPExcel_1/Examples/01report60section01xlsx.php?rid=1')">
                <i class="fa fa-file-excel-o" aria-hidden="true"></i>
                Export to Excel
            </button>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="progress-bar-success-head" role="progressbar"
                 aria-valuenow="<?php echo number_format($dataReportSumAllSec1[0]["sum(treatov)"],0,'.',',') ?>" aria-valuemin="0"
                 aria-valuemax="135000"
                 style="width: <?php echo ReportController::calpercent($dataReportSumAllSec1[0]["sum(treatov)"], $_target_report1, "no") ?>%"></div>
        </div>
        <div class="col-md-12">
            <h5>
                ผลงานในช่วงวันที่ <?php echo Yii::$app->formatter->asDate($datadate[fromDate], 'long') ?>
                ถึงวันที่ <?php echo Yii::$app->formatter->asDate($datadate[toDate], 'long') ?> จำนวน
                <label
                    class="label label-primary"><?php echo number_format($dataReportSumSec1[0]["sum(treatov)"]) ?></label>ราย
                คิดเป็น
                <label
                    class="label label-primary"><?php echo ReportController::calpercent($dataReportSumSec1[0]["sum(treatov)"], $_target_report1, "no") ?>
                    %</label>
            </h5>
        </div>
        <div class="col-md-12">
            <div class="progress-bar-success-head" role="progressbar"
                 aria-valuenow="<?php echo number_format($dataReportSumSec1[0]["sum(treatov)"],0,'.',',') ?>" aria-valuemin="0"
                 aria-valuemax="<?php echo $_target_report1; ?>"
                 style="width: <?php echo ReportController::calpercent($dataReportSumSec1[0]["sum(treatov)"], $_target_report1, "no") ?>%"></div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div id="report84-us-desc" class="alert alert-warning report84-desc">
                ผลงานตรวจคัดกรองพยาธิใบไม้ตับ รวม <?php echo number_format($dataReportSumSec1[0]["sum(treatov)"]) ?> ราย
                ติดเชื้อ <?php echo number_format($dataReportSumSec1[0]["sum(ov)"]) ?> <?php echo ReportController::calpercent($dataReportSumSec1[0]["sum(ov)"], $dataReportSumSec1[0]["sum(treatov)"], "yes") ?>
                ราย
            </div>
        </div>
    </div>
    
    <div class="row">
        <div class="col-md-12">
            หมายเหตุ: จำนวนตัวเลขที่แสดงได้จากการ Submit ข้อมูลเข้ามาแล้วเท่านั้น
        </div>
    </div>

    <div class="table-responsive">
        <div id="report84-dynagrid-2-pjax">
            <div id="report84-dynagrid-2" data-krajee-grid="kvGridInit_adf683c2">
                <div id="report84-dynagrid-2-container" class="table-responsive kv-grid-container">


                    <table class="kv-grid-table table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th class="kv-align-center kv-align-middle" style="width:40px;text-align:center;">#</th>
                            <th class="kv-align-center kv-align-middle" style="width:50px;text-align:left;"><a
                                    href="#table_secshow1" id='sort30' data-sort='asc'
                                    fromDate='<?php echo $datadate[fromDate] ?>'
                                    toDate='<?php echo $datadate[toDate] ?>' sec='1' colum='zone'>เขต</a></th>
                            <th class="kv-align-middle" style="width:50px;text-align:left;"><a href="#table_secshow1"
                                                                                                 id='sort31'
                                                                                                 data-sort='asc'
                                                                                                 fromDate='<?php echo $datadate[fromDate] ?>'
                                                                                                 toDate='<?php echo $datadate[toDate] ?>'
                                                                                                 sec='1'
                                                                                                 colum='province'>จังหวัด</a>
                            </th>
                            <th class="kv-align-middle" style="width:50px;text-align:left;"><a href="#table_secshow1"
                                                                                                 id='sort31'
                                                                                                 data-sort='asc'
                                                                                                 fromDate='<?php echo $datadate[fromDate] ?>'
                                                                                                 toDate='<?php echo $datadate[toDate] ?>'
                                                                                                 sec='1' colum='amphur'>อำเภอ</a>
                            </th>
                            <th class="kv-align-middle" style="width:50px;text-align:left;"><a href="#table_secshow1"
                                                                                                 id='sort31'
                                                                                                 data-sort='asc'
                                                                                                 fromDate='<?php echo $datadate[fromDate] ?>'
                                                                                                 toDate='<?php echo $datadate[toDate] ?>'
                                                                                                 sec='1' colum='tambon'>ตำบล</a>
                            </th>
                            <th class="kv-align-center kv-align-middle" style="min-width:120px;text-align:center;"><a
                                    href="#table_secshow1" id='sort32' data-sort='asc'
                                    fromDate='<?php echo $datadate[fromDate] ?>'
                                    toDate='<?php echo $datadate[toDate] ?>' sec='1' colum='treatov'>ความก้าวหน้า</a></th>
                            <th class="kv-align-right kv-align-middle" style="min-width:100px;text-align:right;"><a
                                    href="#table_secshow1" id='sort33' data-sort='asc'
                                    fromDate='<?php echo $datadate[fromDate] ?>'
                                    toDate='<?php echo $datadate[toDate] ?>' sec='1' colum='tccbot'>ประชากร</a></th>
                            <th class="kv-align-right kv-align-middle" style="min-width:110px;text-align:right;"><a
                                    href="#table_secshow1" id='sort34' data-sort='asc'
                                    fromDate='<?php echo $datadate[fromDate] ?>'
                                    toDate='<?php echo $datadate[toDate] ?>' sec='1'
                                    colum='riskgroup'>เลือกกลุ่มเสี่ยง</a></th>
                            <th class="kv-align-right kv-align-middle" style="min-width:90px;text-align:right;"><a
                                    href="#table_secshow1" id='sort35' data-sort='asc'
                                    fromDate='<?php echo $datadate[fromDate] ?>'
                                    toDate='<?php echo $datadate[toDate] ?>' sec='1' colum='icf'>มีใบยินยอม</a></th>
                            <th class="kv-align-right kv-align-middle" style="min-width:100px;text-align:right;"><a
                                    href="#table_secshow1" id='sort36' data-sort='asc'
                                    fromDate='<?php echo $datadate[fromDate] ?>'
                                    toDate='<?php echo $datadate[toDate] ?>' sec='1' colum='register'
                                    title='มีข้อมูล CCA-01 ที่เก็บในปีนี้ หรือมีผลตรวจ OV ในปีนี้'>ข้อมูลพื้นฐาน</a></th>
                            <th class="kv-align-right kv-align-middle" style="min-width:100px;text-align:right;"><a
                                    href="#table_secshow1" id='sort37' data-sort='asc'
                                    fromDate='<?php echo $datadate[fromDate] ?>'
                                    toDate='<?php echo $datadate[toDate] ?>' sec='1' colum='treatov'>ตรวจพยาธิ</a></th>
                            <th class="kv-align-right kv-align-middle" style="min-width:100px;text-align:right;"><a
                                    href="#table_secshow1" id='sort38' data-sort='asc'
                                    fromDate='<?php echo $datadate[fromDate] ?>'
                                    toDate='<?php echo $datadate[toDate] ?>' sec='1' colum='ov'>ติดเชื้อ OV</a></th>
                            <!--th class="kv-align-right kv-align-middle" style="min-width:100px;text-align:right;"><a
                                    href="#table_secshow1" id='sort39' data-sort='asc'
                                    fromDate='<?php echo $datadate[fromDate] ?>'
                                    toDate='<?php echo $datadate[toDate] ?>' sec='1' colum='ov02'>ให้สุขศึกษา</a></th-->
                            <th class="kv-align-right kv-align-middle" style="min-width:100px;text-align:right;"><a
                                    href="#table_secshow1" id='sort40' data-sort='asc'
                                    fromDate='<?php echo $datadate[fromDate] ?>'
                                    toDate='<?php echo $datadate[toDate] ?>' sec='1' colum='ov03'>ให้การรักษา</a></th>
                        </tr>

                        </thead>
                        <tbody>
                        <!-- TABLE DATA -->

                        <?php
                        $num = 1;
                        foreach ($dataProviderSec1 as $keysec1) {
                            ?>
                            <tr>
                                <td class="kv-align-center kv-align-middle"
                                    style="width:40px;text-align:center;"><?php echo $num ?></td>
                                <td class="kv-align-center kv-align-middle"
                                    style="width:50px;text-align:left;"><?php echo $keysec1[zone] ?></td>
                                <td class="kv-align-middle"
                                    style="width:138px;text-align:left;"><?php echo $keysec1[province] ?></td>
                                <td class="kv-align-middle"
                                    style="width:138px;text-align:left;"><?php echo $keysec1[amphur] ?></td>
                                <td class="kv-align-middle" style="width:138px;text-align:left;"><a id="drilldown"
                                                                                                      report_id="<?php echo $report_id ?>"
                                                                                                      tamboncode="<?php echo $keysec1[address] ?>"
                                                                                                      title="<?php echo $keysec1[province] ?>"
                                                                                                      sec="1"
                                                                                                      data-toggle="modal"
                                                                                                      data-target="#myModal"><?php echo $keysec1[tambon] ?></a>
                                </td>
                                <td class="kv-align-center kv-align-middle" style="width:10%;">
                                    <div onmouseover="$(this).popover('show')" onmouseout="$(this).popover('hide')"
                                         data-toggle="popover" data-html="true" data-placement="top"
                                         data-content="<?php echo number_format($keysec1[treatov]) ?><?php echo ReportController::calpercent($keysec1[treatov], 905, "yes") ?>">
                                        <div id="w88" class="progress">
                                            <div class="progress-bar-success progress-bar" role="progressbar"
                                                 aria-valuenow="229.4" aria-valuemin="0" aria-valuemax="100"
                                                 style="/*min-width: 2em;*/ max-width: 100%; width:<?php echo ReportController::calpercent($keysec1[treatov], 905, "no") ?>%;">
                                                <span class="sr-only"><?php ?>% Complete</span></div>
                                        </div>
                                    </div>
                                </td>
                                <td style="text-align:right;"><?php echo number_format($keysec1[tccbot]) ?></td>
                                <td style="text-align:right;">
                                    <?php
                                    if( strlen($user_sitecode)>0 && $hdet['address']==$keysec1[address]){
                                        echo Html::a(number_format($keysec1[riskgroup]), NULL, [
                                            'id'=>"drilldown",
                                            'report_id' => $report_id,
                                            'tamboncode' => $keysec1[address],
                                            'title' => $keysec1[province],
                                            'sec' => '1',
                                            'col' => 'riskgroup',
                                            'data-toggle' => 'modal',
                                            'data-target' => '#myModal',
                                        ]);
                                    }else{
                                        echo number_format($keysec1[riskgroup]);
                                    }
                                    ?>
                                </td>
                                <td style="text-align:right;">
                                    <?php 
                                    if( strlen($user_sitecode)>0 && $hdet['address']==$keysec1[address]){
                                        echo Html::a(number_format($keysec1[icf]), NULL, [
                                            'id'=>"drilldown",
                                            'report_id' => $report_id,
                                            'tamboncode' => $keysec1[address],
                                            'title' => $keysec1[province],
                                            'sec' => '1',
                                            'col' => 'icf',
                                            'data-toggle' => 'modal',
                                            'data-target' => '#myModal',
                                        ]);
                                    }else{
                                        echo number_format($keysec1[icf]); 
                                    }
                                    ?>
                                </td>
                                <td style="text-align:right;">
                                    <?php 
                                    if( strlen($user_sitecode)>0 && $hdet['address']==$keysec1[address]){
                                        echo Html::a(number_format($keysec1[cca01]), NULL, [
                                            'id'=>"drilldown",
                                            'report_id' => $report_id,
                                            'tamboncode' => $keysec1[address],
                                            'title' => 'มีข้อมูล CCA-01 ที่เก็บในปีนี้ หรือมีผลตรวจ OV ในปีนี้',
                                            'sec' => '1',
                                            'col' => 'cca01',
                                            'data-toggle' => 'modal',
                                            'data-target' => '#myModal',
                                        ]);
                                    }else{
                                        echo number_format($keysec1[cca01]);
                                    }
                                    ?>
                                </td>
                                <td style="text-align:right;">
                                    <?php 
                                    if( strlen($user_sitecode)>0 && $hdet['address']==$keysec1[address]){
                                        echo Html::a(number_format($keysec1[treatov]), NULL, [
                                            'id'=>"drilldown",
                                            'report_id' => $report_id,
                                            'tamboncode' => $keysec1[address],
                                            'title' => 'มีการบันทึกผลการตรวจ พยาธิใบไม้ตับ',
                                            'sec' => '1',
                                            'col' => 'treatov',
                                            'data-toggle' => 'modal',
                                            'data-target' => '#myModal',
                                        ]);
                                    }else{
                                        echo number_format($keysec1[treatov]);
                                    } 
                                    ?>
                                </td>
                                <td style="text-align:right;">
                                    <?php 
                                    if( strlen($user_sitecode)>0 && $hdet['address']==$keysec1[address]){
                                        echo Html::a(number_format($keysec1[ov]), NULL, [
                                            'id'=>"drilldown",
                                            'report_id' => $report_id,
                                            'tamboncode' => $keysec1[address],
                                            'title' => 'มีผลตรวจเป็น + (Positive)',
                                            'sec' => '1',
                                            'col' => 'ov',
                                            'data-toggle' => 'modal',
                                            'data-target' => '#myModal',
                                            'class' => 'link',
                                        ]);
                                    }else{
                                        echo number_format($keysec1[ov]); 
                                    }
                                    ?>
                                    <?php echo ReportController::calpercent($keysec1[ov], $keysec1[treatov], "yes") ?>
                                </td>
                                <!--td style="text-align:right;"><?php echo number_format($keysec1[ov02]) ?></td-->
                                <td style="text-align:right;">
                                    <?php 
                                    if( strlen($user_sitecode)>0 && $hdet['address']==$keysec1[address]){
                                        echo Html::a(number_format($keysec1[ov03]), NULL, [
                                            'id'=>"drilldown",
                                            'report_id' => $report_id,
                                            'tamboncode' => $keysec1[address],
                                            'title' => 'ให้การรักษา (ได้รับยาหลังจากตรวจเจอพยาธิ์)',
                                            'sec' => '1',
                                            'col' => 'ov03',
                                            'data-toggle' => 'modal',
                                            'data-target' => '#myModal',
                                            'class' => 'link',
                                        ]);
                                    }else{
                                        echo number_format($keysec1[ov03]); 
                                    }
                                    ?>
                                </td>
                            </tr>
                            <?php
                            $sumtccbot += $keysec1[tccbot];
                            $sumriskgroup += $keysec1[riskgroup];
                            $sumicf += $keysec1[icf];
                            $sumregister += $keysec1[register];
                            $sumtreatov += $keysec1[treatov];
                            $sumrovpos += $keysec1[ov];
                            $sumov02 += $keysec1[ov02];
                            $sumov03 += $keysec1[ov03];
                            $num++;
                        }
                        ?>

                        <tr>
                            <td class="kv-align-center kv-align-middle" style="width:40px;">&nbsp;</td>
                            <td class="kv-align-center kv-align-middle" style="width:80px;">&nbsp;</td>
                            <td class="kv-align-center kv-align-middle" style="width:10%;">&nbsp;</td>
                            <td class="kv-align-center kv-align-middle" style="width:10%;">&nbsp;</td>
                            <td class="kv-align-center kv-align-middle" style="width:10%;">&nbsp;</td>
                            <td class="kv-align-middle" style="text-align:center;">รวม</td>
                            <td style="text-align:right;"><?= number_format($sumtccbot) ?></td>
                            <td style="text-align:right;"><?= number_format($sumriskgroup) ?></td>
                            <td style="text-align:right;"><?= number_format($sumicf) ?></td>
                            <td style="text-align:right;"><?= number_format($sumregister) ?></td>
                            <td style="text-align:right;"><?= number_format($sumtreatov) ?></td>
                            <td style="text-align:right;"><?= number_format($sumrovpos) ?><?php echo ReportController::calpercent($sumrovpos, $sumtreatov, "yes") ?></td>
                            <!--td style="text-align:right;"><?= number_format($sumov02) ?></td-->
                            <td style="text-align:right;"><?= number_format($sumov03) ?></td>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <?php
    $jsAdd = <<< JS
var sort="$sort";
switch (sort) {
    case "asc":
        var data_sortadd=$("#$div").attr("data-sort","desc");
        break;
    case "desc":
        var data_sortadd=$("#$div").attr("data-sort","asc");
}

JS;
    $this->registerJs($jsAdd);
    ?>
