<?php

use yii\helpers\Html;
use yii\grid\GridView;

Yii::$app->formatter->locale = 'th-TH';

$_target_report3 = 800;

?>
<div class="container" style="width: 100%">
    <div class="alert alert-info" role="alert"><h4>ส่วนที่ 3 : รายงาน Tertiary care
            (รักษามะเร็งท่อน้ำดีด้วยการผ่าตัด)</h4></div>
    <div class="row">
        <div class="col-md-10">
            <h5 title="เป้าหมายผ่าตัดรวมทั้งประเทศ <?php echo $_target_report3; ?>  รายนับเฉพาะ Liver resection, Hilar resection, Bypass, Exploratory laparotomy ± biopsy และ Whipple’s operation">
                ผลงานผ่าตัด
                <label class="label label-primary"><?php echo number_format($dataReportSumAllSec3[0]["sum(surgery)"],0,'.',',') ?></label>
                ราย คิดเป็น
                <label
                    class="label label-primary"><?php echo number_format((($dataReportSumAllSec3[0]["sum(surgery)"]) / $_target_report3) * 100, 1) ?>
                    %</label> จากเป้าหมายรวมทั้งประเทศ <?php echo number_format($_target_report3,0,'.',','); ?> ราย

            </h5>
        </div>
        <div class="col-md-2" style="alignment-adjust: right;">
            <button type="button" class="btn btn-info" onclick="javascript:window.open('http://tools.cascap.in.th/v4cascap/testexcel/PHPExcel_1/Examples/01report60section03xlsx.php?rid=1')">
                <i class="fa fa-file-excel-o" aria-hidden="true"></i>
                Export to Excel
            </button>
        </div>
    </div>
    <div class="progress overall-progress" onmouseover="$(this).popover('show')" onmouseout="$(this).popover('hide')"
         data-toggle="popover" data-html="true" data-placement="top" data-content="<?php echo $dataReportSumAllSec3[0]["sum(surgery)"]; ?> (<?php echo number_format(((($dataReportSumAllSec3[0]["sum(surgery)"]) / $_target_report3) * 100),1,'.',',') ?> %)" data-original-title=""
         title="">
        <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="<?php echo number_format((($dataReportSumAllSec3[0]["sum(surgery)"]) / $_target_report3) * 100,1) ?>" aria-valuemin="0"
             aria-valuemax="<?php echo $_target_report3; ?>"
             style="width:<?php echo number_format((($dataReportSumAllSec3[0]["sum(surgery)"]) / $_target_report3) * 100,1) ?>%">
        </div>
    </div>
    <h5 title="เป้าหมายผ่าตัดรวมทั้งประเทศ 600 รายนับเฉพาะ Liver resection, Hilar resection, Bypass, Exploratory laparotomy ± biopsy และ Whipple’s operation">
        ผลงานในช่วงวันที่ <?php echo Yii::$app->formatter->asDate($datadate[fromDate], 'long') ?>
        ถึงวันที่ <?php echo Yii::$app->formatter->asDate($datadate[toDate], 'long') ?>
        <label class="label label-primary"><?php echo number_format($dataReportSumSec3[0]["sum(surgery)"],0,'.',',') ?></label>ราย
        คิดเป็น
        <label
            class="label label-primary"><?php echo number_format((($dataReportSumSec3[0]["sum(surgery)"]) / $_target_report3) * 100, 1) ?>
            %</label>
    </h5>
    <!--div class="progress overall-progress" onmouseover="$(this).popover('show')" onmouseout="$(this).popover('hide')"
         data-toggle="popover" data-html="true" data-placement="top" data-content="444 (74.0 %)" data-original-title=""
         title="">
        <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="74" aria-valuemin="0"
             aria-valuemax="<?php echo $_target_report3; ?>"
             style="width: <?php echo number_format((($dataReportSumSec3[0]["sum(surgery)"]) / $_target_report3) * 100) ?>%">
        </div>
    </div-->
    <div id="report84-cca-desc" class="alert alert-warning report84-desc">
        ผลงานการรักษาทั้งหมด <?php echo number_format($dataReportSumSec3[0]["sum(treatccapos)"],0,'.',',') ?> ราย
        จำแนกเป็นผ่าตัด <?php echo number_format($dataReportSumSec3[0]["sum(surgery)"],0,'.',',') ?> ราย
        คิดเป็น <?php echo number_format((($dataReportSumSec3[0]["sum(surgery)"]) / $dataReportSumSec3[0]["sum(treatccapos)"]) * 100, 1) ?>
        % ของทั้งหมด และคิดเป็น <?php echo number_format((($dataReportSumSec3[0]["sum(surgery)"]) / $_target_report3) * 100, 1) ?> %
        จากเป้าหมายผ่าตัด <?php echo number_format($_target_report3,0,'.',','); ?> คน ในจำนวนที่ผ่าตัดนี้
        เป็นผ่าตัดให้หายขาด <?php echo number_format($dataReportSumSec3[0]["sum(c_surgery)"],0,'.',',') ?> ราย
        และผ่าตัดเพื่อการประคับประคอง <?php echo number_format(($dataReportSumSec3[0]["sum(surgery)"] - $dataReportSumSec3[0]["sum(c_surgery)"])) ?>
        ราย
        สำหรับการรักษาแบบประคับประคองทั้งสิ้น <?php echo number_format(($dataReportSumSec3[0]["sum(treatccapos)"]-($dataReportSumSec3[0]["sum(chemo_adjuvant)"]+$dataReportSumSec3[0]["sum(surgery)"]))) ?>
        ราย
        คิดเป็น <?php echo number_format((($dataReportSumSec3[0]["sum(treatccapos)"]-($dataReportSumSec3[0]["sum(chemo_adjuvant)"]+$dataReportSumSec3[0]["sum(surgery)"])) / $dataReportSumSec3[0]["sum(treatccapos)"]) * 100, 1) ?>
        % ของผู้ป่วยทั้งหมด ทั้งนี้ ได้รวมการผ่าตัดเพื่อการประคับประคองไว้ในจำนวนนี้ด้วยแล้ว
    </div>
    
    <div class="row">
        <div class="col-md-12">
            หมายเหตุ: จำนวนตัวเลขที่แสดงได้จากการ Submit ข้อมูลเข้ามาแล้วเท่านั้น
        </div>
    </div>

    <div style="overflow-x:auto;">

        <table>
            <tr>
                <th rowspan="2" style="min-width:20px;text-align:center; ">#</th>
                <th width="500px" style="min-width:250px;text-align:center; font-size:12px;" rowspan="2"><a href="#table_secshow3"
                                                                                            id='sort12' data-sort='asc'
                                                                                            fromDate='<?php echo $datadate[fromDate] ?>'
                                                                                            toDate='<?php echo $datadate[toDate] ?>'
                                                                                            sec='3' colum='hosname'>ชื่อโรงพยาบาล</a>
                </th>
                
                <th style="min-width:100px;text-align:center; font-size:12px;" rowspan="2"><a href="#table_secshow3" id='sort13'
                                                                              data-sort='asc'
                                                                              fromDate='<?php echo $datadate[fromDate] ?>'
                                                                              toDate='<?php echo $datadate[toDate] ?>'
                                                                              sec='3' colum='resgis'>ลงทะเบียน <br />เพื่อ <br />วินิจฉัยรักษา</a></th>
                
                <th style="min-width:120px;text-align:center; font-size:12px;" rowspan="2"><a href="#table_secshow3" id='sort14'
                                                                              data-sort='desc'
                                                                              fromDate='<?php echo $datadate[fromDate] ?>'
                                                                              toDate='<?php echo $datadate[toDate] ?>'
                                                                              sec='3' colum='treat'>รักษารวม CCA</a></th>
                <th colspan="3" style="min-width:200px;text-align:center;font-size:12px;">ผ่าตัด</th>
                <th colspan="6" style="text-align:center;font-size:12px;">ประเภทผ่าตัด</th>
                <th colspan="3" style="text-align:center;font-size:12px;">หัตถการอื่นๆ</th>
                <th colspan="3" style="text-align:center;font-size:14px;">การรักษาอื่นๆ</th>

            </tr>
            <tr style="font-weight:bold; text-align:center; font-size:12px;">
                <td style="text-align:center;"><a href="#table_secshow3" id='sort15' data-sort='asc'
                                                  fromDate='<?php echo $datadate[fromDate] ?>'
                                                  toDate='<?php echo $datadate[toDate] ?>' sec='3' colum='surgery'>รวมผ่าตัด</a>
                </td>
                <td style="text-align:center;"><a href="#table_secshow3" id='sort16' data-sort='asc'
                                                  fromDate='<?php echo $datadate[fromDate] ?>'
                                                  toDate='<?php echo $datadate[toDate] ?>' sec='3' colum='c_surgery'>ผ่าตัดให้หายขาด</a>
                </td>
                <td style="text-align:center;"><a href="#table_secshow3" id='sort17' data-sort='asc'
                                                  fromDate='<?php echo $datadate[fromDate] ?>'
                                                  toDate='<?php echo $datadate[toDate] ?>' sec='3' colum='c_surgery'>ผ่าตัดเพื่อประคับประคอง</a>
                </td>
                <td style="text-align:center;"><a href="#table_secshow3" id='sort18' data-sort='asc'
                                                  fromDate='<?php echo $datadate[fromDate] ?>'
                                                  toDate='<?php echo $datadate[toDate] ?>' sec='3' colum='livrec'>Liver
                        resection</a></td>
                <td style="text-align:center;"><a href="#table_secshow3" id='sort19' data-sort='asc'
                                                  fromDate='<?php echo $datadate[fromDate] ?>'
                                                  toDate='<?php echo $datadate[toDate] ?>' sec='3' colum='livhilar'>Liver
                        + Hilar</a></td>
                <td style="text-align:center;"><a href="#table_secshow3" id='sort20' data-sort='asc'
                                                  fromDate='<?php echo $datadate[fromDate] ?>'
                                                  toDate='<?php echo $datadate[toDate] ?>' sec='3' colum='whipple'>Whipple's
                        opeation</a></td>
                <td style="text-align:center;"><a href="#table_secshow3" id='sort21' data-sort='asc'
                                                  fromDate='<?php echo $datadate[fromDate] ?>'
                                                  toDate='<?php echo $datadate[toDate] ?>' sec='3' colum='hilar'>Hilar
                        resection</a></td>
                <td style="text-align:center;"><a href="#table_secshow3" id='sort22' data-sort='asc'
                                                  fromDate='<?php echo $datadate[fromDate] ?>'
                                                  toDate='<?php echo $datadate[toDate] ?>' sec='3'
                                                  colum='bypass'>Bypass</a></td>
                <td style="text-align:center;"><a href="#table_secshow3" id='sort23' data-sort='asc'
                                                  fromDate='<?php echo $datadate[fromDate] ?>'
                                                  toDate='<?php echo $datadate[toDate] ?>' sec='3' colum='biopsy'>Biopsy(EL+Bx)</a>
                </td>
                <td style="text-align:center;"><a href="#table_secshow3" id='sort24' data-sort='asc'
                                                  fromDate='<?php echo $datadate[fromDate] ?>'
                                                  toDate='<?php echo $datadate[toDate] ?>' sec='3'
                                                  colum='needle_biopsy'>Needle Biopsy</a></td>
                <td style="text-align:center;"><a href="#table_secshow3" id='sort25' data-sort='asc'
                                                  fromDate='<?php echo $datadate[fromDate] ?>'
                                                  toDate='<?php echo $datadate[toDate] ?>' sec='3' colum='ptbd'>PTBD</a>
                </td>
                <td style="text-align:center;"><a href="#table_secshow3" id='sort26' data-sort='asc'
                                                  fromDate='<?php echo $datadate[fromDate] ?>'
                                                  toDate='<?php echo $datadate[toDate] ?>' sec='3' colum='endoscopic'>Endoscopic
                        Stent</a></td>
                <td style="text-align:center;"><a href="#table_secshow3" id='sort27' data-sort='asc'
                                                  fromDate='<?php echo $datadate[fromDate] ?>'
                                                  toDate='<?php echo $datadate[toDate] ?>' sec='3' colum='chemo_all'>Chemotheraphy(All)</a>
                </td>
                <td style="text-align:center;"><a href="#table_secshow3" id='sort28' data-sort='asc'
                                                  fromDate='<?php echo $datadate[fromDate] ?>'
                                                  toDate='<?php echo $datadate[toDate] ?>' sec='3'
                                                  colum='chemo_adjuvant'>Chemotheraphy(Adjuvant)</a></td>
                <td style="text-align:center;"><a href="#table_secshow3" id='sort29' data-sort='asc'
                                                  fromDate='<?php echo $datadate[fromDate] ?>'
                                                  toDate='<?php echo $datadate[toDate] ?>' sec='3' colum='medication'>Medication</a>
                </td>


            </tr>

            <?php
            $num = 1;
            foreach ($dataProviderSec3 as $key) {
                ?>
                <tr>
                    <td style="text-align:center;"><?php echo $num ?></td>
                    <td id="hospital"><a id="drilldown" report_id="<?php echo $report_id ?>"
                                         hsitecode="<?php echo $key[sitecode] ?>" 
                                         vsite="<?php echo $key[sitecode] ?>"
                                         title="<?php echo $key[hosname] ?>"
                                         treat="<?php echo $key[treat] ?>" surgery="<?php echo $key[surgery] ?>"
                                         c_surgery="<?php echo $key[c_surgery] ?>"
                                         p_surgery="<?php echo($key[surgery] - $key[c_surgery]) ?>" sec="3"
                                         data-toggle="modal" data-target="#myModal"><?php echo $key[hosname] ?></a></td>
                    <td id="register" style="text-align:right;"><?php echo number_format($key[resgis]) ?></td>
                    <td style="text-align:right;"><?php echo number_format($key[treatccapos]) ?></td>
                    <td style="text-align:right;"><?php echo number_format($key[surgery]) ?></td>
                    <td style="text-align:right;"><?php echo number_format($key[c_surgery]) ?></td>
                    <td style="text-align:right;"><?php echo number_format(($key[surgery] - $key[c_surgery])) ?></td>
                    <td style="text-align:right;"><?php echo number_format($key[livrec]) ?></td>
                    <td style="text-align:right;"><?php echo number_format($key[livhilar]) ?></td>
                    <td style="text-align:right;"><?php echo number_format($key[whipple]) ?></td>
                    <td style="text-align:right;"><?php echo number_format($key[hilar]) ?></td>
                    <td style="text-align:right;"><?php echo number_format($key[bypass]) ?></td>
                    <td style="text-align:right;"><?php echo number_format($key[biopsy]) ?></td>
                    <td style="text-align:right;"><?php echo number_format($key[needle_biopsy]) ?></td>
                    <td style="text-align:right;"><?php echo number_format($key[ptbd]) ?></td>
                    <td style="text-align:right;"><?php echo number_format($key[endoscopic]) ?></td>
                    <td style="text-align:right;"><?php echo number_format($key[chemo_all]) ?></td>
                    <td style="text-align:right;"><?php echo number_format($key[chemo_adjuvant]) ?></td>
                    <td style="text-align:right;"><?php echo number_format($key[medication]) ?></td>

                </tr>
                <?php
                $sumregis += $key[resgis];
                $sumtreat += $key[treatccapos];
                $sumsurgery += $key[surgery];
                $sumc_surgery += $key[c_surgery];
                $sumn_surgery += ($key[surgery] - $key[c_surgery]);
                $sumlivrec += $key[livrec];
                $sumlivhilar += $key[livhilar];
                $sumwhipple += $key[whipple];
                $sumhilar += $key[hilar];
                $sumbypass += $key[bypass];
                $sumbiopsy += $key[biopsy];
                $sumneedle_biopsy += $key[needle_biopsy];
                $sumchemo_all += $key[chemo_all];
                $sumchemo_adjuvant += $key[chemo_adjuvant];
                $sumptbd += $key[ptbd];
                $sumendoscopic += $key[endoscopic];
                $summedication += $key[medication];
                $num++;
            }

            ?>

            <tr>
                <td colspan="2" style="text-align:center;"> รวม</td>
                <td style="text-align:right;"><?php echo number_format($sumregis) ?></td>
                <td style="text-align:right;"><?php echo number_format($sumtreat) ?></td>
                <td style="text-align:right;"><?php echo number_format($sumsurgery) ?></td>
                <td style="text-align:right;"> <?php echo number_format($sumc_surgery) ?></td>
                <td style="text-align:right;"> <?php echo number_format($sumn_surgery) ?></td>
                <td style="text-align:right;"> <?php echo number_format($sumlivrec) ?></td>
                <td style="text-align:right;"> <?php echo number_format($sumlivhilar) ?></td>
                <td style="text-align:right;"> <?php echo number_format($sumwhipple) ?></td>
                <td style="text-align:right;"><?php echo number_format($sumhilar) ?> </td>
                <td style="text-align:right;"> <?php echo number_format($sumbypass) ?></td>
                <td style="text-align:right;"> <?php echo number_format($sumbiopsy) ?></td>
                <td style="text-align:right;"> <?php echo number_format($sumneedle_biopsy) ?></td>
                <td style="text-align:right;"> <?php echo number_format($sumchemo_all) ?></td>
                <td style="text-align:right;"> <?php echo number_format($sumchemo_adjuvant) ?></td>
                <td style="text-align:right;"> <?php echo number_format($sumptbd) ?></td>
                <td style="text-align:right;"><?php echo number_format($sumendoscopic) ?> </td>
                <td style="text-align:right;"><?php echo number_format($summedication) ?> </td>
            </tr>


        </table>


    </div>
</div>
<?php
$jsAdd = <<< JS
var sort="$sort";
switch (sort) {
    case "asc":
  var data_sortadd=$("#$div").attr("data-sort","desc");
        break;
        case "desc":
var data_sortadd=$("#$div").attr("data-sort","asc");
}

JS;
$this->registerJs($jsAdd);
?>
