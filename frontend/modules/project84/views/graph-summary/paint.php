<?php 
use yii\web\JsExpression;
?>
<html>

<body>
  
               <div  id="getimage"  >
                <?php 
                //echo $this->render('/project84/graph-summary/paint');
                ?>
                      
                   <div class="img-container"> <img class="img-responsive" src="/img/bgsummary1.png" /></div> 
                      
                            <div id="text1" class="col-xs-5 col-sm-8 col-lg-12"><?= number_format($summaryshow['data12']) ?> ราย</div>
                            <div id="text2" class="col-xs-5 col-sm-8 col-lg-12">(ผ่าตัดเพื่อหายขาด <?= number_format($summaryshow['data13'])?> ราย/ ผ่าตัดประคับประคอง <?= number_format($summaryshow['data14'])?> ราย/ อื่นๆ <?= number_format($summaryshow['data15'])?> ราย)</div>
                            <div id="text3" class="col-xs-5 col-sm-8 col-lg-12"><?= number_format($summaryshow['data10'])?> ราย / <?= number_format($summaryshow['data11'])?> ครั้ง (positive = <?= number_format($summaryshow['postb8'])?> ราย)</div>
                            <div id="text4" class="col-xs-5 col-sm-8 col-lg-12"><?= number_format($summaryshow['data8'])?> ราย / <?= number_format($summaryshow['data9'])?> ครั้ง   (confirmed = <?= number_format($summaryshow['confirm'])?> ราย)</div> 
                            <div id="text5" class="col-xs-5 col-sm-8 col-lg-12"><?= number_format($summaryshow['data6'])?> ราย / <?= number_format($summaryshow['data7'])?> ครั้ง  (suspected = <?= number_format($summaryshow['suspect'])?> ราย)</div>
                            <div id="text6" class="col-xs-5 col-sm-8 col-lg-12"><?= number_format($summaryshow['data4'])?> ราย / <?= number_format($summaryshow['data5'])?> ครั้ง (positive = <?= number_format($summaryshow['posov'])?> ราย)</div>
                            <div id="text7" class="col-xs-5 col-sm-8 col-lg-12">ลงทะเบียน <?= number_format($summaryshow['data2']) ?> ราย / ข้อมูลพื้นฐาน <?= number_format($summaryshow['data3'])?> ราย</div>
                            <div id="text8" class="col-xs-5 col-sm-8 col-lg-12"><?= number_format($summaryshow['data1'])?> ราย</div>
              
                </div> 
                <div id="calsum" class="col-xs-8 col-sm-8 col-lg-12">
                    <b>[&nbsp;คำนวนกราฟล่าสุดเมื่อ&nbsp;<?= $summaryshow['lastupdate']?>&nbsp;]</b> 
                </div>         
<?php 

$this->registerJS("

        $('#text1').fadeIn(5000);
        $('#text2').fadeIn(4800);
        $('#text3').fadeIn(4200);
        $('#text4').fadeIn(3800);
        $('#text5').fadeIn(3200);
        $('#text6').fadeIn(2800);
        $('#text7').fadeIn(2500);
        $('#text8').fadeIn(2000); 
        
"); 

 
?>

</body>
</html>