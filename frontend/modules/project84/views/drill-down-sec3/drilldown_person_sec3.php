<?php

use yii\helpers\Html;
use yii\grid\GridView;
use frontend\modules\project84\controllers\DrillDownSec3Controller;

if ($status == "no") {
    //echo "ท่านไม่มีสิทธิ์เข้าดูข้อมูลในหน่วยบริการนี้";
?>
    <div class="alert alert-info">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>ข้อความแจ้ง! </strong>
        ท่านไม่มีสิทธิ์เข้าดูข้อมูลในหน่วยบริการนี้ หรือ ไม่มีข้อมูลในหน่วยบริการของท่าน
    </div>
<?php
    exit();
}
$checkyes = '<span class="glyphicon glyphicon-ok" aria-hidden="true" style="color:blue"></i>';
$checkno = '<i class="glyphicon glyphicon-minus" aria-hidden="true" style="color:blue"></i>';
$num = 1;
$hospital_name = $hosname[0][name];
$amphur_name = $hosname[0][amphur];
$province_name = $hosname[0][province];
?>
<h3><?= $hospital_name ?> อำเภอ <?= $amphur_name ?> จังหวัด <?= $province_name ?></h3>
<div id="report84-ajax-report-div">
    <div style="overflow-x:auto;">
        <table>
            <thead>
            <tr>
                <th style="min-width: 80px; text-align: right;">
                    ลำดับที่
                </th>
                <th style="min-width: 60px; text-align: left;">
                    PID
                </th>
                <th style="min-width: 200px; text-align: left;">
                    ชื่อ สกุล
                </th>
                <th style="min-width: 250px; text-align: left;">
                    รับการรักษาที่
                </th>
                <th>
                    รักษา
                </th>
                <th style="min-width: 80px; text-align: right;">รวมผ่าตัด</th>
                <th style="min-width: 130px; text-align: right;">ผ่าตัดให้หายขาด</th>
                <th style="min-width: 180px; text-align: right;">ผ่าตัดเพื่อประคับประคอง</th>
                <th style="min-width: 120px; text-align: right;">Liver resection</th>
                <th style="min-width: 100px; text-align: right;">Liver + Hilar</th>
                <th style="min-width: 160px; text-align: right;">Whipples opeation</th>
                <th style="min-width: 130px; text-align: right;">Hilar resection</th>
                <th style="min-width: 70px; text-align: right;">Bypass</th>
                <th style="min-width: 100px; text-align: right;">Biopsy(EL+Bx)</th>
                <th style="min-width: 120px; text-align: right;">Needle Biopsy</th>
                <th style="min-width: 40px; text-align: right;">PTBD</th>
                <th style="min-width: 160px; text-align: right;">Endoscopic Stent</th>
                <th style="min-width: 160px; text-align: right;">Chemotheraphy(All)</th>
                <th style="min-width: 200px; text-align: right;">Chemotheraphy(Adjuvant)</th>
                <th style="min-width: 100px; text-align: right;">Medication</th>
            </tr>
            </thead>
            <tbody>
            <?php
            foreach ($dataProvider as $value3) {
                $target = base64_encode($value3[target]);
                $p_surgery1 = ($value3[surgery] - $value3[c_surgery]);
                $treat = DrillDownSec3Controller::GetIcon($value3[treat]);
                $surgery = DrillDownSec3Controller::GetIcon($value3[surgery]);
                $c_surgery = DrillDownSec3Controller::GetIcon($value3[c_surgery]);
                $p_surgery = DrillDownSec3Controller::GetIcon($p_surgery1);
                $livrec = DrillDownSec3Controller::GetIcon($value3[livrec]);
                $livhilar = DrillDownSec3Controller::GetIcon($value3[livhilar]);
                $whipple = DrillDownSec3Controller::GetIcon($value3[whipple]);
                $hilar = DrillDownSec3Controller::GetIcon($value3[hilar]);
                $bypass = DrillDownSec3Controller::GetIcon($value3[bypass]);
                $biopsy = DrillDownSec3Controller::GetIcon($value3[biopsy]);
                $needle_biopsy = DrillDownSec3Controller::GetIcon($value3[needle_biopsy]);
                $ptbd = DrillDownSec3Controller::GetIcon($value3[ptbd]);
                $endoscopic = DrillDownSec3Controller::GetIcon($value3[endoscopic]);
                $chemo_all = DrillDownSec3Controller::GetIcon($value3[chemo_all]);
                $chemo_adjuvant = DrillDownSec3Controller::GetIcon($value3[chemo_adjuvant]);
                $medication = DrillDownSec3Controller::GetIcon($value3[medication]);
                $nameperson = $value3[title] . " " . $value3[fname] . " " . $value3[surname];
                ?>
                <tr>
                    <td style="text-align: right;">
                        <?= $num ?>
                    </td>
                    <td style="text-align: right;">
                        <?php echo $value3['hptcode'] ?>
                    </td>
                    <td style="text-align: left;">
                        <?php
                            $url_id="https://cloudbackend.cascap.in.th/inputdata/redirect-page/?ezf_id=1437377239070461301&dataid=".$value3['id'];
                        ?>
                        <a href="<?php echo $url_id; ?>"
                           target="_blank"><?= $nameperson ?></a>
                    </td>
                    <td style="text-align: left;">
                        <?= DrillDownSec3Controller::ShortName($value3[hospitalname]) ?>
                    </td>
                    <td>
                        <?= $treat ?>
                    </td>
                    <td>
                        <?= $surgery ?>
                    </td>
                    <td>
                        <?= $c_surgery ?>
                    </td>
                    <td>
                        <?= $p_surgery ?>
                    </td>
                    <td>
                        <?= $livrec ?>
                    </td>
                    <td>
                        <?= $livhilar ?>
                    </td>
                    <td>
                        <?= $whipple ?>
                    </td>
                    <td>
                        <?= $hilar ?>
                    </td>
                    <td>
                        <?= $bypass ?>
                    </td>
                    <td>
                        <?= $biopsy ?>
                    </td>
                    <td>
                        <?= $needle_biopsy ?>
                    </td>
                    <td>
                        <?= $ptbd ?>
                    </td>
                    <td>
                        <?= $endoscopic ?>
                    </td>
                    <td>
                        <?= $chemo_all ?>
                    </td>
                    <td>
                        <?= $chemo_adjuvant ?>
                    </td>
                    <td>
                        <?= $medication ?>
                    </td>
                </tr>
                <?php
                $num++;
            }
            ?>
            </tbody>
        </table>
    </div>
</div>
