<?php

use yii\helpers\Html;
use yii\grid\GridView;
use frontend\modules\project84\controllers\DrillDownSec3Controller;

$province = DrillDownSec3Controller::GetProvince($hsitecode, $report_id);
?>
<div class="table-responsive">
    <ul class="nav nav-tabs" id="myTab">
        <li class="active"><a href="#report1" data-toggle="tab">รายงาน</a></li>
        <li><a id="drilldownperson" report_id="<?= $report_id ?>" hsitecode="<?=$hsitecode;?>" div="#report2" sec="3" href="#report2" tabmenu="yes"
               data-toggle="tab">รายงานรายบุคคลของสถานบริการ</a></li>
    </ul>
    <div class="tab-content">
        <div id="report1" class="tab-pane fade in active">
            <h4>ก. จำนวนรวมรักษาทั้งหมด <label class="label label-primary"><?= number_format($dataget[treat]) ?></label>
                ราย</h4>
            <h4>ข. จำนวนที่รับการรักษาด้วยการผ่าตัดทั้งหมด <label
                    class="label label-primary"><?= number_format($dataget[surgery]) ?></label> ราย</h4>
            <h4 style="padding-left: 22px;">จำนวนที่รับการผ่าตัดให้หายขาด <label
                    class="label label-primary"><?= number_format($dataget[c_surgery]) ?></label> ราย</h4>
            <h4 style="padding-left: 22px;">จำนวนที่รับการผ่าตัดเพื่อการประคับประคอง <label
                    class="label label-primary"><?= number_format($dataget[p_surgery]) ?></label> ราย</h4>
            <h4>ค. จำแนกตามหน่วยบริการที่ส่งตรวจ (Hospital-based) ดังนี้</h4><br>

            <?php
            foreach ($province as $value) {
                $province_name = $value[province];
                $provincecode = $value[provincecode];
                $amphur = DrillDownSec3Controller::GetAmphur($hsitecode, $report_id, $provincecode);
                ?>
                <h4>จังหวัด <?= $province_name ?></h4>
                <div style="overflow-x:auto;">
                    <table>
                        <thead>
                        <tr>
                            <th style="min-width: 200px;">
                            </th>
                            <th style="min-width: 60px; text-align: right;">
                                รักษา
                            </th>
                            <th style="min-width: 80px; text-align: right;">รวมผ่าตัด</th>
                            <th style="min-width: 140px; text-align: right;">ผ่าตัด ให้หายขาด</th>
                            <th style="min-width: 190px; text-align: right;">ผ่าตัด เพื่อประคับประคอง</th>
                            <th style="min-width: 130px; text-align: right;">Liver resection</th>
                            <th style="min-width: 110px; text-align: right;">Liver + Hilar</th>
                            <th style="min-width: 150px; text-align: right;">Whipples opeation</th>
                            <th style="min-width: 130px; text-align: right;">Hilar resection</th>
                            <th style="min-width: 70px; text-align: right;">Bypass</th>
                            <th style="min-width: 130px; text-align: right;">Biopsy(EL+Bx)</th>
                            <th style="min-width: 130px; text-align: right;">Needle Biopsy</th>
                            <th style="min-width: 70px; text-align: right;">PTBD</th>
                            <th style="min-width: 160px; text-align: right;">Endoscopic Stent</th>
                            <th style="min-width: 160px; text-align: right;">Chemotheraphy(All)</th>
                            <th style="min-width: 220px; text-align: right;">Chemotheraphy(Adjuvant)</th>
                            <th style="min-width: 100px; text-align: right;">Medication</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        foreach ($amphur as $value2) {
                            $amphur_name = $value2[amphur];
                            $amphurcode = $value2[amphurcode];
                            $hospital = DrillDownSec3Controller::GetHospital($hsitecode, $report_id, $provincecode, $amphurcode);
                            ?>
                            <tr>
                                <td colspan="17">
                                    <b>อำเภอ <?= $amphur_name ?></b>
                                </td>
                            </tr>
                            <?php
                            foreach ($hospital as $value3) {
                                $sitecode = $value3[sitecode];
                                $hospital_name = DrillDownSec3Controller::ShortName($value3[name]);
                                $p_surgery = ($value3[surgery] - $value3[c_surgery]);
                                ?>
                                <tr>
                                    <td style="text-align: left;">
                                        <?php
                                        if ($checkadmin == "yes") {

                                            ?>
                                            <a href="#report2" data-toggle="tab" id="drilldownperson"
                                               report_id="<?= $report_id ?>" sitecode="<?= $sitecode ?>"
                                               hsitecode="<?= $hsitecode ?>" vsite="<?= $hsitecode ?>"
                                               title="<?= $hospital_name ?>" 
                                               sec="3"
                                               div="#report2"><?= $sitecode . ": " . $hospital_name ?></a>
                                            <?php
                                        } else {
                                            echo $sitecode . ": " . $hospital_name;
                                        }
                                        ?>
                                    </td>
                                    <td style="text-align: right;">
                                        <?= number_format($value3[treat],0,'.',','); ?>
                                    </td>
                                    <td style="text-align: right;">
                                        <?= number_format($value3[surgery],0,'.',','); ?>
                                    </td>
                                    <td style="text-align: right;">
                                        <?= number_format($value3[c_surgery],0,'.',','); ?>
                                    </td>
                                    <td style="text-align: right;">
                                        <?= number_format($p_surgery,0,'.',','); ?>
                                    </td>
                                    <td style="text-align: right;">
                                        <?= number_format($value3[livrec],0,'.',','); ?>
                                    </td>
                                    <td style="text-align: right;">
                                        <?= number_format($value3[livhilar],0,'.',','); ?>
                                    </td>
                                    <td style="text-align: right;">
                                        <?= number_format($value3[whipple],0,'.',','); ?>
                                    </td>
                                    <td style="text-align: right;">
                                        <?= number_format($value3[hilar],0,'.',','); ?>
                                    </td>
                                    <td style="text-align: right;">
                                        <?= number_format($value3[bypass],0,'.',','); ?>
                                    </td>
                                    <td style="text-align: right;">
                                        <?= number_format($value3[biopsy],0,'.',','); ?>
                                    </td>
                                    <td style="text-align: right;">
                                        <?= number_format($value3[needle_biopsy],0,'.',','); ?>
                                    </td>
                                    <td style="text-align: right;">
                                        <?= number_format($value3[ptbd],0,'.',','); ?>
                                    </td>
                                    <td style="text-align: right;">
                                        <?= number_format($value3[endoscopic],0,'.',','); ?>
                                    </td>
                                    <td style="text-align: right;">
                                        <?= number_format($value3[chemo_all],0,'.',','); ?>
                                    </td>
                                    <td style="text-align: right;">
                                        <?= number_format($value3[chemo_adjuvant],0,'.',','); ?>
                                    </td>
                                    <td style="text-align: right;">
                                        <?= number_format($value3[medication],0,'.',','); ?>
                                    </td>
                                </tr>
                                <?php
                            }
                        }
                        ?>
                        </tbody>
                    </table>
                    </br>
                    </br>
                </div>
                <?php
            }
            ?>

        </div>
        <div id="report2" class="tab-pane fade">


        </div>
    </div>
</div>
