<?php

namespace frontend\modules\project84\controllers;
use Yii;
use yii\web\JsExpression;

class GraphSummaryController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $date = date('Y-m-d');
         $last = date('Y-m-d H:i:s');



    $check = Yii::$app->db->createCommand("SELECT datecout FROM  summary_count ORDER BY datecout DESC limit 1")->queryAll();

    
    if ($date==date($check[0]['datecout'])){
        $reload=$_GET['reload'];
        if($reload==true){
          $va=$this->Getvalues();
          $query = Yii::$app->db->createCommand("UPDATE summary_count SET  lastupdate= '$last',num_ptid_tb1 = ".$va['data2'].", num_ptid_tb2 = ".$va['data3'].", num_ptid_ov = ".$va['data4'].", num_id_ov = ".$va['data5'].", "
                . " num_ptid_tb3 = ".$va['data6'].", num_id_tb3 = ".$va['data7'].", num_ptid_tb4 = ".$va['data8'].", num_id_tb4 = ".$va['data9'].", num_ptid_tb8 = ".$va['data10'].", num_id_tb8 = ".$va['data11'].","
                . " num_ptid_tb7 = ".$va['data12'].", num_com_tb7 = ".$va['data13'].", num_sup_tb7 = ".$va['data14'].", num_other_tb7 = ".$va['data15'].", num_buffe = ".$va['data1']." ,num_sus = ".$va['sus']." ,num_cf = ".$va['cf']." "
                . "WHERE datecout='$date'")->execute();
          }
      }else if ($date>date($check[0]['datecout'])) {
             $va=$this->Getvalues();
            $sql = "INSERT INTO summary_count (datecout,lastupdate,num_ptid_tb1, num_ptid_tb2, num_ptid_ov, num_id_ov, num_ptid_tb3, num_id_tb3, num_ptid_tb4, num_id_tb4, num_ptid_tb8, num_id_tb8, num_ptid_tb7, num_com_tb7, num_sup_tb7, num_other_tb7, num_buffe, num_sus, num_cf) "
                    . "VALUES ( '$date', '$last', ".$va['data2'].",  ".$va['data3'].", ".$va['data4'].", ".$va['data5'].",".$va['data6'].",  ".$va['data7'].", ".$va['data8'].", ".$va['data9'].", ".$va['data10'].",  ".$va['data11'].",".$va['data12'].", ".$va['data13'].", ".$va['data14'].","
                    . " ".$va['data15'].",  ".$va['data1']." , ".$va['sus']." , ".$va['cf'].")";
                    Yii::$app->db->createCommand($sql)->execute();  
          }


         $sum = Yii::$app->db->createCommand("SELECT lastupdate as lastupdate,num_ptid_tb1 as data2, num_ptid_tb2 as data3, num_ptid_ov as data4, num_id_ov as data5, "
                . " num_ptid_tb3 as data6, num_id_tb3 as data7, num_ptid_tb4 as data8, num_id_tb4 as data9, num_ptid_tb8 as data10, num_id_tb8 as data11,"
                . " num_ptid_tb7 as data12, num_com_tb7 as data13, num_sup_tb7 as data14, num_other_tb7 as data15, num_buffe as data1 ,num_sus as suspect ,num_cf as confirm FROM summary_count ORDER BY datecout DESC limit 1")->queryOne();
        

          
        return $this->renderAjax('index',[
            'data2' => $sum['data2'],
            'data3' => $sum['data3'],
            'data4' => $sum['data4'],
            'data5' => $sum['data5'],
            'data6' => $sum['data6'],
            'data7' => $sum['data7'],
            'data8' => $sum['data8'],
            'data9' => $sum['data9'],
            'data10'=> $sum['data10'],
            'data11'=> $sum['data11'],
            'data12'=> $sum['data12'],
            'data13'=> $sum['data13'],
            'data14'=> $sum['data14'],
            'data15'=> $sum['data15'],
            'data1' => $sum['data1'],
            'suspect'=> $sum['suspect'],
            'confirm'=> $sum['confirm'],
            'lastup' => $sum['lastupdate']
       ]);
    }

    public function actionPaint()
    {
     
         
         $date = date('Y-m-d');
         $last = date('Y-m-d H:i:s');



    $check = Yii::$app->db->createCommand("SELECT datecout FROM  summary_count ORDER BY datecout DESC limit 1")->queryAll();

    
    if ($date==date($check[0]['datecout'])){
        $reload=$_GET['reload'];
        if($reload==true){
          $va=$this->Getvalues();
          $query = Yii::$app->db->createCommand("UPDATE summary_count SET  lastupdate= '$last',num_ptid_tb1 = ".$va['data2'].", num_ptid_tb2 = ".$va['data3'].", num_ptid_ov = ".$va['data4'].", num_id_ov = ".$va['data5'].", "
                . " num_ptid_tb3 = ".$va['data6'].", num_id_tb3 = ".$va['data7'].", num_ptid_tb4 = ".$va['data8'].", num_id_tb4 = ".$va['data9'].", num_ptid_tb8 = ".$va['data10'].", num_id_tb8 = ".$va['data11'].","
                . " num_ptid_tb7 = ".$va['data12'].", num_com_tb7 = ".$va['data13'].", num_sup_tb7 = ".$va['data14'].", num_other_tb7 = ".$va['data15'].", num_buffe = ".$va['data1']." ,num_sus = ".$va['sus']." ,num_cf = ".$va['cf']." "
                . "WHERE datecout='$date'")->execute();
          }
      }else if ($date>date($check[0]['datecout'])) {
             $va=$this->Getvalues();
            $sql = "INSERT INTO summary_count (datecout,lastupdate,num_ptid_tb1, num_ptid_tb2, num_ptid_ov, num_id_ov, num_ptid_tb3, num_id_tb3, num_ptid_tb4, num_id_tb4, num_ptid_tb8, num_id_tb8, num_ptid_tb7, num_com_tb7, num_sup_tb7, num_other_tb7, num_buffe, num_sus, num_cf) "
                    . "VALUES ( '$date', '$last', ".$va['data2'].",  ".$va['data3'].", ".$va['data4'].", ".$va['data5'].",".$va['data6'].",  ".$va['data7'].", ".$va['data8'].", ".$va['data9'].", ".$va['data10'].",  ".$va['data11'].",".$va['data12'].", ".$va['data13'].", ".$va['data14'].","
                    . " ".$va['data15'].",  ".$va['data1']." , ".$va['sus']." , ".$va['cf'].")";
                    Yii::$app->db->createCommand($sql)->execute();  
          }


         $sum = Yii::$app->db->createCommand("SELECT lastupdate as lastupdate,num_ptid_tb1 as data2, num_ptid_tb2 as data3, num_ptid_ov as data4, num_id_ov as data5, "
                . " num_ptid_tb3 as data6, num_id_tb3 as data7, num_ptid_tb4 as data8, num_id_tb4 as data9, num_ptid_tb8 as data10, num_id_tb8 as data11,"
                . " num_ptid_tb7 as data12, num_com_tb7 as data13, num_sup_tb7 as data14, num_other_tb7 as data15, num_buffe as data1 ,num_sus as suspect ,num_cf as confirm FROM summary_count ORDER BY datecout DESC limit 1")->queryOne();
        

          
        return $this->renderAjax('paint',['summaryshow' => $sum,]);
    }
    private function Getvalues(){
       
        
        
          /////////2.ลงทะเบียน (ราย)///////////
         $data2 = Yii::$app->db->createCommand("SELECT COUNT(DISTINCT ptid)as num FROM tb_data_1 WHERE rstat NOT IN(0,3)")->queryOne();
         $data2 = $data2['num'];
         /////////3.ข้อมูลพื้นฐาน (ราย)///////////
         $data3=Yii::$app->db->createCommand("SELECT COUNT(DISTINCT ptid)as num FROM tb_data_2 WHERE rstat NOT IN(0,3)")->queryOne();
         $data3=$data3['num'];
         
         /////////4.ตรวจอุจจาระ/ปัสสาวะ (ราย)///////////
         $num41  = Yii::$app->db->createCommand("SELECT COUNT(DISTINCT ptid) as num21 FROM tbdata_21 WHERE rstat NOT IN(0,3)")->queryOne();
         $data41 = $num41['num21']; 
         $num42  = Yii::$app->db->createCommand("SELECT COUNT(DISTINCT ptid) as num22 FROM tbdata_22 WHERE rstat NOT IN(0,3)")->queryOne();
         $data42 = $num42['num22']; 
         $num43  = Yii::$app->db->createCommand("SELECT COUNT(DISTINCT ptid) as num23 FROM tbdata_23 WHERE rstat NOT IN(0,3)")->queryOne();
         $data43 = $num43['num23'];
         $num44  = Yii::$app->db->createCommand("SELECT COUNT(DISTINCT ptid) as num24 FROM tbdata_24 WHERE rstat NOT IN(0,3)")->queryOne();
         $data44 = $num44['num24'];
         $data4  = $data41+$data42+$data43+$data44;
         
         /////////5.ตรวจอุจจาระ/ปัสสาวะ (ครั้ง)///////////
         $num51  = Yii::$app->db->createCommand("SELECT COUNT(*) as num21 FROM tbdata_21 WHERE rstat NOT IN(0,3)")->queryOne();
         $data51 = $num51['num21']; 
         $num52  = Yii::$app->db->createCommand("SELECT COUNT(*) as num22 FROM tbdata_22 WHERE rstat NOT IN(0,3)")->queryOne();
         $data52 = $num52['num22']; 
         $num53  = Yii::$app->db->createCommand("SELECT COUNT(*) as num23 FROM tbdata_23 WHERE rstat NOT IN(0,3)")->queryOne();
         $data53 = $num53['num23'];
         $num54  = Yii::$app->db->createCommand("SELECT COUNT(*) as num24 FROM tbdata_24 WHERE rstat NOT IN(0,3)")->queryOne();
         $data54 = $num54['num24'];
         $data5  = $data51+$data52+$data53+$data54;
         
         /////////6.ตรวจคัดกรองอัลตร้าซาวด์ (ราย)///////////
         $data6 = Yii::$app->db->createCommand("SELECT COUNT(DISTINCT ptid) as num FROM tb_data_3 WHERE rstat NOT IN(0,3)")->queryOne();
         $data6=$data6['num'];
         /////////7.ตรวจคัดกรองอัลตร้าซาวด์ (ครั้ง)///////////
         $data7 = Yii::$app->db->createCommand("SELECT COUNT(*) as num FROM tb_data_3 WHERE rstat NOT IN(0,3)")->queryOne();
         $data7 = $data7['num'];
         
         //////// suspected /////////////
         $sus = Yii::$app->db->createCommand("SELECT COUNT(DISTINCT ptid) as num FROM tb_data_3 WHERE f2v6a3 = '1' AND f2v6a3b1 = '1' AND rstat NOT IN(0,3)")->queryOne();
         $sus = $sus['num'];
         
         /////////8.ตรวจ CT,MRI,MRCP (ราย)///////////
         $data8 = Yii::$app->db->createCommand("SELECT COUNT(DISTINCT ptid) as num FROM tb_data_4 WHERE rstat NOT IN(0,3)")->queryOne();
         $data8 = $data8['num'];
         /////////9.ตรวจ CT,MRI,MRCP (ครั้ง)///////////
         $data9 = Yii::$app->db->createCommand("SELECT COUNT(*) as num FROM tb_data_4 WHERE rstat NOT IN(0,3)")->queryOne();
         $data9 = $data9['num'];
         
         //////// confirmed /////////////
         $cf = Yii::$app->db->createCommand("SELECT COUNT(DISTINCT ptid) as num FROM tb_data_4 WHERE f2p1v3 IN ('1','2','3') AND rstat NOT IN(0,3)")->queryOne();
         $cf = $cf['num'];
         
          /////////10.ตรวจยืนยันทางพยาธิวิทยา (ราย)///////////
         $data10 = Yii::$app->db->createCommand("SELECT COUNT(DISTINCT ptid) as num FROM tb_data_8 WHERE rstat NOT IN(0,3)")->queryOne();
         $data10 = $data10['num'];
         /////////11.ตรวจยืนยันทางพยาธิวิทยา (ครั้ง)///////////
         $data11 = Yii::$app->db->createCommand("SELECT COUNT(*) as num FROM tb_data_8 WHERE rstat NOT IN(0,3)")->queryOne();
         $data11 = $data11['num'];
         
          /////////12.รับการรักษา(ราย)///////////
         $data12 = Yii::$app->db->createCommand("SELECT COUNT(DISTINCT ptid) as num FROM tb_data_7 WHERE rstat NOT IN(0,3)")->queryOne();
         $data12 = $data12['num'];
         //////////13.ผ่าตัดเพื่อหายขาด 14.ผ่าตัดประคับประคอง 15. อื่นๆ //////////
         $data13 = Yii::$app->db->createCommand("SELECT COUNT(DISTINCT ptid) as num FROM tb_data_7 WHERE f3v5a1b1 ='1' OR f3v5a1b2 = '1' OR f3v5a1b6 = '1' AND rstat NOT IN(0,3)")->queryOne();
         $data13 = $data13['num'];
         $data14 = Yii::$app->db->createCommand("SELECT COUNT(DISTINCT ptid) as num FROM tb_data_7 WHERE f3v5a1b3 ='1' OR f3v5a1b4 = '1' AND rstat NOT IN(0,3)")->queryOne();
         $data14 = $data14['num'];
         $data15 = $data12-($data13+$data14);
         
         /////////1.ประชากร//////////////
         $data1 = Yii::$app->db->createCommand("SELECT SUM(row) as num FROM buffe_tdc_count WHERE table_name ='f_person'")->queryOne();
         $data1 = $data1['num'];
         
         $date = date('Y-m-d');
         $last = date('Y-m-d H:i:s');
         
        $array=[];
        $array['data2'] = $data2;
        $array['data3'] = $data3;
        $array['data4'] = $data4;
        $array['data5'] = $data5;
        $array['data6'] = $data6;
        $array['data7'] = $data7;
        $array['data8'] = $data8;
        $array['data9'] = $data9;
        $array['data10'] = $data10;
        $array['data11'] = $data11;
        $array['data12'] = $data12;
        $array['data13'] = $data13;
        $array['data14'] = $data14;
        $array['data15'] = $data15;
        $array['data1'] = $data1;
        $array['sus'] = $sus;
        $array['cf'] = $cf;
        return $array;
        
    }
}