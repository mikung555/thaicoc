<?php

namespace frontend\modules\project84\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ForumTypeController implements the CRUD actions for ForumType model.
 */
class ReportSec3Controller extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all ForumType models.
     * @return mixed
     */
    public function GetDataTb3($fromDate=null,$toDate=null,$colum,$sort)
    {
        self::CreateTableReportId();
        $report_id=self::CheckControlTb3($fromDate,$toDate,"no");
        $datareport=self::GetDataTb3ByReportId($report_id,$colum,$sort);
        $datasum=self::GetDataSumTb3ByReportId($report_id);
        $data[datareport]=$datareport;
        $data[report_id]=$report_id;
        if($report_id==1){
            $data[datasumall]=$datasum;
        }
        else{
            $data[datasumall]=self::GetDataSumTb3ByReportId(1);
        }
        $data[datasum]=$datasum;
        return $data;
    }
    public function CreateTableReportId()
    {
        $sqlAddTable = 'CREATE TABLE IF NOT EXISTS project84_report_control_tb3(
            report_id INT(5) NOT NULL AUTO_INCREMENT,
            fromDate  DATE DEFAULT NULL,
            toDate  DATE DEFAULT NULL,
            lastcal datetime DEFAULT NULL,
            PRIMARY KEY (report_id)
        );';
        \Yii::$app->db->createCommand($sqlAddTable)->execute();
    }//public function CreateTableReportId

    public function CheckControlTb3($fromDate=null,$toDate=null,$refresh=null)
    {
        self::CreateTableReportId();
        
        // setup default value
        if( $fromDate == null ){
            $fromDate = '2016-10-01';
        }
        if( $toDate == null ){
            $toDate = date('Y-m-d');
        }
        
        // get report id OR setup report ID
        if( $refresh=="yes" ){
            if( $fromDate == '2016-10-01' && $toDate == date('Y-m-d') ){
                $report_id = 1;
                $fromDate = '2016-10-01';
                $toDate = date('Y-m-d');
            }else{
                // get new report id
                $report_id = self::sqlReportGetReportID($fromDate, $toDate );
            }
        }else{
            //
        }
        
        if( 1 ){
            $sql = "insert into project84_check_y60 ";
            $sql.= "set `cgroup`=\"sec\" ";
            $sql.= ",`ccommand`=\"CheckControlTb3 => New record_id: ".$report_id."; fromDate: ".$fromDate."; toDate: ".$toDate."; Refresh: ".$refresh."\" ";
            $sql.= ",dadd=NOW() ";
            Yii::$app->db->createCommand($sql)->execute();
        }
        
        if ($refresh=="yes") {
            $sqlAction="update";
            //$report_id=1;
            if ($fromDate == null){
                $fromDate='2016-10-01';
            }
            if ($fromDate == null){
                $toDate=date('Y-m-d');
            }
            $lastcal=date('Y-m-d G:i:s');
            self::sqlReportControl($sqlAction,$report_id,$fromDate,$toDate,$lastcal);
        }else{
            $dayNow=date('Y-m-d');
            if($fromDate=='2016-10-01' && $toDate==$dayNow){
                $sqlCeckReportControlLast = "SELECT LEFT(lastcal,10) AS daylast FROM project84_report_control_tb3 WHERE report_id='1'";
                $dataCeckReportControlLast = Yii::$app->db->createCommand($sqlCeckReportControlLast)->queryAll();
                $datacount=count($dataCeckReportControlLast);
                if($datacount>0){
                    $dayLast=$dataCeckReportControlLast[0][daylast];
                    if($dayLast==$dayNow){
                        $report_id=1;
                    }else{
                        $sqlAction="update";
                        $report_id=1;
                        $fromDate='2016-10-01';
                        $toDate=date('Y-m-d');
                        $lastcal=date('Y-m-d G:i:s');
                        self::sqlReportControl($sqlAction,$report_id,$fromDate,$toDate,$lastcal);
                    } //if($dayLast==$dayNow)
                }else{
                    $sqlAction="insert";
                    $report_id=1;
                    $fromDate='2016-10-01';
                    $toDate=date('Y-m-d');
                    $lastcal=date('Y-m-d G:i:s');
                    self::sqlReportControl($sqlAction,$report_id,$fromDate,$toDate,$lastcal);
                }//if($datacount==1)
            }else{
                $sqlCeckReportControlfrist = "SELECT * FROM project84_report_control_tb3 WHERE fromDate='$fromDate' and toDate='$toDate'";
                $dataCeckReportControlfrist = Yii::$app->db->createCommand($sqlCeckReportControlfrist)->queryAll();
                $datacountfrist=count($dataCeckReportControlfrist);
                if($datacountfrist > 0){
                    $report_id=$dataCeckReportControlfrist[0][report_id];
                }else{
                    $sqlCeckReportControlLast = "SELECT * FROM project84_report_control_tb3";
                    $dataCeckReportControlLast = Yii::$app->db->createCommand($sqlCeckReportControlLast)->queryAll();
                    $datacount=count($dataCeckReportControlLast);
                    $lastcal=date('Y-m-d G:i:s');
                    $report_id=$datacount+1;
                    $sqlAction="insert";
                    self::sqlReportControl($sqlAction,$report_id,$fromDate,$toDate,$lastcal);
                }//if($datacountfrist>0)
            }//if($fromDate==null && $toDate==null)
        }
        return $report_id;
    }//public function CheckControlTb3($fromDate=null,$toDate=null)

    public function sqlReportControl($sqlAction=null,$report_id=null,$fromDate=null,$toDate=null,$lastcal=null)
    {
        switch ($sqlAction) {
            case 'insert':
                $sqlInsertReportControl = "INSERT INTO project84_report_control_tb3 (`report_id`, `fromDate`, `toDate`,`lastcal`) VALUES (NULL, '$fromDate', '$toDate', '$lastcal')";
                \Yii::$app->db->createCommand($sqlInsertReportControl)->execute();
                self::CallProcedure($report_id,$fromDate,$toDate);
                break;
            case 'update':
                $sqlUpdateReportControl = "UPDATE project84_report_control_tb3 SET fromDate ='$fromDate',toDate ='$toDate',lastcal='$lastcal'  WHERE report_id='$report_id'";
                \Yii::$app->db->createCommand($sqlUpdateReportControl)->execute();
                self::CallProcedure($report_id,$fromDate,$toDate);
                break;
        }//switch
    }//sqlReportControl
    
    public function CallProcedure($report_id=null,$fromDate=null,$toDate=null)
    {
        #$sqlProcedure = "CALL project84_tb3('$fromDate','$toDate','$report_id')";
        $sqlProcedure = "CALL project84_y60('$fromDate','$toDate','$report_id', 'all')";
        if( 1 ){
            // save debug to log
            $sql = "insert into project84_check_y60 ";
            $sql.= "set `cgroup`=\"sec\" ";
            $sql.= ",`ccommand`=\"CallProcedure => fromDate: ".$fromDate."; toDate: ".$toDate."; report_id: ".$report_id."; SQL: ".addslashes($sqlProcedure)."\" ";
            $sql.= ",dadd=NOW() ";
            Yii::$app->db->createCommand($sql)->execute();
        }
        
        if( 1 ){
            Yii::$app->db->createCommand($sqlProcedure)->execute();
        }
        
        # check update record tccbot
        $sqlRecTCC = 'select sum(tccbot) as rectcc ';
        $sqlRecTCC.= 'from project84_report_sec1 ';
        $sqlRecTCC.= 'where report_id=:report_id ';
        $providerRecTCC = Yii::$app->db->createCommand($sqlRecTCC,[':report_id'=>$report_id])->queryOne();
        $tccFroceUpdate = true;
        if($providerRecTCC['rectcc']>0){
            $tccFroceUpdate = false;
        }
        
        # 2017-03-13 update pop from TDC 
        $tccFroceUpdate = false; //  ไม่ต้องทำ อยู่ในส่วน Procedure แล้ว
        if( $tccFroceUpdate ){
            $sqlHCODE = "select address,hcode ";
            $sqlHCODE.= "from project84_zone_60_hcode ";
            $dbSetHCODEAddress = Yii::$app->db->createCommand($sqlHCODE)->queryAll();
            foreach($dbSetHCODEAddress as $key => $row){
                $hcodeAddress[$row['hcode']]=$row['address'];
            }

            $sqlTDC = "select * from buffe_patient_count ";
            //$sqlTDC.= "";
            $dbSetTDCAddress = Yii::$app->dbwebs1->createCommand($sqlTDC)->queryAll();
            if( count($dbSetTDCAddress)>0 ){
                foreach($dbSetTDCAddress as $key => $row){
                    if( strlen($hcodeAddress[$row['sitecode']])>0 ){
                        $sqlTDCUpdate = "update project84_report_sec1 ";
                        $sqlTDCUpdate.= 'SET tccbot = '.$row['npatient'].' ';
                        $sqlTDCUpdate.= 'where address="'.$hcodeAddress[$row['sitecode']].'" ';
                        Yii::$app->db->createCommand($sqlTDCUpdate)->execute();
                    }
                }
            }
        }
        
        if( 0 ){
            // ยกเลิกตัวเดิม 2017-03-13
            $queryTccAddress = Yii::$app->db->createCommand('SELECT address FROM project84_zone')->queryAll();
            foreach($queryTccAddress as $key => $row){
                $sqlTCC = 'SELECT COUNT(*) AS ncount ';
                $sqlTCC.= 'FROM person ';
                $sqlTCC.= 'WHERE sitecode IN (SELECT hcode FROM buffe_webservice.all_hospital_thai WHERE addresscode="'.$row['address'].'") ';
                $tccbotRow =  Yii::$app->dbbot->createCommand($sqlTCC)->queryOne();
                $queryTccAddress[$key]['tccbot'] = 0;
                $sqlTCC = 'UPDATE project84_report_sec1 ';
                $sqlTCC.= 'SET tccbot = '.$tccbotRow['ncount'].' ';
                $sqlTCC.= 'WHERE report_id='.$report_id.' AND address="'.$row['address'].'" ';
                Yii::$app->db->createCommand($sqlTCC)->execute();
            }
        }
    }//public function CallProcedure
    
    public function sqlReportGetReportID($fromDate, $toDate ){
        if( $fromDate == null ){
            $fromDate = '2016-10-01';
        }
        if( $toDate == null ){
            $toDate = date('Y-m-d');
        }
        $sql="SELECT * FROM project84_report_control_tb3 WHERE fromDate='$fromDate' and toDate='$toDate' ";
        //echo $sql;
        $data = Yii::$app->db->createCommand($sql)->queryAll();
        if ( count($data) > 0) {
            $report_id = $data[0][report_id];
        }else{
            // insert new report control
            $sqlInsert = "INSERT INTO project84_report_control_tb3 (`report_id`, `fromDate`, `toDate`,`lastcal`) ";
            $sqlInsert.= "VALUES (NULL, '$fromDate', '$toDate', '".date('Y-m-d G:i:s')."')";
            Yii::$app->db->createCommand($sqlInsert)->execute();
            //
            $sql="SELECT * FROM project84_report_control_tb3 WHERE fromDate='$fromDate' and toDate='$toDate' ";
            $data = Yii::$app->db->createCommand($sql)->queryAll();
            if ( count($data) > 0) {
                $report_id = $data[0][report_id];
            }
        }
        return $report_id;
    }

    public function GetDataTb3ByReportId($report_id,$colum,$sort)
    {
        $sqlControl = "SELECT * FROM  project84_zone_p3 a LEFT JOIN project84_report_sec3 b on a.sitecode=b.sitecode WHERE report_id='$report_id' GROUP BY a.sitecode ORDER BY $colum $sort";
        $dataProvider = Yii::$app->db->createCommand($sqlControl)->queryAll();
        return $dataProvider;
    }//public function GetDataTb3ByReportId($report_id)

    public function GetDataSumTb3ByReportId($report_id)
    {
        $sql = "SELECT sum(resgis),sum(treat),sum(treatccapos),sum(surgery),sum(c_surgery)";
        $sql.= ",sum(livrec),sum(livhilar),sum(hilar),sum(bypass),sum(biopsy)";
        $sql.= ",sum(needle_biopsy),sum(whipple),sum(chemo_all)";
        $sql.= ",sum(chemo_adjuvant),sum(ptbd),sum(endoscopic),sum(medication)  ";
        $sql.= "FROM  project84_report_sec3  where report_id='$report_id'";
        
        $dataProvider = Yii::$app->db->createCommand($sql)->queryAll();
        return $dataProvider;
    }//public function GetDataSumTb3ByReportId($report_id)
}//class
