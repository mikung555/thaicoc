<?php

namespace frontend\modules\project84\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\VarDumper;

use frontend\modules\project84\classes\Sec1Query;

/**
 * ForumTypeController implements the CRUD actions for ForumType model.
 */
class DrillDownSec1Controller extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all ForumType models.
     * @return mixed
     */
    public function actionAjaxData($tamboncode = null, $report_id = null)
    {
        $request = Yii::$app->getRequest();
        if( 0 ){
            echo "<br />";
            echo "<br />";
            echo "<br />";
            echo "<br />_GET";
            echo "<br />col: ".$request->get('col');
            
        }
        $render = "drilldown_sec1";
        $checkadmin = self::CheckAdmin();
        $dataArrayDrillSec1 = self::Getdata($tamboncode, $report_id);
        $datakpos = self::Getdataov($tamboncode, $report_id, "kpos");
        $datappos = self::Getdataov($tamboncode, $report_id, "ppos");
        $datafpos = self::Getdataov($tamboncode, $report_id, "fpos");
        $dataupos = self::Getdataov($tamboncode, $report_id, "upos");
        $url = $this->renderAjax($render, [
            'tamboncode' => $tamboncode,
            'report_id' => $report_id,
            'checkadmin' => $checkadmin,
            'dataArrayDrillSec1' => $dataArrayDrillSec1,
            'datakpos' => $datakpos,
            'datappos' => $datappos,
            'datafpos' => $datafpos,
            'dataupos' => $dataupos,
        ]);
        return $url;
    }


    public function actionAjaxDataPerson($report_id = null, $tabmenu = null, $tamboncode = null)
    {
        //echo $tamboncode;
        $request = Yii::$app->getRequest();
        
        
        if( strlen($request->get('col'))>0 ){
            // กรณีที่มีการเลือกตัวแปร เพื่อแสดง List data
            $sitecode = Yii::$app->user->identity->userProfile->sitecode;
            if( 0 ){
                echo "<br />col: ".$request->get('col');
                echo "<br />_GET[hsitecode]:".$request->get('hsitecode');
                echo "<br />sitecode: ".$sitecode;
                echo "<br />tamboncode:".$tamboncode;
                echo "<br />report_id: ".$report_id;
            }
            $render = "drilldown_person_sec11";
            $dataProvider = Sec1Query::getData($report_id, $tamboncode, $sitecode, $request->get('col'));
            $hosname = '';
            $status = '';
            $url = $this->renderAjax($render, [
                'dataProvider' => $dataProvider,
                'hosname' => $hosname,
                'status' => $status,
            ]);
        }else if ($tabmenu == null) {
            $dataProvider = self::GetProvince($report_id, $tamboncode);
            $render = "drilldown_person_sec1";
            $url = $this->renderAjax($render, [
                'dataProvider' => $dataProvider,
                'report_id' => $report_id,
            ]);
        } else {
            
            $sitecode = Yii::$app->user->identity->userProfile->sitecode;
            
            $hosname = self::GetHospitalName($sitecode);
            $dataProvider = self::Getperson($report_id,$tamboncode);
            $countdata = count($dataProvider);
            if ($countdata > 0) {
                $status = "yes";
            } else {
                $status = "no";
            }
            $render = "drilldown_person_sec11";
            $url = $this->renderAjax($render, [
                'dataProvider' => $dataProvider,
                'hosname' => $hosname,
                'status' => $status,
            ]);
        }


        return $url;

    }

    public function GetHospitalName($sitecode)
    {
        $sql = "SELECT name,amphur,province FROM  `all_hospital_thai` WHERE hcode='$sitecode'";
        $dataProvider = Yii::$app->db->createCommand($sql)->queryAll();
        return $dataProvider;
    }

    public function Getdata($tamboncode, $report_id)
    {
        $sql = "SELECT * FROM  project84_report_sec1  where report_id='$report_id' and address='$tamboncode'";
        $dataProvider = Yii::$app->db->createCommand($sql)->queryAll();
        return $dataProvider;
    }

    public function Getdataov($tamboncode, $report_id, $fill)
    {
        $sql = "SELECT sum(ov) as ov,sum(mif) as mif,sum(ss) as ss,sum(ech) as ech,sum(taenia) as taenia,sum(tt) as tt,sum(other) as other FROM  project84_tb1_data  where report_id='$report_id' and tamboncode='$tamboncode' and $fill='1'";
        $dataProvider = Yii::$app->db->createCommand($sql)->queryAll();
        return $dataProvider;
    }


    public function GetProvince($report_id, $tamboncode)
    {
        $sql = "SELECT province,provincecode FROM `project84_tb1_data` a INNER JOIN `all_hospital_thai` b ON a.sitecode=b.hcode WHERE  report_id='$report_id' and a.tamboncode='$tamboncode' and treatov >0    GROUP BY province ORDER BY province ASC";
        $dataProvider = Yii::$app->db->createCommand($sql)->queryAll();
        return $dataProvider;
    }

    public function GetAmphur($report_id, $provincecode)
    {
        $sql = "SELECT province,provincecode,amphurcode,amphur FROM `project84_tb1_data` a INNER JOIN `all_hospital_thai` b ON a.sitecode=b.hcode WHERE  report_id='$report_id'  and provincecode='$provincecode' and treatov >0  GROUP BY amphur ORDER BY amphur ASC";
        $dataProvider = Yii::$app->db->createCommand($sql)->queryAll();
        return $dataProvider;
    }

    public function GetTumbon($report_id, $provincecode, $amphurcode)
    {
        $sql = "SELECT tambon,amphurcode,amphur,province,provincecode,a.tamboncode,COUNT(ptid) as total
FROM `project84_tb1_data` a
INNER JOIN `all_hospital_thai` b
ON a.sitecode=b.hcode
WHERE
report_id='$report_id'  and amphurcode='$amphurcode' and provincecode='$provincecode' and treatov >0
GROUP BY tambon ORDER BY tambon ASC";
        
        $dataProvider = Yii::$app->db->createCommand($sql)->queryAll();
        return $dataProvider;
    }

    public function GetSite($report_id, $tamboncode)
    {
        $sql = "SELECT sitecode,name,COUNT(ptid) as total ";
        $sql.= "FROM `project84_tb1_data` a ";
        $sql.= "INNER JOIN `all_hospital_thai` b ";
        $sql.= "ON a.sitecode=b.hcode ";
        $sql.= "WHERE ";
        $sql.= "report_id='$report_id'  and a.tamboncode='$tamboncode' AND treatov >0 ";
        $sql.= "GROUP BY name ORDER BY name ASC ";
        $dataProvider = Yii::$app->db->createCommand($sql)->queryAll();
        return $dataProvider;
    }

    public function CheckAdmin()
    {
        $user_id = Yii::$app->user->identity->userProfile->user_id;
        $sqlControl = "SELECT user_id FROM `rbac_auth_assignment` ";
        $sqlControl.= "WHERE `item_name` LIKE '%administrator%' ";
        $sqlControl.= "AND user_id='$user_id' ";
        $dataProvider = Yii::$app->db->createCommand($sqlControl)->queryAll();
        $countadmin = count($dataProvider);
        if ($countadmin > 0) {
            $admin = "yes";
        } else {
            $admin = "no";
        }
        return $admin;
    }

    public function Getperson($report_id, $tamboncode)
    {
        $sitecode = Yii::$app->user->identity->userProfile->sitecode;
        $userid = Yii::$app->user->identity->id;
        $sql = "SELECT ";
        $sql.= "id,ptid,title,fname,surname,hsitecode,sitecode,`name` as hospitalname,report_id,kpos, ";
        $sql.= "ppos,fpos,upos,ov,mif,ss,ech,taenia,tt,other ";
        $sql.= "FROM ";
        $sql.= "( ";
        $sql.= "SELECT DISTINCT b.id, (a.ptid),title,`name` as fname,surname,a.sitecode, ";
        $sql.= "a.hsitecode,report_id,kpos,ppos,fpos,upos,ov,mif, ";
        $sql.= "ss,ech,taenia,tt,other ";
        $sql.= "FROM `project84_tb1_data` a ";
        $sql.= "INNER JOIN `tb_data_1` b ON a.ptid=b.ptid and a.hsitecode=b.hsitecode ";
        $sql.= "WHERE  report_id='$report_id' and a.tamboncode='$tamboncode' ";
        if( $userid=='1435745159010043375' ){
            //
        }else{
            //$sql.= "and a.sitecode='$sitecode'  AND treatov > '0' ORDER BY ov DESC limit 1000 ";
        }
        $sql.= "and a.sitecode='$sitecode'  AND treatov > '0' ORDER BY ov DESC  ";
        $sql.= ")as a ";
        $sql.= "INNER JOIN `all_hospital_thai` b ON a.hsitecode=b.hcode ";
        //echo $sql;
        $dataProvider = Yii::$app->db->createCommand($sql)->queryAll();
        return $dataProvider;
    }

}//class
