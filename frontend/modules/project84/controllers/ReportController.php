<?php

namespace frontend\modules\project84\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Session;
//use frontend\modules\project84\controllers\ReportSec3Controller;

/**
 * ForumTypeController implements the CRUD actions for ForumType model.
 */
class ReportController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all ForumType models.
     * @return mixed
     */
    public function actionIndex($fromDate = null, $toDate = null)
    {
        $dataDateTime = self::GetLastReport();
        $_webcounter = self::ReportCounter();
        $render = "index";
        $url = $this->render($render, [
            'dataDateTime' => $dataDateTime,
            '_webcounter' => $_webcounter,
        ]);
        return $url;
    }//public function actionIndex()


    public function actionGetDataSort($fromDate = null, $toDate = null, $colum = null, $sort = null, $sec = null, $div = null, $refresh = null)
    {
        if ($fromDate == null){
            $fromDate = '2016-10-01';
        }
        if( $toDate == null) {
            $toDate = date('Y-m-d');
        }
        $sql = "insert into project84_check_y60 ";
        $sql.= "set `cgroup`=\"sec\" ";
        $sql.= ",`ccommand`=\"SEC: ".$sec."; fromDate: ".$fromDate."; toDate: ".$toDate."; Refresh: ".$refresh."\" ";
        $sql.= ",dadd=NOW() ";
        Yii::$app->db->createCommand($sql)->execute();
        
        $report_id = ReportSec3Controller::CheckControlTb3($fromDate, $toDate, $refresh);
        //echo "<br />report_id: ".$report_id;
        switch ($sec) {
            case '1':
                $render = "report_sec1";
                $dataArraySec1 = ReportSec1Controller::GetDataTb1($report_id, $colum, $sort);
                break;
            case '2':
                $sitemanager=Yii::$app->user->can('sitemanager'); 
                $sitecode = Yii::$app->user->identity->userProfile->sitecode;
                $admin=Yii::$app->user->can('administrator');
                $sql="SELECT provincecode FROM all_hospital_thai where hcode=:sitecode";
                $obj= Yii::$app->db->createCommand($sql,[
                    ':sitecode'=>$sitecode
                ])->queryOne();
                $session = new Session;
                $session->open();
                unset($session['ss_key']);
                $session->close();
                
                $render = "report_sec2";
                $dataArraySec2 = ReportSec2Controller::GetDataTb2($report_id, $colum, $sort);
                break;
            case '3':
                $render = "report_sec3";
                $dataArraySec3 = ReportSec3Controller::GetDataTb3($fromDate, $toDate, $colum, $sort);
                break;
        }
        $dataProviderSec3 = $dataArraySec3[datareport];
        $dataReportSumSec3 = $dataArraySec3[datasum];
        $dataReportSumAllSec3 = $dataArraySec3[datasumall];
        $dataProviderSec2 = $dataArraySec2[datareport];
        $dataReportSumSec2 = $dataArraySec2[datasum];
        $dataReportSumAllSec2 = $dataArraySec2[datasumall];
        $dataProviderSec1 = $dataArraySec1[datareport];
        $dataReportSumSec1 = $dataArraySec1[datasum];
        $dataReportSumAllSec1 = $dataArraySec1[datasumall];
        $datadate[fromDate] = $fromDate;
        $datadate[toDate] = $toDate;
        $url = $this->renderAjax($render, [
            'dataProviderSec1' => $dataProviderSec1,
            'dataReportSumSec1' => $dataReportSumSec1,
            'dataReportSumAllSec1' => $dataReportSumAllSec1,
            'dataProviderSec2' => $dataProviderSec2,
            'dataReportSumSec2' => $dataReportSumSec2,
            'dataReportSumAllSec2' => $dataReportSumAllSec2,
            'dataProviderSec3' => $dataProviderSec3,
            'dataReportSumSec3' => $dataReportSumSec3,
            'dataReportSumAllSec3' => $dataReportSumAllSec3,
            'datadate' => $datadate,
            'div' => $div,
            'sort' => $sort,
            'report_id' => $report_id,
            'sitemanager' => $sitemanager,
            'obj'=> $obj,
            'admin' => $admin,
        ]);
        return $url;
    }


    public function calpercent($value1, $value2, $str)
    {
        if ($value1 == 0 || $value2 == 0) {
            $result1 = '';
        } else {
            $result1 = number_format(($value1 / $value2) * 100, 1);
            if ($str == "yes") {
                $result = "(" . $result1 . "%)";
            } else {
                $result = $result1;
            }
        }

        return $result;
    }

    public function GetLastReport()
    {
        $sql = "SELECT LEFT(lastcal,10) AS daylast,RIGHT(lastcal,8) AS timelast FROM project84_report_control_tb3 WHERE report_id='1'";
        $dataProvider = Yii::$app->db->createCommand($sql)->queryAll();
        return $dataProvider;
    }

    public function SetNum($value, $type = null)
    {
        if ($type == null) {
            $data = number_format($value);
        } else {
            $data = number_format($value, $type);
        }
        return $data;
    }
    
    public function ReportCounter(){
        //*** Select วันที่ในตาราง Counter ว่าปัจจุบันเก็บของวันที่เท่าไหร่  ***//
	//*** ถ้าเป็นของเมื่อวานให้ทำการ Update Counter ไปยังตาราง daily และลบข้อมูล เพื่อเก็บของวันปัจจุบัน ***//
	$strSQL = " SELECT DATE FROM project84_counter LIMIT 0,1";
	$objResult = Yii::$app->db->createCommand($strSQL)->queryAll();
	if($objResult[0]["DATE"] != date("Y-m-d"))
	{
            //*** บันทึกข้อมูลของเมื่อวานไปยังตาราง daily ***//
            $strSQL = " REPLACE INTO project84_counter_daily (DATE,NUM) SELECT '".date('Y-m-d',strtotime("-1 day"))."',COUNT(*) AS intYesterday FROM  project84_counter WHERE 1 AND DATE = '".date('Y-m-d',strtotime("-1 day"))."'";
            Yii::$app->db->createCommand($strSQL)->execute();
                

            //*** ลบข้อมูลของเมื่อวานในตาราง counter ***//
            $strSQL = " DELETE FROM project84_counter WHERE DATE != '".date("Y-m-d")."' ";
            Yii::$app->db->createCommand($strSQL)->execute();
	}

	//*** Insert Counter ปัจจุบัน ***//
        $currentIP = Yii::$app->getRequest()->getUserIP();
        $currentSessionID = Yii::$app->session->getId();
        $currentUserID = Yii::$app->user->identity->username;
        $currentUserAgent = $_SERVER[HTTP_USER_AGENT];
	$strSQL = " REPLACE INTO project84_counter (DATE,IP,getId,userId,HTTP_USER_AGENT) VALUES ('".date("Y-m-d")."','".$currentIP."','".$currentSessionID."','".$currentUserID."','".$currentUserAgent."') ";
        Yii::$app->db->createCommand($strSQL)->execute();
        
        // log database all view
        try {
            $strSQLLog = " REPLACE INTO cascap_log.log_page_view_all (id,DATE,IP,getId,userId,HTTP_USER_AGENT,dview,SERVER,dadd) VALUES (NULL,'".date("Y-m-d")."','".$currentIP."','".$currentSessionID."','".$currentUserID."','".$currentUserAgent."',NOW(),\"".  addslashes(json_encode($_SERVER))."\", NOW()) ";
            Yii::$app->db->createCommand($strSQLLog)->execute();
        } catch (Exception $e) {
            //
            echo 'มีบางอย่างไม่สมบูรณ์ ',  $e->getMessage(), "\n";
        }
        
        // ******************* log_for_project84-60_view ********* //
        $strSQL = " INSERT INTO cascap_log.project84_counter (DATE,IP,getId,userId,HTTP_USER_AGENT";
        $strSQL.= ",dview,SERVER";
        $strSQL.= ") VALUES (";
        $strSQL.= "'".date("Y-m-d")."','".$currentIP."','".$currentSessionID."','".$currentUserID."','".$currentUserAgent."'";
        $strSQL.= ",NOW(),\"".  addslashes(json_encode($_SERVER))."\"";
        $strSQL.= ") ";
        Yii::$app->db->createCommand($strSQL)->execute();

	//******************** Get Counter ************************//

	// Today //
	$strSQL = " SELECT COUNT(DATE) AS CounterToday FROM project84_counter WHERE DATE = '".date("Y-m-d")."' ";
	$objResult = Yii::$app->db->createCommand($strSQL)->queryAll();
        //$objQuery = mysql_query($strSQL);
	//$objResult = mysql_fetch_array($objQuery);
	$out[strToday] = $objResult[0]["CounterToday"];

	// Yesterday //
	$strSQL = " SELECT NUM FROM project84_counter_daily WHERE DATE = '".date('Y-m-d',strtotime("-1 day"))."' ";
	//$objQuery = mysql_query($strSQL);
	//$objResult = mysql_fetch_array($objQuery);
	//$strYesterday = $objResult["NUM"];
        $objResult = Yii::$app->db->createCommand($strSQL)->queryAll();
        $out[strYesterday] = $objResult[0]["NUM"];

	// This Month //
	$strSQL = " SELECT SUM(NUM) AS CountMonth FROM project84_counter_daily WHERE DATE_FORMAT(DATE,'%Y-%m')  = '".date('Y-m')."' ";
	//$objQuery = mysql_query($strSQL);
	//$objResult = mysql_fetch_array($objQuery);
	//$strThisMonth = $objResult["CountMonth"];
        $objResult = Yii::$app->db->createCommand($strSQL)->queryAll();
        $out[strThisMonth] = $objResult[0]["CountMonth"];

	// Last Month //
        $strSQL = " SELECT SUM(NUM) AS CountMonth FROM project84_counter_daily WHERE DATE_FORMAT(DATE,'%Y-%m')  = '".date('Y-m',strtotime("-1 month"))."' ";
	//$objQuery = mysql_query($strSQL);
	//$objResult = mysql_fetch_array($objQuery);
	//$strLastMonth = $objResult["CountMonth"];
        $objResult = Yii::$app->db->createCommand($strSQL)->queryAll();
        $out[strLastMonth] = $objResult[0]["CountMonth"];

	// This Year //
	$strSQL = " SELECT SUM(NUM) AS CountYear FROM project84_counter_daily WHERE DATE_FORMAT(DATE,'%Y')  = '".date('Y')."' ";
	//$objQuery = mysql_query($strSQL);
	//$objResult = mysql_fetch_array($objQuery);
	//$strThisYear = $objResult["CountYear"];
        $objResult = Yii::$app->db->createCommand($strSQL)->queryAll();
        $out[strThisYear] = $objResult[0]["CountYear"];

	// Last Year //
	$strSQL = " SELECT SUM(NUM) AS CountYear FROM project84_counter_daily WHERE DATE_FORMAT(DATE,'%Y')  = '".date('Y',strtotime("-1 year"))."' ";
	//$objQuery = mysql_query($strSQL);
	//$objResult = mysql_fetch_array($objQuery);
	//$strLastYear = $objResult["CountYear"];
        $objResult = Yii::$app->db->createCommand($strSQL)->queryAll();
        $out[strLastYear] = $objResult[0]["CountYear"];
        
        
        return $out;
    }

}//class
