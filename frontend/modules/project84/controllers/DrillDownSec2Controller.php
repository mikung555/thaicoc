<?php

namespace frontend\modules\project84\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\VarDumper;
use \frontend\modules\project84\classes\Drilldown;
use yii\web\Session;
use yii\helpers\Json;
/**
 * ForumTypeController implements the CRUD actions for ForumType model.
 */
class DrillDownSec2Controller extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all ForumType models.
     * @return mixed
     */
    public function actionAjaxData($provincecode = null, $report_id = null, $province_name = null, $sumtotal = null)
    {
        $render = "drilldown_sec2";
        $checkadmin = self::CheckAdmin();
        $url = $this->renderAjax($render, [
            'provincecode' => $provincecode,
            'report_id' => $report_id,
            'province_name' => $province_name,
            'sumtotal' => $sumtotal,
            'checkadmin' => $checkadmin,
        ]);
        return $url;
    }


    public function actionAjaxDataPerson($report_id = null, $sitecode = null, $hsitecode = null, $tabmenu = null, $provincecode = null)
    {   
        if( 0 ){
            print_r($_GET);
        
            echo "ค่าที่ส่งจาก _GET ";
            echo "<br />Report ID: ".$report_id;
            echo "<br />tabmenu: ".$tabmenu;
            echo "<br />Province Code: ".$provincecode;
        }
        
        $varsite = \Yii::$app->request->get('varsite');
        if( strlen($varsite)>0 ){
            $sitecode = \Yii::$app->request->get($varsite);
        }
        $checkadmin = self::CheckAdmin();
        if ($sitecode != "") {
            $data = $sitecode;
        } else {
            $sitecode = Yii::$app->user->identity->userProfile->sitecode;
            $data = $sitecode;
        }
        $hosname = self::GetHospitalName($data);
        $dataProvider = self::Getperson($report_id, $varsite, $sitecode, $tabmenu, $provincecode);
        $countdata = count($dataProvider);
        //VarDumper::dump($dataProvider,10,true);

        if ($countdata > 0) {
            $status = "yes";
        } else {
            $status = "no";
        }
        $render = "drilldown_person_sec2";
        $url = $this->renderAjax($render, [
            'dataProvider' => $dataProvider,
            'hosname' => $hosname,
            'status' => $status,
        ]);
        return $url;

    }

    public function GetHospitalName($sitecode)
    {
        $sql = "SELECT name,amphur,province FROM  `all_hospital_thai` WHERE hcode='$sitecode'";
        $dataProvider = Yii::$app->db->createCommand($sql)->queryAll();
        return $dataProvider;
    }


    public function GetAmphur($report_id, $provincecode)
    {
        $sql = "SELECT provincecode,amphurcode,amphur,hcode,sitecode,report_id ";
        $sql.= "FROM `project84_tb2_data` a ";
        $sql.= "INNER JOIN `all_hospital_thai` b ON a.sitecode=b.hcode ";
        $sql.= "WHERE  report_id='$report_id' AND provincecode = '$provincecode'  ";
        $sql.= "GROUP BY amphurcode ORDER BY amphurcode ASC";
        //echo $sql;
        $dataProvider = Yii::$app->db->createCommand($sql)->queryAll();
        return $dataProvider;
    }


    public function GetHospital($report_id, $provincecode, $amphurcode)
    {
        $sql = "SELECT provincecode,amphurcode,code9,`name`,hcode,hsitecode,sitecode,report_id,
  COUNT(DISTINCT ptid) AS nall,
  COUNT(DISTINCT IF(cca02=1,ptid,NULL)) AS cca02,
  COUNT(DISTINCT IF(abnormal=1,ptid,NULL)) AS abnormal,
  COUNT(DISTINCT IF(suspected=1,ptid,NULL)) AS suspected,
  COUNT(DISTINCT IF(ctmri=1,ptid,NULL)) AS ctmri,
  COUNT(DISTINCT IF(cca=1,ptid,NULL)) AS cca,
  COUNT(DISTINCT IF(cca=1 AND ctmri=1 AND treated=1,ptid,NULL)) AS treated
  FROM `project84_tb2_data` a INNER JOIN `all_hospital_thai` b ON a.hsitecode=b.hcode WHERE  report_id='$report_id' AND provincecode = '$provincecode' AND amphurcode='$amphurcode'  GROUP BY `name` ORDER BY `code9` DESC";
        //echo $sql;
        $dataProvider = Yii::$app->db->createCommand($sql)->queryAll();
        return $dataProvider;
    }
    
    public function GetHospitalCCA01ICF($report_id, $provincecode, $amphurcode)
    {
        $sql = "SELECT provincecode,amphurcode,code9,`name`,hcode,hsitecode,sitecode,report_id,
  COUNT(DISTINCT ptid) AS nall,
  COUNT(DISTINCT IF(cca02=1,ptid,NULL)) AS cca02,
  COUNT(DISTINCT IF(abnormal=1,ptid,NULL)) AS abnormal,
  COUNT(DISTINCT IF(suspected=1,ptid,NULL)) AS suspected,
  COUNT(DISTINCT IF(ctmri=1,ptid,NULL)) AS ctmri,
  COUNT(DISTINCT IF(cca=1,ptid,NULL)) AS cca,
  COUNT(DISTINCT IF(cca=1 AND ctmri=1 AND treated=1,ptid,NULL)) AS treated
  FROM `project84_tb2_data` a INNER JOIN `all_hospital_thai` b ON a.sitecode=b.hcode WHERE  report_id='$report_id' AND provincecode = '$provincecode' AND amphurcode='$amphurcode'  GROUP BY `name` ORDER BY `code9` DESC";
        $dataProvider = Yii::$app->db->createCommand($sql)->queryAll();
        return $dataProvider;
    }

    public function ShortName($name)
    {
        $hospital_name = str_replace("โรงพยาบาล", "รพ.", $name);
        $hospital_name = str_replace("ส่งเสริมสุขภาพตำบล", "สต.", $hospital_name);
        $hospital_name = str_replace("สถานีอนามัย", "สอ.", $hospital_name);
        $hospital_name = str_replace("สำนักงานสาธารณสุขอำเภอ", "สสอ.", $hospital_name);
        $hospital_name = str_replace("สำนักงานสาธารณสุขจังหวัด", "สสจ.", $hospital_name);
        return $hospital_name;
    }


    public function Getperson($report_id, $varsite = '', $sitecode = null, $tabmenu = null, $provincecode = null)
    {
        if ($tabmenu == "yes") {
            $sitecode = Yii::$app->user->identity->userProfile->sitecode;
            $data = "AND a.hsitecode='$sitecode' and b.hsitecode='$sitecode' ";
        } else {
            $checkadmin = self::CheckAdmin();
            if ($checkadmin == "yes") {
                $data = "AND a.hsitecode='$sitecode' and b.hsitecode='$sitecode'";
            } else {
                $sitecode = Yii::$app->user->identity->userProfile->sitecode;
                $data = "AND a.hsitecode='$sitecode' and b.hsitecode='$sitecode'";
            }
        }
        if( strlen($provincecode)>0 ){
            $provinceSql    = "and b.provincecode=\"".addslashes($provincecode)."\"";
        }
        if( strlen($varsite)>0 ){
            if( strlen($sitecode)>0 ){
                $checkadmin = self::CheckAdmin();
                if ( $checkadmin == "yes") {
                    $data = "AND a.".$varsite."='$sitecode' and b.".$varsite."='$sitecode'";
                } else {
                    $sitecode = Yii::$app->user->identity->userProfile->sitecode;
                    $data = "AND a.".$varsite."='$sitecode' and b.".$varsite."='$sitecode'";
                }
            }
        }else{
            if( strlen($sitecode)>0 ){
                $checkadmin = self::CheckAdmin();
                if ($checkadmin == "yes") {
                    $data = "AND a.hsitecode='$sitecode' and b.hsitecode='$sitecode'";
                } else {
                    $sitecode = Yii::$app->user->identity->userProfile->sitecode;
                    $data = "AND a.hsitecode='$sitecode' and b.hsitecode='$sitecode'";
                }
            }
        }
        $sql = "SELECT
id,
ptid,
title,
fname,
surname,
target,
hsitecode,
hptcode,
sitecode,
`name` as hospitalname,
report_id,
cca02,
abnormal,
suspected,
ctmri,
cca,
treated
FROM
(
SELECT
DISTINCT
b.id,
(a.ptid),
title,
`name` as fname,
surname,
a.target,
a.hsitecode,
b.hptcode,
a.sitecode,
report_id,
cca02,
abnormal,
suspected,
IF(cca02=2 AND ctmri=1,1,0) AS ctmri,
IF(cca02=2 AND cca=1,1,0) AS cca,
IF(cca02=2 AND cca=1 AND ctmri=1 AND treated=1,1,0) AS treated,
count(*)
FROM `project84_tb2_data` a
INNER JOIN
`tb_data_1` b
ON a.ptid=b.ptid
WHERE  report_id='$report_id' $data  AND cca02 > '0' group by b.ptid ORDER BY cca DESC
)as a
INNER JOIN `all_hospital_thai` b
ON a.hsitecode=b.hcode
$provinceSql order by hptcode ";

        //echo $sql;
        
        $dataProvider = Yii::$app->db->createCommand($sql)->queryAll();
        return $dataProvider;
    }

    public function CheckAdmin()
    {
        $user_id = Yii::$app->user->identity->userProfile->user_id;
        $sqlControl = "SELECT
user_id
FROM
`rbac_auth_assignment`
WHERE
`item_name`
LIKE '%administrator%'
AND user_id='$user_id'
";
        $dataProvider = Yii::$app->db->createCommand($sqlControl)->queryAll();
        $countadmin = count($dataProvider);
        // เปลี่ยนวิธีการดึง
        $_arrayuserfix = array('1435745159010043375');
        if(in_array($user_id, $_arrayuserfix ) ){
            $countadmin = 1;
        }else{
            $countadmin = 0;
        }
        // ดึงข้อมูลตามเงื่อนไข
        if ($countadmin > 0) {
            $admin = "yes";
        } else {
            $admin = "no";
        }
        return $admin;
    }
    public function actionDilldownperson()
    {     
        $dataGet = \Yii::$app->request->get();
        $status="ok";
        $check_province = Drilldown::findProvince($dataGet[provincecode]);//check ว่าจังหวัดนี่มีรหัสผ่านแล้วหรือยัง
        if($check_province[total]==0){
            $objResult = Drilldown::getProvincename($dataGet[provincecode]);
            if(Yii::$app->user->can('administrator')==TRUE){
                $status="adminNopass";
            }else{
                $status="noPass";
            }
            $key=FALSE;
        }else{
            $session = Yii::$app->session;
            $session->open();
            $objResult=Drilldown::getPassword($dataGet[provincecode]);
            if($session['ss_key']==NULL){
                $check_key=NULL;
            }else{
               $check_key=$session['ss_key']; 
            }
            if($check_key == $objResult[hash_password]){//ถ้ารหัสที่รับมาตรงกับที่อยู่ในฐานข้อมูล
                $key=TRUE;
            }else{
                $key=FALSE;
            }
            $session->close();
        }
        if($dataGet[show] == '1'){//เลือกเงื่อนไขการ select ข้อมูล
            $query= Drilldown::getDrilldownPerson($dataGet[zone], $dataGet[report_id], $dataGet[provincecode]);
            $show='ที่ลงทะเบียน';
        }else if($dataGet[show] == '2'){
            $query= Drilldown::getDrilldownPerson($dataGet[zone], $dataGet[report_id], $dataGet[provincecode], "AND cca02=1");
            $show='อัลตร้าซาวด์';  
        }else if($dataGet[show] == '3'){
            $query= Drilldown::getDrilldownPerson($dataGet[zone], $dataGet[report_id], $dataGet[provincecode], "AND abnormal=1");
            $show='ผิดปกติอย่างใดอย่างหนึ่ง'; 
        }else if($dataGet[show] == '4'){
            $query= Drilldown::getDrilldownPerson($dataGet[zone], $dataGet[report_id], $dataGet[provincecode], "AND suspected=1");
            $show='สงสัย CCA';  
        }else if($dataGet[show] == '5'){
            $query= Drilldown::getDrilldownPerson($dataGet[zone], $dataGet[report_id], $dataGet[provincecode], "AND suspected=1 AND ctmri=1");
            $show='CT / MRI'; 
        }else if($dataGet[show] == '6'){
            $query= Drilldown::getDrilldownPerson($dataGet[zone], $dataGet[report_id], $dataGet[provincecode], "AND suspected=1 AND ctmri=1 AND cca=1");
            $show='พบเป็นมะเร็ง';
        }else if($dataGet[show] == '7'){
            $query= Drilldown::getDrilldownPerson($dataGet[zone], $dataGet[report_id], $dataGet[provincecode], "AND suspected=1 AND ctmri=1 AND cca=1 AND treated=1");
            $show='ได้รับการรักษา';
        }
        $dataProvider = new \yii\data\ArrayDataProvider([
            'allModels' => $query
        ]);
        return $this->renderAjax('dilldown_show_sec2'
            ,[
                'show' => $show,
                'objResult' => $objResult,
                'dataProvider' => $dataProvider,
                'dataGet' => $dataGet,
                'key'=> $key,
                'status'=> $status,
            ]
        );
    }
    public function actionCheckpassword()
    {
        $dataGet = \Yii::$app->request->get();
        $status="ok";
        $check_province = Drilldown::findProvince($dataGet[provincecode]);//checkว่าจังหวัดนี่มีรหัสผ่านแล้วหรือยัง
        if($check_province['total']==0 and Yii::$app->user->can('administrator')== FALSE){
            $key=FALSE;
            $status="noPass";
            $objResult = Drilldown::getProvincename($dataGet[provincecode]);
        }elseif($check_province['total']==0 and Yii::$app->user->can('administrator')== TRUE){
            $status="createPass";
            $provin_name = Drilldown::getProvincename($dataGet[provincecode]);
            $create = Drilldown::insertPassword($dataGet[provincecode],$provin_name[PROVINCE_NAME],md5($dataGet[input_pass]));
            $array = [
                'province_code' => $dataGet['provincecode']
            ];
            $code=JSON::encode($array);
            $user_id = Yii::$app->user->identity->userProfile->user_id;
            $log = Drilldown::insertLog(createpassword,$code,$dataGet[input_pass],$user_id);
            $objResult=Drilldown::getPassword($dataGet[provincecode]);
            $session = new Session;
            $session->open();
            $session['ss_key'] = md5($dataGet[input_pass]);
            $check_key=$session['ss_key'];
            if($check_key == $objResult['hash_password']){
                $key=TRUE;
            }else{
                $key=FALSE;
                $status="error";
            }
            $session->close();
        }else{
            $objResult=Drilldown::getPassword($dataGet[provincecode]);
            $session = new Session;
            $session->open();
            $session['ss_key'] = md5($dataGet[input_pass]);
            $check_key=$session['ss_key'];
            if($check_key == $objResult['hash_password']){
                $key=TRUE;
            }else{
                $key=FALSE;
                $status="error";
            }
            $session->close();
        }
        if((int)$dataGet['status']==1 ){//ถ้ามีการติ้ก reset รหัสผ่าน
            $status="success";
            $update = Drilldown::updatePassword($dataGet['provincecode'],md5($dataGet[input_pass])); 
            $array = [
                'province_code' => $dataGet['provincecode']
            ];
            $code=JSON::encode($array);
            $user_id = Yii::$app->user->identity->userProfile->user_id;
            $log = Drilldown::insertLog(changepassword,$code,$dataGet[input_pass],$user_id);
            $session = new Session;
            $session->open();
            $session['ss_key'] = md5($dataGet[input_pass]);
            $key=TRUE;
        }
        if($dataGet['show'] == '1'){
            $option="";
            $show='ที่ลงทะเบียน';
        }else if($dataGet['show'] == '2'){
            $option="AND cca02=1";
            $show='อัลตร้าซาวด์';  
        }else if($dataGet['show'] == '3'){
            $option="AND abnormal=1";
            $show='ผิดปกติอย่างใดอย่างหนึ่ง'; 
        }else if($dataGet['show'] == '4'){
            $option="AND suspected=1";
            $show='สงสัย CCA';  
        }else if($dataGet['show'] == '5'){
            $option="AND suspected=1 AND ctmri=1";
            $show='CT / MRI'; 
        }else if($dataGet['show'] == '6'){
            $option="AND suspected=1 AND ctmri=1 AND cca=1";
            $show='พบเป็นมะเร็ง';
        }else if($dataGet['show'] == '7'){
            $option="AND suspected=1 AND ctmri=1 AND cca=1 AND treated=1";
            $show='ได้รับการรักษา';
        }
         $sql = "SELECT 
                CONCAT(c.ptcode,c.hptcode)as fullid,
                CONCAT(c.title,c.name,' ',c.surname)as fullname,
                a.*
                FROM project84_tb2_data a 
                INNER JOIN all_hospital_thai b ON a.hsitecode = b.hcode
                INNER JOIN tb_data_1 c ON a.ptid = c.ptid
                WHERE zone_code =$dataGet[zone]
                AND a.report_id=$dataGet[report_id]
                AND provincecode =$dataGet[provincecode] $option
                GROUP BY a.ptid";
                $query = Yii::$app->db->createCommand($sql)->queryAll();
        $dataProvider = new \yii\data\ArrayDataProvider([
            'allModels' => $query
        ]);

        return $this->renderAjax('dilldown_show_sec2',[
            'show' => $show,
            'objResult' => $objResult,
            'dataProvider' => $dataProvider,
            'dataGet' => $dataGet,
            'key'=> $key,
            'status'=> $status,
        ]);
    }
}//class