<?php

namespace frontend\modules\project84\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\VarDumper;

/**
 * ForumTypeController implements the CRUD actions for ForumType model.
 * 
 * เงื่อนไข สำหรับการทำรายงานส่วนที่ 1 
 * เงื่อนไขการ count ในรายงานถวายส่วนที่ 1: Primary prevention
    1. จำนวน “เลือกกลุ่มเสี่ยง (ใบทำบัตร)” ให้นับจำนวนคนที่เข้าข่ายดังนี้ 
 *      - ถ้ามีข้อมูลพื้นฐาน (CCA01) ให้ยึดวันที่ complete (f1vdcomp) ในช่วง 1 ต.ค. 2559 ถึง 31 ก.ย. 2560
 *      - ถ้าไม่มีข้อมูลพื้นฐาน มีแต่ “เลือกกลุ่มเสี่ยง (ใบทำบัตร)” ให้ยึดวันที่ใน create_date ในช่วง 1 ต.ค. 2559 ถึง 31 ก.ย. 2560
 *      - ถ้าไม่มีข้อมูลทั้ง “เลือกกลุ่มเสี่ยง และ “ข้อมูลพื้นฐาน” แต่มีการมาตรวจ OV ในช่วง 1 ต.ค. 2559 ถึง 31 ก.ย. 2560 ก็ให้ count ลงไปใน “เลือกกลุ่มเสี่ยง” และถ้าเคยมีการเก็บข้อมูลพื้นฐานมาแล้วก็ให้ count ลงใน “ข้อมูลพื้นฐาน” ด้วยแต่ถ้าไม่มีข้อมูลพื้นฐานก็ไม่ต้อง count ลงข้อมูลพื้นฐาน ให้ count เฉพาะใน “เลื่อกกลุ่มเสี่ยง”
 *      - ถ้ามีการบันคีย์ข้อมูลพื้นฐานในปีงบประมาณนี้ แต่ back date วัน complete form (f1vdcomp) ไปเป็นปีงบประมาณที่แล้ว โดยที่ปีนี้ยังไม่มี activity ใดๆ ก็ไม่ต้องนับ แม้ว่าโดยอัตโนมัติแล้ว วันที่ในใบทำบัตร (create_date) จะเป็นวันปัจจุบันที่คีย์ก็ตาม
 *      - “เลือกกลุ่มเสียง” และ “ข้อมูลพื้นฐาน สามารถ count คนซ้ำกันได้หากมีการทำ activity ข้าม site แต่จะไม่ count ซ้ำใน site เดียวกัน
    2. สำหรับ “ตรวจพยาธิ” และ “ติดเชื้อ OV” นับจากทั้ง kato, parasep, fect และ urine เฉพาะที่มี vdate ในช่วง 1 ต.ค 2559 ถึง  31 ก.ย. 2560 เท่านั้น
    3. “ให้สุขศึกษา” นับจากฟอร์ม OV-03 ที่ตัวแปร hedu มีค่าเท่ากับ 1 หรือ มีข้อมูลอยู่ในฟอร์ม OV-02
    4. “ให้การรักษา” นับจากเฉพาะคนที่ฟอร์ม OV-03 ตัวแปร pzq = 1 
    5. การนับข้อมูลจากฟอร์มต่างๆ จะยึด rstat เท่า 2 คือ submitted แล้วเท่านั้น ยกเว้นฟร์อมใบทำบัตร
 *      count rstat = 2,4,5 
 * 
 * 
 */
class ReportSec1Controller extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all ForumType models.
     * @return mixed
     */
    public function GetDataTb1($report_id, $colum, $sort)
    {
        $datareport = self::GetDataTb1ByReportId($report_id, $colum, $sort);
        $datasum = self::GetDataSumTb1ByReportId($report_id);
        $data[datareport] = $datareport;
        if ($report_id == 1) {
            $data[datasumall] = $datasum;
        } else {
            $data[datasumall] = self::GetDataSumTb1ByReportId($report_id);
        }
        $data[datasum] = $datasum;
        return $data;
    }

    public function GetDataTb1ByReportId($report_id, $colum, $sort)
    {
        $sqlControl = "SELECT * FROM   project84_report_sec1 WHERE report_id='$report_id'  ORDER BY $colum $sort";
        $dataProvider = Yii::$app->db->createCommand($sqlControl)->queryAll();
        //var_dump($dataProvider);
        
        return $dataProvider;
    }//public function GetDataTb3ByReportId($report_id)

    public function GetDataSumTb1ByReportId($report_id)
    {
        $sql = "SELECT sum(riskgroup),sum(icf),sum(register),sum(treatov),sum(ov),sum(ov02),sum(ov03) FROM  project84_report_sec1  where report_id='$report_id'";
        $dataProvider = Yii::$app->db->createCommand($sql)->queryAll();
        return $dataProvider;
    }//public function GetDataSumTb3ByReportId($report_id)

    public function actionTest()
    {
        $report_id = 1;
        $queryTccAddress = Yii::$app->db->createCommand('SELECT address FROM project84_zone')->queryAll();
        foreach ($queryTccAddress as $key => $row) {
            $tccbotRow = Yii::$app->dbbot->createCommand('SELECT COUNT(*) AS ncount
          FROM person
          WHERE sitecode IN (SELECT hcode FROM buffe_webservice.all_hospital_thai WHERE addresscode="' . $row['address'] . '")')->queryOne();
            $queryTccAddress[$key]['tccbot'] = 0;
            Yii::$app->db->createCommand('UPDATE project84_report_sec1 SET tccbot = ' . $tccbotRow['ncount'] . ' WHERE report_id=' . $report_id . ' AND address="' . $row['address'] . '"')->execute();
        }
        /*
  $dataProviderSec1=self::GetDataTb1ByReportId(1,"treatov","desc");
  return $this->render('report_sec1', [
      'dataProviderSec1' => $dataProviderSec1
  ]);
  */
    }//public function GetDataTb3ByReportId($report_id)


    function Getperson()
    {
        $sql = "SELECT count(*) as nall from person";
        $data = Yii::$app->dbbot->createCommand($sql)->queryOne();
    }

}//class
