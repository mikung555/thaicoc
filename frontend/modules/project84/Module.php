<?php

namespace frontend\modules\project84;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'frontend\modules\project84\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
