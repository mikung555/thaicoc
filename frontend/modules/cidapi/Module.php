<?php

namespace app\modules\cidapi;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\cidapi\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
