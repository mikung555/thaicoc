<?php

namespace app\modules\cidapi\controllers;

use Yii;
use frontend\modules\emobile\models\InvUser;
use frontend\modules\emobile\models\InvUserSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

class UserController extends Controller {

    public $enableCsrfValidation = false;

    public function beforeAction($action) {
        $this->enableCsrfValidation = false;


        if (parent::beforeAction($action)) {
            if (in_array($action->id, array('create', 'update'))) {
                
            }
            return true;
        } else {
            return false;
        }
    }

    public function actionIndex() {
        return $this->render('index');
    }

    public function actionCheckLogin() {

        Yii::$app->response->format = Response::FORMAT_JSON;

        $username = \Yii::$app->request->post('username');
        $password = \Yii::$app->request->post('password');

        $model = \frontend\modules\emobile\models\InvUser::find()
                ->select(['`inv_user`.*'])
                ->where('inv_user.username=:username', [':username' => $username])
                ->one();
        $password_hash = $model->password;
        //  return $password;

        $check = Yii::$app->getSecurity()->validatePassword($password, $password_hash);
        if ($check) {
            return $model->id;
        } else {

            return 0;
        }
    }

    public function actionGetPin() {

        Yii::$app->response->format = Response::FORMAT_JSON;

        $username = \Yii::$app->request->post('username');
        $password = \Yii::$app->request->post('password');

        $model = \frontend\modules\emobile\models\InvUser::find()
                ->select(['`inv_user`.*'])
                ->where('inv_user.username=:username', [':username' => $username])
                ->one();
        $password_hash = $model->password;
        //  return $password;
       
        $check = Yii::$app->getSecurity()->validatePassword($password, $password_hash);
        if ($check) {
             $userid=$model->user_id;
             
                                      
        } else {

            return 0;
        }
    }

}
