<?php

namespace app\modules\cidapi\controllers;

use yii\web\Controller;

class DefaultController extends Controller
{
     public $enableCsrfValidation = false;
   
        public function beforeAction($action)
    {
            $this->enableCsrfValidation = false;
      

        if (parent::beforeAction($action)) {
            if (in_array($action->id, array('create', 'update'))) {

            }
            return true;
        } else {
            return false;
        }
      
    }
    public function actionIndex()
    {
        return $this->render('index');
    }
    
       public function  actionCheckLogin(){
            
         Yii::$app->response->format = Response::FORMAT_JSON;
         
         $username =$_POST['username'];
         $password =$_POST['password'];
        
           $model = \frontend\modules\emobile\models\InvUser::find()
                ->select(['`inv_user`.*'])
                 ->where('inv_user.username=:username', [':username'=>$username])
                ->one();
        $password_hash = $model->password;
      //  return $password;
        
    $check =     Yii::$app->getSecurity()->validatePassword ($password,$password_hash);
    if($check){
        return  $model->id;
        
    }else{
        
          return  0;
    }
            
         
    }
}
