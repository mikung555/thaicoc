<?php
use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\widgets\InputWidget;
use yii\widgets\MaskedInput;
use yii\web\JsExpression;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use kartik\depdrop\DepDrop;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
/* @var $model \frontend\modules\user\models\SignupForm */

$this->title = Yii::t('frontend', 'สมัครสมาชิก');

?>


<?php
$form = ActiveForm::begin([
    'type' => ActiveForm::TYPE_HORIZONTAL,
    'options' => ['enctype' => 'multipart/form-data'
	]]);
?>
<div class="row">
    <div class="col-md-6 col-md-offset-3">
	
    
<div class='box box-solid box-success' id='cidForm'>
    <div class="box-header">
       <i class="glyphicon glyphicon-user"></i> สมัครสมาชิก
    </div>
    <br>
    <div class="box-body">
	<?php
	    echo $form->field($signup,'username', [
	    ])->textInput()->hint('ใช้เลข 13 หลักเป็น Username ในการเข้าใช้งาน ท่านสามารถเปลี่ยน Username ได้หลังจากการ Login ครั้งแรก');
         ?>
	
	<?php
            echo $form->field($signup,'password')->passwordInput()->hint('*เป็นอักษรภาษาอังกฤษหรือตัวเลขเท่านั้น โดยต้องไม่มีช่องว่าง');
        ?>
        
	<?php
	    echo $form->field($mprofile,'title')->dropDownList(['0'=>'นาย','1'=>'นาง','2'=>'นางสาว']);
	?>
	
	<?php
            echo $form->field($mprofile,'firstname')->textInput();
        ?>
	
	<?php
	    echo $form->field($mprofile,'lastname')->textInput();
	?>
	
	<?php
	echo $form->field($mprofile, 'gender')->radioList(['1' => 'ชาย', '2' => 'หญิง']);
	?>
	
	<?php 
	    echo $form->field($mprofile, 'email')->textInput();
//		    echo $form->field($mprofile,'email')->widget(MaskedInput::className(),[
//                        'clientOptions'=>[
//                            'alias'=>'email'
//                        ]
//                    ])->label(false);
	?>
	
	<?php echo $form->field($mprofile,'telephone')->textInput();?>
    
	
	<div class="form-group">
	    <div class="col-md-offset-2 col-md-10">
		<label>เลือกบทบาท (สามารถเลือกได้มากกว่าหนึ่งบทบาท)</label>
		<div class="checkbox">
		    <?=  Html::checkbox('tmpStatus', true, ['label'=>'ผู้รับบริการหรือบุคคลทั่วไป'])?>
		</div>
	    </div>
	</div>
	
	<div class="form-group">
	    <div class="col-md-offset-2 col-md-10">
		<div class="checkbox">
		    <?= Html::activeCheckbox($mprofile, 'volunteer_status', [])?>
		</div>
	    </div>
	</div>
	
	<div id="volunteer-form" class="col-md-offset-2 col-md-6" style="<?=($mprofile->volunteer_status==1)?'':'display:none;'?>">
	    <?= Html::activeDropDownList($mprofile, 'volunteer', [1=>'อสค. (อาสาสมัครประจำครอบครัว)', 2=>'อสม. (อาสาสมัครสาธารณสุขประจำหมู่บ้าน)'], ['class'=>'form-control', 'prompt'=>'---เลือกอาสาสมัคร---'])?>
	</div>
	
	<div class="form-group">
	    <div class="col-md-offset-2 col-md-10">
		<div class="checkbox">
		    <?= Html::activeCheckbox($mprofile, 'status', ['label'=>'บุคลากร'])?>
		</div>
	    </div>
	</div>
	
	<div class="col-md-offset-2 col-md-10">
	<div id="status-personal-form" style="display:none;">
	    
            <div class="row" id="rowPersonal">
                <div class="col-lg-6">
                    <label>เลือกบทบาทเฉพาะสังกัด</label>
                    <?php
                    echo $form->field($mprofile,'status_personal')->dropDownList(
                        [
                            ''=>'-- เลือกหน่วยงานสังกัดและบทบาท --',
                            '2'=>'ผู้ดูแลระบบฐานข้อมูล',
                            '3'=>'ผู้ให้บริการด้านการแพทย์และสาธารณสุข',
                            '4'=>'ผู้บริหาร',
                            '10'=>'นักวิจัย',
                            '11'=>'อื่นๆ ',

                        ])->label(false)
                    ?>
                </div>
                <div class="col-lg-6" id="rowManager" style="display:none;">
                    <label>เลือกระดับ</label>
                    <?php
                      echo $form->field($mprofile,'status_manager')->dropDownList([
                              ''=>'-- เลือกระดับหน่วยงาน --',
                              '5'=>'หน่วยงานระดับประเทศ',
                              '6'=>'หน่วยงานระดับเขต',
                              '7'=>'หน่วยงานระดับจังหวัด',
                              '8'=>'หน่วยงานระดับอำเภอ',
                              '9'=>'หน่วยงานทางการแพทย์และสาธารณสุข'
                              ])->label(false);
                    ?>
                </div>

                <div class="col-lg-6" id="rowStatusOther" style="display:none;">
                    <label>บทบาทอื่นๆ</label>
                    <?php
                    echo $form->field($mprofile,'status_other')->label(false);
                    ?>
                </div>
            </div>
            <div class="row" id="rowNation" style="display:none;">
                <div class="col-lg-12">
                    <label>ชื่อหน่วยงานระดับประเทศ</label>
                    <?php
                    echo $form->field($mprofile,'department_area_text')->textInput()->label(false);
                    ?>
                </div>
            </div>
            <div class="row" id="rowArea" style="display:none;">
                <div class="col-lg-6">
                    <label>หน่วยงานระดับเขต</label>
                    <?php
                    echo $form->field($mprofile,'department_area')->dropDownList([''=>'-- เลือกเขตบริการ --',
                        '1'=>'เขตบริการที่ 1','2'=>'เขตบริการที่ 2','3'=>'เขตบริการที่ 3','4'=>'เขตบริการที่ 4','5'=>'เขตบริการที่ 5','6'=>'เขตบริการที่ 6','7'=>'เขตบริการที่ 7',
                        '8'=>'เขตบริการที่ 8','9'=>'เขตบริการที่ 9','10'=>'เขตบริการที่ 10','11'=>'เขตบริการที่ 12','13'=>'เขตบริการที่ 13','14'=>'เขตบริการที่ 14'
                    ])->label(false)
                    ?>
                </div>
                <div class="col-lg-6">
                    <label>ชื่อหน่วยงานระดับเขต</label>
                    <?php
                    echo $form->field($mprofile,'department_area_text')->textInput()->label(false);
                    ?>
                </div>
            </div>
            <div class="row" id="rowProvince" style="display:none;">
                <div class="col-lg-6">
                    <label>หน่วยงานระดับจังหวัด</label>
                    <?php
		    echo $form->field($mprofile, 'department_province')->widget(Select2::className(), [
			    'options' => ['placeholder' => 'จังหวัด','id'=>'province','value'=>'test'],
			    'data' => ArrayHelper::map($dataProvince,'PROVINCE_CODE','PROVINCE_NAME'),
			    'pluginOptions' => [
				'allowClear' => true
			    ],
			])->label(false);
                    ?>
                </div>
                <div class="col-lg-6">
                    <label>ชื่อหน่วยงาน</label>
                    <?php
                        echo $form->field($mprofile,'department_province_text')->textInput()->label(false);
                    ?>
                </div>
            </div>
            <div class="row"  id="rowAmphur" style="display:none;">
                <div class="col-lg-6"  >
                    <label>หน่วยงานระดับอำเภอ</label>
                    <?php
                    echo $form->field($mprofile,'department_amphur')->widget(DepDrop::classname(),[
                        'type'=>  DepDrop::TYPE_SELECT2,
                        'options'=>['id'=>'amphur'],
                        'model'=>$mprofile,
                        'attribute'=>'department_amphur',
                        'pluginOptions'=>[
                            'depends'=>['province'],
                            'placeholder'=>'อำเภอ',
                            'url'=>'/user/formquery/genamphur'
                        ]
                    ])->label(false);
                    ?>
                </div>
                <div class="col-lg-6">
                    <label>ชื่อหน่วยงานระดับอำเภอ</label>
                    <?php
                    echo $form->field($mprofile,'department_amphur_text')->textInput()->label(false);
                    ?>
                </div>
            </div>
        </div> </div><!-- END -->
	
	
	<?php
	    echo Html::activeHiddenInput($mprofile, 'department');
            if(Yii::$app->keyStorage->get('frontend.domain') == 'thaicarecloud.org' || Yii::$app->keyStorage->get('frontend.domain')=='yii2-starter-kit.dev'){
                echo $form->field($mprofile, 'inout')->radioList([0=>'หน่วยงานบริการสุขภาพ', 2=>'ร้านยา', 3=>'องค์กรปกครองส่วนท้องถิน', 1=>'หน่วยงานอื่นๆ']);
            } else {
                echo $form->field($mprofile, 'inout')->radioList([0=>'หน่วยงานบริการสุขภาพ', 1=>'หน่วยงานอื่นๆ']);
            }
	?>
	
	<?php
                       
	    echo $form->field($mprofile, 'sitecode')->widget(yii\jui\AutoComplete::className(), [
		'clientOptions' => [
		'minChars' => 1,
		'source' => new JsExpression("function(request, response) {
		    var userType = $('input[name=\"UserProfile[inout]\"]:checked').val();
		    if(userType==2){
			$.getJSON('".yii\helpers\Url::to(['/user/formquery/querys2'])."', {
			    q: request.term
			}, response);
		    } else if(userType==3){
			$.getJSON('".yii\helpers\Url::to(['/user/formquery/querys3'])."', {
			    q: request.term
			}, response);
		    } else {
			$.getJSON('".yii\helpers\Url::to(['/user/formquery/querys'])."', {
			    q: request.term
			}, response);
		    }
		    
		}"),
		'select' => new JsExpression("function( event, ui ) {
		    $('#userprofile-department').val(ui.item.id);
		 }"),
		'change' => new JsExpression("function( event, ui ) {
		    if (!ui.item) {
			$('#userprofile-department').val('');
		    }
		 }"),
		],
	     'options'=>['class'=>'form-control'] 
	    ])->hint('กรุณาค้นด้วยเลขห้าหลัก หรือส่วนหนึ่งของชื่อแล้วคลิกเลือกรายการที่ปรากฏ หากไม่คลิกเลือกจะถือว่าเป็นการเพิ่มหน่วยงานใหม่');
	?>
	
    </div>
</div>


<div class="box box-solid box-success  " id="personalForm4" >
    <?php if(Yii::$app->keyStorage->get('frontend.domain')=='dpmcloud.org') { ?>
    <div class="box-header">
       Enroll key
    </div>
    <div class="box-body">
        <?php
        echo $form->field($mprofile,'enroll_key')->textInput(['placeholder'=>'กรอกถ้ามี']);
        ?>
    </div>
    <?php }else{ ?>
    <div class="box-header">
        อัพโหลดเอกสารสำคัญ <span style="color: #FF0000; background-color: #FFF;">*สำคัญมาก หากไม่มีท่านจะไม่สามารถใช้งานได้</span>
    </div>
    <div class="text-right" style="margin: 10px;">ดาวน์โหลดเอกสารรักษาความลับ เพื่อให้ผู้บริหารอนุมัติ <a href="<?php echo Yii::$app->keyStorage->get('frontend.domain')=='cascap.in.th' ? 'http://www1.cascap.in.th/v4cascap/central_docs/siteadmin20140210_01.pdf' : 'https://www.thaicarecloud.org/imgfile/agreement 20160503.pdf'; ?>" target="_blank" > Download</a></div>
    <div class="box-body">
	<?php
	echo $form->field($mprofile,'enroll_key')->textInput(['placeholder'=>'กรอกถ้ามี']);
	?>
	<label class="control-label" for="userprofile-citizenid_file">สำเนาบัตรประชาชน <span style="color: #999;">(สำหรับสมาชิคทุกประเภท โดยถ่ายภาพสำเนาบัตรประชาชนที่มีรหัส 13 หลัก ชัดเจนพร้อมทั้งลายเซ็นรับรองสำเนาถูกต้อง)</span></label>
	<?= $form->field($mprofile, 'citizenid_file')->fileInput()->label(false); ?>
	<label class="control-label" for="userprofile-secret_file">เอกสารรักษาความลับ <span style="color: #999;">(สำหรับผู้ดูแลระบบเท่านั้น)</span></label>
	<?= $form->field($mprofile, 'secret_file')->fileInput()->label(false); ?>
    </div>
    <?php }?>
</div>
</div>
</div>
    <div class="form-group text-center">
        <input type="submit" class="btn btn-success" id="saveFormButton" value="สมัครสมาชิก">
    </div>
<br>
<?php ActiveForm::end()?>
<?php
$this->registerJs(
    "	    $('body').tooltip('destroy');

            $('input:checkbox[name=\"UserProfile[status]\"]').on('click',function(){
                if($(this).is(':checked')){
                    $('#status-personal-form').show();  // checked
                } else {
                    $('#status-personal-form').hide();
		    $('#userprofile-status_personal').val('');
		    $('#userprofile-status_personal').trigger('change');
		}
            });

            $('#userprofile-status_personal').on('change',function(){
                var type = $(this).val();
                if(type==4){
		    $('#rowManager').show();
                    $('#rowStatusOther').hide();
                    // $('#rowDepartment').hide();
                }else if(type==11){
                    $('#rowStatusOther').show();
                    // $('#rowDepartment').hide();
		    $('#userprofile-status_manager').val('');
		    $('#userprofile-status_manager').trigger('change');
                    $('#rowManager').hide();
                }else{
                    $('#rowDepartment').show();
                    $('#rowStatusOther').hide();
		    $('#userprofile-status_manager').val('');
		    $('#userprofile-status_manager').trigger('change');
                    $('#rowManager').hide();
                }
            });
            $('#userprofile-status_manager').on('change',function(){
                var type = $(this).val();

                if(type==5){
                  $('#rowNation').show();
                  $('#rowProvince').hide();
                  $('#rowArea').hide();
                  $('#rowAmphur').hide();
                }else if(type==6){
                  $('#rowArea').show();
                  $('#rowNation').hide();
                  $('#rowProvince').hide();
                  $('#rowAmphur').hide();
                }
                else if(type==7){
                  $('#rowProvince').show();
                  $('#rowNation').hide();
                  $('#rowArea').hide();
                  $('#rowAmphur').hide();
                }else if(type==8){
                  $('#rowProvince').show();
                  $('#rowAmphur').show();
                  $('#rowNation').hide();
                  $('#rowArea').hide();
                }else{
                  $('#rowDepartment').show();
                  $('#rowArea').hide();
                  $('#rowNation').hide();
                  $('#rowProvince').hide();
                  $('#rowAmphur').hide();
                }
            });
	    
	    $('input:checkbox[name=\"UserProfile[volunteer_status]\"]').on('click',function(){
                if($(this).is(':checked')){
                    $('#volunteer-form').show();  // checked
                } else {
                    $('#volunteer-form').hide();
		    $('#userprofile-volunteer').val('');
		}
            });
    "
);
?>
