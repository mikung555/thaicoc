<?php
$this->title = Yii::t('frontend', 'สมัครสมาชิก ... ');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class='container'>
  <div class="col-md-7 col-md-offset-3">
      <div class="panel panel-default">
	<div class="panel-heading">
	    <h3 class="panel-title"><i class="glyphicon glyphicon-exclamation-sign"></i> คุณได้ลงทะเบียนเป็นสมาชิกเป็นที่เรียบร้อยแล้ว</h3>
	</div>
	<div class="panel-body">
	    <dl class="dl-horizontal">
		
		<dt>Username</dt>
		<dd class="text-left"><?= $signupData->username ?></dd>
		<dt>Password</dt>
		<dd class="text-left"><?= $password ?></dd>
		<br/>
		<dt></dt>
		<dd class="text-left"><a class='btn btn-primary' href='<?php 
		    echo \Yii::getAlias('@frontendUrl').'/user/sign-in/login'; 
		?>'>เข้าสู่ระบบ</a></dd>
	    </dl>
	</div>
      </div>
    
  </div>
</div>
