<?php
namespace frontend\modules\user\models;

use common\models\User;
use Symfony\Component\VarDumper\VarDumper;
use yii\base\Model;
use Yii;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $id;
    public $username;
    public $email;
    public $password;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username'],'required','message'=>'กรุณาระบุเลขบัตรประจำตัวประชาชน'],
            [['username'],'integer','message'=>'กรุณาระบตัวเลข 13 หลักขึ้นไป'],
	    ['username', \common\lib\sdii\validators\CitizenIdValidator::className()],
            ['username', 'filter', 'filter' => 'trim'],
            ['username', 'unique',
                'targetClass'=>'\common\models\User',
                'message' => Yii::t('frontend', 'เลขบัตรประชาชนซ้ำกรุณากรอกใหม่')
            ],
            //['username', 'string', 'min' => 2, 'max' => 255],
		    
            ['email', 'filter', 'filter' => 'trim'],
            // ['email', 'required'],
            // ['email', 'email'],
          //  ['email', 'unique',
          //      'targetClass'=> '\common\models\User',
          //      'message' => Yii::t('frontend', 'This email address has already been taken.')
          //  ],

            ['password', 'required'],
            ['password', 'string', 'min' => 6],
        ];
    }

    public function ssn($attribute) {
	// add custom validation
//	\yii\helpers\VarDumper::dump($this->$attribute);
//	exit();
	$this->addError($attribute, 'Custom Validation Error');
    }

    public function attributeLabels()
    {
        return [
            'id'=>Yii::t('frontend','id'),
            'username'=>Yii::t('frontend', 'เลขบัตรประชาชน'),
            'email'=>Yii::t('frontend', 'E-mail'),
            'password'=>Yii::t('frontend', 'รหัสผ่าน'),
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup($mprofile)
    {
	$model = User::find()->where('status_del = 1 AND username=:username', [':username'=>$this->username])->one();
	if($model){
	    Yii::$app->authManager->revokeAll($model->id);
	    
	    $model->email = $this->email;
	    $model->setPassword($this->password);
	    $model->status = 1;
	    $model->status_del = 0;
	    if($model->save()){
		$model->afterUpdateSignup($mprofile,$model->username);
		return $model;
	    } else {
		return null;
	    }
	} else {
	    if ($this->validate()) {

		$user = new User();
		$user->id = \common\lib\codeerror\helpers\GenMillisecTime::getMillisecTime();
		$user->username = $this->username;
		$user->email = $this->email;
		$user->setPassword($this->password);
		$user->save();
		$user->afterSignup($mprofile,$user->username);
		return $user;
	    }
	}
        

        return null;
    }
}
