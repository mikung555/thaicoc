<?php
namespace frontend\modules\user\models;

use common\models\UserAutoSignup;
use yii\base\Model;
use Yii;

/**
 * Signup form
 */
class Signupauto extends Model
{
    public $id;
    public $username;
    public $email;
    public $password;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username'],'required','message'=>'กรุณาระบุเลขบัตรประจำตัวประชาชน'],
            ['username', 'filter', 'filter' => 'trim'],
            ['username', 'unique',
                'targetClass'=>'\common\models\User',
                'message' => Yii::t('frontend', 'This username has already been taken.')
            ],
            //['username', 'string', 'min' => 2, 'max' => 255],

            ['email', 'filter', 'filter' => 'trim'],
            // ['email', 'required'],
            // ['email', 'email'],
            //  ['email', 'unique',
            //      'targetClass'=> '\common\models\User',
            //      'message' => Yii::t('frontend', 'This email address has already been taken.')
            //  ],

            ['password', 'required'],
        ];
    }

    public function ssn($attribute) {
        // add custom validation
        \yii\helpers\VarDumper::dump($this->$attribute);
        exit();
        $this->addError($attribute, 'Custom Validation Error');
    }

    public function attributeLabels()
    {
        return [
            'id'=>Yii::t('frontend','id'),
            'username'=>Yii::t('frontend', 'Username'),
            'email'=>Yii::t('frontend', 'E-mail'),
            'password'=>Yii::t('frontend', 'Password'),
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup($mprofile)
    {
        if ($this->validate()) {
            $user = new UserAutoSignup();
            $user->id = \common\lib\codeerror\helpers\GenMillisecTime::getMillisecTime();
            $user->username = $this->username;
            $user->email = $this->email;
            $user->setPassword($this->password);
            $user->save();
            $user->afterSignup($mprofile, $mprofile['cid']);
            return $user;
        }
        return null;
    }
}
