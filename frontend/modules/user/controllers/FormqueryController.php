<?php

namespace frontend\modules\user\controllers;

use backend\models\Ezform;
use backend\modules\ezforms\components\EzformQuery;
use common\commands\command\SendEmailCommand;
use common\models\User;
use common\models\UserProfile;
use frontend\models\Puser;
use frontend\modules\user\models\LoginForm;
use frontend\modules\user\models\PasswordResetRequestForm;
use frontend\modules\user\models\ResetPasswordForm;
use frontend\modules\user\models\SignupForm;
use Yii;
use yii\base\Exception;
use yii\base\InvalidParamException;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;
use yii\web\BadRequestHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;

use yii\helpers\Json;

class FormqueryController extends \yii\web\Controller
{
    public function actionGenamphur(){
        $param = $_POST["depdrop_parents"][0];
        $sql = "SELECT `AMPHUR_CODE` as id,`AMPHUR_NAME` as name FROM `const_amphur` WHERE `AMPHUR_CODE` LIKE '".$param."%' ";
        $data = Yii::$app->db->createCommand($sql)->queryAll();
        $array = array('id'=>'','name'=>'');
        $array = array_values($data);
        echo Json::encode(['output'=>$array]);
    }
    public function actionQuerys($q){
        
        $sql = "SELECT hcode AS `code`, CONCAT(COALESCE(`hcode`), ' : ', COALESCE(`name`), ' ต.', COALESCE(`tambon`), ' อ.', COALESCE(`amphur`), ' จ.', COALESCE(`province`)) AS `name` FROM `all_hospital_thai` WHERE `name` LIKE '%".$q."%' OR `hcode` LIKE '%".$q."%' LIMIT 0,10";
        Yii::$app->response->format = Response::FORMAT_JSON;
        $data = Yii::$app->db->createCommand($sql)->queryAll();

        $json = array();
        foreach($data as $value){
            $json[] = ['id'=>$value['code'],'label'=>$value["name"]];
        }
        return $json;

    }
    
    public function actionQuerys2($q){
        if(Yii::$app->keyStorage->get('frontend.domain')=='thaicarecloud.org'){
	    $sql = "SELECT medshop AS `code`, CONCAT(COALESCE(`medshop`), ' : ', COALESCE(`nameshop`)) AS `name` FROM `tbdata_1483603520065899000` WHERE `nameshop` LIKE '%".$q."%' OR `medshop` LIKE '%".$q."%' LIMIT 0,10";
	} else {
	    $sql = "SELECT hcode AS `code`, CONCAT(COALESCE(`hcode`), ' : ', COALESCE(`name`), ' ต.', COALESCE(`tambon`), ' อ.', COALESCE(`amphur`), ' จ.', COALESCE(`province`)) AS `name` FROM `all_hospital_thai` WHERE `name` LIKE '%".$q."%' OR `hcode` LIKE '%".$q."%' LIMIT 0,10";
	}
        
        Yii::$app->response->format = Response::FORMAT_JSON;
        $data = Yii::$app->db->createCommand($sql)->queryAll();

        $json = array();
        foreach($data as $value){
            $json[] = ['id'=>$value['code'],'label'=>$value["name"]];
        }
        return $json;

    }
    
    public function actionQuerys3($q){
        if(Yii::$app->keyStorage->get('frontend.domain')=='thaicarecloud.org' || Yii::$app->keyStorage->get('frontend.domain')=='yii2-starter-kit.dev'){
	    $sql = "SELECT orgcode AS `code`, CONCAT(COALESCE(`orgcode`), ' : ', COALESCE(`name`), ' จ.', COALESCE(`province`)) AS `name` FROM `all_hos_org` WHERE `name` LIKE '%".$q."%' OR `orgcode` LIKE '%".$q."%' LIMIT 0,10";
	} else {
	    $sql = "SELECT hcode AS `code`, CONCAT(COALESCE(`hcode`), ' : ', COALESCE(`name`), ' ต.', COALESCE(`tambon`), ' อ.', COALESCE(`amphur`), ' จ.', COALESCE(`province`)) AS `name` FROM `all_hospital_thai` WHERE `name` LIKE '%".$q."%' OR `hcode` LIKE '%".$q."%' LIMIT 0,10";
	}
        
        Yii::$app->response->format = Response::FORMAT_JSON;
        $data = Yii::$app->db->createCommand($sql)->queryAll();

        $json = array();
        foreach($data as $value){
            $json[] = ['id'=>$value['code'],'label'=>$value["name"]];
        }
        return $json;

    }
    
    public function actionAddmorehospital(){
       $sql = "SELECT * FROM `const_hospital` WHERE `code` LIKE '%A%'";
       $count = Yii::$app->db->createCommand($sql)->query()->count();
       $count++;
       $code = 'A'.str_pad($count,'4','0',STR_PAD_LEFT);
       $sqlInsert = "INSERT INTO `const_hospital`(`code`,`name`) VALUES('".$code."','".$_POST["nameHospital"]."')";
       $queryInsert = Yii::$app->db->createCommand($sqlInsert)->execute();
    }
}
