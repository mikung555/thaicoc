<?php

namespace frontend\modules\user\controllers;

use backend\models\Ezform;
use backend\modules\ezforms\components\EzformQuery;
use common\commands\command\SendEmailCommand;
use common\models\User;
use common\models\UserProfile;
use frontend\models\Puser;
use frontend\modules\user\models\LoginForm;
use frontend\modules\user\models\PasswordResetRequestForm;
use frontend\modules\user\models\ResetPasswordForm;
use frontend\modules\user\models\Signupauto;
use frontend\modules\user\models\SignupForm;
use Yii;
use yii\base\Exception;
use yii\base\InvalidParamException;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;
use yii\web\BadRequestHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;
use yii\web\UploadedFile;

class SignInController extends \yii\web\Controller
{

    public function actions()
    {
        return [
            'oauth' => [
                'class' => 'yii\authclient\AuthAction',
                'successCallback' => [$this, 'successOAuthCallback']
            ]
        ];
    }

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['signup', 'login', 'checklogin', 'logindetail','request-password-reset', 'reset-password', 'oauth', 'a'],
                        'allow' => true,
                        'roles' => ['?']
                    ],
                    [
                        'actions' => ['signup', 'request-password-reset', 'reset-password', 'oauth'],
                        'allow' => false,
                        'roles' => ['@'],
                        'denyCallback' => function () {
                            return Yii::$app->controller->redirect(['/user/default/profile']);
                        }
                    ],
                    [
                        'actions' => ['logout', 'a'],
                        'allow' => true,
                        'roles' => ['@'],
                    ]
                ]
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post']
                ]
            ]
        ];
    }

    public static function tis620_to_utf8($text) {
        $utf8 = "";
        for ($i = 0; $i < strlen($text); $i++) {
            $a = substr($text, $i, 1);
            $val = ord($a);

            if ($val < 0x80) {
                $utf8 .= $a;
            } elseif ((0xA1 <= $val && $val < 0xDA) || (0xDF <= $val && $val <= 0xFB)) {
                $unicode = 0x0E00+$val-0xA0; $utf8 .= chr(0xE0 | ($unicode >> 12));
                $utf8 .= chr(0x80 | (($unicode >> 6) & 0x3F));
                $utf8 .= chr(0x80 | ($unicode & 0x3F));
            }
        }
        return $utf8;
    }
    
    public function actionChecklogin()
    {
        $model = new LoginForm();
        
        $data=Yii::$app->request->get();
        $model->identity = $data['username'];
        $model->password = $data['passwd'];
        if ($model->login()) {
            echo "OK";
        }else{
            echo "NOT OK";
        }
        exit;
    }
    public function actionLogindetail()
    {
        $model = new LoginForm();
        
        $data=Yii::$app->request->get();
        $model->identity = $data['username'];
        $model->password = $data['passwd'];
        if ($model->login()) {
            $res = Yii::$app->db->createCommand("select * from user left join user_profile on user.id=user_profile.user_id where user.username='".$data['username']."';")->queryOne();
            echo json_encode($res);
        }else{
            echo "Login failed";
        }
        
        exit;
    }    
    public function actionLogin()
    {
        $model = new LoginForm();
        /*
        if (Yii::$app->request->isAjax) {
            $model->load($_POST);
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }
        */
        /*
        if(Yii::$app->request->post()) {
            VarDumper::dump('test', 10, true);
            Yii::$app->end();
        }
        */
       
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            //return $this->goBack();
            $profile = Yii::$app->user->identity->userProfile;
            
            $profile->sitecode = Yii::$app->user->identity->userProfile->department;
            if (!$profile->save()) {
                VarDumper::dump($model->getErrors(),10,true);
            }
            
            
            setcookie("CloudUserID", Yii::$app->user->identity->userProfile->user->username, time()+180000, "/", Yii::$app->keyStorage->get('frontend.domain'), false);
            
            
            $TCC['username']=Yii::$app->request->post('LoginForm')['identity'];
            $TCC['password']=Yii::$app->request->post('LoginForm')['password'];
            $TCC['email']=Yii::$app->user->identity->userProfile->email;
            $TCC['fname']=Yii::$app->user->identity->userProfile->firstname;
            $TCC['lname']=Yii::$app->user->identity->userProfile->lastname;
            $TCC['cid']=Yii::$app->user->identity->userProfile->cid;
            $TCC['mobile']=Yii::$app->user->identity->userProfile->telephone;
            $TCC['office']=Yii::$app->user->identity->userProfile->sitecode;
            $addr = Yii::$app->db->createCommand("Select * from all_hospital_thai where code5=:office",[':office'=>$TCC['office']])->queryOne();
            $TCC['address']=$addr['name'];
            $TCC['tambon']=$addr['provincecode'].$addr['amphurcode'].$addr['tamboncode'];
            $TCC['amphur']=$addr['provincecode'].$addr['amphurcode']."00";
            $TCC['changwat']=$addr['provincecode']."0000";
            
            setcookie("CloudUser", base64_encode(json_encode($TCC)), time()+180000, "/", Yii::$app->keyStorage->get('frontend.domain'), false);
            
            if (Yii::$app->user->can('manager')) {
                $this->redirect(Yii::getAlias('@backendUrl')); //return $this->redirect("/");
            }else{
                $this->redirect(Yii::getAlias('@backendUrl')); //return $this->redirect("/");
            }
        } else {
            //check from CASCAP Tools Database
            if(Yii::$app->request->post()){
                $loginForm = Yii::$app->request->post('LoginForm');
                //identity -> frontend
                //username -> backtend
                //เช็คข้อมูลที่เดิม ว่ามีข้อมูลหรือไม่
                $resx = Yii::$app->db->createCommand("SELECT `id` FROM user WHERE username = '".$loginForm['identity']."';")->queryOne();
                if($resx['id']) {
                    return $this->render('login', [
                        'model' => $model
                    ]);
                }else{
                    $resx = Yii::$app->db->createCommand("SELECT `user_id` FROM user_profile WHERE cid = '".$loginForm['identity']."';")->queryOne();
                    if($resx['user_id']) {
                        return $this->render('login', [
                            'model' => $model
                        ]);
                    }
                }
                $res = Yii::$app->dbcascap->createCommand("SELECT `pid`, username, passwords, `name`, `surname`, cid, OrganizationName, `Email`, `Phone` FROM puser WHERE username = '".$loginForm['identity']."';");
                //VarDumper::dump($res->rawSql, 10, true);
                //Yii::$app->end();
                if($res->query()->count()){
                    $res = $res->queryOne();

                    if($loginForm['password'] == $res['passwords']){
                        //VarDumper::dump('pass', 10, true);
                        //Yii::$app->end();
                        $signup = new Signupauto();
                        $signup->username = $res['username'];
                        $signup->password = $res['passwords'];
                        $signup->email = $res['Email'];
                        //
                        $mprofile = new UserProfile();
                        $mprofile->sitecode = $res['OrganizationName'];
                        $mprofile->firstname = $this->tis620_to_utf8($res['name']);
                        $mprofile->lastname = $this->tis620_to_utf8($res['surname']);
                        $mprofile->email = $res['Email'];

                        $resx = Yii::$app->dbcascap->createCommand("select nologin from puser_priv WHERE pid = '".$res['pid']."';");
                        if($resx->query()->count()) {
                            $resx = $resx->queryOne();
                            //ยัง login ไม่ได้
                            if($resx['nologin']==1){
                                $mprofile->status = 0; // ให้เป็นบุคลากรทั่วไป
                            }else if($resx['nologin']==0 || $resx['nologin'] == ""){
                                $mprofile->status = 1; // ให้เป็นบุคลากร
                            }
                        }else {
                            $mprofile->status = 0; // ให้เป็นบุคลากรทั่วไป
                        }

                        $mprofile->status_personal = 3; //ผู้ให้บริการด้านการแพทย์และสาธารณสุข
                        $mprofile->locale = Yii::$app->language;
                        $mprofile->nation = 0;
                        $mprofile->cid = $res['cid'];
                        $mprofile->telephone = $res['Phone'];
                        $mprofile->department = $res['OrganizationName'];

                        //VarDumper::dump($mprofile, 10, true);
                        //Yii::$app->end();
                        $signup->signup($mprofile);

                        $model = new LoginForm();
                        if ($model->load(Yii::$app->request->post()) && $model->login()) {
                            setcookie("CloudUserID", Yii::$app->user->identity->userProfile->user->username, time()+180000, "/", Yii::$app->keyStorage->get('frontend.domain'), false);
                            return $this->goBack();
                        }
                    }
                }
                //Yii::$app->end();
            }

            return $this->render('login', [
                'model' => $model
            ]);
        }
    }

    public function actionA(){
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', 0);
        $res = \Yii::$app->dbcascap->createCommand("SELECT `pid`, username, passwords, `name`, `surname`, cid, OrganizationName, `Email`, `Phone` FROM puser WHERE title = 'surgery';")->queryAll();
        //VarDumper::dump($res, 10, true);
        //Yii::$app->end();

        foreach ($res as $val) {
            //เช็คข้อมูลที่เดิม ว่ามีข้อมูลหรือไม่
            $resx = Yii::$app->db->createCommand("SELECT `id` FROM user WHERE username = '".$val['username']."';")->queryOne();
            $resy = Yii::$app->db->createCommand("SELECT `user_id` FROM user_profile WHERE cid = '".$val['username']."';")->queryOne();
            if(!$resx['id'] && !$resy['user_id']) {
                $signup = new Signupauto();
                $signup->username = $val['username'];
                $signup->password = $val['passwords'];
                $signup->email = $val['Email'];
                //
                $mprofile = new UserProfile();
                $mprofile->sitecode = $val['OrganizationName'];
                $mprofile->firstname = $this->tis620_to_utf8($val['name']);
                $mprofile->lastname = $this->tis620_to_utf8($val['surname']);
                $mprofile->email = $val['Email'];

                $resx = \Yii::$app->dbcascap->createCommand("select nologin from puser_priv WHERE pid = '" . $val['pid'] . "';");
                if ($resx->query()->count()) {
                    $resx = $resx->queryOne();
                    //ยัง login ไม่ได้
                    if ($resx['nologin'] == 1) {
                        $mprofile->status = 0; // ให้เป็นบุคลากรทั่วไป
                    } else if ($resx['nologin'] == 0 || $resx['nologin'] == "") {
                        $mprofile->status = 1; // ให้เป็นบุคลากร
                    }
                } else {
                    $mprofile->status = 0; // ให้เป็นบุคลากรทั่วไป
                }

                $mprofile->status_personal = 3; //ผู้ให้บริการด้านการแพทย์และสาธารณสุข
                $mprofile->locale = \Yii::$app->language;
                $mprofile->nation = 0;
                $mprofile->cid = $val['cid'];
                $mprofile->telephone = $val['Phone'];
                $mprofile->department = $val['OrganizationName'];

                //VarDumper::dump($mprofile, 10, true);
                //Yii::$app->end();
                $signup->signup($mprofile);
            }else{


            }

        }

        //Yii::$app->end();
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();
        setcookie("CloudUserID", Yii::$app->user->identity->userProfile->user->username, time()-1, "/", Yii::$app->keyStorage->get('frontend.domain'), false);
        setcookie("CloudUser", Yii::$app->user->identity->userProfile->user->username, time()-1, "/", Yii::$app->keyStorage->get('frontend.domain'), false);
        return $this->redirect("/");//$this->goHome();
    }
    public function actionQuerys($q){
        $sql = "SELECT `code`,`name` FROM `const_hospital` WHERE `name` LIKE '%".$q."%' OR `code` LIKE '%".$q."%' LIMIT 1,100";
        Yii::$app->response->format = Response::FORMAT_JSON;
        $data = Yii::$app->db->createCommand($sql)->queryAll();
        $i = 0;
        $json = "";
        foreach($data as $value){
            $json["items"][$i] = ['id'=>$value['code'],'text'=>$value["code"]." : ".$value["name"]];
            $i++;
        }
        return $json;
    }

    public function actionSignup()
    {
        $signup = new SignupForm();
        $mprofile = new UserProfile();
	
	$mprofile->status = 0;
	$mprofile->inout = 0;
        $dataProvince = EzformQuery::getProvince();
        if($signup->load(Yii::$app->request->post()) && $mprofile->load(Yii::$app->request->post())){
	
	    
	    $sql = "SELECT * FROM `user` WHERE `email` = :email ";
	    $checkmail = Yii::$app->db->createCommand($sql, [':email'=>$mprofile->email])->query()->count();
	    
	    if($checkmail>0){
		Yii::$app->session->setFlash('alert', [
		    'options' => ['class' => 'alert-danger'],
		    'body' => Yii::t('backend', 'อีเมลซ้ำ กรุณาระบุอีเมลใหม่', [], $mprofile->locale)
	      ]);
		return $this->render('signup', [
		    'dataProvince'=>$dataProvince,
		    'signup'=>$signup,
		    'mprofile'=>$mprofile,
		]);
	    }
	    
	    if($mprofile->volunteer_status==1 && $mprofile->volunteer==1){
		  $mprofile->sitecode = 'อาสาสมัคร อสค';
		  $mprofile->inout=1;
		  $_POST["UserProfile"]["inout"] = $mprofile->inout;
	    }
	      
//	  if($mprofile->inout==2 && $mprofile->department==''){
//	      Yii::$app->session->setFlash('alert', [
//		    'options' => ['class' => 'alert-danger'],
//		    'body' => Yii::t('backend', 'กรุณาระบุหน่วยบริการของท่าน', [], $mprofile->locale)
//	      ]);
//		return $this->render('signup', [
//		    'dataProvince'=>$dataProvince,
//		    'signup'=>$signup,
//		    'mprofile'=>$mprofile,
//		]);
//	  }
	  
          if(strlen($mprofile->sitecode)<8){
            Yii::$app->session->setFlash('alert', [
                'options' => ['class' => 'alert-danger'],
                'body' => Yii::t('backend', 'กรุณาระบุหน่วยบริการของท่าน', [], $mprofile->locale)
            ]);
            return $this->render('signup', [
                'dataProvince'=>$dataProvince,
                'signup'=>$signup,
                'mprofile'=>$mprofile,
            ]);
          }else{
	      $name = $mprofile->sitecode;
	      if($mprofile->inout==1 && $mprofile->department==''){
		    $sql = "SELECT * FROM `all_hospital_thai` WHERE `name` = :name ";
		    $dataHos = Yii::$app->db->createCommand($sql, [':name'=>$name])->queryOne();
		    if($dataHos){
			$mprofile->department = $dataHos['hcode'];
			$_POST["UserProfile"]["department"] = $dataHos['hcode'];
		    } else {
			$sql = "SELECT * FROM `all_hospital_thai` WHERE `hcode` LIKE 'C%'";
			$count = Yii::$app->db->createCommand($sql)->query()->count();
			$count++;
			$code = 'C'.str_pad($count,'4','0',STR_PAD_LEFT);
			
			$queryInsert = Yii::$app->db->createCommand()
				->insert('all_hospital_thai', [
				    'hcode' => $code,
				    'code2' => 'demo',
				    'name' => $name,
				])
				->execute();
			
			$mprofile->department = $code;
			$_POST["UserProfile"]["department"] = $code;
		    }
	      } elseif($mprofile->inout==0 && $mprofile->department==''){
		  $sql = "SELECT * FROM `all_hospital_thai` WHERE `name` = :name ";
		    $dataHos = Yii::$app->db->createCommand($sql, [':name'=>$name])->queryOne();
		    if($dataHos){
			$mprofile->department = $dataHos['hcode'];
			$_POST["UserProfile"]["department"] = $dataHos['hcode'];
		    } else {
			$sql = "SELECT * FROM `all_hospital_thai` WHERE `hcode` LIKE 'B%'";
			$count = Yii::$app->db->createCommand($sql)->query()->count();
			$count++;
			$code = 'B'.str_pad($count,'5','0',STR_PAD_LEFT);
			
			$queryInsert = Yii::$app->db->createCommand()
				->insert('all_hospital_thai', [
				    'hcode' => $code,
				    'code2' => 'demo',
				    'name' => $name,
				])
				->execute();
			
			$mprofile->department = $code;
			$_POST["UserProfile"]["department"] = $code;
		    }
	      } elseif($mprofile->inout==2 && $mprofile->department!=''){
		  if(Yii::$app->keyStorage->get('frontend.domain')=='thaicarecloud.org'){
		    $sql = "SELECT * FROM `all_hospital_thai` WHERE `hcode` = :hcode ";
		    $dataHos = Yii::$app->db->createCommand($sql, [':hcode'=>$mprofile->department])->queryOne();
		    if($dataHos){
//			$mprofile->department = $dataHos['hcode'];
//			$_POST["UserProfile"]["department"] = $dataHos['hcode'];
		    } else {
			
			$sql = "SELECT * FROM `tbdata_1483603520065899000` WHERE `medshop` = :hcode";
			$dataDrug = Yii::$app->db->createCommand($sql, [':hcode'=>$mprofile->department])->queryOne();
			if($dataDrug){
			    $queryInsert = Yii::$app->db->createCommand()
				    ->insert('all_hospital_thai', [
					'hcode' => $dataDrug['medshop'],
					'code' => $dataDrug['id'],
					'code2' => 'demo',
					'code4' => '100',
					'name5' => 'ร้านยา',
					'name' => $dataDrug['nameshop'],
					'code7' => '1',
					'name8' => 'เปิดดำเนินการ',
					'provincecode' => $dataDrug['var13_province'],
					'amphurcode' => $dataDrug['var13_amphur'],
					'tamboncode' => $dataDrug['var13_tumbon'],
					'moo' => $dataDrug['b2'],
					'address' => $dataDrug['b1'],
					'postcode' => $dataDrug['postcode'],
					'tel' => $dataDrug['phone'],
				    ])
				    ->execute();

			}
			
		    }
		}
	    } elseif($mprofile->inout==2 && $mprofile->department==''){
		$sql = "SELECT * FROM `all_hospital_thai` WHERE `name` = :name ";
		$dataHos = Yii::$app->db->createCommand($sql, [':name'=>$name])->queryOne();
		if($dataHos){
		    $mprofile->department = $dataHos['hcode'];
		    $_POST["UserProfile"]["department"] = $dataHos['hcode'];
		} else {
		    $sql = "SELECT * FROM `all_hospital_thai` WHERE `hcode` LIKE 'D%'";
		    $count = Yii::$app->db->createCommand($sql)->query()->count();
		    $count++;
		    $code = 'D'.str_pad($count,'4','0',STR_PAD_LEFT);
		    
		    $queryInsert = Yii::$app->db->createCommand()
			    ->insert('all_hospital_thai', [
				'hcode' => $code,
				'code2' => 'demo',
				'code4' => '100',
				'name5' => 'ร้านยา',
				'name' => $name,
				'code7' => '1',
				'name8' => 'เปิดดำเนินการ',
			    ])
			    ->execute();

		    $mprofile->department = $code;
		    $_POST["UserProfile"]["department"] = $code;
		}
		    
	    } elseif($mprofile->inout==3 && $mprofile->department!=''){
		  if(Yii::$app->keyStorage->get('frontend.domain') == 'thaicarecloud.org' || Yii::$app->keyStorage->get('frontend.domain')=='yii2-starter-kit.dev'){
		    $sql = "SELECT * FROM `all_hospital_thai` WHERE `hcode` = :hcode ";
		    $dataHos = Yii::$app->db->createCommand($sql, [':hcode'=>$mprofile->department])->queryOne();
		    if($dataHos){
//			$mprofile->department = $dataHos['hcode'];
//			$_POST["UserProfile"]["department"] = $dataHos['hcode'];
		    } else {
			
			$sql = "SELECT * FROM `all_hos_org` WHERE `orgcode` = :hcode";
			$dataDrug = Yii::$app->db->createCommand($sql, [':hcode'=>$mprofile->department])->queryOne();
			if($dataDrug){
                            $tambon = substr($dataDrug['orgcode'], 1);
                            $amphur = substr($tambon, 2, 4);
                            $province = substr($tambon, 0, 2);
			    $queryInsert = Yii::$app->db->createCommand()
				    ->insert('all_hospital_thai', [
					'hcode' => $dataDrug['orgcode'],
					'code' => $dataDrug['orgcode'],
					'code2' => 'demo',
					'code4' => '200',
					'name5' => 'องค์กรปกครองส่วนท้องถิน',
					'name' => $dataDrug['name'],
					'code7' => '1',
					'name8' => 'เปิดดำเนินการ',
					'provincecode' => $province,
					'amphurcode' => $amphur,
					'tamboncode' => $tambon,
				    ])
				    ->execute();

			}
			
		    }
		}
	    } elseif($mprofile->inout==3 && $mprofile->department==''){
		$sql = "SELECT * FROM `all_hospital_thai` WHERE `name` = :name ";
		$dataHos = Yii::$app->db->createCommand($sql, [':name'=>$name])->queryOne();
		if($dataHos){
		    $mprofile->department = $dataHos['hcode'];
		    $_POST["UserProfile"]["department"] = $dataHos['hcode'];
		} else {
		    $sql = "SELECT * FROM `all_hospital_thai` WHERE `hcode` LIKE 'E%'";
		    $count = Yii::$app->db->createCommand($sql)->query()->count();
		    $count++;
		    $code = 'E'.str_pad($count,'7','0',STR_PAD_LEFT);
		    
		    $queryInsert = Yii::$app->db->createCommand()
			    ->insert('all_hospital_thai', [
				'hcode' => $code,
				'code2' => 'demo',
				'code4' => '200',
				'name5' => 'องค์กรปกครองส่วนท้องถิน',
				'name' => $name,
				'code7' => '1',
				'name8' => 'เปิดดำเนินการ',
			    ])
			    ->execute();

		    $mprofile->department = $code;
		    $_POST["UserProfile"]["department"] = $code;
		}
		    
	    }
	      
            //$signup->password = $_POST["SignupForm"]["password"];
            $mprofile->sitecode = $_POST["UserProfile"]["department"].' '.$mprofile->sitecode;
	    $mprofile->inout = $_POST["UserProfile"]["inout"];
	    $mprofile->enroll_key = $_POST["UserProfile"]["enroll_key"];
	    $mprofile->volunteer = $_POST["UserProfile"]["volunteer"];
	    $mprofile->volunteer_status = $_POST["UserProfile"]["volunteer_status"];
            $signup->email = $mprofile->email;

	    $mprofile->citizenid_file = UploadedFile::getInstance($mprofile, 'citizenid_file');
	    $mprofile->secret_file = UploadedFile::getInstance($mprofile, 'secret_file');
			
	    if ($mprofile->secret_file !== null) {
		$fileItems = $_FILES['UserProfile']['name']['secret_file'];
		$fileType = $_FILES['UserProfile']['type']['secret_file'];
		
		
	    
		$lname = 'jpg';
		$fileArr = explode('/', $fileType);
		if (isset($fileArr[1])) {
		    $lname = $fileArr[1];
		}
		$idName = '';
		if ($_SERVER["REMOTE_ADDR"] == '::1' || $_SERVER["REMOTE_ADDR"] == '127.0.0.1') {
			$idName = 'mycom';
		} else {
			$idName = $_SERVER["REMOTE_ADDR"];
		}
		//date("YmdHis")
		$nowFileName = 'secret_'.date('YmdHis').'.'.$lname;
		$fullPath = Yii::$app->basePath . '/../storage/web/source/' . $nowFileName;

		$mprofile->secret_file->saveAs($fullPath);
		$mprofile->secret_file = $nowFileName;
	    }else {
		$mprofile->secret_file = '';
	    }
	    
	    if ($mprofile->citizenid_file !== null) {
		$fileItems = $_FILES['UserProfile']['name']['citizenid_file'];
		$fileType = $_FILES['UserProfile']['type']['citizenid_file'];
		
		$lname = 'jpg';
		$fileArr = explode('/', $fileType);
		if (isset($fileArr[1])) {
		    $lname = $fileArr[1];
		}
		$idName = '';
		if ($_SERVER["REMOTE_ADDR"] == '::1' || $_SERVER["REMOTE_ADDR"] == '127.0.0.1') {
			$idName = 'mycom';
		} else {
			$idName = $_SERVER["REMOTE_ADDR"];
		}
		//date("YmdHis")
		$nowFileName = 'citizenid_'.date('YmdHis').'.'.$lname;
		$fullPath = Yii::$app->basePath . '/../storage/web/source/' . $nowFileName;

		$mprofile->citizenid_file->saveAs($fullPath);
		$mprofile->citizenid_file = $nowFileName;
	    }else {
		$mprofile->citizenid_file = '';
	    }
	    $password = $signup->password;
            if($signup->signup($mprofile)){
		
                if (Yii::$app->keyStorage->get('signup.sendmail') != '0') {
                    $sendMail = Yii::$app->commandBus->handle(new \common\commands\command\SendEmailCommand([
                        'from' => [Yii::$app->params['adminEmail'] => Yii::$app->keyStorage->get('sender')],
                        'to' => $mprofile->email,
                        'subject' => 'ยินดีตอนรับสู่ระบบ',
                        'view' => 'register',
                        'params' => ['user' => $mprofile, 'username'=>$signup->username, 'password'=>$password]
                    ]));

                    $sendMailCC = Yii::$app->commandBus->handle(new \common\commands\command\SendEmailCommand([
                        'from' => [Yii::$app->params['adminEmail'] => Yii::$app->keyStorage->get('sender')],
                        'to' => \backend\modules\ezforms\components\EzformFunc::getEmailSite(),
                        'subject' => 'New user of '.Yii::$app->keyStorage->get('sender'),
                        'view' => 'sign_up',
                        'params' => ['user' => $mprofile, 'username'=>$signup->username]
                    ]));
                }
//		Yii::$app->mailer->compose('@app/mail/layouts/register',[
//		    'user' => $mprofile, 'username'=>$signup->username, 'password'=>$password
//		])
//		->setFrom([Yii::$app->params['adminEmail'] => Yii::$app->name])
//		->setTo($mprofile->email)
//		->setCc(['c.tawarungruang@gmail.com', 'bermkubpom@gmail.com'])	
//		->setSubject('ยินดีตอนรับสู่ระบบ')
//		->send();
		Yii::$app->session->setFlash('alert', [
		    'options' => ['class' => 'alert-success'],
		    'body' => Yii::t('backend', 'คุณได้สมัครสมาชิกเป็นที่เรียบร้อย', [], $mprofile->locale)
		]);
		return $this->render('finish',['signupData'=>$signup, 'mprofile'=>$mprofile, 'password'=>$_POST["SignupForm"]["password"]]);

                
            }else {
                Yii::$app->session->setFlash('alert', [
                    'options' => ['class' => 'alert-danger'],
                    'body' => Yii::t('backend', 'เลขบัตรประชาชนซ้ำกรุณากรอกใหม่', [], $mprofile->locale)
                ]);
                return $this->render('signup', [
		    'dataProvince'=>$dataProvince,
		    'signup'=>$signup,
		    'mprofile'=>$mprofile,
		]);
            }
          }
        }else{
            return $this->render('signup', [
                'dataProvince'=>$dataProvince,
                'signup'=>$signup,
                'mprofile'=>$mprofile,
            ]);
        }

    }
    public function actionGenamphur(){
        $param = $_POST["depdrop_parents"][0];
        $sql = "SELECT `AMPHUR_CODE` as id,`AMPHUR_NAME` as name FROM `const_amphur` WHERE `AMPHUR_CODE` LIKE '".$param."%' ";
        $data = Yii::$app->db->createCommand($sql)->queryAll();
        $array = array('id'=>'','name'=>'');
        $array = array_values($data);
        echo Json::encode(['output'=>$array]);
    }
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->getSession()->setFlash('alert', [
                    'body'=>Yii::t('frontend', 'Check your email for further instructions.'),
                    'options'=>['class'=>'alert-success']
                ]);

                return $this->goHome();//$this->goHome();
            } else {
                Yii::$app->getSession()->setFlash('alert', [
                    'body'=>Yii::t('frontend', 'Sorry, we are unable to reset password for email provided.'),
                    'options'=>['class'=>'alert-danger']
                ]);
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException('ท่านได้เปลี่ยนรหัสผ่านไปแล้ว หากท่านจำรหัสผ่านไม่ได้กรุณากดรีเซ็ตรหัสผ่านใหม่');
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->getSession()->setFlash('alert', [
                'body'=> Yii::t('frontend', 'New password was saved.'),
                'options'=>['class'=>'alert-success']
            ]);
            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }

    /**
     * @param $client \yii\authclient\BaseClient
     * @return bool
     * @throws Exception
     */
    public function successOAuthCallback($client)
    {
        // use BaseClient::normalizeUserAttributeMap to provide consistency for user attribute`s names
        $attributes = $client->getUserAttributes();
        $user = User::find()->where([
                'oauth_client'=>$client->getName(),
                'oauth_client_user_id'=>ArrayHelper::getValue($attributes, 'id')
            ])
            ->one();
        if (!$user) {
            $user = new User();
            $user->scenario = 'oauth_create';
            $user->username = ArrayHelper::getValue($attributes, 'login');
            $user->email = ArrayHelper::getValue($attributes, 'email');
            $user->oauth_client = $client->getName();
            $user->oauth_client_user_id = ArrayHelper::getValue($attributes, 'id');
            $password = Yii::$app->security->generateRandomString(8);
            $user->setPassword($password);
            if ($user->save()) {
                $user->afterSignup();
                $sentSuccess = Yii::$app->commandBus->handle(new SendEmailCommand([
                    'view' => 'oauth_welcome',
                    'params' => ['user'=>$user, 'password'=>$password],
                    'subject' => Yii::t('frontend', '{app-name} | Your login information', ['app-name'=>Yii::$app->name]),
                    'to' => $user->email
                ]));
                if ($sentSuccess) {
                    Yii::$app->session->setFlash(
                        'alert',
                        [
                            'options'=>['class'=>'alert-success'],
                            'body'=>Yii::t('frontend', 'Welcome to {app-name}. Email with your login information was sent to your email.', [
                                'app-name'=>Yii::$app->name
                            ])
                        ]
                    );
                }

            } else {
                // We already have a user with this email. Do what you want in such case
                if (User::find()->where(['email'=>$user->email])->count()) {
                    Yii::$app->session->setFlash(
                        'alert',
                        [
                            'options'=>['class'=>'alert-danger'],
                            'body'=>Yii::t('frontend', 'We already have a user with email {email}', [
                                'email'=>$user->email
                            ])
                        ]
                    );
                } else {
                    Yii::$app->session->setFlash(
                        'alert',
                        [
                            'options'=>['class'=>'alert-danger'],
                            'body'=>Yii::t('frontend', 'Error while oauth process.')
                        ]
                    );
                }

            };
        }
        if (Yii::$app->user->login($user, 3600 * 24 * 30)) {
            return true;
        } else {
            throw new Exception('OAuth error');
        }
    }
}
