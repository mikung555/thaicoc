<?php

namespace frontend\modules\reportpdf\controllers;
use yii;
use yii\web\Controller;
use kartik\mpdf\Pdf;
use yii\web\View;
use yii\web\NotFoundHttpException;

class DefaultController extends Controller
{
    public function actionIndex()
    {
        $total = 0;
        $CTMRI = 0;
        $Tsceeing = 0;
        $randow = 0 ;
        $drugs = 0;
        
        $xsourcex = Yii::$app->user->identity->userProfile->sitecode; 
        $firstpatiententered = Yii::$app->db->createCommand("Select f1v47 as `date` from tbdata_1474424530033118700 where rstat<>3 and xsourcex = '13777' and f1v45_x='001'")->queryOne();
        $firstpatienttreatment = Yii::$app->db->createCommand("Select f3v1  as `date` from tbdata_1474424608054538800 where rstat<>3 and xsourcex = '13777' and f1v45_x='001'")->queryOne();
        $lastpatiententered = Yii::$app->db->createCommand("Select f1v47 as `date` from tbdata_1474424530033118700 where rstat<>3 and xsourcex = '13777' and f1_initial<>'' and f1_initial is not null ORDER by f1v45_x DESC limit 1")->queryOne();
        $lastpatienttreatment = Yii::$app->db->createCommand("Select f3v1  as `date` from tbdata_1474424608054538800 where rstat<>3 and xsourcex = '13777' ORDER by f1v45_x DESC limit 1")->queryOne();
        $lastpatientcompleted = Yii::$app->db->createCommand("Select f8v1  as `date` from tbdata_1474424789002483800 where rstat<>3 and xsourcex = '13777' ORDER by f1v45_x DESC limit 1")->queryOne();
        $closeout['date']='';
        
        $fristcase = Yii::$app->db->createCommand("Select max(f2v1) as data1 from tbdata_1474424574070259800  where rstat<>3 and xsourcex = '13777'
                                                   and MONTH(create_date) = MONTH(CURDATE())")->queryOne();
                                                                                     
        $total = Yii::$app->db->createCommand("Select count(hptcode) as Thptcode from tbdata_1474424530033118700 where rstat<>3 and xsourcex = '13777' and MONTH(create_date) = MONTH(CURDATE())   ")->queryOne();
       
        $Tsceeing = Yii::$app->db->createCommand("Select count(f7v2) as Tsceen from tbdata_1474424762000145000 where rstat<>3 and xsourcex = '13777' and MONTH(create_date) = MONTH(CURDATE()) ")->queryOne();     
        $CTMRI = \Yii::$app->db->createCommand("Select count(f7v2) as TCTMRI from tbdata_1474424762000145000 where rstat<>3 and xsourcex = '13777' and f7v2 = 1 and MONTH(create_date) = MONTH(CURDATE())  ")->queryOne();
        $Laboratory = \Yii::$app->db->createCommand("Select count(f7v2) as  Laboratory from tbdata_1474424762000145000 where rstat<>3 and xsourcex = '13777' and f7v2 = 2 and MONTH(create_date) = MONTH(CURDATE())  ")->queryOne();
        $Others = \Yii::$app->db->createCommand("Select count(f7v2) as  Others from tbdata_1474424762000145000 where rstat<>3 and xsourcex = '13777' and f7v2 = 3 and MONTH(create_date) = MONTH(CURDATE())  ")->queryOne();
        
        
        
        $randow = \Yii::$app->db->createCommand("Select count(random) as Trandom from tbdata_1474424574070259800 where  rstat<>3 and xsourcex = '13777' and MONTH(create_date) = MONTH(CURDATE()) ")->queryOne();
        $drugs = \Yii::$app->db->createCommand("select COUNT(chemo_drugs)as Tdrugs from   tbdata_1474424530033118700
                                                where rstat<>3 and chemo_drugs = 2  and xsourcex = '13777' and MONTH(create_date) = MONTH(CURDATE()) ")->queryOne();
       
        $drug2 = \Yii::$app->db->createCommand("select COUNT(chemo_drugs)as drugs2 from   tbdata_1474424530033118700 where rstat<>3 and chemo_drugs = 1 and xsourcex = '13777' and MONTH(create_date) = MONTH(CURDATE()) ")->queryOne();
        $sae = \Yii::$app->db->createCommand("SELECT COUNT(f5v3) as Tsae from tbdata_1474424683056884600  where rstat<>3 and f5v3 = 1 and xsourcex = '13777' and MONTH(create_date) = MONTH(CURDATE()) ")->queryOne();
        $Dropout = \Yii::$app->db->createCommand("Select count(f7v4e) as Tdropout from tbdata_1474424762000145000 where rstat<>3 and f7v4e = 1  and xsourcex = '13777' and  MONTH(create_date) = MONTH(CURDATE())")->queryOne();
        $Stop = \Yii::$app->db->createCommand("Select count(f7v4e) as TStop from tbdata_1474424762000145000 where rstat<>3 and f7v4e = 1  and xsourcex = '13777' and  MONTH(create_date) = MONTH(CURDATE()) ")->queryOne();
        $Loss = \Yii::$app->db->createCommand("Select count(f7v4i) as TLoss from tbdata_1474424762000145000 where rstat<>3 and f7v4i = 1  and xsourcex = '13777' and  MONTH(create_date) = MONTH(CURDATE()) ")->queryOne();
        $comple = \Yii::$app->db->createCommand("Select count(f7v3a) as Tcomple from tbdata_1474424762000145000 where rstat<>3 and f7v3a = 1 and xsourcex = '13777'  and  MONTH(create_date) = MONTH(CURDATE()) ")->queryOne();
        $drug1 = \Yii::$app->db->createCommand("Select count(chemo_drugs)as chemo_drugs from  tbdata_1474424530033118700 where rstat<>3 and xsourcex = '13777' and  MONTH(create_date) = MONTH(CURDATE()) ")->queryOne();
        /*..............................................*/
        
        $total2 = Yii::$app->db->createCommand("Select count(hptcode) as Thptcode2 from tbdata_1474424530033118700 where rstat<>3 and xsourcex = '13777' and f1_initial<>'' and f1_initial is not null  ")->queryOne();
        $Tsceeing2 = Yii::$app->db->createCommand("Select count(f7v2) as Tsceen2 from tbdata_1474424762000145000 where rstat<>3 and xsourcex = '13777'   ")->queryOne();
        $CTMRI2 = \Yii::$app->db->createCommand("Select count(f1ve4) as TCTMRI2 from tbdata_1474424530033118700 where rstat<>3 and xsourcex = '13777' and f1ve4 = 1   ")->queryOne();
        $Laboratory2 = \Yii::$app->db->createCommand("Select count(f7v2) as  Laboratory2 from tbdata_1474424762000145000 where rstat<>3 and xsourcex = '13777' and f7v2 = 2   ")->queryOne();
        $Others2 = \Yii::$app->db->createCommand("Select count(f7v2) as  Others2 from tbdata_1474424762000145000 where rstat<>3 and xsourcex = '13777' and f7v2 = 3  ")->queryOne();
        
        
        $randow2 = \Yii::$app->db->createCommand("Select count(random) as Trandom2 from tbdata_1474424574070259800 where  rstat<>3 and xsourcex = '13777'  ")->queryOne();
       
        $chemo_drugs2 = \Yii::$app->db->createCommand("select COUNT(chemo_drugs)as chemo_drugs2 from   tbdata_1474424530033118700
                                                where rstat<>3 and chemo_drugs = 2  and xsourcex = '13777' ")->queryOne();
       
        $chemo_drugs1 = \Yii::$app->db->createCommand("select COUNT(chemo_drugs)as chemo_drugs1 from   tbdata_1474424530033118700 where rstat<>3 and chemo_drugs = 1 and xsourcex = '13777'  ")->queryOne();
       
        $sae2 = \Yii::$app->db->createCommand("SELECT COUNT(f5v3) as Tsae2 from tbdata_1474424683056884600  where rstat<>3 and f5v3 = 1 and xsourcex = '13777'  ")->queryOne();
        $Dropout2 = \Yii::$app->db->createCommand("Select count(f7v4e) as Tdropout2 from tbdata_1474424762000145000 where rstat<>3 and f7v4e = 1  and xsourcex = '13777' ")->queryOne();
        $Stop2 = \Yii::$app->db->createCommand("Select count(f7v4e) as TStop2 from tbdata_1474424762000145000 where rstat<>3 and f7v4e = 1  and xsourcex = '13777'  ")->queryOne();
        $Loss2 = \Yii::$app->db->createCommand("Select count(f7v4i) as TLoss2 from tbdata_1474424762000145000 where rstat<>3 and f7v4i = 1  and xsourcex = '13777'  ")->queryOne();
        $comple2 = \Yii::$app->db->createCommand("Select count(f7v3a) as Tcomple2 from tbdata_1474424762000145000 where rstat<>3 and f7v3a = 1 and xsourcex = '13777'   ")->queryOne();
      
        $Treatment = \Yii::$app->db->createCommand("Select count(chemo_drugs)as Treatment from  tbdata_1474424530033118700 where rstat<>3 and xsourcex = '13777'  ")->queryOne();
        
//        return $this->renderPartial('index',['total'=>$total ,
//                                                   'Tsceeing'=>$Tsceeing,
//                                                   'CTMRI'=>$CTMRI,
//                                                   'randow'=>$randow,
//                                                   'drugs'=>$drugs,
//                                                   'sae'=>$sae,
//                                                   'drugs2'=>$drug2,
//                                                   'Dropout'=>$Dropout,
//                                                   'Loss'=>$Loss,
//                                                   'comple'=>$comple,
//                                                   'drug1'=>$drug1,
//                                                   'Laboratory'=>$Laboratory,
//                                                   'Others'=>$Others,
//                                                   
//            
//                                                   'total2'=>$total2 ,
//                                                   'Tsceeing2'=>$Tsceeing2,
//                                                   'CTMRI2'=>$CTMRI2,
//                                                   'randow2'=>$randow2,
//                                                   'chemo_drugs1'=>$chemo_drugs1,
//                                                   'sae2'=>$sae2,
//                                                   'chemo_drugs2'=>$chemo_drugs2,
//                                                   'Dropout2'=>$Dropout2,
//                                                   'Loss2'=>$Loss2,
//                                                   'comple2'=>$comple2,
//                                                   'Treatment'=>$Treatment,
//                                                   'Laboratory2'=>$Laboratory2,
//                                                   'Others2'=>$Others2
//            
//                                                   
//                
//                
//                ]);
//        
        $content = $this->renderPartial('index',['total'=>$total ,
                                                   'firstpatiententered'=>$firstpatiententered,
                                                   'firstpatienttreatment'=>$firstpatienttreatment,
                                                   'lastpatiententered'=>$lastpatiententered,
                                                   'lastpatienttreatment'=>$lastpatienttreatment,
                                                   'Tsceeing'=>$Tsceeing,
                                                   'CTMRI'=>$CTMRI,
                                                   'randow'=>$randow,
                                                   'drugs'=>$drugs,
                                                   'sae'=>$sae,
                                                   'drugs2'=>$drug2,
                                                   'Dropout'=>$Dropout,
                                                   'Stop'=>$Stop,
                                                   'Loss'=>$Loss,
                                                   'comple'=>$comple,
                                                   'drug1'=>$drug1,
                                                   'Laboratory'=>$Laboratory,
                                                   'Others'=>$Others,
                                                   
            
                                                   'total2'=>$total2 ,
                                                   'Tsceeing2'=>$Tsceeing2,
                                                   'CTMRI2'=>$CTMRI2,
                                                   'randow2'=>$randow2,
                                                   'chemo_drugs1'=>$chemo_drugs1,
                                                   'sae2'=>$sae2,
                                                   'chemo_drugs2'=>$chemo_drugs2,
                                                   'Dropout2'=>$Dropout2,
                                                   'Stop2'=>$Stop2,
                                                   'Loss2'=>$Loss2,
                                                   'comple2'=>$comple2,
                                                   'Treatment'=>$Treatment,
                                                   'Laboratory2'=>$Laboratory2,
                                                   'Others2'=>$Others2
            
                                                   
                
                
                ]);
        
        
        $pdf = new Pdf(['mode' => Pdf::MODE_UTF8,
                        'format' => Pdf::FORMAT_A4,
                        'orientation'=> Pdf::ORIENT_PORTRAIT,
                        'destination'=> Pdf::DEST_BROWSER,
                        'content' => $content,
                        'cssFile' => '@frontend/web/css/style.css',
                        'cssInline' => '.bd{border:1px solid; text-align: center;} .table{1px solid;} .ar{text-align:right} .imgbd{border:1px solid}',
                        'options' => ['title' => 'Preview Report Case: '],
                        'methods' =>[]
             ]);
       
        return $pdf->render();
    }
}
