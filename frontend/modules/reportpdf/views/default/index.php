<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\VarDumper;

?>

<html>
    <head>
      
</style>
    </head>
    <body>
    
        <div>
            <img src="/img/gecicca_logo.png" style="height: 65px;">
        </div>
        <div class="btn btn-primary"><h2  style="font-family: 'Times New Roman', Times, serif; color: #000099; border-bottom:5px solid #green; border-bottom-style: double;  "  ><B>GeCiCCA Monthly Progress Report</h2></p></div>
<div class="alert btn-primary" >
    
    <p style="font-family:'Time New Roman',Times,serif; font-size: 14px; " ><B>Trial name :</B> A randomized controlled trial of Gemcitabine aleen versus Gemcitabine plus Cisplatin as an adjuvant 
    chemotherapy after curative intent resection of cholangiocarcinoma
    <br> <B> Trial Registry Identifier: </B>TCTR20161101003 (Trial approved by the Thai Clinical Trials Registry (TCTR))</p>
</div>
        <br>
<div>
    <table border="1" style="border-spacing: 0px;  width:100%;  font-family: 'Times New Roman', Times, serif;" class="table-striped">
        <tr bgcolor="#C0C0C0">
            <td align="center" ><B>Events</B></td>
            <td colspan="2" align="center" ><B>Number of trial subjects</B></td>
        </tr>
        <tr>
            <td>Study Site</td>
            <td align="center">Srinagarind Hospital</td>
            <td align="center" width="150px">Total</td>
        </tr>
        <tr>
            <td>Ethical Clearance</td>
            <td></td>
            <td></td>
        </tr>
        
        <tr>
            <td>&nbsp;&nbsp;&nbsp;Approval</td>
            <td align="center" >23 Sep 2016</td>
            <td></td>
           
        </tr>
        
        <tr>
            <td>&nbsp;&nbsp;&nbsp;Expiration</td>
            <td align="center">30 Aug 2017</td>
            <td></td>
           
        </tr>

        <tr>
            <td>Amendment Protocol</td>
            <td></td>
            <td></td>
        </tr>
        
        <tr>
            <td>&nbsp;&nbsp;&nbsp;Approval</td>
            <td align="center" >23 Sep 2016</td>
            <td></td>
           
        </tr>
        
        <tr>
            <td>&nbsp;&nbsp;&nbsp;Expiration</td>
            <td align="center">30 Aug 2017</td>
            <td></td>
           
        </tr>
        
        <tr>
            <td>Trial Registration</td>
            <td></td>
            <td></td>
           
        </tr>
        
        <tr>
            <td>&nbsp;&nbsp;&nbsp;Approval</td>
            <td align="center">31 Oct 2016</td>
            <td></td>
            
        </tr>
        
         <tr>
            <td>&nbsp;&nbsp;&nbsp;Update Status</td>
            <td align="center">29 Apr 2017</td>
            <td></td>
            
        </tr>
        
        <tr>
            <td>&nbsp;&nbsp;&nbsp;First Patient Entered Trial</td>      
            <td align="center"><?=Yii::$app->formatter->asDate($firstpatiententered['date'],'dd MMM yyyy');?></td>
            <td></td>
            
        </tr>
        
        <tr>
            <td>&nbsp;&nbsp;&nbsp;First Patient First Treatment</td>      
            <td align="center"><?=Yii::$app->formatter->asDate($firstpatienttreatment['date'],'dd MMM yyyy');?></td>
            <td></td>
            
        </tr>
        
        <tr>
            <td>&nbsp;&nbsp;&nbsp;Last Patient Entered Trial</td>      
            <td align="center"><?=Yii::$app->formatter->asDate($lastpatiententered['date'],'dd MMM yyyy');?></td>
            <td></td>
            
        </tr>
      
        <tr>
            <td>&nbsp;&nbsp;&nbsp;Last Patient First Treatment</td>      
            <td align="center"><?=Yii::$app->formatter->asDate($lastpatienttreatment['date'],'dd MMM yyyy');?></td>
            <td></td>
            
        </tr>
        
        <tr>
            <td>&nbsp;&nbsp;&nbsp;Last Patient﻿Completed Trial</td>      
            <td align="center"><?=Yii::$app->formatter->asDate($lastpatientcompleted['date'],'dd MMM yyyy');?></td>
            <td></td>
            
        </tr>
      
        <tr>
            <td>&nbsp;&nbsp;&nbsp;Close Out Visit</td>      
            <td align="center"><?=Yii::$app->formatter->asDate($closeout['date'],'dd MMM yyyy');?></td>
            <td></td>
            
        </tr>
        
        <tr>
            <?php Yii::$app->formatter->locale = 'en_GB'; date_default_timezone_set('Asia/Bangkok'); ?>
            <td colspan="3" bgcolor="#C0C0C0"><B>&nbsp;&nbsp;<?php echo Yii::$app->formatter->asDate('now','dd MMMM yyyy') ?></B> </td>
        </tr>
        
        <tr><td>1.Total of screening</td>
            <td align="center">
                 <?php echo $total['Thptcode'] ;?>  cases
            </td>
            <td align="center"><?php echo $total2['Thptcode2'] ;?>  cases</td>
        </tr>
        
        <tr>
           <td>&nbsp;&nbsp;&nbsp;1.1 Screening failure</td>
            <td align="center"><?php echo $Tsceeing['Tsceen'];?> cases </td>
            <td align="center"><?php echo $Tsceeing2['Tsceen2'];?> cases</td>
            
        </tr>
        
        <tr >
            <td>&nbsp;&nbsp;&nbsp;&nbsp;1.1.1 CT/MRI </td>
            <td align="center"><?php echo $CTMRI['TCTMRI'];?> cases</td>
            <td align="center"><?php echo $CTMRI2['TCTMRI2'];?> cases</td>
            
        </tr>
        
        <tr >
            <td>&nbsp;&nbsp;&nbsp;&nbsp;1.1.2 Laboratory results</td>
            <td align="center"><?php echo $Laboratory['Laboratory'];?> cases</td>
            <td align="center"><?php echo $Laboratory2['Laboratory2']?> cases</td>
            
        </tr>
        
        <tr>
            <td>&nbsp;&nbsp;&nbsp;&nbsp;1.1.3 Others</td>
            <td align="center"><?php echo $Others['Others'];?> cases </td>
            <td align="center"><?php echo $Others2['Others2'];?> cases </td>
            
        </tr>
        <tr>
            <td>2.Total of randomization</td>
            <td align="center"><?php echo $randow['Trandom'];?> cases</td>
            <td align="center"><?php echo $randow2['Trandom2'];?> cases</td>
            
        </tr>
        <tr>
            <td>3.Treatment groups</td>
            <td align="center"><?php echo $drug1['chemo_drugs'];?> cases</td>
            <td align="center"><?php echo $Treatment['Treatment'];?> cases</td>
            
            
        </tr>
        <tr>
            <td>&nbsp;&nbsp;&nbsp;3.1 Gemcitabine plus Cisplatin</td>
            <td align="center"><?php echo $drugs['Tdrugs'];?>  cases</td>
            <td align="center"><?php echo $chemo_drugs2['chemo_drugs2']?> cases</td>
                        
        </tr>
        <tr>
            <td>&nbsp;&nbsp;&nbsp;3.2 Gemcitabine alone</td>
            <td align="center"><?php echo $drugs2['drugs2'];?>  cases</td>
            <td align="center"><?php echo $chemo_drugs1['chemo_drugs1'];?> cases</td>
            
            
        </tr>
        
        <tr>
            <td>4.Severe adverse event (SAE)</td>
            <td align="center"><?php echo $sae['Tsae'];?>  cases</td>
            <td align="center"><?php echo $sae2['Tsae2'];?>  cases</td>
            
        </tr>
        <tr>
            <td>5. Drop out</td>
            <td align="center"><?php echo $Dropout['Tdropout'];?>  cases</td>
            <td align="center"><?php echo $Dropout2['Tdropout2'];?>  cases</td>
            
        </tr>

        <tr>
            <td>6. Stopped (With Patients)</td>
            <td align="center"><?php echo $Stop['TStop'];?>  cases</td>
            <td align="center"><?php echo $Stop2['TStop2'];?>  cases</td>
            
        </tr>
        
        <tr>
            <td>7. Loss to follow-up</td>
            <td align="center"><?php echo $Loss['TLoss'];?>  cases</td>
            <td align="center"><?php echo $Loss2['TLoss2'];?>  cases</td>
            
        </tr>
       
        <tr>
            <td>8.Completion of the course of therapy</td>
            <td align="center"><?php echo $comple['Tcomple'];?>  cases</td>
            <td align="center"><?php echo $comple2['Tcomple2'];?>  cases</td>
            
        </tr>
    </table>
</div>
</body>
</html>