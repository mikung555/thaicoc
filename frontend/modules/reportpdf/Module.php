<?php

namespace frontend\modules\reportpdf;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'frontend\modules\reportpdf\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
