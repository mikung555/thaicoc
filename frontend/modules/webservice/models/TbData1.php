<?php

namespace frontend\modules\webservice\models;

use Yii;

/**
 * This is the model class for table "tb_data_1".
 *
 * @property integer $id
 * @property integer $ptid
 * @property integer $ptid_key
 * @property string $xsourcex
 * @property string $xdepartmentx
 * @property integer $rstat
 * @property string $sitecode
 * @property string $ptcode
 * @property string $ptcodefull
 * @property string $hptcode
 * @property string $hsitecode
 * @property integer $user_create
 * @property string $create_date
 * @property integer $user_update
 * @property string $update_date
 * @property integer $target
 * @property string $error
 * @property string $hn
 * @property string $visitdb
 * @property string $name
 * @property string $surname
 * @property string $cid
 * @property string $bdate
 * @property string $age
 * @property string $status
 * @property string $gender
 * @property string $nation
 * @property string $nation1
 * @property string $race
 * @property string $race1
 * @property string $rela
 * @property string $rela1
 * @property string $add1
 * @property string $add2
 * @property string $insu
 * @property string $insu1
 * @property string $var26
 * @property string $comdate
 * @property string $nohome
 * @property string $moo1
 * @property string $road1
 * @property string $atp1_province
 * @property string $atp1_amphur
 * @property string $pcode1
 * @property string $tel1
 * @property string $homeno2
 * @property string $moo2
 * @property string $road2
 * @property string $atp2_province
 * @property string $atp2_amphur
 * @property string $atp2_tumbon
 * @property string $pcode2
 * @property string $tel2
 * @property string $usmobile
 * @property integer $ptid_link
 * @property integer $cid_check
 * @property integer $uidadd
 * @property integer $uidedit
 * @property string $sys_assigncheck
 * @property string $confirm
 * @property string $pay
 * @property string $recieve
 * @property integer $pidcheck
 * @property integer $agecheck
 * @property integer $reccheck
 * @property string $title
 * @property string $dadd
 * @property string $dedit
 * @property string $v2
 * @property string $v3
 */
class TbData1 extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tb_data_1';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'xsourcex', 'usmobile', 'ptid_link', 'cid_check', 'uidadd', 'uidedit', 'sys_assigncheck', 'confirm', 'pay', 'recieve', 'pidcheck', 'agecheck', 'reccheck', 'title', 'dadd', 'dedit', 'v2', 'v3'], 'required'],
            [['id', 'ptid', 'ptid_key', 'rstat', 'user_create', 'user_update', 'target', 'ptid_link', 'cid_check', 'uidadd', 'uidedit', 'pidcheck', 'agecheck', 'reccheck'], 'integer'],
            [['create_date', 'update_date', 'visitdb', 'bdate', 'comdate', 'dadd', 'dedit'], 'safe'],
            [['error', 'add2'], 'string'],
            [['xsourcex', 'xdepartmentx', 'ptcodefull', 'title'], 'string', 'max' => 20],
            [['sitecode', 'ptcode', 'hptcode', 'hsitecode', 'status', 'gender', 'usmobile', 'sys_assigncheck', 'confirm', 'pay', 'recieve', 'v2'], 'string', 'max' => 10],
            [['hn', 'name', 'surname', 'age', 'nation1', 'race1', 'rela1', 'add1', 'insu1', 'var26', 'nohome', 'moo1', 'road1', 'pcode1', 'tel1', 'homeno2', 'moo2', 'road2', 'pcode2', 'tel2', 'v3'], 'string', 'max' => 255],
            [['cid', 'nation', 'race', 'rela', 'insu', 'atp1_province', 'atp1_amphur', 'atp2_province', 'atp2_amphur', 'atp2_tumbon'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ptid' => 'Ptid',
            'ptid_key' => 'Ptid Key',
            'xsourcex' => 'Xsourcex',
            'xdepartmentx' => 'Xdepartmentx',
            'rstat' => 'Rstat',
            'sitecode' => 'Sitecode',
            'ptcode' => 'Ptcode',
            'ptcodefull' => 'Ptcodefull',
            'hptcode' => 'Hptcode',
            'hsitecode' => 'Hsitecode',
            'user_create' => 'User Create',
            'create_date' => 'Create Date',
            'user_update' => 'User Update',
            'update_date' => 'Update Date',
            'target' => 'Target',
            'error' => 'Error',
            'hn' => 'Hn',
            'visitdb' => 'Visitdb',
            'name' => 'Name',
            'surname' => 'Surname',
            'cid' => 'Cid',
            'bdate' => 'Bdate',
            'age' => 'Age',
            'status' => 'Status',
            'gender' => 'Gender',
            'nation' => 'Nation',
            'nation1' => 'Nation1',
            'race' => 'Race',
            'race1' => 'Race1',
            'rela' => 'Rela',
            'rela1' => 'Rela1',
            'add1' => 'Add1',
            'add2' => 'Add2',
            'insu' => 'Insu',
            'insu1' => 'Insu1',
            'var26' => 'Var26',
            'comdate' => 'Comdate',
            'nohome' => 'Nohome',
            'moo1' => 'Moo1',
            'road1' => 'Road1',
            'atp1_province' => 'Atp1 Province',
            'atp1_amphur' => 'Atp1 Amphur',
            'pcode1' => 'Pcode1',
            'tel1' => 'Tel1',
            'homeno2' => 'Homeno2',
            'moo2' => 'Moo2',
            'road2' => 'Road2',
            'atp2_province' => 'Atp2 Province',
            'atp2_amphur' => 'Atp2 Amphur',
            'atp2_tumbon' => 'Atp2 Tumbon',
            'pcode2' => 'Pcode2',
            'tel2' => 'Tel2',
            'usmobile' => 'Usmobile',
            'ptid_link' => 'Ptid Link',
            'cid_check' => 'Cid Check',
            'uidadd' => 'Uidadd',
            'uidedit' => 'Uidedit',
            'sys_assigncheck' => 'Sys Assigncheck',
            'confirm' => 'Confirm',
            'pay' => 'Pay',
            'recieve' => 'Recieve',
            'pidcheck' => 'Pidcheck',
            'agecheck' => 'Agecheck',
            'reccheck' => 'Reccheck',
            'title' => 'Title',
            'dadd' => 'Dadd',
            'dedit' => 'Dedit',
            'v2' => 'V2',
            'v3' => 'V3',
        ];
    }
}
