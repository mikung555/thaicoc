<?php

namespace frontend\modules\webservice\controllers;


use yii\filters\auth\HttpBasicAuth;
use common\models\User;
use yii\rest\ActiveController;

class UserController extends ActiveController
{
    public $modelClass = 'app\models\User';
    
    protected function verbs()
    {
        return [
            'index' => ['GET', 'HEAD'],
            'view' => [],
            'create' => [],
            'update' => [],
            'delete' => [],
        ];
    }  
    
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => HttpBasicAuth::className(),
            'auth' => [$this, 'auth']
        ];
        return $behaviors;
    }    
    public function auth($username, $password)
    {
        $user = User::findByUsername($username);
        
        if($user != null){
            return $user->validatePassword($password) ? $user : null;
        }else{
            return null;
        }
    }
    
    public function actions()
    {
        $actions = parent::actions();
        // customize the data provider preparation with the "prepareDataProvider()" method
        $actions['index']['prepareDataProvider'] = [$this, 'prepareDataProvider'];
        return $actions;
    }
    
    public function prepareDataProvider()
    {   
        return \Yii::$app->user->identity;
    }    
}
