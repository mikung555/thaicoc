<?php

namespace frontend\modules\webservice\controllers;
use frontend\modules\webservice\models\TbData1;
use common\models\User;
use common\models\UserProfile;
use backend\models\UserAllow;

class PatientController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $sitecode=$_GET['sitecode'];
        $auth_key=$_GET['auth_key'];
        $user = User::findIdentityByAccessToken($auth_key);
        $usersitecode=  UserProfile::findOne($user->id)->sitecode;
        
        $usersitecodeallow = UserAllow::find()->andWhere(['user_id'=>$user->id,'sitecode'=>$sitecode])->one();
        
        
        if (($sitecode == $usersitecode)||($usersitecodeallow)) {
            $sql = "SELECT id,hsitecode hospcode,hptcode pid,cid,name,surname from tb_data_1 where rstat<>3 and hsitecode='$sitecode'";
            $data = \Yii::$app->db->createCommand($sql)->queryAll();
            
            return json_encode($data);
        }else{
            $message['Error']="Access denied.";
            return json_encode($message);
        }
    }
    public function actionView()
    {
        $auth_key=$_GET['auth_key'];
        $dataid=$_GET['id'];

        $sql = "SELECT * from tb_data_1 where id='$dataid'";
        $data = \Yii::$app->db->createCommand($sql)->queryOne();
        
        $user = User::findIdentityByAccessToken($auth_key);
        $usersitecode=  UserProfile::findOne($user->id)->sitecode;
        
        $usersitecodeallow = UserAllow::find()->andWhere(['user_id'=>$user->id,'sitecode'=>$data['hsitecode']])->one();
        
        
        if (($data['hsitecode'] == $usersitecode)||($usersitecodeallow)) {
            return json_encode($data);
        }else{
            $message['Error']="Access denied.";
            return json_encode($message);
        }
    }
}
