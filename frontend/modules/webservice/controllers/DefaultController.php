<?php

namespace frontend\modules\webservice\controllers;

use yii\web\Controller;
use yii\helpers\Json;

class DefaultController extends Controller
{
    public function actionIndex()
    {
        $data[]='CASCAP WEBSERVICE';
        return json_encode($data);
        //return $this->render('index');
    }
}
