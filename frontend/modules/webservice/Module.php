<?php

namespace frontend\modules\webservice;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'frontend\modules\webservice\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
