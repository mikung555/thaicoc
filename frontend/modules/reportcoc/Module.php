<?php

namespace frontend\modules\reportcoc;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'frontend\modules\reportcoc\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
