<?php

namespace frontend\modules\reportcoc\controllers;

use yii\web\Controller;
use yii\web\Response;
use Yii;

class DefaultController extends Controller
{
    public function actionIndex()
    {
        return $this->render('index');
    }
    public function actionGetcountry()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $sdate = $_GET[sdate];
        $edate = $_GET[edate];
        $sql = "
		SELECT DISTINCT
CONCAT('เขต ',a.zone_code) zone,
IFNULL(b.countsend,0) as 'countsend',
IFNULL(b.countreply,0) as 'countreply',
IFNULL(b.elder1countsend,0) as 'elder1countsend',
IFNULL(b.elder1countreply,0) as 'elder1countreply',
IFNULL(b.elder2countsend,0) as 'elder2countsend',
IFNULL(b.elder2countreply,0) as 'elder2countreply',
IFNULL(b.elder3countsend,0) as 'elder3countsend',
IFNULL(b.elder3countreply,0) as 'elder3countreply',
IFNULL(b.elder4countsend,0) as 'elder4countsend',
IFNULL(b.elder4countreply,0) as 'elder4countreply',
IFNULL(b.elder5countsend,0) as 'elder5countsend',
IFNULL(b.elder5countreply,0) as 'elder5countreply',
IFNULL(b.elder0countsend,0) as 'elder0countsend',
IFNULL(b.elder0countreply,0) as 'elder0countreply',
IFNULL(persenAll,0.00) as 'persenAll',
IFNULL(persen1,0.00) as 'persen1',
IFNULL(persen2,0.00) as 'persen2',
IFNULL(persen3,0.00) as 'persen3',
IFNULL(persen4,0.00) as 'persen4',
IFNULL(persen5,0.00) as 'persen5',
IFNULL(persen0,0.00) as 'persen0'
FROM all_hospital_thai a
LEFT JOIN (
		SELECT
CONCAT('เขต ',t1.zone_code) as 'zone',
SUM(t1.countsend) countsend,
SUM(t1.countreply) countreply,
SUM(CASE t1.elder WHEN 1 THEN t1.countsend ELSE 0 END) elder1countsend,
SUM(CASE t1.elder WHEN 1 THEN t1.countreply ELSE 0 END) elder1countreply,
SUM(CASE t1.elder WHEN 2 THEN t1.countsend ELSE 0 END) elder2countsend,
SUM(CASE t1.elder WHEN 2 THEN t1.countreply ELSE 0 END) elder2countreply,
SUM(CASE t1.elder WHEN 3 THEN t1.countsend ELSE 0 END) elder3countsend,
SUM(CASE t1.elder WHEN 3 THEN t1.countreply ELSE 0 END) elder3countreply,
SUM(CASE t1.elder WHEN 4 THEN t1.countsend ELSE 0 END) elder4countsend,
SUM(CASE t1.elder WHEN 4 THEN t1.countreply ELSE 0 END) elder4countreply,
SUM(CASE t1.elder WHEN 5 THEN t1.countsend ELSE 0 END) elder5countsend,
SUM(CASE t1.elder WHEN 5 THEN t1.countreply ELSE 0 END) elder5countreply,
SUM(CASE t1.elder WHEN 0 THEN t1.countsend ELSE 0 END) elder0countsend,
SUM(CASE t1.elder WHEN 0 THEN t1.countreply ELSE 0 END) elder0countreply,
IFNULL(
	ROUND(
		SUM(t1.countreply)/SUM(t1.countsend)*100,2
	)
,0.00) as 'persenAll',
IFNULL(
	ROUND(
		SUM(CASE t1.elder WHEN 1 THEN t1.countreply ELSE 0 END)/SUM(CASE t1.elder WHEN 1 THEN t1.countsend ELSE 0 END)*100,2
	)
,0.00) as 'persen1',
IFNULL(
	ROUND(
		SUM(CASE t1.elder WHEN 2 THEN t1.countreply ELSE 0 END)/SUM(CASE t1.elder WHEN 2 THEN t1.countsend ELSE 0 END)*100,2
	)
,0.00) as 'persen2',
IFNULL(
	ROUND(
		SUM(CASE t1.elder WHEN 3 THEN t1.countreply ELSE 0 END)/SUM(CASE t1.elder WHEN 3 THEN t1.countsend ELSE 0 END)*100,2
	)
,0.00) as 'persen3',
IFNULL(
	ROUND(
		SUM(CASE t1.elder WHEN 4 THEN t1.countreply ELSE 0 END)/SUM(CASE t1.elder WHEN 4 THEN t1.countsend ELSE 0 END)*100,2
	)
,0.00) as 'persen4',
IFNULL(
	ROUND(
		SUM(CASE t1.elder WHEN 5 THEN t1.countreply ELSE 0 END)/SUM(CASE t1.elder WHEN 5 THEN t1.countsend ELSE 0 END)*100,2
	)
,0.00) as 'persen5',
IFNULL(
	ROUND(
		SUM(CASE t1.elder WHEN 0 THEN t1.countreply ELSE 0 END)/SUM(CASE t1.elder WHEN 0 THEN t1.countsend ELSE 0 END)*100,2
	)
,0.00) as 'persen0'
FROM
(
SELECT
a.form_send_id,
a.form_send_inputunit inputunit,
h.`name`,
h.zone_code,
h.zone_name,
h.provincecode,
h.province,
COUNT(DISTINCT a.form_send_id) countsend,
IFNULL(FLOOR(COUNT(DISTINCT b.form_send_id,b.form_reply_diag)/COUNT(DISTINCT a.form_send_id,a.form_send_diag)),0) countreply,
IFNULL(c.elder,0) elder
FROM tbdata_reportcoc a
LEFT JOIN tbdata_reportcoc_reply b ON a.form_send_id=b.form_send_id AND a.form_send_diag=b.form_reply_diag AND b.form_reply_rstat IN (1,2)
LEFT JOIN tbdata_1462172833035880400 c ON a.form_send_id=c.id AND c.rstat IN (1,2)
INNER JOIN all_hospital_thai h ON a.form_send_inputunit=h.hcode
WHERE a.form_send_rstat IN (1,2)
AND a.form_send_diag = 'elderly'
AND a.form_send_date BETWEEN '$sdate 00:00:00' AND '$edate 23:59:59'
AND h.zone_code is not null
GROUP BY a.form_send_id,h.zone_code
) t1
GROUP BY CONCAT('เขต ',t1.zone_code)
) b ON CONCAT('เขต ',a.zone_code)=b.zone
WHERE NOT a.zone_code IS NULL
ORDER BY 1
		
		";
        $data = \Yii::$app->db->createCommand($sql)->queryAll();
        $result[data] = $data;
        return $result;
    }

    public function actionGetbar(){
        Yii::$app->response->format = Response::FORMAT_JSON;
        $sdate = $_GET[sdate];
        $edate = $_GET[edate];
        $sql = "
		SELECT DISTINCT
CONCAT('เขต ',a.zone_code) zone,
b.countsend,
b.countreply,
IFNULL(b.persenAll,0.00) as 'persenAll'
FROM all_hospital_thai a
LEFT JOIN (
SELECT
CONCAT('เขต ',t1.zone_code) as 'zone',
SUM(t1.countsend) countsend,
SUM(t1.countreply) countreply,
IFNULL(
	ROUND(
		SUM(t1.countreply)/SUM(t1.countsend)*100,2
	)
,0.00) as 'persenAll'
FROM
(
SELECT
a.form_send_id,
a.form_send_inputunit inputunit,
h.`name`,
h.zone_code,
h.zone_name,
h.provincecode,
h.province,
COUNT(DISTINCT a.form_send_id) countsend,
IFNULL(FLOOR(COUNT(DISTINCT b.form_send_id,b.form_reply_diag)/COUNT(DISTINCT a.form_send_id,a.form_send_diag)),0) countreply,
IFNULL(c.elder,0) elder
FROM all_hospital_thai h  LEFT JOIN
tbdata_reportcoc a ON a.form_send_inputunit=h.hcode 
LEFT JOIN tbdata_reportcoc_reply b ON a.form_send_id=b.form_send_id AND a.form_send_diag=b.form_reply_diag AND b.form_reply_rstat IN (1,2)
LEFT JOIN tbdata_1462172833035880400 c ON a.form_send_id=c.id AND c.rstat IN (1,2)
 
WHERE a.form_send_rstat IN (1,2)
AND a.form_send_diag = 'elderly'
AND a.form_send_date BETWEEN '$sdate 00:00:00' AND '$edate 23:59:59'
AND h.zone_code is not null
GROUP BY a.form_send_id,h.zone_code
) t1
GROUP BY CONCAT('เขต ',t1.zone_code)
) b ON CONCAT('เขต ',a.zone_code)=b.zone
WHERE NOT a.zone_code IS NULL
ORDER BY 1

		";
        $data = \Yii::$app->db->createCommand($sql)->queryAll();
        $i=0;
        foreach ($data as $value){
            $chartLabel[] = $value[zone];
            if(!$value[persenAll]){
                $dataresult = 0;
            }else{
                $dataresult = number_format($value[persenAll],2);
            }
            $chartValue[$i][y] = $dataresult*1;
            $chartValue[$i][code] = $value[zone];
            $chartValue[$i][countsend] = $value[countsend];
            $chartValue[$i][countreply] = $value[countreply];

            $i++;
        }
        $bar[chartLabel] =  $chartLabel;
        $bar[chartValue] =  $chartValue;
        $bar[chartTitle] =  "ประเทศไทย";

        return $bar;
    }

}
