<?php
/* @var $this yii\web\View */

\backend\assets\BackendAsset::register($this);
use backend\modules\coc\assets\CocAsset;
CocAsset::register($this);
\common\lib\damasac\assets\datetime\DMSDatetimeAsset::register($this);
use yii\widgets\Pjax;
use yii\helpers\Url;
use yii\helpers\Html;

use kartik\select2\Select2;
use backend\modules\coc\classes\CocQuery;
use kartik\widgets\DepDrop;
$this->title = Yii::t('app', 'Report');
?>

    <div class="report-search" >
        <div class="row">
            <div class="col-md-12">
                <div class="box box-info flat">
                    <div class="box-header with-border text-center">
                        <h3 class="box-title">รายงานผู้สูงอายุ</h3>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <form class="form-horizontal">
                            <div class="col-md-offset-2 col-md-8">
                                <div class="form-group col-md-12">
                                    <div class="col-md-12">
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <span>เลือกวันที่</span>
                                            </div>
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text" class="form-control" value="<?= $sdate ?>" name="sdate" id="date_timepicker_start" placeholder="วันที่เริ่ม">
                                            <div class="input-group-addon">ถึง</div>
                                            <input type="text" class="form-control" value="<?= $edate ?>" name="edate" id="date_timepicker_end" placeholder="วันที่สิ้นสุด">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="col-md-12 text-center">
                                        <hr>
                                        <input id="form-token" type="hidden" name="<?=Yii::$app->request->csrfParam?>"
                                               value="<?=Yii::$app->request->csrfToken?>"/>
                                        <button type="button" id="getdatareport" class="btn btn-info btn-flat text-center">ดูข้อมูล</button>
                                        <br>
                                    </div>
                                    <!-- /.input group -->
                                </div>
                                <!-- /.form group -->
                        </form>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div>
        </div>
    </div>
    <div class="row" id="showreport1" style="display: none">
        <div class="col-md-12">
            <div class="box box-info flat">
                <div class="box-header with-border text-center">
                    <h3 class="box-title center">รายงานผู้สูงอายุ ระดับประเทศ</h3>
                    <div id="show_diage_area"></div>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class=" col-md-12">
                        <!-- datatables -->

                        <table id="coc01" class="table-bordered table-striped table-condensed  table-hover" width="100%">
                            <thead class="bg-blue">
                            <tr>
                                <th rowspan="3" colspan="1" class="text-center" >เขต</th>
                                <th rowspan="2" colspan="1" class="text-center" >ส่งเยี่ยม<br>ทั้งหมด</th>
                                <th rowspan="2" colspan="2" class="text-center" >ตอบกลับ<br>ทั้งหมด</th>
                                <th rowspan="1" colspan="3" class="text-center">ติดสังคม</th>
                                <th rowspan="1" colspan="3" class="text-center">ติดบ้านไม่สับสน</th>
                                <th rowspan="1" colspan="3" class="text-center">ติดบ้านสับสน</th>
                                <th rowspan="1" colspan="3" class="text-center">ติดเตียง</th>
                                <th rowspan="1" colspan="3" class="text-center">ติดเตียงระยะท้าย</th>
                                <th rowspan="1" colspan="3" class="text-center">ไม่ระบุ</th>
                            </tr>
                            <tr>
                                <th class="text-center" >ส่งเยี่ยม</th>
                                <th class="text-center" rowspan="1" colspan="2">ตอบกลับ</th>
                                <th class="text-center" >ส่งเยี่ยม</th>
                                <th class="text-center" rowspan="1" colspan="2">ตอบกลับ</th>
                                <th class="text-center" >ส่งเยี่ยม</th>
                                <th class="text-center" rowspan="1" colspan="2">ตอบกลับ</th>
                                <th class="text-center" >ส่งเยี่ยม</th>
                                <th class="text-center" rowspan="1" colspan="2">ตอบกลับ</th>
                                <th class="text-center" >ส่งเยี่ยม</th>
                                <th class="text-center" rowspan="1" colspan="2">ตอบกลับ</th>
                                <th class="text-center" >ส่งเยี่ยม</th>
                                <th class="text-center" rowspan="1" colspan="2">ตอบกลับ</th>
                            </tr>
                            <tr>
                                <th class="text-center">(ครั้ง)</th>
                                <th class="text-center">(ครั้ง)</th>
                                <th class="text-center">(%)</th>
                                <th class="text-center">(ครั้ง)</th>
                                <th class="text-center">(ครั้ง)</th>
                                <th class="text-center">(%)</th>
                                <th class="text-center">(ครั้ง)</th>
                                <th class="text-center">(ครั้ง)</th>
                                <th class="text-center">(%)</th>
                                <th class="text-center">(ครั้ง)</th>
                                <th class="text-center">(ครั้ง)</th>
                                <th class="text-center">(%)</th>
                                <th class="text-center">(ครั้ง)</th>
                                <th class="text-center">(ครั้ง)</th>
                                <th class="text-center">(%)</th>
                                <th class="text-center">(ครั้ง)</th>
                                <th class="text-center">(ครั้ง)</th>
                                <th class="text-center">(%)</th>
                                <th class="text-center">(ครั้ง)</th>
                                <th class="text-center">(ครั้ง)</th>
                                <th class="text-center">(%)</th>
                            </tr>
                            </thead>
                            <tfoot>
                            <tr>
                                <th style="text-align:right">รวม</th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                            </tr>
                            </tfoot>
                            <tbody>
                            </tbody>
                        </table>
                        <!-- end datatables -->
                    </div>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
        <div class="col-md-12">
            <div class="box box-info flat">
                <div class="box-header with-border text-center">
                    <h3 class="box-title">อัตราการตอบกลับ การดูแลต่อเนื่องที่บ้าน</h3>
                    <div class="box-tools pull-right">
                        <!-- Buttons, labels, and many other things can be placed here! -->

                    </div><!-- /.box-tools -->
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div id="bar-chart" style="hight:400px;margin:0 auto"></div>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
    </div>

<?php
$this->registerJs("
jQuery.datetimepicker.setLocale('th');
jQuery(function(){
 jQuery('#date_timepicker_start').datetimepicker({
  format:'Y/m/d',
  value: '2017/10/01',
  onShow:function( ct ){
   this.setOptions({
    maxDate:jQuery('#date_timepicker_end').val()?jQuery('#date_timepicker_end').val():false
   })
  },
  timepicker:false
 });
 jQuery('#date_timepicker_end').datetimepicker({
  format:'Y/m/d',
  value: Date.now().toString(),
  onShow:function( ct ){
   this.setOptions({
    minDate:jQuery('#date_timepicker_start').val()?jQuery('#date_timepicker_start').val():false
   })
  },
  timepicker:false
 });
});

$('#reportdiv').on('click', function(){
    var printContents = document.getElementById('report').innerHTML;
       var originalContents = document.body.innerHTML;
       document.body.innerHTML = printContents;
       window.print();
       document.body.innerHTML = originalContents;
});
$( document ).ready(function() {
//$('#showreport1').show();

var sdate,edate;
    if(sdate == null && edate == null){
        sdate = moment().format('YYYY-MM-DD');
        edate = moment().format('YYYY-MM-DD');
            }
    
var right = '".$right[p]."';
var zone = $('#zone').val();
console.log(typeof(zone));
$('#getdatareport').on('click',function(){
if(sdate != null){
            $('#showreport1').show();
            //console.log('".URL::to(['default/getbar'])."' + '?sdate=' + $('#date_timepicker_start').val() +'&edate=' + $('#date_timepicker_end').val());
            $.ajax({
                dataType:'json',
                url:'".URL::to(['default/getbar'])."?sdate=' + $('#date_timepicker_start').val() +'&edate=' + $('#date_timepicker_end').val() +'',
                async:true,
   
                success:function(data){
                    var resp = data;
                    bar_reportcoc('bar-chart', 80 ,data.chartLabel,data.chartValue, '' ,data.chartTitle);
                }
            });
            $('#coc01').DataTable({
                ajax: '".URL::to(['default/getcountry'])."?sdate=' + $('#date_timepicker_start').val() +'&edate=' + $('#date_timepicker_end').val() + '' ,
                paging: false,
                destroy: true,
                dom: 'Bfrtip',
                scrollX: true,
                scrollY: true,
                buttons: [
                    'copy', 'excel'
                ],
                language:{
                    info:'แสดง _START_ ถึง _END_ จากทั้งหมด _TOTAL_',
                    lengthMenu:'แสดง _MENU_ แถว',
                    search:'ค้นหา',
                    loadingRecords:'กำลังค้นหา...',
                    paginate:{
                        previous:'ก่อนหน้า',
                        next:'ถัดไป',
                        first:'เริ่มต้น',
                        last:'สุดท้าย',
                    }
                }, 
                columnDefs:[
                    {className:'text-right',targets:[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21]},
                ],
                columns: [
                    { 'data': 'zone' },
                    { 'data': 'countsend',render: $.fn.dataTable.render.number(',') },
                    { 'data': 'countreply',render: $.fn.dataTable.render.number(',') },
                    { 'data': 'persenAll'},
                    { 'data': 'elder1countsend',render: $.fn.dataTable.render.number(',') },
                    { 'data': 'elder1countreply',render: $.fn.dataTable.render.number(',') },
                    { 'data': 'persen1'},
                    { 'data': 'elder2countsend',render: $.fn.dataTable.render.number(',') },
                    { 'data': 'elder2countreply',render: $.fn.dataTable.render.number(',') },
                    { 'data': 'persen2'},
                    { 'data': 'elder3countsend',render: $.fn.dataTable.render.number(',') },
                    { 'data': 'elder3countreply',render: $.fn.dataTable.render.number(',') },
                    { 'data': 'persen3'},
                    { 'data': 'elder4countsend',render: $.fn.dataTable.render.number(',') },
                    { 'data': 'elder4countreply',render: $.fn.dataTable.render.number(',') },
                    { 'data': 'persen4'},
                    { 'data': 'elder5countsend',render: $.fn.dataTable.render.number(',') },
                    { 'data': 'elder5countreply',render: $.fn.dataTable.render.number(',') },
                    { 'data': 'persen5'},
                    { 'data': 'elder0countsend',render: $.fn.dataTable.render.number(',') },
                    { 'data': 'elder0countreply',render: $.fn.dataTable.render.number(',') },
                    { 'data': 'persen0'},
                ],
                footerCallback: function (row, data, start, end, display) {
                    var numFormat = $.fn.dataTable.render.number(',').display;
                    var api = this.api();
                    var intVal = function ( i ) {
                    return typeof i === 'string' ?
                        i.replace(/[\$,]/g, '')*1 :
                        typeof i === 'number' ?
                            i : 0;
                    };
                    total1 = api.column( 1 ).data().reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );
                    total2 = api.column( 2 ).data().reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );
                    
                    total4 = api.column( 4 ).data().reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );
                    total5 = api.column( 5 ).data().reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );
                    
                    total7 = api.column( 7 ).data().reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );
                    total8 = api.column( 8 ).data().reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );
                    
                    total10 = api.column( 10 ).data().reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );
                    total11 = api.column( 11 ).data().reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );

                    total13 = api.column( 13 ).data().reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );
                    total14 = api.column( 14 ).data().reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );
                    
                    total16 = api.column( 16 ).data().reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );
                    total17 = api.column( 17 ).data().reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );
                    
                    total19 = api.column( 19 ).data().reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );
                    total20 = api.column( 20 ).data().reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );

                    if(total1 == 0){
                        total3 = '0.00';
                        
                    }else{
                        total3 = (total2/total1*100).toFixed(2)
                    }
                    if(total4 == 0){
                        total6 = '0.00';
                        
                    }else{
                        total6 = (total5/total4*100).toFixed(2)
                    }
                    if(total7 == 0){
                        total9 = '0.00';
                        
                    }else{
                        total9 = (total8/total7*100).toFixed(2)
                    }
                    if(total10 == 0){
                        total12 = '0.00';
                        
                    }else{
                        total12 = (total11/total10*100).toFixed(2)
                    }
                    if(total13 == 0){
                        total15 = '0.00';
                        
                    }else{
                        total15 = (total14/total13*100).toFixed(2)
                    }
                    if(total16 == 0){
                        total18 = '0.00';
                        
                    }else{
                        total18 = (total17/total16*100).toFixed(2)
                    }
                    if(total19 == 0){
                        total21 = '0.00';
                        
                    }else{
                        total21 = (total20/total19*100).toFixed(2)
                    }
                    
                    $(api.column( 1 ).footer()).html(
                        numFormat(total1)
                    );
                    $(api.column( 2 ).footer()).html(
                        numFormat(total2)
                    );
                    $(api.column( 3 ).footer()).html(
                        total3
                        
                    );
                    $(api.column( 4 ).footer()).html(
                        numFormat(total4)
                    );
                    $(api.column( 5 ).footer()).html(
                        numFormat(total5)
                    );
                    $(api.column( 6 ).footer()).html(
                        total6
                    );
                    $(api.column( 7 ).footer()).html(
                        numFormat(total7)
                    );
                    $(api.column( 8 ).footer()).html(
                        numFormat(total8)
                    );
                    $(api.column( 9 ).footer()).html(
                        total9
                    );
                    $(api.column( 10 ).footer()).html(
                        numFormat(total10)
                    );
                    $(api.column( 11 ).footer()).html(
                        numFormat(total11)
                    );
                    $(api.column( 12 ).footer()).html(
                        total12
                    );
                    $(api.column( 13 ).footer()).html(
                        numFormat(total13)
                    );
                    $(api.column( 14 ).footer()).html(
                        numFormat(total14)
                    );
                    $(api.column( 15 ).footer()).html(
                        total15
                    );
                    $(api.column( 16 ).footer()).html(
                        numFormat(total16)
                    );
                    $(api.column( 17 ).footer()).html(
                        numFormat(total17)
                    );
                    $(api.column( 18 ).footer()).html(
                        total18
                    );
                    $(api.column( 19 ).footer()).html(
                        numFormat(total19)
                    );
                    $(api.column( 20 ).footer()).html(
                        numFormat(total20)
                    );
                    $(api.column( 21 ).footer()).html(
                        total21
                    );
                }
            });
        }
});

});
");
?>