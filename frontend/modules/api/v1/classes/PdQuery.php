<?php
/**
 * Created by PhpStorm.
 * User: kawin
 * Date: 8/9/2017
 * Time: 11:41 AM
 */

namespace frontend\modules\api\v1\classes;

use Yii;
use yii\db\Exception;
use yii\base\ErrorException;
use yii\db\Query;

class PdQuery
{
    public static function CheckToken($username, $token)
    {
        try{
            $resUser = self::GetUser($username);
            if ($token == $resUser["auth_key"]) {
                return true;
            }
            return false;
        }catch (Exception $e){
            return $e;
        }catch (ErrorException $e){
            return $e;
        }
    }

    public static function LoginWithToken($username, $token)
    {
        try{
            $resUser = self::GetUser($username);
            $cause = "user not found.";
            // print_r(json_encode($resUser));
            if ($resUser) {
                $res_profile = (new Query())->select('*')->from('user_profile')->where(['user_id' => $resUser['id']])->one();
                if (!$res_profile) {
                    $cause = "user exist but profile not found.";
                    $result["success"] = false;
                    $result["message"] = $cause;
                    return $result;
                }
                if ($token == $resUser["auth_key"]) {
                    $result["success"] = true;
                    $result["data"] = $array = array(
                        'id'=>$resUser["id"],
                        'username'=>$resUser["username"],
                        'token'=>$resUser["auth_key"],
                        'email'=>$resUser["email"],
                        'profile'=>$res_profile,
                    );
                    return $result;
                }
                $cause = "token not correct.";
            }

            $result["success"] = false;
            $result["message"] = $cause;
            return $result;
        }catch (Exception $e){
            return $e;
        }catch (ErrorException $e){
            return $e;
        }
    }

    public static function Login($username, $password)
    {
        try{
            $query = (new Query())
                ->select(['inv_user.username as username','inv_user.user_id as id', 'inv_user.password as password' ,'user.email as email','user.auth_key as auth_key' ]) //
                ->from('inv_user')
                ->innerJoin('user','user.id = inv_user.user_id')
                ->where(['inv_user.username' => $username]);
            $resUser = $query->one();
            $cause = "user not found -.".$username;
            if ($resUser) {

                $cause = "password not correct.";
                $res_profile = (new Query())->select('*')->from('user_profile')->where(['user_id' => $resUser['id']])->one();
                if (!$res_profile) {
                    $cause = "user exist but profile not found.";
                    $result["success"] = false;
                    $result["message"] = $cause;
                    return $result;
                }

                if (\Yii::$app->getSecurity()->validatePassword($password, $resUser['password'])) {
                    $result["success"] = true;
                    $result["data"] = $array = array(
                        'id'=>$resUser["id"],
                        'username'=>$resUser["username"],
                        'token'=>$resUser["auth_key"],
                        'email'=>$resUser["email"],
                        'profile'=>$res_profile,
                    );
                    return $result;
                }
            }
            $result["success"] = false;
            $result["message"] = $cause;
            return $result;

        }catch (Exception $e){
            return $e;
        }catch (ErrorException $e){
            return $e;
        }
    }

    /**
     * Get user by username
     * @param $username
     * @return array|bool|\Exception|Exception
     */
    public static function GetUser($username)
    {
        try{
            $query = (new Query())
                ->select(['username as username','user_id as id','user.email as email','user.auth_key as auth_key'])
                ->from('inv_user')
                ->innerJoin('user','user.id = inv_user.user_id')
                ->where(['inv_user.username' => $username]);
            return $query->one();
        }catch (Exception $e){
            return $e;
        }
    }
}