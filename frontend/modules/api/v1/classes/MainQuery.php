<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace frontend\modules\api\v1\classes;

use Yii;
use yii\db\Query;
use yii\helpers\VarDumper;
use common\models\User;
use common\lib\codeerror\helpers\GenMillisecTime;
use yii\db\Exception;
use yii\base\ErrorException;

class MainQuery
{

    /* great sample for clear query
     * $query1 = (new \yii\db\Query())
	    ->select("inv_gen.*")
	    ->from('inv_gen')
	    ->innerJoin('inv_favorite', 'inv_favorite.gid=inv_gen.gid')
	    ->where("gsystem=0 AND active=1 AND inv_favorite.user_id=:user_id", [':user_id'=>$userId]);
     * */

    public static function GetHospitalInfo($hcode){
        try{
            $query = (new Query())->select("*")->from('all_hospital_thai')->where(['hcode'=>$hcode]);
            $res = $query->one();
            return $res;
        }catch (Exception $e){
            return $e;
        }catch (ErrorException $e){
            return $e;
        }
    }

    public static function GetDoctors(){
        try{
            $query = (new Query())->select(["doctorcode","pincode","doctorfullname"])->from('doctor_all')->where('rstat != 3');
            $res = $query->all();
            return $res;
        }catch (Exception $e){
            return $e;
        }catch (ErrorException $e){
            return $e;
        }
    }

    public static function FindDoctor($name){
        try{
            $query = (new Query())->select(["doctorcode","doctorfullname","pincode"])->from('doctor_all')->where('rstat != 3')->andWhere(['like', 'doctorfullname', $name]);
            $res = $query->all();
            return $res;
        }catch (Exception $e){
            return $e;
        }catch (ErrorException $e){
            return $e;
        }
    }


    public static function GetEzFields($ezf_id)
    {
        try{
            $query = (new Query())->select(
                ['ezform_fields.ezf_field_id',
                'ezform_fields.ezf_field_name',
                'ezform_fields.ezf_field_type',
                'ezform_fields.ezf_field_lenght',
                'ezform_fields.ezf_field_order',
                'ezform.ezf_table',
                'ezform_input.input_name',
                'ezform_fields.ezf_field_label'])
                ->from('ezform')
                ->innerJoin('ezform_fields','ezform_fields.ezf_id = ezform.ezf_id')
                ->innerJoin('ezform_input','ezform_fields.ezf_field_type = ezform_input.input_id')
                ->where(['ezform.ezf_id'=>$ezf_id]);
            $res = $query->all();

            $query = (new Query())->select(
                ['ezform_fields.ezf_field_id',
                    'ezform_fields.ezf_field_name',
                    'ezform_fields.ezf_field_type',
                    'ezform_fields.ezf_field_lenght',
                    'ezform_fields.ezf_field_order',
                    'ezform_fields.ezf_field_sub_id',
                    'ezform_fields.ezf_field_sub_textvalue',
                    'ezform.ezf_table',
                    'ezform_fields.ezf_field_label'])
                ->from('ezform')
                ->innerJoin('ezform_fields','ezform_fields.ezf_id = ezform.ezf_id and ezform_fields.ezf_field_type = 0 ')
                ->where(['ezform.ezf_id'=>$ezf_id]);
            $res3 = $query->all();

            $query2 = (new Query())->select([
                'ezform_fields.ezf_field_id',
                'ezform_fields.ezf_field_name',
                'ezform_fields.ezf_field_type',
                'ezform_fields.ezf_field_lenght',
                'ezform_fields.ezf_field_order',
                'ezform.ezf_table',
                'ezform_fields.ezf_field_label'])
                ->from('ezform')
                ->innerJoin('ezform_fields', 'ezform_fields.ezf_id = ezform.ezf_id')
                ->where( ['ezform.ezf_id' => $ezf_id ])
                ->andWhere(['ezform_fields.ezf_field_type' => 21]);
            $res2 = $query2->all();
            $res = array_merge($res, $res2);
            $res = array_merge($res, $res3);


            for ($i=0; $i < count ($res) ; $i++) {
                if($res[$i]["ezf_field_type"] == "13"){
                    $res[$i]["child_field_id"] = (new Query())
                        ->select('ezf_field_name')
                        ->from('ezform_fields')
                        ->where(['ezf_field_sub_id'=>$res[$i]["ezf_field_id"]])->all();
                }
            }
            return $res;
        }catch (Exception $e){
            return $e;
        }catch (ErrorException $e){
            return $e;
        }
    }

    public static function GetEzFieldsOnly($ezf_id)
    {
        try{
            $query = (new Query())
                ->select(['ezf_field_id','ezf_field_name'])
                ->from('ezform_fields')
                ->where(['ezf_id' => $ezf_id]);
            return  $query->all();
        }catch (Exception $e){
            return $e;
        }
    }

    public static function GetEZFormById($formId)
    {
        try{
            $out = [];
            $query = (new Query())
                ->select(['ezf_name','ezf_id'])
                ->from('ezform')
                ->where(['ezf_id' => $formId]);
            $out["ezform"] = $query->all()[0];
            $out["ezfields"] = MainQuery::GetEzFields($formId);

            $query_choices = (new Query())
                ->select(['ezform_fields.ezf_field_id','ezform_choice.ezf_choicelabel','ezform_choice.ezf_choice_id',
                    'ezform_choice.ezf_choicevalue','ezform_fields.ezf_field_label','ezform_fields.ezf_field_order'])
                ->from('ezform_fields')
                ->where(['ezform_fields.ezf_id' => $formId])
                ->innerJoin('ezform_choice' , 'ezform_fields.ezf_field_id = ezform_choice.ezf_field_id');

            $out["ezchoices"] = $query_choices->all();
            $out["checksum"] = md5 (json_encode($out));
            return $out;

        }catch (Exception $e){
            return $e;
        }catch (ErrorException $e){
            return $e;
        }
    }

    public static function CheckToken($user_id, $token)
    {
        try{
            $resUser = self::GetUserById($user_id);

            if($resUser == null)return false;

            if ($token == $resUser["auth_key"] || "aabb1212" == $token) {
                return true;
            }
            return false;
        }catch (Exception $e){
            return $e;
        }catch (ErrorException $e){
            return $e;
        }
    }

    public static function LoginWithToken($username, $token)
    {
        try{
            $resUser = self::GetUser($username);
            $cause = "user not found.";
            // print_r(json_encode($resUser));
            if ($resUser) {
                $res_profile = (new Query())->select('*')->from('user_profile')->where(['user_id' => $resUser['id']])->one();
                if (!$res_profile) {
                    $cause = "user exist but profile not found.";
                    $result["success"] = false;
                    $result["message"] = $cause;
                    return $result;
                }
                if ($token == $resUser["auth_key"]) {
                    $result["success"] = true;
                    $result["data"] = $array = array(
                        'id'=>$resUser["id"],
                        'username'=>$resUser["username"],
                        'token'=>$resUser["auth_key"],
                        'email'=>$resUser["email"],
                        'profile'=>$res_profile,
                    );
                    return $result;
                }
                $cause = "token not correct.";
            }

            $result["success"] = false;
            $result["message"] = $cause;
            return $result;
        }catch (Exception $e){
            return $e;
        }catch (ErrorException $e){
            return $e;
        }
    }

    public static function Login($username, $password)
    {
        try{
            $query = (new Query())
                ->select(['username','id','email', 'password_hash' ,'auth_key'])
                ->from('user')
                ->where(['username' => $username]);
            $resUser = $query->one();
            $cause = "user not found -.";
            if ($resUser) {
                $res_profile = (new Query())->select('*')->from('user_profile')->where(['user_id' => $resUser['id']])->one();
                if (!$res_profile) {
                    $cause = "user exist but profile not found.";
                    $result["success"] = false;
                    $result["message"] = $cause;
                    return $result;
                }
                if (\Yii::$app->getSecurity()->validatePassword($password, $resUser['password_hash'])) {
                    $result["success"] = true;
                    $result["data"] = $array = array(
                        'id'=>$resUser["id"],
                        'username'=>$resUser["username"],
                        'token'=>$resUser["auth_key"],
                        'email'=>$resUser["email"],
                        'profile'=>$res_profile,
                    );
                    return $result;
                }
                $cause = "password not correct.";
            }
            $result["success"] = false;
            $result["message"] = $cause;
            return $result;

        }catch (Exception $e){
            return $e;
        }catch (ErrorException $e){
            return $e;
        }
    }

    /**
     * Get user by username
     * @param $username
     * @return array|bool|\Exception|Exception
     */
    public static function GetUser($username)
    {
        try{
            $query = (new Query())
                ->select(['username','id','email','auth_key'])
                ->from('user')
                ->where(['username' => $username]);
            return $query->one();
        }catch (Exception $e){
            return $e;
        }
    }


    /**
     * Get user by username
     * @param $username
     * @return array|bool|\Exception|Exception
     */
    public static function GetUserById($user_id)
    {
        try{
            $query = (new Query())
                ->select(['username','id','email','auth_key'])
                ->from('user')
                ->where(['id' => $user_id]);
            return $query->one();
        }catch (Exception $e){
            return $e;
        }
    }

    public static function GetRelatePatientBySite($sitecode, $limit)
    {
        /*try{

           $sql = "SELECT tb_data_1.ptid,
               tb_data_3.sitecode,
               tb_data_1.`name`,
               tb_data_1.surname
               FROM
               tb_data_3
               INNER JOIN tb_data_1
               WHERE tb_data_1.sitecode = ".$sitecode." AND tb_data_1.sitecode = tb_data_3.sitecode AND tb_data_3.ptid = tb_data_1.id
               GROUP BY tb_data_1.ptid
               LIMIT ".$limit;
           return Yii::$app->db->createCommand($sql)->queryAll();

       }catch (Exception $e){
           return $e;
       }*/
    }

    public static function GetRegRecordById($ptid)
    {
        return self::GetFormRecordById('tb_data_1',$ptid);
    }

    public static function GetRegRecordsBySite($sitecode)
    {
        return self::GetFormRecordsBySite('tb_data_1',$sitecode);
    }

    public static function SyncRegRecords($sitecode,$syncdate)
    {
        return self::SyncFormRecords('tb_data_1',$sitecode,$syncdate);
    }

    public static function GetEmrRecordsBySite($sitecode)
    {
        try{
            $query = (new Query())
                ->select(['ezf_id', 'data_id','comp_id','target_id','user_create','user_update','create_date','update_date','rstat','xsourcex'])
                ->from('ezform_target')
                ->where('xsourcex=:sitecode', [':sitecode' => $sitecode]);
            return $query->all();
        }catch (Exception $e){
            return $e;
        }
    }

    public static function GetCca01RecordById($ptid)
    {
        return self::GetFormRecordById('tb_data_2' , $ptid);
    }

    public static function GetCca01RecordsBySite($sitecode)
    {
        return self::GetFormRecordsBySite('tb_data_2',$sitecode);
    }

    public static function GetCca01RecordsByRefReg($sitecode)
    {
        return self::GetFormRecordsByRefReg('tb_data_2',$sitecode);
    }

    public static function SyncCca01RecordsBySite($sitecode,$syncdate)
    {
        return self::SyncFormRecords('tb_data_2',$sitecode,$syncdate);
    }

    public static function SyncCca01RecordsByRefReg($sitecode,$syncdate)
    {
        return self::SyncFormRecordsByRefReg('tb_data_2',$sitecode,$syncdate);
    }

    public static function SyncCca01RecordsByRefCreatedReg($sitecode,$syncdate)
    {
        return self::SyncFormCreatedRecordsByRefReg('tb_data_2',$sitecode,$syncdate);
    }

    public static function GetCca02RecordById($ptid)
    {
        return self::GetFormRecordById('tb_data_3',$ptid);
    }

    public static function GetCca02RecordsBySite($sitecode)
    {
        return self::GetFormRecordsBySite('tb_data_3',$sitecode);
    }

    public static function GetCca02RecordsByRefReg($sitecode)
    {
       return self::GetFormRecordsByRefReg('tb_data_3',$sitecode);
    }

    public static function SyncCca02RecordsByRefReg($sitecode,$syncdate)
    {
        return self::SyncFormRecordsByRefReg('tb_data_3',$sitecode,$syncdate);
    }

    public static function SyncCca02RecordsByRefCreatedReg($sitecode,$syncdate)
    {
        return self::SyncFormCreatedRecordsByRefReg('tb_data_3',$sitecode,$syncdate);
    }


    /**
     * Get one record from table ref by ptid
     * @param $table_name
     * @param $ptid
     * @return array|bool|\Exception|Exception
     */
    public static function GetFormRecordById($table_name , $ptid)
    {
        try{
            $query1 = (new Query())
                ->select("*")
                ->from($table_name)
                ->where([$table_name.'.ptid' => $ptid]);
            return $query1->one();
        }catch (Exception $e){
            return $e;
        }
    }

    /**
     * Get form record in table with same sitecode
     * @param $table_name
     * @param $sitecode
     * @return array|\Exception|Exception
     */
    public static function GetFormRecordsBySite($table_name , $sitecode)
    {
        try{
            $query1 = (new Query())
                ->select("*")
                ->from($table_name)
                ->where([$table_name.'.hsitecode' => $sitecode]);
            return $query1->all();
        }catch (Exception $e) {
            return $e;
        }
    }

    /**
     * Get form record ref by ptid of reg table with sitecode
     * @param $table_name
     * @param $sitecode
     * @return array|\Exception|Exception
     */
    public static function GetFormRecordsByRefReg($table_name , $sitecode)
    {
        try{
            $query1 = (new Query())
                ->select($table_name.".*")
                ->from($table_name)
                ->innerJoin('tb_data_1', 'tb_data_1.ptid = '.$table_name.'.ptid and tb_data_1.hsitecode = :sitecode and tb_data_1.rstat != 3', [':sitecode' => $sitecode ])
                ->where(['not', [$table_name.'.rstat' => 3]]);
            return $query1->all();
        }catch (Exception $e){
            return $e;
        }
    }

    public static function SyncFormRecords($table_name , $sitecode , $syncfrom)
    {
        try{
            $query1 = (new Query())
                ->select($table_name.".*")
                ->from($table_name)
                ->where(['not', [$table_name.'.rstat' => 3]])
                ->andWhere($table_name.".update_date > :syncdate")
                ->andWhere(["hsitecode"=>$sitecode])
                ->addParams([':syncdate' => $syncfrom]);
            return $query1->all();
        }catch (Exception $e){
            return $e;
        }
    }

    /**
     * Query form ref by tb_data_1 and date > $syncform
     * @param $table_name
     * @param $sitecode
     * @param $syncfrom
     * @return array|\Exception|Exception
     */
    public static function SyncFormRecordsByRefReg($table_name , $sitecode , $syncfrom)
    {
        try{
            $query1 = (new Query())
                ->select($table_name.".*")
                ->from($table_name)
                ->innerJoin('tb_data_1', 'tb_data_1.ptid = '.$table_name.'.ptid and tb_data_1.hsitecode = :sitecode and tb_data_1.rstat != 3', [':sitecode' => $sitecode ])
                ->where(['not', [$table_name.'.rstat' => 3]])
                ->andWhere($table_name.".update_date > :syncdate")
                ->addParams([':syncdate' => $syncfrom]);
            return $query1->all();
        }catch (Exception $e){
            return $e;
        }
    }

    public static function SyncFormCreatedRecordsByRefReg($table_name , $sitecode , $syncfrom)
    {
        try{
            $query1 = (new Query())
                ->select($table_name.".*")
                ->from($table_name)
                ->innerJoin('tb_data_1', 'tb_data_1.ptid = '.$table_name.'.ptid and tb_data_1.hsitecode = :sitecode and tb_data_1.rstat != 3', [':sitecode' => $sitecode ])
                ->where(['not', [$table_name.'.rstat' => 3]])
                ->andWhere("tb_data_1.create_date > :syncdate")
                ->andWhere($table_name.".update_date < :syncdate")
                ->addParams([':syncdate' => $syncfrom]);
            return $query1->all();
        }catch (Exception $e){
            return $e;
        }
    }

    public static function SaveImage($username , $img){
        try{
            $now = WorklistQuery::GetNowSql();
            $userId = WorklistQuery::GetUserIdbyUsername($username);
            $tmpName     = $img['tmp_name'];       // name of the temporary stored file name
//        $fileSize           = $img['size'];   // size of the uploaded file
//        $fileType         = $img['type'];    // file type
            $fp                    = fopen($tmpName, 'r');  // open a file handle of the temporary file
            $imgContent  = fread($fp, filesize($tmpName)); // read the temp file
            fclose($fp); // close the file handle

            $sql = "INSERT INTO image_blob (image, create_date, create_user,rstat) VALUES ( :image , :now , :create_user , 0)";
            Yii::$app->db->createCommand($sql)->bindValue(":now", $now)
                ->bindValue(":image", $imgContent   )
                ->bindValue(":create_user", $userId)
                ->execute();
            return  $id = Yii::$app->db->getLastInsertID();
        }catch (Exception $e){
            return $e;
        }
    }

    public static function GetImageById($id){
        try{
            $query = (new Query()) -> select('image') -> from('image_blob')->where(['id'=>$id]);
            $imgRes = $query->scalar();
            return  $imgRes;
        }catch (Exception $e){
            return $e;
        }
    }

    static function GetNowSql(){
        $sql = "SELECT NOW()";
        $rs = Yii::$app->db->createCommand($sql)->queryOne()['NOW()'];
        return $rs;
    }

    static function GetUserIdbyUsername($uname){
        $userId = (new Query())->select('id')->from('user')->where(['username'=>$uname])->one()['id'];
        if(!$userId){
            $error = [];
            $error["success"] = false ;
            $error["message"] = "user not found!" ;
            print_r( json_encode($error));
            exit();
        }
        return $userId;
    }
    static function GetUserProfileById($user_id){
        $userId = (new Query())->select(['firstname' , 'lastname'])->from('user_profile')->where(['user_id'=>$user_id])->one();
        $username = $userId['firstname'].' '.$userId['lastname'];
        if(!$username){
            $error = [];
            $error["success"] = false ;
            $error["message"] = "user not found!" ;
            print_r( json_encode($error));
            exit();
        }
        return $username;
    }






    /*
     * Old and maybe unuse
    * */

    //put your code here
    public static function GetDefaultUSFinding()
    {
        $siteCode = Yii::$app->user->identity->userProfile->sitecode;
        $user_id=Yii::$app->user->identity->userProfile->user_id;
        $hospital = self::GetHospitalDetail($siteCode);                 // ดึงรายละเอียด ของข้อมูลในโรงพยาบาลนั้นๆ
        $usdet = self::GetUSDetail($siteCode);                          // รายละเอียด เกี่ยวกับการ Ultrasound
        $out['sitecode']=$siteCode;
        $out['userid']=$user_id;
        $out['zone_code'] = $hospital['zone_code'];
        $out['provincecode'] = $hospital['provincecode'];
        $out['amphurcode'] = (strlen($hospital['amphurcode'])==0 ? '01' : $hospital['amphurcode']);
        $out['amphur'] = $hospital['amphur'];
        $out['amphurlist'] = self::GetAmphurListFromProvince($out['provincecode']);
        $out['tamboncode'] = $hospital['tamboncode'];
        $out['hospitalname'] = $hospital['name'];
        $out['hospitallist'] = self::GetHospitalListFromAmphur($out['provincecode'], $out['amphurcode']);
        $out['inputStartDate'] = ( (strlen($usdet['sf2v1'])==0 || $usdet['sf2v1']=='0000-00-00') ? '2013-02-09' : $usdet['sf2v1']);
        $out['inputEndDate'] = ( (strlen($usdet['ef2v1'])==0 || $usdet['ef2v1']=='0000-00-00') ? date('Y-m-d') : $usdet['ef2v1']);
        return $out;
    }

    public static function GetHospitalDetail($hsitecode)
    {
        $sql = "select * from all_hospital_thai where hcode='".$hsitecode."'";
        $out = Yii::$app->db->createCommand($sql)->queryOne();
        return $out;
    }

    public static function GetAmphurListFromProvince($provincecode)
    {
        $sql = "select distinct amphurcode,amphur from all_hospital_thai where provincecode='".$provincecode."' order by amphurcode ";
        $out = Yii::$app->db->createCommand($sql)->queryAll();
        return $out;
    }

    public static function GetHospitalListFromAmphur($provincecode, $amphurcode)
    {
        $sql = "select distinct hcode,name from all_hospital_thai where provincecode='".$provincecode."' and amphurcode='".$amphurcode."' order by hcode ";
        $out = Yii::$app->db->createCommand($sql)->queryAll();
        return $out;
    }

    public static function GetUSDetail($hsitecode)
    {
        $sql = "select min(f2v1) as sf2v1, max(f2v1) as ef2v1 ";
        $sql.= "from tb_data_3 ";
        $sql.= "where hsitecode='".$hsitecode."' or sitecode='".$hsitecode."' ";
        $out = Yii::$app->db->createCommand($sql)->queryOne();
        return $out;
    }
  
}
