<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace frontend\modules\api\v1\classes;

use Yii;
use yii\db\Exception;
use yii\db\Query;
use yii\base\ErrorException;

class WorklistQuery
{
  
    public static function CreateWorklist($username,$title,$sitecode,$start = null,$end = null,$isUserId = true)
    {
        try {
            $now = WorklistQuery::GetNowSql();
            $userId = $isUserId ? $username : WorklistQuery::GetUserIdbyUsername($username);
            if ($start == null)
                $start = $now;
            if ($end == null){
                $end =  date("Y-m-d H:i:s", strtotime($now. ' + 1 days')) ;

            }
            // $end = $end == null ? $end : $now['NOW()'];


            $sql = "INSERT INTO worklist ( title , 
                sitecode , 
                start_date , 
                end_date , 
                create_date , 
                create_user  , 
                update_date , 
                update_user , 
                rstat) 
                VALUES (:title,:sitecode,:start,:end,NOW(),:user,NOW(),:user,'0')";
            Yii::$app->db->createCommand($sql)
                ->bindValue(":title", $title)
                ->bindValue(":sitecode", $sitecode)
                ->bindValue(":user", $userId)
                ->bindValue(":start", $start)
                ->bindValue(":end", $end)
                ->query();
            $id = Yii::$app->db->getLastInsertID();
            $e = WorklistQuery::AddUserById($username,$id,$userId,2,$isUserId,true);
            //var_dump($e);
            //$message = "success";
            return $id;
        }catch (Exception $e){
            return $e;
        }
    }

    public static function GetWorklist( $username ,$isUserId = true)
    {
        try {
            $userId = $isUserId ? $username : WorklistQuery::GetUserIdbyUsername($username);
            $sitecode = (new Query())
                ->select(['sitecode'])
                ->from('user_profile')
                ->where(['user_id' => $userId])->one()["sitecode"];

            $query = (new Query())
                ->select(['worklist.*'])
                ->from('worklist')
                ->where(["sitecode"=>$sitecode]);
//                ->innerJoin('worklist_map_user', 'worklist_map_user.user_id = :userId', [':userId' => $userId])
//                ->andWhere('worklist.id = worklist_map_user.worklist_id');
            $res = $query->all();
            return $res;
        } catch (Exception $e) {
            return $e;
        }
    }

    public static function GetWorklistByDate( $username, $syncdate ,$isUserId = true)
    {
        try {
            $userId = $isUserId ? $username : WorklistQuery::GetUserIdbyUsername($username);
            $sitecode = (new Query())
                ->select(['sitecode'])
                ->from('user_profile')
                ->where(['user_id' => $userId])->one()["sitecode"];
            $query = (new Query())
                ->select(['worklist.*'])
                ->from('worklist')
                ->where(["sitecode"=>$sitecode])
//                ->innerJoin('worklist_map_user','worklist_map_user.user_id = :userId',[':userId'=>$userId])
//                ->andWhere('worklist.id = worklist_map_user.worklist_id')
                ->andWhere("worklist.update_date > :syncdate")->addParams([":syncdate"=>$syncdate]);
            $res = $query->all();
            return $res;
        }catch (Exception $e){
            return $e;
        }
    }

    public static function ApplyDoctorAndRoom($user_id,$room_name,$doctor_code){
        try {
            $now = WorklistQuery::GetNowSql();
            $activeWorklist = self::GetActiveWorklist();
            $res = [];
            $res["result"] = [];
            for($i = 0; $i < count($activeWorklist); ++$i) {
                $worklist_id = $activeWorklist[$i]["id"];

                $queryUser = self::GetUserByWorklistId($worklist_id, $user_id);
                //$worklist_id
                if ($queryUser == null) continue;
                $res_mes = self::UpdateUserInWorklist($user_id,$worklist_id,$user_id,["permission"=>$queryUser["permission"],"room_name"=>$room_name,"doctor_code"=>$doctor_code],true,true);
                array_push($res["result"],$res_mes);
            }
            $res["message"] = "apply to ".count($activeWorklist)." worklist";
            return $res;
        }catch (Exception $e){
            return $e;
        }
    }

    public static function GetWorklistDataById( $worklist_id )
    {

        try {
            $res["worklist"] = (new Query())->select('*')->from('worklist')->where(['id'=>$worklist_id])->one();
            $res["patient_list"] = [];
            $res["patient_list"] = WorklistQuery::GetPatientsByWorklistId($worklist_id);
            $res["user_list"] = [];
            $res["user_list"] = WorklistQuery::GetUsersByWorklistId($worklist_id);
            return $res;
        }catch (Exception $e){
            return $e;
        }
    }

    public static function GetActiveWorklist( )
    {
        try{
            $query = (new Query())->select('*')
                ->from('worklist')->where( "NOW() BETWEEN start_date and end_date")->andWhere('rstat != 3');
            return $query->all();
        }catch (Exception $e){
            return $e;
        }
    }


    public static function RemoveWorklistById($username,$worklist_id, $isUserId  = true){
          //TODO is worklist exist
        try {
            $now = WorklistQuery::GetNowSql();
            $userId = $isUserId ? $username : WorklistQuery::GetUserIdbyUsername($username);
            $user_permission = self::ValidatePermission($userId,$worklist_id);
            if($user_permission < 2)
                return "user not have permission to edit worklist.";

            $sql = "UPDATE worklist SET rstat = 3 , update_date = :now ,update_user = :userId 
            WHERE id = :worklist_id";
            Yii::$app->db->createCommand($sql)
                ->bindValue(":worklist_id", $worklist_id)
                ->bindValue(":now", $now)
                ->bindValue(":userId", $userId)
                ->query();

            WorklistQuery::RemovePatientByWorklistId($username, $worklist_id);
            $message = "success";
            return $message;

        }catch (Exception $e){
            return $e;
        }
    }

    public static function AddPatientArrById($username , $worklist_id , $ptids , $isUserId  = true)
    {
        //TODO is user exist
        //TODO is worklist_id exist
        $now = WorklistQuery::GetNowSql();
        try {
            $userId = $isUserId ? $username : WorklistQuery::GetUserIdbyUsername($username);
            $user_permission = self::ValidatePermission($userId,$worklist_id);
            if($user_permission < 1)
                return "user not have permission to edit Patient.";
            if(!$ptids || $ptids == "")
                return "please insert ptdi.";
//            $sql = "SELECT count(*) FROM worklist_map_patient WHERE worklist_id = :worklist_id AND ptid = :ptid AND rstat != 3";
//            $res_count = Yii::$app->db->createCommand($sql)
//                ->bindValue(":ptid", $ptid)
//                ->bindValue(":worklist_id", $worklist_id)
//                ->queryOne();
//            if ($res_count['count(*)'] == '1')
//                return "success with dulpicated record.";
            $sqlValue = "";
            //LogStash::Log("AddPatientArrById",var_export($ptids,true),"");
            for($i = 0; $i < count($ptids); ++$i) {
                $sqlValue = $sqlValue."(".$ptids[$i].",:worklist_id,NOW(),:user,NOW(),:user,'0'),";
            }
            //LogStash::Log("AddPatientArrById",var_export($sqlValue,true),"");

            $sqlValue = substr($sqlValue, 0, -1);
            $sql = "INSERT INTO worklist_map_patient ( ptid , 
                        worklist_id ,
                        create_date , 
                        create_user  , 
                        update_date , 
                        update_user , 
                        rstat) 
                        VALUES ".$sqlValue;
            //LogStash::Log("AddPatientArrById",$sql,"");
            Yii::$app->db->createCommand($sql)
                ->bindValue(":user", $userId)
                ->bindValue(":worklist_id", $worklist_id)
                ->query();
            $message = "success";
            self::UpdateWorklist($userId,$worklist_id);

            return $message;
        }catch (Exception $e) {
            LogStash::Error("AddPatientArrById",$e,"");
            return $e;
        }catch (ErrorException $e){
            LogStash::ErrorEx("AddPatientArrByIde",$e,"");
            return $e;
        }
    }

    public static function AddPatientById($username , $worklist_id , $ptid , $isUserId  = true)
    {
          //TODO is user exist
            //TODO is worklist_id exist
        $now = WorklistQuery::GetNowSql();
        try {
            $userId = $isUserId ? $username : WorklistQuery::GetUserIdbyUsername($username);
            $user_permission = self::ValidatePermission($userId,$worklist_id);
            if($user_permission < 1)
                return "user not have permission to edit Patient.";
            if(!$ptid || $ptid == "")
                return "please insert ptdi.";
            $sql = "SELECT count(*) FROM worklist_map_patient WHERE worklist_id = :worklist_id AND ptid = :ptid AND rstat != 3";
            $res_count = Yii::$app->db->createCommand($sql)
                ->bindValue(":ptid", $ptid)
                ->bindValue(":worklist_id", $worklist_id)
                ->queryOne();
            if ($res_count['count(*)'] == '1')
                return "success with dulpicated record.";
            $sql = "INSERT INTO worklist_map_patient ( ptid , 
                        worklist_id ,
                        create_date , 
                        create_user  , 
                        update_date , 
                        update_user , 
                        rstat) 
                        VALUES (:ptid,:worklist_id,NOW(),:user,NOW(),:user,'0')";
            Yii::$app->db->createCommand($sql)
                ->bindValue(":ptid", $ptid)
                ->bindValue(":user", $userId)
                ->bindValue(":worklist_id", $worklist_id)
                ->query();
            self::UpdateWorklist($userId,$worklist_id);

            $message = "success";
            return $message;
        }catch (Exception $e) {
            return $e;
        }
    }

//mobile call lite
    public static function GetPatientByWorklistId($worklist_id , $ptid )
    {
            //TODO is worklist_id exist
        try {
            $query = (new Query())
                ->select(['worklist_map_patient.ptid','worklist_map_patient.create_date','worklist_map_patient.create_user',
                'worklist_map_patient.update_date','worklist_map_patient.update_user','worklist_map_patient.rstat'])
                ->from('worklist_map_patient')
                ->where(['worklist_id'=>$worklist_id,'ptid'=>$ptid])->andWhere('worklist_map_patient.rstat != 3');
            return $query->one();
        }catch (Exception $e){
            return $e;
        }
    }

    public static function GetPatientsByWorklistId($worklist_id )
    {
        //TODO is worklist_id exist
        try {
            $query = (new Query())
                ->select(['worklist_map_patient.ptid','worklist_map_patient.create_date','worklist_map_patient.create_user',
                    'worklist_map_patient.update_date','worklist_map_patient.update_user','worklist_map_patient.rstat'])
                ->from('worklist_map_patient')
                ->where(['worklist_id'=>$worklist_id])->andWhere('worklist_map_patient.rstat != 3');
            return $query->all();
        }catch (Exception $e){
            return $e;
        }
    }

    /**
     * Add log
     * @param $ptid
     * @param $user_id
     * @param $room_name
     * @param $doctor_code
     * @return \Exception|string|Exception
     * @internal param $worklist_id
     */
    public static function AddLog($ptid,$user_id,$room_name,$doctor_code){
            //if worklist+id have user // have ptid
            //then stamp doctor in
        try {
            $now = WorklistQuery::GetNowSql();
            $activeWorklist = self::GetActiveWorklist();

            for($i = 0; $i < count($activeWorklist); ++$i) {
                $worklist_id = $activeWorklist[$i]["id"];
                
                $queryUser = self::GetUserByWorklistId($worklist_id,$user_id);
//                $worklist_id
                if($queryUser ==  null)
                    self::AddUserById($user_id,$worklist_id,$user_id,"0",true,true);

                self::UpdateUserInWorklist($user_id,$worklist_id,$user_id,["room_name"=>$room_name,"doctor_code"=>$doctor_code],true,true);
                $queryPatient = self::GetPatientByWorklistId($worklist_id,$ptid);
                if($queryPatient ==  null) continue;
//                $queryDoctor = (new Query())->select("*")->from('doctor_all')->where(['doctorcode'=>$doctor_code])->one();

//                if($queryDoctor ==  null) continue;
                // room doctor
                //find room_id by user id
                //update patient room
                $us_room = " SELECT * FROM us_room WHERE  user_id=:user_id AND worklist_id=:worklist_id ";
                $resRoom = \Yii::$app->db->createCommand($us_room, [':user_id'=>$user_id,':worklist_id'=>$worklist_id])->queryOne();
                $room_name = $resRoom['room_name'];
                
                $updateRoom = " UPDATE worklist_map_patient SET room_name=:room_name WHERE ptid=:ptid AND worklist_id=:worklist_id  ";
                $resUpdate = \Yii::$app->db->createCommand($updateRoom, [':room_name'=>$room_name,':ptid'=>$queryPatient['ptid'],':worklist_id'=>$worklist_id])->execute();
                LogStash::Log("AddLogCheckRoom", $room_name, var_export($resUpdate,true));

                Yii::$app->db->createCommand()->insert("worklist_map_log",[
                    "worklist_id"=>$worklist_id,
                    "room_name"=>$room_name,
                    "user_id"=>$user_id,
                    "doctor_code"=>$doctor_code,
                    "ptid"=>$ptid,
                    "create_date"=>$now
                ]) ->execute();
            }
            LogStash::Log("AddLog","done","");
        }catch (Exception $e){

            LogStash::Error("AddLog",$e,"");
            return $e;
        }
    }

    public static function GetPatientFullByWorklistId($worklist_id) {
        try {
            $querySitecode = (new Query())
                ->select(['sitecode'])
                ->from('worklist')
                ->where(['id' => $worklist_id]);
            $sitecode = $querySitecode->one()["sitecode"];
            $query = (new Query())
                ->select(['worklist_id', 'wmp.ptid','tb1.ptcodefull', 'wmp.create_date', 'wmp.create_user',
                    'wmp.update_date', 'wmp.update_user', 'wmp.rstat',
                    'tb1.title', 'tb1.name', 'tb1.surname'])
                ->from('worklist_map_patient wmp')
                ->innerJoin('tb_data_1 tb1', 'wmp.ptid=tb1.ptid')
                ->where(['wmp.worklist_id' => $worklist_id, 'tb1.hsitecode' => $sitecode])
                ->andWhere('wmp.rstat <> 3')
                ->orderBy('tb1.ptcodefull');

            return $query->all();
        } catch (Exception $e) {
            return $e;
        }
    }

    public static function RemovePatientByWorklistId($username , $worklist_id ,$isUserId = true)
    {
            //TODO is worklist_id exist
        try {
            $now = WorklistQuery::GetNowSql();
            $userId = $isUserId ? $username : WorklistQuery::GetUserIdbyUsername($username);
            $user_permission = self::ValidatePermission($userId,$worklist_id);
            if($user_permission < 1)
                return "user not have permission to edit Patient.";
            $sql = "UPDATE worklist_map_patient SET rstat = 3 , update_date = :now ,update_user = :userId 
            WHERE worklist_id = :worklist_id";
            Yii::$app->db->createCommand($sql)
                ->bindValue(":worklist_id", $worklist_id)
                ->bindValue(":now", $now)
                ->bindValue(":userId", $userId)
                ->query();
            self::UpdateWorklist($userId,$worklist_id);

            $message = "success";
            return $message;
        }catch (Exception $e){
            return $e;
        }
    }

    public static function RemovePatientByPtid($username , $worklist_id , $ptid, $isUserId = true)
    {
        //TODO is user exist
        //TODO is worklist_id exist
        try {
            $now = WorklistQuery::GetNowSql();
            $userId = $isUserId ? $username : WorklistQuery::GetUserIdbyUsername($username);
            $user_permission = self::ValidatePermission($userId,$worklist_id);
            if($user_permission < 1)
                return "user not have permission to edit Patient.";
            $sql = "UPDATE worklist_map_patient SET rstat = 3 , update_date = :now ,update_user = :userId 
            WHERE worklist_id = :worklist_id AND ptid = :ptid";
            Yii::$app->db->createCommand($sql)
                ->bindValue(":worklist_id", $worklist_id)
                ->bindValue(":ptid", $ptid)
                ->bindValue(":now", $now)
                ->bindValue(":userId", $userId)
                ->query();
            self::UpdateWorklist($userId,$worklist_id);

            $message = "success";
            return $message;
        }catch (Exception $e){
            return $e;
        }
    }

    public static function ValidatePermission($userId,$worklist_id)
    {
        return intval( (new Query())
            ->select(['permission'])
            ->from('worklist_map_user')
            ->where(['worklist_id'=>$worklist_id , 'user_id'=>$userId])
            ->andWhere('rstat != 3')->one()["permission"]);
    }

    /**
     * @param $username
     * @param $worklist_id
     * @param $add_user
     * @param int $permission 0 read 1 edit 2 admin
     * @param bool $isUserId
     * @return \Exception|string|Exception
     */
    public static function AddUserById($username , $worklist_id , $add_user , $permission = '0', $isUserId = true, $isSystem = false)
    {
        //TODO is user exist
        //TODO is worklist_id exist
        try {
            $now = WorklistQuery::GetNowSql();
            $userId = $isUserId ? $username : WorklistQuery::GetUserIdbyUsername($username);
            $user_permission = self::ValidatePermission($userId,$worklist_id);

            if($user_permission < 2 && !$isSystem)
                return "user not have permission to add new user.";
            $sql = "SELECT count(*) FROM worklist_map_user WHERE worklist_id = :worklist_id AND user_id = :user_id AND rstat != 3";
            $res_count = Yii::$app->db->createCommand($sql)
                ->bindValue(":user_id", $add_user)
                ->bindValue(":worklist_id", $worklist_id)
                ->queryOne();
            if ($res_count['count(*)'] == '1')
                return "success with dulpicated record.";

            $sql = "INSERT INTO worklist_map_user ( 
                user_id , 
                worklist_id ,
                create_user  , 
                create_date  , 
                update_user , 
                update_date , 
                permission ,
                rstat) 
                VALUES (:user_id,:worklist_id,:user,:now,:user,:now,:permission,'0')";
            Yii::$app->db->createCommand($sql)
                ->bindValue(":user_id", $add_user)
                ->bindValue(":user", $userId)
                ->bindValue(":now", $now)
                ->bindValue(":permission", $permission)
                ->bindValue(":worklist_id", $worklist_id)
                ->query();
            self::UpdateWorklist($userId,$worklist_id);
            $message = "success";
            return $message;
        }catch (Exception $e){
            return $e;
        }
    }

    public static function UpdateWorklist($username , $worklist_id,$arg = [], $isUserId = true, $isSystem = false)
    {
        try {
            $now = WorklistQuery::GetNowSql();
            $userId = $isUserId ? $username : WorklistQuery::GetUserIdbyUsername($username);
            $input_arr = [];
            if($arg["start_date"] && $arg["start_date"] != null)
                $input_arr["start_date"] = $arg["start_date"];
            if($arg["end_date"] && $arg["end_date"] != null)
                $input_arr["end_date"] = $arg["end_date"];

            $input_arr["update_date"] = $now;
            $input_arr["update_user"] = $userId;

                $input_arr["rstat"] = 1;
            Yii::$app->db->createCommand()
                ->update("worklist",$input_arr,['id'=>$worklist_id,'rstat'=>[0,1]])
                ->execute();
        } catch (Exception $e) {

            return $e->errorInfo;
        }
    }

    public static function UpdateUserInWorklist($username , $worklist_id ,$target_user , $arg = [], $isUserId = true, $isSystem = false)
    {
        //$permission
        try {
            $now = WorklistQuery::GetNowSql();
            $input_arr = [];
            $userId = $isUserId ? $username : WorklistQuery::GetUserIdbyUsername($username);
            if($arg["permission"] && $arg["permission"] != null)
                $input_arr["permission"] = $arg["permission"];
            if($arg["room_name"] && $arg["room_name"] != null)
                $input_arr["room_name"] = $arg["room_name"];
            if($arg["doctor_code"] && $arg["doctor_code"] != null)
                $input_arr["doctor_code"] = $arg["doctor_code"];
            $input_arr["update_user"] = $username;
            $input_arr["update_date"] = $now;
            $input_arr["rstat"] = 1;
            LogStash::Log("UpdateUserInWorklist","input",var_export($input_arr,true));



            $user_permission = self::ValidatePermission($userId,$worklist_id);

            if($user_permission < 2 && !$isSystem )
                return "user not have permission to edit user.";

            try {
                Yii::$app->db->createCommand()
                    ->update("worklist_map_user",$input_arr,['worklist_id'=>$worklist_id,'user_id' => $target_user,'rstat'=>[0,1]])
                    ->execute();
                $message = "success_id : ".$worklist_id;
            } catch (Exception $ex) {
                $message = "error_id : ".$worklist_id;
            }
            LogStash::Log("UpdateUserInWorklist",$message,"");
            self::UpdateWorklist($userId,$worklist_id);

            return $message ;
        } catch (Exception $e) {
            LogStash::Error("UpdateUserInWorklist",$e,"");
            return $e->errorInfo;
        }
    }

    public static function RemoveUserInWorklist($username , $worklist_id , $rm_user , $isUserId = true)
    {
        try {
            $now = WorklistQuery::GetNowSql();
            $userId = $isUserId ? $username : WorklistQuery::GetUserIdbyUsername($username);
            $user_permission = self::ValidatePermission($userId,$worklist_id);
            if($user_permission < 2)
                return "user not have permission to add new user.";
            $sql = "UPDATE worklist_map_user SET
                    update_user = :user, 
                    update_date = :now,
                    permission = 0,
                    rstat = '3' 
                    WHERE worklist_id = :worklist_id AND user_id = :user_id AND rstat != 3";

            Yii::$app->db->createCommand($sql)
                ->bindValue(":user_id",$rm_user )
                ->bindValue(":user",$userId)
                ->bindValue(":now",$now)
                ->bindValue(":worklist_id",$worklist_id )
                ->query();
            $message = "success";
            self::UpdateWorklist($userId,$worklist_id);

            return $message ;
        } catch (Exception $e) {
            return $e->errorInfo;
        }

    }

    public static function GetUserByWorklistId($worklist_id , $user_id)
    {
        //TODO is worklist_id exist
        try{
             $query = (new Query())->select('*')
                 ->from('worklist_map_user')->where(['worklist_id'=>$worklist_id,'user_id'=>$user_id])->andWhere('rstat != 3');
            return $query->one();
        }catch (Exception $e){
            return $e;
        }
    }

    public static function GetUsersByWorklistId($worklist_id )
    {
        //TODO is worklist_id exist
        try{
            $query = (new Query())->select(['worklist_id','user_id','create_date'])
                ->from('worklist_map_user')->where(['worklist_id'=>$worklist_id])->andWhere('rstat != 3');
            return $query->all();
        }catch (Exception $e){
            return $e;
        }
    }

    public static function GetUserFullByWorklistId($worklist_id )
    {
        //TODO is worklist_id exist
        try{
            //SELECT worklist_map_user.user_id,user.username,user.email FROM worklist_map_user inner join user on user.id = worklist_map_user.user_id WHERE worklist_id = 24 AND rstat != 3
            $sql = "SELECT wmu.worklist_id,wmu.user_id,wmu.create_date,wmu.permission,wmu.create_user , usp.firstname, usp.lastname
                FROM worklist_map_user wmu INNER JOIN user_profile usp ON wmu.user_id=usp.user_id WHERE worklist_id = :worklist_id AND rstat != 3";
            return Yii::$app->db->createCommand($sql) ->bindValue(":worklist_id",$worklist_id)->queryAll();
        }catch (Exception $e){
            return $e;
        }
    }



    static function GetNowSql(){
        $sql = "SELECT NOW()";
        $rs = Yii::$app->db->createCommand($sql)->queryOne()['NOW()'];
        return $rs;
    }

    static function GetUserIdbyUsername($uname){
        $query = (new Query())
            ->select(['id'])
            ->from('user')
            ->where(['username' => $uname]);
        $userId = $query->one()['id'];

        if(!$userId){
            $error = [];
            $error["success"] = false ;
            $error["message"] = "user not found!" ;
            print_r( json_encode($error));
            exit();
        }
        return $userId;
    }
    
    static function GetUserbyId($user_id){
        $sql = "SELECT firstname, lastname FROM user_profile WHERE user_id = :user_id";
        $userId = Yii::$app->db->createCommand($sql)->bindValue(":user_id",$user_id)->queryOne();
        $username = $userId['firstname'].' '.$userId['lastname'];
        if(!$username){
            $error = [];
            $error["success"] = false ;
            $error["message"] = "user not found!" ;
            print_r( json_encode($error));
            exit();
        }
        return $username;
    }


}