<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace frontend\modules\api\v1\classes;

#use yii\helpers\VarDumper;
#use yii\web\Controller;
use Yii;

class QueryCID {
    public static function getListPatienByCID($cid = null)
    {
        if(strlen(trim($cid))>0 ){
            $sql = "select id,ptid,hsitecode,hptcode,cid,title,name,surname ";
            $sql.= ",case 1 when reg.confirm is null or length(reg.confirm)=0 then '0: ยังไม่ Upload ใบยินยอม หรือข้อมูลใน CCA-01 ไม่สมบูรณ์ (รอตรวจ)' when reg.confirm='0' then '1: ตรวจไม่ผ่าน เอกสารไม่สมบูรณ์' when reg.confirm>0 then '2: ICF ตรวจผ่านแล้ว' end as 'icf_check' ";
            $sql.= ",case 1 when reg.rstat='0' then '0: ข้อมูลใหม่' when reg.rstat='1' then '1: Wating' when reg.rstat='2' then '2: Submitted' when reg.rstat>3 then '4: Submitted' else ' - ' end as rstat ";
            $sql.= ",create_date, update_date ";
            $sql.= "from tb_data_1 reg ";
            $sql.= "where cid=:cid ";
            //echo $sql;
            $dataProvider = Yii::$app->db->createCommand($sql,[":cid"=>$cid])->queryAll();
            return $dataProvider;
        }
    }
    
    public static function checkNewCIDExist($newcid){
        if( strlen(trim($newcid))>0){
            $return = true;
            $sql = "select count(*) as recs ";
            $sql.= "from tb_data_1 ";
            $sql.= "where rstat<>'3' ";
            $sql.= "and cid=:cid ";
            $dataCount = Yii::$app->db->createCommand($sql,[":cid"=>$newcid])->queryOne();
            //echo $dataCount['recs'];
            if($dataCount['recs']>0){
                $return = false;
            }else{
                $return = true;
            }
        }else{
            $return = false;
        }
        return $return;
    }

    public function updateCIDByPTID($newcid,$oldcid){
        #   1. กระบวนการคือ 1 เก็บค่าเก่าไว้ 
        #   2. Update ค่าใหม่ที่ถูกต้องลงไป
        if(strlen($oldcid)>0 && strlen($newcid)>0 ){
            $user_id=Yii::$app->user->identity->userProfile->user_id;
            $user_firstname=Yii::$app->user->identity->userProfile->firstname;
            $user_lastname=Yii::$app->user->identity->userProfile->lastname;
            $user_name = $user_firstname." ".$user_lastname;
            // save to log
            $sql = "replace into cascap_log.cid_change_log ";
            $sql.= "select NOW(),id,ptid,cid,:newcid,hsitecode,hptcode,title,name,surname,sitecode,ptcode,:user_id,:user_name ";
            $sql.= "from tb_data_1 ";
            $sql.= "where cid=:oldcid ";
            $sql.= "order by create_date ";
            //echo $sql;
            Yii::$app->db->createCommand($sql,[":newcid"=>$newcid,":oldcid"=>$oldcid,":user_id"=>$user_id,":user_name"=>$user_name])->execute();
            // update 
            $sqlupdate = "update tb_data_1 ";
            $sqlupdate.= "set cid=:newcid ";
            $sqlupdate.= "where cid=:oldcid ";
            //echo $sql;
            Yii::$app->db->createCommand($sqlupdate,[":newcid"=>$newcid,":oldcid"=>$oldcid])->execute();
        }
        
        
    }
    
    public static function historyUpdateCID($cid){
        if(strlen(trim($cid))>0){
            $sqlgetptid = "select ptid ";
            $sqlgetptid.= "from cascap_log.cid_change_log ";
            $sqlgetptid.= "where cid=:cid ";
            $sqlgetptid.= "or cid_new=:cid ";
            $sqlgetptid.= "limit 1 ";
            $dataPTID = Yii::$app->db->createCommand($sqlgetptid,[":cid"=>$cid])->queryOne();
            if($dataPTID['ptid']>0){
                $vptid = $dataPTID['ptid'];
            }else{
                $vptid = "";
            }
        }
        
        if( strlen($vptid)>0 ){
            $sql = "select distinct dchange,ptid,user_id,user_name,cid,cid_new ";
            $sql.= "from cascap_log.cid_change_log ";
            $sql.= "where ptid=:ptid ";
            $sql.= "order by dchange desc ";
            $dataHistory = Yii::$app->db->createCommand($sql,[":ptid"=>$vptid])->queryAll();
            return $dataHistory;
        }
    }
}

