<?php
/**
 * Created by PhpStorm.
 * User: kawin
 * Date: 8/24/2017
 * Time: 11:24 AM
 */


namespace frontend\modules\api\v1\classes;
use Yii;
use \yii\base\Exception;
use yii\base\ErrorException;


class LogStash
{
    private static function SaveLog($type,$name,$input,$res){
        try{
            Yii::$app->db->createCommand()->insert("log_api",[
                "type"=>$type,
                "name"=>$name,
                "input"=>$input,
                "result"=>$res
            ])->execute();
        }catch (Exception $e){
            return $e;
        }catch (ErrorException $e){
            return $e;
        }
    }

    public static function Log($name,$input,$res){
        self::SaveLog("log",$name,$input,$res);
    }

    public static function Info($name,$input,$res){
        self::SaveLog("info",$name,$input,$res);
    }

    public static function Error($name,Exception $execption,$res){
        $input = ["message" => $execption->getMessage()];
        $input["code"] = $execption->getCode();
        self::SaveLog("error",$name,var_export($input,true),$res);
    }

    public static function ErrorEx($name,\Exception $execption,$res){
        $input = ["message" => $execption->getMessage()];
        self::SaveLog("error",$name,var_export($input,true),$res);
    }
}