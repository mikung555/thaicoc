<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace frontend\modules\api\v1\classes;

use Yii;
use yii\helpers\VarDumper;
use common\lib\codeerror\helpers\GenMillisecTime;
use backend\modules\ezforms\components\EzformQuery;
use yii\db\Exception;
use yii\base\ErrorException;
use backend\models\EzformTarget;
use yii\db\Expression;
use frontend\modules\api\v1\classes\MainQuery;
use yii\db\Query;


class EzForm
{
    public static function CreateNewRecord($user_id, $ezf_id, $source_site, $target, $submit, $data_id, $data)
    {
        try{

            $model_fields = EzformQuery::getFieldsByEzf_id($ezf_id);
            $table_name = EzformQuery::getFormTableName($ezf_id);

            //model_form assign value
            $model = new \backend\modules\ezforms\models\EzformDynamic($table_name->ezf_table);
            $fileds = MainQuery::GetEzFieldsOnly($ezf_id);
            $arr = array();
            $data = (array)$data;

            for ($i=0; $i < count($fileds); $i++) {
                $field_name =  $fileds[$i]['ezf_field_name'];
                $arr[ $field_name ] = $data[ $field_name ];
            }

            $tempArr =  $model->attributes;
            foreach ($arr as $key => $value) {
                $tempArr[$key]  = $arr[$key] ;
            }

            $tempArr["user_create"] = $data[ "user_create" ];
            $tempArr["create_date"]= $data[ "create_date" ];
            $tempArr["hsitecode"] = $data[ "hsitecode" ];
            $tempArr["hptcode"]= $data[ "hptcode" ];
            $tempArr["hncode"]= $data[ "hncode" ];
            $tempArr["ptid"]= $data[ "ptid" ];
            $tempArr["ptcode"]= $data[ "ptcode" ];
            $tempArr["ptcodefull"]  = $data[ "ptcodefull" ];
            $tempArr["sitecode"] = $data[ "sitecode" ];

            $submit ?  $rstat = 2 : $rstat = 1;
            $previousRecord = null;
            if ($data_id == null) {
                $field_id = GenMillisecTime::getMillisecTime();
                $model->user_create = $user_id;
                $model->create_date = new Expression('NOW()');
            } else {
                $field_id = $data_id;
                $previousRecord = (new Query())->select('*')->from($table_name->ezf_table)->where(['id' => $field_id])->one();
            }
//            VarDumper::dump(  $tempArr );
            $model->attributes = $tempArr;
            $model->id = $field_id;
            $model->rstat = $rstat;
            $model->user_create = $data[ "user_create" ];
            $model->create_date = $data[ "create_date" ];
            $model->user_update = $user_id;
            $model->update_date = new Expression('NOW()');
            $model->target = $target;
            $model->xsourcex = $source_site; //hospitalcode
            $myVarDumper=var_export($model_fields, true);
            //return $myVarDumper;
            //VarDumper::dump(  $model_fields );


                 //is_new_record
            if($previousRecord == null){
                if ($model->save()) {
                    //save EMR
                    $model = new EzformTarget();
                    $model->ezf_id = $ezf_id;
                    $model->data_id = $data_id;
                    $model->target_id = $target;
                    $model->user_create = $user_id;
                    $model->create_date = new Expression('NOW()');
                    $model->user_update = $user_id;
                    $model->update_date = new Expression('NOW()');
                    $model->rstat = $rstat;
                    $model->xsourcex = $source_site;
                    $model->save();
                    //--//--
                    Yii::$app->db->createCommand()
                        ->update("tb_data_3", ["hptcodefull"=> $data[ "hsitecode"].$data[ "hptcode"]] ,['id'=>$field_id])
                        ->execute();
                    $log = [ "id" => "" , "rstat" => $rstat];
                    LogStash::Info("CreateNewRecord",var_export($log, true),"added");

                    return $field_id;
                }else{
                    $e =  new ErrorException("cannot save data.");
                    LogStash::ErrorEx("CreateNewRecord",$e,"error#0");
                    throw $e;
                }
            }else{
                $model->update();
                //save EMR
//                $model = new EzformTarget();
//
//                $model->ezf_id = $ezf_id;
//                $model->data_id = $data_id;
//                $model->target_id = $target;
//                $model->user_create = $user_id;
//                $model->create_date = new Expression('NOW()');
//                $model->user_update = $user_id;
//                $model->update_date = new Expression('NOW()');
//                $model->rstat = $rstat;
//                $model->xsourcex = $source_site;
//                $model->save();
                //--//--
                Yii::$app->db->createCommand()
                    ->update("tb_data_3", ["hptcodefull"=> $data[ "hsitecode"].$data[ "hptcode"]] ,['id'=>$field_id])
                    ->execute();
                $log = [ "id" => "" , "rstat" => $rstat];
                LogStash::Info("CreateNewRecord",var_export($log, true),"updated");
                return $field_id;
            }

        }catch (Exception $e){
            LogStash::Error("CreateNewRecord",$e,"error#1");
            return $e;
        }catch (\Exception $e){
            LogStash::ErrorEx("CreateNewRecord",$e,"error#");
            return $e;
        }
    }

   /* public function actionSaveData($ezf_id, $target, $data, $submit = false, $id = null)
    {

        // No id mean create new
        // No id with submit create and submit
        if ($id != null) {
            if ($submit) {
                //save submit
                $rstat = 2;
            } else {
                //save draft
                $rstat = 1;
            }

      //replace data set
//            if ($params) {
//                foreach ($params as $key => $value) {
//                    $model_form->{$key} = $value;
//                }
//            }

            $model_fields = EzformQuery::getFieldsByEzf_id($ezf_id);
            $model_form = EzformFunc::setDynamicModelLimit($model_fields);
            $table_name = EzformQuery::getFormTableName($ezf_id);
            if ($model_form->load(Yii::$app->request->post())) {
                $model_form->attributes = $_POST['SDDynamicModel'];

                $model = new \backend\modules\ezforms\models\EzformDynamic($table_name->ezf_table);
                $modelOld = $model->find()->where('id = :id', ['id' => $id])->One();

                //if click final save (rstat not change)
                if (Yii::$app->request->post('finalSave')) {
                    $rstat = $modelOld->rstat;
                    $xsourcex = $modelOld->xsourcex;
                } else {
                    $xsourcex = Yii::$app->user->identity->userProfile->sitecode; //hospitalcode
                }
                //VarDumper::dump($modelOld->attributes,10,true);
                //VarDumper::dump($model_form->attributes,10,true);
                $model->attributes = $model_form->attributes;
                $model->id = $id; //ห้ามเอาออก ไม่งั้นจะ save ไม่ได้
                $model->rstat = $rstat;
                $model->xsourcex = $xsourcex;
                //$model->create_date = new Expression('NOW()');
                $model->user_update = Yii::$app->user->id;
                $model->update_date = new Expression('NOW()');
                //echo $rstat;

                if ($target != 'skip' && $target != 'all') {
                    //จริงๆถ้าไม่มีการเปลี่ยนเป้าหมาย ให้ไปอัพเดจใน ezform_target ด้วย
                    $ezform = EzformQuery::checkIsTableComponent($ezf_id);
                    if ($ezform['special']) {
                        // something
                    } else {
                        $model->target = $taget;
                    }
                }

                //end track change
                //send email to user key
                if (\backend\controllers\InputdataController::checkUserConsultant(Yii::$app->user->id, $_POST['ezf_id']) && $modelOld->user_create) {
                    \backend\controllers\InputdataController::consultNotify($EzformReply, $modelOld->user_create);
                }
                $model->update();
            } else {
            }
        }
    }*/
}
