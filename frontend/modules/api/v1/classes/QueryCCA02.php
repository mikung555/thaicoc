<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace frontend\modules\api\v1\classes;

/**
 * Description of QueryCCA02
 *
 * @author chaiwat
 */
use Yii;

class QueryCCA02 {
    //put your code here
    public static function getListDuplicate60()
    {
        if( TRUE ){
            $sql = "select hsitecode,hptcode,f2v1,count(*) as recs ";
            $sql.= "from tb_data_3 ";
            $sql.= "where f2v1 between '2016-10-01' and NOW() ";
            $sql.= "and rstat in ('1','2') ";
            $sql.= "group by hsitecode,hptcode,f2v1 having recs>1 ";
            $sql.= "order by hsitecode,hptcode,f2v1 ";
            //echo $sql;
            $dataProvider = Yii::$app->db->createCommand($sql)->queryAll();
            return $dataProvider;
        }
    }
    
    public static function getListVisit($hsitecode, $hptcode, $f2v1)
    {
        if( TRUE ){
            $ezf_id = "1437619524091524800";
            $sql = "select id,ptid,target,hsitecode,hptcode,f2v1,'$ezf_id' as ezf_id,rstat ";
            $sql.= "from tb_data_3 ";
            $sql.= "where f2v1 = :f2v1 ";
            $sql.= "and rstat in ('1','2') ";
            $sql.= "and hsitecode = :hsitecode ";
            $sql.= "and hptcode = :hptcode ";
            $sql.= "order by hsitecode,hptcode,f2v1 ";
            //echo $sql;
            $dataProvider = Yii::$app->db->createCommand($sql,[":hsitecode"=>$hsitecode, ":hptcode"=>$hptcode, ":f2v1"=>$f2v1])->queryAll();
            return $dataProvider;
        }
    }
    
    
}
