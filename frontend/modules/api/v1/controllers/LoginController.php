<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace frontend\modules\api\v1\controllers;
use yii\web\Controller;
use yii\helpers\Json;
use yii\helpers\VarDumper;
use Yii;
use frontend\modules\api\v1\classes\MainQuery;
use frontend\modules\api\v1\classes\PdQuery;

/**
 * THE CONTROLLER ACTION
 */
class LoginController extends Controller
{

    public function beforeAction($action)
    {
        header("Access-Control-Allow-Origin: *", false);
        $this->enableCsrfValidation = false;
        return true;
    }

    public function actionIndex()
    {
        header("Access-Control-Allow-Methods: POST");
        $username = $_SERVER['PHP_AUTH_USER'];
        $headers = Yii::$app->request->headers;
        if ($headers->has('x-token')) {
            $token = $headers->get('x-token');
            //TODO fix later
            print_r(json_encode(MainQuery::LoginWithToken( $username, $token)));
        } else {
            $password = $_SERVER['PHP_AUTH_PW'];
            print_r(json_encode(MainQuery::Login( $username, $password)));
        }
    }

    public function actionPd()
    {
        header("Access-Control-Allow-Methods: POST");
        $username = $_SERVER['PHP_AUTH_USER'];
        $headers = Yii::$app->request->headers;
        if ($headers->has('x-token')) {
            $token = $headers->get('x-token');
            //TODO fix later
            print_r(json_encode(PdQuery::LoginWithToken( $username, $token)));
        } else {
            $password = $_SERVER['PHP_AUTH_PW'];
            print_r(json_encode(PdQuery::Login( $username, $password)));
        }
    }
}