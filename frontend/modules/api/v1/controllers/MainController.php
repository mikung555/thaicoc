<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace frontend\modules\api\v1\controllers;

use yii\helpers\Json;
use yii\helpers\VarDumper;
use Yii;
use frontend\modules\api\v1\classes\QueryCCA02;
use frontend\modules\api\v1\classes\MainQuery;
use frontend\modules\api\v1\classes\EzForm;

/**
 * THE CONTROLLER ACTION
 */
class MainController extends BaseApiController
{
    // public $enableCsrfValidation = false;

    public $username = null;
    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        header("Access-Control-Allow-Origin: *", false);
        header("Access-Control-Allow-Headers: Origin, Content-Type,x-token");
        $this->username =  $this->validateUser();
        return parent::beforeAction($action);
    }

    public function actionUploadImage(){
        header("Access-Control-Allow-Methods: POST");
        $image = $_FILES['file'];
        $result = MainQuery::SaveImage($this->username, $image);
        $this->createResponseByVerifyData($result);
    }

    public function actionGetHospitalInfo(){
        header("Access-Control-Allow-Methods: POST");
        $hcode = Yii::$app->request->post('hcode', null);
        $result = MainQuery::GetHospitalInfo( $hcode);
        $this->createResponseByVerifyData($result);
    }

    public function actionGetAllDoctor(){
        header("Access-Control-Allow-Methods: POST");
        $result = MainQuery::GetDoctors();
        $this->createResponseByVerifyData($result);
    }

    public function actionFindDoctor(){
        header("Access-Control-Allow-Methods: POST");
        $name = Yii::$app->request->post('name', null);
        $result = MainQuery::FindDoctor($name);
        $this->createResponseByVerifyData($result);
    }


    public function actionGetImage(){
        header("Access-Control-Allow-Methods: GET");

        $id = Yii::$app->request->get('id', null);
        if($id == null)
        {
            print_r("this function require id");
            exit();
        }
        $res = MainQuery::GetImageById($id);
        echo '<img src="data:image/jpeg;base64,'.base64_encode($res).'"/>';
    }
}