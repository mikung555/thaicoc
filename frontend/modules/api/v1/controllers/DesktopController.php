<?php

namespace frontend\modules\api\v1\controllers;

use yii\helpers\Json;
use yii\helpers\VarDumper;
use yii\web\Controller;
use Yii;
use frontend\modules\api\v1\classes\QueryCCA02;
use frontend\modules\api\v1\classes\MainQuery;
use frontend\modules\api\v1\classes\EzForm;

class DesktopController extends Controller {

    public function beforeAction($action) {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }
    public function actionIndex(){
        echo 'Hello';
    }

    public function actionGetEzform() {
        header("Access-Control-Allow-Origin: *", false);
        $nut_token = \Yii::$app->request->get("nut_token", "");
        if (!empty($nut_token) && $nut_token == "549968af6006a2fe6c016bf9070b4899") {
            $id = Yii::$app->request->get('ezf_id');
            $data = MainQuery::GetEZFormById($id);
            
            return Json::encode($data);
        }else{
            echo "Error";
        } 
    }
    public function actionTest() {
        header("Access-Control-Allow-Origin: *", false);
        header('Access-Control-Allow-Methods: POST');
        header("Access-Control-Allow-Headers: Origin, Content-Type,x-token");
        
        /*access token*/
        $headers = Yii::$app->request->headers;
        $id = \Yii::$app->request->post("id");
        if($headers->get("x-token") === "11223344"){
            return Json::encode(["id"=>$headers->get("x-token")]);
        }
        
    }
}
