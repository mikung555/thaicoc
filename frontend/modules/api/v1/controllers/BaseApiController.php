<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace frontend\modules\api\v1\controllers;

use yii\web\Controller;
use Yii;
use frontend\modules\api\v1\classes\MainQuery;
use yii\base\Exception;
use frontend\modules\api\v1\classes\LogStash;


/**
 * THE CONTROLLER ACTION
 */
class BaseApiController extends Controller
{
    public $enableCsrfValidation = false;
    public function beforeAction($action)
    {
        $log = [];
        $log["header"] = Yii::$app->request->headers;
        $log["body"] = Yii::$app->request->bodyParams;
        $strLog = var_export($log, true);
        LogStash::Log(Yii::$app->controller->action->id,$strLog,"");
        return true;
    }

    function validateUser()
    {
        $headers = Yii::$app->request->headers;
        // if ($user == null) {
        //     $this->createResponse(false, null, "require username.");
        //     exit();
        // }

      if ($headers->has('user_id')) {
            $user = $headers->get('user_id');

            if (!$headers->has('x-token')) {
                $this->createResponse(false, null, "token require please login.");
                exit();
            }
              if (! MainQuery::CheckToken($user, $headers->get('x-token'))) {
                  $this->createResponse(false, null, "invalid token.");
                  exit();
              }else{
                  return $user;
              }
//            $validatedToken = ("aabb1212" === $headers->get('x-token')) ? "true": "false";
//            if ($validatedToken == "false") {
//
//            }else{
//                return $user;
//            }
        }else{
            $this->createResponse(false, null, "require userid.");
            exit();
        }
    }

    function createResponseByVerifyData($data , $message = null)
    {
        if ($data == null) {
            $this->createResponse(false, null, "result is null.");
            exit();
        }
        if( $data instanceof Exception){
            $this->createResponse(false, null, $data->getMessage());
            exit();
        }
        $this->createResponse(true, $data , $message);
    }

    function createResponse($success, $data, $message)
    {
        $result["success"] = $success;
        if ($message != null) {
            $result["message"] = $message;
        } else {
            if ($message == null) {
                if (!$success) {
                    $result["message"] = "something not correct!";
                }
            }
        }
        if ($data != null) {
            $result["data"] = $data;
        }
        print_r(json_encode($result));
    }
}