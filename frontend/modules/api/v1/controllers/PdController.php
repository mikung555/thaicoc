<?php
/**
 * Created by PhpStorm.
 * User: kawin
 * Date: 8/9/2017
 * Time: 11:46 AM
 */

namespace frontend\modules\api\v1\controllers;
use yii\db\Exception;
use yii\helpers\Json;
use yii\helpers\VarDumper;
use Yii;
use frontend\modules\api\v1\classes\EzForm;
use frontend\modules\api\v1\classes\MainQuery;
use frontend\modules\api\v1\classes\PdQuery;
use yii\base\ErrorException;

class PdController extends BaseApiController
{
    // public $enableCsrfValidation = false;

    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    public function actionCreateForm(){

    }

    public function actionGetUserData(){

    }

    public function actionSyncData(){

    }

}