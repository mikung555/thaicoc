<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace frontend\modules\api\v1\controllers;

use frontend\modules\api\v1\classes\LogStash;
use yii\helpers\Json;
use yii\helpers\VarDumper;
use yii\web\Controller;
use Yii;
use frontend\modules\api\v1\classes\QueryCCA02;
use frontend\modules\api\v1\classes\MainQuery;
use frontend\modules\api\v1\classes\WorklistQuery;
use backend\controllers\SignInController;
use frontend\modules\api\v1\classes\EzForm;

/**
 * THE CONTROLLER ACTION
 */
class WorklistController extends BaseApiController
{
    public $enableCsrfValidation = false;
    public $user_id = null;
    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        header("Access-Control-Allow-Origin: *", false);
        header("Access-Control-Allow-Methods: POST");
        header("Access-Control-Allow-Headers: Origin, Content-Type,x-token");
        $this->user_id =  $this->validateUser();
        return parent::beforeAction($action);
    }


    public function actionGetActiveWorklist()
    {
        if (!$this->user_id  ) {
            $res["success"] = false;
            $res["message"] = "please insert missing field user_id : ". $this->user_id;
            print_r(json_encode($res));
            exit();
        }

        $data = WorklistQuery::GetActiveWorklist();
        $this->createResponseByVerifyData($data);
    }

    public function actionCreateWorklist()
    {

        $title = Yii::$app->request->post('title', null);
        $sitecode = Yii::$app->request->post('sitecode',null);
        $start = Yii::$app->request->post('start', null);
        $end = Yii::$app->request->post('end', null);

          if (!$this->user_id || !$title || !$sitecode  ) {
            $res["success"] = false;
            $res["message"] = "please insert missing field user_id : ". $this->user_id ."    title : ".$title."     sitecode : ".$sitecode ;
            print_r(json_encode($res));
            exit();
        }

        $data = WorklistQuery::CreateWorklist($this->user_id,$title,$sitecode,$start,$end);
        $this->createResponseByVerifyData($data);
    }

    public function actionConfirm(){
        $worklist_id = Yii::$app->request->post('worklist_id', null);
        $doctor_code = Yii::$app->request->post('doctor_code', null);
        $ptid = Yii::$app->request->post('ptid', null);

        if ($worklist_id == null ) {
            $res["success"] = false;
            $res["message"] = "please insert worklist_id : ".$worklist_id;
            print_r(json_encode($res));
            exit();
        }
        if ($doctor_code == null ) {
            $res["success"] = false;
            $res["message"] = "please insert $doctor_code : ".$doctor_code;
            print_r(json_encode($res));
            exit();
        }
        if ($doctor_code == null ) {
            $res["success"] = false;
            $res["message"] = "please insert $doctor_code : ".$doctor_code;
            print_r(json_encode($res));
            exit();
        }
        $data = WorklistQuery::ConfirmPatient($worklist_id,$doctor_code,$ptid,$this->user_id);
        $this->createResponseByVerifyData($data);
    }

    public function actionRemoveWorklist()
    {
        $worklist_id = Yii::$app->request->post('worklist_id', null);

        if ($worklist_id == null ) {
            $res["success"] = false;
            $res["message"] = "please insert worklist_id : ".$worklist_id;
            print_r(json_encode($res));
            exit();
        }
        $data = WorklistQuery::RemoveWorklistById($this->user_id,$worklist_id);
        $this->createResponseByVerifyData($data);
    }

    public function actionGetWorklist()
    {
        $worklist_id = Yii::$app->request->post('worklist_id', null);
        $data = WorklistQuery::GetWorklist($this->user_id);
        $this->createResponseByVerifyData($data);
    }

    public function actionGetWorklistById()
    {
        $worklist_id = Yii::$app->request->post('worklist_id', null);
        if ($worklist_id == null) {
            $res["success"] = false;
            $res["message"] = "please insert worklist_id";
            print_r(json_encode($res));
            exit();
        }
        $data = WorklistQuery::GetWorklistDataById($worklist_id);
        $this->createResponseByVerifyData($data);
    }

    public function actionAddPatientArr()
    {
        $ptids = Yii::$app->request->post('ptids', "");
        $worklist_id = Yii::$app->request->post('worklist_id', "0");

        if ($ptids == "") {
            $res["success"] = false;
            $res["message"] = "please insert ptid";
            print_r(json_encode($res));
            exit();
        }

        if ($worklist_id == "0") {
            $res["success"] = false;
            $res["message"] = "please insert worklist_id";
            print_r(json_encode($res));
            exit();
        }

        $ptids =  (array)json_decode($ptids);
        $data = WorklistQuery::AddPatientArrById($this->user_id,$worklist_id,$ptids["arr"]);
        $this->createResponseByVerifyData($data);
    }

    public function actionAddPatient()
    {
        $ptid = Yii::$app->request->post('ptid', "0");
        $worklist_id = Yii::$app->request->post('worklist_id', "0");
        
        if ($ptid == "0") {
            $res["success"] = false;
            $res["message"] = "please insert ptid";
            print_r(json_encode($res));
            exit();
        }

        if ($worklist_id == "0") {
            $res["success"] = false;
            $res["message"] = "please insert worklist_id";
            print_r(json_encode($res));
            exit();
        }
        $data = WorklistQuery::AddPatientById($this->user_id,$worklist_id,$ptid);
        $this->createResponseByVerifyData($data);
    }

    public function actionGetPatient()
    {
        $worklist_id = Yii::$app->request->post('worklist_id', "0");
        if ($worklist_id == "0") {
            $res["success"] = false;
            $res["message"] = "please insert worklist_id";
            print_r(json_encode($res));
            exit();
        }
        $data = WorklistQuery::GetPatientsByWorklistId($worklist_id);
        $this->createResponseByVerifyData($data);
    }

    public function actionRemovePatient()
    {
        $ptid = Yii::$app->request->post('ptid', "0");
        $worklist_id = Yii::$app->request->post('worklist_id', "0");
        if ($ptid == "0") {
            $res["success"] = false;
            $res["message"] = "please insert ptid";
            print_r(json_encode($res));
            exit();
        }
        if ($worklist_id == "0") {
            $res["success"] = false;
            $res["message"] = "please insert worklist_id";
            print_r(json_encode($res));
            exit();
        }

        $data = WorklistQuery::RemovePatientByPtid($this->user_id,$worklist_id,$ptid);
        $this->createResponseByVerifyData($data);
    }

    /**
     * Add user to worklist able to add multiple user
     */
    public function actionAddUser(){
        $user_id = Yii::$app->request->post('user_id', null);
        $worklist_id = Yii::$app->request->post('worklist_id', null);
        $permission = Yii::$app->request->post('permission', 0);
        if ($user_id == null) {
            $res["success"] = false;
            $res["message"] = "please insert added user_id";
            print_r(json_encode($res));
            exit();
        }
        if ($worklist_id == null) {
            $res["success"] = false;
            $res["message"] = "please insert worklist_id";
            print_r(json_encode($res));
            exit();
        }
        $data = WorklistQuery::AddUserById($this->user_id,$worklist_id,$user_id,$permission);
        $this->createResponseByVerifyData($data);
    }

    /**
     * Add user to worklist able to add multiple user
     */
    public function actionUpdateUser(){
        $user_id = Yii::$app->request->post('user_id', null);
        $worklist_id = Yii::$app->request->post('worklist_id', null);
        $permission = Yii::$app->request->post('permission', null);
        $doctor_code = Yii::$app->request->post('doctor_code', null);
        $room_name = Yii::$app->request->post('room_name', null);
        if ($user_id == null) {
            $res["success"] = false;
            $res["message"] = "please insert added user_id";
            print_r(json_encode($res));
            exit();
        }
        if ($worklist_id == null) {
            $res["success"] = false;
            $res["message"] = "please insert worklist_id";
            print_r(json_encode($res));
            exit();
        }
        $data = WorklistQuery::UpdateUserInWorklist($this->user_id,$worklist_id,$user_id,["permission"=>$permission,"room_name"=>$room_name,"doctor_code"=>$doctor_code]);
        $this->createResponseByVerifyData($data);
    }

    /**
     * return relate user in worklist
     */
    public function actionGetUser(){
        $worklist_id = Yii::$app->request->post('worklist_id', null);
        if ($worklist_id == null) {
            $res["success"] = false;
            $res["message"] = "please insert worklist_id";
            print_r(json_encode($res));
            exit();
        }
        $data = WorklistQuery::GetUsersByWorklistId($worklist_id);
        $this->createResponseByVerifyData($data);
    }


}