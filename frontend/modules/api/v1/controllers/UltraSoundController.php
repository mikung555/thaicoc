<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace frontend\modules\api\v1\controllers;

use frontend\modules\api\v1\classes\WorklistQuery;
use Yii;
use frontend\modules\api\v1\classes\QueryCCA02;
use frontend\modules\api\v1\classes\MainQuery;
use frontend\modules\api\v1\classes\EzForm;
use frontend\modules\api\v1\classes\LogStash;
use yii\base\ErrorException;
use yii\base\Exception;

/**
 * THE CONTROLLER ACTION
 */
class UltraSoundController extends BaseApiController
{
    // public $enableCsrfValidation = false;
    public $user_id = null;
    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        $this->user_id =  $this->validateUser();
        return parent::beforeAction($action);
    }

    public function actionIndex()
    {
        header("Access-Control-Allow-Origin: *", false);
        header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE");
        header("Access-Control-Allow-Headers: Authorization");
        //sample model
        for ($i=0; $i < 24; $i++) {
            $data[$i]['hn'] = 232323+$i;
            $data[$i]['name'] = "Anun".$i;
            $data[$i]['address'] = 435;
        }
      
        print_r(json_encode($data));
        exit();
    }

     # สำหรับการทำงานของส่วน การลง CCA-02 ซ้ำ
    public function actionCca02Dup()
    {
        $duplist= QueryCCA02::getListDuplicate60();
         print_r(json_encode($duplist));
        exit();
    }

    public function actionGetUsDetail()
    {
        $id = Yii::$app->request->get('hid', '00001');
        $data= MainQuery::GetUSDetail($id);
        print_r(json_encode($data));
        exit();
    }

    public function actionGetHospitalDetail()
    {
        $id = Yii::$app->request->get('hid', '00001');
        $data= MainQuery::GetHospitalDetail($id);
        print_r(json_encode($data));
        exit();
    }

    public function actionGetAmphurListFromProvince()
    {
        $id = Yii::$app->request->get('pid', 0);
        $data= MainQuery::GetAmphurListFromProvince($id);
        $this->createResponseByVerifyData($data);
    }

    public function actionGetHospitalListFromAmphur()
    {
        $aid = Yii::$app->request->get('aid', 0);
        $pid = Yii::$app->request->get('pid', 0);
        $data= MainQuery::GetHospitalListFromAmphur($pid, $aid);
        $this->createResponseByVerifyData($data);
    }

    public function actionGetRelatePatientBySite()
    {
        header("Access-Control-Allow-Origin: *", false);
        header("Access-Control-Allow-Headers: Origin, Content-Type,x-token");
        header("Access-Control-Allow-Methods: POST");
        $this->validateUser();
        $sitecode = Yii::$app->request->post('sitecode', 0);
        $limit = Yii::$app->request->post('limit', 50);
          
//        $data= MainQuery::GetRelatePatientBySite($sitecode, $limit);
//        $this->createResponseByVerifyData($data);
    }

    public function actionGetRegRecordById()
    {
        header("Access-Control-Allow-Origin: *", false);
        header("Access-Control-Allow-Methods: POST");
        header("Access-Control-Allow-Headers: Origin, Content-Type,x-token");
        $this->validateUser();
        $ptid = Yii::$app->request->post('ptid', "0");
        if ($ptid == "0") {
            $res["success"] = false;
            $res["message"] = "please insert ptid";
            print_r(json_encode($res));
            exit();
        }
        $data= MainQuery::GetRegRecordById($ptid);
        $this->createResponseByVerifyData($data);
    }

    public function actionGetRegRecordsBySite()
    {
        header("Access-Control-Allow-Origin: *", false);
        header("Access-Control-Allow-Methods: POST");
        header("Access-Control-Allow-Headers: Origin, Content-Type,x-token");
        $this->validateUser();
        $sitecode = Yii::$app->request->post('sitecode', "0");
        if ($sitecode == "0") {
            $res["success"] = false;
            $res["message"] = "please insert sitecode";
            print_r(json_encode($res));
            exit();
        }
        $data= MainQuery::GetRegRecordsBySite($sitecode);
        $this->createResponseByVerifyData($data);
    }

    public function actionGetCca01RecordById()
    {
        header("Access-Control-Allow-Origin: *", false);
        header("Access-Control-Allow-Methods: POST");
        header("Access-Control-Allow-Headers: Origin, Content-Type,x-token");
        $this->validateUser();
        $ptid = Yii::$app->request->post('ptid', "0");
        if ($ptid == "0") {
            $res["success"] = false;
            $res["message"] = "please insert ptid";
            print_r(json_encode($res));
            exit();
        }
        $data= MainQuery::GetCca01RecordById($ptid);
        $this->createResponseByVerifyData($data);
    }

    public function actionGetCca01RecordsBySite()
    {
        header("Access-Control-Allow-Origin: *", false);
        header("Access-Control-Allow-Methods: POST");
        header("Access-Control-Allow-Headers: Origin, Content-Type,x-token");
        $this->validateUser();
        $sitecode = Yii::$app->request->post('sitecode', "0");
        if ($sitecode == "0") {
            $res["success"] = false;
            $res["message"] = "please insert sitecode";
            print_r(json_encode($res));
            exit();
        }
        $data= MainQuery::GetCca01RecordsBySite($sitecode);
        $this->createResponseByVerifyData($data);
    }

    public function actionGetCca01RecordsByRefSite()
    {
        header("Access-Control-Allow-Origin: *", false);
        header("Access-Control-Allow-Methods: POST");
        header("Access-Control-Allow-Headers: Origin, Content-Type,x-token");
        $this->validateUser();
        $sitecode = Yii::$app->request->post('sitecode', "0");
        if ($sitecode == "0") {
            $res["success"] = false;
            $res["message"] = "please insert sitecode";
            print_r(json_encode($res));
            exit();
        }
        $data= MainQuery::GetCca01RecordsByRefReg($sitecode);
        $this->createResponseByVerifyData($data);
    }



    public function actionGetCca02RecordById()
    {
        header("Access-Control-Allow-Origin: *", false);
        header("Access-Control-Allow-Methods: POST");
        header("Access-Control-Allow-Headers: Origin, Content-Type,x-token");
        $this->validateUser();
        $ptid = Yii::$app->request->post('ptid', "0");
        if ($ptid == "0") {
            $res["success"] = false;
            $res["message"] = "please insert ptid";
            print_r(json_encode($res));
            exit();
        }
        $data= MainQuery::GetCca02RecordById($ptid);
        $this->createResponseByVerifyData($data);
    }

    public function actionGetCca02RecordsBySite()
    {
        header("Access-Control-Allow-Origin: *", false);
        header("Access-Control-Allow-Methods: POST");
        header("Access-Control-Allow-Headers: Origin, Content-Type,x-token");
        $this->validateUser();
        $sitecode = Yii::$app->request->post('sitecode', "0");
        if ($sitecode == "0") {
            $res["success"] = false;
            $res["message"] = "please insert sitecode";
            print_r(json_encode($res));
            exit();
        }
        $data= MainQuery::GetCca02RecordsBySite($sitecode);
        $this->createResponseByVerifyData($data);
    }

    public function actionGetCca02RecordsByRefSite()
    {
        header("Access-Control-Allow-Origin: *", false);
        header("Access-Control-Allow-Methods: POST");
        header("Access-Control-Allow-Headers: Origin, Content-Type,x-token");
        $this->validateUser();
        $sitecode = Yii::$app->request->post('sitecode', "0");
        if ($sitecode == "0") {
            $res["success"] = false;
            $res["message"] = "please insert sitecode";
            print_r(json_encode($res));
            exit();
        }
        $data= MainQuery::GetCca02RecordsByRefReg($sitecode);
        $this->createResponseByVerifyData($data);
    }

    public function actionSyncAllByRefSite()
    {
        header("Access-Control-Allow-Origin: *", false);
        header("Access-Control-Allow-Methods: POST");
        header("Access-Control-Allow-Headers: Origin, Content-Type,x-token");
        ini_set('max_execution_time', 0);
        set_time_limit(0);
        ini_set('memory_limit','512M');
        try{
            $this->validateUser();
            $sitecode = Yii::$app->request->post('sitecode', null);
            $lastSync = Yii::$app->request->post('last_sync', null);
            if ($sitecode == null) {
                $res["success"] = false;
                $res["message"] = "please insert sitecode";
                print_r(json_encode($res));
                exit();
            }

            if($lastSync != null && $lastSync != "null" && $lastSync != ""){

                $reg = MainQuery::SyncRegRecords($sitecode,$lastSync);

                $cca01_temp = MainQuery::SyncCca01RecordsByRefCreatedReg($sitecode,$lastSync);
                $cca01 = MainQuery::SyncCca01RecordsByRefReg($sitecode,$lastSync);
//                $cca01 = MainQuery::SyncCca01RecordsBySite($sitecode,$lastSync);
                $cca01 = array_merge($cca01, $cca01_temp);

                $cca02_temp = MainQuery::SyncCca02RecordsByRefCreatedReg($sitecode,$lastSync);
                $cca02 = MainQuery::SyncCca02RecordsByRefReg($sitecode,$lastSync);
                $cca02 = array_merge($cca02, $cca02_temp);


                $worklist = WorklistQuery::GetWorklistByDate($this->user_id,$lastSync);
                $worklistData = [];
                for($i = 0 ; $i < count($worklist); $i++){
                    array_push($worklistData,WorklistQuery::GetWorklistDataById($worklist[$i]["id"]));
                }

                $data = ["reg" => $reg , "cca01" => $cca01 , "cca02" => $cca02 , "worklists" => $worklistData];
                $this->createResponseByVerifyData($data);
            }else {
//                LogStash::Log("sync-all-by-ref-site","Pass#1","");
                $reg = MainQuery::GetRegRecordsBySite($sitecode);
                $cca01 = MainQuery::GetCca01RecordsByRefReg($sitecode);
                $cca02 = MainQuery::GetCca02RecordsByRefReg($sitecode);
                $worklist = WorklistQuery::GetWorklist($this->user_id);

                //var_dump($worklist);
                $worklistData = [];
                for ($i = 0; $i < count($worklist); $i++) {
                    array_push($worklistData, WorklistQuery::GetWorklistDataById($worklist[$i]["id"]));
                }
                $data = ["reg" => $reg, "cca01" => $cca01, "cca02" => $cca02, "worklists" => $worklistData];
                $this->createResponseByVerifyData($data);
            }
        }catch (ErrorException $ex){
            LogStash::ErrorEx("sync-all-by-ref-site",$ex,"");
            $this->createResponseByVerifyData($ex);
        }catch (\Exception $ex){
            LogStash::ErrorEx("sync-all-by-ref-site",$ex,"");
            $this->createResponseByVerifyData($ex);
        }

    }

    public function actionSyncCca02RecordsByRefSite()
    {
        header("Access-Control-Allow-Origin: *", false);
        header("Access-Control-Allow-Methods: POST");
        header("Access-Control-Allow-Headers: Origin, Content-Type,x-token");
        $this->validateUser();
        $sitecode = Yii::$app->request->post('sitecode', null);
        $lastSync = Yii::$app->request->post('last_sync', null);
        if ($sitecode == null) {
            $res["success"] = false;
            $res["message"] = "please insert sitecode";
            print_r(json_encode($res));
            exit();
        }
        if ($lastSync == null) {
            $res["success"] = false;
            $res["message"] = "please insert lastSync time";
            print_r(json_encode($res));
            exit();
        }

        $data= MainQuery::SyncCca02RecordsByRefReg($sitecode,$lastSync);
        $this->createResponseByVerifyData($data);
    }

    public function actionSubmitSetting(){
        header("Access-Control-Allow-Origin: *", false);
        header("Access-Control-Allow-Methods: POST");
        header("Access-Control-Allow-Headers: Origin, Content-Type,x-token");
        $this->validateUser();
        $room_name = Yii::$app->request->post('room_name', null);
        $doctor_code = Yii::$app->request->post('doctor_code', null);
        if ($doctor_code == null) {
            $res["success"] = false;
            $res["message"] = "please insert doctor_name";
            print_r(json_encode($res));
            exit();
        } if ($room_name == null) {
            $res["success"] = false;
            $res["message"] = "please insert room_name";
            print_r(json_encode($res));
            exit();
        }
        $data= WorklistQuery::ApplyDoctorAndRoom($this->user_id,$room_name,$doctor_code);
        $this->createResponseByVerifyData($data);
    }

    public function actionGetEmrRecordsBySite()
    {
        header("Access-Control-Allow-Origin: *", false);
        header("Access-Control-Allow-Methods: POST");
        header("Access-Control-Allow-Headers: Origin, Content-Type,x-token");
        $this->validateUser();
        $sitecode = Yii::$app->request->post('sitecode', "0");
        if ($sitecode == "0") {
            $res["success"] = false;
            $res["message"] = "please insert sitecode";
            print_r(json_encode($res));
            exit();
        }
        $data= MainQuery::GetEmrRecordsBySite($sitecode);
        $this->createResponseByVerifyData($data);
    }

    public function actionGetEzform()
    {
         header("Access-Control-Allow-Origin: *", false);
        $nut_token = \Yii::$app->request->get("nut-token", "");
        if($nut_token =="549968af6006a2fe6c016bf9070b4899"){
//            $id = Yii::$app->request->get('ezf_id');
//            $data = Query::GetEZFormById($id);
//            $this->createResponseByVerifyData($data);  
            echo $nut_token;
        }else{
            
            header("Access-Control-Allow-Methods: POST");
            header("Access-Control-Allow-Headers: Origin, Content-Type,x-token");
            $this->validateUser();
            $id = Yii::$app->request->post('ezf_id');
            $data = MainQuery::GetEZFormById($id);
            $this->createResponseByVerifyData($data);
        }
       
    }

    public function actionGetEzformByDate()
    {
        header("Access-Control-Allow-Origin: *", false);
        header("Access-Control-Allow-Methods: POST");
        header("Access-Control-Allow-Headers: Origin, Content-Type,x-token");
        $this->validateUser();
        $start_date = Yii::$app->request->post('start_date',"0");
        $end_date = Yii::$app->request->post('end_date',"0");

        if ($start_date == "0") {
            $res["success"] = false;
            $res["message"] = "please insert start_date";
            print_r(json_encode($res));
            exit();
        }
           if ($end_date == "0") {
            $res["success"] = false;
            $res["message"] = "please insert end_date";
            print_r(json_encode($res));
            exit();
        }

        $data = "";
        $this->createResponseByVerifyData($data);
    }

    public function actionGetCca02()
    {
        header("Access-Control-Allow-Origin: *", false);
        header("Access-Control-Allow-Methods: POST");
        header("Access-Control-Allow-Headers: Origin, Content-Type,x-token");
        $this->validateUser();        
        $id = Yii::$app->request->post('ezf_id');
        $data = MainQuery::GetEZFormById($id);
        $this->createResponseByVerifyData($data);
    }

   
    
    public function actionSubmitForm()
    {
    }
    
    public function actionGetUser()
    {
        header("Access-Control-Allow-Origin: *", false);
        header("Access-Control-Allow-Methods: POST");
      
        // $username =Yii::$app->request->post('username');
        $username = $this->validateUser();        
        $data = MainQuery::GetUser($username);
        $this->createResponseByVerifyData($data);
    }

    public function actionLogin()
    {
        header("Access-Control-Allow-Origin: *", false);
        header("Access-Control-Allow-Methods: POST");

        $username = $_SERVER['PHP_AUTH_USER'];
        $headers = Yii::$app->request->headers;
        if ($headers->has('x-token')) {
             $token = $headers->get('x-token');
            //TODO fix later
            print_r(json_encode(MainQuery::LoginWithToken( $username, $token)));
        } else {
            $password = $_SERVER['PHP_AUTH_PW'];
            print_r(json_encode(MainQuery::Login( $username, $password)));
        }
    }

    public function actionCreateForm()
    {
        header("Access-Control-Allow-Origin: *", false);
        header("Access-Control-Allow-Methods: POST");
        $ezf_id = Yii::$app->request->post('ezf_id');   
        $target = Yii::$app->request->post('target');   
        $submit = filter_var(Yii::$app->request->post('submit'), FILTER_VALIDATE_BOOLEAN);
        $sitecode = Yii::$app->request->post('sitecode');
        $id = Yii::$app->request->post('data_id');        
        $rawdata = Yii::$app->request->post('data');
        $room_name = Yii::$app->request->post('room_name');
        $doctor_code = Yii::$app->request->post('doctor_code');
        $data = json_decode($rawdata);
        LogStash::Info("CreateNewRecord",var_export($data, true),"data");

        $create_res = EzForm::CreateNewRecord($this->user_id, $ezf_id, $sitecode, $target,  $submit,$id, $data );
        $data = (array)$data;
        if($ezf_id == "1437619524091524800")
            WorklistQuery::AddLog( $data[ "ptid" ],$this->user_id,$room_name,$doctor_code);
        $this->createResponseByVerifyData($create_res);
    }

}
