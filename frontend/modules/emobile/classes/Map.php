<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace frontend\modules\emobile\classes;

/**
 * Description of Map
 *
 * @author engiball
 */
class Map {

    public static function getPoints() {


        $hcode = \Yii::$app->request->get('hcode');
        $datas = array();
        $sql = "select sys_lat as lat,sys_lng as lng from tb_data_coc where hsitecode =:sitecode  and sys_lat is not null and  sys_lat <> '' ";
        $data = \Yii::$app->db->createCommand($sql, [':sitecode' => $hcode])->queryAll();
        $data0 = ["lat" => $data[0]["lat"], "lng" => $data[0]["lng"]];
        array_push($datas, $data0, $data);

        return $datas;
    }

    public static function getHosPoint() {
        $hcode = \Yii::$app->request->get('hcode');
        $datas = array();
        $sql = "select sys_lat as lat,sys_lng as lng from tb_data_coc where sitecode =:sitecode  and sys_lat is not null and  sys_lat <> '' ";
        $data = \Yii::$app->db->createCommand($sql, [':sitecode' => $hcode])->queryAll();
        $data0 = ["lat" => $data[0]["lat"], "lng" => $data[0]["lng"]];
        array_push($datas, $data0, $data);

        return $datas;
    }

    public static function getPeoplePoint() {
        $hcode = \Yii::$app->request->get('hcode');

        $datas = array();
        $sql = "select sys_lat as lat,sys_lng as lng from tb_data_coc where hsitecode =:sitecode  and sys_lat is not null and  sys_lat <> '' ";
        $data = \Yii::$app->db->createCommand($sql, [':sitecode' => $hcode])->queryAll();
        $data0 = ["lat" => $data[0]["lat"], "lng" => $data[0]["lng"]];
        array_push($datas, $data0, $data);

        return $datas;
    }

    public static function getPlace($hcode) {

       $sql = "SELECT DISTINCT hsitecode as hcode from tbdata_1462172833035880400 where InputUnit=:hcode  and hsitecode <>  InputUnit and  hsitecode <> '' UNION SELECT DISTINCT InputUnit as hcode from tbdata_1462172833035880400 where hsitecode =:hcode  and hsitecode <>  InputUnit  and InputUnit <>''";
       //$sql="select hsitecode,InputUnit as hcode,count(*) from tbdata_1462172833035880400 where (hsitecode=:hcode or InputUnit=:hcode ) and rstat<>3 and InputUnit<>''  group by hsitecode,InputUnit having count(*) > 50;";
        $data = \Yii::$app->db->createCommand($sql, [':hcode' => $hcode])->queryAll();
         array_unshift($data,["hcode"=>$hcode]);
        return $data;
    }
        public static function getPlaceCustom($cTable,$cCenterCol,$cCeterCode,$cEndCol) {

       $sql = "SELECT DISTINCT hsitecode as hcode from :cTable where :cEndCol=:hcode  and :cCenterCal <>  :cEndCol and  :cCenterCal <> '' UNION SELECT DISTINCT :cEndCol as hcode from :cTable where :cCenterCol =:cCenterCode  and hsitecode <>  InputUnit  and InputUnit <>''";
       //$sql="select hsitecode,InputUnit as hcode,count(*) from tbdata_1462172833035880400 where (hsitecode=:hcode or InputUnit=:hcode ) and rstat<>3 and InputUnit<>''  group by hsitecode,InputUnit having count(*) > 50;";
        $data = \Yii::$app->db->createCommand($sql, [':cTable'=>$cTable,':cCenterCal'=>$cCenterCol,':cCenterCode'=> $cCeterCode,':cEndCol'=>$cEndCol])->queryAll();
         array_unshift($data,["centerCode"=>$cCeterCode]);
        return $data;
    }

    public static function hDetail($hcode,$sitecode) {
    $sql = " select 
    (select hcode from all_hospital_thai  where hcode =:hcode) as hcode
    ,(select name from all_hospital_thai where hcode = :hcode) as hname
    ,(select lat from all_hospital_thai where hcode =:hcode ) as lat 
    ,(select lng from all_hospital_thai where hcode =:hcode ) as lng
    ,(select count(*) from tb_data_coc where hsitecode =:hcode ) as pcount
    ,(SELECT count(DISTINCT ptid)  from tbdata_1462172833035880400 where InputUnit=:hcode and hsitecode <> InputUnit and hsitecode <> '')  as pin
    ,(SELECT count(*)  from tbdata_1462172833035880400 where InputUnit=:hcode  and hsitecode <> InputUnit and hsitecode <> '' )  as rin 
    ,( SELECT count(DISTINCT ptid)   from tbdata_1462172833035880400 where hsitecode =:hcode  and hsitecode <> InputUnit) as pout
    ,( SELECT count(*)  from tbdata_1462172833035880400 where hsitecode =:hcode  and hsitecode <> InputUnit) as rout
    ,(SELECT $sitecode) as sitecode
     ";
        $data = \Yii::$app->db->createCommand($sql, [':hcode' => $hcode, ':sitecode' => $sitecode])->queryAll();
        return ($data);
    }

}
