<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace frontend\modules\emobile\models;

use yii\base\Model;
use yii\web\UploadedFile;
use Yii;
use common\lib\codeerror\helpers\GenMillisecTime;



class UploadForm extends Model
{
    /**
     * @var UploadedFile
     */
    public $imageFile;
     public $cid;

    public function rules()
    {
        return [
            [['imageFile'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg'],
         //   [['cid'], 'string'],

        ];
    }
    
    public function upload()
    {
         //     $fullPath = Yii::$app->basePath . '/../storage/web/source/' . $nowFileName;
//        if ($this->validate()) {
//            $fileName ="www_pd"
//                    ."_"
//                    .GenMillisecTime::getMillisecTime()
//                    . '.' . $this->imageFile->extension;
//                    $filepath =Yii::$app->basePath . '/../storage/web/source/'
//                  . $fileName;
//            $this->imageFile->saveAs( $filepath );
//            return  $fileName;
//        } else {
//            return "error";
//            
//        }
        
            $fileName ="www_pd"
                    ."_"
                    .GenMillisecTime::getMillisecTime()
                    . '.' . $this->imageFile->extension;
                    $filepath =Yii::$app->basePath . '/../storage/web/img/pdimg/'
                  . $fileName;
            $this->imageFile->saveAs( $filepath );
            return  $fileName;
     
    }
}