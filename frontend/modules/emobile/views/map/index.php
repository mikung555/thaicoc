<?php

use yii\helpers\Url;
?>
<!DOCTYPE html>
<html>
    <head>


        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">


        <link rel="stylesheet" href="https://unpkg.com/leaflet@1.0.3/dist/leaflet.css" integrity="sha512-07I2e+7D8p6he1SIM+1twR5TIrhUQn9+I6yjqD53JQjFiMf8EtC93ty0/5vJTZGF8aAocvHYNEDJajGdNx1IsQ==" crossorigin=""/>
       <link rel="stylesheet" href="https://leaflet.github.io/Leaflet.label/leaflet.label.css" />
        <link rel="stylesheet" href="https://marslan390.github.io/BeautifyMarker/leaflet-beautify-marker-icon.css" />
        <link rel="stylesheet" href="<?php echo Url::to(['/emobile/map/popup']); ?>" />
                <link rel="stylesheet" href="<?php echo Url::to(['/emobile/map/fullscreencss']); ?>" />



    </head>
    <body>


        <div align="center">
            <?php
            $sitecode = Yii::$app->user->identity->userProfile->sitecode;
            if ($sitecode === NULL)
                $sitecode = '10668';

            // echo $sitecode;
            ?>
        </div>
        <div align="center" width="100%" height="100%">
            <div id="mapid" style="width: 100%; height: 800px;" align="center"></div>
        </div>




    </body>


    <script src="<?php echo Url::to(['/emobile/map/leafjs']); ?>" ></script>
    <script src="<?php echo Url::to(['/emobile/map/curve']); ?>" ></script>
    <script src="<?php echo Url::to(['/emobile/map/b-icon']); ?>" ></script>
    <script src="<?php echo Url::to(['/emobile/map/b-marker']); ?>" ></script>
    <script src="<?php echo Url::to(['/emobile/map/label']); ?>" ></script>
    <script src="<?php echo Url::to(['/emobile/map/fullscreen']); ?>" ></script>

    <script src="<?php echo Url::to(['/emobile/map/map-js2']); ?>" ></script>


    <?php
    $jstext = frontend\modules\emobile\classes\Mapjs::getTemplete($sitecode);
//appxq\sdii\utils\VarDumper::dump($jstext);

    $this->registerJs($jstext);
    ?>

</html>
