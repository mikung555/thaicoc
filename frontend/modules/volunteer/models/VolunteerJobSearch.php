<?php

namespace frontend\modules\volunteer\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\modules\volunteer\models\VolunteerJob;

/**
 * VolunteerJobSearch represents the model behind the search form about `frontend\modules\volunteer\models\VolunteerJob`.
 */
class VolunteerJobSearch extends VolunteerJob
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['job_id', 'job_type', 'job_parent', 'job_status', 'job_active', 'created_by', 'updated_by'], 'integer'],
            [['job_cid', 'job_fname', 'job_lname', 'job_start', 'job_end', 'sitecode', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = VolunteerJob::find()->where('job_status=0 AND job_active=0')
		->select(['volunteer_job.*',
		    "concat(user_profile.firstname, ' ', user_profile.lastname) AS user_name",
		    ])
		->join('inner join', 'user_profile', 'volunteer_job.created_by=user_profile.user_id');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'job_id' => $this->job_id,
            'job_start' => $this->job_start,
            'job_end' => $this->job_end,
            'job_type' => $this->job_type,
            'job_parent' => $this->job_parent,
            'job_status' => $this->job_status,
            'job_active' => $this->job_active,
            'created_by' => $this->created_by,
            'created_at' => $this->created_at,
            'updated_by' => $this->updated_by,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'job_cid', $this->job_cid])
            ->andFilterWhere(['like', 'job_fname', $this->job_fname])
            ->andFilterWhere(['like', 'job_lname', $this->job_lname])
            ->andFilterWhere(['like', 'sitecode', $this->sitecode]);

        return $dataProvider;
    }
}
