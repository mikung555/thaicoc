<?php

namespace frontend\modules\volunteer\models;

use Yii;

/**
 * This is the model class for table "volunteer_job".
 *
 * @property integer $job_id
 * @property string $job_cid
 * @property string $job_fname
 * @property string $job_lname
 * @property string $job_start
 * @property string $job_end
 * @property integer $job_type
 * @property integer $job_parent
 * @property string $sitecode
 * @property integer $job_status
 * @property integer $job_active
 * @property integer $created_by
 * @property string $created_at
 * @property integer $updated_by
 * @property string $updated_at
 */
class VolunteerJob extends \yii\db\ActiveRecord
{
    public $user_name;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'volunteer_job';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['job_id', 'job_cid', 'job_fname', 'job_lname', 'job_type'], 'required'],
            [['job_id', 'job_type',  'job_status', 'job_active', 'created_by', 'updated_by'], 'integer'],
            [['job_parent', 'job_start', 'job_end', 'created_at', 'updated_at'], 'safe'],
            [['job_cid'], 'string', 'max' => 50],
            [['job_fname', 'job_lname'], 'string', 'max' => 200],
            [['sitecode'], 'string', 'max' => 10]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'job_id' => Yii::t('app', 'Job ID'),
            'job_cid' => Yii::t('app', 'เลขที่บัตรประชาชน'),
            'job_fname' => Yii::t('app', 'ชื่อ'),
            'job_lname' => Yii::t('app', 'สกุล'),
            'job_start' => Yii::t('app', 'Job Start'),
            'job_end' => Yii::t('app', 'Job End'),
            'job_type' => Yii::t('app', 'Job Type'),
            'job_parent' => Yii::t('app', 'อสม. (อาสาสมัครสาธารณสุขประจำหมู่บ้าน)'),
            'sitecode' => Yii::t('app', 'Sitecode'),
            'job_status' => Yii::t('app', 'Job Status'),
            'job_active' => Yii::t('app', 'Job Active'),
            'created_by' => Yii::t('app', 'Created By'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
}
