<?php

namespace frontend\modules\volunteer;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'frontend\modules\volunteer\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
