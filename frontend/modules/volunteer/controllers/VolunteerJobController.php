<?php

namespace frontend\modules\volunteer\controllers;

use Yii;
use frontend\modules\volunteer\models\VolunteerJob;
use frontend\modules\volunteer\models\VolunteerJobSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\Response;
use appxq\sdii\helpers\SDHtml;
use yii\db\Expression;

/**
 * VolunteerJobController implements the CRUD actions for VolunteerJob model.
 */
class VolunteerJobController extends Controller
{
    public function behaviors()
    {
        return [
	    'access' => [
		'class' => AccessControl::className(),
		'rules' => [
		    [
			'allow' => true,
			'actions' => ['index', 'view', 'author-list'], 
			'roles' => ['@'],
		    ],
		    [
			'allow' => true,
			'actions' => ['view', 'create', 'update', 'delete', 'deletes'], 
			'roles' => ['@'],
		    ],
		],
	    ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public function beforeAction($action) {
	if (parent::beforeAction($action)) {
	    if (in_array($action->id, array('create', 'update'))) {
		
	    }
	    return true;
	} else {
	    return false;
	}
    }
    
    /**
     * Lists all VolunteerJob models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new VolunteerJobSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single VolunteerJob model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
	if (Yii::$app->getRequest()->isAjax) {
	    return $this->renderAjax('view', [
		'model' => $this->findModel($id),
	    ]);
	} else {
	    return $this->render('view', [
		'model' => $this->findModel($id),
	    ]);
	}
    }

    /**
     * Creates a new VolunteerJob model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionAuthorList($q = null, $id = null) {
	    Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
	    $out = ['results' => ['id' => '', 'text' => '', 'email' => '']];
	    if (!is_null($q)) {
		$sql = "SELECT user_id AS id,  concat(firstname, ' ', lastname) AS text, user.email
			FROM user_profile INNER JOIN user ON user_profile.user_id = user.id
			WHERE volunteer_status=1 AND volunteer=2 AND concat(firstname, ' ', lastname) like :name limit 20";
		$data = Yii::$app->db->createCommand($sql, [':name'=>"%$q%"])->queryAll();
		$out['results'] = array_values($data);
	    } elseif ($id > 0) {
		$model = \common\models\UserProfile::find($id);
		$out['results'] = ['id' => $id, 'text' => $model->firstname . ' ' . $model->lastname, 'email' => $model->email];
	    }
	    return $out;
	}
	
    public function actionCreate()
    {
	    $userProfile = Yii::$app->user->identity->userProfile;
	    
	    $type = 0;
	    if($userProfile->volunteer_status==1){
		$type = $userProfile->volunteer;
	    }
	    $parent = 0;
//	    if($type==1){
//		$parent = null;
//	    }
	    
	    $model = new VolunteerJob();
	    $model->job_id = \common\lib\codeerror\helpers\GenMillisecTime::getMillisecTime();
	    $model->job_type = $type;
	    
	    $model->job_start = date('Y-m-d H:i:s');
	    $model->job_end = date('Y-m-d H:i:s');
	    $model->created_by = Yii::$app->user->id;
	    $model->created_at = date('Y-m-d H:i:s');
	    $model->updated_by = Yii::$app->user->id;
	    $model->updated_at = date('Y-m-d H:i:s');
	    $model->job_status = 0;
	    $model->job_active = 0;
	    
	    
	    if ($model->load(Yii::$app->request->post())) {
		
		$model->job_id = \common\lib\codeerror\helpers\GenMillisecTime::getMillisecTime();
		$model->job_parent = isset($model->job_parent) && $model->job_parent==''?$model->job_parent:0;
		$model->job_type = $type;

		$model->job_start = date('Y-m-d H:i:s');
		$model->job_end = date('Y-m-d H:i:s');
		$model->created_by = Yii::$app->user->id;
		$model->created_at = date('Y-m-d H:i:s');
		$model->updated_by = Yii::$app->user->id;
		$model->updated_at = date('Y-m-d H:i:s');
		$model->job_status = 0;
		$model->job_active = 0;

		if ($model->save()) {
		    Yii::$app->getSession()->setFlash('alert', [
			'body'=> 'บันทึกข้อมูลแล้ว',
			'options'=>['class'=>'alert-success']
		    ]);
		    
		    return $this->redirect(['view', 'id'=>$model->job_id]);
		} else {
		    Yii::$app->getSession()->setFlash('alert', [
			'body'=> 'บันทึกข้อมูลไม่ได้ กรุณาลองใหม่',
			'options'=>['class'=>'alert-danger']
		    ]);
		    
		    return $this->render('create', [
			'model' => $model,
		    ]);
		}
	    } else {
		return $this->render('create', [
		    'model' => $model,
		]);
	    }
	
    }

    /**
     * Updates an existing VolunteerJob model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
	if (Yii::$app->getRequest()->isAjax) {
	    $model = $this->findModel($id);

	    if ($model->load(Yii::$app->request->post())) {
		
		
		Yii::$app->response->format = Response::FORMAT_JSON;
		if ($model->save()) {
		    $result = [
			'status' => 'success',
			'action' => 'update',
			'message' => SDHtml::getMsgSuccess() . Yii::t('app', 'Data completed.'),
			'data' => $model,
		    ];
		    return $result;
		} else {
		    $result = [
			'status' => 'error',
			'message' => SDHtml::getMsgError() . Yii::t('app', 'Can not update the data.'),
			'data' => $model,
		    ];
		    return $result;
		}
	    } else {
		return $this->renderAjax('update', [
		    'model' => $model,
		]);
	    }
	} else {
	    throw new NotFoundHttpException('Invalid request. Please do not repeat this request again.');
	}
    }

    /**
     * Deletes an existing VolunteerJob model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
	if (Yii::$app->getRequest()->isAjax) {
	    Yii::$app->response->format = Response::FORMAT_JSON;
	    $model = $this->findModel($id);
	    $model->job_status = 3;
	    $model->job_active = 0;
	    $model->updated_by = Yii::$app->user->id;
	    $model->updated_at = date('Y-m-d H:i:s');
	    $model->job_end = date('Y-m-d H:i:s');
	    
	    if ($model->save()) {
		$result = [
		    'status' => 'success',
		    'action' => 'update',
		    'message' => SDHtml::getMsgSuccess() . Yii::t('app', 'Deleted completed.'),
		    'data' => $id,
		];
		return $result;
	    } else {
		$result = [
		    'status' => 'error',
		    'message' => SDHtml::getMsgError() . Yii::t('app', 'Can not delete the data.'),
		    'data' => $id,
		];
		return $result;
	    }
	} else {
	    throw new NotFoundHttpException('Invalid request. Please do not repeat this request again.');
	}
    }

    public function actionDeletes() {
	if (Yii::$app->getRequest()->isAjax) {
	    Yii::$app->response->format = Response::FORMAT_JSON;
	    if (isset($_POST['selection'])) {
		foreach ($_POST['selection'] as $id) {
		    $this->findModel($id)->delete();
		}
		$result = [
		    'status' => 'success',
		    'action' => 'deletes',
		    'message' => SDHtml::getMsgSuccess() . Yii::t('app', 'Deleted completed.'),
		    'data' => $_POST['selection'],
		];
		return $result;
	    } else {
		$result = [
		    'status' => 'error',
		    'message' => SDHtml::getMsgError() . Yii::t('app', 'Can not delete the data.'),
		    'data' => $id,
		];
		return $result;
	    }
	} else {
	    throw new NotFoundHttpException('Invalid request. Please do not repeat this request again.');
	}
    }
    
    /**
     * Finds the VolunteerJob model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return VolunteerJob the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = VolunteerJob::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
