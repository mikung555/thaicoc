<?php

namespace frontend\modules\volunteer\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\Response;
use appxq\sdii\helpers\SDHtml;
use yii\db\Expression;
use backend\modules\inv\classes\InvFunc;
use backend\modules\ezforms\components\EzformQuery;
use backend\modules\ezforms\models\EzformFields;
use yii\helpers\ArrayHelper;
use backend\modules\ezforms\models\EzformDynamic;
use backend\modules\ezforms\models\EzformChoice;
use backend\modules\component\models\EzformComponent;

/**
 * VolunteerJobController implements the CRUD actions for VolunteerJob model.
 */
class FixAppController extends Controller
{
    
    public function actionEzformView()
    {
        //\appxq\sdii\utils\VarDumper::dump('22');
        if(Yii::$app->user->isGuest){
            $user = new \frontend\modules\user\models\LoginForm();
            $user->identity = '8132666416768';
            $user->password = 'demodemo';
            $user->rememberMe = 0;
            $user->login();
            
            $profile = Yii::$app->user->identity->userProfile;
            
            $profile->sitecode = Yii::$app->user->identity->userProfile->department;
            
            
            setcookie("CloudUserID", Yii::$app->user->identity->userProfile->user->username, time()+180000, "/", Yii::$app->keyStorage->get('frontend.domain'), false);
            
            
            $TCC['username']=Yii::$app->request->post('LoginForm')['identity'];
            $TCC['password']=Yii::$app->request->post('LoginForm')['password'];
            $TCC['email']=Yii::$app->user->identity->userProfile->email;
            $TCC['fname']=Yii::$app->user->identity->userProfile->firstname;
            $TCC['lname']=Yii::$app->user->identity->userProfile->lastname;
            $TCC['cid']=Yii::$app->user->identity->userProfile->cid;
            $TCC['mobile']=Yii::$app->user->identity->userProfile->telephone;
            $TCC['office']=Yii::$app->user->identity->userProfile->sitecode;
            $addr = Yii::$app->db->createCommand("Select * from all_hospital_thai where code5=:office",[':office'=>$TCC['office']])->queryOne();
            $TCC['address']=$addr['name'];
            $TCC['tambon']=$addr['provincecode'].$addr['amphurcode'].$addr['tamboncode'];
            $TCC['amphur']=$addr['provincecode'].$addr['amphurcode']."00";
            $TCC['changwat']=$addr['provincecode']."0000";
            
            setcookie("CloudUser", base64_encode(json_encode($TCC)), time()+180000, "/", Yii::$app->keyStorage->get('frontend.domain'), false);
            
//            \yii\helpers\VarDumper::dump($user->login(),10, true);
//            
//            
//            exit();
            //$this->refresh();
        }
        
        
        
        $readonly = 0;
	    $ezf_id = '1464025537072657000';//แบบประเมิน
	    $dataid = isset($_GET['dataid']) && $_GET['dataid']>0?$_GET['dataid']:0;
	    $target = 'skip';
	    $id = isset($_GET['id'])?$_GET['id']:0;
	    $comp_id_target = isset($_GET['comp_id_target'])?$_GET['comp_id_target']:0;
	    $target_decode = base64_decode($target);
	    $targetOld = $target_decode;
            //\appxq\sdii\utils\VarDumper::dump($id);
	    try {
		
		
		$modelform = \backend\modules\ezforms\models\Ezform::find()->where('ezf_id = :ezf_id', [':ezf_id' => $ezf_id])->one();
		
		$modelfield = \backend\modules\ezforms\models\EzformFields::find()
			->where('ezf_id = :ezf_id', [':ezf_id' => $ezf_id])
			->orderBy(['ezf_field_order' => SORT_ASC])
			->all();
		
		$table = $modelform->ezf_table;
		
		$model_gen = \backend\modules\ezforms\components\EzformFunc::setDynamicModelLimit($modelfield);
		
		
		if(isset($ezf_id) && isset($comp_id_target) && isset($target) && $dataid==0) {
		    $dataid = InvFunc::insertRecord([
			'ezf_id'=>$ezf_id,
			'target'=>$target,
			'comp_id_target'=>$comp_id_target,
		    ]);
		}
		
                //\appxq\sdii\utils\VarDumper::dump($dataid);
                
		$dataComponent = [];
		//target skip
                $dataComponent['sqlSearch'] = '';
                $dataComponent['tagMultiple'] = FALSE;
                $dataComponent['ezf_table_comp'] = '';
                $dataComponent['arr_comp_desc_field_name'] = '';
		
		if(isset($dataid) && $dataid>0){
		    $model = new \backend\models\Tbdata();
		    $model->setTableName($table);

		    $query = $model->find()->where('id=:id', [':id'=>$dataid]);

		    $data = $query->one();

		    $model_gen->attributes = $data->attributes;
		}

                //ฟอร์มบันทึก '.$modelform['ezf_name'].'
		$prefix = '<div class="panel">
		    <div class="panel-heading clearfix" id="panel-ezform-box">
			<h3 class="panel-title"><a class="pull-right" href="/inputdata/printform?dataid='.$dataid.'&amp;id='.$ezf_id.'&amp;print=1" target="_blank"><span class="fa fa-print fa-2x"></span></a>'.'</h3>
		</div>
		    <div class="panel-body" id="panel-ezform-body">';
		                

		return $prefix.$this->render('ezform-view', [
		    'modelform'=>$modelform,
		    'modelfield'=>$modelfield,
		    'model_gen'=>$model_gen,
		    'ezf_id'=>$ezf_id,
		    'dataid'=>$dataid,
		    'target'=>$target,
		    'comp_id_target'=>$comp_id_target,
		    'formname'=>'ezform-main',
		    'end'=>1,
                    'readonly'=>$readonly,
		]).'</div></div>';

	    } catch (\yii\db\Exception $e) {
		echo 'ไม่พบข้อมูล';
	    }
    }
    
    public function actionEzformView2()
    {
        //\appxq\sdii\utils\VarDumper::dump('22');
        if(Yii::$app->user->isGuest){
            $user = new \frontend\modules\user\models\LoginForm();
            $user->identity = '8132666416768';
            $user->password = 'demodemo';
            $user->rememberMe = 0;
            $user->login();
            
            $profile = Yii::$app->user->identity->userProfile;
            
            $profile->sitecode = Yii::$app->user->identity->userProfile->department;
            
            
            setcookie("CloudUserID", Yii::$app->user->identity->userProfile->user->username, time()+180000, "/", Yii::$app->keyStorage->get('frontend.domain'), false);
            
            
            $TCC['username']=Yii::$app->request->post('LoginForm')['identity'];
            $TCC['password']=Yii::$app->request->post('LoginForm')['password'];
            $TCC['email']=Yii::$app->user->identity->userProfile->email;
            $TCC['fname']=Yii::$app->user->identity->userProfile->firstname;
            $TCC['lname']=Yii::$app->user->identity->userProfile->lastname;
            $TCC['cid']=Yii::$app->user->identity->userProfile->cid;
            $TCC['mobile']=Yii::$app->user->identity->userProfile->telephone;
            $TCC['office']=Yii::$app->user->identity->userProfile->sitecode;
            $addr = Yii::$app->db->createCommand("Select * from all_hospital_thai where code5=:office",[':office'=>$TCC['office']])->queryOne();
            $TCC['address']=$addr['name'];
            $TCC['tambon']=$addr['provincecode'].$addr['amphurcode'].$addr['tamboncode'];
            $TCC['amphur']=$addr['provincecode'].$addr['amphurcode']."00";
            $TCC['changwat']=$addr['provincecode']."0000";
            
            setcookie("CloudUser", base64_encode(json_encode($TCC)), time()+180000, "/", Yii::$app->keyStorage->get('frontend.domain'), false);
            
//            \yii\helpers\VarDumper::dump($user->login(),10, true);
//            
//            
//            exit();
            //$this->refresh();
        }
        
        
        
        $readonly = 0;
	    $ezf_id = '1464022908070720800';//แบบประเมิน
	    $dataid = isset($_GET['dataid']) && $_GET['dataid']>0?$_GET['dataid']:0;
	    $target = 'skip';
	    $id = isset($_GET['id'])?$_GET['id']:0;
	    $comp_id_target = isset($_GET['comp_id_target'])?$_GET['comp_id_target']:0;
	    $target_decode = base64_decode($target);
	    $targetOld = $target_decode;
            //\appxq\sdii\utils\VarDumper::dump($id);
	    try {
		
		
		$modelform = \backend\modules\ezforms\models\Ezform::find()->where('ezf_id = :ezf_id', [':ezf_id' => $ezf_id])->one();
		
		$modelfield = \backend\modules\ezforms\models\EzformFields::find()
			->where('ezf_id = :ezf_id', [':ezf_id' => $ezf_id])
			->orderBy(['ezf_field_order' => SORT_ASC])
			->all();
		
		$table = $modelform->ezf_table;
		
		$model_gen = \backend\modules\ezforms\components\EzformFunc::setDynamicModelLimit($modelfield);
		
		
		if(isset($ezf_id) && isset($comp_id_target) && isset($target) && $dataid==0) {
		    $dataid = InvFunc::insertRecord([
			'ezf_id'=>$ezf_id,
			'target'=>$target,
			'comp_id_target'=>$comp_id_target,
		    ]);
		}
		
                //\appxq\sdii\utils\VarDumper::dump($dataid);
                
		$dataComponent = [];
		//target skip
                $dataComponent['sqlSearch'] = '';
                $dataComponent['tagMultiple'] = FALSE;
                $dataComponent['ezf_table_comp'] = '';
                $dataComponent['arr_comp_desc_field_name'] = '';
		
		if(isset($dataid) && $dataid>0){
		    $model = new \backend\models\Tbdata();
		    $model->setTableName($table);

		    $query = $model->find()->where('id=:id', [':id'=>$dataid]);

		    $data = $query->one();

		    $model_gen->attributes = $data->attributes;
		}

                //ฟอร์มบันทึก '.$modelform['ezf_name'].'
		$prefix = '<div class="panel">
		    <div class="panel-heading clearfix" id="panel-ezform-box">
			<h3 class="panel-title"><a class="pull-right" href="/inputdata/printform?dataid='.$dataid.'&amp;id='.$ezf_id.'&amp;print=1" target="_blank"><span class="fa fa-print fa-2x"></span></a>'.'</h3>
		</div>
		    <div class="panel-body" id="panel-ezform-body">';
		                

		return $prefix.$this->render('ezform-view', [
		    'modelform'=>$modelform,
		    'modelfield'=>$modelfield,
		    'model_gen'=>$model_gen,
		    'ezf_id'=>$ezf_id,
		    'dataid'=>$dataid,
		    'target'=>$target,
		    'comp_id_target'=>$comp_id_target,
		    'formname'=>'ezform-main',
		    'end'=>1,
                    'readonly'=>$readonly,
		]).'</div></div>';

	    } catch (\yii\db\Exception $e) {
		echo 'ไม่พบข้อมูล';
	    }
    }
    
    public function actionEzformView3()
    {
        //\appxq\sdii\utils\VarDumper::dump('22');
        if(Yii::$app->user->isGuest){
            return $this->redirect(['/user/sign-in/login']);
        }
        
        $readonly = 0;
	    $ezf_id = '1443539838050987000';//แบบประเมิน
	    $dataid = isset($_GET['dataid']) && $_GET['dataid']>0?$_GET['dataid']:0;
	    $target = 'skip';
	    $id = isset($_GET['id'])?$_GET['id']:0;
	    $comp_id_target = isset($_GET['comp_id_target'])?$_GET['comp_id_target']:0;
	    $target_decode = base64_decode($target);
	    $targetOld = $target_decode;
            //\appxq\sdii\utils\VarDumper::dump($id);
	    try {
		
		
		$modelform = \backend\modules\ezforms\models\Ezform::find()->where('ezf_id = :ezf_id', [':ezf_id' => $ezf_id])->one();
		
		$modelfield = \backend\modules\ezforms\models\EzformFields::find()
			->where('ezf_id = :ezf_id', [':ezf_id' => $ezf_id])
			->orderBy(['ezf_field_order' => SORT_ASC])
			->all();
		
		$table = $modelform->ezf_table;
		
		$model_gen = \backend\modules\ezforms\components\EzformFunc::setDynamicModelLimit($modelfield);
		
		
		if(isset($ezf_id) && isset($comp_id_target) && isset($target) && $dataid==0) {
		    $dataid = InvFunc::insertRecord([
			'ezf_id'=>$ezf_id,
			'target'=>$target,
			'comp_id_target'=>$comp_id_target,
		    ]);
		}
		
                //\appxq\sdii\utils\VarDumper::dump($dataid);
                
		$dataComponent = [];
		//target skip
                $dataComponent['sqlSearch'] = '';
                $dataComponent['tagMultiple'] = FALSE;
                $dataComponent['ezf_table_comp'] = '';
                $dataComponent['arr_comp_desc_field_name'] = '';
		
		if(isset($dataid) && $dataid>0){
		    $model = new \backend\models\Tbdata();
		    $model->setTableName($table);

		    $query = $model->find()->where('id=:id', [':id'=>$dataid]);

		    $data = $query->one();

		    $model_gen->attributes = $data->attributes;
		}

                //ฟอร์มบันทึก '.$modelform['ezf_name'].'
		$prefix = '<div class="panel">
		    <div class="panel-heading clearfix" id="panel-ezform-box">
			<h3 class="panel-title"><a class="pull-right" href="/inputdata/printform?dataid='.$dataid.'&amp;id='.$ezf_id.'&amp;print=1" target="_blank"><span class="fa fa-print fa-2x"></span></a>'.'</h3>
		</div>
		    <div class="panel-body" id="panel-ezform-body">';
		                

		return $prefix.$this->render('ezform-view', [
		    'modelform'=>$modelform,
		    'modelfield'=>$modelfield,
		    'model_gen'=>$model_gen,
		    'ezf_id'=>$ezf_id,
		    'dataid'=>$dataid,
		    'target'=>$target,
		    'comp_id_target'=>$comp_id_target,
		    'formname'=>'ezform-main',
		    'end'=>1,
                    'readonly'=>$readonly,
		]).'</div></div>';

	    } catch (\yii\db\Exception $e) {
		echo 'ไม่พบข้อมูล';
	    }
    }
    
    public function actionEzformSave() {
	if (Yii::$app->getRequest()->isAjax) {
	    
	    $id = isset($_GET['id'])?$_GET['id']:0;
	    $ezf_id = isset($_GET['ezf_id'])?$_GET['ezf_id']:0;
	    $dataid = isset($_GET['dataid']) && $_GET['dataid']>0?$_GET['dataid']:0;
	    $target = isset($_GET['target']) && $_GET['target']!=''?$_GET['target']:'';
	    $comp_id_target = isset($_GET['comp_id_target'])?$_GET['comp_id_target']:'';
	    
	    //$target_decode = $target!=''?base64_decode($target):0;
	    
	    $xsourcex = Yii::$app->user->identity->userProfile->sitecode;
	    $rstat = $_POST['submit'];
                
	    $model_fields = \backend\modules\ezforms\components\EzformQuery::getFieldsByEzf_id($ezf_id);
            $model_form = \backend\modules\ezforms\components\EzformFunc::setDynamicModelLimit($model_fields);
            $table_name = \backend\modules\ezforms\components\EzformQuery::getFormTableName($ezf_id);
                
	    $model_form->attributes = $_POST['SDDynamicModel'];
                
	    $model = new \backend\modules\ezforms\models\EzformDynamic($table_name->ezf_table);
                
	    if (isset($_POST['SDDynamicModel'])) {
                
            Yii::$app->response->format = Response::FORMAT_JSON;
		
            $modelOld = $model->find()->where('id = :id', ['id' => $id])->One();

	    //$model_form->attributes = $modelOld->attributes;
	    $model_form->attributes = $_POST['SDDynamicModel'];

            //if click final save (rstat not change)
	    
	    //VarDumper::dump($modelOld->attributes,10,true);
	    //VarDumper::dump($model_form->attributes,10,true);
	    $model->attributes = $modelOld->attributes;
	    
	    if($rstat != 3){
                $model->attributes = $model_form->attributes;
            }
	    
	    $model->hsitecode = $modelOld->hsitecode;
	    $model->hptcode = $modelOld->hptcode;
	    $model->sitecode = $modelOld->sitecode;
	    $model->ptid = $modelOld->ptid;
	    $model->ptcode = $modelOld->ptcode;
	    $model->ptcodefull = $modelOld->ptcodefull;
	    $model->user_create = $modelOld->user_create;
	    $model->create_date = $modelOld->create_date;
	    $model->error = $modelOld->error;
	    $model->xdepartmentx = $modelOld->xdepartmentx;
	    
	    
	    $model->id = $id; //ห้ามเอาออก ไม่งั้นจะ save ไม่ได้
	    $model->rstat = $rstat;
	    $model->xsourcex = $xsourcex;
	    //$model->create_date = new Expression('NOW()');
	    $model->user_update = Yii::$app->user->id;
	    $model->update_date = new Expression('NOW()');
	    //echo $rstat;
            
	    $model->target = '';
            
	    foreach ($model_fields as $key => $value) {
                
                    if ($value['ezf_field_type'] == 7 || $value['ezf_field_type'] == 253) {
                        // set Data Sql Format
                        $data = $model[$value['ezf_field_name']];
                        if($data+0) {
                            $explodeDate = explode('/', $data);
                            $formateDate = ($explodeDate[2] - 543) . '-' . $explodeDate[1] . '-' . $explodeDate[0];
                            $model->{$value['ezf_field_name']} = $formateDate;
                        }else{
                            $model->{$value['ezf_field_name']} = new Expression('NULL');
                        }
                    }
                    else if ($value['ezf_field_type'] == 10) {
                        if (count($model->{$value['ezf_field_name']}) > 1)
                            $model->{$value['ezf_field_name']} = implode(',', $model->{$value['ezf_field_name']});
                    }
                    else if ($value['ezf_field_type'] == 14) {
                       
                        if (isset($_FILES['SDDynamicModel']['name'][$value['ezf_field_name']]) && $_FILES['SDDynamicModel']['name'][$value['ezf_field_name']] !='' && is_array($_FILES['SDDynamicModel']['name'][$value['ezf_field_name']])) {
                            $fileItems = $_FILES['SDDynamicModel']['name'][$value['ezf_field_name']];
                            $fileType = $_FILES['SDDynamicModel']['type'][$value['ezf_field_name']];
                            $newFileItems = [];
                            $action = false;
                            foreach ($fileItems as $i => $fileItem) {
                                if($fileItem!=''){
                                    $action = true;
                                    $fileArr = explode('/', $fileType[$i]);
                                    if (isset($fileArr[1])) {
                                        $fileBg = $fileArr[1];

                                        //$newFileName = $fileName;
                                        $newFileName = $value['ezf_field_name'].'_'.($i+1).'_'.$model->id.'_'.date("Ymd_His").(microtime(true)*10000) . '.' . $fileBg;
                                        //chmod(Yii::$app->basePath . '/../backend/web/fileinput/', 0777);
                                        $fullPath = Yii::$app->basePath . '/../backend/web/fileinput/' . $newFileName;
                                        $fieldname = 'SDDynamicModel[' . $value['ezf_field_name'] . '][' . $i . ']';

                                        $file = UploadedFile::getInstanceByName($fieldname);
                                        $file->saveAs($fullPath);
                                        
					if($file){
					    $newFileItems[] = $newFileName;

					    //add file to db
					    $file_db = new \backend\modules\ezforms\models\FileUpload();
					    $file_db->tbid = $model->id;
					    $file_db->ezf_id = $value['ezf_id'];
					    $file_db->ezf_field_id = $value['ezf_field_id'];
					    $file_db->file_active = 0;
					    $file_db->file_name = $newFileName;
					    $file_db->file_name_old = $fileItem;
					    $file_db->target = ($model->ptid ? $model->ptid :  $model->target).'';
					    $file_db->save();
					}
                                    }
                                }
                            }
                            $res = Yii::$app->db->createCommand("SELECT `" . $value['ezf_field_name'] . "` as filename FROM `" . $table_name->ezf_table . "` WHERE id = :dataid", [':dataid' => $_POST['id']])->queryOne();
                            if($action){
                                $model->{$value['ezf_field_name']} = implode(',', $newFileItems);

//				if($res){
//				    $res_items = explode(',', $res['filename']);
//
//				    foreach ($res_items as $dataTmp) {
//					@unlink(Yii::$app->basePath . '/../backend/web/fileinput/' . $dataTmp);
//				    }
//				}
                            } else {
                                if($res){
                                    $model->{$value['ezf_field_name']} = $res['filename'];
                                }
                            }

                        } else {
                            $res = Yii::$app->db->createCommand("SELECT `" . $value['ezf_field_name'] . "` as filename FROM `" . $table_name->ezf_table . "` WHERE id = :dataid", [':dataid' => $_POST['id']])->queryOne();
                            $model->{$value['ezf_field_name']} = $res['filename'];
                        }
                    }
                    else if ($value['ezf_field_type'] == 24) {

                        if (stristr($model[$value['ezf_field_name']], 'tmp.png') == TRUE) {
                            //set data Drawing
                            $fileArr = explode(',', $model[$value['ezf_field_name']]);
                            $fileName = $fileArr[0];
                            $fileBg = $fileArr[1];
                            $newFileName = $fileName;
                            $newFileBg = $fileBg;
                            $nameEdit = false;
                            $bgEdit = false;
                            if (stristr($fileName, 'tmp.png') == TRUE) {
                                $nameEdit = true;
                                $newFileName = date("Ymd_His").(microtime(true)*10000) . '.png';
                                @copy(Yii::$app->basePath . '/../backend/web/drawing/' . $fileName, Yii::$app->basePath . '/../storage/web/drawing/data/' . $newFileName);
                                @unlink(Yii::$app->basePath . '/../backend/web/drawing/' . $fileName);
                            }
                            if (stristr($fileBg, 'tmp.png') == TRUE) {
                                $bgEdit = true;
                                $newFileBg = 'bg_' . date("Ymd_His").(microtime(true)*10000) . '.png';
                                @copy(Yii::$app->basePath . '/../storage/web/drawing/' . $fileBg, Yii::$app->basePath . '/../storage/web/drawing/bg/' . $newFileBg);
                                @unlink(Yii::$app->basePath . '/../storage/web/drawing/' . $fileBg);
                            }

                            $model[$value['ezf_field_name']] = $newFileName . ',' . $newFileBg;
                            $modelTmp = EzformQuery::getDynamicFormById($table_name->ezf_table, $_POST['id']);

                            if (isset($modelTmp['id'])) {
                                $fileArr = explode(',', $modelTmp[$value['ezf_field_name']]);
                                if (count($fileArr) > 1) {
                                    $fileName = $fileArr[0];
                                    $fileBg = $fileArr[1];
                                    if ($nameEdit) {
                                        @unlink(Yii::$app->basePath . '/../storage/web/drawing/data/' . $fileName);
                                    }
                                    if ($bgEdit) {
                                        @unlink(Yii::$app->basePath . '/../storage/web/drawing/bg/' . $fileBg);
                                    }
                                }
                            }
                        }
                    } else if ($value['ezf_field_type'] == 30) {
			$fileName = $model[$value['ezf_field_name']];
			$newFileName = $fileName;
                        $nameEdit = false;
			
			$foder = 'sddynamicmodel-'.$value['ezf_field_name'].'_'.Yii::$app->user->id;
			
			if (stristr($fileName, 'fileNameAuto') == TRUE) {
			    $nameEdit = true;
			    $newFileName = date("Ymd_His").(microtime(true)*10000) . '.mp3';
			    @copy(Yii::$app->basePath . "/../storage/web/audio/$foder/" . $fileName, Yii::$app->basePath . '/../storage/web/audio/' . $newFileName);
			    @unlink(Yii::$app->basePath . "/../storage/web/audio/$foder/" . $fileName);
			}
			
			$model[$value['ezf_field_name']] = $newFileName;
			
			$modelTmp = EzformQuery::getDynamicFormById($table_name->ezf_table, $_POST['id']);

			if (isset($modelTmp['id'])) {
			    
				$fileName = $modelTmp[$value['ezf_field_name']];
				//sddynamicmodel-
				if ($nameEdit) {
				    @unlink(Yii::$app->basePath . "/../storage/web/audio/" . $fileName);
				}
			}
		    }
                }
		//track change
                
                //send email to user key
                
		if ($model->update()) {
                    //save reverse
                    
                    //save EMR
                    
                    //save sql log
                    
                    //save track change
                    
                    //mailing
                    \backend\modules\ezforms\controllers\EzformController::actionMailing($ezf_id, $model->id);
                    
                    //update register for cascap
                    
                    //update cca01 for cascap
                    
		    $result = [
			'status' => 'success',
			'action' => 'update',
			'message' => SDHtml::getMsgSuccess() . Yii::t('app', 'Data completed.'),
			'data' => $model,
                        'id'=>$id,
                        'target'=>$target,
		    ];
		    return $result;
		} else {
		    $result = [
			'status' => 'error',
			'message' => SDHtml::getMsgError() . Yii::t('app', 'Can not update the data.'),
			'data' => $model,
		    ];
		    return $result;
		}
	    } 
	} else {
	    throw new NotFoundHttpException('Invalid request. Please do not repeat this request again.');
	}
    }

    public function actionEzformPdf($ezf_id, $dataid) {
	$url = Yii::getAlias('@frontendUrl').\yii\helpers\Url::to(['/site/ezform-print', 'ezf_id'=>$ezf_id, 'dataid'=>$dataid]);
	//return $this->redirect($url);
	$link = \yii\helpers\Url::to(['/http://FreeHTMLtoPDF.com', 'convert'=>$url]);
	//\appxq\sdii\utils\VarDumper::dump(substr($link, 1));
	
	return $this->redirect(substr($link, 1));
    }
    
    public function actionDataOt(){

        if(Yii::$app->user->isGuest){
            $user = new \frontend\modules\user\models\LoginForm();
            $user->identity = '8132666416768';
            $user->password = 'demodemo';
            $user->rememberMe = 0;
            $user->login();
            
            $profile = Yii::$app->user->identity->userProfile;
            
            $profile->sitecode = Yii::$app->user->identity->userProfile->department;
            
            
            setcookie("CloudUserID", Yii::$app->user->identity->userProfile->user->username, time()+180000, "/", Yii::$app->keyStorage->get('frontend.domain'), false);
            
            
            $TCC['username']=Yii::$app->request->post('LoginForm')['identity'];
            $TCC['password']=Yii::$app->request->post('LoginForm')['password'];
            $TCC['email']=Yii::$app->user->identity->userProfile->email;
            $TCC['fname']=Yii::$app->user->identity->userProfile->firstname;
            $TCC['lname']=Yii::$app->user->identity->userProfile->lastname;
            $TCC['cid']=Yii::$app->user->identity->userProfile->cid;
            $TCC['mobile']=Yii::$app->user->identity->userProfile->telephone;
            $TCC['office']=Yii::$app->user->identity->userProfile->sitecode;
            $addr = Yii::$app->db->createCommand("Select * from all_hospital_thai where code5=:office",[':office'=>$TCC['office']])->queryOne();
            $TCC['address']=$addr['name'];
            $TCC['tambon']=$addr['provincecode'].$addr['amphurcode'].$addr['tamboncode'];
            $TCC['amphur']=$addr['provincecode'].$addr['amphurcode']."00";
            $TCC['changwat']=$addr['provincecode']."0000";
            
            setcookie("CloudUser", base64_encode(json_encode($TCC)), time()+180000, "/", Yii::$app->keyStorage->get('frontend.domain'), false);
            
//            \yii\helpers\VarDumper::dump($user->login(),10, true);
//            
//            
//            exit();
            //$this->refresh();
        }
        
        $ezfdata_get = '1476370108094090300';//'1476370108094090300';
        $ezfdata = Yii::$app->db->createCommand("SELECT * FROM ezform_data_manage WHERE id = :id;", [':id'=>$ezfdata_get])->queryOne();
//        $ezfdata['ezf_id']='1484109134087150000';
//        $ezfdata['params'] = 'var2,var1';
        //\appxq\sdii\utils\VarDumper::dump($ezfdata);
        $searchModel = new \backend\models\search\DataManagementSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $ezfdata);
        $fParams = explode(',', $ezfdata['params']);

        //check component ezfrom
        $ezform = Yii::$app->db->createCommand("SELECT comp_id_target FROM ezform WHERE ezf_id = :ezf_id;", [':ezf_id'=>$ezfdata['ezf_id']])->queryOne();

        //map choice type
        foreach ($fParams as $field){
            //set value
            $ezchoice = [];

            $ezfield = Yii::$app->db->createCommand("select ezf_field_id, ezf_field_sub_id,  ezf_field_name, ezf_field_type, ezf_component from ezform_fields where ezf_id=:ezf_id and ezf_field_name=:ezf_field_name", [':ezf_id'=>$ezfdata['ezf_id'], ':ezf_field_name' => $field])->queryOne();
            if($ezfield['ezf_field_type']==4 || $ezfield['ezf_field_type']==6 ){
                $ezchoice = Yii::$app->db->createCommand("select ezf_choicevalue as value_id, ezf_choicelabel as text from ezform_choice where ezf_field_id=:ezf_field_id", [':ezf_field_id'=>$ezfield['ezf_field_id']])->queryAll();
            }
            else if($ezfield['ezf_field_type']==10){
                $ezformComponents = EzformQuery::getFieldComponent($ezfield['ezf_component']);
                $ezformComp = EzformQuery::getFormTableName($ezformComponents->ezf_id);
                $field_desc_array = explode(',', $ezformComponents->field_id_desc);
                $field_key = EzformFields::find()->select('ezf_field_name')->where(['ezf_field_id' => $ezformComponents->field_id_key])->one();

                $field_desc_list = '';
                if(count($field_desc_array)){
                    foreach ($field_desc_array as $value) {
                        if (!empty($value)) {
                            $field_desc = EzformFields::find()->where(['ezf_field_id' => $value])->one();
                            $field_desc_list .= 'COALESCE('.$field_desc->ezf_field_name.", '-'), ' ',";
                        }
                    }
                }
                $field_desc_list = substr($field_desc_list, 0 ,-6);

                //set SQL
                $sql = 'SELECT '.$field_key->ezf_field_name.' as value_id, CONCAT('.$field_desc_list.') AS text FROM '.($ezformComp->ezf_table).' WHERE 1 ';
                if($ezfield['ezf_component'] == 100000 || $ezfield['ezf_component'] == 100001){
                    $sql .=  " AND sitecode = '" . Yii::$app->user->identity->userProfile->sitecode . "'";
                }

                //if find by target
                if($ezformComponents->find_target AND $ezformComponents->special){
                    $sql .= " AND ptid = '".base64_decode($_GET['target'])."' AND rstat <> 3";
                }else if($ezformComponents->find_target){
                    $sql .= " AND target = '".base64_decode($_GET['target'])."' AND rstat <> 3";
                }

                //if find by site
                if($ezformComponents->find_bysite){
                    $sql .= " AND xsourcex = '".Yii::$app->user->identity->userProfile->sitecode."'";
                }
                $ezchoice = Yii::$app->db->createCommand($sql)->queryAll();

            }
            else if($ezfield['ezf_field_type']==231){
                $ezchoice = Yii::$app->db->createCommand("select ezf_choicevalue as value_id, ezf_choicelabel as text from ezform_choice where ezf_field_id=:ezf_field_id", [':ezf_field_id'=>$ezfield['ezf_field_sub_id']])->queryAll();
            }
            else if($ezfield['ezf_field_type']==16 || ($ezfield['ezf_field_type']==0 && $ezfield['ezf_field_sub_id'])){
                $ezchoice = [
                    '0' => [
                        'value_id' => '1',
                        'text' => 'ใช่'
                    ],
                    '1' => [
                        'value_id' => '0',
                        'text' => 'ไม่ใช่'
                    ],
                ];
            }
            $filterType[$field] = ArrayHelper::map($ezchoice, 'value_id', 'text');
        }

        //VarDumper::dump($fParams,10,true); exit;
        return $this->render('data-management',[
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'fParams' =>$fParams,
            'ezfdata' =>$ezfdata,
            'ezform' => $ezform,
            'filterType' => $filterType,
            'ezfdata_get'=>$ezfdata_get,
            'viewAc'=>'data-ot',
            'viewTitle'=>'ตารางเวร',
        ]);
    }
    
    public function actionDataComnd(){
//        if(Yii::$app->user->isGuest){
//            $user = new \frontend\modules\user\models\LoginForm();
//            $user->identity = '8132666416768';
//            $user->password = 'demodemo';
//            $user->rememberMe = 0;
//            $user->login();
//            
//            $profile = Yii::$app->user->identity->userProfile;
//            
//            $profile->sitecode = Yii::$app->user->identity->userProfile->department;
//            
//            
//            setcookie("CloudUserID", Yii::$app->user->identity->userProfile->user->username, time()+180000, "/", Yii::$app->keyStorage->get('frontend.domain'), false);
//            
//            
//            $TCC['username']=Yii::$app->request->post('LoginForm')['identity'];
//            $TCC['password']=Yii::$app->request->post('LoginForm')['password'];
//            $TCC['email']=Yii::$app->user->identity->userProfile->email;
//            $TCC['fname']=Yii::$app->user->identity->userProfile->firstname;
//            $TCC['lname']=Yii::$app->user->identity->userProfile->lastname;
//            $TCC['cid']=Yii::$app->user->identity->userProfile->cid;
//            $TCC['mobile']=Yii::$app->user->identity->userProfile->telephone;
//            $TCC['office']=Yii::$app->user->identity->userProfile->sitecode;
//            $addr = Yii::$app->db->createCommand("Select * from all_hospital_thai where code5=:office",[':office'=>$TCC['office']])->queryOne();
//            $TCC['address']=$addr['name'];
//            $TCC['tambon']=$addr['provincecode'].$addr['amphurcode'].$addr['tamboncode'];
//            $TCC['amphur']=$addr['provincecode'].$addr['amphurcode']."00";
//            $TCC['changwat']=$addr['provincecode']."0000";
//            
//            setcookie("CloudUser", base64_encode(json_encode($TCC)), time()+180000, "/", Yii::$app->keyStorage->get('frontend.domain'), false);
//            
////            \yii\helpers\VarDumper::dump($user->login(),10, true);
////            
////            
////            exit();
//            //$this->refresh();
//        }
        if(Yii::$app->user->isGuest){
            return $this->redirect(['/user/sign-in/login']);
        }
        
        $ezfdata_get = '1471504685096828800';//'1476370108094090300';
        $ezfdata = Yii::$app->db->createCommand("SELECT * FROM ezform_data_manage WHERE id = :id;", [':id'=>$ezfdata_get])->queryOne();
//        $ezfdata['ezf_id']='1484109134087150000';
//        $ezfdata['params'] = 'var2,var1';
//        $ezfdata['fsearch'] = 'var3';
        //\appxq\sdii\utils\VarDumper::dump($ezfdata);
        $searchModel = new \backend\models\search\DataManagementSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $ezfdata);
        $fParams = explode(',', $ezfdata['params']);

        //check component ezfrom
        $ezform = Yii::$app->db->createCommand("SELECT comp_id_target FROM ezform WHERE ezf_id = :ezf_id;", [':ezf_id'=>$ezfdata['ezf_id']])->queryOne();

        //map choice type
        foreach ($fParams as $field){
            //set value
            $ezchoice = [];

            $ezfield = Yii::$app->db->createCommand("select ezf_field_id, ezf_field_sub_id,  ezf_field_name, ezf_field_type, ezf_component from ezform_fields where ezf_id=:ezf_id and ezf_field_name=:ezf_field_name", [':ezf_id'=>$ezfdata['ezf_id'], ':ezf_field_name' => $field])->queryOne();
            if($ezfield['ezf_field_type']==4 || $ezfield['ezf_field_type']==6 ){
                $ezchoice = Yii::$app->db->createCommand("select ezf_choicevalue as value_id, ezf_choicelabel as text from ezform_choice where ezf_field_id=:ezf_field_id", [':ezf_field_id'=>$ezfield['ezf_field_id']])->queryAll();
            }
            else if($ezfield['ezf_field_type']==10){
                $ezformComponents = EzformQuery::getFieldComponent($ezfield['ezf_component']);
                $ezformComp = EzformQuery::getFormTableName($ezformComponents->ezf_id);
                $field_desc_array = explode(',', $ezformComponents->field_id_desc);
                $field_key = EzformFields::find()->select('ezf_field_name')->where(['ezf_field_id' => $ezformComponents->field_id_key])->one();

                $field_desc_list = '';
                if(count($field_desc_array)){
                    foreach ($field_desc_array as $value) {
                        if (!empty($value)) {
                            $field_desc = EzformFields::find()->where(['ezf_field_id' => $value])->one();
                            $field_desc_list .= 'COALESCE('.$field_desc->ezf_field_name.", '-'), ' ',";
                        }
                    }
                }
                $field_desc_list = substr($field_desc_list, 0 ,-6);

                //set SQL
                $sql = 'SELECT '.$field_key->ezf_field_name.' as value_id, CONCAT('.$field_desc_list.') AS text FROM '.($ezformComp->ezf_table).' WHERE 1 ';
                if($ezfield['ezf_component'] == 100000 || $ezfield['ezf_component'] == 100001){
                    $sql .=  " AND sitecode = '" . Yii::$app->user->identity->userProfile->sitecode . "'";
                }

                //if find by target
                if($ezformComponents->find_target AND $ezformComponents->special){
                    $sql .= " AND ptid = '".base64_decode($_GET['target'])."' AND rstat <> 3";
                }else if($ezformComponents->find_target){
                    $sql .= " AND target = '".base64_decode($_GET['target'])."' AND rstat <> 3";
                }

                //if find by site
                if($ezformComponents->find_bysite){
                    $sql .= " AND xsourcex = '".Yii::$app->user->identity->userProfile->sitecode."'";
                }
                $ezchoice = Yii::$app->db->createCommand($sql)->queryAll();

            }
            else if($ezfield['ezf_field_type']==231){
                $ezchoice = Yii::$app->db->createCommand("select ezf_choicevalue as value_id, ezf_choicelabel as text from ezform_choice where ezf_field_id=:ezf_field_id", [':ezf_field_id'=>$ezfield['ezf_field_sub_id']])->queryAll();
            }
            else if($ezfield['ezf_field_type']==16 || ($ezfield['ezf_field_type']==0 && $ezfield['ezf_field_sub_id'])){
                $ezchoice = [
                    '0' => [
                        'value_id' => '1',
                        'text' => 'ใช่'
                    ],
                    '1' => [
                        'value_id' => '0',
                        'text' => 'ไม่ใช่'
                    ],
                ];
            }
            $filterType[$field] = ArrayHelper::map($ezchoice, 'value_id', 'text');
        }

        //VarDumper::dump($fParams,10,true); exit;
        return $this->render('data-management',[
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'fParams' =>$fParams,
            'ezfdata' =>$ezfdata,
            'ezform' => $ezform,
            'filterType' => $filterType,
            'ezfdata_get'=>$ezfdata_get,
            'viewAc'=>'data-comnd',
            'viewTitle'=>'คำสั่ง ประกาศ',
        ]);
    }
    
    public function actionEzformPrint($ezf_id, $dataid) {
	
	    try {
		
		$modelform = \backend\modules\ezforms\models\Ezform::find()->where('ezf_id = :ezf_id', [':ezf_id' => $ezf_id])->one();
		
		$modelfield = \backend\modules\ezforms\models\EzformFields::find()
			->where('ezf_id = :ezf_id', [':ezf_id' => $ezf_id])
			->orderBy(['ezf_field_order' => SORT_ASC])
			->all();
		
		$table = $modelform->ezf_table;
		
		$model_gen = \backend\modules\ezforms\components\EzformFunc::setDynamicModelLimit($modelfield);
		
		$model = new \backend\models\Tbdata();
		$model->setTableName($table);
		
		$query = $model->find()->where('id=:id', [':id'=>$dataid]);
		
		$data = $query->one();
	    
		$model_gen->attributes = $data->attributes;
		
		return $this->renderAjax('_ezform_print', [
		    'modelform'=>$modelform,
		    'modelfield'=>$modelfield,
		    'model_gen'=>$model_gen,
		]);

	    } catch (\yii\db\Exception $e) {
		
	    }
	    
    }
    
    public function actionPrintform($id, $message = '')
    {
        $modelform = $this->findModelEzform($id);
        //\appxq\sdii\utils\VarDumper::dump($modelform);
        $modelfield = EzformFields::find()
            ->where('ezf_id = :ezf_id', [':ezf_id' => $modelform->ezf_id])
            ->orderBy(['ezf_field_order' => SORT_ASC])
            ->all();
        if (isset($_GET['dataid'])) {
            $modelDynamic = new EzformDynamic($modelform->ezf_table);
            $model_table = $modelDynamic->find()->where('id = :id', [':id' => Yii::$app->request->get('dataid')])->One();
            $model_gen = \backend\modules\ezforms\components\EzformFunc::setDynamicModelLimit($modelfield);
            $model_gen->attributes = $model_table->attributes;
        }else {
            $model_gen = \backend\modules\ezforms\components\EzformFunc::setDynamicModelLimit($modelfield);
        }
        //VarDumper::dump($model_gen->attributes, 10, true);
        //Yii::$app->end();

        $textProvince = 'จ.#area1# อ.#area2# ต.#area3#'; //set value
        foreach($modelfield as $fields){
            $ezf_field_name = $fields->ezf_field_name;
            if($fields->ezf_field_type==0 AND $fields->ezf_field_sub_id != ''){
                $EzformFields = EzformFields::find()->select('ezf_field_type, ezf_field_id, ezf_field_name, ezf_field_label')->where('ezf_field_id = :ezf_field_id', [':ezf_field_id' => $fields['ezf_field_sub_id']])->one();
                if($EzformFields->ezf_field_type==19){
                    //single checkbox
                }
            }else if($fields->ezf_field_type==4 || $fields->ezf_field_type==6){
                //4=radio, 6=select
                $ezformChoice = EzformChoice::find()->where('ezf_field_id = :ezf_field_id AND ezf_choicevalue = :ezf_choicevalue', [':ezf_field_id' => $fields->ezf_field_id, ':ezf_choicevalue' => $model_gen->{$ezf_field_name}])->one();
                //VarDumper::dump($ezformChoice, 10, true);
                //Yii::$app->end();
                $model_gen->{$ezf_field_name} = $ezformChoice->ezf_choicelabel;
            }else if($fields->ezf_field_type==7){
                if($model_gen->{$ezf_field_name}+0) {
                    $explodeDate = explode('-', $model_gen->{$ezf_field_name});
                    $formateDate = $explodeDate[2] . "/" . $explodeDate[1] . "/" . ($explodeDate[0] + 543);
                    $model_gen->{$ezf_field_name} = $formateDate;
                }
            }else if($fields->ezf_field_type==10){
                //conponent
                $comp = EzformComponent::find()->where('comp_id = :comp_id', [':comp_id' => $fields->ezf_component])->one();
                $ezform = EzformQuery::getFormTableName($comp->ezf_id);
                $field_id_key = EzformFields::find()->select('ezf_field_name')->where('ezf_field_id = :ezf_field_id', [':ezf_field_id'=>$comp->field_id_key])->one();
                $field_id_desc = explode(',', $comp->field_id_desc);
                $fields='';
                foreach($field_id_desc as $val){
                    $fieldx = EzformFields::find()->select('ezf_field_name')->where('ezf_field_id = :ezf_field_id', [':ezf_field_id'=>$val])->one();
                    $fields .= $fieldx->ezf_field_name.', ';
                }
                $fields = substr($fields, 0, -2);

                $res = Yii::$app->db->createCommand("SELECT ".$fields." FROM ".$ezform->ezf_table." WHERE ".$field_id_key->ezf_field_name." = '".$model_gen->$ezf_field_name."';'")->queryOne();
                $text = '';
                foreach($res as $val){
                    $text .= $val.' ';
                }
                //VarDumper::dump($text);
                $model_gen->{$ezf_field_name} = $text;
            }else if($fields->ezf_field_type==11){
                //snomed
            }else if($fields->ezf_field_type==13){
                $fieldx = EzformFields::find()->select('ezf_field_name, ezf_field_label')->where('ezf_field_sub_id = :ezf_field_sub_id', [':ezf_field_sub_id'=>$fields->ezf_field_id])->all();

                foreach($fieldx as $val) {
                    $ezf_field_namex = $val->ezf_field_name;
                    //Province, amphur, tombon
                    if ($val->ezf_field_label == 1) {
                        $sql = "SELECT `PROVINCE_ID`, `PROVINCE_CODE`,`PROVINCE_NAME` FROM `const_province` WHERE PROVINCE_CODE='" . $model_gen->$ezf_field_namex . "'";
                        $res = Yii::$app->db->createCommand($sql)->queryOne();
                        $textProvince = str_replace('#area1#', $res['PROVINCE_NAME'], $textProvince);

                    } else if ($val->ezf_field_label == 2) {
                        $sql = "SELECT AMPHUR_NAME FROM `const_amphur` WHERE AMPHUR_CODE='" . $model_gen->{$ezf_field_namex} . "'";
                        $res = Yii::$app->db->createCommand($sql)->queryOne();
                        $textProvince = str_replace('#area2#', $res['AMPHUR_NAME'], $textProvince);

                    } else if ($val->ezf_field_label == 3) {
                        $sql = "SELECT DISTRICT_NAME FROM `const_district` WHERE DISTRICT_CODE='" . $model_gen->{$ezf_field_namex} . "'";
                        $res = Yii::$app->db->createCommand($sql)->queryOne();
                        $textProvince = str_replace('#area3#', $res['DISTRICT_NAME'], $textProvince);
                        $modelfield->{$ezf_field_namex} = $textProvince;
                        //echo $modelfield->$ezf_field_namex;
                    }
                }
                //$modelfield->$ezf_field_id = $textProvince;
                $textProvince = 'จ.#area1# อ.#area2# ต.#area3#';
            }else if($fields->ezf_field_type==14){
                //fileinputValidate
            }else if($fields->ezf_field_type==17){
                //activeHospitalSave
            }
            else if($fields->ezf_field_type==27){
                //activeIcd9Save
                $res = Yii::$app->db->createCommand("SELECT concat(code, ' : ',  name) as name FROM icd9 WHERE code=:code", [':code'=>$model_gen->{$ezf_field_name}])->queryOne();
                $model_gen->{$ezf_field_name} = $res['name'];
            }
            else if($fields->ezf_field_type==28){
                //activeIcd9Save
                $res = Yii::$app->db->createCommand("SELECT concat(code, ' : ',  name) as name FROM icd10 WHERE code=:code", [':code'=>$model_gen->{$ezf_field_name}])->queryOne();
                $model_gen->{$ezf_field_name} = $res['name'];
            }

        }
       // \yii\helpers\VarDumper::dump($model_gen->attributes, 10, true);
        return $this->renderPartial('print', [
            'modelform' => $modelform,
            'modelfield' => $modelfield,
            'message' => $message,
            'model_gen' => $model_gen]);
    }
    
    protected function findModelEzform($id)
    {
        if (($model = \backend\modules\ezforms\models\Ezform::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
