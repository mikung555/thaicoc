<?php

namespace frontend\modules\volunteer\controllers;

use Yii;
use yii\web\Controller;
use appxq\sdii\helpers\SDHtml;
use yii\web\Response;
use yii\filters\AccessControl;

class DefaultController extends Controller
{
    public function behaviors()
    {
        return [
	    'access' => [
		'class' => AccessControl::className(),
		'rules' => [
		    [
			'allow' => true,
			'actions' => ['index', 'myemr', 'content-alert', 'count'], 
			'roles' => ['@'],
		    ],
		    
		],
	    ],
            
        ];
    }
    
    public function actionIndex()
    {
        return $this->render('index');
    }
    
    public function actionMyemr()
    {
        return $this->render('myemr');
    }
    
    public function actionContentAlert()
    {
	 if (Yii::$app->getRequest()->isAjax) {
	     Yii::$app->response->format = Response::FORMAT_JSON;
	     
	     $userId = Yii::$app->user->id;
	     $model = \frontend\modules\volunteer\models\VolunteerJob::find()->where('created_by=:created_by AND job_active=1', [':created_by'=>$userId])->all();
	    
	     $html = $this->renderAjax('_items', ['model'=>$model]);
	    
	    $result = [
		'status' => 'error',
		'message' => SDHtml::getMsgError() . Yii::t('app', 'Can not create the data.'),
		'html' => $html,
	    ];
	    return $result;
	} else {
	    throw new NotFoundHttpException('Invalid request. Please do not repeat this request again.');
	}
    }
    
    public function actionCount()
    {
	 if (Yii::$app->getRequest()->isAjax) {
	     Yii::$app->response->format = Response::FORMAT_JSON;
	     
	     $userId = Yii::$app->user->id;
	     $model = \frontend\modules\volunteer\models\VolunteerJob::find()->where('created_by=:created_by AND job_active=1', [':created_by'=>$userId])->count();
	    
	    $result = [
		'status' => 'error',
		'message' => SDHtml::getMsgError() . Yii::t('app', 'Can not create the data.'),
		'num' => $model,
	    ];
	    return $result;
	} else {
	    throw new NotFoundHttpException('Invalid request. Please do not repeat this request again.');
	}
    }
    
    
}
