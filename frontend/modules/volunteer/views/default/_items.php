<?php
use appxq\sdii\helpers\SDNoty;
/**
 * _items file UTF-8
 * @author SDII <iencoded@gmail.com>
 * @copyright Copyright &copy; 2015 AppXQ
 * @license http://www.appxq.com/license/
 * @version 1.0.0 Date: 18 ธ.ค. 2559 9:32:46
 * @link http://www.appxq.com/
 */
if(!empty($model)){
 foreach ($model as $key => $value) {?>
    <li>
       <div class="itemAvatar">
	   <a class="calAction close avatar" title="ปิดแฟ้ม" data-url="<?=  \yii\helpers\Url::to(['/volunteer/volunteer-job/delete', 'id'=>$value['job_id']])?>" style="cursor: pointer" data-id="<?=$value['job_id']?>"><i class="glyphicon glyphicon-ban-circle"></i></a>
	   <a href="<?=\yii\helpers\Url::to(['/volunteer/volunteer-job/view', 'id'=>$value['job_id']])?>" class="items ">
	       <span class="avatarheading">[<?=$value['job_cid']?>] <?=$value['job_fname']?> <?=$value['job_lname']?></span>
	       <span><strong>วันที่: </strong> <?=\common\lib\sdii\components\utils\SDdate::mysql2phpDateTime($value['created_at'])?></span>
	       <span><strong>สถานะ: </strong> <?php
	       if($value['job_status']==0){
		   echo 'รอการอนุมัติ';
	       } elseif($value['job_status']==1){
		   echo 'อนุมัติแล้ว';
	       } elseif($value['job_status']==3){
		   echo 'ปิดแฟ้ม';
	       } 
	       ?></span>
	       <span class="avatarType"><strong><?=\common\lib\sdii\components\utils\SDdate::differenceTimer($value['created_at'])?></strong></span>
	   </a>
       </div>
   </li>
<?php }} else {
    
    echo '<br><div class="text-center"><i>ไม่พบข้อมูล</i></div>';
}
?>

<?php  $this->registerJs("

$('.calAction').on('click', function() {
    $(this).parent().parent().remove();
    endPerson($(this).attr('data-url'));
});

function endPerson(url){
    $.ajax({
	method: 'POST',
	url: url,
	dataType: 'JSON',
	success: function(result, textStatus) {
	    ". SDNoty::show('result.message', 'result.status') ."
	}
    });

}
");?>