<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\modules\volunteer\models\VolunteerJob */

$this->title = Yii::t('frontend', 'Create Volunteer Job');

?>
<div class="volunteer-job-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
