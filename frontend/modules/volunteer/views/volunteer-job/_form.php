<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use appxq\sdii\helpers\SDNoty;
use appxq\sdii\helpers\SDHtml;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $model frontend\modules\volunteer\models\VolunteerJob */
/* @var $form yii\bootstrap\ActiveForm */
?>

<div class="volunteer-job-form">

    <?php $form = ActiveForm::begin([
	'id'=>$model->formName(),
    ]); ?>
    <div class="row">
	    <div class="col-md-6 col-md-offset-3">
		<div class="panel panel-primary"> 
		    <div class="panel-heading"> 
			<h3 class="panel-title">เพิ่มผู้ป่วย</h3> 
		    </div> 
		    <div class="panel-body">
			
			<div class="modal-body">
	
<?= $form->field($model, 'job_cid')->textInput(['maxlength' => true]) ?>
			    <?= $form->field($model, 'job_fname')->textInput(['maxlength' => true]) ?>
			    <?= $form->field($model, 'job_lname')->textInput(['maxlength' => true]) ?>
	
	<?php
		if($model->job_type==1){
		    echo $form->field($model, 'job_parent')->widget(kartik\select2\Select2::className(),[
				//'initValueText' => $model->job_parent, // set the initial display text
				'options' => ['placeholder' => 'ค้นหา อสม.'],
				'pluginOptions' => [
					'allowClear' => true,
					'minimumInputLength' => 0,
					'ajax' => [
					    'url' => \yii\helpers\Url::to(['/volunteer/volunteer-job/author-list']),
					    'dataType' => 'json',
					    'data' => new JsExpression('function(params) { return {q:params.term}; }')
					],
					'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
					'templateResult' => new JsExpression('function(author) { return author.text; }'),
					'templateSelection' => new JsExpression('function (author) { return author.text; }'),
				],
			    ]) ;
		} else {
		    echo $form->field($model, 'job_parent')->hiddenInput()->label(false);
		    
		}
		
		?>
		

	
	<?= $form->field($model, 'job_id')->hiddenInput()->label(false) ?>
	<?= $form->field($model, 'job_start')->hiddenInput()->label(false) ?>

	<?= $form->field($model, 'job_end')->hiddenInput()->label(false) ?>

	<?= $form->field($model, 'job_type')->hiddenInput()->label(false) ?>

	<?= $form->field($model, 'sitecode')->hiddenInput()->label(false) ?>

	<?= $form->field($model, 'job_status')->hiddenInput()->label(false) ?>

	<?= $form->field($model, 'job_active')->hiddenInput()->label(false) ?>

	<?= $form->field($model, 'created_by')->hiddenInput()->label(false) ?>

	<?= $form->field($model, 'created_at')->hiddenInput()->label(false) ?>

	<?= $form->field($model, 'updated_by')->hiddenInput()->label(false) ?>

	<?= $form->field($model, 'updated_at')->hiddenInput()->label(false) ?>

    </div>
    <div class="modal-footer">
	<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	<?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>
		    </div> 
		</div>
		
		
	    </div>
	</div>

    

    <?php ActiveForm::end(); ?>

</div>

<?php  $this->registerJs("


");?>