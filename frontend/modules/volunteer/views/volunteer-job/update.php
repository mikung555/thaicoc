<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\modules\volunteer\models\VolunteerJob */

$this->title = Yii::t('frontend', 'Update {modelClass}: ', [
    'modelClass' => 'Volunteer Job',
]) . ' ' . $model->job_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Volunteer Jobs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->job_id, 'url' => ['view', 'id' => $model->job_id]];
$this->params['breadcrumbs'][] = Yii::t('frontend', 'Update');
?>
<div class="volunteer-job-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
