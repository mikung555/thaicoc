<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\helpers\Url;
use appxq\sdii\widgets\GridView;
use appxq\sdii\widgets\ModalForm;
use appxq\sdii\helpers\SDNoty;
use appxq\sdii\helpers\SDHtml;

/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\volunteer\models\VolunteerJobSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('frontend', 'Volunteer Jobs');
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="volunteer-job-index">

    <div class="sdbox-header">
	<h3><?=  Html::encode($this->title) ?></h3>
    </div>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p style="padding-top: 10px;">
	<span class="label label-primary">Notice</span>
	<?= Yii::t('app', 'You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b> or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.') ?>
    </p>

    <?php  Pjax::begin(['id'=>'volunteer-job-grid-pjax']);?>
    <?= GridView::widget([
	'id' => 'volunteer-job-grid',
	'panelBtn' => Html::button(SDHtml::getBtnAdd(), ['data-url'=>Url::to(['volunteer-job/create']), 'class' => 'btn btn-success btn-sm', 'id'=>'modal-addbtn-volunteer-job']). ' ' .
		      Html::button(SDHtml::getBtnDelete(), ['data-url'=>Url::to(['volunteer-job/deletes']), 'class' => 'btn btn-danger btn-sm', 'id'=>'modal-delbtn-volunteer-job', 'disabled'=>true]),
	'dataProvider' => $dataProvider,
	'filterModel' => $searchModel,
        'columns' => [
	    [
		'class' => 'yii\grid\CheckboxColumn',
		'checkboxOptions' => [
		    'class' => 'selectionVolunteerJobIds'
		],
		'headerOptions' => ['style'=>'text-align: center;'],
		'contentOptions' => ['style'=>'width:40px;text-align: center;'],
	    ],
	    [
		'class' => 'yii\grid\SerialColumn',
		'headerOptions' => ['style'=>'text-align: center;'],
		'contentOptions' => ['style'=>'width:60px;text-align: center;'],
	    ],

            'job_id',
            'job_cid',
            'job_fname',
            'job_lname',
            'job_start',
            // 'job_end',
            // 'job_type',
            // 'job_parent',
            // 'sitecode',
            // 'job_status',
            // 'job_active',
            // 'created_by',
            // 'created_at',
            // 'updated_by',
            // 'updated_at',

	    [
		'class' => 'appxq\sdii\widgets\ActionColumn',
		'contentOptions' => ['style'=>'width:80px;text-align: center;'],
		'template' => '{view} {update} {delete}',
	    ],
        ],
    ]); ?>
    <?php  Pjax::end();?>

</div>

<?=  ModalForm::widget([
    'id' => 'modal-volunteer-job',
    'size'=>'modal-lg',
]);
?>

<?php  $this->registerJs("
$('#volunteer-job-grid-pjax').on('click', '#modal-addbtn-volunteer-job', function() {
    modalVolunteerJob($(this).attr('data-url'));
});

$('#volunteer-job-grid-pjax').on('click', '#modal-delbtn-volunteer-job', function() {
    selectionVolunteerJobGrid($(this).attr('data-url'));
});

$('#volunteer-job-grid-pjax').on('click', '.select-on-check-all', function() {
    window.setTimeout(function() {
	var key = $('#volunteer-job-grid').yiiGridView('getSelectedRows');
	disabledVolunteerJobBtn(key.length);
    },100);
});

$('#volunteer-job-grid-pjax').on('click', '.selectionVolunteerJobIds', function() {
    var key = $('input:checked[class=\"'+$(this).attr('class')+'\"]');
    disabledVolunteerJobBtn(key.length);
});

$('#volunteer-job-grid-pjax').on('dblclick', 'tbody tr', function() {
    var id = $(this).attr('data-key');
    modalVolunteerJob('".Url::to(['volunteer-job/update', 'id'=>''])."'+id);
});	

$('#volunteer-job-grid-pjax').on('click', 'tbody tr td a', function() {
    var url = $(this).attr('href');
    var action = $(this).attr('data-action');

    if(action === 'update' || action === 'view') {
	modalVolunteerJob(url);
    } else if(action === 'delete') {
	yii.confirm('".Yii::t('app', 'Are you sure you want to delete this item?')."', function() {
	    $.post(
		url
	    ).done(function(result) {
		if(result.status == 'success') {
		    ". SDNoty::show('result.message', 'result.status') ."
		    $.pjax.reload({container:'#volunteer-job-grid-pjax'});
		} else {
		    ". SDNoty::show('result.message', 'result.status') ."
		}
	    }).fail(function() {
		". SDNoty::show("'" . SDHtml::getMsgError() . "Server Error'", '"error"') ."
		console.log('server error');
	    });
	});
    }
    return false;
});

function disabledVolunteerJobBtn(num) {
    if(num>0) {
	$('#modal-delbtn-volunteer-job').attr('disabled', false);
    } else {
	$('#modal-delbtn-volunteer-job').attr('disabled', true);
    }
}

function selectionVolunteerJobGrid(url) {
    yii.confirm('".Yii::t('app', 'Are you sure you want to delete these items?')."', function() {
	$.ajax({
	    method: 'POST',
	    url: url,
	    data: $('.selectionVolunteerJobIds:checked[name=\"selection[]\"]').serialize(),
	    dataType: 'JSON',
	    success: function(result, textStatus) {
		if(result.status == 'success') {
		    ". SDNoty::show('result.message', 'result.status') ."
		    $.pjax.reload({container:'#volunteer-job-grid-pjax'});
		} else {
		    ". SDNoty::show('result.message', 'result.status') ."
		}
	    }
	});
    });
}

function modalVolunteerJob(url) {
    $('#modal-volunteer-job .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
    $('#modal-volunteer-job').modal('show')
    .find('.modal-content')
    .load(url);
}

");?>