<?php
/**
 * Created by PhpStorm.
 * User: hackablex
 * Date: 7/26/2016 AD
 * Time: 13:23
 */

use appxq\sdii\widgets\GridView;
use yii\widgets\Pjax;
use kartik\helpers\Html;
use yii\helpers\Url;
use appxq\sdii\helpers\SDNoty;
use appxq\sdii\widgets\ModalForm;

$this->registerJS($ezfdata['js']);
?>
<div id="inv-person-grid-pjax">
<h3><?=$viewTitle?></h3><hr>
<h4>เลือกช่วงเวลาแสดงข้อมูล</h4>
<div class="row">
    <form id="view-data-range" action="" method="get">
        <div class="col-md-6">
            จาก
            <?php
            echo \common\lib\damasac\widgets\DMSDateWidget::widget([
                //'model'=>$model,
                'id'=>'start_date',
                'value' => $_GET['start_date'] ? $_GET['start_date'] : '01'.(date('-m-').(date('Y')+543)),
                'name' =>'start_date',
            ]);
            ?>
        </div>
        <div class="col-md-6">
            ถึง
            <?php
            echo \common\lib\damasac\widgets\DMSDateWidget::widget([
                //'model'=>$model,
                'id'=>'end_date',
                'value' => $_GET['end_date'] ? $_GET['end_date'] : (date('d-m-').(date('Y')+543)),
                'name' =>'end_date',
            ]);
            ?>
        </div>
        <div class="col-md-12 text-right">
            <br>
            <?php
            echo ' '.Html::a('<span class="fa fa-list"></span> แสดงข้อมูลทั้งหมด',
                Url::to(['/volunteer/fix-app/'.$viewAc, 'ezfdata' => $ezfdata_get, 'show'=>'all']),
                [
                    'class' => 'btn btn-danger  btn-md',
                    'type' =>'submit',
                ]);
            ?>
            <?php
            echo Html::hiddenInput('ezfdata', $ezfdata_get);
            echo Html::button('<span class="fa fa-search"></span> ค้นหา', [
                'class' => 'btn btn-primary btn-md',
                'type' =>'submit',
            ]);
            ?>
        </div>
    </form>
</div>
<hr>

    <?php

    if($ezfdata_get == '1476696513076161100'){
        $total = Yii::$app->db->createCommand('
            SELECT COUNT(*) FROM `dep_calendar`
        ', [':status' => 1])->queryScalar();

        $dpFix = new \yii\data\SqlDataProvider([
            'sql' => 'SELECT * FROM `dep_calendar`',
            'totalCount' => $total,
            'sort' => [
                'attributes' => [
                    'hday', 'hmonth','hname'
                ],
            ],
            'pagination' => [
                'pageSize' => 50,
            ],
        ]);


        echo GridView::widget([
            'panelBtn' => '',
            'dataProvider' => $dpFix,
            'columns' => [
                [
                    'header'=>'วันที่',
                    'attribute' => 'hday',
                ],
                [
                    'header'=>'วันเดือน',
                    'attribute' => 'hmonth',
                ],
                [
                    'header'=>'วันหยุด',
                    'attribute' => 'hname',
                ]
            ],
        ]);

    }
    

    $fieldSearch = $_GET['DataManagementSearch'];
    $columns = [
        [
            'class' => 'yii\grid\SerialColumn',
            'headerOptions'=>['style'=>'text-align: center;'],
            'contentOptions'=>['style'=>'width:50px;text-align: center;'],
        ],
    ];
    foreach ($fParams as $val){
        $columns[] = [
            'attribute' => $val,
            'value' =>  function($model) use($val){
                return $model->{$val} !='' ? $model->{$val} : '';
            },
            'filter' => count($filterType[$val]) ? (
            \kartik\widgets\Select2::widget([
                'name' => 'DataManagementSearch['.$val.']',
                'value' => is_null($fieldSearch[$val]) ? null : $fieldSearch[$val],
                'data' => $filterType[$val],
                'options' => ['placeholder' => 'Select for filter ...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ])
            ) : true,
            //'filterOptions'=> count($filterType[$val]) ? ['style'=>'width: 50px;'] : [],
            'headerOptions'=> count($filterType[$val]) ? ['style'=>'width: 50px; text-align: center;'] : ['style'=>'text-align: center;'],
            'contentOptions'=>['style'=>'text-align: left;', 'field-id'=>$val],
        ];
    }
    $columns[] = ['class' => 'yii\grid\ActionColumn',
            'header'=>'จัดการ',
            'template' => '{view} {print}',
            'buttons' => [

                //view button
                'view' => function ($url, $model) use ($ezfdata) {
                    return Html::a('<span class="fa fa-edit"></span>', NULL, [
                        'data-url'=>Url::to(['/volunteer/fix-app/ezform-print', 
                        'ezf_id'=>$ezfdata['ezf_id'],
                        'readonly'=>0,
                        'dataid'=>$model['id']]),
                        'title' => Yii::t('app', 'View'),
                        'class'=>'btn btn-info btn-xs print-ezform',
                    ]);
		    
                },
			
                /*
                'update' => function ($url, $model) use ($ezfdata) {
                    return Html::a('<span class="fa fa-edit"></span> Edit', ['/inputdata/redirect-page', 'ezf_id' => $ezfdata['ezf_id'], 'dataid'=>$model->id, 'rurl' => base64_encode(Yii::$app->request->url)], [
                        'title' => Yii::t('app', 'Update'),
                        'class'=>'btn btn-warning btn-xs',
                        'target'=>'_blank'
                    ]);

                },
                */
            ],
            'headerOptions'=>['style'=>'text-align: center;'],
            'contentOptions' => ['style' => 'width:100px;text-align: center;']
        ];
?>

<?php

    echo GridView::widget([
        'panelBtn' => '',//Html::a('<i class="fa fa-plus"></i> เพิ่มข้อมูล', $url, ['data-url'=>$url, 'class' => 'btn btn-success btn-sm', 'id'=>'btn-add-data', 'disabled'=>false]),
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $columns,
]); ?>
</div>
<?=  ModalForm::widget([
    'id' => 'modal-print',
    'size'=>'modal-lg',
    'tabindexEnable'=>false,
    'options'=>['style'=>'overflow-y:scroll;']
]);
?>

<?php  $this->registerJs("


$('#inv-person-grid-pjax').on('click', '.print-ezform', function() {
    var url = $(this).attr('data-url');
    modalEzfrom(url);
});

$('#inv-person-grid-pjax').on('dblclick', 'tbody tr', function() {
    var id = $(this).attr('data-key');
    modalEzfrom('".Url::to(['/volunteer/fix-app/ezform-print', 
		'ezf_id'=>$ezfdata['ezf_id'],
		'readonly'=>0,
		'dataid'=>''])."'+id);
});	

function modalEzfrom(url) {
    $('#modal-print .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
    $('#modal-print').modal('show');
    $.ajax({
	method: 'POST',
	url: url,
	dataType: 'HTML',
	success: function(result, textStatus) {
	    $('#modal-print .modal-content').html(result);
	    return false;
	}
    });
}


");?>