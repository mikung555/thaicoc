<?php

use Yii;
use yii\helpers\Html;
use kartik\select2\Select2;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;
use appxq\sdii\helpers\SDNoty;
use appxq\sdii\helpers\SDHtml;

$this->registerCssFile('/css/ezform.css');


$form = backend\modules\ezforms\components\EzActiveForm::begin([
	    'id'=>$formname,
	    'action' => ['/inv/inv-person/ezform-save', 'id'=>$dataid,
		'ezf_id'=>$ezf_id,
		    'dataid'=>$dataid,
		    'target'=>$target,
		    'comp_id_target'=>$comp_id_target,
                    'comp_target'=>$comp_target,
		],
	    'options' => ['enctype' => 'multipart/form-data']
	]);
?>
	<?php 
backend\assets\EzfGenAsset::register($this);
 ?>    
<?php
echo '<div class="row">';

$sql = "SELECT rstat, xsourcex FROM {$modelform['ezf_table']} WHERE id=:id ";
$dataModel = Yii::$app->db->createCommand($sql, [':id'=>$dataid])->queryOne();
//set data
$input_target = $target;
$input_ezf_id= $ezf_id;
$input_dataid = $dataid;

//1437377239070461302 f1v3
foreach ($modelfield as $field) {

    //reference field
    $options_input = [];
    if($field->ezf_field_type == 18) {
        //ถ้ามีการกำหนดเป้าหมาย

        if ($field->ezf_field_val == 1) {
            //แสดงอย่างเดียวแก้ไขไม่ได้ (Read-only)
            $modelDynamic = \backend\modules\ezforms\components\EzformQuery::getFormTableName($field->ezf_field_ref_table);
            //set form disable
            $options_input['readonly'] = true;
            $form->attributes['rstat'] = 0;
        } else if ($field->ezf_field_val == 2) {
            //ถ้ามีการแก้ไขค่า จะอัพเดจค่านั้นทั้งตารางต้นทาง - ปลายทาง
            $modelDynamic = \backend\modules\ezforms\components\EzformQuery::getFormTableName($field->ezf_id);
            $form->attributes['rstat'] = $dataModel['rstat'];
        } else if ($field->ezf_field_val == 3) {
            //แก้ไขเฉพาะตารางปลายทาง
            $modelDynamic = \backend\modules\ezforms\components\EzformQuery::getFormTableName($field->ezf_field_ref_table);
            //\yii\helpers\VarDumper::dump($ezfField, 10, true);
            //echo $field->ezf_field_ref_field,'-';
            $options_input['readonly'] = true;
            $form->attributes['rstat'] = 0;
        }
        $ezfTable = $modelDynamic->ezf_table;

        $ezfField = \backend\modules\ezforms\models\EzformFields::find()->select('ezf_field_name')->where('ezf_field_id = :ezf_field_id', [':ezf_field_id' => $field->ezf_field_ref_field])->one();
        if(!$ezfField->ezf_field_name){
            $ezfField = \backend\modules\ezforms\models\EzformFields::find()->select('ezf_field_name, ezf_field_label')->where('ezf_field_id = :ezf_field_id', [':ezf_field_id' => $field->ezf_field_id])->one();
            $html = '<h1>Error</h1><hr>';
            $html .= '<h3>เนื่องการเชื่อมโยงของประเภทคำถาม Reference field ผิดพลาด การแก้ไขคือ ลบคำถามออกแล้วสร้างใหม่</h3>';
            $html .= '<h4>คำถามที่ผิดพลาดคือ : </h4>'. $ezfField->ezf_field_name.' ('. $ezfField->ezf_field_label.')';
            echo $html;
            Yii::$app->end();
        }
        $modelDynamic = new backend\modules\ezforms\models\EzformDynamic($ezfTable);

        //กำหนดตัวแปร
        $field_name = $field->ezf_field_name; //field name ต้นทาง
        $field_name_ref = $ezfField->ezf_field_name; //field name ปลายทาง

        //ดูตาราง Ezform ฟิลด์ target ที่มีเป้าหมายเดียวกัน
        if ($input_target == 'byme' || $input_target == 'skip' || $input_target == 'all') {
            $target = \backend\modules\ezforms\components\EzformQuery::getTargetFormEzf($input_ezf_id, $input_dataid);
            $target = $target['ptid'];
        } else {
            $target = base64_decode($input_target);
        }
        //หา target ใน site ตัวเองก่อน (ก็ไม่แนะนำ)
        $res = $modelDynamic->find()->select($field_name_ref)->where('ptid = :target AND ' . $field_name_ref . ' <> "" AND xsourcex = :xsourcex AND rstat <>3', [':target' => $target, ':xsourcex' => $dataModel['xsourcex']])->orderBy('create_date DESC');

        //echo $target.' -';
        //หาที่ target ก่อน
        if ($res->count()) {
            $model_table = $res->One();
            if ($field->ezf_field_val == 1) {
                //echo 'target 1-1<br>';
                //เปลี่ยนค่า field name ระหว่างต้นทาง และปลายทาง
                $model_gen->$field_name = $model_table->$field_name_ref;
                $model_gen->attributes[$field_name] = $model_table->$field_name_ref;
            } else if ($field->ezf_field_val == 2) {

            } else if ($field->ezf_field_val == 3) {
                if ($input_dataid) {
                    //echo 'target 1<br>';
                    $res = \backend\modules\ezforms\components\EzformQuery::getReferenceData($field->ezf_id, $field->ezf_field_id, $input_dataid, $field_name_ref);
                    //\yii\helpers\VarDumper::dump($model_table->attributes, 10,true);
                    if ($res[$field_name_ref]) {
                        $model_table->$field_name_ref = $res[$field_name_ref];
                    }
                    //เปลี่ยนค่า field name ระหว่างต้นทาง และปลายทาง
                    $model_gen->$field_name = $model_table->$field_name_ref;
                    $model_gen->attributes[$field_name] = $model_table->$field_name_ref;
                } else {
                    //\yii\helpers\VarDumper::dump($model_table->attributes, 10,true);
                    // echo 'target else 1<br>';
                    //เปลี่ยนค่า field name ระหว่างต้นทาง และปลายทาง
                    $model_gen->$field_name = $model_table->$field_name_ref;
                    $model_gen->attributes[$field_name] = $model_table->$field_name_ref;
                }
            }

        } else {
            //กรณีหาไม่เจอ ให้ดู primary key
            $res = $modelDynamic->find()->where('id = :id', [':id' => $target]);
            if ($res->count()) {
                $model_table = $res->One();

                if ($field->ezf_field_val == 1) {
                    //echo 'target 2-1<br>';
                    //เปลี่ยนค่า field name ระหว่างต้นทาง และปลายทาง
                    $model_gen->$field_name = $model_table->$field_name_ref;
                    $model_gen->attributes[$field_name] = $model_table->$field_name_ref;
                } else if ($field->ezf_field_val == 2) {

                } else if ($field->ezf_field_val == 3) {
                    if ($input_dataid) {
                        //echo 'target 2<br>';
                        $res = \backend\modules\ezforms\components\EzformQuery::getReferenceData($field->ezf_id, $field->ezf_field_id, $input_dataid, $field_name_ref);
                        if ($res[$field_name_ref]) {
                            $model_table->$field_name_ref = $res[$field_name_ref];
                        }
                        //เปลี่ยนค่า field name ระหว่างต้นทาง และปลายทาง
                        $model_gen->$field_name = $model_table->$field_name_ref;
                        $model_gen->attributes[$field_name] = $model_table->$field_name_ref;
                    } else {
                        //echo 'target else 2<br>';
                        //\yii\helpers\VarDumper::dump($model_table->attributes, 10,true);

                        //เปลี่ยนค่า field name ระหว่างต้นทาง และปลายทาง
                        $model_gen->$field_name = $model_table->$field_name_ref;
                        $model_gen->attributes[$field_name] = $model_table->$field_name_ref;
                    }
                }
            }
        }

        // yii\helpers\VarDumper::dump($model_table, 10, true);
        //Yii::$app->end();
    }else if($modelEzform->query_tools == 2 && Yii::$app->keyStorage->get('frontend.domain') == "cascap.in.th" && ($input_ezf_id == "1437377239070461301" || $input_ezf_id == "1437377239070461302")){
        //not doing every thing
    }else if($modelEzform->query_tools == 2){
        $form->attributes['rstat'] = $dataModel['rstat'];
    }
    //end reference field

    echo \backend\modules\ezforms\components\EzformFunc::getTypeEprintform($model_gen, $field, $form);
    
}
echo '</div>';

foreach ($modelfield as $value) {
    $inputId = Html::getInputId($model_gen, $value['ezf_field_name']);
    $inputValue = Html::getAttributeValue($model_gen, $value['ezf_field_name']);

    $dataCond = backend\modules\ezforms\components\EzformQuery::getCondition($value['ezf_id'], $value['ezf_field_name']);

    
    
    if ($dataCond) {
	
	//Edit Html
	$fieldId = Html::getInputId($model_gen, $value['ezf_field_name']);
	if ($value['ezf_field_type'] == 4) {
	    $fieldId = $value['ezf_field_name'];
	}
	$enable = TRUE;
	foreach ($dataCond as $index => $cvalue) {
	    $dataCond[$index]['cond_jump'] = json_decode($cvalue['cond_jump']);
	    $dataCond[$index]['cond_require'] = json_decode($cvalue['cond_require']);
		
	    if ($inputValue == $cvalue['ezf_field_value'] || $inputValue == '') {
		
		if ($value['ezf_field_type'] == '4' || $value['ezf_field_type'] == '6') {
		    if ($enable) {
			$enable = false;
			$jumpArr = json_decode($cvalue['cond_jump']);
			if (is_array($jumpArr)) {
			    foreach ($jumpArr as $j => $jvalue) {
				$this->registerJs("
										var fieldIdj = '" . $jvalue . "';
										var inputIdj = '" . $fieldId . "';
										var valueIdj = '" . $inputValue . "';
										var fixValuej = '" . $cvalue['ezf_field_value'] . "';
										var fTypej = '" . $value['ezf_field_type'] . "';
										domHtml(fieldIdj, inputIdj, valueIdj, fixValuej, fTypej, 'none');
									");
			    }
			}

			$requireArr = json_decode($cvalue['cond_require']);
			if (is_array($requireArr)) {
			    foreach ($requireArr as $r => $rvalue) {
				$this->registerJs("
										var fieldIdr = '" . $rvalue . "';
										var inputIdr = '" . $fieldId . "';
										var valueIdr = '" . $inputValue . "';
										var fixValuer = '" . $cvalue['ezf_field_value'] . "';
										var fTyper = '" . $value['ezf_field_type'] . "';
										domHtml(fieldIdr, inputIdr, valueIdr, fixValuer, fTyper, 'block');
									");
			    }
			}
		    }
		} else {
		    $jumpArr = json_decode($cvalue['cond_jump']);
		    if (is_array($jumpArr)) {
			foreach ($jumpArr as $j => $jvalue) {
			    $this->registerJs("
									    var fieldIdj = '" . $jvalue . "';
									    var inputIdj = '" . $fieldId . "';
									    var valueIdj = '" . $inputValue . "';
									    var fixValuej = '" . $cvalue['ezf_field_value'] . "';
									    var fTypej = '" . $value['ezf_field_type'] . "';
									    domHtml(fieldIdj, inputIdj, valueIdj, fixValuej, fTypej, 'block');
								    ");
			}
		    }

		    $requireArr = json_decode($cvalue['cond_require']);
		    if (is_array($requireArr)) {
			foreach ($requireArr as $r => $rvalue) {
			    $this->registerJs("
									    var fieldIdr = '" . $rvalue . "';
									    var inputIdr = '" . $fieldId . "';
									    var valueIdr = '" . $inputValue . "';
									    var fixValuer = '" . $cvalue['ezf_field_value'] . "';
									    var fTyper = '" . $value['ezf_field_type'] . "';
									    domHtml(fieldIdr, inputIdr, valueIdr, fixValuer, fTyper, 'none');
								    ");
			}
		    }
		}
	    }
	}
	
	//Add Event
	if ($value['ezf_field_type'] == 20 || $value['ezf_field_type'] == 0 || $value['ezf_field_type'] == 16) {
	    $this->registerJs("
		    var dataCond = '" . yii\helpers\Json::encode($dataCond) . "';
		    var inputId = '" . $inputId . "';
		    eventCheckBox(inputId, dataCond);
		    setCheckBox(inputId, dataCond);
		");
	} else if ($value['ezf_field_type'] == 6) {
	    $this->registerJs("
		    var dataCond = '" . yii\helpers\Json::encode($dataCond) . "';
		    var inputId = '" . $inputId . "';
		    eventSelect(inputId, dataCond);
		    setSelect(inputId, dataCond);
		");
	} else if ($value['ezf_field_type'] == 4) {
	    $this->registerJs("
		    var dataCond = '" . yii\helpers\Json::encode($dataCond) . "';
		    var inputName = '" . $value['ezf_field_name'] . "';
		    eventRadio(inputName, dataCond);
		    setRadio(inputName, dataCond);
		");
	}
    }
}

if($readonly==0){
?>
    <div class="modal-footer">
	<?php
	if(isset($modelform['unique_record']) && $modelform['unique_record']==1 && isset($model_gen->id) && !empty($model_gen->id) ){
	    $sql = "SELECT rstat, xsourcex FROM {$modelform['ezf_table']} WHERE id=:id ";
	
	    $dataGen = Yii::$app->db->createCommand($sql, [':id'=>$model_gen->id])->queryOne();
	    $xsourcex = Yii::$app->user->identity->userProfile->sitecode;
	    
	    if($xsourcex != $dataGen['xsourcex']){//$dataGen['rstat']>1 || 
		
	    } else {
		if(isset($modelform['query_tools']) && $modelform['query_tools']>1){
		    echo Html::submitButton('Save Draft', ['class' => 'btn btn-info', 'id' => 'save-draft', 'name' => 'submit', 'value'=>'1']);
		    echo Html::submitButton('Submit', ['class' => 'btn btn-primary', 'name' => 'submit', 'value'=>'2']);
		} else {
		    echo Html::submitButton('Submit', ['class' => 'btn btn-primary', 'name' => 'submit', 'value'=>'1']);
		}
		echo Html::submitButton('Delete', ['class' => 'btn btn-danger', 'name' => 'submit', 'value'=>'3']);
	    }
	} else {
	    if(isset($modelform['query_tools']) && $modelform['query_tools']>1){

		echo Html::submitButton('Save Draft', ['class' => 'btn btn-info', 'id' => 'save-draft', 'name' => 'submit', 'value'=>'1']);
		echo Html::submitButton('Submit', ['class' => 'btn btn-primary', 'name' => 'submit', 'value'=>'2']);
	    } else {
		echo Html::submitButton('Submit', ['class' => 'btn btn-primary', 'name' => 'submit', 'value'=>'1']);
	    }
	    echo Html::submitButton('Delete', ['class' => 'btn btn-danger', 'name' => 'submit', 'value'=>'3']);
	}   
	?>
	
	<?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>
	    <?php } backend\modules\ezforms\components\EzActiveForm::end();?>

<?php  //window.print();
$js = $modelform->js;
$this->registerJs("
  $js
      
      $('input[type=\"radio\"]').dblclick(function(){
	this.checked = false;
	$(this).trigger('change');
    });

$('form#$formname').on('beforeSubmit', function(e) {
    var \$form = $(this);
    var formData = new FormData($(this)[0]);
    $.ajax({
          url: \$form.attr('action'),
          type: 'POST',
          data: formData,
	  dataType: 'JSON',
	  enctype: 'multipart/form-data',
	  processData: false,  // tell jQuery not to process the data
	  contentType: false,   // tell jQuery not to set contentType
          success: function (result) {
	    if(result.status == 'success') {
                ". SDNoty::show('result.message', 'result.status') ."
                 if(result.data.rstat==3){
                    $(document).find('#modal-print').modal('hide'); 
                    $.pjax.reload({container:'#inv-person-grid-pjax'});
                    return false;
                 }

                ".($end==1?"$(document).find('#modal-print').modal('hide'); $.pjax.reload({container:'#inv-person-grid-pjax'});return false;":'')."

                js_target = result.target;
                js_target_decode = result.id;
                js_id = result.id;    

                menu_main();
                js_end=1;
                ezform_mian(js_dataid, js_id);

            } else {
                ". SDNoty::show('result.message', 'result.status') ."
            } 
          },
          error: function () {
            ". SDNoty::show("'" . SDHtml::getMsgError() . "Server Error'", '"error"') ."
	    console.log('server error');
          }
      });
    return false;
    
});

");?>