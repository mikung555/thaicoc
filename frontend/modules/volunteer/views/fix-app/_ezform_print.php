<?php

use Yii;
use yii\helpers\Html;

/**
 * _ezform_popup file UTF-8
 * @author SDII <iencoded@gmail.com>
 * @copyright Copyright &copy; 2015 AppXQ
 * @license http://www.appxq.com/license/
 * @version 1.0.0 Date: 17 พ.ย. 2559 12:32:13
 * @link http://www.appxq.com/
 */

?>

<div class="fields-form">
<?php

$form = backend\modules\ezforms\components\EzActiveForm::begin([
	    'action' => '#',
	    'options' => ['enctype' => 'multipart/form-data']
	]);
?>
<div class="modal-header">
    <h4 class="modal-title" id="itemModalLabel"><?=$modelform->ezf_name?></h4>
</div>
    <div class="modal-body" >
    <h3 class="panel-title"><?='<a class="" href="printform?dataid='.$model_gen->id.'&amp;id='.$modelform->ezf_id.'&amp;print=1" target="_blank"><span class="fa fa-print fa-2x"></span></a>';?></h3>
<?php
echo '<div class="row">';

foreach ($modelfield as $value) {
    $target = '';
    
    echo \backend\modules\ezforms\components\EzformFunc::getTypeEprintform($model_gen, $value, $form);
    
}
echo '</div>';

foreach ($modelfield as $value) {
    $inputId = Html::getInputId($model_gen, $value['ezf_field_name']);
    $inputValue = Html::getAttributeValue($model_gen, $value['ezf_field_name']);

    $dataCond = backend\modules\ezforms\components\EzformQuery::getCondition($value['ezf_id'], $value['ezf_field_name']);

    if ($dataCond) {
	//Edit Html
	$fieldId = Html::getInputId($model_gen, $value['ezf_field_name']);
	if ($value['ezf_field_type'] == 4) {
	    $fieldId = $value['ezf_field_name'];
	}
	$enable = TRUE;
	foreach ($dataCond as $index => $cvalue) {
	    if ($inputValue == $cvalue['ezf_field_value'] || $inputValue == '') {
		$dataCond[$index]['cond_jump'] = json_decode($cvalue['cond_jump']);
		$dataCond[$index]['cond_require'] = json_decode($cvalue['cond_require']);

		if ($value['ezf_field_type'] == '4' || $value['ezf_field_type'] == '6') {
		    if ($enable) {
			$enable = false;
			$jumpArr = json_decode($cvalue['cond_jump']);
			if (is_array($jumpArr)) {
			    foreach ($jumpArr as $j => $jvalue) {
				$this->registerJs("
										var fieldIdj = '" . $jvalue . "';
										var inputIdj = '" . $fieldId . "';
										var valueIdj = '" . $inputValue . "';
										var fixValuej = '" . $cvalue['ezf_field_value'] . "';
										var fTypej = '" . $value['ezf_field_type'] . "';
										domHtml(fieldIdj, inputIdj, valueIdj, fixValuej, fTypej, 'none');
									");
			    }
			}

			$requireArr = json_decode($cvalue['cond_require']);
			if (is_array($requireArr)) {
			    foreach ($requireArr as $r => $rvalue) {
				$this->registerJs("
										var fieldIdr = '" . $rvalue . "';
										var inputIdr = '" . $fieldId . "';
										var valueIdr = '" . $inputValue . "';
										var fixValuer = '" . $cvalue['ezf_field_value'] . "';
										var fTyper = '" . $value['ezf_field_type'] . "';
										domHtml(fieldIdr, inputIdr, valueIdr, fixValuer, fTyper, 'block');
									");
			    }
			}
		    }
		} else {
		    $jumpArr = json_decode($cvalue['cond_jump']);
		    if (is_array($jumpArr)) {
			foreach ($jumpArr as $j => $jvalue) {
			    $this->registerJs("
									    var fieldIdj = '" . $jvalue . "';
									    var inputIdj = '" . $fieldId . "';
									    var valueIdj = '" . $inputValue . "';
									    var fixValuej = '" . $cvalue['ezf_field_value'] . "';
									    var fTypej = '" . $value['ezf_field_type'] . "';
									    domHtml(fieldIdj, inputIdj, valueIdj, fixValuej, fTypej, 'block');
								    ");
			}
		    }

		    $requireArr = json_decode($cvalue['cond_require']);
		    if (is_array($requireArr)) {
			foreach ($requireArr as $r => $rvalue) {
			    $this->registerJs("
									    var fieldIdr = '" . $rvalue . "';
									    var inputIdr = '" . $fieldId . "';
									    var valueIdr = '" . $inputValue . "';
									    var fixValuer = '" . $cvalue['ezf_field_value'] . "';
									    var fTyper = '" . $value['ezf_field_type'] . "';
									    domHtml(fieldIdr, inputIdr, valueIdr, fixValuer, fTyper, 'none');
								    ");
			}
		    }
		}
	    }
	}

	//Add Event
	if ($value['ezf_field_type'] == 20 || $value['ezf_field_type'] == 0 || $value['ezf_field_type'] == 16) {
	    $this->registerJs("
		    var dataCond = '" . yii\helpers\Json::encode($dataCond) . "';
		    var inputId = '" . $inputId . "';
		    eventCheckBox(inputId, dataCond);
		    setCheckBox(inputId, dataCond);
		");
	} else if ($value['ezf_field_type'] == 6) {
	    $this->registerJs("
		    var dataCond = '" . yii\helpers\Json::encode($dataCond) . "';
		    var inputId = '" . $inputId . "';
		    eventSelect(inputId, dataCond);
		    setSelect(inputId, dataCond);
		");
	} else if ($value['ezf_field_type'] == 4) {
	    $this->registerJs("
		    var dataCond = '" . yii\helpers\Json::encode($dataCond) . "';
		    var inputName = '" . $value['ezf_field_name'] . "';
		    eventRadio(inputName, dataCond);
		    setRadio(inputName, dataCond);
		");
	}
    }
}

backend\assets\EzfGenAsset::register($this);
?>
</div>
<?php backend\modules\ezforms\components\EzActiveForm::end(); ?>

</div>

<?php  //window.print();
$js = $modelform->js;
$this->registerJs("
    
	$js


");?>

    
