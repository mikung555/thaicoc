<?php

namespace frontend\modules\ezforms;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'frontend\modules\ezforms\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
