<?php
namespace frontend\modules\ezforms\classets;
use Yii;
use yii\web\Response;
use yii\db\Query;
use yii\db\Exception;
use yii\base\ErrorException;
use frontend\modules\api\v1\classes\MainQuery;
class EzformDesktop {
    public static function GetEZFormById($formId)
    {
        try{
            $out = [];
            $query = (new Query())
                ->select(['ezf_name','ezf_id'])
                ->from('ezform')
                ->where(['ezf_id' => $formId]);
            $out["ezform"] = $query->all()[0];
            $out["ezfields"] = MainQuery::GetEzFields($formId);

            $query_choices = (new Query())
                ->select(['ezform_fields.ezf_field_id','ezform_choice.ezf_choicelabel','ezform_choice.ezf_choice_id',
                    'ezform_choice.ezf_choicevalue','ezform_fields.ezf_field_label','ezform_fields.ezf_field_order'
                    ])
                ->from('ezform_fields')
                ->where(['ezform_fields.ezf_id' => $formId])
                ->innerJoin('ezform_choice' , 'ezform_fields.ezf_field_id = ezform_choice.ezf_field_id');

            $out["ezchoices"] = $query_choices->all();
            $out["checksum"] = md5 (json_encode($out));
            return $out;

        }catch (Exception $e){
            return $e;
        }catch (ErrorException $e){
            return $e;
        }
    }
    
}
