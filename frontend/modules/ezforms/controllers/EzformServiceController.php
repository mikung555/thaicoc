<?php
namespace frontend\modules\ezforms\controllers;
use Yii; 
use yii\web\Response;
use frontend\modules\ezforms\classets\EzformDesktop;
use frontend\modules\ezforms\classets\HeaderAccess;
class EzformServiceController extends \yii\web\Controller{
    public function behaviors()
    {
        return [

            'contentNegotiator' => [
                'class' => \yii\filters\ContentNegotiator::className(),
                'only' => ['get-ezform','get-ezf-table'],
                'formatParam' => 'format',
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                    'application/xml' => Response::FORMAT_XML,
                ],
            ],
        ];
    }

    public function actionGetEzform(){
        header("Access-Control-Allow-Origin: *", false);
        header("Access-Control-Allow-Headers: Origin, Content-Type,x-token");
        $ezf_id = \Yii::$app->request->get("ezf_id","");
        if(HeaderAccess::GetToken(\Yii::$app->request->headers->get('x-token')) === TRUE){
            $model = EzformDesktop::GetEZFormById($ezf_id);
            return $model;
        }
        
    }//GetEzform
    public function actionCreateForm()
    {
        header("Access-Control-Allow-Origin: *", false);
        header("Access-Control-Allow-Methods: POST");
        $ezf_id = Yii::$app->request->post('ezf_id','');   
        $target = Yii::$app->request->post('target','');   
        $submit = filter_var(Yii::$app->request->post('submit'), FILTER_VALIDATE_BOOLEAN);
        $sitecode = Yii::$app->request->post('sitecode','');
        $id = Yii::$app->request->post('data_id','');        
        $rawdata = Yii::$app->request->post('data','');
        $room_name = Yii::$app->request->post('room_name','');
        $doctor_code = Yii::$app->request->post('doctor_code','');
        $data = json_decode($rawdata);
        $create_res = \frontend\modules\api\v1\classes\EzForm::CreateNewRecord($this->user_id, $ezf_id, $sitecode, $target,  $submit,$id, $data );
        $data = (array)$data;
        \frontend\modules\api\v1\classes\WorklistQuery::AddLog( $data[ "ptid" ],$this->user_id,$room_name,$doctor_code);

        $this->createResponseByVerifyData($create_res);
    }
    public function actionGetEzfTable(){
        header("Access-Control-Allow-Origin: *", false);
        header("Access-Control-Allow-Headers: Origin, Content-Type,x-token");
        if(HeaderAccess::GetToken(\Yii::$app->request->headers->get('x-token')) === TRUE){
            $tbdata=\Yii::$app->request->get("tbdata","tbdata_1504586024078967800");
            $sql="SHOW COLUMNS FROM $tbdata";
            $query = \Yii::$app->db->createCommand($sql)->queryAll();
            return $query;  
        }
    }
    

//    public function actionToken(){
//        header("Access-Control-Allow-Origin: *", false);
//        header('Access-Control-Allow-Methods: POST');
//        header("Access-Control-Allow-Headers: Origin, Content-Type,x-token");
//        echo $_POST["id"];
//        /*access token*/
////        $headers = \Yii::$app->request->headers;
////        //$id = \Yii::$app->request->post("id");
////        if($headers->get("x-token") === "1234"){
////           return ["name"=>"chanpan","lname"=>"nuttaphon"];
////        }
//    }
}
