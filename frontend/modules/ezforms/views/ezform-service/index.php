<?php 
    //composer require richardfan1126/yii2-js-register "*"
    use richardfan\widget\JSRegister;

?>
<?php JSRegister::begin([
    'key' => 'bootstrap-modal',
    'position' => \yii\web\View::POS_READY
]); ?>
<script>
    $.ajax({
        url:'<?= yii\helpers\Url::to(['/ezforms/ezform-service/token'])?>',
        type:'POST',
        data:{id:12345},
        success:function(data){
            console.log(data);
        }
    })
</script>
<?php JSRegister::end(); ?>