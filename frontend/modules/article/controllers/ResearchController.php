<?php

namespace app\modules\article\controllers;

use Yii;
use backend\modules\article\models\Research;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\data\ActiveDataProvider;

/**
 * ResearchController implements the CRUD actions for Research model.
 */
class ResearchController extends Controller {

	public function behaviors() {
		return [
			
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					'delete' => ['post'],
				],
			],
		];
	}

	public function beforeAction($action) {
		if (parent::beforeAction($action)) {
			if (in_array($action->id, array('create', 'update'))) {
				
			}
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Lists all Research models.
	 * @return mixed
	 */
	public function actionIndex() {
	    $cca = isset($_GET['cca'])?$_GET['cca']:0;
	    
	    $where = '`show` = 1';
	    $arrWhere[':created_by'] = Yii::$app->user->id;
	    
	    $domain=Yii::$app->keyStorage->get('frontend.domain');
	    $siteprefix= trim(str_replace($domain, "", $_SERVER['HTTP_HOST']));
	    $siteprefix= trim(str_replace(".", "",$siteprefix));
	    
	    
	    if($siteprefix!==''){
		$sqlSiteURL = "SELECT code,`url`,`name` FROM site_url WHERE code=:code OR `url`=:url ";
		$dataSiteURL = Yii::$app->db->createCommand($sqlSiteURL, [':code'=> $siteprefix, ':url'=>$siteprefix])->queryOne();
		
		if($dataSiteURL){
		    
		    if($dataSiteURL['code']!=0){
			$where .= ' AND user_profile.sitecode = "'.$dataSiteURL['code'].'"';
			//$arrWhere[':sitecode'] = $dataSiteURL['code'];
		    }
		}
	    }
	    
	    $where .= ' AND author_public = 1';
//	    if (Yii::$app->user->isGuest) {
//		$where .= ' AND ((created_by = :created_by OR author_public = 1 AND author_user = 0))';
//	    } else {
//		$where .= ' AND (author_public = 1 OR author_user = 1 OR (FIND_IN_SET(:auth, co_auther_email)))';
//		$arrWhere[':auth'] = Yii::$app->user->identity->email;
//	    }
	    
	    if($cca==1){
		$where .= " AND cca_status = $cca";
	    }
		$dataProvider = new ActiveDataProvider([
            'query' => Research::find()->innerJoin('user_profile', 'research.author_id = user_profile.user_id')
			->where($where, $arrWhere)
			->orderBy('expected_completion DESC'),
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);
	
		return $this->render('index', [
					'dataProvider' => $dataProvider,
					'cca' => $cca,
		]);
	}
	
	/**
	 * Displays a single Research model.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionView($id) {
		return $this->render('view', [
					'model' => $this->findModel($id),
		]);
	}
	
	public function actionMabstract($id) {
		return $this->renderAjax('_info', [
		    'title'=>'Mock Abstract',
		    'data' => $this->findModel($id)->mabstract,
		]);
	}
	
	public function actionMmanuscript($id) {
		return $this->renderAjax('_info', [
		    'title'=>'Mock Manuscript',
		    'data' => $this->findModel($id)->mmanuscript,
		]);
	}
	
	/**
	 * Finds the Research model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $id
	 * @return Research the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id) {
		if (($model = Research::findOne($id)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}

}
