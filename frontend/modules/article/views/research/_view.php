<?php

use yii\helpers\Html;
use backend\modules\article\components\ArticleQuery;

?>

<?php

if(empty(Yii::$app->params['research_yyyy'])){
	Yii::$app->params['research_yyyy'] = '';
}

//<h4 class="page-header">งานวิจัยของฉัน</h4>

if ($model->expected_completion != Yii::$app->params['research_yyyy']) {

	if ($model->expected_completion == '') {
		echo '<h4 class="page-header">ไม่ได้กำหนดปีที่คาดว่าจะเสร็จ</h4>';
		Yii::$app->params['research_yyyy'] = '';
	}else {
		echo '<h4 class="page-header">' . $model->expected_completion . '</h4>';
		Yii::$app->params['research_yyyy'] = $model->expected_completion;
	}
}
?>


<div class="media">
	<div class="media-left">
        <a href="#">
	    <?php
	    $icon = 'data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIHZpZXdCb3g9IjAgMCA2NCA2NCIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+PCEtLQpTb3VyY2UgVVJMOiBob2xkZXIuanMvNjR4NjQKQ3JlYXRlZCB3aXRoIEhvbGRlci5qcyAyLjYuMC4KTGVhcm4gbW9yZSBhdCBodHRwOi8vaG9sZGVyanMuY29tCihjKSAyMDEyLTIwMTUgSXZhbiBNYWxvcGluc2t5IC0gaHR0cDovL2ltc2t5LmNvCi0tPjxkZWZzPjxzdHlsZSB0eXBlPSJ0ZXh0L2NzcyI+PCFbQ0RBVEFbI2hvbGRlcl8xNTAyMjljNTY3YSB0ZXh0IHsgZmlsbDojQUFBQUFBO2ZvbnQtd2VpZ2h0OmJvbGQ7Zm9udC1mYW1pbHk6QXJpYWwsIEhlbHZldGljYSwgT3BlbiBTYW5zLCBzYW5zLXNlcmlmLCBtb25vc3BhY2U7Zm9udC1zaXplOjEwcHQgfSBdXT48L3N0eWxlPjwvZGVmcz48ZyBpZD0iaG9sZGVyXzE1MDIyOWM1NjdhIj48cmVjdCB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIGZpbGw9IiNFRUVFRUUiLz48Zz48dGV4dCB4PSIxMy40Njg3NSIgeT0iMzYuNSI+NjR4NjQ8L3RleHQ+PC9nPjwvZz48L3N2Zz4=';
	    if($model->icon!=''){
		$icon = Yii::getAlias('@storageUrl') . '/source/images/'.$model->icon;
	    }
	    ?>
			<img class="media-object" alt="64x64" src="<?=$icon?>" data-holder-rendered="true" style="width: 64px; height: 64px;">
        </a>
	</div>
	<div class="media-body">
        <h4 class="media-heading"><?= Html::a(Html::encode($model->res_topic), ['view', 'id' => $model->rid]); ?> <?=($model->mabstract_show)?'<a style="cursor: pointer" class="mabstract_show" data-id="'.$model->rid.'" ><i class="fa fa-file-text-o"></i></a>':''?> <?=($model->mmanuscript_show)?'<a style="cursor: pointer" class="mmanuscript_show" data-id="'.$model->rid.'" ><i class="fa fa-book"></i></a>':''?></h4>
	<small class="pull-right"><div class="alert alert-info" style="padding: 0 5px;"><?=  \backend\modules\article\components\ArticleFunc::itemAlias('status', $model->status)?></div></small>
	<strong>ผู้แต่ง : </strong> <?=Html::encode($model->author)?> (<a href="mailto:#"><?=Html::encode($model->author_email)?></a>)
			<br>
			<?php
			$arrA = explode(',', $model->co_auther);
			$arrE = explode(',', $model->co_auther_email);
			$arr = [];
			foreach ($arrA as $key => $value) {
				$arr[] = $arrA[$key] . ' (<a href="mailto:#">' . $arrE[$key] . '</a>)';
			}
			
			$arrP = ArticleQuery::getTreeById($model->projid);
			
			$enable = FALSE;
			if($model->contact_auth==1){
			    $arr = explode(',', $model->co_auther_id);
			    if(in_array(Yii::$app->user->id, $arr)){
				$enable = true;
			    }
			}
			if($model->contact_user==1){
			    if(!Yii::$app->user->isGuest){
				$enable = true;
			    }
			}
			if($model->contact_public==1){
			    $enable = true;
			}
			if($model->created_by==Yii::$app->user->id){
			    $enable = true;
			}
			?>
			
			<strong>ผู้แต่งร่วม : </strong> <?= implode(', ', $arr) ?>
			<br>
			<strong>ภายใต้โครงการ : </strong> <?= implode(', ', $arrP) ?>
			<br>
			<?php if($enable):?>
			<address>
				<strong>ติดต่อ <?=Html::encode($model->contact_name)?></strong><br>
				<?=Html::encode($model->contact_address)?><br>
				<abbr title="Phone">Tel:</abbr> <?=Html::encode($model->contact_tel)?> &nbsp; 
				<abbr title="Fax">Fax:</abbr> <?=Html::encode($model->contact_fax)?><br>
				<a href="mailto:#"><?=Html::encode($model->contact_email)?></a>
			</address>
			<?php endif; ?>
			<?php
	if($model->manuscript_file!=''){
	    $url = Yii::getAlias('@storageUrl') . '/source/'.$model->manuscript_file;
	    echo Html::a('Download manuscript file', $url, ['class'=>'label label-success', 'target'=>'_blank']);
	}
	?>
	
	<?php
	if($model->abstract_file!=''){
	    $url = Yii::getAlias('@storageUrl') . '/source/'.$model->abstract_file;
	    echo Html::a('Download abstract file', $url, ['class'=>'label label-warning', 'target'=>'_blank']);
	}
	?>
	<br>
	</div>
</div>