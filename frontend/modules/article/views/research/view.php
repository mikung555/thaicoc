<?php

use yii\helpers\Html;
use backend\modules\article\components\ArticleQuery;

/* @var $this yii\web\View */
/* @var $model app\modules\article\models\Research */

$this->title = 'Research#' . $model->rid;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Researches'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="research-view">


	<h3 class="page-header"><?= Html::encode($model->res_topic); ?></h3>
	<strong>ผู้แต่ง : </strong> <?= Html::encode($model->author) ?> (<a href="mailto:#"><?= Html::encode($model->author_email) ?></a>)
	<br>
	<?php
	$arrA = explode(',', $model->co_auther);
	$arrE = explode(',', $model->co_auther_email);
	$arr = [];
	foreach ($arrA as $key => $value) {
		$arr[] = $arrA[$key] . ' (<a href="mailto:#">' . $arrE[$key] . '</a>)';
	}

	$arrP = ArticleQuery::getTreeById($model->projid);
	?>

	<strong>ผู้แต่งร่วม : </strong> <?= implode(', ', $arr) ?>
	<br>
	<strong>ภายใต้โครงการ : </strong> <?= implode(', ', $arrP) ?>
	<br>
	<address>
		<strong>ติดต่อ <?= Html::encode($model->contact_name) ?></strong><br>
		<?= Html::encode($model->contact_address) ?><br>
		<abbr title="Phone">Tel:</abbr> <?= Html::encode($model->contact_tel) ?> &nbsp; 
		<abbr title="Fax">Fax:</abbr> <?= Html::encode($model->contact_fax) ?><br>
		<a href="mailto:#"><?= Html::encode($model->contact_email) ?></a>
	</address>
	<br>
	<h4 class="page-header">Mock Abstract</h4>
	<?php
	if($model->mabstract_show==1){
		echo $model->mabstract;
	} else {
		echo '<em>ไม่เปิดเผยข้อมูล</em>';
	}
	?>
	<br>
	<h4 class="page-header">Mock Manuscript</h4>
	<?php
	if($model->mmanuscript_show==1){
		echo $model->mmanuscript;
	} else {
		echo '<em>ไม่เปิดเผยข้อมูล</em>';
	}
	?>
</div>
