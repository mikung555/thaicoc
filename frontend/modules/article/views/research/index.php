<?php

use yii\helpers\Html;
use yii\widgets\ListView;
use common\lib\sdii\widgets\SDModalForm;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $searchModel app\modules\article\models\ResearchSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Research ';
$this->params['breadcrumbs'][] = $this->title;

?>


<div class="research-index">

    <div class="sdbox-header">
	<h3><?=  Html::encode($this->title) ?>
	<div class="btn-group" role="group" aria-label="...">
	    <a href="<?=Url::to(['/article/research/index', 'cca'=>0])?>" class="btn btn-<?=$cca==0?'success':'default'?>">All</a>
	    <a href="<?=Url::to(['/article/research/index', 'cca'=>1])?>" class="btn btn-<?=$cca==1?'success':'default'?>">CCA-related</a>
	</div>
	</h3>
	<small><span class="label label-info">*หมายเหตุ</span> <i class="fa fa-file-text-o"></i> = Mock Abstract, <i class="fa fa-book"></i> = Mock Manuscript</small>
    </div>
    
	<?= ListView::widget([
			'dataProvider' => $dataProvider,
			'itemOptions' => ['class' => 'article-item'],
			'options' => [
				'tag' => 'div',
				'class' => 'list-wrapper',
				'id' => 'list-wrapper',
			],
			//'layout' => "{pager}\n{items}\n{summary}",
			'itemView' => '_view',
			'pager' => [
				'firstPageLabel' => 'first',
				'lastPageLabel' => 'last',
				'nextPageLabel' => 'next',
				'prevPageLabel' => 'previous',
				'maxButtonCount' => 3,
			],
	]) ?>


</div>

<?=  SDModalForm::widget([
    'id' => 'modal-research',
    'size'=>'modal-lg',
]);
?>

<?php  $this->registerJs("
var url = '';

$('body').on('click', '.mabstract_show', function() {
    console.log($(this).attr('data-id'));
    modalResearch('".Url::to(['research/mabstract', 'id'=>''])."'+$(this).attr('data-id'));
});

$('body').on('click', '.mmanuscript_show', function() {
    console.log($(this).attr('data-id'));
    modalResearch('".Url::to(['research/mmanuscript', 'id'=>''])."'+$(this).attr('data-id'));
});

function modalResearch(url) {
    $('#modal-research .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
    $('#modal-research').modal('show')
    .find('.modal-content')
    .load(url);
}

");?>