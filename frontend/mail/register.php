<?php
use yii\helpers\Html;

$key = Yii::$app->keyStorage->get('app_url');
$suburl = isset($key)?$key.'.':'';
?>

ยินดีต้อนรับเข้าสู่ระบบ <?php echo $suburl.Yii::$app->keyStorage->get('frontend.domain');?><br>
<br>
Username : <?php echo $username;?><br>
Password : <?php echo $password;?><br>
ชื่อ-สกุล : <?php echo Html::encode($user->firstname.' '.$user->lastname) ?><br>

<br>
<br>

<?php echo Yii::$app->keyStorage->get('email_signature');?><br>Download
