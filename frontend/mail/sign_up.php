<?php
use yii\helpers\Html;

$key = Yii::$app->keyStorage->get('app_url');
$suburl = isset($key)?$key.'.':'';
?>

คุณ<?php echo Html::encode($user->firstname.' '.$user->lastname) ?> ได้สมัครสมาชิกเข้าสู่ระบบ <?php echo $suburl.Yii::$app->keyStorage->get('frontend.domain');?><br>
<br>
Username: <?php echo $username;?><br>
<?php
$hos = backend\modules\ezforms\components\EzformQuery::getHospital($user->sitecode);
echo "หน่วยงาน: {$hos['name']} ต.{$hos['tambon']} อ.{$hos['amphur']} จ.{$hos['province']}<br>";
echo \backend\modules\article\components\ArticleFunc::owner($user).'<br>';
?>
<br><br>

<?php echo Yii::$app->keyStorage->get('email_signature');?><br>