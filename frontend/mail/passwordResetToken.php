<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */

$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['/user/sign-in/reset-password', 'token' => $user->password_reset_token]);
?>

เรียน คุณ<?php echo Html::encode($user->userProfile->firstname.' '.$user->userProfile->lastname) ?><br>

<p>
&nbsp; &nbsp; <?php echo Yii::$app->keyStorage->get('password_reset_text');?>
</p>
<br>
&nbsp; &nbsp; ท่านได้แจ้งลืมรหัสผ่าน <br>
&nbsp; &nbsp; &nbsp; สามารถแก้ไข เป็นรหัสผ่านใหม่ได้ตาม Link ด้านล่าง <br>
<br>
&nbsp; &nbsp; กรุณาคลิ๊กตาม Link: <?php echo Html::a(Html::encode($resetLink), $resetLink) ?><br>
<br>
&nbsp; &nbsp; เวลาที่แจ้ง เมื่อ <?php echo date('Y-m-d H:i:s');?> <br>
<br>
ขอแสดงความนับถือ