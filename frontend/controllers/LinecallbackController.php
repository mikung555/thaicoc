<?php

namespace frontend\controllers;
use Yii; 
use yii\web\Response;
use common\models\User;
use common\models\UserProfile;
use frontend\models\UserLine;

class LinecallbackController extends \yii\web\Controller
{
    public function beforeAction($action) { 
        if ($action->id == 'index') { 
            $this->enableCsrfValidation = false; //ปิดการใช้งาน csrf 
        } 
        if ($action->id == 'test') { 
            $this->enableCsrfValidation = false; //ปิดการใช้งาน csrf 
        }         
        return parent::beforeAction($action);
    }
    public function actionTest() {

            $username = "user";
            $password = "user";
            $user = User::findByUsername($username);

            if($user != null){
                if ($user->validatePassword($password)) {
                    $profile = UserProfile::findOne(['user_id'=>$user->id]);
                    $fullname = $profile->firstname . " ".$profile->lastname;
                    echo $result_text = "Successful Login!!!! ยินดีต้อนรับคุณ {$fullname} สู่ระบบ CASCAP (Isan cohort)";
                    $response_format_text = ['contentType'=>1,"toType"=>1,"text"=>$result_text];                    
                }else{
                    echo $result_text = "Login ไม่ถูกต้อง กรุณา Login ใหม่ โดยพิมพ์ : login รหัสผู้ใช้  รหัสผ่าน";
                    $response_format_text = ['contentType'=>1,"toType"=>1,"text"=>$result_text];                                        
                }
            }  
            return;
            
        $to="u60895c03ba00b211e48fa2d551aff8c4";
        $response_format_text = ['contentType'=>1,"toType"=>1,"text"=>"สวัสดีนะจ๊ะ https://cloud.cascap.in.th"];
        $post_data = ["to"=>[$to],"toChannel"=>"1383378250","eventType"=>"138311608800106203","content"=>$response_format_text];  //ส่งข้อมูลไป 
        $ch = curl_init("https://trialbot-api.line.me/v1/events");
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post_data));
        curl_setopt($ch, CURLOPT_HTTPHEADER, array( 
            'Content-Type: application/json;charser=UTF-8', 
            'X-Line-ChannelID: 1475458215',
            'X-Line-ChannelSecret: 686ba69f05cf7e38657d00e0b9161711',
            'X-Line-Trusted-User-With-ACL: u7060bfe93bd5ecbf0e2d980ce5312908'
        ));
        $result = curl_exec($ch);
        curl_close($ch);        
    }
    public function sendmsg($to,$message) {
        $response_format_text = ['contentType'=>1,"toType"=>1,"text"=>$message];
        $post_data = ["to"=>[$to],"toChannel"=>"1383378250","eventType"=>"138311608800106203","content"=>$response_format_text];  //ส่งข้อมูลไป 
        $ch = curl_init("https://trialbot-api.line.me/v1/events");
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post_data));
        curl_setopt($ch, CURLOPT_HTTPHEADER, array( 
            'Content-Type: application/json;charser=UTF-8', 
            'X-Line-ChannelID: 1475458215',
            'X-Line-ChannelSecret: 686ba69f05cf7e38657d00e0b9161711',
            'X-Line-Trusted-User-With-ACL: u7060bfe93bd5ecbf0e2d980ce5312908'
        ));
        $result = curl_exec($ch);
        curl_close($ch);        
    }
    public function actionIndex() { 
        $json_string = file_get_contents('php://input');
        $jsonObj = json_decode($json_string); 
        $to = $jsonObj->{"result"}[0]->{"content"}->{"from"};
        $text = $jsonObj->{"result"}[0]->{"content"}->{"text"}; 
        $optype = $jsonObj->{"result"}[0]->{"content"}->{"opType"}; 
        $text_ex = explode(' ', preg_replace('!\s+!', ' ', $text));
        if ($optype == "4") {
            $to = $jsonObj->{"result"}[0]->{"content"}->{"params"}[0];
            $message = "ยินดีตอนรับสู่ CASCAP";
            $this->sendmsg($to,$message);
        }else if($text_ex[0] == "อยากรู้"){ //ถ้าข้อความคือ "อยากรู้" ให้ทำการดึงข้อมูลจาก Wikipedia หาจากไทยก่อน //https://en.wikipedia.org/w/api.php?format=json&action=query&prop=extracts&exintro=&explaintext=&titles=PHP 
            $param = substr($text,strpos($text," "));
            $ch1 = curl_init();
            curl_setopt($ch1, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch1, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch1, CURLOPT_URL, 'https://en.wikipedia.org/w/api.php?format=json&action=query&prop=extracts&exintro=&explaintext=&titles='.$param);
            $result1 = curl_exec($ch1);
            curl_close($ch1);
            $obj = json_decode($result1, true);
            foreach($obj['query']['pages'] as $key => $val){ 
                $result_text = $val['extract'];
            }

            if(empty($result_text)){//หาจาก en ไม่พบก็บอกว่า ไม่พบข้อมูล ตอบกลับไป 
                $result_text = 'ไม่พบข้อมูล';
            }
            $response_format_text = ['contentType'=>1,"toType"=>1,"text"=>$result_text];
        }else if($text_ex[0] == "login"){//ถ้าพิมพ์มาว่า อากาศ ก็ให้ไปดึง API จาก wunderground มา //http://api.wunderground.com/api/yourkey/forecast/lang:TH/q/Thailand/%E0%B8%81%E0%B8%A3%E0%B8%B8%E0%B8%87%E0%B9%80%E0%B8%97%E0%B8%9E%E0%B8%A1%E0%B8%AB%E0%B8%B2%E0%B8%99%E0%B8%84%E0%B8%A3.json 
            $username = $text_ex[1];
            $password = $text_ex[2];

            $user = User::findByUsername($username);

            if($user != null){
                if ($user->validatePassword($password)) {
                    $profile = UserProfile::findOne(['user_id'=>$user->id]);
                    $fullname = $profile->firstname . " ".$profile->lastname;
                    $result_text = "Successful Login!!!! ยินดีต้อนรับคุณ {$fullname} สู่ระบบ CASCAP (Isan cohort) https://cloudbackend.cascap.in.th/";
                    $response_format_text = ['contentType'=>1,"toType"=>1,"text"=>$result_text];                    
                    if ( UserLine::findOne(['mid'=>$to]) == null) {
                        $userline = new UserLine();
                        $userline->mid = $to;
                        $userline->userid = $user->id;
                        $userline->dadd = date("Y-m-d H:i:s");
                        $userline->save();
                    }else{
                        $userline = UserLine::findOne(['mid'=>$to]);
                        $userline->userid = $user->id;
                        $userline->save();
                    }                    
                }else{
                    $result_text = "Login ไม่ถูกต้อง กรุณา Login ใหม่ โดยพิมพ์ : login รหัสผู้ใช้  รหัสผ่าน";
                    $response_format_text = ['contentType'=>1,"toType"=>1,"text"=>$result_text];                                        
                }
            }  
        }else if($text_ex[0] == "userinfo"){//ถ้าพิมพ์มาว่า อากาศ ก็ให้ไปดึง API จาก wunderground มา //http://api.wunderground.com/api/yourkey/forecast/lang:TH/q/Thailand/%E0%B8%81%E0%B8%A3%E0%B8%B8%E0%B8%87%E0%B9%80%E0%B8%97%E0%B8%9E%E0%B8%A1%E0%B8%AB%E0%B8%B2%E0%B8%99%E0%B8%84%E0%B8%A3.json 
            $userline = UserLine::findOne($to);
            
            if($userline != null){
                $user = User::findIdentity($userline->userid);
                if($user != null){
                    $profile = UserProfile::findOne(['user_id'=>$user->id]);
                    $fullname = $profile->firstname . " ".$profile->lastname;
                    $sitecode = $profile->sitecode;
                    $result_text = "ท่านเข้าใช้งานแล้วในชื่อ {$fullname} ($sitecode) เข้าสู่การทำงาน";
                    $response_format_text = ['contentType'=>1,"toType"=>1,"text"=>$result_text];                                   
                }else{
                    $result_text = "ท่านยังไม่ได้ Login กรุณา Login ใหม่ โดยพิมพ์ : login รหัสผู้ใช้  รหัสผ่าน";
                    $response_format_text = ['contentType'=>1,"toType"=>1,"text"=>$result_text];                                        
                }
            }else{
                $result_text = "ท่านยังไม่ได้ Login กรุณา Login ใหม่ โดยพิมพ์ : login รหัสผู้ใช้  รหัสผ่าน";
                $response_format_text = ['contentType'=>1,"toType"=>1,"text"=>$result_text];                                        
            }               
        }else if($text == 'บอกมา'){//คำอื่นๆ ที่ต้องการให้ Bot ตอบกลับเมื่อโพสคำนี้มา เช่นโพสว่า บอกมา ให้ตอบว่า ความลับนะ 
            $response_format_text = ['contentType'=>1,"toType"=>1,"text"=>"ความลับนะ"];
        }else{//นอกนั้นให้โพส สวัสดี 
            $response_format_text = ['contentType'=>1,"toType"=>1,"text"=>"สวัสดี ความพยายามอยู่ที่ไหน ความพยายามก็อยู่ที่นั่นแหละ อันนี้พี่นุไม่ได้กล่าวไว้"];
        } 
        // toChannel?eventType 
        $post_data = ["to"=>[$to],"toChannel"=>"1383378250","eventType"=>"138311608800106203","content"=>$response_format_text];  //ส่งข้อมูลไป 
        $ch = curl_init("https://trialbot-api.line.me/v1/events");
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post_data));
        curl_setopt($ch, CURLOPT_HTTPHEADER, array( 
            'Content-Type: application/json;charser=UTF-8', 
            'X-Line-ChannelID: 1475458215',
            'X-Line-ChannelSecret: 686ba69f05cf7e38657d00e0b9161711',
            'X-Line-Trusted-User-With-ACL: u7060bfe93bd5ecbf0e2d980ce5312908'
        ));
        $result = curl_exec($ch);
        curl_close($ch);
    }  
}
