<?php

namespace frontend\modules\ezforms\controllers;

use backend\modules\ezforms\models\EzformDataLog;
use backend\modules\ezforms\models\EzformSqlLog;
use backend\modules\ezforms\models\EzformReply;
use NumberFormatter;
use yii\data\ArrayDataProvider;
use yii\db\Exception;
use yii\db\Expression;
use \yii\web\UploadedFile;
use backend\models\EzformTarget;
use backend\modules\ezforms\models\EzformDynamic;
use trntv\filekit\widget\Upload;
use Yii;
use backend\modules\ezforms\models\Ezform;
use backend\modules\ezforms\models\EzformSearch;
use backend\modules\ezforms\models\EzformFields;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\Response;
use common\models\User;
use common\lib\codeerror\helpers\GenMillisecTime;
use backend\modules\ezforms\models\EzformChoice;
use backend\modules\component\models\EzformComponent;
use yii\helpers\ArrayHelper;
use backend\models\EzformCoDev;
use backend\models\EzformAssign;
use yii\helpers\Url;
use backend\modules\ezforms\components\EzformQuery;
use backend\modules\ezforms\components\EzformFunc;
use yii\widgets\ActiveForm;

/**
 * EzformController implements the CRUD actions for Ezform model.
 */
class EzformController extends Controller
{

    public $layout = '@backend/views/layouts/common';

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'view'],
                        'roles' => ['?', '@'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['create', 'update', 'delete', 'undo'],
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        //Yii::$app->response->format = Response::FORMAT_JSON;
        return [
            'image-upload' => [
                'class' => 'vova07\imperavi\actions\UploadAction',
                'url' => Yii::getAlias('@storageUrl') . '/form-upload', // Directory URL address, where files are stored.
                'path' => '@app/../storage/web/form-upload', // Or absolute path to directory where files are stored.
                //'type' => GetAction::TYPE_IMAGES,
            ],
        ];
        //use vova07/imperavi/actions/UploadAction
    }



    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            if (in_array($action->id, array('create', 'update'))) {

            }
            return true;
        } else {
            return false;
        }
    }

    /**
     * Updates an existing Ezform model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */


    /**
     * Finds the Ezform model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Ezform the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Ezform::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findFieldModel($id)
    {
        if (($model = EzformFields::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }


    public function actionSavedata2()
    {

        if (isset($_POST["id"])) {
            $model_fields = EzformQuery::getFieldsByEzf_id($_POST["ezf_id"]);
            $model_form = EzformFunc::setDynamicModelLimit($model_fields);
            $table_name = EzformQuery::getFormTableName($_POST["ezf_id"]);
            if(Yii::$app->request->post('txtSubmit')){
                //save submit
                $rstat = 2;
            }else{
                //save draft
                $rstat = 1;
            }

            if ($model_form->load(Yii::$app->request->post())) {
                $model_form->attributes = $_POST['SDDynamicModel'];

                $model = new \backend\modules\ezforms\models\EzformDynamic($table_name->ezf_table);
                $modelOld = $model->find()->where('id = :id', ['id' => $_POST['id']])->One();

                //if click final save (rstat not change)
                if(Yii::$app->request->post('finalSave')){
                    $rstat = $modelOld->rstat;
                    $xsourcex = $modelOld->xsourcex;
                }else{
                    $xsourcex = Yii::$app->user->identity->userProfile->sitecode;//hospitalcode
                }
                //VarDumper::dump($modelOld->attributes,10,true);
                //VarDumper::dump($model_form->attributes,10,true);
                $model->attributes = $model_form->attributes;
                $model->id = $_POST['id']; //ห้ามเอาออก ไม่งั้นจะ save ไม่ได้
                $model->rstat = $rstat;
                $model->xsourcex = $xsourcex;
                //$model->create_date = new Expression('NOW()');
                $model->user_update = Yii::$app->user->id;
                $model->update_date = new Expression('NOW()');
                //echo $rstat;

                if (Yii::$app->request->post('target') <> 'skip' AND Yii::$app->request->post('target') <> 'all') {
                    //จริงๆถ้าไม่มีการเปลี่ยนเป้าหมาย ให้ไปอัพเดจใน ezform_target ด้วย
                    $ezform = EzformQuery::checkIsTableComponent(Yii::$app->request->post('ezf_id'));
                    if ($ezform['special']) {
                        // something
                    } else {
                        $model->target = isset($_POST['target']) ? base64_decode($_POST['target']) : NULL;
                    }
                }

                foreach ($model_fields as $key => $value) {
                    if ($value['ezf_field_type'] == 7 || $value['ezf_field_type'] == 253) {
                        // set Data Sql Format
                        $data = $model[$value['ezf_field_name']];
                        if($data+0) {
                            $explodeDate = explode('/', $data);
                            $formateDate = ($explodeDate[2] - 543) . '-' . $explodeDate[1] . '-' . $explodeDate[0];
                            $model->{$value['ezf_field_name']} = $formateDate;
                        }else{
                            $model->{$value['ezf_field_name']} = new Expression('NULL');
                        }
                    }
                    else if ($value['ezf_field_type'] == 10) {
                        if (count($model->{$value['ezf_field_name']}) > 1)
                            $model->{$value['ezf_field_name']} = implode(',', $model->{$value['ezf_field_name']});
                    }
                    else if ($value['ezf_field_type'] == 14) {
                        if (isset($_FILES['SDDynamicModel']['name'][$value['ezf_field_name']]) && $_FILES['SDDynamicModel']['name'][$value['ezf_field_name']] !='' && is_array($_FILES['SDDynamicModel']['name'][$value['ezf_field_name']])) {
                            $fileItems = $_FILES['SDDynamicModel']['name'][$value['ezf_field_name']];
                            $fileType = $_FILES['SDDynamicModel']['type'][$value['ezf_field_name']];
                            $newFileItems = [];
                            $action = false;
                            foreach ($fileItems as $i => $fileItem) {
                                if($fileItem!=''){
                                    $action = true;
                                    $fileArr = explode('/', $fileType[$i]);
                                    if (isset($fileArr[1])) {
                                        $fileBg = $fileArr[1];

                                        //$newFileName = $fileName;
                                        $newFileName = $value['ezf_field_name'].'_'.($i+1).'_'.$model->id.'_'.date("Ymd_His").(microtime(true)*10000) . '.' . $fileBg;
                                        //chmod(Yii::$app->basePath . '/../backend/web/fileinput/', 0777);
                                        $fullPath = Yii::$app->basePath . '/../backend/web/fileinput/' . $newFileName;
                                        $fieldname = 'SDDynamicModel[' . $value['ezf_field_name'] . '][' . $i . ']';

                                        $file = UploadedFile::getInstanceByName($fieldname);
                                        $file->saveAs($fullPath);

					if($file){
					    $newFileItems[] = $newFileName;

					    //add file to db
					    $file_db = new \backend\modules\ezforms\models\FileUpload();
					    $file_db->tbid = $model->id;
					    $file_db->ezf_id = $value['ezf_id'];
					    $file_db->ezf_field_id = $value['ezf_field_id'];
					    $file_db->file_active = 0;
					    $file_db->file_name = $newFileName;
					    $file_db->file_name_old = $fileItem;
					    $file_db->target = ($model->ptid ? $model->ptid :  $model->target).'';
					    $file_db->save();
					}
                                    }
                                }
                            }
                            $res = Yii::$app->db->createCommand("SELECT `" . $value['ezf_field_name'] . "` as filename FROM `" . $table_name->ezf_table . "` WHERE id = :dataid", [':dataid' => $_POST['id']])->queryOne();
                            if($action){
                                $model->{$value['ezf_field_name']} = implode(',', $newFileItems);

//				if($res){
//				    $res_items = explode(',', $res['filename']);
//
//				    foreach ($res_items as $dataTmp) {
//					@unlink(Yii::$app->basePath . '/../backend/web/fileinput/' . $dataTmp);
//				    }
//				}
                            } else {
                                if($res){
                                    $model->{$value['ezf_field_name']} = $res['filename'];
                                }
                            }

                        } else {
                            $res = Yii::$app->db->createCommand("SELECT `" . $value['ezf_field_name'] . "` as filename FROM `" . $table_name->ezf_table . "` WHERE id = :dataid", [':dataid' => $_POST['id']])->queryOne();
                            $model->{$value['ezf_field_name']} = $res['filename'];
                        }
                    }
                    else if ($value['ezf_field_type'] == 24) {

                        if (stristr($model[$value['ezf_field_name']], 'tmp.png') == TRUE) {
                            //set data Drawing
                            $fileArr = explode(',', $model[$value['ezf_field_name']]);
                            $fileName = $fileArr[0];
                            $fileBg = $fileArr[1];
                            $newFileName = $fileName;
                            $newFileBg = $fileBg;
                            $nameEdit = false;
                            $bgEdit = false;
                            if (stristr($fileName, 'tmp.png') == TRUE) {
                                $nameEdit = true;
                                $newFileName = date("Ymd_His").(microtime(true)*10000) . '.png';
                                @copy(Yii::$app->basePath . '/../backend/web/drawing/' . $fileName, Yii::$app->basePath . '/../storage/web/drawing/data/' . $newFileName);
                                @unlink(Yii::$app->basePath . '/../backend/web/drawing/' . $fileName);
                            }
                            if (stristr($fileBg, 'tmp.png') == TRUE) {
                                $bgEdit = true;
                                $newFileBg = 'bg_' . date("Ymd_His").(microtime(true)*10000) . '.png';
                                @copy(Yii::$app->basePath . '/../storage/web/drawing/' . $fileBg, Yii::$app->basePath . '/../storage/web/drawing/bg/' . $newFileBg);
                                @unlink(Yii::$app->basePath . '/../storage/web/drawing/' . $fileBg);
                            }

                            $model[$value['ezf_field_name']] = $newFileName . ',' . $newFileBg;
                            $modelTmp = EzformQuery::getDynamicFormById($table_name->ezf_table, $_POST['id']);

                            if (isset($modelTmp['id'])) {
                                $fileArr = explode(',', $modelTmp[$value['ezf_field_name']]);
                                if (count($fileArr) > 1) {
                                    $fileName = $fileArr[0];
                                    $fileBg = $fileArr[1];
                                    if ($nameEdit) {
                                        @unlink(Yii::$app->basePath . '/../storage/web/drawing/data/' . $fileName);
                                    }
                                    if ($bgEdit) {
                                        @unlink(Yii::$app->basePath . '/../storage/web/drawing/bg/' . $fileBg);
                                    }
                                }
                            }
                        }
                    } else if ($value['ezf_field_type'] == 30) {
			$fileName = $model[$value['ezf_field_name']];
			$newFileName = $fileName;
                        $nameEdit = false;
			
			$foder = 'sddynamicmodel-'.$value['ezf_field_name'].'_'.Yii::$app->user->id;
			
			if (stristr($fileName, 'fileNameAuto') == TRUE) {
			    $nameEdit = true;
			    $newFileName = date("Ymd_His").(microtime(true)*10000) . '.mp3';
			    @copy(Yii::$app->basePath . "/../storage/web/audio/$foder/" . $fileName, Yii::$app->basePath . '/../storage/web/audio/' . $newFileName);
			    @unlink(Yii::$app->basePath . "/../storage/web/audio/$foder/" . $fileName);
			}
			
			$model[$value['ezf_field_name']] = $newFileName;
			
			$modelTmp = EzformQuery::getDynamicFormById($table_name->ezf_table, $_POST['id']);

			if (isset($modelTmp['id'])) {
			    
				$fileName = $modelTmp[$value['ezf_field_name']];
				//sddynamicmodel-
				if ($nameEdit) {
				    @unlink(Yii::$app->basePath . "/../storage/web/audio/" . $fileName);
				}
				
			    
			}
		    }
                }

                //
                $EzformReply = new EzformReply();
                $EzformReply->data_id = $_POST['id'];
                $EzformReply->ezf_id = $_POST['ezf_id'];
                $EzformReply->ezf_comment = 'บันทึกข้อมูล';
                $EzformReply->ezf_json_new = json_encode($model->attributes);
                $rst = Yii::$app->db->createCommand("select ezf_json_old from ezform_reply_temp where ezf_id=:ezf_id and data_id=:data_id;", [':ezf_id'=>$_POST['ezf_id'], ':data_id'=>$_POST['id']])->queryOne();
                $EzformReply->ezf_json_old = $rst['ezf_json_old'];
                $EzformReply->type= $modelOld->rstat == 0 ? 0 : 9 ;
                $EzformReply->create_date=new Expression('NOW()');
                $EzformReply->user_create=Yii::$app->user->id;
                $EzformReply->xsourcex = $model->xsourcex;
                $EzformReply->save();
                //end track change

                //send email to user key
                if(\backend\controllers\InputdataController::checkUserConsultant(Yii::$app->user->id, $_POST['ezf_id']) && $modelOld->user_create) {
                    \backend\controllers\InputdataController::consultNotify($EzformReply, $modelOld->user_create);
                }

                if ($model->update()) {

                    //save EMR
                    $modelx = EzformTarget::find()->where('data_id = :data_id AND ezf_id = :ezf_id ', [':data_id' => $_POST["id"], ':ezf_id' => $_POST["ezf_id"]])->one();
                    if($modelx->data_id) {
                        $modelx->user_update = Yii::$app->user->id;
                        $modelx->rstat = $rstat;
                        $modelx->update_date = new Expression('NOW()');
                        $modelx->save();
                    }
                    else{
                        //save EMR
                        $modelx = new EzformTarget();
                        $modelx->ezf_id = $_POST['ezf_id'];
                        $modelx->data_id = $model->id;
                        isset($_POST['target']) ? $modelx->target_id = base64_decode($_POST['target']) : NULL;
                        isset($_POST['comp_id_target']) ? $modelx->comp_id = $_POST['comp_id_target'] : NULL;
                        $modelx->user_create = $model->user_create;
                        $modelx->create_date = $model->create_date;
                        $modelx->user_update = Yii::$app->user->id;
                        $modelx->update_date = new Expression('NOW()');
                        $modelx->rstat = $model->rstat;
                        $modelx->xsourcex = $model->xsourcex;
                        $modelx->save();
                    }
                    //save sql log
                    $sqlLog = new EzformSqlLog();
                    $sqlLog->data_id = $model->id;
                    $sqlLog->ezf_id = $_POST['ezf_id'];
                    $sqlLog->sql_log =  (Yii::$app->db->createCommand()->update($table_name->ezf_table, $model->attributes, ['id'=>$_POST['id']])->rawSql);
                    $sqlLog->user_id = Yii::$app->user->id;
                    $sqlLog->create_date = new Expression('NOW()');
                    $sqlLog->rstat = $model->rstat;
                    $sqlLog->xsourcex = $model->xsourcex;
                    $sqlLog->save();


                }

                Yii::$app->getSession()->setFlash('save_status', [
                    'body'=> '<span class="h3" style="text-align: center;"><b>บันทึกข้อมูลสำเร็จ!</b></span>',
                    'options'=>['class'=>'alert-success']
                ]);
                return $this->redirect(Yii::$app->request->referrer, 302);
            }
        }
    }
    
        public function actionSavedata3()
    {

        if (isset($_POST["id"])) {
            $model_fields = EzformQuery::getFieldsByEzf_id($_POST["ezf_id"]);
            $model_form = EzformFunc::setDynamicModelLimit($model_fields);
            $table_name = EzformQuery::getFormTableName($_POST["ezf_id"]);
            if(Yii::$app->request->post('txtSubmit')){
                //save submit
                $rstat = 2;
            }else{
                //save draft
                $rstat = 1;
            }

            if ($model_form->load(Yii::$app->request->post())) {
                $model_form->attributes = $_POST['SDDynamicModel'];

                $model = new \backend\modules\ezforms\models\EzformDynamic($table_name->ezf_table);
                $modelOld = $model->find()->where('id = :id', ['id' => $_POST['id']])->One();

                //if click final save (rstat not change)
                if(Yii::$app->request->post('finalSave')){
                    $rstat = $modelOld->rstat;
                    $xsourcex = $modelOld->xsourcex;
                }else{
                    $xsourcex = Yii::$app->user->identity->userProfile->sitecode;//hospitalcode
                }
                //VarDumper::dump($modelOld->attributes,10,true);
                //VarDumper::dump($model_form->attributes,10,true);
                $model->attributes = $model_form->attributes;
                $model->id = $_POST['id']; //ห้ามเอาออก ไม่งั้นจะ save ไม่ได้
                $model->rstat = $rstat;
                $model->xsourcex = $xsourcex;
                //$model->hsitecode = $xsourcex;
                //$model->create_date = new Expression('NOW()');
                $model->user_update = Yii::$app->user->id;
                $model->update_date = new Expression('NOW()');
                //echo $rstat;

                if (Yii::$app->request->post('target') <> 'skip' AND Yii::$app->request->post('target') <> 'all') {
                    //จริงๆถ้าไม่มีการเปลี่ยนเป้าหมาย ให้ไปอัพเดจใน ezform_target ด้วย
                    $ezform = EzformQuery::checkIsTableComponent(Yii::$app->request->post('ezf_id'));
                    if ($ezform['special']) {
                        // something
                    } else {
                        $model->target = isset($_POST['target']) ? base64_decode($_POST['target']) : NULL;
                    }
                }

                foreach ($model_fields as $key => $value) {
                    if ($value['ezf_field_type'] == 7 || $value['ezf_field_type'] == 253) {
                        // set Data Sql Format
                        $data = $model[$value['ezf_field_name']];
                        if($data+0) {
                            $explodeDate = explode('/', $data);
                            $formateDate = ($explodeDate[2] - 543) . '-' . $explodeDate[1] . '-' . $explodeDate[0];
                            $model->{$value['ezf_field_name']} = $formateDate;
                        }else{
                            $model->{$value['ezf_field_name']} = new Expression('NULL');
                        }
                    }
                    else if ($value['ezf_field_type'] == 10) {
                        if (count($model->{$value['ezf_field_name']}) > 1)
                            $model->{$value['ezf_field_name']} = implode(',', $model->{$value['ezf_field_name']});
                    }
                    else if ($value['ezf_field_type'] == 14) {
                        if (isset($_FILES['SDDynamicModel']['name'][$value['ezf_field_name']]) && $_FILES['SDDynamicModel']['name'][$value['ezf_field_name']] !='' && is_array($_FILES['SDDynamicModel']['name'][$value['ezf_field_name']])) {
                            $fileItems = $_FILES['SDDynamicModel']['name'][$value['ezf_field_name']];
                            $fileType = $_FILES['SDDynamicModel']['type'][$value['ezf_field_name']];
                            $newFileItems = [];
                            $action = false;
                            foreach ($fileItems as $i => $fileItem) {
                                if($fileItem!=''){
                                    $action = true;
                                    $fileArr = explode('/', $fileType[$i]);
                                    if (isset($fileArr[1])) {
                                        $fileBg = $fileArr[1];

                                        //$newFileName = $fileName;
                                        $newFileName = $value['ezf_field_name'].'_'.($i+1).'_'.$model->id.'_'.date("Ymd_His").(microtime(true)*10000) . '.' . $fileBg;
                                        //chmod(Yii::$app->basePath . '/../backend/web/fileinput/', 0777);
                                        $fullPath = Yii::$app->basePath . '/../backend/web/fileinput/' . $newFileName;
                                        $fieldname = 'SDDynamicModel[' . $value['ezf_field_name'] . '][' . $i . ']';

                                        $file = UploadedFile::getInstanceByName($fieldname);
                                        $file->saveAs($fullPath);

					if($file){
					    $newFileItems[] = $newFileName;

					    //add file to db
					    $file_db = new \backend\modules\ezforms\models\FileUpload();
					    $file_db->tbid = $model->id;
					    $file_db->ezf_id = $value['ezf_id'];
					    $file_db->ezf_field_id = $value['ezf_field_id'];
					    $file_db->file_active = 0;
					    $file_db->file_name = $newFileName;
					    $file_db->file_name_old = $fileItem;
					    $file_db->target = ($model->ptid ? $model->ptid :  $model->target).'';
					    $file_db->save();
					}
                                    }
                                }
                            }
                            $res = Yii::$app->db->createCommand("SELECT `" . $value['ezf_field_name'] . "` as filename FROM `" . $table_name->ezf_table . "` WHERE id = :dataid", [':dataid' => $_POST['id']])->queryOne();
                            if($action){
                                $model->{$value['ezf_field_name']} = implode(',', $newFileItems);

//				if($res){
//				    $res_items = explode(',', $res['filename']);
//
//				    foreach ($res_items as $dataTmp) {
//					@unlink(Yii::$app->basePath . '/../backend/web/fileinput/' . $dataTmp);
//				    }
//				}
                            } else {
                                if($res){
                                    $model->{$value['ezf_field_name']} = $res['filename'];
                                }
                            }

                        } else {
                            $res = Yii::$app->db->createCommand("SELECT `" . $value['ezf_field_name'] . "` as filename FROM `" . $table_name->ezf_table . "` WHERE id = :dataid", [':dataid' => $_POST['id']])->queryOne();
                            $model->{$value['ezf_field_name']} = $res['filename'];
                        }
                    }
                    else if ($value['ezf_field_type'] == 24) {

                        if (stristr($model[$value['ezf_field_name']], 'tmp.png') == TRUE) {
                            //set data Drawing
                            $fileArr = explode(',', $model[$value['ezf_field_name']]);
                            $fileName = $fileArr[0];
                            $fileBg = $fileArr[1];
                            $newFileName = $fileName;
                            $newFileBg = $fileBg;
                            $nameEdit = false;
                            $bgEdit = false;
                            if (stristr($fileName, 'tmp.png') == TRUE) {
                                $nameEdit = true;
                                $newFileName = date("Ymd_His").(microtime(true)*10000) . '.png';
                                @copy(Yii::$app->basePath . '/../backend/web/drawing/' . $fileName, Yii::$app->basePath . '/../storage/web/drawing/data/' . $newFileName);
                                @unlink(Yii::$app->basePath . '/../backend/web/drawing/' . $fileName);
                            }
                            if (stristr($fileBg, 'tmp.png') == TRUE) {
                                $bgEdit = true;
                                $newFileBg = 'bg_' . date("Ymd_His").(microtime(true)*10000) . '.png';
                                @copy(Yii::$app->basePath . '/../storage/web/drawing/' . $fileBg, Yii::$app->basePath . '/../storage/web/drawing/bg/' . $newFileBg);
                                @unlink(Yii::$app->basePath . '/../storage/web/drawing/' . $fileBg);
                            }

                            $model[$value['ezf_field_name']] = $newFileName . ',' . $newFileBg;
                            $modelTmp = EzformQuery::getDynamicFormById($table_name->ezf_table, $_POST['id']);

                            if (isset($modelTmp['id'])) {
                                $fileArr = explode(',', $modelTmp[$value['ezf_field_name']]);
                                if (count($fileArr) > 1) {
                                    $fileName = $fileArr[0];
                                    $fileBg = $fileArr[1];
                                    if ($nameEdit) {
                                        @unlink(Yii::$app->basePath . '/../storage/web/drawing/data/' . $fileName);
                                    }
                                    if ($bgEdit) {
                                        @unlink(Yii::$app->basePath . '/../storage/web/drawing/bg/' . $fileBg);
                                    }
                                }
                            }
                        }
                    } else if ($value['ezf_field_type'] == 30) {
			$fileName = $model[$value['ezf_field_name']];
			$newFileName = $fileName;
                        $nameEdit = false;
			
			$foder = 'sddynamicmodel-'.$value['ezf_field_name'].'_'.Yii::$app->user->id;
			
			if (stristr($fileName, 'fileNameAuto') == TRUE) {
			    $nameEdit = true;
			    $newFileName = date("Ymd_His").(microtime(true)*10000) . '.mp3';
			    @copy(Yii::$app->basePath . "/../storage/web/audio/$foder/" . $fileName, Yii::$app->basePath . '/../storage/web/audio/' . $newFileName);
			    @unlink(Yii::$app->basePath . "/../storage/web/audio/$foder/" . $fileName);
			}
			
			$model[$value['ezf_field_name']] = $newFileName;
			
			$modelTmp = EzformQuery::getDynamicFormById($table_name->ezf_table, $_POST['id']);

			if (isset($modelTmp['id'])) {
			    
				$fileName = $modelTmp[$value['ezf_field_name']];
				//sddynamicmodel-
				if ($nameEdit) {
				    @unlink(Yii::$app->basePath . "/../storage/web/audio/" . $fileName);
				}
				
			    
			}
		    }
                }

                //
                $EzformReply = new EzformReply();
                $EzformReply->data_id = $_POST['id'];
                $EzformReply->ezf_id = $_POST['ezf_id'];
                $EzformReply->ezf_comment = 'บันทึกข้อมูล';
                $EzformReply->ezf_json_new = json_encode($model->attributes);
                $rst = Yii::$app->db->createCommand("select ezf_json_old from ezform_reply_temp where ezf_id=:ezf_id and data_id=:data_id;", [':ezf_id'=>$_POST['ezf_id'], ':data_id'=>$_POST['id']])->queryOne();
                $EzformReply->ezf_json_old = $rst['ezf_json_old'];
                $EzformReply->type= $modelOld->rstat == 0 ? 0 : 9 ;
                $EzformReply->create_date=new Expression('NOW()');
                $EzformReply->user_create=Yii::$app->user->id;
                $EzformReply->xsourcex = $model->xsourcex;
                $EzformReply->save();
                //end track change

                //send email to user key
                if(\backend\controllers\InputdataController::checkUserConsultant(Yii::$app->user->id, $_POST['ezf_id']) && $modelOld->user_create) {
                    \backend\controllers\InputdataController::consultNotify($EzformReply, $modelOld->user_create);
                }

                if ($model->update()) {

                    //save EMR
                    $modelx = EzformTarget::find()->where('data_id = :data_id AND ezf_id = :ezf_id ', [':data_id' => $_POST["id"], ':ezf_id' => $_POST["ezf_id"]])->one();
                    if($modelx->data_id) {
                        $modelx->user_update = Yii::$app->user->id;
                        $modelx->rstat = $rstat;
                        $modelx->update_date = new Expression('NOW()');
                        //$modelx->hsitecode = $model->hsitecode;
                        $modelx->save();
                    }
                    else{
                        //save EMR
                        $modelx = new EzformTarget();
                        $modelx->ezf_id = $_POST['ezf_id'];
                        $modelx->data_id = $model->id;
                        isset($_POST['target']) ? $modelx->target_id = base64_decode($_POST['target']) : NULL;
                        isset($_POST['comp_id_target']) ? $modelx->comp_id = $_POST['comp_id_target'] : NULL;
                        $modelx->user_create = $model->user_create;
                        $modelx->create_date = $model->create_date;
                        $modelx->user_update = Yii::$app->user->id;
                        $modelx->update_date = new Expression('NOW()');
                        $modelx->rstat = $model->rstat;
                        $modelx->xsourcex = $model->xsourcex;
                        //$modelx->hsitecode = $model->hsitecode;
                        $modelx->save();
                    }
                    //save sql log
                    $sqlLog = new EzformSqlLog();
                    $sqlLog->data_id = $model->id;
                    $sqlLog->ezf_id = $_POST['ezf_id'];
                    $sqlLog->sql_log =  (Yii::$app->db->createCommand()->update($table_name->ezf_table, $model->attributes, ['id'=>$_POST['id']])->rawSql);
                    $sqlLog->user_id = Yii::$app->user->id;
                    $sqlLog->create_date = new Expression('NOW()');
                    $sqlLog->rstat = $model->rstat;
                    $sqlLog->xsourcex = $model->xsourcex;
                    $sqlLog->save();


                }

                Yii::$app->getSession()->setFlash('save_status', [
                    'body'=> '<span class="h3" style="text-align: center;"><b>บันทึกข้อมูลสำเร็จ!</b></span>',
                    'options'=>['class'=>'alert-success']
                ]);
                return $this->redirect(Yii::$app->request->referrer, 302);
            }
        }
    }



}
