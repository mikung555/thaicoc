<?php

namespace frontend\controllers;

use Yii;
use frontend\models\Project84Sum;
use frontend\models\Project84SumSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * Project84SumController implements the CRUD actions for Project84Sum model.
 */
class Project84SumController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Project84Sum models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new Project84SumSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Project84Sum model.
     * @param string $zone
     * @param string $address
     * @return mixed
     */
    public function actionView($zone, $address)
    {
        return $this->render('view', [
            'model' => $this->findModel($zone, $address),
        ]);
    }

    /**
     * Creates a new Project84Sum model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Project84Sum();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'zone' => $model->zone, 'address' => $model->address]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Project84Sum model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $zone
     * @param string $address
     * @return mixed
     */
    public function actionUpdate($zone, $address)
    {
        $model = $this->findModel($zone, $address);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'zone' => $model->zone, 'address' => $model->address]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Project84Sum model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $zone
     * @param string $address
     * @return mixed
     */
    public function actionDelete($zone, $address)
    {
        $this->findModel($zone, $address)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Project84Sum model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $zone
     * @param string $address
     * @return Project84Sum the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($zone, $address)
    {
        if (($model = Project84Sum::findOne(['zone' => $zone, 'address' => $address])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
     public function GetHospitalName($sitecode)
    {
        $sql = "SELECT name,amphur,province FROM  `all_hospital_thai` WHERE hcode='$sitecode'";
        $dataProvider = Yii::$app->db->createCommand($sql)->queryAll();
        return $dataProvider;
    }


    public function GetAmphur($report_id, $provincecode)
    {
        
        $sql = 'SELECT
            aht.provincecode,
            aht.province,
            aht.amphurcode,
            aht.amphur,
            aht.tamboncode,
            aht.tambon,
            aht.hcode,
            aht.`name`,
            cca02
            f2v2a1,
            f2v2a1b2,
            f2v6a3b1
        FROM (
            SELECT
                COUNT(ptid) AS cca02,
                add1n6code,
                f2v2a1,
                f2v2a1b2,
                f2v6a3b1,
                LEFT(add1n6code, 2) AS provincecode,
                SUBSTR(add1n6code FROM 3 FOR 2) AS amphurcode,
                SUBSTR(add1n6code FROM 5 FOR 2) AS tamboncode
            FROM (
                SELECT
                    ptid,
                    f2v2a1,
                    f2v2a1b2,
                    f2v6a3b1,
                    (SELECT add1n6code FROM tb_data_1 WHERE add1n6code IS NOT NULL AND id = t.ptid LIMIT 1) AS add1n6code
                FROM (
                    SELECT
                        DISTINCT ptid AS ptid,
                        f2v2a1,
                        f2v2a1b2,
                        f2v6a3b1
                    FROM
                        tb_data_3
                    WHERE
                        (
                            f2v1 BETWEEN "2015-10-01"
                            AND NOW()
                        )
                    AND rstat != 0
                    AND rstat != 3
                    AND hsitecode IS NOT NULL
                    AND hsitecode != 0
                    AND hsitecode NOT LIKE "A%"
                    AND hsitecode NOT LIKE "Z%"
                    AND hsitecode NOT LIKE "90%"
                    AND hsitecode NOT LIKE "91%"
                    AND hsitecode NOT LIKE "92%"
                ) AS t
            ) AS t
            WHERE add1n6code IS NOT NULL
            AND LEFT(add1n6code, 2) = '.$provincecode.'
            GROUP BY SUBSTR(add1n6code FROM 3 FOR 4)
        ) AS t
        INNER JOIN (
            SELECT
                provincecode,
                province,
                amphurcode,
                amphur,
                tamboncode,
                tambon,
                hcode,
                `name`

            FROM
                all_hospital_thai
            WHERE provincecode = '.$provincecode.'
            GROUP BY CONCAT(amphurcode,tamboncode)
        ) AS aht USING (provincecode, amphurcode, tamboncode)
				ORDER BY
		    CASE 1
		        WHEN aht.amphurcode = 1 THEN
		            1
		        ELSE
		            aht.amphur
		    END,
		    aht.tambon';
        $dataProvider = Yii::$app->db->createCommand($sql)->queryAll();
        return $dataProvider;
    }


    public function GetHospital($report_id, $provincecode, $amphurcode)
    {
        $sql = "SELECT
            al.provincecode,
            al.hcode,
            a.sitecode,
            b.provincecode,
            b.report_id,
            d.fromDate,
            d.toDate,
            al.amphurcode,
            b.cca02,
            b.cca,
            b.nall,
            b.abnormal,
            b.suspected,
            b.ctmri,
            al.tambon,
            al.amphur,
            e.cca01,
            c.p_surgery,
            b.zone
            FROM
            all_hospital_thai al
            INNER JOIN tb_data_3 as a ON al.hcode = a.sitecode
            INNER JOIN project84_report_sum_province as b ON b.provincecode = al.provincecode
            INNER JOIN project84_report_sum_hospital as c ON c.report_id = b.report_id
            INNER JOIN project84_report_control as d ON d.report_id = c.report_id
            INNER JOIN project84_report_sum as e ON e.report_id = c.report_id
            WHERE al.provincecode IS NOT NULL AND b.provincecode IS NOT NULL 
            AND al.provincecode='$provincecode' AND b.report_id='$report_id' 
            AND al.amphurcode='$amphurcode' GROUP BY `name`, code9";
        $dataProvider = Yii::$app->db->createCommand($sql)->queryAll();
        return $dataProvider;
    }
}
