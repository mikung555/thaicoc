<?php

namespace frontend\controllers\classes;

use Yii;
use yii\helpers\Html;
use backend\modules\ezforms\components\EzformQuery;
use backend\modules\ezforms\models\EzformFields;
use backend\modules\ezforms\components\EzformFunc;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of EzformFuncPrint
 *
 * @author AR9
 */
class EzformFuncPrint extends EzformFunc {

    public static function getTypeEprintform($model_from, $model, $form = null, $options_input = null) {
        //ถ้าเป็น Reference field
        if ($model->ezf_field_type == 18) {
            //เก็บ temp ไว้
            $ezf_field_order = $model->ezf_field_order;
            $ezf_field_id = $model->ezf_field_id;
            $ezf_field_name = $model->ezf_field_name;
            $ezf_field_lenght = $model->ezf_field_lenght;
            $name = $model->ezf_field_name;
            //
//			VarDumper::dump($model, 10, true);
            //echo $model->ezf_field_ref_field;
//			Yii::$app->end();

            $model = EzformFields::find()->where(['ezf_field_id' => $model->ezf_field_ref_field])->andWhere('ezf_field_type IS NOT NULL')->one();
            //VarDumper::dump($model, 10, true);
            //Yii::$app->end();
            $model->ezf_field_name = $ezf_field_name;
            $model->ezf_field_order = $ezf_field_order;
            $model->ezf_field_lenght = $ezf_field_lenght;
            $model->ezf_field_ref_field = $ezf_field_id; //เก็บ id field ต้นทางไปด้วย
            //VarDumper::dump($model, 10, true);
        }
        if ($model->ezf_field_type == 31) {
            //เก็บ temp ไว้
            $ezf_field_order = $model->ezf_field_order;
            $ezf_field_id = $model->ezf_field_id;
            $ezf_field_name = $model->ezf_field_name;
            $ezf_field_lenght = $model->ezf_field_lenght;
            //
//			VarDumper::dump($model, 10, true);
            //echo $model->ezf_field_ref_field;
//			Yii::$app->end();

            $model = EzformFields::find()->where(['ezf_field_id' => $model->ezf_field_ref_field])->andWhere('ezf_field_type IS NOT NULL')->one();
            //VarDumper::dump($model, 10, true);
            //Yii::$app->end();
            $model->ezf_field_name = $ezf_field_name;
            $model->ezf_field_order = $ezf_field_order;
            $model->ezf_field_lenght = $ezf_field_lenght;
            $model->ezf_field_ref_field = $ezf_field_id; //เก็บ id field ต้นทางไปด้วย
            //VarDumper::dump($model, 10, true);
        }
        return self::typeInputValidatePrint($model_from, $model, $form, $options_input, $name);
    }

    public static function typeInputValidatePrint($model_from, $model, $form, $options_input, $name) {

        $field_item = EzformQuery::getInputId($model->ezf_field_type);

        if ($field_item['input_id'] == 10 || $field_item['input_id'] == 13 || $field_item['input_id'] == 75) {
            $scrip = "<script>$('#sddynamicmodel-{$name}, #w0-{$name}').change(function(){
                        getAmphur($(this).val());
                     });</script>";
        }

        if ($field_item) {

            try {
                $options = ['class' => 'form-control'];
                $fieldArray = @unserialize($field_item['input_option']);
                if (is_array($fieldArray)) {
                    $options = $fieldArray;
                }

                //ตรวจสอบ field_help
                if ($model['ezf_field_val'] == 99) {
                    $model["ezf_field_help"] = '<br>' . $model["ezf_field_help"];
                } else {
                    $model["ezf_field_help"] = '';
                }

                $enableLabel = true;
                if ($model['ezf_field_type'] == 24 && $model['ezf_field_default'] != '') {

                    $options['default_bg'] = $model['ezf_field_default'];
                    $arr = @unserialize($model['ezf_field_options']);
                    if (is_array($arr)) {
                        $options['allow_bg'] = $arr['allow_bg'];
                    }
                } elseif (in_array($model['ezf_field_type'], [13])) {
                    $enableLabel = false;
                } else if ($model['ezf_field_type'] == 3) {
                    $options = array_merge($options, ['rows' => $model['ezf_field_rows']]);
                }

                if ($options_input != null) {
                    $options = array_merge($options, $options_input);
                }

                $labelOptions = [];
                $label = $model["ezf_field_label"];

                if ($model["ezf_field_color"]) {
                    $labelOptions['style'] = 'color: ' . $model["ezf_field_color"] . ';';
                }

                if (isset($model["ezf_field_icon"]) && $model["ezf_field_icon"] == 1) {
                    $label .= ' <i class="glyphicon glyphicon-star"></i>';
                }
                if (!$enableLabel) {
                    $label = '';
                }
                $labelOptions['label'] = $label;
                $options['labelOptions'] = $labelOptions;

                $html = '';
                if ($field_item['input_function_validate'] == 'widget') {
                    unset($options['labelOptions']);
                    $options['model'] = $model_from;
                    $options['attribute'] = $model['ezf_field_name'];
                    $options['options'] = ['field' => $model, 'options_custom' => $form->attributes];
                    eval("\$html = \$form->field(\$model_from, \$model['ezf_field_name'])->hint(\$model['ezf_field_hint'])->{$field_item['input_function_validate']}({$field_item['input_class_validate']}, \$options)->label(\$label, \$labelOptions);");
                } else if ($field_item['input_class_validate'] == 'EzformWidget') {
                    eval("\$html = \\backend\\modules\\ezforms\\components\\" . $field_item['input_class_validate'] . "::{$field_item['input_function_validate']}(\$form, \$model_from, \$model['ezf_field_name'], \$model, \$options);");
                    //eval("\$html = \\backend\\modules\\ezforms\\components\\{$field_item['input_class_validate']}::{$field_item['input_function_validate']}(\$form, \$model_from, \$model['ezf_field_name'], \$model, \$options);");
                } else {
                    eval("\$html = \$form->field(\$model_from, \$model['ezf_field_name'])->hint(\$model['ezf_field_hint'])->{$field_item['input_function_validate']}(\$options)->label(\$label, \$labelOptions);");
                }




                $message = "<div";
                if ($options_input['query_tools_render']) {
                    $message .= " class='col-md-12'>";
                } else {
                    $message .= " class='col-md-$model->ezf_field_lenght col-md-offset-$model->ezf_margin_col" . ($form->attributes['print'] == 1 ? ' visible-print-inline-block' : null) . "' id='rowItem' item-id='$model->ezf_field_id' data-dad-id='$model->ezf_field_order'>";
                }
                //change Query tools
                if ($form->attributes['rstat'] == 2 && $model->ezf_field_type != 2 && $model->ezf_field_type != 15) {
                    $message .= Html::button('<i class=\'fa fa-pencil\'></i> ขอแก้ไข', [
                                'class' => 'ezform-btn-change btn btn-primary btn-xs',
                                'title' => Yii::t('app', 'ส่งคำขอแก้ไขข้อมูล'),
                                'data-url' => Url::to(['query-request/create', 'ezf_id' => Yii::$app->request->get('ezf_id'), 'dataid' => Yii::$app->request->get('dataid'), 'ezf_field_id' => $model->ezf_field_id]),
                            ]) . '<br />';
                } else if ((string) $form->attributes['rstat'] == 'annotated' && $model->ezf_field_type != 2 && $model->ezf_field_type != 15 && $model->ezf_field_type != 13 && $model->ezf_field_type != 19 && $model->ezf_field_type != 23 && $model->ezf_field_type != 26) {
                    $message .= '<code>' . $model->ezf_field_name . '</code><br>';
                }


//			if($model['ezf_field_type']==3){
//			    $html='
//				<div class="form-group field-'.$model['ezf_field_name'].'">
//				    <label class="control-label" for="'.$model['ezf_field_name'].'">'.$model['ezf_field_label'].'</label>
//				    <div style="border: 1px solid #ccc; padding: 0 10px;">'.(($model_from[$model['ezf_field_name']]!='')?$model_from[$model['ezf_field_name']]:'<br>').'</div>
//				</div>
//			    
//			    ';
//			}


                $message .= $html . "</div>" . $scrip;
            } catch (\yii\base\Exception $ex) {
                $message = '<code>' . $ex->getMessage() . ' !!!กรุณาลบแล้วสร้างคำถามใหม่</code>';
            }
            return $message;
        } else if ($field_item['input_function'] == 'hidden') {
            return Html::activeHiddenInput($model_from, $model['ezf_field_name']);
        }
    }

}
