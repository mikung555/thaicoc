<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace frontend\controllers\classes;

/**
 * Description of TimelineReport
 *
 * @author TOSHIBA
 */
class TimelineReport {

    //put your code here


   
    public function getNewTimeline($dataPost) {
        ini_set("memory_limit","256M");
        $startdate = $dataPost['startdate'] == "" ? $dataPost['enddate'] : $dataPost['startdate'];
        $enddate = $dataPost['enddate'];

        $sqlM = "SELECT gid,gname,gdetail, active, created_by, sitecode, ezf_id, ezf_name, ezf_table, enable_form FROM inv_gen WHERE public=1 AND active=1 AND approved=1 AND gtype =0 AND comp_id_target='1461911117076186800' AND gid= :gid";
        $queryM = \Yii::$app->db->createCommand($sqlM, [':gid' => $dataPost['module_id']])->queryOne();
        $form_data = \appxq\sdii\utils\SDUtility::string2Array($queryM['enable_form']);

        $form_lst = $form_data['form'];


        $datalist = [];

        if ($queryM) {  

            foreach ($form_lst as $val) {
               $sql = "SELECT ezf_name,ezf_table,field_detail,ezf_timeline from ezform where ezf_id = :ezf_id";
               $ezform = \Yii::$app->db->createCommand($sql, [':ezf_id' => $val])->queryOne();
               //\appxq\sdii\utils\VarDumper::dump($ezform);
               if ($ezform['ezf_timeline'] != "") {
                   $ezftimeline = "CONCAT({$ezform['ezf_timeline']})";
               }else if ($ezform['field_detail'] != "") {
                   $ezftimeline = "CONCAT({$ezform['field_detail']})";
               }else{
                   $ezftimeline = "''";
               }

               $sql = "SELECT id,create_date,DATE(create_date) `date`,TIME(create_date) `time`,{$ezftimeline} `data` from {$ezform['ezf_table']} WHERE DATE(create_date)=CURDATE()";
               
               $results = \Yii::$app->db->createCommand($sql,[':startdate' => $startdate,':enddate' => $enddate])->queryAll();

               $datalist = array_merge($datalist,$results);
            }   
            $datalist = \appxq\sdii\utils\SortArray::sortingArrayDESC($datalist, 'create_date');

        }
        
        return $datalist;
    }
    public function getTimeline($dataPost) {
        $startdate = $dataPost['startdate'];
        $enddate = $dataPost['enddate'];
        
        $sqlM = "SELECT gid,main_ezf_table,gname,gdetail, active, created_by, sitecode, ezf_id, ezf_name, ezf_table, enable_form FROM inv_gen WHERE public=1 AND active=1 AND approved=1 AND gtype =0 AND comp_id_target='1461911117076186800' AND gid= :gid";
        $queryM = \Yii::$app->db->createCommand($sqlM, [':gid' => $dataPost['module_id']])->queryOne();
        $form_data = \appxq\sdii\utils\SDUtility::string2Array($queryM['enable_form']);
        
        //$form_lst = $form_data['form'];
        
        
        foreach ($form_data['form'] as $val) {
            if ($form_lst == null) {
                $form_lst .= "'" . $val . "'";
            } else {
                $form_lst .= ",'" . $val . "'";
            }
        }
        //\appxq\sdii\utils\VarDumper::dump($form_lst); exit();
        $datalist = [];

        if ($queryM) {


            $sqlEzf = " SELECT ezf_id, ezf_name,ezf_detail,ezf_table, user_create, create_date,field_detail , ezf_timeline 
                    FROM ezform WHERE ezf_id in($form_lst) ";
            $resEzf = \Yii::$app->db->createCommand($sqlEzf)->queryAll();
            //\appxq\sdii\utils\VarDumper::dump($resEzf); 
            $sql = "";
                foreach ($resEzf as $tabledata) {
                    if ($tabledata['ezf_timeline'] != "") {
                        $ezftimeline = "CONCAT('{$tabledata['ezf_name']}<br>',{$tabledata['ezf_timeline']})";
                    }else if ($tabledata['field_detail'] != "") {
                        $ezftimeline = "CONCAT('{$tabledata['ezf_name']}<br>',{$tabledata['field_detail']})";
                    }else{
                        $ezftimeline = "'{$tabledata['ezf_name']}'";
                    }                    
                    if ($sql == '') {
                        $sql = "SELECT `id`,`create_date`,date(create_date) as 'date', time(create_date) as 'time',{$tabledata['ezf_id']} as 'ezf_id',{$ezftimeline} `data`  FROM `" . $tabledata['ezf_table'] . "` WHERE DATE(create_date)=CURDATE()";
                    } else {
                        $sql .= "UNION SELECT `id`,`create_date`,date(create_date) as 'date', time(create_date) as 'time',{$tabledata['ezf_id']} as 'ezf_id',{$ezftimeline} `data` FROM `" . $tabledata['ezf_table'] . "` WHERE DATE(create_date) =CURDATE()";
                    }
                }
                $datalist = \Yii::$app->dbutf8mb4->createCommand($sql)->queryAll();

            $datalist = \appxq\sdii\utils\SortArray::sortingArrayDESC($datalist, 'create_date');
//            $datalist = \appxq\sdii\utils\SortArray::sortingArrayDESC($datalist, 'date');
            
        }
        return $datalist;
    }
    
    public function getCon($id,$startdate,$enddate){
        
        $SqlEzf = "SELECT ezf_id, ezf_name,ezf_table,field_detail,ezf_timeline FROM ezform WHERE ezf_id = :gid ";
        $QueryEzf = \Yii::$app->db->createCommand($SqlEzf, [':gid' => $id])->queryOne();
        //\appxq\sdii\utils\VarDumper::dump($QueryEzf);
        $timeline = $QueryEzf['ezf_timeline'];
        $field_detail = $QueryEzf['field_detail'];
        $table = $QueryEzf['ezf_table'];
       
        if ($timeline != NULL) {
            // รับ id ezf มาแปลงค่า field_detail/ezf_timeline 
            $Sql = "SELECT `id`,create_date,date(create_date) as 'date',TIME(create_date) as 'time',IFNULL(CONCAT(" . $timeline . "),' ') as 'ezf_con' FROM $table WHERE DATE($table.create_date) BETWEEN '$startdate' AND '$enddate' ORDER BY  DATE($table.create_date) DESC";
            $Query = \Yii::$app->db->createCommand($Sql)->queryAll();
            //$Query = "timeline";
            //WHERE DATE($table.create_date) >= '$startdate' && DATE($table.create_date) <= '$enddate'
            
        } else {
            if ($field_detail != NULL) {
                $Sql = "SELECT `id`,create_date,date(create_date) as 'date',TIME(create_date) as 'time',IFNULL(CONCAT(" . $field_detail . "),' ') as 'ezf_con' FROM $table WHERE DATE($table.create_date) BETWEEN '$startdate' AND '$enddate' ORDER BY  DATE($table.create_date) DESC";
                $Query = \Yii::$app->db->createCommand($Sql)->queryAll();
                //$Query = "field";
                //\appxq\sdii\utils\VarDumper::dump($Query);        exit();
            } else {
                
                $Sql = "SELECT `id`,create_date,date(create_date) as 'date',TIME(create_date) as 'time','' as 'ezf_con' FROM $table WHERE DATE($table.create_date) BETWEEN '$startdate' AND '$enddate' ORDER BY  DATE($table.create_date) DESC";
                $Query = \Yii::$app->db->createCommand($Sql)->queryAll();
                
            }
        }
        
        return $Query;
    }
    
    
    

}
