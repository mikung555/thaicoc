<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace frontend\controllers\classes;

/**
 * Description of ModulsReport
 *
 * @author jokeclancool
 */
class ModulesReport {

    //put your code here
    public function getReportOfModules() {
        $data = null;
        $dataProvider = [];
        $dataTotal = [];
        $allpatient = '';
        $sql = " SELECT gid, gname,gdetail, active, created_by, sitecode, ezf_id, ezf_name, ezf_table, enable_form "
                . " FROM inv_gen WHERE gid<>'1491376176059784300' AND public=1 AND active=1 AND approved=1 AND gtype =0 AND comp_id_target='1461911117076186800'  ";
        $result = \Yii::$app->db->createCommand($sql)->queryAll();
        if ($result) {
            $x = 0;
            foreach ($result as $val) {
                $form_data = \appxq\sdii\utils\SDUtility::string2Array($val['enable_form']);
                $amt_patient = 0;
                $j = 0;
                $sqlEzf = " SELECT ezf_id, ezf_table FROM ezform WHERE ezf_id='" . $val['ezf_id'] . "' ";
                $resEzf = \Yii::$app->db->createCommand($sqlEzf)->queryOne();

                if ($resEzf['ezf_table'] != null && $resEzf['ezf_id'] > 2) {
                    $sqlPatient = " SELECT count(DISTINCT ptid) as pacount FROM " . $resEzf['ezf_table'] . "  ";
                    $resPatient = \Yii::$app->db->createCommand($sqlPatient)->queryOne();

                    $amt_patient += $resPatient['pacount'];
                }
                $j ++;

                // count user
                $sqlUCount = " SELECT count(user_id) as ucount FROM inv_favorite WHERE gid='" . $val['gid'] . "' ";
                $resUCount = \Yii::$app->db->createCommand($sqlUCount)->queryOne();

                // count site
                $sqlSCount = " SELECT count(DISTINCT sitecode) as scount FROM inv_favorite inv "
                        . " inner join user_profile usp ON inv.user_id=usp.user_id WHERE inv.gid= :gid ";
                $resSCount = \Yii::$app->db->createCommand($sqlSCount, [':gid' => $val['gid']])->queryOne();

                $sqlPatient = " SELECT SUM(AllPatient) as allpatient, SUM(IF(gid=:gidnp,nPatient,0)) as npatient
,                               SUM(IF(gid=:gidnt,nTime,0)) as ntime FROM rpt_patient_count
                    ";
                $resPatient = \Yii::$app->db->createCommand($sqlPatient, [':gidnp' => $val['gid'], ':gidnt' => $val['gid']])->queryOne();
                //\appxq\sdii\utils\VarDumper::dump($resPatient);
                $dataProvider[$x]['mod_id'] = $val['gid'];
                $dataProvider[$x]['mod_name'] = $val['gname'];
                $dataProvider[$x]['mod_form'] = count($form_data['form']);
                $dataProvider[$x]['mod_site'] = $resSCount['scount'];
                $dataProvider[$x]['mod_user'] = $resUCount['ucount'];
                $dataProvider[$x]['mod_patient'] = $resPatient['npatient'];
                $dataProvider[$x]['mod_allpatient'] = $resPatient['allpatient'];
                $dataProvider[$x]['mod_ntime'] = $resPatient['ntime'];

                $dataTotal['mod_form_total'] += $dataProvider[$x]['mod_form'];
                $dataTotal['mod_site_total'] += $dataProvider[$x]['mod_site'];
                $dataTotal['mod_user_total'] += $dataProvider[$x]['mod_user'];
                $dataTotal['mod_patient_total'] += $dataProvider[$x]['mod_patient'];
                $dataTotal['mod_allpatient_total'] += $dataProvider[$x]['mod_allpatient'];
                $dataTotal['mod_ntime_total'] += $dataProvider[$x]['mod_ntime'];

                 if($allpatient == '')
                     $allpatient = $resPatient['allpatient'];
                //$dataProvider[$x]['mod_site'] = number_format($dataProvider[$x]['mod_site']);

                $x++;
            }
        }
        $dataProvider = \appxq\sdii\utils\SortArray::sortingArrayDESC($dataProvider, 'mod_site');
        $dataProvider = new \yii\data\ArrayDataProvider([
            'allModels' => $dataProvider,
            'pagination' => [
                'pageSize' => 50,
            ],
        ]);
        return [
            'dataProvider' => $dataProvider,
            'dataTotal' => $dataTotal,
            'allpatient' => $allpatient
        ];
    }

    public function getReportOfForms($module_id, $sitecode = null, $startDate = null, $endDate = null) {
        $data = null;
        $dataProvider = [];
        $sql = " SELECT gid, gname,gdetail, active, created_by, sitecode, ezf_id, ezf_name, ezf_table, enable_form "
                . " FROM inv_gen WHERE gid<>'1491376176059784300' AND public=1 AND active=1 AND approved=1 AND gtype =0 AND comp_id_target='1461911117076186800' AND gid= :gid";
        $result = \Yii::$app->db->createCommand($sql, [':gid' => $module_id])->queryOne();
        $form_data = \appxq\sdii\utils\SDUtility::string2Array($result['enable_form']);
        //\appxq\sdii\utils\VarDumper::dump($sitecode);

        $form_lst = $form_data['form'];
        if ($result) {
            $x = 0;
            $params = [];
            foreach ($form_lst as $val) {

                $sqlEzf = " SELECT ez.ezf_id, ez.ezf_name,ez.ezf_table, ez.user_create, ez.create_date 
                    FROM (ezform ez INNER JOIN ezform_favorite ezf ON ez.ezf_id=ezf.ezf_id )
                    INNER JOIN user_profile upro ON ezf.userid=upro.user_id
                    WHERE ez.ezf_id = :ezf_id ";
                $params = [
                    ':ezf_id' => $val,
                ];
                if ($startDate != null || $endDate != null) {
                    $sqlEzf .= " AND ez.create_date BETWEEN :startDate AND :endDate ";
                    $params = [
                        ':ezf_id' => $val,
                        ':startDate' => $startDate,
                        ':endDate' => $endDate
                    ];
                }
                if ($sitecode != null) {
                    $sqlEzf .= " AND upro.sitecode=:sitecode ";
                    $params = [
                        ':ezf_id' => $val,
                        ':sitecode' => $sitecode,
                        ':startDate' => $startDate,
                        ':endDate' => $endDate
                    ];
                }

                $resEzf = \Yii::$app->db->createCommand($sqlEzf, $params)->queryOne();

                $tb_data = $resEzf['ezf_table'];
                //\appxq\sdii\utils\VarDumper::dump($resEzf);
                if ($tb_data != null) {
                    $params = [];
                    if ($sitecode != null) {
                        $tbwhere = " WHERE sitecode = :sitecode ";
                        $params = [':sitecode' => $sitecode];
                    }
                    $sqlIndat = " SELECT count(DISTINCT ptid) as form_all 
                        ,SUM(IF(YEAR(create_date) = YEAR(create_date) ,1,0)) as form_year
                        ,SUM(IF(YEAR(NOW()) = YEAR(create_date) AND MONTH(NOW()) = MONTH(create_date) ,1,0)) as form_month
                        ,SUM(IF(YEAR(create_date) = YEAR(CURDATE()) AND MONTH(create_date) = MONTH(CURDATE()) AND WEEKDAY(create_date)= WEEKDAY(CURDATE()) ,1,0)) as form_week
                        ,SUM(IF(DATE(create_date) = CURDATE() ,1,0)) as form_date
                        FROM (SELECT ptid, create_date FROM $tb_data $tbwhere GROUP BY ptid) as tbdata  ";
                    $resIndata = \Yii::$app->db->createCommand($sqlIndat, $params)->queryOne();

                    $dataProvider[$x]['form_id'] = $resEzf['ezf_id'];
                    $dataProvider[$x]['form_name'] = $resEzf['ezf_name'];
                    $dataProvider[$x]['form_all'] = $resIndata['form_all'];
                    $dataProvider[$x]['form_year'] = $resIndata['form_year'];
                    $dataProvider[$x]['form_month'] = $resIndata['form_month'];
                    $dataProvider[$x]['form_week'] = $resIndata['form_week'];
                    $dataProvider[$x]['form_date'] = $resIndata['form_date'];

                    $x++;
                }
            }
            $dataProvider = \appxq\sdii\utils\SortArray::sortingArrayDESC($dataProvider, 'form_all');
        }

        return $dataProvider;
    }

    public function getReportOfSites($module_id) {
        $data = null;
        $dataProvider = [];
        $sqlSite = " SELECT sitecode, `name`  FROM (SELECT  DISTINCT sitecode FROM inv_favorite inv 
                inner join user_profile usp ON inv.user_id=usp.user_id WHERE inv.gid= :gid) as invble
                INNER JOIN all_hospital_thai ah ON invble.sitecode=ah.hcode ORDER BY sitecode
                ";
        $resSite = \Yii::$app->db->createCommand($sqlSite, [':gid' => $module_id])->queryAll();
        $sites = "";
        foreach ($resSite as $val) {
            if ($sites == '') {
                $sites .= "'" . $val['sitecode'] . "'";
            } else {
                $sites .= ",'" . $val['sitecode'] . "'";
            }
        }

        $sqlCount = " SELECT sitecode, SUM(AllPatient)as AllPatient, SUM(IF(gid=:gidnp,nPatient,0)) as nPatient, SUM(IF(gid=:gidnt,nTime,0)) as nTime 
                FROM rpt_patient_count  WHERE sitecode IN($sites) GROUP BY sitecode ORDER BY sitecode";
        $resCount = \Yii::$app->db->createCommand($sqlCount, [':gidnp' => $module_id, ':gidnt' => $module_id])->queryAll();

        $sqlUser = " SELECT sitecode, SUM(nUser)as users 
                FROM rpt_user_count  WHERE sitecode IN($sites) GROUP BY sitecode ORDER BY sitecode";
        $resUser = \Yii::$app->db->createCommand($sqlUser)->queryAll();
        //\appxq\sdii\utils\VarDumper::dump($resUser);
        $x = 0;
        $resTarget = null;
        $resMain = null;
        $target_table = '';
        $main_table = '';
        $x = 0;
        foreach ($resSite as $val) {

            $dataProvider[$x]['sitecode'] = $val['sitecode'];
            $dataProvider[$x]['site_name'] = $val['name'];

            foreach ($resCount as $valC) {
                if ($val['sitecode'] == $valC['sitecode']) {
                    $dataProvider[$x]['form_all'] = $valC['AllPatient'];
                    $dataProvider[$x]['form_patient'] = $valC['nPatient'];
                    $dataProvider[$x]['form_amt'] = $valC['nTime'];
                }
            }

            foreach ($resUser as $valU) {
                if ($val['sitecode'] == $valU['sitecode']) {
                    $dataProvider[$x]['form_user'] = $valU['users'];
                }
            }
            $x++;

            $dataProvider = \appxq\sdii\utils\SortArray::sortingArrayDESC($dataProvider, 'form_all');
        }
        return $dataProvider;
    }

    public function actionModulesPart3() {

        return $this->renderAjax('/report-cocs/_modules_part3');
    }

    public function getSiteName($sitecode) {
        $sqlSite = " SELECT `name` FROM all_hospital_thai WHERE hcode=:sitecode";
        $resSite = \Yii::$app->db->createCommand($sqlSite, [':sitecode' => $sitecode])->queryOne();

        return $resSite['name'];
    }

}
