<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace frontend\controllers;

use backend\models\EzformTarget;
use backend\models\QueryManager;
use backend\models\QueryRequest;
use backend\modules\ckdnet\classes\CkdnetFunc;
use backend\modules\component\models\EzformComponent;
use backend\modules\ezforms\components\EzformFunc;
use backend\modules\ezforms\components\EzformQuery;
use backend\modules\ezforms\models\Ezform;
use backend\modules\ezforms\models\EzformChoice;
use backend\modules\ezforms\models\EzformDynamic;
use backend\modules\ezforms\models\EzformReply;
use backend\modules\ovcca\classes\OvccaFunc;
use common\lib\codeerror\helpers\GenMillisecTime;
use yii\data\ActiveDataProvider;
use yii\db\Exception;
use yii\db\Expression;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Request;
use Yii;
use backend\modules\ezforms\models\EzformFields;
use yii\web\Response;
use yii\web\UploadedFile;
use backend\modules\inv\models\TbdataAll;

/**
 * Description of TestController
 *
 * @author Gundam-macbook
 */
class InputdataController extends Controller
{
    public function beforeAction($action)
    {
        if($_POST['onmobile']) {
            $this->enableCsrfValidation = false;
        }

        if (parent::beforeAction($action)) {
            return true;
        } else {
            return false;
        }
    }

    //put your code here
        public static function actionInsertRecord($params=null, $redirect=true){

        /*post
        ezf_id
        target
        comp_id_target
        */

        //session rurl
        if($_REQUEST['rurl']){
            $session = Yii::$app->session;
            $session->set('input_rurl', $_REQUEST['rurl']);
        }

        $ezf_id = $_REQUEST['ezf_id'];
        $target = $_REQUEST['target'];
        $comp_id_target = $_REQUEST['comp_id_target'];
        $chk_json = $_REQUEST['json'];
        $purl = $_REQUEST['purl'];
        $xsourcex = Yii::$app->user->identity->userProfile->sitecode;//hospitalcode
        $xdepartmentx = Yii::$app->user->identity->userProfile->department_area;//xdepartmentx

        //is set params
        if($params){
            $ezf_id = $params['ezf_id'];
            $target = $params['target'];
            $comp_id_target = $params['comp_id_target'];
            $xsourcex = $params['xsourcex'];
            $chk_json = $params['json'];
        }

        $haveTarget = ($target == 'all' || $target == 'byme' || $target == 'skip') ? false : true;

        //reset record
        if(Yii::$app->request->get('dataid')){
            $dataid = Yii::$app->request->get('dataid');

            $modelform = Ezform::findOne($ezf_id);
            Yii::$app->db->createCommand("DELETE FROM `".($modelform->ezf_table)."` WHERE id = :data_id;", [':data_id'=>$dataid])->execute();
            Yii::$app->db->createCommand("DELETE FROM `ezform_target` WHERE `data_id` = :data_id;", [':data_id'=>$dataid])->execute();
            Yii::$app->db->createCommand("DELETE FROM `ezform_data_relation` WHERE `target_ezf_data_id` = :data_id;", [':data_id'=>$dataid])->execute();
            Yii::$app->db->createCommand("DELETE FROM `ezform_reply` WHERE `data_id` = :data_id;", [':data_id'=>$dataid])->execute();
            Yii::$app->db->createCommand("DELETE FROM `query_request` WHERE `data_id` = :data_id;", [':data_id'=>$dataid])->execute();
            Yii::$app->db->createCommand("DELETE FROM `ezform_data_log` WHERE `dataid` = :data_id;", [':data_id'=>$dataid])->execute();

            //กรณี dpmcloud ลบข้อมูล ให้ลบฝั่ง cascap ออกด้วย
            if(Yii::$app->keyStorage->get('frontend.domain') == 'dpmcloud.org'){
                $res = Yii::$app->db->createCommand("SELECT * FROM ezform_sync WHERE ezf_local = :ezf_id", [':ezf_id'=>$_POST['ezf_id']])->queryOne();
                //ดึงข้อมูลที่ Remote server
                $ezform = Yii::$app->dbcascaputf8->createCommand("SELECT ezf_table FROM `ezform` WHERE  ezf_id = :ezf_id", [':ezf_id'=>$res['ezf_remote']])->queryOne();
                if($ezform['ezf_id']){
                    //ถ้าพบข้อมูลให้ลบข้อมูลฝั่งนั้นออก
                    Yii::$app->dbcascaputf8->createCommand("DELETE FROM `".($ezform['ezf_table'])."` WHERE id = :data_id;", [':data_id'=>$dataid])->execute();
                    Yii::$app->dbcascaputf8->createCommand("DELETE FROM `ezform_target` WHERE `data_id` = :data_id;", [':data_id'=>$dataid])->execute();
                    Yii::$app->dbcascaputf8->createCommand("DELETE FROM `ezform_data_relation` WHERE `target_ezf_data_id` = :data_id;", [':data_id'=>$dataid])->execute();
                    Yii::$app->dbcascaputf8->createCommand("DELETE FROM `ezform_reply` WHERE `data_id` = :data_id;", [':data_id'=>$dataid])->execute();
                    Yii::$app->dbcascaputf8->createCommand("DELETE FROM `query_request` WHERE `data_id` = :data_id;", [':data_id'=>$dataid])->execute();
                    Yii::$app->dbcascaputf8->createCommand("DELETE FROM `ezform_data_log` WHERE `dataid` = :data_id;", [':data_id'=>$dataid])->execute();
                }
            }

        }else {
            $dataid = GenMillisecTime::getMillisecTime();
        }
        //VarDumper::dump($_POST, 10, true);
        //Yii::$app->end();

        $model_fields = EzformQuery::getFieldsByEzf_id($ezf_id);
        $model_form = EzformFunc::setDynamicModelLimit($model_fields);
        $table_name = EzformQuery::getFormTableName($ezf_id);

        $model = new \backend\modules\ezforms\models\EzformDynamic($table_name->ezf_table);

        //check unique_record
        if($table_name->unique_record==2){
            $model_chk = $model->find()->select('id')->where('ptid = :target AND rstat <>3', [':target' =>base64_decode($target)])->one();
            if($model_chk->id){
                if($chk_json){
                    header('Access-Control-Allow-Origin: *');
                    header("content-type:text/javascript;charset=utf-8");
                    echo json_encode(['dataid'=>$model_chk->id]);
                    return ;
                }
                else if($redirect) {
                    //page url
                    if ($purl) {
                        $url = base64_decode($purl)."&comp_id_target=".$comp_id_target."&ezf_id=".$ezf_id."&target=".$target."&dataid=".$model_chk->id;
                    }else{
                        $url = Url::to(['step4', 'comp_id_target' => $comp_id_target, 'ezf_id' => $ezf_id, 'target' => $target, 'dataid' => $model_chk->id,]);
                    }
                    return Yii::$app->getResponse()->redirect($url);
                }else{
                    return $model_chk->id;
                }
            }
        }

        //check new record
        if($haveTarget)
            $model_chk = $model->find()->select('id')->where('(ptid = :target OR target = :target) AND xsourcex = :xsourcex AND rstat =0 AND user_create=:user', [':target' =>base64_decode($target), ':xsourcex' => $xsourcex, ':user'=>Yii::$app->user->id])->one();
        else
            $model_chk = $model->find()->select('id')->where('xsourcex = :xsourcex AND rstat =0 AND user_create=:user', [':xsourcex' => $xsourcex, ':user'=>Yii::$app->user->id])->one();

        if($model_chk->id){
            if($chk_json){
                header('Access-Control-Allow-Origin: *');
                header("content-type:text/javascript;charset=utf-8");
                echo json_encode(['dataid'=>$model_chk->id]);
                return ;
            }
            else if($redirect) {
                //page url
                if ($purl) {
                    $url = base64_decode($purl)."&comp_id_target=".$comp_id_target."&ezf_id=".$ezf_id."&target=".$target."&dataid=".$model_chk->id;
                }else{
                    $url = Url::to(['step4', 'comp_id_target' => $comp_id_target, 'ezf_id' => $ezf_id, 'target' => $target, 'dataid' => $model_chk->id,]);
                }
                return Yii::$app->getResponse()->redirect($url);
            }else{
                return $model_chk->id;
            }
        }


        //replace data set
        if($_REQUEST['dataset']) {
            $dataSet = Json::decode(base64_decode($_REQUEST['dataset']));
            foreach ($dataSet as $key=>$value){
                $model_form->{$key} = $value;
            }
        }
        $model->attributes = $model_form->attributes;


        $model->id = $dataid;
        $model->rstat = 0;
        $model->user_create = Yii::$app->user->id;
        $model->create_date = new Expression('NOW()');
        $model->user_update = Yii::$app->user->id;
        $model->update_date = new Expression('NOW()');
        $model->xsourcex = $xsourcex;//hospitalcode
        $model->xdepartmentx = $xdepartmentx;

        $ezform = EzformQuery::checkIsTableComponent($ezf_id);
        // ตาราง Register ของ  Component
        if($ezform['ezf_id'] == $ezf_id){
            //save target
            $model->target = $dataid;
            $target = $dataid;
        }
        // ตาราง ทั่วไป ของ special Component
        else if ($ezform['special']) {
            //save target
            $target = base64_decode($target);
            $ezform = EzformQuery::getTablenameFromComponent($comp_id_target);
            $targetx = EzformQuery::getTargetFromtable($ezform->ezf_table, $target, $xsourcex);
            $model->target = $targetx['id'];
            //save ptid

            $targetx = EzformQuery::getIDkeyFromtable($ezform->ezf_table, $target);
            $model->ptid = $targetx['ptid'];

            //save sitecode, ptcode. ptcodefull
            $ptcodefull = $targetx['sitecode'].$targetx['ptcode'];
            $model->sitecode = $targetx['sitecode'];
            $model->ptcode = $targetx['ptcode'];

            $res = Yii::$app->db->createCommand("select hsitecode, hptcode from `".($ezform->ezf_table)."` where ptid = :target AND hptcode <> '' AND hsitecode <> '' AND xsourcex = :xsourcex AND rstat <>3 order by create_date DESC", [':target' => $targetx['ptid'], ':xsourcex' => $xsourcex])->queryOne();
            $model->hsitecode = $res['hsitecode'];
            $model->hptcode = $res['hptcode'];
            $model->ptcodefull = $ptcodefull;
            //VarDumper::dump($targetx, 10, true);

        } else if($ezform['comp_id']) {
            $target = base64_decode($target);
            $model->target = $target;
        }

        foreach ($model_fields as $key => $value) {

            if ($value['ezf_field_type'] == 7 || $value['ezf_field_type'] == 253) {

                //$model->{$value['ezf_field_name']} = date('Y-m-d');

            }
            else if ($value['ezf_field_type'] == 9) {

                //$model->{$value['ezf_field_name']} = date('Y-m-d H:i:s');

            }
            else if ($value['ezf_field_type'] == 18) {
                //EzformQuery::saveReferenceFields($ezf_field_ref_field, $ezf_field_ref_table, $target, $ezf_id, $ezf_field_id, $dataid);
                EzformQuery::saveReferenceFields($value['ezf_field_ref_field'], $value['ezf_field_ref_table'], $target, $value['ezf_id'], $value['ezf_field_id'], $dataid);
            }

        }

        if ($model->save()) {
            //save EMR
            $model = new EzformTarget();
            $model->ezf_id = $ezf_id;
            $model->data_id = $dataid;
            $model->target_id = $target;
            isset($_POST['comp_id_target']) ? $model->comp_id = $_POST['comp_id_target'] : '';
            $model->user_create = Yii::$app->user->id;
            $model->create_date = new Expression('NOW()');
            $model->user_update = Yii::$app->user->id;
            $model->update_date = new Expression('NOW()');
            $model->rstat = 0;
            $model->xsourcex = $xsourcex;
            $model->save();
            //
        }

        //cascap reference fields CCA01
        if($ezf_id =='1437377239070461302') {
            $res = Yii::$app->db->createCommand("SELECT v2 as birthd, v3 as sex, add1n8code, add1n7code, add1n6code FROM tb_data_1 WHERE ptid = :ptid;", [':ptid'=>$target])->queryOne();
            Yii::$app->db->createCommand("UPDATE tb_data_2 SET f1v1a1 = :f1v1a1, f1v1a2 = :f1v1a2, f1v1a3 = :f1v1a3, f1v2 = :f1v2, f1v3 = :f1v3 WHERE id = :id;", [
                ':id' => $dataid,
                ':f1v1a1' => $res['add1n8code'],
                ':f1v1a2' => $res['add1n7code'],
                ':f1v1a3' => $res['add1n6code'],
                ':f1v2' => $res['birthd'],
                ':f1v3' => $res['sex'],
            ])->query();
        }

        //Palliative reference fields Refer
        if($ezf_id =='1450928555015607100') {
            $res = Yii::$app->db->createCommand("SELECT var27_province, var27_amphur, var27_tumbon FROM  tbdata_1 WHERE ptid = '".$target."' AND xsourcex = '".$xsourcex."';")->queryOne();
            Yii::$app->db->createCommand("UPDATE tbdata_4 SET refer_province = :val_province, refer_amphur = :val_amphur, refer_tumbon = :val_tumbon WHERE id = :id;", [
                ':id' => $dataid,
                ':val_province' => $res['var27_province'],
                ':val_amphur' => $res['var27_amphur'],
                ':val_tumbon' => $res['var27_tumbon'],
            ])->query();
        }

        //save next record
        if(Yii::$app->request->post('addnext')){
            $dataid_before = Yii::$app->request->post('addnext');
            $fields = EzformFields::find()->select('ezf_field_name, ezf_field_options')->where('ezf_id = :ezf_id', [':ezf_id' => $ezf_id])->all();
            $listField ='';
            $listFieldUpdate = '';
            foreach($fields as $field){
                $json = json_decode($field->ezf_field_options, true);
                if($json['remember_field']){
                    $listField .= $field->ezf_field_name.', ';
                    $listFieldUpdate .= $field->ezf_field_name.' = :'.$field->ezf_field_name.', ';
                }
            }

            $listField = substr($listField, 0, -2);
            $listFieldUpdate = substr($listFieldUpdate, 0, -2);
            if($listField) {
                $res = Yii::$app->db->createCommand("SELECT " . $listField . " FROM " . $table_name->ezf_table . " WHERE id = '" . $dataid_before . "';")->queryOne();
                Yii::$app->db->createCommand("UPDATE " . $table_name->ezf_table . " SET " . $listFieldUpdate . " WHERE id = '$dataid';", $res)->query();
            }
        }



        if($chk_json){
            header('Access-Control-Allow-Origin: *');
            header("content-type:text/javascript;charset=utf-8");
            echo json_encode(['dataid'=>$dataid]);
            return ;
        }
        else if($redirect) {
            //page url
            if ($purl) {
                $url = base64_decode($purl)."&comp_id_target=".$comp_id_target."&ezf_id=".$ezf_id."&target=".$haveTarget ? base64_encode($target) : $target."&dataid=".$model_chk->id;
            }else{
                $url = Url::to(['step4', 'comp_id_target' => $comp_id_target, 'ezf_id' => $ezf_id, 'target' => $haveTarget ? base64_encode($target) : $target, 'dataid' => $dataid,]);
            }
            return Yii::$app->getResponse()->redirect($url);
        }else{
            return $dataid;
        }


    }
 
}
