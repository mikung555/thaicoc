<?php
namespace frontend\controllers;

use backend\modules\ezforms\components\EzformQuery;
use backend\modules\ezforms\models\EzformFields;
use Yii;
use backend\models\Ezform;
use yii\data\ActiveDataProvider;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;

class SurgeryFormController extends \yii\web\Controller
{
    public function actionIndex()
    {

    }
    public function actionDataManagement($ezfdata){

        $ezfdata = Yii::$app->db->createCommand("SELECT * FROM ezform_data_manage WHERE id = :id;", [':id'=>$ezfdata])->queryOne();
        $searchModel = new \backend\models\search\DataManagementSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $ezfdata);
        $fParams = explode(',', $ezfdata['params']);

        //check component ezfrom
        $ezform = Yii::$app->db->createCommand("SELECT comp_id_target FROM ezform WHERE ezf_id = :ezf_id;", [':ezf_id'=>$ezfdata['ezf_id']])->queryOne();

        //map choice type
        foreach ($fParams as $field){
            //set value
            $ezchoice = [];

            $ezfield = Yii::$app->db->createCommand("select ezf_field_id, ezf_field_sub_id,  ezf_field_name, ezf_field_type, ezf_component from ezform_fields where ezf_id=:ezf_id and ezf_field_name=:ezf_field_name", [':ezf_id'=>$ezfdata['ezf_id'], ':ezf_field_name' => $field])->queryOne();
            if($ezfield['ezf_field_type']==4 || $ezfield['ezf_field_type']==6 ){
                $ezchoice = Yii::$app->db->createCommand("select ezf_choicevalue as value_id, ezf_choicelabel as text from ezform_choice where ezf_field_id=:ezf_field_id", [':ezf_field_id'=>$ezfield['ezf_field_id']])->queryAll();
            }
            else if($ezfield['ezf_field_type']==10){
                $ezformComponents = EzformQuery::getFieldComponent($ezfield['ezf_component']);
                $ezformComp = EzformQuery::getFormTableName($ezformComponents->ezf_id);
                $field_desc_array = explode(',', $ezformComponents->field_id_desc);
                $field_key = EzformFields::find()->select('ezf_field_name')->where(['ezf_field_id' => $ezformComponents->field_id_key])->one();

                $field_desc_list = '';
                if(count($field_desc_array)){
                    foreach ($field_desc_array as $value) {
                        if (!empty($value)) {
                            $field_desc = EzformFields::find()->where(['ezf_field_id' => $value])->one();
                            $field_desc_list .= 'COALESCE('.$field_desc->ezf_field_name.", '-'), ' ',";
                        }
                    }
                }
                $field_desc_list = substr($field_desc_list, 0 ,-6);

                //set SQL
                $sql = 'SELECT '.$field_key->ezf_field_name.' as value_id, CONCAT('.$field_desc_list.') AS text FROM '.($ezformComp->ezf_table).' WHERE 1 ';
                if($ezfield['ezf_component'] == 100000 || $ezfield['ezf_component'] == 100001){
                    $sql .=  " AND sitecode = '" . Yii::$app->user->identity->userProfile->sitecode . "'";
                }

                //if find by target
                if($ezformComponents->find_target AND $ezformComponents->special){
                    $sql .= " AND ptid = '".base64_decode($_GET['target'])."' AND rstat <> 3";
                }else if($ezformComponents->find_target){
                    $sql .= " AND target = '".base64_decode($_GET['target'])."' AND rstat <> 3";
                }

                //if find by site
                if($ezformComponents->find_bysite){
                    $sql .= " AND xsourcex = '".Yii::$app->user->identity->userProfile->sitecode."'";
                }
                $ezchoice = Yii::$app->db->createCommand($sql)->queryAll();

            }
            else if($ezfield['ezf_field_type']==231){
                $ezchoice = Yii::$app->db->createCommand("select ezf_choicevalue as value_id, ezf_choicelabel as text from ezform_choice where ezf_field_id=:ezf_field_id", [':ezf_field_id'=>$ezfield['ezf_field_sub_id']])->queryAll();
            }
            else if($ezfield['ezf_field_type']==16 || ($ezfield['ezf_field_type']==0 && $ezfield['ezf_field_sub_id'])){
                $ezchoice = [
                    '0' => [
                        'value_id' => '1',
                        'text' => 'ใช่'
                    ],
                    '1' => [
                        'value_id' => '0',
                        'text' => 'ไม่ใช่'
                    ],
                ];
            }
            $filterType[$field] = ArrayHelper::map($ezchoice, 'value_id', 'text');
        }

        //VarDumper::dump($fParams,10,true); exit;
        return $this->render('@frontend/views/surgery-form/data-management',[
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'fParams' =>$fParams,
            'ezfdata' =>$ezfdata,
            'ezform' => $ezform,
            'filterType' => $filterType
        ]);
    }
}
