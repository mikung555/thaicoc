<?php

namespace frontend\controllers;

use frontend\models\Project84ReportData;
use frontend\models\Project84ReportSum;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use frontend\models\Project84SumSearch;
use Yii;
use appxq\sdii\helpers\SDHtml;
use yii\helpers\VarDumper;
use yii\web\Response;
use yii\web\NotFoundHttpException;
use frontend\modules\project84\controllers\DrillDownSec3Controller;

class ReportController extends \yii\web\Controller {

    public function actionIndex() {

        $tab = Yii::$app->request->get('tab');
        if (Yii::$app->keyStorage->get('frontend.domain') == 'cascap.in.th' || Yii::$app->keyStorage->get('frontend.domain') == 'yii2-starter-kit.dev') {
            if ($tab == "")
                return $this->redirect('/project84/report');
        } else if (Yii::$app->keyStorage->get('frontend.domain') == 'thaicarecloud.org') {
            if ($tab == "") {
                
            }
            return $this->redirect(['/report-cocs']);
        } else {
            if ($tab == "")
                $tab = "main";
        }

        $debug = false;
        //$debug = true;

        if ($debug || Yii::$app->keyStorage->get('frontend.domain') == 'cascap.in.th' || Yii::$app->keyStorage->get('frontend.domain') == 'yii2-starter-kit.dev' || Yii::$app->keyStorage->get('frontend.domain') == 'dpmcloud.dev') {
            $searchModel = new Project84SumSearch();
            if (isset(Yii::$app->session['text'])) {
                $searchModel->zone = Yii::$app->session['text'];
            }
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            $model = new \frontend\models\Project84Sum();
            $report84 = $this->renderAjax('project84', ['searchModel' => $searchModel,
                'dataProvider' => $dataProvider, 'model' => $model]);
            $overview = $this->renderPartial('overview');
            $gismap = $this->renderPartial('gismap');
            $ultrasound = $this->renderPartial('ultrasound');
            $summaryshow = ReportController::actionSummary();
            $summary = $this->renderPartial('summary',['summaryshow'=>$summaryshow ]);

            $checkadmin = DrillDownSec3Controller::CheckAdmin();
            if ($checkadmin == "yes") {
                $menu = [
                    [
                        'label' => '<i class="fa fa-table fa-lg"></i> ถวายเป็นพระราชกุศล',
                        'content' => $report84,
                        //'linkOptions'=>['data-url'=>\yii\helpers\Url::to(['/report/project84'])],
                        'active' => ($tab == 'main' ? true : false)
                    ],
                    [
                        'label' => '<i class="fa fa-table fa-lg"></i> ถวายเป็นพระราชกุศลปีงบประมาณ 2560',
                        'url' => \yii\helpers\Url::to(['/project84/report']),
                        //'linkOptions'=>['data-url'=>\yii\helpers\Url::to(['/project84/report'])],
                        'active' => ($tab == 'project84' ? true : false)
                    ],
                    [
                        'label' => '<i class="fa fa-line-chart fa-lg"></i> Overview',
                        'content' => $overview,
                        //'linkOptions'=>['data-url'=>\yii\helpers\Url::to(['/report/overview'])],
                        'active' => ($tab == 'overview' ? true : false)
                    ],
                    [
                        'label' => '<i class="fa fa-desktop fa-lg"></i> Fluke Free Thailand',
                        'url' => Yii::getAlias('@backendUrl') . "/tccbots/monitor-report"
                    ],
                    [
                        'label' => '<i class="fa fa-globe fa-lg"></i> GIS Map',
                        'content' => $gismap,
                        //'linkOptions' => ['data-url' => \yii\helpers\Url::to(['/report/gismap'])],
                        'active' => ($tab == 'gismap' ? true : false)
                    ],
                    [
                        'label' => '<i class="fa fa-laptop fa-lg"></i> Ultrasound',
                        'content' => $ultrasound,
                        //'linkOptions' => ['data-url' => \yii\helpers\Url::to(['/report/ultrasound'])],
                        'active' => ($tab == 'ultrasound' ? true : false)
                    ],
                    [
                        
                        'label' => '<i class="fa fa-list-alt fa-lg"></i> Summary',
                        'content' => $summary,
                        //'linkOptions' => ['data-url' => \yii\helpers\Url::to(['/report/summary'])],
                        'active' => ($tab == 'summary' ? true : false)
                    ]
                ];
            } else {
                $menu = [
                    [
                        'label' => '<i class="fa fa-table fa-lg"></i> ถวายเป็นพระราชกุศล',
                        'content' => $report84,
                        //'linkOptions'=>['data-url'=>\yii\helpers\Url::to(['/report/project84'])],
                        'active' => ($tab == 'main' ? true : false)
                    ],
                    [
                        'label' => '<i class="fa fa-table fa-lg"></i> ถวายเป็นพระราชกุศลปีงบประมาณ 2560',
                        'url' => \yii\helpers\Url::to(['/project84/report']),
                        //'linkOptions' => ['data-url' => \yii\helpers\Url::to(['/project84/report'])],
                        'active' => ($tab == 'project84' ? true : false)
                    ],
                    [
                        'label' => '<i class="fa fa-line-chart fa-lg"></i> Overview',
                        'content' => $overview,
                        //'linkOptions'=>['data-url'=>\yii\helpers\Url::to(['/report/overview'])],
                        'active' => ($tab == 'overview' ? true : false)
                    ],
                    [
                        'label' => '<i class="fa fa-desktop fa-lg"></i> Fluke Free Thailand',
                        'url' => Yii::getAlias('@backendUrl') . "/tccbots/monitor-report"
                    ],
                    [
                        'label' => '<i class="fa fa-globe fa-lg"></i> GIS Map',
                        'content' => $gismap,
                        //'linkOptions' => ['data-url' => \yii\helpers\Url::to(['/report/gismap'])],
                        'active' => ($tab == 'gismap' ? true : false)
                    ],
                    [
                        'label' => '<i class="fa fa-laptop fa-lg"></i> Ultrasound',
                        'content' => $ultrasound,
                        //'linkOptions' => ['data-url' => \yii\helpers\Url::to(['/report/ultrasound'])],
                        'active' => ($tab == 'ultrasound' ? true : false)
                    ],
                    [
                       
                        'label' => '<i class="fa fa-list-alt fa-lg"></i> Summary',
                        'content' => $summary,
                        //'linkOptions' => ['data-url' => \yii\helpers\Url::to(['/report/summary'])],
                        'active' => ($tab == 'summary' ? true : false)
                    ]
                ];
            }
            return $this->render('index', [
                        'menuitems' => $menu
            ]);
        } else {
            $dataAll = array();

            array_push($dataAll, [
                'section', ['จำนวนหน่วยงาน (หน่วย)', 'ทั้งหมด', 'เป็นสมาชิก (%)', 'ปีนี้', 'เดือนนี้', 'สัปดาห์นี้', 'วันนี้']
                    ]
            );

            $sqlDeptMember = "select code4,count(*) nall from  all_hospital_thai h  where h.code2 not like '%demo%'  group by code4 order by code4+0;";
            $qryDeptMember = Yii::$app->db->createCommand($sqlDeptMember)->queryAll();

            foreach ($qryDeptMember as $membercount) {
                if (in_array($membercount['code4'], array("10"))) {
                    $orgname = "หน่วยงานส่วนกลาง";
                } else if (in_array($membercount['code4'], array("1"))) {
                    $orgname = "สำนักงานสาธารณสุขจังหวัด";
                } else if (in_array($membercount['code4'], array("2"))) {
                    $orgname = "สำนักงานสาธารณสุขอำเภอ";
                } else if (in_array($membercount['code4'], array("5"))) {
                    $orgname = "รพศ.";
                } else if (in_array($membercount['code4'], array("6"))) {
                    $orgname = "รพท.";
                } else if (in_array($membercount['code4'], array("7"))) {
                    $orgname = "รพช. รพร.";
                } else if (in_array($membercount['code4'], array("18", "3", "4", "8", "13", "17"))) {
                    $orgname = "รพ.สต. สถานีอนามัย";
                } else if (in_array($membercount['code4'], array("11", "12"))) {
                    $orgname = "โรงพยาบาล นอก สธ.";
                } else if (in_array($membercount['code4'], array("15", "16"))) {
                    $orgname = "โรงพยาบาลเอกชน และคลินิกเอกชน";
                } else {
                    $orgname = "อื่นๆ";
                }

                $dataAllH[$orgname]['All'] += $membercount['nall'];
                $dataAllH['All']['All'] += $membercount['nall'];
                $countp[$orgname]['HAll'] += $membercount['nall'];
            }

            $sqlDeptMember = "select code4,COUNT(distinct sitecode) as nall,count(distinct IF(YEAR(FROM_UNIXTIME(created_at))=YEAR(CURDATE()),sitecode,null)) as thisYear,count(distinct IF(YEAR(FROM_UNIXTIME(created_at))=YEAR(CURDATE()) AND MONTH(FROM_UNIXTIME(created_at))=MONTH(CURDATE()),sitecode,null)) as thisMonth,count(distinct IF(YEARWEEK(FROM_UNIXTIME(`created_at`)) = YEARWEEK(NOW()),sitecode,null)) as thisWeek,count(distinct IF(DATE(FROM_UNIXTIME(created_at))=CURDATE(),sitecode,null)) as today  from user u inner join user_profile p ON u.id=p.user_id inner join all_hospital_thai h on p.sitecode=h.hcode where h.code2 not like '%demo%' and status_del=0 group by code4 order by code4+0;";
            $qryDeptMember = Yii::$app->db->createCommand($sqlDeptMember)->queryAll();

            foreach ($qryDeptMember as $membercount) {
                if (in_array($membercount['code4'], array("10"))) {
                    $orgname = "หน่วยงานส่วนกลาง";
                } else if (in_array($membercount['code4'], array("1"))) {
                    $orgname = "สำนักงานสาธารณสุขจังหวัด";
                } else if (in_array($membercount['code4'], array("2"))) {
                    $orgname = "สำนักงานสาธารณสุขอำเภอ";
                } else if (in_array($membercount['code4'], array("5"))) {
                    $orgname = "รพศ.";
                } else if (in_array($membercount['code4'], array("6"))) {
                    $orgname = "รพท.";
                } else if (in_array($membercount['code4'], array("7"))) {
                    $orgname = "รพช. รพร.";
                } else if (in_array($membercount['code4'], array("18", "3", "4", "8", "13", "17"))) {
                    $orgname = "รพ.สต. สถานีอนามัย";
                } else if (in_array($membercount['code4'], array("11", "12"))) {
                    $orgname = "โรงพยาบาล นอก สธ.";
                } else if (in_array($membercount['code4'], array("15", "16"))) {
                    $orgname = "โรงพยาบาลเอกชน และคลินิกเอกชน";
                } else {
                    $orgname = "อื่นๆ";
                }

                $dataAllP[$orgname]['All'] += $membercount['nall'];
                $dataAllP['All']['All'] += $membercount['nall'];

                $countp[$orgname]['All'] += $membercount['nall'];
                $countp[$orgname]['Year'] += $membercount['thisYear'];
                $countp[$orgname]['Month'] += $membercount['thisMonth'];
                $countp[$orgname]['Week'] += $membercount['thisWeek'];
                $countp[$orgname]['Day'] += $membercount['today'];
            }

            foreach ($countp as $orgname => $data) {
                $countAllH = $data['HAll'];
                $countAllP = $data['All'] . " (" . number_format($data['All'] / $countAllH * 100, 1) . "%)";
                $countYearP = $data['Year'];
                $countMonthP = $data['Month'];
                $countYearWeekRegisP = $data['Week'];
                $countDayP = $data['Day'];

                $sumAllH += $countAllH;
                $sumAllP += $countAllP;
                $sumYearP += $countYearP;
                $sumMonthP += $countMonthP;
                $sumYearWeekRegisP += $countYearWeekRegisP;
                $sumDayP += $countDayP;


                array_push($dataAll, [
                    'name', [$orgname, $countAllH, $countAllP, $countYearP, $countMonthP, $countYearWeekRegisP, $countDayP]
                ]);
            }

            array_push($dataAll, [
                'name', ['รวมทั้งหมด', $sumAllH, $sumAllP . " (" . number_format($sumAllP / $sumAllH * 100, 1) . "%)", $sumYearP, $sumMonthP, $sumYearWeekRegisP, $sumDayP]
                    ]
            );

            array_push($dataAll, [
                'section', ['จำนวนสมาชิก บุคลากร (คน)', '', 'ทั้งหมด', 'ปีนี้', 'เดือนนี้', 'สัปดาห์นี้', 'วันนี้']
                    ]
            );
            $sqlDeptMember = "select code4,COUNT(*) as nall,SUM(YEAR(FROM_UNIXTIME(created_at))=YEAR(CURDATE())) as thisYear,SUM(YEAR(FROM_UNIXTIME(created_at))=YEAR(CURDATE()) AND MONTH(FROM_UNIXTIME(created_at))=MONTH(CURDATE())) as thisMonth,SUM(YEARWEEK(FROM_UNIXTIME(`created_at`)) = YEARWEEK(NOW())) as thisWeek,SUM(DATE(FROM_UNIXTIME(created_at))=CURDATE()) as today  from user u inner join user_profile p ON u.id=p.user_id inner join all_hospital_thai h on p.sitecode=h.hcode where h.code2 not like '%demo%' and status_del=0 and volunteer_status=0 group by code4 order by code4+0;";
            $qryDeptMember = Yii::$app->db->createCommand($sqlDeptMember)->queryAll();

            foreach ($qryDeptMember as $membercount) {
                if (in_array($membercount['code4'], array("10"))) {
                    $orgname = "หน่วยงานส่วนกลาง";
                } else if (in_array($membercount['code4'], array("1"))) {
                    $orgname = "สำนักงานสาธารณสุขจังหวัด";
                } else if (in_array($membercount['code4'], array("2"))) {
                    $orgname = "สำนักงานสาธารณสุขอำเภอ";
                } else if (in_array($membercount['code4'], array("5"))) {
                    $orgname = "รพศ.";
                } else if (in_array($membercount['code4'], array("6"))) {
                    $orgname = "รพท.";
                } else if (in_array($membercount['code4'], array("7"))) {
                    $orgname = "รพช. รพร.";
                } else if (in_array($membercount['code4'], array("18", "3", "4", "8", "13", "17"))) {
                    $orgname = "รพ.สต. สถานีอนามัย";
                } else if (in_array($membercount['code4'], array("11", "12"))) {
                    $orgname = "โรงพยาบาล นอก สธ.";
                } else if (in_array($membercount['code4'], array("15", "16"))) {
                    $orgname = "โรงพยาบาลเอกชน และคลินิกเอกชน";
                } else {
                    $orgname = "อื่นๆ";
                }

                $count[$orgname]['All'] += $membercount['nall'];
                $count[$orgname]['Year'] += $membercount['thisYear'];
                $count[$orgname]['Month'] += $membercount['thisMonth'];
                $count[$orgname]['Week'] += $membercount['thisWeek'];
                $count[$orgname]['Day'] += $membercount['today'];
            }

            foreach ($count as $orgname => $data) {
                $countAll = $data['All'];
                $countYear = $data['Year'];
                $countMonth = $data['Month'];
                $countYearWeekRegis = $data['Week'];
                $countDay = $data['Day'];

                $sumAll += $countAll;
                $sumYear += $countYear;
                $sumMonth += $countMonth;
                $sumYearWeekRegis += $countYearWeekRegis;
                $sumDay += $countDay;


                array_push($dataAll, [
                    'name', [$orgname, '', $countAll, $countYear, $countMonth, $countYearWeekRegis, $countDay]
                ]);
            }
            array_push($dataAll, [
                'name', ['รวมทั้งหมด', '', $sumAll, $sumYear, $sumMonth, $sumYearWeekRegis, $sumDay]
                    ]
            );
            array_push($dataAll, [
                'section', ['จำนวนสมาชิก อาสาสมัคร (คน)', '', 'ทั้งหมด', 'ปีนี้', 'เดือนนี้', 'สัปดาห์นี้', 'วันนี้']
                    ]
            );
            $sqlVolMember = "select province,COUNT(*) as nall,SUM(YEAR(FROM_UNIXTIME(created_at))=YEAR(CURDATE())) as thisYear,SUM(YEAR(FROM_UNIXTIME(created_at))=YEAR(CURDATE()) AND MONTH(FROM_UNIXTIME(created_at))=MONTH(CURDATE())) as thisMonth,SUM(YEARWEEK(FROM_UNIXTIME(`created_at`)) = YEARWEEK(NOW())) as thisWeek,SUM(DATE(FROM_UNIXTIME(created_at))=CURDATE()) as today  from user u inner join user_profile p ON u.id=p.user_id inner join all_hospital_thai h on p.sitecode=h.hcode where h.code2 not like '%demo%' and status_del=0 and volunteer_status<>0 group by province order by province;";
            $qryVolMember = Yii::$app->db->createCommand($sqlVolMember)->queryAll();

            foreach ($qryVolMember as $membervcount) {
                $orgname = $membervcount['province'];
                $countv[$orgname]['All'] += $membervcount['nall'];
                $countv[$orgname]['Year'] += $membervcount['thisYear'];
                $countv[$orgname]['Month'] += $membervcount['thisMonth'];
                $countv[$orgname]['Week'] += $membervcount['thisWeek'];
                $countv[$orgname]['Day'] += $membervcount['today'];
            }

            foreach ($countv as $orgname => $data) {
                $countvAll = $data['All'];
                $countvYear = $data['Year'];
                $countvMonth = $data['Month'];
                $countvYearWeekRegis = $data['Week'];
                $countvDay = $data['Day'];

                $sumvAll += $countvAll;
                $sumvYear += $countvYear;
                $sumvMonth += $countvMonth;
                $sumvYearWeekRegis += $countvYearWeekRegis;
                $sumvDay += $countvDay;


                array_push($dataAll, [
                    'name', [$orgname, '', $countvAll, $countvYear, $countvMonth, $countvYearWeekRegis, $countvDay]
                ]);
            }

            array_push($dataAll, [
                'name', ['รวมทั้งหมด', '', $sumvAll, $sumvYear, $sumvMonth, $sumvYearWeekRegis, $sumvDay]
                    ]
            );

            $sql = "SELECT count(*) as nall from person";
            $sql = "SHOW TABLE STATUS LIKE 'person'";
            $data = Yii::$app->dbbot->createCommand($sql)->queryOne();
            $dataAllP['TCC'] = $data['Rows'];
            $sql = "SELECT count(*) as nall from person";
            $sql = "SHOW TABLE STATUS LIKE 'person'";
            $data = Yii::$app->dbnemo->createCommand($sql)->queryOne();
            $dataAllP['NEMO'] = $data['Rows'];

            Yii::$app->response->format = \yii\web\Response::FORMAT_HTML;
            return $this->render('_ajaxReportOverviewOnly', [
                        'dataAll' => $dataAll,
                        'dataAllP' => $dataAllP,
            ]);
        }
    }

    public function actionOverview() {
        $html = $this->renderPartial('overview');
        return Json::encode($html);
    }

    public function actionGismap() {
        $html = $this->renderPartial('gismap');
        return Json::encode($html);
    }

    public function actionLoading() {
        $html = $this->renderPartial('loading');
        return Json::encode($html);
    }

    public function actionMap() {
        if (\Yii::$app->user->isGuest) {
            $html = $this->renderPartial('map');
        } else {
            $html = $this->renderPartial('mapuser');
        }

        return $html;
    }

    public function actionUltrasound() {
        $us = \Yii::$app->db->createCommand("SELECT times,name from v04cascap.history_us_tour")->queryAll();

        $html = $this->renderPartial('ultrasound', ['us' => $us]);
        return Json::encode($html);
    }

    public function actionProject84() {
        $searchModel = new Project84SumSearch();
        if (isset(Yii::$app->session['text'])) {
            $searchModel->zone = Yii::$app->session['text'];
        }
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $model = new \frontend\models\Project84Sum();
        $html = $this->renderAjax('project84', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider, 'model' => $model]);
        return Json::encode($html);
    }

    public function actionFind84() {
        if (Yii::$app->getRequest()->isAjax) {

            //if ($model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $searchModel = new Project84SumSearch();
            $model = new \frontend\models\Project84Sum();
            $model->load(Yii::$app->request->post());
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

            $html = $this->renderAjax('_project84_grid', ['searchModel' => $searchModel,
                'dataProvider' => $dataProvider, 'model' => $model]);
            return $html;
            //}
        } else {
            throw new NotFoundHttpException('Invalid request. Please do not repeat this request again.');
        }
    }

    public function actionGetProvinceByZone() {
        $zones = $_GET['zones'];
        echo '<option>เลือกจังหวัด</option>';
        foreach ($zones as $key => $value) {
            $sqlGetProvince = "SELECT province, LEFT(address,2) as provinceCode FROM `project84_zone` WHERE project84_zone.zone = $value GROUP BY project84_zone.province ORDER BY project84_zone.province ASC";
            $qryGetProvince = Yii::$app->db->createCommand($sqlGetProvince)->queryAll();
            echo '<optgroup label="เขต ' . $value . '">';
            foreach ($qryGetProvince as $keyQry => $valueQry) {
                echo '<option value="' . $valueQry['provinceCode'] . '">' . trim($valueQry['province']) . '</option>';
            }
            echo '</optgroup>';
        }
    }

    public function actionAjaxReportCca02($fiscalYear, $cca02Level, $zone1 = null, $zone6 = null, $zone7 = null, $zone8 = null, $zone9 = null, $zone10 = null, $refresh = false) {
        if (
                (!is_numeric($fiscalYear)) ||
                ($zone1 != null && $zone1 != 1) ||
                ($zone6 != null && $zone6 != 6) ||
                ($zone7 != null && $zone7 != 7) ||
                ($zone8 != null && $zone8 != 8) ||
                ($zone9 != null && $zone9 != 9) ||
                ($zone10 != null && $zone10 != 10)
        )
            return;

        if (1 || Yii::$app->keyStorage->get('frontend.domain') == 'cascap.in.th') {
            if (1 || Yii::$app->getRequest()->isAjax) {

                $thaiMonth = ['',
                    1 => ['abvt' => 'ม.ค.', 'full' => 'มกราคม'],
                    2 => ['abvt' => 'ก.พ.', 'full' => 'กุมภาพันธ์'],
                    3 => ['abvt' => 'มี.ค.', 'full' => 'มีนาคม'],
                    4 => ['abvt' => 'เม.ย.', 'full' => 'เมษายน'],
                    5 => ['abvt' => 'พ.ค.', 'full' => 'พฤษภาคม'],
                    6 => ['abvt' => 'มิ.ย.', 'full' => 'มิถุนายน'],
                    7 => ['abvt' => 'ก.ค.', 'full' => 'กรกฎาคม'],
                    8 => ['abvt' => 'ส.ค.', 'full' => 'สิงหาคม'],
                    9 => ['abvt' => 'ก.ย.', 'full' => 'กันยายน'],
                    10 => ['abvt' => 'ต.ค.', 'full' => 'ตุลาคม'],
                    11 => ['abvt' => 'พ.ย.', 'full' => 'พฤศจิกายน'],
                    12 => ['abvt' => 'ธ.ค.', 'full' => 'ธันวาคม']
                ];

                $fiscalYear = intval($fiscalYear);

                $sqlAddControlTable = 'CREATE TABLE IF NOT EXISTS report_control_cca02(
                    `reportid`  int(5) NOT NULL AUTO_INCREMENT ,
                    `fiscalyear`  varchar(4) NULL ,
                    `lastcal`  datetime NULL ,
                    PRIMARY KEY (`reportid`)
                );';
                \Yii::$app->db->createCommand($sqlAddControlTable)->execute();

                $sqlControl = 'SELECT reportid,lastcal FROM report_control_cca02 WHERE fiscalyear = ' . $fiscalYear . ' LIMIT 0,1';
                $queryControl = \Yii::$app->db->createCommand($sqlControl)->queryOne();

                /*
                  echo '---'.($refresh=='true'?'yes':'no').'---';
                  echo '---'.($queryControl == null?'yes':'no').'---';
                  echo '---'.(((strtotime( date('Y-m-d H:i:s') ) - strtotime( $queryControl['lastcal'] )) > 3600)?'yes':'no').'---';
                 */
                if ($refresh == 'true' || $queryControl == null || ((strtotime(date('Y-m-d H:i:s')) - strtotime($queryControl['lastcal'])) > 86400)) {

                    if ($queryControl == null) {

                        $sqlReportId = 'insert into report_control_cca02(fiscalyear,lastcal) values(' . $fiscalYear . ',NOW())';
                        \Yii::$app->db->createCommand($sqlReportId)->execute();

                        $reportId = \Yii::$app->db->getLastInsertID();
                    } else {
                        //echo '';

                        $reportId = $queryControl['reportid'];
                        $sqlReportId = 'update report_control_cca02 set lastcal = NOW() WHERE reportid = ' . $reportId;
                        \Yii::$app->db->createCommand($sqlReportId)->execute();
                    }

                    $sqlProcedure = 'CALL cascap_report.spCca02(' . $fiscalYear . ',' . $reportId . ')';
                    \Yii::$app->dbreport1->createCommand($sqlProcedure)->execute();

                    unset($queryControl);
                }
                //return;

                $queryControl = \Yii::$app->db->createCommand($sqlControl)->queryOne();

                $lastcal = $queryControl['lastcal'];

                //var_dump($querycontrol);

                $heading = [
                    'heading' => 'พื้นที่'
                ];

                $queryAllZone = array(['section' => 'เขตบริการสุขภาพ']);
                array_push($queryAllZone, $heading);

                for ($z_code = 1; $z_code <= 10; ++$z_code) {
                    if ($z_code > 1 && $z_code < 6)
                        continue;

                    $sqlZone = 'SELECT CONCAT("เขต ",' . $z_code . '+0) as zone,
                        sum(registerCount) AS registerCount,
                        sum(IF(rc.fiscalyear=' . $fiscalYear . ',rb.regisYearCount,0)) AS regisYearCount,
                        sum(m10) as m10,
                        sum(m11) as m11,
                        sum(m12) as m12,
                        sum(m01) as m01,
                        sum(m02) as m02,
                        sum(m03) as m03,
                        sum(m04) as m04,
                        sum(m05) as m05,
                        sum(m06) as m06,
                        sum(m07) as m07,
                        sum(m08) as m08,
                        sum(m09) as m09
                    FROM report_cca02_backend rb INNER JOIN report_control_cca02 rc
                    ON rb.rptid=rc.reportid WHERE rptid = ' . $queryControl['reportid'] . '
                    AND zone_code=' . $z_code;

                    //echo $sqlzone;

                    $queryZone = \Yii::$app->db->createCommand($sqlZone)->queryOne();
                    array_push($queryAllZone, $queryZone);
                }

                $sumEachMonth = array('summation' => 'รวม');
                $sumTotal = 0;
                //\appxq\sdii\utils\VarDumper::dump($queryAllZone);
                foreach ($queryAllZone as $key => $row) {

                    if (isset($row['heading']) || isset($row['section']) || isset($row['separator']))
                        continue;

                    $sumEachMonth['registerCount'] += (0 + intval($row['registerCount']));
                    $sumEachMonth['regisYearCount'] += (0 + intval($row['regisYearCount']));

                    foreach ($row as $colIndex => $col) {
                        if (preg_match('/^m[\d]{2}/', $colIndex)) {
                            $sumEachMonth[$colIndex] += (0 + intval($col));
                            $sumTotal += intval($col);
                        }
                    }

                    $sumEachZone = intval($row['m10']) +
                            intval($row['m11']) +
                            intval($row['m12']) +
                            intval($row['m01']) +
                            intval($row['m02']) +
                            intval($row['m03']) +
                            intval($row['m04']) +
                            intval($row['m05']) +
                            intval($row['m06']) +
                            intval($row['m07']) +
                            intval($row['m08']) +
                            intval($row['m09']);

                    $queryAllZone[$key]['total'] = $sumEachZone;
                    $queryAllZone[$key]['pct'] = ($sumEachZone == 0 || intval($row['registerCount']) == 0 ? 0 : ($sumEachZone / intval($row['registerCount']) ) * 100);
                }

                $sumEachMonth['total'] = $sumTotal;
                $sumEachMonth['pct'] = ($sumTotal == 0 || intval($sumEachMonth['registerCount']) == 0 ? 0 : ($sumTotal / intval($sumEachMonth['registerCount']) ) * 100);


                array_push($queryAllZone, $sumEachMonth);

                //var_dump($queryAllZone);

                if ($zone1 == null && $zone6 == null && $zone7 == null && $zone8 == null && $zone9 == null && $zone10 == null) {
                    \Yii::$app->response->format = \yii\web\Response::FORMAT_HTML;
                    return $this->renderAjax('_ajaxReportCca02', [
                                'fiscalYear' => $fiscalYear,
                                'lastcal' => $lastcal,
                                'thaimonth' => $thaiMonth,
                                'cca02AllZone' => $queryAllZone
                    ]);
                }


                $sqlBody = 'sum(registerCount) AS registerCount,
                    sum(IF(rc.fiscalyear=' . $fiscalYear . ',rb.regisYearCount,0)) AS regisYearCount,
                    sum(m10) as m10,
                    sum(m11) as m11,
                    sum(m12) as m12,
                    sum(m01) as m01,
                    sum(m02) as m02,
                    sum(m03) as m03,
                    sum(m04) as m04,
                    sum(m05) as m05,
                    sum(m06) as m06,
                    sum(m07) as m07,
                    sum(m08) as m08,
                    sum(m09) as m09
                FROM report_cca02_backend rb INNER JOIN report_control_cca02 rc
                    ON rb.rptid=rc.reportid WHERE rptid = ' . $queryControl['reportid'] . '
                AND zone_code=';

                $sqlHead = 'SELECT province,';

                $sqlTail = ' GROUP BY provincecode ORDER BY province';



                // province in zone
                if ($cca02Level == 1 || $cca02Level == 0) {
                    $sumCol = null;
                    array_push($queryAllZone, array('section' => 'รายจังหวัด'));
                    for ($z_code = 1; $z_code <= 10; ++$z_code) {
                        if ($z_code > 1 && $z_code < 6)
                            continue;
                        if ($z_code == 1 && $zone1 != 1)
                            continue;
                        else if ($z_code == 6 && $zone6 != 6)
                            continue;
                        else if ($z_code == 7 && $zone7 != 7)
                            continue;
                        else if ($z_code == 8 && $zone8 != 8)
                            continue;
                        else if ($z_code == 9 && $zone9 != 9)
                            continue;
                        else if ($z_code == 10 && $zone10 != 10)
                            continue;


                        $sql = $sqlHead . $sqlBody . $z_code . $sqlTail;
                        $query = \Yii::$app->db->createCommand($sql)->queryAll();


                        if (!$query == null) {

                            if ($sumCol != null) {

                                $sumCol['pct'] = $sumCol['registerCount'] == 0 ? 0 : (floatval($sumCol['total']) / floatval($sumCol['registerCount'])) * 100;
                                array_push($queryAllZone, $sumCol);
                            }

                            array_push($queryAllZone, array('separator' => 'เขต ' . $z_code));
                            $heading['heading'] = 'จังหวัด';
                            array_push($queryAllZone, $heading);

                            $sumCol = [
                                'summation' => 'รวม',
                                'registerCount' => 0,
                                'regisYearCount' => 0,
                                'm10' => 0,
                                'm11' => 0,
                                'm12' => 0,
                                'm01' => 0,
                                'm02' => 0,
                                'm03' => 0,
                                'm04' => 0,
                                'm05' => 0,
                                'm06' => 0,
                                'm07' => 0,
                                'm08' => 0,
                                'm09' => 0,
                                'total' => 0
                            ];
                        }


                        //var_dump($queryProvince);
                        foreach ($query as $key => $row) {
                            $sumEachProvince = intval($row['m10']) +
                                    intval($row['m11']) +
                                    intval($row['m12']) +
                                    intval($row['m01']) +
                                    intval($row['m02']) +
                                    intval($row['m03']) +
                                    intval($row['m04']) +
                                    intval($row['m05']) +
                                    intval($row['m06']) +
                                    intval($row['m07']) +
                                    intval($row['m08']) +
                                    intval($row['m09']);

                            $query[$key]['total'] = $sumEachProvince;
                            $query[$key]['pct'] = ($sumEachProvince == 0 || intval($row['registerCount']) == 0 ? 0 : ($sumEachProvince / intval($row['registerCount']) ) * 100);

                            array_push($queryAllZone, $query[$key]);

                            $sumCol['registerCount'] += $row['registerCount'];
                            $sumCol['regisYearCount'] += $row['regisYearCount'];
                            $sumCol['m10'] += $row['m10'];
                            $sumCol['m11'] += $row['m11'];
                            $sumCol['m12'] += $row['m12'];
                            $sumCol['m01'] += $row['m01'];
                            $sumCol['m02'] += $row['m02'];
                            $sumCol['m03'] += $row['m03'];
                            $sumCol['m04'] += $row['m04'];
                            $sumCol['m05'] += $row['m05'];
                            $sumCol['m06'] += $row['m06'];
                            $sumCol['m07'] += $row['m07'];
                            $sumCol['m08'] += $row['m08'];
                            $sumCol['m09'] += $row['m09'];
                            $sumCol['total'] += $sumEachProvince;
                        }
                    }
                }
                if (!$query == null) {
                    $sumCol['pct'] = $sumCol['registerCount'] == 0 ? 0 : ($sumCol['total'] / $sumCol['registerCount']) * 100;
                    array_push($queryAllZone, $sumCol);
                }


//                var_dump($queryAllZone);
//                return;
                // amphur in zone

                $sqlHead = 'SELECT province,amphur,';
                $sqlTail = ' GROUP BY provincecode, amphurcode ORDER BY province, amphurcode';
                if ($cca02Level == 2 || $cca02Level == 0) {
                    array_push($queryAllZone, array('section' => 'รายอำเภอ'));

                    $sumCol = null;

                    for ($z_code = 1; $z_code <= 10; ++$z_code) {
                        if ($z_code > 1 && $z_code < 6)
                            continue;
                        if ($z_code == 1 && $zone1 != 1)
                            continue;
                        else if ($z_code == 6 && $zone6 != 6)
                            continue;
                        else if ($z_code == 7 && $zone7 != 7)
                            continue;
                        else if ($z_code == 8 && $zone8 != 8)
                            continue;
                        else if ($z_code == 9 && $zone9 != 9)
                            continue;
                        else if ($z_code == 10 && $zone10 != 10)
                            continue;

                        $sql = $sqlHead . $sqlBody . $z_code . $sqlTail;

                        $query = \Yii::$app->db->createCommand($sql)->queryAll();
                        if (!$query == null) {
                            //array_push($queryAllZone,array('separator'=>'เขต '.$z_code));
                        }

                        //var_dump($queryProvince);
                        $province = '';

                        foreach ($query as $key => $row) {

                            if ($province != $row['province']) {

                                if ($sumCol != null) {
                                    $sumCol['pct'] = $sumCol['registerCount'] == 0 ? 0 : (($sumCol['total'] * 1.00) / $sumCol['registerCount']) * 100;
                                    array_push($queryAllZone, $sumCol);
                                }

                                $province = $row['province'];
                                array_push($queryAllZone, array('separator' => 'เขต ' . $z_code . ' / จ.' . $province));
                                $heading['heading'] = 'อำเภอ';
                                array_push($queryAllZone, $heading);

                                $sumCol = [
                                    'summation' => 'รวม',
                                    'registerCount' => 0,
                                    'regisYearCount' => 0,
                                    'm10' => 0,
                                    'm11' => 0,
                                    'm12' => 0,
                                    'm01' => 0,
                                    'm02' => 0,
                                    'm03' => 0,
                                    'm04' => 0,
                                    'm05' => 0,
                                    'm06' => 0,
                                    'm07' => 0,
                                    'm08' => 0,
                                    'm09' => 0,
                                    'total' => 0
                                ];
                            }

                            unset($query[$key]['province']);

                            $sumEachProvince = intval($row['m10']) +
                                    intval($row['m11']) +
                                    intval($row['m12']) +
                                    intval($row['m01']) +
                                    intval($row['m02']) +
                                    intval($row['m03']) +
                                    intval($row['m04']) +
                                    intval($row['m05']) +
                                    intval($row['m06']) +
                                    intval($row['m07']) +
                                    intval($row['m08']) +
                                    intval($row['m09']);

                            $query[$key]['total'] = $sumEachProvince;
                            $query[$key]['pct'] = ($sumEachProvince == 0 || intval($row['registerCount']) == 0 ? 0 : ($sumEachProvince / intval($row['registerCount']) ) * 100);

                            array_push($queryAllZone, $query[$key]);

                            $sumCol['registerCount'] += $row['registerCount'];
                            $sumCol['regisYearCount'] += $row['regisYearCount'];
                            $sumCol['m10'] += $row['m10'];
                            $sumCol['m11'] += $row['m11'];
                            $sumCol['m12'] += $row['m12'];
                            $sumCol['m01'] += $row['m01'];
                            $sumCol['m02'] += $row['m02'];
                            $sumCol['m03'] += $row['m03'];
                            $sumCol['m04'] += $row['m04'];
                            $sumCol['m05'] += $row['m05'];
                            $sumCol['m06'] += $row['m06'];
                            $sumCol['m07'] += $row['m07'];
                            $sumCol['m08'] += $row['m08'];
                            $sumCol['m09'] += $row['m09'];
                            $sumCol['total'] += $sumEachProvince;
                        }
                    }

                    if (!$query == null) {
                        $sumCol['pct'] = $sumCol['registerCount'] == 0 ? 0 : (($sumCol['total'] * 1.00) / $sumCol['registerCount']) * 100;
                        array_push($queryAllZone, $sumCol);
                    }
                }

                if ($cca02Level == 3 || $cca02Level == 0) {
                    // hcode in zone
                    $sqlHead = 'SELECT province,amphur,levelname,name,';
                    $sqlTail = ' GROUP BY hsitecode ORDER BY province,amphurcode,FIELD(hlevel,1,2,12,11,10,5,17,6,15,7,4,18,3,13,16,8,80,""),hsitecode+0';
                    array_push($queryAllZone, array('section' => 'ระดับสถานบริการ'));
                    $sumCol = null;

                    for ($z_code = 1; $z_code <= 10; ++$z_code) {
                        if ($z_code > 1 && $z_code < 6)
                            continue;
                        if ($z_code == 1 && $zone1 != 1)
                            continue;
                        else if ($z_code == 6 && $zone6 != 6)
                            continue;
                        else if ($z_code == 7 && $zone7 != 7)
                            continue;
                        else if ($z_code == 8 && $zone8 != 8)
                            continue;
                        else if ($z_code == 9 && $zone9 != 9)
                            continue;
                        else if ($z_code == 10 && $zone10 != 10)
                            continue;

                        $sql = $sqlHead . $sqlBody . $z_code . $sqlTail;

                        $query = \Yii::$app->db->createCommand($sql)->queryAll();
                        if (!$query == null) {
                            
                        }
                        //var_dump($queryProvince);
                        $province = '';
                        $amphur = '';
                        $levelname = '';
                        foreach ($query as $key => $row) {
                            if ($amphur != $row['amphur'] || $province != $row['province']) {

                                if ($sumCol != null) {
                                    $sumCol['pct'] = $sumCol['registerCount'] == 0 ? 0 : ($sumCol['total'] / $sumCol['registerCount']) * 100;
                                    array_push($queryAllZone, $sumCol);
                                }

                                $levelname = '';
                                $amphur = $row['amphur'];
                                $province = $row['province'];
                                array_push($queryAllZone, array('separator' => 'เขต ' . $z_code . ' / จ.' . $province . ' / อ.' . $amphur));
                                //array_push($queryAllZone,array('separator'=>'อ.'.$amphur));
                                $heading['heading'] = 'สถานบริการ';
                                array_push($queryAllZone, $heading);

                                $sumCol = [
                                    'summation' => 'รวม',
                                    'registerCount' => 0,
                                    'regisYearCount' => 0,
                                    'm10' => 0,
                                    'm11' => 0,
                                    'm12' => 0,
                                    'm01' => 0,
                                    'm02' => 0,
                                    'm03' => 0,
                                    'm04' => 0,
                                    'm05' => 0,
                                    'm06' => 0,
                                    'm07' => 0,
                                    'm08' => 0,
                                    'm09' => 0,
                                    'total' => 0
                                ];
                            }

                            if ($levelname != $row['levelname']) {
                                $levelname = $row['levelname'];
                                array_push($queryAllZone, array('typesplit' => $levelname));
                            }
                            unset($query[$key]['province']);
                            unset($query[$key]['amphur']);
                            unset($query[$key]['levelname']);

                            $sumEachProvince = intval($row['m10']) +
                                    intval($row['m11']) +
                                    intval($row['m12']) +
                                    intval($row['m01']) +
                                    intval($row['m02']) +
                                    intval($row['m03']) +
                                    intval($row['m04']) +
                                    intval($row['m05']) +
                                    intval($row['m06']) +
                                    intval($row['m07']) +
                                    intval($row['m08']) +
                                    intval($row['m09']);

                            $query[$key]['total'] = $sumEachProvince;
                            $query[$key]['pct'] = ($sumEachProvince == 0 || intval($row['registerCount']) == 0 ? 0 : ($sumEachProvince / intval($row['registerCount']) ) * 100);

                            array_push($queryAllZone, $query[$key]);

                            $sumCol['registerCount'] += $row['registerCount'];
                            $sumCol['regisYearCount'] += $row['regisYearCount'];
                            $sumCol['m10'] += $row['m10'];
                            $sumCol['m11'] += $row['m11'];
                            $sumCol['m12'] += $row['m12'];
                            $sumCol['m01'] += $row['m01'];
                            $sumCol['m02'] += $row['m02'];
                            $sumCol['m03'] += $row['m03'];
                            $sumCol['m04'] += $row['m04'];
                            $sumCol['m05'] += $row['m05'];
                            $sumCol['m06'] += $row['m06'];
                            $sumCol['m07'] += $row['m07'];
                            $sumCol['m08'] += $row['m08'];
                            $sumCol['m09'] += $row['m09'];
                            $sumCol['total'] += $sumEachProvince;
                        }
                    }

                    if (!$query == null) {
                        $sumCol['pct'] = $sumCol['registerCount'] == 0 ? 0 : ($sumCol['total'] / $sumCol['registerCount']) * 100;
                        array_push($queryAllZone, $sumCol);
                    }
                }
                //\appxq\sdii\utils\VarDumper::dump($queryAllZone);
                \Yii::$app->response->format = \yii\web\Response::FORMAT_HTML;
                return $this->renderAjax('_ajaxReportCca02', [
                            'fiscalYear' => $fiscalYear,
                            'lastcal' => $lastcal,
                            'thaimonth' => $thaiMonth,
                            'cca02AllZone' => $queryAllZone
                ]);
            }
        }
    }

    public function actionAjaxReportOverviewOnly() {
        $dataAll = array();

        array_push($dataAll, [
            'section', ['จำนวนหน่วยงาน (หน่วย)', 'ทั้งหมด', 'ปีนี้', 'เดือนนี้', 'สัปดาห์นี้', 'วันนี้']
                ]
        );


        $sqlDeptMember = "select code4,COUNT(distinct sitecode) as nall,count(distinct IF(YEAR(FROM_UNIXTIME(created_at))=YEAR(CURDATE()),sitecode,null)) as thisYear,count(distinct IF(YEAR(FROM_UNIXTIME(created_at))=YEAR(CURDATE()) AND MONTH(FROM_UNIXTIME(created_at))=MONTH(CURDATE()),sitecode,null)) as thisMonth,count(distinct IF(YEARWEEK(FROM_UNIXTIME(`created_at`)) = YEARWEEK(NOW()),sitecode,null)) as thisWeek,count(distinct IF(DATE(FROM_UNIXTIME(created_at))=CURDATE(),sitecode,null)) as today  from user u inner join user_profile p ON u.id=p.user_id inner join all_hospital_thai h on p.sitecode=h.hcode where h.code2 not like '%demo%' and status_del=0 group by code4 order by FIELD(code4+0,5,6,7,18,3,4,8,13,17,11,12,15,16,1,2,10,0,null);";
        $qryDeptMember = Yii::$app->db->createCommand($sqlDeptMember)->queryAll();

        foreach ($qryDeptMember as $membercount) {
            if (in_array($membercount['code4'], array("10"))) {
                $orgname = "หน่วยงานส่วนกลาง";
            } else if (in_array($membercount['code4'], array("1"))) {
                $orgname = "สำนักงานสาธารณสุขจังหวัด";
            } else if (in_array($membercount['code4'], array("2"))) {
                $orgname = "สำนักงานสาธารณสุขอำเภอ";
            } else if (in_array($membercount['code4'], array("5"))) {
                $orgname = "รพศ.";
            } else if (in_array($membercount['code4'], array("6"))) {
                $orgname = "รพท.";
            } else if (in_array($membercount['code4'], array("7"))) {
                $orgname = "รพช. รพร.";
            } else if (in_array($membercount['code4'], array("18", "3", "4", "8", "13", "17"))) {
                $orgname = "รพ.สต. สถานีอนามัย";
            } else if (in_array($membercount['code4'], array("11", "12"))) {
                $orgname = "โรงพยาบาล นอก สธ.";
            } else if (in_array($membercount['code4'], array("15", "16"))) {
                $orgname = "โรงพยาบาลเอกชน และคลินิกเอกชน";
            } else {
                $orgname = "อื่นๆ";
            }

            $dataAllP[$orgname]['All'] += $membercount['nall'];
            $dataAllP['All']['All'] += $membercount['nall'];

            $countp[$orgname]['All'] += $membercount['nall'];
            $countp[$orgname]['Year'] += $membercount['thisYear'];
            $countp[$orgname]['Month'] += $membercount['thisMonth'];
            $countp[$orgname]['Week'] += $membercount['thisWeek'];
            $countp[$orgname]['Day'] += $membercount['today'];
        }

        foreach ($countp as $orgname => $data) {
            $countAllP = $data['All'];
            $countYearP = $data['Year'];
            $countMonthP = $data['Month'];
            $countYearWeekRegisP = $data['Week'];
            $countDayP = $data['Day'];

            $sumAllP += $countAllP;
            $sumYearP += $countYearP;
            $sumMonthP += $countMonthP;
            $sumYearWeekRegisP += $countYearWeekRegisP;
            $sumDayP += $countDayP;


            array_push($dataAll, [
                'name', [$orgname, $countAllP, $countYearP, $countMonthP, $countYearWeekRegisP, $countDayP]
            ]);
        }

        array_push($dataAll, [
            'name', ['รวมทั้งหมด', $sumAllP, $sumYearP, $sumMonthP, $sumYearWeekRegisP, $sumDayP]
                ]
        );

        array_push($dataAll, [
            'section', ['จำนวนสมาชิก (คน)', 'ทั้งหมด', 'ปีนี้', 'เดือนนี้', 'สัปดาห์นี้', 'วันนี้']
                ]
        );
        $sqlDeptMember = "select code4,COUNT(*) as nall,SUM(YEAR(FROM_UNIXTIME(created_at))=YEAR(CURDATE())) as thisYear,SUM(YEAR(FROM_UNIXTIME(created_at))=YEAR(CURDATE()) AND MONTH(FROM_UNIXTIME(created_at))=MONTH(CURDATE())) as thisMonth,SUM(YEARWEEK(FROM_UNIXTIME(`created_at`)) = YEARWEEK(NOW())) as thisWeek,SUM(DATE(FROM_UNIXTIME(created_at))=CURDATE()) as today  from user u inner join user_profile p ON u.id=p.user_id inner join all_hospital_thai h on p.sitecode=h.hcode where h.code2 not like '%demo%' and status_del=0 group by code4 order by FIELD(code4+0,5,6,7,18,3,4,8,13,17,11,12,15,16,1,2,10,0,null);";
        $qryDeptMember = Yii::$app->db->createCommand($sqlDeptMember)->queryAll();

        foreach ($qryDeptMember as $membercount) {
            if (in_array($membercount['code4'], array("10"))) {
                $orgname = "หน่วยงานส่วนกลาง";
            } else if (in_array($membercount['code4'], array("1"))) {
                $orgname = "สำนักงานสาธารณสุขจังหวัด";
            } else if (in_array($membercount['code4'], array("2"))) {
                $orgname = "สำนักงานสาธารณสุขอำเภอ";
            } else if (in_array($membercount['code4'], array("5"))) {
                $orgname = "รพศ.";
            } else if (in_array($membercount['code4'], array("6"))) {
                $orgname = "รพท.";
            } else if (in_array($membercount['code4'], array("7"))) {
                $orgname = "รพช. รพร.";
            } else if (in_array($membercount['code4'], array("18", "3", "4", "8", "13", "17"))) {
                $orgname = "รพ.สต. สถานีอนามัย";
            } else if (in_array($membercount['code4'], array("11", "12"))) {
                $orgname = "โรงพยาบาล นอก สธ.";
            } else if (in_array($membercount['code4'], array("15", "16"))) {
                $orgname = "โรงพยาบาลเอกชน และคลินิกเอกชน";
            } else {
                $orgname = "อื่นๆ";
            }

            $count[$orgname]['All'] += $membercount['nall'];
            $count[$orgname]['Year'] += $membercount['thisYear'];
            $count[$orgname]['Month'] += $membercount['thisMonth'];
            $count[$orgname]['Week'] += $membercount['thisWeek'];
            $count[$orgname]['Day'] += $membercount['today'];
        }

        foreach ($count as $orgname => $data) {
            $countAll = $data['All'];
            $countYear = $data['Year'];
            $countMonth = $data['Month'];
            $countYearWeekRegis = $data['Week'];
            $countDay = $data['Day'];

            $sumAll += $countAll;
            $sumYear += $countYear;
            $sumMonth += $countMonth;
            $sumYearWeekRegis += $countYearWeekRegis;
            $sumDay += $countDay;


            array_push($dataAll, [
                'name', [$orgname, $countAll, $countYear, $countMonth, $countYearWeekRegis, $countDay]
            ]);
        }

        array_push($dataAll, [
            'name', ['รวมทั้งหมด', $sumAll, $sumYear, $sumMonth, $sumYearWeekRegis, $sumDay]
                ]
        );

        $sql = "SELECT count(*) as nall from person";
        $sql = "SHOW TABLE STATUS LIKE 'person'";
        $data = Yii::$app->dbbot->createCommand($sql)->queryOne();
        $dataAllP['TCC'] = $data['Rows'];
        $sql = "SELECT count(*) as nall from person";
        $sql = "SHOW TABLE STATUS LIKE 'person'";
        $data = Yii::$app->dbnemo->createCommand($sql)->queryOne();
        $dataAllP['NEMO'] = $data['Rows'];

        Yii::$app->response->format = \yii\web\Response::FORMAT_HTML;
        return $this->render('_ajaxReportOverviewOnly', [
                    'dataAll' => $dataAll,
                    'dataAllP' => $dataAllP,
        ]);
    }

    public function actionAjaxReportOverview() {
        $dataAll = array();

        array_push($dataAll, [
            'section', ['จำนวนหน่วยงาน (หน่วย)', 'ทั้งหมด', 'ปีนี้', 'เดือนนี้', 'สัปดาห์นี้', 'วันนี้']
                ]
        );


        $sqlDeptMember = "select code4,COUNT(distinct sitecode) as nall,count(distinct IF(YEAR(FROM_UNIXTIME(created_at))=YEAR(CURDATE()),sitecode,null)) as thisYear,count(distinct IF(YEAR(FROM_UNIXTIME(created_at))=YEAR(CURDATE()) AND MONTH(FROM_UNIXTIME(created_at))=MONTH(CURDATE()),sitecode,null)) as thisMonth,count(distinct IF(YEARWEEK(FROM_UNIXTIME(`created_at`)) = YEARWEEK(NOW()),sitecode,null)) as thisWeek,count(distinct IF(DATE(FROM_UNIXTIME(created_at))=CURDATE(),sitecode,null)) as today  from user u inner join user_profile p ON u.id=p.user_id inner join all_hospital_thai h on p.sitecode=h.hcode where h.code2 not like '%demo%' and status_del=0 group by code4 order by FIELD(code4+0,5,6,7,18,3,4,8,13,17,11,12,15,16,1,2,10,0,null);";
        $qryDeptMember = Yii::$app->db->createCommand($sqlDeptMember)->queryAll();

        foreach ($qryDeptMember as $membercount) {
            if (in_array($membercount['code4'], array("10"))) {
                $orgname = "หน่วยงานส่วนกลาง";
            } else if (in_array($membercount['code4'], array("1"))) {
                $orgname = "สำนักงานสาธารณสุขจังหวัด";
            } else if (in_array($membercount['code4'], array("2"))) {
                $orgname = "สำนักงานสาธารณสุขอำเภอ";
            } else if (in_array($membercount['code4'], array("5"))) {
                $orgname = "โรงพยาบาลศูนย์"; //"รพศ.";
            } else if (in_array($membercount['code4'], array("6"))) {
                $orgname = "โรงพยาบาลทั่วไป"; //"รพท.";
            } else if (in_array($membercount['code4'], array("7"))) {
                $orgname = "โรงพยาบาลชุมชน หรือโรงพยาบาลสมเด็จพระยุพราช"; //"รพช. รพร.";
            } else if (in_array($membercount['code4'], array("18", "3", "4", "8", "13", "17"))) {
                $orgname = "โรงพยาบาลส่งเสริมสุขภาพประจำตำบล"; //"รพ.สต. สถานีอนามัย";
            } else if (in_array($membercount['code4'], array("11", "12"))) {
                $orgname = "โรงพยาบาล นอก สธ.";
            } else if (in_array($membercount['code4'], array("15", "16"))) {
                $orgname = "โรงพยาบาลเอกชน และคลินิกเอกชน";
            } else {
                $orgname = "อื่นๆ";
            }

            $dataAllP[$orgname]['All'] += $membercount['nall'];
            $dataAllP['All']['All'] += $membercount['nall'];

            $countp[$orgname]['All'] += $membercount['nall'];
            $countp[$orgname]['Year'] += $membercount['thisYear'];
            $countp[$orgname]['Month'] += $membercount['thisMonth'];
            $countp[$orgname]['Week'] += $membercount['thisWeek'];
            $countp[$orgname]['Day'] += $membercount['today'];
        }

        foreach ($countp as $orgname => $data) {
            $countAllP = $data['All'];
            $countYearP = $data['Year'];
            $countMonthP = $data['Month'];
            $countYearWeekRegisP = $data['Week'];
            $countDayP = $data['Day'];

            $sumAllP += $countAllP;
            $sumYearP += $countYearP;
            $sumMonthP += $countMonthP;
            $sumYearWeekRegisP += $countYearWeekRegisP;
            $sumDayP += $countDayP;


            array_push($dataAll, [
                'name', [$orgname, $countAllP, $countYearP, $countMonthP, $countYearWeekRegisP, $countDayP]
            ]);
        }

        array_push($dataAll, [
            'name', ['รวมทั้งหมด', $sumAllP, $sumYearP, $sumMonthP, $sumYearWeekRegisP, $sumDayP]
                ]
        );

        array_push($dataAll, [
            'section', ['จำนวนสมาชิก (คน)', 'ทั้งหมด', 'ปีนี้', 'เดือนนี้', 'สัปดาห์นี้', 'วันนี้']
                ]
        );
        $sqlDeptMember = "select code4,COUNT(*) as nall,SUM(YEAR(FROM_UNIXTIME(created_at))=YEAR(CURDATE())) as thisYear,SUM(YEAR(FROM_UNIXTIME(created_at))=YEAR(CURDATE()) AND MONTH(FROM_UNIXTIME(created_at))=MONTH(CURDATE())) as thisMonth,SUM(YEARWEEK(FROM_UNIXTIME(`created_at`)) = YEARWEEK(NOW())) as thisWeek,SUM(DATE(FROM_UNIXTIME(created_at))=CURDATE()) as today  from user u inner join user_profile p ON u.id=p.user_id inner join all_hospital_thai h on p.sitecode=h.hcode where h.code2 not like '%demo%' and status_del=0 group by code4 order by FIELD(code4+0,5,6,7,18,3,4,8,13,17,11,12,15,16,1,2,0,null,10);";
        $qryDeptMember = Yii::$app->db->createCommand($sqlDeptMember)->queryAll();

        foreach ($qryDeptMember as $membercount) {
            if (in_array($membercount['code4'], array("10"))) {
                $orgname = "หน่วยงานส่วนกลาง";
            } else if (in_array($membercount['code4'], array("1"))) {
                $orgname = "สำนักงานสาธารณสุขจังหวัด";
            } else if (in_array($membercount['code4'], array("2"))) {
                $orgname = "สำนักงานสาธารณสุขอำเภอ";
            } else if (in_array($membercount['code4'], array("5"))) {
                $orgname = "โรงพยาบาลศูนย์"; //"รพศ.";
            } else if (in_array($membercount['code4'], array("6"))) {
                $orgname = "โรงพยาบาลทั่วไป"; //"รพท.";
            } else if (in_array($membercount['code4'], array("7"))) {
                $orgname = "โรงพยาบาลชุมชน หรือโรงพยาบาลสมเด็จพระยุพราช"; //"รพช. รพร.";
            } else if (in_array($membercount['code4'], array("18", "3", "4", "8", "13", "17"))) {
                $orgname = "โรงพยาบาลส่งเสริมสุขภาพประจำตำบล"; //"รพ.สต. สถานีอนามัย";
            } else if (in_array($membercount['code4'], array("11", "12"))) {
                $orgname = "โรงพยาบาล นอก สธ.";
            } else if (in_array($membercount['code4'], array("15", "16"))) {
                $orgname = "โรงพยาบาลเอกชน และคลินิกเอกชน";
            } else {
                $orgname = "อื่นๆ";
            }

            $count[$orgname]['All'] += $membercount['nall'];
            $count[$orgname]['Year'] += $membercount['thisYear'];
            $count[$orgname]['Month'] += $membercount['thisMonth'];
            $count[$orgname]['Week'] += $membercount['thisWeek'];
            $count[$orgname]['Day'] += $membercount['today'];
        }

        foreach ($count as $orgname => $data) {
            $countAll = $data['All'];
            $countYear = $data['Year'];
            $countMonth = $data['Month'];
            $countYearWeekRegis = $data['Week'];
            $countDay = $data['Day'];

            $sumAll += $countAll;
            $sumYear += $countYear;
            $sumMonth += $countMonth;
            $sumYearWeekRegis += $countYearWeekRegis;
            $sumDay += $countDay;


            array_push($dataAll, [
                'name', [$orgname, $countAll, $countYear, $countMonth, $countYearWeekRegis, $countDay]
            ]);
        }

        array_push($dataAll, [
            'name', ['รวมทั้งหมด', $sumAll, $sumYear, $sumMonth, $sumYearWeekRegis, $sumDay]
                ]
        );

        /*
          10 	หน่วยงานส่วนกลาง
          1	สำนักงานสาธารณะสุขจังหวัด
          2	สำนักงานสาธารณะสุขอำเภอ
          5, 6	รพศ. รพท.
          7	รพช. รพร.
          18, 3, 4, 8, 13, 17	รพ.สต. สถานีอนามัย
          หน่วยงานบริการอื่นๆ
          11, 12	โรงพยาบาล นอก สธ.
          15, 16	โรงพยาบาลเอกชน และคลินิกเอกชน
         */
        /*
          $listDeptMember = [
          "publicAgencies" => "`code4` = '10'",
          "provincialPublicHealthOffice" => "`code4` = '1'",
          "districtPublicHealthOffice" => "`code4` = '2'",
          "deptCentral" => "`code4` = '5' OR `code4` = '6'",
          "centralGeneral" => "`code4` = '7'",
          "rural" => "`code4` = '18' OR `code4` = '3' OR `code4` = '4' OR `code4` = '8' OR `code4` = '13' OR `code4` = '17'",
          "outsideTheHospital" => "`code4` = '11' OR `code4` = '12'",
          "privateHospitalsAndClinics" => "`code4` = '15' OR `code4` = '16'",
          "other" => "`code4`+0 >= '19' OR `code4` = '9' OR `code4` = '14'"
          ];

          $dataAll = array();
          array_push($dataAll, [
          'section', ['จำนวนหน่วยงานสมาชิก', 'ทั้งหมด', 'ปีนี้', 'เดือนนี้', 'สัปดาห์นี้', 'วันนี้']
          ]
          );
          $sumAll = 0;
          $sumYear = 0;
          $sumMonth = 0;
          $sumYearWeekRegis = 0;
          $sumDay = 0;

          foreach ($listDeptMember as $key => $deptMember) {
          $sqlDeptMember = "SELECT countAll,
          YEAR(NOW()) as `yearNow`,
          countYear,
          MONTH(NOW()) as `monthNow`,
          countMonth,
          YEARWEEK(NOW()) as `yearWeekNow`,
          countYearWeekRegis,
          DAY(NOW()) as `dayNow`,
          countDay
          FROM (
          SELECT COUNT(*) as countAll
          FROM `user_profile`, `user`, `all_hospital_thai`
          WHERE `user_profile`.`sitecode` = `all_hospital_thai`.`hcode`
          AND `user_profile`.`user_id` = `user`.`id`
          AND ( " . ($deptMember) . " )
          ) as `allRegis`
          LEFT OUTER JOIN (
          SELECT COUNT(*) as countYear
          FROM `user_profile`, `user`, `all_hospital_thai`
          WHERE `user_profile`.`sitecode` = `all_hospital_thai`.`hcode`
          AND `user_profile`.`user_id` = `user`.`id`
          AND YEAR(FROM_UNIXTIME(`created_at`)) = YEAR(NOW())
          AND ( " . ($deptMember) . " )
          ) as `yearRegis`
          ON ( 1 )
          LEFT OUTER JOIN (
          SELECT COUNT(*) as countMonth
          FROM `user_profile`, `user`, `all_hospital_thai`
          WHERE `user_profile`.`sitecode` = `all_hospital_thai`.`hcode`
          AND `user_profile`.`user_id` = `user`.`id`
          AND YEAR(FROM_UNIXTIME(`created_at`)) = YEAR(NOW())
          AND MONTH(FROM_UNIXTIME(`created_at`)) = MONTH(NOW())
          AND ( " . ($deptMember) . " )
          ) as `monthRegis`
          ON ( 1 )
          LEFT OUTER JOIN (
          SELECT COUNT(*) as countYearWeekRegis
          FROM `user_profile`, `user`, `all_hospital_thai`
          WHERE `user_profile`.`sitecode` = `all_hospital_thai`.`hcode`
          AND `user_profile`.`user_id` = `user`.`id`
          AND YEAR(FROM_UNIXTIME(`created_at`)) = YEAR(NOW())
          AND YEARWEEK(FROM_UNIXTIME(`created_at`)) = YEARWEEK(NOW())
          AND ( " . ($deptMember) . " )
          ) as `yearWeekRegis`
          ON ( 1 )
          LEFT OUTER JOIN (
          SELECT COUNT(*) as countDay
          FROM `user_profile`, `user`, `all_hospital_thai`
          WHERE `user_profile`.`sitecode` = `all_hospital_thai`.`hcode`
          AND `user_profile`.`user_id` = `user`.`id`
          AND YEAR(FROM_UNIXTIME(`created_at`)) = YEAR(NOW())
          AND MONTH(FROM_UNIXTIME(`created_at`)) = MONTH(NOW())
          AND DAY(FROM_UNIXTIME(`created_at`)) = DAY(NOW())
          AND ( " . ($deptMember) . " )
          ) as `day`
          ON ( 1 ) ";

          //            echo "<pre>$sqlDeptMember</pre>";

          if ($key == 'publicAgencies') {
          $strHeadDeptMember = "หน่วยงานส่วนกลาง (กรม สำนัก ศูนย์)";
          } else if ($key == 'provincialPublicHealthOffice') {
          $strHeadDeptMember = "สำนักงานสาธารณสุขจังหวัด";
          } else if ($key == 'districtPublicHealthOffice') {
          $strHeadDeptMember = "สำนักงานสาธารณสุขอำเภอ";
          } else if ($key == 'deptCentral') {
          $strHeadDeptMember = "รพศ. รพท.";
          } else if ($key == 'centralGeneral') {
          $strHeadDeptMember = "รพช. รพร.";
          } else if ($key == 'rural') {
          $strHeadDeptMember = "รพ.สต. และ สถานีอนามัย";
          } else if ($key == 'outsideTheHospital') {
          $strHeadDeptMember = "โรงพยาบาล นอก สธ.";
          } else if ($key == 'privateHospitalsAndClinics') {
          $strHeadDeptMember = "โรงพยาบาลเอกชน และคลินิกเอกชน";
          } else if ($key == 'other') {
          $strHeadDeptMember = "หน่วยงานบริการอื่นๆ";
          }

          $qryDeptMember = Yii::$app->db->createCommand($sqlDeptMember)->queryAll();

          $countAll = $qryDeptMember[0]['countAll'];
          $countYear = $qryDeptMember[0]['countYear'];
          $countMonth = $qryDeptMember[0]['countMonth'];
          $countYearWeekRegis = $qryDeptMember[0]['countYearWeekRegis'];
          $countDay = $qryDeptMember[0]['countDay'];

          array_push($dataAll, [
          'name', [$strHeadDeptMember, $countAll, $countYear, $countMonth, $countYearWeekRegis, $countDay]
          ]
          );

          $sumAll += $countAll;
          $sumYear += $countYear;
          $sumMonth += $countMonth;
          $sumYearWeekRegis += $countYearWeekRegis;
          $sumDay += $countDay;
          }

          array_push($dataAll, [
          'name', ['รวมทั้งหมด', $sumAll, $sumYear, $sumMonth, $sumYearWeekRegis, $sumDay]
          ]
          );
         */

        $listResultReport = [
            "Register" => ["tb_data_1", "ptid", "id", "create_date"],
            "CCA-01 (ลงทะเบียนกลุ่มเสี่ยง)" => ["tb_data_2", "ptid", "id", "f1vdcomp"],
            "OV-01K (Kato-Katz)" => ["tbdata_21", "target", "id", "exdate"],
            "OV-01P (Parasep)" => ["tbdata_22", "target", "id", "exdate"],
            "OV-01F (FECT)" => ["tbdata_23", "target", "id", "exdate"],
            "OV-01U (Urine)" => ["tbdata_24", "target", "id", "exdate"],
            "OV-02 (ปรับเปลี่ยนพฤติกรรมเสี่ยงปรับเปลี่ยนพฤติกรรมเสี่ยง)" => ["tbdata_25", "target", "id", "vdate"],
            "OV-03 (ให้การรักษาพยาธิ)" => ["tbdata_26", "target", "id", "vdate"],
            "CCA-02 (ตรวจคัดกรองมะเร็งท่อน้ำดี)" => ["tb_data_3", "ptid", "id", "f2v1"],
            "CCA-02.1 (ตรวจ CT/MRI)" => ["tb_data_4", "ptid", "id", "f2p1v1"],
            "CCA-03 (ให้การรักษา)" => ["tb_data_7", "ptid", "id", "f3v2a2d"],
            //"CCA-03.1" => ["tb_data_6", "ptid", "id", "f3p1v1a1"],
            "CCA-04 (ตรวจพยาธิวิทยา)" => ["tb_data_8", "ptid", "id", "f4complete"],
            "CCA-05 (ติดตามผลการรักษามะเร็งท่อน้ำดี)" => ["tb_data_11", "ptid", "id", "f5v1a1"]
        ];
        array_push($dataAll, [
            'section', ['สรุปผลงานรวม', '', '', '', '', '']
                ]
        );

        $sql = "SELECT count(*) as nall from person";
        $sql = "SHOW TABLE STATUS LIKE 'person'";
        $data = Yii::$app->dbbot->createCommand($sql)->queryOne();
        $dataAllP['TCC'] = $data['Rows'];
        $sql = "SELECT count(*) as nall from person";
        $sql = "SHOW TABLE STATUS LIKE 'person'";
        $data = Yii::$app->dbnemo->createCommand($sql)->queryOne();
        $dataAllP['NEMO'] = $data['Rows'];
        foreach ($listResultReport as $key => $param) {
            if ($param[0] == "tb_data_1") {
                $sql = "SELECT count(distinct ptid) as AllP from tb_data_1 p INNER JOIN all_hospital_thai h on p.hsitecode=h.hcode where code2<>'demo' and rstat not in ('0','3') and DATE(create_date)>='2013-02-09';";
                $data = Yii::$app->db->createCommand($sql)->queryOne();
                $dataAllP['Form']['Register'] = $data['AllP'];
            } else if ($param[0] == "tb_data_2") {
                array_push($dataAll, [
                    'section', [$key, 'ทั้งหมด', 'ปีนี้', 'เดือนนี้', 'สัปดาห์นี้', 'วันนี้']
                        ]
                );
                $sql = "SELECT count(distinct ptid) as AllP,count(distinct IF(YEAR(f1vdcomp)=YEAR(CURDATE()),ptid,null)) as YearP,count(distinct IF(YEAR(f1vdcomp)=YEAR(CURDATE())  AND MONTH(f1vdcomp)=MONTH(CURDATE()),ptid,null)) as MonthP,count(distinct IF(YEARWEEK(f1vdcomp)=YEARWEEK(CURDATE()),ptid,null)) as WeekP,count(distinct IF(f1vdcomp=CURDATE(),ptid,null)) as DayP from tb_data_2 p INNER JOIN all_hospital_thai h on p.hsitecode=h.hcode where code2<>'demo' and rstat not in ('0','3') and DATE(f1vdcomp)>='2013-02-09';";
                $data = Yii::$app->db->createCommand($sql)->queryOne();
                $dataAllP['Form']['CCA-01'] = $data['AllP'];

                array_push($dataAll, [
                    'name', ['&emsp;&emsp;จำนวนราย', $data['AllP'], $data['YearP'], $data['MonthP'], $data['WeekP'], $data['DayP']]
                        ]
                );

                array_push($dataAll, [
                    'section', ['ตรวจรักษาพยาธิใบไม้ตับ (OV-01)', '', '', '', '', '']
                        ]
                );
                $dataAllP['Form']['CCA-01'] = $data['AllP'];
            } else {
                array_push($dataAll, [
                    'section', [$key, 'ทั้งหมด', 'ปีนี้', 'เดือนนี้', 'สัปดาห์นี้', 'วันนี้']
                        ]
                );

                if ($param[0] == "tb_data_3") {
                    $sql = "SELECT count(*) as AllT,count(distinct ptid) as AllP,SUM(YEAR(visitdate)=YEAR(CURDATE())) as YearT,count(distinct IF(YEAR(visitdate)=YEAR(CURDATE()),ptid,null)) as YearP,SUM(YEAR(visitdate)=YEAR(CURDATE()) AND MONTH(visitdate)=MONTH(CURDATE())) as MonthT,count(distinct IF(YEAR(visitdate)=YEAR(CURDATE())  AND MONTH(visitdate)=MONTH(CURDATE()),ptid,null)) as MonthP,SUM(YEARWEEK(visitdate)=YEARWEEK(CURDATE())) as WeekT,count(distinct IF(YEARWEEK(visitdate)=YEARWEEK(CURDATE()),ptid,null)) as WeekP,SUM(DATE(visitdate)=CURDATE()) as DayT,count(distinct IF(visitdate=CURDATE(),ptid,null)) as DayP,count(distinct IF(f2v6a3b1=1,ptid,null)) as suspected from table p INNER JOIN all_hospital_thai h on p.hsitecode=h.hcode where code2<>'demo' and rstat not in ('0','3') and DATE(visitdate)>='2013-02-09';";
                    $sql = str_replace('table', $param[0], $sql);
                    $sql = str_replace('visitdate', $param[3], $sql);
                    $data = Yii::$app->db->createCommand($sql)->queryOne();

                    $dataAllP['Form']['CCA-02P'] = $data['AllP'];
                    $dataAllP['Form']['CCA-02T'] = $data['AllT'];
                    $dataAllP['Form']['CCA-02S'] = $data['suspected'];
                } else {
                    $sql = "SELECT count(*) as AllT,count(distinct ptid) as AllP,SUM(YEAR(visitdate)=YEAR(CURDATE())) as YearT,count(distinct IF(YEAR(visitdate)=YEAR(CURDATE()),ptid,null)) as YearP,SUM(YEAR(visitdate)=YEAR(CURDATE()) AND MONTH(visitdate)=MONTH(CURDATE())) as MonthT,count(distinct IF(YEAR(visitdate)=YEAR(CURDATE())  AND MONTH(visitdate)=MONTH(CURDATE()),ptid,null)) as MonthP,SUM(YEARWEEK(visitdate)=YEARWEEK(CURDATE())) as WeekT,count(distinct IF(YEARWEEK(visitdate)=YEARWEEK(CURDATE()),ptid,null)) as WeekP,SUM(DATE(visitdate)=CURDATE()) as DayT,count(distinct IF(visitdate=CURDATE(),ptid,null)) as DayP from table p INNER JOIN all_hospital_thai h on p.hsitecode=h.hcode where code2<>'demo' and rstat not in ('0','3') and DATE(visitdate)>='2013-02-09';";
                    $sql = str_replace('table', $param[0], $sql);
                    $sql = str_replace('visitdate', $param[3], $sql);
                    $data = Yii::$app->db->createCommand($sql)->queryOne();
                }
                array_push($dataAll, [
                    'name', ['&emsp;&emsp;จำนวนราย', $data['AllP'], $data['YearP'], $data['MonthP'], $data['WeekP'], $data['DayP']]
                        ]
                );
                array_push($dataAll, [
                    'name', ['&emsp;&emsp;จำนวนครั้ง', $data['AllT'], $data['YearT'], $data['MonthT'], $data['WeekT'], $data['DayT']]
                        ]
                );
            }
        }

//=====================
        /*
          $listResultReport = [
          "CCA-01" => ["tb_data_2", "ptid", "id", "f1vdcomp"],
          "OV-01K" => ["tbdata_21", "target", "id", "exdate"],
          "OV-01P" => ["tbdata_22", "target", "id", "exdate"],
          "OV-01F" => ["tbdata_23", "target", "id", "exdate"],
          "OV-01U" => ["tbdata_24", "target", "id", "exdate"],
          "OV-02" => ["tbdata_25", "target", "id", "vdate"],
          "OV-03" => ["tbdata_26", "target", "id", "vdate"],
          "CCA-02" => ["tb_data_3", "ptid", "id", "f2v1"],
          "CCA-02.1" => ["tb_data_4", "ptid", "id", "f2p1v1"],
          "CCA-03" => ["tb_data_7", "ptid", "id", "f3v2a2d"],
          //"CCA-03.1" => ["tb_data_6", "ptid", "id", "f3p1v1a1"],
          "CCA-04" => ["tb_data_8", "ptid", "id", "f4complete"],
          "CCA-05" => ["tb_data_11", "ptid", "id", "f5v1a1"]
          ];

          array_push($dataAll, [
          'section', ['สรุปผลงานรวม', '', '', '', '', '']
          ]
          );
          foreach ($listResultReport as $key => $resultReport) {
          $sqlResultReportTarget = "SELECT *
          FROM (
          SELECT COUNT(DISTINCT(" . $resultReport[1] . ")) as countAll
          FROM " . $resultReport[0] . ",	all_hospital_thai
          WHERE	" . $resultReport[0] . "." . $resultReport[3] . " IS NOT NULL
          AND " . $resultReport[0] . "." . $resultReport[3] . " >= '2013-02-09 00:00:00'
          AND " . $resultReport[0] . ".hsitecode = all_hospital_thai.hcode
          ) as resultReportAll
          LEFT OUTER JOIN(
          SELECT COUNT(DISTINCT(" . $resultReport[1] . ")) as countYear
          FROM " . $resultReport[0] . ", all_hospital_thai
          WHERE " . $resultReport[0] . "." . $resultReport[3] . " IS NOT NULL
          AND YEAR(" . $resultReport[0] . "." . $resultReport[3] . " ) = YEAR(NOW())
          AND " . $resultReport[0] . ".hsitecode = all_hospital_thai.hcode
          ) as resultReportYear ON ( 1 )
          LEFT OUTER JOIN(
          SELECT COUNT(DISTINCT(" . $resultReport[1] . ")) as countMonth
          FROM " . $resultReport[0] . ", all_hospital_thai
          WHERE " . $resultReport[0] . "." . $resultReport[3] . " IS NOT NULL
          AND YEAR(" . $resultReport[0] . "." . $resultReport[3] . " ) = YEAR(NOW())
          AND MONTH(" . $resultReport[0] . "." . $resultReport[3] . " ) = MONTH(NOW())
          AND " . $resultReport[0] . ".hsitecode = all_hospital_thai.hcode
          ) as resultReportMonth ON ( 1 )
          LEFT OUTER JOIN(
          SELECT COUNT(DISTINCT(" . $resultReport[1] . ")) as countYearWeek
          FROM " . $resultReport[0] . ", all_hospital_thai
          WHERE " . $resultReport[0] . "." . $resultReport[3] . " IS NOT NULL
          AND YEAR(" . $resultReport[0] . "." . $resultReport[3] . " ) = YEAR(NOW())
          AND YEARWEEK(" . $resultReport[0] . "." . $resultReport[3] . " ) = YEARWEEK(NOW())
          AND " . $resultReport[0] . ".hsitecode = all_hospital_thai.hcode
          ) as resultReportYearWeek ON ( 1 )
          LEFT OUTER JOIN(
          SELECT COUNT(DISTINCT(" . $resultReport[1] . ")) as countDay
          FROM " . $resultReport[0] . ", all_hospital_thai
          WHERE " . $resultReport[0] . "." . $resultReport[3] . " IS NOT NULL
          AND YEAR(" . $resultReport[0] . "." . $resultReport[3] . " ) = YEAR(NOW())
          AND MONTH(" . $resultReport[0] . "." . $resultReport[3] . " ) = MONTH(NOW())
          AND DAY(" . $resultReport[0] . "." . $resultReport[3] . " ) = DAY(NOW())
          AND " . $resultReport[0] . ".hsitecode = all_hospital_thai.hcode
          ) as resultReportDay ON ( 1 )";

          $sqlResultReportAll = "SELECT *
          FROM (
          SELECT COUNT((" . $resultReport[2] . ")) as countAll
          FROM " . $resultReport[0] . ",	all_hospital_thai
          WHERE	" . $resultReport[0] . "." . $resultReport[3] . " IS NOT NULL
          AND " . $resultReport[0] . "." . $resultReport[3] . " >= '2013-02-09 00:00:00'
          AND " . $resultReport[0] . ".hsitecode = all_hospital_thai.hcode
          ) as resultReportAll
          LEFT OUTER JOIN(
          SELECT COUNT((" . $resultReport[2] . ")) as countYear
          FROM " . $resultReport[0] . ", all_hospital_thai
          WHERE " . $resultReport[0] . "." . $resultReport[3] . " IS NOT NULL
          AND YEAR(" . $resultReport[0] . "." . $resultReport[3] . " ) = YEAR(NOW())
          AND " . $resultReport[0] . ".hsitecode = all_hospital_thai.hcode
          ) as resultReportYear ON ( 1 )
          LEFT OUTER JOIN(
          SELECT COUNT((" . $resultReport[2] . ")) as countMonth
          FROM " . $resultReport[0] . ", all_hospital_thai
          WHERE " . $resultReport[0] . "." . $resultReport[3] . " IS NOT NULL
          AND YEAR(" . $resultReport[0] . "." . $resultReport[3] . " ) = YEAR(NOW())
          AND MONTH(" . $resultReport[0] . "." . $resultReport[3] . " ) = MONTH(NOW())
          AND " . $resultReport[0] . ".hsitecode = all_hospital_thai.hcode
          ) as resultReportMonth ON ( 1 )
          LEFT OUTER JOIN(
          SELECT COUNT((" . $resultReport[2] . ")) as countYearWeek
          FROM " . $resultReport[0] . ", all_hospital_thai
          WHERE " . $resultReport[0] . "." . $resultReport[3] . " IS NOT NULL
          AND YEAR(" . $resultReport[0] . "." . $resultReport[3] . " ) = YEAR(NOW())
          AND YEARWEEK(" . $resultReport[0] . "." . $resultReport[3] . " ) = YEARWEEK(NOW())
          AND " . $resultReport[0] . ".hsitecode = all_hospital_thai.hcode
          ) as resultReportYearWeek ON ( 1 )
          LEFT OUTER JOIN(
          SELECT COUNT((" . $resultReport[2] . ")) as countDay
          FROM " . $resultReport[0] . ", all_hospital_thai
          WHERE " . $resultReport[0] . "." . $resultReport[3] . " IS NOT NULL
          AND YEAR(" . $resultReport[0] . "." . $resultReport[3] . " ) = YEAR(NOW())
          AND MONTH(" . $resultReport[0] . "." . $resultReport[3] . " ) = MONTH(NOW())
          AND DAY(" . $resultReport[0] . "." . $resultReport[3] . " ) = DAY(NOW())
          AND " . $resultReport[0] . ".hsitecode = all_hospital_thai.hcode
          ) as resultReportDay ON ( 1 )";

          //            echo "<pre>$sqlResultReportTarget</pre>";
          //            echo "<pre>$sqlResultReportAll</pre>";
          //            echo "###############################################<br/>";
          //            echo "###############################################<br/>";
          //            echo "###############################################<br/>";

          if ($key == 'CCA-01') {
          $strHeadDeptMember = "ลงทะเบียนกลุ่มเสี่ยง (CCA-01)";
          } else if ($key == 'OV-01K') {
          array_push($dataAll, [
          'section', ['ตรวจรักษาพยาธิใบไม้ตับ (OV-01)', '', '', '', '', '']
          ]
          );
          $strHeadDeptMember = "OV-01K (Kato-Katz)";
          } else if ($key == 'OV-01P') {
          $strHeadDeptMember = "OV-01P (Parasep)";
          } else if ($key == 'OV-01F') {
          $strHeadDeptMember = "OV-01F (FECT)";
          } else if ($key == 'OV-01U') {
          $strHeadDeptMember = "OV-01U (Urine)";
          } else if ($key == 'OV-02') {
          $strHeadDeptMember = "ปรับเปลี่ยนพฤติกรรมเสี่ยง (OV-02)	";
          } else if ($key == 'OV-03') {
          $strHeadDeptMember = "ให้การรักษาพยาธิ (OV-03)	";
          } else if ($key == 'CCA-02') {
          $strHeadDeptMember = "ตรวจคัดกรองมะเร็งท่อน้ำดี (CCA-02)";
          } else if ($key == 'CCA-02.1') {
          $strHeadDeptMember = "ตรวจ CT/MRI (CCA-02.1)";
          } else if ($key == 'CCA-03') {
          //                array_push($dataAll, [
          //                        'section', ['ให้การรักษา', '', '', '', '', '']
          //                    ]
          //                );
          $strHeadDeptMember = "ให้การรักษา (CCA-03)";
          } else if ($key == 'CCA-03.1') {
          $strHeadDeptMember = "ติดตามผลการรักษา (CCA-03.1)";
          } else if ($key == 'CCA-04') {
          $strHeadDeptMember = "ตรวจพยาธิวิทยา (CCA-04)";
          } else if ($key == 'CCA-05') {
          $strHeadDeptMember = "ติดตามผลการรักษามะเร็งท่อน้ำดี (CCA-05)";
          }
          array_push($dataAll, [
          'section', [$strHeadDeptMember, 'ทั้งหมด', 'ปีนี้', 'เดือนนี้', 'สัปดาห์นี้', 'วันนี้']
          ]
          );

          $qryResultReportTarget = Yii::$app->db->createCommand($sqlResultReportTarget)->queryAll();
          $countAll = $qryResultReportTarget[0]['countAll'];
          $countYear = $qryResultReportTarget[0]['countYear'];
          $countMonth = $qryResultReportTarget[0]['countMonth'];
          $countYearWeekRegis = $qryResultReportTarget[0]['countYearWeek'];
          $countDay = $qryResultReportTarget[0]['countDay'];

          array_push($dataAll, [
          'name', ['&emsp;&emsp;จำนวนราย', $countAll, $countYear, $countMonth, $countYearWeekRegis, $countDay]
          ]
          );
          if($key!='CCA-01') {
          $qryResultReportAll = Yii::$app->db->createCommand($sqlResultReportAll)->queryAll();
          $countAll = $qryResultReportAll[0]['countAll'];
          $countYear = $qryResultReportAll[0]['countYear'];
          $countMonth = $qryResultReportAll[0]['countMonth'];
          $countYearWeekRegis = $qryResultReportAll[0]['countYearWeek'];
          $countDay = $qryResultReportAll[0]['countDay'];

          array_push($dataAll, [
          'name', ['&emsp;&emsp;จำนวนครั้ง', $countAll, $countYear, $countMonth, $countYearWeekRegis, $countDay]
          ]
          );
          }

          //            echo "<pre>$sqlResultReportTarget</pre>";
          //            echo "#####################################################################<br>";
          //            echo "#####################################################################<br>";
          //            echo "#####################################################################<br>";
          //            echo "<pre>$sqlResultReportAll</pre>";
          //            echo "#####################################################################<br>";
          //            echo "#####################################################################<br>";
          //            echo "#####################################################################<br>";
          //            echo "#####################################################################<br>";
          //            echo "#####################################################################<br>";
          //            echo "#####################################################################<br>";
          }
         *
         */
//        \yii\helpers\VarDumper::dump($dataAll,10,true);
        Yii::$app->response->format = \yii\web\Response::FORMAT_HTML;
        return $this->renderAjax('_ajaxReportOverview', [
                    'dataAll' => $dataAll,
                    'dataAllP' => $dataAllP,
        ]);
    }

    public function actionAjaxReportOv($fiscalYear, $ov01Level, $zone1 = null, $zone6 = null, $zone7 = null, $zone8 = null, $zone9 = null, $zone10 = null, $refresh = false, $type = null, $str = null) {
        ini_set('max_execution_time', 0);
        set_time_limit(0);
        ini_set('memory_limit', '512M');
        if (
                (!is_numeric($fiscalYear)) ||
                ($zone1 != null && $zone1 != 1) ||
                ($zone6 != null && $zone6 != 6) ||
                ($zone7 != null && $zone7 != 7) ||
                ($zone8 != null && $zone8 != 8) ||
                ($zone9 != null && $zone9 != 9) ||
                ($zone10 != null && $zone10 != 10)
        )
            return;

        if (1 || Yii::$app->keyStorage->get('frontend.domain') == 'cascap.in.th') {
            if (1 || Yii::$app->getRequest()->isAjax) {

                $thaiMonth = ['',
                    1 => ['abvt' => 'ม.ค.', 'full' => 'มกราคม'],
                    2 => ['abvt' => 'ก.พ.', 'full' => 'กุมภาพันธ์'],
                    3 => ['abvt' => 'มี.ค.', 'full' => 'มีนาคม'],
                    4 => ['abvt' => 'เม.ย.', 'full' => 'เมษายน'],
                    5 => ['abvt' => 'พ.ค.', 'full' => 'พฤษภาคม'],
                    6 => ['abvt' => 'มิ.ย.', 'full' => 'มิถุนายน'],
                    7 => ['abvt' => 'ก.ค.', 'full' => 'กรกฎาคม'],
                    8 => ['abvt' => 'ส.ค.', 'full' => 'สิงหาคม'],
                    9 => ['abvt' => 'ก.ย.', 'full' => 'กันยายน'],
                    10 => ['abvt' => 'ต.ค.', 'full' => 'ตุลาคม'],
                    11 => ['abvt' => 'พ.ย.', 'full' => 'พฤศจิกายน'],
                    12 => ['abvt' => 'ธ.ค.', 'full' => 'ธันวาคม']
                ];

                $fiscalYear = intval($fiscalYear);

                $listMonth = function($fiscalYear) use ($fiscalYear) {
                    $f = ($fiscalYear - 1) . '-10-01';
                    $t = $fiscalYear . '-09-30';
                    $aml = array();
                    list($sYear, $sMonth, $sDay) = explode("-", $f);
                    list($eYear, $eMonth, $eDay) = explode("-", $t);
                    $startMonth = $sMonth;
                    do {
                        $mk = mktime(0, 0, 0, $sMonth, 1, $sYear);
                        $mY = date("Ym", $mk);
                        $endFormat = "Y-m-t";
                        if ($sMonth == $startMonth) {
                            $mk = mktime(0, 0, 0, $sMonth, $sDay, $sYear);
                        } else if ($mY == $eYear . $eMonth) {
                            $endFormat = "Y-m-" . $eDay;
                        }
                        //echo date("Y-m-d", $mk)    . " to " . date($endFormat, $mk) . "<br />";
                        array_push($aml, array('fromDate' => date("Y-m-d", $mk), 'toDate' => date($endFormat, $mk), 'Ym' => date("Y-m", $mk)));
                        $sMonth++;
                    } while ($eYear . $eMonth > $mY);
                    return $aml;
                };

                $allMonthList = $listMonth($fiscalYear);
//                var_dump($allMonthList);
//                return;

                $sqlAddControlTable = 'CREATE TABLE IF NOT EXISTS report_control_ov(
                    `reportid`  int(5) NOT NULL AUTO_INCREMENT ,
                    `fiscalyear`  varchar(4) NULL ,
                    `lastcal`  datetime NULL ,
                    PRIMARY KEY (`reportid`)
                );';
                Yii::$app->db->createCommand($sqlAddControlTable)->execute();

                $sqlControl = 'SELECT reportid,lastcal FROM report_control_ov WHERE fiscalyear = ' . $fiscalYear . ' LIMIT 0,1';
                $queryControl = Yii::$app->db->createCommand($sqlControl)->queryOne();


                if ($refresh == 'true' || $queryControl == null || ((strtotime(date('Y-m-d H:i:s')) - strtotime($queryControl['lastcal'])) > 86400)) {

                    if ($queryControl == null) {

                        $sqlReportId = 'INSERT INTO report_control_ov(fiscalyear,lastcal) VALUES (' . $fiscalYear . ',NOW())';
                        Yii::$app->db->createCommand($sqlReportId)->execute();

                        $reportId = Yii::$app->db->getLastInsertID();
                    } else {
                        //echo '';

                        $reportId = $queryControl['reportid'];
                        $sqlReportId = 'UPDATE report_control_ov SET lastcal = NOW() WHERE reportid = ' . $reportId;
                        Yii::$app->db->createCommand($sqlReportId)->execute();
                    }

                    $sqlProcedure = 'CALL cascap_report.spOv(' . $fiscalYear . ',' . $reportId . ')';
                    Yii::$app->dbreport1->createCommand($sqlProcedure)->execute();

                    unset($queryControl);
                }
                //return;

                $queryControl = Yii::$app->db->createCommand($sqlControl)->queryOne();

                $lastcal = $queryControl['lastcal'];
                //return;

                /*                 * ************************* query section ************************** */

                $heading = [
                    'heading' => 'พื้นที่'
                ];
                //echo $queryControl['reportid'];

                $queryAllZone = array(['section' => 'เขตบริการสุขภาพ']);
                array_push($queryAllZone, $heading);

                $sumAllZone = ['label' => 'รวม', 'sumRegis' => 0];

                for ($z_code = 1; $z_code <= 10; ++$z_code) {
                    if ($z_code > 1 && $z_code < 6)
                        continue;

                    $queryAllZone['zone_' . $z_code]['label'] = 'เขต ' . $z_code;
                    $queryAllZone['zone_' . $z_code]['sumRegis'] = 0;

                    foreach ($allMonthList as $key => $eachMonth) {

                        $sqlZone = 'SELECT
                            SUM(registerCount) AS registerCount,
                            SUM(kCount) AS ov01KCount,
                            SUM(kPosCount) AS infectedKCount,
                            SUM(pCount) AS ov01PCount,
                            SUM(pPosCount) AS infectedPCount,
                            SUM(fCount) AS ov01FCount,
                            SUM(fPosCount) AS infectedFCount,
                            SUM(uCount) AS ov01UCount,
                            SUM(uPosCount) AS infectedUCount,
                            SUM(ov02Count) AS ov02Count,
                            SUM(ov03Count) AS ov03Count
                        FROM report_ov_backend
                        WHERE rptid = ' . $queryControl['reportid'] . '
                        AND zone_code=' . $z_code . '
                        AND ym="' . $eachMonth['Ym'] . '"';

                        //echo $sqlZone;

                        $queryZone = Yii::$app->db->createCommand($sqlZone)->queryOne();


                        $queryAllZone['zone_' . $z_code][$eachMonth['Ym']] = $queryZone;

                        $queryAllZone['zone_' . $z_code]['sumRegis'] += $queryZone['registerCount'];



                        $sumAllZone['sumRegis'] += $queryZone['registerCount'];


                        foreach ($queryZone as $colIndex => $col) {
                            $sumAllZone[$eachMonth['Ym']][$colIndex] += $col;
                        }
                    }
                }

//                var_dump($queryAllZone);
//                return;





                array_push($queryAllZone, $sumAllZone);

//                var_dump($sumAllZone);

                if ($zone1 == null && $zone6 == null && $zone7 == null && $zone8 == null && $zone9 == null && $zone10 == null) {
                    \Yii::$app->response->format = \yii\web\Response::FORMAT_HTML;
                    return $this->renderAjax('_ajaxReportOv', [
                                'fiscalYear' => $fiscalYear,
                                'allMonthList' => $allMonthList,
                                'lastcal' => $lastcal,
                                'thaimonth' => $thaiMonth,
                                'ovAllZone' => $queryAllZone
                    ]);
                }



                //province in zone

                array_push($queryAllZone, array('section' => 'รายจังหวัด'));



                $sqlBody = 'SUM(registerCount) AS registerCount,
                    SUM(registerCount) AS registerCount,
                    SUM(kCount) AS ov01KCount,
                    SUM(kPosCount) AS infectedKCount,
                    SUM(pCount) AS ov01PCount,
                    SUM(pPosCount) AS infectedPCount,
                    SUM(fCount) AS ov01FCount,
                    SUM(fPosCount) AS infectedFCount,
                    SUM(uCount) AS ov01UCount,
                    SUM(uPosCount) AS infectedUCount,
                    SUM(ov02Count) AS ov02Count,
                    SUM(ov03Count) AS ov03Count
                FROM report_ov_backend WHERE rptid = ' . $queryControl['reportid'] . '
                AND zone_code = ';

                $sqlHead = 'SELECT provincecode,';

                $sqlMonth = ' AND ym = "';

                $sqlTail = '" GROUP BY provincecode ORDER BY province';

                if ($ov01Level == 1 || $ov01Level == 0) {
                    for ($z_code = 1; $z_code <= 10; ++$z_code) {
                        if ($z_code > 1 && $z_code < 6)
                            continue;
                        if ($z_code == 1 && $zone1 != 1)
                            continue;
                        else if ($z_code == 6 && $zone6 != 6)
                            continue;
                        else if ($z_code == 7 && $zone7 != 7)
                            continue;
                        else if ($z_code == 8 && $zone8 != 8)
                            continue;
                        else if ($z_code == 9 && $zone9 != 9)
                            continue;
                        else if ($z_code == 10 && $zone10 != 10)
                            continue;


                        $sumAllProvince = ['label' => 'รวม', 'sumRegis' => 0];

                        $sqlDataRow = 'SELECT provincecode,province
                        FROM report_ov_backend
                        WHERE rptid = ' . $queryControl['reportid'] . '
                        AND zone_code = ' . $z_code . '
                        GROUP BY province';

                        $queryDataRow = Yii::$app->db->createCommand($sqlDataRow)->queryAll();

                        if (!$queryDataRow == null) {
                            array_push($queryAllZone, array('separator' => 'เขต ' . $z_code));

                            $heading['heading'] = 'จังหวัด';
                            array_push($queryAllZone, $heading);

                            foreach ($queryDataRow as $key => $row) {
                                $queryAllZone['province_' . $row['provincecode']]['label'] = $row['province'];
                                $queryAllZone['province_' . $row['provincecode']]['sumRegis'] = 0;
                                foreach ($allMonthList as $key => $eachMonth) {
                                    $queryAllZone['province_' . $row['provincecode']][$eachMonth['Ym']] = [];
                                    $sumAllProvince[$eachMonth['Ym']] = [];
                                }
                            }

                            foreach ($allMonthList as $key => $eachMonth) {
                                $sql = $sqlHead . $sqlBody . $z_code . $sqlMonth . $eachMonth['Ym'] . $sqlTail;
                                //echo $sql;
                                $query = Yii::$app->db->createCommand($sql)->queryAll();

                                foreach ($query as $keyQuery => $row) {
                                    $queryAllZone['province_' . $row['provincecode']][$eachMonth['Ym']] = $row;
                                    $queryAllZone['province_' . $row['provincecode']]['sumRegis'] += $row['registerCount'];
                                    $sumAllProvince['sumRegis'] += $row['registerCount'];
                                    foreach ($row as $indexQuery => $col) {

                                        $sumAllProvince[$eachMonth['Ym']][$indexQuery] += (0 + $col);
                                    }
                                }
                            }

                            array_push($queryAllZone, $sumAllProvince);
                        }
                    }
                }


                //amphur in zone
                array_push($queryAllZone, array('section' => 'รายอำเภอ'));

                $sqlHead = 'SELECT CONCAT(provincecode,"_",amphurcode) AS pamphurcode,provincecode,';

                $sqlMonth = ' AND ym = "';

                $sqlTail = '" GROUP BY provincecode, amphurcode ORDER BY province, amphurcode';


                for ($z_code = 1; $z_code <= 10; ++$z_code) {
                    if ($z_code > 1 && $z_code < 6)
                        continue;
                    if ($z_code == 1 && $zone1 != 1)
                        continue;
                    else if ($z_code == 6 && $zone6 != 6)
                        continue;
                    else if ($z_code == 7 && $zone7 != 7)
                        continue;
                    else if ($z_code == 8 && $zone8 != 8)
                        continue;
                    else if ($z_code == 9 && $zone9 != 9)
                        continue;
                    else if ($z_code == 10 && $zone10 != 10)
                        continue;


                    $sqlDataRow = 'SELECT CONCAT(provincecode,"_",amphurcode) AS pamphurcode,amphur,provincecode,province
                        FROM report_ov_backend
                        WHERE rptid = ' . $queryControl['reportid'] . '
                        AND zone_code = ' . $z_code . '
                        GROUP BY provincecode,amphurcode
                        ORDER BY province,amphurcode';

                    $queryDataRow = Yii::$app->db->createCommand($sqlDataRow)->queryAll();
//                    var_dump($queryAllZone);
                    //var_dump($queryDataRow);

                    if (!$queryDataRow == null) {
                        $province = '';
                        $provincecode = null;
                        if ($ov01Level == 2 || $ov01Level == 0) {
                            $heading['heading'] = 'อำเภอ';

                            foreach ($queryDataRow as $key => $row) {
                                if ($province != $row['province']) {
                                    if (isset($sumAllAmphur) && isset($provincecode)) {
                                        $queryAllZone['sumAllAmphur_' . $provincecode] = $sumAllAmphur;
                                    }
                                    $province = $row['province'];
                                    $provincecode = $row['provincecode'];
                                    $sumAllAmphur = ['label' => 'รวม', 'sumRegis' => 0];
                                    array_push($queryAllZone, ['separator' => 'เขต ' . $z_code . ' / จ.' . $row['province']]);
                                    array_push($queryAllZone, $heading);
                                }
                                $queryAllZone['amphur_' . $row['pamphurcode']]['label'] = $row['amphur'];
                                $queryAllZone['amphur_' . $row['pamphurcode']]['sumRegis'] = 0;
                                foreach ($allMonthList as $key => $eachMonth) {
                                    $queryAllZone['amphur_' . $row['pamphurcode']][$eachMonth['Ym']] = [];
                                    $sumAllAmphur[$eachMonth['Ym']] = [];
                                }
                            }
                            $queryAllZone['sumAllAmphur_' . $provincecode] = $sumAllAmphur;
                            //echo '++++'.$provincecode;





                            foreach ($allMonthList as $key => $eachMonth) {
                                $sql = $sqlHead . $sqlBody . $z_code . $sqlMonth . $eachMonth['Ym'] . $sqlTail;
                                //echo $sql;
                                $query = Yii::$app->db->createCommand($sql)->queryAll();

                                foreach ($query as $keyQuery => $row) {

                                    $queryAllZone['amphur_' . $row['pamphurcode']][$eachMonth['Ym']] = $row;
                                    $queryAllZone['amphur_' . $row['pamphurcode']]['sumRegis'] += $row['registerCount'];

                                    $queryAllZone['sumAllAmphur_' . $row['provincecode']]['sumRegis'] += intval($row['registerCount']);

                                    foreach ($row as $indexQuery => $col) {
                                        if ($indexQuery == 'pamphurcode' || $indexQuery == 'provincecode')
                                            continue;
                                        $queryAllZone['sumAllAmphur_' . $row['provincecode']][$eachMonth['Ym']][$indexQuery] += (0 + $col);
                                    }
                                }
                            }
//                        if($z_code=8){
//                            var_dump($queryAllZone);
//
//                        }
                        }
                    }
                    //var_dump($queryAllZone);
                }

                array_push($queryAllZone, array('section' => 'ระดับสถานบริการ'));

                $sqlHead = 'SELECT sitecode,CONCAT(provincecode,"_",amphurcode) AS pamphurcode,';

                $sqlMonth = ' AND ym = "';

                $sqlTail = '" GROUP BY sitecode ORDER BY province,amphurcode,FIELD(hlevel,1,2,12,11,10,5,17,6,15,7,4,18,3,13,16,8,80,""),sitecode+0';


                for ($z_code = 1; $z_code <= 10; ++$z_code) {
                    if ($z_code > 1 && $z_code < 6)
                        continue;
                    if ($z_code == 1 && $zone1 != 1)
                        continue;
                    else if ($z_code == 6 && $zone6 != 6)
                        continue;
                    else if ($z_code == 7 && $zone7 != 7)
                        continue;
                    else if ($z_code == 8 && $zone8 != 8)
                        continue;
                    else if ($z_code == 9 && $zone9 != 9)
                        continue;
                    else if ($z_code == 10 && $zone10 != 10)
                        continue;


                    $sqlDataRow = 'SELECT sitecode,`name`,hlevel,levelname,CONCAT(provincecode,"_",amphurcode) AS pamphurcode,amphur,province
                        FROM report_ov_backend
                        WHERE rptid = ' . $queryControl['reportid'] . '
                        AND zone_code = ' . $z_code . '
                        GROUP BY sitecode
                        ORDER BY province,amphurcode,FIELD(hlevel,1,2,12,11,10,5,17,6,15,7,4,18,3,13,16,8,80,""),sitecode+0';
                    //echo $sqlDataRow;
                    $queryDataRow = Yii::$app->db->createCommand($sqlDataRow)->queryAll();
                    //var_dump($queryDataRow);

                    if (!$queryDataRow == null) {
                        $province = '';
                        $amphur = '';
                        $levelname = '';
                        $pamphurcode = null;
                        if ($ov01Level == 3 || $ov01Level == 0) {
                            $heading['heading'] = 'สถานบริการ';

                            foreach ($queryDataRow as $key => $row) {
                                if ($amphur != $row['amphur'] || $province != $row['province']) {
                                    if (isset($sumAllSite) && isset($pamphurcode)) {
                                        $queryAllZone['sumAllSite_' . $pamphurcode] = $sumAllSite;
                                    }
                                    $levelname = '';
                                    $amphur = $row['amphur'];
                                    $pamphurcode = $row['pamphurcode'];
                                    $province = $row['province'];
                                    array_push($queryAllZone, ['separator' => 'เขต ' . $z_code . ' / จ.' . $row['province'] . ' / อ.' . $row['amphur']]);
                                    array_push($queryAllZone, $heading);
                                }
                                if ($levelname != $row['levelname']) {
                                    $levelname = $row['levelname'];
                                    array_push($queryAllZone, ['typesplit' => $row['levelname']]);
                                }

                                $sumAllSite = ['label' => 'รวม', 'sumRegis' => 0];

                                $queryAllZone['sitecode_' . $row['sitecode']]['label'] = $row['name'];
                                $queryAllZone['sitecode_' . $row['sitecode']]['sumRegis'] = 0;

                                foreach ($allMonthList as $key => $eachMonth) {
                                    $queryAllZone['sitecode_' . $row['sitecode']][$eachMonth['Ym']] = [];
                                    $sumAllSite[$eachMonth['Ym']] = [];
                                }
                            }
                            $queryAllZone['sumAllSite_' . $pamphurcode] = $sumAllSite;

                            foreach ($allMonthList as $key => $eachMonth) {
                                $sql = $sqlHead . $sqlBody . $z_code . $sqlMonth . $eachMonth['Ym'] . $sqlTail;
                                //echo $sql;
                                $query = Yii::$app->db->createCommand($sql)->queryAll();
                                //var_dump($query);



                                foreach ($query as $keyQuery => $row) {


                                    $queryAllZone['sitecode_' . $row['sitecode']][$eachMonth['Ym']] = $row;

                                    $queryAllZone['sitecode_' . $row['sitecode']]['sumRegis'] += $row['registerCount'];

                                    $queryAllZone['sumAllSite_' . $row['pamphurcode']]['sumRegis'] += intval($row['registerCount']);

                                    foreach ($row as $indexQuery => $col) {
                                        if ($indexQuery == 'amphurcode')
                                            continue;
                                        $queryAllZone['sumAllSite_' . $row['pamphurcode']][$eachMonth['Ym']][$indexQuery] += (0 + $col);
                                    }
                                }
                            }
                        }
                    }
                }
                //var_dump($queryAllZone);
                \Yii::$app->response->format = \yii\web\Response::FORMAT_HTML;
                return $this->renderAjax('_ajaxReportOv', [
                            'fiscalYear' => $fiscalYear,
                            'allMonthList' => $allMonthList,
                            'lastcal' => $lastcal,
                            'thaimonth' => $thaiMonth,
                            'ovAllZone' => $queryAllZone
                ]);
            }
        }
    }

    public function actionAjaxReport84($fromDate, $toDate, $refresh = null) {

        $sqlAddControlTable = 'CREATE TABLE IF NOT EXISTS project84_report_control (
            report_id INT(5) NOT NULL AUTO_INCREMENT,
            fromDate  DATE DEFAULT NULL,
            toDate  DATE DEFAULT NULL,
            lastcal datetime DEFAULT NULL,
            PRIMARY KEY (report_id)
        );';
        \Yii::$app->db->createCommand($sqlAddControlTable)->execute();

        $sqlControl = 'SELECT report_id,fromDate,toDate,lastcal
            FROM project84_report_control
            WHERE ' .
                (
                ( $fromDate == '2015-10-01' && $toDate == date('Y-m-d') ) ?
                ' report_id=1 ' :
                ' fromDate = "' . $fromDate . '" AND toDate = "' . $toDate . '" '
                ) .
                ' LIMIT 0,1';
        $queryControl = Yii::$app->db->createCommand($sqlControl)->queryOne();

        $queryControlTccbot = $queryControl;

        $reportId = $queryControl['report_id'];

        if ($refresh == 'true' || $queryControl == null || ($queryControl['report_id'] == 1 && $queryControl['toDate'] != date('Y-m-d') )) {
            if ($queryControl == null) {
                $sqlReportId = 'INSERT INTO project84_report_control(fromDate,toDate,lastcal) VALUES("' . $fromDate . '","' . $toDate . '",NOW())';
                Yii::$app->db->createCommand($sqlReportId)->execute();
                $reportId = Yii::$app->db->getLastInsertID();
            } else {
                $reportId = $queryControl['report_id'];
            }
            self::calculateReport84($fromDate, $toDate, $reportId);
        }
        $queryControl = Yii::$app->db->createCommand($sqlControl)->queryOne();


        $sql1 = 'SELECT
            zone,
            province,
            amphur,
            tambon,
            address,
            tccbot,
            nall,
            icf,
            cca01,
            ov,
            ovpos,
            ov02,
            ov03
        FROM project84_report_sum
        WHERE report_id=' . $reportId . '
        GROUP BY address';

        $sql2 = 'SELECT
            zone AS zone_2,
            provincecode,
            province AS province_2,
            nall AS nall_2,
            cca02,
            abnormal,
            suspected,
            ctmri,
            cca,
			treated
        FROM project84_report_sum_province
        WHERE report_id=' . $reportId . '
        ';

        $sql3 = 'SELECT
            sitecode,
            hospital,
            nall AS nall_3,
            treatment,
            all_surgery,
            c_surgery,
            p_surgery,
            liver,
            liverhilar,
            hilar,
            bypass,
            elbx,
            ndbx,
            whipple,
            chemo_all,
            chemo_adjuvant,
            ptbd,
            endoscopic,
            medication
        FROM project84_report_sum_hospital
        WHERE  report_id=' . $reportId . '
        ';

        $query1 = Yii::$app->db->createCommand($sql1)->queryAll();
        $query2 = Yii::$app->db->createCommand($sql2)->queryAll();
        $query3 = Yii::$app->db->createCommand($sql3)->queryAll();
        if (!isset($_GET['sort'])) {
//            ArrayHelper::multisort($query1,['zone','province'],SORT_ASC,SORT_NUMERIC);
//            ArrayHelper::multisort($query2,['zone_2','province'],SORT_ASC,SORT_NUMERIC);
//            ArrayHelper::multisort($query3,['sitecode','province'],SORT_ASC,SORT_NUMERIC);
            ArrayHelper::multisort($query1, ['ov', 'zone', 'province'], [SORT_DESC, SORT_ASC, SORT_ASC], SORT_NUMERIC);
            ArrayHelper::multisort($query2, ['cca02', 'zone_2', 'province'], [SORT_DESC, SORT_ASC, SORT_ASC], SORT_NUMERIC);
            ArrayHelper::multisort($query3, ['all_surgery', 'sitecode', 'province'], [SORT_DESC, SORT_ASC, SORT_ASC], SORT_NUMERIC);
        }

        $primaryDataProvider = new ArrayDataProvider([
            //$primaryDataProvider = new ActiveDataProvider([
            //'query' => $qry,
            'allModels' => $query1,
            'sort' => [
                'attributes' => [
                    'zone' => [
                        'default' => SORT_ASC
                    ],
                    'province',
                    'amphur',
                    'tambon',
                    'tccbot',
                    'nall',
                    'icf',
                    'cca01',
                    'ov',
                    'ovpos',
                    'ov02',
                    'ov03',
                    'progress' => [
                        'asc' => [
                            'ov' => SORT_ASC
                        ],
                        'desc' => [
                            'ov' => SORT_DESC
                        ],
                    ]
                ]
            ],
            'pagination' => false,
        ]);

        $secondaryDataProvider = new ArrayDataProvider([
//        $secondaryDataProvider = new \yii\data\SqlDataProvider([
//            'sql' => $query2,
            'allModels' => $query2,
            'sort' => [
                'attributes' => [
                    'zone_2' => [
                        'default' => SORT_ASC,
                    ],
                    'province_2' => [
                        'default' => SORT_ASC,
                    ],
                    'nall_2',
                    'cca02',
                    'abnormal',
                    'suspected',
                    'ctmri',
                    'cca',
                    'treated',
                    'progress' => [
                        'asc' => [
                            'cca02' => SORT_ASC
                        ],
                        'desc' => [
                            'cca02' => SORT_DESC
                        ],
                    ]
                ],
            ],
            'pagination' => false,
        ]);

        $model2 = $secondaryDataProvider->getModels();

        //$tertiaryDataProvider = new \yii\data\SqlDataProvider([
        $tertiaryDataProvider = new ArrayDataProvider([
            //'sql' => $query3,
            'allModels' => $query3,
            'sort' => [
                'attributes' => [
                    'sitecode' => [
                        'default' => SORT_ASC
                    ],
                    'hospital',
                    'nall_3',
                    'treatment',
                    'all_surgery',
                    'c_surgery',
                    'p_surgery',
                    'liver',
                    'liverhilar',
                    'hilar',
                    'bypass',
                    'elbx',
                    'whipple',
                    'ndbx',
                    'chemo_all',
                    'chemo_adjuvant',
                    'ptbd',
                    'endoscopic',
                    'medication'
                ],
            ],
            'pagination' => false,
        ]);

        $sumOvSqlAll = 'SELECT SUM(ov) AS ov FROM project84_report_sum WHERE report_id = 1';
        $sumOvQryAll = Yii::$app->db->createCommand($sumOvSqlAll)->queryOne();

        $sumOvSql = 'SELECT SUM(ov) AS ov FROM project84_report_sum WHERE report_id = ' . $reportId;
        $sumOvQry = Yii::$app->db->createCommand($sumOvSql)->queryOne();

        $sumCca02SqlAll = 'SELECT SUM(cca02) AS cca02 FROM project84_report_sum_province WHERE report_id = 1';
        $sumCca02QryAll = Yii::$app->db->createCommand($sumCca02SqlAll)->queryOne();

        $sumCca02Sql = 'SELECT SUM(cca02) AS cca02 FROM project84_report_sum_province WHERE report_id = ' . $reportId;
        $sumCca02Qry = Yii::$app->db->createCommand($sumCca02Sql)->queryOne();

        $summationTreatmentAllSql = 'SELECT
            SUM(treatment) AS treatment,
            SUM(all_surgery) AS all_surgery,
            SUM(c_surgery) AS c_surgery,
            SUM(p_surgery) AS p_surgery
        FROM project84_report_sum_hospital
        WHERE report_id = 1';
        $summationTreatmentAllQry = Yii::$app->db->createCommand($summationTreatmentAllSql)->queryOne();

        $summationTreatmentRidSql = 'SELECT
            SUM(treatment) AS treatment,
            SUM(all_surgery) AS all_surgery,
            SUM(c_surgery) AS c_surgery,
            SUM(p_surgery) AS p_surgery
        FROM project84_report_sum_hospital
        WHERE report_id = ' . $reportId;
        $summationTreatmentRidQry = Yii::$app->db->createCommand($summationTreatmentRidSql)->queryOne();

        $sumOvposSql = 'SELECT SUM(ovpos) AS ovpos FROM project84_report_sum WHERE report_id = 1'; //.$reportId;
        $sumOvposQry = Yii::$app->db->createCommand($sumOvposSql)->queryOne();

        $usOverallSql = 'SELECT
            SUM(cca02) AS cca02,
            SUM(abnormal) AS abnormal,
            SUM(suspected) AS suspected,
            SUM(ctmri) AS ctmri,
            SUM(cca) AS cca,
			SUM(treated) AS treated
        FROM project84_report_sum_province
        WHERE report_id = 1';
        $usOverallQry = Yii::$app->db->createCommand($usOverallSql)->queryOne();



        return $this->renderAjax('_project84_report', [
                    'primaryDataProvider' => $primaryDataProvider,
                    'secondaryDataProvider' => $secondaryDataProvider,
                    'tertiaryDataProvider' => $tertiaryDataProvider,
                    'model2' => $model2,
                    'sum' => [
                        'ovall' => intval($sumOvQryAll['ov']),
                        'ov' => intval($sumOvQry['ov']),
                        'cca02all' => intval($sumCca02QryAll['cca02']),
                        'cca02' => intval($sumCca02Qry['cca02']),
                        'treatmentall' => $summationTreatmentAllQry,
                        'treatmentrid' => $summationTreatmentRidQry,
                        'ovpos' => intval($sumOvposQry['ovpos'])
                    ],
                    'usoverall' => $usOverallQry,
                    'date' => [
                        'f' => $fromDate,
                        't' => $toDate
                    ],
                    'lastcal' => $queryControl['lastcal']
        ]);
    }

    public function calculateReport84($fromDate, $toDate, $reportId) {
        if ($fromDate == '2015-10-01' && $toDate == date('Y-m-d')) {
            $reportId = 1;
            $sqlReportId = 'UPDATE project84_report_control SET fromDate = "2015-10-01",toDate = "' . date('Y-m-d') . '" WHERE report_id = ' . $reportId;
            Yii::$app->db->createCommand($sqlReportId)->execute();
        }

        $sqlReportId = 'UPDATE project84_report_control SET lastcal = NOW() WHERE report_id = ' . $reportId;
        Yii::$app->db->createCommand($sqlReportId)->execute();
        $sqlProcedure = 'CALL sp84("' . $fromDate . '","' . $toDate . '",' . $reportId . ')';
        Yii::$app->db->createCommand($sqlProcedure)->execute();

        // HIS part
        $queryTccAddress = Yii::$app->db->createCommand('SELECT address FROM project84_zone')->queryAll();
        foreach ($queryTccAddress as $key => $row) {
            $tccbotRow = Yii::$app->dbbot->createCommand('SELECT COUNT(*) AS ncount
            FROM person
            WHERE sitecode IN (SELECT hcode FROM buffe_webservice.all_hospital_thai WHERE addresscode="' . $row['address'] . '")')->queryOne();
            $queryTccAddress[$key]['tccbot'] = 0;
            Yii::$app->db->createCommand('UPDATE project84_report_sum SET tccbot = ' . $tccbotRow['ncount'] . ' WHERE report_id=' . $reportId . ' AND address="' . $row['address'] . '"')->execute();
        }
    }

    public function actionReport84Default() {

        Yii::$app->db->createCommand('REPLACE INTO project84_report_sum_log
        SELECT
            DATE_FORMAT(NOW(),"%Y-%m-%d") AS log_date,
            SUM(tccbot) AS tccbot,
            SUM(nall) AS nall,
            SUM(icf) AS icf,
            SUM(ov) AS ov,
            SUM(ovpos) AS ovpos,
            SUM(ov02) AS ov02,
            SUM(ov03) AS ov03,
            SUM(cca01) AS cca01
        FROM
            project84_report_sum
        WHERE report_id = 1')->execute();

        Yii::$app->db->createCommand('REPLACE INTO project84_report_sum_log_province
        SELECT
            DATE_FORMAT(NOW(),"%Y-%m-%d") AS log_date,
            SUM(nall) AS nall,
            SUM(cca02) AS cca02,
            SUM(abnormal) AS abnormal,
            SUM(suspected) AS suspected,
            SUM(ctmri) AS ctmri,
            SUM(cca) AS cca,
			SUM(treated) AS treated
        FROM
            project84_report_sum_province
        WHERE report_id = 1')->execute();

        Yii::$app->db->createCommand('REPLACE INTO project84_report_sum_log_hospital
        SELECT
            DATE_FORMAT(NOW(),"%Y-%m-%d") AS log_date,
            SUM(nall) AS nall,
            SUM(treatment) AS treatment,
            SUM(all_surgery) AS all_surgery,
            SUM(c_surgery) AS c_surgery,
            SUM(p_surgery) AS p_surgery,
            SUM(liver) AS liver,
            SUM(liverhilar) AS liverhilar,
            SUM(hilar) AS hilar,
            SUM(bypass) AS bypass,
            SUM(elbx) AS elbx,
            SUM(ndbx) AS ndbx,
            SUM(whipple) AS whipple,
            SUM(chemo_all) AS chemo_all,
            SUM(chemo_adjuvant) AS chemo_adjuvant,
            SUM(ptbd) AS ptbd,
            SUM(endoscopic) AS endoscopic,
            SUM(medication) AS medication
        FROM
            project84_report_sum_hospital
        WHERE report_id = 1')->execute();

        self::calculateReport84('2015-10-01', date('Y-m-d'), 1);
        return json_encode(['refresh' => 'ok']);
    }

    public function actionReport84DetailProvince($report, $provincecode = null) {
        if ($report != 2 || $provincecode == null)
            return;
        $provincecode = intval($provincecode);

        $startDate = "2015-10-01";
        $endDate = "2016-09-030";
        $sqlProvince = 'SELECT
            provincecode,
            province,
            SUM(cca02) AS cca02
        FROM (
            SELECT
                provincecode,
                province,
                COUNT(DISTINCT ptid) AS cca02
            FROM
                tb_data_3
            INNER JOIN all_hospital_thai ON tb_data_3.hsitecode = all_hospital_thai.hcode
            WHERE
                (
                    f2v1 BETWEEN "' . $startDate . '"
                    AND "' . $endDate . '"
                )
            AND hsitecode IS NOT NULL
            AND hsitecode != 0
			AND hsitecode NOT LIKE "A%"
			AND hsitecode NOT LIKE "Z%"
            AND hsitecode NOT LIKE "90%"
            AND hsitecode NOT LIKE "91%"
            AND hsitecode NOT LIKE "92%"
            AND provincecode = ' . $provincecode . '
            GROUP BY hsitecode
        ) AS t';
        $queryProvince = Yii::$app->db->createCommand($sqlProvince)->queryOne();

        //print_r($queryProvince);

        $sqlHospital = 'SELECT
            hsitecode,
            name,
            COUNT(DISTINCT ptid) AS cca02
        FROM
            tb_data_3
        INNER JOIN all_hospital_thai ON tb_data_3.hsitecode = all_hospital_thai.hcode
        WHERE
            (
                f2v1 BETWEEN "' . $startDate . '"
                AND "' . $endDate . '"
            )
        AND rstat != 0
        AND rstat != 3
        AND hsitecode IS NOT NULL
        AND hsitecode != 0
		AND hsitecode NOT LIKE "A%"
		AND hsitecode NOT LIKE "Z%"
        AND hsitecode NOT LIKE "90%"
	    AND hsitecode NOT LIKE "91%"
        AND hsitecode NOT LIKE "92%"
        AND provincecode = ' . $provincecode . '
        GROUP BY hsitecode
        HAVING COUNT(DISTINCT ptid) > 0
        ORDER BY FIELD(code4,1,2,12,11,10,5,17,6,15,7,4,18,3,13,16,8,80,""),code4,hsitecode';
        $queryHospital = Yii::$app->db->createCommand($sqlHospital)->queryAll();

        //print_r($queryHospital);

        $sqlAmphur = 'SELECT
            aht.provincecode,
            aht.province,
            aht.amphurcode,
            aht.amphur,
            aht.tamboncode,
            aht.tambon,
            aht.hcode,
            aht.`name`,
            cca02,
            f2v2a1,
            f2v2a1b2,
            f2v6a3b1,
            f2p1v2,
            f2p1v3,
            regis,
            f3v1
           
        FROM (
            SELECT
                COUNT(ptid) AS cca02,
                COUNT(hptcode) AS regis,
                add1n6code,
                SUM(IF(f2v2a1=1,1,0)) AS f2v2a1,
                SUM(IF(f2v2a1b2=1 OR f2v2a1b2=2 OR f2v2a1b2=3,1,0)) AS f2v2a1b2,
                COUNT(distinct IF(f2v6a3=1 and f2v6a3b1=1,ptid,NULL)) as f2v6a3b1,
                SUM(IF((f2p1v2=1 OR f2p1v2=2)AND(f2v6a3b1=1) ,1,0)) AS f2p1v2,
                SUM(IF((f2v6a3b1=1)AND(f2p1v2=1 OR f2p1v2=2) AND (f2p1v3=1 OR f2p1v3=2 OR f2p1v3=3),1, 0)) AS f2p1v3,
                SUM(IF((f3v1 IS NOT NULL) AND (f2p1v3=1 OR f2p1v3=2 OR f2p1v3=3) AND (f2p1v2=1 OR f2p1v2=2) AND(f2v6a3b1=1),1,0)) AS f3v1,
                
                LEFT(add1n6code, 2) AS provincecode,
                SUBSTR(add1n6code FROM 3 FOR 2) AS amphurcode,
                SUBSTR(add1n6code FROM 5 FOR 2) AS tamboncode
            FROM (
                SELECT
                    ptid,
                    f2v2a1,
                    f2v2a1b2,
                    f2v6a3b1,
                    f2p1v2,
                    f2p1v3,
                    f2v6a3,
                    hptcode,
                    f3v1,
                    (SELECT add1n6code FROM tb_data_1 WHERE add1n6code IS NOT NULL AND id = t.ptid LIMIT 1) AS add1n6code
                FROM (
                    SELECT
                        DISTINCT t3.ptid AS ptid,
                        t3.hptcode AS hptcode,
                        f2v2a1,
                        f2v2a1b2,
                        f2v6a3b1,
                        f2p1v2,
                        f2v6a3,
                        f2p1v3,
                        f3v1
                    FROM
                        tb_data_3 as t3 LEFT JOIN tb_data_4 as t4 ON t3.ptid=t4.ptid
                        LEFT JOIN tb_data_7 t7 ON t3.ptid=t7.ptid 
                    WHERE
                        (
                            f2v1 BETWEEN "' . $startDate . '"
                            AND "' . $endDate . '"
                        )
                    AND t3.sitecode IS NOT NULL
                    AND t3.ptid IS NOT NULL
                    AND t3.rstat != 0
                    AND t3.rstat != 3
                    AND t3.hsitecode IS NOT NULL
                    AND t3.hsitecode != 0
                    AND t3.hsitecode NOT LIKE "A%"
                    AND t3.hsitecode NOT LIKE "Z%"
                    AND t3.hsitecode NOT LIKE "90%"
                    AND t3.hsitecode NOT LIKE "91%"
                    AND t3.hsitecode NOT LIKE "92%"
                ) AS t
            ) AS t
            WHERE add1n6code IS NOT NULL
            AND LEFT(add1n6code, 2) = ' . $provincecode . '
            GROUP BY SUBSTR(add1n6code FROM 3 FOR 4)
        ) AS t
        INNER JOIN (
            SELECT
                provincecode,
                province,
                amphurcode,
                amphur,
                tamboncode,
                tambon,
                hcode,
                `name`

            FROM
                all_hospital_thai
            WHERE provincecode = ' . $provincecode . '
            GROUP BY CONCAT(amphurcode,tamboncode)
        ) AS aht USING (provincecode, amphurcode, tamboncode)
				ORDER BY
		    CASE 1
		        WHEN aht.amphurcode = 1 THEN
		            1
		        ELSE
		            aht.amphur
		    END,
		    aht.tambon';
        $queryAmphur = Yii::$app->db->createCommand($sqlAmphur)->queryAll();

        //print_r($queryAmphur);
        return $this->renderAjax('_project84_detail_province', [
                    'result' => [
                        'province' => $queryProvince,
                        'hospital' => $queryHospital,
                        'amphur' => $queryAmphur
                    ]
        ]);
    }

    public function actionReport84DetailHospital($report, $hsitecode = null) {
        if ($report != 3 || $hsitecode == null)
            return;
//        echo $hsitecode;
//        return;
        //$hsitecode = intval($hsitecode);
        $sqlHospital = 'SELECT
            hsitecode,
            `name`,
            COUNT(DISTINCT IF((f3v5a1b1="1" OR f3v5a1b2="1" OR f3v5a1b3="1" OR f3v5a1b4="1" OR f3v5a1b5="1" OR f3v5a1b6="1" OR f3v5a2="1" OR (f3v5a2="1" AND f3v5a2b1="1") OR f3v5a3="1" OR f3v5a4="1" OR f3v5a5="1"), ptid, NULL)) AS treatment,
            COUNT(DISTINCT IF((f3v5a1b1="1" OR f3v5a1b2="1" OR f3v5a1b3="1" OR f3v5a1b4="1" OR f3v5a1b6="1"), ptid, NULL)) AS all_surgery,
            COUNT(DISTINCT IF((f3v5a1b1="1" OR f3v5a1b6="1"), ptid, NULL)) AS c_surgery,
            COUNT(DISTINCT IF(((f3v5a1b2="1" AND IFNULL(f3v5a1b1, 0) = 0) OR f3v5a1b3="1" OR f3v5a1b4="1"), ptid, NULL)) AS p_surgery
        FROM
            tb_data_7
        INNER JOIN all_hospital_thai ON tb_data_7.hsitecode = all_hospital_thai.hcode
        WHERE
            (
                f3v4dvisit BETWEEN "2015-10-01"
                AND NOW()
            )
        AND rstat != 0 AND rstat != 3
        AND hsitecode = "' . $hsitecode . '"
        GROUP BY hsitecode';
        $queryHospital = Yii::$app->db->createCommand($sqlHospital)->queryOne();

        $sqlProvince = 'SELECT province FROM all_hospital_thai WHERE hcode = ' . $hsitecode;
        $queryProvince = Yii::$app->db->createCommand($sqlProvince)->queryOne();
        //print_r($queryProvince);
//        $sqlHospital = 'SELECT
//            hsitecode,
//            name,
//            COUNT(DISTINCT ptid) AS cca02
//        FROM
//            tb_data_3
//        INNER JOIN all_hospital_thai ON tb_data_3.hsitecode = all_hospital_thai.hcode
//        WHERE
//            (
//                f2v1 BETWEEN "2015-10-01"
//                AND NOW()
//            )
//        AND rstat != 0
//        AND rstat != 3
//        AND hsitecode IS NOT NULL
//        AND hsitecode != 0
//        AND hsitecode NOT LIKE "90%"
//	    AND hsitecode NOT LIKE "91%"
//        AND hsitecode NOT LIKE "92%"
//        AND provincecode = '.$provincecode.'
//        GROUP BY hsitecode
//        ORDER BY FIELD(code4,1,2,12,11,10,5,17,6,15,7,4,18,3,13,16,8,80,""),hsitecode';
//        $queryHospital = Yii::$app->db->createCommand($sqlHospital)->queryAll();
//
//        //print_r($queryHospital);
//
        $sqlCommunity = 'SELECT
            aht.provincecode,
            aht.province,
            aht.amphurcode,
            aht.amphur,
            aht.tamboncode,
            aht.tambon,
            treatment
        FROM (
            SELECT
                add1n6code,
                LEFT(add1n6code, 2) AS provincecode,
                SUBSTR(add1n6code FROM 3 FOR 2) AS amphurcode,
                RIGHT(add1n6code, 2) AS tamboncode,
                COUNT(ptid) AS treatment

            FROM (
                SELECT
                    ptid,
                    (SELECT add1n6code FROM tb_data_1 WHERE add1n6code IS NOT NULL AND id = t.ptid LIMIT 1) AS add1n6code
                FROM (
                    SELECT
                        DISTINCT ptid AS ptid
                    FROM
                        tb_data_7
                    WHERE
                        (
                            f3v4dvisit BETWEEN "2015-10-01"
                            AND NOW()
                        )
                    AND rstat != 0 AND rstat != 3
                    AND hsitecode = "' . $hsitecode . '"
                    AND (f3v5a1b1="1" OR f3v5a1b2="1" OR f3v5a1b3="1" OR f3v5a1b4="1" OR f3v5a1b5="1" OR f3v5a1b6="1" OR f3v5a2="1" OR (f3v5a2="1" AND f3v5a2b1="1") OR f3v5a3="1" OR f3v5a4="1" OR f3v5a5="1")
                ) AS t
            ) AS t
            WHERE add1n6code IS NOT NULL
            GROUP BY add1n6code
			ORDER BY add1n6code
        ) AS t
        INNER JOIN (
            SELECT
                provincecode,
                province,
                amphurcode,
                amphur,
                tamboncode,
                tambon
            FROM
                all_hospital_thai
            WHERE provincecode != 0 AND amphurcode != 0 AND tamboncode != 0
            GROUP BY provincecode, amphurcode ,tamboncode
        ) AS aht USING (provincecode, amphurcode, tamboncode)
        ORDER BY FIELD(aht.province, "' . $queryProvince['province'] . '", aht.province), aht.province,
		    CASE 1
		        WHEN aht.amphurcode = 1 THEN
		            1
		        ELSE
		            aht.amphur
		    END,
		    CASE 1
		        WHEN aht.tamboncode = 1 THEN
		            1
		        ELSE
		            aht.tambon
		    END
       ';
        $queryCommunity = Yii::$app->db->createCommand($sqlCommunity)->queryAll();

//        echo $queryProvince['province'];
//        echo '<pre>';
//        print_r($queryCommunity);
//        echo '</pre>';

        return $this->renderAjax('_project84_detail_hospital', [
                    'result' => [
                        'hospital' => $queryHospital,
                        'community' => $queryCommunity
//                'province'=>$queryProvince,
//                'amphur'=>$queryAmphur,
                    ]
        ]);
    }

    public function actionReport84DetailOv($report, $tamboncode = null) {
//        echo $report;
        if ($report != 1 || $tamboncode == null)
            return;
        $sql = 'SELECT
            COUNT(DISTINCT IF(k=1, ptid, NULL)) AS k,
            COUNT(DISTINCT IF(k=1 AND kpos=1, ptid, NULL)) AS kpos,
            COUNT(DISTINCT IF(p=1, ptid, NULL)) AS p,
            COUNT(DISTINCT IF(p=1 AND ppos=1, ptid, NULL)) AS ppos,
            COUNT(DISTINCT IF(f=1, ptid, NULL)) AS f,
            COUNT(DISTINCT IF(f=1 AND fpos=1, ptid, NULL)) AS fpos,
            COUNT(DISTINCT IF(u=1, ptid, NULL)) AS u,
            COUNT(DISTINCT IF(u=1 AND upos=1, ptid, NULL)) AS upos
        FROM
        (
            SELECT
                *
            FROM
            (
                SELECT
                    hsitecode,
                    ptid,
                    IFNULL(results IN ("0","1"),0) AS k,
                    ov AS kpos,
                    NULL AS p,
                    NULL AS ppos,
                    NULL AS f,
                    NULL AS fpos,
                    NULL AS u,
                    NULL AS upos
                FROM tbdata_21
                WHERE rstat != 0 AND rstat != 3
                AND (exdate BETWEEN "2015-10-01" AND NOW())

                UNION ALL

                SELECT
                    hsitecode,
                    ptid,
                    NULL AS k,
                    NULL AS kpos,
                    IFNULL(results IN ("0","1"),0) AS p,
                    ov AS ppos,
                    NULL AS f,
                    NULL AS fpos,
                    NULL AS u,
                    NULL AS upos
                FROM tbdata_22
                WHERE rstat != 0 AND rstat != 3
                AND (exdate BETWEEN "2015-10-01" AND NOW())

                UNION ALL

                SELECT
                    hsitecode,
                    ptid,
                    NULL AS k,
                    NULL AS kpos,
                    NULL AS p,
                    NULL AS ppos,
                    IFNULL(results IN ("0","1"),0) AS f,
                    ov AS fpos,
                    NULL AS u,
                    NULL AS upos
                FROM tbdata_23
                WHERE rstat != 0 AND rstat != 3
                AND (exdate BETWEEN "2015-10-01" AND NOW())

                UNION ALL

                SELECT
                    hsitecode,
                    ptid,
                    NULL AS k,
                    NULL AS kpos,
                    NULL AS p,
                    NULL AS ppos,
                    NULL AS f,
                    NULL AS fpos,
                    IFNULL(results IN ("0","1"),0) AS u,
                    results AS upos
                FROM tbdata_24
                WHERE rstat != 0 AND rstat != 3
                AND (exdate BETWEEN "2015-10-01" AND NOW())
            ) AS t
            WHERE hsitecode IN
            (
                SELECT
                    hcode
                FROM
                    all_hospital_thai
                WHERE
                    LEFT (code6, 6) LIKE "' . $tamboncode . '"
            )
        ) AS t';
        $qry = Yii::$app->db->createCommand($sql)->queryAll();
//        var_dump($qry);
//        return $tamboncode;
        return $this->renderAjax('_project84_detail_ov', [
                    'result' => $qry
        ]);
    }

    public function actionAjaxReportSummaryGraph() {
        $ShortMonth = array('', 'ม.ค.', 'ก.พ.', 'มี.ค.', 'เม.ย.', 'พ.ค.', 'มิ.ย.', 'ก.ค.', 'ส.ค.', 'ก.ย.', 'ต.ค.', 'พ.ย.', 'ธ.ค.');
        $g1data = array();
        $g2data = array();
        $g3data = array();
        $g4data = array();
        $g5data = array();
        $g6data = array();
        $g1label = array();
        $g2label = array();
        $g3label = array();
        $g4label = array();
        $g5label = array();
        $g6label = array();
        $myData = array();
        $g7data = array();
        $CON = " AND rstat NOT IN('0','3','6')
        AND hsitecode IS NOT NULL
        AND hsitecode != 0
        AND hsitecode NOT LIKE 'A%'
        AND hsitecode NOT LIKE 'Z%'
        AND hsitecode NOT LIKE '90%'
        AND hsitecode NOT LIKE '91%'
        AND hsitecode NOT LIKE '92%' ";
        /* G1 */
        Yii::$app->db->createCommand("DELETE FROM summary_graph1;")->execute();
        $sql = "INSERT INTO summary_graph1
                SELECT MONTH(f1vdcomp) AS M,YEAR(f1vdcomp) AS Y,COUNT(DISTINCT ptid) AS P_CNT FROM tb_data_2
                WHERE DATE(f1vdcomp) BETWEEN '2013-02-09' AND NOW()" . $CON . "
                GROUP BY MONTH(f1vdcomp),YEAR(f1vdcomp)
                ORDER BY Y ASC, M ASC";
        Yii::$app->db->createCommand($sql)->execute();
        /* G2 */
        Yii::$app->db->createCommand("DELETE FROM summary_graph2;")->execute();
        $sql = "INSERT INTO summary_graph2
                SELECT MONTH(f2v1) AS M,YEAR(f2v1) AS Y,COUNT(DISTINCT ptid) AS P_CNT FROM tb_data_3
                WHERE DATE(f2v1) BETWEEN '2013-02-09' AND NOW()" . $CON . "
                GROUP BY MONTH(f2v1),YEAR(f2v1)
                ORDER BY Y ASC, M ASC";
        Yii::$app->db->createCommand($sql)->execute();
        /* G3 */
        Yii::$app->db->createCommand("DELETE FROM summary_graph3;")->execute();
        $sql = "INSERT INTO summary_graph3
                SELECT b.zone_code AS ZONE,COUNT(DISTINCT ptid) AS P_CNT FROM tb_data_2 a
                LEFT JOIN all_hospital_thai b ON a.hsitecode=b.hcode
                WHERE DATE(a.f1vdcomp) BETWEEN '2013-02-09' AND NOW()" . $CON . "
                GROUP BY b.zone_code
                ORDER BY b.zone_code ASC";
        Yii::$app->db->createCommand($sql)->execute();
        /* G4 */
        Yii::$app->db->createCommand("DELETE FROM summary_graph4;")->execute();
        $sql = "INSERT INTO summary_graph4
                SELECT b.zone_code AS ZONE,COUNT(DISTINCT ptid) AS P_CNT FROM tb_data_3 a
                LEFT JOIN all_hospital_thai b ON a.hsitecode=b.hcode
                WHERE DATE(a.f2v1) BETWEEN '2013-02-09' AND NOW()" . $CON . "
                GROUP BY b.zone_code
                ORDER BY b.zone_code ASC";
        Yii::$app->db->createCommand($sql)->execute();
        /* G5 */
        Yii::$app->db->createCommand("DELETE FROM summary_graph5;")->execute();
        $sql = "INSERT INTO summary_graph5
                SELECT b.province AS PROVINCE,COUNT(DISTINCT ptid) AS P_CNT FROM tb_data_2 a
                LEFT JOIN all_hospital_thai b ON a.hsitecode=b.hcode
                WHERE DATE(a.f1vdcomp) BETWEEN '2013-02-09' AND NOW()" . $CON . "
                GROUP BY b.province
                ORDER BY BINARY b.province ASC";
        Yii::$app->db->createCommand($sql)->execute();
        /* G6 */
        Yii::$app->db->createCommand("DELETE FROM summary_graph6;")->execute();
        $sql = "INSERT INTO summary_graph6
                SELECT b.province AS PROVINCE,COUNT(DISTINCT ptid) AS P_CNT FROM tb_data_3 a
                LEFT JOIN all_hospital_thai b ON a.hsitecode=b.hcode
                WHERE DATE(a.f2v1) BETWEEN '2013-02-09' AND NOW()" . $CON . "
                GROUP BY b.province
                ORDER BY BINARY b.province ASC";
        Yii::$app->db->createCommand($sql)->execute();
        /* G7 */
        $TB = array("tb_data_4", "tb_data_7", "tb_data_8", "tb_data_11");
        $DVAR = array("f2p1v1", "f3v4dvisit", "f4v1", "create_date");
        $TARGET_FIELD = array("cca21", "cca3", "cca4", "cca5");
        for ($i = 0; $i < count($TB); $i++) {
            $sql = "UPDATE summary_graph7,
                    (SELECT rawtb.hsitecode, P_CNT
                    FROM (
                        SELECT hsitecode,COUNT(DISTINCT ptid) AS P_CNT FROM " . $TB[$i] . "
                        WHERE DATE(" . $DVAR[$i] . ") BETWEEN '2013-02-09' AND NOW()
                        AND hsitecode IN ('10666','10668','10669','10670','10671','10708','12276','13777','14201')" . $CON . "
                        GROUP BY hsitecode
                    ) AS rawtb
                ) AS rawdata
                SET summary_graph7." . $TARGET_FIELD[$i] . " = rawdata.P_CNT
                WHERE summary_graph7.hsitecode = rawdata.hsitecode";
            Yii::$app->db->createCommand($sql)->execute();
        }
        /*         * ******************************************************************* */
        $sql = "SELECT UPDATE_TIME FROM information_schema.tables WHERE TABLE_NAME LIKE 'summary_graph1'";
        $rs = Yii::$app->db->createCommand($sql)->queryOne();
        $GraphLastUpdate = $rs['UPDATE_TIME'];

        $sql = "SELECT * FROM summary_graph1";
        $rs1 = Yii::$app->db->createCommand($sql)->queryAll();
        $FDate = (date("Y") - 2) . '-' . date("m") . '-01';
        $TempDate = date("Y-m-d", strtotime(date("Y-m-d", strtotime($FDate)) . " +1 month"));
        $FDate = date("Y-m-d", strtotime(date("Y-m-d", strtotime($TempDate)) . " -1 day"));
        $index = 0;
        foreach ($rs1 as $row) {
            $date_run = $row['Y'] . '-' . str_pad($row['M'], 2, '0', STR_PAD_LEFT) . '-01';
            if ($date_run <= $FDate) {
                $g1label[0] = $row['M'] . ' ' . $row['Y'];
                $g1data[0] += $row['P_CNT'];
                $index = 1;
            } else {
                $g1label[$index] = $row['M'] . ' ' . $row['Y'];
                $g1data[$index++] = $row['P_CNT'];
            }
        }
        $sql = "SELECT * FROM summary_graph2";
        $rs2 = Yii::$app->db->createCommand($sql)->queryAll();
        $index = 0;
        foreach ($rs2 as $row) {
            $date_run = $row['Y'] . '-' . str_pad($row['M'], 2, '0', STR_PAD_LEFT) . '-01';
            if ($date_run <= $FDate) {
                $g2label[0] = $row['M'] . ' ' . $row['Y'];
                $g2data[0] += $row['P_CNT'];
                $index = 1;
            } else {
                $g2label[$index] = $row['M'] . ' ' . $row['Y'];
                $g2data[$index++] = $row['P_CNT'];
            }
        }
        $sql = "SELECT * FROM summary_graph3";
        $rs3 = Yii::$app->db->createCommand($sql)->queryAll();
        $index = 0;
        foreach ($rs3 as $row) {
            $g3label[$index] = 'เขต ' . $row['ZONE'];
            $g3data[$index++] = $row['P_CNT'];
        }
        $sql = "SELECT * FROM summary_graph4";
        $rs4 = Yii::$app->db->createCommand($sql)->queryAll();
        $index = 0;
        foreach ($rs4 as $row) {
            $g4label[$index] = 'เขต ' . $row['ZONE'];
            $g4data[$index++] = $row['P_CNT'];
        }
        $sql = "SELECT * FROM summary_graph5";
        $rs5 = Yii::$app->db->createCommand($sql)->queryAll();
        $index = 0;
        foreach ($rs5 as $row) {
            $g5label[$index] = $row['PROVINCE'];
            $g5data[$index++] = $row['P_CNT'];
        }
        $sql = "SELECT * FROM summary_graph6";
        $rs6 = Yii::$app->db->createCommand($sql)->queryAll();
        $index = 0;
        foreach ($rs6 as $row) {
            $g6label[$index] = $row['PROVINCE'];
            $g6data[$index++] = $row['P_CNT'];
        }
        $sql = "SELECT b.`name`,a.* FROM summary_graph7 a LEFT JOIN all_hospital_thai b
                ON a.hsitecode=b.hcode ORDER BY BINARY b.`name`";
        $rs7 = Yii::$app->db->createCommand($sql)->queryAll();
        $index = 0;
        foreach ($rs7 as $row) {
            $g7data[$index][0] = $row['name'];
            $g7data[$index][2] = $row['cca21'];
            $g7data[$index][3] = $row['cca3'];
            $g7data[$index][4] = $row['cca4'];
            $g7data[$index][5] = $row['cca5'];
            $index++;
        }
        for ($i = 0; $i < count($g1label); $i++) {
            $x = explode(' ', $g1label[$i]);
            $g1label[$i] = ($i == 0) ? 'ปี 56 ถึง ' . $ShortMonth[$x[0] + 0] . ' ' . substr($x[1] + 543, -2) : $ShortMonth[$x[0] + 0] . ' ' . substr($x[1] + 543, -2);
        }

        return $this->renderAjax('_ajaxReportSummaryGraph', [
                    'LastUpdateTime' => $GraphLastUpdate,
                    'g1label' => $g1label, 'g1data' => $g1data,
                    'g2label' => $g2label, 'g2data' => $g2data,
                    'g3label' => $g3label, 'g3data' => $g3data,
                    'g4label' => $g4label, 'g4data' => $g4data,
                    'g5label' => $g5label, 'g5data' => $g5data,
                    'g6label' => $g6label, 'g6data' => $g6data,
                    'g7data' => $g7data,
        ]);
    }

    public function actionAjaxReportSummary() {

        $myData = array();

        /* Call basic procedures */
        Yii::$app->db->createCommand("CALL basicInfo_RegisterCNT();")->execute();
        Yii::$app->db->createCommand("CALL basicInfo_CCA01CNT();")->execute();
        Yii::$app->db->createCommand("CALL basicInfo_UltrasoundCNT();")->execute();
        Yii::$app->db->createCommand("CALL basicInfo_CTMRICNT();")->execute();
        $data = Yii::$app->db->createCommand("SELECT * FROM basicInfo_CNT;")->queryOne();
        $myData['Register'] = $data['register_p'];
        $myData['CCA-01'] = $data['cca01_p'];
        $myData['CCA-02P'] = $data['ultrasound_p'];
        $myData['CCA-02T'] = $data['ultrasound_t'];
        $myData['CTMRI_P'] = $data['ctmri_p'];
        $myData['CTMRI_T'] = $data['ctmri_t'];
        /*         * *************************** */

        $CON = " AND rstat NOT IN('0','3','6')
        AND hsitecode IS NOT NULL
        AND hsitecode != 0
        AND hsitecode NOT LIKE 'A%'
        AND hsitecode NOT LIKE 'Z%'
        AND hsitecode NOT LIKE '90%'
        AND hsitecode NOT LIKE '91%'
        AND hsitecode NOT LIKE '92%' ";
        //AND hsitecode NOT IN(SELECT DISTINCT(hcode) AS xhcode FROM all_hospital_thai WHERE code2 LIKE '%demo%') ";

        $sql = "SHOW TABLE STATUS LIKE 'person'";
        $data = Yii::$app->dbbot->createCommand($sql)->queryOne();
        $myData['TCC'] = $data['Rows'];
        $sql = "SHOW TABLE STATUS LIKE 'person'";
        $data = Yii::$app->dbnemo->createCommand($sql)->queryOne();
        $myData['NEMO'] = $data['Rows'];


        $sql = "SELECT COUNT(DISTINCT ptid) AS P_CNT FROM tb_data_7 WHERE (DATE(f3v4dvisit) BETWEEN '2013-02-09' AND NOW())" . $CON;
        $data = Yii::$app->db->createCommand($sql)->queryOne();
        $myData['TREATED_P'] = $data['P_CNT'];

        $sql = "SELECT COUNT(DISTINCT ptid) AS P_CNT,COUNT(ptid) AS T_CNT FROM
                (SELECT ptid,rstat,sitecode,create_date FROM tbdata_21 UNION ALL
                SELECT ptid,rstat,sitecode,create_date FROM tbdata_22 UNION ALL
                SELECT ptid,rstat,sitecode,create_date FROM tbdata_23 UNION ALL
                SELECT ptid,rstat,sitecode,create_date FROM tbdata_24) AS MyAlias
                WHERE (date(create_date) BETWEEN '2015-02-09' AND NOW())" . str_replace("hsitecode", "sitecode", $CON);
        $data = Yii::$app->db->createCommand($sql)->queryOne();
        $myData['MENU3_P'] = $data['P_CNT'];
        $myData['MENU3_T'] = $data['T_CNT'];

        $sql = "SELECT COUNT(DISTINCT ptid) AS P_CNT, COUNT(ptid) AS T_CNT FROM tb_data_8 WHERE DATE(create_date) BETWEEN '2013-02-09' AND NOW()" . $CON;
        $data = Yii::$app->db->createCommand($sql)->queryOne();
        $myData['PHO_P'] = $data['P_CNT'];
        $myData['PHO_T'] = $data['T_CNT'];

        Yii::$app->db->createCommand("DELETE FROM treated_ptid;")->execute();
        $sql = "INSERT INTO treated_ptid
                    SELECT DISTINCT ptid FROM tb_data_7
                    WHERE DATE(f3v4dvisit) BETWEEN '2013-02-09' AND NOW()
                    AND (f3v5a1b1 = '1' OR f3v5a1b1c1 = '1' OR f3v5a1b1c2 = '1' OR f3v5a1b1c3 = '1' OR f3v5a1b1c4 = '1'
                        OR f3v5a1b1c5 = '1' OR f3v5a1b1c6 = '1' OR f3v5a1b1c7 = '1' OR f3v5a1b1c8 = '1' OR f3v5a1b1c9 = '1'
                        OR f3v5a1b6 = '1')" . $CON;
        Yii::$app->db->createCommand($sql)->execute();
        $data = Yii::$app->db->createCommand("SELECT COUNT(ptid) AS P_CNT FROM treated_ptid;")->queryOne();
        $myData['T1_P'] = $data['P_CNT'];

        Yii::$app->db->createCommand("DELETE FROM treated_ptid_2;")->execute();
        $sql = "INSERT INTO treated_ptid_2
                    SELECT DISTINCT ptid FROM tb_data_7
                    WHERE ((DATE(f3v4dvisit) BETWEEN '2013-02-09' AND NOW())
                    OR (DATE(f3v5a1b2dtreat) BETWEEN '2013-02-09' AND NOW())
                    OR (DATE(f3v5a1b3dtreat) BETWEEN '2013-02-09' AND NOW())
                    OR (DATE(f3v5a1b4dtreat) BETWEEN '2013-02-09' AND NOW()))
                    AND (f3v5a1b2='1' OR f3v5a1b3='1' OR f3v5a1b4='1')
                    AND ptid NOT IN(SELECT ptid FROM treated_ptid)" . $CON;
        Yii::$app->db->createCommand($sql)->execute();
        $data = Yii::$app->db->createCommand("SELECT COUNT(ptid) AS P_CNT FROM treated_ptid_2;")->queryOne();
        $myData['T2_P'] = $data['P_CNT'];

        $sql = "SELECT COUNT(DISTINCT ptid) AS P_CNT FROM tb_data_8
                WHERE DATE(f4v1) BETWEEN '2013-02-09' AND NOW()
                AND ptid NOT IN(SELECT DISTINCT ptid FROM tb_data_7
                    WHERE f3v4dvisit BETWEEN '2013-02-09' AND NOW()
                    " . $CON . ")" . $CON;
        $data = Yii::$app->db->createCommand($sql)->queryOne();
        //$myData['T2_P'] += $data['P_CNT'];
        $myData['TREATED_P'] += $data['P_CNT'];

        $sql = "SELECT COUNT(DISTINCT ptid) AS P_CNT FROM tb_data_8
                WHERE DATE(f4v1) BETWEEN '2013-02-09' AND NOW()
                    AND ptid NOT IN(SELECT ptid FROM treated_ptid UNION ALL SELECT ptid FROM treated_ptid_2)" . $CON;
        $data = Yii::$app->db->createCommand($sql)->queryOne();
        $myData['T2_P'] += $data['P_CNT'];

        $myData['T3_P'] = $myData['TREATED_P'] - ($myData['T1_P'] + $myData['T2_P']);

        Yii::$app->response->format = \yii\web\Response::FORMAT_HTML;
        return $this->renderAjax('_ajaxReportSummary', [
                    'myData' => $myData,
        ]);
    }

    public function actionSummaryRefresh() {
        $refresh = $_POST['refresh'];
     
        $date = date('Y-m-d');
        $last = date('Y-m-d H:i:s');


        $va = $this->Getvalues();
        $sql = "UPDATE summary_count SET  lastupdate= '$last',num_ptid_tb1 = " . $va['data2'] . ", num_ptid_tb2 = " . $va['data3'] . ", num_ptid_ov = " . $va['data4'] . ", num_id_ov = " . $va['data5'] . ", num_ptid_tb3 = " . $va['data6'] . ", num_id_tb3 = " . $va['data7'] . ", num_ptid_tb4 = " . $va['data8'] . ", num_id_tb4 = " . $va['data9'] . ", num_ptid_tb8 = " . $va['data10'] . ", num_id_tb8 = " . $va['data11'] . ",num_ptid_tb7 = " . $va['data12'] . ", num_com_tb7 = " . $va['data13'] . ", num_sup_tb7 = " . $va['data14'] . ", num_other_tb7 = " . $va['data15'] . ", num_buffe = " . $va['data1'] . " ,num_sus = " . $va['sus'] . " ,num_cf = " . $va['cf'] . " ,num_pos_ov =  " . $va['ovpos'] . ", num_pos_tb8 =  " . $va['tb8pos'] . ", num_cf_tb47 =  " . $va['data16'] . " WHERE datecout='$date'";
        Yii::$app->db->createCommand($sql)->execute();


        $sum = Yii::$app->db->createCommand("SELECT lastupdate as lastupdate,num_ptid_tb1 as data2, num_ptid_tb2 as data3, num_ptid_ov as data4, num_id_ov as data5, "
                        . " num_ptid_tb3 as data6, num_id_tb3 as data7, num_ptid_tb4 as data8, num_id_tb4 as data9, num_ptid_tb8 as data10, num_id_tb8 as data11,"
                        . " num_ptid_tb7 as data12, num_com_tb7 as data13, num_sup_tb7 as data14, num_other_tb7 as data15, num_buffe as data1 ,num_sus as suspect ,num_cf as confirm ,num_pos_ov as posov, num_pos_tb8 as postb8 ,num_cf_tb47 as data16 FROM summary_count ORDER BY datecout DESC limit 1")->queryOne();



        return json_encode($sum);
    }

    public function actionSummary() {
        $date = date('Y-m-d');
        $last = date('Y-m-d H:i:s');
        $check = Yii::$app->db->createCommand("SELECT datecout FROM  summary_count ORDER BY datecout DESC limit 1")->queryAll();

        if ($date > date($check[0]['datecout'])) {
            $va = $this->Getvalues();
            $sql = "INSERT INTO summary_count (datecout,lastupdate,num_ptid_tb1, num_ptid_tb2, num_ptid_ov, num_id_ov, num_ptid_tb3, num_id_tb3, num_ptid_tb4, num_id_tb4, num_ptid_tb8, num_id_tb8, num_ptid_tb7, num_com_tb7, num_sup_tb7, num_other_tb7, num_buffe, num_sus, num_cf, num_pos_ov, num_pos_tb8, num_cf_tb47) VALUES ( '$date', '$last', " . $va['data2'] . ",  " . $va['data3'] . ", " . $va['data4'] . ", " . $va['data5'] . "," . $va['data6'] . ",  " . $va['data7'] . ", " . $va['data8'] . ", " . $va['data9'] . ", " . $va['data10'] . ",  " . $va['data11'] . "," . $va['data12'] . ", " . $va['data13'] . ", " . $va['data14'] . "," . $va['data15'] . ",  " . $va['data1'] . " , " . $va['sus'] . " , " . $va['cf'] . ", " . $va['ovpos'] . ", " . $va['tb8pos'] . ", " . $va['data16'] . ")";
            Yii::$app->db->createCommand($sql)->execute();
        }
        $sum = Yii::$app->db->createCommand("SELECT lastupdate as lastupdate,num_ptid_tb1 as data2, num_ptid_tb2 as data3, num_ptid_ov as data4, num_id_ov as data5, "
                        . " num_ptid_tb3 as data6, num_id_tb3 as data7, num_ptid_tb4 as data8, num_id_tb4 as data9, num_ptid_tb8 as data10, num_id_tb8 as data11,"
                        . " num_ptid_tb7 as data12, num_com_tb7 as data13, num_sup_tb7 as data14, num_other_tb7 as data15, num_buffe as data1 ,num_sus as suspect ,num_cf as confirm ,num_pos_ov as posov, num_pos_tb8 as postb8, num_cf_tb47 as data16  FROM summary_count ORDER BY datecout DESC limit 1")->queryOne();



        return ($sum);
    }

    private function Getvalues() {
        /////////2.ลงทะเบียน (ราย)///////////
        $num2 = Yii::$app->db->createCommand("SELECT count(distinct ptid) as num from tb_data_1 p INNER JOIN all_hospital_thai h on p.hsitecode=h.hcode where code2<>'demo' and rstat not in ('0','3') and DATE(create_date)>='2013-02-09';")->queryOne();
        $data2 = $num2['num'];
        /////////3.ข้อมูลพื้นฐาน (ราย)///////////
        $num3 = Yii::$app->db->createCommand("SELECT count(distinct ptid) as num from tb_data_2 p INNER JOIN all_hospital_thai h on p.hsitecode=h.hcode where code2<>'demo' and rstat not in ('0','3') and DATE(f1vdcomp)>='2013-02-09';")->queryOne();
        $data3 = $num3['num'];
        
        /////////4.ตรวจอุจจาระ/ปัสสาวะ (ราย)///////////
        $num41 = Yii::$app->db->createCommand("SELECT COUNT(DISTINCT ptid) as num21 FROM tbdata_21 WHERE rstat NOT IN(0,3)")->queryOne();
        $data41 = $num41['num21'];
        $num42 = Yii::$app->db->createCommand("SELECT COUNT(DISTINCT ptid) as num22 FROM tbdata_22 WHERE rstat NOT IN(0,3)")->queryOne();
        $data42 = $num42['num22'];
        $num43 = Yii::$app->db->createCommand("SELECT COUNT(DISTINCT ptid) as num23 FROM tbdata_23 WHERE rstat NOT IN(0,3)")->queryOne();
        $data43 = $num43['num23'];
        $num44 = Yii::$app->db->createCommand("SELECT COUNT(DISTINCT ptid) as num24 FROM tbdata_24 WHERE rstat NOT IN(0,3)")->queryOne();
        $data44 = $num44['num24'];
        $data4 = $data41 + $data42 + $data43 + $data44;
        /////////5.ตรวจอุจจาระ/ปัสสาวะ (ครั้ง)///////////
        $num51 = Yii::$app->db->createCommand("SELECT COUNT(*) as num21 FROM tbdata_21 WHERE rstat NOT IN(0,3)")->queryOne();
        $data51 = $num51['num21'];
        $num52 = Yii::$app->db->createCommand("SELECT COUNT(*) as num22 FROM tbdata_22 WHERE rstat NOT IN(0,3)")->queryOne();
        $data52 = $num52['num22'];
        $num53 = Yii::$app->db->createCommand("SELECT COUNT(*) as num23 FROM tbdata_23 WHERE rstat NOT IN(0,3)")->queryOne();
        $data53 = $num53['num23'];
        $num54 = Yii::$app->db->createCommand("SELECT COUNT(*) as num24 FROM tbdata_24 WHERE rstat NOT IN(0,3)")->queryOne();
        $data54 = $num54['num24'];
        $data5 = $data51 + $data52 + $data53 + $data54;
        /////////(positive)//////////////////
        $numov21 = Yii::$app->db->createCommand("SELECT COUNT(*) as ov21 FROM tbdata_21 WHERE rstat NOT IN(0,3) AND ov = '1'")->queryOne();
        $dtov21 = $numov21['ov21'];
        $numov22 = Yii::$app->db->createCommand("SELECT COUNT(*) as ov22 FROM tbdata_22 WHERE rstat NOT IN(0,3) AND ov = '1'")->queryOne();
        $dtov22 = $numov22['ov22'];
        $numov23 = Yii::$app->db->createCommand("SELECT COUNT(*) as ov23 FROM tbdata_23 WHERE rstat NOT IN(0,3) AND ov = '1'")->queryOne();
        $dtov23 = $numov23['ov23'];
        $numov24 = Yii::$app->db->createCommand("SELECT COUNT(*) as re24 FROM tbdata_24 WHERE rstat NOT IN(0,3) AND results = '1'")->queryOne();
        $dtre24 = $numov24['re24'];
        $ovpos = $dtov21 + $dtov22 + $dtov23 + $dtre24;



        /////////6.ตรวจคัดกรองอัลตร้าซาวด์ (ราย)///////////
        $data6 = Yii::$app->db->createCommand("SELECT COUNT(DISTINCT ptid) as num FROM tb_data_3 WHERE rstat NOT IN(0,3)")->queryOne();
        $data6 = $data6['num'];
        /////////7.ตรวจคัดกรองอัลตร้าซาวด์ (ครั้ง)///////////
        $data7 = Yii::$app->db->createCommand("SELECT COUNT(*) as num FROM tb_data_3 WHERE rstat NOT IN(0,3)")->queryOne();
        $data7 = $data7['num'];
        ////////( suspected )/////////////
        $sus = Yii::$app->db->createCommand("SELECT COUNT(DISTINCT ptid) as num FROM tb_data_3 WHERE f2v6a3 = '1' AND f2v6a3b1 = '1' AND rstat NOT IN(0,3)")->queryOne();
        $sus = $sus['num'];


        /////////8.ตรวจ CT,MRI,MRCP (ราย)///////////
        $data8 = Yii::$app->db->createCommand("SELECT COUNT(DISTINCT ptid) as num FROM tb_data_4 WHERE rstat NOT IN(0,3)")->queryOne();
        $data8 = $data8['num'];
        /////////9.ตรวจ CT,MRI,MRCP (ครั้ง)///////////
        $data9 = Yii::$app->db->createCommand("SELECT COUNT(*) as num FROM tb_data_4 WHERE rstat NOT IN(0,3)")->queryOne();
        $data9 = $data9['num'];
        ////////( confirmed ) /////////////
        $cf = Yii::$app->db->createCommand("SELECT COUNT(DISTINCT ptid) as num FROM tb_data_4 WHERE f2p1v3 IN ('1','2','3') AND rstat NOT IN(0,3)")->queryOne();
        $cf = $cf['num'];


        /////////10.ตรวจยืนยันทางพยาธิวิทยา (ราย)///////////
        $data10 = Yii::$app->db->createCommand("SELECT COUNT(DISTINCT ptid) as num FROM tb_data_8 WHERE rstat NOT IN(0,3)")->queryOne();
        $data10 = $data10['num'];
        /////////11.ตรวจยืนยันทางพยาธิวิทยา (ครั้ง)///////////
        $data11 = Yii::$app->db->createCommand("SELECT COUNT(*) as num FROM tb_data_8 WHERE rstat NOT IN(0,3)")->queryOne();
        $data11 = $data11['num'];
        /////////( positive )///////////////
        $tb8pos = Yii::$app->db->createCommand("SELECT COUNT(*) as num FROM tb_data_8 WHERE f4v4 IN ('1','2','3') AND rstat NOT IN(0,3)")->queryOne();
        $tb8pos = $tb8pos['num'];

        /////////12.รับการรักษา(ราย)///////////
        $data12 = Yii::$app->db->createCommand("SELECT COUNT(DISTINCT ptid) as num FROM tb_data_7 WHERE rstat NOT IN(0,3)")->queryOne();
        $data12 = $data12['num'];
        //////////13.ผ่าตัดเพื่อหายขาด 14.ผ่าตัดประคับประคอง 15. อื่นๆ //////////
        $data13 = Yii::$app->db->createCommand("SELECT COUNT(DISTINCT ptid) as num FROM tb_data_7 WHERE f3v5a1b1 ='1' OR f3v5a1b2 = '1' OR f3v5a1b6 = '1' AND rstat NOT IN(0,3)")->queryOne();
        $data13 = $data13['num'];
        $data14 = Yii::$app->db->createCommand("SELECT COUNT(DISTINCT ptid) as num FROM tb_data_7 WHERE f3v5a1b3 ='1' OR f3v5a1b4 = '1' AND rstat NOT IN(0,3)")->queryOne();
        $data14 = $data14['num'];
        $data15 = $data12 - ($data13 + $data14);
        /////////16.จากกลุ่ม Confirm CT/MRI ผ่าตัดเพื่อหายขาด(ราย)///////////
        $data16 = Yii::$app->db->createCommand("SELECT COUNT(*) as num FROM tb_data_4 t INNER JOIN tb_data_7 d on t.ptid=d.ptid WHERE  t.f2p1v3 IN ('1','2','3') and d.f3v5a1b1 ='1' OR d.f3v5a1b2 = '1' OR d.f3v5a1b6 = '1' AND t.rstat NOT IN(0,3) AND d.rstat NOT IN(0,3)")->queryOne();
        $data16 = $data16['num'];
        
        /////////1.ประชากร//////////////

        $sql1 = "SHOW TABLE STATUS LIKE 'person'";
        $databot = Yii::$app->dbbot->createCommand($sql1)->queryOne();
        $bot = $databot['Rows'];

        $sql2 = "SHOW TABLE STATUS LIKE 'person'";
        $datanemo = Yii::$app->dbnemo->createCommand($sql2)->queryOne();
        $nemo = $datanemo['Rows'];
        $data1 = $bot + $nemo;


        $date = date('Y-m-d');
        $last = date('Y-m-d H:i:s');

        $array = [];
        $array['data2'] = $data2;
        $array['data3'] = $data3;
        $array['data4'] = $data4;
        $array['data5'] = $data5;
        $array['data6'] = $data6;
        $array['data7'] = $data7;
        $array['data8'] = $data8;
        $array['data9'] = $data9;
        $array['data10'] = $data10;
        $array['data11'] = $data11;
        $array['data12'] = $data12;
        $array['data13'] = $data13;
        $array['data14'] = $data14;
        $array['data15'] = $data15;
        $array['data16'] = $data16;
        $array['data1'] = $data1;
        $array['sus'] = $sus;
        $array['cf'] = $cf;
        $array['ovpos'] = $ovpos;
        $array['tb8pos'] = $tb8pos;
        return $array;
    }

}
