<?php
namespace frontend\controllers;

use common\models\SiteConfig;
use common\models\User;
use Yii;
use frontend\models\ContactForm;
use yii\helpers\VarDumper;
use yii\web\Controller;
use frontend\models\SiteMenu;
use yii\helpers\Url;
use \yii\helpers\ArrayHelper;
use yii\helpers\Html;
/**
 * Site controller
 */
class EzformapiController extends Controller
{
   public $enableCsrfValidation=false;
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction'
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null
            ],
            'set-locale'=>[
                'class'=>'common\actions\SetLocaleAction',
                'locales'=>array_keys(Yii::$app->params['availableLocales'])
            ]
        ];
    }
    public function actionIndex(){
        //15871
        $sql="SELECT * FROM ezform_fields LIMIT 15871,10000";
        $query = Yii::$app->db->createCommand($sql)->queryAll();
        echo \yii\helpers\Json::encode($query);
    }
    public function actionEzformPrint($ezf_id, $dataid) {
	
	    try {
		
		$modelform = \backend\modules\ezforms\models\Ezform::find()->where('ezf_id = :ezf_id', [':ezf_id' => $ezf_id])->one();
		
		$modelfield = \backend\modules\ezforms\models\EzformFields::find()
			->where('ezf_id = :ezf_id', [':ezf_id' => $ezf_id])
			->orderBy(['ezf_field_order' => SORT_ASC])
			->all();
		
		$table = $modelform->ezf_table;
		
		$model_gen = \backend\modules\ezforms\components\EzformFunc::setDynamicModelLimit($modelfield);
		
		if($dataid>0){
		    $model = new \backend\models\Tbdata();
		    $model->setTableName($table);

		    $query = $model->find()->where('id=:id', [':id'=>$dataid]);

		    $data = $query->one();

		    $model_gen->attributes = $data->attributes;
		}
		
		 
		return $this->renderPartial('index', [
		    'modelform'=>$modelform,
		    'modelfield'=>$modelfield,
		    'model_gen'=>$model_gen,
		]);

	    } catch (\yii\db\Exception $e) {
		
	    }
	    
    }
    public function actionEzformSave(){
          
       print_r($_POST);
    }
    
    public function  actionTest(){
        return $this->render("test");
    }
    
    public function  actionTest2(){
//        $dataid=[
//            ['id' => 100],
//            ['id' => 102],
//            ['id' => 103],
//        ];
        $user = \Yii::$app->db->createCommand('SELECT * FROM user')->queryAll();
   
        return $this->renderAjax("test2",['user'=>$user]);
    }
    
    public function  actionTest3(){
        $n = isset($_GET['n']) ? $_GET['n'] : 4;
        $user = \Yii::$app->db->createCommand("SELECT * FROM user LIMIT 0,$n")->queryAll();
        return \yii\helpers\Json::encode($user);
    }
    public function actionCounts(){
        $user = \Yii::$app->db->createCommand("SELECT count(*) as count FROM user")->queryAll();
        echo $user['count'];
    }
     
}
