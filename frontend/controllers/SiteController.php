<?php
namespace frontend\controllers;

use common\models\SiteConfig;
use common\models\User;
use Yii;
use frontend\models\ContactForm;
use yii\helpers\VarDumper;
use yii\web\Controller;
use frontend\models\SiteMenu;
use yii\helpers\Url;
use \yii\helpers\ArrayHelper;
use yii\helpers\Html;
/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction'
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null
            ],
            'set-locale'=>[
                'class'=>'common\actions\SetLocaleAction',
                'locales'=>array_keys(Yii::$app->params['availableLocales'])
            ]
        ];
    }
    public static function getTranslated($fields) {
	    $arr = [];
	    if (!empty($fields)) {
		    foreach ($fields as $key => $value) {
			    $arr['{' . $key . '}'] = $fields[$key];
		    }
	    }
	    return $arr;
    }
    
    public function actionEzformPrint($ezf_id, $dataid) {
	
	    try {
		
		$modelform = \backend\modules\ezforms\models\Ezform::find()->where('ezf_id = :ezf_id', [':ezf_id' => $ezf_id])->one();
		
		$modelfield = \backend\modules\ezforms\models\EzformFields::find()
			->where('ezf_id = :ezf_id', [':ezf_id' => $ezf_id])
			->orderBy(['ezf_field_order' => SORT_ASC])
			->all();
		
		$table = $modelform->ezf_table;
		
		$model_gen = \backend\modules\ezforms\components\EzformFunc::setDynamicModelLimit($modelfield);
		
		if($dataid>0){
		    $model = new \backend\models\Tbdata();
		    $model->setTableName($table);

		    $query = $model->find()->where('id=:id', [':id'=>$dataid]);

		    $data = $query->one();

		    $model_gen->attributes = $data->attributes;
		}
		
		header("Access-Control-Allow-Origin: *",false);
                
		return $this->renderPartial('_ezform_print', [
		    'modelform'=>$modelform,
		    'modelfield'=>$modelfield,
		    'model_gen'=>$model_gen,
		]);

	    } catch (\yii\db\Exception $e) {
		
	    }
	    
    }
    
    public function actionBotScript()
    {
	ini_set('max_execution_time', 0);
	set_time_limit(0);
	ini_set('memory_limit','1012M'); 
	
        $data = \backend\models\ScriptValidate::find()->where('enable=1')->orderBy('rating DESC')->all();
	
	if($data){
	    $x=1;
	    $gid_validate = \common\lib\codeerror\helpers\GenMillisecTime::getMillisecTime();
	    $error_validate = 0;
	    $total_validate = 0;
	    $passed_validate = 0;
	    foreach ($data as $key => $value_validate) {
		$sql = $value_validate['sql'];
		$data_script;
		if($value_validate['return']=='one'){
		    $data_script = Yii::$app->db->createCommand($sql)->queryOne();
		} else {
		    $data_script = Yii::$app->db->createCommand($sql)->queryAll();
		}
		
		if($data_script){
		    $error_validate++;
		    
		    $fields = $data_script;
		    
		    if($value_validate['process'] != ''){
			eval($value_validate['process']);
		    }
		    
		    if($value_validate['sql_log'] != ''){
			try {
			    $sql_log = $value_validate['sql_log'];
			    Yii::$app->db->createCommand($sql_log)->execute();
			} catch (\yii\db\Exception $e) {
			    echo '<code>SQL LOG ERROR => CODE : '.$e->getCode(). ' MESSAGE : '.$e->getMessage().'</code><br>';
			}
		    }
		    
		    $result = strtr($value_validate['template'], self::getTranslated($fields));
		    
		    $model_log = new \backend\models\ScriptLog();
		    $model_log->gid = $gid_validate;
		    $model_log->active = 1;
		    $model_log->action_at = new \yii\db\Expression('NOW()');
		    $model_log->sid = $value_validate['sid'];
		    $model_log->error = 1;
		    $model_log->save();
		    
		    $model_result = new \backend\models\ScriptResults();
		    $model_result->gid = $gid_validate;
		    $model_result->sid = $value_validate['sid'];
		    $model_result->aid = $model_log->aid;
		    $model_result->title = $value_validate['title'];
		    $model_result->name = $value_validate['name'];
		    $model_result->description = $value_validate['description'];
		    $model_result->rating = $value_validate['rating'];
		    $model_result->result = $result;
		    $model_result->result_at = new \yii\db\Expression('NOW()');
		    $model_result->save();
		    
		    echo $value_validate['title'].' => '.$value_validate['description'].' [ '.$value_validate['name'].' ] ERROR = 1 Result = '.$result.'<br>';
		} else {
		    $passed_validate++;
		    
		    $model_log = new \backend\models\ScriptLog();
		    $model_log->gid = $gid_validate;
		    $model_log->active = 1;
		    $model_log->action_at = new \yii\db\Expression('NOW()');
		    $model_log->sid = $value_validate['sid'];
		    $model_log->error = 0;
		    $model_log->save();
		    
		    echo $value_validate['title'].' => '.$value_validate['description'].' [ '.$value_validate['name'].' ] ERROR = 0 <br>';
		}
		$x++;
		$total_validate++;
	    }
	    $model_group = new \backend\models\ScriptGroup();
	    $model_group->gid = $gid_validate;
	    $model_group->total = $total_validate;
	    $model_group->created_at = new \yii\db\Expression('NOW()');
	    $model_group->passed = $passed_validate;
	    $model_group->error = $error_validate;
	    $model_group->save();

	    echo 'END';
	    
	}
    }
//    public function actionDrumpxxx999555()
//    {
//	
//	$limit = isset($_GET['row'])?$_GET['row']:0;
//	
//	ini_set('max_execution_time', 0);
//	set_time_limit(0);
//	ini_set('memory_limit','512M'); 
//	
//	$sql = "SELECT patient.ptid, patient.ptid_key
//	    FROM patient
//	    WHERE patient.vconsent=1
//	    limit $limit, 1
//	    ";
//	
//	$data = Yii::$app->dbcascap->createCommand($sql)->queryAll();
//	
//	if($data){
//	    $i=0;
//	    $icf_upload1=0;
//	    $icf_upload2=0;
//	    
//	    $ptid = 0;
//	    $ptid_key = 0;
//	    foreach ($data as $key => $value) {
//		$ptid = $value['ptid'];
//		$ptid_key = $value['ptid_key'];
//		
//		$sql = "SELECT id, ptid, icf_upload1, icf_upload2, icf_upload3
//		FROM tb_data_1
//		WHERE id=ptid AND id=:ptid
//		";
//		$tb_data_1 = Yii::$app->db->createCommand($sql, [':ptid'=>$value['ptid']])->queryOne();
//		
//		$icf_upload1=0;
//		$icf_upload2=0;
//		
//		if($tb_data_1){
//		    $i++;
//		    
//		    if($tb_data_1['icf_upload1']==null){
//			
//			$sql = "SELECT file_upload.id, file_upload.tablename, 
//				    file_upload.fieldname, 
//				    file_upload.ptid, 
//				    file_upload.fileactive, 
//				    file_upload.filename, 
//				    file_upload.filenamesys, 
//				    file_upload.fullpath, 
//				    file_upload.filetype, 
//				    file_upload.filesize
//			    FROM file_upload
//			    WHERE file_upload.ptid = :ptid AND file_upload.fieldname='vconsent'
//			    ";
//
//			$data_file = Yii::$app->dbcascap->createCommand($sql, [':ptid'=>$value['ptid_key']])->queryAll();
//			
//			if($data_file){
//			    
//			    foreach ($data_file as $value_file) {
//				$icf_upload1++;
//				$urlfile = "http://www1.cascap.in.th{$value_file['fullpath']}/{$value_file['filenamesys']}";
//				$file_target = Yii::$app->basePath . '/../backend/web/fileinput/' . $value_file['filenamesys'];
//				
//				$imageString = file_get_contents($urlfile);
//				$save = file_put_contents($file_target, $imageString);
//				
//				$status = 0;
//				if($save){
//				    $status = 1;
//				    
//				    $file_db = new \backend\modules\ezforms\models\FileUpload();
//				    $file_db->tbid = $tb_data_1['id'];
//				    $file_db->ezf_id = '1437377239070461301';
//				    $file_db->ezf_field_id = '1446652646071443400';
//				    $file_db->file_active = $value_file['fileactive']==1?1:0;
//				    $file_db->file_name = $value_file['filenamesys'];
//				    $file_db->file_name_old = $value_file['filename'];
//				    $file_db->target = $tb_data_1['ptid'];
//				    $file_db->mode = 1;
//				    
//				    $file_db->save();
//				    
//				    if($value_file['fileactive']==1){
//					Yii::$app->db->createCommand()->update('tb_data_1', [
//					    'icf_upload1'=>$value_file['filenamesys']
//					], 'id=:id', [':id'=>$tb_data_1['id']])
//					->execute();
//				    }
//				    
//				}
//				
//				Yii::$app->db->createCommand()->insert('log_file', [
//				    'ptid'=>$value['ptid'],
//				    'ptid_key'=>$value['ptid_key'],
//				    'file_id'=>$value_file['id'],
//				    'upload_date'=>new \yii\db\Expression('NOW()'),
//				    'file_type'=>'vconsent',
//				    'status'=>$status,
//				])->execute();
//				
//			    }
//			}
//			
//		    }
//		    
//		    if($tb_data_1['icf_upload2']==null){
//			$icf_upload2++;
//			$sql = "SELECT file_upload.id,
//				    file_upload.tablename, 
//				    file_upload.fieldname, 
//				    file_upload.ptid, 
//				    file_upload.fileactive, 
//				    file_upload.filename, 
//				    file_upload.filenamesys, 
//				    file_upload.fullpath, 
//				    file_upload.filetype, 
//				    file_upload.filesize
//			    FROM file_upload
//			    WHERE file_upload.ptid = :ptid AND file_upload.fieldname='cid'
//			    ";
//
//			$data_file = Yii::$app->dbcascap->createCommand($sql, [':ptid'=>$value['ptid']])->queryAll();
//			
//			if($data_file){
//			    foreach ($data_file as $value_file) {
//				$urlfile = "http://www1.cascap.in.th{$value_file['fullpath']}/{$value_file['filenamesys']}";
//				$file_target = Yii::$app->basePath . '/../backend/web/fileinput/' . $value_file['filenamesys'];
//				
//				$imageString = file_get_contents($urlfile);
//				$save = file_put_contents($file_target, $imageString);
//				$status = 0;
//				if($save){
//				    $status = 1;
//				    
//				    $file_db = new \backend\modules\ezforms\models\FileUpload();
//				    $file_db->tbid = $tb_data_1['id'];
//				    $file_db->ezf_id = '1437377239070461301';
//				    $file_db->ezf_field_id = '1446652758038914800';
//				    $file_db->file_active = $value_file['fileactive']==1?1:0;
//				    $file_db->file_name = $value_file['filenamesys'];
//				    $file_db->file_name_old = $value_file['filename'];
//				    $file_db->target = $tb_data_1['ptid'];
//				    $file_db->mode = 1;
//				    
//				    $file_db->save();
//				    
//				    if($value_file['fileactive']==1){
//					Yii::$app->db->createCommand()->update('tb_data_1', [
//					    'icf_upload2'=>$value_file['filenamesys']
//					], 'id=:id', [':id'=>$tb_data_1['id']])
//					->execute();
//				    }
//				    
//				}
//				
//				Yii::$app->db->createCommand()->insert('log_file', [
//				    'ptid'=>$value['ptid'],
//				    'ptid_key'=>$value['ptid_key'],
//				    'file_id'=>$value_file['id'],
//				    'upload_date'=>new \yii\db\Expression('NOW()'),
//				    'file_type'=>'cid',
//				    'status'=>$status,
//				])->execute();
//				
//			    }
//			} else {
//			    Yii::$app->db->createCommand()->insert('log_file', [
//				'ptid'=>$value['ptid'],
//				'ptid_key'=>$value['ptid_key'],
//				'file_id'=>0,
//				'upload_date'=>new \yii\db\Expression('NOW()'),
//				'file_type'=>'icf_not_found',
//				'status'=>0,
//			    ])->execute();
//			}
//			
//		    }
//		    
//		}
//	    }
//	    echo 'PTID:'.$ptid.' / PTID_KEY:'.$ptid_key.' => tb_data_1='.$i.' / icf_upload1='.$icf_upload1. ' / icf_upload2='. $icf_upload2;
//	} else {
//	    echo "not found";
//	}
//	
//    }
    
    public function actionSendMail()
    {
	//mail bot
	$sql = "SELECT user_profile.*, `user`.email AS email2
		FROM `user` INNER JOIN user_profile ON `user`.id = user_profile.user_id
			 INNER JOIN (SELECT user_id, 
					GROUP_CONCAT(item_name) AS items
				FROM rbac_auth_assignment 
				GROUP BY user_id) tmp ON `user`.id = tmp.user_id
		WHERE (user_profile.activate = 0 OR user_profile.activate IS NULL ) AND DATEDIFF(CURRENT_DATE, FROM_UNIXTIME(`user`.created_at)) >=20 AND items NOT LIKE '%manager%' AND items NOT LIKE '%administrator%' AND items NOT LIKE '%adminsite%' AND items NOT LIKE '%frontend%'
		GROUP BY `user`.username Limit 2
		";
	
	$data = Yii::$app->db->createCommand($sql)->queryAll();
	
	if($data){
	    foreach ($data as $key => $value) {
		$sendMail = Yii::$app->commandBus->handle(new \common\commands\command\SendEmailCommand([
		    'from' => [Yii::$app->params['adminEmail'] => Yii::$app->keyStorage->get('sender')],
		    'to' => $value['email2'],
		    'subject' => 'กรุณาอัพโหลดเอกสารยืนยันตัวตน',
		    'view' => 'alermail',
		    'params' => ['user' => $value]
		]));
		
		$dataSave = Yii::$app->db->createCommand()->update('user_profile', ['activate'=>1], 'user_id=:user_id', [':user_id'=>$value['user_id']])->execute();
	    }
	    
	}
	
	//return png
	$imgname = 'mailBot';
	$im = imagecreatefrompng($imgname);
	if(!$im)
	{
	    $im  = imagecreatetruecolor(1, 1);
	    imagesavealpha($im, true); 
	    $color = imagecolorallocatealpha($im,0x00,0x00,0x00,127); 
	    imagefill($im, 0, 0, $color); 
	}
	header('Content-Type: image/png');
	imagepng($im);
	imagedestroy($im);
    }

    public function actionRenderEzf($ezf_id)
    {
        $ezf_id = $_GET['ezf_id'];
        if($_GET['comp_id_target']){
            //ยังไม่รอบรับการเพิ่มแบบมีเป้าหมาย
            return ;
        }else{
            $target = 'skip';
        }
        $params = [
            'ezf_id' => $ezf_id,
            'target' => $target,
            'xsourcex' => '93001'
        ];
        $dataid = \backend\controllers\InputdataController::actionInsertRecord($params, false);


        $modelform = \backend\modules\ezforms\models\Ezform::find()->where(['ezf_id'=>$ezf_id])->one();
        //VarDumper::dump($modelform,10,true); exit;
        $modelfield = \backend\modules\ezforms\models\EzformFields::find()
            ->where('ezf_id = :ezf_id', [':ezf_id' => $modelform->ezf_id])
            ->orderBy(['ezf_field_order' => SORT_ASC])
            ->all();
        $model_gen = \backend\modules\ezforms\components\EzformFunc::setDynamicModelLimit($modelfield);
        $modelDynamic = new \backend\modules\ezforms\models\EzformDynamic($modelform->ezf_table);

        $model_table = $modelDynamic->find()->where('id = :id', [':id' =>$dataid])->One();
        $model_gen->attributes = $model_table->attributes;

        //VarDumper::dump($model_table,10,true); exit;
        return $this->renderAjax('_ezform_render', [
            'modelfield' => $modelfield,
            'model_gen' => $model_gen,
            'datamodel_table' => $model_table
        ]);
    }
    public function actionRenderEzfData($ezf_id)
    {
        $modelform = \backend\modules\ezforms\models\Ezform::find()->where(['ezf_id'=>$ezf_id])->one();
        //VarDumper::dump($modelform,10,true); exit;
        $modelfield = \backend\modules\ezforms\models\EzformFields::find()
            ->where('ezf_id = :ezf_id', [':ezf_id' => $modelform->ezf_id])
            ->orderBy(['ezf_field_order' => SORT_ASC])
            ->all();
        $model_gen = \backend\modules\ezforms\components\EzformFunc::setDynamicModelLimit($modelfield);
        $modelDynamic = new \backend\modules\ezforms\models\EzformDynamic($modelform->ezf_table);
        if($_GET['dataid']) {
            $model_table = $modelDynamic->find()->where('id = :id', [':id' =>$_GET['dataid']])->One();
            $model_gen->attributes = $model_table->attributes;
        }
        //VarDumper::dump($model_table,10,true); exit;
        return $this->renderAjax('_ezform_render', [
            'modelfield' => $modelfield,
            'model_gen' => $model_gen,
            'datamodel_table' => $model_table
        ]);
    }
    
    public function actionMsg($msg, $rurl)
    {
	Yii::$app->getSession()->setFlash('alert', [
		'body'=> $msg,
		'options'=>['class'=>'alert-info']
	    ]);
	    return $this->redirect(base64_decode($rurl));
    }
    
    public function actionIndex()
    {
        $modelsiteconfig = new SiteConfig();
        $siteconfig = $modelsiteconfig->find()->one();
//        $siteconfig->sitecode = $_POST['UserProfile']['sitecode'];

        $domain=Yii::$app->keyStorage->get('frontend.domain');
        $siteprefix= trim(str_replace($domain, "", $_SERVER['HTTP_HOST']));
        $siteprefix= trim(str_replace(".", "",$siteprefix));
        if (is_numeric(substr($siteprefix,0,1))) {
            $sitecode=$siteprefix+0;
        }else{
            $sitecode=$siteprefix;
        }
        if ($sitecode==0) $sitecode="";
        $sqlSiteURL = "SELECT lower(code) code,`url`,`name` FROM site_url WHERE status in (0,3) AND (code='".($sitecode)."' or url='".$siteprefix."')";
        $dataSiteURL = Yii::$app->db->createCommand($sqlSiteURL)->queryOne();
        //if ($dataSiteURL['code']!="") if ($dataSiteURL['code']!="0") {
	
        if ($dataSiteURL['code']!="") {
             $sitemenu = new SiteMenu();
             
             //$menuitems = $sitemenu->findAll(['code' => $dataSiteURL['code']]);
             $menuitems = SiteMenu::find()->where(['lower(code)' => $dataSiteURL['code'],'parentid'=>'0'])->orderBy('order')->all();
             //$sitemenu->findAll("code='".$dataSiteURL['code']."'");
             //\yii\helpers\VarDumper::dump($menuitems->all());exit;
             $i=0;
	    
             if (count($menuitems)>0) foreach ($menuitems as $key => $items) {
		 
                 $submenu = SiteMenu::find()->where(['lower(code)' => $dataSiteURL['code'],'parentid'=>$items->id])->orderBy('order')->all();
                 if (count($submenu)>0) {
                     $menu[$i]['label']='<i class="glyphicon glyphicon-list-alt"></i> ' . $items->name;
                     $j=0;
                     foreach ($submenu as $subkey => $subitems) {
                         $menu[$i]['items'][$j]['label']=$subitems->name;
                         $menu[$i]['items'][$j]['content']=$this->renderAjax('/site-menu/viewajax', [
                                   'model' => SiteMenu::findOne(['code'=>$subitems->code,'id'=>$subitems->id]),
                               ]);                         
                         $menu[$i]['items'][$j]['encode']=false;
                         $j++;
                     }
                 }else{
                     $menu[$i]['label']=$items->name;
                 }
                 $menu[$i]['content']=$this->renderAjax('/site-menu/viewajax', [
                            'model' => SiteMenu::findOne(['code'=>$items->code,'id'=>$items->id]),
                        ]);
                 //$menu[$i]['linkOptions']=['data-url'=>Url::to(['/site-menu/viewajax?code='.$items->code.'&id='.$items->id])];
                 if (Yii::$app->request->get('id')!="") {
                     if ($items->id==Yii::$app->request->get('id')) $menu[$i]['active']=true;
                 }else{
                    if ($items->active=="1") {
                        $menu[$i]['active']=true;
                    }
                 }
                 $i++;
             }

        }


        $stats_tb_data_2 = array();
        $stats_tb_data_3 = array();
        $stats_tb_data_2_month = array();
        $stats_tb_data_3_month = array();
        $graphSiteOrder = array();
        $graphSiteOrder2 = array();
        if(Yii::$app->keyStorage->get('frontend.domain')=='cascapx.in.th') {
            //Yii::$app->keyStorage->get('frontend.domain')=='cascap.in.th'
            $sql = "SELECT
			COUNT(DISTINCT target) as Count,
			date_format(f1vdcompdb,'%Y') as Year,
			quarter(f1vdcompdb) as Quarter
            FROM tb_data_2
            WHERE rstat = '4'
                AND f1vdcompdb != ''
                AND f1vdcompdb >= '2013-02-09 00:00:00'
            GROUP BY Year, Quarter
            ORDER BY f1vdcompdb DESC";
            $query = Yii::$app->db->createCommand($sql)->queryAll();
            ArrayHelper::multisort($query, 'Year', SORT_ASC);
            $countQuery = count($query);
            if($countQuery > 0){
                $stats_tb_data_2['count'] = $countQuery;
            }
            $stats_tb_data_2['options']['title']['text'] = 'จำนวนกลุ่มเสี่ยงที่มีข้อมูลพื้นฐาน (CCA 01)';
            $categories = array();
            $stat = array();
            $accumulateArray = array();
            $accumulate = 0;
            foreach ($query as $value) {
                if ($value['Quarter'] == 1) {
                    array_push($categories, ('ม.ค. - มี.ค. ' . ($value[Year] + 543)));
                } else if ($value['Quarter'] == 2) {
                    array_push($categories, ('เม.ย. - มิ.ย. ' . ($value[Year] + 543)));
                } else if ($value['Quarter'] == 3) {
                    array_push($categories, ('ก.ค. - ก.ย. ' . ($value[Year] + 543)));
                } else if ($value['Quarter'] == 4) {
                    array_push($categories, ('ต.ค. - ธ.ค. ' . ($value[Year] + 543)));
                }
                $accumulate +=(int)$value['Count'];
                array_push($accumulateArray, $accumulate);
                array_push($stat, (int)$value['Count']);
            }
            $stats_tb_data_2['options']['xAxis'] = ['categories' => $categories];
            $stats_tb_data_2['options']['series'] = [
                [
                    'type' => 'column',
                    'name' => 'จำนวน',
                    'data' => $stat
                ],
                [
                    'type' => 'spline',
                    'name' => 'จำนวน',
                    'data' => $accumulateArray,
                ]
            ];


            $sql2 = "SELECT
			COUNT(DISTINCT target) as Count,
			date_format(f2v1db,'%Y') as Year,
			quarter(f2v1db) as Quarter
            FROM tb_data_3
            WHERE rstat = '2'
                AND f2v1db != ''
                AND f2v1db >= '2013-02-09 00:00:00'
            GROUP BY Year, Quarter
            ORDER BY f2v1db DESC";
            $query2 = Yii::$app->db->createCommand($sql2)->queryAll();
            ArrayHelper::multisort($query2, 'Year', SORT_ASC);
            $countQuery2 = count($query2);
            if($countQuery2 > 0){
                $stats_tb_data_3['count'] = $countQuery2;
            }
            $stats_tb_data_3['options']['title']['text'] = 'จำนวนกลุ่มเสี่ยงที่ได้รับการตรวจอัลตร้าซาวด์ (CCA 02)';
            $categories = array();
            $stat = array();
            $accumulateArray = array();
            $accumulate = 0;
            foreach ($query2 as $value) {
                if ($value['Quarter'] == 1) {
                    array_push($categories, ('มกราคม - มีนาคม ' . ($value[Year] + 543)));
                } else if ($value['Quarter'] == 2) {
                    array_push($categories, ('เมษายน - มิถุนายน ' . ($value[Year] + 543)));
                } else if ($value['Quarter'] == 3) {
                    array_push($categories, ('กรกฎาคม - กันยายน ' . ($value[Year] + 543)));
                } else if ($value['Quarter'] == 4) {
                    array_push($categories, ('ตุลาคม - ธันวาคม ' . ($value[Year] + 543)));
                }
                $accumulate +=(int)$value['Count'];
                array_push($accumulateArray, $accumulate);
                array_push($stat, (int)$value['Count']);
            }
            $stats_tb_data_3['options']['xAxis'] = ['categories' => $categories];
            $stats_tb_data_3['options']['series'] = [
                [
                    'type' => 'column',
                    'name' => 'จำนวน',
                    'data' => $stat
                ],
                [
                    'type' => 'spline',
                    'name' => 'จำนวน',
                    'data' => $accumulateArray,
                ]
            ];



            $sql3 = "SELECT
			COUNT(DISTINCT target) as countTarget,
			date_format(f1vdcompdb,'%Y-%m') as yearMonth
            FROM tb_data_2
            WHERE rstat = '4'
                AND f1vdcompdb is not null
                AND f1vdcompdb >= '2013-02-09 00:00:00'
            GROUP BY yearMonth
            ORDER BY f1vdcompdb ASC";
            $query3 = Yii::$app->db->createCommand($sql3)->queryAll();
            $month = array(
                '',
                'มกราคม',
                'กุมภาพันธ์',
                'มีนาคม',
                'เมษายน',
                'พฤษภาคม',
                'มิถุนายน',
                'กรกฎาคม',
                'สิงหาคม',
                'กันยายน',
                'ตุลาคม',
                'พฤศจิกายน',
                'ธันวาคม'
            );
            $countQuery3 = count($query3);
            if($countQuery3 > 0){
                $stats_tb_data_2_month['count'] = $countQuery3;
            }
            $stats_tb_data_2_month['options']['title']['text'] = 'จำนวนกลุ่มเสี่ยงที่มีข้อมูลพื้นฐาน (CCA 01)';
            $categories = array();
            $stat = array();
            $accumulateArray = array();
            $accumulate = 0;
            foreach ($query3 as $value) {
                $yearmonth = explode('-',$value['yearMonth']);
                array_push($categories, $month[intval($yearmonth[1])] . ' '.(intval($yearmonth[0]) + 543));
                $accumulate +=(int)$value['countTarget'];
                array_push($accumulateArray, $accumulate);
                array_push($stat, (int)$value['countTarget']);
            }
            $stats_tb_data_2_month['options']['xAxis'] = ['categories' => $categories];
            $stats_tb_data_2_month['options']['series'] = [
                [
                    'type' => 'column',
                    'name' => 'จำนวน',
                    'data' => $stat
                ],
                [
                    'type' => 'spline',
                    'name' => 'จำนวน',
                    'data' => $accumulateArray,
                ]
            ];

            $sql4 = "SELECT
			COUNT(DISTINCT target) as countTarget,
			date_format(f2v1db,'%Y-%m') as yearMonth
            FROM tb_data_3
            WHERE rstat = '2'
                AND f2v1db is not null
                AND f2v1db >= '2013-02-09 00:00:00'
            GROUP BY yearMonth
            ORDER BY f2v1db ASC";
            $query4 = Yii::$app->db->createCommand($sql4)->queryAll();
            $countQuery4 = count($query4);
            if($countQuery4 > 0){
                $stats_tb_data_3_month['count'] = $countQuery4;
            }
            $stats_tb_data_3_month['options']['title']['text'] = 'จำนวนกลุ่มเสี่ยงที่ได้รับการตรวจอัลตร้าซาวด์ (CCA 02)';
            $categories = array();
            $stat = array();
            $accumulateArray = array();
            $accumulate = 0;
            foreach ($query4 as $value) {
                $yearmonth = explode('-',$value['yearMonth']);
                array_push($categories, $month[intval($yearmonth[1])] . ' '.(intval($yearmonth[0]) + 543));
                $accumulate +=(int)$value['countTarget'];
                array_push($accumulateArray, $accumulate);
                array_push($stat, (int)$value['countTarget']);
            }
            $stats_tb_data_3_month['options']['xAxis'] = ['categories' => $categories];
            $stats_tb_data_3_month['options']['series'] = [
                [
                    'type' => 'column',
                    'name' => 'จำนวน',
                    'data' => $stat
                ],
                [
                    'type' => 'spline',
                    'name' => 'จำนวน',
                    'data' => $accumulateArray,
                ]
            ];



            $sql5 = "SELECT DISTINCT count(*) as count, zone_name
                    FROM 	tb_data_2,
                                all_hospital_thai
                    WHERE tb_data_2.hsitecode = all_hospital_thai.hcode
                    GROUP BY all_hospital_thai.zone_code";
            $query5 = Yii::$app->db->createCommand($sql5)->queryAll();
            $countQuery5 = count($query5);
            if($countQuery5 > 0){
                $graphSiteOrder['count'] = $countQuery5;
            }
            $graphSiteOrder['options']['title']['text'] = 'จำแนกตามจังหวัด CCA - 01';
            $categories = array();
            $stat = array();
            foreach ($query5 as $value) {
                array_push($categories, ($value['zone_name']));
                array_push($stat, (int)$value['count']);
            }
            $graphSiteOrder['options']['xAxis'] = ['categories' => $categories];
            $graphSiteOrder['options']['series'] = [
                [
                    'type' => 'column',
                    'name' => 'จำนวน',
                    'data' => $stat
                ]
            ];



            $sql6 = "SELECT DISTINCT count(*) as count, zone_name
                        FROM tb_data_3, all_hospital_thai
                        WHERE tb_data_3.hsitecode = all_hospital_thai.hcode
                        GROUP BY all_hospital_thai.zone_code";
            $query6 = Yii::$app->db->createCommand($sql6)->queryAll();
            $countQuery6 = count($query6);
            if($countQuery6 > 0){
                $graphSiteOrder2['count'] = $countQuery6;
            }
            $graphSiteOrder2['options']['title']['text'] = 'จำแนกตามจังหวัด CCA -02';
            $categories = array();
            $stat = array();
            foreach ($query6 as $value) {
                array_push($categories, ($value['zone_name']));
                array_push($stat, (int)$value['count']);
            }
            $graphSiteOrder2['options']['xAxis'] = ['categories' => $categories];
            $graphSiteOrder2['options']['series'] = [
                [
                    'type' => 'column',
                    'name' => 'จำนวน',
                    'data' => $stat
                ]
            ];

            $sqlMemberRegis = 'SELECT user_id,
                code4,
                name5,
                FROM_UNIXTIME(created_at) dateTimeRegis
            FROM user_profile,
                `user`,
                all_hospital_thai
            WHERE user_profile.sitecode = all_hospital_thai.hcode
                AND user_profile.user_id = `user`.id
            ORDER BY cast(code4 as decimal)';

            $qryMemberRegis = Yii::$app->db->createCommand($sqlMemberRegis)->queryAll();


            $resultMemberRegis = ['central'=>[],'general'=>[],'rural'=>[],'district'=>[]];
            foreach ($resultMemberRegis as $key => $value) {
                //echo $key;
                $resultMemberRegis[$key]['all'] = 0;
                $resultMemberRegis[$key]['year'] = 0;
                $resultMemberRegis[$key]['month'] = 0;
                $resultMemberRegis[$key]['week'] = 0;
            }
            //var_dump($memberRegis);
            $thisYear = date('Y');
            $thisMonth = date('m');
            $thisWeek = date('W');
            foreach ($qryMemberRegis as $key => $value) {

                if($value['code4']==5){
                    $indexName = 'central';
                }else if($value['code4']==6){
                    $indexName = 'general';
                }else if($value['code4']==7 || $value['code4']==11 || $value['code4']==12){
                    $indexName = 'rural';
                }else if($value['code4']==8 || $value['code4']==18){
                    $indexName = 'district';
                }else{
                    continue;
                }

                $dt = explode(' ', $value['dateTimeRegis']);
                $createWeek = date('W',strtotime($value['dateTimeRegis']));
                $d = explode('-', $dt[0]);
                $createMonth = $d[1];
                $createYear = $d[0];

                $incAll = 1;
                $incYear = $incMonth = $incWeek = 0;

                if($createWeek == thisWeek && $createYear == $thisYear){
                    $incYear = $incMonth = $incWeek = 1;
                }else if($createMonth == thisMonth && $createYear == $thisYear){
                    $incYear = $incMonth = 1;
                }else if($createYear == $thisYear){
                    $incYear = 1;
                }



                $resultMemberRegis[$indexName]['all']+=$incAll;
                $resultMemberRegis[$indexName]['year']+=$incYear;
                $resultMemberRegis[$indexName]['month']+=$incMonth;
                $resultMemberRegis[$indexName]['week']+=$incWeek;
            }
        }


        $this->layout = 'main_gray';
        return $this->render('index',[
            'siteconfig'=>$siteconfig,
            'menuitems'=>$menu,
            'query' => $stats_tb_data_2,
            'query3' => $stats_tb_data_3,
            'table2month' => $stats_tb_data_2_month,
            'table3month' => $stats_tb_data_3_month,
            'graphSiteOrder' => $graphSiteOrder,
            'graphSiteOrder2' => $graphSiteOrder2,
            'memberRegis' => $resultMemberRegis
        ]);
    }

    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($model->contact(Yii::$app->params['adminEmail'])) {
                Yii::$app->getSession()->setFlash('alert', [
                    'body'=>Yii::t('frontend', 'Thank you for contacting us. We will respond to you as soon as possible.'),
                    'options'=>['class'=>'alert-success']
                ]);
                return $this->refresh();
            } else {
                Yii::$app->getSession()->setFlash('alert', [
                    'body'=>\Yii::t('frontend', 'There was an error sending email.'),
                    'options'=>['class'=>'alert-danger']
                ]);
            }
        }

        return $this->render('contact', [
            'model' => $model
        ]);
    }

    public function actionUpdatePassword(){
        //update user cascap
        $username = $_GET['username'];
        $password = Yii::$app->getSecurity()->generatePasswordHash($_GET['password']);
        //1350100190818
        //$model = User::find()->where('username = :username', [':username' => $username])->one();
        //$model->password_hash = Yii::$app->getSecurity()->generatePasswordHash($password);
        //$model->save();

        Yii::$app->db->createCommand("UPDATE user SET password_hash = :password_hash WHERE username = :username;", [
            ':password_hash' => $password,
            ':username' => $username
        ])->query();

        return ;
    }
}
