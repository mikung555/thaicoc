<?php

namespace frontend\controllers;
use yii\helpers\Json;
use yii\web\Response;
class UltrasoundController extends \yii\web\Controller
{
    public function actionIndex() {
        //$json='[{"id":"1382601222466","ptid":"1382601084419","sitecode":"10949","ptcode":"00088","hsitecode":"10949","hptcode":"00088","url":"https://cloudbackend.cascap.in.th/inputdata/step4?comp_id_target=1437725343023632100&ezf_id=1437619524091524800&target=MTM4MjYwMTA4NDQxOQ%3D%3D&dataid=1382601222466"},{"id":"1382601263258","ptid":"1382601084419","sitecode":"10949","ptcode":"00088","hsitecode":"10949","hptcode":"00088","url":"https://cloudbackend.cascap.in.th/inputdata/step4?comp_id_target=1437725343023632100&ezf_id=1437619524091524800&target=MTM4MjYwMTA4NDQxOQ%3D%3D&dataid=1382601263258"},{"id":"1394521946830","ptid":"1382601084419","sitecode":"10949","ptcode":"00088","hsitecode":"10949","hptcode":"00088","url":"https://cloudbackend.cascap.in.th/inputdata/step4?comp_id_target=1437725343023632100&ezf_id=1437619524091524800&target=MTM4MjYwMTA4NDQxOQ%3D%3D&dataid=1394521946830"},{"id":"1394521972187","ptid":"1382601084419","sitecode":"10949","ptcode":"00088","hsitecode":"10949","hptcode":"00088","url":"https://cloudbackend.cascap.in.th/inputdata/step4?comp_id_target=1437725343023632100&ezf_id=1437619524091524800&target=MTM4MjYwMTA4NDQxOQ%3D%3D&dataid=1394521972187"}]';
        //\yii\helpers\VarDumper::dump(json_decode($json,true),10,true);
        $response['result']="OK";
        \Yii::$app->response->format = Response::FORMAT_JSON;
        return $response;
    }
    
    public function actionPatient($ptcode) {
        $hsitecode=  substr($ptcode, 0, 5);
        $hptcode=  substr($ptcode, 5, 5);
        $sql = "SELECT ptid from tb_data_1 where hsitecode='$hsitecode' and hptcode='$hptcode'  and rstat<>'3'";
        $patient = \Yii::$app->db->createCommand($sql)->queryOne();
        $ptid = $patient['ptid'];
        $sql="SELECT id,ptid,sitecode,ptcode,hsitecode,hptcode,f2v1 from tb_data_3 WHERE ptid='$ptid'";       
        $cca02=\Yii::$app->db->createCommand($sql)->queryAll();
        
        $url="https://cloudbackend.cascap.in.th/inputdata/step4?comp_id_target=1437725343023632100&ezf_id=1437619524091524800&target=TARGET&dataid=DATAID";
        if (count($cca02)>0) foreach($cca02 as $key => $us) {
            $urlx=  str_replace("TARGET", urlencode(base64_encode($us['ptid'])), $url);
            $urlx=  str_replace("DATAID", $us['id'], $urlx);
            $cca02[$key]['url']=$urlx;
        }
        //\yii\helpers\VarDumper::dump($cca02,10,true);
        
        //echo Json::encode($cca02,JSON_UNESCAPED_UNICODE);
        \Yii::$app->response->format = Response::FORMAT_JSON;
        return $cca02;
    }
    public function actionAdd($ptcode) {
        $hsitecode=  substr($ptcode, 0, 5);
        $hptcode=  substr($ptcode, 5, 5);        
        $url="https://cloudbackend.cascap.in.th/inputdata/step4?comp_id_target=1437725343023632100&ezf_id=1437619524091524800&target=TARGET&dataid=DATAID"; 
        $url="https://cloudbackend.cascap.in.th/inputdata/step4?comp_id_target=1437725343023632100&ezf_id=1437619524091524800&target=TARGET";
        $sql = "SELECT ptid from tb_data_1 where hsitecode='$hsitecode' and hptcode='$hptcode'  and rstat<>'3'";
        $patient = \Yii::$app->db->createCommand($sql)->queryOne();        
        
        $response['url']=  str_replace("TARGET", urlencode(base64_encode($patient['ptid'])), $url);
        \Yii::$app->response->format = Response::FORMAT_JSON;
        return $response;        
    }
    public function actionAddCid($cid,$sitecode) {      
        $url="https://cloudbackend.cascap.in.th/inputdata/step4?comp_id_target=1437725343023632100&ezf_id=1437619524091524800&target=TARGET&dataid=DATAID"; 
        $url="https://cloudbackend.cascap.in.th/inputdata/step4?comp_id_target=1437725343023632100&ezf_id=1437619524091524800&target=TARGET";
        $sql = "SELECT ptid from tb_data_1 where cid LIKE '$cid' and hsitecode='$sitecode' and rstat<>'3'";
        $patient = \Yii::$app->db->createCommand($sql)->queryOne();        
        
        $response['url']=  str_replace("TARGET", urlencode(base64_encode($patient['ptid'])), $url);
        \Yii::$app->response->format = Response::FORMAT_JSON;
        return $response;        
    }    
}
