<?php

namespace frontend\controllers;

use backend\models\QueryList;
use backend\models\QueryRequest;
use backend\modules\ezforms\components\EzformQuery;
use yii\db\Expression;
use yii\helpers\VarDumper;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * @author Eugene Terentev <eugene@terentev.net>
 */
class BotController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
       return;
    }

    /**
     * @param $slug
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionPhoneBook()
    {
        //$api = 'https://api.telegram.org/bot185575370:AAFiFWiv0KWkuZFb16fLOoK097kKiS4-AU8/sendmessage?chat_id=&text=Phone&reply_markup={"keyboard":[["Kongvut","Panuwat"]]}';
        $api = 'https://api.telegram.org/bot185575370:AAFiFWiv0KWkuZFb16fLOoK097kKiS4-AU8';

        // read incoming info and grab the chatID
        $content = file_get_contents("php://input");
        $update = json_decode($content, true);
        $chatID = $update["message"]["chat"]["id"];
        file_put_contents('/img/bot/log.txt','zzz:'.$_POST);

        if($update["message"]['text']=='phone') {
            // compose reply
            $replyMarkup = array(
                'keyboard' => array(
                    array("Kongvut", "Panuwat")
                )
            );
            $encodedMarkup = json_encode($replyMarkup);

            // send reply
            $sendto = $api . "/sendmessage?chat_id=" . $chatID . "&text=Phone&reply_markup=".$encodedMarkup;
        }
        if($update["message"]['text']=='Kongvut') {
            // compose reply


            // send reply
            $sendto = $api . "/sendmessage?chat_id=" . $chatID . "&text=0875442559";
        }
        if($_GET['test']==1){
            $sendto = $api . "/sendmessage?chat_id=-1001037928555&text=0875442559";
            echo $sendto;
        }
        file_get_contents($sendto);
    }

    public function actionQueryTools(){
        //GET Values
        $ezf_id = $_GET['ezf_id'];//get
        $target_id = $_GET['target_id'];//get
        $data_id = $_GET['data_id'];//get
        $ezf_field_name = $_GET['ezf_field_name'];//get

        $ezform = EzformQuery::getFormTableName($ezf_id);
        $res = Yii::$app->db->createCommand("SELECT `".($ezf_field_name)."`, xsourcex, user_create FROM `".($ezform->ezf_table)."` WHERE id=:id", [':id'=>$data_id])->queryOne();
        $xsourcex = $res['xsourcex'];
        //
        $ezf_field = Yii::$app->db->createCommand("SELECT ezf_field_id FROM `ezform_fields` WHERE ezf_id = :ezf_id AND ezf_field_name = :ezf_field_name", [':ezf_id' => $ezf_id, ':ezf_field_name' => $ezf_field_name])->queryOne();
        //
        if($ezf_field['ezf_field_id']) {
            //
            $model = new QueryRequest();
            $modelQueryList = new QueryList();
            //
            $model->ezf_id = $ezf_id;
            $model->ezf_field_id = $ezf_field['ezf_field_id'];
            $model->target_id = $target_id;
            $model->data_id = $data_id;
            $model->note = $_GET['note']; //get
            $field[$ezf_field_name] = $res[$ezf_field_name];
            $model->old_val = json_encode($field);
            $field[$ezf_field_name] = $_GET['new_val']; //get
            $model->new_val = json_encode($field);
            $model->new_changed = json_encode($field);
            $model->userkey = $res['user_create'];
            $model->userkey_status = 3; //purify
            $model->xsourcex = $xsourcex;
            $model->status = 2; //resolve with some change
            $model->user_create = $_GET['user_create']; //get
            $model->approved_by = $_GET['user_create']; //get
            $model->approved_note = $_GET['approved_note']; //get
            $model->time_create = new Expression('NOW()');
            //VarDumper::dump($model, 10, true); Yii::$app->end();
            if ($model->save()) {
                //save query list
                $modelQueryList->query_id = $model->id;
                $modelQueryList->user_id_by = Yii::$app->user->identity->id;
                $modelQueryList->user_id_to = $_POST['queryUserTo'] ? $_POST['queryUserTo'] : $model->userkey;
                $modelQueryList->create_at = new Expression('NOW()');
                $modelQueryList->save();
                ///process update value from table data
                $ezform = EzformQuery::getFormTableName($ezf_id);
                Yii::$app->db->createCommand()->update($ezform->ezf_table, json_decode($model->new_val, true), 'id = :idx', [':idx' => $data_id])->execute();
                //end save
                return 'success';
            }

        }
        return ;
    }
}
