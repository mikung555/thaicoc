<?php
 
namespace frontend\controllers;
use Yii;
use yii\web\Controller;
class FeedTestController extends Controller{
    public function actionIndex(){
        header("access-control-allow-origin: *");
        $user = \backend\modules\inv\models\InvUser::find()->all();
        echo \yii\helpers\Json::encode($user);
    }
}
