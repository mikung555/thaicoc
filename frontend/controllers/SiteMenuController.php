<?php

namespace frontend\controllers;

use Yii;
use frontend\models\SiteMenu;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Json;
/**
 * SiteMenuController implements the CRUD actions for SiteMenu model.
 */
class SiteMenuController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all SiteMenu models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => SiteMenu::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single SiteMenu model.
     * @param string $code
     * @param integer $id
     * @return mixed
     */
    public function actionView($code, $id)
    {
        return $this->render('view', [
            'model' => $this->findModel($code, $id),
        ]);
    }
    public function actionViewajax($code, $id)
    {
        $html = $this->renderAjax('viewajax', [
            'model' => $this->findModel($code, $id),
        ]);
        return Json::encode($html);
    }
    /**
     * Creates a new SiteMenu model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($code)
    {
        $model = new SiteMenu();
        $modelmainmenu = SiteMenu::find()->where(['code' => $code,'parentid'=>0])->addOrderBy('order')->all();
        
        $modelmenu = SiteMenu::find()->where(['code' => $code])->addOrderBy('order')->all();
        
        
        if ($model->load(Yii::$app->request->post())) {
            
            $max=Yii::$app->db->createCommand("SELECT max(id)+1 as id from site_menu where code='$code'")->queryOne();
            
            $model->id=$max['id'];
            
            $model->code=$code;
            if ($model->save()) {
                $this->reorder($model->code);
            }
            return $this->redirect(['site/index']);
        } else {
            
            return $this->render('create', [
                'model' => $model,
                'modelmainmenu' => $modelmainmenu,
                'modelmenu' => $modelmenu,                 
            ]);
        }
    }

    /**
     * Updates an existing SiteMenu model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $code
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($code, $id)
    {
        $model = $this->findModel($code, $id);
        //$modelmainmenu = SiteMenu::findAll(['code' => $code,'parentid'=>'0']);
        $modelmainmenu = SiteMenu::find()->where(['code' => $code,'parentid'=>0])->addOrderBy('order')->all();
        //$modelmenu = SiteMenu::findAll(['code' => $code]);
        $modelmenu = SiteMenu::find()->where(['code' => $code])->addOrderBy('order')->all();
        
        if ($model->load(Yii::$app->request->post()) ) {
            $model->order=$model->order+5;
	    
            if($model->save()){
		$this->reorder($model->code);
	    }
            
            return $this->redirect(['/', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model, 
                'modelmainmenu' => $modelmainmenu,
                'modelmenu' => $modelmenu, 
            ]);
        }
    }
    public function reorder($code) {
        Yii::$app->db->createCommand("set @i:=0;UPDATE site_menu set `order`=(@i:=@i+1)*10 WHERE code='$code' order by `order`")->query();
    }
    /**
     * Deletes an existing SiteMenu model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $code
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($code, $id)
    {
        $this->findModel($code, $id)->delete();

        return $this->goHome();
    }

    /**
     * Finds the SiteMenu model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $code
     * @param integer $id
     * @return SiteMenu the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($code, $id)
    {
        if (($model = SiteMenu::findOne(['code' => $code, 'id' => $id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
