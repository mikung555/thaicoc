<?php

namespace frontend\controllers;

use frontend\controllers\classes\ModulesReport;
use frontend\controllers\classes\TimelineReport;
use Yii;
use yii\web\Controller;

class ReportCocsController extends Controller {

    //put your code here
    public function actionIndex() {
        return $this->render("index");
    }

    public function actionShowOverview() {

        $dataAll = array();

        array_push($dataAll, [
            'section', ['จำนวนหน่วยงาน (หน่วย)', 'ทั้งหมด', 'เป็นสมาชิก', '(%)', 'ปีนี้', 'เดือนนี้', 'สัปดาห์นี้', 'วันนี้']
                ]
        );

        $sqlDeptMember = "select code4,count(*) nall from  all_hospital_thai h  where h.code2 not like '%demo%' and code4 is not null and (code4 <> 15 and code4 <> 16 AND code4 <> 80 ) group by code4 order by FIELD(code4+0,5,6,7,18,3,4,8,13,17,11,12,15,16,1,2,10,0,null);";
        $qryDeptMember = Yii::$app->db->createCommand($sqlDeptMember)->queryAll();
        // FIELD(code4+0,5,6,7,18,3,4,8,13,17,11,12,15,16,1,2,0,null,10)
        foreach ($qryDeptMember as $membercount) {
            if (in_array($membercount['code4'], array("10"))) {
                $orgname = "หน่วยงานส่วนกลาง";
            } else if (in_array($membercount['code4'], array("1"))) {
                $orgname = "สำนักงานสาธารณสุขจังหวัด";
            } else if (in_array($membercount['code4'], array("2"))) {
                $orgname = "สำนักงานสาธารณสุขอำเภอ";
            } else if (in_array($membercount['code4'], array("5"))) {
                $orgname = "โรงพยาบาลศูนย์"; //"รพศ.";
            } else if (in_array($membercount['code4'], array("6"))) {
                $orgname = "โรงพยาบาลทั่วไป"; //"รพท.";
            } else if (in_array($membercount['code4'], array("7"))) {
                $orgname = "โรงพยาบาลชุมชน หรือโรงพยาบาลสมเด็จพระยุพราช"; //"รพช. รพร.";
            } else if (in_array($membercount['code4'], array("18", "3", "4", "8", "13", "17"))) {
                $orgname = "โรงพยาบาลส่งเสริมสุขภาพประจำตำบล"; //"รพ.สต. สถานีอนามัย";
            } else if (in_array($membercount['code4'], array("11", "12"))) {
                $orgname = "โรงพยาบาล นอก สธ.";
            }
//            else if (in_array($membercount['code4'], array("15", "16"))) {
//                $orgname = "โรงพยาบาลเอกชน และคลินิกเอกชน";
//            } 
//            else {
//                $orgname = "อื่นๆ";
//            }

            $dataAllH[$orgname]['All'] += $membercount['nall'];
            $dataAllH['All']['All'] += $membercount['nall'];
            $countp[$orgname]['HAll'] += $membercount['nall'];
        }

        $sqlDeptMember = "select code4,COUNT(distinct sitecode) as nall,count(distinct IF(YEAR(FROM_UNIXTIME(created_at))=YEAR(CURDATE()),sitecode,null)) as thisYear,count(distinct IF(YEAR(FROM_UNIXTIME(created_at))=YEAR(CURDATE()) AND MONTH(FROM_UNIXTIME(created_at))=MONTH(CURDATE()),sitecode,null)) as thisMonth,count(distinct IF(YEARWEEK(FROM_UNIXTIME(`created_at`)) = YEARWEEK(NOW()),sitecode,null)) as thisWeek,count(distinct IF(DATE(FROM_UNIXTIME(created_at))=CURDATE(),sitecode,null)) as today  from user u inner join user_profile p ON u.id=p.user_id inner join all_hospital_thai h on p.sitecode=h.hcode where h.code2 not like '%demo%'  and status_del=0 and (code4 <> 15 and code4 <> 16 AND code4 <> 80 ) group by code4 order by FIELD(code4+0,5,6,7,18,3,4,8,13,17,11,12,15,16,1,2,10,0,null);";
        $qryDeptMember = Yii::$app->db->createCommand($sqlDeptMember)->queryAll();

        foreach ($qryDeptMember as $membercount) {
            if (in_array($membercount['code4'], array("10"))) {
                $orgname = "หน่วยงานส่วนกลาง";
            } else if (in_array($membercount['code4'], array("1"))) {
                $orgname = "สำนักงานสาธารณสุขจังหวัด";
            } else if (in_array($membercount['code4'], array("2"))) {
                $orgname = "สำนักงานสาธารณสุขอำเภอ";
            } else if (in_array($membercount['code4'], array("5"))) {
                $orgname = "โรงพยาบาลศูนย์"; //"รพศ.";
            } else if (in_array($membercount['code4'], array("6"))) {
                $orgname = "โรงพยาบาลทั่วไป"; //"รพท.";
            } else if (in_array($membercount['code4'], array("7"))) {
                $orgname = "โรงพยาบาลชุมชน หรือโรงพยาบาลสมเด็จพระยุพราช"; //"รพช. รพร.";
            } else if (in_array($membercount['code4'], array("18", "3", "4", "8", "13", "17"))) {
                $orgname = "โรงพยาบาลส่งเสริมสุขภาพประจำตำบล"; //"รพ.สต. สถานีอนามัย";
            } else if (in_array($membercount['code4'], array("11", "12"))) {
                $orgname = "โรงพยาบาล นอก สธ.";
            }
//            else if (in_array($membercount['code4'], array("15", "16"))) {
//                $orgname = "โรงพยาบาลเอกชน และคลินิกเอกชน";
//            }
//            else {
//                $orgname = "อื่นๆ";
//            }

            $dataAllP[$orgname]['All'] += $membercount['nall'];
            $dataAllP['All']['All'] += $membercount['nall'];

            $countp[$orgname]['All'] += $membercount['nall'];
            $countp[$orgname]['Year'] += $membercount['thisYear'];
            $countp[$orgname]['Month'] += $membercount['thisMonth'];
            $countp[$orgname]['Week'] += $membercount['thisWeek'];
            $countp[$orgname]['Day'] += $membercount['today'];
        }

        foreach ($countp as $orgname => $data) {
            $countAllH = $data['HAll'];
            $countAllP = $data['All'];
            $countpercentAllP = number_format($data['All'] / $countAllH * 100, 1) . "%";
            $countYearP = $data['Year'];
            $countMonthP = $data['Month'];
            $countYearWeekRegisP = $data['Week'];
            $countDayP = $data['Day'];

            $sumAllH += $countAllH;
            $sumAllP += $countAllP;
            $sumYearP += $countYearP;
            $sumMonthP += $countMonthP;
            $sumYearWeekRegisP += $countYearWeekRegisP;
            $sumDayP += $countDayP;
            $percentAllP = number_format($sumAllP / $sumAllH * 100, 1) . "%";


            array_push($dataAll, [
                'name', [$orgname, number_format($countAllH), number_format($countAllP), $countpercentAllP, number_format($countYearP), number_format($countMonthP), number_format($countYearWeekRegisP), number_format($countDayP)]
            ]);
        }

        array_push($dataAll, [
            'name', ['รวมทั้งหมด', number_format($sumAllH), number_format($sumAllP), $percentAllP, number_format($sumYearP), number_format($sumMonthP), number_format($sumYearWeekRegisP), number_format($sumDayP)]
                ]
        );

        array_push($dataAll, [
            'section', ['จำนวนสมาชิก บุคลากร (คน)', '', '', 'ทั้งหมด', 'ปีนี้', 'เดือนนี้', 'สัปดาห์นี้', 'วันนี้']
                ]
        );
        $sqlDeptMember = "select code4,COUNT(*) as nall,SUM(YEAR(FROM_UNIXTIME(created_at))=YEAR(CURDATE())) as thisYear,SUM(YEAR(FROM_UNIXTIME(created_at))=YEAR(CURDATE()) AND MONTH(FROM_UNIXTIME(created_at))=MONTH(CURDATE())) as thisMonth,SUM(YEARWEEK(FROM_UNIXTIME(`created_at`)) = YEARWEEK(NOW())) as thisWeek,SUM(DATE(FROM_UNIXTIME(created_at))=CURDATE()) as today  from user u inner join user_profile p ON u.id=p.user_id inner join all_hospital_thai h on p.sitecode=h.hcode where h.code2 not like '%demo%' and code4 is not null and status_del=0 and volunteer_status=0 group by code4 order by FIELD(code4+0,5,6,7,18,3,4,8,13,17,11,12,15,16,1,2,10,0,null);";
        $qryDeptMember = Yii::$app->db->createCommand($sqlDeptMember)->queryAll();

        foreach ($qryDeptMember as $membercount) {
            if (in_array($membercount['code4'], array("10"))) {
                $orgname = "หน่วยงานส่วนกลาง";
            } else if (in_array($membercount['code4'], array("1"))) {
                $orgname = "สำนักงานสาธารณสุขจังหวัด";
            } else if (in_array($membercount['code4'], array("2"))) {
                $orgname = "สำนักงานสาธารณสุขอำเภอ";
            } else if (in_array($membercount['code4'], array("5"))) {
                $orgname = "โรงพยาบาลศูนย์"; //"รพศ.";
            } else if (in_array($membercount['code4'], array("6"))) {
                $orgname = "โรงพยาบาลทั่วไป"; //"รพท.";
            } else if (in_array($membercount['code4'], array("7"))) {
                $orgname = "โรงพยาบาลชุมชน หรือโรงพยาบาลสมเด็จพระยุพราช"; //"รพช. รพร.";
            } else if (in_array($membercount['code4'], array("18", "3", "4", "8", "13", "17"))) {
                $orgname = "โรงพยาบาลส่งเสริมสุขภาพประจำตำบล"; //รพ.สต. สถานีอนามัย";
            } else if (in_array($membercount['code4'], array("11", "12"))) {
                $orgname = "โรงพยาบาล นอก สธ.";
            } else if (in_array($membercount['code4'], array("15", "16"))) {
                $orgname = "โรงพยาบาลเอกชน และคลินิกเอกชน";
            } else {
                $orgname = "อื่นๆ";
            }

            $count[$orgname]['All'] += $membercount['nall'];
            $count[$orgname]['Year'] += $membercount['thisYear'];
            $count[$orgname]['Month'] += $membercount['thisMonth'];
            $count[$orgname]['Week'] += $membercount['thisWeek'];
            $count[$orgname]['Day'] += $membercount['today'];
        }

        foreach ($count as $orgname => $data) {
            $countAll = $data['All'];
            $countYear = $data['Year'];
            $countMonth = $data['Month'];
            $countYearWeekRegis = $data['Week'];
            $countDay = $data['Day'];

            $sumAll += $countAll;
            $sumYear += $countYear;
            $sumMonth += $countMonth;
            $sumYearWeekRegis += $countYearWeekRegis;
            $sumDay += $countDay;


            array_push($dataAll, [
                'name', [$orgname, '', '', number_format($countAll), number_format($countYear), number_format($countMonth), number_format($countYearWeekRegis), number_format($countDay)]
            ]);
        }
        array_push($dataAll, [
            'name', ['รวมทั้งหมด', '', '', number_format($sumAll), number_format($sumYear), number_format($sumMonth), number_format($sumYearWeekRegis), number_format($sumDay)]
                ]
        );
        array_push($dataAll, [
            'section', ['จำนวนสมาชิก อาสาสมัคร (คน)', '', '', 'ทั้งหมด', 'ปีนี้', 'เดือนนี้', 'สัปดาห์นี้', 'วันนี้']
                ]
        );
        $sqlVolMember = "select province,COUNT(*) as nall,SUM(YEAR(FROM_UNIXTIME(created_at))=YEAR(CURDATE())) as thisYear,SUM(YEAR(FROM_UNIXTIME(created_at))=YEAR(CURDATE()) AND MONTH(FROM_UNIXTIME(created_at))=MONTH(CURDATE())) as thisMonth,SUM(YEARWEEK(FROM_UNIXTIME(`created_at`)) = YEARWEEK(NOW())) as thisWeek,SUM(DATE(FROM_UNIXTIME(created_at))=CURDATE()) as today  from user u inner join user_profile p ON u.id=p.user_id inner join all_hospital_thai h on p.sitecode=h.hcode where h.code2 not like '%demo%' and status_del=0 and volunteer_status<>0 group by province order by province;";
        $qryVolMember = Yii::$app->db->createCommand($sqlVolMember)->queryAll();

        foreach ($qryVolMember as $membervcount) {
            $orgname = $membervcount['province'];
            $countv[$orgname]['All'] += $membervcount['nall'];
            $countv[$orgname]['Year'] += $membervcount['thisYear'];
            $countv[$orgname]['Month'] += $membervcount['thisMonth'];
            $countv[$orgname]['Week'] += $membervcount['thisWeek'];
            $countv[$orgname]['Day'] += $membervcount['today'];
        }

        foreach ($countv as $orgname => $data) {
            $countvAll = $data['All'];
            $countvYear = $data['Year'];
            $countvMonth = $data['Month'];
            $countvYearWeekRegis = $data['Week'];
            $countvDay = $data['Day'];

            $sumvAll += $countvAll;
            $sumvYear += $countvYear;
            $sumvMonth += $countvMonth;
            $sumvYearWeekRegis += $countvYearWeekRegis;
            $sumvDay += $countvDay;


            array_push($dataAll, [
                'name', [$orgname, '', '', number_format($countvAll), number_format($countvYear), number_format($countvMonth), number_format($countvYearWeekRegis), number_format($countvDay)]
            ]);
        }

        array_push($dataAll, [
            'name', ['รวมทั้งหมด', '', '', number_format($sumvAll), number_format($sumvYear), number_format($sumvMonth), number_format($sumvYearWeekRegis), number_format($sumvDay)]
                ]
        );

        $sql = "SELECT count(*) as nall from person";
        $sql = "SHOW TABLE STATUS LIKE 'person'";
        $data = Yii::$app->dbbot->createCommand($sql)->queryOne();
        $dataAllP['TCC'] = $data['Rows'];
        $sql = "SELECT count(*) as nall from person";
        $sql = "SHOW TABLE STATUS LIKE 'person'";
        $data = Yii::$app->dbnemo->createCommand($sql)->queryOne();
        $dataAllP['NEMO'] = $data['Rows'];

        Yii::$app->response->format = \yii\web\Response::FORMAT_HTML;

        //TdcCountAll
        $query = \common\lib\nut\Lib::ArrayToObject(\frontend\modules\hismonitor\controllers\ReportController::getSumTdcCount("sum(item) as sum "));
        $sumtdccount = 0;
        foreach ($query as $q) {
            $sumtdccount += $q->sum;
        }


        return $this->renderAjax('/report-cocs/_overviewTab', [
                    'dataAll' => $dataAll,
                    'dataAllP' => $dataAllP,
                    'sumTdcCount' => $sumtdccount
        ]);
    }

    public function actionShowModules() {
        return $this->renderAjax('/report-cocs/_modulesTab');
    }

    public function actionShowModuleReport() {
        $request = \Yii::$app->request;
        $module_id = $request->post('module_id');
        $sqlMain = " SELECT main_ezf_table FROM inv_gen WHERE gid=:gid ";
        $resMain = \Yii::$app->db->createCommand($sqlMain, [':gid' => $module_id])->queryOne();
        $sqlDate = " SELECT MIN(DATE(create_date)) as begin_date FROM " . $resMain['main_ezf_table'] . " ";
        $resDate = \Yii::$app->db->createCommand($sqlDate)->queryOne();

        return $this->renderAjax('/report-cocs/_module-report-tab', ['module_id' => $module_id, 'begin_date' => $resDate['begin_date']]);
    }

    public function actionModulesReport() {
        $dataProviderArray = ModulesReport::getReportOfModules();
        $render_page = '/report-cocs/_modules_part1';

        return $this->renderAjax($render_page, $dataProviderArray);
    }

    public function actionFormsReport() {
        $request = \Yii::$app->request;
        $module_id = $request->post('module_id');
        $sitecode = $request->post('sitecode');
        $startDate = $request->post('startdate');
        $endDate = $request->post('enddate');

        $module_name = $request->get('module_name');

        $sqlMain = " SELECT main_ezf_table FROM inv_gen WHERE gid=:gid ";
        $resMain = \Yii::$app->db->createCommand($sqlMain, [':gid' => $module_id])->queryOne();
        $whereSite = "";
        if ($sitecode != null) {
            $wheresite = " AND xsourcex='$sitecode' ";
        }
        $sqlDate = " SELECT MIN(DATE(create_date)) as begin_date FROM " . $resMain['main_ezf_table'] . " WHERE 1 $wheresite ";
        $resDate = \Yii::$app->db->createCommand($sqlDate)->queryOne();

        $dataQuery = ModulesReport::getReportOfForms($module_id, $sitecode, $startDate, $endDate);
        $dataForms = new \yii\data\ArrayDataProvider([
            'allModels' => $dataQuery,
            'pagination' => [
                'pageSize' => 50,
            ],
        ]);
        
        $begin_date = $resDate['begin_date'];
        if ($sitecode == null) {
            $begin_date = $startDate;
        }
        $siteName = "";
        if ($sitecode != null) {
            $siteName = ModulesReport::getSiteName($sitecode);
        }
        $render_page = '/report-cocs/_form-report';
        return $this->renderAjax($render_page, [
                    'dataForms' => $dataForms, 'mod_name' => $module_name, 'sitename' => $siteName,
                    'startDate' => $startDate,
                    'endDate' => $endDate,
                    'begin_date'=>$begin_date
        ]);
    }

    public function actionSitesReport() {
        $request = \Yii::$app->request;
        $module_id = $request->get('module_id');
        $sitecode = $request->get('sitecode');
        $module_name = $request->get('module_name');
        $dataQuery = ModulesReport::getReportOfSites($module_id);
        $dataSites = new \yii\data\ArrayDataProvider([
            'allModels' => $dataQuery,
            'pagination' => [
                'pageSize' => 50,
            ],
        ]);
        $siteName = "";
        if ($sitecode != null) {
            $siteName = ModulesReport::getSiteName($sitecode);
        }
        $render_page = '/report-cocs/_site-report';
        return $this->renderAjax($render_page, ['dataSites' => $dataSites, 'mod_name' => $module_name, 'sitename' => $siteName]);
    }

    public function actionCumulativeGraph() {
        $request = \Yii::$app->request;
        $module_id = $request->post('module_id');
        $sitecode = $request->post('sitecode');
        $startDate = $request->post('startdate');
        $endDate = $request->post('enddate');

        $siteName = "";
        //$dataQuery = ModulesReport::getCumulativeGraph($module_id, $sitecode, $startDate, $endDate);
        //exit();
        $sqlInv = " SELECT upr.sitecode,main_ezf_table, ezf_table, enable_form FROM inv_gen ig INNER JOIN inv_favorite ifa ON ig.gid=ifa.gid
                INNER JOIN user_profile upr ON ifa.user_id=upr.user_id WHERE ig.gid=:gid GROUP BY upr.sitecode";
        $resInv = \Yii::$app->db->createCommand($sqlInv, [':gid' => $module_id])->queryOne();
        //\appxq\sdii\utils\VarDumper::dump($resInv);
        $form_data = \appxq\sdii\utils\SDUtility::string2Array($resInv['enable_form']);

        $form_lst = $form_data['form'];
        $whereDate = "";
        if ($startDate != null || $endDate != null) {
            $whereDate = " AND create_date BETWEEN '$startDate' AND '$endDate'";
        }
        if ($sitecode != null) {
            $whereDate .= " AND sitecode ='$sitecode' ";
            $siteName = ModulesReport::getSiteName($sitecode);
        }
        $sqlPatient = " SELECT count(DISTINCT ptid) as pcount, count(*) as ccount ,DAYOFMONTH(create_date) as 'day',MONTH(create_date) as 'month',YEAR(create_date) as 'year'
                FROM " . $resInv['main_ezf_table'] . " WHERE YEAR(create_date) > '2013' $whereDate
                GROUP by DATE(create_date) ORDER BY DATE(create_date) ASC;
            ";
        $resPatient = \Yii::$app->db->createCommand($sqlPatient)->queryAll();
        $x = 0;
        $sumPatient = 0;
        $sumCare = '';
        $beginDay = '';
        $beginMonth = '';
        $beginYear = '';
        foreach ($resPatient as $val) {
            if ($beginDay == null) {
                $beginDay = $val['day'];
            }
            if ($beginMonth == null) {
                $beginMonth = $val['month'];
            }
            if ($beginYear == null) {
                $beginYear = $val['year'];
            }
            if ($x < count($resPatient) - 1) {
                $sumPatient += $val['pcount'];
                $sumCare += $val['ccount'];
                $day = $day . $val['day'] . ',';
                $dataPatient = $dataPatient . $sumPatient . ',';
                $dataCare = $dataCare . $sumCare . ',';
            } else {
                $sumPatient += $val['pcount'];
                $sumCare += $val['ccount'];
                $day = $day . "วันที่ " . $val['day'];
                $dataPatient = $dataPatient . $sumPatient;
                $dataCare = $dataCare . $sumCare;
            }
            $x++;
        }

        return $this->renderAjax('/report-cocs/_cumulative-graph', [
                    'module_id' => $module_id,
                    'sitename' => $siteName,
                    'dataSeries' => $day,
                    'dataPatient' => $dataPatient,
                    'dataCare' => $dataCare,
                    'day' => $beginDay,
                    'month' => $beginMonth,
                    'year' => $beginYear,
                    'startDate' => $startDate,
                    'endDate' => $endDate
        ]);
    }

    public function actionModulesPart3() {

        return $this->renderAjax('/report-cocs/_modules_part3');
    }

    public function actionGetSite($q = null, $id = null) {
        $sqlSite = " SELECT `name`, hcode FROM all_hospital_thai WHERE CONCAT(`hcode`, ' ', `name`) LIKE '%" . $q . "%' AND `hcode` NOT RLIKE '^[a-zA-Z]' LIMIT 0,100";
        $resSite = \Yii::$app->db->createCommand($sqlSite)->queryAll();
        $out = [];
        $i = 0;
        foreach ($resSite as $value) {
            $out["results"][$i] = ['id' => $value['hcode'], 'text' => $value["hcode"] . " " . $value["name"]];
            $i++;
        }

        return json_encode($out);
    }

    public function actionTimeline() {
        ini_set("memory_limit", "256M");
        $dataPost = \Yii::$app->request->post();
        $page = $dataPost['page'];
        if (isset($page)) {
            $page = $page;
        } else {
            $page = 1;
        }
        $perPage = 50;

        $pageStart = ($page - 1) * $perPage;

        if (!empty($dataPost['module_id'])) { //ไม่เท่าค่าว่าง
            $result_timeline = TimelineReport::getTimeline($dataPost);
            $module_id = $dataPost['module_id'];
        }
       
//        $date = [];
//        foreach ($result_timeline as $value) {
//            $date[$value['date']]  = $value['date'];
//        }
//        krsort($date);
//        return $this->renderAjax('/report-cocs/_timeline_report',[
//            'result_timeline' => $result_timeline,
//            'date' => $date]);

        $data['timeline'] = $this->renderAjax('/report-cocs/_timeline_report', [
            'result_timeline' => array_slice($result_timeline, $pageStart, $perPage),
            'module_id' => $module_id,
//            'date' => $date
        ]);
        $totalPage = ceil(count($result_timeline) / 50);
//         \appxq\sdii\utils\VarDumper::dump($result_timeline);
        $html = "<ul class='pagination'>";

        for ($i = 1; $i <= $totalPage; $i++) {
            if ($page == $i) {
                $html .= "<li class='active'><a href='javascript:void(0)' onclick='pageSet({$i});' '>" . $i . "</a></li>";
            } else {
                $html .= "<li><a href='javascript:void(0)'  onclick='pageSet({$i});' >" . $i . "</a></li>";
            }
        }

        $html .= "</ul>";
        $data['page'] = $html;
        return json_encode($data);
    }

}
