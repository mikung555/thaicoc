<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "puser".
 *
 * @property integer $pid
 * @property string $username
 * @property string $passwords
 * @property string $title
 * @property string $name
 * @property string $surname
 * @property string $upriv
 * @property string $priv29txt
 * @property string $Cid
 * @property string $InvestigatorName
 * @property string $Affiliation
 * @property string $InvestigatorPhone
 * @property string $InvestigatorEmail
 * @property string $Registering
 * @property string $TypeofOrg
 * @property string $Country
 * @property string $HospitalName
 * @property string $OrganizationName
 * @property string $OrganizationAddress
 * @property string $OrganizationAcronyms
 * @property string $OrganizationParent
 * @property string $OrgProvCode
 * @property string $OrgProvince
 * @property string $OfficialRepresentative
 * @property string $Phone
 * @property string $Email
 * @property string $OrganizationURL
 * @property string $FundingOrganization
 * @property string $RegulatoryAuthority
 * @property string $RegulatoryAuthorityAddress
 * @property string $dadd
 * @property string $dupdate
 * @property string $lastsessionreg
 * @property string $stdid
 * @property string $stdprogram
 * @property string $stduniversity
 * @property string $hcode
 * @property string $tamboncode
 * @property string $tambon
 * @property string $amphurcode
 * @property string $amphur
 * @property string $provincecode
 * @property string $province
 * @property string $postcode
 * @property string $pid_link
 */
class Puser extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'puser';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['OrganizationAcronyms', 'OrganizationParent', 'RegulatoryAuthorityAddress'], 'string'],
            [['cid'],'required','message'=>'กรุณาระบุเลขบัตรประจำตัวประชาชน'],
            [['cid'],'number','message'=>'กรุณาระบตัวเลข 13 หลักขึ้นไป'],
//            ['cid', 'checkCid'],
            [['name','surname','Email','Phone',],'required','message'=>'กรุณาระบุ'],
            [['dadd', 'dupdate','cid'], 'safe'],
            [['pid_link'], 'integer'],
            [['username', 'priv29txt', 'FundingOrganization', 'stdprogram', 'stduniversity'], 'string', 'max' => 250],
            [['passwords'], 'string', 'max' => 30],
            [['title', 'upriv', 'InvestigatorName', 'Affiliation', 'InvestigatorPhone', 'InvestigatorEmail', 'Country', 'OrganizationName', 'OrganizationAddress', 'OrgProvince', 'OfficialRepresentative', 'Phone', 'Email', 'RegulatoryAuthority'], 'string', 'max' => 100],
            [[ 'HospitalName', 'OrganizationURL', 'tambon', 'amphur', 'province'], 'string', 'max' => 200],
            [['stdid'], 'string', 'max' => 20],
            [['Registering', 'TypeofOrg', 'OrgProvCode', 'provincecode'], 'string', 'max' => 2],
            [['lastsessionreg'], 'string', 'max' => 40],
            [['hcode'], 'string', 'max' => 5],
            [['tamboncode'], 'string', 'max' => 6],
            [['amphurcode'], 'string', 'max' => 4],
            [['postcode'], 'string', 'max' => 10]
        ];
    }
    public function checkFormatCID($pid) {
        if(strlen($pid) != 13) return false;
        for($i=0, $sum=0; $i<12;$i++)
            $sum += (int)($pid{$i})*(13-$i);
        if((11-($sum%11))%10 == (int)($pid{12}))
            return true;
        return false;
    }
    public function checkCid($attribute,$params){
        $result =  $this->checkFormatCID($this->cid);
        if($result != 1){
            $this->addError($attribute,'เลขบัตรประชาชนไม่ถูกต้อง');
        }
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pid' => 'Pid',
            'username' => 'Username',
            'passwords' => 'Passwords',
            'title' => 'Title',
            'name' => 'Name',
            'surname' => 'Surname',
            'upriv' => 'Upriv',
            'priv29txt' => 'Priv29txt',
            'cid' => 'Cid',
            'InvestigatorName' => 'Investigator Name',
            'Affiliation' => 'Affiliation',
            'InvestigatorPhone' => 'Investigator Phone',
            'InvestigatorEmail' => 'Investigator Email',
            'Registering' => 'Registering',
            'TypeofOrg' => 'Typeof Org',
            'Country' => 'Country',
            'HospitalName' => 'Hospital Name',
            'OrganizationName' => 'Organization Name',
            'OrganizationAddress' => 'Organization Address',
            'OrganizationAcronyms' => 'Organization Acronyms',
            'OrganizationParent' => 'Organization Parent',
            'OrgProvCode' => 'Org Prov Code',
            'OrgProvince' => 'Org Province',
            'OfficialRepresentative' => 'Official Representative',
            'Phone' => 'Phone',
            'Email' => 'Email',
            'OrganizationURL' => 'Organization Url',
            'FundingOrganization' => 'Funding Organization',
            'RegulatoryAuthority' => 'Regulatory Authority',
            'RegulatoryAuthorityAddress' => 'Regulatory Authority Address',
            'dadd' => 'Dadd',
            'dupdate' => 'Dupdate',
            'lastsessionreg' => 'Lastsessionreg',
            'stdid' => 'Stdid',
            'stdprogram' => 'Stdprogram',
            'stduniversity' => 'Stduniversity',
            'hcode' => 'Hcode',
            'tamboncode' => 'Tamboncode',
            'tambon' => 'Tambon',
            'amphurcode' => 'Amphurcode',
            'amphur' => 'Amphur',
            'provincecode' => 'Provincecode',
            'province' => 'Province',
            'postcode' => 'Postcode',
            'pid_link' => 'Pid Link',
        ];
    }
}
