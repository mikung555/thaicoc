<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "user_line".
 *
 * @property string $mid
 * @property integer $userid
 * @property string $dadd
 */
class UserLine extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_line';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['mid'], 'required'],
            [['userid'], 'integer'],
            [['dadd'], 'safe'],
            [['mid'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'mid' => 'Mid',
            'userid' => 'Userid',
            'dadd' => 'Dadd',
        ];
    }
}
