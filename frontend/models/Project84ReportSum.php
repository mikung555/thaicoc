<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "project84_report_sum".
 *
 * @property integer $report_id
 * @property string $zone
 * @property string $provincecode
 * @property string $amphurcode
 * @property string $tamboncode
 * @property string $address
 * @property string $province
 * @property string $amphur
 * @property string $tambon
 * @property integer $nall
 * @property integer $ov
 * @property integer $ovpos
 * @property integer $ov02
 * @property integer $ov03
 * @property integer $cca01
 * @property integer $cca02
 * @property integer $abnormal
 * @property integer $suspected
 * @property integer $ctmri
 * @property integer $cca
 * @property integer $treatment
 * @property string $hsitecode
 * @property integer $dead
 */
class Project84ReportSum extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'project84_report_sum';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['report_id', 'address'], 'required'],
            [['report_id', 'nall', 'ov', 'ovpos', 'ov02', 'ov03', 'cca01', 'cca02', 'abnormal', 'suspected', 'ctmri', 'treatment', 'dead'], 'integer'],
            [['zone', 'provincecode', 'amphurcode', 'tamboncode'], 'string', 'max' => 5],
            [['address'], 'string', 'max' => 10],
            [['province', 'amphur', 'tambon'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'report_id' => 'Report ID',
            'zone' => 'เขต',
            'provincecode' => 'Provincecode',
            'amphurcode' => 'Amphurcode',
            'tamboncode' => 'Tamboncode',
            'address' => 'Address',
            'province' => 'จังหวัด',
            'amphur' => 'อำเภอ',
            'tambon' => 'ตำบล',
            'nall' => 'ลงทะเบียน',
            'ov' => 'ตรวจ OV',
            'ovpos' => 'ติดเชื้อ OV',
            'ov02' => 'ให้สุขศึกษา',
            'ov03' => 'ให้การรักษา',
            'cca01' => 'คัดกรอง',
            'cca02' => 'อัลตราซาวด์',
            'abnormal' => 'ผิดปกติ',
            'suspected' => 'สงสัยมะเร็ง',
            'ctmri' => 'CT/MRI',
            'cca' => 'พบเป็นมะเร็ง',
            'treatment' => 'Treatment',
            'hsitecode' => 'Hsitecode',
            'dead' => 'ตาย',
        ];
    }
}
