<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "project84_sum".
 *
 * @property string $zone
 * @property string $province
 * @property string $amphur
 * @property string $tambon
 * @property string $address
 * @property integer $nall
 * @property integer $ov
 * @property integer $ovpos
 * @property integer $cca02
 * @property integer $abnormal
 * @property integer $suspected
 * @property integer $ctmri
 * @property integer $treatment
 * @property integer $dead
 */
class Project84Sum extends \yii\db\ActiveRecord
{
    public $year15;
    public $month15;
    public $year16;
    public $month16;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'project84_sum';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['zone', 'province', 'amphur', 'tambon', 'address', 'nall', 'ov', 'ovpos'], 'required'],
            [['nall', 'ov', 'ovpos', 'cca02', 'abnormal', 'suspected', 'ctmri', 'treatment', 'dead'], 'integer'],
            [['zone'], 'string', 'max' => 5],
	    [['year15', 'month15', 'year16', 'month16'], 'safe'],
            [['province', 'amphur', 'tambon'], 'string', 'max' => 100],
            [['address'], 'string', 'max' => 10]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'zone' => 'เขต',
            'province' => 'จังหวัด',
            'amphur' => 'อำเภอ',
            'tambon' => 'ตำบล',
            'address' => 'Address',
            'nall' => 'ลงทะเบียน',
            'ov' => 'ตรวจ OV',
            'ovpos' => 'จำนวนผู้ติดเชื้อ OV',
            'cca02' => 'อัลตร้าซาวด์',
            'abnormal' => 'อัลตร้าซาวด์ผิดปกติ',
            'suspected' => 'Suspected CCA',
            'ctmri' => 'CT/MRI',
            'treatment' => 'ผ่าตัด',
            'dead' => 'ตาย',
        ];
    }

    /**
     * @inheritdoc
     * @return Project84SumQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new Project84SumQuery(get_called_class());
    }
}
