<?php

namespace frontend\models;

/**
 * This is the ActiveQuery class for [[Project84Sum]].
 *
 * @see Project84Sum
 */
class Project84SumQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return Project84Sum[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Project84Sum|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}