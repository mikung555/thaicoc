<?php

namespace frontend\models;
 
use Yii;
 
class TblTree extends \kartik\tree\models\Tree
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_tree';
    }
    public function rules() {
        return [
            ['userid','default','value'=>Yii::$app->user->id],
        ];
    }
    public function beforeSave()
    {
        if (parent::beforeSave()) {
            $this->userid=Yii::$app->user->id;
            \yii\helpers\VarDumper::dump($this);exit;
            return true;
        } else {
            return false;
        }
    }    
    /**
     * Override isDisabled method if you need as shown in the  
     * example below. You can override similarly other methods
     * like isActive, isMovable etc.
     */
}

?>