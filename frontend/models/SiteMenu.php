<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "site_menu".
 *
 * @property string $code
 * @property integer $id
 * @property integer $parentid
 * @property string $name
 * @property string $content
 * @property integer $active
 * @property integer $order
 */
class SiteMenu extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'site_menu';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [

            [['code', 'name'], 'required'],
            [['id', 'active', 'order'], 'integer'],
            [['parentid'],'safe'],
            [['content'], 'string'],
            [['code'], 'string', 'max' => 5],
            [['name'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'code' => Yii::t('app', 'Code'),
            'id' => Yii::t('app', 'ID'),
            'parentid' => Yii::t('app', 'เป็น Sub Menu ของ'),
            'name' => Yii::t('app', 'ชื่อเมนู'),
            'content' => Yii::t('app', 'เนื้อหา'),
            'active' => Yii::t('app', 'Active'),
            'order' => Yii::t('app', 'เรียงไว้ถัดจาก'),
        ];
    }
}
