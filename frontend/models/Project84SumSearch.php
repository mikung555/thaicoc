<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\Project84Sum;

/**
 * Project84SumSearch represents the model behind the search form about `frontend\models\Project84Sum`.
 */
class Project84SumSearch extends Project84Sum
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['zone', 'province', 'amphur', 'tambon', 'address'], 'safe'],
            //[['nall', 'ov', 'ovpos', 'cca02', 'abnormal', 'suspected', 'ctmri', 'treatment', 'dead'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Project84Sum::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'nall' => $this->nall,
            'ov' => $this->ov,
            'ovpos' => $this->ovpos,
            'cca02' => $this->cca02,
            'abnormal' => $this->abnormal,
            'suspected' => $this->suspected,
            'ctmri' => $this->ctmri,
            'treatment' => $this->treatment,
            'dead' => $this->dead,
        ]);

        $query->andFilterWhere(['like', 'zone', $this->zone])
            ->andFilterWhere(['like', 'province', $this->province])
            ->andFilterWhere(['like', 'amphur', $this->amphur])
            ->andFilterWhere(['like', 'tambon', $this->tambon])
            ->andFilterWhere(['like', 'address', $this->address]);

        return $dataProvider;
    }
}
