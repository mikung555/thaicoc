<?php
$config = [
    'timeZone' => 'Asia/Bangkok',
    'homeUrl'=>Yii::getAlias('@frontendUrl'),
    'controllerNamespace' => 'frontend\controllers',
    'defaultRoute' => 'site/index',
    'modules' => [
        'reportcoc' => [
            'class' => 'frontend\modules\reportcoc\Module',
        ],
        //Nut
        'hismonitor' => [

            'class' => 'frontend\modules\hismonitor\Module',

        ],
        'webservice' => [
            'class' => 'frontend\modules\webservice\Module',
        ],        
        'reportpdf' => [
            'class' => 'frontend\modules\reportpdf\Module',
        ],         
        'purify' => [
            'class' => 'frontend\modules\purify\Module',
        ],          
        'cpay' => [
            'class' => 'app\modules\cpay\Module',
        ],        
        'social' => [
            // the module class
            'class' => 'kartik\social\Module',

            // the global settings for the Disqus widget
            'disqus' => [
                'settings' => ['shortname' => 'DISQUS_SHORTNAME'] // default settings
            ],

            // the global settings for the Facebook plugins widget
            'facebook' => [
                'appId' => '343194372680640',
                'secret' => '580c3bb40e27c5412ae04957346b4ab2',
            ],

            // the global settings for the Google+ Plugins widget
            'google' => [
                'clientId' => 'GOOGLE_API_CLIENT_ID',
                'pageId' => 'GOOGLE_PLUS_PAGE_ID',
                'profileId' => 'GOOGLE_PLUS_PROFILE_ID',
            ],

            // the global settings for the Google Analytics plugin widget
            'googleAnalytics' => [
                'id' => 'TRACKING_ID',
                'domain' => 'TRACKING_DOMAIN',
            ],

            // the global settings for the Twitter plugin widget
            'twitter' => [
                'screenName' => 'TWITTER_SCREEN_NAME'
            ],

            // the global settings for the GitHub plugin widget
            'github' => [
                'settings' => ['user' => 'GITHUB_USER', 'repo' => 'GITHUB_REPO']
            ],
        ],
        'project84' => [
    'class' => 'frontend\modules\project84\Module',
        ],
        'user' => [
            'class' => 'frontend\modules\user\Module'
        ],
		'article' => [
            'class' => 'app\modules\article\Module',
        ],
        'api' => [
            'class' => 'frontend\modules\api\Module',
            'modules' => [
                'v1' => 'frontend\modules\api\v1\Module'
            ]
        ],
        'dynagrid'=> [
             'class'=>'\kartik\dynagrid\Module',
             // other module settings
            'defaultPageSize'=>100,
         ],
         'gridview'=> [
             'class'=>'\kartik\grid\Module',
             // other module settings
         ],
        'treemanager' =>  [
             'class' => '\kartik\tree\Module',

         ],
	'volunteer' => [
            'class' => 'frontend\modules\volunteer\Module',
        ],
        'ezforms' => [
            'class' => 'frontend\modules\ezforms\Module',
        ],
          'emobile' => [
            'class' => 'frontend\modules\emobile\Module',
        ],
         'cidapi' => [
            'class' => 'app\modules\cidapi\Module',
        ],
    ],
    'components' => [
        'authClientCollection' => [
            'class' => 'yii\authclient\Collection',
            'clients' => [
                'github' => [
                    'class' => 'yii\authclient\clients\GitHub',
                    'clientId' => getenv('GITHUB_CLIENT_ID'),
                    'clientSecret' => getenv('GITHUB_CLIENT_SECRET')
                ]
            ]
        ],
        'errorHandler' => [
            'errorAction' => 'site/error'
        ],
        'request' => [
            'cookieValidationKey' => getenv('FRONTEND_COOKIE_VALIDATION_KEY')
        ],
        'user' => [
            'class'=>'yii\web\User',
            'identityClass' => 'common\models\User',
            'loginUrl'=>['/user/sign-in/login'],
            'enableAutoLogin' => true,
            'authTimeout' => 86400, //Seconds **
            'as afterLogin' => 'common\behaviors\LoginTimestampBehavior'
        ]
    ],
    
];

if (YII_ENV_DEV) {
    $config['modules']['gii'] = [
        'class'=>'yii\gii\Module',
        'generators' => [
            'crud' => [
                'class'=>'yii\gii\generators\crud\Generator',
                'templates'=>[
                    'yii2-starter-kit' => Yii::getAlias('@backend/views/_gii/templates'),
		    'mycrud' => '@common/templates/mycrud'
                ],
                'messageCategory' => 'backend'
            ]
        ]
    ];
}

if (YII_ENV_PROD) {
    // Maintenance mode
    $config['bootstrap'] = ['maintenance'];
    $config['components']['maintenance'] = [
        'class' => 'common\components\maintenance\Maintenance',
        'enabled' => function ($app) {
            return $app->keyStorage->get('frontend.maintenance') === 'enabled';
        }
    ];
}

return $config;
