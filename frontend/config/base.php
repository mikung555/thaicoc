<?php
return [
    'id' => 'frontend',
    'basePath'=>dirname(__DIR__),
    'bootstrap' => ['frontend\components\AppComponent'],
    'components' => [
        'urlManager'=>require(__DIR__.'/_urlManager.php'),
    ],
];
