<?php

namespace frontend\components;

use Yii;
use yii\base\Component;

class AppComponent extends Component {

    public function init() {
	parent::init();

	self::currentUsername();
	self::siteURL();
    }

    public static function siteURL() {
	$domain = Yii::$app->keyStorage->get('frontend.domain');
	$siteprefix = trim(str_replace($domain, "", $_SERVER['HTTP_HOST']));
	$siteprefix = trim(str_replace(".", "", $siteprefix));
	$sitecode = $siteprefix + 0;
	if ($sitecode == 0)
	    $sitecode = "";
	$sqlSiteURL = "SELECT code,`url`,`name` FROM site_url WHERE code='" . ($sitecode) . "' or url='" . $siteprefix . "'";
	$dataSiteURL = Yii::$app->db->createCommand($sqlSiteURL)->queryOne();
	
	if ($siteprefix != "")
	    if ($dataSiteURL['code'] == "") {
		$sqlHospital = "SELECT `code`,`name` FROM const_hospital WHERE code+0='" . ($siteprefix + 0) . "' ";
		$dataHospital = Yii::$app->db->createCommand($sqlHospital)->queryOne();
		if ($dataHospital['code'] != "")
		    if ($sitecode != "") {
			$SiteURL = new \backend\models\SiteUrl;
			$SiteURL->code = $dataHospital['code'] + 0;
			$SiteURL->name = $dataHospital['name'];
			$SiteURL->url = str_pad($dataHospital['code'], 5, "0", STR_PAD_LEFT);
			$SiteURL->save();
			$SQLquery = Yii::$app->db->createCommand("REPLACE INTO site_menu (SELECT " . $SiteURL->code . ",a.* from site_menu_init as a)")->query();
			echo "<META http-equiv=\"refresh\" content=\"0;URL=/\">";
			exit;
		    } else {

			//$this->redirect(Yii::getAlias('@frontendUrl'));
			echo "<META http-equiv=\"refresh\" content=\"0;URL=" . Yii::getAlias('@frontendUrl') . "\">";
			exit;
		    }
	    }
    }

    public static function currentUsername() {

	$currentUsername = Yii::$app->user->identity->userProfile->user->username;
	//echo $currentUsername," ",$_COOKIE['CloudUserID'];exit;
	if (($_COOKIE['CloudUserID'] != "") && ($_COOKIE['CloudUserID'] != $currentUsername)) {
	    $LoginForm = new \backend\models\LoginForm();
	    Yii::$app->user->login($LoginForm->searchUser($_COOKIE['CloudUserID']), 0);
	    //return Yii::$app->getResponse()->redirect(Yii::$app->getHomeUrl());
	} else {
	    if ($currentUsername != "")
		if ($_COOKIE['CloudUserID'] == "") {
		    Yii::$app->user->logout();
		    setcookie("CloudUserID", "", time() - 1, "/", Yii::$app->keyStorage->get('frontend.domain'), false);
		    return Yii::$app->getResponse()->redirect(Yii::$app->getHomeUrl());
		    //echo "<META http-equiv=\"refresh\" content=\"0;URL=".Yii::getAlias('@frontendUrl')."\">";
		    exit;
		}
	}
    }

}
