<?php

namespace common\components\category;

use yii\base\Widget;
use yii\helpers\Html;

class CategoryWidget extends Widget
{
    public $categoryType;

    public function init()
    {
        parent::init();
        if ($this->categoryType === null) {
            $this->categoryType = 'full';
        }
    }

    public function run()
    {
        $script =
<<< JS
    var defaultData = [
          {
            text: 'Parent 1',
            href: '#parent1',
            tags: ['4'],
            nodes: [
              {
                text: 'Child 1',
                href: '#child1',
                tags: ['2'],
                nodes: [
                  {
                    text: 'Grandchild 1',
                    href: '#grandchild1',
                    tags: ['0']
                  },
                  {
                    text: 'Grandchild 2',
                    href: '#grandchild2',
                    tags: ['0']
                  }
                ]
              },
              {
                text: 'Child 2',
                href: '#child2',
                tags: ['0']
              }
            ]
          },
          {
            text: 'Parent 2',
            href: '#parent2',
            tags: ['0']
          },
          {
            text: 'Parent 3',
            href: '#parent3',
             tags: ['0']
          },
          {
            text: 'Parent 4',
            href: '#parent4',
            tags: ['0']
          },
          {
            text: 'Parent 5',
            href: '#parent5'  ,
            tags: ['0']
          }
        ];
    $('.js-treeview1').treeview({
    data: defaultData
    });
JS;

        return $script;
    }
}
