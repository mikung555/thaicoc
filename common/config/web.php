<?php
$config = [
    'timeZone' => 'Asia/Bangkok',
    'aliases' => [
        '@lib' => '@common/lib',
	'@appxq/sdii' => '@common/lib/yii2-sdii',
	'@appxq/calendar' => '@common/lib/calendar',
    ],    
	'modules' => [
        'admin' => [
            'class' => 'mdm\admin\Module',
			'layout' => 'left-menu', // defaults to null, using the application's layou
			'mainLayout' => '@app/views/layouts/main.php',
			'controllerMap' => [
                 'assignment' => [
                    'class' => 'mdm\admin\controllers\AssignmentController',
                    'userClassName' => 'common\models\User', // fully qualified class name of your User model
                    // Usually you don't need to specify it explicitly, since the module will detect it automatically
                    //'idField' => 'user_id',        // id field of your User model that corresponds to Yii::$app->user->id
                    //'usernameField' => 'username', // username field of your User model
                    'searchClass' => 'backend\models\search\UserSearch'    // fully qualified class name of your User model for searching
                ]
            ],
        ]
    ],
    'components' => [
        'assetManager' => [
            'class' => 'yii\web\AssetManager',
            'linkAssets' => true,
            'appendTimestamp' => YII_ENV_DEV,
	    
        ],
		'authManager' => [
			'class' => 'yii\rbac\DbManager',
			//'defaultRoles' => ['guest', 'user'],
			'cache' => 'yii\caching\FileCache'
		],
    ],
//	'as access' => [
//		'class' => 'mdm\admin\components\AccessControl',
//		'allowActions' => [
//			'site/*',
//			'admin/*',
//		// The actions listed here will be allowed to everyone including guests.
//		// So, 'admin/*' should not appear here in the production, of course.
//		// But in the earlier stages of your development, you may probably want to
//		// add a lot of actions here until you finally completed setting up rbac,
//		// otherwise you may not even take a first step.
//		]
//	],
    'as locale' => [
        'class' => 'common\behaviors\LocaleBehavior',
        'enablePreferredLanguage' => true
    ]
];

if (YII_DEBUG) {
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        'allowedIPs' => ['127.0.0.1', '::1', '192.168.33.1'],
    ];
}

if (YII_ENV_DEV) {
    $config['modules']['gii'] = [
        'allowedIPs' => ['127.0.0.1', '::1', '192.168.33.1'],
    ];
}


return $config;
