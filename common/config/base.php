<?php
$config = [
    'name'=>'Faculty Cloud',
    'vendorPath'=>dirname(dirname(__DIR__)).'/vendor',
    'extensions' => require(__DIR__ . '/../../vendor/yiisoft/extensions.php'),
    'sourceLanguage'=>'en-US',
    'language'=>'th',
    'bootstrap' => ['log'],
    'components' => [

        'authManager' => [
            'class' => 'yii\rbac\DbManager',
            'itemTable' => '{{%rbac_auth_item}}',
            'itemChildTable' => '{{%rbac_auth_item_child}}',
            'assignmentTable' => '{{%rbac_auth_assignment}}',
            'ruleTable' => '{{%rbac_auth_rule}}'
        ],

        'cache' => [
            'class' => 'yii\caching\DummyCache',
        ],

        'commandBus' => [
            'class' => '\trntv\tactician\Tactician',
            'commandNameExtractor' => '\League\Tactician\Handler\CommandNameExtractor\ClassNameExtractor',
            'methodNameInflector' => '\League\Tactician\Handler\MethodNameInflector\HandleInflector',
            'commandToHandlerMap' => [
                'common\commands\command\SendEmailCommand' => '\common\commands\handler\SendEmailHandler',
                'common\commands\command\AddToTimelineCommand' => '\common\commands\handler\AddToTimelineHandler',
            ]
        ],

        'formatter'=>[
            'class'=>'yii\i18n\Formatter'
        ],

        'glide' => [
            'class' => 'trntv\glide\components\Glide',
            'sourcePath' => '@storage/web/source',
            'cachePath' => '@storage/cache',
            'urlManager' => 'urlManagerStorage',
            'maxImageSize' => getenv('GLIDE_MAX_IMAGE_SIZE'),
            'signKey' => getenv('GLIDE_SIGN_KEY')
        ],

        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
	    'viewPath' => '@app/mail',
            'useFileTransport' => false,
	    'transport' => [
		'class' => 'Swift_SmtpTransport',
		'host' => 'smtp.gmail.com',
		'username' => 'cascapinfo@gmail.com',
		'password' => 'cascap2013',
		'port' => '587',
		'encryption' => 'tls',
	    ],
        ],

        'db'=>[
            'class'=>'yii\db\Connection',
            'dsn' => getenv('DB_DSN'),
            'username' => getenv('DB_USERNAME'),
            'password' => getenv('DB_PASSWORD'),
            'tablePrefix' => getenv('DB_TABLE_PREFIX'),
            'charset' => 'utf8',
            'enableSchemaCache' => YII_ENV_PROD,
        ],
        
        'dbreport1'=>[
            'class'=>'yii\db\Connection',
            'dsn' => getenv('DB_DSNR1'),
            'username' => getenv('DB_USERNAME'),
            'password' => getenv('DB_PASSWORD'),
            'tablePrefix' => getenv('DB_TABLE_PREFIX'),
            'charset' => 'utf8',
            'enableSchemaCache' => YII_ENV_PROD,
        ],
        
        /// add cascap log connection
        
        'dblog'=>[
            'class'=>'yii\db\Connection',
            'dsn' => getenv('DB_DSN99'),
            'username' => getenv('DB_USERNAME99'),
            'password' => getenv('DB_PASSWORD99'),
            'tablePrefix' => getenv('DB_TABLE_PREFIX99'),
            'charset' => 'utf8',
            'enableSchemaCache' => YII_ENV_PROD,
        ],
        
        'dbutf8mb4'=>[
            'class'=>'yii\db\Connection',
            'dsn' => getenv('DB_DSN'),
            'username' => getenv('DB_USERNAME'),
            'password' => getenv('DB_PASSWORD'),
            'tablePrefix' => getenv('DB_TABLE_PREFIX'),
            'charset' => 'utf8mb4',
            'enableSchemaCache' => YII_ENV_PROD,            
        ],
        'dbcascap'=>[
            'class'=>'yii\db\Connection',
            'dsn' => getenv('DB_DSN2'),
            'username' => getenv('DB_USERNAME2'),
            'password' => getenv('DB_PASSWORD2'),
            'charset' => 'latin1',
        ],
	
	'dbbot'=>[
            'class'=>'yii\db\Connection',
            'dsn' => getenv('DB_DSN8'),//3=18 , 8=8
            'username' => getenv('DB_USERNAME8'),
            'password' => getenv('DB_PASSWORD8'),
            'charset' => 'utf8',
        ],
        'dbbot_ip8'=>[
            'class'=>'yii\db\Connection',
            'dsn' => getenv('DB_DSN8'),
            'username' => getenv('DB_USERNAME8'),
            'password' => getenv('DB_PASSWORD8'),
            'charset' => 'utf8',
        ],
	'dbbot2'=>[
            'class'=>'yii\db\Connection',
            'dsn' => getenv('DB_DSN4'),
            'username' => getenv('DB_USERNAME4'),
            'password' => getenv('DB_PASSWORD4'),
            'charset' => 'utf8',
        ],
	'dbtdc2'=>[
            'class'=>'yii\db\Connection',
            'dsn' => getenv('DB_DSN8'),
            'username' => getenv('DB_USERNAME8'),
            'password' => getenv('DB_PASSWORD8'),
            'charset' => 'tis620',
        ],        
	'dbtdc'=>[
            'class'=>'yii\db\Connection',
            'dsn' => getenv('DB_DSN5'),
            'username' => getenv('DB_USERNAME5'),
            'password' => getenv('DB_PASSWORD5'),
            'charset' => 'utf8',
        ],
	'dbnemo'=>[
            'class'=>'yii\db\Connection',
            'dsn' => getenv('DB_DSN6'),
            'username' => getenv('DB_USERNAME6'),
            'password' => getenv('DB_PASSWORD6'),
            'charset' => 'utf8',
        ],        
        'dbcascaputf8'=>[
            'class'=>'yii\db\Connection',
            'dsn' => getenv('DB_DSN7'),
            'username' => getenv('DB_USERNAME7'),
            'password' => getenv('DB_PASSWORD7'),
            'charset' => 'utf8',
        ],
	'dbsr'=>[
            'class'=>'yii\db\Connection',
            'dsn' => 'mysql:host=61.19.254.18;port=3306;dbname=surgery',
            'username' => 'webservice',
            'password' => 'webservice!@#$%',
            'charset' => 'utf8',
        ],
	'dbdmp'=>[
            'class'=>'yii\db\Connection',
            'dsn' => 'mysql:host=61.19.254.15;port=3306;dbname=dpmcloud',
            'username' => 'webmaster',
            'password' => 'd@t@b@se!@#$%',
            'charset' => 'utf8',
        ],
	'dbckd'=>[
            'class'=>'yii\db\Connection',
            'dsn' => 'mysql:host=61.19.254.10;port=3306;dbname=tdc_data',
            'username' => 'webservice',
            'password' => 'webservice!@#$%',
            'charset' => 'utf8',
        ],
	'dbwebs1'=>[
            'class'=>'yii\db\Connection',
            'dsn' => 'mysql:host=61.19.254.8;port=3306;dbname=buffe_webservice',
            'username' => 'webservice',
            'password' => 'webservice!@#$%',
            'charset' => 'utf8',
        ],
	'dbsvr1'=>[
            'class'=>'yii\db\Connection',
            'dsn' => 'mysql:host=61.19.254.8;port=3306;dbname=tdc_data',
            'username' => 'webservice',
            'password' => 'webservice!@#$%',
            'charset' => 'utf8',
        ],
	'dbsvr2'=>[
            'class'=>'yii\db\Connection',
            'dsn' => 'mysql:host=61.19.254.9;port=3306;dbname=tdc_data',
            'username' => 'webservice',
            'password' => 'webservice!@#$%',
            'charset' => 'utf8',
        ],
	'dbsvr3'=>[
            'class'=>'yii\db\Connection',
            'dsn' => 'mysql:host=61.19.254.10;port=3306;dbname=tdc_data',
            'username' => 'webservice',
            'password' => 'webservice!@#$%',
            'charset' => 'utf8',
        ],
	'dbsvr4'=>[
            'class'=>'yii\db\Connection',
            'dsn' => 'mysql:host=61.19.254.11;port=3306;dbname=tdc_data',
            'username' => 'webservice',
            'password' => 'webservice!@#$%',
            'charset' => 'utf8',
        ],
	/*
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                'db'=>[
                    'class' => 'yii\log\DbTarget',
                    'levels' => ['error', 'warning'],
                    'except'=>['yii\web\HttpException:*', 'yii\i18n\I18N\*'],
                    'prefix'=>function () {
                        $url = !Yii::$app->request->isConsoleRequest ? Yii::$app->request->getUrl() : null;
                        return sprintf('[%s][%s]', Yii::$app->id, $url);
                    },
                    'logVars'=>[],
                    'logTable'=>'{{%system_log}}'
                ]
            ],
        ],
*/
        'i18n' => [
            'translations' => [
                'app'=>[
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath'=>'@common/messages',
                ],
                '*'=> [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath'=>'@common/messages',
                    'fileMap'=>[
                        'common'=>'common.php',
                        'backend'=>'backend.php',
                        'frontend'=>'frontend.php',
                    ]
                ],
		
                // Uncomment this code to use DbMessageSource
                 '*'=> [
                    'class' => 'yii\i18n\DbMessageSource',
                    'sourceMessageTable'=>'{{%i18n_source_message}}',
                    'messageTable'=>'{{%i18n_message}}',
                    'enableCaching' => YII_ENV_DEV,
                    'cachingDuration' => 3600
                ],
                

            ],
        ],

        'fileStorage' => [
            'class' => '\trntv\filekit\Storage',
            'baseUrl' => '@storageUrl/source',
            'filesystem' => [
                'class' => 'common\components\filesystem\LocalFlysystemBuilder',
                'path' => '@storage/web/source'
            ],
            'as log' => [
                'class' => 'common\behaviors\FileStorageLogBehavior',
                'component' => 'fileStorage'
            ]
        ],

        'keyStorage' => [
            'class' => 'common\components\keyStorage\KeyStorage'
        ],

        'urlManagerBackend' => \yii\helpers\ArrayHelper::merge(
            [
                'hostInfo' => Yii::getAlias('@backendUrl')
            ],
            require(Yii::getAlias('@backend/config/_urlManager.php'))
        ),
        'urlManagerFrontend' => \yii\helpers\ArrayHelper::merge(
            [
                'hostInfo'=>Yii::getAlias('@frontendUrl')
            ],
            require(Yii::getAlias('@frontend/config/_urlManager.php'))
        ),
        'urlManagerStorage' => \yii\helpers\ArrayHelper::merge(
            [
                'hostInfo'=>Yii::getAlias('@storageUrl')
            ],
            require(Yii::getAlias('@storage/config/_urlManager.php'))
        )
    ],
    'params' => [
        'adminEmail' => getenv('ADMIN_EMAIL'),
        'robotEmail' => getenv('ROBOT_EMAIL'),
        'availableLocales'=>[
            'en-US'=>'English (US)',
            'ru-RU'=>'Русский (РФ)',
            'uk-UA'=>'Українська (Україна)',
            'es' => 'Español',
	    //'th'=> 'ไทย'
        ],
    ],
];

if (YII_ENV_PROD) {
    $config['components']['cache'] = [
        'class' => 'yii\caching\FileCache',
        'cachePath' => '@common/runtime/cache'
    ];

    /*
    $config['components']['log']['targets']['email'] = [
        'class' => 'yii\log\EmailTarget',
        'except' => ['yii\web\HttpException:*'],
        'levels' => ['error', 'warning'],
        'message' => ['from' => getenv('ROBOT_EMAIL'), 'to' => getenv('ADMIN_EMAIL')]
    ];
    */
}

if (YII_ENV_DEV) {
    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class'=>'yii\gii\Module'
    ];
}

return $config;
