<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace appxq\sdii\utils;

/**
 * Description of TothaiDate
 *
 * @author jokeclancool
 */
class ToDate {
    public function ToThaiDate($date){
        $month=['มกราคม','กุมภาพันธ์','มีนาคม','เมษายน','พฤษภาคม','มิถุนายน','กรกฎาคม','สิงหาคม','กันยายน','ตุลาคม','พฤศจิกายน','ธันวาคม'];
        $date = date($date);
        $dateArr = explode('-', $date);
        $dateOut['y'] = $dateArr[0] + 543;
        $dateOut['m'] = $month[$dateArr[1]-1];
        $dateOut['d'] = $dateArr[2];
        //VarDumper::dump($dateArr);
        
        return $dateOut;
    }
    
    public function ToEngDate($date){
        $month=['January','February','March','April','May','June','July','August','September','October','November','December'];
        $date = date('Y-m-d',$date);
         $dateArr = explode('-', $date);
        $dateOut['y'] = $dateArr[0];
        $dateOut['m'] = $month[${$dateArr[1]-1}];
        $dateOut['d'] = $dateArr[2];
        //VarDumper::dump($dateArr);
        
        return $dateOut;
    }
}

