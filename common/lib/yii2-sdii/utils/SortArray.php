<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace appxq\sdii\utils;

/**
 * Description of SortArray
 *
 * @author jokeclancool
 */
class SortArray {
    /*
    public static function sortingArray2DESC($array, $key) {
        $arrayNew = $array;
        for ($i = 0; $i < count($array); $i++) {
            for ($x = 0; $x < count($arrayNew); $x++) {
                if ($array[$i][$key] < $arrayNew[$x][$key]) {
                    $dataStore = $array[$i];
                    $array[$i] = $arrayNew[$x];
                    $arrayNew[$x] = $dataStore;
                }
            }
        }
        return $array;
    }
     * */
     

    public static function sortingArrayDESC($array, $key) {
        $data = array();
        foreach ($array as $key2 => $row) {
            $data[$key2] = $row[$key];
        }
        \array_multisort($data, SORT_DESC, $array);
        
        return $array;
    }
    public static function sortingArrayASC($array, $key) {
        $data = array();
        foreach ($array as $key2 => $row) {
            $data[$key2] = $row[$key];
        }
        \array_multisort($data, SORT_ASC, $array);
        
        return $array;
    }
    
    public static function sortDESC($array) {
        $data = array();
        foreach ($array as $key2 => $row) {
            $data[$key2] = $row[$key];
        }
        \array_multisort($data, SORT_DESC, $array);
        
        return $array;
    }
    public static function sortASC($array) {
        $data = array();
        foreach ($array as $key2 => $row) {
            $data[$key2] = $row[$key];
        }
        \array_multisort($data, SORT_ASC, $array);
        
        return $array;
    }

}
