<?php

Yii::import('zii.widgets.jui.CJuiWidget');

/**
 * GCalendar extension for Yii.
 *
 * @author AppXQ <iencoded@gmail.com>
 * @version 0.1
 *
 */
class GCalendar extends CJuiWidget {

	/**
	 * รูปแบบการแสดงผล
	 * @var string
	 */
	//public $view;
	
	/**
	 * popup Id in html 
	 * @var string
	 */
	public $popupId;
	public $loadFunc;
	public $linkFunc;
	/**
	 * add method function name
	 * @var string
	 */
	public $addFunc;
	/**
	 * update method function name
	 * @var string
	 * @param function(id, start, end, isallday, title)
	 */
	public $updateFunc;
	/**
	 * update method function name
	 * @var string
	 * @param function(id, start, end, isallday, title)
	 */
	public $calType = 'none';//support event,?...
	public $popupOp;
	
	public $readonly = false;
	/**
	 * Publishes the required assets
	 */
	public function init() {
		parent::init();
		$this->publishAssets();
	}

	/**
	 * Generates the required HTML and Javascript
	 */
	public function run() {

		//list($name, $id) = $this->resolveNameID();

		if (!isset($this->htmlOptions['id'])) {
			$this->htmlOptions['id'] = "calhead";
		}
		
		$this->options['readonly'] = $this->readonly;
		$options = CJavaScript::encode($this->options);

		$js = "gcalendar_init('{$this->htmlOptions['id']}', {$options} ". (($this->popupId)?", '{$this->popupId}'":'').(($this->addFunc)?", {$this->addFunc}":'').(($this->updateFunc)?", {$this->updateFunc}":'').(($this->linkFunc)?", {$this->linkFunc}":'').(($this->loadFunc)?", {$this->loadFunc}":'').");";
		
		Yii::app()->clientScript->registerScript(__CLASS__ . '#' . $this->htmlOptions['id'], $js, CClientScript::POS_READY);
		
		Yii::app()->clientScript->registerScript(__CLASS__ . '#CalType' . $this->htmlOptions['id'], "typeVal='".$this->calType."'; popupOp='".$this->popupOp."';", CClientScript::POS_READY);
		
		$this->render("calview", compact('htmlOptions'));
	}

	/**
	 * Publises and registers the required CSS and Javascript
	 * @throws CHttpException if the assets folder was not found
	 */
	public function publishAssets() {
		$assets = dirname(__FILE__) . '/assets';
		$baseUrl = Yii::app()->assetManager->publish($assets);
		if (is_dir($assets)) {
			//CSS File
			Yii::app()->clientScript->registerCssFile($baseUrl . '/css/calendar.css');
			Yii::app()->clientScript->registerCssFile($baseUrl . '/css/dailog.css');
			Yii::app()->clientScript->registerCssFile($baseUrl . '/css/dp.css');
			Yii::app()->clientScript->registerCssFile($baseUrl . '/css/alert.css');
			Yii::app()->clientScript->registerCssFile($baseUrl . '/css/main.css');
			//Javascript File 
			Yii::app()->clientScript->registerScriptFile($baseUrl . '/js/Common.js', CClientScript::POS_END);
			//Yii::app()->clientScript->registerScriptFile($baseUrl . '/js/datepicker_lang_US.js', CClientScript::POS_END);
			//Yii::app()->clientScript->registerScriptFile($baseUrl . '/js/jquery.datepicker.js', CClientScript::POS_END);
			Yii::app()->clientScript->registerScriptFile($baseUrl . '/js/jquery.alert.js', CClientScript::POS_END);
			//Yii::app()->clientScript->registerScriptFile($baseUrl . '/js/jquery.ifrmdailog.js', CClientScript::POS_END);
			Yii::app()->clientScript->registerScriptFile($baseUrl . '/js/wdCalendar_lang_TH.js', CClientScript::POS_END);
			Yii::app()->clientScript->registerScriptFile($baseUrl . '/js/jquery.calendar.js', CClientScript::POS_END);
			Yii::app()->clientScript->registerScriptFile($baseUrl . '/js/gcalendar.js', CClientScript::POS_END);
			//Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/appxqCore.js', CClientScript::POS_END);

		} else {
			throw new CHttpException(500, __CLASS__ . ' - Error: Couldn\'t find assets to publish.');
		}
	}

}
