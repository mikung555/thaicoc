<?php
namespace common\lib\damasac\widgets;

use common\lib\damasac\assets\checkbox\DMSCheckboxAsset;
use Yii;
use yii\helpers\Json;
use yii\helpers\VarDumper;
use yii\widgets\InputWidget;
use yii\helpers\Html;
use yii\helpers\BaseHtml;
/**
 * Created by PhpStorm.
 * User: Balz PC
 * Date: 31-Aug-15
 * Time: 12:52
 */
class DMSCheckboxWidget extends InputWidget
{
    public $id;

    public $clientOptions;

    public $pluginOptions;

    public function init()
    {
        parent::init();
        if (!isset($this->options['id'])) {
            $this->options['id'] = $this->getId();
        }
        DMSCheckboxAsset::register($this->getView());
    }
    public function run(){
        $this->javascriptClient();
        $this->options['label'] = $this->model->getAttributeLabel($this->attribute);
        $this->options['class'] = 'checkbox-custom';

        if ($this->hasModel()) {

			echo "<input type='radio' id='radio-1-1' name='radio-1-set' class='checkbox-custom' />";
		    echo "<label for='radio-1-1' class='checkbox-custom-labelz'>Test</label>";
//          echo "</div>";
//            echo Html::activeCheckbox($this->model,$this->attribute,$this->options);
//            echo "<input type='checkbox' class='myClass' name='' id='' data-label='".$this->options['data-label']."'>";
        } else {
            echo Html::checkbox($this->model,$this->attribute,$this->options);
        }

    }
    public function setClientOptions(){
        $this->pluginOptions = Json::htmlEncode($this->clientOptions);
    }
    public function javascriptClient(){
        $this->setClientOptions();
//        $this->getView()->registerJs("
//
//            $('input.myClass').prettyCheckable(
//
//            )
//
//        ");
    }

}