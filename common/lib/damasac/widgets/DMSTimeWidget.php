<?php
/**
 * Created by PhpStorm.
 * User: balz
 * Date: 1/9/2558
 * Time: 16:39
 */

namespace common\lib\damasac\widgets;

use common\lib\damasac\assets\time\DMSTimeAsset;
use yii\helpers\Json;
use yii\helpers\Html;
use yii\widgets\InputWidget;

class DMSTimeWidget extends InputWidget
{
    public $id;

    public $name;

    public $options = ['class'=>'form-control'];

    public $clientOptions = ['minuteStep'=>1,'showMeridian'=>false,'showSeconds'=>true];

    public $pluginOptions;

    public function init()
    {
        parent::init();
        if (!isset($this->options['id'])) {
            $this->options['id'] = $this->getId();
        }
        DMSTimeAsset::register($this->getView());
    }
    public function run(){
        $this->javascriptClient();
        echo Html::beginTag('div',['class'=>'input-group']);
        if ($this->hasModel()) {
            echo Html::activeTextInput($this->model, $this->attribute, $this->options);
        } else {
            echo Html::textInput($this->name, $this->value, $this->options);
        }
        echo Html::beginTag('div',['class'=>'input-group-addon']);
        echo Html::beginTag('i',['class'=>'fa fa-clock-o']);
        echo Html::endTag('i');
        echo Html::endTag('div');
        echo Html::endTag('div');
    }
    public function setClientOptions(){
        $this->pluginOptions = Json::htmlEncode($this->clientOptions);
    }
    public function javascriptClient(){
        $this->setClientOptions();
        $this->getView()->registerJs("
            $('#".$this->options["id"]."').timepicker(
                ".$this->pluginOptions."
            );
        ");
    }
}