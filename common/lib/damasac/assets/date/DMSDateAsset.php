<?php
namespace common\lib\damasac\assets\date;

use yii\web\AssetBundle;

class DMSDateAsset extends AssetBundle
{
    public $sourcePath='@lib/damasac/assets/date';

    public $css=[
        'css/datepicker.css'
    ];

    public $js=[
        'js/bootstrap-datepicker.js',
        'js/bootstrap-datepicker-thai.js',
        'js/locales/bootstrap-datepicker.th.js',
        'js/inputmask.js',
        'js/jquery.inputmask.js',
    ];

    public $depends=[
        'yii\jui\JuiAsset',
    ];
}
