<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "ezform_report_editor".
 *
 * @property integer $id
 * @property string $ezf_id
 * @property string $ezf_report_name
 */
class EzformReportEditor extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ezform_report_editor';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ezf_id'], 'integer'],
            [['ezf_report_name'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ezf_id' => 'Ezf ID',
            'ezf_report_name' => 'Ezf Report Name',
        ];
    }
}
