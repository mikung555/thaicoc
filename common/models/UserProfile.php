<?php

namespace common\models;

use trntv\filekit\behaviors\UploadBehavior;
use Yii;

/**
 * This is the model class for table "user_profile".
 *
 * @property integer $user_id
 * @property integer $locale
 * @property string $firstname
 * @property string $middlename
 * @property string $lastname
 * @property string $picture
 * @property string $avatar
 * @property string $avatar_path
 * @property string $avatar_base_url
 * @property integer $gender
 * @property string $hostpialcode
 *
 * @property User $user
 */
class UserProfile extends \yii\db\ActiveRecord
{
    const GENDER_MALE = 1;
    const GENDER_FEMALE = 2;

    public $picture;

    public function behaviors()
    {
        return [
            'picture' => [
                'class' => UploadBehavior::className(),
                'attribute' => 'picture',
                'pathAttribute' => 'avatar_path',
                'baseUrlAttribute' => 'avatar_base_url'
            ]
        ];
    }


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user_profile}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id','cid'], 'required'],
            //[['user_id', 'gender'], 'integer'],
            [['gender'], 'in', 'range'=>[self::GENDER_FEMALE, self::GENDER_MALE]],
            [['firstname','lastname','email','telephone','gender'],'required','message'=>'กรุณาระบุ'],
            [['firstname', 'middlename', 'lastname', 'avatar_path', 'avatar_base_url'], 'string'],
            ['locale', 'default', 'value' => Yii::$app->language],
            //['locale', 'in', 'range' => array_keys(Yii::$app->params['availableLocales'])],
            [ 'picture', 'safe'],
            [['volunteer_status','volunteer', 'approved','enroll_key', 'inout','sitecode', 'department', 'status','status_personal','status_manager','status_other','department_nation_text','department_area','department_area_text','department_province','department_province_text','department_amphur',
            'department_amphur_text','department_group','activate', 'address_province', 'address_amphur', 'address_tambon',  'address_text','title'
            ], 'safe'],
            ['telephone','safe'],
	    [['citizenid_file', 'secret_file'], 'file', 'skipOnEmpty' => true, 'extensions' => ['pdf','png','jpg','jpeg']],
            [['email'],'email'],
	    //[['email'], 'unique'],
        ];
    }

    public static function getNotAdminsite() {
	$sql = "SELECT user_profile.sitecode
		FROM user_profile INNER JOIN rbac_auth_assignment ON rbac_auth_assignment.user_id = user_profile.user_id
		WHERE rbac_auth_assignment.item_name = 'adminsite' 
		GROUP BY user_profile.sitecode";
	
	return Yii::$app->db->createCommand($sql)->queryAll();
    }
    
    public static function getAdminsite($sitecode) {
	$sql = "SELECT CONCAT(user_profile.firstname, ' ', user_profile.lastname) AS fullname,
		user_profile.email,
		user_profile.telephone
		FROM user_profile INNER JOIN rbac_auth_assignment ON rbac_auth_assignment.user_id = user_profile.user_id
		WHERE rbac_auth_assignment.item_name = 'adminsite' AND user_profile.sitecode = :sitecode
		";
	
	return Yii::$app->db->createCommand($sql, [':sitecode'=>$sitecode])->queryAll();
    }
    
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'cid'=>Yii::t('common','เลขบัตรประจำตัวประชาชน 13 หลัก'),
            'user_id' => Yii::t('common', 'User ID'),
	    'title' => Yii::t('common', 'คำนำหน้า'),
            'firstname' => Yii::t('common', 'ชื่อ'),
            'middlename' => Yii::t('common', 'Middlename'),
            'lastname' => Yii::t('common', 'นามสกุล'),
            'locale' => Yii::t('common', 'Locale'),
            'picture' => Yii::t('common', 'Picture'),
            'gender' => Yii::t('common', 'เพศ'),
            'sitecode'=>Yii::t('common','เลือกหน่วยงาน'),
            'email'=>Yii::t('common','อีเมล์'),
            'telephone'=>Yii::t('common','เบอร์โทรศัพท์'),
            'address_amphur' => Yii::t('common','อำเภอ'),
            'address_province' => Yii::t('common','จังหวัด'),
            'address_tambon' => Yii::t('common','ตำบล'),
            'address_text' => Yii::t('common','ที่อยู่'),
	    'citizenid_file' => Yii::t('common','สำเนาบัตรประชาชน'),
	    'secret_file' => Yii::t('common','เอกสารรักษาความลับ '),
	    'inout' => Yii::t('common','ประเภท'),
	    'enroll_key' => Yii::t('common','Enroll key'),
	    'approved'=>'สถานะการตรวจสอบ',
	    
	    'volunteer_status'=>'อาสาสมัคร',
	    'volunteer'=>'เลือกอาสาสมัคร',
        ];
    }
    
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        Yii::$app->session->setFlash('forceUpdateLocale');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getFullName()
    {
        if ($this->firstname || $this->lastname) {
            return implode(' ', [$this->firstname, $this->lastname]);
        }
        return null;
    }

    public function getAvatar()
    {
        return $this->avatar_path
            ? Yii::getAlias($this->avatar_base_url . '/' . $this->avatar_path)
            : false;
    }
    public function getAvatarById($uid)
    {
        $user = self::find()->select('avatar_path, avatar_base_url')->where(['user_id' => $uid])->one();
        return $user->avatar_path ? Yii::getAlias($user->avatar_base_url . '/' . $user->avatar_path) : false;
    }
}
